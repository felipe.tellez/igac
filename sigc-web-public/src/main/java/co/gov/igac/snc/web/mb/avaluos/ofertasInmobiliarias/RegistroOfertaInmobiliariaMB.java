/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaAreaUsoExclusivo;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaRecursoHidrico;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioComunal;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioPublico;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.util.ECondicionJuridica;
import co.gov.igac.snc.persistence.util.ECondicionJuridicaNumPredial;
import co.gov.igac.snc.persistence.util.EOfertaDestino;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.persistence.util.EOfertaFormaLote;
import co.gov.igac.snc.persistence.util.EOfertaFormaNegociacion;
import co.gov.igac.snc.persistence.util.EOfertaServicioComunal;
import co.gov.igac.snc.persistence.util.EOfertaTipoInmueble;
import co.gov.igac.snc.persistence.util.EOfertaTipoInvestigacion;
import co.gov.igac.snc.persistence.util.EOfertaUbicacionOficina;
import co.gov.igac.snc.persistence.util.EOfertaUnidadArea;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.enumsOfertasInmob.EOfertaTipoOferta;
import co.gov.igac.snc.persistence.util.enumsOfertasInmob.EOfertaTipoPredio;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.EWVVentanasOfertaTipoInmueble;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * managed bean para el registro de las oferts inmobiliarias
 *
 * @author pedro.garcia
 * @modified juan.agudelo
 * @modified christian.rodriguez
 *
 * @modified rodrigo.hernandez 15-06-2012 Adición de campos: - recolectorId -
 * banderaCreacionOfertaDesdeCargaAlSNC - regionCapturaOfertaId Adición de método: -
 * asignarRegionCapturaOferta Modificación a métodos: - guardarOfertaInmobiliaria - terminar
 */
@Component("registroOfertaInmob")
@Scope("session")
public class RegistroOfertaInmobiliariaMB extends SNCManagedBean {

    private static final long serialVersionUID = -5179769835166965075L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistroOfertaInmobiliariaMB.class);

    // ------------------ services ----------
    @Autowired
    private GeneralMB generalMBService;

    @Autowired
    private IContextListener contextoWebApp;

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamentoCod;
    private String selectedMunicipioCod;
    private String selectedDepartamentoNom;
    private String selectedMunicipioNom;

    /**
     * datos de la oferta
     */
    private Departamento departamentoOferta;
    private Municipio municipioOferta;
    private OfertaInmobiliaria selectedOfertaInmobiliaria;

    /**
     * Información de la oferta inmobiliaria en detallo
     */
    private OfertaInmobiliaria selectedOfertaInmobiliariaDetalle;

    /**
     * Datos de oferta de servicios públicos
     */
    private DualListModel<String> serviciosPublicos;
    private String observacionesServicios;

    /**
     * información de locales asociados a la oferta inmobiliaria
     */
    private List<OfertaInmobiliaria> locales;

    /**
     * información de fincas asociados a la oferta inmobiliaria
     */
    private List<OfertaInmobiliaria> fincas;

    /**
     * información de casas / Apartamentos / Suites hoteleras asociados a la oferta inmobiliaria
     */
    private List<OfertaInmobiliaria> casasAptosSHs;

    /**
     * información bodegas asociadas a la oferta inmobiliaria
     */
    private List<OfertaInmobiliaria> bodegas;

    /**
     * información de parqueaderos asociados a la oferta inmobiliaria
     */
    private List<OfertaInmobiliaria> parqueaderos;

    /**
     * información de oficinas / consultorios asociados a la oferta inmobiliaria
     */
    private List<OfertaInmobiliaria> oficinasConsultorios;

    /**
     * Oferta inmobiliaria asociada a la oferta inmobiliaria
     */
    private OfertaInmobiliaria selectedOfertaOferta;

    /**
     * Fotografía seleccionada para cargar en el registro fotográfico
     */
    private Fotografia selectedFotografia;

    /**
     * Archivo a cargar en la carpeta temporal
     */
    private File archivoResultado;

    /**
     * Mensaje informativo antes de cargar fotos de la oferta principal
     */
    private String mensajeSalidaFotosOP;

    /**
     * Mensaje de confirmación para asociar fotografías a oferta principal
     */
    private String mensajeSalidaConfirmationDialogFOP = "Al" +
         " aceptar cargar fotos de la oferta inmobiliaria principal" +
         " no podra editar ni agregar otras ofertas inmobiliarias." +
         " Esta seguro que desea continuar?";

    /**
     * Predio que se esta adicionando a la tabla de predios de la oferta
     */
    private OfertaPredio selectedOfertaPredio;

    private Predio[] selectedPredios;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportes = ReportesUtil.getInstance();

    // -------------------- VARIABLES USADAS PARA INTEGRACION CON VISOR
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    // ---------variables booleanas para los proyectos de ofertas
    // inmobiliarias-------
    /**
     * Variable booleana donde se almacena si la oferta inmobiliaria tiene mezzanine
     */
    private boolean mezzanine;

    /**
     * Determina si tiene predios desactualizados para enviar las observaciones a catastro
     */
    private boolean tienePrediosDesactualizados;

    private boolean banderaActivarServicioComunalOtro;
    /**
     * Variable booleana donde se almacena si la oferta inmobiliaria tiene cableado de red
     */
    private boolean cableadoRed;

    /**
     * Variable booleana donde se almacena si la oferta inmobiliaria está en zona franca
     */
    private boolean zonaFranca;

    /**
     * Variable booleana donde se almacena si tiene sevidumbre
     */
    private boolean servidumbre;

    /**
     * Variable booleana donde se almacena si la oferta inmobiliaria tiene puente grua
     */
    private boolean puenteGrua;

    /**
     * Variable booleana donde se almacena si el proyecto finca tiene riego
     */
    private boolean riego;

    /**
     * Variable booleana donde se almacena si el proyecto finca tiene recursos hídricos
     */
    private boolean recursosHidricos;

    /**
     * Variable booleana donde se almacena si el proyecto finca tiene casa
     */
    private boolean casa;

    /**
     * Variable booleana donde se almacena si el proyecto finca tiene anexos
     */
    private boolean anexos;

    /**
     * Lista con los predios disponibles paara la región de captura de ofertas
     */
    private List<Predio> prediosRegionCaptura;

    // ---------- Datos para los pick's list de Propiedad Horizontal
    // ------------
    private DualListModel<String> areasUsoExclusivo;
    private List<String> sourceAreasUsoExclusivo;
    private List<String> targetAreasUsoExclusivo;

    private DualListModel<String> serviciosComunalesOferta;
    private List<String> sourceServiciosComunalesOferta;
    private List<String> targetServiciosComunalesOferta;

    // ---------- Datos para el pick list de Proyecto Finca ------------
    private DualListModel<String> ofertasRecursosHidricos;
    private List<String> sourceOfertasRecursosHidricos;
    private List<String> targetOfertasRecursosHidricos;

    /**
     * Lista de los servicios públicos seleccionados
     */
    private ArrayList<String> targetServiciosPublicos;

    /**
     * Lista de los servicios públicos iniciales
     */
    private ArrayList<String> sourceServiciosPublicos;

    /**
     * Lista de ofertas eliminadas
     */
    private List<OfertaInmobiliaria> listaOfertasEliminadas;

    /**
     * Identificador del WV de la ventana de ingreso de datos de la oferta inmobiliaria
     */
    private String selectedVentanIngreso;

    private Fotografia fotoAEliminar;

    /**
     * Lista de items para oferta tipo inmueble usado
     */
    private List<SelectItem> ofertaTipoInmuebleUsadoV;

    /**
     * Lista de items para oferta tipo inmueble proyecto
     */
    private List<SelectItem> ofertaTipoInmuebleProyectoV;

    /**
     * Lista de items para oferta frente a vías
     */
    private List<SelectItem> ofertaFrenteViasV;

    /**
     * Lista de items para oferta estado vías
     */
    private List<SelectItem> ofertaEstadoViasV;

    /**
     * Almacena el numero predial que se va a agregar a la tabla de predios
     */
    private String currentNumeroPredial;

    /**
     * Almacena el Id de la RegionCapturaOferta
     */
    private Long regionCapturaOfertaId;

    /**
     * Almacena la región de captura oferta
     */
    private RegionCapturaOferta regionCapturaOferta;

    /**
     * Almacena id del recolector asociado
     */
    private String recolectorId;

    // ---------- flags --------------
    /**
     * Bandera que deshabilita la funcionalidad de agregar y editar ofertas inmobilirias asociadas a
     * la oferta principal para cargar las imagenes de la misma
     */
    private boolean banderaCargarFotosOfertaPrincipal;

    /**
     * Bandera que determina si la oferta inmmobiliaria es nueva (de proyectos) o usada
     */
    private boolean banderaOfertaNueva;

    /**
     * Bandera que determina si la oferta esta en propiedad horizontal
     */
    private boolean banderaPropiedadHorizontal;

    /**
     * Bandera que determina si la oferta es una casa, un apartamento o una suite hotelera
     */
    private boolean banderaCasaApartamentoSuiteHotelera;

    /**
     * Bandera que determina si la oferta es un lote o una casa lote
     */
    private boolean banderaLoteCasaLote;

    /**
     * Bandera que determina si la oferta es un local
     */
    private boolean banderaLocal;

    /**
     * Bandera que determina si la oferta es una oficina o un consultorio
     */
    private boolean banderaOficinaConsultorio;

    /**
     * Bandera que determina si la oferta es una bodega
     */
    private boolean banderaBodega;

    /**
     * Bandera que determina si la oferta es una finca
     */
    private boolean banderaFinca;

    /**
     * Bandera que determina si la oferta es un edificio
     */
    private boolean banderaEdificio;

    /**
     * Bandera que determina si la oferta es un parqueadero
     */
    private boolean banderaParqueadero;

    /**
     * Bandera que determina si las imagenes se desean guardar o no las fotos asociadas a la oferta
     */
    private boolean banderaGuardarFotografias;

    /**
     * Bandera que indica si los datos se estan consultando de una oferta inmobiliaria o son de
     * ingreso
     */
    private boolean banderaEsConsulta;

    /**
     * Bandera que indica si se estÃ¡ editando una oferta exitente
     */
    private boolean banderaEsEdicion;

    /**
     * Bandera que indica si la lista de ofertas inmobiliarias se encontrara asociada a la
     * ofertaOferta en caso de ser true o a la ofertaInmobiliaria en caso de ser false
     */
    private boolean banderaFotografiaOfertaOferta;

    /**
     * Bandera que indica si se cargaron fotografias en el componente fotografico para habilitar el
     * boton de guardar
     */
    private boolean banderaFotografiasCargadas;

    /**
     * Bandera que indica si se selecciono tipo de oferta
     */
    private boolean banderaTipoOfertaSeleccionada;

    /**
     * Bandera que indica la visualización del panel de edición de fotografías
     */
    private boolean banderaPanelEdicionFotos;

    /**
     * Bandera que indica si el múnicipio seleccionado pertenece a un catastro descentralizado
     */
    private boolean banderaPerteneceACatastroDescentralizado;

    /**
     * Bandera que indica si este MB es llamado desde CargaOfertaAlSNCMB True - el MB se carga desde
     * CargaOfertaAlSNCMB False - el MB NO se carga desde CargaOfertaAlSNCMB
     */
    private boolean banderaCreacionOfertaDesdeCargaAlSNC;

    /**
     * Bandera que indica si este MB es llamado desde ConsultaOfertasInmobiliariasMB True - el MB se
     * carga desde ConsultaOfertasInmobiliarias False - el MB NO se carga desde
     * ConsultaOfertasInmobiliarias
     */
    private boolean banderaCreacionOfertaDesdeConsultaOfertas;

    /**
     * Bandera que indica si este MB es llamado desde ModificacionOfertasDevuletas
     */
    private boolean banderaCreacionOfertaDesdeModOfertasDevueltas;

    // -------------- methods ---------------
    public OfertaInmobiliaria getSelectedOfertaInmobiliariaDetalle() {
        return selectedOfertaInmobiliariaDetalle;
    }

    public void setSelectedOfertaInmobiliariaDetalle(
        OfertaInmobiliaria selectedOfertaInmobiliariaDetalle) {
        this.selectedOfertaInmobiliariaDetalle = selectedOfertaInmobiliariaDetalle;
    }

    public boolean isBanderaTipoOfertaSeleccionada() {
        return banderaTipoOfertaSeleccionada;
    }

    public void setBanderaTipoOfertaSeleccionada(
        boolean banderaTipoOfertaSeleccionada) {
        this.banderaTipoOfertaSeleccionada = banderaTipoOfertaSeleccionada;
    }

    public boolean isTienePrediosDesactualizados() {
        return tienePrediosDesactualizados;
    }

    public void setTienePrediosDesactualizados(
        boolean tienePrediosDesactualizados) {
        this.tienePrediosDesactualizados = tienePrediosDesactualizados;
    }

    public boolean isBanderaPerteneceACatastroDescentralizado() {
        return banderaPerteneceACatastroDescentralizado;
    }

    public void setBanderaPerteneceACatastroDescentralizado(
        boolean banderaPerteneceACatastroDescentralizado) {
        this.banderaPerteneceACatastroDescentralizado = banderaPerteneceACatastroDescentralizado;
    }

    public boolean isBanderaEsEdicion() {
        return banderaEsEdicion;
    }

    public void setBanderaEsEdicion(boolean banderaEsEdicion) {
        this.banderaEsEdicion = banderaEsEdicion;
    }

    public List<SelectItem> getOfertaTipoInmuebleProyectoV() {
        return ofertaTipoInmuebleProyectoV;
    }

    public String getMensajeSalidaConfirmationDialogFOP() {
        return mensajeSalidaConfirmationDialogFOP;
    }

    public void setMensajeSalidaConfirmationDialogFOP(
        String mensajeSalidaConfirmationDialogFOP) {
        this.mensajeSalidaConfirmationDialogFOP = mensajeSalidaConfirmationDialogFOP;
    }

    public String getMensajeSalidaFotosOP() {
        return mensajeSalidaFotosOP;
    }

    public void setMensajeSalidaFotosOP(String mensajeSalidaFotosOP) {
        this.mensajeSalidaFotosOP = mensajeSalidaFotosOP;
    }

    public boolean isBanderaCargarFotosOfertaPrincipal() {
        return banderaCargarFotosOfertaPrincipal;
    }

    public void setBanderaCargarFotosOfertaPrincipal(
        boolean banderaCargarFotosOfertaPrincipal) {
        this.banderaCargarFotosOfertaPrincipal = banderaCargarFotosOfertaPrincipal;
    }

    public void setOfertaTipoInmuebleProyectoV(
        List<SelectItem> ofertaTipoInmuebleProyectoV) {
        this.ofertaTipoInmuebleProyectoV = ofertaTipoInmuebleProyectoV;
    }

    public Fotografia getFotoAEliminar() {
        return this.fotoAEliminar;
    }

    public boolean isBanderaFotografiasCargadas() {
        return this.banderaFotografiasCargadas;
    }

    public boolean isBanderaPanelEdicionFotos() {
        return this.banderaPanelEdicionFotos;
    }

    public void setBanderaPanelEdicionFotos(boolean banderaPanelEdicionFotos) {
        this.banderaPanelEdicionFotos = banderaPanelEdicionFotos;
    }

    public void setBanderaFotografiasCargadas(boolean banderaFotografiasCargadas) {
        this.banderaFotografiasCargadas = banderaFotografiasCargadas;
    }

    public boolean isBanderaFotografiaOfertaOferta() {
        return this.banderaFotografiaOfertaOferta;
    }

    public void setBanderaFotografiaOfertaOferta(
        boolean banderaFotografiaOfertaOferta) {
        this.banderaFotografiaOfertaOferta = banderaFotografiaOfertaOferta;
    }

    public void setFotoAEliminar(Fotografia fotoAEliminar) {
        this.fotoAEliminar = fotoAEliminar;
    }

    public List<SelectItem> getOfertaTipoInmuebleUsadoV() {
        return this.ofertaTipoInmuebleUsadoV;
    }

    public void setOfertaTipoInmuebleUsadoV(
        List<SelectItem> ofertaTipoInmuebleUsadoV) {
        this.ofertaTipoInmuebleUsadoV = ofertaTipoInmuebleUsadoV;
    }

    public boolean isBanderaEsConsulta() {
        return banderaEsConsulta;
    }

    public void setBanderaEsConsulta(boolean banderaEsConsulta) {
        this.banderaEsConsulta = banderaEsConsulta;
    }

    public DualListModel<String> getServiciosPublicos() {
        return serviciosPublicos;
    }

    public List<OfertaInmobiliaria> getOficinasConsultorios() {
        return oficinasConsultorios;
    }

    public List<OfertaInmobiliaria> getListaOfertasEliminadas() {
        return listaOfertasEliminadas;
    }

    public String getSelectedVentanIngreso() {
        return selectedVentanIngreso;
    }

    public void setSelectedVentanIngreso(String selectedVentanIngreso) {
        this.selectedVentanIngreso = selectedVentanIngreso;
    }

    public void setListaOfertasEliminadas(
        List<OfertaInmobiliaria> listaOfertasEliminadas) {
        this.listaOfertasEliminadas = listaOfertasEliminadas;
    }

    public void setOficinasConsultorios(
        List<OfertaInmobiliaria> oficinasConsultorios) {
        this.oficinasConsultorios = oficinasConsultorios;
    }

    public Fotografia getSelectedFotografia() {
        return selectedFotografia;
    }

    public void setSelectedFotografia(Fotografia selectedFotografia) {
        this.selectedFotografia = selectedFotografia;
    }

    public List<OfertaInmobiliaria> getLocales() {
        return locales;
    }

    public void setLocales(List<OfertaInmobiliaria> locales) {
        this.locales = locales;
    }

    public List<OfertaInmobiliaria> getCasasAptosSHs() {
        return casasAptosSHs;
    }

    public void setCasasAptosSHs(List<OfertaInmobiliaria> casasAptosSHs) {
        this.casasAptosSHs = casasAptosSHs;
    }

    public List<OfertaInmobiliaria> getFincas() {
        return fincas;
    }

    public void setFincas(List<OfertaInmobiliaria> fincas) {
        this.fincas = fincas;
    }

    public List<OfertaInmobiliaria> getBodegas() {
        return bodegas;
    }

    public void setBodegas(List<OfertaInmobiliaria> bodegas) {
        this.bodegas = bodegas;
    }

    public List<OfertaInmobiliaria> getParqueaderos() {
        return parqueaderos;
    }

    public void setParqueaderos(List<OfertaInmobiliaria> parqueaderos) {
        this.parqueaderos = parqueaderos;
    }

    public OfertaInmobiliaria getSelectedOfertaOferta() {
        return selectedOfertaOferta;
    }

    public void setSelectedOfertaOferta(OfertaInmobiliaria selectedOfertaOferta) {
        this.selectedOfertaOferta = selectedOfertaOferta;
    }

    public boolean isBanderaPropiedadHorizontal() {
        return banderaPropiedadHorizontal;
    }

    public OfertaInmobiliaria getSelectedOfertaInmobiliaria() {
        return selectedOfertaInmobiliaria;
    }

    public void setSelectedOfertaInmobiliaria(
        OfertaInmobiliaria selectedOfertaInmobiliaria) {
        this.selectedOfertaInmobiliaria = selectedOfertaInmobiliaria;
    }

    public void setBanderaPropiedadHorizontal(boolean banderaPropiedadHorizontal) {
        this.banderaPropiedadHorizontal = banderaPropiedadHorizontal;
    }

    public boolean isBanderaCasaApartamentoSuiteHotelera() {
        return banderaCasaApartamentoSuiteHotelera;
    }

    public void setBanderaCasaApartamentoSuiteHotelera(
        boolean banderaCasaApartamentoSuiteHotelera) {
        this.banderaCasaApartamentoSuiteHotelera = banderaCasaApartamentoSuiteHotelera;
    }

    public boolean isBanderaLoteCasaLote() {
        return banderaLoteCasaLote;
    }

    public void setBanderaLoteCasaLote(boolean banderaLoteCasaLote) {
        this.banderaLoteCasaLote = banderaLoteCasaLote;
    }

    public boolean isBanderaLocal() {
        return banderaLocal;
    }

    public void setBanderaLocal(boolean banderaLocal) {
        this.banderaLocal = banderaLocal;
    }

    public boolean isBanderaOficinaConsultorio() {
        return banderaOficinaConsultorio;
    }

    public void setBanderaOficinaConsultorio(boolean banderaOficinaConsultorio) {
        this.banderaOficinaConsultorio = banderaOficinaConsultorio;
    }

    public boolean isBanderaBodega() {
        return banderaBodega;
    }

    public void setBanderaBodega(boolean banderaBodega) {
        this.banderaBodega = banderaBodega;
    }

    public boolean isBanderaFinca() {
        return banderaFinca;
    }

    public void setBanderaFinca(boolean banderaFinca) {
        this.banderaFinca = banderaFinca;
    }

    public boolean isBanderaEdificio() {
        return banderaEdificio;
    }

    public void setBanderaEdificio(boolean banderaEdificio) {
        this.banderaEdificio = banderaEdificio;
    }

    public boolean isBanderaParqueadero() {
        return banderaParqueadero;
    }

    public void setBanderaParqueadero(boolean banderaParqueadero) {
        this.banderaParqueadero = banderaParqueadero;
    }

    public String getObservacionesServicios() {
        return observacionesServicios;
    }

    public boolean isBanderaOfertaNueva() {
        return banderaOfertaNueva;
    }

    public void setBanderaOfertaNueva(boolean banderaOfertaNueva) {
        this.banderaOfertaNueva = banderaOfertaNueva;
    }

    public void setObservacionesServicios(String observacionesServicios) {
        this.observacionesServicios = observacionesServicios;
    }

    public void setServiciosPublicos(DualListModel<String> serviciosPublicos) {
        this.serviciosPublicos = serviciosPublicos;
    }

    public DualListModel<String> getAreasUsoExclusivo() {
        return areasUsoExclusivo;
    }

    public void setAreasUsoExclusivo(DualListModel<String> areasUsoExclusivo) {
        this.areasUsoExclusivo = areasUsoExclusivo;
    }

    public DualListModel<String> getServiciosComunalesOferta() {
        return serviciosComunalesOferta;
    }

    public void setServiciosComunalesOferta(
        DualListModel<String> serviciosComunalesOferta) {
        this.serviciosComunalesOferta = serviciosComunalesOferta;
    }

    public DualListModel<String> getOfertasRecursosHidricos() {
        return ofertasRecursosHidricos;
    }

    public void setOfertasRecursosHidricos(
        DualListModel<String> ofertasRecursosHidricos) {
        this.ofertasRecursosHidricos = ofertasRecursosHidricos;
    }

    public boolean isMezzanine() {
        return mezzanine;
    }

    public void setMezzanine(boolean mezzanine) {
        this.mezzanine = mezzanine;
    }

    public boolean isCableadoRed() {
        return cableadoRed;
    }

    public void setCableadoRed(boolean cableadoRed) {
        this.cableadoRed = cableadoRed;
    }

    public boolean isZonaFranca() {
        return zonaFranca;
    }

    public void setZonaFranca(boolean zonaFranca) {
        this.zonaFranca = zonaFranca;
    }

    public boolean isServidumbre() {
        return servidumbre;
    }

    public void setServidumbre(boolean servidumbre) {
        this.servidumbre = servidumbre;
    }

    public boolean isPuenteGrua() {
        return puenteGrua;
    }

    public void setPuenteGrua(boolean puenteGrua) {
        this.puenteGrua = puenteGrua;
    }

    public boolean isRiego() {
        return riego;
    }

    public void setRiego(boolean riego) {
        this.riego = riego;
    }

    public boolean isRecursosHidricos() {
        return recursosHidricos;
    }

    public List<SelectItem> getOfertaFrenteViasV() {
        return ofertaFrenteViasV;
    }

    public void setOfertaFrenteViasV(List<SelectItem> ofertaFrenteViasV) {
        this.ofertaFrenteViasV = ofertaFrenteViasV;
    }

    public List<SelectItem> getOfertaEstadoViasV() {
        return ofertaEstadoViasV;
    }

    public void setOfertaEstadoViasV(List<SelectItem> ofertaEstadoViasV) {
        this.ofertaEstadoViasV = ofertaEstadoViasV;
    }

    public void setRecursosHidricos(boolean recursosHidricos) {
        this.recursosHidricos = recursosHidricos;
    }

    public boolean isCasa() {
        return casa;
    }

    public void setCasa(boolean casa) {
        this.casa = casa;
    }

    public boolean isAnexos() {
        return anexos;
    }

    public void setAnexos(boolean anexos) {
        this.anexos = anexos;
    }

    public void setCurrentNumeroPredial(String currentNumeroPredial) {
        this.currentNumeroPredial = currentNumeroPredial;
    }

    public String getCurrentNumeroPredial() {
        return currentNumeroPredial;
    }

    public String getSelectedDepartamentoNom() {
        return selectedDepartamentoNom;
    }

    public String getSelectedMunicipioNom() {
        return selectedMunicipioNom;
    }

    // ---------------------------------------------------------------
    public List<Predio> getPrediosRegionCaptura() {
        return prediosRegionCaptura;
    }

    public void setPrediosRegionCaptura(List<Predio> prediosRegionCaptura) {
        this.prediosRegionCaptura = prediosRegionCaptura;
    }

    // ----------------------------------------------------------------------
    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    // ----------------------------------------------------------------------
    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    // ----------------------------------------------------------------------
    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    // ----------------------------------------------------------------------
    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    // ----------------------------------------------------------------------
    public Predio[] getSelectedPredios() {
        return selectedPredios;
    }

    public void setSelectedPredios(Predio[] selectedPredios) {
        this.selectedPredios = selectedPredios;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    // --------------------------------------------------------------------------------
    public String getSelectedDepartamentoCod() {
        return this.selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String codigo) {

        this.selectedDepartamentoCod = codigo;
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento departamento : this.departamentos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        this.departamentoOferta = dptoSeleccionado;
    }

    // ---------------------------------------------------------------------
    public String getSelectedMunicipioCod() {
        return this.selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String codigo) {
        this.selectedMunicipioCod = codigo;

        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.municipioOferta = municipioSelected;
    }

    public Long getRegionId() {
        return regionCapturaOfertaId;
    }

    public void setRegionId(Long regionId) {
        this.regionCapturaOfertaId = regionId;
    }

    public boolean isBanderaCreacionOfertaDesdeCargaAlSNC() {
        return banderaCreacionOfertaDesdeCargaAlSNC;
    }

    public void setBanderaCreacionOfertaDesdeCargaAlSNC(
        boolean banderaCreacionOfertaDesdeCargaAlSNC) {
        this.banderaCreacionOfertaDesdeCargaAlSNC = banderaCreacionOfertaDesdeCargaAlSNC;
    }

    public boolean isBanderaCreacionOfertaDesdeConsultaOfertas() {
        return banderaCreacionOfertaDesdeConsultaOfertas;
    }

    public void setBanderaCreacionOfertaDesdeConsultaOfertas(
        boolean banderaCreacionOfertaDesdeConsultaOfertas) {
        this.banderaCreacionOfertaDesdeConsultaOfertas = banderaCreacionOfertaDesdeConsultaOfertas;
    }

    public boolean isBanderaCreacionOfertaDesdeModOfertasDevueltas() {
        return banderaCreacionOfertaDesdeModOfertasDevueltas;
    }

    public void setBanderaCreacionOfertaDesdeModOfertasDevueltas(
        boolean banderaCreacionOfertaDesdeModOfertasDevueltas) {
        this.banderaCreacionOfertaDesdeModOfertasDevueltas =
            banderaCreacionOfertaDesdeModOfertasDevueltas;
    }

    public String getRecolectorId() {
        return recolectorId;
    }

    public void setRecolectorId(String recolectorId) {
        this.recolectorId = recolectorId;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.banderaEsConsulta = false;

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.inicializarVariablesVisorGIS();

        this.mensajeSalidaFotosOP = "Al agregar las fotos asociadas a " +
             " la oferta inmobiliaria principal esta no podra ser editada posteriormente." +
             " ¿Está seguro de que desea guardar y terminar?";

        if (this.banderaEsEdicion &&
             this.selectedOfertaInmobiliaria.getId() != null) {
            this.cargarDatosParaEdicion();
        } else {
            this.cargarDatosParaRegistroNuevo();
        }

        this.tienePrediosDesactualizados = false;
        this.listaOfertasEliminadas = new ArrayList<OfertaInmobiliaria>();

        this.preCargarDatosConsulta();

    }

    private void cargarDatosParaRegistroNuevo() {

        this.banderaEsEdicion = true;

        this.selectedOfertaInmobiliaria = cargarDatosBaseOferta();

        this.selectedOfertaInmobiliaria.setFechaIngresoOferta(new Date(System
            .currentTimeMillis()));

        this.departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorTerritorial(
                usuario.getCodigoTerritorial());
        this.updateMunicipiosItemList();

        this.inicializarBanderasProyectosOfertasInmobiliarias();

        this.cargarServiciosPublicos();

        this.casasAptosSHs = new ArrayList<OfertaInmobiliaria>();
        this.locales = new ArrayList<OfertaInmobiliaria>();
        this.oficinasConsultorios = new ArrayList<OfertaInmobiliaria>();
        this.bodegas = new ArrayList<OfertaInmobiliaria>();
        this.parqueaderos = new ArrayList<OfertaInmobiliaria>();
        this.fincas = new ArrayList<OfertaInmobiliaria>();

        this.banderaGuardarFotografias = false;
        this.recursosHidricos = false;

        this.cargarCombosOfertas();
    }

    private void cargarDatosParaEdicion() {

        this.inicializarBanderasProyectosOfertasInmobiliarias();

        this.selectedOfertaInmobiliaria = this.getAvaluosService()
            .buscarOfertaInmobiliariaPorId(
                this.selectedOfertaInmobiliaria.getId());

        this.cambioCondicionJuridica();
        this.cargarServiciosPublicos();
        this.cargarServiciosComunales();
        this.cargarAreasUsoExclusivo();

        this.precargarDatosDeptoYMunic(
            this.selectedOfertaInmobiliaria.getDepartamento(),
            this.selectedOfertaInmobiliaria.getMunicipio());

        this.cargarDatosInmueble();

        if (this.selectedOfertaInmobiliaria.getFotografias() == null) {
            this.selectedOfertaInmobiliaria
                .setFotografias(new ArrayList<Fotografia>());
        }

        this.banderaTipoOfertaSeleccionada = true;

        if (this.selectedOfertaInmobiliaria.getTipoOferta() != null &&
             this.selectedOfertaInmobiliaria.getTipoOferta().equals(
                EOfertaTipoOferta.USADOS.getCodigo())) {

            this.banderaOfertaNueva = false;
            this.selectedOfertaInmobiliariaDetalle = selectedOfertaInmobiliaria
                .getOfertaInmobiliaria();
            this.asignarBanderasTipoInmueble();

        } else {

            this.actualizarOfertaInmueble();
            this.banderaOfertaNueva = true;
            this.selectedOfertaInmobiliariaDetalle = selectedOfertaInmobiliaria;

        }

    }

    /**
     * Pre carga los valores booleanos de las ofertas al momento de edición
     *
     * @author christian.rodriguez
     */
    private void cargarDatosInmueble() {

        if (this.selectedOfertaInmobiliaria.getTipoInmueble() == null) {
            return;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.BODEGA.getCodigo())) {
            this.cargarDatosBodega(this.selectedOfertaInmobiliaria);
            this.banderaBodega = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.FINCA.getCodigo())) {
            this.cargarDatosFinca(this.selectedOfertaInmobiliaria);
            this.banderaFinca = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.LOCAL.getCodigo())) {
            this.cargarDatosLocal(this.selectedOfertaInmobiliaria);
            this.banderaLocal = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.OFICINA.getCodigo()) ||
             this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.CONSULTORIO.getCodigo())) {
            this.cargarDatosOficinaConsultorio(this.selectedOfertaInmobiliaria);
            this.banderaOficinaConsultorio = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.OFICINA.getCodigo()) ||
             this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.PARQUEADERO.getCodigo())) {
            this.cargarDatosParqueadero(this.selectedOfertaInmobiliaria);
            this.banderaParqueadero = true;
        }

    }

    /**
     * Inicialización de los datos requeridos para servicios públicos
     */
    public void cargarServiciosPublicos() {

        this.sourceServiciosPublicos = new ArrayList<String>();

        List<SelectItem> fuente = this.generalMBService
            .getOfertaServicioPublicoV();
        for (SelectItem si : fuente) {
            this.sourceServiciosPublicos.add(si.getLabel().toString());
        }
        this.targetServiciosPublicos = new ArrayList<String>();

        this.serviciosPublicos = new DualListModel<String>(
            this.sourceServiciosPublicos, this.targetServiciosPublicos);

        if (this.selectedOfertaInmobiliaria.getOfertaServicioPublicos() != null &&
             !this.selectedOfertaInmobiliaria.getOfertaServicioPublicos()
                .isEmpty()) {

            for (OfertaServicioPublico osp : this.selectedOfertaInmobiliaria
                .getOfertaServicioPublicos()) {
                this.serviciosPublicos.getTarget()
                    .add(osp.getServicioPublico());
                this.serviciosPublicos.getSource().remove(
                    osp.getServicioPublico());
            }
        }
    }

    /**
     * Precarga los servicios comunales que tiene la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarServiciosComunales() {

        this.sourceServiciosComunalesOferta = this.generalMBService
            .getOfertasServiciosComunalesSource();
        this.targetServiciosComunalesOferta = new ArrayList<String>();

        this.serviciosComunalesOferta = new DualListModel<String>(
            this.sourceServiciosComunalesOferta,
            this.targetServiciosComunalesOferta);

        if (this.selectedOfertaInmobiliaria.getOfertaServicioComunals() != null &&
             !this.selectedOfertaInmobiliaria.getOfertaServicioComunals()
                .isEmpty()) {

            this.serviciosComunalesOferta = new DualListModel<String>();
            this.serviciosComunalesOferta.setSource(this.generalMBService
                .getOfertasServiciosComunalesSource());
            for (OfertaServicioComunal osc : this.selectedOfertaInmobiliaria
                .getOfertaServicioComunals()) {
                this.serviciosComunalesOferta.getTarget().add(
                    osc.getServicioComunal());
                this.serviciosComunalesOferta.getSource().remove(
                    osc.getServicioComunal());
            }
        }
    }

    /**
     * Precarga las areas de uso exclusiv que tiene la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarAreasUsoExclusivo() {

        this.sourceAreasUsoExclusivo = this.generalMBService
            .getOfertasAreasUsoExclusivoSource();
        this.targetAreasUsoExclusivo = new ArrayList<String>();

        this.areasUsoExclusivo = new DualListModel<String>(
            this.sourceAreasUsoExclusivo, this.targetAreasUsoExclusivo);

        if (this.selectedOfertaInmobiliaria.getOfertaAreaUsoExclusivos() != null &&
             !this.selectedOfertaInmobiliaria
                .getOfertaAreaUsoExclusivos().isEmpty()) {

            this.areasUsoExclusivo = new DualListModel<String>();
            this.areasUsoExclusivo.setSource(this.generalMBService
                .getOfertasAreasUsoExclusivoSource());
            for (OfertaAreaUsoExclusivo oauc : this.selectedOfertaInmobiliaria
                .getOfertaAreaUsoExclusivos()) {
                this.areasUsoExclusivo.getTarget().add(
                    oauc.getAreaUsoExclusivo());
                this.areasUsoExclusivo.getSource().remove(
                    oauc.getAreaUsoExclusivo());
            }
        }
    }

    /**
     * Precarga los datos del parqueadero asociado a la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarDatosParqueadero(OfertaInmobiliaria oferta) {

        if (oferta.getServidumbre() != null &&
             oferta.getServidumbre().equals(ESiNo.SI.getCodigo())) {
            this.servidumbre = true;
        }
    }

    /**
     * Precarga los datos del local asociado a la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarDatosLocal(OfertaInmobiliaria oferta) {

        if (oferta.getMezanine() != null &&
             oferta.getMezanine().equals(ESiNo.SI.getCodigo())) {
            this.mezzanine = true;
        }
    }

    /**
     * Precarga los datos del consultorio asociado a la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarDatosOficinaConsultorio(OfertaInmobiliaria oferta) {

        this.cargarDatosLocal(oferta);

        if (oferta.getCableadoRed() != null &&
             oferta.getCableadoRed().equals(ESiNo.SI.getCodigo())) {
            this.cableadoRed = true;
        }

    }

    /**
     * Precarga los datos de la bodega asociada a la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarDatosBodega(OfertaInmobiliaria oferta) {

        this.cargarDatosLocal(oferta);

        if (oferta.getZonaFranca() != null &&
             oferta.getZonaFranca().equals(ESiNo.SI.getCodigo())) {
            this.zonaFranca = true;
        }

        if (oferta.getPuenteGrua() != null &&
             oferta.getPuenteGrua().equals(ESiNo.SI.getCodigo())) {
            this.puenteGrua = true;
        }

    }

    /**
     * Precarga los datos de la finca asociada a la oferta para la vista de edición
     *
     * @author christian.rodriguez
     */
    private void cargarDatosFinca(OfertaInmobiliaria oferta) {
        if (oferta.getCasa() != null &&
             oferta.getCasa().equals(ESiNo.SI.getCodigo())) {
            this.casa = true;
        }

        if (oferta.getRiego() != null &&
             oferta.getRiego().equals(ESiNo.SI.getCodigo())) {
            this.riego = true;
        }

        if (oferta.getAnexos() != null &&
             oferta.getAnexos().equals(ESiNo.SI.getCodigo())) {
            this.anexos = true;
        }

        if (oferta.getRecursosHidricos() != null &&
             oferta.getRecursosHidricos().equals(ESiNo.SI.getCodigo())) {
            this.recursosHidricos = true;
        }

        if (oferta.getOfertaRecursoHidricos() != null &&
             !oferta.getOfertaRecursoHidricos().isEmpty()) {
            this.ofertasRecursosHidricos = new DualListModel<String>();
            this.ofertasRecursosHidricos.setSource(this.generalMBService
                .getOfertasRecursosHidricosSource());
            for (OfertaRecursoHidrico oreh : oferta.getOfertaRecursoHidricos()) {
                this.ofertasRecursosHidricos.getTarget().add(
                    oreh.getRecursoHidrico());
                this.ofertasRecursosHidricos.getSource().remove(
                    oreh.getRecursoHidrico());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Cargar los combos de ofertas inmobiliarias
     *
     * @author pedro.garcia
     */
    private void cargarCombosOfertas() {

        this.ofertaTipoInmuebleUsadoV = this.generalMBService
            .getOfertaTipoInmuebleUsadoV();
        this.ofertaTipoInmuebleUsadoV.add(0, new SelectItem(null,
            this.generalMBService.getCombosDefaultValue()));

        this.ofertaTipoInmuebleProyectoV = this.generalMBService
            .getOfertaTipoInmuebleProyectoV();
        this.ofertaTipoInmuebleProyectoV.add(0, new SelectItem(null,
            this.generalMBService.getCombosDefaultValue()));

        this.ofertaFrenteViasV = this.generalMBService.getOfertaFrenteViasV();
        this.ofertaFrenteViasV.add(0, new SelectItem(null,
            this.generalMBService.getCombosDefaultValue()));

        this.ofertaEstadoViasV = this.generalMBService.getOfertaEstadoViasV();
        this.ofertaEstadoViasV.add(0, new SelectItem(null,
            this.generalMBService.getCombosDefaultValue()));
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
            .add(new SelectItem(null, this.DEFAULT_COMBOS));

        Collections
            .sort(this.departamentos, Departamento.getComparatorNombre());

        for (Departamento departamento : this.departamentos) {
            this.departamentosItemList.add(new SelectItem(departamento
                .getCodigo(), departamento.getNombre()));
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * actualiza la lista de municipios según el departamento seleccionado
     *
     * @author pedro.garcia
     */
    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.selectedDepartamentoCod != null) {
            this.municipios = new ArrayList<Municipio>();
            this.municipios = (ArrayList<Municipio>) this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.selectedDepartamentoCod);

            Collections.sort(this.municipios, Municipio.getComparatorNombre());

            for (Municipio municipio : this.municipios) {
                this.municipiosItemList.add(new SelectItem(municipio
                    .getCodigo(), municipio.getNombre()));
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener del cambio en el combo de departamentos en la pagina de informacion fisica
     */
    public void onChangeDepartamentos() {
        this.selectedOfertaInmobiliaria
            .setOfertaPredios(new ArrayList<OfertaPredio>());
        this.municipioOferta = null;
        this.selectedMunicipioCod = "";
        this.updateMunicipiosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar la condición juridica
     */
    public void cambioCondicionJuridica() {

        this.selectedOfertaOferta = new OfertaInmobiliaria();

        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getCondicionJuridica() != null &&
             this.selectedOfertaInmobiliaria.getCondicionJuridica()
                .equals(ECondicionJuridica.PROPIEDAD_HORIZONTAL
                    .getCodigo())) {
            this.banderaPropiedadHorizontal = true;

            this.cargarServiciosComunales();
            this.cargarAreasUsoExclusivo();

            if (!this.banderaOfertaNueva) {
                this.eliminarTiposInmueblesPH();
            }

        } else {
            this.banderaPropiedadHorizontal = false;
            this.ofertaTipoInmuebleUsadoV = this.generalMBService
                .getOfertaTipoInmuebleUsadoV();
            this.ofertaTipoInmuebleUsadoV.add(0, new SelectItem(null,
                this.generalMBService.getCombosDefaultValue()));
        }

    }

    /**
     * Devuelve si está seleccionada la opción Arriendo en el campo tipo de investigación
     *
     * @author christian.rodriguez
     * @return verdadero si está seleccionada la opción ARRIENDO, falso si no
     */
    public boolean isTipoInvestigacionArriendo() {

        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getTipoInvestigacion() != null &&
             this.selectedOfertaInmobiliaria.getTipoInvestigacion()
                .equals(EOfertaTipoInvestigacion.ARRIENDO.toString())) {

            this.selectedOfertaInmobiliaria.setValorDepurado(0.0);
            this.selectedOfertaInmobiliaria.setValorPedido(0.0);
            this.selectedOfertaInmobiliaria.setPorcentajeNegociacion(0.0);
            this.selectedOfertaInmobiliaria.setFormaNegociacion(null);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Elimina de la lista tipos de inmuebles dependiendo de la condición jurídica de la oferta
     *
     * @author christian.rodriguez
     */
    private void eliminarTiposInmueblesPH() {

        ArrayList<SelectItem> opcionesAEliminar = new ArrayList<SelectItem>();

        for (Iterator<SelectItem> it = ofertaTipoInmuebleUsadoV.iterator(); it
            .hasNext();) {
            SelectItem tipoInmueble = (SelectItem) it.next();
            if (tipoInmueble.getValue() != null &&
                 (tipoInmueble.getValue().equals(
                    EOfertaTipoInmueble.CASALOTE.getCodigo()) || tipoInmueble
                .getValue().equals(
                    EOfertaTipoInmueble.FINCA.getCodigo()))) {
                opcionesAEliminar.add(tipoInmueble);
            }
        }

        for (SelectItem opcion : opcionesAEliminar) {
            ofertaTipoInmuebleUsadoV.remove(opcion);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para borrar locales de la oferta inmobiliaria
     */
    public void removerLocales() {

        if (this.selectedOfertaOferta != null) {
            this.locales.remove(this.selectedOfertaOferta);
            this.eliminarOferta();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para borrar casas, apartamentos o suites hoteleras
     */
    public void removerCasasAptosSHs() {

        if (this.selectedOfertaOferta != null) {
            this.casasAptosSHs.remove(this.selectedOfertaOferta);
            this.eliminarOferta();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para borrar oficinas / consultorios
     */
    public void removerOficinas() {

        if (this.selectedOfertaOferta != null) {
            this.oficinasConsultorios.remove(this.selectedOfertaOferta);
            this.eliminarOferta();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para borrar bodegas
     */
    public void removerBodegas() {

        if (this.selectedOfertaOferta != null) {
            this.bodegas.remove(this.selectedOfertaOferta);
            this.eliminarOferta();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para borrar parqueaderos
     */
    public void removerParqueaderos() {

        if (this.selectedOfertaOferta != null) {
            this.parqueaderos.remove(this.selectedOfertaOferta);
            this.eliminarOferta();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para adicionar ofertas inmobiliarias asociadas al oferta principal
     */
    public void adicionarOfertas() {
        RequestContext context = RequestContext.getCurrentInstance();

        if (!this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            Constantes.CONSTANTE_CADENA_VACIA_DB)) {
            this.selectedOfertaOferta = this.cargarDatosBaseOferta();
        } else {
            String mensaje = "Debe seleccionar un tipo de inmueble," +
                 " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar aceptar en la pantalla de proyecto casa / apartamento / suite
     * hotelera
     */
    public void guardarProyectoCasaAptoSuiteHotelera() {

        this.selectedOfertaOferta.setTipoOferta(this.selectedOfertaInmobiliaria
            .getTipoOferta());
        this.casasAptosSHs = this.actualizarOfertaOferta(this.casasAptosSHs,
            this.selectedOfertaOferta);
        this.guardarOfertaInmobiliaria();

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar aceptar en la pantalla de proyecto local
     */
    public void guardarProyectoLocal() {

        this.actualizarBooleanosProyectoLocal();

        this.selectedOfertaOferta.setTipoOferta(this.selectedOfertaInmobiliaria
            .getTipoOferta());

        this.locales = this.actualizarOfertaOferta(this.locales,
            this.selectedOfertaOferta);
        this.guardarOfertaInmobiliaria();

    }

    /**
     * Actualiza los valores booleanos de los proyectos inmobiliarios de tipo Local
     *
     * @author christian.rodriguez
     */
    private void actualizarBooleanosProyectoLocal() {

        if (this.banderaOfertaNueva) {
            this.selectedOfertaOferta.setMezanine(ESiNo.NO.getCodigo());
            if (this.mezzanine) {
                this.selectedOfertaOferta.setMezanine(ESiNo.SI.getCodigo());
            }
        } else {
            this.selectedOfertaInmobiliaria.setMezanine(ESiNo.NO.getCodigo());
            if (this.mezzanine) {
                this.selectedOfertaInmobiliaria.setMezanine(ESiNo.SI
                    .getCodigo());
            }
        }
    }

    /**
     * Método ejecutado al seleccionar aceptar en la pantalla de proyecto finca
     */
    public void guardarProyectoFinca() {
        this.actualizarBooleanosProyectoFinca();

        this.selectedOfertaOferta.setTipoOferta(this.selectedOfertaInmobiliaria
            .getTipoOferta());

        this.fincas = this.actualizarOfertaOferta(this.fincas,
            this.selectedOfertaOferta);
        this.guardarOfertaInmobiliaria();
    }

    /**
     * Actualiza los valores booleanos de los proyectos inmobiliarios de tipo Finca
     *
     * @author christian.rodriguez
     */
    private void actualizarBooleanosProyectoFinca() {

        if (!this.banderaOfertaNueva) {
            this.selectedOfertaInmobiliaria.setRiego(ESiNo.NO.getCodigo());
            if (this.riego) {
                this.selectedOfertaInmobiliaria.setRiego(ESiNo.SI.getCodigo());
            }
            this.selectedOfertaInmobiliaria.setCasa(ESiNo.NO.getCodigo());
            if (this.casa) {
                this.selectedOfertaInmobiliaria.setCasa(ESiNo.SI.getCodigo());
            }
            this.selectedOfertaInmobiliaria.setRecursosHidricos(ESiNo.NO
                .getCodigo());
            if (this.recursosHidricos) {
                this.selectedOfertaInmobiliaria.setRecursosHidricos(ESiNo.SI
                    .getCodigo());
            }
            this.selectedOfertaInmobiliaria.setAnexos(ESiNo.NO.getCodigo());
            if (this.anexos) {
                this.selectedOfertaInmobiliaria.setAnexos(ESiNo.SI.getCodigo());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar aceptar en la pantalla de proyecto oficina consultorio
     */
    public void guardarProyectoOficinaConsultorio() {

        this.actualizarBooleanosProyectoOficinaConsultorio();

        this.selectedOfertaOferta.setTipoOferta(this.selectedOfertaInmobiliaria
            .getTipoOferta());

        this.oficinasConsultorios = this.actualizarOfertaOferta(
            this.oficinasConsultorios, this.selectedOfertaOferta);
        this.guardarOfertaInmobiliaria();

    }

    /**
     * Actualiza los valores booleanos de los proyectos inmobiliarios de tipo oficina o consultorio
     *
     * @author christian.rodriguez
     */
    private void actualizarBooleanosProyectoOficinaConsultorio() {
        this.actualizarBooleanosProyectoLocal();
        if (this.banderaOfertaNueva) {
            this.selectedOfertaOferta.setCableadoRed(ESiNo.NO.getCodigo());
            if (this.cableadoRed) {
                this.selectedOfertaOferta.setCableadoRed(ESiNo.SI.getCodigo());
            }
        } else {
            this.selectedOfertaInmobiliaria
                .setCableadoRed(ESiNo.NO.getCodigo());
            if (this.cableadoRed) {
                this.selectedOfertaInmobiliaria.setCableadoRed(ESiNo.SI
                    .getCodigo());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar aceptar en la pantalla de proyecto bodega
     */
    public void guardarProyectoBodega() {

        this.actualizarBooleanosProyectoBodega();

        this.selectedOfertaOferta.setTipoOferta(this.selectedOfertaInmobiliaria
            .getTipoOferta());
        this.bodegas = this.actualizarOfertaOferta(this.bodegas,
            this.selectedOfertaOferta);
        this.guardarOfertaInmobiliaria();
    }

    /**
     * Actualiza los valores booleanos de los proyectos inmobiliarios de tipo bodega
     *
     * @author christian.rodriguez
     */
    private void actualizarBooleanosProyectoBodega() {
        this.actualizarBooleanosProyectoLocal();

        if (this.banderaOfertaNueva) {
            this.selectedOfertaOferta.setZonaFranca(ESiNo.NO.getCodigo());
            if (this.zonaFranca) {
                this.selectedOfertaOferta.setZonaFranca(ESiNo.SI.getCodigo());
            }

            this.selectedOfertaOferta.setPuenteGrua(ESiNo.NO.getCodigo());
            if (this.puenteGrua) {
                this.selectedOfertaOferta.setPuenteGrua(ESiNo.SI.getCodigo());
            }
        } else {
            this.selectedOfertaInmobiliaria.setZonaFranca(ESiNo.NO.getCodigo());
            if (this.zonaFranca) {
                this.selectedOfertaInmobiliaria.setZonaFranca(ESiNo.SI
                    .getCodigo());
            }

            this.selectedOfertaInmobiliaria.setPuenteGrua(ESiNo.NO.getCodigo());
            if (this.puenteGrua) {
                this.selectedOfertaInmobiliaria.setPuenteGrua(ESiNo.SI
                    .getCodigo());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar aceptar en la pantalla de proyecto parqueadero
     */
    public void guardarProyectoParqueadero() {

        this.actualizarBooleanosProyectoParqueadero();

        this.selectedOfertaOferta.setTipoOferta(this.selectedOfertaInmobiliaria
            .getTipoOferta());

        this.parqueaderos = this.actualizarOfertaOferta(this.parqueaderos,
            this.selectedOfertaOferta);
        this.guardarOfertaInmobiliaria();

    }

    /**
     * Actualiza los valores booleanos de los proyectos inmobiliarios de tipo parqueadero
     *
     * @author christian.rodriguez
     */
    private void actualizarBooleanosProyectoParqueadero() {
        if (this.banderaOfertaNueva) {
            this.selectedOfertaOferta.setServidumbre(ESiNo.NO.getCodigo());
            if (this.servidumbre) {
                this.selectedOfertaOferta.setServidumbre(ESiNo.SI.getCodigo());
            }
        } else {
            this.selectedOfertaInmobiliaria
                .setServidumbre(ESiNo.NO.getCodigo());
            if (this.servidumbre) {
                this.selectedOfertaInmobiliaria.setServidumbre(ESiNo.SI
                    .getCodigo());
            }
        }
    }

    /**
     * Actualiza una lista de ofertas para evitar duplicar una oferta al editarla en la pantalla de
     * resumenes de ofertas
     *
     * @author christian.rodriguez
     * @param ofertas lista de ofertas asociadas al predio
     * @param ofertaAActualizar oferta que se actualizó
     * @return
     */
    public List<OfertaInmobiliaria> actualizarOfertaOferta(
        List<OfertaInmobiliaria> ofertas,
        OfertaInmobiliaria ofertaAActualizar) {
        int posicion = ofertas.indexOf(ofertaAActualizar);
        if (posicion == -1) {
            ofertas.add(ofertaAActualizar);
        } else {
            ofertas.set(posicion, ofertaAActualizar);
        }
        return ofertas;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cargar el componente fotográfico en edición de ofertas inmobilirias
     *
     * @modifiedby christian.rodriguez
     */
    public void cargarComponenteFotograficoYFotos() {

        OfertaInmobiliaria oferta = getOfertaEnEdicionParaFotografias();

        this.selectedFotografia = new Fotografia();

        if (oferta != null && oferta.getFotografias() != null &&
             !oferta.getFotografias().isEmpty()) {

            for (Fotografia f : oferta.getFotografias()) {

                ReporteDTO foto = this.reportes.consultarReporteDeGestorDocumental(f
                    .getIdRepositorioDocumentos());

                f.setNombreDescargaFotoAlfresco(foto.getUrlWebReporte());
                this.banderaFotografiasCargadas = true;
            }

        } else {
            oferta.setFotografias(new ArrayList<Fotografia>());
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        Object[] cargaArchivo = UtilidadesWeb.cargarArchivo(eventoCarga,
            this.contextoWebApp);

        if (this.selectedFotografia.getTipoFoto() == null) {
            this.addMensajeError("Debe seleccionar un tipo para la fotografía");
        } else {
            if (cargaArchivo[0] != null) {

                this.archivoResultado = (File) cargaArchivo[0];

                if (this.selectedFotografia == null) {
                    this.selectedFotografia = new Fotografia();
                }

                this.selectedFotografia.setArchivoFotografia(eventoCarga
                    .getFile().getFileName());
                this.selectedFotografia
                    .setNombreDescargaFotoAlfresco(eventoCarga.getFile()
                        .getFileName());
                this.selectedFotografia.setFecha(new Date());

                this.selectedFotografia.setUsuarioLog(this.usuario.getLogin());
                this.selectedFotografia.setFechaLog(new Date());

                ReporteDTO foto = this.reportes
                    .generarReporteDesdeUnFile(this.archivoResultado);

                this.selectedFotografia.setNombreDescargaFotoAlfresco(foto
                    .getUrlWebReporte());

                this.getOfertaEnEdicionParaFotografias().getFotografias()
                    .add(this.selectedFotografia);

                this.banderaFotografiasCargadas = true;

                this.cargarSiguienteFotografia();

            } else {

                LOGGER.error("ERROR: " + cargaArchivo[2]);
                String mensaje = (String) cargaArchivo[1];
                this.addMensajeError(mensaje);
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de inicialización de la fotografia actual
     */
    private void cargarSiguienteFotografia() {

        if (this.selectedFotografia.getArchivoFotografia() != null) {
            this.selectedFotografia = new Fotografia();
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que permite guardar una oferta inmobiliaria con las ofertas asociadas a esta misma
     *
     * @modified rodrigo.hernandez 15-06-2012 adicion de validacion sobre campo
     * banderaCreacionOfertaDesdeCargaAlSNC para asociar la oferta con un objeto RegionCapturaOferta
     */
    public void guardarOfertaInmobiliaria() {

        if (this.selectedOfertaInmobiliaria != null) {

            RequestContext context = RequestContext.getCurrentInstance();

            if (banderaCreacionOfertaDesdeCargaAlSNC) {
                this.selectedOfertaInmobiliaria
                    .setRegionCapturaOfertaId(this.regionCapturaOfertaId);
                this.selectedOfertaInmobiliaria
                    .setRecolectorId(this.recolectorId);
            }

            this.selectedOfertaInmobiliaria
                .setOfertaInmobiliarias(new ArrayList<OfertaInmobiliaria>());

            this.selectedOfertaInmobiliaria.getOfertaInmobiliarias().addAll(
                this.bodegas);
            this.selectedOfertaInmobiliaria.getOfertaInmobiliarias().addAll(
                this.casasAptosSHs);
            this.selectedOfertaInmobiliaria.getOfertaInmobiliarias().addAll(
                this.locales);
            this.selectedOfertaInmobiliaria.getOfertaInmobiliarias().addAll(
                this.oficinasConsultorios);
            this.selectedOfertaInmobiliaria.getOfertaInmobiliarias().addAll(
                this.parqueaderos);
            this.selectedOfertaInmobiliaria.getOfertaInmobiliarias().addAll(
                this.fincas);

            this.actualizarOfertaServiciosPublicos();
            this.actualizarOfertaServiciosComunales();
            this.actualizarOfertaAreasServicioExclusivo();
            this.actualizarOfertaPredios();
            this.actualizarOfertaRecursosHidricos();
            this.asignarUnidadesArea();
            this.borrarVetustezFincaYLote();

            this.selectedOfertaInmobiliaria
                .setDepartamento(this.departamentoOferta);
            this.selectedOfertaInmobiliaria.setMunicipio(this.municipioOferta);

            if (this.selectedOfertaInmobiliaria.getResponsableInformacion() == null) {
                this.selectedOfertaInmobiliaria
                    .setResponsableInformacion(usuario.getNombreCompleto());
            }

            try {
                this.selectedOfertaInmobiliaria = this.getAvaluosService()
                    .guardarActualizarOfertaInmobiliaria(
                        this.selectedOfertaInmobiliaria,
                        this.listaOfertasEliminadas, this.usuario);

                this.actualizarOfertaInmueble();

                this.banderaEsConsulta = true;
                this.banderaEsEdicion = false;

            } catch (Exception e) {
                String mensaje = "Ocurrió un error al Guardar la oferta inmobiliaria" +
                     ", por favor intente nuevamente";
                this.addMensajeError(mensaje);
                LOGGER.error(e.getMessage(), e);
                context.addCallbackParam("error", "error");
            }
        }
    }

    /**
     * Actualiza los datos de la oferta para mostrar correctamente los inmuebles en las tablas de
     * resumen
     */
    private void actualizarOfertaInmueble() {
        this.listaOfertasEliminadas = new ArrayList<OfertaInmobiliaria>();

        this.bodegas = new ArrayList<OfertaInmobiliaria>();
        this.casasAptosSHs = new ArrayList<OfertaInmobiliaria>();
        this.locales = new ArrayList<OfertaInmobiliaria>();
        this.oficinasConsultorios = new ArrayList<OfertaInmobiliaria>();
        this.parqueaderos = new ArrayList<OfertaInmobiliaria>();
        this.fincas = new ArrayList<OfertaInmobiliaria>();

        for (OfertaInmobiliaria oiTemp : this.selectedOfertaInmobiliaria
            .getOfertaInmobiliarias()) {

            if (oiTemp.getTipoInmueble().equals(
                EOfertaTipoInmueble.CASA.getCodigo()) ||
                 oiTemp.getTipoInmueble().equals(
                    EOfertaTipoInmueble.APARTAMENTO.getCodigo()) ||
                 oiTemp.getTipoInmueble().equals(
                    EOfertaTipoInmueble.SUITEHOTELERA.getCodigo())) {
                this.casasAptosSHs.add(oiTemp);
            } else if (oiTemp.getTipoInmueble().equals(
                EOfertaTipoInmueble.BODEGA.getCodigo())) {
                this.bodegas.add(oiTemp);
            } else if (oiTemp.getTipoInmueble().equals(
                EOfertaTipoInmueble.LOCAL.getCodigo())) {
                this.locales.add(oiTemp);
            } else if (oiTemp.getTipoInmueble().equals(
                EOfertaTipoInmueble.FINCA.getCodigo())) {
                this.fincas.add(oiTemp);
            } else if (oiTemp.getTipoInmueble().equals(
                EOfertaTipoInmueble.OFICINA.getCodigo()) ||
                 oiTemp.getTipoInmueble().equals(
                    EOfertaTipoInmueble.CONSULTORIO.getCodigo())) {
                this.oficinasConsultorios.add(oiTemp);
            } else if (oiTemp.getTipoInmueble().equals(
                EOfertaTipoInmueble.PARQUEADERO.getCodigo())) {
                this.parqueaderos.add(oiTemp);
            }
        }
    }

    /**
     * Coloca los datos básicos de los predios de la oferta
     *
     * @author christian.rodriguez
     */
    private void actualizarOfertaPredios() {
        if (this.selectedOfertaInmobiliaria.getOfertaPredios() != null) {
            for (int i = 0; i < this.selectedOfertaInmobiliaria
                .getOfertaPredios().size(); i++) {
                if (this.selectedOfertaInmobiliaria.getOfertaPredios().get(i)
                    .getFechaLog() == null) {
                    this.selectedOfertaInmobiliaria
                        .getOfertaPredios()
                        .get(i)
                        .setOfertaInmobiliaria(
                            this.selectedOfertaInmobiliaria);
                    this.selectedOfertaInmobiliaria.getOfertaPredios().get(i)
                        .setFechaLog(new Date());
                    this.selectedOfertaInmobiliaria.getOfertaPredios().get(i)
                        .setUsuarioLog(this.usuario.getLogin());
                }
            }
        }
    }

    /**
     * Revisa que los recursos hídricos de la oferta correspondan a los mostrados en los pick lists
     * de la página
     *
     * @author christian.rodriguez
     */
    private void actualizarOfertaRecursosHidricos() {
        OfertaRecursoHidrico reh;
        if (this.ofertasRecursosHidricos != null &&
             this.ofertasRecursosHidricos.getTarget() != null &&
             !this.ofertasRecursosHidricos.getTarget().isEmpty()) {

            if (this.selectedOfertaInmobiliaria.getOfertaRecursoHidricos() == null) {
                this.selectedOfertaInmobiliaria
                    .setOfertaRecursoHidricos(new ArrayList<OfertaRecursoHidrico>());
            }

            for (String recursoHidrico : this.ofertasRecursosHidricos
                .getTarget()) {

                reh = new OfertaRecursoHidrico();
                reh.setRecursoHidrico(recursoHidrico);
                if (!this.selectedOfertaInmobiliaria.getOfertaRecursoHidricos()
                    .contains(reh)) {
                    reh.setOfertaInmobiliaria(this.selectedOfertaInmobiliaria);
                    reh.setUsuarioLog(this.usuario.getLogin());
                    reh.setFechaLog(new Date());
                    this.selectedOfertaInmobiliaria.getOfertaRecursoHidricos()
                        .add(reh);
                }
            }

            for (String recursosHidricos : this.ofertasRecursosHidricos
                .getSource()) {

                reh = new OfertaRecursoHidrico();
                reh.setRecursoHidrico(recursosHidricos);
                if (this.selectedOfertaInmobiliaria.getOfertaRecursoHidricos()
                    .contains(reh)) {
                    this.selectedOfertaInmobiliaria.getOfertaRecursoHidricos()
                        .remove(reh);
                }
            }
        }
    }

    /**
     * Revisa que los áreas de servicio exclusivo de la oferta correspondan a los mostrados en los
     * pick lists de la página
     *
     * @author christian.rodriguez
     */
    private void actualizarOfertaAreasServicioExclusivo() {
        OfertaAreaUsoExclusivo aue;
        if (this.areasUsoExclusivo != null &&
             this.areasUsoExclusivo.getTarget() != null &&
             !this.areasUsoExclusivo.getTarget().isEmpty()) {

            if (this.selectedOfertaInmobiliaria.getOfertaAreaUsoExclusivos() == null) {
                this.selectedOfertaInmobiliaria
                    .setOfertaAreaUsoExclusivos(new ArrayList<OfertaAreaUsoExclusivo>());
            }

            for (String areasUsoExclusivo : this.areasUsoExclusivo.getTarget()) {

                aue = new OfertaAreaUsoExclusivo();
                aue.setAreaUsoExclusivo(areasUsoExclusivo);
                if (!this.selectedOfertaInmobiliaria
                    .getOfertaAreaUsoExclusivos().contains(aue)) {
                    aue.setOfertaInmobiliaria(this.selectedOfertaInmobiliaria);
                    aue.setUsuarioLog(this.usuario.getLogin());
                    aue.setFechaLog(new Date());
                    this.selectedOfertaInmobiliaria
                        .getOfertaAreaUsoExclusivos().add(aue);
                }
            }

            for (String areasUsoExclusivo : this.areasUsoExclusivo.getSource()) {

                aue = new OfertaAreaUsoExclusivo();
                aue.setAreaUsoExclusivo(areasUsoExclusivo);
                if (this.selectedOfertaInmobiliaria
                    .getOfertaAreaUsoExclusivos().contains(aue)) {
                    this.selectedOfertaInmobiliaria
                        .getOfertaAreaUsoExclusivos().remove(aue);
                }
            }
        }
    }

    /**
     * Revisa que los servicios comunales de la oferta correspondan a los mostrados en los pick
     * lists de la página
     *
     * @author christian.rodriguez
     */
    private void actualizarOfertaServiciosComunales() {
        OfertaServicioComunal sc;
        if (this.serviciosComunalesOferta != null &&
             this.serviciosComunalesOferta.getTarget() != null &&
             !this.serviciosComunalesOferta.getTarget().isEmpty()) {

            if (this.selectedOfertaInmobiliaria.getOfertaServicioComunals() == null) {
                this.selectedOfertaInmobiliaria
                    .setOfertaServicioComunals(new ArrayList<OfertaServicioComunal>());
            }

            for (String servicioComunal : this.serviciosComunalesOferta
                .getTarget()) {

                sc = new OfertaServicioComunal();
                sc.setServicioComunal(servicioComunal);
                if (!this.selectedOfertaInmobiliaria
                    .getOfertaServicioComunals().contains(sc)) {
                    sc.setOfertaInmobiliaria(this.selectedOfertaInmobiliaria);
                    sc.setUsuarioLog(this.usuario.getLogin());
                    sc.setFechaLog(new Date());
                    this.selectedOfertaInmobiliaria.getOfertaServicioComunals()
                        .add(sc);
                }
            }

            for (String servicioComunal : this.serviciosComunalesOferta
                .getSource()) {

                sc = new OfertaServicioComunal();
                sc.setServicioComunal(servicioComunal);
                if (this.selectedOfertaInmobiliaria.getOfertaServicioComunals()
                    .contains(sc)) {
                    this.selectedOfertaInmobiliaria.getOfertaServicioComunals()
                        .remove(sc);
                }
            }
        }
    }

    /**
     * Revisa que los servicios públicos de la oferta correspondan a los mostrados en los pick lists
     * de la página
     *
     * @author christian.rodriguez
     */
    private void actualizarOfertaServiciosPublicos() {
        OfertaServicioPublico sp;
        if (this.serviciosPublicos != null &&
             this.serviciosPublicos.getTarget() != null &&
             !this.serviciosPublicos.getTarget().isEmpty()) {

            if (this.selectedOfertaInmobiliaria.getOfertaServicioPublicos() == null) {
                this.selectedOfertaInmobiliaria
                    .setOfertaServicioPublicos(new ArrayList<OfertaServicioPublico>());
            }

            for (String servicioP : this.serviciosPublicos.getTarget()) {

                sp = new OfertaServicioPublico();
                sp.setServicioPublico(servicioP);
                if (!this.selectedOfertaInmobiliaria
                    .getOfertaServicioPublicos().contains(sp)) {
                    sp.setOfertaInmobiliaria(this.selectedOfertaInmobiliaria);
                    sp.setUsuarioLog(this.usuario.getLogin());
                    sp.setFechaLog(new Date());
                    this.selectedOfertaInmobiliaria.getOfertaServicioPublicos()
                        .add(sp);
                }
            }

            for (String servicioP : this.serviciosPublicos.getSource()) {

                sp = new OfertaServicioPublico();
                sp.setServicioPublico(servicioP);

                if (this.selectedOfertaInmobiliaria.getOfertaServicioPublicos()
                    .contains(sp)) {
                    this.selectedOfertaInmobiliaria.getOfertaServicioPublicos()
                        .remove(sp);
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método encargado de realizar la validación de los datos minimos requeridos para guardar una
     * oferta inmobiliaria
     */
    private boolean validarDatosRequeridosOferta() {

        if (this.banderaPropiedadHorizontal) {

            if (!this.validarAreasUsoExclusivo()) {
                return false;
            }

            if (!this.validarServicioComunalOtro()) {
                return false;
            }

        }

        if (this.banderaCasaApartamentoSuiteHotelera) {
            if (!this.validarTipoNumeroCocina()) {
                return false;
            }
        }

        if (!this.validarInformacionACatastro()) {
            return false;
        }

        if (this.banderaOfertaNueva == true) {
            if (this.selectedOfertaInmobiliaria != null &&
                 this.selectedOfertaInmobiliaria.getOfertaInmobiliarias() != null &&
                 !this.selectedOfertaInmobiliaria
                    .getOfertaInmobiliarias().isEmpty()) {
                return true;
            } else {
                String mensaje = "Para guardar debe agregar por lo menos una oferta inmobiliaria." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                return false;
            }
        } else {

            if (this.selectedOfertaInmobiliaria != null) {

                return true;
            } else {
                String mensaje = "No se han diligenciado todos los datos de la oferta. " +
                     "Intentelo nuevamente.";
                this.addMensajeError(mensaje);
                return false;
            }

        }
    }

    /**
     * valida que al seleccionar la opción "Tiene predios desactualizados" se ingrese al menos una
     * observación para alguno de los predios
     *
     * @return verdadero si al menos uno de los predios tiene observaciones, falso en otro caso
     * @author christian.rodriguez
     */
    private boolean validarInformacionACatastro() {
        if (!this.selectedOfertaInmobiliaria.getOfertaPredios().isEmpty() &&
             this.tienePrediosDesactualizados) {
            for (OfertaPredio ofertaPredio : this.selectedOfertaInmobiliaria
                .getOfertaPredios()) {
                if (!ofertaPredio.getObservaciones().isEmpty()) {
                    return true;
                }
            }
        } else {
            return true;
        }
        String mensaje =
            "Información a catastro: Marcó la opción \"Tiene predios desactualizados\" pero no se " +
             "agregaron las correspondientes observaciones";
        this.addMensajeError(mensaje);
        return false;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que construye una oferta con los datos base
     *
     * @modified rodrigo.hernandez se asigna la fecha_oferta a oferta inmobiliaria hija
     */
    public OfertaInmobiliaria cargarDatosBaseOferta() {

        OfertaInmobiliaria oi = new OfertaInmobiliaria();

        oi.setNumeroBanios(0);
        oi.setCableadoRed(ESiNo.NO.getCodigo());
        oi.setNumeroOficinas(0);
        oi.setRiego(ESiNo.NO.getCodigo());
        oi.setRecursosHidricos(ESiNo.NO.getCodigo());
        oi.setAreaTotalTerrenoCatastral(0.0);
        oi.setAreaTotalConstruccionCata(0.0);
        oi.setAreaTerreno(0.0);
        oi.setAreaConstruccion(0.0);
        oi.setFechaIngresoOferta(new Date());
        oi.setValorPedido(0.0);
        oi.setPorcentajeNegociacion(0.0);
        oi.setValorArriendo(0.0);
        oi.setValorAdministracion(0.0);
        oi.setValorDepurado(0.0);
        oi.setNumeroPisos(1);
        oi.setNumeroHabitaciones(0);
        oi.setNumeroCocinas(0);
        oi.setEstado(EOfertaEstado.CREADA.getCodigo());
        oi.setOfertaPredios(new ArrayList<OfertaPredio>());

        if (this.banderaOfertaNueva && this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getTipoInmueble() != null) {
            oi.setTipoInmueble(this.selectedOfertaInmobiliaria
                .getTipoInmueble());
            oi.setFechaOferta(this.selectedOfertaInmobiliaria.getFechaOferta());
        }
        return oi;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al eliminar una oferta de la lista de ofertas asociadas a la oferta
     * inmobiliaria
     */
    public void eliminarOferta() {

        if (this.listaOfertasEliminadas == null) {
            this.listaOfertasEliminadas = new ArrayList<OfertaInmobiliaria>();
        }

        this.listaOfertasEliminadas.add(this.selectedOfertaOferta);
    }

    /**
     * Método que toma la lista de predios seleccionados y los agrega a la oferta siempre y cuando
     * sean válidos
     *
     * @author christian.rodriguez
     */
    public void usarPrediosSeleccionados() {

        if (this.selectedPredios.length != 0) {

            for (Predio selectedPredio : this.selectedPredios) {
                this.currentNumeroPredial = selectedPredio.getNumeroPredial();
                if (selectedPredio.getCodigoMunicipio().equals(
                    this.selectedMunicipioCod)) {
                    this.adicionarPredioAOferta();
                }
            }

            this.selectedPredios = new Predio[]{};
        }
    }

    /**
     * realiza la consulta correspondiente a las áreas de terreno y construcción catastrales
     * asociadas al número predial ingresado. Ahora permite calcular y acumular las areas para todos
     * los predios de la oferta
     *
     * @author
     * @modifiedBy christian.rodriguez
     */
    public void calcularAreasCatastrales() {

        Double areaTotalTerreno = 0.0;
        Double areaTotalConstruccion = 0.0;

        try {
            if (this.selectedOfertaPredio != null) {

                areaTotalTerreno = this.selectedOfertaInmobiliaria
                    .getAreaTotalTerrenoCatastral();
                areaTotalConstruccion = this.selectedOfertaInmobiliaria
                    .getAreaTotalConstruccionCata();

                double[] areasCatastrales = this.getConservacionService()
                    .getAreasCatastralesPorNumeroPredial(
                        this.selectedOfertaPredio.getNumeroPredial());

                areaTotalTerreno += areasCatastrales[0];
                areaTotalConstruccion += areasCatastrales[1];

            }
        } catch (NullPointerException e) {
            LOGGER.debug(
                "Error al calcular las areas totales del predio: No se tiene información del predio.");
        } finally {
            this.selectedOfertaInmobiliaria
                .setAreaTotalTerrenoCatastral(areaTotalTerreno);
            this.selectedOfertaInmobiliaria
                .setAreaTotalConstruccionCata(areaTotalConstruccion);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la cción de cambio de inmueble
     */
    public void cambioInmueble() {

        this.inicializarBanderasProyectosOfertasInmobiliarias();

        this.asignarBanderasTipoInmueble();

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Asigna las banderas de visualización dependiendo del tipo de inmueble
     */
    public void asignarBanderasTipoInmueble() {

        if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.CASA.getCodigo()) ||
             this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.APARTAMENTO.getCodigo()) ||
             this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.SUITEHOTELERA.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.CASA_APARTAMENTO_SH
                .getWidgetVar();
            this.banderaCasaApartamentoSuiteHotelera = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.CASALOTE.getCodigo()) ||
             this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.LOTE.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.LOTE_CASALOTE
                .getWidgetVar();
            this.banderaLoteCasaLote = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.LOCAL.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.LOCAL
                .getWidgetVar();
            this.banderaLocal = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.OFICINA.getCodigo()) ||
             this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.CONSULTORIO.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.OFICINA_CONSULTORIO
                .getWidgetVar();
            this.banderaOficinaConsultorio = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.BODEGA.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.BODEGA
                .getWidgetVar();
            this.banderaBodega = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.FINCA.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.FINCA
                .getWidgetVar();
            this.banderaFinca = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.EDIFICIO.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.EDIFICIO
                .getWidgetVar();
            this.banderaEdificio = true;
        } else if (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
            EOfertaTipoInmueble.PARQUEADERO.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.PARQUEADERO
                .getWidgetVar();
            this.banderaParqueadero = true;
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Asigna las banderas de visualización dependiendo del tipo de inmueble
     */
    public void asignarBanderasTipoInmuebleEdicion() {

        if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.CASA.getCodigo()) ||
             this.selectedOfertaOferta.getTipoInmueble().equals(
                EOfertaTipoInmueble.APARTAMENTO.getCodigo()) ||
             this.selectedOfertaOferta.getTipoInmueble().equals(
                EOfertaTipoInmueble.SUITEHOTELERA.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.CASA_APARTAMENTO_SH
                .getWidgetVar();
            this.banderaCasaApartamentoSuiteHotelera = true;
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.CASALOTE.getCodigo()) ||
             this.selectedOfertaOferta.getTipoInmueble().equals(
                EOfertaTipoInmueble.LOTE.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.LOTE_CASALOTE
                .getWidgetVar();
            this.banderaLoteCasaLote = true;
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.LOCAL.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.LOCAL
                .getWidgetVar();
            this.banderaLocal = true;
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.OFICINA.getCodigo()) ||
             this.selectedOfertaOferta.getTipoInmueble().equals(
                EOfertaTipoInmueble.CONSULTORIO.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.OFICINA_CONSULTORIO
                .getWidgetVar();
            this.banderaOficinaConsultorio = true;
            this.cargarDatosOficinaConsultorio(this.selectedOfertaOferta);
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.BODEGA.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.BODEGA
                .getWidgetVar();
            this.banderaBodega = true;
            this.cargarDatosBodega(this.selectedOfertaOferta);
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.FINCA.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.FINCA
                .getWidgetVar();
            this.banderaFinca = true;
            this.cargarDatosFinca(this.selectedOfertaOferta);
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.EDIFICIO.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.EDIFICIO
                .getWidgetVar();
            this.banderaEdificio = true;
        } else if (this.selectedOfertaOferta.getTipoInmueble().equals(
            EOfertaTipoInmueble.PARQUEADERO.getCodigo())) {
            this.selectedVentanIngreso = EWVVentanasOfertaTipoInmueble.PARQUEADERO
                .getWidgetVar();
            this.banderaParqueadero = true;
            this.cargarDatosParqueadero(this.selectedOfertaOferta);
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar el componente fotográfico
     */
    public void cerrarComponenteFotografico(CloseEvent ce) {

        this.selectedFotografia = null;

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en el componente fotográfico
     */
    public void guardarFotos() {
        this.banderaGuardarFotografias = true;
        this.banderaFotografiasCargadas = false;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de oferta
     */
    public void cambioTipoOferta() {

        if (this.selectedOfertaInmobiliaria.getTipoOferta() != null) {
            if (this.selectedOfertaInmobiliaria.getTipoOferta().equals(
                EOfertaTipoOferta.PROYECTOS.getCodigo())) {
                this.banderaOfertaNueva = true;

            } else if (this.selectedOfertaInmobiliaria.getTipoOferta().equals(
                EOfertaTipoOferta.USADOS.getCodigo())) {
                this.banderaOfertaNueva = false;
                this.selectedOfertaInmobiliaria
                    .setFormaNegociacion(EOfertaFormaNegociacion.CALCULADA
                        .toString());

            }
            this.banderaTipoOfertaSeleccionada = true;
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado cuando en la pantalla de proyecto finca se selecciona recurso hídrico
     */
    public void cambiarRecursosHidricos() {

        if (this.recursosHidricos && this.ofertasRecursosHidricos == null) {
            // Carga datos para el componente PickList de Prime Faces para
            // recursos
            // hídricos
            this.riego = false;
            this.sourceOfertasRecursosHidricos = this.generalMBService
                .getOfertasRecursosHidricosSource();
            this.targetOfertasRecursosHidricos = new ArrayList<String>();

            this.ofertasRecursosHidricos = new DualListModel<String>(
                this.sourceOfertasRecursosHidricos,
                this.targetOfertasRecursosHidricos);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que inicializa las banderas relacionadas con proyectos de oferta inmobiliaria
     *
     * @author pedro.garcia
     */
    public void inicializarBanderasProyectosOfertasInmobiliarias() {

        this.banderaCasaApartamentoSuiteHotelera = false;
        this.banderaLoteCasaLote = false;
        this.banderaLocal = false;
        this.banderaOficinaConsultorio = false;
        this.banderaBodega = false;
        this.banderaFinca = false;
        this.banderaEdificio = false;
        this.banderaParqueadero = false;
        this.banderaActivarServicioComunalOtro = false;

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que permite calcular el porcentaje de negociación según la fórmula (valorDepurado *
     * 100) / valorPedido
     *
     * @author juan.agudelo
     * @modifiedby christian.rodriguez
     */
    public void calcularPorcentajeNegociacion() {

        Double valorDepurado = this.selectedOfertaInmobiliaria
            .getValorDepurado();
        Double valorPedido = this.selectedOfertaInmobiliaria.getValorPedido();

        if (valorPedido < valorDepurado) {
            this.selectedOfertaInmobiliaria.setPorcentajeNegociacion(0.0);
            this.selectedOfertaInmobiliaria.setValorDepurado(0.0);
            String mensaje =
                "Valor depurado: Error de validación: el valor es superior que el valor pedido";
            this.addMensajeError(
                "registroOfertaInmobiliariaFormId:valorDepuradoIE", mensaje);
            return;
        } else {

            if (valorPedido >= 0 && valorDepurado >= 0) {

                Double porcentajeNegociacion = (100 * (valorPedido - valorDepurado)) /
                     valorPedido;
                this.selectedOfertaInmobiliaria
                    .setPorcentajeNegociacion(porcentajeNegociacion);

                this.selectedOfertaInmobiliaria
                    .setFormaNegociacion(EOfertaFormaNegociacion.DEPURADA
                        .toString());
            }
        }
    }

    /**
     * Método que permite calcular el valor depurado según la fórmula valorPedido *
     * porcentajeNegociacion;
     *
     * @author christian.rodriguez
     */
    public void calcularValorDepurado() {

        Double valorPedido = this.selectedOfertaInmobiliaria.getValorPedido();
        Double porcentajeNegociacion = this.selectedOfertaInmobiliaria
            .getPorcentajeNegociacion();

        if (porcentajeNegociacion < 0) {
            this.selectedOfertaInmobiliaria.setValorDepurado(0.0);
            this.selectedOfertaInmobiliaria.setPorcentajeNegociacion(0.0);
            String mensaje =
                "Porcentaje de negociación: Error de validación: el valor es inferior que el mínimo permitido de '1'";
            this.addMensajeError(
                "registroOfertaInmobiliariaFormId:porcentajeNegociacionIE",
                mensaje);
            return;
        } else {

            if (valorPedido >= 0 && porcentajeNegociacion >= 0) {

                Double valorDepurado = valorPedido -
                     ((valorPedido * porcentajeNegociacion) / 100);
                this.selectedOfertaInmobiliaria.setValorDepurado(valorDepurado);

                this.selectedOfertaInmobiliaria
                    .setFormaNegociacion(EOfertaFormaNegociacion.CALCULADA
                        .toString());
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que elimina las fotografias seleccionadas
     */
    public void eliminarFotos() {

        OfertaInmobiliaria oferta = getOfertaEnEdicionParaFotografias();

        if (oferta != null && oferta.getFotografias() != null &&
             !oferta.getFotografias().isEmpty()) {

            List<Fotografia> fotosTemp = new ArrayList<Fotografia>();
            for (Fotografia f : oferta.getFotografias()) {
                if (f.isFotografiaSeleccionada()) {
                    fotosTemp.add(f);
                }
            }

            if (!fotosTemp.isEmpty()) {
                for (Fotografia ft : fotosTemp) {
                    oferta.getFotografias().remove(ft);
                }
            }

            if (oferta.getFotografias().isEmpty()) {
                this.banderaPanelEdicionFotos = false;
            }
        }
    }

    /**
     * Método que devuelve la oferta que se está editando para cargar en el componente de fotos
     *
     * @author christian.rodriguez
     * @return oferta activa
     */
    private OfertaInmobiliaria getOfertaEnEdicionParaFotografias() {
        OfertaInmobiliaria oferta;
        if (this.banderaFotografiaOfertaOferta) {
            oferta = this.selectedOfertaOferta;
        } else {
            oferta = this.selectedOfertaInmobiliaria;
        }
        return oferta;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que elimina todas las fotografias asociadas a una oferta inmobiliaria
     */
    public void eliminarFotosAll() {

        OfertaInmobiliaria oferta = this.getOfertaEnEdicionParaFotografias();

        oferta.setFotografias(new ArrayList<Fotografia>());

        this.banderaPanelEdicionFotos = false;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método encargado de guardar la oferta inmobiliaria y terminar
     */
    public void guardarYTerminar() {

        try {
            if (!this.validarDatosRequeridosOferta()) {
                this.listaOfertasEliminadas
                    .add(this.selectedOfertaInmobiliaria);
            } else {
                if (this.selectedOfertaInmobiliaria.getTipoOferta().equals(
                    EOfertaTipoOferta.USADOS.getCodigo()) ||
                     !this.banderaOfertaNueva) {
                    this.actualizarBooleanosOfertaUsada();
                }
                this.guardarOfertaInmobiliaria();
            }
        } catch (Exception e) {
            String mensaje = "Ocurrió un error al guardar la oferta inmobiliaria." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }
    }

    /**
     * Devuelve si el inmueble seleccionado es de tipo Casa
     *
     * @author christian.rodriguez
     * @return verdadero si es tipo casa, falso si no
     */
    public boolean isInmuebleTipoCasa() {
        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getTipoInmueble() != null) {
            return this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.CASA.getCodigo());
        }
        return false;
    }

    /**
     * Devuelve si el inmueble seleccionado es de tipo Suite Hotelera
     *
     * @author christian.rodriguez
     * @return verdadero si es tipo Suite Hotelera, falso si no
     */
    public boolean isInmuebleTipoSH() {
        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getTipoInmueble() != null) {
            return this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.SUITEHOTELERA.getCodigo());
        }
        return false;
    }

    /**
     * Devuelve si el inmueble seleccionado es de tipo Consultorio
     *
     * @author christian.rodriguez
     * @return verdadero si es tipo consultorio, falso si no
     */
    public boolean isInmuebleTipoConsultorio() {
        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getTipoInmueble() != null) {
            return this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.CONSULTORIO.getCodigo());
        }
        return false;
    }

    /**
     * Actualiza los valores booleanos de los proyectos inmoboliarios que tiene atributos de este
     * tipo
     *
     * @author christian.rodriguez
     */
    private void actualizarBooleanosOfertaUsada() {
        String tipoInmueble = this.selectedOfertaInmobiliaria.getTipoInmueble();
        if (tipoInmueble.equals(EOfertaTipoInmueble.LOCAL.getCodigo())) {
            this.actualizarBooleanosProyectoLocal();
            return;
        }
        if (tipoInmueble.equals(EOfertaTipoInmueble.FINCA.getCodigo())) {
            this.actualizarBooleanosProyectoFinca();
            return;
        }
        if (tipoInmueble.equals(EOfertaTipoInmueble.OFICINA.getCodigo()) ||
             tipoInmueble.equals(EOfertaTipoInmueble.CONSULTORIO
                .getCodigo())) {
            this.actualizarBooleanosProyectoOficinaConsultorio();
            return;
        }
        if (tipoInmueble.equals(EOfertaTipoInmueble.BODEGA.getCodigo())) {
            this.actualizarBooleanosProyectoBodega();
            return;
        }
        if (tipoInmueble.equals(EOfertaTipoInmueble.PARQUEADERO.getCodigo())) {
            this.actualizarBooleanosProyectoParqueadero();
            return;
        }
    }

    public boolean isActivarComponenteBusquedaGeneral() {
        if (this.banderaCreacionOfertaDesdeCargaAlSNC ||
             this.banderaCreacionOfertaDesdeModOfertasDevueltas) {
            return false;
        }
        return true;
    }

    /**
     * Asigna el departamento y municipio seleccionados en los datos de consulta para que sean
     * precargados en la consulta de predios
     *
     * @author christian.rodriguez
     */
    public void preCargarDatosConsulta() {

        this.consultaPredioMB.setSeUsaTerritorial(false);
        this.consultaPredioMB.setSeUsanDeptosFijos(true);

        FiltroDatosConsultaPredio consulta = this.consultaPredioMB
            .getDatosConsultaPredio();

        if (this.departamentos != null) {
            this.consultaPredioMB.setDepartamentosItemList(this.departamentos);
        }

        if (this.municipios != null) {
            this.consultaPredioMB.setMunicipiosItemList(this.municipios);
        }

        if (this.selectedDepartamentoCod != null &&
             !this.selectedDepartamentoCod.isEmpty()) {
            consulta.setDepartamentoId(this.selectedDepartamentoCod);
            consulta.setNumeroPredialS1(this.selectedDepartamentoCod);
        } else {
            consulta.setDepartamentoId(null);
            consulta.setNumeroPredialS1(null);
        }
        if (this.selectedMunicipioCod != null &&
             !this.selectedMunicipioCod.isEmpty()) {
            consulta.setMunicipioId(this.selectedMunicipioCod);
            consulta.setNumeroPredialS2(this.selectedMunicipioCod.substring(2));
        } else {
            consulta.setMunicipioId(null);
            consulta.setNumeroPredialS2(null);
        }

        this.consultaPredioMB.setDatosConsultaPredio(consulta);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * toma el predio seleccionado del carrito y define los valores de los atributos de este MB que
     * correspondan
     *
     * @author pedro.garcia
     */
    public void usarPredioSeleccionado() {

        ArrayList<Predio> selectedPredios;

        if (this.consultaPredioMB.getPrediosSeleccionados2() != null &&
             !this.consultaPredioMB.getPrediosSeleccionados2().isEmpty()) {

            selectedPredios = (ArrayList<Predio>) this.consultaPredioMB
                .getPrediosSeleccionados2();
            this.selectedDepartamentoCod = selectedPredios.get(0)
                .getDepartamento().getCodigo();
            this.setSelectedDepartamentoCod(this.selectedDepartamentoCod);
            this.selectedMunicipioCod = selectedPredios.get(0).getMunicipio()
                .getCodigo();
            this.updateMunicipiosItemList();
            this.setSelectedMunicipioCod(this.selectedMunicipioCod);

            this.selectedOfertaInmobiliaria.setDireccion(selectedPredios.get(0)
                .getDireccionPrincipal());
            this.asignarTipoPredio(selectedPredios.get(0));

            for (Predio selectedPredio : selectedPredios) {
                this.currentNumeroPredial = selectedPredio.getNumeroPredial();
                if (selectedPredio.getCodigoMunicipio().equals(
                    this.selectedMunicipioCod)) {
                    this.adicionarPredioAOferta();
                }
            }

            this.obtenerNombreDeptoMunicSeleccionados();
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar
     *
     * @modified rodrigo.hernandez 15-06-2012 adición de validacion para ajustar navegacion despues
     * de cerrada la página.
     */
    public String terminar() {

        UtilidadesWeb.removerManagedBean("consultaPredio");

        if (this.banderaCreacionOfertaDesdeCargaAlSNC) {
            UtilidadesWeb.removerManagedBean("registroOfertaInmob");
            return "/avaluos/ofertasInmob/ejecucion/cargaOfertasAlSNC.jsf";
        } else if (this.banderaCreacionOfertaDesdeModOfertasDevueltas) {
            UtilidadesWeb.removerManagedBean("registroOfertaInmob");
            return "/avaluos/ofertasInmob/validacion/modificarOfertasDevuelta.jsf";
        } else if (this.banderaCreacionOfertaDesdeConsultaOfertas) {
            UtilidadesWeb.removerManagedBean("registroOfertaInmob");
            return "/avaluos/ofertasInmob/consultaOfertas.jsf";
        } else {
            UtilidadesWeb.removerManagedBean("registroOfertaInmob");
            return ConstantesNavegacionWeb.INDEX;
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al agregar fotografías a la oferta inmobiliaria principal
     */
    public void bloquearAEdicionOfertas() {
        this.banderaCargarFotosOfertaPrincipal = true;
        this.cargarComponenteFotograficoYFotos();
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para validar la adición de fotografías a la oferta inmobiliaria principal
     */
    public void validarCargarFotosOPrincipal() {
        RequestContext context = RequestContext.getCurrentInstance();

        if (this.selectedOfertaInmobiliaria == null ||
             this.selectedOfertaInmobiliaria.getOfertaInmobiliarias() == null ||
             this.selectedOfertaInmobiliaria.getOfertaInmobiliarias()
                .isEmpty()) {

            String mensaje = "La oferta inmobiliaria principal no cuenta con ofertas" +
                 " inmobiliarias asociadas." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }
    }

    /**
     * Metodo usado para obtener el codigo de la condición juridica para luego ser al seleccionar un
     * predio
     *
     * author christian.rodriguez
     */
    public void obtenerCondicionJuridica() {

        if (this.selectedOfertaPredio != null &&
             this.selectedOfertaPredio.getNumeroPredial().length() >= 22) {

            String numPredial22 = this.selectedOfertaPredio.getNumeroPredial()
                .substring(21, 22);

            String codigoCondicionJuridica;

            if (ECondicionJuridicaNumPredial.PROPIEDAD_HORIZONTAL.getCodigo()
                .contains(numPredial22)) {
                codigoCondicionJuridica = ECondicionJuridica.PROPIEDAD_HORIZONTAL
                    .getCodigo();
            } else {
                codigoCondicionJuridica = ECondicionJuridica.NO_PROPIEDAD_HORIZONTAL
                    .getCodigo();
            }

            this.selectedOfertaInmobiliaria
                .setCondicionJuridica(codigoCondicionJuridica);

        }

    }

    /**
     * Devuelve una bandera que determina si se debe pintar el picklist de areas de uso exclusivo
     *
     * @author christian.rodriguez
     * @return boolean bandera que indica si se pinta o no el picklist
     */
    public boolean isMostrarAreasUsoExclusivo() {

        try {
            if (this.selectedOfertaInmobiliaria.getAreasUsoExclusivo().equals(
                "SI")) {
                return true;
            } else {
                this.sourceAreasUsoExclusivo = this.generalMBService
                    .getOfertasAreasUsoExclusivoSource();
                this.targetAreasUsoExclusivo = new ArrayList<String>();
                this.areasUsoExclusivo = new DualListModel<String>(
                    this.sourceAreasUsoExclusivo,
                    this.targetAreasUsoExclusivo);
                return false;
            }
        } catch (NullPointerException ex) {
            return true;
        }

    }

    /**
     * Valida que si se seleccionó el servicio comunal OTRO, debe ingresarse algún texto en la
     * casilla Otro servicio cumnal
     *
     * @author christian.rodriguez
     * @return verdadero si se ingreso el otro servicio comunal, falso en otro caso
     */
    private boolean validarServicioComunalOtro() {
        if (this.selectedOfertaInmobiliaria != null) {

            List<String> servComunalesSeleccionados = this
                .getServiciosComunalesOferta().getTarget();

            if (servComunalesSeleccionados.isEmpty()) {
                return true;
            }

            for (String servicioComunal : servComunalesSeleccionados) {
                if (servicioComunal.toLowerCase().equals(
                    EOfertaServicioComunal.OTRO.getCodigo().toLowerCase())) {
                    if (this.selectedOfertaInmobiliaria
                        .getOtrosServiciosComunales().isEmpty()) {
                        String mensaje =
                            "Otros Servicios Comunales: Error de validación: se necesita un valor.";
                        this.addMensajeError(
                            "registroOfertaInmobiliariaFormId:otrosServiciosComunales",
                            mensaje);
                        this.banderaActivarServicioComunalOtro = true;
                        return false;
                    }
                }
            }
            return true;
        }
        return true;

    }

    /**
     * Devuelve el estilo determinado para mostrar u ocultar el campo de otros servicios comunales,
     * se ua generalmente para cuando se refresca la página despues de que haya ocurrido un error
     *
     * @author christian.rodriguez
     * @return el estilo css que hace visible el campo otro servicio comunal cuando la bandera está
     * activada
     */
    public String getEstiloServicioComunalOtro() {
        if (this.banderaActivarServicioComunalOtro) {
            return "display:block; margin:0;";
        } else {
            return "display:none; margin:0;";
        }
    }

    /**
     * Valida que si indico que hay mas de una cocina se debe obligar a seleccionar el tipo de
     * cocina
     *
     * @author christian.rodriguez
     * @return verdadero si se selecciono el tipo de cocina, falso si no
     */
    public boolean validarTipoNumeroCocina() {

        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getNumeroCocinas() != null &&
             this.selectedOfertaInmobiliaria.getNumeroCocinas() > 0) {

            if (this.selectedOfertaInmobiliaria.getTipoCocina() == null) {
                String mensaje = "Tipo Cocina: Error de validación: se necesita un valor.";
                this.addMensajeError(
                    "registroOfertaInmobiliariaFormId:tipoCocinaCASH",
                    mensaje);
                return false;
            }
        }
        return true;
    }

    /**
     * Valida que si se seleccionó que tiene áreas de uso exclusivo se seleccione al menos un área
     * de uso exclusivo
     *
     * @author christian.rodriguez
     * @return falso si se selecciono que si tiene áreas de uso exclusivo y no se selecciono al
     * menus una de la lista, v en otro caso.
     */
    public boolean validarAreasUsoExclusivo() {
        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getAreasUsoExclusivo()
                .equals(ESiNo.SI.toString())) {
            if (this.areasUsoExclusivo.getTarget().isEmpty()) {
                String mensaje =
                    "Áreas de uso exclusivo: Se requiere seleccionar al menos un área de uso exclusivo de la lista.";
                this.addMensajeError(
                    "registroOfertaInmobiliariaFormId:tipoCocinaCASH",
                    mensaje);
                return false;
            }
        }
        return true;
    }

    /**
     * Retorna una bandera que activa o desactiva la visualización del campo para ingresar las
     * caracteristicas del fondo del lote
     *
     * @author christian.rodriguez
     * @return boolean
     */
    public boolean isActivarFondoLote() {

        OfertaInmobiliaria oferta = this.getOfertaEnEdicion();

        if (oferta.getFormaLote() != null) {
            String formaLote = oferta.getFormaLote();
            if (formaLote.toLowerCase().equals(
                EOfertaFormaLote.REGULAR.getCodigo().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna una bandera que activa o desactiva la visualización del campo para ingresar las
     * caracteristicas del tipo de edificio
     *
     * @author christian.rodriguez
     * @return boolean
     */
    public boolean isActivarTipoEdificio() {

        OfertaInmobiliaria oferta = this.getOfertaEnEdicion();

        if (oferta.getUbicacionOficina() != null) {
            String ubicacion = oferta.getUbicacionOficina();
            if (ubicacion.toLowerCase().equals(
                EOfertaUbicacionOficina.EDIFICIO.getCodigo().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que devuelve la oferta inmobiliaria que se está editando, de ser un proyecto devuelve
     * el inmueble que se este actualizando
     *
     * @author christian.rodriguez
     * @return oferta inmobiliaria que se está editando
     */
    private OfertaInmobiliaria getOfertaEnEdicion() {
        OfertaInmobiliaria oferta;

        if (this.selectedOfertaInmobiliaria.isEsProyecto()) {
            oferta = this.selectedOfertaOferta;
        } else {
            oferta = this.selectedOfertaInmobiliaria;
        }
        return oferta;
    }

    /**
     * Elimina un predio de la tabla de predios
     *
     * @author christian.rodriguez
     */
    public void eliminarPredioDeOferta() {
        OfertaPredio currentPredio = new OfertaPredio();
        currentPredio.setNumeroPredial(this.currentNumeroPredial);
        this.selectedOfertaInmobiliaria.getOfertaPredios()
            .remove(currentPredio);

        if (this.selectedOfertaInmobiliaria.getOfertaPredios().isEmpty()) {
            this.selectedOfertaInmobiliaria.setCondicionJuridica(null);
            this.selectedOfertaInmobiliaria.setTipoPredio(null);
        }
    }

    /**
     * Realiza las validaciones necesarias y agrega un predio a la tabla de predios
     *
     * @author christian.rodriguez
     */
    public void adicionarPredioAOferta() {

        String mensajeError = "";
        boolean predioValido = true;
        this.selectedOfertaPredio = new OfertaPredio();
        this.selectedOfertaPredio.setNumeroPredial(this.currentNumeroPredial);

        String numeroPredial = this.selectedOfertaPredio.getNumeroPredial();

        if (predioValido &&
             this.selectedOfertaInmobiliaria.getOfertaPredios().contains(
                this.selectedOfertaPredio)) {
            mensajeError = "Adicionar Predio: El predio " + numeroPredial +
                 " ya ha sido agregado a la oferta.\n";
            predioValido = false;
        }
        if (predioValido && this.selectedDepartamentoCod.isEmpty()) {
            mensajeError =
                "Adicionar Predio: No se ha seleccionado un departamento al que pertenece el predio.\n";
            predioValido = false;
        }
        if (predioValido && this.selectedMunicipioCod.isEmpty()) {
            mensajeError =
                "Adicionar Predio: No se ha seleccionado municipio al que pertenece el predio.\n";
            predioValido = false;
        }
        if (predioValido) {
            Predio predio = this.validarNumeroPredial(numeroPredial);
            if (predio == null) {
                mensajeError = "Adicionar Predio: El predio ingresado no se ha encontrado.\n";
                predioValido = false;
            } else if (predio.getId() != null) {
                // N: los predios no centralizados uqedan con id_predio null en
                // la tabla OFERTA_PREDIO
                this.selectedOfertaPredio.setPredioId(new BigDecimal(predio
                    .getId()));
            }
            if (predioValido &&
                 predio.getMunicipio() != null &&
                 !predio.getCodigoMunicipio().equals(
                    this.selectedMunicipioCod)) {
                mensajeError =
                    "Adicionar Predio: El predio ingresado no pertenece al Municipio seleccionado.\n";
                predioValido = false;
            }
        }
        if (predioValido && !this.validarColindancia(numeroPredial)) {
            mensajeError = "Adicionar Predio: El predio " + numeroPredial +
                 " no es colindante con los ingresados anteriormente.\n";
            predioValido = false;
        }

        if (predioValido) {

            if (this.selectedOfertaInmobiliaria.getOfertaPredios().isEmpty() &&
                 !this.banderaEsEdicion) {
                this.obtenerCondicionJuridica();
                this.cambioCondicionJuridica();
                this.obtenerNombreDeptoMunicSeleccionados();
            }
            this.calcularAreasCatastrales();
            this.selectedOfertaInmobiliaria.getOfertaPredios().add(
                this.selectedOfertaPredio);
        } else {

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeError(mensajeError);
        }
    }

    /**
     * Asigna el atributo tipo predio
     *
     * @author christian.rodriguez
     * @param predio
     */
    public void asignarTipoPredio(Predio predio) {

        if (predio != null) {
            String tipoPredio = (predio.getTipoAvaluo()
                .equals(EOfertaTipoPredio.RURAL.getCodigo())) ? EOfertaTipoPredio.RURAL
                .toString() : EOfertaTipoPredio.URBANO.toString();
            this.selectedOfertaInmobiliaria.setTipoPredio(tipoPredio);
        }
    }

    /**
     * Asigna el atributo unidad de area para los inmuebles que no son fincas
     *
     * @author christian.rodriguez
     */
    public void asignarUnidadesArea() {

        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getUnidadArea() == null) {
            this.selectedOfertaInmobiliaria
                .setUnidadArea(EOfertaUnidadArea.METRO_CUADRADO.getCodigo());
        }
    }

    /**
     * Asigna el atributo vetustez a null para los inmuebles de tipo FINCA y LOTE
     *
     * @author christian.rodriguez
     */
    public void borrarVetustezFincaYLote() {

        if (this.selectedOfertaInmobiliaria != null &&
             this.selectedOfertaInmobiliaria.getTipoInmueble() != null &&
             (this.selectedOfertaInmobiliaria.getTipoInmueble().equals(
                EOfertaTipoInmueble.FINCA.getCodigo()) || this.selectedOfertaInmobiliaria
            .getTipoInmueble().equals(
                EOfertaTipoInmueble.LOTE.getCodigo()))) {
            this.selectedOfertaInmobiliaria.setVetustez(null);
        }
    }

    /**
     * Valida el número predial.
     *
     * @param numeroPredial numero predial a validar
     * @return Si pertenece a un municipio descentralizado se toma como valido, si no entonces debe
     * existir en la base
     * @author christian.rodriguez
     */
    private Predio validarNumeroPredial(String numeroPredial) {

        if (banderaPerteneceACatastroDescentralizado) {
            return new Predio();
        }

        Predio predio = this.getConservacionService().getPredioByNumeroPredial(
            numeroPredial);

        if (predio == null) {
            return null;
        }
        this.asignarTipoPredio(predio);
        return predio;
    }

    /**
     * En un futuro valida que un predio sea colindante con otro ¬¬
     *
     * @author christian.rodriguez
     * @param numeroPredial
     * @return
     */
    private boolean validarColindancia(String numeroPredial) {

        if (this.selectedOfertaInmobiliaria.getOfertaPredios().isEmpty()) {
            return true;
        }

        if (this.getGeneralesService()
            .municipioPerteneceACatastroDescentralizado(
                this.selectedMunicipioCod)) {
            return true;
        }

        List<String> numerosPrediales = new ArrayList<String>();
        for (OfertaPredio numeroPredialOP : this.selectedOfertaInmobiliaria
            .getOfertaPredios()) {
            numerosPrediales.add(numeroPredialOP.getNumeroPredial());
        }
        return this.getGeneralesService().esColindante(numeroPredial,
            numerosPrediales);
    }

    /**
     * Obtiene y setea en la tabla de predios los nombres del departamento y municipio seleccionados
     *
     * @author christian.rodriguez
     */
    private void obtenerNombreDeptoMunicSeleccionados() {
        for (Iterator<Departamento> departamentoIt = this.departamentos
            .iterator(); departamentoIt.hasNext();) {
            Departamento departamento = (Departamento) departamentoIt.next();
            if (departamento.getCodigo().equals(this.selectedDepartamentoCod)) {
                this.selectedDepartamentoNom = departamento.getNombre();
                break;
            }
        }

        for (Iterator<Municipio> municipioIt = this.municipios.iterator(); municipioIt
            .hasNext();) {
            Municipio municipio = (Municipio) municipioIt.next();
            if (municipio.getCodigo().equals(this.selectedMunicipioCod)) {
                this.selectedMunicipioNom = municipio.getNombre();
                break;
            }
        }
    }

    /**
     * Retorna una bandera que activa o desactiva la visualización del campo para ingresar estrato
     *
     * @author christian.rodriguez
     * @return boolean
     */
    public boolean isActivarEstrato() {

        if (this.selectedOfertaInmobiliaria.getTipoPredio() != null &&
             this.selectedOfertaInmobiliaria.getDestino() != null) {

            String tipoPredio = this.selectedOfertaInmobiliaria.getTipoPredio();
            if (!tipoPredio.equalsIgnoreCase(EOfertaTipoPredio.URBANO
                .toString())) {
                this.selectedOfertaInmobiliaria.setEstratoSocioeconomico(null);
                return false;
            }

            String destino = this.selectedOfertaInmobiliaria.getDestino();
            if (!destino
                .equalsIgnoreCase(EOfertaDestino.RESIDENCIAL.toString())) {
                this.selectedOfertaInmobiliaria.setEstratoSocioeconomico(null);
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * Método para asignar una RegionCapturaOferta
     *
     * @param regionCapturaOfertaId - Id de la RegionCapturaOferta
     * @param recolectorId - Id del recolector
     *
     * @author rodrigo.hernandez
     * @modifiedby christian.rodriguez se agregó métodos para asignar departamento y municipio de
     * acuerdo a la region enviada
     */
    public void precargaDatosCapturaOferta(
        RegionCapturaOferta regionCapturaOferta, String recolectorId) {

        if (regionCapturaOferta != null) {
            this.regionCapturaOferta = regionCapturaOferta;
            this.regionCapturaOfertaId = regionCapturaOferta.getId();

            this.precargarDatosDeptoYMunic(regionCapturaOferta
                .getAreaCapturaOferta().getDepartamento(),
                regionCapturaOferta.getAreaCapturaOferta().getMunicipio());

        } else {
            this.precargarDatosDeptoYMunic(
                this.selectedOfertaInmobiliaria.getDepartamento(),
                this.selectedOfertaInmobiliaria.getMunicipio());
        }

        this.banderaPerteneceACatastroDescentralizado = this
            .getGeneralesService()
            .municipioPerteneceACatastroDescentralizado(
                this.selectedMunicipioCod);

        this.recolectorId = recolectorId;
    }

    /**
     * Método que crea y selecciona los combos de departamento y municipio con los parametros
     * definidos
     *
     * @author christian.rodriguez
     * @param departamento departamento que va a aparecer en el combo departamentos como única
     * opción
     * @param municipio municipio que va a aparecer en el combo municipio como única opción
     */
    public void precargarDatosDeptoYMunic(Departamento departamento,
        Municipio municipio) {

        this.departamentoOferta = departamento;
        this.municipioOferta = municipio;

        this.departamentos = new ArrayList<Departamento>();
        this.municipios = new ArrayList<Municipio>();

        this.departamentos.add(departamento);
        this.municipios.add(municipio);

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList = new ArrayList<SelectItem>();

        this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
            departamento.getNombre()));
        this.municipiosItemList.add(new SelectItem(municipio.getCodigo(),
            municipio.getNombre()));

        this.selectedDepartamentoCod = departamento.getCodigo();
        this.selectedMunicipioCod = municipio.getCodigo();

        this.selectedOfertaInmobiliaria.setDepartamento(departamento);
        this.selectedOfertaInmobiliaria.setMunicipio(municipio);

        this.obtenerNombreDeptoMunicSeleccionados();

    }

    /**
     * Método que carga los predios de captura asociados a la región de captura oferta y los
     * almacena en el atributo
     *
     * @author christian.rodriguez
     */
    public void cargarPrediosRegionCaptura() {

        this.prediosRegionCaptura = null;

        if (this.regionCapturaOferta != null) {

            this.prediosRegionCaptura = this.getAvaluosService()
                .consultarPrediosPorRegionCapturaOferta(
                    this.regionCapturaOferta, this.usuario.getLogin());

        }
    }

    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {

        this.applicationClientName = this.contextoWebApp.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }

}
