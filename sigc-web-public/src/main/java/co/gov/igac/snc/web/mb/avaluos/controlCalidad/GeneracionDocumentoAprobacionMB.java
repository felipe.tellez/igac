/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.MonedaConverter;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso CU-SA-AC-056 Generar Documento de Aprobación Avalúo para Solicitante viene
 * del caso de uso CU-SA-AC-025
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-056
 */
@Component("generacionDocumentoAprobacion")
@Scope("session")
public class GeneracionDocumentoAprobacionMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 604549457386987112L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneracionDocumentoAprobacionMB.class);
    /**
     * Avalúo asociado a la consulta de controles de calidad
     */
    private Avaluo avaluo;
    /**
     * Lista de avaluos relacionados al documento
     */
    private List<Avaluo> avaluos;
    /**
     * Solicitud relacionada a los avaluos del documento
     */
    private Solicitud solicitud;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Orden de practica relacionada al avaluo
     */
    private OrdenPractica ordenPractica;
    /**
     * Coordinador GIT de avalúos de la territorial en la que se realizó el avaluo
     */
    private UsuarioDTO coordinadorGITAvaluos;

    /**
     * Documento relacionado al reporte generado
     */
    private Documento documentoAprobacion;

    /**
     * Archivo del reporte de orden de práctica
     */
    private File oficioFile;
    /**
     * DTO del reporte del oficio
     */
    private ReporteDTO oficioDto;

    /**
     * Bandera para determinar que el oficio ya fue generado.
     */
    private Boolean banderaOficioGenerado;

    /**
     * Fecha de generación del oficio
     */
    private Date fechaOficio;

    /**
     * Valor total del avaluo comercial de los sec radicados relacionados al documento.
     */
    private Double valorTotal;

    /**
     * Valor total del avaluo comercial de los sec radicados relacionados al documento.
     */
    private EstructuraOrganizacional territorial;
    /**
     * Valor total del avaluo comercial de los sec radicados relacionados al documento.
     */
    private Boolean banderaPersonaJuridica;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();
    @Autowired
    protected IContextListener contexto;

    // ----------------------------Métodos SET y GET----------------------------
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ReporteDTO getOficioDto() {
        return oficioDto;
    }

    public void setOficioDto(ReporteDTO oficioDto) {
        this.oficioDto = oficioDto;
    }

    public Boolean getBanderaPersonaJuridica() {
        return banderaPersonaJuridica;
    }

    public void setBanderaPersonaJuridica(Boolean banderaPersonaJuridica) {
        this.banderaPersonaJuridica = banderaPersonaJuridica;
    }

    public EstructuraOrganizacional getTerritorial() {
        return territorial;
    }

    public void setTerritorial(EstructuraOrganizacional territorial) {
        this.territorial = territorial;
    }

    public Double getValorTotal() {
        return this.valorTotal;
    }

    public String getValorTotalAsString() {
        String ans = MonedaConverter.getAsString(this.valorTotal);

        return ans;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public List<Avaluo> getAvaluos() {
        return avaluos;
    }

    public void setAvaluos(List<Avaluo> avaluos) {
        this.avaluos = avaluos;
    }

    public Date getFechaOficio() {
        return fechaOficio;
    }

    public void setFechaOficio(Date fechaOficio) {
        this.fechaOficio = fechaOficio;
    }

    public Documento getDocumentoAprobacion() {
        return documentoAprobacion;
    }

    public void setDocumentoAprobacion(Documento documentoAprobacion) {
        this.documentoAprobacion = documentoAprobacion;
    }

    public File getOficioFile() {
        return oficioFile;
    }

    public void setOficioFile(File oficioFile) {
        this.oficioFile = oficioFile;
    }

    public Boolean getBanderaOficioGenerado() {
        return banderaOficioGenerado;
    }

    public void setBanderaOficioGenerado(Boolean banderaOficioGenerado) {
        this.banderaOficioGenerado = banderaOficioGenerado;
    }

    public OrdenPractica getOrdenPractica() {
        return ordenPractica;
    }

    public void setOrdenPractica(OrdenPractica ordenPractica) {
        this.ordenPractica = ordenPractica;
    }

    public UsuarioDTO getCoordinadorGITAvaluos() {
        return coordinadorGITAvaluos;
    }

    public void setCoordinadorGITAvaluos(UsuarioDTO coordinadorGITAvaluos) {
        this.coordinadorGITAvaluos = coordinadorGITAvaluos;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on GeneracionDocumentoAprobacionMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        this.fechaOficio = new Date();
    }

    /**
     * Método para mostrar la vista previa del oficio remisorio
     *
     * @author felipe.cadena
     */
    public void verVistaPrevia() {
        LOGGER.debug("Generando vista previa...");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        previsualizacionDocMB.setReporteDTO(this.oficioDto);

    }

    /**
     * Método para generar el documento del oficio desde el servidor de reo¿portes
     *
     * @author felipe.cadena
     *
     */
    private void generarDocumento() {

        TipoDocumento tipoDocumentoOP = new TipoDocumento();
        tipoDocumentoOP.setId(ETipoDocumento.OFICIOS_REMISORIOS
            .getId());
        tipoDocumentoOP.setNombre(ETipoDocumento.OFICIOS_REMISORIOS
            .getNombre());

        this.documentoAprobacion = new Documento();
        this.documentoAprobacion.setArchivo(this.oficioDto.getArchivoReporte().getName());
        this.documentoAprobacion.setTipoDocumento(tipoDocumentoOP);
        this.documentoAprobacion.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.documentoAprobacion.setBloqueado(ESiNo.NO.getCodigo());
        this.documentoAprobacion.setUsuarioCreador(this.usuario.getLogin());
        this.documentoAprobacion.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        this.documentoAprobacion.setFechaLog(new Date(System.currentTimeMillis()));
        this.documentoAprobacion.setFechaDocumento(this.fechaOficio);
        this.documentoAprobacion.setUsuarioLog(this.usuario.getLogin());
        this.documentoAprobacion.setDescripcion("Oficio remisorio de aprobación del avalúo " +
             this.avaluo.getSecRadicado());

    }

    /**
     * Método para guardar el documento del oficio en alfresco y asignarlo a la revisión de control
     * de calidad correspondiente.
     *
     * @author felipe.cadena
     * @param tipoDocumento {@link TipoDocumento} del documento a guardar
     * @return
     */
    private void guardarDocumentoAlfresco(TipoDocumento tipoDocumento) {

        Solicitud solTemp = this.avaluo.getTramite().getSolicitud();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(
                solTemp.getNumero(), solTemp.getFecha(),
                tipoDocumento.getNombre(),
                this.oficioDto.getArchivoReporte().getAbsolutePath());

        this.documentoAprobacion = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, this.documentoAprobacion);

        if (this.documentoAprobacion == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

    }

    /**
     * Método para guardar la información del oficio en base de datos y en alfresco.
     *
     * @author felipe.cadena
     *
     */
    public void guardarDocumento() {

        this.guardarDocumentoAlfresco(this.documentoAprobacion.getTipoDocumento());

        this.documentoAprobacion = this.getGeneralesService().guardarDocumento(
            this.documentoAprobacion);
//TODO :: felipe.cadena::relacionar el documento con la entidad cons.documento y persistir

        TramiteDocumento td = new TramiteDocumento();
        td.setDocumento(this.documentoAprobacion);
        td.setTramite(this.avaluo.getTramite());
        td.setFechaLog(new Date());
        td.setfecha(new Date());
        td.setUsuarioLog(this.usuario.getLogin());

        td = this.getTramiteService().actualizarTramiteDocumento(
            td);
        if (td == null) {
            this.addMensajeError("Error al relacionar el documento al tramite");
        }

    }

    /**
     * Método para enviar el oficio por correo al coordinador del GIT de avalúos
     *
     * @author felipe.cadena
     */
    public void confirmarOficio() {
        LOGGER.debug("Confirmando oficio");

        this.generarReporte();
        this.generarDocumento();
        this.guardarDocumento();

        this.banderaOficioGenerado = true;

        LOGGER.debug("Oficio confirmado");
    }

    /**
     * Método para habilitar la modificación del documento
     *
     * @author felipe.cadena
     */
    public void modificarDocumento() {
        LOGGER.debug("modificacndo documento");

        this.banderaOficioGenerado = true;

    }

    /**
     * Método para generar el oficio remisorio
     *
     * @author felipe.cadena
     */
    public void generarReporte() {
        LOGGER.debug("En GeneracionDocumentoAprobacionMB#generarReporte...");

        // TODO :: felipe.cadena :: Cambiar la url del reporte cuando esté definido
        // Nombre del reporte
        String urlReporte = EReporteServiceSNC.REPORTE_PRUEBA
            .getUrlReporte();

        // Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("NUMERO_RADICADO", "");

        try {
            this.oficioDto = this.reportsService.
                generarReporte(parameters, EReporteServiceSNC.REPORTE_PRUEBA, usuario);
        } catch (Exception ex) {
            LOGGER.error("error generando reporte: " + ex.getMessage());
            this.addMensajeError("Error al generar el reporte");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER,
                ex, urlReporte);
        }
    }

    /**
     * Método para cargar el coordinador del git avaluos y el director territorial
     *
     * @author felipe.cadena
     */
    private void cargarCoordinadores() {

        List<UsuarioDTO> coordinadoresGITAvaluos = this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                ERol.COORDINADOR_GIT_AVALUOS);

        if (!coordinadoresGITAvaluos.isEmpty()) {
            this.coordinadorGITAvaluos = coordinadoresGITAvaluos.get(0);
        }

    }

    /**
     * Método para llamar este caso de uso desde otros CU se debe inicializar el avaluo.id para
     * cargar los datos del avalúo
     *
     * @author felipe.cadena
     */
    public void cargarDesdeOtrosMB(List<Avaluo> avaluos) {
        this.avaluos = new ArrayList<Avaluo>();
        this.valorTotal = 0D;

        for (Avaluo ava : avaluos) {
            ava = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(ava.getId());
            Double avaComercial = 0D;
            for (AvaluoPredio ap : ava.getAvaluoPredios()) {
                avaComercial += ap.getAvaluoComercial();
                this.valorTotal += ap.getAvaluoComercial();
            }
            ava.setAvaluoComercial(avaComercial);
            ava.setAvaluoComercialAsString(MonedaConverter.getAsString(ava.getAvaluoComercial()));
            this.avaluos.add(ava);

        }

        this.solicitud = this.avaluos.get(0).getTramite().getSolicitud();
        this.banderaOficioGenerado = false;
        if (this.solicitud.getSolicitante().getTipoPersona().
            equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {
            this.banderaPersonaJuridica = true;
        } else {
            this.banderaPersonaJuridica = false;
        }
        this.territorial = this.getGeneralesService().
            buscarEstructuraOrganizacionalPorCodigo(this.solicitud.getTerritorialCodigo());
        this.cargarCoordinadores();

    }

}
