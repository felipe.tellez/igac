/*
 *
 * Proyecto SNC
 *
 */
package co.gov.igac.snc.web.mb.conservacion.asignacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaContingencia;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.colecciones.Pair;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SelectItemComparator;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Clase asociada al caso de uso CU-NP-CO-219 Administrar asignación de usuarios en el SNC
 *
 * @cu CU-NP-CO-219
 * @author felipe.cadena
 */
@Component("administracionAsignacionUsuarios")
@Scope("session")
public class AdministracionAsignacionUsuariosMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 7576908157594023347L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionAsignacionUsuariosMB.class);

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Filtro para los datos de la consulta.
     */
    private FiltroDatosConsultaTramite datosConsultaTramite;

    /**
     * Objeto usado como contenedor de los datos de un solicitante para su consulta.
     */
    private FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private NumeroPredialPartes numeroPredialPartes;

    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Actividad seleccionada para la consulta
     */
    private String actividadSeleccionada;

    /**
     * Valor asociado al radio de seleccion de tipo de persona
     */
    private int tipoPersona;

    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean tipoPersonaN;

    /**
     * Bandera de seleccion de solicitante para la busqueda de solicitudes
     */
    private boolean banderaSolicitanteSolicitudSeleccionado;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites
     */
    private List<Tramite> tramitesResult;
    /**
     * variable donde se cargan los resultados paginados de la consulta de actividades
     */
    private List<Actividad> actividadesResult;

    /**
     * variable donde se cargan los tramites seleccionados para la asignacion
     */
    private List<Tramite> tramitesSeleccionadosAsignacion;

    /**
     * variable donde se cargan las actividades seleccionadas para la asignacion
     */
    private List<Actividad> actividadesSeleccionadosAsignacion;

    /*
     * Lista de tramites seleccionados
     */
    private Tramite[] tramitesSeleccionados;

    /*
     * Lista de actividades seleccionados
     */
    private Actividad[] actividadesSeleccionados;

    /*
     * Lista de tramites seleccionados para la eliminacion
     */
    private Tramite[] tramitesSeleccionadosEliminacion;

    /*
     * Lista de actividades seleccionados para la eliminacion
     */
    private Actividad[] actividadesSeleccionadosEliminacion;

    /**
     * Variable que almacena el login del funcionario ejecutor seleccionado.
     */
    private UsuarioDTO funcionarioSeleccionadoParaAsignacion;

    /**
     * Lista que contiene los funcionarios ejecutores que se encuentren asociados, a la territorial
     * del usuario autenticado
     */
    private List<SelectItem> ejecutoresSelectItem;

    /**
     * Variable item list con los valores del tipo de actividad
     */
    private List<SelectItem> tipoActividadItemList;

    /**
     * Variable item list con los valores de las UOC de la territorial
     */
    private List<SelectItem> UOCItemList;

    /**
     * Variable item list con los valores de los roles disponibles
     */
    private List<SelectItem> rolesItemList;
    /**
     * Variable item list con los valores de los roles de la UOC
     */
    private List<SelectItem> rolesUOCItemList;
    /**
     * Variable item list con los valores de los roles de la Territoial
     */
    private List<SelectItem> rolesTerritorialItemList;

    /**
     * Variable item list con los valores de los usuarios aociados al rol
     */
    private List<SelectItem> usuariosRolItemList;

    /**
     * UOC seleccionada
     */
    private String UOCseleccionada;

    /**
     * Rol seleccionado
     */
    private String rolSeleccionado;

    /**
     * Usuario seleccionado
     */
    private String usuarioSeleccionado;

    /**
     * Se almacenan los roles asociados a las OUCs
     */
    private Map<String, List<SelectItem>> rolesUOCs;
    /**
     * Se almacenan los usuarios asociados a las OUCs clasificados por rol
     */
    private Map<String, List<SelectItem>> usuariosRolUOCs;

    /**
     * Variable item list con los valores de los departamentos disponibles
     */
    private List<SelectItem> departamentosItemList;
    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosItemList;

    /**
     * Se almacenan los municipios clasificados por departamentos y OUC
     */
    private Map<String, List<SelectItem>> municipiosUOCs;

    /**
     * determina si esta habilitada la seleccion de uocs
     */
    private boolean banderaSeleccionUOC;

    /**
     * determina los tipos de solicitudes para seleccionar
     */
    private List<SelectItem> solitudesItemList;

    /**
     * determina los tipos de tramites para seleccionar
     */
    private List<SelectItem> tipoTramiteItemList;

    /**
     * determina los tipos de mutaciones para seleccionar
     */
    private List<SelectItem> claseMutacionItemList;

    /**
     * determina los tipos de mutaciones para seleccionar
     */
    private List<SelectItem> subtipoMutacionItemList;

    /**
     * Determina si la busqueda se realiza por tramite (true) o por actividad (false)
     */
    private boolean busquedaPorTramite;

    private Tramite[] selectTramites;

    /**
     * Actividad la cual se quirre el detalle de los tramites
     */
    private Actividad actividadSeleccionadaDetalle;

    /**
     * Lista para los tramites agrupados por activiad y usuario
     */
    private Map<String, List<Tramite>> tramitesActividad;

    /**
     * Lista para los tramites agrupados por activiad y usuario
     */
    private Map<String, List<Tramite>> tramitesActividadSeleccionados;

    /**
     * Lista para los usuarios clasificaos por territorial
     */
    private Map<String, List<UsuarioDTO>> usuariosDtoPorUOC;

    /**
     * variable donde se cargan los tramites seleccionados para la asignacion
     */
    private List<Tramite> tramitesPorActividad;

    /**
     * variable donde se cargan los tramites seleccionados para la asignacion luego de las
     * validaciones
     */
    private List<Tramite> tramitesDefinitivosReasignacion;

    /**
     * variable donde se almacena el motivo de la reasignacion
     */
    private String motivoReasignacion;

    /**
     * Lista de funcionarios para asignacion
     */
    private List<UsuarioDTO> funcionariosTramiteDTO;

    //---- variables para la carga de tramits por usuario ----//
    /**
     * Tramites de la tabla de Tramites asignado a un ejecutor
     */
    private List<Tramite> assignedTramitesAEjecutor;

    /**
     * Almacena el id del trámite con la actividad en la que se encuentra actualmente.
     */
    private Map<Long, String> tablaActividades;

    private Map<String, List<SelectItem>> datosTipoTramiteSolicitud;

    /**
     * charts
     */
    private StreamedContent chartTerreno;
    private StreamedContent chartOficina;

    // entero para el numero de secuencia de la grafica
    private int fileG = 0;

    private List<SEjecutoresTramite> ejecutoresTramite;
    private SEjecutoresTramite ejecutorSeleccionado;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String prediosParaZoom;
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;
    @Autowired
    private IContextListener contextoWeb;

    private boolean usuarioDelegado;
    private boolean usuarioHabilitado;
    private List<Municipio> municipiosDelegados;
    private List<Municipio> municipiosHabilitados;

    // --------------------GETTERS AND SETTERS--------------------//
    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public boolean isUsuarioHabilitado() {
        return usuarioHabilitado;
    }

    public void setUsuarioHabilitado(boolean usuarioHabilitado) {
        this.usuarioHabilitado = usuarioHabilitado;
    }
    
    

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public boolean getMostrarClaseMutacion() {
        return this.claseMutacionItemList != null;
    }

    public boolean getMostrarSubClaseMutacion() {
        return this.subtipoMutacionItemList != null;
    }

    public Actividad getActividadSeleccionadaDetalle() {
        return actividadSeleccionadaDetalle;
    }

    public List<SelectItem> getClaseMutacionItemList() {
        return claseMutacionItemList;
    }

    public void setClaseMutacionItemList(List<SelectItem> claseMutacionItemList) {
        this.claseMutacionItemList = claseMutacionItemList;
    }

    public List<SelectItem> getSubtipoMutacionItemList() {
        return subtipoMutacionItemList;
    }

    public void setSubtipoMutacionItemList(List<SelectItem> subtipoMutacionItemList) {
        this.subtipoMutacionItemList = subtipoMutacionItemList;
    }

    public void setActividadSeleccionadaDetalle(Actividad actividadSeleccionadaDetalle) {
        this.actividadSeleccionadaDetalle = actividadSeleccionadaDetalle;

        if (this.actividadSeleccionadaDetalle != null) {

            String key = this.actividadSeleccionadaDetalle.getNombre() + "_" +
                this.actividadSeleccionadaDetalle.getUsuarioEjecutor();
            this.tramitesPorActividad = this.tramitesActividad.get(key);

            if (this.tramitesPorActividad == null || this.tramitesPorActividad.isEmpty()) {
                this.tramitesPorActividad = this.tramitesActividadSeleccionados.get(key);
            }
        }
    }

    public List<Tramite> getAssignedTramitesAEjecutor() {
        return assignedTramitesAEjecutor;
    }

    public void setAssignedTramitesAEjecutor(List<Tramite> assignedTramitesAEjecutor) {
        this.assignedTramitesAEjecutor = assignedTramitesAEjecutor;
    }

    public StreamedContent getChartTerreno() {
        return chartTerreno;
    }

    public void setChartTerreno(StreamedContent chartTerreno) {
        this.chartTerreno = chartTerreno;
    }

    public StreamedContent getChartOficina() {
        return chartOficina;
    }

    public void setChartOficina(StreamedContent chartOficina) {
        this.chartOficina = chartOficina;
    }

    public List<SEjecutoresTramite> getEjecutoresTramite() {
        return ejecutoresTramite;
    }

    public void setEjecutoresTramite(List<SEjecutoresTramite> ejecutoresTramite) {
        this.ejecutoresTramite = ejecutoresTramite;
    }

    public SEjecutoresTramite getEjecutorSeleccionado() {
        return ejecutorSeleccionado;
    }

    public void setEjecutorSeleccionado(SEjecutoresTramite ejecutorSeleccionado) {
        this.ejecutorSeleccionado = ejecutorSeleccionado;
    }

    public Map<String, List<UsuarioDTO>> getUsuariosDtoPorUOC() {
        return usuariosDtoPorUOC;
    }

    public void setUsuariosDtoPorUOC(Map<String, List<UsuarioDTO>> usuariosDtoPorUOC) {
        this.usuariosDtoPorUOC = usuariosDtoPorUOC;
    }

    public List<UsuarioDTO> getFuncionariosTramiteDTO() {
        return funcionariosTramiteDTO;
    }

    public void setFuncionariosTramiteDTO(List<UsuarioDTO> funcionariosTramiteDTO) {
        this.funcionariosTramiteDTO = funcionariosTramiteDTO;
    }

    public String getMotivoReasignacion() {
        return motivoReasignacion;
    }

    public void setMotivoReasignacion(String motivoReasignacion) {
        this.motivoReasignacion = motivoReasignacion;
    }

    public List<Tramite> getTramitesPorActividad() {
        return tramitesPorActividad;
    }

    public void setTramitesPorActividad(List<Tramite> tramitesPorActividad) {
        this.tramitesPorActividad = tramitesPorActividad;
    }

    public boolean isBusquedaPorTramite() {
        return busquedaPorTramite;
    }

    public void setBusquedaPorTramite(boolean busquedaPorTramite) {
        this.busquedaPorTramite = busquedaPorTramite;
    }

    public Map<String, List<Tramite>> getTramitesActividad() {
        return tramitesActividad;
    }

    public void setTramitesActividad(Map<String, List<Tramite>> tramitesActividad) {
        this.tramitesActividad = tramitesActividad;
    }

    public Tramite[] getSelectTramites() {
        return selectTramites;
    }

    public void setSelectTramites(Tramite[] selectTramites) {
        this.selectTramites = selectTramites;
    }

    public FiltroDatosConsultaTramite getDatosConsultaTramite() {
        return datosConsultaTramite;
    }

    public void setDatosConsultaTramite(FiltroDatosConsultaTramite datosConsultaTramite) {
        this.datosConsultaTramite = datosConsultaTramite;
    }

    public FiltroDatosConsultaSolicitante getFiltroDatosConsultaSolicitante() {
        return filtroDatosConsultaSolicitante;
    }

    public void setFiltroDatosConsultaSolicitante(
        FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante) {
        this.filtroDatosConsultaSolicitante = filtroDatosConsultaSolicitante;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public NumeroPredialPartes getNumeroPredialPartes() {
        return numeroPredialPartes;
    }

    public void setNumeroPredialPartes(NumeroPredialPartes numeroPredialPartes) {
        this.numeroPredialPartes = numeroPredialPartes;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getActividadSeleccionada() {
        return actividadSeleccionada;
    }

    public void setActividadSeleccionada(String actividadSeleccionada) {
        this.actividadSeleccionada = actividadSeleccionada;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public boolean isTipoPersonaN() {
        return tipoPersonaN;
    }

    public void setTipoPersonaN(boolean tipoPersonaN) {
        this.tipoPersonaN = tipoPersonaN;
    }

    public boolean isBanderaSolicitanteSolicitudSeleccionado() {
        return banderaSolicitanteSolicitudSeleccionado;
    }

    public void setBanderaSolicitanteSolicitudSeleccionado(
        boolean banderaSolicitanteSolicitudSeleccionado) {
        this.banderaSolicitanteSolicitudSeleccionado = banderaSolicitanteSolicitudSeleccionado;
    }

    public List<Tramite> getTramitesResult() {
        return tramitesResult;
    }

    public void setTramitesResult(List<Tramite> tramitesResult) {
        this.tramitesResult = tramitesResult;
    }

    public Tramite[] getTramitesSeleccionados() {
        return tramitesSeleccionados;
    }

    public void setTramitesSeleccionados(Tramite[] tramitesSeleccionados) {
        this.tramitesSeleccionados = tramitesSeleccionados;
    }

    public UsuarioDTO getFuncionarioSeleccionadoParaAsignacion() {
        return funcionarioSeleccionadoParaAsignacion;
    }

    public void setFuncionarioSeleccionadoParaAsignacion(
        UsuarioDTO funcionarioSeleccionadoParaAsignacion) {
        this.funcionarioSeleccionadoParaAsignacion = funcionarioSeleccionadoParaAsignacion;
    }

    public List<SelectItem> getEjecutoresSelectItem() {
        return ejecutoresSelectItem;
    }

    public void setEjecutoresSelectItem(List<SelectItem> ejecutoresSelectItem) {
        this.ejecutoresSelectItem = ejecutoresSelectItem;
    }

    public TareasPendientesMB getTareasPendientesMB() {
        return tareasPendientesMB;
    }

    public void setTareasPendientesMB(TareasPendientesMB tareasPendientesMB) {
        this.tareasPendientesMB = tareasPendientesMB;
    }

    public List<SelectItem> getTipoActividadItemList() {
        return tipoActividadItemList;
    }

    public void setTipoActividadItemList(List<SelectItem> tipoActividadItemList) {
        this.tipoActividadItemList = tipoActividadItemList;
    }

    public List<SelectItem> getUOCItemList() {
        return UOCItemList;
    }

    public void setUOCItemList(List<SelectItem> UOCItemList) {
        this.UOCItemList = UOCItemList;
    }

    public List<SelectItem> getRolesItemList() {
        return this.ordenarItemList(rolesItemList);
    }

    public void setRolesItemList(List<SelectItem> rolesItemList) {
        this.rolesItemList = rolesItemList;
    }

    public List<SelectItem> getUsuariosRolItemList() {
        return usuariosRolItemList;
    }

    public void setUsuariosRolItemList(List<SelectItem> usuariosRolItemList) {
        this.usuariosRolItemList = usuariosRolItemList;
    }

    public String getUOCseleccionada() {
        return UOCseleccionada;
    }

    public void setUOCseleccionada(String UOCseleccionada) {
        this.UOCseleccionada = UOCseleccionada;
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public String getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(String usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public Map<String, List<SelectItem>> getRolesUOCs() {
        return rolesUOCs;
    }

    public void setRolesUOCs(Map<String, List<SelectItem>> rolesUOCs) {
        this.rolesUOCs = rolesUOCs;
    }

    public Map<String, List<SelectItem>> getUsuaiosRolUOCs() {
        return usuariosRolUOCs;
    }

    public void setUsuaiosRolUOCs(Map<String, List<SelectItem>> usuaiosRolUOCs) {
        this.usuariosRolUOCs = usuaiosRolUOCs;
    }

    public boolean isBanderaSeleccionUOC() {
        return banderaSeleccionUOC;
    }

    public void setBanderaSeleccionUOC(boolean banderaSeleccionUOC) {
        this.banderaSeleccionUOC = banderaSeleccionUOC;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public List<SelectItem> getSolitudesItemList() {
        return solitudesItemList;
    }

    public void setSolitudesItemList(List<SelectItem> solitudesItemList) {
        this.solitudesItemList = solitudesItemList;
    }

    public List<SelectItem> getTipoTramiteItemList() {
        return tipoTramiteItemList;
    }

    public void setTipoTramiteItemList(List<SelectItem> tipoTramiteItemList) {
        this.tipoTramiteItemList = tipoTramiteItemList;
    }

    public List<Tramite> getTramitesSeleccionadosAsignacion() {
        return tramitesSeleccionadosAsignacion;
    }

    public void setTramitesSeleccionadosAsignacion(List<Tramite> tramitesSeleccionadosAsignacion) {
        this.tramitesSeleccionadosAsignacion = tramitesSeleccionadosAsignacion;
    }

    public Tramite[] getTramitesSeleccionadosEliminacion() {
        return tramitesSeleccionadosEliminacion;
    }

    public void setTramitesSeleccionadosEliminacion(Tramite[] tramitesSeleccionadosEliminacion) {
        this.tramitesSeleccionadosEliminacion = tramitesSeleccionadosEliminacion;
    }

    public List<Actividad> getActividadesResult() {
        return actividadesResult;
    }

    public void setActividadesResult(List<Actividad> actividadesResult) {
        this.actividadesResult = actividadesResult;
    }

    public List<Actividad> getActividadesSeleccionadosAsignacion() {
        return actividadesSeleccionadosAsignacion;
    }

    public void setActividadesSeleccionadosAsignacion(
        List<Actividad> actividadesSeleccionadosAsignacion) {
        this.actividadesSeleccionadosAsignacion = actividadesSeleccionadosAsignacion;
    }

    public Actividad[] getActividadesSeleccionados() {
        return actividadesSeleccionados;
    }

    public void setActividadesSeleccionados(Actividad[] actividadesSeleccionados) {
        this.actividadesSeleccionados = actividadesSeleccionados;
    }

    public Actividad[] getActividadesSeleccionadosEliminacion() {
        return actividadesSeleccionadosEliminacion;
    }

    public void setActividadesSeleccionadosEliminacion(
        Actividad[] actividadesSeleccionadosEliminacion) {
        this.actividadesSeleccionadosEliminacion = actividadesSeleccionadosEliminacion;
    }

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("deprecation")
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del AdministracionAsignacionUsuariosMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.usuarioHabilitado = MenuMB.getMenu().isUsuarioHabilitado();
        this.municipiosDelegados = this.getGeneralesService().obtenerMunicipiosDelegados();
        this.municipiosHabilitados=this.getGeneralesService().obtenerMunicipiosHabilitados();
        this.datosConsultaTramite = new FiltroDatosConsultaTramite();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        if (usuario.getDescripcionUOC() != null) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionUOC(),
                ERol.RESPONSABLE_SIG);;
        }

        //si no existe Responsable SIG en la UOC se buscan en la territorial.
        if (usuarios.isEmpty()) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_SIG);
        }

        //Se cargan las UOCs asociadas a la territorial del usuario.
        this.cargarUOCs();

        //Se cargan los tipos de solicitud
        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
        //this.solitudesItemList = generalMB.getSolicitudTipoV();

        this.tramitesSeleccionadosAsignacion = new ArrayList<Tramite>();
        this.actividadesSeleccionadosAsignacion = new ArrayList<Actividad>();
        this.tramitesResult = new ArrayList<Tramite>();
        this.actividadesResult = new ArrayList<Actividad>();
        this.tramitesActividadSeleccionados = new HashMap<String, List<Tramite>>();

        this.datosTipoTramiteSolicitud = generalMB.obtenerTipoSolicitudesYTramitesSI();
        this.solitudesItemList = this.datosTipoTramiteSolicitud.get(EDominio.SOLICITUD_TIPO.
            toString());

        //Se inicializan las variables para el visor geografico
        this.inicializarVariablesVisorGIS();

    }

    /**
     * Metodo para buscar los tramites para reasignar
     *
     *
     * @author felipe.cadena
     */
    public void buscar() {

        //Se borran los resultados de consultas anteriores
        this.tramitesResult = new ArrayList<Tramite>();
        this.actividadesResult = new ArrayList<Actividad>();
        if (this.busquedaPorTramite) {
            //Busqueda por tramite en SQL
            this.inicializarNumeroPredial();
            this.datosConsultaTramite.setNumeroPredialPartes(this.numeroPredialPartes);

            //Validaciones filtro tramites
            if (this.datosConsultaTramite.getFechaRadicacion() != null && this.datosConsultaTramite.
                getFechaRadicacionFinal() != null) {
                if (!this.datosConsultaTramite.getFechaRadicacion().before(
                    this.datosConsultaTramite.getFechaRadicacionFinal())) {
                    this.addMensajeError(
                        "La Fecha radicación inicial debe ser anterior a la fecha Fecha radicación final");
                    return;
                }
            }

            String nPredial = UtilidadesWeb.obtenerNumeroPredial(datosConsultaTramite.
                getNumeroPredialPartes());

            //felipe.cadena::07-12-2016::Se validan las consultas para los usuarios delegados
            if (this.usuarioDelegado || this.usuarioHabilitado) {
                //Se selecciona territorial o delegada por defecto
                this.datosConsultaTramite.setTerritorialId(this.usuario.getCodigoTerritorial());

                String codigoMun = (this.datosConsultaPredio.getNumeroPredialS1() +
                    datosConsultaPredio.getNumeroPredialS2());

                if (codigoMun != null && !codigoMun.isEmpty()) {
                    List<Municipio> municipiosList;

                    boolean predioExiste = false;
                    municipiosList = this.getGeneralesService().buscarMunicipioPorIdOficina(
                        this.usuario.getCodigoTerritorial());

                    for (Municipio mun : municipiosList) {
                        if (mun.getCodigo().equals(this.datosConsultaPredio.getNumeroPredialS1() +
                            datosConsultaPredio.getNumeroPredialS2())) {
                            predioExiste = true;
                            break;
                        }
                    }

                    if (!predioExiste) {
                        this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_001.toString());
                        LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_001.getMensajeTecnico());
                        return;
                    }
                }
            } else {
                for (Municipio mun : this.municipiosDelegados) {
                    if (mun.getCodigo().equals(this.datosConsultaPredio.getNumeroPredialS1() +
                        datosConsultaPredio.getNumeroPredialS2())) {
                        this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_012.toString());
                        LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_012.getMensajeTecnico());
                        return;
                    }
                }
                for (Municipio mun : this.municipiosHabilitados) {
                    if (mun.getCodigo().equals(this.datosConsultaPredio.getNumeroPredialS1() +
                        datosConsultaPredio.getNumeroPredialS2())) {
                        this.addMensajeError(ECodigoErrorVista.CONSULTA_HABILITACION_012.toString());
                        LOGGER.error(ECodigoErrorVista.CONSULTA_HABILITACION_012.getMensajeTecnico());
                        return;
                    }
                }
            }

            if (nPredial == null) {
                this.addMensajeError("EL número predial fue ingresado de manera incorrecta");
                return;
            }
            if (!nPredial.isEmpty()) {

                if (nPredial.length() < 17) {
                    this.addMensajeError(
                        "EL número predial debe ser ingresado por lo menos hasta manzana  o vereda");
                    return;
                }

            }

            if (this.datosConsultaTramite.getDepartamentoId() == null ||
                 "".equals(this.datosConsultaTramite.getDepartamentoId())) {
                this.datosConsultaTramite.setDepartamentoId(this.departamentosItemList.get(0).
                    getValue().toString());
            }

            Object[] result = this.getTramiteService().
                buscarTramitesParaAdminAsignaciones(this.datosConsultaTramite,
                    this.filtroDatosConsultaSolicitante);
            if (result[0] != null) {
                this.tramitesResult = new ArrayList<Tramite>();
                this.addMensajeError(result[0].toString());
            } else if (((List) result[1]).isEmpty()) {
                this.tramitesResult = new ArrayList<Tramite>();
                this.addMensajeInfo("No se encontraron resultados para la consulta");
            } else {

                //felipe.cadena::11-01-2017::extraer tramites delegados
                if (!this.usuarioDelegado) {
                    List<Tramite> tramitesAux = (List<Tramite>) result[1];
                    this.tramitesResult = new ArrayList<Tramite>();
                    for (Tramite tramite : tramitesAux) {
                        boolean delegado = false;
                        for (Municipio mun : this.municipiosDelegados) {
                            if (tramite.getMunicipio().getCodigo().equals(mun.getCodigo())) {
                                delegado = true;
                                break;
                            }
                        }
                        if (!delegado) {
                            this.tramitesResult.add(tramite);
                        }
                    }
                } else if(!this.usuarioHabilitado){
                    List<Tramite> tramitesAux = (List<Tramite>) result[1];
                    this.tramitesResult = new ArrayList<Tramite>();
                    for (Tramite tramite : tramitesAux) {
                        boolean habilitado = false;
                        for (Municipio mun : this.municipiosHabilitados) {
                            if (tramite.getMunicipio().getCodigo().equals(mun.getCodigo())) {
                                habilitado = true;
                                break;
                            }
                        }
                        if (!habilitado) {
                            this.tramitesResult.add(tramite);
                        }
                    }
                }else{
                    this.tramitesResult = (List<Tramite>) result[1];
                }
                //se agrega la uocSeleccionada al tramite
                String codOficina = this.UOCseleccionada;
                if (codOficina == null) {
                    codOficina = this.usuario.getCodigoTerritorial();
                }
                for (Tramite t : this.tramitesResult) {
                    t.setOficinaCodigo(codOficina);
                }
            }
        } else {
            //Busqueda por actividad en process
            String codigoOficina;
            if (this.UOCseleccionada != null && !this.UOCseleccionada.isEmpty()) {
                codigoOficina = this.UOCseleccionada;
            } else {
                codigoOficina = this.usuario.getCodigoTerritorial();
            }

            List<String> usuarios = new ArrayList<String>();

            if (this.usuarioSeleccionado != null && !this.usuarioSeleccionado.isEmpty()) {
                usuarios.add(this.usuarioSeleccionado);
            } else {
                this.usuariosRolItemList = this.usuariosRolUOCs.get(this.UOCseleccionada +
                    this.rolSeleccionado);
                for (SelectItem si : this.usuariosRolItemList) {
                    usuarios.add(si.getValue().toString());
                }
            }
            Object[] result = this.getTramiteService().buscarActividadesParaAdminAsignaciones(
                codigoOficina, usuarios);
            if (result[0] != null) {
                this.tramitesResult = new ArrayList<Tramite>();
                this.addMensajeError(result[0].toString());
            } else if (((List) result[1]).isEmpty()) {
                this.tramitesResult = new ArrayList<Tramite>();
                this.addMensajeInfo("No se encontraron resultados para la consulta");
            } else {
                this.tramitesResult = (List<Tramite>) result[1];
                //se agrega la uocSeleccionada al tramite
                String codOficina = this.UOCseleccionada;
                if (codOficina == null || codOficina.isEmpty()) {
                    codOficina = this.usuario.getCodigoTerritorial();
                }
                for (Tramite t : this.tramitesResult) {
                    t.setOficinaCodigo(codOficina);
                }
                this.agruparTramitesPorActividadUsuario();
            }

        }

    }

    /**
     * Prepara el numero predial del predio para la consulta basado en los parametros del usuario.
     *
     * @author felipe.cadena
     */
    public void inicializarNumeroPredial() {

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartes.
            setDepartamentoCodigo(this.datosConsultaPredio.getNumeroPredialS1());
        this.numeroPredialPartes.setMunicipioCodigo(this.datosConsultaPredio.getNumeroPredialS2());
        this.numeroPredialPartes.setTipoAvaluo(this.datosConsultaPredio.getNumeroPredialS3());
        this.numeroPredialPartes.setSectorCodigo(this.datosConsultaPredio.getNumeroPredialS4());
        this.numeroPredialPartes.setComunaCodigo(this.datosConsultaPredio.getNumeroPredialS5());
        this.numeroPredialPartes.setBarrioCodigo(this.datosConsultaPredio.getNumeroPredialS6());
        this.numeroPredialPartes.setManzanaVeredaCodigo(this.datosConsultaPredio.
            getNumeroPredialS7());
        this.numeroPredialPartes.setPredio(this.datosConsultaPredio.getNumeroPredialS8());
        this.numeroPredialPartes.
            setCondicionPropiedad(this.datosConsultaPredio.getNumeroPredialS9());
        this.numeroPredialPartes.setEdificio(this.datosConsultaPredio.getNumeroPredialS10());
        this.numeroPredialPartes.setPiso(this.datosConsultaPredio.getNumeroPredialS11());
        this.numeroPredialPartes.setUnidad(this.datosConsultaPredio.getNumeroPredialS12());

    }

    /**
     * Metodo para inicializar las UOC relacionadas a la territorial
     *
     * @author felipe.cadena
     */
    public void cargarUOCs() {

        String codigoTerritorial;

        this.rolesUOCs = new HashMap<String, List<SelectItem>>();

        this.usuariosRolUOCs = new HashMap<String, List<SelectItem>>();
        this.usuariosDtoPorUOC = new HashMap<String, List<UsuarioDTO>>();

        codigoTerritorial = this.usuario.getCodigoTerritorial();

        List<EstructuraOrganizacional> oucs = this.getGeneralesService().
            buscarUOCporCodigoTerritorial(codigoTerritorial);

        this.UOCItemList = new ArrayList<SelectItem>();
        for (EstructuraOrganizacional eo : oucs) {
            //Inicio lista de seleccion
            this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            //Inicio roles
            this.cargarRoles(eo);
        }
        //cargar roles para la territorial
        this.cargarRoles(new EstructuraOrganizacional(codigoTerritorial, this.usuario.
            getDescripcionTerritorial(), null, null, null));

        this.banderaSeleccionUOC = !UOCItemList.isEmpty();
        this.UOCseleccionada = this.usuario.getCodigoTerritorial();
        this.actualizarRolesListener();
        this.cargarDepartamentos();

    }

    /**
     * Metodo para inicializar los roles asociados a las UOC
     *
     * @author felipe.cadena
     * @param uoc
     */
    public void cargarRoles(EstructuraOrganizacional uoc) {

        List<SelectItem> rolesUoc = new ArrayList<SelectItem>();
        List<SelectItem> usuarioUocRol = new ArrayList<SelectItem>();
        this.rolesItemList = new ArrayList<SelectItem>();
        this.rolesUOCItemList = new ArrayList<SelectItem>();
        this.rolesTerritorialItemList = new ArrayList<SelectItem>();
        List<String> roles = new ArrayList<String>();
        List<UsuarioDTO> usuarios;
        if (this.usuario.getDescripcionTerritorial().equals(uoc.getNombre())) {
            usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), null);
        } else {
            usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), uoc.getNombre().replace(' ', '_'));
        }

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para " + uoc.getNombre());
            return;
        }

        //Se clasifican para la posterior asignacion
        this.usuariosDtoPorUOC.put(uoc.getCodigo(), usuarios);

        for (UsuarioDTO user : usuarios) {
            for (String rol : user.getRoles()) {
                if (rol.equals(ERol.EJECUTOR_TRAMITE.toString())) {
                    continue;
                }

                if (!roles.contains(rol)) {
                    roles.add(rol);
                }
                String userKey = uoc.getCodigo() + rol;
                if (!this.usuariosRolUOCs.containsKey(userKey)) {
                    usuarioUocRol = new LinkedList<SelectItem>();
                    usuarioUocRol.add(new SelectItem(user.getLogin(), user.getLogin()));
                    this.usuariosRolUOCs.put(userKey, usuarioUocRol);
                } else {
                    this.usuariosRolUOCs.get(userKey).add(new SelectItem(user.getLogin(), user.
                        getLogin()));
                }
            }
        }

        for (String rol : roles) {
            rolesUoc.add(new SelectItem(rol, rol));
        }
        this.rolesUOCs.put(uoc.getCodigo(), rolesUoc);

    }

    /**
     * Metodo para actualizar los roles asociados la oficina seleccionada
     *
     * @author felipe.cadena
     */
    public void actualizarRolesListener() {
        List<Departamento> deptosList;

        if (this.UOCseleccionada == null || this.UOCseleccionada.isEmpty()) {
            this.UOCseleccionada = this.usuario.getCodigoTerritorial();
        }

        if (this.busquedaPorTramite) {
            deptosList = this.getGeneralesService().getCacheDepartamentosByCodigoUOC(
                this.UOCseleccionada);
            this.cargarDepartamentosItemList(deptosList);
            if (this.municipiosUOCs != null) {
                if (this.UOCseleccionada != null && !this.UOCseleccionada.isEmpty()) {
                    this.municipiosItemList = this.municipiosUOCs.get(this.UOCseleccionada);
                } else {
                    this.municipiosItemList = this.municipiosUOCs.get(this.usuario.
                        getCodigoTerritorial());
                }
            }
        } else {
            this.rolesItemList = this.rolesUOCs.get(this.UOCseleccionada);
            this.usuariosRolItemList = new ArrayList<SelectItem>();
        }

    }

    /**
     * Metodo para actualizar los usuarios asociados al rol seleccionado
     *
     * @author felipe.cadena
     */
    public void actualizarUsuariosListener() {

        if (this.UOCItemList.isEmpty()) {
            this.UOCseleccionada = this.usuario.getCodigoTerritorial();
        }

        if (UOCseleccionada == null || UOCseleccionada.isEmpty()) {
            this.UOCseleccionada = this.usuario.getCodigoTerritorial();
        }

        if (this.rolSeleccionado == null) {
            this.usuariosRolItemList = new ArrayList<SelectItem>();

            Set<String> keys = this.usuariosRolUOCs.keySet();
            for (String key : keys) {
                if (key.startsWith(this.UOCseleccionada)) {
                    this.usuariosRolItemList.addAll(this.usuariosRolUOCs.get(key));
                }
            }
        } else {
            this.usuariosRolItemList = this.usuariosRolUOCs.get(this.UOCseleccionada +
                this.rolSeleccionado);
        }
    }

    /**
     * Metodo para cargar los departamentos asociados a la territorial.
     *
     * @author felipe.cadena
     */
    public void cargarDepartamentos() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList = new LinkedList<Municipio>();
        this.municipiosUOCs = new HashMap<String, List<SelectItem>>();
        boolean munDelegado = false;
        List<SelectItem> municipiosItemListTemp;

        deptosList = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
            this.usuario.getCodigoTerritorial());

        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {

            if (this.usuarioDelegado) {
                for (Municipio mun : this.municipiosDelegados) {
                    if (mun.getDepartamento().getCodigo().equals(dpto.getCodigo())) {
                        municipiosList.add(mun);
                    }
                }

//                this.selectedMunicipioCod = this.municipios.get(0).getCodigo();
//                this.selectedMunicipio = this.municipios.get(0);
            } if (this.usuarioHabilitado) {
                for (Municipio mun : this.municipiosHabilitados) {
                    if (mun.getDepartamento().getCodigo().equals(dpto.getCodigo())) {
                        municipiosList.add(mun);
                    }
                }
            } else {
                List<Municipio> municipiosTotal = (ArrayList<Municipio>) this.getGeneralesService()
                    .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(dpto.getCodigo());
                for (Municipio municipio : municipiosTotal) {
                    for (Municipio munDel : this.municipiosDelegados) {
                        if (munDel.getCodigo().equals(municipio.getCodigo())) {
                            munDelegado = true;
                            break;
                        }
                        if (!munDelegado) {
                            municipiosList.add(municipio);
                        }
                    }

                }

            }
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getNombre()));
            }

            this.municipiosUOCs.put(dpto.getCodigo(), municipiosItemListTemp);

        }

        for (SelectItem selectItem : this.UOCItemList) {
            municipiosList = this.getGeneralesService().
                buscarMunicipioPorIdOficina(selectItem.getValue().toString());

            municipiosItemListTemp = new ArrayList<SelectItem>();
            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getNombre()));
            }
            this.municipiosUOCs.put(selectItem.getValue().toString(), municipiosItemListTemp);
        }

    }

    /**
     * Método para recuperar el tipo de solicitud de acuerdo al rol del usuario
     *
     * @return
     * @author javier.aponte
     */
    public List<SelectItem> cargarSolicitudTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<SelectItem> listaAux;

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

        listaAux = generalMB.getSolicitudTipoV();

        boolean isResponsableAtencionUsuario = false;
        boolean isFuncionarioRadicador = false;

        for (String rol : this.usuario.getRoles()) {
            if (ERol.RESPONSABLE_ATENCION_USUARIO.getRol().equalsIgnoreCase(rol)) {
                isResponsableAtencionUsuario = true;
            }
        }

        for (String rol : this.usuario.getRoles()) {
            if (ERol.FUNCIONARIO_RADICADOR.getRol().equalsIgnoreCase(rol)) {
                isFuncionarioRadicador = true;
            }
        }

        if (listaAux != null && !listaAux.isEmpty()) {
            if (isFuncionarioRadicador) {
                for (SelectItem l : listaAux) {
                    if (!ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo().equals(l.getValue())) {
                        lista.add(l);
                    }
                }
            }
            if (isResponsableAtencionUsuario) {
                for (SelectItem l : listaAux) {
                    if (ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo().equals(l.getValue())) {
                        lista.add(l);
                    }
                }
            }
        }

        return lista;
    }

    /**
     * Metodo de inicializacion para la busqueda de actividades
     *
     * @author felipe.cadena
     */
    public void initBusquedaActividad() {
        this.busquedaPorTramite = false;
        this.UOCseleccionada = null;
        this.rolSeleccionado = null;
        this.usuarioSeleccionado = null;
        this.actividadesResult = new ArrayList<Actividad>();
        this.actividadesSeleccionados = null;

    }

    /**
     * Metodo de inicializacion para la busqueda de tramites
     *
     * @author felipe.cadena
     */
    public void initBusquedaTramite() {
        this.busquedaPorTramite = true;
        this.UOCseleccionada = null;
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.numeroPredialPartes = new NumeroPredialPartes();
        this.datosConsultaPredio.setNumeroPredialS1("");
        this.datosConsultaPredio.setNumeroPredialS2("");
        this.datosConsultaPredio.setNumeroPredialS3("");
        this.datosConsultaPredio.setNumeroPredialS4("");
        this.datosConsultaPredio.setNumeroPredialS5("");
        this.datosConsultaPredio.setNumeroPredialS6("");
        this.datosConsultaPredio.setNumeroPredialS7("");
        this.datosConsultaPredio.setNumeroPredialS8("");
        this.datosConsultaPredio.setNumeroPredialS9("");
        this.datosConsultaPredio.setNumeroPredialS10("");
        this.datosConsultaPredio.setNumeroPredialS11("");
        this.datosConsultaPredio.setNumeroPredialS12("");
        this.datosConsultaTramite = new FiltroDatosConsultaTramite();
        this.tramitesResult = new ArrayList<Tramite>();
        this.subtipoMutacionItemList = null;
        this.claseMutacionItemList = null;
        this.tipoTramiteItemList = null;

        //this.tramitesSeleccionados = null;
    }

    /**
     * Metodo para constuir la lista de departamentos asociados a la territorial
     *
     * @author felipe.cadena
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                departamento.getNombre()));
        }
    }

    /**
     * Metodo para actualizar los usuarios asociados al rol seleccionado
     *
     * @author felipe.cadena
     */
    public void actualizarMunicipiosListener() {

        if (this.municipiosUOCs != null) {
            if (this.UOCseleccionada != null && !this.UOCseleccionada.isEmpty()) {
                this.municipiosItemList = this.municipiosUOCs.get(this.UOCseleccionada);
            } else {
                this.municipiosItemList = this.municipiosUOCs.get(this.datosConsultaTramite.
                    getDepartamentoId());
            }

        }

    }

    /**
     * Actualiza los tipos de tramite de acuerdo con la solicitub seleccionadad
     *
     * @author felipe.cadena
     */
    public void actualizarTiposTramitePorSolicitudListener() {
        if (!("".equals(this.datosConsultaTramite.getTipoSolicitud()))) {
            this.tipoTramiteItemList = this.datosTipoTramiteSolicitud.
                get(EDominio.SOLICITUD_TIPO.toString() + this.datosConsultaTramite.
                    getTipoSolicitud());
        } else {
            this.tipoTramiteItemList = new ArrayList<SelectItem>();
        }
        this.subtipoMutacionItemList = null;
        this.claseMutacionItemList = null;
    }

    /**
     * Actualiza los tipos de tramite de acuerdo con la solicitub seleccionadad
     *
     * @author felipe.cadena
     */
    public void actualizarSubtramitesPorTramiteListener() {
        this.claseMutacionItemList = this.datosTipoTramiteSolicitud.
            get(EDominio.MUTACION_CLASE.toString() + this.datosConsultaTramite.getTipoTramite());
        this.subtipoMutacionItemList = null;
    }

    /**
     * Actualiza los tipos de tramite de acuerdo con la solicitub seleccionadad
     *
     * @author felipe.cadena
     */
    public void actualizarTiposmutacionListener() {
        this.subtipoMutacionItemList = this.datosTipoTramiteSolicitud.
            get(EDominio.MUTACION_SUBTIPO.toString() + this.datosConsultaTramite.getClaseMutacion());

    }

    /**
     * Agrega los tramites o las actividades seleccionadas a las listas disponibles para la
     * asignacion
     *
     * @author felipe.cadena
     */
    public void agregarSeleccion() {

        String tramitesNoAgregados = "";
        if (this.busquedaPorTramite) {
            // agrega los tramites
            for (Tramite t : this.tramitesSeleccionados) {
                boolean contain = false;
                for (List<Tramite> ts : this.tramitesActividadSeleccionados.values()) {
                    for (Tramite tramite : ts) {
                        if (tramite.getId().equals(t.getId())) {
                            contain = true;
                            break;
                        }
                    }
                }

                if (!contain) {
                    if (!this.tramitesSeleccionadosAsignacion.contains(t)) {
                        this.tramitesSeleccionadosAsignacion.add(t);
                    }
                } else {
                    tramitesNoAgregados += t.getNumeroRadicacion() + ", ";
                }

            }
            this.tramitesSeleccionados = null;
        } else {
            // agrega las actividades
            for (Actividad a : this.actividadesSeleccionados) {
                if (!this.contieneActividad(a, actividadesSeleccionadosAsignacion)) {

                    String key = a.getNombre() + "_" + a.getUsuarioEjecutor();
                    List<Tramite> tramitesAct = new ArrayList<Tramite>();
                    if (this.tramitesSeleccionadosAsignacion != null) {
                        for (Tramite t : this.tramitesActividad.get(key)) {
                            boolean contain = false;
                            for (Tramite tramite : this.tramitesSeleccionadosAsignacion) {
                                if (tramite.getId().equals(t.getId())) {
                                    contain = true;
                                    break;
                                }

                            }
                            if (!contain) {
                                tramitesAct.add(t);
                            } else {
                                tramitesNoAgregados += t.getNumeroRadicacion() + ", ";
                            }
                        }
                    }
                    if (!tramitesAct.isEmpty()) {
                        this.actividadesSeleccionadosAsignacion.add(a);
                        this.tramitesActividadSeleccionados.put(key, tramitesAct);
                    }

                }
            }

        }

        if (!tramitesNoAgregados.isEmpty()) {
            this.addMensajeWarn("El(Los) trámite(s) " + tramitesNoAgregados +
                " no se agregaron por que ya se encuentran en la lista de trámites para asignar ");
        }
    }

    /**
     * Determina si la actividad dada esta contenida en la lista, basandose en el nombre de la
     * actividad, el rol del usuario y el nombre de usuario
     *
     * @param a
     * @param lista
     * @return
     */
    private boolean contieneActividad(Actividad a, List<Actividad> lista) {

        for (Actividad actividad : lista) {
            if (actividad.getNombre().equals(a.getNombre()) &&
                 actividad.getUsuarioEjecutor().equals(a.getUsuarioEjecutor()) &&
                 actividad.getRoles().get(0).equals(a.getRoles().get(0))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo para eliminar los tramites de la lista de asignaciones
     *
     * @author felipe.cadena
     */
    public void eliminarTramites() {

        for (Tramite t : this.tramitesSeleccionadosEliminacion) {
            this.tramitesSeleccionadosAsignacion.remove(t);

        }
    }

    /**
     * Metodo para eliminar las actividades de la lista de asignaciones
     *
     * @author felipe.cadena
     */
    public void eliminarActividades() {
        for (Actividad a : this.actividadesSeleccionadosEliminacion) {
            this.actividadesSeleccionadosAsignacion.remove(a);
            String key = a.getNombre() + "_" + a.getUsuarioEjecutor();
            this.tramitesActividadSeleccionados.remove(key);
        }
    }

    /**
     * Metodo para agrupar los tramites por actividad y usuario
     *
     * @author felipe.cadena
     */
    public void agruparTramitesPorActividadUsuario() {

        this.tramitesActividad = new HashMap<String, List<Tramite>>();
        Actividad act;
        this.actividadesResult = new ArrayList<Actividad>();

        List<Tramite> tramitesTemp = new ArrayList<Tramite>();

        if (this.tramitesResult != null && !this.tramitesResult.isEmpty()) {

            for (int i = 0; i < this.tramitesResult.size(); i++) {
                Tramite t = this.tramitesResult.get(i);
                act = t.getActividadActualTramite();
                String key = act.getNombre() + "_" + act.getUsuarioEjecutor();

                if (this.tramitesActividad.containsKey(key)) {
                    this.tramitesActividad.get(key).add(t);

                } else {
                    this.actividadesResult.add(act);
                    tramitesTemp = new ArrayList<Tramite>();
                    tramitesTemp.add(t);
                    this.tramitesActividad.put(key, tramitesTemp);
                }

            }

            //inicilizar cantidad de tramite por actividad
            for (Actividad a : this.actividadesResult) {
                int nTramites = this.tramitesActividad.get(a.getNombre() + "_" + a.
                    getUsuarioEjecutor()).size();
                a.setObservacion(String.valueOf(nTramites));
            }

        }

    }

    /**
     * MEtodo para determinar si una lista de tramites tiene el mismo Rol de actividad asignado
     *
     * @author felipe.cadena
     * @param tramites
     * @return
     */
    private boolean validarMismoRolTramites(List<Tramite> tramites) {

        boolean resultado = true;

        if (tramites == null || tramites.isEmpty()) {
            return false;
        } else {
            String rolBase = tramites.get(0).getActividadActualTramite().getRoles().get(0).trim();
            for (Tramite tramite : tramites) {

                if (!rolBase.equals(tramite.getActividadActualTramite().getRoles().get(0).trim())) {
                    resultado = false;
                    break;
                }
            }
        }

        return resultado;
    }

    /**
     * MEtodo para determinar si una lista de tramites tiene el mismo Rol de actividad asignado
     *
     * @author felipe.cadena
     * @param tramites
     * @return
     */
    private boolean validarMismaUocTramites(List<Tramite> tramites) {

        boolean resultado = true;

        if (tramites == null || tramites.isEmpty()) {
            return false;
        } else {
            String rolBase = tramites.get(0).getOficinaCodigo();
            for (Tramite tramite : tramites) {

                if (!rolBase.equals(tramite.getOficinaCodigo())) {
                    resultado = false;
                    break;
                }
            }
        }

        return resultado;
    }

    /**
     * Metodo que se ejecuta al activar el boton reasignar, y valida y prepara los datos para la
     * reasignacion
     *
     * @author felipe.cadena
     */
    public void reasignarTramites() {

        List<Tramite> tramitesParaAsignacion = new ArrayList<Tramite>();
        this.funcionariosTramiteDTO = new ArrayList<UsuarioDTO>();

        //Se agregan los tramites de la seccion tramites
        if (this.tramitesSeleccionadosEliminacion != null &&
            this.tramitesSeleccionadosEliminacion.length > 0) {
            tramitesParaAsignacion.addAll(Arrays.asList(this.tramitesSeleccionadosEliminacion));
        }

        //Se agregan los tramites de la seccion actividades
        if (this.actividadesSeleccionadosEliminacion != null &&
            this.actividadesSeleccionadosEliminacion.length > 0) {
            for (Actividad act : this.actividadesSeleccionadosEliminacion) {
                tramitesParaAsignacion.addAll(this.tramitesActividadSeleccionados.get(act.
                    getNombre() + "_" + act.getUsuarioEjecutor()));
            }
        }

        //--VALIDACIONES--//
        RequestContext context = RequestContext.getCurrentInstance();
        //valida motivo
        if (this.motivoReasignacion == null || this.motivoReasignacion.isEmpty()) {
            this.addMensajeError("Se debe ingresar un motivo para la reasignación");
            context.addCallbackParam("error", "error");
            return;
        }
        //valida seleccion
        if (tramitesParaAsignacion.isEmpty()) {
            this.addMensajeError(
                "Se debe seleccionar al menos una actividad o un trámite para la reasignación");
            context.addCallbackParam("error", "error");
            return;
        }
        //valida roles
        if (!this.validarMismoRolTramites(tramitesParaAsignacion)) {
            this.addMensajeError(
                "Las actividades y trámites seleccionados están asignados a usuarios con roles diferentes entre sí. Asegúrese de que las actividades y trámites estén asignadas a usuarios con el mismo rol");
            context.addCallbackParam("error", "error");
            return;
        }
        //valida oficina
        if (!this.validarMismaUocTramites(tramitesParaAsignacion)) {
            this.addMensajeError(
                "Las actividades y trámites seleccionados no pertenecen a la misma Unidad Operativa Catastral (UOC) ni a la misma territorial");
            context.addCallbackParam("error", "error");
            return;
        }

        this.tramitesDefinitivosReasignacion = tramitesParaAsignacion;

        //---------------------//
        //se cargan los usuarios disponibles para la asignacion
        /*
         * @modified by leidy.gonzalez:: 02/06/2016::#17668 Se realiza precarga de codigo de la
         * oficina del tramite para asignacion.
         */
        String codOficina = this.UOCseleccionada;
        if (codOficina == null || codOficina.isEmpty()) {
            codOficina = this.usuario.getCodigoTerritorial();
            tramitesParaAsignacion.get(0).setOficinaCodigo(codOficina);
        }

        String uoc = tramitesParaAsignacion.get(0).getOficinaCodigo();

        this.usuariosRolItemList = this.usuariosRolUOCs.get(uoc + tramitesParaAsignacion.get(0).
            getActividadActualTramite().getRoles().get(0).toUpperCase().trim());

        List<UsuarioDTO> usuariosUoc = this.usuariosDtoPorUOC.get(uoc);
        if (this.usuariosRolItemList != null && !this.usuariosRolItemList.isEmpty() &&
             usuariosUoc != null && !usuariosUoc.isEmpty()) {
            for (SelectItem user : this.usuariosRolItemList) {
                for (UsuarioDTO usuarioDTO : usuariosUoc) {
                    if (usuarioDTO.getLogin().equals(user.getValue())) {
                        //felipe.cadena::#15864::01-03-2016::Se valida que no esten disponibles
                        //para asignacion los usuarios expirados o inactivos
                        if (!usuarioDTO.getFechaExpiracion().before(new Date()) &&
                             ELDAPEstadoUsuario.ACTIVO.codeExist(usuarioDTO.getEstado())) {
                            this.funcionariosTramiteDTO.add(usuarioDTO);
                            break;
                        }

                    }
                }
            }
        }

//        this.funcionariosTramiteDTO = this.getTramiteService()
//            .buscarFuncionariosEjecutoresPorTerritorialDTO(this.usuario
//                .getDescripcionEstructuraOrganizacional());
    }

    /**
     * Confirma la asignacion una vez se ha seleccionado el usuario de destino
     *
     * @author felipe.cadena
     *
     */
    public void confirmarReasignacion() {

        List<TramiteEstado> estados = new ArrayList<TramiteEstado>();
        List<TramiteReasignacion> reasignaciones = new ArrayList<TramiteReasignacion>();
        List<String> idsActividadesTransferir = new ArrayList<String>();

        //validar si el usuario seleccionado ya esta en los tramites asociados
        for (Tramite tramite : this.tramitesDefinitivosReasignacion) {
            if (this.funcionarioSeleccionadoParaAsignacion.getLogin().equals(tramite.
                getActividadActualTramite().getUsuarioEjecutor())) {
                this.addMensajeError(
                    "El usuario seleccionado ya tiene asignados uno o más de los trámites y/o actividades que han sido seleccionados para reasignación. Debe asegurarse de seleccionar un usuario diferente a los relacionados en la selección para reasignación");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        }

        for (Tramite tramite : this.tramitesDefinitivosReasignacion) {

            //Se guarda estado para historico de asignacion
            TramiteEstado te = new TramiteEstado();
            te.setFechaInicio(new Date());
            te.setFechaLog(new Date());
            te.setMotivo(this.motivoReasignacion);
            te.setEstado(ETramiteEstado.REASIGNADO.getCodigo());
            te.setResponsable(this.usuario.getLogin());
            te.setUsuarioLog(this.usuario.getLogin());
            te.setTramite(tramite);
            estados.add(te);

            //Se prepara el objeto reasignacion para auditoria
            TramiteReasignacion tr = new TramiteReasignacion();
            tr.setTramite(tramite);
            tr.setFuncionarioOrigen(tramite.getActividadActualTramite().getFuncionario());
            tr.setFuncionarioDestino(this.funcionarioSeleccionadoParaAsignacion.getLogin());
            tr.setMotivo(this.motivoReasignacion);
            tr.setFechaLog(new Date());
            tr.setUsuarioLog(this.usuario.getLogin());

            reasignaciones.add(tr);

            //se agrega a la lista de actividades para transferencia
            idsActividadesTransferir.add(tramite.getActividadActualTramite().getId());
        }

        //se transfieren las actividades
        try {
            this.getProcesosService().transferirActividadConservacion(idsActividadesTransferir,
                this.funcionarioSeleccionadoParaAsignacion);
            //se eliminan de las listas los tramites y las actividades seleccionadas
            this.eliminarActividades();
            this.eliminarTramites();
            //se persisten los estados de los tramites
            this.getTramiteService().insertarTramiteEstados(estados);

            //se persisten las reasignaciones asociadas a la transferencia
            this.getTramiteService().guardarActualizarTramiteReasignacionMultiple(reasignaciones);

            this.motivoReasignacion = "";

        } catch (Exception e) {
            this.addMensajeError("Error al transferir los tramites seleccionados");
            LOGGER.error(e.getMessage());
        }

    }

    //---- Manejo de la carga de actividades por usuario---//
    //----      tomado de AsignacionTramitesMB          ---//
    /**
     * Método que retorna los trámites asignados al ejecutor seleccionado
     *
     * @author franz.gamba
     */
    public void consultarTramitesAsignadosAEjecutor() {

        LOGGER.debug("En AsignacionTramitesMB entra en consultarTramitesAsignadosAEjecutor");
        String nombreFileT;
        String nombreFileO;
        PiePlot plotO;
        PiePlot plotT;

        if (this.funcionarioSeleccionadoParaAsignacion == null) {
            this.addMensajeError("Debe seleccionar un ejecutor!");
            return;
        }

        Pair<List<Tramite>, Map<Long, String>> datosActividades = this.
            consultarTramitesEjecutorActividades();
        this.assignedTramitesAEjecutor = datosActividades.getFirst();
        this.tablaActividades = datosActividades.getSecond();
        //Construir tabla de contingencia 
        TablaContingencia t = new TablaContingencia();
        Map<Long, String> tablaTramites = new HashMap<Long, String>();

        //Obtener un mapa con id y clasificación de trámite (terreno u oficina)
        for (Tramite tr : this.assignedTramitesAEjecutor) {
            tablaTramites.put(tr.getId(), tr.getClasificacion());
        }
        String clasificacion;
        for (Long id : this.tablaActividades.keySet()) {
            clasificacion = tablaTramites.get(id);
            t.addValor(this.tablaActividades.get(id), clasificacion != null ? clasificacion :
                "No clasificado");
        }
        try {
            //Se formatea el label del grafico
            PieSectionLabelGenerator generator = new StandardPieSectionLabelGenerator(
                "{1} ({2})", new DecimalFormat("0"), new DecimalFormat("0.00%"));

            JFreeChart jfreechartO = ChartFactory.createPieChart(
                "Trámites de oficina", createDatasetO(t), true, true, false);
            plotO = (PiePlot) jfreechartO.getPlot();
            plotO.setLabelGenerator(generator);

            nombreFileO = "dynamichartO" + this.fileG;

            File chartFileO = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(), nombreFileO);
            LOGGER.debug("Archivo a crear " + chartFileO.getAbsolutePath());
            ChartUtilities.saveChartAsPNG(chartFileO, jfreechartO, 375, 300);
            this.chartOficina = new DefaultStreamedContent(new FileInputStream(
                chartFileO), "image/png");

            JFreeChart jfreechartT = ChartFactory.createPieChart(
                "Trámites de terreno", createDatasetT(t), true, true, false);

            plotT = (PiePlot) jfreechartT.getPlot();
            plotT.setLabelGenerator(generator);

            nombreFileT = "dynamichartT" + this.fileG;
            File chartFileT = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(), nombreFileT);
            ChartUtilities.saveChartAsPNG(chartFileT, jfreechartT, 375, 300);

            this.chartTerreno = new DefaultStreamedContent(new FileInputStream(
                chartFileT), "image/png");
            this.fileG++;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        LOGGER.debug("*****NUMERO DE CUENTA= " +
             this.assignedTramitesAEjecutor.size() + " *****");

    }

    /**
     * Método que genera la gráfica de Torta de los estados de los trámites de Oficina
     *
     * @author franz.gamba
     * @return
     */
    /*
     * @modified pedro.garcia 25-10-2013 eliminación de parámetro no usado. Ortografía
     */
    private PieDataset createDatasetO(TablaContingencia t) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        boolean existenTramites = false;
        try {
            if (t != null) {
                existenTramites = true;
            }
            if (existenTramites) {
                Set<String> tuplas = t.getValores().keySet();
                for (String tupla : tuplas) {
                    String[] datos = tupla.split(TablaContingencia.SEPARADOR);
                    if (datos[1].equals(ETramiteClasificacion.OFICINA.toString())) {
                        if (dataset.getKeys().contains(datos[0])) {
                            Double a = dataset.getValue(datos[0])
                                .doubleValue();
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue() + a));
                            LOGGER.debug("***** VALOR : " + a);
                        } else {
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue()));
                        }
                    }
                }
            }
            /*
             * consulta si en los tramites por asignar existen tramites de oficina y de ser el caso
             * los agrega en la grafica con nombre de actividad: RevisarTramiteAsignado
             */
//            for (Tramite[] tramites : this.tramitesPorAsignar) {
//				// consulta primero si existe una lista de tramites por asignar
//                // para
//                // ese ejecutor
//                if (tramites[0] != null) {
//                    if (tramites[0].getFuncionarioEjecutor().equals(
//                        this.ejecutorSeleccionado.getFuncionarioId())) {
//
//                        existenTramites = true;
//                        for (Tramite tram : tramites) {
//                            // pregunta si el tramite es de Oficina
//                            if (tram.isTramiteOficina()) {
//                                if (dataset
//                                    .getKeys()
//                                    .contains(
//                                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
//                                    Double a = dataset
//                                        .getValue(
//                                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)
//                                        .doubleValue();
//                                    dataset.setValue(
//                                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
//                                        new Double(a + 1));
//                                    LOGGER.debug("***** VALOR : " + a);
//                                } else {
//                                    dataset.setValue(
//                                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
//                                        new Double(1));
//                                }
//                            }
//                        }
//
//                    }
//                }
//            }
            if (!existenTramites) {
                dataset.setValue("No existen trámites de este tipo",
                    new Double(0));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dataset;

    }

    /**
     * Metodo que toma los tramites asignados a un ejecutor segun las actividades que le
     * corresponden
     *
     * @author franz.gamba
     * @param ejecutorSeleccionado
     */
    private Pair<List<Tramite>, Map<Long, String>> consultarTramitesEjecutorActividades() {
        UsuarioDTO usuario_1 = this.funcionarioSeleccionadoParaAsignacion;
        /**
         * TODO Es posible que aquí toque especificar el rol ejecutor.
         */
        List<Actividad> actividadesUsuario = this.getProcesosService()
            .consultarListaActividades(usuario_1);

        Map<Long, String> tablaActividades_1 = new HashMap<Long, String>();
        List<Tramite> tramitesDeEjecutor = new ArrayList<Tramite>();
        List<Long> tramiteIds = new ArrayList<Long>();
        if (actividadesUsuario != null && !actividadesUsuario.isEmpty()) {
            for (Actividad act : actividadesUsuario) {
                tablaActividades_1.put(act.getIdObjetoNegocio(), act.getRutaActividad().
                    getActividad());
            }
            tramiteIds.addAll(tablaActividades_1.keySet());
            tramitesDeEjecutor = this.getTramiteService()
                .obtenerTramitesPorIds(tramiteIds);
        }
        return new Pair<List<Tramite>, Map<Long, String>>(tramitesDeEjecutor, tablaActividades_1);
    }

    /**
     * Método que genera la gráfica de torta de los estados de los trámites de Terreno
     *
     * @author franz.gamba
     * @return
     *
     * @modified pedro.garcia 25-10-2013 eliminación de parámetro no usado. Ortografía
     */
    private PieDataset createDatasetT(TablaContingencia t) {

        DefaultPieDataset dataset = new DefaultPieDataset();
        boolean existenTramites = false;
        try {
            if (t != null) {
                existenTramites = true;
            }
            if (existenTramites) {
                Set<String> tuplas = t.getValores().keySet();
                for (String tupla : tuplas) {
                    String[] datos = tupla.split(TablaContingencia.SEPARADOR);
                    if (datos[1].equals(ETramiteClasificacion.TERRENO.toString())) {
                        if (dataset.getKeys().contains(datos[0])) {
                            Double a = dataset.getValue(datos[0])
                                .doubleValue();
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue() + a));
                            LOGGER.debug("***** VALOR : " + a);
                        } else {
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue()));
                        }
                    }
                }
            }
            /*
             * consulta si en los tramites por asignar existen tramites de terreno y de ser el caso
             * los agrega en la grafica con nombre de actividad: RevisarTramiteAsignado
             */
//			if (this.tramitesPorAsignar.size() > 0) {
//				for (Tramite[] tramites : this.tramitesPorAsignar) {
//					// consulta primero si existe una lista de tramites por
//					// asignar
//					// para
//					// ese ejecutor
//					if (tramites.length > 0) {
//						if (tramites[0].getFuncionarioEjecutor().equals(
//								this.ejecutorSeleccionado.getFuncionarioId())) {
//
//							existenTramites = true;
//							for (Tramite tram : tramites) {
//								// pregunta si el tramite es de Terreno
//								if (tram.isTramiteTerreno()) {
//									if (dataset
//											.getKeys()
//											.contains(
//													ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
//										Double a = dataset
//												.getValue(
//														ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)
//												.doubleValue();
//										dataset.setValue(
//												ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
//												new Double(a + 1));
//										LOGGER.debug("***** VALOR : " + a);
//									} else {
//										dataset.setValue(
//												ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
//												new Double(1));
//									}
//								}
//							}
//
//						}
//					}
//				}
//			}
            if (!existenTramites) {
                dataset.setValue("No existen trámites de este tipo",
                    new Double(0));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dataset;
    }

    /**
     * Método ejecutado al cambiar el tipo de solicitud
     *
     * @author javier.aponte
     */
    public void cambioTipoSolicitud() {

        this.datosConsultaTramite.setTipoTramite(null);
        this.datosConsultaTramite.setClaseMutacion(null);
        this.datosConsultaTramite.setSubtipo(null);
    }

    /**
     * Método para preparar los números prediales de los predios asociados a los trámites
     * seleccionados
     *
     * @author felipe.cadena
     */
    public void prepararPrediosVisor() {
        LOGGER.debug("on AsignarDigitalizadorTopografoMB#prepararPrediosVisor");
        this.prediosParaZoom = "";

        List<Tramite> tramitesParaAsignacion = new ArrayList<Tramite>();

        //Se agregan los tramites de la seccion tramites
        if (this.tramitesSeleccionadosEliminacion != null &&
            this.tramitesSeleccionadosEliminacion.length > 0) {
            tramitesParaAsignacion.addAll(Arrays.asList(this.tramitesSeleccionadosEliminacion));
        }

        //Se agregan los tramites de la seccion actividades
        if (this.actividadesSeleccionadosEliminacion != null &&
            this.actividadesSeleccionadosEliminacion.length > 0) {
            for (Actividad act : this.actividadesSeleccionadosEliminacion) {
                tramitesParaAsignacion.addAll(this.tramitesActividadSeleccionados.get(act.
                    getNombre() + "_" + act.getUsuarioEjecutor()));
            }
        }

        for (Tramite td : tramitesParaAsignacion) {
            this.prediosParaZoom += td.getPredio().getNumeroPredial() + ",";
        }

        this.prediosParaZoom = this.prediosParaZoom.substring(0, this.prediosParaZoom.length() - 1);
        LOGGER.debug("P zoom " + this.prediosParaZoom);
    }

    /**
     * Retorna la lista de parametro ordenada alfanumericamente
     *
     * @author felipe.cadena
     * @param lista
     * @return
     */
    public List<SelectItem> ordenarItemList(List<SelectItem> lista) {

        if (lista != null) {
            Collections.sort(lista, new SelectItemComparator());
        }

        return lista;
    }

    /**
     * Método para inicializar las variables del visor GIS
     *
     * @author felipe.cadena
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";
    }

    /**
     * Action sobre el botón 'Cerrar', redirige a la página principal removiendo el managed bean
     * actual.
     *
     * @author felipe.cadena
     * @return
     */
    public String volverAlInicio() {
        UtilidadesWeb.removerManagedBean("consultaTramite");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;

    }

}
