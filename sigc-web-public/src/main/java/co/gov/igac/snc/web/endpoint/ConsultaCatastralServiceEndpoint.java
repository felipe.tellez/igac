package co.gov.igac.snc.web.endpoint;

import co.gov.igac.data.converted.xsd.ConstruccionType;
import co.gov.igac.data.converted.xsd.ConsultaIGACConv;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.newrelic.api.agent.NewRelic;

import co.gov.igac.data.converted.xsd.ConsultaIGACParams;
import co.gov.igac.data.converted.xsd.ObjectFactory;
import co.gov.igac.data.converted.xsd.PredioConv;
import co.gov.igac.data.converted.xsd.PropietarioType;
import co.gov.igac.data.converted.xsd.TerrenoType;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.web.controller.AbstractLocator;
import co.gov.igac.snc.web.util.MonitoringAgent;
import co.gov.igac.snc.web.ws.consultacatastralservice.ConsultaCatastralServiceRequest;
import co.gov.igac.snc.web.ws.consultacatastralservice.ConsultaCatastralServiceResponse;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBElement;

/**
 * The Class MovieServiceEndpoint.
 */
@Endpoint
public class ConsultaCatastralServiceEndpoint extends AbstractLocator {

    /**
     *
     */
    private static final long serialVersionUID = -6724932096688748719L;

    private static final String TARGET_NAMESPACE =
        "http://co/gov/igac/snc/web/ws/consultacatastralservice";

    public static Logger LOGGER = LoggerFactory.getLogger(ConsultaCatastralServiceEndpoint.class);

    /**
     * Gets the account details.
     *
     * @param accountNumber the account number
     * @return the account details
     */
    @PayloadRoot(localPart = "ConsultaCatastralServiceRequest", namespace = TARGET_NAMESPACE)
    @ResponsePayload
    public ConsultaCatastralServiceResponse consultarPredios(
        @RequestPayload ConsultaCatastralServiceRequest request) {
        ConsultaCatastralServiceResponse response = new ConsultaCatastralServiceResponse();
        try {
            ConsultaIGACParams params = request.getParams();
            FiltroConsultaCatastralWebService filtro = mapeoParametrosRequestFiltroConsulta(params);
            Long numeroPagina = Long.valueOf(URLDecoder.decode(request.getParams().getPagina().
                getValue(), "UTF-8"));
            List<Predio> predios = this.getConservacionService().
                buscaPrediosConsultaCatastralWebService(filtro, numeroPagina);
            response = this.construirRespuesta(response, predios);

            //////////////////////////////////////////////////////////////////////////////////////////////
            // newrelic custom instrumentation 
            if (MonitoringAgent.getInstance().isEnabled()) {
                addInstrumentation(params, predios.size(), numeroPagina);
            }
            //////////////////////////////////////////////////////////////////////////////////////////////
        } catch (Exception e) {
            LOGGER.error("Error en el servicio web de consultas catastrales.", e);
            ObjectFactory fabrica = new ObjectFactory();
            fabrica.createExceptionException(e);

        }
        return response;
    }

    /**
     * mapea los predios resultado de la consulta en un objeto de respuesta del web service
     *
     * @param respuesta objeto del tipo response definido en el WSDL
     * @param predios predios recuperados del SNC
     * @return respuesta construida con la informacion predial requerida.
     * @author andres.eslava
     */
    private ConsultaCatastralServiceResponse construirRespuesta(
        ConsultaCatastralServiceResponse respuesta, List<Predio> predios) {
        co.gov.igac.data.converted.xsd.ObjectFactory factory = new ObjectFactory();
        ConsultaIGACConv consultaIGACConv = factory.createConsultaIGACConv();

        try {

            for (Predio predio : predios) {

                // DatosBasicosPredio
                PredioConv unPredioConv = factory.createPredioConv();

                unPredioConv.setAreaConstruccion(predio.getAreaConstruccion());
                unPredioConv.setAreaTerreno(predio.getAreaTerreno());
                unPredioConv.setAvaluo(predio.getAvaluoCatastral());
                unPredioConv.setCodDpto(factory.createPredioConvCodDpto(predio
                    .getDepartamento().getCodigo()));
                unPredioConv.setCodMpio(factory.createPredioConvCodMpio(predio
                    .getMunicipio().getCodigo()));
                unPredioConv.setCodDestino(factory
                    .createPredioConvCodDestino(predio.getDestino()));
                unPredioConv.setMatricula(factory
                    .createPredioConvMatricula(predio
                        .getMatriculaInmobiliaria()));
                unPredioConv.setNumPredialNuevo(factory
                    .createPredioConvNumPredialNuevo(predio
                        .getNumeroPredial()));
                unPredioConv.setNumPredialAnterior(factory
                    .createPredioConvNumPredialAnterior(predio
                        .getNumeroPredialAnterior()));

                // Dato Direccion
                unPredioConv.setDireccion(factory
                    .createConsultaIGACParamsDireccion(predio
                        .getPredioDireccions().get(0).getDireccion()));

                // Datos de las personas
                for (PersonaPredio personaPredio : predio.getPersonaPredios()) {
                    PropietarioType unPropietario = factory
                        .createPropietarioType();
                    Persona unaPersona = personaPredio.getPersona();
                    unPropietario.setNombre(factory.createPropietarioTypeNombre(unaPersona.
                        getNombre()));
                    unPropietario.setNumDoc(factory.createPropietarioTypeNumDoc(unaPersona.
                        getNumeroIdentificacion()));
                    unPropietario.setTipoDoc(factory.createPropietarioTypeTipoDoc(unaPersona.
                        getTipoIdentificacion()));
                    unPredioConv.getPropietario().add(unPropietario);
                }

                // Datos de las zonas
                for (PredioZona predioZona : predio.getPredioZonas()) {
                    TerrenoType unTerreno = factory.createTerrenoType();
                    unTerreno.setArea(predioZona.getArea());
                    unTerreno.setZonaFisica(predioZona.getZonaFisica());
                    unTerreno.setZonaGeoEconomica(predioZona.getZonaGeoeconomica());
                    unPredioConv.getTerreno().add(unTerreno);

                }

                // Datos de las construcciones
                for (UnidadConstruccion construccion : predio
                    .getUnidadConstruccions()) {
                    ConstruccionType unaConstruccion = factory.createConstruccionType();
                    if (construccion.getAreaConstruida() != null) {
                        unaConstruccion.setArea(construccion.getAreaConstruida());
                    }
                    if (construccion.getTotalBanios() != null) {
                        unaConstruccion.setNumBanos(construccion.getTotalBanios());
                    }
                    if (construccion.getTotalHabitaciones() != null) {
                        unaConstruccion.setNumHabitaciones(construccion.getTotalHabitaciones());
                    }
                    if (construccion.getTotalLocales() != null) {
                        unaConstruccion.setNumLocales(construccion.getTotalLocales());
                    }
                    if (construccion.getTotalPuntaje() != null) {
                        unaConstruccion.setPuntaje(construccion.getTotalPuntaje().intValue());
                    }

                    if (construccion.getUsoConstruccion() != null) {
                        unaConstruccion.setUso(Integer.valueOf(construccion.getUsoConstruccion().
                            getNombre()));
                    }

                    unPredioConv.getConstruccion().add(unaConstruccion);
                }
                consultaIGACConv.getPredio().add(unPredioConv);
            }
            respuesta.setReturn(consultaIGACConv);
            return respuesta;
        } catch (Exception e) {
            ConsultaIGACConv attrib1 = null;
            respuesta.setReturn(attrib1);
            LOGGER.error(e.getMessage(), e);
            return respuesta;
        }
    }

    /**
     *
     * Instrumentación personalizada de la aplicación utilizando new relic
     *
     * https://docs.newrelic.com/docs/agents/java-agent/custom-instrumentation/java-agent-api-example-program
     * https://docs.newrelic.com/docs/agents/java-agent/custom-instrumentation/java-agent-api
     *
     * @param parametrosRequest
     */
    private void addInstrumentation(ConsultaIGACParams parametrosRequest, Integer cantidadResultados,
        Long numeroPagina) {
        try {
            NewRelic.setTransactionName("api", "api/consultaCatastral");

            NewRelic.addCustomParameter("service", "consultaCatastral");

            if (parametrosRequest.getTipoNumeroPredial() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getTipoNumeroPredial().getValue(), "UTF-8");
                NewRelic.addCustomParameter("tipoNumeroPredial", valor);

            }

            if (parametrosRequest.getNumeroPredial() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getNumeroPredial().getValue(), "UTF-8");
                NewRelic.addCustomParameter("numeroPredial", valor);
                NewRelic.addCustomParameter("municipio", valor.substring(0, 5));
            }

            if (parametrosRequest.getMatricula() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getMatricula().getValue(), "UTF-8");
                NewRelic.addCustomParameter("matricula", valor);
            }

            if (parametrosRequest.getPrimerNombre() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getPrimerNombre().getValue(), "UTF-8");
                NewRelic.addCustomParameter("primerNombre", valor);
            }

            if (parametrosRequest.getSegundoNombre() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getSegundoNombre().getValue(), "UTF-8");
                NewRelic.addCustomParameter("segundoNombre", valor);
            }

            if (parametrosRequest.getPrimerApellido() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getPrimerApellido().getValue(), "UTF-8");
                NewRelic.addCustomParameter("primerApellido", valor);
            }
            if (parametrosRequest.getSegundoApellido() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getSegundoApellido().getValue(), "UTF-8");
                NewRelic.addCustomParameter("segundoApellido", valor);
            }
            if (parametrosRequest.getNumeroDocumento() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getNumeroDocumento().getValue(), "UTF-8");
                NewRelic.addCustomParameter("numeroDocumento", valor);
            }
            if (parametrosRequest.getRazonSocial() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getRazonSocial().getValue(), "UTF-8");
                NewRelic.addCustomParameter("razonSocial", valor);
            }
            if (parametrosRequest.getTipoDocumento() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getTipoDocumento().getValue(), "UTF-8");
                NewRelic.addCustomParameter("tipoDocumento", valor);
            }
            if (parametrosRequest.getDireccion() != null) {
                String valor = URLDecoder.decode(parametrosRequest
                    .getDireccion().getValue(), "UTF-8");
                NewRelic.addCustomParameter("direccion", valor);
            }

            NewRelic.recordMetric("Custom/ServiceNumberOfResults", cantidadResultados.longValue());
            NewRelic.recordMetric("Custom/ServicePageNumber", numeroPagina);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            NewRelic.noticeError(e);
        }
    }

    /**
     * mapea los parametros del esquema definido en las operaciones publicas por el servicio en un
     * objeto denominado FiltroDatosConsultaPredios
     *
     * @param parametrosRequest
     * @return
     * @throws UnsupportedEncodingException
     * @author andres.eslava
     */
    private FiltroConsultaCatastralWebService mapeoParametrosRequestFiltroConsulta(
        ConsultaIGACParams parametrosRequest) throws UnsupportedEncodingException {
        FiltroConsultaCatastralWebService filtro = new FiltroConsultaCatastralWebService();
        try {
            if (parametrosRequest.getTipoNumeroPredial() != null) {
                String tipoNumeroPredial = URLDecoder.decode(parametrosRequest.
                    getTipoNumeroPredial().getValue(), "UTF-8");
                if (tipoNumeroPredial != null && !tipoNumeroPredial.isEmpty()) {
                    if (parametrosRequest.getNumeroPredial() != null) {
                        if (tipoNumeroPredial.equals("nuevo")) {
                            filtro.setPredio_numeroPredialNuevo(URLDecoder.decode(parametrosRequest.
                                getNumeroPredial().getValue(), "UTF-8"));
                        } else if (tipoNumeroPredial.equals("anterior")) {
                            filtro.setPredio_numeroPredialAnterior(URLDecoder.decode(
                                parametrosRequest.getNumeroPredial().getValue(), "UTF-8"));
                        }
                    }
                }
            }
            if (parametrosRequest.getMatricula() != null) {
                filtro.setPredio_matricula(URLDecoder.decode(parametrosRequest.getMatricula().
                    getValue(), "UTF-8"));
            }

            if (parametrosRequest.getPrimerNombre() != null) {
                filtro.setPersona_primerNombre(URLDecoder.decode(
                    parametrosRequest.getPrimerNombre().getValue(), "UTF-8"));
            }

            if (parametrosRequest.getSegundoNombre() != null) {
                filtro.setPersona_segundoNombre(URLDecoder.decode(parametrosRequest.
                    getSegundoNombre().getValue(), "UTF-8"));
            }

            if (parametrosRequest.getPrimerApellido() != null) {
                filtro.setPersona_primerApellido(URLDecoder.decode(parametrosRequest.
                    getPrimerApellido().getValue(), "UTF-8"));
            }

            if (parametrosRequest.getSegundoApellido() != null) {
                filtro.setPersona_segundoApellido(URLDecoder.decode(parametrosRequest.
                    getSegundoApellido().getValue(), "UTF-8"));
            }

            if (parametrosRequest.getNumeroDocumento() != null) {
                filtro.setPersona_numeroIdentificacion(URLDecoder.decode(parametrosRequest.
                    getNumeroDocumento().getValue(), "UTF-8"));
            }

            if (parametrosRequest.getRazonSocial() != null) {
                filtro.setPersona_razonSocial(URLDecoder.decode(parametrosRequest.getRazonSocial().
                    getValue(), "UTF-8"));
            }

            if (parametrosRequest.getTipoDocumento() != null) {
                filtro.setPersona_tipoIdentificacion(URLDecoder.decode(parametrosRequest.
                    getTipoDocumento().getValue(), "UTF-8"));
            }

            if (parametrosRequest.getDireccion() != null) {
                filtro.setPredioDireccion_direccion(URLDecoder.decode(parametrosRequest.
                    getDireccion().getValue(), "UTF-8"));
            }

            return filtro;

        } catch (Exception e) {
            throw new UnsupportedEncodingException(e.getMessage());
        }
    }

}
