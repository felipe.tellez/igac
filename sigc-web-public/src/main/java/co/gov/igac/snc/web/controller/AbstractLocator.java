package co.gov.igac.snc.web.controller;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.fachadas.IActualizacion;
import co.gov.igac.snc.fachadas.IAvaluos;
import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.fachadas.IFormacion;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.IInterrelacion;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.web.components.IAvanzarProcesoConservacion;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.locator.SNCServiceLocator;

/**
 * Clase base para la ubicación de los servicios de negocio para el proyecto SNC además de
 * utilidades generales requeridas para los controladores web y los Managed Beans.
 *
 * @author juan.mendez
 * @version 2.0
 */
public abstract class AbstractLocator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7324938788303726080L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractLocator.class);

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    @Autowired
    private IContextListener contexto;

//----------------     methods   --------------------------------
    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    public IProcesos getProcesosService() {
        IProcesos service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getProcesosService();
            //LOGGER.debug("getConservacionService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    /**
     *
     * @return
     */
    public IConservacion getConservacionService() {
        IConservacion service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getConservacionService();
            //LOGGER.debug("getConservacionService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }
//--------------------------------------------------------------------------------------------------

    /**
     *
     * @return
     */
    public IGenerales getGeneralesService() {
        IGenerales service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getGeneralesService();
            //LOGGER.debug("getConservacionService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }
//--------------------------------------------------------------------------------------------------

    /**
     *
     * @return
     */
    public ITramite getTramiteService() {
        ITramite service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getTramiteService();
            //LOGGER.debug("getConservacionService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }
//--------------------------------------------------------------------------------------------------

    /**
     *
     * @return
     */
    public IActualizacion getActualizacionService() {
        IActualizacion service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getActualizacionService();
            //LOGGER.debug("getActualizacionService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    /**
     *
     * @return
     */
    public IAvaluos getAvaluosService() {
        IAvaluos service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getAvaluosService();
            //LOGGER.debug("getAvaluosService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    /**
     * Obtiene una referencia al servicio IFormacion
     *
     * @return
     */
    public IFormacion getFormacionService() {
        IFormacion service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getFormacionService();
            //LOGGER.debug("getFormacionService service:" + service);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    /**
     * Obtiene una referencia al servicio IInterrelacion
     *
     * @author fredy.wilches
     * @return
     */
    public IInterrelacion getInterrelacionService() {
        IInterrelacion service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getInterrelacionService();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    /**
     * Obtiene una referencia al servicio IFormacion
     *
     * @return
     */
//TODO :: juan.mendez :: revisar doumentación del método (IFormacion ??) :: pedro.garcia    
    public IAvanzarProcesoConservacion getAvanzarProcesoConservacion() {
        IAvanzarProcesoConservacion service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getAvanzarProcesoConservacion();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    /**
     *
     * @return
     */
    public IContextListener getContexto() {
        return contexto;
    }

    /**
     * @modified pedro.garcia 17-05-2012 se adiciona la lectura de la propiedad "osmi" del
     * properties en este método para poder usarla en el mb de tareas pendientes para controlar el
     * armado del árbol de tareas
     *
     * @param contexto
     */
    public void setContexto(IContextListener contexto) {
        this.contexto = contexto;
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    //
    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Obtiene los datos del usuario que se autenticó en el sistema
     *
     * @return
     */
    public UsuarioDTO obtenerDatosUsuarioAutenticado() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        /* Collection<? extends GrantedAuthority> c=authentication.getAuthorities(); for
         * (GrantedAuthority ga:c) System.out.println(ga.getAuthority()); GrantedAuthorityImpl
         * gai=new GrantedAuthorityImpl("test");
         *
         * authentication.getAuthorities().add(gai); */
        if (this.getGeneralesService() == null) {
            LOGGER.error("No se pudo localizar el servicio Generales");
        } else if (authentication == null) {
            LOGGER.error("Problemas localizando el usuario del contexto");
        } else {
            UsuarioDTO usuarioDto = this.getGeneralesService().getCacheUsuario(authentication.
                getName());
            return usuarioDto;
        }
        return null;
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////
}
