package co.gov.igac.snc.web.mb.actualizacion.reconocimiento;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * @cu306 Gestiona novedades de reconocimiento.
 *
 * @author andres.eslava
 *
 */
@Component("gestionaNovedadesDeReconocimiento")
@Scope("session")
public class GestionaNovedadesDeReconocimientoMB extends SNCManagedBean {

    /**
     * Atributo de serializacion
     */
    private static final long serialVersionUID = 1L;

    /**
     * Miembro que determina la busqueda por valor inicial de manzana
     */
    private String manzanaInicial;

    /**
     * Miembro que determina la busqueda por valor inicial de manzana
     */
    private String manzanaFinal;

    /**
     * Observaciones a la novedad de reconocimiento
     */
    private String observaciones;

    /**
     * Determina si se solicita la digitalizacion de nuevas areas. {1-0} 1:Digitaliza 0: No
     * Digitaliza
     */
    private Boolean solicitudDigitalizacionNuevasAreas;

    /**
     * Listado de manzanas del municipio.
     */
    private List<ManzanaVereda> manzanasMunicipio;

    /**
     * Listado de manzanas seleccionadas en la tabla de manzanas municipio.
     */
    private ManzanaVereda[] manzanasSeleccionadas;

    @PostConstruct
    public void init() {

    }

    // **************************************************************************************************************************
    // Getters y Setters
    // **************************************************************************************************************************
    public String getManzanaInicial() {
        return manzanaInicial;
    }

    public void setManzanaInicial(String manzanaInicial) {
        this.manzanaInicial = manzanaInicial;
    }

    public String getManzanaFinal() {
        return manzanaFinal;
    }

    public void setManzanaFinal(String manzanaFinal) {
        this.manzanaFinal = manzanaFinal;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Boolean getSolicitudDigitalizacionNuevasAreas() {
        return solicitudDigitalizacionNuevasAreas;
    }

    public void setSolicitudDigitalizacionNuevasAreas(
        Boolean solicitudDigitalizacionNuevasAreas) {
        this.solicitudDigitalizacionNuevasAreas = solicitudDigitalizacionNuevasAreas;
    }

    public List<ManzanaVereda> getManzanasMunicipio() {
        return manzanasMunicipio;
    }

    public void setManzanasMunicipio(List<ManzanaVereda> manzanasMunicipio) {
        this.manzanasMunicipio = manzanasMunicipio;
    }

    public ManzanaVereda[] getManzanasSeleccionadas() {
        return manzanasSeleccionadas;
    }

    public void setManzanasSeleccionadas(ManzanaVereda[] manzanasSeleccionadas) {
        this.manzanasSeleccionadas = manzanasSeleccionadas;
    }

}
