package co.gov.igac.snc.web.mb.conservacion;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace de MB para manejar el ingreso de la Firma del usuario que se va a mostrar en la
 * resolucion generada.
 *
 * @author leidy.gonzalez
 *
 */
/**
 * @author leidy.gonzalez
 *
 */
@Component("administrarFirma")
@Scope("session")
public class AdministracionFirmaMecanicaMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionFirmaMecanicaMB.class);

    /*
     * Interfaces de servicio
     */
    @Autowired
    private GeneralMB generalService;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteFirma;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Archivo resultado cargado
     */
    private File archivoResultado;

    /**
     * Banderas de visualización de los botones guardar
     */
    private boolean banderaBotonGuardar;

    /**
     * Control de carga de archivos de soporte
     */
    private int aSoporte;
    private int aBMasa;

    /**
     * Territorial seleccionada
     */
    private String territorialSeleccionada;

    /**
     * Lista de Territoriales del usuario autenticado
     */
    private List<SelectItem> listaTerritoriales;

    /**
     * determina si esta habilitada la seleccion de la territorial
     */
    private boolean banderaSeleccionTerritorial;

    /**
     * Tipo de identificación para la busqueda de personas bloqueadas
     */
    private String tipoIdentificacion;

    /**
     * Tipo de identificación de una persona
     */
    private List<SelectItem> personaTiposIdentificacion;

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    private StreamedContent fileTempDownload;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * ReporteDTO del documento
     */
    private ReporteDTO reporteDocumento;

    /**
     * Persona seleccionada de una lista de personas coincidentes.
     */
    private Persona personaSeleccionada;

    /**
     * Se almacenan los roles asociados a las OUCs
     */
    private Map<String, List<SelectItem>> rolesUOCs;

    /**
     * Se almacenan los usuarios asociados a las OUCs clasificados por rol
     */
    private Map<String, List<SelectItem>> usuariosRolUOCs;

    /**
     * Variable item list con los valores de las UOC de la territorial
     */
    private List<SelectItem> UOCItemList;

    /**
     * determina si esta habilitada la seleccion de uocs
     */
    private boolean banderaSeleccionUOC;

    /**
     * UOC seleccionada
     */
    private String uOCseleccionada;

    /**
     * Nombre de la UOC seleccionada
     */
    private String uOCseleccionadaNombre;

    /**
     * Variable item list con los valores de los roles disponibles
     */
    private List<SelectItem> rolesItemList;

    /**
     * Variable item list con los valores de los roles de la UOC
     */
    private List<SelectItem> rolesUOCItemList;
    /**
     * Variable item list con los valores de los roles de la Territoial
     */
    private List<SelectItem> rolesTerritorialItemList;

    /**
     * Lista para los usuarios clasificaos por territorial
     */
    private Map<String, List<UsuarioDTO>> usuariosDtoPorUOC;

    /**
     * Variable item list con los valores de los usuarios aociados al rol
     */
    private List<SelectItem> usuariosRolItemList;

    /**
     * Rol seleccionado
     */
    private String rolSeleccionado;

    /**
     * Usuario seleccionado
     */
    private String usuarioSeleccionado;

    /**
     * Codigo de la Territorial
     */
    private String codigoTerritorial;

    /**
     * Firma Usuario
     */
    private FirmaUsuario firmaUsuario;

    /**
     * Lista de rutas de archivos generados del usuario
     */
    private List<ReporteDTO> rutasArchivosAsociadosAFirmaUsuario;

    /**
     * determina si esta habilitada la seleccion de botones Carga, Elimina
     */
    private boolean banderaHabilitaAceptaCargaElimina;

    /**
     * Lista de usuarios
     */
    private List<UsuarioDTO> usuarios;

    /**
     * Lista de usuarios
     */
    private List<UsuarioDTO> usuariosUOC;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    private EOrden ordenRoles = EOrden.NOMBRE;

    private boolean activaBotonEliminar;

    /**
     * Lista de las UOC de la territorial
     */
    private List<EstructuraOrganizacional> uocs;

    // ------------- methods ------------------------------------
    public String getuOCseleccionadaNombre() {
        return uOCseleccionadaNombre;
    }

    public void setuOCseleccionadaNombre(String uOCseleccionadaNombre) {
        this.uOCseleccionadaNombre = uOCseleccionadaNombre;
    }

    public List<EstructuraOrganizacional> getUocs() {
        return uocs;
    }

    public void setUocs(List<EstructuraOrganizacional> uocs) {
        this.uocs = uocs;
    }

    public boolean isActivaBotonEliminar() {
        return activaBotonEliminar;
    }

    public void setActivaBotonEliminar(boolean activaBotonEliminar) {
        this.activaBotonEliminar = activaBotonEliminar;
    }

    public EOrden getOrdenRoles() {
        return ordenRoles;
    }

    public void setOrdenRoles(EOrden ordenRoles) {
        this.ordenRoles = ordenRoles;
    }

    public boolean isBanderaHabilitaAceptaCargaElimina() {
        return banderaHabilitaAceptaCargaElimina;
    }

    public void setBanderaHabilitaAceptaCargaElimina(
        boolean banderaHabilitaAceptaCargaElimina) {
        this.banderaHabilitaAceptaCargaElimina = banderaHabilitaAceptaCargaElimina;
    }

    public ReportesUtil getReportsService() {
        return reportsService;
    }

    public void setReportsService(ReportesUtil reportsService) {
        this.reportsService = reportsService;
    }

    public ReporteDTO getReporteDocumento() {
        return reporteDocumento;
    }

    public void setReporteDocumento(ReporteDTO reporteDocumento) {
        this.reporteDocumento = reporteDocumento;
    }

    public List<UsuarioDTO> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    public List<UsuarioDTO> getUsuariosUOC() {
        return usuariosUOC;
    }

    public void setUsuariosUOC(List<UsuarioDTO> usuariosUOC) {
        this.usuariosUOC = usuariosUOC;
    }

    public List<ReporteDTO> getRutasArchivosAsociadosAFirmaUsuario() {
        return rutasArchivosAsociadosAFirmaUsuario;
    }

    public void setRutasArchivosAsociadosAFirmaUsuario(
        List<ReporteDTO> rutasArchivosAsociadosAFirmaUsuario) {
        this.rutasArchivosAsociadosAFirmaUsuario = rutasArchivosAsociadosAFirmaUsuario;
    }

    public FirmaUsuario getFirmaUsuario() {
        return firmaUsuario;
    }

    public void setFirmaUsuario(FirmaUsuario firmaUsuario) {
        this.firmaUsuario = firmaUsuario;
    }

    public Documento getDocumentoSoporteFirma() {
        return documentoSoporteFirma;
    }

    public void setDocumentoSoporteFirma(Documento documentoSoporteFirma) {
        this.documentoSoporteFirma = documentoSoporteFirma;
    }

    public String getCodigoTerritorial() {
        return codigoTerritorial;
    }

    public void setCodigoTerritorial(String codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public String getTerritorialSeleccionada() {
        return territorialSeleccionada;
    }

    public void setTerritorialSeleccionada(String territorialSeleccionada) {
        this.territorialSeleccionada = territorialSeleccionada;
    }

    public String getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(String usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public List<SelectItem> getUsuariosRolItemList() {
        return usuariosRolItemList;
    }

    public void setUsuariosRolItemList(List<SelectItem> usuariosRolItemList) {
        this.usuariosRolItemList = usuariosRolItemList;
    }

    public Map<String, List<UsuarioDTO>> getUsuariosDtoPorUOC() {
        return usuariosDtoPorUOC;
    }

    public void setUsuariosDtoPorUOC(Map<String, List<UsuarioDTO>> usuariosDtoPorUOC) {
        this.usuariosDtoPorUOC = usuariosDtoPorUOC;
    }

    public List<SelectItem> getRolesUOCItemList() {
        return rolesUOCItemList;
    }

    public void setRolesUOCItemList(List<SelectItem> rolesUOCItemList) {
        this.rolesUOCItemList = rolesUOCItemList;
    }

    public List<SelectItem> getRolesTerritorialItemList() {
        return rolesTerritorialItemList;
    }

    public void setRolesTerritorialItemList(
        List<SelectItem> rolesTerritorialItemList) {
        this.rolesTerritorialItemList = rolesTerritorialItemList;
    }

    public List<SelectItem> getRolesItemList() {
        return rolesItemList;
    }

    public void setRolesItemList(List<SelectItem> rolesItemList) {
        this.rolesItemList = rolesItemList;
    }

    public String getuOCseleccionada() {
        return uOCseleccionada;
    }

    public void setuOCseleccionada(String uOCseleccionada) {
        this.uOCseleccionada = uOCseleccionada;
    }

    public Map<String, List<SelectItem>> getUsuariosRolUOCs() {
        return usuariosRolUOCs;
    }

    public void setUsuariosRolUOCs(Map<String, List<SelectItem>> usuariosRolUOCs) {
        this.usuariosRolUOCs = usuariosRolUOCs;
    }

    public List<SelectItem> getUOCItemList() {
        return UOCItemList;
    }

    public void setUOCItemList(List<SelectItem> uOCItemList) {
        UOCItemList = uOCItemList;
    }

    public boolean isBanderaSeleccionUOC() {
        return banderaSeleccionUOC;
    }

    public void setBanderaSeleccionUOC(boolean banderaSeleccionUOC) {
        this.banderaSeleccionUOC = banderaSeleccionUOC;
    }

    public Map<String, List<SelectItem>> getRolesUOCs() {
        return rolesUOCs;
    }

    public void setRolesUOCs(Map<String, List<SelectItem>> rolesUOCs) {
        this.rolesUOCs = rolesUOCs;
    }

    public boolean isBanderaSeleccionTerritorial() {
        return banderaSeleccionTerritorial;
    }

    public void setBanderaSeleccionTerritorial(boolean banderaSeleccionTerritorial) {
        this.banderaSeleccionTerritorial = banderaSeleccionTerritorial;
    }

    public String getTerritorial() {
        return territorialSeleccionada;
    }

    public void setTerritorial(String territorial) {
        this.territorialSeleccionada = territorial;
    }

    public List<SelectItem> getListaTerritoriales() {
        return listaTerritoriales;
    }

    public void setListaTerritoriales(List<SelectItem> listaTerritoriales) {
        this.listaTerritoriales = listaTerritoriales;
    }

    public Documento getdocumentoSoporteFirma() {
        return documentoSoporteFirma;
    }

    public void setdocumentoSoporteFirma(
        Documento documentoSoporteFirma) {
        this.documentoSoporteFirma = documentoSoporteFirma;
    }

    public StreamedContent getFileTempDownload() {
        controladorDescargaArchivos();
        return fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public Persona getPersonaSeleccionada() {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(Persona personaSeleccionada) {
        this.personaSeleccionada = personaSeleccionada;
    }

    public Documento getDocumentoSoporteBloqueo() {
        return documentoSoporteFirma;
    }

    public void setDocumentoSoporteBloqueo(Documento documentoSoporteBloqueo) {
        this.documentoSoporteFirma = documentoSoporteBloqueo;
    }

    public List<SelectItem> getPersonaTiposIdentificacion() {
        return personaTiposIdentificacion;
    }

    public void setPersonaTiposIdentificacion(
        List<SelectItem> personaTiposIdentificacion) {
        this.personaTiposIdentificacion = personaTiposIdentificacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("BloqueoPredioMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.usuariosUOC = new ArrayList<UsuarioDTO>();

        this.activaBotonEliminar = false;
        this.banderaBotonGuardar = false;
        this.aSoporte = 0;
        this.aBMasa = 0;
        this.firmaUsuario = new FirmaUsuario();
        this.uOCseleccionada = null;

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        if (usuario.getDescripcionUOC() != null) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionUOC(),
                ERol.RESPONSABLE_SIG);
        }

        //si no existe Responsable SIG en la UOC se buscan en la territorial.
        if (usuarios.isEmpty()) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_SIG);
        }

        //Se cargan las UOCs asociadas al usuario.
        this.cargarTerritoriales();

        //Se cargan las UOCs asociadas al usuario.
        this.cargarUOCs();

        this.cargaUsuariosUOC();

    }

    public void cargaUsuariosUOC() {

        List<EstructuraOrganizacional> oucs = this.getGeneralesService().
            buscarUOCporCodigoTerritorial(this.codigoTerritorial);

        for (EstructuraOrganizacional eo : oucs) {
            if (this.uOCseleccionada != null &&
                this.uOCseleccionada.equals(eo.getCodigo())) {
                String nombreUOC = eo.getNombre().replace(' ', '_');
                if (this.usuario.getDescripcionUOC() != null &&
                     this.usuario.getDescripcionUOC().equals(nombreUOC)) {

                    this.usuariosUOC = this.getTramiteService().buscarFuncionariosPorTerritorial(
                        this.usuario.getDescripcionTerritorial(), nombreUOC);
                    this.uOCseleccionadaNombre = nombreUOC;

                }
                break;
            }
        }

    }

    /**
     * Metodo para verificar si se selecciono un usuario
     *
     * @author leidy.gonzalez
     */
    /*
     * @modified leidy.gonzalez::02/03/2016::#16198:: Se inializa el documento soporte para que no
     * se muestre el anterior al haber cambiado de funcionario. Y activa o desactiva variable
     * activaBotonEliminar dependiendo si el usuario tiene documento de firma o no.
     */
    public void verificaUsuarioSeleccionado() {
        this.firmaUsuario = new FirmaUsuario();

        if (this.usuarioSeleccionado != null && !this.usuarioSeleccionado.isEmpty()) {
            this.banderaHabilitaAceptaCargaElimina = true;
        } else {
            this.banderaHabilitaAceptaCargaElimina = false;
            this.activaBotonEliminar = false;
        }

        if (this.usuarios != null && !this.usuarios.isEmpty()) {
            for (UsuarioDTO usuarioFuncionario : this.usuarios) {
                if (usuarioFuncionario.getLogin().equals(this.usuarioSeleccionado)) {

                    if (usuarioFuncionario.getNombreCompleto() != null) {
                        this.firmaUsuario.setNombreFuncionarioFirma((usuarioFuncionario.
                            getNombreCompleto()));
                        this.firmaUsuario = this.getGeneralesService().consultarUsuarioFirma(
                            this.firmaUsuario.getNombreFuncionarioFirma());
                    }

                    if (this.firmaUsuario != null && this.firmaUsuario.getDocumentoId().getId() !=
                        null) {
                        this.documentoSoporteFirma = this.getGeneralesService().buscarFirmaUsuario(
                            this.firmaUsuario.getDocumentoId().getId());
                        this.activaBotonEliminar = true;
                        break;
                    } else {
                        this.documentoSoporteFirma = null;
                        this.activaBotonEliminar = false;
                    }

                }
            }
        }

        if (this.usuariosUOC != null && !this.usuariosUOC.isEmpty()) {
            for (UsuarioDTO usuarioFuncionarioUOC : this.usuariosUOC) {
                if (usuarioFuncionarioUOC.getLogin().equals(this.usuarioSeleccionado)) {

                    this.firmaUsuario = new FirmaUsuario();

                    if (usuarioFuncionarioUOC.getNombreCompleto() != null) {
                        this.firmaUsuario.setNombreFuncionarioFirma((usuarioFuncionarioUOC
                            .getNombreCompleto()));
                        this.firmaUsuario = this.getGeneralesService()
                            .consultarUsuarioFirma(
                                this.firmaUsuario.getNombreFuncionarioFirma());

                        if (this.firmaUsuario != null &&
                             this.firmaUsuario.getDocumentoId().getId() != null) {
                            this.documentoSoporteFirma = this.getGeneralesService().
                                buscarFirmaUsuario(
                                    this.firmaUsuario.getDocumentoId().getId());

                        }

                    }
                    break;
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para inicializar las Territoriales asociadas al usuario
     *
     * @author leidy.gonzalez
     */
    public void cargarTerritoriales() {

        this.territorialSeleccionada = this.usuario.getDescripcionTerritorial();

        this.rolesUOCs = new HashMap<String, List<SelectItem>>();

        this.usuariosRolUOCs = new HashMap<String, List<SelectItem>>();

        this.codigoTerritorial = this.usuario.getCodigoTerritorial();

        //Inicio lista de seleccion
        this.listaTerritoriales = new ArrayList<SelectItem>();
        this.listaTerritoriales.add(new SelectItem(this.territorialSeleccionada));

        //cargar roles para la territorial
        if (listaTerritoriales.size() > 1) {
            this.banderaSeleccionTerritorial = true;
        } else {
            this.cargarRoles(new EstructuraOrganizacional(this.codigoTerritorial,
                this.territorialSeleccionada, null, null, null));
        }

        this.uOCseleccionada = this.usuario.getCodigoTerritorial();
        this.actualizarRolesListener();

    }

    /**
     * Metodo para inicializar las UOC asociadas al usuario
     *
     * @author leidy.gonzalez
     */
    /*
     * @modified by leidy.gonzalez :: #16746 :: 07/04/2016 Se modifica consulta de donde se cargan
     * la lista de UOC
     */
    public void cargarUOCs() {

        this.rolesUOCs = new HashMap<String, List<SelectItem>>();

        this.usuariosRolUOCs = new HashMap<String, List<SelectItem>>();

        this.codigoTerritorial = this.usuario.getCodigoTerritorial();

        if (this.usuario.getCodigoUOC() != null) {
            uocs = this.getGeneralesService().buscarUOCporCodigoTerritorial(this.codigoTerritorial);
        } else {
            uocs = this.getGeneralesService().buscarUOCporTerritorialSinRolEnUOC(
                this.codigoTerritorial, ERol.LIDER_TECNICO);
        }

        this.UOCItemList = new ArrayList<SelectItem>();

        if (uocs != null && !uocs.isEmpty()) {
            for (EstructuraOrganizacional eo : uocs) {
                //Inicio lista de seleccion
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        } else {
            uocs = this.getGeneralesService().buscarUOCporCodigoTerritorial(this.codigoTerritorial);

            for (EstructuraOrganizacional eo : uocs) {
                //Inicio lista de seleccion
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        }

        if (!UOCItemList.isEmpty() && this.usuario.getCodigoUOC() == null) {
            this.banderaSeleccionUOC = true;
        }

        this.uOCseleccionada = this.usuario.getCodigoUOC();
        this.actualizarRolesListener();

    }

    /**
     * Metodo para inicializar los roles asociados a las UOC
     *
     * @author leidy.gonzalez
     * @param uoc
     */
    /*
     * @modofied by leidy.gonzalez #14104
     */
    public void cargarRoles(EstructuraOrganizacional uoc) {

        List<SelectItem> rolesUoc = new ArrayList<SelectItem>();
        List<SelectItem> usuarioUocRol = new ArrayList<SelectItem>();
        this.rolesItemList = new ArrayList<SelectItem>();
        this.rolesUOCItemList = new ArrayList<SelectItem>();
        this.rolesTerritorialItemList = new ArrayList<SelectItem>();
        this.rolesUOCs = new HashMap<String, List<SelectItem>>();
        List<String> roles = new ArrayList<String>();

        String nombreUOC = uoc.getNombre().replace(' ', '_');

        if (this.usuario.getDescripcionTerritorial().equals(uoc.getNombre())) {

            this.usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), null);

        } else if (uoc.getNombre() != null && uoc.getNombre().contains(" ")) {

            this.usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), nombreUOC);

        } else if (uoc.getNombre() != null) {

            this.usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), null);
        }

        if (this.usuario.getDescripcionUOC() != null &&
             this.usuario.getDescripcionUOC().equals(uoc.getNombre())) {
            this.usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), nombreUOC);
            this.usuariosUOC = this.usuarios;
        }

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para " + uoc.getNombre());
            return;
        }
        for (UsuarioDTO user : usuarios) {

            for (String rol : user.getRoles()) {

                if (rol.equals(ERol.EJECUTOR_TRAMITE.toString())) {
                    continue;
                }

                if (!roles.contains(rol)) {

                    if (rol.equals(ERol.FUNCIONARIO_RADICADOR.toString()) ||
                         rol.equals(ERol.SECRETARIA_CONSERVACION.toString()) ||
                         rol.equals(ERol.DIRECTOR_TERRITORIAL.toString()) ||
                         rol.equals(ERol.RESPONSABLE_ACTUALIZACION.toString()) ||
                         rol.contains("PROFESIONAL_ABOGADO") ||
                         rol.equals(ERol.RESPONSABLE_CONSERVACION.toString())) {
                        roles.add(rol);
                    }

                    if (this.ordenRoles.equals(EOrden.NOMBRE)) {
                        Collections.sort(roles);
                    }
                }
                String userKey = uoc.getCodigo() + rol;
                if (!this.usuariosRolUOCs.containsKey(userKey)) {
                    usuarioUocRol = new LinkedList<SelectItem>();
                    usuarioUocRol.add(new SelectItem(user.getLogin(), user.getLogin()));
                    this.usuariosRolUOCs.put(userKey, usuarioUocRol);
                } else {
                    this.usuariosRolUOCs.get(userKey).add(new SelectItem(user.getLogin(), user.
                        getLogin()));
                }
            }
        }

        for (String rol : roles) {
            rolesUoc.add(new SelectItem(rol, rol));
        }
        this.rolesUOCs.put(uoc.getCodigo(), rolesUoc);

    }

    /**
     * Metodo para actualizar los roles asociados la oficina seleccionada
     *
     * @author leidy.gonzalez
     */
    public void actualizarRolesListener() {

        this.rolesItemList = new ArrayList<SelectItem>();
        this.rolSeleccionado = null;
        this.rolesUOCs = new HashMap<String, List<SelectItem>>();
        this.usuariosRolUOCs = new HashMap<String, List<SelectItem>>();

        if (!this.banderaSeleccionUOC && this.uOCseleccionada != null && !this.uOCseleccionada.
            isEmpty() && uocs != null && !uocs.isEmpty()) {

            for (EstructuraOrganizacional estructura : uocs) {
                if (this.uOCseleccionada.equals(estructura.getCodigo())) {

                    this.UOCItemList = new ArrayList<SelectItem>();
                    this.uOCseleccionadaNombre = estructura.getNombre();
                    this.usuario.setDescripcionUOC(this.uOCseleccionadaNombre);
                    this.UOCItemList.add(new SelectItem(estructura.getCodigo(), estructura.
                        getNombre()));
                    break;
                }
            }
            this.cargarRoles(new EstructuraOrganizacional(this.uOCseleccionada,
                this.uOCseleccionadaNombre, null, null, null));
            this.rolesItemList = this.rolesUOCs.get(this.uOCseleccionada);

        } else if (this.codigoTerritorial != null) {

            this.territorialSeleccionada = this.usuario.getDescripcionTerritorial();

            if (UOCItemList != null && !UOCItemList.isEmpty() && uocs != null && !uocs.isEmpty() &&
                this.banderaSeleccionUOC && this.uOCseleccionada != null) {

                for (EstructuraOrganizacional estructura : uocs) {
                    if (this.uOCseleccionada.equals(estructura.getCodigo())) {

                        this.uOCseleccionadaNombre = estructura.getNombre();
                        this.usuario.setDescripcionUOC(this.uOCseleccionadaNombre);
                        break;
                    }
                }
                this.cargarRoles(new EstructuraOrganizacional(this.uOCseleccionada,
                    this.uOCseleccionadaNombre, null, null, null));
                this.rolesItemList = this.rolesUOCs.get(this.uOCseleccionada);

            } else {
                this.cargarRoles(new EstructuraOrganizacional(this.codigoTerritorial,
                    this.territorialSeleccionada, null, null, null));
                this.territorialSeleccionada = this.usuario.getCodigoTerritorial();
                this.rolesItemList = this.rolesUOCs.get(this.territorialSeleccionada);
            }

        }

        this.usuariosRolItemList = new ArrayList<SelectItem>();
        this.usuarioSeleccionado = null;
        this.banderaHabilitaAceptaCargaElimina = false;
        this.activaBotonEliminar = false;

    }

    /**
     * Metodo para actualizar los usuarios asociados al rol seleccionado
     *
     * @author leidy.gonzalez
     */
    /*
     * @modified leidy.gonzalez::02/03/2016::#16198:: Se inializa usuario seleccionado para que no
     * se muestre el anterior al haber cargado un rol
     */
    public void actualizarUsuariosListener() {

        this.usuarioSeleccionado = null;
        this.usuariosRolItemList = new ArrayList<SelectItem>();

        if ((this.codigoTerritorial == null || this.codigoTerritorial.isEmpty()) &&
            (this.uOCseleccionada == null || this.uOCseleccionada.isEmpty())) {
            this.addMensajeError("Debe seleccionar territorial o UOC correspondiente");
        }

        if (this.rolSeleccionado == null) {

            Set<String> keys = this.usuariosRolUOCs.keySet();
            for (String key : keys) {
                if (key.startsWith(this.codigoTerritorial)) {
                    this.usuariosRolItemList.addAll(this.usuariosRolUOCs.get(key));
                } else if (key.startsWith(this.uOCseleccionada)) {
                    this.usuariosRolItemList.addAll(this.usuariosRolUOCs.get(key));
                }
            }
        } else {
            if (this.codigoTerritorial != null && !this.codigoTerritorial.isEmpty() &&
                this.uOCseleccionada == null) {

                this.usuariosRolItemList = this.usuariosRolUOCs.get(this.codigoTerritorial +
                    this.rolSeleccionado);

            }
            if (this.uOCseleccionada != null && !this.uOCseleccionada.isEmpty()) {
                this.usuariosRolItemList = this.usuariosRolUOCs.get(this.uOCseleccionada +
                    this.rolSeleccionado);
            }
            if ((this.codigoTerritorial == null || this.codigoTerritorial.isEmpty()) &&
                 (this.uOCseleccionada == null || this.uOCseleccionada.isEmpty())) {

                String mensaje =
                    "No existen funcionarios activos en esta Territorial/Unidad Operativa con el rol seleccionado";
                this.addMensajeError(mensaje);
            }
        }
        cargaUsuariosUOC();
        this.banderaHabilitaAceptaCargaElimina = false;
        this.activaBotonEliminar = false;
        this.documentoSoporteFirma = new Documento();
    }

    /**
     * metodo que recupera los datos del documento de soporte a cargar
     */
    @SuppressWarnings("deprecation")
    private void nuevoDocumento() {

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento.setId(ETipoDocumento.FIRMA_MECANICA_USUARIO
            .getId());

        this.documentoSoporteFirma.setFechaDocumento(new Date());
        this.documentoSoporteFirma.setEstado(EDocumentoEstado.RECIBIDO
            .getCodigo());
        this.documentoSoporteFirma.setBloqueado(ESiNo.NO.getCodigo());

        this.documentoSoporteFirma.setTipoDocumento(tipoDocumento);
        this.documentoSoporteFirma.setUsuarioCreador(this.usuario.getLogin());
        this.documentoSoporteFirma.setUsuarioLog(this.usuario.getLogin());
        this.documentoSoporteFirma.setFechaLog(new Date());

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        String rutaProducto;

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                eventoCarga.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;

            InputStream inputStream = eventoCarga.getFile().getInputstream();

            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            BufferedImage bimg = ImageIO.read(new File(this.archivoResultado.getPath()));

            int width = bimg.getWidth();
            int height = bimg.getHeight();

            int maxWith = 378;
            int maxHeight = 113;

            if (width > maxWith || height > maxHeight) {
                String mensaje =
                    "El ancho de la imagen debe ser de 10 cm(378 pixeles) y el alto de la imagen debe ser de 3 cm(113 pixeles)";
                this.addMensajeError(mensaje);
                return;
            }

            this.documentoSoporteFirma = new Documento();
            nuevoDocumento();
            this.documentoSoporteFirma.setArchivo(this.archivoResultado.getName());

            rutaProducto = this.getGeneralesService().guardarFirmaMecanicaUsuarioEnGestorDocumental(
                this.archivoResultado.getPath(),
                usuario.getCodigoEstructuraOrganizacional());

            this.reporteDocumento = this.reportsService.consultarReporteDeGestorDocumental(
                rutaProducto);

            if (this.reporteDocumento != null) {
                PrevisualizacionDocMB previsualizacion = (PrevisualizacionDocMB) UtilidadesWeb.
                    getManagedBean("previsualizacionDocumento");

                previsualizacion.setRutaArchivoTemporal(this.reporteDocumento.getUrlWebReporte());
                previsualizacion.setReporteDTO(this.reporteDocumento);
                this.documentoSoporteFirma.setIdRepositorioDocumentos(rutaProducto);
                this.documentoSoporteFirma = this.getGeneralesService().
                    guardarDocumentoFirmaMecanicaUsuario(this.documentoSoporteFirma);
            } else {
                String mensaje = "No se pudo almacenar el documento en el servidor documental";
                this.addMensajeError(mensaje);
                return;
            }

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;

            String mensaje = "El documento se cargó adecuadamente.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            LOGGER.error("ERROR: " + e);
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
            return;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo ejecutado sobre el boton cerrar
     */
    public void cerrarGestionArchivo() {

        this.aBMasa = 0;
        this.aSoporte = 0;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado al cerrar las ventanas modales
     */
    public void cerrarEsc(org.primefaces.event.CloseEvent event) {

        this.aBMasa = 0;
        this.aSoporte = 0;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que permite almacenar la informaciòn ingresada en la tabla FIRMA_USUARIO
     */
    /*
     * @modified by leidy.gonzalez #14104
     */
    public void almacenarInformacionFirmaUsuario() {

        if (this.firmaUsuario != null && this.firmaUsuario.getNombreFuncionarioFirma() != null &&
            this.firmaUsuario.getDocumentoId() != null) {
            this.getGeneralesService().eliminarFirmaUsuario(this.firmaUsuario);
            this.getGeneralesService().eliminarDocumentoFirmaMecanica(this.firmaUsuario.
                getDocumentoId());
        }

        this.firmaUsuario = new FirmaUsuario();

        if (this.territorialSeleccionada != null &&
             this.rolSeleccionado != null && this.usuarioSeleccionado != null &&
             this.documentoSoporteFirma != null) {

            if (this.usuarios != null && !this.usuarios.isEmpty()) {
                for (UsuarioDTO usuarioFuncionario : this.usuarios) {
                    if (usuarioFuncionario.getLogin().equals(this.usuarioSeleccionado)) {

                        this.firmaUsuario.setNombreFuncionarioFirma((usuarioFuncionario.
                            getNombreCompleto()));
                        break;
                    }
                }
            }

            if (this.firmaUsuario.getNombreFuncionarioFirma() == null) {
                if (this.usuariosUOC != null && !this.usuariosUOC.isEmpty()) {
                    for (UsuarioDTO usuarioFuncionario : this.usuariosUOC) {
                        if (usuarioFuncionario.getLogin().equals(this.usuarioSeleccionado)) {

                            this.firmaUsuario.setNombreFuncionarioFirma((usuarioFuncionario.
                                getNombreCompleto()));
                            break;
                        }
                    }
                }
            }

            this.firmaUsuario.setUsuarioIngresaFirma(usuario.getLogin());
            this.firmaUsuario.setFechaFirmaDocumento(new Date());
            this.firmaUsuario.setUsuarioLog(usuario.getLogin());
            this.firmaUsuario.setFechaLog(new Date());

            if (this.documentoSoporteFirma != null) {
                this.firmaUsuario.setDocumentoId(this.documentoSoporteFirma);
                this.firmaUsuario.setUsuarioConFirma(ESiNo.SI.getCodigo());
            } else {
                String mensaje = "Es necesario cargar un documento para almacenar la firma";
                this.addMensajeError(mensaje);
                return;
            }

            if (this.firmaUsuario.getNombreFuncionarioFirma() != null) {

                this.firmaUsuario = this.getGeneralesService().
                    guardarFirmaUsuario(this.firmaUsuario);
                String mensaje = "La firma se almaceno exitosamente.";
                this.addMensajeInfo(mensaje);

            } else {
                String mensaje = "No se encontro usuario seleccionado para almacenar la firma";
                this.addMensajeError(mensaje);
                return;
            }

        } else {
            String mensaje = "Debe seleccionar alguno de los campos obligatorios";
            this.addMensajeError(mensaje);
            return;
        }

        this.limpiarCampos();
    }

    /*
     * @modified by leidy.gonzalez #16300:: 18/03/2016:: Se valida para que limpie los campos que
     * necesitan vover a cargarse.
     */
    public void limpiarCampos() {
        this.documentoSoporteFirma = new Documento();
        this.rolSeleccionado = null;
        this.usuarioSeleccionado = null;
        this.usuariosRolItemList = new ArrayList<SelectItem>();
        this.banderaHabilitaAceptaCargaElimina = false;
        this.activaBotonEliminar = false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de la descarga de un archivo temporal de office
     */
    public void controladorDescargaArchivos() {

        if (this.documentoSoporteFirma != null &&
             this.documentoSoporteFirma.getArchivo() != null &&
             !this.documentoSoporteFirma.getArchivo().isEmpty()) {

            File fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.documentoSoporteFirma.getArchivo());
            InputStream stream;

            try {

                stream = new FileInputStream(fileTemp);
                cargarDocumentoTemporalPrev();
                this.fileTempDownload = new DefaultStreamedContent(stream,
                    this.tipoMimeDocTemporal,
                    this.documentoSoporteFirma.getArchivo());

            } catch (FileNotFoundException e) {
                String mensaje = "Ocurrió un error al cargar al archivo." +
                     " Por favor intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización Se
     * adiciono un atributo tipoMimeDocumento para la clase de previsualización de documento y este
     * debe ser el que se consuma
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoSoporteFirma
                .getArchivo());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Remueve la imagen de la firma cargada
     *
     * @return
     *
     * @author leidy.gonzalez
     */
    /*
     * @modified by leidy.gonzalez:: #14104
     */
    public void eliminarDocumento() {
        LOGGER.debug("eliminarDocumento...INICIA");
        try {
            if (this.firmaUsuario != null && this.firmaUsuario.getDocumentoId() != null &&
                this.documentoSoporteFirma != null) {
                this.getGeneralesService().eliminarFirmaUsuario(this.firmaUsuario);

                this.getGeneralesService().
                    eliminarDocumentoFirmaMecanica(this.documentoSoporteFirma);
                this.limpiarCampos();

                String mensaje = "La firma se elimino exitosamente.";
                this.addMensajeInfo(mensaje);
                LOGGER.debug("eliminarDocumento...FINALIZA");

            } else {
                String mensaje = "Usuario sin firma cargada.";
                this.addMensajeWarn(mensaje);
            }
        } catch (Exception e) {
            this.addMensajeError("Error eliminando la firma.");
        }
    }

}
