package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EOfertaComisionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

public class OrdenComisionOfertasMB extends SNCManagedBean {

    private static final long serialVersionUID = 7665534743536070325L;

    protected static final Logger LOGGER = LoggerFactory
        .getLogger(OrdenComisionOfertasMB.class);

    /**
     * Usuario del sístema
     */
    protected UsuarioDTO loggedInUser;

    /**
     * Estructura organizacional del usuario del sistema
     */
    protected String estructuraOrganizacionalUsuario;

    /**
     * Código de la estructura organizacional del sistema
     */
    protected String estructuraOrganizacionalCodUsuario;

    /**
     * id de las regiones captura oferta que viene del árbol de tareas
     */
    protected List<Long> idRegiones;

    /**
     * Contiene las comisiones de ofertas que están en estado POR_APROBAR
     */
    protected List<ComisionOferta> comisionesPorAprobar;

    /**
     * Contiene las comisiones de ofertas seleccionadas de la tabla de comisiones por enviar a
     * aprobación o por aprobar
     */
    protected ComisionOferta[] selectedComisionesPorAprobar;

    /**
     * Comision seleccionada para edición
     */
    protected ComisionOferta selectedComisionPorAprobar;

    /**
     * es la región que se selecciona de las relacionadas con una comisión para retirar al
     * recolector de la comisión (la funcionalidad es retirar recolector, pero lo que se selecciona
     * son regiones captura oferta)
     */
    protected RegionCapturaOferta selectedRecolectorRegionCapturaOferta;

    /**
     * Guarda el estado inicial con la cual se deben cargar las comisiones
     */
    protected String estadoInicialComisiones;

    @Autowired
    protected IContextListener contextoWeb;

    /**
     * Determina si se está creando una comisión
     */
    protected boolean banderaEsCreacion;

    // --------------------------------Reportes---------------------------------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte de la orden de comisión
     */
    private ReporteDTO ordenComisionReporte;

    // ------------------ procesos
    @Autowired
    protected TareasPendientesMB tareasPendientesMB;

    protected ArrayList<Actividad> actividadesDeNodoArbol;

    /**
     * guarda las parejas id Actividad en el árbol :: id región
     */
    protected HashMap<Long, String> mapaActividadRegion;

    // --------------- GET - SET Metodos --------------- //
    public ComisionOferta[] getSelectedComisionesPorAprobar() {
        return this.selectedComisionesPorAprobar;
    }

    public void setSelectedComisionesPorAprobar(
        ComisionOferta[] selectedComisionesPorAprobar) {
        this.selectedComisionesPorAprobar = selectedComisionesPorAprobar;
    }

    public List<ComisionOferta> getComisionesPorAprobar() {
        return this.comisionesPorAprobar;
    }

    public void setComisionesPorAprobar(
        List<ComisionOferta> comisionesPorAprobar) {
        this.comisionesPorAprobar = comisionesPorAprobar;
    }

    public ComisionOferta getSelectedComisionPorAprobar() {
        return this.selectedComisionPorAprobar;
    }

    public void setSelectedComisionPorAprobar(
        ComisionOferta selectedComisionPorAprobar) {
        this.selectedComisionPorAprobar = selectedComisionPorAprobar;
    }

    public RegionCapturaOferta getSelectedRecolectorRegionCapturaOferta() {
        return this.selectedRecolectorRegionCapturaOferta;
    }

    public void setSelectedRecolectorRegionCapturaOferta(
        RegionCapturaOferta currentRecolector) {
        this.selectedRecolectorRegionCapturaOferta = currentRecolector;
    }

    public boolean isBanderaEsCreacion() {
        return this.banderaEsCreacion;
    }

    public void setBanderaEsCreacion(boolean banderaEsCreacion) {
        this.banderaEsCreacion = banderaEsCreacion;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getOrdenComisionReporte() {
        return this.ordenComisionReporte;
    }

    // ------------------------------ Metodos ------------------------------ //
    /**
     * Extrae el código de la estructura territorial del usuario, si pertenece a la cede central, le
     * setea un código por defecto
     *
     * @author christian.rodriguez
     */
    protected void extraerCodigoEstructuraOrganizacional() {

        this.estructuraOrganizacionalUsuario = this.loggedInUser
            .getDescripcionEstructuraOrganizacional();
        this.estructuraOrganizacionalCodUsuario = this.loggedInUser
            .getCodigoEstructuraOrganizacional();

        if (this.estructuraOrganizacionalCodUsuario == null) {
            this.estructuraOrganizacionalUsuario =
                Constantes.NOMBRE_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL;
            this.estructuraOrganizacionalCodUsuario =
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL;
            this.loggedInUser
                .setCodigoTerritorial(this.estructuraOrganizacionalUsuario);
        }
    }

    /**
     * Ejecutado al presionar el botón modificar comisión. Asigna la única comisión seleccionada (se
     * restringe a que la longitud de la selección sea 1) de las tablas de comisiones por enviar a
     * aprobación o por aprobar.
     *
     * @author christian.rodriguez
     * @modified pedrio.garcia 29-06-2012 Se cambió selection a multiple en ambas tablas (comisiones
     * por enviar a aprobación y por aprobar) por lo que se toma aquí la comision seleccionada para
     * edición; y queda un método comun para ambos cu
     */
    public void cargarComisionParaEdicion() {

        this.selectedComisionPorAprobar = this.selectedComisionesPorAprobar[0];
        this.actualizarRegionesComision();

        this.banderaEsCreacion = false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Actualiza las regiones asociadas a la comisión
     *
     * @author christian.rodriguez
     */
    protected void actualizarRegionesComision() {
        this.selectedComisionPorAprobar
            .setRegionCapturaOfertas(this
                .obtenerRegionesCapturaOfertaPorComision(this.selectedComisionPorAprobar
                    .getId()));
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * busca las regiones captura oferta relacionadas con una comisión. Se usa para actualizar las
     * regiones de la comisión con la que se está trabajando.
     *
     * @author pedro.garcia
     * @param comisionId
     */
    protected List<RegionCapturaOferta> obtenerRegionesCapturaOfertaPorComision(
        Long comisionId) {

        List<RegionCapturaOferta> regionesDeComision = null;

        try {
            regionesDeComision = this.getAvaluosService()
                .obtenerRegionesCapturaOfertaPorComision(comisionId);
        } catch (Exception ex) {
            LOGGER.error("error obteniendo las regiones asociadas a la comisión con id " +
                 comisionId.toString());
        }
        return regionesDeComision;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Borrar un recolector de la comisión
     *
     * @author christian.rodriguez
     */
    public void eliminarRecolectorComision() {

        ArrayList<RegionCapturaOferta> regionesAEliminar = new ArrayList<RegionCapturaOferta>();
        regionesAEliminar.add(this.selectedRecolectorRegionCapturaOferta);

        String mensaje =
            "Si elimina todos los recolectores de la comisión esta será cancelada automáticamente.";
        this.addMensajeWarn(mensaje);

        String estadoInicialComision = this.selectedComisionPorAprobar
            .getEstado();

        this.guardarComisionOferta(regionesAEliminar);

        if (this.selectedComisionPorAprobar.getEstado().equalsIgnoreCase(
            EOfertaComisionEstado.CANCELADA.getCodigo()) &&
             estadoInicialComision
                .equals(EOfertaComisionEstado.POR_APROBAR.getCodigo())) {

            this.devolverProceso(regionesAEliminar);
        }

        this.actualizarRegionesComision();

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Se llama cada vez que se necesite refrescar y cargar los datos de la tabla de comisiones por
     * aprobar
     *
     * @author christian.rodriguez
     */
    protected void cargarListaComisiones() {

        this.idRegiones = new ArrayList<Long>();
        for (Actividad actividad : this.actividadesDeNodoArbol) {
            this.idRegiones.add(actividad.getIdObjetoNegocio());
        }

        this.comisionesPorAprobar = this.getAvaluosService()
            .buscarComisionesOfertaPorEstadoYRCO(
                this.estadoInicialComisiones, this.idRegiones);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que llama al metodo de guardar la comisión, este se usa unicamente como acción del
     * botón de la GUI
     *
     * @author christian.rodriguez
     */
    public void guardarComisionOferta() {

        ArrayList<RegionCapturaOferta> regionesAEliminar = new ArrayList<RegionCapturaOferta>();

        if (this.validarFechasComision()) {
            this.guardarComisionOferta(regionesAEliminar);

            // Llama al método que genera el reporte del memorando de comisión
            // de ofertas
            this.generarReporteMemorando();

            this.cargarListaComisiones();
            this.reinicializarComisionesSeleccionadas();

        } else {
            String mensaje = "Le fecha de inicio debe ser anterior a la " +
                 "fecha de finalización de la comisión y posterior a la fecha actual.";
            this.addMensajeError(mensaje);

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Deselecciona las comisiones seleccioandas para que al refrescar la página no queden
     * activadas.
     *
     * @author christian.rodriguez
     */
    protected void reinicializarComisionesSeleccionadas() {
        this.selectedComisionPorAprobar = new ComisionOferta();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Acción ejecutada cuando se hace click en el botón "Guardar" del dialogo "Detalle de la
     * comisión". Guarda o actualiza una comisión de ofertas.
     *
     * @author christian.rodriguez
     * @param regionesAEliminar opcional que contiene las regiones a eliminar
     */
    public void guardarComisionOferta(
        ArrayList<RegionCapturaOferta> regionesAEliminar) {

        if (this.selectedComisionPorAprobar != null) {

            this.selectedComisionPorAprobar = this.getAvaluosService()
                .guardarActualizarComisionOferta(
                    this.selectedComisionPorAprobar, this.loggedInUser,
                    regionesAEliminar);

            this.actualizarRegionesComision();
            this.cargarListaComisiones();

        }
    }

    /**
     * Método que es llamado cuando se cancela una comisión, se encarga de devolver los procesos
     * asociados a cada región para que estén disponibles para otra comisión
     *
     * @author christian.rodriguez
     * @param regiones
     */
    private void devolverProceso(List<RegionCapturaOferta> regiones) {

        for (RegionCapturaOferta rco : regiones) {

            List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();

            // D: si el usuario en un investigador de marcado, la comisión la
            // aprueba el director de la
            // territorial; si es el coordinador GIT avaluos, la aprueba el
            // subdirector de catastro
            String[] rolesUsuario = this.loggedInUser.getRoles();
            ArrayList<UsuarioDTO> usuariosRolDestino = null;

            for (String rol : rolesUsuario) {
                // N: no se deben comparar con equals porque por alguna razón
                // ponene en mayúsculas los
                // roles del usuario al crearlo en el inicio de sesión
                if (rol.compareToIgnoreCase(ERol.DIRECTOR_TERRITORIAL
                    .toString()) == 0) {
                    usuariosRolDestino = (ArrayList<UsuarioDTO>) this
                        .getTramiteService()
                        .buscarFuncionariosPorRolYTerritorial(
                            this.loggedInUser
                                .getDescripcionTerritorial(),
                            ERol.INVESTIGADOR_MERCADO);
                } else if (rol.compareToIgnoreCase(ERol.SUBDIRECTOR_CATASTRO
                    .toString()) == 0) {
                    usuariosRolDestino = (ArrayList<UsuarioDTO>) this
                        .getTramiteService()
                        .buscarFuncionariosPorRolYTerritorial(
                            this.loggedInUser
                                .getDescripcionTerritorial(),
                            ERol.COORDINADOR_GIT_AVALUOS);
                }
            }

            listaUsuarios.add(usuariosRolDestino.get(0));

            String trancision =
                ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION;

            OfertaInmobiliaria ofertaBPM = new OfertaInmobiliaria();
            ofertaBPM = new OfertaInmobiliaria();
            ofertaBPM.setUsuarios(listaUsuarios);
            ofertaBPM.setTransicion(trancision);
            ofertaBPM.setTerritorial(this.estructuraOrganizacionalUsuario);
            String idActividad = this.mapaActividadRegion.get(rco.getId());

            if (idActividad != null) {
                try {
                    this.getProcesosService().avanzarActividad(idActividad,
                        ofertaBPM);
                } catch (Exception ex) {
                    LOGGER.error("Error moviendo actividad con id " +
                         idActividad + " a la " + "actividad " +
                         trancision + " : " + ex.getMessage());
                    break;
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de generar el memorando asociado a la comisión en caso de que la comisión no
     * tenga recolectores no se debe generar memorando ni mostrar el componente de memorandos
     *
     * @author christian.rodriguez
     */
    void generarReporteMemorando() {

        if (!this.selectedComisionPorAprobar.getRegionCapturaOfertas()
            .isEmpty()) {

            if (this.generarReporteMemorandoComisionOfertas()) {

                Documento memorando = this.guardarDocumentoMemorando();

                if (memorando != null) {
                    this.selectedComisionPorAprobar
                        .setOficioDocumento(memorando);
                    this.guardarComisionOferta(new ArrayList<RegionCapturaOferta>());
                } else {
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    this.addMensajeError(
                        "No se poudo guardar el documento asociado a la orden de comisión.");
                    context.addCallbackParam("error", "error");
                }
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError("No pudo generarse el documento de la orde de comisión.");
                context.addCallbackParam("error", "error");
            }
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError(
                "Debe haber seleccionado almenos una región para crear la orden de comisión.");
            context.addCallbackParam("error", "error");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Valida que las fechas de inicio de la comisión sea menor a la fecha de fin de al comisión
     *
     * @author christian.rodriguez
     * @return verdadero si se la fecha de inicio e smenor a la fecha de fin
     */
    boolean validarFechasComision() {
        if (this.selectedComisionPorAprobar != null &&
             this.selectedComisionPorAprobar.getFechaInicio() != null &&
             this.selectedComisionPorAprobar.getFechaFin() != null) {

            Date fechaActual = new Date();
            if (this.selectedComisionPorAprobar.getFechaInicio().before(
                fechaActual)) {
                return false;
            }
            if (this.selectedComisionPorAprobar.getFechaInicio().getTime() >
                this.selectedComisionPorAprobar
                    .getFechaFin().getTime()) {
                return false;
            }
        }
        return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que genera el reporte de memorando de comisión de ofertas
     *
     * @author javier.aponte
     */
    public boolean generarReporteMemorandoComisionOfertas() {

        boolean reporteGenerado = false;

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.MEMORANDO_COMISION_OFERTAS;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("COMISION_ID",
            String.valueOf(this.selectedComisionPorAprobar.getId()));
        parameters.put("ES_DIRECTOR_TERRITORIAL", "true");

        if (this.estadoInicialComisiones
            .equalsIgnoreCase(EOfertaComisionEstado.ENVIO_APROBACION
                .getCodigo())) {
            parameters.put("COPIA_USO_RESTRINGIDO", "true");
        }

        this.ordenComisionReporte = this.reportsService.generarReporte(
            parameters, enumeracionReporte, this.loggedInUser);

        reporteGenerado = true;

        return reporteGenerado;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Guarda el documento con el memorando generado según la acción hecha sobre la comisión
     *
     * @return true si pudo hacerlo
     */
    protected Documento guardarDocumentoMemorando() {

        Documento tempDocumento;
        TipoDocumento tipoDoc = null;
        Date fechaLogYRadicacion;

        Long memorandoTipoDocumentoId = ETipoDocumento.MEMORANDO_CREACION_COMISION
            .getId();

        tipoDoc = this.getGeneralesService().buscarTipoDocumentoPorId(
            memorandoTipoDocumentoId);

        if (tipoDoc == null) {
            LOGGER.error("Error: no se encontró un tipo de documento " +
                 memorandoTipoDocumentoId + " para el memorando");
            return null;
        }

        tempDocumento = new Documento();
        fechaLogYRadicacion = new Date(System.currentTimeMillis());

        // D: se arma el objeto Documento con todos los atributos posibles...
        // D: el método que guarda el documento en el gestor documental recibe
        // el nombre del archivo sin la ruta
        tempDocumento.setArchivo(this.ordenComisionReporte.getNombreReporte());
        tempDocumento.setTipoDocumento(tipoDoc);

        tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());

        tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
        tempDocumento.setUsuarioCreador(this.loggedInUser.getLogin());
        tempDocumento
            .setEstructuraOrganizacionalCod(this.estructuraOrganizacionalUsuario);
        tempDocumento.setFechaRadicacion(fechaLogYRadicacion);

        tempDocumento.setFechaLog(fechaLogYRadicacion);
        tempDocumento.setUsuarioLog(this.loggedInUser.getLogin());
        tempDocumento
            .setDescripcion("Memorando de orden de comisión de ofertas");

        try {
            // D: se guarda el documento
            String numeroComision = this.selectedComisionPorAprobar.getNumero()
                .concat(String.valueOf((new Date()).getTime()));

            DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
                .crearArgumentoCargarSolicitudAvaluoComercial(
                    numeroComision, new Date(),
                    ETipoDocumento.MEMORANDO_DE_ORDEN_DE_PRACTICA
                        .getNombre(), this.ordenComisionReporte
                        .getRutaCargarReporteAAlfresco());

            tempDocumento = this.getGeneralesService()
                .guardarDocumentosSolicitudAvaluoAlfresco(
                    this.loggedInUser, datosGestorDocumental,
                    tempDocumento);

        } catch (Exception ex) {
            LOGGER.error("error guardando Documento memorando: " +
                 ex.getMessage());
            return null;
        }

        return tempDocumento;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Calcula el tiempo de duración de la comisión restando la fecha de inicio a la fecha de fin.
     * La medida son días
     *
     * @author christian.rodriguez
     */
    public Long getDuracionComision() {

        Long duracion = 0L;

        if (this.selectedComisionPorAprobar.getFechaInicio() != null &&
             this.selectedComisionPorAprobar.getFechaFin() != null) {

            duracion = this.selectedComisionPorAprobar.getFechaFin().getTime() -
                 this.selectedComisionPorAprobar.getFechaInicio()
                    .getTime();

            duracion = duracion / (24 * 60 * 60 * 1000);

            if (duracion < 0) {
                duracion = 0L;
            }

            this.selectedComisionPorAprobar.setDuracion(new Double(duracion));

        }

        return duracion;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Calcula la fecha mínima que se puede seleccionar como fecha final de la comisión
     *
     * @author christian.rodriguez
     * @return retorna la fecha de inicio mas un día
     */
    public Date getMinFechaFin() {
        if (this.selectedComisionPorAprobar.getFechaInicio() != null) {

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.selectedComisionPorAprobar.getFechaInicio());
            cal.add(Calendar.DATE, +1);
            return cal.getTime();

        }
        return null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Retorna la fecha mínima para la comisión (fecha actual)
     *
     * @author christian.rodriguez
     * @return retorna la fecha mínima para la comisión (fecha actual)
     */
    public Date getMinFechaInicio() {
        return new Date();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Guarda en un hashmap entradas con idActividad::idRegion. Se usa para no tener que recorrer
     * varias veces la lista de actividades para obtener el id de actividad correspondiente a una
     * región cuando se esté moviendo el proceso de las regiones asociadas a una comisión
     *
     * @author pedro.garcia
     */
    protected void almacenarIdsActividadesArbol() {

        try {
            this.actividadesDeNodoArbol = (ArrayList<Actividad>) this.tareasPendientesMB
                .getListaInstanciasActividadesSeleccionadas();
        } catch (Exception ex) {
            LOGGER.error("Error obteniendo lista de actividades: " +
                 ex.getMessage());
            return;
        }

        if (this.actividadesDeNodoArbol != null &&
             !this.actividadesDeNodoArbol.isEmpty()) {
            this.mapaActividadRegion = new HashMap<Long, String>();
            for (Actividad activity : this.actividadesDeNodoArbol) {
                this.mapaActividadRegion.put(
                    new Long(activity.getIdObjetoNegocio()),
                    activity.getId());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Hace los cambios de estado de las comisiones cuando se mueve el proceso
     *
     * @author pedro.garcia
     */
    protected boolean doDatabaseStatesUpdate() {

        boolean ok = true;
        ArrayList<ComisionOferta> comisionesAActualizar = new ArrayList<ComisionOferta>();

        comisionesAActualizar.addAll(Arrays
            .asList(this.selectedComisionesPorAprobar));

        try {
            this.getAvaluosService().guardarActualizarComisionesOfertas(
                comisionesAActualizar);
        } catch (Exception ex) {
            LOGGER.error("Error actualizando las comisiones de ofertas al mover el proceso: " +
                 ex.getMessage());
            ok = false;
        }

        return ok;

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se encarga de recuperar el acta del avaluo seleccionado para ser visualizada
     *
     * @author christian.rodriguez
     */
    public void cargarDocuemntoOrdenPractica() {
        if (this.selectedComisionPorAprobar != null) {

            Documento ordenComisionDocumento = this.selectedComisionPorAprobar
                .getOficioDocumento();

            this.ordenComisionReporte = this.reportsService
                .consultarReporteDeGestorDocumental(
                    ordenComisionDocumento.getIdRepositorioDocumentos());

            if (this.ordenComisionReporte == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "La orden de comisión asociada no pudo ser cargada.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;

    }

    // end of class
}
