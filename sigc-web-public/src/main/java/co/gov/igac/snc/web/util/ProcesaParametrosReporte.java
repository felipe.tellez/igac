package co.gov.igac.snc.web.util;


import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.util.ESiNo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Procesa la naturaleza de los parametros de un reporte (requerido, deshabilitado, etc)
 *
 * @author felipe.cadena
 */
public class ProcesaParametrosReporte {


    private List<RepConfigParametroReporte> listaParametros;
    private Map<String, Boolean[]> values;





    public ProcesaParametrosReporte(List<RepConfigParametroReporte> lista){

        this.listaParametros = lista;

        values = new HashMap<String, Boolean[]>();

        for(RepConfigParametroReporte item:lista){
            values.put(generateKey(item.getReporteId(), item.getNombreParametro()), initValues(item));
        }
    }


    /**
     * Genera el identificador para cada item en el mapa
     * @param idReporte
     * @param parametro
     * @return
     */
    private String generateKey(Long idReporte, String parametro){

        return idReporte + "-" + parametro;
    }


    /**
     * Procesa los valores a guardar en el mapa
     * @param value
     * @return
     */
    private Boolean[] initValues(RepConfigParametroReporte value){

        Boolean[] itemValues = new Boolean[2];

        itemValues[0] = value.getHabilitado().equals(ESiNo.SI.getCodigo()); //habilitado
        itemValues[1] = value.getRequerido().equals(ESiNo.SI.getCodigo()); //requerido

        return itemValues;
    }

    /**
     * Retorna los valores para un reporte en particular
     * @param idReporte
     * @return
     */
    public Boolean[] getValues(Long idReporte, String parametro){
        return this.values.get(generateKey(idReporte, parametro));
    }

    public boolean isRequerido(Long idReporte, String parametro){
        return this.values.get(generateKey(idReporte, parametro))[1];
    }

    public boolean isHabilitado(Long idReporte, String parametro){
        return this.values.get(generateKey(idReporte, parametro))[0];
    }

    /**
     * Retorna las condiciones por tipo de reporte
     * @param idReporte
     * @return
     */
    public Map<String, Boolean[]> getMapForTipoReporte(Long idReporte){

        Map<String, Boolean[]> result = new HashMap<String, Boolean[]>();

        for (String key:this.values.keySet()) {

            if(key.startsWith(idReporte.toString())){
                String newKey = key.substring(key.lastIndexOf("-")+1);
                result.put(newKey, this.values.get(key));
            }
        }

        return result;


    }/**
     * Retorna las condiciones por defecto, todo deshabilitado y no requerido
     * @return
     */
    public Map<String, Boolean[]> getMapDefault(){

        Map<String, Boolean[]> result = new HashMap<String, Boolean[]>();

        Boolean[] defaultValues = {false, false};

        Long idReporte = this.listaParametros.get(0).getReporteId();

        for (String key:this.values.keySet()) {

            if(key.startsWith(idReporte.toString())){
                String newKey = key.substring(key.lastIndexOf("-")+1);
                result.put(newKey, defaultValues);
            }
        }

        return result;


    }


}
