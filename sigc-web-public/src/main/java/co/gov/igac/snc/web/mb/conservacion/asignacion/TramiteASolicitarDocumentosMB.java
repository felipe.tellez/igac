package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ETramiteDocumentoMetoComu;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author david.cifuentes
 */
@Component("tramiteASolicitarDocumentos")
@Scope("session")
public class TramiteASolicitarDocumentosMB extends ProcessFlowManager {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteASolicitarDocumentosMB.class);

    /**
     * Variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena la territorial del usuario en sesión
     */
    private String idTerritorial;

    // -------- SERVICIOS -------- //
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Managed Bean usado para consultar los detalles del predio
     */
    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    // -------- VARIABLES -------- //
    /**
     * Contador de los documentos asociados al Tramite Seleccionado
     */
    private Integer countDocumentosTramiteSeleccionado;

    /**
     * Tramite seleccionado de la lista de Actividades - Tramites a Solicitar Documentos
     */
    private Tramite tramiteSeleccionado;

    /**
     * Lista de Predios del Tramite seleccionado
     */
    private List<Predio> prediosTramiteSeleccionado;

    /**
     * Lista de Solicitantes del Tramite seleccionado
     */
    private List<SolicitanteTramite> solicitantesTramite;

    /**
     * Lista de los tipos de documentos existentes en la BD
     */
    private List<TipoDocumento> listTiposDeDocumento;

    /**
     * Tipo de documento adicional
     */
    private TipoDocumento tipoDocumentoAdicional;

    /**
     * Detalle del documento adicional
     */
    private String detalleDocumentoAdicional;

    /**
     * Lista de documentos por solicitar, se cargan todos los requeridos no aportados en forma
     * predefinida
     */
    private List<TramiteDocumentacion> documentosPorSolicitar;

    /**
     * Arreglo de documentos por solicitar seleccionados
     */
    private TramiteDocumentacion[] selectedDocumentosPorSolicitar;

    /**
     * Dirección de correo electrónico a la que se enviará la solicitud de documentos
     */
    private String email;

    /**
     * Variable booleana para saber si una solicitud de documentos se envia o no por correo
     * electrónico
     */
    private Boolean emailBool;

    /**
     * Variable booleana que me indica si un trámite ya fue radicado
     */
    private Boolean radicadoBool;

    /**
     * Variable booleana usada para indicar si la visualización del reporte de solicitud se hace
     * despues de ser radicado o antes.
     */
    private boolean vistaReporteDespuesDeRadicar;

    /**
     * Variable booleana para saber si se van a ver los documentos asociados o si se va a solicitar
     * nuevos Documentos
     */
    private Boolean verSolicitudDocsBool;

    /**
     * Variable usada para registrar el medio por el cual se solicitaron los documentos
     */
    private String medioDeSolicitudDocumentos;

    /**
     * Variable usada para registrar la persona a la que se notifico la solicitud de documentos
     */
    private String personaNotificada;

    /**
     * Variable usada para guardar las observaciones del registro de una solicitud de documentos
     */
    private String observacionNotificacion;

    /**
     * Lista de medios de solicitud de documentos
     */
    private List<Dominio> listMediosDeSolicitudDocs;

    /**
     * Oficio que se genera de la solicitud de documentos
     */
    private TramiteDocumento oficioGenerado;

    /**
     * Lista de los nombres de los tipos de documento
     */
    private List<SelectItem> itemListTiposDeDocumento = new ArrayList<SelectItem>();

    /**
     * Lista de los nombres de los medios de notificacion
     */
    private List<SelectItem> itemListMedioSolicitudDoc = new ArrayList<SelectItem>();

    /**
     * Variables usadas para registrar la notificacion de la solicitud de documentos
     */
    private Date fechaRadicado;

    /**
     * Variable para almacenar la fecha de la notificacion de solicitud de documentos
     */
    private Date fechaNotificacion;

    /**
     * Variable del numero predial seleccionado para ver su detalle
     */
    private String numeroPredial;

    /**
     * Lista de Solicitantes con emails
     */
    private List<Solicitante> solicitantesEmail;

    /**
     * Lista de Tramites documentación que fueron solicitados inicialmente pero se eliminaron en el
     * proceso de solicitud
     */
    private List<TramiteDocumentacion> documentosPorEliminar;

    /**
     * Fecha de hoy para el uso de max date en un calendar
     */
    private Date fechaHoy;

    /**
     * Lista de Solicitantes del Tramite seleccionado
     */
    private List<SolicitanteSolicitud> solicitantesSolicitud;

    /**
     * Lista de solicitantes
     */
    private List<Solicitante> solicitantes;

    //--------------Variables para el reporte ----------------------------
    /**
     * Número de radicación del trámite (Este número se busca al momento de radicar)
     */
    private String numeroRadicacion;

    /**
     * Objeto que contiene los datos del reporte de la solicitud de documentos
     */
    private ReporteDTO reporteSolicitudDocumentos;

    /**
     * Variable que almacena el documento de la solicitud de documentos.
     */
    private Documento documentoSoporte;

    private String destinoListaTareas;

    /**
     * lista de usuarios con el rol de responsables de conservación
     */
    private List<UsuarioDTO> responsablesConservacion;

    // ----- BPM
    private Actividad currentProcessActivity;

    private long idTramiteDeTrabajo;

    private SelectItem medio;

    private boolean mostrarBotonMover;

    // -------------------------//
    @PostConstruct
    public void init() {

        LOGGER.debug("Init de TramiteASolicitarDocumentosMB");

        // D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "tramiteASolicitarDocumentos");

        this.tramiteSeleccionado = new Tramite();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.idTerritorial = this.usuario.getCodigoEstructuraOrganizacional();

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        // D: se obtiene el id del trámite
        this.idTramiteDeTrabajo = this.tareasPendientesMB
            .getInstanciaSeleccionada().getIdObjetoNegocio();
        // D: se obtiene la actividad actual

        try {
            this.tramiteSeleccionado = this.getTramiteService()
                .consultarDocumentacionDeTramite(this.idTramiteDeTrabajo);
        } catch (Exception e) {
            LOGGER.error(
                "error en TramiteASolicitarDocumentosMB#init buscando el trámite seleccionado " +
                 " del árbol");
        }

        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.tramiteSeleccionado.getId());

        this.documentosPorSolicitar = new ArrayList<TramiteDocumentacion>();
        this.prediosTramiteSeleccionado = new ArrayList<Predio>();
        this.itemListMedioSolicitudDoc = new ArrayList<SelectItem>();
        this.listMediosDeSolicitudDocs = new ArrayList<Dominio>();
        this.solicitantesTramite = new ArrayList<SolicitanteTramite>();
        this.solicitantesSolicitud = new ArrayList<SolicitanteSolicitud>();
        this.solicitantes = new ArrayList<Solicitante>();
        this.emailBool = true;

        try {
            // TRÁMITE Y DOCUMENTACIÓN
            this.cargarDocumentacion();

            // PREDIOS
            this.prediosTramiteSeleccionado = this.getConservacionService()
                .buscarPrediosPorTramiteId(this.idTramiteDeTrabajo);

            // SOLICITANTES
            this.solicitantesSolicitud = this.getTramiteService()
                .obtenerSolicitantesSolicitud2(this.tramiteSeleccionado
                    .getSolicitud().getId());
            for (SolicitanteSolicitud ss : this.solicitantesSolicitud) {
                if (ss != null && ss.getSolicitante() != null) {
                    this.solicitantes.add(ss.getSolicitante());
                }
            }

            if (this.tramiteSeleccionado.getSolicitud().getTipo()
                .equals(ESolicitudTipo.AUTOAVALUO.getCodigo())) {
                this.solicitantesTramite = this.getTramiteService()
                    .obtenerSolicitantesDeTramite(idTramiteDeTrabajo);
                for (SolicitanteTramite st : this.solicitantesTramite) {
                    if (st != null) {
                        this.solicitantes.add(st.getSolicitante());
                    }
                }
            }

            // CORREOS ELECTRÓNICOS DE LOS SOLICITANTES
            this.buscarSolicitantesConEmail();

            // DOMINIOS
            this.listMediosDeSolicitudDocs = this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.TRAMITE_DOCUMENTO_METO_COMU);
            //@modified by juanfelipe.garcia :: 26-11-2013 :: Cuando el solicitante no tenga correo electrónico, 
            //                                  no debe permitir que seleccione como medio de comunicación el correo electrónico (5646).
            if (!this.listMediosDeSolicitudDocs.isEmpty()) {
                //Dominio con valor correo electrónico
                Dominio d = this.getGeneralesService().
                    buscarValorDelDominioPorDominioYCodigo(EDominio.TRAMITE_DOCUMENTO_METO_COMU,
                        ETramiteDocumentoMetoComu.CORREO_ELECTRONICO.getValor());
                for (Dominio dom : this.listMediosDeSolicitudDocs) {
                    if (dom != null) {
                        //Si los solicitantes no tienen e-mail, no se debe adicionar e-mail a la lista
                        if (dom.getId().equals(d.getId()) && this.solicitantesEmail.isEmpty()) {
                            this.emailBool = false;
                        } else {
                            medio = new SelectItem();
                            medio.setValue(dom.getValor());
                            medio.setLabel(dom.getValor());
                            this.itemListMedioSolicitudDoc.add(medio);
                        }
                    }
                }
            }

            // Se buscan los tipos de documentos que filtrados por Clase "OTRO"
            // pues estos son los que se van a utilizar
            // y de igual manera que sean generados por el sistema
            this.listTiposDeDocumento = new ArrayList<TipoDocumento>();
            this.listTiposDeDocumento = this.getGeneralesService()
                .buscarTiposDeDocumentos(
                    ETipoDocumentoClase.OTRO.toString(),
                    ESiNo.NO.getCodigo());
            if (!this.listTiposDeDocumento.isEmpty()) {
                SelectItem nombre;
                for (TipoDocumento tip : this.listTiposDeDocumento) {
                    nombre = new SelectItem();
                    nombre.setValue(tip);
                    nombre.setLabel(tip.getNombre());
                    this.itemListTiposDeDocumento.add(nombre);
                }
            }

            this.destinoListaTareas = ConstantesNavegacionWeb.LISTA_TAREAS;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(e);
        }
    }

    // -------- MÉTODOS GET Y SET --------//
    public List<SelectItem> getItemListMedioSolicitudDoc() {
        return itemListMedioSolicitudDoc;
    }

    public void setItemListMedioSolicitudDoc(
        List<SelectItem> itemListMedioSolicitudDoc) {
        this.itemListMedioSolicitudDoc = itemListMedioSolicitudDoc;
    }

    public Integer getCountDocumentosTramiteSeleccionado() {
        return countDocumentosTramiteSeleccionado;
    }

    public void setCountDocumentosTramiteSeleccionado(
        Integer countDocumentosTramiteSeleccionado) {
        this.countDocumentosTramiteSeleccionado = countDocumentosTramiteSeleccionado;
    }

    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public List<Predio> getPrediosTramiteSeleccionado() {
        return prediosTramiteSeleccionado;
    }

    public void setPrediosTramiteSeleccionado(
        List<Predio> prediosTramiteSeleccionado) {
        this.prediosTramiteSeleccionado = prediosTramiteSeleccionado;
    }

    public List<SolicitanteTramite> getSolicitantesTramite() {
        return solicitantesTramite;
    }

    public void setSolicitantesTramite(
        List<SolicitanteTramite> solicitantesTramite) {
        this.solicitantesTramite = solicitantesTramite;
    }

    public List<TipoDocumento> getListTiposDeDocumento() {
        return listTiposDeDocumento;
    }

    public void setListTiposDeDocumento(List<TipoDocumento> listTiposDeDocumento) {
        this.listTiposDeDocumento = listTiposDeDocumento;
    }

    public TipoDocumento getTipoDocumentoAdicional() {
        return tipoDocumentoAdicional;
    }

    public void setTipoDocumentoAdicional(TipoDocumento tipoDocumentoAdicional) {
        this.tipoDocumentoAdicional = tipoDocumentoAdicional;
    }

    public void setDetalleDocumentoAdicional(String detalleDocumentoAdicional) {
        this.detalleDocumentoAdicional = detalleDocumentoAdicional;
    }

    public String getDetalleDocumentoAdicional() {
        return detalleDocumentoAdicional;
    }

    public void setDocumentosPorSolicitar(
        List<TramiteDocumentacion> documentosPorSolicitar) {
        this.documentosPorSolicitar = documentosPorSolicitar;
    }

    public List<TramiteDocumentacion> getDocumentosPorSolicitar() {
        return documentosPorSolicitar;
    }

    public TramiteDocumentacion[] getSelectedDocumentosPorSolicitar() {
        return selectedDocumentosPorSolicitar;
    }

    public void setSelectedDocumentosPorSolicitar(
        TramiteDocumentacion[] selectedDocumentosPorSolicitar) {
        this.selectedDocumentosPorSolicitar = selectedDocumentosPorSolicitar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailBool() {
        return emailBool;
    }

    public void setEmailBool(Boolean emailBool) {
        this.emailBool = emailBool;
    }

    public boolean isMostrarBotonMover() {
        return mostrarBotonMover;
    }

    public void setMostrarBotonMover(boolean mostrarBotonMover) {
        this.mostrarBotonMover = mostrarBotonMover;
    }

    // --------------------------------------------------------------------------------------------------
    public Boolean getRadicadoBool() {

        radicadoBool = false;
        if (tramiteSeleccionado != null &&
             this.tramiteSeleccionado.getTramiteDocumentacions() != null &&
             !this.tramiteSeleccionado.getTramiteDocumentacions()
                .isEmpty()) {

            for (TramiteDocumentacion td : this.tramiteSeleccionado
                .getTramiteDocumentacions()) {

                if (td.getRequerido().equals(ESiNo.SI.getCodigo()) &&
                     td.getAdicional().equals(ESiNo.SI.getCodigo())) {

                    if (td.getDocumentoSoporte() != null &&
                         td.getDocumentoSoporte().getNumeroRadicacion() != null &&
                         !td.getDocumentoSoporte().getNumeroRadicacion()
                            .trim().isEmpty()) {

                        if (td.getDocumentoSoporte().getFechaRadicacion() != null) {
                            this.fechaRadicado = td.getDocumentoSoporte()
                                .getFechaRadicacion();
                        }
                        return radicadoBool = true;
                    }
                }
            }
        }
        // Debido a que la documentación solicitada una vez se ha radicado se
        // vuelve requerida, y su estado sería el mismo que un trámite inicial,
        // se verifica antes con una variable booleana si ya se realizó el paso
        // por el botón radicar.
        if (this.vistaReporteDespuesDeRadicar) {
            this.radicadoBool = true;
        }
        return radicadoBool;
    }

    // --------------------------------------------------------------------------------------------------
    public void setRadicadoBool(Boolean radicadoBool) {
        this.radicadoBool = radicadoBool;
    }

    public String getMedioDeSolicitudDocumentos() {
        return medioDeSolicitudDocumentos;
    }

    public void setMedioDeSolicitudDocumentos(String medioDeSolicitudDocumentos) {
        this.medioDeSolicitudDocumentos = medioDeSolicitudDocumentos;
    }

    public List<Dominio> getListMediosDeSolicitudDocs() {
        return listMediosDeSolicitudDocs;
    }

    public void setListMediosDeSolicitudDocs(
        List<Dominio> listMediosDeSolicitudDocs) {
        this.listMediosDeSolicitudDocs = listMediosDeSolicitudDocs;
    }

    public boolean isVistaReporteDespuesDeRadicar() {
        return vistaReporteDespuesDeRadicar;
    }

    public void setVistaReporteDespuesDeRadicar(boolean vistaReporteDespuesDeRadicar) {
        this.vistaReporteDespuesDeRadicar = vistaReporteDespuesDeRadicar;
    }

    public void setOficioGenerado(TramiteDocumento oficioGenerado) {
        this.oficioGenerado = oficioGenerado;
    }

    public TramiteDocumento getOficioGenerado() {
        return oficioGenerado;
    }

    public Boolean getVerSolicitudDocsBool() {
        return verSolicitudDocsBool;
    }

    public void setVerSolicitudDocsBool(Boolean verSolicitudDocsBool) {
        this.verSolicitudDocsBool = verSolicitudDocsBool;
    }

    public List<SelectItem> getItemListTiposDeDocumento() {
        return itemListTiposDeDocumento;
    }

    public void setItemListTiposDeDocumento(
        List<SelectItem> itemListTiposDeDocumento) {
        this.itemListTiposDeDocumento = itemListTiposDeDocumento;
    }

    public String getPersonaNotificada() {
        return personaNotificada;
    }

    public void setPersonaNotificada(String personaNotificada) {
        this.personaNotificada = personaNotificada;
    }

    public Date getFechaRadicado() {
        return fechaRadicado;
    }

    public void setFechaRadicado(Date fechaRadicado) {
        this.fechaRadicado = fechaRadicado;
    }

    public String getObservacionNotificacion() {
        return observacionNotificacion;
    }

    public void setObservacionNotificacion(String observacionNotificacion) {
        this.observacionNotificacion = observacionNotificacion;
    }

    public ConsultaPredioMB getConsultaPredioMB() {
        return consultaPredioMB;
    }

    public void setConsultaPredioMB(ConsultaPredioMB consultaPredioMB) {
        this.consultaPredioMB = consultaPredioMB;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public List<Solicitante> getSolicitantesEmail() {
        return solicitantesEmail;
    }

    public void setSolicitantesEmail(List<Solicitante> solicitantesEmail) {
        this.solicitantesEmail = solicitantesEmail;
    }

    public Date getFechaNotificacion() {
        return fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    public List<TramiteDocumentacion> getDocumentosPorEliminar() {
        return documentosPorEliminar;
    }

    public void setDocumentosPorEliminar(
        List<TramiteDocumentacion> documentosPorEliminar) {
        this.documentosPorEliminar = documentosPorEliminar;
    }

    public Date getFechaHoy() {
        fechaHoy = new Date(System.currentTimeMillis());
        return fechaHoy;
    }

    public void setFechaHoy(Date fechaHoy) {
        this.fechaHoy = fechaHoy;
    }

    public String getDestinoListaTareas() {
        return destinoListaTareas;
    }

    public void setDestinoListaTareas(String destinoListaTareas) {
        this.destinoListaTareas = destinoListaTareas;
    }

    public List<SolicitanteSolicitud> getSolicitantesSolicitud() {
        return solicitantesSolicitud;
    }

    public void setSolicitantesSolicitud(
        List<SolicitanteSolicitud> solicitantesSolicitud) {
        this.solicitantesSolicitud = solicitantesSolicitud;
    }

    public void setSolicitantes(List<Solicitante> solicitantes) {
        this.solicitantes = solicitantes;
    }

    public List<Solicitante> getSolicitantes() {
        return solicitantes;
    }

    public ReporteDTO getReporteSolicitudDocumentos() {
        return reporteSolicitudDocumentos;
    }

    public void setReporteSolicitudDocumentos(ReporteDTO reporteSolicitudDocumentos) {
        this.reporteSolicitudDocumentos = reporteSolicitudDocumentos;
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getIdTerritorial() {
        return idTerritorial;
    }

    public void setIdTerritorial(String idTerritorial) {
        this.idTerritorial = idTerritorial;
    }

    public Documento getDocumentoSoporte() {
        return documentoSoporte;
    }

    public void setDocumentoSoporte(Documento documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    // -------- MÉTODOS --------//
    public SelectItem getMedio() {
        return medio;
    }

    public void setMedio(SelectItem medio) {
        this.medio = medio;
    }

    /**
     * Método que busca los mails de los solicitantes
     */
    public void buscarSolicitantesConEmail() {

        this.solicitantesEmail = new ArrayList<Solicitante>();

        if (this.emailBool) {
            if (!this.solicitantes.isEmpty()) {
                for (Solicitante s : this.solicitantes) {
                    if (s.getCorreoElectronico() != null &&
                         !s.getCorreoElectronico().trim().isEmpty()) {
                        this.solicitantesEmail.add(s);
                    }
                }
            }
        }
    }

    // -----------------------------------------------//
    /**
     * Método que carga la documentación (TramiteDocumentacion) asociada al trámite
     */
    public void cargarDocumentacion() {
        Long idTramite = this.tramiteSeleccionado.getId();
        this.documentosPorSolicitar = new ArrayList<TramiteDocumentacion>();
        this.tramiteSeleccionado = this.getTramiteService()
            .consultarDocumentacionDeTramite(idTramite);
        if (!tramiteSeleccionado.getTramiteDocumentacions().isEmpty()) {
            this.countDocumentosTramiteSeleccionado = 0;
            for (TramiteDocumentacion td : this.tramiteSeleccionado
                .getTramiteDocumentacions()) {
                if (ESiNo.NO.getCodigo().equals(td.getAportado()) &&
                     ESiNo.NO.getCodigo().equals(td.getAdicional())) {
                    this.documentosPorSolicitar.add(td);
                } else if (ESiNo.SI.getCodigo().equals(td.getAportado()) &&
                     ESiNo.SI.getCodigo().equals(td.getRequerido())) {
                    this.countDocumentosTramiteSeleccionado++;
                }
            }
        }
        this.reiniciarVariablesDocumentacion();
    }

    // -----------------------------------------------//
    /**
     * Método que adiciona un tramiteDocumentacion a la lista de documentos asociados
     */
    public void adicionarDocumentoALaSolicitud() {

        TramiteDocumentacion nuevoTramiteDocumentacion = new TramiteDocumentacion();

        try {
            if (this.tipoDocumentoAdicional == null) {
                this.addMensajeError("El Tipo de documento es un campo obligatorio");
                return;
            }

            if (this.tipoDocumentoAdicional.getId().equals(ETipoDocumento.OTRO.getId()) &&
                this.detalleDocumentoAdicional.trim().isEmpty()) {
                this.addMensajeError(
                    "Para este tipo de documento por favor debe especificar el detalle");
                return;
            }

            for (TramiteDocumentacion td : this.documentosPorSolicitar) {
                if (td.getTipoDocumento().getId() == this.tipoDocumentoAdicional.getId()) {
                    this.addMensajeError(
                        "Ya se ha solicitado este tipo de documento, seleccione uno diferente, o haga la corrección para el tipo de documento ya seleccionado.");
                    return;
                }
            }

            // ---------------------------------------- //
            // Adición de un nuevo TramiteDocumentacion //
            // ---------------------------------------- //
            if (this.tramiteSeleccionado != null) {
                nuevoTramiteDocumentacion
                    .setTramite(this.tramiteSeleccionado);
            }
            nuevoTramiteDocumentacion.setAportado(ESiNo.NO.getCodigo());
            nuevoTramiteDocumentacion.setDetalle(this
                .getDetalleDocumentoAdicional());
            nuevoTramiteDocumentacion.setFechaLog(new Date(System
                .currentTimeMillis()));
            nuevoTramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            nuevoTramiteDocumentacion.setRequerido(ESiNo.NO.getCodigo());
            nuevoTramiteDocumentacion.setCantidadFolios(0);
            nuevoTramiteDocumentacion
                .setTipoDocumento(this.tipoDocumentoAdicional);
            nuevoTramiteDocumentacion
                .setUsuarioLog(this.usuario.getLogin());

            this.documentosPorSolicitar.add(nuevoTramiteDocumentacion);
            this.addMensajeInfo("Documento adicionado");
            reiniciarVariablesDocumentacion();

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al adicionar el documento... ");
            LOGGER.error(e.getMessage(), e);
        }
    }

    // -----------------------------------------------//
    /**
     * Método que elimina tramiteDocumentacion seleccionado de la lista de documentos asociados al
     * tramite
     */
    public void eliminarDocumentoDeLaSolicitud() {

        /** Verifica que documentos se pueden eliminar */
        TramiteDocumentacion element;

        if (selectedDocumentosPorSolicitar == null || selectedDocumentosPorSolicitar.length < 1) {
            this.addMensajeWarn("Por favor seleccione un documento a eliminar");
            return;
        }

        try {
            this.documentosPorEliminar = new ArrayList<TramiteDocumentacion>();
            if (this.selectedDocumentosPorSolicitar.length > 0) {
                for (int i = 0; i < this.selectedDocumentosPorSolicitar.length; i++) {
                    element = new TramiteDocumentacion();
                    element = this.selectedDocumentosPorSolicitar[i];
                    if (this.selectedDocumentosPorSolicitar[i].getRequerido()
                        .equals(ESiNo.SI.getCodigo())) {
                        this.selectedDocumentosPorSolicitar = null;
                        this.addMensajeError("No se pueden eliminar los documentos predefinidos");
                        return;
                    } else {
                        int auxCount = -1;
                        for (int j = 0; j < this.documentosPorSolicitar.size(); j++) {
                            if (this.documentosPorSolicitar.get(j).getTipoDocumento().equals(
                                element.getTipoDocumento())) {
                                auxCount = j;
                                break;
                            }
                        }
                        if (auxCount >= 0) {
                            this.documentosPorSolicitar.remove(auxCount);
                        }
                        this.documentosPorEliminar.add(element);
                    }
                }
                this.addMensajeInfo("Documento retirado!");
                this.reiniciarVariablesDocumentacion();
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al eliminar documentos asociados.");
            LOGGER.error(e.getMessage(), e);
        }
    }

    // -----------------------------------------------//
    /**
     * Método que guarda los TramiteDocumentacion solicitados en la BD. Dádo que hasta éste momento
     * no se ha radicado, los trámites guardados tienen en las columnas Adicional y Requerido el
     * valor 'NO'. Al momento de radicar, el valor de estas ccolumnas cambia a 'SI' para indicar que
     * es un requerido adicional.
     */
    public void guardarDocumentosSolicitados() {
        List<TramiteDocumentacion> documentosEliminarEnBD = new ArrayList<TramiteDocumentacion>();
        try {
            this.getTramiteService()
                .actualizarTramiteDocumentacion(this.documentosPorSolicitar);
            this.documentosPorSolicitar = null;
            if (this.documentosPorEliminar != null &&
                 !this.documentosPorEliminar.isEmpty()) {
                for (TramiteDocumentacion td : this.documentosPorEliminar) {
                    if (td.getId() != null) {
                        documentosEliminarEnBD.add(td);
                    }
                }
                this.getTramiteService()
                    .eliminarTramitesDocumentacion(documentosEliminarEnBD);
                this.documentosPorEliminar = null;
            }
            this.cargarDocumentacion();
            this.generarReporteDeSolicitudDocumentos();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al guardar los documentos solicitados.");
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------//
    /**
     * Método que consulta los detalles de un predio por su id usando el managed bean
     * ConsultaPredioMB
     */
    public void buscarPredioPorNumeroPredial() {
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb
            .getManagedBean("consultaPredio");
        mb001.cleanAllMB();
        if (this.numeroPredial != null) {
            Predio auxPredio = this.getConservacionService()
                .getPredioByNumeroPredial(this.numeroPredial);
            this.consultaPredioMB.setSelectedPredioId(auxPredio.getId());
        }
    }

    // -----------------------------------------------//
    /**
     * genera el reporte que contiene el el reporte de solicitud de documentos Hay dos opciones para
     * generarlo, dependiendo de sí el documento ya fue radicado, en el caso en que ya se radicó
     * primero radica el memorando y genera el reporte ya con el número de radicación, en el caso
     * contrario genera el reporte sin el número de radicado Este método se llama desde el método de
     * radicación.(Paso 2 al Radicar)
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez:: #16709:: 05/04/2016 Se agrega parametro firma de usuario a
     * reporte de solicitud d edocumentos faltantes
     */
    public void generarReporteDeSolicitudDocumentos() throws Exception {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.SOLICITUD_DOCUMENTOS_FALTANTES;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", "" + this.tramiteSeleccionado.getId());

        UsuarioDTO responsableConservacion = null;
        UsuarioDTO directorTerritorial = null;
        List<UsuarioDTO> directoresTerritoriales = new ArrayList<UsuarioDTO>();
        String documentoFirma;
        FirmaUsuario firma = null;

        if (this.responsablesConservacion == null) {
            this.responsablesConservacion = (List<UsuarioDTO>) this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);
        }

        if (this.responsablesConservacion != null &&
             !this.responsablesConservacion.isEmpty() &&
             this.responsablesConservacion.get(0) != null) {
            responsableConservacion = this.responsablesConservacion.get(0);
        }

        if (responsableConservacion != null &&
             responsableConservacion.getNombreCompleto() != null) {
            parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                responsableConservacion.getNombreCompleto());
        }

        if (this.numeroRadicacion != null) {
            parameters.put("NUMERO_RADICADO", this.numeroRadicacion);
        }

        if (directorTerritorial == null) {
            directoresTerritoriales = (List<UsuarioDTO>) this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.DIRECTOR_TERRITORIAL);
        }

        if (directoresTerritoriales != null && !directoresTerritoriales.isEmpty() &&
             directoresTerritoriales.get(0) != null) {
            directorTerritorial = directoresTerritoriales.get(0);
        }

        if (directorTerritorial != null &&
             directorTerritorial.getNombreCompleto() != null) {
            parameters.put("NOMBRE_DIRECTOR_TERRITORIAL",
                directorTerritorial.getNombreCompleto());
        }

        if (directorTerritorial != null && directorTerritorial.getNombreCompleto() != null) {

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                directorTerritorial.getNombreCompleto());

            if (firma != null && directorTerritorial.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                parameters.put("FIRMA_USUARIO", documentoFirma);
            } else {
                this.addMensajeInfo("El tramite no tiene firma de usuario asociada.");
            }
        }

        this.reporteSolicitudDocumentos = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);
    }

    // -----------------------------------------------//
    /**
     * Método que guarda un documento en Alfresco creando su documento y tramiteDocumento asociados
     * en la base de datos. (Paso 3 al Radicar)
     *
     * @throws Exception
     */
    public void guardarSolicitudDeDocumentos() throws Exception {

        // Creación del Tramite Documento
        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        // Creación del Documento y se asocia al Tramite documento
        Documento documento = new Documento();

        tramiteDocumento.setTramite(this.tramiteSeleccionado);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            // Dado que aún no se ha guardado el documento en Alfresco creamos
            // su dirección como una cadena vacía.
            documento.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);

            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(
                ETipoDocumento.OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES
                    .getId());

            documento.setTipoDocumento(tipoDocumento);
            documento.setEstado(EDocumentoEstado.SIN_REGISTRAR_SOLICITUD
                .getCodigo());
            documento.setTramiteId(this.tramiteSeleccionado.getId());
            if (this.tramiteSeleccionado.getPredioId() != null) {
                documento.setPredioId(this.tramiteSeleccionado.getPredioId());
            }
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setNumeroRadicacion(this.numeroRadicacion);
            documento.setFechaRadicacion(this.fechaRadicado);
            documento.setFechaDocumento(this.fechaRadicado);
            documento.setEstructuraOrganizacionalCod(this.usuario
                .getCodigoEstructuraOrganizacional());

            // Nombre del archivo
            if (this.reporteSolicitudDocumentos != null) {
                documento.
                    setArchivo(this.reporteSolicitudDocumentos.getRutaCargarReporteAAlfresco());
            }

            // Subo el archivo a Alfresco y actualizo el documento
            if (this.tramiteSeleccionado.isQuinta() ||
                 this.tramiteSeleccionado.getSolicitud()
                    .isViaGubernativa()) {
                this.documentoSoporte = this.getTramiteService()
                    .guardarDocumentoSinPredio(this.usuario, documento);
            } else {

                // Se debe garantizar que el documento lleve un predio asociado.
                if (documento.getPredioId() == null) {
                    Long predioIdBuscado = null;
                    if (this.prediosTramiteSeleccionado != null && !this.prediosTramiteSeleccionado.
                        isEmpty()) {
                        predioIdBuscado = this.prediosTramiteSeleccionado.get(0).getId();
                        documento.setPredioId(predioIdBuscado);

                    }
                    // Lanzar excepción si no se tiene predio aún cuando el trámite no es
                    // una mutación de quinta ni una solicitud de via gubernativa.
                    if (predioIdBuscado == null) {
                        throw (new Exception("ERROR : No se tiene un predio asociado al trámite"));
                    }
                }

                this.documentoSoporte = this.getTramiteService()
                    .guardarDocumento(this.usuario, documento);
            }

            if (this.documentoSoporte != null &&
                 this.documentoSoporte.getIdRepositorioDocumentos() != null &&
                 !this.documentoSoporte.getIdRepositorioDocumentos()
                    .trim().isEmpty()) {
                tramiteDocumento.setDocumento(this.documentoSoporte);
                tramiteDocumento
                    .setIdRepositorioDocumentos(this.documentoSoporte
                        .getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                this.oficioGenerado = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumento);
            }
        } catch (Exception e) {
            LOGGER.debug("Error al guardar la solicitud de documentos (Paso 3 de 5)... ");
            LOGGER.error(e.getMessage(), e);
            // Lanzar la excepción al método padre (radicar).
            this.addMensajeError("Error al guardar el reporte de la solicitud de documentos.");
            throw (e);
        }
    }

    // ---------------------------------------------------------//
    /**
     * Método que actualiza los documentos solicitados. Genera un documento con el número de
     * radicación para cada TramiteDocumentacion de la solicitud, y hace un set de la fecha de
     * solicitud y de la documentación solicitada como requerida adicional. (Paso 4 al Radicar)
     */
    public void actualizarDocumentosSolicitados() throws Exception {

        Documento documentoSoporte;
        try {
            for (TramiteDocumentacion td : this.documentosPorSolicitar) {
                documentoSoporte = new Documento();
                documentoSoporte.setTipoDocumento(td.getTipoDocumento());
                documentoSoporte
                    .setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                documentoSoporte.setEstado(EDocumentoEstado.SIN_CARGAR
                    .getCodigo());
                documentoSoporte.setTramiteId(this.tramiteSeleccionado.getId());
                documentoSoporte.setBloqueado(ESiNo.NO.getCodigo());
                documentoSoporte.setUsuarioCreador(this.usuario.getLogin());
                documentoSoporte.setUsuarioLog(usuario.getLogin());
                documentoSoporte.setFechaLog(new Date(System
                    .currentTimeMillis()));
                documentoSoporte.setNumeroRadicacion(numeroRadicacion);
                documentoSoporte.setFechaRadicacion(fechaRadicado);
                documentoSoporte.setFechaDocumento(new Date(System
                    .currentTimeMillis()));
                documentoSoporte.setEstructuraOrganizacionalCod(this.usuario
                    .getCodigoEstructuraOrganizacional());

                Documento documentoGuardado = this.getGeneralesService()
                    .guardarDocumento(documentoSoporte);
                if (documentoGuardado != null) {
                    td.setDocumentoSoporte(documentoGuardado);
                }
                // Set de la documentación solicitada como requerida adicional,
                // y su fecha de solicitud
                td.setFechaSolicitudDocumento(new Date(System
                    .currentTimeMillis()));
                td.setRequerido(ESiNo.SI.getCodigo());
                td.setAdicional(ESiNo.SI.getCodigo());

            }
            this.getTramiteService()
                .actualizarTramiteDocumentacion(this.documentosPorSolicitar);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al actualizar los documentos a solicitar (Paso 4 de 5)... ");
            LOGGER.error(e.getMessage(), e);
            // Lanzar la excepción al método padre (radicar).
            this.addMensajeError("Error al actualizar el estado de los documentos solicitados.");
            throw (e);
        }

    }

    // -----------------------------------------------//
    /**
     * Método que envia un correo con el reporte de solicitud de documentos como adjunto, a los
     * solicitantes que tengan email. Este envio se hace al momento de Radicar siempre que se haya
     * seleccionado la opcion enviar correo (Paso 5 al Radicar)
     */
    public void enviarCorreo() {

        String asunto = ConstantesComunicacionesCorreoElectronico.ASUNTO_SOLICITUD_DOCUMENTOS;
        String contenido = ConstantesComunicacionesCorreoElectronico.CONTENIDO_SOLICITUD_DOCUMENTOS;
        String[] adjuntos = null;
        String[] titulos;

        if (this.reporteSolicitudDocumentos != null) {
            adjuntos = new String[]{this.reporteSolicitudDocumentos.getRutaCargarReporteAAlfresco()};
        }
        titulos = new String[]{
            ConstantesComunicacionesCorreoElectronico.ARCHIVO_ADJUNTO_SOLICITUD_DOCUMENTOS};

        try {
            for (Solicitante s : this.solicitantesEmail) {
                if (s.getCorreoElectronico() != null &&
                     !s.getCorreoElectronico().trim().isEmpty()) {
                    this.getGeneralesService().enviarCorreo(
                        s.getCorreoElectronico(), asunto, contenido,
                        adjuntos, titulos);
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico (Paso 5 de 5)... ");
            LOGGER.error(e.getMessage(), e);
            // Lanzar la excepción al método padre (radicar).
            this.addMensajeWarn(
                "No se pudo enviar el correo electrónico. Por favor seleccione una opción de envio diferente al momento de registrar la solicitud de documentos.");
        }
    }

    // ---------------------------------------//
    // -------------- RADICAR ----------------//
    // ---------------------------------------//
    /**
     * Método ejecutado al momento de radicar la solicitud de documentos faltantes de revisión
     * autoavalúo y mutaciones
     */
    public void radicar() {
        if (!this.documentosPorSolicitar.isEmpty()) {
            try {

                this.fechaRadicado = new Date(System.currentTimeMillis());
                // 1. Generación del número de Radicación en correspondencia
                // (sticker de radicación)
                this.numeroRadicacion = this.obtenerNumeroRadicado();

                if (this.numeroRadicacion == null) {
                    throw new Exception(
                        "No se pudo generar el número de radicación");
                }

                // 2. Generación del reporte con el número de radicación
                this.generarReporteDeSolicitudDocumentos();

                // 3. Guardar la solicitud de documentos en Alfresco, generando
                // el TramiteDocumento y un Documento que guarden
                // mi Reporte de la solicitud.
                this.guardarSolicitudDeDocumentos();

                // 4. Actualización de los documentos solicitados, este método
                // hace un set
                // de la fecha de solicitud y de la documentación solicitada
                // como requerida adicional.
                this.actualizarDocumentosSolicitados();

                // 5. Envio del correo electrónico con el reporte adjunto a los
                // solicitantes
                if (this.emailBool && !this.solicitantesEmail.isEmpty()) {
                    this.enviarCorreo();
                }

                // 6. Se actualiza el trámite indicando que ahora tiene
                // documentación pendiente,
                // e indicando que su estado está ahora en
                // EN_ESPERA_DE_COMPLETAR_DOC
                if (this.tramiteSeleccionado != null) {
                    this.tramiteSeleccionado.setDocumentacionCompleta(ESiNo.NO
                        .getCodigo());
                    this.getTramiteService()
                        .actualizarTramite(this.tramiteSeleccionado, this.usuario);
                }

                this.addMensajeInfo("Se radicó la solicitud de documentos correctamente");
                this.vistaReporteDespuesDeRadicar = true;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError("No se radicó la solicitud de documentos.");
            }
        }
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     *
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     */
    private String obtenerNumeroRadicado() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(
            ETipoDocumento.OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES.
                getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Solicitud documentos faltantes"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getDireccion()); // Direccion destino
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    // -----------------------------------------------//
    /**
     * action del botón "aceptar" en la pantalla de registro de solicitud de documentos
     *
     * Método que actualiza la solicitud de documentos (TramiteDocumento) se ejecuta al momento de
     * hacer el registro de la solicitud de documentos.
     *
     * @modified pedro.garcia
     */
    public String actualizarSolicitudDeDocumentos() {

        mostrarBotonMover = false;
        Documento doc = new Documento();

        if (this.observacionNotificacion.length() > Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO) {
            this.addMensajeError("El tamaño del texto del motivo no debe ser superior a " +
                Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO + " caracteres");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return null;
        }

        try {
            if (this.oficioGenerado == null ||
                 this.oficioGenerado.getIdRepositorioDocumentos() == null) {
                this.oficioGenerado = this.getTramiteService()
                    .buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(
                        this.idTramiteDeTrabajo);
            }

            if (this.oficioGenerado != null) {
                this.oficioGenerado
                    .setMetodoNotificacion(this.medioDeSolicitudDocumentos);
                if (this.fechaNotificacion.after(new Date(System
                    .currentTimeMillis()))) {
                    this.addMensajeError("La fecha ingresada es inválida!");
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return null;
                }
                this.oficioGenerado
                    .setFechaNotificacion(this.fechaNotificacion);
                this.oficioGenerado
                    .setPersonaNotificacion(this.personaNotificada);
                this.oficioGenerado
                    .setObservacionNotificacion(this.observacionNotificacion);

                if (this.oficioGenerado.getDocumento() != null &&
                     this.oficioGenerado.getDocumento().getId() != null) {
                    doc = this.getGeneralesService()
                        .buscarDocumentoPorId(this.oficioGenerado
                            .getDocumento().getId());
                    doc.setEstado(EDocumentoEstado.SOLICITUD_DOCS_REGISTRADA
                        .getCodigo());
                    this.documentoSoporte = this.getGeneralesService()
                        .guardarDocumento(doc);
                    this.oficioGenerado.setDocumento(this.documentoSoporte);
                }

                this.oficioGenerado = this.getTramiteService()
                    .actualizarTramiteDocumento(this.oficioGenerado);
            } else {
                this.addMensajeError("Error al recuperar la solicitud de documentos guardada.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return null;
            }

            // Debido a que los documentos solicitados no deben tener un número ni fecha de radicado,
            // se actualizan, quitando dichos valores.
            // Se hace aquí debido a que es la última instancia antes de avanzar el proceso.
            List<Documento> documentosActualizar = new ArrayList<Documento>();
            boolean auxBool = false;
            for (TramiteDocumentacion td : this.tramiteSeleccionado
                .getTramiteDocumentacions()) {

                if (td.getRequerido().equals(ESiNo.SI.getCodigo()) &&
                     td.getAdicional().equals(ESiNo.SI.getCodigo())) {

                    if (td.getDocumentoSoporte() != null) {
                        td.getDocumentoSoporte().setNumeroRadicacion(new String());
                        td.getDocumentoSoporte().setFechaRadicacion(null);
                        documentosActualizar.add(td.getDocumentoSoporte());
                    }
                }
            }

            if (documentosActualizar != null && !documentosActualizar.isEmpty()) {
                auxBool = this.getGeneralesService().actualizarListaDeDocumentos(
                    documentosActualizar);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al Registrar la solicitud de documentos... ");
            LOGGER.error(e.getMessage(), e);
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            if (this.oficioGenerado == null) {
                this.addMensajeError("Error al recuperar la solicitud de documentos guardada.");
            }
            return null;
        }

        this.addMensajeInfo("Se realizó el registro de la solicitud de documentos correctamente");
        mostrarBotonMover = true;
        return null;
    }

    public String moverProceso() {
        try {
            this.forwardThisProcess();
            this.tareasPendientesMB.init();

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al mover el proceso");
            LOGGER.error(e.getMessage(), e);
        }
        return ConstantesNavegacionWeb.INDEX;
    }

    // ---------------------------------------------------------//
    /**
     * Método que consulta la solicitud de Documentos guardada en Alfresco
     */
    public void consultarEnAlfrescoDocumentoRadicado() {

        try {
            if (this.oficioGenerado == null ||
                 this.oficioGenerado.getIdRepositorioDocumentos() == null) {
                this.oficioGenerado = this
                    .getTramiteService()
                    .buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(
                        this.idTramiteDeTrabajo);
            }
            if (this.oficioGenerado != null &&
                 this.oficioGenerado.getDocumento() != null &&
                 this.oficioGenerado.getIdRepositorioDocumentos() != null) {

                this.reporteSolicitudDocumentos = this.reportsService.
                    consultarReporteDeGestorDocumental(
                        this.oficioGenerado.getIdRepositorioDocumentos());

            } else {
                this.addMensajeError("Error al recuperar la solicitud de documentos guardada.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error al recuperar la solicitud de documentos de Alfresco.");
            LOGGER.error(e.getMessage(), e);
            if (this.oficioGenerado == null) {
                this.addMensajeError("Error al recuperar la solicitud de documentos guardada.");
            }
        }
    }

    // -----------------------------------------------//
    /**
     * Método que limpia las variables usadas para adicionar TramiteDocumentacion
     */
    public void reiniciarVariablesDocumentacion() {
        this.detalleDocumentoAdicional = "";
        this.tipoDocumentoAdicional = null;
        this.selectedDocumentosPorSolicitar = null;

    }

    // -----------------------------------------------//
    // Método que controla eventos en la interfaz para hacer updates en algunos
    // elementos //
    public void cerrar(org.primefaces.event.CloseEvent event) {
        this.reiniciarVariablesDocumentacion();

        this.documentosPorSolicitar = new ArrayList<TramiteDocumentacion>();
        for (TramiteDocumentacion td : this.tramiteSeleccionado
            .getTramiteDocumentacions()) {
            if (td.getAportado().equals(ESiNo.NO.getCodigo()) &&
                 td.getAdicional().equals(ESiNo.NO.getCodigo())) {
                this.documentosPorSolicitar.add(td);
            }
        }
    }

    // -------- PROCESS --------//
    /**
     * No hay necesidad de validar cosa alguna. Si se permite llegar hasta el punto donde se debe
     * mover el proceso, todo está bien
     *
     * @author pedro.garcia
     */
    @Implement
    public boolean validateProcess() {
        return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();

        usuariosActividad = new ArrayList<UsuarioDTO>();
        messageDTO = new ActivityMessageDTO();

        messageDTO.setActivityId(this.currentProcessActivity.getId());

        // D: OJO: el trámite puede haber llegado aquí desde varias actividades,
        // pero siempre se va a
        // la actividad de completar documentos, por lo tanto es responsabilidad
        // del que maneje esa pantalla
        // verificar a dónde debe enviarlo (ej: si llegó desde la de trámite
        // devuelto, puede que se
        // le haya desasignado un ejecutor allí, por lo tanto hay que devolverlo
        // a esa actividad)
        processTransition = ProcesoDeConservacion.ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA;
        observaciones = "El trámite pasa a la actividad de completar documentos.";

        // D: se debe dirigir la actividad al ejecutor del trámite
        if (this.tramiteSeleccionado.getFuncionarioEjecutor() != null) {
            usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                this.tramiteSeleccionado.getFuncionarioEjecutor());
        } else {
            // Si el trámite no tiene un funcionario ejecutor asignado,
            // cuando se complete la documentación que se está solicitando
            // el trámite avanzará de actividad y se le asignará un ejecutor,
            // por tal razón se pasa el usuario actual debido a que es necesario
            // tener un usuario para avanzar el proceso, y en el caso de uso
            // de completar documentación solicitada se avanzará a asignar tramites.
            usuarioDestinoActividad = this.usuario;
        }

        usuariosActividad.add(usuarioDestinoActividad);
        messageDTO.setComment(observaciones);
        // D: según alejandro.sanchez la transición que importa es la que se
        // define en la
        // solicitud catastral; sin embargo, por si acaso se hace también en el
        // messageDTO
        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);

        //solicitudCatastral.setUsuarios(usuariosActividad);
        List<ActividadUsuarios> actividadUsuarios = new LinkedList<ActividadUsuarios>();
        actividadUsuarios.add(new ActividadUsuarios(processTransition, usuariosActividad));

        solicitudCatastral.setActividadesUsuarios(actividadUsuarios);
        messageDTO.setSolicitudCatastral(solicitudCatastral);
        messageDTO.setUsuarioActual(this.usuario);
        this.setMessage(messageDTO);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        // Al mover el trámite a la actividad ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA, 
        // se debe actualizar el estado del trámite en la BD a EN_ESPERA_DE_COMPLETAR_DOC.
        // D: david.cifuentes
        this.tramiteSeleccionado
            .setEstado(ETramiteEstado.EN_ESPERA_DE_COMPLETAR_DOC.getCodigo());

        TramiteEstado te = new TramiteEstado(ETramiteEstado.EN_ESPERA_DE_COMPLETAR_DOC.getCodigo(),
            this.tramiteSeleccionado, this.usuario.getLogin());
        this.getTramiteService().actualizarTramite(tramiteSeleccionado, this.usuario);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * en el caso en que el llamado al BPM se debe hacer desde un botón que no está definido en
     * alguno de los template de procesos (porque por ejemplo hay al mismo tiempo dos botones que
     * mueven la actividad a diferentes transiciones), se debe hacer el llamado a los métodos como
     * si se estuviera implementando alguno de los métodos que avanzan el proceso del ProcessMB
     *
     * @author pedro.garcia
     */
    private void forwardThisProcess() {

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();

            super.forwardProcess();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     *
     * @author pedro.garcia
     */
    public String cerrarPaginaPrincipal() {

        UtilidadesWeb.removerManagedBean("tramiteASolicitarDocumentos");
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    // -----------------------------------------------//
    /**
     * Método ejecutado al cerrar la ventana de solicitar documentación, para reiniciar las
     * variables de la misma.
     *
     * @author david.cifuentes
     */
    public void cerrarSolicitarDocumentacion() {
        LOGGER.debug("tramiteASolicitarDocumentos#cerrarSolicitarDocumentacion");
        this.reiniciarVariablesDocumentacion();

        this.documentosPorSolicitar = new ArrayList<TramiteDocumentacion>();
        for (TramiteDocumentacion td : this.tramiteSeleccionado
            .getTramiteDocumentacions()) {
            if (td.getAportado().equals(ESiNo.NO.getCodigo()) &&
                 td.getAdicional().equals(ESiNo.NO.getCodigo())) {
                this.documentosPorSolicitar.add(td);
            }
        }
    }

    // end of class
}
