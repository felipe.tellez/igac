package co.gov.igac.snc.web.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.web.mb.generales.ProcessMB;

public abstract class ProcessFlowManager extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 6857408900585092920L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProcessFlowManager.class);

    private ActivityMessageDTO message;
    private List<ActivityMessageDTO> masiveMessages;

    /**
     * Método que debe ser implementado por cada managed bean para validar si se puede o no avanzar
     * en el proceso. Usado por managed beans que tienen vínculo directo con el proceso (creación,
     * movimiento).
     *
     * @return Retorna true si el método valida exitosamente.
     * @author fabio.navarrete
     */
    public abstract boolean validateProcess();

    /**
     * Método que pone el mensaje usado para el avance del proceso.
     *
     * @param message
     */
    public abstract void setupProcessMessage();

    /**
     * método en el que se implementará la parte de base de datos que hay que hacer en cuanto a
     * actualiza estados (de los trámites) y demás para reflejar el movimiento en el bpm
     */
    public abstract void doDatabaseStatesUpdate();

    /**
     * Método que registra el managed bean actual encargado de mover el proceso para llamar a los
     * métodos de validación, ejecución de información lógica y configuración del mensaje para
     * enviar al servidor de procesos.
     */
    public void registerCurrentManagedBean(ProcessFlowManager managedBean, String nombreMB) {
        ProcessMB processMB = (ProcessMB) UtilidadesWeb.getManagedBean("process");
        processMB.setCurrentMB(managedBean);
        processMB.setNombreMB(nombreMB);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de avanzar el proceso. El proceso se mueve a partir de un mensaje recibido.
     * Se requiere este mensaje para realizar el avance en el flujo. El avance se realizará o bien
     * con la lista de mensajes o bien de forma individual.
     *
     * @author fabio.navarrete
     */
    public void forwardProcess() {
        LOGGER.debug("ProcessFlowManager#avanzarProceso");

        String numeroRadicacionTramite, idActividad;

        numeroRadicacionTramite = "sin definir";
        idActividad = "sin definir";

        try {
            if (this.masiveMessages != null && !this.masiveMessages.isEmpty()) {
                //Avance actividades masivas
                for (ActivityMessageDTO messageDTO : this.masiveMessages) {
                    numeroRadicacionTramite = messageDTO.getSolicitudCatastral().
                        getNumeroRadicacion();
                    idActividad = messageDTO.getActivityId();
                    if (messageDTO.getUsuarioActual() != null) {
                        getProcesosService().avanzarActividad(messageDTO.getUsuarioActual(),
                            messageDTO.getActivityId(),
                            messageDTO.getSolicitudCatastral());
                    } else {
                        if (messageDTO.getUsuarioActual() != null) {
                            getProcesosService().avanzarActividad(messageDTO.getUsuarioActual(),
                                messageDTO.getActivityId(),
                                messageDTO.getSolicitudCatastral());
                        } else {
                            getProcesosService().avanzarActividad(messageDTO.getActivityId(),
                                messageDTO.getSolicitudCatastral());
                        }
                    }
                }
            } else {
                //Avance actividades individuales
                idActividad = this.message.getActivityId();
                numeroRadicacionTramite = this.message.getSolicitudCatastral().getNumeroRadicacion();

                if (this.message.getUsuarioActual() != null) {
                    getProcesosService().avanzarActividad(this.message.getUsuarioActual(),
                        this.message.getActivityId(),
                        this.message.getSolicitudCatastral());
                } else {
                    getProcesosService().avanzarActividad(this.message.getUsuarioActual(),
                        this.message.getActivityId(),
                        this.message.getSolicitudCatastral());
                }
            }
        } catch (Exception e) {
            LOGGER.error("error avanzando actividad: " + e.getMessage(), e);
            throw SNCWebServiceExceptions.EXCEPCION_0008.getExcepcion(LOGGER, e, "unkown",
                numeroRadicacionTramite, idActividad);
        }
    }
//--------------------------------------------------------------------------------------------------

    public void setMessage(ActivityMessageDTO message) {
        this.message = new ActivityMessageDTO();
        this.message = message;
    }

    public ActivityMessageDTO getMessage() {
        return this.message;
    }

    public List<ActivityMessageDTO> getMasiveMessages() {
        return masiveMessages;
    }

    /**
     * Utilizar este método para setear la lista que avanza los procesos masivos.
     *
     * @param masiveMessages
     */
    public void setMasiveMessages(List<ActivityMessageDTO> masiveMessages) {
        this.masiveMessages = masiveMessages;
    }

}
