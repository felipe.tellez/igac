package co.gov.igac.snc.web.mb;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.locator.SNCServiceLocator;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component("login")
@Scope("session")
public class LoginMB {

    @Autowired
    private IContextListener contexto;
    private static AuthenticationManager am = new SampleAuthenticationManager();
    private String usuario;
    private String contrasena;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginMB.class);

    public LoginMB() {
        /*
         * Exception ex = (Exception)
         * FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
         * .get(AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY);
         *
         * if (ex != null)
         *
         * FacesContext.getCurrentInstance().addMessage(null,new
         * FacesMessage(FacesMessage.SEVERITY_ERROR, ex .getMessage(), ex.getMessage())); */
    }

    public void login() throws java.io.IOException {

        /* FacesContext.getCurrentInstance().getExternalContext()
         * .redirect("j_spring_security_check?j_username=" + userId
         *
         * + "&j_password=" + password); */
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(usuario, contrasena);
            Authentication result = am.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
        } catch (AuthenticationException e) {
            LOGGER.error(e.getMessage());
            System.out.println("Authentication failed: " + e.getMessage());
        }
    }

    public void logout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.invalidateSession();
        LOGGER.info("Sesion cerrada");

        // return "login";
    }

    /**
     * Método para cerrar la sesión cuando se bloquea el sistema.
     *
     * @author felipe.cadena
     */
    public void logoutIdle() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.invalidateSession();
        UsuarioDTO user = MenuMB.getMenu().getUsuarioDto();
        //this.getGeneralesService().registrarLogout(user.getLogin(), EMotivoLogout.SESION_EXPIRADA.getValor());

    }

    /**
     * Se necesita obtener el servicio para persistir el registro.
     *
     * @author felipe.cadena
     * @return
     */
    private IGenerales getGeneralesService() {
        IGenerales service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getGeneralesService();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}

class SampleAuthenticationManager implements AuthenticationManager {

    static final List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();

    static {
        AUTHORITIES.add(new GrantedAuthorityImpl("ROLE_USER"));
    }

    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        if (auth.getName().equals(auth.getCredentials())) {
            return new UsernamePasswordAuthenticationToken(auth.getName(),
                auth.getCredentials(), AUTHORITIES);
        }
        throw new BadCredentialsException("Bad Credentials");
    }
}
