/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace las veces de managed bean para la consulta de trámites de un predio
 *
 * @author pedro.garcia
 */
@Component("consultaTramitesPredio")
@Scope("session")
public class ConsultaTramitesPredioMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 6381451457858693320L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaTramitesPredioMB.class);

    /**
     * predio del que se ven los detalles de trámites
     */
    private Predio predioForTramites;

    /**
     * lista de trámites pendientes del predio
     */
    private ArrayList<Tramite> tramitesPendientes;

    /**
     * lista de trámites que se han realizado sobre el predio
     */
    private ArrayList<Tramite> tramitesRealizados;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDocumentoWaterMark;

    /**
     * Trámite seleccionado en la tabla de trámites
     *
     * @return
     */
    private Tramite tramiteSeleccionado;

    // ------------------ services ------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

//---------------    methods   ----------------------------
    public ArrayList<Tramite> getTramitesPendientes() {
        return this.tramitesPendientes;
    }

    public void setTramitesPendientes(ArrayList<Tramite> tramitesPendientes) {
        this.tramitesPendientes = tramitesPendientes;
    }

    public ArrayList<Tramite> getTramitesRealizados() {
        return this.tramitesRealizados;
    }

    public void setTramitesRealizados(ArrayList<Tramite> tramitesRealizados) {
        this.tramitesRealizados = tramitesRealizados;
    }

    public Predio getPredioForTramites() {
        return this.predioForTramites;
    }

    public void setPredioForTramites(Predio predioForTramites) {
        this.predioForTramites = predioForTramites;
    }

    public ReporteDTO getReporteDocumentoWaterMark() {
        return reporteDocumentoWaterMark;
    }

    public void setReporteDocumentoWaterMark(ReporteDTO reporteDocumentoWaterMark) {
        this.reporteDocumentoWaterMark = reporteDocumentoWaterMark;
    }

    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    //--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaTramitesPredioMB");

        //D: se obtiene la referencia al mb que maneja los detalles del predio
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");

        /**
         * C: ahora se hacen las consultas por id del predio N: ver método
         * ConsultaPredioMB#getSelectedPredioId para entender de dónde se obtiene el id
         */
        Long predioSeleccionadoId = mb001.getSelectedPredioId();
        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioSeleccionadoId != null) {
            this.predioForTramites = new Predio();
            if (!esPredioOrigen) {
                this.getPredioById(predioSeleccionadoId);
            } else {
                this.predioForTramites = mb001.getSelectedPredio1();
                getTramitesbyPredioId(this.predioForTramites.getId());
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * se define este método solo para mantener el estándar de tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * También se hacen las consultas de los trámites pendientes y realizados
     *
     * @author pedro.garcia
     * @param predioId
     */
    private void getPredioById(Long predioId) {

        this.predioForTramites = this.getConservacionService().obtenerPredioPorId(predioId);

        getTramitesbyPredioId(predioId);

    }

    /**
     * Método get que consulta la resolución en alfresco y realiza el llamado al método que adiciona
     * una marca de agua a ésta.
     *
     * @author david.cifuentes
     * @modified javier.aponte
     */
    public void cargarUrlDocumentoConMarcaDeAgua() {

        if (this.tramiteSeleccionado.getResultadoDocumento() != null &&
             this.tramiteSeleccionado.getResultadoDocumento().getIdRepositorioDocumentos() != null) {

            this.reporteDocumentoWaterMark = this.reportsService.consultarReporteDeGestorDocumental(
                this.tramiteSeleccionado.getResultadoDocumento().getIdRepositorioDocumentos());

        }
    }

    /**
     * Método que consulta los tramites pendientes y realizados bajo el id del predio.
     *
     * @author juan.cruz
     * @param predioId
     */
    public void getTramitesbyPredioId(Long predioId) {
        this.tramitesPendientes =
            (ArrayList<Tramite>) this.getTramiteService().buscarTramitesPendientesDePredio(predioId);

        this.tramitesRealizados =
            (ArrayList<Tramite>) this.getTramiteService().
                buscarTramitesFinalizadosDePredio(predioId);
    }

//end of class
}
