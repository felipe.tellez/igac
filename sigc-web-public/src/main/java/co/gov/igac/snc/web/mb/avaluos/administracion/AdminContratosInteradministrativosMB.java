package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.Consignacion;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean del caso de uso 008 de avalúos
 *
 * @author christian.rodriguez
 */
@Component("adminContratosInteradministrativos")
@Scope("session")
public class AdminContratosInteradministrativosMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdminContratosInteradministrativosMB.class);

    /**
     * ManagedBean del caso de uso 89
     */
    private AdministracionAdicionesContratoMB adminAdicionesContratoMB;

    /**
     * ManagedBean del caso de uso 88
     */
    private AdminAvaluosAsociadosAContratoMB adminAvaluosAsociadosContratoMB;

    /**
     * ManagedBean del caso de uso 87
     */
    private ConsultaAvaluosAprobadosMB consultaAvaluosAprobadosMB;

    /**
     * ManagedBean del caso de uso 86
     */
    private ConsultaConsignacionAnticipoMB consultaConsignacionAnticipoMB;

    /**
     * Entidad solicitante del contrato
     */
    private Solicitante entidadSolicitante;

    /**
     * Almacena el contrato actualmente en edición
     */
    private ContratoInteradministrativo contratoSeleccionado;

    /**
     * Almacena la Consignacion actualmente en edición
     */
    private Consignacion consignacionSeleccionada;

    /**
     * Texto que se va a mostrar en el dialogo de confirmación del guardado del contrato. Varia
     * según si se está editando un contrato o creando uno
     */
    private String mensajeConfirmacionGuardar;

    /**
     * Constantes con los posibles labels que puede tener el botón de guardar dependiendo de si se
     * está creando o editando un contrato
     */
    private final String LABEL_BTN_GUARDAR_ADICION =
        "¿Está seguro de almacenar el presente contrato, " +
         "recuerde que una vez lo almacene quedará asignado como interventor?";
    private final String LABEL_BTN_GUARDAR_EDICION =
        "¿Está seguro de actualizar el presente contrato?";

    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Valor sumatoria de los datos 'Valor Adición' de cada una de las adiciones asociadas al
     * contrato. Inicialmente, cuando no existen adiciones tomará el valor de cero (0) para el dato
     * de salida 'Total Adiciones Contrato'.
     */
    private Double valorTotalAdicionesContrato;

    /**
     * Valor sumatoria de los datos 'Valor Honorarios IGAC' de los avalúos aprobados asociados al
     * contrato, incrementados en el porcentaje de IVA parametrizado en el sistema, cuando la
     * empresa pertenece al régimen común. Inicialmente, cuando no se han aprobados avalúos
     * asociados al contrato será cero (0) para el dato de salida 'Valor Ejecutado'
     */
    private Double valorEjecutado;

    /**
     * sumatoria de los datos 'Valor Cotizado_Estimado' de los avalúos en trámite asociados al
     * contrato, incrementados en el porcentaje de IVA parametrizado en el sistema, cuando la
     * empresa pertenece al régimen común. Inicialmente, cuando no existen avalúos en tramite será
     * cero (0) para el dato de salida 'Valor Comprometido en Ejecución'.
     */
    private Double valorCompromentidoEjecucion;

    /**
     * Número de avalúos asociados al trámite que se encuentran en estado EN_TRAMITE
     */
    private int numeroAvaluosEnTramite;

    /**
     * Número de avalúos asociados al trámite que se encuentran en estado APROBADO
     */
    private int numeroAvaluosAprobados;

    // --------------------------------Banderas---------------------------------
    /**
     * Se usa para representar el atributo acta de liquidación de la clase
     * {@link ContratoInteradministrativo}
     */
    public boolean banderaActaLiquidacion;

    // ----------------------------Métodos SET y GET----------------------------
    public Solicitante getEntidadSolicitante() {
        return this.entidadSolicitante;
    }

    public void setEntidadSolicitante(Solicitante entidadSolicitante) {
        this.entidadSolicitante = entidadSolicitante;
    }

    public ContratoInteradministrativo getContratoSeleccionado() {
        return this.contratoSeleccionado;
    }

    public void setContratoSeleccionado(ContratoInteradministrativo contratoSeleccionado) {
        this.contratoSeleccionado = contratoSeleccionado;
    }

    public String getMensajeConfirmacionGuardar() {
        return this.mensajeConfirmacionGuardar;
    }

    public Double getValorTotalAdicionesContrato() {
        return this.valorTotalAdicionesContrato;
    }

    public Double getValorEjecutado() {
        return this.valorEjecutado;
    }

    public Double getValorCompromentidoEjecucion() {
        return this.valorCompromentidoEjecucion;
    }

    public int getNumeroAvaluosEnTramite() {
        return this.numeroAvaluosEnTramite;
    }

    public int getNumeroAvaluosAprobados() {
        return this.numeroAvaluosAprobados;
    }

    public Consignacion getConsignacionSeleccionada() {
        return this.consignacionSeleccionada;
    }

    public void setConsignacionSeleccionada(Consignacion consignacionSeleccionada) {
        this.consignacionSeleccionada = consignacionSeleccionada;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaActaLiquidacion() {
        return this.banderaActaLiquidacion;
    }

    public void setBanderaActaLiquidacion(boolean banderaActaLiquidacion) {
        this.banderaActaLiquidacion = banderaActaLiquidacion;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on AdministracionContratosInteradministrativosMB init");
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.mensajeConfirmacionGuardar = this.LABEL_BTN_GUARDAR_ADICION;

    }

    // --------------------------------------------------------------------------
    /**
     * Listener del botón de guardar. Método que guarda/actualiza el contrato seleccionado
     *
     * @author christian.rodriguez
     */
    public void guardarContrato() {

        if (this.contratoSeleccionado != null) {

            if (this.validarDatosContrato()) {

                if (this.banderaActaLiquidacion) {
                    this.contratoSeleccionado.setActaLiquidacion(ESiNo.SI.getCodigo());
                } else {
                    this.contratoSeleccionado.setActaLiquidacion(ESiNo.NO.getCodigo());
                }

                this.contratoSeleccionado = this.getAvaluosService()
                    .guardarActualizarContratoInteradminsitrativo(this.contratoSeleccionado,
                        this.entidadSolicitante, this.usuario);

                if (this.contratoSeleccionado != null) {
                    this.addMensajeInfo("El contrato con id " + this.contratoSeleccionado.getId() +
                         " fue guardado satisfactoriamente.");
                } else {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("No se pudo guardar el contrato.");
                }
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Válida que todos los datos del contrato sean válidos
     *
     * @author christian.rodriguez
     * @return true si los datos del contrato son válidos, false si no
     */
    private boolean validarDatosContrato() {

        if (this.validarFechasContrato()) {
            return true;
        } else {
            return false;
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Válida que las fechas del contrato seán validas. Revisa que la de inicio sea menor a la de
     * finalización y que la fecha de inicio no sea menro a al de consignación del anticipo
     *
     * @author christian.rodriguez
     * @return true si las fechas son válidas, false en otro caso
     */
    private boolean validarFechasContrato() {

        if (this.contratoSeleccionado != null) {

            RequestContext context = RequestContext.getCurrentInstance();

            Date fechaFin = this.contratoSeleccionado.getFechaFin();
            Date fechaInicio = this.contratoSeleccionado.getFechaInicio();

            if (fechaFin == null && fechaInicio == null) {
                return true;
            }

            Date fechaConsignacion = null;

            if (this.contratoSeleccionado.getConsignaciones() != null) {
                this.contratoSeleccionado.getConsignaciones().get(0).getFecha();
            }

            if (!fechaFin.after(fechaInicio)) {

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "administrarContratorInteradministrativosForm:fechaFinContrato",
                    "Fecha fin contrato: La fecha de fin debe ser posterior a la fecha de inicio.");
                return false;
            }

            if (fechaInicio != null && fechaConsignacion == null) {

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "Fecha de inicio: Debe ser diligenciada una vez se haya registrado la fecha de consignación de anticipo");
                return false;
            }

            if (fechaInicio != null && fechaConsignacion != null &&
                 fechaInicio.before(fechaConsignacion)) {

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "administrarContratorInteradministrativosForm:fechaInicioContrato",
                    "Fecha inicio contrato: Debe ser posterior o igual a la fecha de consignación del anticipo.");
                return false;
            }

            if (fechaInicio == null && fechaConsignacion != null) {

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "administrarContratorInteradministrativosForm:fechaInicioContrato",
                    "Fecha inicio contrato: Debe ser ingresada, porque ya existe la fecha de consignación.");
                return false;
            }
        }
        return true;
    }

    // --------------------------------------------------------------------------
    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado por el caso de uso 52
     *
     * @author christian.rodriguez
     */
    public void cargarDesdeCU52(Solicitante entidadSolicitante) {

        this.entidadSolicitante = entidadSolicitante;
        this.contratoSeleccionado = new ContratoInteradministrativo();
        this.mensajeConfirmacionGuardar = this.LABEL_BTN_GUARDAR_ADICION;

        ProfesionalAvaluo interventor = new ProfesionalAvaluo();
        interventor = this
            .getAvaluosService()
            .consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion(
                this.usuario.getLogin(),
                this.usuario.getIdentificacion());
        this.contratoSeleccionado.setInterventor(interventor);

        this.banderaActaLiquidacion = false;

        this.valorTotalAdicionesContrato = 0.0;
        this.valorEjecutado = 0.0;
        this.valorCompromentidoEjecucion = 0.0;
        this.numeroAvaluosEnTramite = 0;
        this.numeroAvaluosAprobados = 0;
    }

    // --------------------------------------------------------------------------
    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado por el caso de uso 53
     *
     * @author christian.rodriguez
     * @param contratoAEditar {@link ContratoInteradministrativo} que se desea editar
     * @param entidadSolicitante {@link Solicitante} entidad solicitante del contrato
     */
    public void cargarDesdeCU53(Solicitante entidadSolicitante,
        ContratoInteradministrativo contratoAEditar) {

        this.entidadSolicitante = entidadSolicitante;
        this.contratoSeleccionado = contratoAEditar;
        this.mensajeConfirmacionGuardar = this.LABEL_BTN_GUARDAR_EDICION;

        if (contratoAEditar.getActaLiquidacion().equals(ESiNo.SI.getCodigo())) {
            this.banderaActaLiquidacion = true;
        } else {
            this.banderaActaLiquidacion = false;
        }

        this.calcularValorTotalAdiciones();
        this.calcularValorEjecutado();
        this.calcularValorComprometidoEjecucion();
        this.calcularNumeroAvaluosEnTramite();
        this.calcularNumeroAvaluosAprobados();

    }

    // --------------------------------------------------------------------------
    /**
     * Método que consulta el número de avalúos de un contrato que están en estado en tramite
     *
     * @author christian.rodriguez
     */
    private void calcularNumeroAvaluosEnTramite() {

        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();
        filtro.setEstadoTramiteAvaluo(ETramiteEstado.EN_PROCESO.getCodigo());

        List<Avaluo> avaluosEnTramite = this.getAvaluosService().consultarAvaluosDeContrato(
            this.contratoSeleccionado.getId(), filtro);

        if (avaluosEnTramite != null) {
            this.numeroAvaluosEnTramite = avaluosEnTramite.size();
        } else {
            this.numeroAvaluosEnTramite = 0;
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Método que consulta el número de avalúos de un contrato que están en estado aprobado
     *
     * @author christian.rodriguez
     */
    private void calcularNumeroAvaluosAprobados() {

        ArrayList<Avaluo> avaluosAprobados = null;
        FiltroDatosConsultaAvaluo filtro;

        filtro = new FiltroDatosConsultaAvaluo();

        filtro.setAprobado(ESiNo.SI.getCodigo());
        avaluosAprobados = (ArrayList<Avaluo>) this.getAvaluosService()
            .consultarAvaluosDeContrato(this.contratoSeleccionado.getId(),
                filtro);

        if (avaluosAprobados != null) {
            this.numeroAvaluosAprobados = avaluosAprobados.size();
        } else {
            this.numeroAvaluosAprobados = 0;
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Método que calcula el valor total de adiciones para el contrato seleccionado.
     *
     * @author christian.rodriguez
     */
    private void calcularValorTotalAdiciones() {
        this.valorTotalAdicionesContrato = this.getAvaluosService()
            .calcularTotalAdicionesPorIdContratoInteradministrativo(
                this.contratoSeleccionado.getId());
    }

    // --------------------------------------------------------------------------
    /**
     * Método que calcula el valor ejecutado del contrato seleccionado
     *
     * @author christian.rodriguez
     */
    private void calcularValorEjecutado() {
        this.valorEjecutado = this.getAvaluosService()
            .calcularValorEjecutadoPAContrato(
                this.contratoSeleccionado.getId());

    }

    // --------------------------------------------------------------------------
    /**
     * Listener que calcula el valor comprometido en ejecución. Se llama al cerrar la ventana de
     * administración de avalúos asociados al contrato
     *
     * @author christian.rodriguez
     */
    public void calcularValorComprometidoEjecucion() {
        this.valorCompromentidoEjecucion = this.getAvaluosService()
            .calcularValorComprometidoEjecucionPorIdContratoInteradministrativo(
                this.contratoSeleccionado.getId());

    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga los objetos que pudieron haber sido cambiados despues de realizar
     * adiciones al contrato. Se llama al cargar la ventana para editar un contrato y al cerrar el
     * dialogo de administrar adiciones al contrato
     *
     * @author christian.rodriguez
     */
    public void cargarValoresDespuesDeAdiciones() {

        this.calcularValorTotalAdiciones();
        this.contratoSeleccionado = this
            .getAvaluosService()
            .obtenerContratoInteradministrativoPorIdConEntidad(this.contratoSeleccionado.getId());
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga las consignaciones asocaidas al contrato seleccionado. Se usa para
     * ejecutar el caso de uso CU86. Si no se cuenta con consignaciones de anticipo se muestra una
     * advertencia al usuario
     *
     * @author christian.rodriguez
     */
    public void cargarConsignacionesAnticipos() {

        if (this.contratoSeleccionado != null) {

            this.consultaConsignacionAnticipoMB = (ConsultaConsignacionAnticipoMB) UtilidadesWeb
                .getManagedBean("consultaConsignacionAnticipo");
            this.consultaConsignacionAnticipoMB.cargarDesdeCU08(this.contratoSeleccionado,
                this.entidadSolicitante);

            if (this.consultaConsignacionAnticipoMB.getContratoSeleccionado() == null ||
                 this.consultaConsignacionAnticipoMB.getContratoSeleccionado() == null ||
                 this.consultaConsignacionAnticipoMB.getContratoSeleccionado()
                    .getConsignaciones().isEmpty()) {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeWarn("El contrato no cuenta con consignaciones de anticipo.");
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que llama el caso de uso de administrar adiciones a un contrato
     *
     * @author christian.rodriguez
     */
    public void cargarAdicionesContrato() {

        if (this.contratoSeleccionado != null) {
            if (this.valorTotalAdicionesContrato != 0.0) {
                this.adminAdicionesContratoMB = (AdministracionAdicionesContratoMB) UtilidadesWeb
                    .getManagedBean("administracionAdicionesContrato");
                this.adminAdicionesContratoMB.cargarDesdeCU008(this.contratoSeleccionado);
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeWarn("El contrato no cuenta con adiciones.");
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que llama el caso de uso de administrar avalúos asociados al contrato
     *
     * @author christian.rodriguez
     */
    public void cargarAvaluosAsociadosContrato() {
        if (this.contratoSeleccionado != null) {
            if (this.numeroAvaluosEnTramite != 0) {
                this.adminAvaluosAsociadosContratoMB =
                    (AdminAvaluosAsociadosAContratoMB) UtilidadesWeb
                        .getManagedBean("adminAvaluosAsociadosAContrato");
                this.adminAvaluosAsociadosContratoMB.cargarDesdeCU008(this.contratoSeleccionado);
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeWarn("El contrato no cuenta con avalúos en trámite.");
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que determina si el campo de entrada debe estar habilitado. El dato de entrada "Acta
     * Liquidación" deberá habilitarse cuando exista el dato "fecha terminación contrato" y dicha
     * fecha sea menor a la fecha actual del sistema.
     *
     * @author christian.rodriguez
     * @return true si el campo debe ser habilitado, false si no
     */
    public boolean isActaLiquidacionHabilitada() {

        if (this.contratoSeleccionado != null &&
             this.contratoSeleccionado.getFechaTerminacion() != null) {

            Calendar fechaActual = Calendar.getInstance();

            int anioActual = fechaActual.get(Calendar.YEAR);
            int diaActual = fechaActual.get(Calendar.DAY_OF_YEAR);

            Calendar fechaFinContrato = Calendar.getInstance();
            fechaFinContrato.setTime(this.contratoSeleccionado.getFechaTerminacion());

            int anioFinContrato = fechaFinContrato.get(Calendar.YEAR);
            int diaFinContrato = fechaFinContrato.get(Calendar.DAY_OF_YEAR);

            if ((anioFinContrato <= anioActual) && (diaFinContrato < diaActual)) {
                return true;
            }

        }

        return false;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Listener que carga el MB de consulta de avaluos de contrato (cuando se lega desde un
     * contrato)
     *
     * @author pedro.garcia
     */
    public void cargarConsultaAvaluosDeContrato() {

        if (this.contratoSeleccionado != null) {

            if (this.numeroAvaluosAprobados != 0) {
                this.consultaAvaluosAprobadosMB = (ConsultaAvaluosAprobadosMB) UtilidadesWeb
                    .getManagedBean("consultaAvaluosAprobados");
                this.consultaAvaluosAprobadosMB.cargarDesdeCU008(this.contratoSeleccionado);

            } else {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeWarn("El contrato no cuenta con avalúos aprobados.");
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Listener que carga el MB de consulta de avaluos de contrato cuando se llega desde una
     * consignación
     *
     * @author pedro.garcia
     */
    public void cargarConsultaAvaluosDeConsignacion() {

        if (this.consignacionSeleccionada != null) {
            this.consultaAvaluosAprobadosMB = (ConsultaAvaluosAprobadosMB) UtilidadesWeb
                .getManagedBean("consultaAvaluosAprobados");
            this.consultaAvaluosAprobadosMB.cargarDesdeCU086(this.consignacionSeleccionada);
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Método que determina si el botón de guardar debe estar habilitado.Se habilitará cuando el
     * dato "Acta Liquidación" del contrato no esté seleccionado y el usuario autenticado
     * corresponda con el interventor del contrato en consulta. Inicialmente, cuando no existe
     * información del contrato, el sistema debe habilitar la opción "Aceptar" cuando el usuario
     * autenticado se encuentre dentro de la lista de funcionarios de posibles interventores
     *
     * @author christian.rodriguez
     * @return true si el botón debe ser habilitado, false si no
     */
    public boolean isBotonGuardarHabilitado() {

        if (this.contratoSeleccionado != null) {

            if (contratoSeleccionado.getId() == null) {
                return true;
            } else {
                if (!this.usuario.getNombreCompleto().equals(
                    this.contratoSeleccionado.getInterventor()
                        .getNombreCompleto()) ||
                     !this.usuario.getIdentificacion().equals(
                        this.contratoSeleccionado.getInterventor()
                            .getNumeroIdentificacion())) {
                    return false;
                }
                if (this.banderaActaLiquidacion) {
                    return false;
                }
                return true;
            }

        } else {
            return false;
        }

    }

}
