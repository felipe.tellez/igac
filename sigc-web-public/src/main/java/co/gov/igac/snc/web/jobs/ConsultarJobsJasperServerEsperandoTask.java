package co.gov.igac.snc.web.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Tarea que se encarga de finalizar los jobs pendientes de procesar en SNC (Estas tareas son
 * manejadas por un pool de instancias asociados a una cola administrada por spring en la capa web)
 *
 * @author javier.aponte
 *
 */
public class ConsultarJobsJasperServerEsperandoTask extends AbstractLocator implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = -7332773578521079455L;
    public static Logger LOGGER = LoggerFactory.getLogger(
        ConsultarJobsJasperServerEsperandoTask.class);

    private ProductoCatastralJob job;

    /**
     *
     * @param job a ejecutar
     */
    public ConsultarJobsJasperServerEsperandoTask(ProductoCatastralJob job) {
        this.job = job;
    }

    /**
     * Método encargado de enviar la ejecución del job pendiente
     */
    public void run() {
        LOGGER.debug("Run: verificarEjecucionJobJasperServerPendientesEnSNC" +
             "job con id: " + job.getId());
        this.getGeneralesService().verificarEjecucionJobJasperServerPendientesEnSNC(job.getId());
    }

}
