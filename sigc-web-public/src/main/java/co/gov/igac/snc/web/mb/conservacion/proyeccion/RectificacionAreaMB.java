/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.conservacion.RectificacionComplementacionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.tramite.gestion.RecuperacionTramitesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.LinkedList;

/**
 * MB para el caso de uso CU-NP-CO-188 Rectificación de área
 *
 * @cu CU-SA-AC-85
 * @author felipe.cadena
 */
@Component("rectificarArea")
@Scope("session")
public class RectificacionAreaMB extends SNCManagedBean {

    private static final long serialVersionUID = 60454957865497112L;
    private static final Logger LOGGER = LoggerFactory.getLogger(RectificacionAreaMB.class);
    /**
     * Usuario en sesión
     */
    private UsuarioDTO usuario;
    /**
     * tramite asociado a la rectificación
     */
    private Tramite tramite;

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    @Autowired
    private ProyectarConservacionMB proyectarConservacionMB;
    /**
     * Listado de predios que se deben modificar en el trámite de rectificación
     */
    private List<Predio> prediosTramite;

    /**
     * Listado de predios asociaron previamente al trámite
     */
    private List<TramiteDetallePredio> tramiteDetallePredios;
    /**
     * Tipos de documento aceptados para la modificación de linderos.
     */
    private List<SelectItem> tiposDocumentoJustificacion;
    /**
     * Id Tipo de documento seleccionado para la justificación
     */
    private long tipoDocumentoJustificacion;
    /**
     * Tipos de documento aceptados para la modificación de linderos.
     */
    private List<Documento> documentosJustificacion;
    /**
     * Documento de justificación previamente asociado.
     */
    private TramiteDocumento documentosJustificacionPrevio;
    /**
     * Bandera para determinar si se deben modificar lo linderos.
     */
    private boolean banderaModificarLinderos;
    /**
     * Bandera para determinar si se deben asociar predios al trámite
     */
    private boolean banderaAgregarPredios;
    /**
     * Bandera para saber cuando se pueden eliminar predios
     */
    private boolean banderaEliminarPredios;
    /**
     * bandera para determinar si existe una incosistencia geografica en los predios seleccionados
     * para realcionar al tramite
     */
    private boolean banderaExistenInconsistencias;
    /**
     * bandera para determinar entre las inconsistencias geograficas existe una prioritaria
     */
    private boolean banderaExistenInconsistenciasPrioritaria;
    /**
     * banderas para habilitar los botones de avance proceso
     */
    private boolean banderaEnviarDepuracion;
    private boolean banderaCoordinador;
    private boolean banderaEnviarGeografica;
    private boolean banderaValidarYGuardar;
    /*
     * Lista de predios seleccionados
     */
    private Predio[] prediosSeleccionados;
    /**
     * Predio proyectado asociado al tramite
     */
    private PPredio predioProyectadoTramite;
    /**
     * Predio iniical asociado al tramite
     */
    private Predio predioInicial;
    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String prediosParaZoom;
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;
    @Autowired
    private IContextListener contextoWeb;

    /**
     * Bandera para determinar si un tramite tiene replica de consulta asociada.
     */
    private boolean banderaReplicaConsulta;

    /**
     * Para tramites de rectificacion de zonas masivas determina si el calculo de zonas,
     * asincronico, se ha realizado previamente
     */
    private boolean jobZonasPrevio;

    //-----------------    getters  y setters   -----------------//
    public boolean isJobZonasPrevio() {
        return jobZonasPrevio;
    }

    public void setJobZonasPrevio(boolean jobZonasPrevio) {
        this.jobZonasPrevio = jobZonasPrevio;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public boolean isBanderaReplicaConsulta() {
        return banderaReplicaConsulta;
    }

    public void setBanderaReplicaConsulta(boolean banderaReplicaConsulta) {
        this.banderaReplicaConsulta = banderaReplicaConsulta;
    }

    public boolean isBanderaModificarLinderos() {
        return this.banderaModificarLinderos;
    }

    public void setBanderaModificarLinderos(boolean banderaModificarLinderos) {
        this.banderaModificarLinderos = banderaModificarLinderos;
    }

    public boolean isBanderaAgregarPredios() {
        return this.banderaAgregarPredios;
    }

    public void setBanderaAgregarPredios(boolean banderaAgregarPredios) {
        this.banderaAgregarPredios = banderaAgregarPredios;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<Predio> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<Predio> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public List<SelectItem> getTiposDocumentoJustificacion() {
        return tiposDocumentoJustificacion;
    }

    public void setTiposDocumentoJustificacion(List<SelectItem> tiposDocumentoJustificacion) {
        this.tiposDocumentoJustificacion = tiposDocumentoJustificacion;
    }

    public List<Documento> getDocumentosJustificacion() {
        return documentosJustificacion;
    }

    public void setDocumentosJustificacion(List<Documento> documentosJustificacion) {
        this.documentosJustificacion = documentosJustificacion;
    }

    public Predio[] getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(Predio[] prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    public boolean isBanderaEliminarPredios() {
        banderaEliminarPredios = false;
        if (this.prediosTramite != null) {
            if (!this.prediosTramite.isEmpty() && this.prediosTramite.size() > 1) {
                banderaEliminarPredios = true;
            }
        }
        return banderaEliminarPredios;
    }

    public void setBanderaEliminarPredios(boolean banderaEliminarPredios) {
        this.banderaEliminarPredios = banderaEliminarPredios;
    }

    public long getTipoDocumentoJustificacion() {
        return tipoDocumentoJustificacion;
    }

    public void setTipoDocumentoJustificacion(long tipoDocumentoJustificacion) {
        this.tipoDocumentoJustificacion = tipoDocumentoJustificacion;
    }

    public PPredio getPredioProyectadoTramite() {
        return predioProyectadoTramite;
    }

    public void setPredioProyectadoTramite(PPredio predioProyectadoTramite) {
        this.predioProyectadoTramite = predioProyectadoTramite;
    }

    public boolean isBanderaExistenInconsistencias() {
        return banderaExistenInconsistencias;
    }

    public void setBanderaExistenInconsistencias(boolean banderaExistenInconsistencias) {
        this.banderaExistenInconsistencias = banderaExistenInconsistencias;
    }

    public boolean isBanderaExistenInconsistenciasPrioritaria() {
        return banderaExistenInconsistenciasPrioritaria;
    }

    public void setBanderaExistenInconsistenciasPrioritaria(
        boolean banderaExistenInconsistenciasPrioritaria) {
        this.banderaExistenInconsistenciasPrioritaria = banderaExistenInconsistenciasPrioritaria;
    }

    public boolean isBanderaEnviarDepuracion() {
        return banderaEnviarDepuracion;
    }

    public void setBanderaEnviarDepuracion(boolean banderaEnviarDepuracion) {
        this.banderaEnviarDepuracion = banderaEnviarDepuracion;
    }

    public boolean isBanderaCoordinador() {
        return banderaCoordinador;
    }

    public void setBanderaCoordinador(boolean banderaCoordinador) {
        this.banderaCoordinador = banderaCoordinador;
    }

    public boolean isBanderaEnviarGeografica() {
        return banderaEnviarGeografica;
    }

    public void setBanderaEnviarGeografica(boolean banderaEnviarGeografica) {
        this.banderaEnviarGeografica = banderaEnviarGeografica;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public IContextListener getContextoWeb() {
        return contextoWeb;
    }

    public void setContextoWeb(IContextListener contextoWeb) {
        this.contextoWeb = contextoWeb;
    }

    public boolean isBanderaValidarYGuardar() {
        return banderaValidarYGuardar;
    }

    public void setBanderaValidarYGuardar(boolean banderaValidarYGuardar) {
        this.banderaValidarYGuardar = banderaValidarYGuardar;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de RectificacionAreaMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.consultaPredioMB.setListenerMethodIdentifier(2);
        this.documentosJustificacion = new ArrayList<Documento>();
        this.cargarInfoTramite();
        this.cargarTipoDocumento();
        this.banderaValidarYGuardar = true;

        if (!this.tramite.tieneReplica() && this.tramite.isRectificacionArea()) {
            this.banderaCoordinador = false;
        } else {
            this.banderaCoordinador = true;
        }
        if (this.proyectarConservacionMB.isBanderaRectificacionZonas()) {
            this.inicializarVariablesVisorGIS();
            this.determinarExistenciaReplica();
        }

        if (this.tramite.isRectificacionMatriz()) {
            this.determinarExistenciaReplica();
            this.determinaJobPrevioZonas();
        }

        if (this.tramite.isRectificacionArea()) {
            this.validarInconsistencias();
        }

    }

    /**
     * Método para carga la inofrmacaión del tramite
     *
     * @author felipe.cadena
     * @modified juanfelipe.garcia : 14-08-2013 en los tramites condición 8, el PPredio selecionado
     * llega null, se adiciono la validación
     */
    public void cargarInfoTramite() {

        ProyectarConservacionMB pc = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");

        this.tramite = pc.getTramite();
        this.predioProyectadoTramite = pc.getPredioSeleccionado();

        this.prediosTramite = new ArrayList<Predio>();
        if (this.predioProyectadoTramite != null) {
            this.predioInicial = this.getConservacionService().obtenerPredioConDatosUbicacionPorId(
                this.predioProyectadoTramite.getId());

            if (!this.tramite.isRectificacionMatriz()) {
                this.prediosTramite.add(this.predioInicial);
            }
        }
        this.agregarPrediosPrevios();

    }

    /**
     * Método para inicializar las variables del visor GIS
     *
     * @author felipe.cadena
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = this.proyectarConservacionMB.getTramite().getPredio().
            getNumeroPredial();
    }

    /**
     * Método para agregar los predios que han sido relacionados al tramite previamente
     *
     * @author felipe.cadena
     */
    public void agregarPrediosPrevios() {

        //Realmente no son predios relacionados a un englobe solo que el metodo no tiene un nombre general.
        this.tramiteDetallePredios = this.getTramiteService().buscarPredioEnglobesPorTramite(
            this.tramite.getId());

        for (TramiteDetallePredio tdp : this.tramiteDetallePredios) {
            Predio p = this.getConservacionService().
                obtenerPredioConDatosUbicacionPorId(tdp.getPredio().getId());
            this.prediosTramite.add(p);
        }
    }

    /**
     * Arma la lista de items para los posibles tipos de documento de la rectificación de area.
     *
     * @author felipe.cadena
     */
    private void cargarTipoDocumento() {

        List<TramiteDocumento> lisdoc = new ArrayList<TramiteDocumento>();
        List<TramiteDocumento> lisAux = new ArrayList<TramiteDocumento>();
        List<TipoDocumento> listaTD = this.getGeneralesService().
            buscarTiposDeDocumentos(ETipoDocumentoClase.JUSTIFICATIVO_RECTIFICACION.toString(),
                ESiNo.NO.toString());
        this.tiposDocumentoJustificacion = new ArrayList<SelectItem>();

        for (TipoDocumento td : listaTD) {
            SelectItem si = new SelectItem(td.getId(), td.getNombre());
            this.tiposDocumentoJustificacion.add(si);
        }
        lisAux = this.getTramiteService().
            obtenerTramiteDocumentosDeTramitePorTipoDoc(this.tramite.getId(), listaTD.get(0).getId());
        if (lisAux.size() > 0) {
            this.tipoDocumentoJustificacion = listaTD.get(0).getId();
            lisdoc.addAll(lisAux);
        }
        lisAux = this.getTramiteService().
            obtenerTramiteDocumentosDeTramitePorTipoDoc(this.tramite.getId(), listaTD.get(1).getId());

        if (lisAux.size() > 0) {
            this.tipoDocumentoJustificacion = listaTD.get(1).getId();
            lisdoc.addAll(lisAux);
        }

        if (lisdoc.size() > 0) {
            this.banderaModificarLinderos = true;
        }

        for (TramiteDocumento tramiteDocumento : lisdoc) {
            this.documentosJustificacionPrevio = tramiteDocumento;
            this.documentosJustificacion.add(tramiteDocumento.getDocumento());
        }
    }

    /**
     * Metodo para determinar si los tramites de rectificacion de area tienen inconsistencias
     *
     * @author felipe.cadena
     */
    private void validarInconsistencias() {

        this.banderaEnviarGeografica = true;

        List<TramiteInconsistencia> tramiteInconsistencias =
            this.getTramiteService().buscarTramiteInconsistenciaPorTramiteId(this.tramite.getId());

        if (tramiteInconsistencias != null && !tramiteInconsistencias.isEmpty()) {
            for (TramiteInconsistencia ti : tramiteInconsistencias) {
                if (!Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA.equals(ti.getTipo())) {
                    this.banderaEnviarGeografica = false;
                    this.banderaEnviarDepuracion = true;
                    break;
                } else {
                    this.banderaEnviarGeografica = true;
                    this.banderaEnviarDepuracion = true;
                    break;
                }
            }
        }
    }

    /**
     * Método para confirmar la modificación de linderos.
     *
     * @author felipe.cadena
     */
    /*
     * @modified by jonathan.chacon 13/08/2015 :: #13366 :: se desactivan los botones validar,
     * enviar a depuracion y enviar a lo geografico en caso de seleccionar modificacion de linderos
     */
    public void aceptarModificacionLinderos() {
        this.banderaEnviarGeografica = true;

        //Si no hay modificación de linderos se borran los predios y los documentos asociados.
        if (!this.banderaModificarLinderos) {
            this.banderaEnviarGeografica = true;
            this.banderaCoordinador = false;

            // Eliminación de documentos
            this.eliminarDocumentoAdjunto();

            // Eliminación de predios
            if (this.prediosTramite != null && this.prediosTramite.size() > 0) {
                List<Predio> prediosSeleccionadosTemp = new ArrayList<Predio>();
                for (Predio p : this.prediosTramite) {
                    if (this.predioInicial != null &&
                         !(p.getId().longValue() == this.predioInicial.getId().longValue())) {
                        prediosSeleccionadosTemp.add(p);
                    }
                }
                if (!prediosSeleccionadosTemp.isEmpty()) {
                    this.prediosSeleccionados = new Predio[prediosSeleccionadosTemp.size()];
                    this.prediosSeleccionados = prediosSeleccionadosTemp.toArray(
                        this.prediosSeleccionados);
                    this.retirarPredios();
                }
            }

            this.tipoDocumentoJustificacion = 0;
            this.prediosTramite = new ArrayList<Predio>();
            this.prediosTramite.add(this.predioInicial);
        } else {
            this.banderaEnviarDepuracion = false;
            this.banderaEnviarGeografica = false;
            this.banderaValidarYGuardar = false;
        }
    }

    /**
     * Método para cargar el documento de justificacion de la modificacion de linderos.
     *
     * @author felipe.cadena
     * @param event
     */
    public void cargaDocumento(FileUploadEvent event) {

        Documento doc = new Documento();
        doc.setArchivo(event.getFile().getFileName());
        UtilidadesWeb.cargarArchivoATemp(event, false);

        this.documentosJustificacion.add(doc);
    }

    /**
     * Método para eliminar el documento adjunto.
     *
     * @author felipe.cadena
     */
    /*
     * @modified by leidy.gonzalez 26/01/2015 :: #11109 :: Se elimina documento de Justificacion de
     * la Base de Datos, no solo de memoria.
     */
    public void eliminarDocumentoAdjunto() {

        Documento docTemp = new Documento();

        if (this.documentosJustificacionPrevio != null && this.documentosJustificacionPrevio.
            getDocumento().getId() != null) {

            docTemp = this.getTramiteService().buscarDocumentoPorId(
                this.documentosJustificacionPrevio.getDocumento().getId());

            if (docTemp != null && docTemp.getId() != null) {
                this.getTramiteService()
                    .eliminarTramiteDocumentoConDocumentoAsociado(
                        this.documentosJustificacionPrevio);
                this.documentosJustificacion
                    .remove(this.documentosJustificacionPrevio);
            }

        }
        if (this.documentosJustificacion != null && this.documentosJustificacion.size() > 0) {
            this.documentosJustificacion
                .remove(this.documentosJustificacion.get(0));
        }

    }

    /**
     * Obtiene la lista de predios relacionados al ph donde pertenece el predio no incluye el predio
     * que se envia como parametro
     *
     * @author felipe.cadena
     * @param predio
     * @return
     */
    private List<Predio> consultarPrediosPH(Predio predio) {

        List<Predio> ps;

        String numeroTerreno = predio.getNumeroPredial().substring(0, 21);

        ps = this.getConservacionService().obtenerPrediosPH(numeroTerreno);

        ps.remove(predio);

        return ps;

    }

    /**
     * Obtiene la lista de predios relacionados al condominio donde pertenece el predio no incluye
     * el predio que se envia como parametro
     *
     * @author felipe.cadena
     * @param predio
     * @return
     */
    private List<Predio> consultarPrediosCondominio(Predio predio) {

        List<Predio> ps = new ArrayList<Predio>();

        FichaMatriz fm = this.getConservacionService().obtenerFichaMatrizOrigen(predio.
            getNumeroPredial());

        if (fm != null) {
            for (FichaMatrizPredio fmp : fm.getFichaMatrizPredios()) {
                Predio p = this.getConservacionService().getPredioByNumeroPredial(fmp.
                    getNumeroPredial());
                ps.add(p);
            }
        }
        return ps;
    }

    /**
     * Método para agregar los predios seleccionados a la tabla de predios involucrados con la
     * rectificación
     *
     * @author felipe.cadena
     */
    public void adicionarPrediosSeleccionados() {

        //felipe.cadena::11-11-2016::Se invoca la funcionalidad de cancelacion masiva desde aca 
        //ya que solo se puede enviar un metodo como parametro al componente de agregar predios.
        if (this.tramite.isCancelacionMasiva()) {
            EntradaProyeccionMB entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.
                getManagedBean("entradaProyeccion");

            entradaProyeccionMB.adicionarPrediosCancelacionMasiva();
            return;
        }

        //felipe.cadena::05-07-2017::Se invoca la funcionalidad de englobe virtual desde aca 
        //ya que solo se puede enviar un metodo como parametro al componente de agregar predios.
        if (this.getEntradaProyeccionMB().getTramite().isEnglobeVirtual()) {
            EntradaProyeccionMB entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.
                getManagedBean("entradaProyeccion");

            entradaProyeccionMB.adicionarPrediosEnglobeVirtual();
            return;
        }

        this.banderaEnviarGeografica = true;
        this.banderaCoordinador = false;
        List<Predio> prediosSel = this.consultaPredioMB.getPrediosSeleccionados2();

        //Validaciones sobre los predios seleccionados
        boolean noMejoras = RectificacionAreaMB.determinarPredioMejora(prediosSel, this);
        boolean mismoMunicipio = RectificacionAreaMB.determinarMunicipio(prediosSel, this.tramite.
            getPredio().getMunicipio().getCodigo(), this);

        //Agregar predios relacionados por PH o condominio
        List<Predio> prediosAdicionales = new ArrayList<Predio>();
        for (Predio predio : prediosSel) {
            if (predio.getCondicionPropiedad() != null && predio.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_9.getCodigo())) {
                List<Predio> prediosPh = this.consultarPrediosPH(predio);
                prediosAdicionales.addAll(prediosPh);
            }
            if (predio.getCondicionPropiedad() != null && predio.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_8.getCodigo())) {
                List<Predio> prediosPh = this.consultarPrediosCondominio(predio);
                prediosAdicionales.addAll(prediosPh);
            }

        }
        prediosSel.addAll(prediosAdicionales);

        boolean colindancia = this.determinarColindancia(prediosSel);

        if (prediosSel.isEmpty()) {
            this.addMensajeWarn("No existen predios seleccionados");
        } else if (colindancia && noMejoras && mismoMunicipio) {
            boolean existePredioBloqueado = false;
            String stNumeroPredialBloqueado = "";
            for (Predio predio : prediosSel) {
                if (!this.prediosTramite.contains(predio)) {
                    //Determinar si el predio tiene tramites asociados
                    List<Tramite> tramites = (ArrayList<Tramite>) this.getTramiteService().
                        buscarTramitesPendientesDePredio(predio.getId());
                    if (tramites != null && tramites.size() > 0) {
                        this.addMensajeError("El predio " + predio.getNumeroPredial() +
                            
                            ", tiene un trÃ¡mite en proceso y no puede ser seleccionado para la rectificaciÃ³n");
                    } else {
                        this.determinarInconsistenciasGeograficas(predio);
                        List<PredioBloqueo> predioBloqueos = this.getConservacionService().
                            obtenerPredioBloqueosPorNumeroPredial(predio.getNumeroPredial());
                        predio.setPredioBloqueos(predioBloqueos);
                        if (!predio.isEstaBloqueado()) {
                            this.prediosTramite.add(predio);
                        } else {
                            existePredioBloqueado = true;
                            stNumeroPredialBloqueado = predio.getNumeroPredial();
                        }
                    }
                }
            }
            if (existePredioBloqueado) {
                this.addMensajeError("El predio " + stNumeroPredialBloqueado +
                     " estÃ¡ bloqueado y no puede ser seleccionado para la rectificaciÃ³n");
            }
        }

        this.validarGuardarTramite();
    }

    /**
     * Método para retirar los predios seleccionados de la lista de predios asociados al tramite.
     *
     * @author felipe.cadena
     */
    public void retirarPredios() {

        this.banderaEnviarGeografica = true;
        this.banderaCoordinador = false;

        List<Predio> prediosRetirar = new ArrayList<Predio>();

        //Agregar predios relacionados por PH o condominio
        for (Predio predio : this.prediosSeleccionados) {
            if (predio.getCondicionPropiedad() != null && predio.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_9.getCodigo())) {
                List<Predio> prediosPh = this.consultarPrediosPH(predio);
                prediosRetirar.addAll(prediosPh);
            }
            if (predio.getCondicionPropiedad() != null && predio.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_8.getCodigo())) {
                List<Predio> prediosPh = this.consultarPrediosCondominio(predio);
                prediosRetirar.addAll(prediosPh);
            }
            prediosRetirar.add(predio);
        }

        //Se utiliza la entidad deprecada paa actualizar la asociacion al tramite que es del tipo deprecado.
        List<TramitePredioEnglobe> prediosTramite = this.tramite.getTramitePredioEnglobes();
        List<TramitePredioEnglobe> prediosEliminados = new LinkedList<TramitePredioEnglobe>();
        List<TramiteInconsistencia> inconsistenciasAEliminar =
            new ArrayList<TramiteInconsistencia>();
        List<TramiteInconsistencia> inconsistenciasTotales = this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramite.getId());

        for (Predio predio : prediosRetirar) {
            if (!predio.equals(this.predioInicial)) {
                this.prediosTramite.remove(predio);
                TramiteDetallePredio tdp = this.getTramiteService().
                    buscarTramiteDetallePredioPorTramiteIdYPredioId(this.tramite.getId(), predio.
                        getId());
                this.getTramiteService().removerTramiteDetallePredio(tdp);
                this.getConservacionService().reversarProyeccionPredio(this.tramite.getId(), predio.
                    getId());

                for (int i = 0; i < prediosTramite.size(); i++) {
                    TramitePredioEnglobe tpe = prediosTramite.get(i);
                    if (tpe.getId().equals(tdp.getId())) {
                        prediosEliminados.add(tpe);
                    }
                }

                //Prepara inconsistencias que deben ser eliminadas
                for (TramiteInconsistencia ti : inconsistenciasTotales) {
                    if (ti.getNumeroPredial().equals(predio.getNumeroPredial())) {
                        inconsistenciasAEliminar.add(ti);
                    }
                }

            } else {
                this.addMensajeError("El predio inicial no puede ser retirado");
            }
        }

        prediosTramite.removeAll(prediosEliminados);

        //Se eliminan las inconsistencias a los predios eliminados
        this.getTramiteService().eliminarTramiteInconsistencia(inconsistenciasTotales);
        this.tramite.setTramitePredioEnglobes(prediosTramite);
    }

    /**
     * Metodo para determinar si los predios son colindantes indirectos con el predio relacionado al
     * tramite.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarColindancia(List<Predio> prediosSel) {

        prediosSel.addAll(this.prediosTramite);
        prediosSel.remove(this.tramite.getPredio());

        List<Predio> prediosNoColindantes = this.getConservacionService().determinarColindancia(
            prediosSel, this.tramite.getPredio(), this.usuario, true);

        if (prediosNoColindantes.isEmpty()) {
            return true;
        }
        String mensaje = "Los predios ";
        for (Predio predio : prediosNoColindantes) {
            mensaje += predio.getNumeroPredial() + ", ";
        }
        mensaje += "no son colindantes";

        this.addMensajeError(mensaje);

        return false;
    }

    /**
     * Metodo para determinar si los predios son condicion 5 o 6
     *
     * @author felipe.cadena
     * @return
     */
    public static boolean determinarPredioMejora(List<Predio> prediosSel, SNCManagedBean beanBase) {

        List<Predio> prediosNoCoindantes = new ArrayList<Predio>();

        for (Predio predio : prediosSel) {
            if (predio.getCondicionPropiedad() == null) {
                continue;
            }
            if (predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo()) ||
                 predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_6.getCodigo())) {
                prediosNoCoindantes.add(predio);
            }
        }
        if (prediosNoCoindantes.isEmpty()) {
            return true;
        }
        String mensaje = "Los predios ";
        for (Predio predio : prediosNoCoindantes) {
            mensaje += predio.getNumeroPredial() + ", ";
        }
        mensaje += "son mejoras. No se puede realizar rectificación de áreas de terreno en mejoras";

        beanBase.addMensajeError(mensaje);

        return false;
    }

    /**
     * Metodo para determinar si uno de los predios seleccionados tiene inconsistencias geográficas.
     *
     * @author felipe.cadena
     * @return
     */
    /*
     * modified::andres.eslava::25-01-2016::Se agrega nueva forma de procesar resultado.
     */
    public void determinarInconsistenciasGeograficas(Predio predioSel) {

        List<TramiteInconsistencia> inconsistencias = null;

        try {
            inconsistencias = this.getGeneralesService().determinarInconsistenciasGeograficas(
                predioSel.getNumeroPredial(), this.tramite, this.usuario);

            if (inconsistencias == null || inconsistencias.isEmpty()) {
                this.banderaExistenInconsistencias = false;
                return;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);

        }

        String mensaje = "El predio No " + predioSel.getNumeroPredial() +
            ", tiene las siguientes inconsistencias: ";
        for (TramiteInconsistencia ti : inconsistencias) {
            mensaje += ti.getInconsistencia();

        }

        this.getTramiteService().guardarActualizarTramiteInconsistencia(inconsistencias);

        this.addMensajeError(mensaje);

        this.banderaExistenInconsistencias = true;
        this.banderaEnviarDepuracion = true;
        this.banderaEnviarGeografica = false;
        this.banderaCoordinador = false;

    }

    /**
     * Metodo para determinar si los predios tienen informaciòn geografica
     *
     * @author felipe.cadena
     * @return
     */
    public static boolean determinarMunicipio(List<Predio> prediosSel, String municipioCodigo,
        SNCManagedBean beanBase) {

        for (Predio predio : prediosSel) {
            if (!predio.getMunicipio().getCodigo().equals(municipioCodigo)) {
                beanBase.addMensajeError(
                    "No se puede realizar rectificación de linderos por predios de diferentes municipios");
                return false;
            }
        }
        return true;
    }

    /**
     * Método para realizar las validaciones finales relacionadas al trámite de rectificación
     *
     * @author felipe.cadena
     * @modified by leidy.gonzalez 11/09/2014 Se modifica que el documento a ser eliminado ya se
     * encuentre en la Base de Datos por lo que su identificador no debe ser nulo
     */
    /*
     * @modified by leidy.gonzalez 26/01/2015 :: #11109 :: Se guarda el documento cargado en las
     * tablas Tramite_Documento y Documento para que sea consistente en la Base de Datos y al
     * ingresar nuevamente a la pagina se visualice los documentos cargados
     */
    public Boolean validarGuardarTramite() {

        if (this.banderaModificarLinderos && this.documentosJustificacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar cual es el documento que sustenta la rectificación de área");
            return false;
        }
        if (this.banderaModificarLinderos && this.prediosTramite.size() == 1) {
            this.addMensajeError(
                "Si hay cambio de linderos este debe tener más de dos predios asociados");
            return false;
        }
        if (this.banderaModificarLinderos && this.tipoDocumentoJustificacion == 0) {
            this.addMensajeError(
                "Debe seleccionar cual es el documento que sustenta la rectificación de área");
            return false;
        }

        this.banderaEnviarDepuracion = true;
        this.banderaValidarYGuardar = true;

        //se eliminan los predios asociados previos para agregar los nuevos.
        this.tramiteDetallePredios = this.getTramiteService().buscarPredioEnglobesPorTramite(
            this.tramite.getId());
        for (TramiteDetallePredio tdp : this.tramiteDetallePredios) {
            this.getTramiteService().removerTramiteDetallePredio(tdp);
        }

        //Se utiliza la entidad deprecada para actualizar la asociacion al tramite que es del tipo deprecado.
        List<TramitePredioEnglobe> nuevosPredios = new LinkedList<TramitePredioEnglobe>();
        if (!this.tramite.isRectificacionMatriz()) {
            for (Predio predio : this.prediosTramite) {
                if (!this.predioInicial.equals(predio)) {
                    TramitePredioEnglobe tdp = new TramitePredioEnglobe();
                    tdp.setPredio(predio);
                    tdp.setEnglobePrincipal(ESiNo.NO.getCodigo());
                    tdp.setTramite(this.tramite);
                    tdp.setUsuarioLog(this.usuario.getLogin());
                    tdp.setFechaLog(new Date());
                    tdp = this.getTramiteService().guardarYactualizarTramitePredioEnglobe(tdp);
                    if (tdp == null) {
                        this.addMensajeError("Error al guardar en la Base de datos");
                        return false;
                    }
                    nuevosPredios.add(tdp);
                }
            }
            this.tramite.setTramitePredioEnglobes(nuevosPredios);
        }

        Documento docJus = null;

        if (this.documentosJustificacion != null && !this.documentosJustificacion.isEmpty()) {
            docJus = this.documentosJustificacion.get(0);
        }
        if (docJus != null && docJus.getId() != null && docJus.getIdRepositorioDocumentos() == null) {

            //Se elimina el documento anterior para evitar redundancia
            if (this.documentosJustificacionPrevio != null) {
                this.getTramiteService().eliminarTramiteDocumentoConDocumentoAsociado(
                    this.documentosJustificacionPrevio);
                this.documentosJustificacion.remove(this.documentosJustificacionPrevio);
            }

            TipoDocumento td = this.getGeneralesService().
                buscarTipoDocumentoPorId(this.tipoDocumentoJustificacion);
            docJus.setTipoDocumento(td);

            docJus.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            docJus.setBloqueado(ESiNo.NO.getCodigo());
            docJus.setUsuarioCreador(this.usuario.getLogin());
            docJus.setUsuarioLog(this.usuario.getLogin());
            docJus.setFechaLog(new Date());

            TramiteDocumento tDoc = new TramiteDocumento();

            tDoc.setTramite(this.tramite);
            tDoc.setUsuarioLog(this.usuario.getLogin());
            tDoc.setFechaLog(new Date());
            tDoc.setfecha(new Date());

            DocumentoTramiteDTO tdDTO = new DocumentoTramiteDTO(
                UtilidadesWeb.obtenerRutaTemporalArchivos() + System.getProperty("file.separator") +
                docJus.getArchivo(),
                td.getNombre(),
                this.tramite.getFechaRadicacion(),
                this.tramite.getTipoTramite(),
                this.tramite.getId());
            tdDTO.setNombreDepartamento(this.tramite.getDepartamento().getNombre());
            tdDTO.setNombreMunicipio(this.tramite.getMunicipio().getNombre());
            tdDTO.setNumeroPredial(this.tramite.getPredio().getNumeroPredial());
            docJus = this.getGeneralesService().guardarDocumentoAlfresco(usuario, tdDTO, docJus);
            tDoc.setDocumento(docJus);
            tDoc.setIdRepositorioDocumentos(docJus.getIdRepositorioDocumentos());
            this.documentosJustificacion = new LinkedList<Documento>();
            this.documentosJustificacion.add(docJus);
            this.getTramiteService().actualizarTramiteDocumento(tDoc);

        } else if (docJus != null && docJus.getIdRepositorioDocumentos() == null) {

            TipoDocumento td = this.getGeneralesService().
                buscarTipoDocumentoPorId(this.tipoDocumentoJustificacion);

            docJus.setTipoDocumento(td);

            docJus.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            docJus.setBloqueado(ESiNo.NO.getCodigo());
            docJus.setUsuarioCreador(this.usuario.getLogin());
            docJus.setUsuarioLog(this.usuario.getLogin());
            docJus.setFechaLog(new Date());

            TramiteDocumento tDoc = new TramiteDocumento();

            tDoc.setTramite(this.tramite);
            tDoc.setUsuarioLog(this.usuario.getLogin());
            tDoc.setFechaLog(new Date());
            tDoc.setfecha(new Date());

            DocumentoTramiteDTO tdDTO = new DocumentoTramiteDTO(
                UtilidadesWeb.obtenerRutaTemporalArchivos() + System.getProperty("file.separator") +
                docJus.getArchivo(),
                td.getNombre(),
                this.tramite.getFechaRadicacion(),
                this.tramite.getTipoTramite(),
                this.tramite.getId());
            tdDTO.setNombreDepartamento(this.tramite.getDepartamento().getNombre());
            tdDTO.setNombreMunicipio(this.tramite.getMunicipio().getNombre());
            tdDTO.setNumeroPredial(this.tramite.getPredio().getNumeroPredial());
            docJus.setTramiteId(this.tramite.getId());
            docJus = this.getGeneralesService().guardarDocumentoAlfresco(usuario, tdDTO, docJus);
            tDoc.setDocumento(docJus);
            tDoc.setIdRepositorioDocumentos(docJus.getIdRepositorioDocumentos());
            this.documentosJustificacionPrevio = tDoc;
            this.documentosJustificacion = new LinkedList<Documento>();
            this.documentosJustificacion.add(docJus);

            this.getTramiteService().actualizarTramiteDocumento(this.documentosJustificacionPrevio);

        }
        return true;
    }

    /**
     * Método para actualizar la ediciones de los predios seleccionados ya que solo se puede
     * modificar los datos relacionados al predio inicial del trámite
     *
     * @author felipe.cadena
     */
    public void actualizarEdicionDatos() {

        RectificacionComplementacionMB rc = (RectificacionComplementacionMB) UtilidadesWeb.
            getManagedBean("rc");
        ProyectarConservacionMB pc = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");

        if (this.tramite.isRectificacionMatriz()) {
            //pc.cargarConstruccionesDesenglobe();
        }

        if (this.prediosSeleccionados.length == 0) {
            rc.setHabilitarTabRectificacion(true);
            return;
        }
        if (this.prediosSeleccionados.length > 1) {
            //deshabilita pestañas
            rc.setHabilitarTabRectificacion(false);

        } else if (this.prediosSeleccionados[0].getId() == this.predioInicial.getId()) {
            // habilita los campos para la rectificación
            pc.setPredioSeleccionado(this.predioProyectadoTramite);
            pc.setAvaluo();////version 1.1.4 - felipe.cadena_7286
            rc.init();
            rc.setHabilitarTabRectificacion(true);
        } else {

            if (this.tramite.isRectificacionMatriz()) {
                rc.setHabilitarTabRectificacion(false);
            } else {
                rc.setHabilitarTabRectificacion(true);
            }

            PPredio predioProyectado = null;
            try {
                predioProyectado = this.getConservacionService().
                    obtenerPPredioCompletoByIdyTramite(this.prediosSeleccionados[0].getId(),
                        this.tramite.getId());

                if (predioProyectado != null) {
                    rc.init();
                    pc.setPredioSeleccionado(predioProyectado);
                    pc.setAvaluo();//version 1.1.4 - felipe.cadena_7286
                    rc.setHabilitarTabRectificacion(true);
                    if (!this.tramite.isRectificacionMatriz()) {
                        rc.deshabilitarTodo();
                    }

                }
            } catch (Exception ex) {
                LOGGER.debug("Predio seleccionado no proyectado");
                this.addMensajeInfo(
                    "Predio seleccionado no proyectado, No se puede mostrar información de proyección");
            }

        }
    }

    /**
     * Método para preparar los datos del trámite y enviarlo a la parte geografica
     *
     * @author felipe.cadena
     * @return
     */
    /*
     * @modified by leidy.gonzalez 22/10/2014 Se guarda la fecha de inscripción para los predios
     * proyectados de tramites de rectificacón.
     */
 /*
     * @modified by leidy.gonzalez::#17785::08/06/2016:: Se ajusta validacion para insertar los
     * datos en la tabla: TRAMITE_DIGITALIZACION
     */
    public String enviarAGeografica() {

        ProyectarConservacionMB proyectarConservacionMB = (ProyectarConservacionMB) UtilidadesWeb.
            getManagedBean("proyectarConservacion");
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb.getManagedBean(
            "tareasPendientes");
        //v1.1.20
        //Se guardan los cambios hechos en la proyeccion
        if (!this.tramite.isCancelacionMasiva()) {
            if (!this.validarGuardarTramite()) {
                return null;
            }
        }

        //validacion de coeficientes antes de enviar a geografico
        if (this.getTramite().isRectificacionMatriz() || this.getTramite().isDesenglobeEtapas() ||
             this.getTramite().isQuintaMasivo()) {
            if (!proyectarConservacionMB.validarCoeficientes(this.getTramite().getPredio().getId(),
                true)) {
                return null;
            }
        } else if (this.getTramite().isTipoInscripcionPH() || this.getTramite().
            isTipoInscripcionCondominio()) {
            if (!proyectarConservacionMB.validarCoeficientes(this.proyectarConservacionMB.
                getFichaMatrizSeleccionada().getPPredio().getId(), true)) {
                return null;
            }
        }

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
        if (!generalMB.validarEnvioModificacionGeografica(this.tramite)) {
            return "";
        }

        if (this.banderaModificarLinderos) {
            //Se reversan las proyecciones de predios realizadas previamente.
            for (Predio predio : this.prediosTramite) {
                if (!predio.equals(this.predioInicial)) {
                    this.getConservacionService().reversarProyeccionPredio(this.tramite.getId(),
                        predio.getId());
                }
            }

            //Se realiza la proyecciòn del tràmite
            Object[] resProyecccion = this.getTramiteService().generarProyeccion(this.tramite.
                getId());

            if (resProyecccion != null) {
                //v1.1.20
                if (resProyecccion.length > 0) {
                    ArrayList msg = (ArrayList) resProyecccion[0];
                    // felipe.cadena:: #7480
                    if (msg.size() > 0) {
                        Object[] l = (Object[]) msg.get(0);
                        String msgStr = (String) l[3];
                        if (msgStr.contains("No se encontraron predios a proyectar.")) {
                            this.addMensajeInfo("No se proyectaron predios");
                        } else {
                            this.addMensajeError("Error en la proyección de los predios");
                        }

                        return null;
                    }
                    for (Object string : msg) {
                        LOGGER.debug(string.toString());
                    }

                }
            }
        }

        this.tramite.setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);

        if (!this.tramite.isRectificacionMatriz()) {
            this.predioProyectadoTramite.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.
                getCodigo());
            this.predioProyectadoTramite = this.getConservacionService().actualizarPPredio(
                predioProyectadoTramite);

            ////version 1.1.4 - felipe.cadena_7286
            List<PPredio> prediosRectificacion = this.getConservacionService().
                buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
            for (PPredio pp : prediosRectificacion) {
                pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                pp.setFechaInscripcionCatastral(
                    this.proyectarConservacionMB.getPredioSeleccionado().
                        getFechaInscripcionCatastral());
                this.getConservacionService().actualizarPPredio(pp);

                if (this.tramite.isRectificacionGeograficaAreaTerreno() || this.tramite.
                    isRectificacionGeograficaAreaConstruccion()) {
                    if (pp.getFechaInscripcionCatastral() == null) {
                        this.addMensajeError(ECodigoErrorVista.FECHA_INSCRIPCION_02.toString());
                        return null;
                    }
                }
            }
        }

        //felipe.cadena::05-08-2015::Validacion fecha inscripcion para rectificacion matriz
        if (this.tramite.isRectificacionMatriz()) {
            List<PPredio> prediosRectificacion = this.getConservacionService().
                buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
            for (PPredio pp : prediosRectificacion) {
                if (pp.getFechaInscripcionCatastral() == null) {
                    this.addMensajeError(ECodigoErrorVista.FECHA_INSCRIPCION_02.toString());
                    return null;
                }
            }
        }

        this.getTramiteService().actualizarTramite(this.tramite);

        this.validacionesMutacionTerceraGeografica();

        if (proyectarConservacionMB.getDigitalizadorAsignado() != null) {

            proyectarConservacionMB.setDigitalizador(this.getGeneralesService().getCacheUsuario(
                proyectarConservacionMB.getDigitalizadorAsignado()));

            this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(
                proyectarConservacionMB.getActividad(), this.usuario);
        } else {
            this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(
                proyectarConservacionMB.getActividad(), this.usuario);
        }

        tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean("proyectarConservacion");
        return "index";

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para mover el tramite a edicion geografica con un digitalizador asociado para
     * rectificacion de area con linderos
     *
     * @author felipe.cadena
     */
    /*
     * @modified by leidy.gonzalez::#17785::08/06/2016:: Se ajusta validacion para insertar los
     * datos en la tabla: TRAMITE_DIGITALIZACION
     */
    public String asignarDigitalizadorRectArea() {

        ProyectarConservacionMB proyectarConservacionMB = (ProyectarConservacionMB) UtilidadesWeb.
            getManagedBean("proyectarConservacion");
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb.getManagedBean(
            "tareasPendientes");

        //v1.1.20
        //Se guardan los cambios hechos en la proyeccion
        if (!this.validarGuardarTramite()) {
            return null;
        }

        // UsuarioDTO digitalizador = (UsuarioDTO)proyectarConservacionMB.getDigitalizadorSeleccionado()[0];
        if (this.banderaModificarLinderos) {
            //Se reversan las proyecciones de predios realizadas previamente.
            for (Predio predio : this.prediosTramite) {
                if (!predio.equals(this.predioInicial)) {
                    this.getConservacionService().reversarProyeccionPredio(this.tramite.getId(),
                        predio.getId());
                }
            }

            //Se realiza la proyecciòn del tràmite
            Object[] resProyecccion = this.getTramiteService().generarProyeccion(this.tramite.
                getId());

            if (resProyecccion != null) {
                //v1.1.20
                if (resProyecccion.length > 0) {
                    ArrayList msg = (ArrayList) resProyecccion[0];
                    // felipe.cadena:: #7480
                    if (msg.size() > 0) {
                        Object[] l = (Object[]) msg.get(0);
                        String msgStr = (String) l[3];
                        if (msgStr.contains("No se encontraron predios a proyectar.")) {
                            this.addMensajeInfo("No se proyectaron predios");
                        } else {
                            this.addMensajeError("Error en la proyección de los predios");
                        }

                        return null;
                    }
                    for (Object string : msg) {
                        LOGGER.debug(string.toString());
                    }

                }
            }
        }

        UsuarioDTO digitalizador = null;
        if (proyectarConservacionMB.isBanderaDigitalizacionAsignada()) {
            digitalizador = this.getGeneralesService().getCacheUsuario(proyectarConservacionMB.
                getDigitalizadorAsignado());

            if (proyectarConservacionMB.getDigitalizadorAsignado() != null &&
                proyectarConservacionMB.getDigitalizadorSeleccionado()[0] != null &&
                 !proyectarConservacionMB.getDigitalizadorAsignado().equals(proyectarConservacionMB.
                    getDigitalizadorSeleccionado()[0])) {

                digitalizador =
                    (UsuarioDTO) proyectarConservacionMB.getDigitalizadorSeleccionado()[0];
            }

        } else {
            digitalizador = (UsuarioDTO) proyectarConservacionMB.getDigitalizadorSeleccionado()[0];
        }

        this.tramite.setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
        //version 1.1.4 - felipe.cadena_7286
        this.getTramiteService().actualizarTramite(this.tramite);

        List<PPredio> prediosRectificacion = this.getConservacionService().
            buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
        for (PPredio pp : prediosRectificacion) {
            pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            pp.setFechaInscripcionCatastral(this.predioProyectadoTramite.
                getFechaInscripcionCatastral());
            this.getConservacionService().actualizarPPredio(pp);

            if (this.tramite.isRectificacionGeograficaAreaTerreno() || this.tramite.
                isRectificacionGeograficaAreaConstruccion()) {
                if (pp.getFechaInscripcionCatastral() == null) {
                    this.addMensajeError(ECodigoErrorVista.FECHA_INSCRIPCION_02.toString());
                    return null;
                }
            }

        }

        this.validacionesMutacionTerceraGeografica();

        this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(
            proyectarConservacionMB.getActividad(), digitalizador);

        //Se verifica que el dgitalizador seleccionado no sea el mismo que asigno anteriormente
        if ((proyectarConservacionMB.getDigitalizadorAsignado() != null &&
             proyectarConservacionMB.getDigitalizadorSeleccionado()[0] != null &&
            !proyectarConservacionMB
                .getDigitalizadorAsignado()
                .equals(proyectarConservacionMB.getDigitalizadorSeleccionado()[0])) ||
             proyectarConservacionMB.getDigitalizadorAsignado() == null) {

            proyectarConservacionMB.setDigitalizadorAsignado(digitalizador
                .getLogin());

            TramiteDigitalizacion td = new TramiteDigitalizacion();

            td.setTramite(this.tramite);
            td.setUsuarioDigitaliza(digitalizador.getLogin());
            td.setUsuarioEnvia(this.usuario.getLogin());
            td.setUsuarioLog(this.usuario.getLogin());
            td.setFechaLog(new Date());
            td.setFechaEnvia(new Date());
            this.getTramiteService().guardarActualizarTramiteDigitalizacion(td);

        }

        tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean("proyectarConservacion");
        return "index";

    }

    /**
     * Método para preparar los datos del trámite y enviarlo al proceso de depuración
     *
     * @author felipe.cadena
     */
    public void enviarADepuracion() {
        LOGGER.debug("Enviar a depuracion geográfica");
//TODO :: felipe.cadena ::mover a proceso de depuracion        

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza unas validaciones y actualizaciones al predio seleccionado para los
     * trámites de mutación de tercera que se requieren mover a la parte geográfica.
     *
     * @author david.cifuentes
     */
    public void validacionesMutacionTerceraGeografica() {
        if (this.tramite != null && this.predioProyectadoTramite != null &&
             this.tramite.isTercera()) {
            // Si las PUnidadConstruccion están como canceladas, en el campo
            // "FECHA_INSCRIPCION_CATASTRAL" se les debe asignar la fecha
            // del
            // sistema.
            if (!predioProyectadoTramite.getPUnidadConstruccions().isEmpty()) {
                for (PUnidadConstruccion pu : predioProyectadoTramite
                    .getPUnidadConstruccions()) {
                    if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(
                        pu.getCancelaInscribe())) {
                        pu.setFechaInscripcionCatastral(new Date(System
                            .currentTimeMillis()));
                    }
                }
            }

            // Para las mutaciones de tercera el
            // predio del trámite debe quedar como modificado
            // puesto que se debió realizar una acción de modificación,
            // eliminación o adición sobre las unidades de construcción del
            // mismo.
            predioProyectadoTramite
                .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                    .getCodigo());
            predioProyectadoTramite = this.getConservacionService()
                .guardarActualizarPPredio(predioProyectadoTramite);
        }
    }

    /**
     *
     * determina si el tramite tiene replica de consulta asociada y marca la bandera correpondiente
     *
     * @author felipe.cadena
     */
    private void determinarExistenciaReplica() {

        Documento d = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.tramite.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());

        this.banderaReplicaConsulta = (d != null);

    }

    /**
     * Genera la replica de consulta cuando se requiere por parte del usuario.
     *
     * @author felipe.cadena
     * @return
     */
    public String generarReplicaConsulta() {

        //se transfiere el tramite a tiempos muertos y se inicia la generacion de la replica
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb.getManagedBean(
            "tareasPendientes");

        RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
            getManagedBean("recuperarTramites");
        recuperarTamitesMB.generarReplicaConsulta(this.tramite, this.usuario, null, 1);

        UtilidadesWeb.removerManagedBean("cargueInfoAlSistema");
        tareasPendientesMB.init();
        return "index";
    }

    /**
     * Determina si se ha enviado un job previo de zonas para est tramite
     *
     * @author felipe.cadena
     */
    private void determinaJobPrevioZonas() {

        this.jobZonasPrevio = false;
        List<String> estados = new ArrayList<String>();
        estados.add(ProductoCatastralJob.SNC_TERMINADO);

        List<ProductoCatastralJob> jobsZonas = this.getGeneralesService().
            obtenerJobsPorEstadoTramiteIdTipo(null, this.tramite.getId(),
                ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS);

        if (jobsZonas != null && !jobsZonas.isEmpty()) {
            if (estados.contains(jobsZonas.get(jobsZonas.size() - 1).getEstado())) {
                this.jobZonasPrevio = true;
            }
        }
    }

    /**
     * Metodo usado para el calculo de zonas, en la rectificacion de zonas sobre la ficha matriz
     *
     * @author felipe.cadena
     */
    public void calcularZonasMatriz() {

        List<String> estados = new ArrayList<String>();
        estados.add(ProductoCatastralJob.AGS_EN_EJECUCION);
        estados.add(ProductoCatastralJob.AGS_ESPERANDO);
        estados.add(ProductoCatastralJob.AGS_TERMINADO);
        estados.add(ProductoCatastralJob.AGS_ERROR);
        estados.add(ProductoCatastralJob.SNC_ERROR);
        estados.add(ProductoCatastralJob.SNC_EN_EJECUCION);

        List<ProductoCatastralJob> jobsZonas = this.getGeneralesService().
            obtenerJobsPorEstadoTramiteIdTipo(null, this.tramite.getId(),
                ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS);

        if (jobsZonas != null && !jobsZonas.isEmpty()) {

            if (estados.contains(jobsZonas.get(jobsZonas.size() - 1).getEstado())) {
                this.addMensajeWarn(ECodigoErrorVista.RECTIFICACION_ZONAS_MASIVO_001.toString());
                LOGGER.warn(ECodigoErrorVista.RECTIFICACION_ZONAS_MASIVO_001.getMensajeTecnico());

                return;
            }
        }

        //Invocar calculo de zonas asincronico
        this.proyectarConservacionMB.calcularZonas();

        this.determinaJobPrevioZonas();
        //cuando finaliza el envio asincronico
        this.addMensajeInfo(ECodigoErrorVista.RECTIFICACION_ZONAS_MASIVO_001.toString());
        LOGGER.info(ECodigoErrorVista.RECTIFICACION_ZONAS_MASIVO_001.getMensajeTecnico());

    }

}
