package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("consultarNumeroPredialAnterior")
@Scope("session")
public class ConsultaNumeroPredialAnteriorMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 761725509620580700L;
    private static final Logger logger = LoggerFactory.getLogger(
        ConsultaNumeroPredialAnteriorMB.class);

    private List<HPredio> prediosHistoricos;

    private Predio predio;

    public ConsultaNumeroPredialAnteriorMB() {

    }

    public ConsultaNumeroPredialAnteriorMB(Predio predio) {
        this.predio = predio;
    }

    //@PostConstruct
    public void init() {

        ConsultaDatosUbicacionPredioMB datosUbicPredioMB =
            (ConsultaDatosUbicacionPredioMB) UtilidadesWeb.getManagedBean(
                "consultaDatosUbicacionPredio");
        String numeroPredial = datosUbicPredioMB.getPredio().getNumeroPredial();

        predio = this.getConservacionService().getPredioByNumeroPredial(numeroPredial);

        // TODO fabio.navarrete :: 01-12-11 :: revisar la pertinencia de el uso
        // de este metodo :: juan.agudelo
        this.prediosHistoricos = new ArrayList<HPredio>();
        List<HPredio> prediosAnteriores = this.getConservacionService().
            findHPredioListByConsecutivoCatastral(predio.getConsecutivoCatastral());
        if (prediosAnteriores.size() != 0) {
            this.prediosHistoricos = prediosAnteriores;
        }
    }

    public List<HPredio> getPrediosHistoricos() {
        return prediosHistoricos;
    }

    public void setPrediosHistoricos(List<HPredio> prediosHistoricos) {
        this.prediosHistoricos = prediosHistoricos;
    }

    public Predio getPredio() {
        return predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }
}
