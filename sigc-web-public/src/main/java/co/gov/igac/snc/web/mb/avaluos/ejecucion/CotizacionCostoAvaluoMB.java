/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EDatosBaseCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.ConsultaOfertasInmobiliariasMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.apache.commons.lang.NotImplementedException;

/**
 *
 * Managed bean del cu CU-SA-AC-005 Cotizar Costo de Avalúo
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-005 Cotizar Costo de Avalúo
 */
@Component("cotizarCostoAvaluo")
@Scope("session")
public class CotizacionCostoAvaluoMB extends SNCManagedBean {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CotizacionCostoAvaluoMB.class);

    /**
     * Avaluos del cual se quiere cotizar el costo
     */
    private Avaluo avaluo;

    /**
     * Valor total de la sumatoria de los valores de cotización de cada predio asociado al
     * {@link #avaluo}
     */
    private Double valorTotalCotizacion;

    /**
     * Lista de predios asociados avaluo {@link #avaluo}
     */
    private AvaluoPredio predioSeleccionado;

    /**
     * Identificador del avalúo que se selecciono desde process
     */
    private Long avaluoId;

    /**
     * Usuario activo en sesión
     */
    private UsuarioDTO usuario;

    // --------------------------------Reportes---------------------------------
    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si se muestran o no todos los datos de la tabla de predios del
     * {@link #avaluo}
     */
    private boolean banderaTablaExpandidaPredio;

    /**
     * Variable que hace obligatorio el ingreso de una justificación cuando el valor de cotización
     * se ajusta
     */
    private boolean banderaRequiereJusiticacionAjuste;

    // ----------------------------Métodos SET y GET----------------------------
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public Double getValorTotalCotizacion() {
        this.valorTotalCotizacion = 0.0;
        if (this.avaluo != null) {
            for (AvaluoPredio predio : this.avaluo.getAvaluoPredios()) {
                this.valorTotalCotizacion += predio.getValorCotizacion();
            }
        }
        return this.valorTotalCotizacion;
    }

    public AvaluoPredio getPredioSeleccionado() {
        return this.predioSeleccionado;
    }

    public void setPredioSeleccionado(AvaluoPredio predioSeleccionado) {
        this.predioSeleccionado = predioSeleccionado;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaTablaExpandidaPredio() {
        return this.banderaTablaExpandidaPredio;
    }

    public void setBanderaTablaExpandidaPredio(
        boolean banderaTablaExpandidaPredio) {
        this.banderaTablaExpandidaPredio = banderaTablaExpandidaPredio;
    }

    public boolean isBanderaRequiereJusiticacionAjuste() {
        return this.banderaRequiereJusiticacionAjuste;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.extraerDatosProcess();
        this.cargarAvaluoSeleccionado();
    }

    // -------------------------------------------------------------------------
    /**
     * método usado para cargar el avalúo asociado al {@link #avaluoId}
     *
     * @author christian.rodriguez
     */
    private void cargarAvaluoSeleccionado() {
        if (this.avaluoId != null) {
            this.avaluo = this.getAvaluosService()
                .obtenerAvaluoPorIdConAtributos(this.avaluoId);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que en un futuro deberá leer los datos del arbol del proceso y extrae el sec radicado
     * con al cual se le va a cotizar el costo
     *
     * @author christian.rodriguez
     */
    private void extraerDatosProcess() {

        this.inicializarDatosQuemados();
    }

    private void inicializarDatosQuemados() {
        this.avaluoId = 172L;
    }

    // -------------------------------------------------------------------------
    /**
     * Calcula el valor preliminar de la cotización del {@link #predioSeleccionado}, este valor es
     * el resultado de la ejecución del cu 137 y se le debe scar el iva, sumarselo y guardarlo en el
     * valor cotización del predio
     *
     * @author christian.rodriguez
     */
    public void calcularValorPreliminar() {

        if (this.predioSeleccionado != null &&
             this.predioSeleccionado.getValorPreliminar() != null) {

            // TODO::christian.rodriguez:: pendiente el llamado al cu 137
            Random generador = new Random();
            this.predioSeleccionado
                .setValorPreliminar(generador.nextDouble() * 100000);

            Double valorIVA = Constantes.IVA.doubleValue() *
                 this.predioSeleccionado.getValorPreliminar();

            this.predioSeleccionado.setValorIva(valorIVA);

            this.predioSeleccionado.setValorCotizacion(this.predioSeleccionado
                .getValorIva() +
                 this.predioSeleccionado.getValorPreliminar());

            this.predioSeleccionado
                .setValorCotizacionAjustado(this.predioSeleccionado
                    .getValorCotizacion());

        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón modificar predio. Se encarga de reinicializar las variables usadas por el
     * dialogo de edición de predio
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaEdicionPredio() {
        this.determinarObligatoriedadJustificacionAjuste();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que determina si se requiere justificacion del valor ajustado o no
     *
     * @author christian.rodriguez
     */
    private void determinarObligatoriedadJustificacionAjuste() {
        if (this.predioSeleccionado.getValorCotizacion() != null &&
             this.predioSeleccionado.getValorCotizacionAjustado() != null &&
             this.predioSeleccionado.getValorCotizacion().doubleValue() != this.predioSeleccionado
            .getValorCotizacionAjustado().doubleValue()) {

            this.banderaRequiereJusiticacionAjuste = true;
        } else {
            this.banderaRequiereJusiticacionAjuste = false;
            this.predioSeleccionado.setJustificacionValorAjustado(null);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Envía correo al director territorial sobre los prdios que requieren actualización catastral.
     *
     * @author ??
     */
    public void enviarNotificacionDeTramitesCatastralesParaPredios() {

        for (AvaluoPredio predio : this.avaluo.getAvaluoPredios()) {

            if (predio.getRequiereTramiteCatastral().equalsIgnoreCase(
                ESiNo.SI.getCodigo())) {

                List<String> destinatarios = new ArrayList<String>();

                EstructuraOrganizacional estructuraOrganizacional = this
                    .getGeneralesService()
                    .obtenerEstructuraOrganizacionalPorMunicipioCodigo(
                        predio.getMunicipio().getCodigo());

                UsuarioDTO usuarioDirectorTerritorial = this
                    .getGeneralesService()
                    .obtenerFuncionarioTerritorialYRol(
                        estructuraOrganizacional.getNombre(),
                        ERol.DIRECTOR_TERRITORIAL).get(0);

                destinatarios
                    .add(usuarioDirectorTerritorial
                        .getLogin()
                        .concat(EDatosBaseCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC
                            .getDato()));

                destinatarios.add("christian.rodriguez@igac.gov.co");

                // Generación de los datos del cuerpo del correo
                Object[] datosCorreo = new Object[7];

                datosCorreo[0] = predio.getNumeroPredial();
                datosCorreo[1] = predio.getDepartamento().getNombre();
                datosCorreo[2] = predio.getMunicipio().getNombre();
                datosCorreo[3] = predio.getDireccion();
                datosCorreo[4] = predio.getNumeroRegistro();
                datosCorreo[5] = predio.getObservaciones();
                datosCorreo[6] = this.usuario.getNombreCompleto();

                // Envio correo
                boolean notificacionExitosa = this
                    .getGeneralesService()
                    .enviarCorreoElectronicoConCuerpo(
                        EPlantilla.MENSAJE_TRAMITE_CATASTRAL_PREDIO_AVALUO,
                        destinatarios, null, null, datosCorreo, null,
                        null);

                if (notificacionExitosa) {
                    this.addMensajeInfo(
                        "Se envio la comunicación para la realización del trámite catastral de los predios marcados");
                } else {
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    this.addMensajeError(
                        "No se pudo enviar la notificación al director/es territorial de los predios que requieren trámites catastrales.");
                    context.addCallbackParam("error", "error");
                }

            }
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón 'Aceptar' del dialogo de modificar predio. Guarda el predio modificado
     * siempre y cuando el valor de cotización sea valido {@link #validarValorCotizacionAjustado()}
     *
     * @author christian.rodriguez
     */
    public void guardarPredio() {
        if (this.predioSeleccionado != null) {

            RequestContext context = RequestContext.getCurrentInstance();

            if (this.validarValorCotizacionAjustado()) {

                this.predioSeleccionado = this.getAvaluosService()
                    .guardarActualizarAvaluoPredio(this.predioSeleccionado);

                if (this.predioSeleccionado != null) {
                    this.cargarAvaluoSeleccionado();
                    this.addMensajeInfo("Se modificó el predio satisfactoriamente");
                } else {
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("No fue posible guardar las modificaciones del predio.");
                }
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Valida si el valor de cotización esta entre el rango permitido. +- el 10% del valor de
     * cotización ajustado
     *
     * @author christian.rodriguez
     * @return true si el valor esta en el rango permitido, false si no
     */
    private boolean validarValorCotizacionAjustado() {

        Double valorCotizacion = this.predioSeleccionado.getValorCotizacion();

        RequestContext context = RequestContext.getCurrentInstance();

        if (valorCotizacion == null) {
            return true;
        }

        Double valoCotizacionMenos10Porciento = valorCotizacion -
             (valorCotizacion * 0.10);

        Double valoCotizacionMas10Porciento = valorCotizacion +
             (valorCotizacion * 0.10);

        if (this.predioSeleccionado.getValorCotizacionAjustado() <= valoCotizacionMas10Porciento &&
             this.predioSeleccionado.getValorCotizacionAjustado() >= valoCotizacionMenos10Porciento) {
            return true;
        }

        context.addCallbackParam("error", "error");
        this.addMensajeError(
            "El valor de cotización ajustado está por fuera del rango del valor preliminar más el IVA. " +
             "Intentelo nuevamente.");

        return false;

    }

    // -------------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia elvalor cotizado ajustado. Valida que el valor
     * ajustado este en el rango permitido y se activa/desactiva la obligacion de ingresar
     * justificacio para el valor
     *
     * @author christian.rodriguez
     */
    public void onChangeValorAjustado() {

        this.validarValorCotizacionAjustado();
        this.determinarObligatoriedadJustificacionAjuste();

    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón 'Consultar ofertas inmobiliarias'. Ejecuta el caso de uso de consulta de
     * ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @return Dirección del caso de uso de consulta de ofertas inmobiliarias
     */
    public String consultarOfertasInmobiliarias() {

        ConsultaOfertasInmobiliariasMB consultaOfertasMB =
            (ConsultaOfertasInmobiliariasMB) UtilidadesWeb
                .getManagedBean("consultaOfertasInmob");

        consultaOfertasMB.cargarDesdeCUCotizarCostoAValuo(this.avaluo
            .getEstructuraOrganizacionalCod());

//PENDING :: ?? :: 25-04-2013 :: revisar por qué remueven el MB :: pedro.garcia    
        UtilidadesWeb.removerManagedBean("cotizarCostoAvaluo");

        return "/avaluos/ofertasInmob/consultaOfertas.jsf";
    }

    // -------------------------------------------------------------------------/
    /**
     * Listener del botón 'Proyectar cotización' se encarga de validar que se pueda proyectar la
     * cotización y mover el proceso
     *
     * @author christian.rodriguez
     */
    public void proyectarCotizacion() {

        if (this.avaluo != null && this.avaluo.getAvaluoPredios() != null) {

            for (AvaluoPredio predio : this.avaluo.getAvaluoPredios()) {

                if (predio.getValorCotizacionAjustado() <= 0) {

                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("Se debe cotizar todos los predios asociados al " +
                         "Sec. Radicado, porque hay valores Cotizados en Cero (0),  " +
                         "intente nuevamente");
                    return;
                }
            }
        }

        this.enviarNotificacionDeTramitesCatastralesParaPredios();
        this.forwardProcess();

    }

    // -------------------------------------------------------------------------
    /**
     * Método que se encargarÁ de mover el proceso
     *
     * @author christian.rodriguez
     */
    private void forwardProcess() {

        throw new NotImplementedException("falta lo del BPM");
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }
}
