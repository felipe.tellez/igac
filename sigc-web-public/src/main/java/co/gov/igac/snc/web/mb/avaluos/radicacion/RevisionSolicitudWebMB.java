package co.gov.igac.snc.web.mb.avaluos.radicacion;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.AdministracionDocumentacionAvaluoComercialMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso CU-SA-AC-150 revisar solicitud web y solicitar correcciones
 *
 * @cu CU-SA-AC-150
 *
 * @author felipe.cadena
 */
@Component("revisarSolicitudWeb")
@Scope("session")
public class RevisionSolicitudWebMB extends SNCManagedBean implements Serializable {

    /**
     * Serial autogenerado
     */
    private static final long serialVersionUID = -6553660079198649432L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionDocumentacionAvaluoComercialMB.class);
    /**
     * Solicitud que se va a revisar
     *
     */
    private Solicitud solicitud;

    /**
     * usuario actual del sistema
     */
    private UsuarioDTO usuario;

    /**
     * Variable para guardar las correcciones solicitadas
     */
    private String correccionesSolicitadas;

    /**
     * Nombre del usuario externo que diligencio la solicitud.
     */
    private String nombreUsuarioExterno;

    /**
     * Correo del usuario externo que diligencio la solicitud.
     */
    private String correoUsuarioExterno;

    /**
     * MB que gestiona la información de la solicitud.
     */
    @Autowired
    private ConsultaInformacionSolicitudMB consultaSolicitudMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * MB para el manejo de las tareas del proceso.
     */
    @Autowired
    TareasPendientesMB tareasPendientes;

    // --------getters-setters----------------
    public String getCorreccionesSolicitadas() {
        return this.correccionesSolicitadas;
    }

    public void setCorreccionesSolicitadas(String correccionesSolicitadas) {
        this.correccionesSolicitadas = correccionesSolicitadas;
    }

    public String getNombreUsuarioExterno() {
        return nombreUsuarioExterno;
    }

    public void setNombreUsuarioExterno(String nombreUsuarioExterno) {
        this.nombreUsuarioExterno = nombreUsuarioExterno;
    }

    public String getCorreoUsuarioExterno() {
        return correoUsuarioExterno;
    }

    public void setCorreoUsuarioExterno(String correoUsuarioExterno) {
        this.correoUsuarioExterno = correoUsuarioExterno;
    }

    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on RevisionSolicitudWebMB init");
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {
        //datos de prueba

//TODO :: felipe.cadena::definir de donde llegan los datos del usuario externo.
        this.solicitud = new Solicitud();
        this.solicitud.setNumero("60402012ER00041214");
        nombreUsuarioExterno = "felipe.cadena";
        correoUsuarioExterno = "felipe.cadena@igac.gov.co";

        //Fin datos prueba
        this.consultaSolicitudMB.setNumeroRadicacion(this.solicitud.getNumero());
        this.consultaSolicitudMB.consultarSolicitud();

    }

    /**
     * Método para aprobar la información de la solicitud
     *
     * Se llama desde revisarSolicitudWeb.xhtml (confirmDialogWV="confirmacionAprobarSolicitudWV")
     *
     * @author
     */
    public void aprobarSolicitud() {

        LOGGER.debug("Aprobando solicitud...");

        this.solicitud.setEstado(ESolicitudEstado.INGRESADA.getCodigo());

//TODO :: felipe.cadena :: 23-04-2013 :: ¿hay que actualizar la solicitud en bd? :: pedro.garcia      
        //this.getAvaluosService().guardar
//TODO :: felipe.cadena::se debe definir el manejo de la actividad en el process
        LOGGER.debug("Solicitud aprobada");

    }

    /**
     * Método para solicitar correcciones, envia un correo al usuario con la lista de correcciones
     * que debe realizar.
     *
     * Se llama desde revisarSolicitudWeb.xhtml (p:commandButton id="correccionesBtn")
     *
     * @author
     */
    public void solicitarCorrecciones() {

        LOGGER.debug("Solicitando correcciones...");

        LOGGER.debug("Correcciones>>> " + this.getCorreccionesSolicitadas());

        List<String> destinatarios = new ArrayList<String>();

        destinatarios.add(this.correoUsuarioExterno);

        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};

        Object[] datosCorreo = new Object[6];
        datosCorreo[0] = this.usuario.getLogin();
        datosCorreo[1] = this.correoUsuarioExterno;
        datosCorreo[2] = this.nombreUsuarioExterno;
        datosCorreo[3] = this.solicitud.getTipo();
        datosCorreo[4] = this.correccionesSolicitadas;
        datosCorreo[5] = this.usuario.getNombreCompleto();

        boolean notificacionExitosa = this
            .getGeneralesService()
            .enviarCorreoElectronicoConCuerpo(
                EPlantilla.MENSAJE_SOLICITUD_CORRECCIONES_WEB,
                destinatarios, null, null, datosCorreo, adjuntos,
                adjuntosNombres);
//TODO :: felipe.cadena::pendiente de la creacion del reporte.
        //guardarDocumentoCorrecciones();

        if (notificacionExitosa) {
            this.addMensajeInfo("Se envió exitosamente el oficio por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo enviar la solicitud de correcciones.");
            context.addCallbackParam("error", "error");
        }

        LOGGER.debug("Correciones solicitadas");

    }

    /**
     * Método para guardar documento de constancia de la solicitud de de correcciones.
     *
     * @author felipe.cadena
     */
    public void guardarDocumentoCorrecciones() {

//TODO::javier.aponte::se debe generar el reporte en el servidor de reportes::felipe.cadena
        String urlReporte =
            EReporteServiceSNC.OFICIO_NO_PROCEDENCIA_AUTOAVALUO_REVISION_Y_VIA_GUBERNATIVA.
                getUrlReporte();

        ReporteDTO reporteDTO;
        File file;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NOMBRE_USUARIO_RADICADOR", "" + this.usuario.getLogin());
        parameters.put("NOMBRE_USUARIO_EXTERNO", "" + nombreUsuarioExterno);
        parameters.put("CORREO_USUARIO_EXTERNO", "" + correoUsuarioExterno);
        parameters.put("CORRECCIONES", "" + correccionesSolicitadas);

        reporteDTO = this.reportsService.generarReporte(parameters,
            EReporteServiceSNC.OFICIO_NO_PROCEDENCIA_AUTOAVALUO_REVISION_Y_VIA_GUBERNATIVA,
            this.usuario);
        file = reporteDTO.getArchivoReporte();

//TODO :: felipe.cadena :: 20-06-2013 :: no se está haciendo nada con el reporte generado. Solo se usa el nombre delñ archivo. ¿entonces qué pasó son el reporte generado? :: pedro.garcia   
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.OTRO.getId());
        Documento docSolicitudCorrecciones = new Documento();
        docSolicitudCorrecciones.setCantidadFolios(1);
        docSolicitudCorrecciones.setDescripcion("Solicitud de correcciones a solicitud");
        docSolicitudCorrecciones.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        docSolicitudCorrecciones.setEstructuraOrganizacionalCod(usuario.getCodigoTerritorial());
        docSolicitudCorrecciones.setFechaDocumento(new Date());
        docSolicitudCorrecciones.setFechaLog(new Date());
        docSolicitudCorrecciones.setUsuarioLog(usuario.getLogin());
        docSolicitudCorrecciones.setBloqueado(ESiNo.NO.getCodigo());
        docSolicitudCorrecciones.setArchivo(file.getName());

        //objeto para almacenar la informacion a cargar en alfresco
        DocumentoSolicitudDTO docSolicitudDTO = DocumentoSolicitudDTO.
            crearArgumentoCargarSolicitudAvaluoComercial(
                this.solicitud.getNumero(),
                this.solicitud.getFecha(),
                ETipoDocumento.OTRO.getNombre(),
                UtilidadesWeb.obtenerRutaTemporalArchivos() + docSolicitudCorrecciones.getArchivo());

        docSolicitudCorrecciones = this.getGeneralesService().guardarDocumento(
            docSolicitudCorrecciones);

        this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(usuario, docSolicitudDTO,
                docSolicitudCorrecciones);

        //objeto para relacionar el documento con la solicitud
        SolicitudDocumento solicitudDocumento = new SolicitudDocumento();
        solicitudDocumento.setFecha(new Date());
        solicitudDocumento.setSoporteDocumento(docSolicitudCorrecciones);
        solicitudDocumento.setSolicitud(this.solicitud);
        solicitudDocumento.setFechaLog(new Date());
        solicitudDocumento.setUsuarioLog(usuario.getLogin());
        solicitudDocumento.setIdRepositorioDocumentos(docSolicitudCorrecciones.
            getIdRepositorioDocumentos());

        this.getTramiteService()
            .guardarActualizarSolicitudDocumento(solicitudDocumento);

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaInformacionSolicitudMB#cerrar");
        return tareasPendientes.cargarInit();
    }
}
