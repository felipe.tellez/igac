/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * @author juan.agudelo
 *
 * @version 2.0
 * @description Clase que hace las veces de managed bean para detalleDocumento para el componenete
 * docViewer, el nombre del MB no es el mismo que el de la clase por problemas con subversion
 * @cu se usa en varios en donde se requiere visualizar un documento
 */
/*
 * @modified felipe.cadena -- Se agrega el atributo streamDocumento
 */
@Component("previsualizacionDocumento")
@Scope("session")
public class PrevisualizacionDocMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = -5172998811646733085L;
    private static final Logger LOGGER = LoggerFactory.getLogger(PrevisualizacionDocMB.class);

    // ------------------------------------------------------------------------
    /**
     * Ruta del documento, este atributo se debe utilizar cuando no se cuenta con un documento sino
     * unicamente con el nombre del archivo
     */
    private String nombreArchivo;

    /**
     * Documento cargado del que se requiere una previsualización temporal. Esto quiere decir que el
     * documento que se va a visualizar es un documento temporal (aún no guardado en alfresco)
     * ubicado en la carpeta de archivos temporales.
     *
     * Este es el atributo que se debe definir con el setPropertyActionListener en donde se llama al
     * componente de previsualización de documentos
     */
    private Documento documentoTemporal;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Documento cargado del que se requiere cargar desde alfresco. Esto quiere decir que el
     * documento que se va a visualizar es un documento ya guardado en alfresco. El setter de este
     * busca el documento en alfresco y lo guarda en la carpeta de archivos temporales
     *
     * Este es el atributo que se debe definir con el setPropertyActionListener en donde se llama al
     * componente de previsualización de documentos
     *
     */
    private Documento documentoAlfresco;

    /**
     * Ruta temporal del archivo
     */
    private String rutaArchivoTemporal;

    /**
     * Tipo MIME del documento seleccionado
     */
    private String tipoMimeDocumento;

    /**
     * Mensaje de salida de la previsualización de documento
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * Stream para poder descargar el documento desde PrimeFaces se debe construir el
     * StreamedContent en el getter del atributo para que se mantenga en diferentes llamados del
     * componente.
     */
    private StreamedContent streamDocumento;

    /**
     * DTO para procesar los archivos que se deben visualizar
     */
    private ReporteDTO reporteDTO;

    // ----------------------------- methods ----------------------------------
    public Documento getDocumentoTemporal() {
        return this.documentoTemporal;
    }

    /**
     * Este atributo se setea solo cuando se va a consultar apenas se ha subido
     *
     * @param documentoTemporal
     */
    public void setDocumentoTemporal(Documento documentoTemporal) {

        File archivoTemporal = null;

        if (!documentoTemporal.getArchivo().contains(FileUtils.getTempDirectory().getAbsolutePath())) {

            archivoTemporal = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                documentoTemporal.getArchivo());
        } else {
            archivoTemporal = new File(documentoTemporal.getArchivo());
        }

        if (archivoTemporal != null && archivoTemporal.exists()) {
            this.reporteDTO = this.reportsService.generarReporteDesdeUnFile(archivoTemporal);

            if (this.reporteDTO != null) {
                this.rutaArchivoTemporal = this.reporteDTO.getUrlWebReporte();
            }
            this.nombreArchivo = documentoTemporal.getArchivo();
        }

        this.documentoTemporal = documentoTemporal;
    }
//--------------------------------------------------------------------------------------------------

    public Documento getDocumentoAlfresco() {
        return this.documentoAlfresco;
    }
//-------------------------------------------------

    /**
     *
     * Este atributo se setea cuando ya existe el documento en alfresco. Es el que se debe usar en
     * el componente para que al hacer la asignación de este atributo se haga la consulta del doc en
     * el alfresco
     *
     * @param documentoAlfresco
     */
    public void setDocumentoAlfresco(Documento documentoAlfresco) {

        if (documentoAlfresco.getIdRepositorioDocumentos() != null &&
             !documentoAlfresco.getIdRepositorioDocumentos().isEmpty()) {

            this.reporteDTO = this.reportsService
                .consultarReporteDeGestorDocumental(
                    documentoAlfresco.getIdRepositorioDocumentos());
            if (this.reporteDTO != null) {
                this.rutaArchivoTemporal = this.reporteDTO.getUrlWebReporte();
            }
            this.nombreArchivo = documentoAlfresco.getArchivo();
        } else {
            this.rutaArchivoTemporal = null;
        }
        this.documentoAlfresco = documentoAlfresco;
    }

    /**
     *
     * Se utiliza para visualizar un documento en general, si tiene un id de alfresco se consulta en
     * alfresco, sino se trae del temporal.
     *
     * @author felipe.cadena
     * @param documentoAlfresco
     */
    public void setDocumentoGeneral(Documento documento) {

        if (documento.getIdRepositorioDocumentos() == null ||
            documento.getIdRepositorioDocumentos().isEmpty()) {
            this.setDocumentoTemporal(documento);
        } else {
            this.setDocumentoAlfresco(documento);
        }
    }

    public Documento getDocumentoGeneral() {
        return this.documentoAlfresco;
    }

//--------------------------------------------------------------------------------------------------
    public String getNombreArchivo() {
        return this.nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public ReporteDTO getReporteDTO() {
        return this.reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
        this.setRutaArchivoTemporal(reporteDTO.getUrlWebReporte());
    }

    public String getRutaArchivoTemporal() {
        return this.rutaArchivoTemporal;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return mensajeSalidaConfirmationDialog;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public void setRutaArchivoTemporal(String rutaArchivoTemporal) {
        this.rutaArchivoTemporal = rutaArchivoTemporal;
    }

    public String getTipoMimeDocumento() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();

        if (this.documentoTemporal != null) {
            tipoMimeDocumento = fileNameMap
                .getContentTypeFor(this.documentoTemporal.getArchivo());
        } else if (this.documentoAlfresco != null) {
            tipoMimeDocumento = fileNameMap
                .getContentTypeFor(this.documentoAlfresco.getArchivo());
        }

        return tipoMimeDocumento;
    }

    public void setTipoMimeDocumento(String tipoMimeDocumento) {
        this.tipoMimeDocumento = tipoMimeDocumento;
    }

    public StreamedContent getstreamDocumento() {

        InputStream stream;
        String urlReporte;

        String fileSeparator = System.getProperty("file.separator");

        if (this.documentoAlfresco != null) {
            urlReporte = FileUtils.getTempDirectory().getAbsolutePath() + this.getGeneralesService()
                .descargarArchivoDeAlfrescoATemp(
                    this.documentoAlfresco.getIdRepositorioDocumentos());
        } else {
            urlReporte = FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                this.documentoTemporal.getArchivo();
        }

        File file = new File(urlReporte);
        try {
            stream = new FileInputStream(file);
            this.streamDocumento = new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_PDF, "Oficio.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte");
        }

        return streamDocumento;
    }

    public void setStreamDocumento(StreamedContent streamDocumento) {
        this.streamDocumento = streamDocumento;
    }

    // --------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del previsualizacionDocumentoMB");

        this.mensajeSalidaConfirmationDialog = "";
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la previsualización del documento, si se deseea una implementación
     * particular se puede redireccionar al método particular en el MB propio
     */
    public void cerrarVistaPreliminarDocumento() {
        this.documentoTemporal = null;
        this.documentoAlfresco = null;
    }

    // --------------------------- //
    /**
     * Método que verifica si el formato del archivo es Dwg, esto con el fin de no mostrarlo en los
     * previsualizadores de documentos, si no por el contrario bloquear esta opción y permitirlo
     * descargar.
     *
     * @return
     */
    public boolean isDocumentoDwg() {
        if (this.nombreArchivo != null && !this.nombreArchivo.trim().isEmpty()) {

            StringTokenizer auxToken = new StringTokenizer(this.nombreArchivo,
                ".");
            String formato = null;
            while (auxToken.hasMoreElements()) {
                formato = (String) auxToken.nextElement();
            }
            if (formato.toUpperCase().equals("DWG")) {
                return true;
            }
        }
        return false;
    }
    // end of class
}
