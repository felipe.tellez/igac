package co.gov.igac.snc.web.mb.actualizacion.reconocimiento;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * ManagedBean para el CU 308 de reconocimiento : Generar muestro control de calidad.
 *
 * @author andres.eslava
 *
 */
@Component("GeneracionMuestraControlDeCalidad")
@Scope("session")
public class GeneracionMuestraControlDeCalidadMB extends
    SNCManagedBean {

    /**
     * Atributo de serialización
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
}
