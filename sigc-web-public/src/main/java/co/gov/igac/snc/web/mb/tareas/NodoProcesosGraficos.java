package co.gov.igac.snc.web.mb.tareas;

import org.primefaces.model.TreeNode;

import co.gov.igac.snc.apiprocesos.contenedores.NodoObjetoProceso;

public class NodoProcesosGraficos {

    private TreeNode treeNode;
    private NodoObjetoProceso objetoProceso;

    public NodoProcesosGraficos(TreeNode treeNode, NodoObjetoProceso padre) {
        this.treeNode = treeNode;
        this.objetoProceso = padre;
    }

    public TreeNode getTreeNode() {
        return treeNode;
    }

    public void setTreeNode(TreeNode treeNode) {
        this.treeNode = treeNode;
    }

    public NodoObjetoProceso getObjetoProceso() {
        return objetoProceso;
    }

    public void setObjetoProceso(NodoObjetoProceso objetoProceso) {
        this.objetoProceso = objetoProceso;
    }

}
