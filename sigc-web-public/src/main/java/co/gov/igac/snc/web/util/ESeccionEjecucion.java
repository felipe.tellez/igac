package co.gov.igac.snc.web.util;

/**
 * Enumeración con los nombres de los tabs de las opciones de datos en la proyección de un trámite
 *
 * @documented pedro.garcia
 */
public enum ESeccionEjecucion {
    JUSTIFICACION_PROPIEDAD("Justificación de propiedad"),
    PROPIETARIOS("Propietarios y/o poseedores"),
    UBICACION("Datos generales del predio"),
    FICHA_MATRIZ("Ficha matriz"),
    AVALUOS("Detalle del avalúo"),
    TEXTO_MOTIVADO("Texto motivado"),
    INSCRIPCIONES_DECRETOS("Inscripciones/Decretos"),
    ASOCIAR_DOCUMENTOS("Asociar documentos"),
    PROYECCION_TRAMITE("Proyección del trámite"),
    HISTORICO_MODIFICACIONES("Histórico de modificaciones");

    private String nombre;

    ESeccionEjecucion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
}
