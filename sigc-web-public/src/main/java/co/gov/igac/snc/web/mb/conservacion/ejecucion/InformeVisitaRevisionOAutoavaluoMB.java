/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EComisionTramiteResultAv;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ComisionTramiteDatoAux;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el caso de uso Realizar Informe de visita de Revisión o Autoavalúo CU-NP-CO-168
 *
 * @author pedro.garcia
 */
@Component("informeVisitaROAA")
@Scope("session")
public class InformeVisitaRevisionOAutoavaluoMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = 5608198030936651458L;

    private static final Logger LOGGER =
        LoggerFactory.getLogger(InformeVisitaRevisionOAutoavaluoMB.class);

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private GeneralMB generalMBService;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    //------------ banderas
    /**
     * indica si ya se había guardado el informe
     */
    private boolean guardoDatosInforme;

    /**
     * indica si ya se generó el informe
     */
    private boolean generoInforme;

    /**
     * variable que indica si hay error cuando el trámite es de terreno y no tiene una comisión
     * asociada
     */
    private boolean error;

    /**
     * fecha de elaboración del informe. Corresponde a la fecha del sistema
     */
    private Date fechaInforme;
    private Date currentDate;

    /**
     * Lista de items para el resultado del trámite (avalúo)
     */
    private List<SelectItem> tramiteMustBeOptions;

    /**
     * opción escogida de la lista tramiteMustBeOptions
     */
    private String tramiteMustBe;

    /**
     * lista que contiene todos los datos para ser pintados en la tabla para marcarlos como
     * verificados o modificados
     */
    private List<ComisionTramiteDatoAux> datosConChecks;

    /**
     * objeto que se va a modificar dependiendo de los valores dados en la forma
     */
    private ComisionTramite currentComisionTramite;

    /**
     * trámite sober el que se está trabajando
     */
    private Tramite currentTramite;

    /**
     * id del trámite seleccionado de la tabla
     */
    private long currentTramiteId;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    private boolean banderaTramiteAsociado;

    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentProcessActivity;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de informe de visita
     */
    private ReporteDTO reporteInformeVisita;

    //-------------     methods ----
    public ReporteDTO getReporteInformeVisita() {
        return reporteInformeVisita;
    }

    public void setReporteInformeVisita(ReporteDTO reporteInformeVisita) {
        this.reporteInformeVisita = reporteInformeVisita;
    }

    public boolean isBanderaTramiteAsociado() {
        return banderaTramiteAsociado;
    }

    public void setBanderaTramiteAsociado(boolean banderaTramiteAsociado) {
        this.banderaTramiteAsociado = banderaTramiteAsociado;
    }

    public boolean isGeneroInforme() {
        return this.generoInforme;
    }

    public void setGeneroInforme(boolean generoInforme) {
        this.generoInforme = generoInforme;
    }

    public boolean isGuardoDatosInforme() {
        return this.guardoDatosInforme;
    }

    public void setGuardoDatosInforme(boolean guardoInforme) {
        this.guardoDatosInforme = guardoInforme;
    }

    public String getTramiteMustBe() {
        return this.tramiteMustBe;
    }

    public void setTramiteMustBe(String tramiteMustBe) {
        this.tramiteMustBe = tramiteMustBe;
    }

    public Tramite getCurrentTramite() {
        return this.currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
    }

    public List<ComisionTramiteDatoAux> getDatosConChecks() {
        return this.datosConChecks;
    }

    public void setDatosConChecks(List<ComisionTramiteDatoAux> datosConChecks) {
        this.datosConChecks = datosConChecks;
    }

    public Date getCurrentDate() {
        return this.currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public ComisionTramite getCurrentComisionTramite() {
        return this.currentComisionTramite;
    }

    public void setCurrentComisionTramite(ComisionTramite currentComisionTramite) {
        this.currentComisionTramite = currentComisionTramite;
    }

    public List<SelectItem> getTramiteMustBeOptions() {
        return this.tramiteMustBeOptions;
    }

    public void setTramiteMustBeOptions(List<SelectItem> tramiteMustBeOptions) {
        this.tramiteMustBeOptions = tramiteMustBeOptions;
    }

    public Date getFechaInforme() {
        return this.fechaInforme;
    }

    public void setFechaInforme(Date fechaInforme) {
        this.fechaInforme = fechaInforme;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.error = false;

        LOGGER.debug("on InformeVisitaRevisionOAutoavaluoMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        this.fechaInforme = new Date(System.currentTimeMillis());
        this.currentDate = this.fechaInforme;

        fillDatosConCheck();

        this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        //D: se obtiene el id del trámite
        this.currentTramiteId = this.currentProcessActivity.getIdObjetoNegocio();

        this.currentTramite =
            this.getTramiteService().buscarTramiteSolicitudPorId(this.currentTramiteId);

        List<TramiteDocumento> documentosPruebas = this.getTramiteService()
            .obtenerTramiteDocumentosDePruebasPorTramiteId(
                this.currentTramite.getId());
        if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
            this.currentTramite.setDocumentosPruebas(documentosPruebas);
        }
        this.tramiteMustBeOptions = this.generalMBService.getComisionTramiteResultAvPorTipoTramiteV(
            this.currentTramite.getTipoTramite());

        List<ComisionTramite> comisiones = this.getTramiteService().
            buscarComisionesTramitePorIdTramite(
                this.currentTramiteId);
        //Si el tramite tiene tramites asociados se deshabilita el campo Modificado.
        this.inicializarModificados();

        if (comisiones != null &&
             comisiones.size() > 0) {
            // N: solo debería haber una comisión para un trámite
            this.currentComisionTramite = this
                .getTramiteService()
                .buscarComisionesTramitePorIdTramite(this.currentTramiteId).get(0);
        } else {

            //N: javier.aponte : sólo puede llegar a este punto sin comisión asociada en el caso que el trámite
            //sea de oficina, por lo cual se crea una comisión ficticia
            if (!this.currentTramite.isTramiteTerreno()) {

                Comision comision = new Comision();
                Date fechaActual = new Date();

                comision.setDuracionAjuste(Double.valueOf(0));
                comision.setDuracionTotal(Double.valueOf(0));
                comision.setEstado(EComisionEstado.CREADA.getCodigo());
                comision.setObservaciones("Comisión ficticia creada para trámites de oficina, " +
                    "necesaria para el informe de autoavalúo sustentando el concepto");
                comision.setUsuarioLog(this.currentUser.getLogin());
                comision.setFechaLog(fechaActual);
                comision.setFechaInicio(fechaActual);
                comision.setFechaFin(fechaActual);

                comision = this.getTramiteService().insertarComision(comision, this.currentUser);

                if (comision != null) {

                    ComisionEstado comisionEstado = new ComisionEstado();
                    comisionEstado.setComision(comision);
                    comisionEstado.setFecha(new Date());
                    comisionEstado.setEstado(EComisionEstado.CREADA.getCodigo());
                    comisionEstado.setMotivo(EComisionEstadoMotivo.OTRO.getCodigo());
                    comisionEstado.setUsuarioLog(this.currentUser.getLogin());
                    comisionEstado.setFechaLog(new Date());
                    this.getTramiteService().
                        insertarComisionEstado(comisionEstado, this.currentUser);

                    this.currentComisionTramite = new ComisionTramite();
                    this.currentComisionTramite.setComision(comision);
                    this.currentComisionTramite.setTramite(this.currentTramite);
                    this.currentComisionTramite.setUsuarioLog(this.currentUser.getLogin());
                    this.currentComisionTramite.setFechaLog(new Date());
                    this.getTramiteService().actualizarComisionTramite(this.currentComisionTramite);
                    this.currentComisionTramite = this.getTramiteService().
                        buscarComisionesTramitePorIdTramite(this.currentTramiteId).get(0);

                }
            } else {
                this.error = true;
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo para establecer el estado inicial de los checks del campo modificado.
     *
     *
     * @author felipe.cadena
     *
     */
    /*
     * @modified by leidy.gonzalez:: 12582:: 25-05-2015 Se valida que para habilitar la bandera de
     * asociados el tramite no puedo haber estado rechazado, es decir en estado NO_APROBADO
     */
    public void inicializarModificados() {
        List<Tramite> tramitesAsociados = this.getTramiteService().
            consultarTramitesAsociadosATramiteId(this.currentTramite.getId());

        if (this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo())) {
            this.banderaTramiteAsociado = true;
            return;
        }

        this.banderaTramiteAsociado = false;
        this.tramiteMustBe = EComisionTramiteResultAv.CONFIRMA.getCodigo();
        if (tramitesAsociados != null) {
            if (tramitesAsociados.size() > 0) {
                this.tramiteMustBe = EComisionTramiteResultAv.MODIFICA.getCodigo();
                for (Tramite tramite : tramitesAsociados) {
                    if (!tramite.getEstado().equals(ETramiteEstado.NO_APROBADO.getCodigo()) ||
                        !tramite.getEstado().equals(ETramiteEstado.NO_APROBADO.getCodigo())) {
                        this.banderaTramiteAsociado = true;
                    }
                }
            }
        }

    }

    /**
     * Metodo para validar si al menos un de los campos modificados ha sido modificado.
     *
     *
     * @author felipe.cadena
     *
     */
    public boolean validarSeleccionModificado() {

        boolean estado = false;

        for (ComisionTramiteDatoAux ctda : this.datosConChecks) {
            if (ctda.isModificado()) {
                estado = true;
                break;
            }
        }
        return estado;
    }

    /**
     * Metodo para validar si al menos un de los campos modificados ha sido verificado.
     *
     *
     * @author felipe.cadena
     *
     */
    public boolean validarSeleccionVerificado() {

        boolean estado = false;

        for (ComisionTramiteDatoAux ctda : this.datosConChecks) {
            if (ctda.isVerificado()) {
                estado = true;
                break;
            }
        }
        return estado;
    }

    /**
     * action del botón "Guardar" Guarda los datos del informa de la visita en ld BD
     */
    /*
     * modified by leidy.gonzalez :: #12582:: 13-05-2015 Se agrega mensaje de error cuando no se ha
     * seleccionado el Dato a modificar, separandolo del mensaje de tramites asociados.
     */
    public void guardarInformeVisita() {

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

        String tipoTramite = generalMB.obtenerNombreDominioPorCodigo(
            this.currentTramite.getTipoTramite(), EDominio.TRAMITE_TIPO_TRAMITE);

        //Elimina los datos anteriores en comision tramite dato
        //D: debido a que puede guardar cuantas veces quiera, es posible que ya haya guardado
        //   y por lo tanto se deben borrar los registros previos
        this.getTramiteService().borrarComisionTramiteDatosPorComisionTramite(
            this.currentComisionTramite.getId());

        //validar restricciones por tramites relacionados.
        boolean tramitesAsociadosFinalizadosAprobados = false;

        List<Tramite> tramitesAsociados = this.getTramiteService().
            consultarTramitesAsociadosATramiteId(this.currentTramite.getId());

        if (tramitesAsociados != null && !tramitesAsociados.isEmpty()) {
            for (Tramite tramite : tramitesAsociados) {
                if (tramite.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.getCodigo())) {
                    tramitesAsociadosFinalizadosAprobados = true;
                    break;
                }
            }
        }

        if (this.banderaTramiteAsociado && !tramitesAsociadosFinalizadosAprobados &&
             this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.
                getCodigo())) {
            this.addMensajeError("La " + tipoTramite + " tiene trámites asociados");
            return;
        }

        if (!this.validarSeleccionVerificado() &&
             !this.banderaTramiteAsociado &&
             this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.
                getCodigo())) {
            this.addMensajeError("La " + tipoTramite + " no tiene trámites asociados," +
                 " por lo menos uno de los datos debe ser marcado como verificados. ");
            return;
        }

        if (!this.validarSeleccionVerificado() &&
             (this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.AUTOESTIMACION.
                getCodigo()) ||
             this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo()))) {
            this.addMensajeError("Por lo menos uno de los datos debe ser marcado como verificado. ");
            return;
        }

        if (this.banderaTramiteAsociado) {
            for (ComisionTramiteDatoAux ctdTemp : this.datosConChecks) {
                if (ctdTemp.isModificado() && !ctdTemp.isVerificado()) {
                    this.addMensajeError("Si un dato se marcó como 'modificado' también se debe " +
                         " marcar como 'verificado': " + ctdTemp.getDato());
                    return;
                }
            }
        }

        //D: definie los valores de la ComisionTramite que no están directamente mapeados en la página
        this.currentComisionTramite.setFechaInforme(this.fechaInforme);
        this.currentComisionTramite.setResultado(this.tramiteMustBe);

        //D: arma los objetos ComisionTramiteDato
        ComisionTramiteDato toInsert;
        List<ComisionTramiteDato> registersToInsert = new ArrayList<ComisionTramiteDato>();
        for (ComisionTramiteDatoAux ctdTemp : this.datosConChecks) {
            toInsert = new ComisionTramiteDato(ctdTemp.getDato());
            toInsert.setComisionTramite(this.currentComisionTramite);
            if (ctdTemp.isModificado()) {
                toInsert.setModificado(ESiNo.SI.getCodigo());
            }
            if (ctdTemp.isVerificado()) {
                toInsert.setVerificado(ESiNo.SI.getCodigo());
            }
            toInsert.setUsuarioLog(this.currentUser.getLogin());
            toInsert.setFechaLog(this.fechaInforme);

            registersToInsert.add(toInsert);
        }

        //D: hace merge de la ComisionTramite
        this.getTramiteService().actualizarComisionTramite(this.currentComisionTramite);

        //D: inserta los ComisionTramiteDato
        try {
            //D: debido a que puede guardar cuantas veces quiera, es posible que ya haya guardado
            //   y por lo tanto se deben borrar los registros previos
            // Esto se implementó al principio del método guardar ya que si se salia del caso de uso
            // sin anvanzar el proceso, al volver la variable guardar datos Informe estaba en false
            // y volvia a guardar en la base de datos
//            if (this.guardoDatosInforme) {
//                this.getTramiteService().borrarComisionTramiteDatosPorComisionTramite(
//                    this.currentComisionTramite.getId());
//            }
            this.getTramiteService().insertarRegistrosComisionTramiteDato(registersToInsert);

            this.guardoDatosInforme = true;
        } catch (Exception e) {
            LOGGER.error(
                "error insertando registros en Itramite#insertarRegistrosComisionTramiteDato :" +
                e.getMessage(), e);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Generar informe".
     *
     * Genera el reporte con el informe y lo deja en la variable que se usa como value en el
     * componente p:download de la página donde se pinta
     */
    public void generarInforme() {

        EReporteServiceSNC enumeracionReporte;
        Map<String, String> parameters;

        enumeracionReporte =
            EReporteServiceSNC.INFORME_SUSTENTANDO_CONCEPTO_DE_AUTOAVALUO_O_REVISION;
        parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", String.valueOf(this.currentTramiteId));
        parameters.put("NUMERO_COMISION", this.currentComisionTramite.getComision().getNumero());
        parameters.put("NUMERO_SOLICITUD", this.currentTramite.getSolicitud().getNumero());

        //D: primero se genera el reporte y luego se carga como archivo
        this.reporteInformeVisita =
            this.reportsService.generarReporte(parameters, enumeracionReporte, this.currentUser);

        if (this.reporteInformeVisita != null) {
            this.generoInforme = true;
        }

    }
//--------------------------------------------------------------------------------------------------
    //----------- bpm

    /**
     * nada se debe validar respecto al proceso
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------

    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO message = new ActivityMessageDTO();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();
        String transitionName;
        UsuarioDTO usuarioDestinoActividad = null;

        transitionName = ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA;
        message.setActivityId(this.currentProcessActivity.getId());
        usuarioDestinoActividad = this.currentUser;

        usuariosActividad.add(usuarioDestinoActividad);
        solicitudCatastral.setUsuarios(usuariosActividad);
        message.setTransition(transitionName);
        solicitudCatastral.setTransicion(transitionName);
        message.setUsuarioActual(usuarioDestinoActividad);

        message.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(message);

        if (this.getMessage().getComment() != null) {
            message.setComment(this.getMessage().getComment());
        }

    }
//--------------------------------------------------

    /**
     * El trámite no cambia de estado en esta actividad
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {
        // do nothing
    }
//--------------------------------------------------------------------------------------------------

    /**
     * llena la lista de objetos que van a ser pintados en la tabla de datos con checks
     *
     */
    private void fillDatosConCheck() {

        List<Dominio> listaDominioValores;
        ComisionTramiteDatoAux newCTD;

        listaDominioValores = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.COMISION_TRAMITE_DATO_DATO);

        this.datosConChecks = new ArrayList<ComisionTramiteDatoAux>();
        for (Dominio currentDominio : listaDominioValores) {
            newCTD = new ComisionTramiteDatoAux(currentDominio.getValor());
            this.datosConChecks.add(newCTD);
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * action del botón con el mismo nombre
     */
    public String avanzarProceso() {

        try {
            //Guarda el documento en Alfresco y en la base de datos y lo asocia con
            //un trámite documento
            this.guardarDocumentoInformeVisitaRevisionOAutoavaluo();
        } catch (Exception ex) {
            LOGGER.error("Error al guardar en alfresco el informe de revisión o autoavalúo: " + ex.
                getMessage(), ex);
            return null;
        }

        if (this.validateProcess()) {
            this.setupProcessMessage();

            try {
                this.doDatabaseStatesUpdate();
                this.forwardProcess();
            } catch (Exception e) {
                LOGGER.error("error al mover proceso en InformeVisitaRevisionOAutoavaluoMB: " +
                    e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en procesos. Comuníquese con el " +
                    " administrador del sistema");
            }

        }

        //OJO: hay necesidad de hacer esto aquí porque no se usa el template que llama a ProcessMB.forwardProcess
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("informeVisitaROAA");

        //D: forzar el refresco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrescan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        UtilidadesWeb.removerManagedBean("informeVisitaROAA");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en Alfresco y
     * en la base de datos mediante el llamado al método guardar documento de la interfaz de
     * ITramite. Posteriormente le asocia el documento al trámite documento creado al principio.
     *
     * @author javier.aponte
     * @throws Exception
     */
    public void guardarDocumentoInformeVisitaRevisionOAutoavaluo() throws Exception {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Documento documento = new Documento();

        tramiteDocumento.setTramite(this.currentTramite);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.currentUser.getLogin());

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.INFORME_VISITA_REVISION_O_AUTOAVALUO
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.reporteInformeVisita != null) {
                documento.setArchivo(this.reporteInformeVisita.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(this.currentTramite.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.currentUser.getLogin());
            documento.setUsuarioLog(this.currentUser.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.currentUser.
                getCodigoEstructuraOrganizacional());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.currentUser, documento);

            if (documento != null &&
                 documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos()
                    .trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                tramiteDocumento = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumento);
            }

        } catch (Exception e) {
            throw new Exception("Error al guardar el informe de visita de revisión o autoavalúo: " +
                e.getMessage());
        }
    }

//end of class
}
