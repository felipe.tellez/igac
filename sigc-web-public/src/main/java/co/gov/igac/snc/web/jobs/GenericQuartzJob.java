package co.gov.igac.snc.web.jobs;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * http://gravspace.wordpress.com/2011/03/07/quartz-scheduling-spring-hibernate-and-clustering-here-be-dragons/
 *
 * @author juan.mendez
 *
 */
public class GenericQuartzJob extends QuartzJobBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -285752870921174876L;

    private static final Logger LOGGER = Logger.getLogger(GenericQuartzJob.class);

    private String batchProcessorName;

    public String getBatchProcessorName() {
        return batchProcessorName;
    }

    public void setBatchProcessorName(String name) {
        this.batchProcessorName = name;
    }

    /**
     *
     */
    @Override
    protected void executeInternal(JobExecutionContext jobCtx) throws JobExecutionException {
        try {
            SchedulerContext schedCtx = jobCtx.getScheduler().getContext();

            ApplicationContext appCtx = (ApplicationContext) schedCtx.get("applicationContext");
            java.lang.Runnable proc = (java.lang.Runnable) appCtx.getBean(this.batchProcessorName);
            proc.run();
        } catch (Exception ex) {
            throw new JobExecutionException(ex);
        }
    }
}
