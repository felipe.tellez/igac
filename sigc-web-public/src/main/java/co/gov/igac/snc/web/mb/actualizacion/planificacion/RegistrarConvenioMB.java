package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.actualizacion.ConvenioEntidad;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EConvenioEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.ServicioTemporalDocumento;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @description Managed bean para registrar la información correspondiente al diagnóstico de un
 * municipio.
 *
 * @author david.cifuentes
 */
@Component("registrarConvenio")
@Scope("session")
public class RegistrarConvenioMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = -6267069366180595948L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistrarConvenioMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Actividad actual del árbol de actividades.
     */
    private Actividad currentActivity;

    /**
     * Variable que almacena los datos del usuario en sesión.
     */
    private UsuarioDTO usuario;

    /**
     * Objeto actualización seleccionado.
     */
    private Actualizacion actualizacion;

    /**
     * Lista de entidades contratantes.
     */
    private List<ConvenioEntidad> entidadesContratantes;

    /**
     * Entidad contratante seleccionada.
     */
    private ConvenioEntidad entidadSeleccionada;

    /**
     * Convenio de la actualizacion seleccionada.
     */
    private Convenio convenioSeleccionado;

    /**
     * Ruta del documento cargado.
     */
    private String rutaMostrar;

    /**
     * Archivo a cargar en la carpeta temporal.
     */
    private File archivoResultado;

    /**
     * Nombre temporal del archivo cargado.
     */
    private String nombreTemporalArchivo;

    /**
     * Variable booleana para saber si un convenio ya fue registrado.
     */
    private boolean convenioRegistrado;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteAprobacionDeConvenio;

    /**
     * Comentarios hechos en la revisión de la información del convenio .
     */
    private String comentariosRevisionConvenio;

    /**
     * Documento que se va a asociar al convenio.
     */
    private Documento selectedDocumento;

    /**
     * Año del convenio .
     */
    private List<SelectItem> anioActualizacion;

    /**
     * Variable usada para distinguir si una entidad es nueva o esta siendo modificada.
     */
    private Boolean editModeBool;

    private ServicioTemporalDocumento servicioTemporalDocumento;

    /** ---------------------------------- */
    /** ------------- INIT -- ------------ */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        // //////////////////////////////////////////////////////////////
        // TODO
        // - Cargar el archivo cuando se defina la ruta en Alfresco.
        // - Reporte de registro del convenio.
        // //////////////////////////////////////////////////////////////
        // Usuario
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.currentActivity = this.tareasPendientesMB
            .getInstanciaSeleccionada();

        Long idActualizacion = this.tareasPendientesMB
            .getInstanciaSeleccionada().getIdObjetoNegocio();

        // Actualización
        this.actualizacion = this
            .getActualizacionService()
            .buscarActualizacionPorIdConConveniosYEntidades(idActualizacion);
        if (this.actualizacion != null) {

            // TODO :: Revisar :: Una actualización tiene muchos convenios, pero
            // en la pantalla de entrada por el arbol la actualización tiene un
            // único convenio en ese momento. (Hablar con Andrés) :: 15/12/2011
            if (this.actualizacion.getConvenios() != null &&
                 !this.actualizacion.getConvenios().isEmpty()) {

                this.convenioSeleccionado = this.actualizacion.getConvenios()
                    .get(0);
            } else {
                this.convenioSeleccionado = new Convenio();
            }

            if (this.convenioSeleccionado != null &&
                 this.convenioSeleccionado.getConvenioEntidads() != null &&
                 !this.convenioSeleccionado.getConvenioEntidads()
                    .isEmpty()) {

                this.entidadesContratantes = this.convenioSeleccionado
                    .getConvenioEntidads();
            } else {
                this.entidadesContratantes = new ArrayList<ConvenioEntidad>();
            }

            // Variable booleana que indica si el convenio no ha sido
            // registrado.
            this.convenioRegistrado = false;

            // Inicia el combobox para la seleccion del anio de la actualizacion
            Date currentDate = new Date();

            @SuppressWarnings("deprecation")
            Integer anioActual = currentDate.getYear() + 1900;

            this.anioActualizacion = new LinkedList<SelectItem>();
            this.anioActualizacion.add(new SelectItem(null, "Seleccionar..."));
            for (Integer i = anioActual; i < anioActual + 6; i++) {
                this.anioActualizacion.add(new SelectItem(i, "" + i));
            }

            this.entidadSeleccionada = null;

        } else {
            this.addMensajeError("Error recuperando la actualización!");
            return;
        }
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS ------- */
    /** ---------------------------------- */
    public RegistrarConvenioMB() {
        // TODO Auto-generated constructor stub
    }

    public Convenio getConvenioSeleccionado() {
        return convenioSeleccionado;
    }

    public void setConvenioSeleccionado(Convenio convenioSeleccionado) {
        this.convenioSeleccionado = convenioSeleccionado;
    }

    public List<ConvenioEntidad> getEntidadesContratantes() {
        return entidadesContratantes;
    }

    public void setEntidadesContratantes(
        List<ConvenioEntidad> entidadesContratantes) {
        this.entidadesContratantes = entidadesContratantes;
    }

    public ConvenioEntidad getEntidadSeleccionada() {
        return entidadSeleccionada;
    }

    public void setEntidadSeleccionada(ConvenioEntidad entidadSeleccionada) {
        this.entidadSeleccionada = entidadSeleccionada;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public File getArchivoResultado() {
        return archivoResultado;
    }

    public void setArchivoResultado(File archivoResultado) {
        this.archivoResultado = archivoResultado;
    }

    public String getNombreTemporalArchivo() {
        return nombreTemporalArchivo;
    }

    public void setNombreTemporalArchivo(String nombreTemporalArchivo) {
        this.nombreTemporalArchivo = nombreTemporalArchivo;
    }

    public Boolean getEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(Boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public Actividad getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Actividad currentActivity) {
        this.currentActivity = currentActivity;
    }

    public Documento getSelectedDocumento() {
        return selectedDocumento;
    }

    public void setSelectedDocumento(Documento selectedDocumento) {
        this.selectedDocumento = selectedDocumento;
    }

    public boolean isConvenioRegistrado() {
        return convenioRegistrado;
    }

    public void setConvenioRegistrado(boolean convenioRegistrado) {
        this.convenioRegistrado = convenioRegistrado;
    }

    public String getComentariosRevisionConvenio() {
        return comentariosRevisionConvenio;
    }

    public void setComentariosRevisionConvenio(
        String comentariosRevisionConvenio) {
        this.comentariosRevisionConvenio = comentariosRevisionConvenio;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<SelectItem> getAnioActualizacion() {
        return anioActualizacion;
    }

    public void setAnioActualizacion(List<SelectItem> anioActualizacion) {
        this.anioActualizacion = anioActualizacion;
    }

    public ReporteDTO getReporteAprobacionDeConvenio() {
        return reporteAprobacionDeConvenio;
    }

    public void setReporteAprobacionDeConvenio(
        ReporteDTO reporteAprobacionDeConvenio) {
        this.reporteAprobacionDeConvenio = reporteAprobacionDeConvenio;
    }

    /** ---------------------------------- */
    /** ------------ MÉTODOS ------------- */
    /** ---------------------------------- */
    /** --------------------------------------------------------------------- */
    /**
     * Método que elimina una entidad seleccionada.
     *
     * @author david.cifuentes
     */
    public void eliminarEntidad() {
        try {
            if (this.entidadSeleccionada != null) {
                this.entidadesContratantes.remove(this.entidadSeleccionada);
                this.getActualizacionService().eliminarEntidad(
                    this.entidadSeleccionada);
                this.addMensajeInfo("Se eliminó la entidad satisfactoriamente");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error eliminando la entidad del convenio.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza las validaciones pertinentes para guardar una entidad.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public boolean validateEntidad() {
        if (this.convenioSeleccionado != null &&
             this.convenioSeleccionado.getId() == null) {
            this.addMensajeError("Antes de adicionar una entidad debe haber guardado el convenio.");
            return false;
        }
        if (this.entidadSeleccionada.getMonto() != null &&
             this.entidadSeleccionada.getMonto() < 0) {
            this.addMensajeError("El monto debe ser un número válido mayor a cero.");
            return false;
        }
        if (this.entidadSeleccionada.getPorcentajeParticipacion() != null &&
             this.entidadSeleccionada.getPorcentajeParticipacion() < 0) {
            this.addMensajeError(
                "El porcentaje de participación debe ser un número válido mayor a cero.");
            return false;
        }
        return true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que guarda un convenio.
     *
     * @author david.cifuentes
     */
    public void guardarConvenio() {
        try {
            if (validateConvenio()) {

                // Actualizar la actualización por el cambio de campos
                this.actualizacion = this.getActualizacionService()
                    .guardarYActualizarActualizacion(this.actualizacion);
                this.convenioSeleccionado.setActualizacion(this.actualizacion);

                this.convenioSeleccionado.setEstado(EConvenioEstado.PROPUESTA
                    .getCodigo());
                this.convenioSeleccionado
                    .setUsuarioLog(this.usuario.getLogin());
                this.convenioSeleccionado.setFechaLog(new Date(System
                    .currentTimeMillis()));
                this.convenioSeleccionado = this.getActualizacionService()
                    .guardarConvenio(this.convenioSeleccionado);
                if (this.convenioSeleccionado != null &&
                     this.convenioSeleccionado.getId() > 0) {
                    this.addMensajeInfo("Se guardó el convenio satisfactoriamente.");
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error al guardar el convenio.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que hace las validaciones para guardar un convenio adecuadamente.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public boolean validateConvenio() {
        if (this.convenioSeleccionado != null) {
            if (this.convenioSeleccionado.getNumero() == null ||
                 this.convenioSeleccionado.getNumero().trim().isEmpty()) {
                this.addMensajeError("Por favor ingrese un valor para el número del convenio");
                return false;
            }
            if (this.convenioSeleccionado.getTipoProyecto() == null ||
                 this.convenioSeleccionado.getTipoProyecto().trim()
                    .isEmpty()) {
                this.addMensajeError("Por favor seleccione el tipo de proyecto");
                return false;
            }
            if (this.convenioSeleccionado.getZona() == null ||
                 this.convenioSeleccionado.getZona().trim().isEmpty()) {
                this.addMensajeError("Por favor seleccione la zona del convenio.");
                return false;
            }
            if (this.convenioSeleccionado.getValor() == null ||
                 this.convenioSeleccionado.getValor() < 0D) {
                this.addMensajeError(
                    "El campo valor está vacio, por favor ingrese un valor para ser asociado al convenio.");
                return false;
            }
            if (this.convenioSeleccionado.getPlazoEjecucion() == null) {
                this.addMensajeError("Por favor seleccione el plazo de ejecución.");
                return false;
            }
        }
        return true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que almacena una entidad.
     *
     * @author david.cifuentes
     */
    public void guardarEntidad() {
        try {
            if (validateEntidad()) {
                entidadSeleccionada.setConvenio(this.convenioSeleccionado);
                entidadSeleccionada.setFechaLog(new Date(System
                    .currentTimeMillis()));
                entidadSeleccionada.setUsuarioLog(this.usuario.getLogin());

                entidadSeleccionada = this.getActualizacionService()
                    .guardarConvenioEntidad(entidadSeleccionada);
                ConvenioEntidad aux = null;
                for (ConvenioEntidad ce : this.entidadesContratantes) {
                    if (this.entidadSeleccionada.getId().longValue() == ce
                        .getId().longValue()) {
                        aux = ce;
                    }
                }
                if (aux != null) {
                    this.entidadesContratantes.remove(aux);
                }
                this.entidadesContratantes.add(entidadSeleccionada);

                this.addMensajeInfo("Se guardó la entidad satisfactoriamente");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        } catch (Exception e) {
            LOGGER.error("Error en guardarEntidad");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que guarda la información modificada del convenio y las entidades y avanza el proceso.
     *
     * @author franz.gamba
     *
     * @return
     */
    public String finalizar() {

        try {
            // Actualización del datos cambiados del convenio.
            if (this.entidadesContratantes != null &&
                 !this.entidadesContratantes.isEmpty()) {

                // Validar porcentaje de participación
                if (validarPorcentajeParticipacion()) {

                    this.entidadesContratantes = this.getActualizacionService()
                        .guardarListaEntidadConvenio(
                            this.entidadesContratantes);
                    if (this.entidadesContratantes != null &&
                         !this.entidadesContratantes.isEmpty()) {
                        this.convenioSeleccionado
                            .setConvenioEntidads(this.entidadesContratantes);
                    }
                    this.getActualizacionService().actualizarConvenio(
                        this.convenioSeleccionado);
                    this.addMensajeInfo("Se guardó el convenio y sus entidades satisfactoriamente!");
                    this.convenioRegistrado = true;
                }

                // TODO :: david.cifuentes :: Aquí se debe generar el reporte de
                // registro de convenios.
                // De momento se quema para que se pueda implementar la
                // funcionalidad de imprimir.
                // Cuando esté definido se debe reemplazar este método.
                // TODO :: Descomentar esto cuando se arregle lo de Alfresco.
                // this.generarReporteDeRegistroDeConvenios();
                // mover proceso
                if (this.currentActivity
                    .getNombre()
                    .equals(ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_CONVENIO)) {

                    this.getActualizacionService()
                        .ordenarAvanzarASiguienteActividad(
                            this.currentActivity.getId(),
                            this.actualizacion,
                            ERol.DIRECTOR_TERRITORIAL,
                            ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_CONVENIO,
                            this.usuario.getDescripcionTerritorial());
                    UtilidadesWeb.removerManagedBean("registrarConvenio");
                    this.tareasPendientesMB.init();
                    return "index";
                } else {
                    return null;
                }
            } else {

                this.addMensajeError("Debe ingresar al menos una entidad para el convenio.");
                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error actualizando el convenio.");
            return null;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que genera el reporte de registro de convenios
     *
     * @author david.cifuentes
     */
    public void generarReporteDeRegistroDeConvenios() {

        // TODO :: david.cifuentes :: Repote quemado, revisar validaciones cuando se realice su implementación ::
        // 25/06/203
        String numeroRadicacion = "123456";
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.SOLICITUD_DOCUMENTOS_FALTANTES;
        Map<String, String> parameters = new HashMap<String, String>();
        // id del trámite quemado
        parameters.put("TRAMITE_ID", "" + 97L);
        if (numeroRadicacion != null) {
            parameters.put("NUMERO_RADICADO", numeroRadicacion);
        }
        this.reporteAprobacionDeConvenio = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que valida que los porcentajes de participación sumen el 100%.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public boolean validarPorcentajeParticipacion() {

        Double sumaPorcentajes = 0D;
        for (ConvenioEntidad c : this.entidadesContratantes) {
            sumaPorcentajes += c.getPorcentajeParticipacion();
        }

        if (sumaPorcentajes != 100D) {
            this.addMensajeError("Los porcentajes de participación deben sumar 100%.");
            this.addMensajeError("Asegurese de revisar los datos ingresados!");
            return false;
        }
        return true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza la carga del archivo.
     *
     * @author david.cifuentes
     */
    public void cargarArchivo(FileUploadEvent eventoCarga) {

        // TODO :: david.cifuentes Descomentar cuando éste estructurado lo de Alfresco para los convenios :: 25/06/2013
        // String extension = org.apache.commons.io.FilenameUtils
        // .getExtension(eventoCarga.getFile().getFileName());
        // this.servicioTemporalDocumento = new ServicioTemporalDocumento();
//		this.rutaMostrar = eventoCarga.getFile().getFileName();
//
//		try {
//
//			String directorioTemporal = this.contexto.getTempDirectoryPath();
//
//			this.archivoResultado = new File(directorioTemporal + "//"
//					+ this.rutaMostrar);
//		} catch (Exception e) {
//			LOGGER.error(e.getMessage(), e);
//		}
//
//		FileOutputStream fileOutputStream = null;
//
//		try {
//			fileOutputStream = new FileOutputStream(this.archivoResultado);
//
//			byte[] buffer = new byte[Math
//					.round(eventoCarga.getFile().getSize())];
//
//			int bulk;
//			InputStream inputStream = eventoCarga.getFile().getInputstream();
//			while (true) {
//				bulk = inputStream.read(buffer);
//				if (bulk < 0) {
//					break;
//				}
//				fileOutputStream.write(buffer, 0, bulk);
//				fileOutputStream.flush();
//			}
//			fileOutputStream.close();
//			inputStream.close();			
//
//			// Generación del objeto para alfresco
//			this.selectedDocumento = new Documento();
//
//			TipoDocumento tipoDoc = this.getGeneralesService().
//					buscarTipoDocumentoPorId(ETipoDocumento.CONVENIO_PARA_ACTUALIZACION.getId());
//			
//			this.selectedDocumento.setTipoDocumento(tipoDoc);			
//			this.selectedDocumento.setArchivo(this.rutaMostrar);
//			this.selectedDocumento.setFechaLog(new Date());
//			this.selectedDocumento.setUsuarioLog(this.usuario.getLogin());
//			this.selectedDocumento.setUsuarioCreador(this.usuario.getLogin());
//			this.selectedDocumento.setBloqueado(ESiNo.NO.getCodigo());						
//			this.selectedDocumento.setEstado(EDocumentoEstado.CARGADO
//					.getCodigo());		
//			
//			this.selectedDocumento.setFechaDocumento(new Date());
//			this.selectedDocumento.setDescripcion(
//					ETipoDocumento.CONVENIO_PARA_ACTUALIZACION.nombre() +
//					" " + this.actualizacion.getId());
//			this.selectedDocumento.setTramiteId(this.actualizacion.getId());
//			this.selectedDocumento.setEstructuraOrganizacionalCod(
//					this.usuario.getDescripcionEstructuraOrganizacional());
//			
//			try {
//				boolean cargue = true;
//				
//				this.selectedDocumento.setNumeroRadicacion(this.getGeneralesService().obtenerNumeroRadicado());
//				
//				
//				// TODO :: Una vez hecha se debe implementar el método que
//				// relacione el documento en // la ruta especificada. Un ejemplo
//				// de ello lo encontramos en el
//				// siguiente método. // Implementar cuando esto esté definido.
//				// cargue =
//				// this.generalesService.guardarMemorandoConservacion(documento,
//				// numeroRadicado, this.usuario);
//
//				if (cargue) {
//					this.addMensajeInfo("El archivo se cargó satisfactoriamente");
//				} else {
//					this.addMensajeError("Falló la carga del archivo. Por favor intente nuevamente");
//				}
//			} catch (Exception e) {
//				String mensaje = "El archivo no pudo ser cargado, por favor intente nuevamente";
//				this.addMensajeError(mensaje);
//			}
//
//		} catch (IOException e) {
//			String mensaje = "El archivo seleccionado no pudo ser cargado";
//			this.addMensajeError(mensaje);
//		}
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que avanza en el proceso a la siguiente actividad (Ingresar convenio)
     *
     * @author franz.gamba
     *
     * @return
     */
    public String ordenarRevisionDeConvenio() {
        this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion, ERol.ABOGADO,
            ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_CONVENIO,
            this.usuario.getDescripcionTerritorial());

        UtilidadesWeb.removerManagedBean("registrarConvenio");
        this.tareasPendientesMB.init();
        return "index";
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que avanza el proceso a la siguiente actividad (Ingresar montos cupos presupuestales)
     *
     * @author franz.gamba
     * @return
     */
    public String aprobarRegistroDeConvenio() {
        UsuarioDTO subdirector = this.getGeneralesService()
            .obtenerSubdirectorCatastro();

        boolean procesoFlag = this
            .getActualizacionService()
            .ordenarAvanzarASiguienteActividad(
                this.currentActivity.getId(),
                this.actualizacion,
                subdirector,
                ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_MONTOS_CUPOS_PRESUPUESTALES,
                subdirector.getDescripcionEstructuraOrganizacional());
        if (procesoFlag) {

            UtilidadesWeb.removerManagedBean("registrarConvenio");
            this.tareasPendientesMB.init();
            return "index";
        } else {
            this.addMensajeError("Ha ocurrido un error al mover el proceso.");
            return null;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para reiniciar los valores de la entidad al momento de crear una nueva.
     *
     * @author david.cifuentes
     */
    public void reiniciarVariables() {
        this.entidadSeleccionada = new ConvenioEntidad();
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que cierra la pantalla
     *
     * @author david.cifuentes
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("registrarConvenio");
        this.tareasPendientesMB.init();
        return "index";
    }

    /** ---------------------------------- */
    /** ------------- EVENTOS ------------ */
    /** ---------------------------------- */
    /**
     * Método que se ejecuta al deseleccionar una entidad de la tabla.
     *
     * @author david.cifuentes
     */
    public void onUnSelectEntidadRow(UnselectEvent ev) {
        LOGGER.debug("RegistrarConvenioMB#onUnSelectDecretoRow");
        this.entidadSeleccionada = null;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que se ejecuta al seleccionar una entidad de la tabla.
     *
     * @author david.cifuentes
     */
    public void onSelectEntidadRow(SelectEvent ev) {
        LOGGER.debug("RegistrarConvenioMB#onSelectDecretoRow");
        this.entidadSeleccionada = (ConvenioEntidad) ev.getObject();
    }
}
