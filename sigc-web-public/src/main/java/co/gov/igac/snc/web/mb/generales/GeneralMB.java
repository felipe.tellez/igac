package co.gov.igac.snc.web.mb.generales;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.model.SelectItem;

import co.gov.igac.snc.persistence.entity.actualizacion.EntidadMunicipalFormato;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;
import co.gov.igac.snc.persistence.entity.tramite.Producto;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EComisionTramiteResultAv;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EDominiosActualizacion;
import co.gov.igac.snc.persistence.util.EDominiosOfertas;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EEntidadGrupoTrabajoPruebas;
import co.gov.igac.snc.util.SNCPropertiesUtil;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.SNCManagedBean;

@Component("general")
@Scope("application")
public class GeneralMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -7479960249839320796L;
    private int maximoTiempoInactividad = 15 * 60 * 1000;

    private boolean errorProceso = false;
    private boolean errorAplicacion = true;

    private HashMap<String, String> tramiteTipoTramiteHM;
    private HashMap<String, String> mutacionClaseHM;
    private HashMap<String, String> mutacion2SubtipoHM;
    private HashMap<String, String> mutacion5SubtipoHM;

    //rango de años
    private int anioInicialRango = 2012;

    private int anioFinalRango = 2030;

    public int getAnioInicialRango() {
        return anioInicialRango;
    }

    public void setAnioInicialRango(int anioInicialRango) {
        this.anioInicialRango = anioInicialRango;
    }

    public int getAnioFinalRango() {
        return anioFinalRango;
    }

    public void setAnioFinalRango(int anioFinalRango) {
        this.anioFinalRango = anioFinalRango;
    }

    public String getFormatoFecha() {
        return Constantes.FORMATO_FECHA;
    }

    public String getFormatoFechaHora() {
        return Constantes.FORMATO_FECHA_HORA;
    }

    public String getFormatoFechaHora12() {
        return Constantes.FORMATO_FECHA_HORA_12;
    }

    public String getFormatoFechaHora24() {
        return Constantes.FORMATO_FECHA_HORA_24;
    }

    public String getFormatoFechaUsuario() {
        return Constantes.FORMATO_FECHA_USUARIO;
    }

    public String getFormatoFechaUsuario2() {
        return Constantes.FORMATO_FECHA_USUARIO_2;
    }

    /**
     * formato fecha para el atributo pattern de los p:calendar
     */
    public String getFormatoFechaComponenteCalendar() {
        return Constantes.FORMATO_FECHA_CALENDAR;
    }

    public String getNoExistenFiltrosDisponibles() {
        return Constantes.NO_EXISTEN_FILTROS_DISPONIBLES;
    }

    public String getNoDataFound() {
        return Constantes.NO_DATA_FOUND;
    }

    public String getTipoMimePDF() {
        return Constantes.TIPO_MIME_PDF;
    }

    public Date getFechaActual() {
        return new Date();
    }

    /*
     * Devuelve la lista llena con los códigos y los valores de los elementos de dominio separados
     * por un -
     */
    private void adicionarDatosCV(List<SelectItem> lista,
        List<Dominio> listDominio) {
        for (Dominio dominio : listDominio) {
            lista.add(new SelectItem(dominio.getCodigo(), dominio.getCodigo() +
                 " - " + dominio.getValor()));
        }
    }

    private void adicionarDatosVCV(List<SelectItem> lista,
        List<Dominio> listDominio) {
        for (Dominio dominio : listDominio) {
            lista.add(new SelectItem(dominio.getValor(), dominio.getCodigo() +
                 " - " + dominio.getValor()));
        }
    }

    /*
     * Devuelve la lista llena con las descripciónes de los elementos de dominio
     */
    private void adicionarDatosD(List<SelectItem> lista,
        List<Dominio> listDominio) {
        for (Dominio dominio : listDominio) {
            lista.add(new SelectItem(dominio.getCodigo(), dominio.getDescripcion()));
        }
    }

    /*
     * Devuelve la lista llena con los valores de los elementos de dominio
     */
    private void adicionarDatosV(List<SelectItem> lista,
        List<Dominio> listDominio) {
        for (Dominio dominio : listDominio) {
            lista.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
        }
    }

    private void adicionarDatosSeleccionarV(List<SelectItem> lista,
        List<Dominio> listDominio) {

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (Dominio dominio : listDominio) {
            lista.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
        }
    }

    private void adicionarDatosSeleccionarVV(List<SelectItem> lista,
        List<Dominio> listDominio) {

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (Dominio dominio : listDominio) {
            lista.add(new SelectItem(dominio.getValor(), dominio.getValor()));
        }
    }

    /**
     * Método que arma una lista de String's a partir de una lista de dominio, se usa para el
     * componente Pick List de PrimeFaces
     *
     * @param lista
     * @param listDominio
     * @author javier.aponte
     */
    private void adicionarDatosListaString(List<String> lista,
        List<Dominio> listDominio) {
        for (Dominio dominio : listDominio) {
            lista.add(dominio.getValor());
        }
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del tipo de
     * persona_predio. Retorna en la lista tanto valor como código del dominio.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getPersonaPredioTipoCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_PREDIO_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del tipo de
     * persona_predio. Retorna en la lista unicamente el valor del dominio.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getPersonaPredioTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_PREDIO_TIPO));
        return lista;
    }

    public List<SelectItem> getPersonaTipoPersonaCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_PERSONA));
        return lista;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getPersonaTipoPersonaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_PERSONA));
        return lista;
    }
//-----------------------------------

    /**
     * Igual que getPersonaTipoPersonaV pero le adiciona un selectItem con el valor default para que
     * el valor del select no sea obligatorio
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getPersonaTipoPersonaV2() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_PERSONA));
        return lista;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getPersonaTipoIdentificacionCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_IDENTIFICACION));
        return lista;
    }

    public List<SelectItem> getPersonaTipoIdentificacionNoNitCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_IDENTIFICACION));
        for (SelectItem s : lista) {
            if (s.getValue().equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                lista.remove(s);
                break;
            }
        }
        return lista;
    }

    public List<SelectItem> getPersonaTipoIdentificacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_IDENTIFICACION));
        return lista;
    }

    public List<SelectItem> getPersonaTipoIdentificacionNoNitV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_IDENTIFICACION));
        for (SelectItem s : lista) {
            if (s.getValue().equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                lista.remove(s);
                break;
            }
        }
        return lista;
    }

    public List<SelectItem> getPersonaEstadoCivilCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_ESTADO_CIVIL));
        return lista;
    }

    public List<SelectItem> getPersonaEstadoCivilV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_ESTADO_CIVIL));
        return lista;
    }

    public List<SelectItem> getDerechoPropiedadModoAdqV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_MODO_ADQU));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * DERECHO_PROPIEDAD_MODO_ADQU. Retorna en las lista los códigos y valores.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getDerechoPropiedadModoAdqCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_MODO_ADQU));
        return lista;
    }

    public List<SelectItem> getDerechoPropiedadModoAdqVCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosVCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_MODO_ADQU));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * DERECHO_PROPIEDAD_TIPO_TITU. Retorna en las lista los códigos y valores.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getDerechoPropiedadTipoTituloCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_TIPO_TITU));
        return lista;
    }

    public List<SelectItem> getDerechoPropiedadTipoTituloV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_TIPO_TITU));
        return lista;
    }

    public List<SelectItem> getDerechoPropiedadTipoTituloVCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosVCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_TIPO_TITU));
        return lista;
    }

    public List<SelectItem> getDerechoPropiedadOficinaEmisoraV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DERECHO_PROPIEDAD_ENTIDAD_E));
        return lista;
    }

    public List<SelectItem> getComisionEstadoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.COMISION_ESTADO));
        return lista;
    }

    public List<SelectItem> getComisionNovedadV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.COMISION_NOVEDAD));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PROYECCION_CANCELA_INSCRIBE. Retorna en la lista tanto valor como código del dominio.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getProyeccionCancelaInscribeCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(null, "Nulo"));
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PROYECCION_CANCELA_INSCRIBE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * AVISO_REGISTRO_MOTI_NO_PROC. Retorna en la lista los valores únicamente.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getAvisoRegistroMotivoNoProcedeV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVISO_REGISTRO_MOTI_NO_PROC));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * SOLICITANTE_TIPO_SOLICITANT. Retorna en la lista los valores
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getSolicitanteTipoSolicitanteV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITANTE_TIPO_SOLICITANT));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * SOLICITUD_CLASE_CORRESPONDE. Retorna en la lista los códigos y los valores
     *
     * @return
     * @author fabio.navarrete
     * @modified juan.agudelo
     */
    public List<SelectItem> getTipoDocumentoCorrespondenciaCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_CLASE_CORRESPONDE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * SOLICITUD_TIPO. Retorna en las lista los valores.
     *
     * @return
     * @author fabio.navarrete
     * @modified juan.agudelo
     */
    public List<SelectItem> getSolicitudTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * SOLICITUD_TIPO. Retorna en las lista los valores.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getSolicitudSistemaEnvioV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        //lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_SISTEMA_ENVIO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TRAMITE_TIPO_TRAMITE.
     *
     * @modified by pedro.garcia se le adicionó la letra 'V' para cuadrar con el estándar de los
     * otros métodos
     * @return
     */
    public List<SelectItem> getTramiteTipoTramiteV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_TIPO_TRAMITE));
        return lista;
    }

    /**
     * Método para obtener el nombre de un dominio por su codigo
     *
     * @author felipe.cadena
     *
     * @param codigo
     * @return
     */
    public String obtenerNombreDominioPorCodigo(String codigo, EDominio eDominio) {
        String nombre = "";
        List<Dominio> lista = this.getGeneralesService()
            .getCacheDominioPorNombre(eDominio);

        for (Dominio dominio : lista) {
            if (dominio.getCodigo().equals(codigo)) {
                nombre = dominio.getValor();
                break;
            }
        }

        return nombre;
    }

    @SuppressWarnings("unchecked")
    public List<SelectItem> getTramiteTipoTramiteVO() {
        List<SelectItem> lista = getTramiteTipoTramiteV();
        Collections.sort(lista, new ComparadorValor());
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * RAZON_CANCELACION.
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<SelectItem> getRazonCancelacion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.RAZON_CANCELACION));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MUTACION_CLASE.
     *
     * @return
     */
    public List<SelectItem> getMutacionClase() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.MUTACION_CLASE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TRAMITE_DOCUMENTO_METO_NOTI.
     *
     * @return
     */
    public List<SelectItem> getFormaNotificacion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_DOCUMENTO_METO_NOTI));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MUTACION_CLASE omitiendo mutación de cuarta por requerimentos funcionales
     *
     * @return
     */
    public List<SelectItem> getMutacionClaseRadicacion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.MUTACION_CLASE));

        for (SelectItem sTemp : lista) {

            if (sTemp.getValue().equals(EMutacionClase.CUARTA.getCodigo())) {
                lista.remove(sTemp);
            }
        }
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MUTACION_CLASE completos, usada para consultar los tramites
     *
     * @return
     */
    public List<SelectItem> getMutacionClaseRadicacionConsulta() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.MUTACION_CLASE));

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MUTACION_2_SUBTIPO.
     *
     * @return
     */
    public List<SelectItem> getSubtipoMutacionSClase() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.MUTACION_2_SUBTIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MUTACION_5_SUBTIPO.
     *
     * @return
     */
    public List<SelectItem> getSubtipoMutacionQClase() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.MUTACION_5_SUBTIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio segun el
     * subtipo de mutación.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getSubtipoClaseMutacionV(String claseMutacion) {
        EDominio subtipo = EDominio.MUTACION_1_SUBTIPO;

        if (claseMutacion.equals(EMutacionClase.PRIMERA.getCodigo())) {
            subtipo = EDominio.MUTACION_1_SUBTIPO;
        } else if (claseMutacion.equals(EMutacionClase.SEGUNDA.getCodigo())) {
            subtipo = EDominio.MUTACION_2_SUBTIPO;
        } else if (claseMutacion.equals(EMutacionClase.TERCERA.getCodigo())) {
            subtipo = EDominio.MUTACION_3_SUBTIPO;
        } else if (claseMutacion.equals(EMutacionClase.CUARTA.getCodigo())) {
            subtipo = EDominio.MUTACION_4_SUBTIPO;
        } else if (claseMutacion.equals(EMutacionClase.QUINTA.getCodigo())) {
            subtipo = EDominio.MUTACION_5_SUBTIPO;
        }

        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista,
            this.getGeneralesService().getCacheDominioPorNombre(subtipo));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TRAMITE_TIPO_INSCRIPCION.
     *
     * @return
     */
    public List<SelectItem> getTramiteTipoInscripcion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_TIPO_INSCRIPCION));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TRAMITE_FUENTE.
     *
     * @return
     */
    public List<SelectItem> getTramiteFuente() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_FUENTE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TIPO_DOCUMENTO_CLASE.
     *
     * @return
     */
    public List<SelectItem> getTipoDocumentoClaseV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TIPO_DOCUMENTO_CLASE));
        return lista;
    }

    public List<Pais> getPaises() {
        return this.getGeneralesService().getCachePaises();
    }

    public List<Departamento> getDepartamentos(String codPais) {
        return this.getGeneralesService().getCacheDepartamentosPorPais(codPais);
    }

    public List<Municipio> getMunicipios(String codDpto) {
        return this.getGeneralesService().getCacheMunicipiosPorDepartamento(codDpto);
    }

    public List<CirculoRegistral> getCirculoRegistrals(String idMunicipio) {
        return this.getGeneralesService().getCacheCirculosRegistralesPorMunicipio(idMunicipio);
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * DATO_RECTIFICAR_TIPO. Retorna en las lista los valores.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getDatoRectificarTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DATO_RECTIFICAR_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * DATO_COMPLEMENTAR_TIPO. Retorna en las lista los valores.
     *
     * @return
     * @author juan.agudelo
     */
    public List<SelectItem> getDatoComplementarTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DATO_COMPLEMENTAR_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * DATO_RECTIFICAR_TIPO. Retorna en las lista los valores.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getSolicitanteSolicitudRelacV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        //lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITANTE_SOLICITUD_RELAC));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFICINAS_EMISORAS_TITULOS. Retorna en las lista los valores.
     *
     * @return
     * @author juan.agudelo
     */
    public List<SelectItem> getOficinasEmisorasTitulosV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.OFICINAS_EMISORAS_TITULOS));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TRAMITE_CLASIFICACION. Retorna en las lista los valores.
     *
     * @return
     */
    public List<SelectItem> getTramiteClasificacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_CLASIFICACION));
        return lista;
    }

    /**
     * Retorna los valores si no
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getSiNo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(ESiNo.SI.getCodigo()));
        lista.add(new SelectItem(ESiNo.NO.getCodigo()));
        return lista;
    }
    
    public List<SelectItem> getNo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(ESiNo.NO.getCodigo()));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MOTIVO_DEVOLUCION. Retorna en las lista los valores.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getMotivoDevolucionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        //TODO: fabio.navarrete :: completar esto con el dominio definido por Julio
        /* this.adicionarDatosV(lista, this.getGeneralesService()
				.getCacheDominioPorNombre(EDominio.MOTIVO_DEVOLUCION)); */
        return lista;
    }

    public int getMaximoTiempoInactividad() {
        return maximoTiempoInactividad;
    }

    public void setMaximoTiempoInactividad(int maximoTiempoInactividad) {
        this.maximoTiempoInactividad = maximoTiempoInactividad;
    }

    public List<MotivoEstadoTramite> getMotivoEstadoTramites() {
        return this.getTramiteService().getCacheMotivoEstadoTramites();
    }

    /**
     * Método que retorna los motivos de devolución del trámite.
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getMotivoEstadoDevolucionTramites() {
        List<SelectItem> motivosDevolucion = new ArrayList<SelectItem>();
        motivosDevolucion.add(new SelectItem(null, "Seleccionar..."));
        for (MotivoEstadoTramite met : this.getTramiteService().getCacheMotivoEstadoTramites()) {
            if (met.getEstado().equals(ETramiteEstado.DEVUELTO.getCodigo())) {
                motivosDevolucion.add(new SelectItem(met.getMotivo(), met.getMotivo()));
            }
        }
        return motivosDevolucion;
    }

    /**
     * Listado de
     *
     * @return
     */
    public List<SelectItem> getEntidadGrupoTrabajoPruebas() {
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", this.DEFAULT_COMBOS));
        selectItems.add(new SelectItem(EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
            .getNombre(), EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
                .getNombre()));
        selectItems.add(new SelectItem(EEntidadGrupoTrabajoPruebas.GRUPO_DE_TRABAJO_INTERNO
            .getNombre(), EEntidadGrupoTrabajoPruebas.GRUPO_DE_TRABAJO_INTERNO.getNombre()));
        selectItems.add(new SelectItem(EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA
            .getNombre(), EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA.getNombre()));
        return selectItems;
    }

    /**
     * Listado de entidades o grupos de trabajo disponibles para entidades delegadas
     *
     * @return
     */
    public List<SelectItem> getEntidadGrupoTrabajoPruebasDelegada() {
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", this.DEFAULT_COMBOS));
        selectItems.add(new SelectItem(EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
            .getNombre(), EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
                .getNombre()));
        selectItems.add(new SelectItem(EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA
            .getNombre(), EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA.getNombre()));
        return selectItems;
    }

    /**
     * Método que retorna las territoriales inculyendo la dirección general o sede central
     *
     * @return
     */
    public List<SelectItem> getTerritorialesConDireccionGeneral() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            lista.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
        }
        return lista;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * TRAMITE_DOCUMENTO_MENOFI que tiene la lista de medios físicos de notificación
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getTramiteDocumentoMeNoFi() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosCV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_DOCUMENTO_MENOFI));
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * TRAMITE_DOCUMENTO_METO_NOTI que tiene la lista de métodos de notificación
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getTramiteDocumentoMetoNoti() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_DOCUMENTO_METO_NOTI));
        return lista;
    }

    /**
     * Método que retorna los tipos de identificación sin el campo No Especificado
     *
     * @return
     */
    public List<SelectItem> getTiposIdentificacionSinNoEspecificado() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        for (SelectItem si : this.getPersonaTipoIdentificacionCV()) {
            if (si.getValue() != EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo()) {
                lista.add(si);
            }
        }
        return lista;
    }

    public void setErrorProceso(boolean errorProceso) {
        this.errorProceso = errorProceso;
    }

    public boolean isErrorProceso() {
        if (errorProceso) {
            errorProceso = false;
            errorAplicacion = true;
            return true;
        }
        return errorProceso;
    }

    public void setErrorAplicacion(boolean errorAplicacion) {
        this.errorAplicacion = errorAplicacion;
    }

    public boolean isErrorAplicacion() {
        return errorAplicacion;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_TIPO. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getPredioTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_TIPO. Retorna en las lista los códigos y valores.
     *
     * @author fabio.navarrete
     * @return
     */
    public List<SelectItem> getPredioTipoCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_DESTINO. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getPredioDestinoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_DESTINO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_DESTINO. Retorna en las lista los valores.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getPredioDestinoCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_DESTINO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del tipo de
     * documento.
     *
     * @author juan.agudelo
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<SelectItem> getTipoDocumentoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<TipoDocumento> td = this.getGeneralesService().getAllTipoDocumento();

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        Collections.sort(td);
        for (TipoDocumento docs : td) {
            lista.add(new SelectItem(docs.getId(), docs.getNombre()));
        }
        return lista;
    }

    //------------------------------------------------------------------------------------
    /**
     * Retorna los valores para el combo box (selectOneMenu) de tipo de {@link Profesion}
     *
     *
     * @author christian.rodriguez
     * @return Lista con los items para un combo selectOneMenu del dominio Profesion
     */
    public List<SelectItem> getProfesionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        List<Profesion> profesiones = this.getGeneralesService()
            .getCacheProfesiones();

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));

        for (Profesion profesion : profesiones) {
            lista.add(new SelectItem(profesion.getId(), profesion.getNombre()));
        }

        return lista;
    }

//--------------------------------------------------------------------------------------------------
    //---------    combos para lo de ofertas inmobiliarias  ------
    /**
     * Retorna los valores para el combo box (selectOneMenu) de tipo de predio de una oferta inmob.
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getOfertaTipoPredioV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_PREDIO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_TIPO_INMUEBLE_PROYEC. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaTipoInmuebleProyectoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService()
                .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_INMUEBLE_PROYEC));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_TIPO_INMUEBLE_USADO. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaTipoInmuebleUsadoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService()
                .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_INMUEBLE_USADO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_FRENTE_VIAS. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaFrenteViasV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_FRENTE_VIAS));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_ESTADO_VIAS. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaEstadoViasV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_ESTADO_VIAS));
        return lista;
    }

    /**
     * Retorna los valores para una lista de String's con los registros del dominio
     * OFERTA_AREA_USO_EXCLUSIVO.
     *
     * @author javier.aponte
     * @return
     */
    public List<String> getOfertasAreasUsoExclusivoSource() {
        List<String> source = new ArrayList<String>();
        this.adicionarDatosListaString(source, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_AREA_USO_EXCLUSIVO));
        return source;
    }

    /**
     * Retorna los valores para una lista de String's con los registros del dominio
     * OFERTA_RECURSO_HIDRICO.
     *
     * @author javier.aponte
     * @return
     */
    public List<String> getOfertasRecursosHidricosSource() {
        List<String> source = new ArrayList<String>();
        this.adicionarDatosListaString(source, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_RECURSO_HIDRICO));
        return source;
    }

    /**
     * Retorna los valores para una lista de String's con los registros del dominio
     * OFERTA_SERVICIO_COMUNAL.
     *
     * @author javier.aponte
     * @return
     */
    public List<String> getOfertasServiciosComunalesSource() {
        List<String> source = new ArrayList<String>();
        this.adicionarDatosListaString(source, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_SERVICIO_COMUNAL));
        return source;
    }

    /**
     *
     */

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_SERVICIO_PUBLICO. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaServicioPublicoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_SERVICIO_PUBLICO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_TIPO_INVESTIGACION. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaTipoInvestigacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_INVESTIGACION));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_ALTURA. Retorna en las lista los valores.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getOfertaAlturaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_ALTURA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_CONDICION_JURIDICA. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaCondicionJuridicaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_CONDICION_JURIDICA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_CONDICION_JURIDICA. Retorna en las lista las descripciones.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaCondicionJuridicaD() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosD(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_CONDICION_JURIDICA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_DESTINO. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaDestinoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_DESTINO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_FUENTE. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaFuenteV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_FUENTE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_INVESTIGACION_INGRES. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaInvestigacionIngresoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_INVESTIGACION_INGRES));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_TIPO_INMUEBLE. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaTipoInmuebleV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_INMUEBLE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_FOTOGRAFIA_TIPO_FOTO. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaFotoTipoFotoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_FOTOGRAFIA_TIPO_FOTO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_TIPO_OFERTA. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaTipoOfertaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_OFERTA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_UBICACION_MANZANA. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaUbicacionManzanaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UBICACION_MANZANA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_VETUSTEZ. Retorna en las lista los valores.
     *
     * @author juan.agudelo
     * @return
     */
    public List<SelectItem> getOfertaVetustezV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_VETUSTEZ));
        return lista;
    }

    /**
     *
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * OFERTA_VETUSTEZ. Retorna en las lista las descripciones ordenadas por el código.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getOfertaVetustezD() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<Dominio> listaDominio = this.getGeneralesService().getCacheDominioPorNombre(
            EDominiosOfertas.OFERTA_VETUSTEZ);
        Collections.sort(listaDominio, new Comparator<Dominio>() {

            @Override
            public int compare(Dominio o1, Dominio o2) {
                return Integer.parseInt(o1.getCodigo()) - Integer.parseInt(o2.getCodigo());
            }
        });
        this.adicionarDatosD(lista, listaDominio);
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_FORMA_LOTE de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaFormaLoteV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_FORMA_LOTE));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_PARQUEADERO de una
     * oferta inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoParqueaderoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_PARQUEADERO));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_COCINA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoCocinaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_COCINA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_UBICACION de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaUbicacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UBICACION));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_LOCAL de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoLocalV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_LOCAL));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_UBICACION_OFICINA de una
     * oferta inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaUbicacionOficinaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UBICACION_OFICINA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_BODEGA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoBodegaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_BODEGA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_UBICACION_BODEGA de una
     * oferta inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaUbicacionBodegaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UBICACION_BODEGA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_CERCHA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoCerchaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_CERCHA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TAMANIO_PARQUEADERO de una
     * oferta inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTamanioParqueaderoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TAMANIO_PARQUEADERO));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_ESTRATO_SOCIOECO de una
     * oferta inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaEstratoSocioecoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_ESTRATO_SOCIOECO));
        return lista;
    }

    /**
     * Retorna los códigos y descripciones para el combo box (selectOneMenu) de
     * OFERTA_ESTRATO_SOCIOECO de una oferta inmobiliaria. *
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getOfertaEstratoSocioecoCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService().getCacheDominioPorNombre(
            EDominiosOfertas.OFERTA_ESTRATO_SOCIOECO));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_LOTE de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoLoteV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_LOTE));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_EDIFICIO de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoEdificioV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_EDIFICIO));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TIPO_FINCA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTipoFincaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TIPO_FINCA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_UNIDAD_AREA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaUnidadAreaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UNIDAD_AREA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de AVALUO_UNIDAD_AREA
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getAvaluoUnidadAreaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista,
            this.getGeneralesService().getCacheDominioPorNombre(EDominio.AVALUO_UNIDAD_AREA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de AVALUO_UNIDAD_CONSTRUCCION
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getAvaluoUnidadConstruccionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService().getCacheDominioPorNombre(
                EDominio.AVALUO_UNIDAD_CONSTRUCCION));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_TOPOGRAFIA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaTopografiaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_TOPOGRAFIA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_CLIMA de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaClimaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_CLIMA));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_FORMA_NEGOCIACION de una
     * oferta inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaFormaNegociacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_FORMA_NEGOCIACION));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de OFERTA_UBICACION_LOCAL de una oferta
     * inmobiliaria.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getOfertaUbicacionLocalV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UBICACION_LOCAL));
        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) de PREDIO_SERVIDUMBRE_SERVIDUM.
     *
     * @author david.cifuentes
     * @return
     */
    public List<SelectItem> getServidumbreV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_SERVIDUMBRE_SERVIDUM));
        return lista;
    }

    /**
     * Retornar los posibles valores para la lista de selección de la zona de actualizacion.
     *
     * @return Una lista con los posibles valores para una zona de actualización.
     * @author alejandro.sanchez
     */
    public List<SelectItem> getActualizacionZonaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ACTUALIZACION_ZONA));
        return lista;
    }

    /**
     * Retornar los posibles valores para la lista de selección de las zonas fisicas
     *
     * @author felipe.cadena
     */
    public List<SelectItem> getZonaFisicasV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ZONA_FISICA));
        return lista;
    }

    /**
     * Retornar los posibles valores para la lista de selección del tipo de proyecto del proceso de
     * actualización.
     *
     * @return Una lista con los posibles valores para un tipo de proyecto.
     * @author alejandro.sanchez
     */
    public List<SelectItem> getActualizacionConvenioTipoProyectoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.CONVENIO_TIPO_PROYECTO));
        return lista;
    }

    /**
     * Retornar los posibles valores para la lista de selección del tipo de convenio del proceso de
     * actualización.
     *
     * @return Una lista con los posibles valores para un tipo de convenio.
     * @author alejandro.sanchez
     */
    public List<SelectItem> getActualizacionConvenioClaseV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.CONVENIO_CLASE));
        return lista;
    }

//--------------------------------------------------------------------------------------------------
    public String getCombosDefaultValue() {
        return super.DEFAULT_COMBOS;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_CONDICION_PROPIEDAD. Retorna en la lista los códigos y los valores
     *
     * @return
     * @author fabio.navarrete
     */
    public List<SelectItem> getPredioCondicionPropiedadCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_CONDICION_PROPIEDAD));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * ACTUALIZACION_TIPO_PROCESO. Retorna en las lista los valores.
     *
     * @author javila
     * @return
     */
    public List<SelectItem> getActualizacionTipoProcesoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosActualizacion.ACTUALIZACION_TIPO_PROCESO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * ACTUALIZACION_CONTRATO_ACTI. Retorna en las lista los valores.
     *
     * @author javila
     * @return
     */
    public List<SelectItem> getActualizacionActividadContratoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosActualizacion.ACTUALIZACION_CONTRATO_ACTI));
        return lista;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * COMISION_TRAMITE_RESULT_AV.
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getComisionTramiteResultAvV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.COMISION_TRAMITE_RESULT_AV));
        return lista;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * COMISION_TRAMITE_RESULT_AV.
     *
     * @author javier.aponte
     * @param tipoTramite
     * @return
     */
    public List<SelectItem> getComisionTramiteResultAvPorTipoTramiteV(String tipoTramite) {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        if (ETramiteTipoTramite.AUTOESTIMACION.getCodigo().equals(tipoTramite) ||
            ETramiteTipoTramite.MUTACION.getCodigo().equals(tipoTramite)) {  /// NO HAY FORMA DE LLEGAR POR OTRA MUTACION QUE NO SEA LA DE CUARTA
            for (Dominio dominio : this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.COMISION_TRAMITE_RESULT_AV)) {
                if (EComisionTramiteResultAv.ACEPTA.getCodigo().equals(dominio.getCodigo()) ||
                    EComisionTramiteResultAv.RECHAZA.getCodigo().equals(dominio.getCodigo())) {
                    lista.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
                }
            }
        } else {
            for (Dominio dominio : this.getGeneralesService() /// SE ASUME PARA REVISION DE AVALUO
                .getCacheDominioPorNombre(EDominio.COMISION_TRAMITE_RESULT_AV)) {
                if (EComisionTramiteResultAv.CONFIRMA.getCodigo().equals(dominio.getCodigo()) ||
                    EComisionTramiteResultAv.MODIFICA.getCodigo().equals(dominio.getCodigo())) {
                    lista.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
                }
            }
        }
        return lista;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * MUTACION_CLASE.
     *
     * @return
     */
    public List<SelectItem> getMutacionClaseV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.MUTACION_CLASE));
        return lista;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * COMISION_ESTADO_MOTIVO.
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getComisionEstadoMotivoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.COMISION_ESTADO_MOTIVO));
        return lista;
    }

    /**
     * Retorna un HashMap con los tipos de trámite. Creado para mejorar el desempeño de la
     * aplicación en un método iterativo.
     *
     * @return
     * @author fabio.navarrete
     */
    public HashMap<String, String> getTramiteTipoTramiteHM() {
        if (tramiteTipoTramiteHM == null || tramiteTipoTramiteHM.isEmpty()) {
            this.tramiteTipoTramiteHM = new HashMap<String, String>();
            for (Dominio dominio : this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.TRAMITE_TIPO_TRAMITE)) {
                tramiteTipoTramiteHM.put(dominio.getCodigo(),
                    dominio.getNombre());
            }
        }
        return tramiteTipoTramiteHM;
    }

    /**
     * Retorna un HashMap con las clases de mutación. Creado para mejorar el desempeño de la
     * aplicación en un método iterativo.
     *
     * @return
     * @author fabio.navarrete
     *
     */
    public HashMap<String, String> getMutacionClaseHM() {
        if (mutacionClaseHM == null || mutacionClaseHM.isEmpty()) {
            this.mutacionClaseHM = new HashMap<String, String>();
            for (Dominio dominio : this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.MUTACION_CLASE)) {
                mutacionClaseHM.put(dominio.getCodigo(), dominio.getNombre());
            }
        }
        return mutacionClaseHM;
    }

    /**
     * Retorna un HashMap con los subtipos correspondientes a la mutación de segunda. Creado para
     * mejorar el desempeño de la aplicación en un método iterativo.
     *
     * @return the mutacion2SubtipoHM
     * @author fabio.navarrete
     */
    public HashMap<String, String> getMutacion2SubtipoHM() {
        if (mutacion2SubtipoHM == null || mutacion2SubtipoHM.isEmpty()) {
            this.mutacion2SubtipoHM = new HashMap<String, String>();
            for (Dominio dominio : this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.MUTACION_2_SUBTIPO)) {
                mutacion2SubtipoHM.put(dominio.getCodigo(), dominio.getNombre());
            }
        }
        return mutacion2SubtipoHM;
    }

    /**
     * Retorna un HashMap con los subtipos correspondientes a la mutación de quinta. Creado para
     * mejorar el desempeño de la aplicación en un método iterativo.
     *
     * @return the mutacion5SubtipoHM
     * @author fabio.navarrete
     */
    public HashMap<String, String> getMutacion5SubtipoHM() {
        if (mutacion5SubtipoHM == null || mutacion5SubtipoHM.isEmpty()) {
            this.mutacion5SubtipoHM = new HashMap<String, String>();
            for (Dominio dominio : this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.MUTACION_5_SUBTIPO)) {
                mutacion5SubtipoHM.put(dominio.getCodigo(), dominio.getNombre());
            }
        }
        return mutacion5SubtipoHM;
    }

    @SuppressWarnings("rawtypes")
    public class ComparadorValor implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            SelectItem s1 = (SelectItem) o1;
            SelectItem s2 = (SelectItem) o2;
            return s1.getLabel().compareTo(s2.getLabel());
        }

    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Solicitud asociadas a un Tipo de
     * Solicitante Fiscalía
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoSolicitudPorFiscalia() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_ORIGEN_FISCALIA));
        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Solicitud asociadas a un Tipo de
     * Solicitante Juzgado
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoSolicitudPorJuzgado() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_ORIGEN_PROCURADUR));
        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Solicitud asociadas a un Tipo de
     * Solicitante Procuraduria
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoSolicitudPorProcuraduria() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_ORIGEN_PROCURADUR));
        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Solicitud asociadas a un Tipo de
     * Solicitante Privada, o Publica o Mixta
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoSolicitudPorPublicaPrivadaMixta() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_ORIGEN));
        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de todos los Tipos de Solicitud
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoSolicitud() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        lista.addAll(getAvaluosComercialesTipoSolicitudPorFiscalia());
        lista.addAll(getAvaluosComercialesTipoSolicitudPorJuzgado());
        lista.addAll(getAvaluosComercialesTipoSolicitudPorProcuraduria());
        lista.addAll(getAvaluosComercialesTipoSolicitudPorPublicaPrivadaMixta());

        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Tramite de Avaluos Comerciales
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoTramite() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<SelectItem> listaAux = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_TIPO_TRAMITE));

        for (SelectItem si : lista) {
            if (si.getValue().equals(
                ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo()) ||
                 si.getValue()
                    .equals(ETramiteTipoTramite.SOLICITUD_DE_CORRECCION
                        .getCodigo()) ||
                 si.getValue()
                    .equals(ETramiteTipoTramite.SOLICITUD_DE_COTIZACION
                        .getCodigo()) ||
                 si.getValue()
                    .equals(ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION
                        .getCodigo()) ||
                 si.getValue()
                    .equals(ETramiteTipoTramite.SOLICITUD_DE_REVISION
                        .getCodigo())) {
                listaAux.add(si);
            }
        }

        return listaAux;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Tramite de Avaluos Comerciales, se utiliza
     * para los filtros de las tablas
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoTramiteFiltro() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", "Todos"));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_TIPO_TRAMITE_AVALUO));
        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Tipos de Tramite de Avaluos Comerciales sin Tipo de
     * trámite de Correccion
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesTipoTramiteSinTramiteCorreccion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_TIPO_TRAMITE_AVALUO));

        for (int cont = 0; cont < lista.size(); cont++) {
            SelectItem si = lista.get(cont);
            if (si.getValue().equals(
                ETramiteTipoTramite.SOLICITUD_DE_CORRECCION
                    .getCodigo())) {
                lista.remove(cont);
                break;
            }
        }

        return lista;
    }

    /**
     * Para Avaluos Comerciales Retorna Lista de Marco Juridico
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesMarcoJuridico() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_MARCO_JURIDICO));
        return lista;
    }

    /**
     * Método para crear una lista de Departamentos con su codigo por territorial p.e: 08 -
     * Atlántico
     *
     * @author rodrigo.hernandez
     *
     * @param código de la territorial
     * @return
     */
    public List<SelectItem> getListaCodigoYDepartamentoPorTerritorial(String codigoTerritorial) {
        List<Departamento> listaDepartamentos = new ArrayList<Departamento>();
        List<SelectItem> lista = new ArrayList<SelectItem>();
        SelectItem item = null;

        listaDepartamentos = this.getGeneralesService()
            .getCacheDepartamentosPorTerritorial(codigoTerritorial);

        for (Departamento dpto : listaDepartamentos) {
            item = new SelectItem(dpto.getCodigo(), dpto.getCodigo() + " - " + dpto.getNombre());
            lista.add(item);
        }

        return lista;
    }

    /**
     * Método para crear una lista de municipios con su codigo por territorial p.e: 08001 -
     * Barranquilla
     *
     * @param código del departamento
     * @author rodrigo.hernandez
     */
    public List<SelectItem> getListaCodigoYMunicipioPorDepartamento(String codigoDepartamento) {
        List<Municipio> listaMunicipios = new ArrayList<Municipio>();
        List<SelectItem> lista = new ArrayList<SelectItem>();
        SelectItem item = null;

        listaMunicipios = this.getGeneralesService()
            .getCacheMunicipiosPorDepartamento(codigoDepartamento);

        for (Municipio mpio : listaMunicipios) {
            item = new SelectItem(mpio.getCodigo(), mpio.getCodigo() + " - " + mpio.getNombre());
            lista.add(item);
        }

        return lista;
    }

    /**
     * Retorna los valores para el combo box (selectOneMenu) del dominio OFERTA_UNIDAD_CONSTRUCCION
     *
     * @author rodrigo.hernandez
     * @return
     */
    public List<SelectItem> getOfertaUnidadConstruccionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominiosOfertas.OFERTA_UNIDAD_CONSTRUCCION));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * AVALUO_PREDIO_TIPO_AVALUO. Retorna en las lista los valores.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getAvaluoPredioTipoAvaluoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_PREDIO_TIPO_AVALUO));

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * CONSIGNACION_TIPO_CUENTA. Retorna en las lista los valores.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getConsignacionTipoCuentaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.CONSIGNACION_TIPO_CUENTA));

        return lista;
    }

    /**
     * Método que retorna las territoriales sin incluir la dirección general o sede central
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<SelectItem> getTerritorialesSinDireccionGeneral() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            if (eo.getCodigo().equals(Constantes.DIRECCION_GENERAL_CODIGO)) {
                continue;
            }
            lista.add(new SelectItem(eo.getCodigo(), eo.getCodigo() + " - " + eo.getNombre()));
        }
        return lista;
    }

    /**
     * Método que retorna las territoriales incluyendo la dirección general o sede central
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<SelectItem> getTerritorialesConDireccionGeneralCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        lista.add(new SelectItem(Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
            Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL + " - " +
            Constantes.NOMBRE_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL));
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            lista.add(new SelectItem(eo.getCodigo(), eo.getCodigo() + " - " + eo.getNombre()));
        }
        return lista;
    }

    /**
     * Método que retorna las territoriales incluyendo la dirección general o sede central, con el
     * valor del item de tipo Long
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<SelectItem> getTerritorialesConDireccionGeneralCVLong() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        lista.add(new SelectItem(0L,
            Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL + " - " +
            Constantes.NOMBRE_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL));
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            lista.add(new SelectItem(Long.valueOf(eo.getCodigo()), eo.getCodigo() + " - " + eo.
                getNombre()));
        }
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * AVALUO_TIPO_AVALUO. Retorna en las lista los valores.
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getAvaluoTipoAvaluoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_TIPO_AVALUO));

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * CONTRATO_INTERADMIN_REGIMEN. Retorna en las lista los valores.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getContratoInteradminRegimenV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService().getCacheDominioPorNombre(
                EDominio.CONTRATO_INTERADMIN_REGIMEN));

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PROFESIONAL_TIPO_VINCULA. Retorna en las lista los valores.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getProfesionalAvaluosTipoVinculacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PROFESIONAL_TIPO_VINCULA));

        return lista;
    }

    /**
     * Retorna los valores para un selectOneMenu con los años en un rango definido por las
     * constantes ANIO_ACTUAL y ANIO_MAXIMO_SEL
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getRangoAniosAscDesdeActual() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(null, this.DEFAULT_COMBOS));
        for (int i = Constantes.ANIO_ACTUAL; i <= Constantes.ANIO_MAXIMO_SEL; i++) {
            lista.add(new SelectItem(new Integer(i), String.valueOf(i)));
        }
        return lista;
    }

    /**
     * Retorna los valores para un selectOneMenu con los años en un rango definido por las
     * constantes ANIO_ACTUAL y ANIO_MAXIMO_SEL
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getRangoAniosAsc() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(null, this.DEFAULT_COMBOS));
        for (int i = this.anioInicialRango; i <= this.anioFinalRango; i++) {
            lista.add(new SelectItem(i, String.valueOf(i)));
        }
        return lista;
    }

    /**
     * Retorna los valores para un selectOneMenu con los años en un rango definido por las
     * constantes ANIO_MINIMO_SEL y ANIO_ACTUAL
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getRangoAniosDescDesdeActual() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(null, this.DEFAULT_COMBOS));
        for (int i = Constantes.ANIO_MINIMO_SEL; i <= Constantes.ANIO_ACTUAL; i++) {
            lista.add(new SelectItem(new Integer(i), String.valueOf(i)));
        }
        return lista;
    }

    public List<SelectItem> getPersonaTipoIdentificacionSoloCedulas() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<Dominio> listaDominio = new ArrayList<Dominio>();
        Dominio cc = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
            EDominio.PERSONA_TIPO_IDENTIFICACION, "CC");
        Dominio ce = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
            EDominio.PERSONA_TIPO_IDENTIFICACION, "CE");
        listaDominio.add(cc);
        listaDominio.add(ce);
        this.adicionarDatosCV(lista, listaDominio);

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * UNIDAD_CONSTRUCION_TIPIFICA.
     *
     * @return
     */
    public List<SelectItem> getUnidadConstruccionTipificacion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.UNIDAD_CONSTRUCION_TIPIFICA));
        return lista;
    }

    public long getMaximoTamanoAdjuntos() {
        return new Long(SNCPropertiesUtil.getInstance().getProperty("sigc.maxUploadSize"));
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * AVALUO_MOVIMIENTO_JUSTIFICA.
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getJustificacionCancelacionAvaluo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_MOVIMIENTO_JUSTIFICA));
        return lista;
    }

    /**
     * Retorna -como cadena- la zona de tiempo GMT que se usa en el sistema
     *
     * @author pedro.garcia
     * @return
     */
    public String getZonaGMT() {
        return Constantes.ZONA_GMT;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * UNIDAD_PISO_UBICACION.
     *
     * @author david.cifuentes
     * @return
     */
    public List<SelectItem> getUnidadPisoUbicacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService().getCacheDominioPorNombre(
                EDominio.UNIDAD_PISO_UBICACION));

        return lista;
    }

    /**
     * Retorna los valores para un menu de normas vigentes
     *
     * @return
     *
     * @author andres.eslava
     */
    public List<SelectItem> getNormasVigentes() {
        List<SelectItem> listaNormasVigentes = new ArrayList<SelectItem>();
        this.adicionarDatosV(listaNormasVigentes, this.getGeneralesService().
            getCacheDominioPorNombre(EDominio.NORMAS_VIGENTES));
        return listaNormasVigentes;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * CONTROL_CALIDAD_APROBO.
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getControlCalidadAprobo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService().getCacheDominioPorNombre(
                EDominio.CONTROL_CALIDAD_APROBO));

        return lista;
    }

    /**
     * Retorna los valores con los registros del dominio CONTROL_CALIDAD_ESPECIFICAC.
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getControlCalidadEspecificacion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(
            lista,
            this.getGeneralesService().getCacheDominioPorNombre(
                EDominio.CONTROL_CALIDAD_ESPECIFICAC));

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * AVALUO_COMITE_PROF_ACTI. Retorna en las lista los valores.
     *
     * @author christian.rodriguez
     * @return
     */
    public List<SelectItem> getAvaluoComiteProfesionalActividadV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        lista.add(new SelectItem(null, this.DEFAULT_COMBOS));
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_COMITE_PROF_ACTI));

        return lista;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * AVALUO_PREDIO_CULT_ASPE_TEC
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getAvaluoPredioCultAspeTecV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_PREDIO_CULT_ASPE_TEC));
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio UNIDAD_MEDIDA
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getUnidadMedidaV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.UNIDAD_MEDIDA));
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio UNIDAD_MEDIDA_AREA
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getUnidadMedidaAreaV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.UNIDAD_MEDIDA_AREA));
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * AVALUO_PREDIO_ANEXO_FUENTE
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getAvaluoPredioAnexoFuenteV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_PREDIO_ANEXO_FUENTE));
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * AVALUO_PREDIO_ANEXO_TIPO
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getAvaluoPredioAnexoTipoV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_PREDIO_ANEXO_TIPO));
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * REPORTE_INCONSISTENCIA
     *
     * @author javier.barajas
     * @return
     */
    public List<SelectItem> getReportesInconsistencia() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTE_INCONSISTENCIA));
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio REPORTE_AUDITORIA
     *
     * @author javier.barajas
     * @return
     */
    public List<SelectItem> getReportesAuditoria() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTE_AUDITORIA));

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * AVALUO_AREA_REG_FUENTE
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getAvaluoAreaRegFuenteV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_AREA_REG_FUENTE));
        return answer;
    }

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio PREDIO_TIPO_INMUEBLE
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getPredioTipoInmuebleV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_TIPO_INMUEBLE));
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Para Avaluos Comerciales Retorna Lista de clases de suelo
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SelectItem> getAvaluosComercialesClaseSuelo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();

        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.AVALUO_CLASE_SUELO));
        return lista;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio
     * UNIDAD_CONSTRUCCION_TIPO
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getUnidadConstruccionTipoV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.UNIDAD_CONSTRUCCION_TIPO));
        return answer;
    }
//--------------------------------------------------------------------------------

    /**
     * Arma la lista de items para combo con los códigos y valores del dominio FOTO_TIPO
     *
     * @author pedro.garcia
     * @return
     */
    public List<SelectItem> getFotoTipoV() {
        List<SelectItem> answer = new ArrayList<SelectItem>();
        this.adicionarDatosV(answer, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.FOTO_TIPO));
        return answer;
    }

    // --------------------------------------------------------------------------------
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * DOCUMENTO_ESTADO.
     *
     * @return
     */
    public List<SelectItem> getDocumentoEstadoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.DOCUMENTO_ESTADO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_INCONSISTENCIA_TIPO.
     *
     * @return
     */
    public List<SelectItem> getPredioInconsistenciaTipo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_INCONSISTENCIA_TIPO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * REPORTES_ESTADISTICOS.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getReportesEstadisticos() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTE_ESTADISTICOS));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * REPORTE_DATOS_DESPLEGAR
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getReportesDatosADesplegar() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTE_DATOS_DESPLEGAR));
        return lista;
    }

    // --------------------------------------------------------------------------------
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * UNIDAD_CONSTRUCION_USO.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getUnidadConstruccionCV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.UNIDAD_CONSTRUCION_USO));
        return lista;
    }

    // --------------------------------------------------------------------------------
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * UNIDAD_CONSTRUCCION_TIP_CAL.
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getUnidadConstruccionTipoCalificacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.UNIDAD_CONSTRUCCION_TIP_CAL));
        return lista;
    }

    // ------------------------------------------------ //
    /**
     * Mètodo para crear el registro inicial de tramite depuracion cuado el tramite se va al
     * subproceso de depuracion.
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     */
    public void crearRegistroTramiteDepuracion(Tramite tramite, UsuarioDTO usuario) {

        TramiteDepuracion td = new TramiteDepuracion();

        List<TramiteInconsistencia> tis = this.getTramiteService()
            .buscarTramiteInconsistenciaPorTramiteId(tramite.getId());

        td.setTramite(tramite);
        td.setFechaLog(new Date());
        td.setUsuarioLog(usuario.getLogin());

        tramite.setTramiteDepuracions(new ArrayList<TramiteDepuracion>());
        tramite.getTramiteDepuracions().add(td);

        this.getTramiteService().guardarActualizarTramite2(tramite);

        td = this.getTramiteService().actualizarTramiteDepuracion(td);

        for (TramiteInconsistencia ti : tis) {
            ti.setTramiteDepuracion(td);
            this.getTramiteService().guardarActualizarTramiteInconsistencia(ti);
        }
    }

    /**
     * Método que realiza las validaciones para permitir el envio de un tramite a depuración
     *
     * @author felipe.cadena modificado por leidy.gonzalez
     * @param tramite
     * @param usuario
     * @param actividad
     * @return -- retorna el responsable sig correspondiente, null si el tramite no pasa en las
     * validaciones
     */
    public UsuarioDTO validarEnvioDepuracion(Tramite tramite,
        UsuarioDTO usuario, Actividad actividad) {

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        if (usuario.getDescripcionUOC() != null) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionUOC(),
                ERol.RESPONSABLE_SIG);
        }

        //si no existe Responsable SIG en la UOC se buscan en la territorial.
        if (usuarios.isEmpty()) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_SIG);
        }

        //Se valida que exista responsable SIG para asignar la depuración
        if (usuarios.isEmpty()) {
            this.addMensajeError("No existe usuario con el rol Responsable SIG");
            return null;
        }

        return usuarios.get(0);
    }

    /**
     * Método que realiza las validaciones para permitir el envio de un tramite a modificar
     * información geográfica. Se valida que no existan tramites de la misma manzana en edicion
     * geografica de depuracion
     *
     * @author felipe.cadena
     * @param tramite
     * @return
     */
    public Boolean validarEnvioModificacionGeografica(Tramite tramite) {

        //Se valida que no existan tramites asociados a los predios de la manzana
        //que se encuentran en actividades del proceso de depuración
        String numeroManzana;
        if (tramite.isQuinta()) {
            numeroManzana = tramite.getNumeroPredial().substring(0, 17);
        } else {
            numeroManzana = tramite.getPredio().getNumeroPredial().substring(0, 17);
        }

        List<Tramite> tramitesManzana = this.getTramiteService().
            obtenerTramitesPorManzanaDepuracion(numeroManzana);

        //Se descarta el tramite actual
        tramitesManzana.remove(tramite);

        if (tramitesManzana != null && !tramitesManzana.isEmpty()) {

            String msg = "Existen tramites asociados a los predios de la" +
                
                " misma manzana en el proceso de depuración, se deberán terminar dichas depuraciones para poder avanzar, los tramites son: ";
            for (Tramite t : tramitesManzana) {
                msg += t.getNumeroRadicacion() + ", ";

                if (t.getTarea().equals("APLICAR_CAMBIOS_DEPURACION")) {
                    this.addMensajeError(ECodigoErrorVista.GENERAR_COPIA_EDICION_001.toString());
                }
            }

            this.addMensajeError(msg);
            return false;
        }

        return true;
    }

    /**
     * Método que realiza las validaciones para permitir el envio de un tramite a modiifcar
     * información geográfica en el subproceso de depuracion se valida que no existan otros tramites
     * de la manzana en modificar de depuracion o de conservacion.
     *
     * @author felipe.cadena
     * @param tramite
     * @return
     */
    public Boolean validarEnvioModificacionGeograficaDepuracion(Tramite tramite) {

        //Se valida que no existan tramites geograficos asociados a los predios de la manzana
        //que se encuentran en actividades de depuracion
        if (!validarEnvioModificacionGeografica(tramite)) {
            return false;
        }

        //Se valida que no existan tramites geograficos asociados a los predios de la manzana
        //que se encuentran en modificacion geografica de depuracion
        if (tramite.isQuinta()) {
            String numeroManzana = tramite.getNumeroPredial().substring(0, 17);
            List<Tramite> tramitesManzana = this.getTramiteService()
                .obtenerTramitesPorManzanaPosteriores(numeroManzana);

            // Se descarta el tramite actual
            for (int i = 0; i < tramitesManzana.size(); i++) {
                Tramite t = tramitesManzana.get(i);
                if (t.getId().equals(tramite.getId())) {
                    tramitesManzana.remove(t);
                    break;
                }
            }

            if (!tramitesManzana.isEmpty()) {
                this.addMensajeError(
                    "Existe un trámite espacial en la misma manzana, espere a que finalice.");
                return false;
            }

        } else {

            String numeroManzana = tramite.getPredio().getNumeroPredial()
                .substring(0, 17);
            List<Tramite> tramitesManzana = this.getTramiteService()
                .obtenerTramitesPorManzanaPosteriores(numeroManzana);

            // Se descarta el tramite actual
            for (int i = 0; i < tramitesManzana.size(); i++) {
                Tramite t = tramitesManzana.get(i);
                if (t.getId().equals(tramite.getId())) {
                    tramitesManzana.remove(t);
                    break;
                }
            }

            if (!tramitesManzana.isEmpty()) {

                String msg =
                    "Existe tramites espaciales en la misma manzana, espere a que finalice.";
                for (Tramite t : tramitesManzana) {
                    msg += t.getNumeroRadicacion() + ", ";
                }
                this.addMensajeError(msg);
                return false;
            }
        }

        return true;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del tipo de documento
     * DOCUMENTO_SOPORTE_PRODUCTOS
     *
     * @author javier.aponte
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<SelectItem> getTipoDocumentoSoporteProductosV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<TipoDocumento> td = this.getGeneralesService().
            buscarTiposDeDocumentos(ETipoDocumentoClase.DOCUMENTO_SOPORTE_PRODUCTO.toString(),
                String.valueOf(""));

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        Collections.sort(td);
        for (TipoDocumento docs : td) {
            lista.add(new SelectItem(docs.getId(), docs.getNombre()));
        }
        return lista;
    }

    /**
     * Método que retorna una lista de los grupos de productos
     *
     * @author javier.aponte
     * @return
     */
    /*
     * @modified by leidy.gonzalez:: 22/01/2015 :: #11143 Se verifica qeu en la lista no muestre el
     * grupo de producto con id 6: Análisis de Muestras de Suelos
     */
    @SuppressWarnings("unchecked")
    public List<SelectItem> getGrupoProductosV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<GrupoProducto> gp = this.getGeneralesService().obtenerTodosGrupoProductos();

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (GrupoProducto gpTemp : gp) {
            if (!gpTemp.getId().equals(new Long(6))) {
                lista.add(new SelectItem(gpTemp.getId(), gpTemp
                    .getDescripcion()));
            }
        }
        return lista;
    }

    /**
     * Método que retorna una lista de los códigos de productos y los filtra por el id del grupo
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se visulizan los productos que solo tengan el campo ejecución del
     * programa diferente de nulo
     * @modified by leidy.gonzalez Se modifica que visualice los productos diferentes del codigo 999
     * debido a que este es generado para registros de cobol.
     * @modified by leidy.gonzalez 08/09/2014 Se agrega en el label del combo el nombre del producto
     * para ser visualizado en la pagina.
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<SelectItem> getCodigoProductosV(Long grupoId) {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<Producto> p = this.getGeneralesService().obtenerListaProductosPorGrupoId(grupoId);

        for (Producto pTemp : p) {
            if (pTemp.getEjecucionPrograma() != null && !pTemp.getCodigo().equals("999")) {
                lista.add(new SelectItem(pTemp.getCodigo(), pTemp.getCodigo() + " -\r" + pTemp.
                    getNombre()));
            }

        }
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PRO_CAT_DET_DES_TRA_CAT
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getProCatDetDescripcionTramiteCatastralV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarVV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PRO_CAT_DET_DES_TRA_CAT));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PRO_CAT_DET_DES_DEP_GEO
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getProCatDetDescripcionDepuracionGeograficaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarVV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PRO_CAT_DET_DES_DEP_GEO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PRO_CAT_DET_CON_DESTINO_A
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getProCatDetConDestinoAV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarVV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PRO_CAT_DET_CON_DESTINO_A));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * REG_PRE_CRITERIO_BUSQUEDA
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getRegPreCriterioBusquedaV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REG_PRE_CRITERIO_BUSQUEDA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * RESOLUCIONES_TIPO_BUSQUEDA
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getResolucionesTipoBusqueda() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.RESOLUCIONES_TIPO_BUSQUEDA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PRODUCTO_CATASTRAL_FORMATO
     *
     * @author javier.aponte modificado por leidy.gonzalez
     * @return
     */
    public List<SelectItem> getRegPreFormatoV() {
        //v1.1.9
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PRODUCTO_CATASTRAL_FORMATO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * REG_PRE_NIVEL_INFORMACION
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getRegPreNivelInformacionV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REG_PRE_NIVEL_INFORMACION));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * CERT_ESP_DATO_CERTIFICAR
     *
     * @author javier.aponte
     * @return
     */
    public List<SelectItem> getCertEspDatoCertificarV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.CERT_ESP_DATO_CERTIFICAR));
        return lista;
    }

    /**
     * Método encargado de copiar una entidad UnidadConstrucción a una entidad PUnidadConstruccion
     *
     * @author javier.aponte
     */
    public PUnidadConstruccion copiarUnidadConstruccionAPUnidadConstruccion(
        UnidadConstruccion unidadConstruccion) {

        PUnidadConstruccion resultado = new PUnidadConstruccion();

        resultado.setId(unidadConstruccion.getId());
        resultado.setAnioConstruccion(unidadConstruccion.getAnioConstruccion());
        resultado.setAreaConstruida(unidadConstruccion.getAreaConstruida());
        resultado.setAvaluo(unidadConstruccion.getAvaluo());
        resultado.setCalificacionAnexoId(unidadConstruccion.getCalificacionAnexoId());
        resultado.setDescripcion(unidadConstruccion.getDescripcion());
        resultado.setFechaInscripcionCatastral(unidadConstruccion.getFechaInscripcionCatastral());
        resultado.setObservaciones(unidadConstruccion.getObservaciones());
        resultado.setPisoUbicacion(unidadConstruccion.getPisoUbicacion());
        resultado.setPPredio(this.getConservacionService().getPPredioByNumeroPredial(
            unidadConstruccion.getPredio().getNumeroPredial()));
        resultado.setTipificacion(unidadConstruccion.getTipificacion());
        resultado.setTipoCalificacion(unidadConstruccion.getTipoCalificacion());
        resultado.setTipoConstruccion(unidadConstruccion.getTipoConstruccion());
        resultado.setTipoDominio(unidadConstruccion.getTipoDominio());
        resultado.setTotalBanios(unidadConstruccion.getTotalBanios());
        resultado.setTotalHabitaciones(unidadConstruccion.getTotalHabitaciones());
        resultado.setTotalLocales(unidadConstruccion.getTotalLocales());
        resultado.setTotalPisosConstruccion(unidadConstruccion.getTotalPisosConstruccion());
        resultado.setTotalPisosUnidad(unidadConstruccion.getTotalPisosUnidad());
        resultado.setTotalPuntaje(unidadConstruccion.getTotalPuntaje());
        resultado.setUnidad(unidadConstruccion.getUnidad());
        resultado.setUsoConstruccion(unidadConstruccion.getUsoConstruccion());
        resultado.setValorM2Construccion(unidadConstruccion.getValorM2Construccion());
        resultado.setUsuarioLog(unidadConstruccion.getUsuarioLog());
        resultado.setFechaLog(unidadConstruccion.getFechaLog());
        
        List<PUnidadConstruccionComp> componentes = new ArrayList<PUnidadConstruccionComp>();
        for (UnidadConstruccionComp elementoIterado : unidadConstruccion.getUnidadConstruccionComps()) {
            PUnidadConstruccionComp nuevoComp = new PUnidadConstruccionComp(elementoIterado,
                unidadConstruccion.getUsuarioLog());
            componentes.add(nuevoComp);
        }
        
        resultado.setPUnidadConstruccionComps(componentes);
        return resultado;

    }

    /**
     * Método encargado de copiar una entidad UnidadConstrucción a una entidad PUnidadConstruccion
     *
     * @author javier.aponte
     */
    public UnidadConstruccion copiarPUnidadConstruccionAUnidadConstruccion(
        PUnidadConstruccion pUnidadConstruccion) {

        UnidadConstruccion resultado = new UnidadConstruccion();

        resultado.setId(pUnidadConstruccion.getId());
        resultado.setAnioConstruccion(pUnidadConstruccion.getAnioConstruccion());
        resultado.setAreaConstruida(pUnidadConstruccion.getAreaConstruida());
        resultado.setAvaluo(pUnidadConstruccion.getAvaluo());
        resultado.setCalificacionAnexoId(pUnidadConstruccion.getCalificacionAnexoId());
        resultado.setDescripcion(pUnidadConstruccion.getDescripcion());
        resultado.setFechaInscripcionCatastral(pUnidadConstruccion.getFechaInscripcionCatastral());
        resultado.setObservaciones(pUnidadConstruccion.getObservaciones());
        resultado.setPisoUbicacion(pUnidadConstruccion.getPisoUbicacion());
        resultado.setPredio(this.getConservacionService().getPredioByNumeroPredial(
            pUnidadConstruccion.getPPredio().getNumeroPredial()));
        resultado.setTipificacion(pUnidadConstruccion.getTipificacion());
        resultado.setTipoCalificacion(pUnidadConstruccion.getTipoCalificacion());
        resultado.setTipoConstruccion(pUnidadConstruccion.getTipoConstruccion());
        resultado.setTipoDominio(pUnidadConstruccion.getTipoDominio());
        resultado.setTotalBanios(pUnidadConstruccion.getTotalBanios());
        resultado.setTotalHabitaciones(pUnidadConstruccion.getTotalHabitaciones());
        resultado.setTotalLocales(pUnidadConstruccion.getTotalLocales());
        resultado.setTotalPisosConstruccion(pUnidadConstruccion.getTotalPisosConstruccion());
        resultado.setTotalPisosUnidad(pUnidadConstruccion.getTotalPisosUnidad());
        resultado.setTotalPuntaje(pUnidadConstruccion.getTotalPuntaje());
        resultado.setUnidad(pUnidadConstruccion.getUnidad());
        resultado.setUsoConstruccion(pUnidadConstruccion.getUsoConstruccion());
        resultado.setValorM2Construccion(pUnidadConstruccion.getValorM2Construccion());
        resultado.setUsuarioLog(pUnidadConstruccion.getUsuarioLog());
        resultado.setFechaLog(pUnidadConstruccion.getFechaLog());

        return resultado;

    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros de calificación anexo
     *
     * @author javier.aponte
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<SelectItem> getDescripcionUnidadV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<CalificacionAnexo> ca = this.getFormacionService().getAllCalificacionAnexo();

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        Collections.sort(ca);
        for (CalificacionAnexo caTemp : ca) {
            lista.add(new SelectItem(caTemp.getId(), caTemp.getDescripcion()));
        }
        return lista;
    }

    public boolean validarExistenciaCopiaFinalEdicion(Tramite tramite) {
        return this.validarExistenciaCopiaEdicion(tramite, ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.
            getId(), "");
    }

    /**
     * Valida si el tramite tiene asociada una copia de consulta final, tanto en la base de datos
     * come en el FTP
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @return
     */
    public boolean validarExistenciaCopiaEdicion(Tramite tramite, Long tipoReplica, String alert) {

        //No realiza la validacion si no es un tramite geografico
        if (!tramite.isTramiteGeografico()) {
            return true;
        }

        //no se realiza esta validacion si el predio es fiscal
        if (tramite.isQuintaOmitido()) {
            Predio p = this.getConservacionService().getPredioPorNumeroPredial(tramite.
                getNumeroPredial());
            if (p != null) {
                if (p.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                    return true;
                }
            }
        }

        //no se realiza esta validacion si el predio es fiscal
        if (tramite.getPredio() != null) {
            if (tramite.getPredio().getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.
                getCodigo())) {
                return true;
            }
        }

        Documento d = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(tramite.getId(), tipoReplica);

        //Se verifica el registro en oracle
        if (d == null) {
            if (alert != null) {
                this.addMensajeError(ECodigoErrorVista.COPIA_98_01.toString());
            }
            return false;
        } else {
            //se verifica en el gestor documental
            try {
                if (!this.getGeneralesService().validarExistenciaDocumentoGestorDocumental(d)) {
                    if (alert != null) {
                        this.addMensajeError(ECodigoErrorVista.COPIA_98_02.toString());
                    }
                    return false;
                }
            } catch (NullPointerException ex) {
                this.addMensajeError(ECodigoErrorVista.COPIA_98_03.toString());
                return false;
            }
        }

        return true;
    }

    /**
     * Retorna un mapa con los select items de los tipos de solicitud, tipo de tramites y subtipos
     * de tramites agrupados, las llaves de los grupos son el nombre del dominio padre seguido del
     * codigo, en el caso de las solicitudes existe un grupo con todos los tipos que solo tiene el
     * nombre del dominio solicitud.
     *
     * @author felipe.cadena
     * @return
     */
    public Map<String, List<SelectItem>> obtenerTipoSolicitudesYTramitesSI() {
        Map<String, List<SelectItem>> resultado = new HashMap<String, List<SelectItem>>();

        Map<String, List<Dominio>> datosBase = this.getGeneralesService().
            obtenerTiposSolicitudesTramites();
        Set<String> keys = datosBase.keySet();

        for (String k : keys) {

            List<SelectItem> sis = new ArrayList<SelectItem>();
            List<Dominio> doms = datosBase.get(k);

            for (Dominio dominio : doms) {
                SelectItem si = new SelectItem(dominio.getCodigo(), dominio.getValor());
                sis.add(si);
            }
            resultado.put(k, sis);
        }

        return resultado;

    }

    /**
     * Método para devolver el formato de mostrar sólo dos cifras decimales
     *
     * @author javier.aponte
     */
    public String getFormatoDosCifrasDecimales() {
        String answer;
        answer = "#0.00";
        return answer;
    }

    /**
     * Método que se utiliza para eliminar la relación entre trámite y comisión de una comisión que
     * se cancela Realiza los siguientes pasos: 1. Actualiza el trámite estableciendo el campo
     * comisionado en 'NO' 2. Elimina las relaciones de trámite comisión 3. Se consulta la lista de
     * comisiones tramite en la base de datos y se establecen en el trámite tiene el trámite
     * asociado
     *
     * @author javier.aponte
     */
    public Tramite eliminarRelacionComisionTramite(Tramite tramite, UsuarioDTO usuario) {

        List<ComisionTramite> comisionTramitesNoRealizadas = this.getTramiteService().
            buscarComisionesTramiteNoRealizadasPorIdTramite(tramite.getId());
        Comision comision = null;
        if (comisionTramitesNoRealizadas != null && !comisionTramitesNoRealizadas.isEmpty()) {
            for (ComisionTramite ct : comisionTramitesNoRealizadas) {
                if (ct.getComision().getTipo() != null && EComisionTipo.CONSERVACION.getCodigo().
                    equals(ct.getComision().getTipo())) {
                    comision = ct.getComision();
                    break;
                }
            }
        }

        List<ComisionTramite> comisionesTramiteVigentes;

        //D: actualizar el trámite
        tramite.setComisionado(ESiNo.NO.getCodigo());
        this.getTramiteService().actualizarTramite2(tramite, usuario);

        //D: eliminar relaciones trámite-comisión
        if (comision != null) {
            this.getTramiteService().borrarRelacionTramiteComision(usuario, tramite.getId(),
                comision.getId());
        }

        //D: Se consulta la lista de comisiones tramite en la base de datos y se establecen en el trámite
        comisionesTramiteVigentes = this.getTramiteService().buscarComisionesTramitePorIdTramite(
            tramite.getId());
        tramite.setComisionTramites(comisionesTramiteVigentes);

        return tramite;

    }

    /**
     * Método que se utiliza validar el tamaño maximo que se debe cargar un documento de firma
     * mecanica
     *
     * @author leidy.gonzalez
     */
    public long getMaximoTamanoAdjuntosFirmaEscaneada() {
        return new Long(SNCPropertiesUtil.getInstance().getProperty("sigc.maxUploadSizeFirma"));
    }

    /**
     * Elimina las replicas 97 y 98 de la base de datos y del FTP si estas existen
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @return
     */
    public void eliminarReplicasDeEdicion(Tramite tramite) {

        //No realiza ninguna accion si el tramite es goegrafico
        if (!tramite.isTramiteGeografico()) {
            return;
        }

        Documento copiaOriginal = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(tramite.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());
        Documento copiaFinal = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(tramite.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());

        //Se eliminan las replicas
        if (copiaOriginal != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(copiaOriginal);
        }

        if (copiaFinal != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(copiaFinal);
        }
    }

    /**
     * Método que realiza la copia de los datos de un {@link HPredio} y los retorna en un
     * {@link PPredio}
     *
     * @author david.cifuentes
     * @param hPredio
     * @return
     */
    public static PPredio copiarHPredioAPPredio(HPredio hPredio) {
        PPredio pPredio = new PPredio();

        pPredio.setAnioUltimaResolucion(hPredio.getAnioUltimaResolucion());
        pPredio.setAreaConstruccion(hPredio.getAreaConstruccion());
        pPredio.setAreaTerreno(hPredio.getAreaTerreno());
        pPredio.setBarrioCodigo(hPredio.getBarrioCodigo());
        pPredio.setChip(hPredio.getChip());
        pPredio.setCirculoRegistral(hPredio.getCirculoRegistral());
        pPredio.setCondicionPropiedad(hPredio.getCondicionPropiedad());
        pPredio.setConsecutivoCatastral(hPredio.getConsecutivoCatastral());
        pPredio.setConsecutivoCatastralAnterior(hPredio.getConsecutivoCatastralAnterior());
        pPredio.setCorregimientoCodigo(hPredio.getCorregimientoCodigo());
        pPredio.setDepartamento(hPredio.getDepartamento());
        pPredio.setDestino(hPredio.getDestino());
        pPredio.setDocumentoId(hPredio.getDocumentoId());
        pPredio.setEdificio(hPredio.getEdificio());
        pPredio.setEstado(hPredio.getEstado());
        pPredio.setEste(hPredio.getEste());
        pPredio.setEstrato(hPredio.getEstrato());
        pPredio.setFechaInscripcionCatastral(hPredio.getFechaInscripcionCatastral());
        pPredio.setFechaLog(hPredio.getFechaLog());
        pPredio.setFechaRegistro(hPredio.getFechaRegistro());
        pPredio.setId(hPredio.getId());
        pPredio.setLocalidadCodigo(hPredio.getLocalidadCodigo());
        pPredio.setManzanaCodigo(hPredio.getManzanaCodigo());
        pPredio.setMunicipio(hPredio.getMunicipio());
        pPredio.setNip(hPredio.getNip());
        pPredio.setNombre(hPredio.getNombre());
        pPredio.setNorte(hPredio.getNorte());
        pPredio.setNumeroPredial(hPredio.getNumeroPredial());
        pPredio.setNumeroPredialAnterior(hPredio.getNumeroPredialAnterior());
        pPredio.setNumeroRegistro(hPredio.getNumeroRegistro());
        pPredio.setNumeroRegistroAnterior(hPredio.getNumeroRegistroAnterior());
        pPredio.setNumeroUltimaResolucion(hPredio.getNumeroUltimaResolucion());
        pPredio.setPiso(hPredio.getPiso());
        pPredio.setPredio(hPredio.getPredio());
        pPredio.setSectorCodigo(hPredio.getSectorCodigo());
        pPredio.setTipo(hPredio.getTipo());
        pPredio.setTipoAvaluo(hPredio.getTipoAvaluo());
        pPredio.setTipoCatastro(hPredio.getTipoCatastro());
        pPredio.setUnidad(hPredio.getUnidad());
        pPredio.setUsuarioLog(hPredio.getUsuarioLog());
        pPredio.setZonaUnidadOrganica(hPredio.getZonaUnidadOrganica());

        return pPredio;
    }

    /**
     * Método que realiza la copia de los datos de un {@link HFichaMatrizPredio} y los retorna en un
     * {@link PFichaMatrizPredio}
     *
     * @author david.cifuentes
     * @param hFichaMatrizPredio
     * @return
     */
    public static PFichaMatrizPredio copiarHFichaMatrizPredioAPFichaMatrizPredio(
        HFichaMatrizPredio hFichaMatrizPredio) {

        PFichaMatrizPredio pFichaMatrizPredio = new PFichaMatrizPredio();

        pFichaMatrizPredio.setCoeficiente(hFichaMatrizPredio.getCoeficiente());
        pFichaMatrizPredio.setEstado(hFichaMatrizPredio.getEstado());
        pFichaMatrizPredio.setFechaLog(hFichaMatrizPredio.getFechaLog());
        pFichaMatrizPredio.setNumeroPredial(hFichaMatrizPredio.getNumeroPredial());
        pFichaMatrizPredio.setUsuarioLog(hFichaMatrizPredio.getUsuarioLog());

        return pFichaMatrizPredio;
    }

    /**
     * Método que realiza la copia de los datos de un {@link Predio} y los retorna en un
     * {@link PPredio}
     *
     * @author lorena.salamanca
     * @param predio
     * @return
     */
    public static PPredio copiarPredioAPPredio(Predio predio) {
        PPredio pPredio = new PPredio();

        pPredio.setAnioUltimaResolucion(predio.getAnioUltimaResolucion());
        pPredio.setAreaConstruccion(predio.getAreaConstruccion());
        pPredio.setAreaTerreno(predio.getAreaTerreno());
        pPredio.setBarrioCodigo(predio.getBarrioCodigo());
        pPredio.setChip(predio.getChip());
        pPredio.setCirculoRegistral(predio.getCirculoRegistral());
        pPredio.setCondicionPropiedad(predio.getCondicionPropiedad());
        pPredio.setConsecutivoCatastral(predio.getConsecutivoCatastral());
        pPredio.setConsecutivoCatastralAnterior(predio.getConsecutivoCatastralAnterior());
        pPredio.setCorregimientoCodigo(predio.getCorregimientoCodigo());
        pPredio.setDepartamento(predio.getDepartamento());
        pPredio.setDestino(predio.getDestino());
        pPredio.setEdificio(predio.getEdificio());
        pPredio.setEstado(predio.getEstado());
        pPredio.setEste(predio.getEste());
        pPredio.setEstrato(predio.getEstrato());
        pPredio.setFechaInscripcionCatastral(predio.getFechaInscripcionCatastral());
        pPredio.setFechaLog(predio.getFechaLog());
        pPredio.setFechaRegistro(predio.getFechaRegistro());
        pPredio.setId(predio.getId());
        pPredio.setLocalidadCodigo(predio.getLocalidadCodigo());
        pPredio.setManzanaCodigo(predio.getManzanaCodigo());
        pPredio.setMunicipio(predio.getMunicipio());
        pPredio.setNip(predio.getNip());
        pPredio.setNombre(predio.getNombre());
        pPredio.setNorte(predio.getNorte());
        pPredio.setNumeroPredial(predio.getNumeroPredial());
        pPredio.setNumeroPredialAnterior(predio.getNumeroPredialAnterior());
        pPredio.setNumeroRegistro(predio.getNumeroRegistro());
        pPredio.setNumeroRegistroAnterior(predio.getNumeroRegistroAnterior());
        pPredio.setNumeroUltimaResolucion(predio.getNumeroUltimaResolucion());
        pPredio.setPiso(predio.getPiso());
        pPredio.setPredio(predio.getPredio());
        pPredio.setSectorCodigo(predio.getSectorCodigo());
        pPredio.setTipo(predio.getTipo());
        pPredio.setTipoAvaluo(predio.getTipoAvaluo());
        pPredio.setTipoCatastro(predio.getTipoCatastro());
        pPredio.setUnidad(predio.getUnidad());
        pPredio.setUsuarioLog(predio.getUsuarioLog());
        pPredio.setZonaUnidadOrganica(predio.getZonaUnidadOrganica());

        return pPredio;
    }

    /**
     * Método que se encarga de determinar el usuario adecuado que debe firmar el oficio de no
     * procedencia del trámite y devuelve un arreglo de objetos que en la primera posición devuelve
     * el usuarioDTO y en la segunda la dirección del ftp donde se encuentra la firma digital del
     * usuario
     *
     * @param tipoTramite Tipo del trámite
     * @param usuario usuario que va a generar el reporte de no procedencia
     * @return Object En la posición 0 devuelve el nombre completo del usuario que firma el oficio
     * En la posición 1 devuelve el rol del usuario En la posición 2 devuelve la dirección en el ftp
     * de la firma del usuario
     * @author javier.aponte
     */
    public Object[] buscarUsuarioFirmaOficionNoProcedencia(String tipoTramite, UsuarioDTO usuario) {

        FirmaUsuario firma;
        String documentoFirma;

        List<UsuarioDTO> usuariosFirmanOficio;

        UsuarioDTO usuarioFirmaOficio = null;

        Object[] answer = new Object[3];

        if (ETramiteTipoTramite.RECURSO_DE_APELACION.getCodigo().equals(tipoTramite) ||
            ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getCodigo().equals(
                tipoTramite)) {
            usuariosFirmanOficio = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionEstructuraOrganizacional(), ERol.DIRECTOR_TERRITORIAL);
            answer[1] = "Director(a) Territorial";

        } else {
            usuariosFirmanOficio = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    usuario.getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_CONSERVACION);
            answer[1] = "Responsable Conservación";
        }

        if (usuariosFirmanOficio != null &&
             !usuariosFirmanOficio.isEmpty() &&
             usuariosFirmanOficio.get(0) != null) {
            usuarioFirmaOficio = usuariosFirmanOficio.get(0);
        }

        if (usuarioFirmaOficio != null && usuarioFirmaOficio.getNombreCompleto() != null) {

            answer[0] = usuarioFirmaOficio.getNombreCompleto();

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                usuarioFirmaOficio.getNombreCompleto());

            if (firma != null && usuario.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                answer[2] = documentoFirma;

            }

        }

        return answer;
    }

    /**
     * Método que retorna los valores para un combo box con los estados de una entidad registrados
     * en la tabla Dominio
     *
     * @author david.cifuentes
     * @return
     */
    public List<SelectItem> getEntidadEstadoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        for (Dominio dominio : this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ENTIDAD_ESTADO)) {
            lista.add(new SelectItem(dominio.getCodigo(), dominio.getCodigo() +
                 " - " + dominio.getValor()));
        }
        return lista;
    }

    /**
     * Retorna los valores el dominio TRAMITE_ESTADO_BLOQUEO.
     *
     * @author dumar.penuela
     * @return
     */
    public List<SelectItem> getTramiteEstadoBloqueoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_ESTADO_BLOQUEO));
        return lista;
    }

    // --------------------------------------------------------------- //
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio . Retorna
     * en la lista unicamente el valor del dominio.
     *
     * @return
     * @author david.cifuentes
     */
    public List<SelectItem> getSeccionesPlantilla() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SECCION_PLANTILLA_REPORTE));
        return lista;
    }

    /**
     * Método que retorna la lista de los formatos de descarga de un reporte
     *
     * @author david.cifuentes.
     * @return
     */
    public List<SelectItem> getRepFormatosDescarga() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REP_FORMATOS_DESCARGA));
        return lista;
    }

    /**
     * Método que retorna la lista de los formatos de descarga de un reporte estadistico
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getRepFormatosDescargaEst() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.REP_FORMATOS_DESCARGA));
        for (int i = 0; i< lista.size(); i++) {

            SelectItem si = lista.get(i);
            if(si.getValue().equals("CSV")){
                lista.remove(si);
            }

        }
        return lista;
    }

    /**
     * Método que retorna la lista de los rangos de días transcurridos utilizados en la consulta y
     * generación de reportes de radicación
     *
     * @author david.cifuentes.
     * @return
     */
    public List<SelectItem> getRepDiasTranscurridos() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombreOrdenadoPorId(EDominio.REP_DIAS_TRANSCURRIDOS));
        return lista;
    }

    /**
     * Método que retorna la lista de las categorías de los reportes utilizados en la consulta y
     * generación de parametrización de reportes a usuarios.
     *
     * @author david.cifuentes.
     * @return
     */
    public List<SelectItem> getRepCategoriaParametrizacion() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTE_CATEGORIA_PARAMETRI));
        return lista;
    }

    /**
     * Método que retorna la lista de los posibles estados de un reporte
     *
     * @author david.cifuentes.
     * @return
     */
    public List<SelectItem> getRepEstadoReporte() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTE_ESTADO));
        return lista;
    }

    /**
     * Retornar los posibles valores para la lista de selección de la zona fisica.
     *
     * @return Una lista con los posibles valores para una zona fisica.
     * @author leidy.gonzalez
     */
    public List<SelectItem> getZonaFisica() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ZONA_FISICA));
        return lista;
    }

    /**
     * Retornar los posibles valores para la lista de selección de la zona geoeconomica.
     *
     * @return Una lista con los posibles valores para una zona geoeconomica.
     * @author leidy.gonzalez
     */
    public List<SelectItem> getZonaEconomica() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ZONA_GEOECONOMICA));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TIPO_TITULO
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getTipoTitulo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TIPO_TITULO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los circulos registrales.
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getCirculoRegistral() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<CirculoRegistral> cr = this.getGeneralesService().obtenerListaCirculoRegistral();

        for (CirculoRegistral crTemp : cr) {
            lista.add(new SelectItem(crTemp.getCodigo(), crTemp.getCodigo() + " -\r" + crTemp.
                getNombre()));
        }
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los valores de la clase RepReporte
     * para el tipo de reporte
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getTipoReporte(String categoria) {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<RepReporte> rp = this.getGeneralesService().obtenerListaTipoReportePorTipoReporte(
            categoria);

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (RepReporte rpTemp : rp) {
            lista.add(new SelectItem(rpTemp.getId(), rpTemp.getTipoInforme()));
        }
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con la categoria de reportes del
     * dominio CATEGORIA_REPORTE
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getCategoriaReporte() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<String> nombresTiposReportes = this.getGeneralesService().
            obtenerListaNombreTipoReporte();

        if (nombresTiposReportes != null && !nombresTiposReportes.isEmpty()) {
            for (String repReporte : nombresTiposReportes) {
                if (repReporte != null) {
                    lista.add(new SelectItem(repReporte));
                }
            }

        }

        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PRODUCTO_CATASTRAL_FORMATO
     *
     * @author javier.aponte modificado por leidy.gonzalez
     * @return
     */
    public List<SelectItem> getRegPreFormatoReportes() {
        //v1.1.9
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosSeleccionarV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.REPORTES_FORMATO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * AÑO_REPORTE
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getAnoReporte() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ANIO_REPORTE));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TIPO_BLOQUEO
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getTipoBloqueo() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TIPO_BLOQUEO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * PREDIO_TIPO_DEL_ESTADO. Retorna en las lista los códigos y valores.
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<SelectItem> getPredioTipoEstado() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        this.adicionarDatosCV(lista, this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_TIPO_DEL_ESTADO));
        return lista;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del dominio
     * TRAMITE_TIPO_TRAMITE y la opcion seleccionar.
     *
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> getTramiteTipoTramiteVS() {

        List<String[]> values = this.getTramiteService().obtenerTipoTramiteCompuestoCodigo();
        List<SelectItem> lista = new ArrayList<SelectItem>();

        for (String[] val : values) {
            lista.add(new SelectItem(val[0], val[1]));
        }

        Collections.sort(lista, new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem lhs, SelectItem rhs) {
                return lhs.getLabel().compareTo(rhs.getLabel());
            }
        });

        lista.add(0, new SelectItem("", "Seleccionar..."));
        return lista;
    }
    
    /**
	 * Retorna los valores para un combo box (selectOneMenu) con los registros
	 * del dominio UNIDAD_CONSTRUCION_USO ordenados por nombre.
	 * @author felipe.cadena
	 * @return
	 */
	public List<SelectItem> getUnidadConstruccionCVOrderByValor() {
		List<SelectItem> lista = new ArrayList<SelectItem>();
		this.adicionarDatosV(lista, this.getGeneralesService()
				.findByNombreOrderByValor(EDominio.UNIDAD_CONSTRUCION_USO));
		return lista;
	}

//end of class
}
