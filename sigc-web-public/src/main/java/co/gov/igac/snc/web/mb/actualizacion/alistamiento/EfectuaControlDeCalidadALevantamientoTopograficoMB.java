package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.AsignacionControlCalidad;
import co.gov.igac.snc.persistence.entity.actualizacion.ControlCalidadCriterio;
import co.gov.igac.snc.persistence.entity.actualizacion.CriterioControlCalidad;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacionInf;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.EAsuntoCorreoElectronicoActualizacion;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author lorena.salamanca
 * @modified javier.barajas adicion de metodos para interactuar con la capa de negocios, creacion de
 * registros en la tabla de criterios de control de calidad
 * @cu CU-NP-FA-212 Efectuar control de calidad a levantamiento topográfico
 */
@Component("efectuarControlDeCalidadALevantamientoTopografico")
@Scope("session")
public class EfectuaControlDeCalidadALevantamientoTopograficoMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        EfectuaControlDeCalidadALevantamientoTopograficoMB.class);

    @Autowired
    private IContextListener contextoWeb;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Lista ActualizacionLevantamiento manzanas Hacer Control de Calidad
     */
    private List<ActualizacionLevantamiento> manzanasControlCalidad;

    /**
     * Lista ActualizacionLevantamiento manzanas Hacer Control de Calidad guarda anterior consulta
     */
    private List<ActualizacionLevantamiento> manzanasControlCalidadH;

    /**
     * Lista LevantamientoAsignacion manzanas Hacer Control de Calidad
     */
    private List<LevantamientoAsignacion> listaLevantamientos;

    /**
     * Lista Recurso humano
     */
    private List<RecursoHumano> listaRecursoHumano;

    /**
     * Seleccion del topografo
     */
    private String selecionTopografo;

    /**
     * Topografo seleccionado del recurso humano
     */
    private RecursoHumano topografoEncargado;

    /**
     * Seleccion de topografo
     */
    private ArrayList<SelectItem> selectTopografo;

    /**
     * Bandera para mostrar las manzas a exportar
     */
    private boolean rendererManzanasExportar;

    /**
     * Seleccion de datatable
     */
    private ActualizacionLevantamiento[] selectManzanas;

    /**
     * Coedigo de manzanas
     */
    private String manzanaCodigo;

    /**
     * Seleccion de un nuevo topografo
     */
    private String nuevoTopografo;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * Opcion de exportacion
     */
    private String seleccionExportacion;

    private String rutaArchivoExportar;

    /**
     * LIsta con los controles de calidad
     */
    private List<ControlCalidadCriterio> listaControlCalidadCriterioGeo;

    private List<ControlCalidadCriterio> listaControlCalidadCriterioLev;

    private List<ControlCalidadCriterio> listaControlCalidadCriterio;

    private List<ControlCalidadCriterio> listaControlCalidadCriterioGuardar;

    private List<ControlCalidadCriterio> listaControlCalidadCriterioGuardarCriterio;

    /**
     * Seleccion de criterios de calidad
     *
     */
    private ControlCalidadCriterio[] cumpleControlCalidadGeo;

    private ControlCalidadCriterio[] cumpleControlCalidadLev;

    private CriterioControlCalidad controlCalidadGeoreferenciacion;

    private CriterioControlCalidad controlCalidadLevantamiento;

    /**
     * Criterio control de calidad
     */
    private List<CriterioControlCalidad> listaCriterioControl;

    /**
     * Variable para manejar la actualizacion levantamiento
     */
    private UsuarioDTO usuario;

    private List<SelectItem> departamentosColombia;

    private java.util.Date fecha;

    /**
     * Datos AsignacionCotnrolCalidad
     */
    private String medicionChequeo;

    private String resultado;

    private String resultadoMedicion;

    private String observaciones;
    /**
     * Variable para saber si selecciono manzanas
     */

    private boolean renderedControlCalidad;

    /**
     * Tamaño de la lista de manzanas a hacer control de calidad
     */
    private int tamanoListaManzanas;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;

    /**
     * Variable de tipo streamed content que contiene el archivo con el .zip con la exportacion de
     * laz manzanas
     */
    private StreamedContent archivosADescargar;

    /**
     * Cuando guarda deshabilita el boton guardar
     */
    private boolean btnGuardarHabilita;

    /**
     * Cuando habilita el boton enviar a topografo cuando el control de calidad no a sido aprobado
     */
    private boolean btnHabilitaEnvio;

    /**
     * Boton que habilita la exportacion de la muestra
     */
    private boolean btnHabilitaExportar;

    /**
     * Variable de estado de la exportacion (exitosa o no)
     */
    private boolean exportacionOK;

    /**
     * Informe levantamiento topografico
     */
    private LevantamientoAsignacionInf informeLevantamiento;

    /**
     * Levantamiento Asignacion seleccionado
     */
    private LevantamientoAsignacion levantamientoAsignado;
    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDTO;
    /**
     * Manejo de reportes
     */

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 24/10/12

        // Obtener la actualizacióin del arbol de tareas.
        this.setActualizacionSeleccionada(this.getActualizacionService()
            .recuperarActualizacionPorId(450L));

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.manzanaCodigo = null;
        this.manzanaCodigo = null;
        this.moduleLayer = "ac";
        this.moduleTools = "ac";
        this.selectTopografo = new ArrayList<SelectItem>();
        this.listaRecursoHumano = new ArrayList<RecursoHumano>();
        this.listaLevantamientos = this.getActualizacionService().
            buscarTopografosLevantamientoActualizacion(
                this.getActualizacionService().buscarActualizacionLevantamientoPorActualizacionId(
                    this.actualizacionSeleccionada.getId(), null));
        this.listaRecursoHumano = this.getActualizacionService().buscarTopografosActualizacionId(
            this.actualizacionSeleccionada.getId());
        this.adicionarDatosVitem(this.selectTopografo, this.listaRecursoHumano);
        this.fecha = new java.util.Date();
        this.cargarCriteriosControlCalidad();
        this.renderedControlCalidad = false;
        this.btnGuardarHabilita = true;
        this.btnHabilitaEnvio = false;
        this.btnHabilitaExportar = false;
        this.exportacionOK = true;
    }

    // **************************************************************************************************************************
    // Getters y Setters 
    // **************************************************************************************************************************
    public LevantamientoAsignacion getLevantamientoAsignado() {
        return levantamientoAsignado;
    }

    public void setLevantamientoAsignado(
        LevantamientoAsignacion levantamientoAsignado) {
        this.levantamientoAsignado = levantamientoAsignado;
    }

    public LevantamientoAsignacionInf getInformeLevantamiento() {
        return informeLevantamiento;
    }

    public void setInformeLevantamiento(
        LevantamientoAsignacionInf informeLevantamiento) {
        this.informeLevantamiento = informeLevantamiento;
    }

    public boolean isExportacionOK() {
        return exportacionOK;
    }

    public void setExportacionOK(boolean exportacionOK) {
        this.exportacionOK = exportacionOK;
    }

    public boolean isBtnHabilitaExportar() {
        return btnHabilitaExportar;
    }

    public void setBtnHabilitaExportar(boolean btnHabilitaExportar) {
        this.btnHabilitaExportar = btnHabilitaExportar;
    }

    public boolean isBtnHabilitaEnvio() {
        return btnHabilitaEnvio;
    }

    public void setBtnHabilitaEnvio(boolean btnHabilitaEnvio) {
        this.btnHabilitaEnvio = btnHabilitaEnvio;
    }

    public boolean isBtnGuardarHabilita() {
        return btnGuardarHabilita;
    }

    public void setBtnGuardarHabilita(boolean btnGuardarHabilita) {
        this.btnGuardarHabilita = btnGuardarHabilita;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public void setActualizacionSeleccionada(Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<SelectItem> getDepartamentosColombia() {
        return departamentosColombia;
    }

    public void setDepartamentosColombia(List<SelectItem> departamentosColombia) {
        this.departamentosColombia = departamentosColombia;
    }

    public String getNuevoTopografo() {
        return nuevoTopografo;
    }

    public void setNuevoTopografo(String nuevoTopografo) {
        this.nuevoTopografo = nuevoTopografo;
    }

    public java.util.Date getFecha() {
        return fecha;
    }

    public void setFecha(java.util.Date fecha) {
        this.fecha = fecha;
    }

    public List<LevantamientoAsignacion> getListaLevantamientos() {
        return listaLevantamientos;
    }

    public void setListaLevantamientos(List<LevantamientoAsignacion> listaLevantamientos) {
        this.listaLevantamientos = listaLevantamientos;
    }

    public List<ActualizacionLevantamiento> getManzanasControlCalidad() {
        return manzanasControlCalidad;
    }

    public void setManzanasControlCalidad(
        List<ActualizacionLevantamiento> manzanasControlCalidad) {
        this.manzanasControlCalidad = manzanasControlCalidad;
    }

    public String getSelecionTopografo() {
        return selecionTopografo;
    }

    public void setSelecionTopografo(String selecionTopografo) {
        this.selecionTopografo = selecionTopografo;
    }

    public ArrayList<SelectItem> getSelectTopografo() {
        return selectTopografo;
    }

    public void setSelectTopografo(ArrayList<SelectItem> selectTopografo) {
        this.selectTopografo = selectTopografo;
    }

    public boolean isRendererManzanasExportar() {
        return rendererManzanasExportar;
    }

    public void setRendererManzanasExportar(boolean rendererManzanasExportar) {
        this.rendererManzanasExportar = rendererManzanasExportar;
    }

    public ActualizacionLevantamiento[] getSelectManzanas() {
        return selectManzanas;
    }

    public void setSelectManzanas(ActualizacionLevantamiento[] selectManzanas) {
        this.selectManzanas = selectManzanas;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getSeleccionExportacion() {
        return seleccionExportacion;
    }

    public void setSeleccionExportacion(String seleccionExportacion) {
        this.seleccionExportacion = seleccionExportacion;
    }

    public String getRutaArchivoExportar() {
        return rutaArchivoExportar;
    }

    public void setRutaArchivoExportar(String rutaArchivoExportar) {
        this.rutaArchivoExportar = rutaArchivoExportar;
    }

    public void setListaRecursoHumano(List<RecursoHumano> listaRecursoHumano) {
        this.listaRecursoHumano = listaRecursoHumano;
    }

    public List<RecursoHumano> getListaRecursoHumano() {
        return listaRecursoHumano;
    }

    public void setControlCalidadGeoreferenciacion(
        CriterioControlCalidad controlCalidadGeoreferenciacion) {
        this.controlCalidadGeoreferenciacion = controlCalidadGeoreferenciacion;
    }

    public CriterioControlCalidad getControlCalidadGeoreferenciacion() {
        return controlCalidadGeoreferenciacion;
    }

    public void setControlCalidadLevantamiento(
        CriterioControlCalidad controlCalidadLevantamiento) {
        this.controlCalidadLevantamiento = controlCalidadLevantamiento;
    }

    public CriterioControlCalidad getControlCalidadLevantamiento() {
        return controlCalidadLevantamiento;
    }

    public RecursoHumano getTopografoEncargado() {
        return topografoEncargado;
    }

    public void setTopografoEncargado(RecursoHumano topografoEncargado) {
        this.topografoEncargado = topografoEncargado;
    }

    public List<ControlCalidadCriterio> getListaControlCalidadCriterioGeo() {
        return listaControlCalidadCriterioGeo;
    }

    public void setListaControlCalidadCriterioGeo(
        List<ControlCalidadCriterio> listaControlCalidadCriterioGeo) {
        this.listaControlCalidadCriterioGeo = listaControlCalidadCriterioGeo;
    }

    public List<ControlCalidadCriterio> getListaControlCalidadCriterioLev() {
        return listaControlCalidadCriterioLev;
    }

    public void setListaControlCalidadCriterioLev(
        List<ControlCalidadCriterio> listaControlCalidadCriterioLev) {
        this.listaControlCalidadCriterioLev = listaControlCalidadCriterioLev;
    }

    public List<CriterioControlCalidad> getListaCriterioControl() {
        return listaCriterioControl;
    }

    public void setListaCriterioControl(
        List<CriterioControlCalidad> listaCriterioControl) {
        this.listaCriterioControl = listaCriterioControl;
    }

    public ControlCalidadCriterio[] getCumpleControlCalidadGeo() {
        return cumpleControlCalidadGeo;
    }

    public void setCumpleControlCalidadGeo(
        ControlCalidadCriterio[] cumpleControlCalidadGeo) {
        this.cumpleControlCalidadGeo = cumpleControlCalidadGeo;
    }

    public ControlCalidadCriterio[] getCumpleControlCalidadLev() {
        return cumpleControlCalidadLev;
    }

    public void setCumpleControlCalidadLev(
        ControlCalidadCriterio[] cumpleControlCalidadLev) {
        this.cumpleControlCalidadLev = cumpleControlCalidadLev;
    }

    public boolean isRenderedControlCalidad() {
        return renderedControlCalidad;
    }

    public void setRenderedControlCalidad(boolean renderedControlCalidad) {
        this.renderedControlCalidad = renderedControlCalidad;
    }

    public List<ControlCalidadCriterio> getListaControlCalidadCriterio() {
        return listaControlCalidadCriterio;
    }

    public void setListaControlCalidadCriterio(
        List<ControlCalidadCriterio> listaControlCalidadCriterio) {
        this.listaControlCalidadCriterio = listaControlCalidadCriterio;
    }

    public String getMedicionChequeo() {
        return medicionChequeo;
    }

    public void setMedicionChequeo(String medicionChequeo) {
        this.medicionChequeo = medicionChequeo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
//		if(resultado.equals("Aprobado")){
//			this.btnHabilitaEnvio=false;
//		}
        this.resultado = resultado;
    }

    public String getResultadoMedicion() {
        return resultadoMedicion;
    }

    public void setResultadoMedicion(String resultadoMedicion) {
        this.resultadoMedicion = resultadoMedicion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<ControlCalidadCriterio> getListaControlCalidadCriterioGuardar() {
        return listaControlCalidadCriterioGuardar;
    }

    public void setListaControlCalidadCriterioGuardar(
        List<ControlCalidadCriterio> listaControlCalidadCriterioGuardar) {
        this.listaControlCalidadCriterioGuardar = listaControlCalidadCriterioGuardar;
    }

    public int getTamanoListaManzanas() {
        return tamanoListaManzanas;
    }

    public void setTamanoListaManzanas(int tamanoListaManzanas) {
        this.tamanoListaManzanas = tamanoListaManzanas;
    }

    public void setManzanasControlCalidadH(List<ActualizacionLevantamiento> manzanasControlCalidadH) {
        this.manzanasControlCalidadH = manzanasControlCalidadH;
    }

    public List<ActualizacionLevantamiento> getManzanasControlCalidadH() {
        return manzanasControlCalidadH;
    }

    public List<ControlCalidadCriterio> getListaControlCalidadCriterioGuardarCriterio() {
        return listaControlCalidadCriterioGuardarCriterio;
    }

    public void setListaControlCalidadCriterioGuardarCriterio(
        List<ControlCalidadCriterio> listaControlCalidadCriterioGuardarCriterio) {
        this.listaControlCalidadCriterioGuardarCriterio = listaControlCalidadCriterioGuardarCriterio;
    }

    public StreamedContent getArchivosADescargar() {
        return archivosADescargar;
    }

    public void setArchivosADescargar(StreamedContent archivosADescargar) {
        this.archivosADescargar = archivosADescargar;
    }

    // **************************************************************************************************************************
    // Funciones
    // **************************************************************************************************************************
    /**
     * Cargar manzana para realizar la exportacion a formato .shp .dxf .dwg
     *
     * @author javier.barajas
     */
    public void cargarManzanasaExportar() {
        this.rendererManzanasExportar = true;

        try {
            this.topografoEncargado = this.getActualizacionService().buscarRecursoHumanoporId(Long.
                valueOf(this.selecionTopografo));
        } catch (Exception e) {
            LOGGER.error("No se pudo obtener el topografo de la actualización", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo obtener el topografo de la actualización",
                "No se pudo obtener el topografo de la actualización"));
        }
        try {
            this.manzanasControlCalidad = this.getActualizacionService().
                buscarManzanasporTopografosActualizacionId(
                    this.actualizacionSeleccionada.getId(), Long.valueOf(this.selecionTopografo));
            this.tamanoListaManzanas = this.manzanasControlCalidad.size();
        } catch (Exception e) {
            LOGGER.error("No se pudo obtener las manzanas asignadas al topografo", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo obtener las manzanas asignadas al topografo",
                "No se pudo obtener las manzanas asignadas al topografo"));
        }
    }

    /**
     * Convertir una lista en una lista de seleccion(selectItem)
     *
     * @author javier.barajas
     *
     */
    private void adicionarDatosVitem(List<SelectItem> lista,
        List<RecursoHumano> listRecursoHumano) {

        for (RecursoHumano t : listRecursoHumano) {
            if (t.getNombre() != null) {
                lista.add(new SelectItem(String.valueOf(t.getId()), t.getNombre()));
            }
        }
    }

    /**
     * Se lanza cuando selecciona una manzana
     *
     * @param evento
     *
     * @author javier.barajas
     */
    public void rowSelected(org.primefaces.event.SelectEvent evento) {
        this.obtenerManzanas();
        this.renderedControlCalidad = true;
    }

    /**
     * Como la seleccion es multiple saca las manzanas y las coloca en una lista
     *
     * @author javier.barajas
     */
    public void obtenerManzanas() {
        int contador = 0;
        if (this.selectManzanas.length > 0) {
            if (this.selectManzanas.length == 1) {
                this.manzanaCodigo = this.selectManzanas[0].getIdentificador().toString();
            } else {
                for (ActualizacionLevantamiento al : this.selectManzanas) {
                    if (contador == 0) {
                        this.manzanaCodigo = al.getIdentificador().toString();
                    } else {
                        this.manzanaCodigo = this.manzanaCodigo + al.getIdentificador().toString();
                    }
                    if (this.selectManzanas.length - 1 != contador) {
                        this.manzanaCodigo = this.manzanaCodigo + ",";
                        contador++;
                    }
                }
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No ha seleccionado ninguna manzana",
                "No ha seleccionado ninguna manzana"));

        }

    }

    /**
     * Letreros de informacion
     *
     * @author javier.barajas
     */
    public void rowSelectedExportar() {
        LOGGER.info("formato:" + this.seleccionExportacion);
        LOGGER.info("cod manzanas:" + this.manzanaCodigo);
        LOGGER.info("usuario:" + this.usuario.getNombreCompleto());
    }

    /**
     * se lanza cuando se seleccion un control de calidad
     *
     * @author javier.barajas
     */
    public void rowSelectedControlCalidadGeo() {
        LOGGER.info("tamaño seleccion:" + this.cumpleControlCalidadGeo.length);

    }

    /**
     * se lanza cuando se seleccion un control de calidad
     *
     * @author javier.barajas
     */
    public void rowSelectedControlCalidadLev() {
        LOGGER.info("tamaño seleccion:" + this.cumpleControlCalidadLev.length);

    }

    /**
     * Funcion para exportar las manzanas
     *
     * @author javier.barajas
     */
    public void exportarManzana() {

        for (ActualizacionLevantamiento al : this.manzanasControlCalidad) {
            this.manzanaCodigo = al.getIdentificador();
        }
        LOGGER.info("MANZNA CODIGO" + this.manzanaCodigo);
        LOGGER.info("SELECCION EXPORTACION" + this.seleccionExportacion);
        LOGGER.info("USUARIO" + this.usuario.getNombreCompleto());
        try {
            this.rutaArchivoExportar = this.getActualizacionService().
                exportarManzanasaTopografos(this.manzanaCodigo, this.seleccionExportacion,
                    this.usuario);
            this.descargarArchivosaExportar();
            LOGGER.info("Ruta Archivo de exportacion:" + this.rutaArchivoExportar);
        } catch (Exception e) {
            //LOGGER.error(e.getMessage(), e);
            LOGGER.error("Error a generar el archivo de exportación", e);
            this.exportacionOK = false;
        }

    }

    /**
     * Carga los criterios de calidad segun se definieron en el caso de uso
     *
     * @author javier.barajas
     */
    public void cargarCriteriosControlCalidad() {
        this.listaCriterioControl = new ArrayList<CriterioControlCalidad>();
        this.listaControlCalidadCriterioGeo = new ArrayList<ControlCalidadCriterio>();
        this.listaControlCalidadCriterio = new ArrayList<ControlCalidadCriterio>();
        ControlCalidadCriterio controlFila = new ControlCalidadCriterio();
        this.controlCalidadGeoreferenciacion = this.getActualizacionService().
            buscarCriterioControlCalidad(1L);
        this.controlCalidadLevantamiento = this.getActualizacionService().
            buscarCriterioControlCalidad(2L);
        controlFila.setDescripcion("Especificaciónes equipos GPS");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Registro mantenimiento y calibración equipos");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Armado del equipo");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Fecha y tiempo de rastreo");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Numero de satélites disponibles");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Diligenciamiento hojas de campo");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Datos registrados en el GPS");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Amarres a punto de control");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Numero de puntos posicionados");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Materialización y descripción de puntos");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Registro fotográfico de puntos");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Procesamiento de la información");
        controlFila.setCriterioControlCalidad(this.controlCalidadGeoreferenciacion);
        this.listaControlCalidadCriterioGeo.add(controlFila);

        this.listaCriterioControl.add(this.controlCalidadGeoreferenciacion);

        //-----------Control Calidad Levantamiento
        this.listaControlCalidadCriterioLev = new ArrayList<ControlCalidadCriterio>();
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Número de puntos de control");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Especificación estaciones totales");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Registro de calibración y mantenimiento");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Limite de perímetro urbano definido de acuerdo a la norma");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Plano con todas las manzanas catastrales existentes");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Forma de manzanas consistente");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Levantamiento con sandinel y/o andén y ancho de vías");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Toponomia y convenciones cartográficas");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Cartera de campo consistente y con información de proyecto");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Numeración de manzanas según convenciones");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Cálculo y ajuste de poligonales");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Precisión en el cierre");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        controlFila = new ControlCalidadCriterio();
        controlFila.setDescripcion("Cierre angular de poligonales");
        controlFila.setCriterioControlCalidad(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterioLev.add(controlFila);
        this.listaCriterioControl.add(this.controlCalidadLevantamiento);
        this.listaControlCalidadCriterio.addAll(listaControlCalidadCriterioGeo);
        this.listaControlCalidadCriterio.addAll(listaControlCalidadCriterioLev);

    }

    /**
     * Guarda el control de calidad tanto en las tablas AsginacionControlCalidad y la lista de
     * ControlCalidadCriterio
     *
     * @author javier.barajas
     */
    public void guardarControlCalidad() {

        LevantamientoAsignacion la = new LevantamientoAsignacion();
        this.cumpleCriterioControl();
        try {
            la = this.getActualizacionService().buscarLevantamientoAsignacionId(
                this.selectManzanas[0].getLevantamientoAsignacion().getId());
        } catch (Exception e) {
            LOGGER.error("No se pudo obtener el Levantamiento Asignación", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo obtener el Levantamiento Asignación",
                "No se pudo obtener el Levantamiento Asignación"));
        }
        AsignacionControlCalidad asignaControl = new AsignacionControlCalidad();
        asignaControl.setControlCalidadCriterios(listaControlCalidadCriterio);
        asignaControl.setFecha(this.fecha);
        asignaControl.setMedicionChequeo(this.medicionChequeo);
        asignaControl.setObservaciones(this.observaciones);
        asignaControl.setResultado(this.resultado);
        asignaControl.setResultadoMedicion(this.resultadoMedicion);
        this.listaControlCalidadCriterioGuardar = new ArrayList<ControlCalidadCriterio>();
        if (!asignaControl.getObservaciones().equals(null) ||
            !asignaControl.getMedicionChequeo().equals(null) ||
            !asignaControl.getResultadoMedicion().equals(null) ||
            !asignaControl.getResultado().equals(null)) {
            asignaControl.setLevantamientoAsignacion(la);
            asignaControl.setRecursoHumano(this.topografoEncargado);
            asignaControl.setFechaLog(fecha);
            try {
                asignaControl = this.getActualizacionService().
                    guardarYactualizarAsignacionControlCalidad(asignaControl);
            } catch (Exception e) {
                LOGGER.error("No se pudo guardar el control de calidad", e);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "No se pudo guardar la Asignación al Control de Calidad",
                    "No se pudo guardar la Asignación al Control de Calidad"));
            }
            for (ControlCalidadCriterio ccc : this.listaControlCalidadCriterio) {
                ccc.setAsignacionControlCalidad(asignaControl);
                ccc.setUsuarioLog(this.usuario.getLogin());
                ccc.setFechaLog(fecha);
                this.listaControlCalidadCriterioGuardar.add(ccc);
            }
            try {
                this.listaControlCalidadCriterioGuardar = this.getActualizacionService().
                    guardarYactualizarControlCalidad(this.listaControlCalidadCriterioGuardar);
            } catch (Exception e) {
                LOGGER.error("No se pudo guardar el control de calidad", e);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN, "No se pudo guardar el control de calidad",
                    "No se pudo guardar el control de calidad"));
            }
        }
        asignaControl = new AsignacionControlCalidad();
        this.btnGuardarHabilita = false;

    }

    /**
     * Verifica y cambia el campo de si cumple o no con el Control de calidad
     *
     * @author javier.barajas
     */
    public void cumpleCriterioControl() {
        this.listaControlCalidadCriterioGuardarCriterio = new ArrayList<ControlCalidadCriterio>();
        for (ControlCalidadCriterio ccc : this.listaControlCalidadCriterio) {
            ccc.setCumple("NO");
        }

        for (ControlCalidadCriterio ccc : this.listaControlCalidadCriterio) {
            for (ControlCalidadCriterio cccv : this.cumpleControlCalidadGeo) {
                if (ccc.equals(cccv)) {
                    ccc.setCumple("SI");
                }
            }
            for (ControlCalidadCriterio cccv : this.cumpleControlCalidadLev) {
                if (ccc.equals(cccv)) {
                    ccc.setCumple("SI");
                }
            }
        }
        for (ControlCalidadCriterio ccc : this.listaControlCalidadCriterio) {
            LOGGER.info("Control Criterio (descrip): " + ccc.getDescripcion() + " cumple:" + ccc.
                getCumple());
        }
    }

    /**
     * Letrero informativos
     *
     * @author javier.barajas
     */
    public void cumpleCriterio() {

    }

    /**
     * Muestra el tamaño de la seleccion
     *
     * @author javier.barajas
     */
    public void verificarControlCalidad() {
    }

    /**
     * Generar Muestra (10% para menos 500)
     *
     * @author javier.barajas
     * @return
     */
    public void generarMuestra() {

        if (this.tamanoListaManzanas > 10) {
            this.manzanasControlCalidadH = new ArrayList<ActualizacionLevantamiento>();
            this.manzanasControlCalidadH = this.manzanasControlCalidad;
            int tamanoMuestra = 0;
            int aleatorio = 0;

            tamanoMuestra = (int) Math.round(this.tamanoListaManzanas * (0.1));
            int[] uno = new int[tamanoMuestra];
            for (int i = 0; i < tamanoMuestra; i++) {
                aleatorio = (int) (Math.random() * this.tamanoListaManzanas);
                if (i == 0) {
                    uno[i] = aleatorio;
                } else {
                    if (!uno.equals(aleatorio)) {
                    } else {
                        i--;
                    }
                }
            }
            this.manzanasControlCalidad = new ArrayList<ActualizacionLevantamiento>();
            for (int indexManazana : uno) {
                this.manzanasControlCalidad.add(this.manzanasControlCalidadH.get(indexManazana));
            }
            this.btnHabilitaExportar = true;
            LOGGER.info("TAMAÑO DE LA MUESTRA" + this.manzanasControlCalidad.size());
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "El numero de manzanas debe ser mayor a diez(10)",
                "El numero de manzanas debe ser mayor a diez(10)"));
            LOGGER.warn("El numero de manzanas debe ser mayor a diez(10)",
                "El numero de manzanas debe ser mayor a diez(10)");
        }

    }

    /**
     * Funcion que permite descargar los archivos
     *
     * @author javier.barajas
     */
    public void descargarArchivosaExportar() {

        File archivoDocumentosAsociadosATramite = new File(this.rutaArchivoExportar);
        if (!archivoDocumentosAsociadosATramite.exists()) {
            LOGGER.warn("Error el archivo de origen no existe");
            this.addMensajeError("Ocurrió un error al descargar los archivos");
            return;
        }

        InputStream stream;

        try {
            stream = new FileInputStream(archivoDocumentosAsociadosATramite);
            this.setArchivosADescargar(new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_ZIP, "archivo_exportacion.zip"));
        } catch (FileNotFoundException e) {
            LOGGER.error(
                "Archivo no encontrado, no se puede descargar los documentos asociados al trámite",
                e);
        }
    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("efectuarControlDeCalidadALevantamientoTopografico");
        this.tareasPendientesMB.init();
        //this.init();
        return "index";
    }

    /**
     * Generacion del reporte de levantamiento topografico
     *
     * @author javier.barajas
     */
    public void generarReporteLevantamientoTopografico() {

        this.informeLevantamiento = new LevantamientoAsignacionInf();

        this.levantamientoAsignado = new LevantamientoAsignacion();

        try {
            this.levantamientoAsignado = this.getActualizacionService().
                buscaLevantamientoAsignacionconRecursoHumanoId(this.topografoEncargado.getId());

        } catch (Exception e) {
            LOGGER.error("No se pudo cargar el Levantamiento Asignado ", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo cargar el Levantamiento Asignado ",
                "No se pudo cargar el Levantamiento Asignado "));
        }
        try {
            this.informeLevantamiento = this.getActualizacionService().
                buscaLevantamientoTopograficoInforme(this.levantamientoAsignado.getId());
            EReporteServiceSNC enumeracionReporte =
                EReporteServiceSNC.INFORME_LEVANTAMIENTO_TOPOGRAFICO;
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.
                put("LEVANTAMIENTO_ASIGNACION_INF_ID", "" + this.informeLevantamiento.getId());
            parameters.put("DIRECTOR_TERRITORIAL", "JAIME LEAL");
            try {
                this.reporteDTO = this.reportsService.generarReporte(parameters,
                    enumeracionReporte, this.usuario);

                this.guardarDocumentosActualizacionYAlfrescoDocumento(this.reporteDTO.
                    getRutaCargarReporteAAlfresco());
            } catch (Exception e) {
                LOGGER.error("No se pudo generar el Informe del Levantamiento Asignado ", e);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "No se pudo generar el Informe del Levantamiento Asignado ",
                    "No se pudo generar el Informe del Levantamiento Asignado "));
            }
        } catch (Exception e) {
            LOGGER.error("No se pudo cargar el Informe del Levantamiento Asignado ", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN,
                "No se pudo cargar el Informe del Levantamiento Asignado ",
                "No se pudo cargar el Informe del Levantamiento Asignado "));
        }

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el cargue de los parametros para el envio del correo electrónico en la
     * solicitud de digitalización y los setea en el managed bean
     * VisualizacionCorreoActualizacionMB.
     *
     * @author david.cifuentes
     */
    public void cargarParametrosSolicitudDigitalizacion() {

        // TODO :: david.cifuentes :: Ajustar los parámetros que están quemados
        // :: 20/11/2012
        // Asunto
        String asunto;
        // Listado de archivos adjuntos
        String[] adjuntos = null;
        // Nombre de los archivos adjuntos
        String[] titulosAdjuntos = null;
        // Plantilla de contenido
        String codigoPlantillaContenido;
        // Destinatarios
        String[] destinatarios = new String[]{"javier.barajas@igac.gov.co"};
        // Remitente
        String remitente = "javier.barajas@igac.gov.co";

        codigoPlantillaContenido = EPlantilla.MENSAJE_AJUSTES_CONTROL_CALIDAD_LEVANTOPOGRAFICO
            .getCodigo();
        asunto =
            EAsuntoCorreoElectronicoActualizacion.ASUNTO_AJUSTES_CONTROL_CALIDAD_LEVATOPOGRAFICO.
                getAsunto();

        // Set del parametro municipio en el asunto.
        // Se instancia el messageFormat.
        Locale colombia = new Locale("ES", "es_CO");
        MessageFormat messageFormat = new MessageFormat(asunto, colombia);
        asunto = messageFormat
            .format(new String[]{this.actualizacionSeleccionada
            .getMunicipio().getNombre()});

        // Parámetros de la plantilla de contenido
        UsuarioDTO usuarioDestionatario = MenuMB.getMenu().getUsuarioDto();
        String[] parametrosPlantillaContenido = new String[6];
        parametrosPlantillaContenido[0] = "Cuerpo de correo electrónico";
        parametrosPlantillaContenido[1] = this.topografoEncargado.getNombre();
        parametrosPlantillaContenido[2] = this.cadenaManzanas();
        parametrosPlantillaContenido[3] = this.tablaCriteriosControlCalidad();
        parametrosPlantillaContenido[4] = this.observaciones;

        if (this.usuario.getPrimerRol().equals("responsable_topografia")) {
            parametrosPlantillaContenido[5] = this.usuario.getNombreCompleto() + "</br></br>" +
                "Responsable control de calidad de levantamiento topográfico";
        } else {
            parametrosPlantillaContenido[5] = this.usuario.getNombreCompleto() + "</br></br>" +
                "Responsable formación o actualización de la formación";
        }

        // Cargar parámetros en el managed bean del componente.
        VisualizacionCorreoActualizacionMB visualCorreoMB =
            (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                .getManagedBean("visualizacionCorreoActualizacion");
        visualCorreoMB.inicializarParametros(asunto, destinatarios, remitente,
            adjuntos, codigoPlantillaContenido, null,
            parametrosPlantillaContenido, titulosAdjuntos);
    }

    /**
     * Cadena de manzanas a corregir
     *
     * @author javier.barajas
     */
    public String cadenaManzanas() {
        String cadena = new String();
        cadena = "<ul>";

        for (ActualizacionLevantamiento al : this.selectManzanas) {
            //for(int i=0;i<this.selectManzanas.length;i++){

            cadena += "<li>" + al.getIdentificador();

        }
        cadena += "</ul>";
        //LOGGER.debug(cadena);
        return cadena;

        //}
    }

    /**
     * Tabla de criterios de control de calidad en correo electronico
     *
     * @author javier.barajas
     */
    public String tablaCriteriosControlCalidad() {
        String cadena = new String();
        cadena = "<table>";
        this.cumpleCriterioControl();
        for (ControlCalidadCriterio ccc : this.listaControlCalidadCriterio) {
            //for(int i=0;i<this.selectManzanas.length;i++){
            cadena += "<tr>";
            cadena += "<td>" + ccc.getDescripcion() + "</td>";
            cadena += "<td>" + ccc.getCumple() + "</td>";
            cadena += "<tr>";
        }
        cadena += "</table>";
        //LOGGER.debug(cadena);
        return cadena;

    }

    /**
     * Verifica y habilita el boton de enviar el correo a topografo
     *
     * @author javier.barajas
     */
    public void apruebaControlCalidad() {
        if (this.resultado.equals("Aprobado")) {
            this.btnHabilitaEnvio = true;
        } else {
            this.btnHabilitaEnvio = false;
        }
    }

    /**
     * Guardar documentos en actualizacion documento
     *
     */
    public void guardarDocumentosActualizacionYAlfrescoDocumento(String ruta) {

        Documento documento = new Documento();
        documento.setArchivo(ruta);
        try {
            documento = this.getActualizacionService().guardarDocumentoDeActualizacionEnAlfresco(
                documento, this.actualizacionSeleccionada, this.usuario);
        } catch (Exception e) {
            LOGGER.error("No se pudo guardar el documento en Alfresco", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo guardar el documento en Alfresco",
                "No se pudo guardar el documento en Alfresco"));
        }
        ActualizacionDocumento docActualizacion = new ActualizacionDocumento();
        docActualizacion.setActualizacion(this.actualizacionSeleccionada);
        docActualizacion.setFechaLog(new Date(System.currentTimeMillis()));
        docActualizacion.setFueGenerado(true);
        docActualizacion.setDocumento(documento);
        docActualizacion.setUsuarioLog(this.usuario.getLogin());
        try {
            docActualizacion = this.getActualizacionService().
                guardarYActualizarActualizacionDocumento(docActualizacion);
        } catch (Exception e) {
            LOGGER.error("No se pudo guardar el documento en Actualizacion Documento", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN,
                "No se pudo guardar el documento en Actualizacion Documento",
                "No se pudo guardar el documento en Actualizacion Documento"));
        }

    }
}
