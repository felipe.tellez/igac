/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu Alistar Información.
 *
 * @author pedro.garcia
 */
/*
 * Es la copia de EjecucionTramitesTerrenoMB cuyo nombre no tenía nada que ver con el cu, por eso se
 * renombró y se reubicó. El autor original era @author fabio.navarrete
 */
@Component("alistamientoInformacion")
@Scope("session")
public class AlistamientoInformacionMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 7452320089822090070L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AlistamientoInformacionMB.class);

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Managed bean para usar métodos generales de web
     */
    @Autowired
    private GeneralMB generalMB;

    private UsuarioDTO usuario;

    private ComisionTramite selectedComisionTramite;

    /**
     * Lista de trámites que se usan para los casos de uso asociados a la ejecución de trámites en
     * terreno
     */
    private List<Tramite> tramitesEjecucion;

    /**
     * Objeto usado para acceder a los trámites seleccionados en la tabla principal de la interfaz
     * gráfica
     */
    private Tramite[] selectedTramites;

    /**
     * Lista de documentos asociados al Tramite seleccionado
     */
    private List<TramiteDocumentacion> documentacionesTramite;

    /**
     * Lista de las documentaciones del trámite seleccionadas.
     */
    private TramiteDocumentacion[] selectedDocumentacionesTramite;

    /**
     * Variable usada para consultas de detalle del trámite.
     */
    private Tramite selectedTramiteDetalle;

    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO documentoMemorandoComision;

    /**
     * Mensaje de finalización de la descarga al SNC
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * Variable usada para traer el documento del trámite y permitir la descarga
     */
    TramiteDocumentacion selectedTramiteDocumentacion;

    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    // -----------------    Banderas de visualización
    /**
     * Bandera que determina la visualización del boton exportar
     */
    private boolean banderaBotonExportar;

    /**
     * indica si el botón "Devolver por comisión modificada" se debe mostrar en la interfaz de
     * usuario
     */
    private boolean mostrarBotonDevolverXComisCancelada;

    /**
     * indica si el botón "Devolver por comisión modificada" se debe mostrar en la interfaz de
     * usuario
     */
    private boolean mostrarBotonAvanzarProceso;

    /**
     * Variable de tipo streamed content que contiene los documentos asociados al trámite
     */
    private StreamedContent documentosAsociadosATramite;

    /**
     * Documento de la ficha predial digital
     */
    private Documento fichaPredialDigital;

    /**
     * ReporteDTO de la ficha predial dígital
     */
    private ReporteDTO reporteFichaPredialDigital;

    /**
     * Documento de la ficha predial digital
     */
    private Documento cartaCatastralUrbana;

    /**
     * ReporteDTO de la carta catastral urbana
     */
    private ReporteDTO reporteCartaCatastralUrbana;

    /**
     * Predio seleccionado en la tabla de la pantalla de documentos e información
     */
    private Predio selectedPredio;

    /**
     * Variable que indica si el usuario desea descargar la carta catastral cuando es un trámite de
     * quinta y la carta existe
     */
    private boolean descargarCartaCatastralQuinta;

    /**
     * Bandera para identificar si los predios del tramite seleccionado son fiscales
     */
    private boolean banderaPredioFiscal;

//--------------------------------------------------------------------------------------------------
    public boolean isMostrarBotonAvanzarProceso() {
        return this.mostrarBotonAvanzarProceso;
    }

    public void setMostrarBotonAvanzarProceso(boolean mostrarBotonAvanzarProceso) {
        this.mostrarBotonAvanzarProceso = mostrarBotonAvanzarProceso;
    }

    public boolean isMostrarBotonDevolverXComisCancelada() {
        return this.mostrarBotonDevolverXComisCancelada;
    }

    public void setMostrarBotonDevolverXComisCancelada(boolean mostrarBotonDevolverXComisCancelada) {
        this.mostrarBotonDevolverXComisCancelada = mostrarBotonDevolverXComisCancelada;
    }

    public List<Tramite> getTramitesEjecucion() {
        return this.tramitesEjecucion;
    }

    public void setTramitesEjecucion(List<Tramite> tramitesEjecucion) {
        this.tramitesEjecucion = tramitesEjecucion;
    }

    public List<TramiteDocumentacion> getDocumentacionesTramite() {
        return documentacionesTramite;
    }

    public void setDocumentacionesTramite(
        List<TramiteDocumentacion> documentacionesTramite) {
        this.documentacionesTramite = documentacionesTramite;
    }

    public Tramite[] getSelectedTramites() {
        return this.selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public boolean isBanderaBotonExportar() {
        return this.banderaBotonExportar;
    }

    public void setBanderaBotonExportar(boolean banderaBotonExportar) {
        this.banderaBotonExportar = banderaBotonExportar;
    }

    public void setSelectedTramiteDetalle(Tramite selectedTramiteDetalle) {
        this.selectedTramiteDetalle = selectedTramiteDetalle;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return this.mensajeSalidaConfirmationDialog;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public ReporteDTO getDocumentoMemorandoComision() {
        return this.documentoMemorandoComision;
    }

    public void setDocumentoMemorandoComision(ReporteDTO documentoMemorandoComision) {
        this.documentoMemorandoComision = documentoMemorandoComision;
    }

    public ReporteDTO getReporteFichaPredialDigital() {
        return this.reporteFichaPredialDigital;
    }

    public void setReporteFichaPredialDigital(ReporteDTO reporteFichaPredialDigital) {
        this.reporteFichaPredialDigital = reporteFichaPredialDigital;
    }

    public ReporteDTO getReporteCartaCatastralUrbana() {
        return this.reporteCartaCatastralUrbana;
    }

    public void setReporteCartaCatastralUrbana(
        ReporteDTO reporteCartaCatastralUrbana) {
        this.reporteCartaCatastralUrbana = reporteCartaCatastralUrbana;
    }

    public Predio getSelectedPredio() {
        return this.selectedPredio;
    }

    public void setSelectedPredio(Predio selectedPredio) {
        this.selectedPredio = selectedPredio;
    }

    /**
     * Método que retorna un trámite de la lista de trámites seleccionados cuando sólo hay uno
     * seleccionado
     *
     * @return
     * @author fabio.navarrete
     */
    public Tramite getSelectedTramite() {
        if (this.getSelectedTramites() != null &&
             this.getSelectedTramites().length == 1) {
            return this.getSelectedTramites()[0];
        }
        return null;
    }

    /**
     * Consulta una lista de TramiteDocumentacion asociada al trámite selccionado
     */
    public void cargarDocumentacionTramite() {
        if (this.getSelectedTramite() != null) {
            this.documentacionesTramite = this.getTramiteService()
                .obtenerTramiteDocumentacionPorIdTramite(this
                    .getSelectedTramite().getId());

            if (this.getSelectedTramite().getPredio().getTipoCatastro().equals(
                EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                this.banderaPredioFiscal = true;
            }

        }
    }

    public TramiteDocumentacion[] getSelectedDocumentacionesTramite() {
        return this.selectedDocumentacionesTramite;
    }

    public void setSelectedDocumentacionesTramite(
        TramiteDocumentacion[] selectedDocumentacionesTramite) {
        this.selectedDocumentacionesTramite = selectedDocumentacionesTramite;
    }

    public Tramite getSelectedTramiteDetalle() {
        return this.selectedTramiteDetalle;
    }

    public boolean isDescargarCartaCatastralQuinta() {
        return descargarCartaCatastralQuinta;
    }

    public void setDescargarCartaCatastralQuinta(
        boolean descargarCartaCatastralQuinta) {
        this.descargarCartaCatastralQuinta = descargarCartaCatastralQuinta;
    }

    public boolean isBanderaPredioFiscal() {
        return banderaPredioFiscal;
    }

    public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
        this.banderaPredioFiscal = banderaPredioFiscal;
    }
    //--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {

        this.idsTramitesActividades =
            this.tareasPendientesMB.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        this.tramitesEjecucion = this.getTramiteService().consultarTramitesParaAlistarInfo(
            this.idsTramitesActividades);

        List<TramiteDocumento> documentosPruebas;
        Long idComision;
        ComisionEstado comisionUltimoEstado;
        for (Tramite tram : this.tramitesEjecucion) {
            documentosPruebas = this.getTramiteService()
                .obtenerTramiteDocumentosDePruebasPorTramiteId(tram.getId());
            if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                tram.setDocumentosPruebas(documentosPruebas);
            }

            //D: obtener el último estado de la comisión asociada al trámite. Se supone que solo
            // se trajo una comisión asociada al trámite porque se excluyeron las finalizadas y solo
            // se tienen en cuenta las de conservación.
            // Se hace esto para poder mostrar o no el link al documento relacionado con el último 
            // estado de la comisión, ya que no todos tienen uno.
            idComision = tram.getComisionTramite().getComision().getId();
            comisionUltimoEstado =
                this.getTramiteService().consultarUltimoCambioEstadoComision(idComision);
            if (comisionUltimoEstado != null) {
                if (tram.getComisionTramite().getComision().getComisionEstados() == null) {
                    tram.getComisionTramite().getComision().
                        setComisionEstados(new ArrayList<ComisionEstado>());
                }
                tram.getComisionTramite().getComision().getComisionEstados().
                    add(0, comisionUltimoEstado);
            }
        }

        this.banderaPredioFiscal = false;

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.mensajeSalidaConfirmationDialog =
            "Los procesos seleccionados se avanzarán y ya no se podrá exportar" +
            " de nuevo la información de los trámites de dichos procesos. Desea confirmar esta acción?";
        this.banderaBotonExportar = false;

    }

//--------------------------------------------------------------------------------------------------    
    /**
     * Método para generar la ficha predial digital
     *
     * @author javier.aponte
     */
    public void generarFichaPredialDigital() {

        try {

            this.fichaPredialDigital = this.getConservacionService().
                buscarDocumentoPorPredioIdAndTipoDocumento(this.selectedPredio.getId(),
                    ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());

            if (this.fichaPredialDigital != null) {

                this.reporteFichaPredialDigital = this.reportsService.
                    consultarReporteDeGestorDocumental(
                        this.fichaPredialDigital.getIdRepositorioDocumentos());

            } else {
                this.addMensajeWarn("No se ha generado la ficha predial digital para este predio.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage(), ex);
        }

    }

    /**
     * Método para generar la carta catastral urbana
     *
     * @author javier.aponte
     */
    public void generarCartaCatastralUrbana() {

        try {

            this.cartaCatastralUrbana = this.getConservacionService().
                buscarDocumentoPorNumeroPredialAndTipoDocumento(this.selectedPredio.
                    getNumeroPredial().substring(0, 17),
                    ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

            if (this.cartaCatastralUrbana != null) {

                this.reporteCartaCatastralUrbana = this.reportsService.
                    consultarReporteDeGestorDocumental(
                        this.cartaCatastralUrbana.getIdRepositorioDocumentos());

            } else {
                this.addMensajeWarn("No se ha generado la carta catastral urbana para este predio.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage(), ex);
        }

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método para generar la carta catastral urbana para predios de quinta
     *
     * @author javier.aponte
     */
    public void generarCartaCatastralUrbanaQuinta() {

        try {

            this.cartaCatastralUrbana = this.getConservacionService().
                buscarDocumentoPorNumeroPredialAndTipoDocumento(this.getSelectedTramite().
                    getNumeroPredial().substring(0, 17),
                    ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

            if (this.cartaCatastralUrbana != null) {

                this.reporteCartaCatastralUrbana = this.reportsService.
                    consultarReporteDeGestorDocumental(
                        this.cartaCatastralUrbana.getIdRepositorioDocumentos());

            } else {
                this.addMensajeWarn("No se ha generado la carta catastral urbana para este predio.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage(), ex);
        }

    }
//--------------------------------------------------------------------------------------------------	

    public void generarMemoComision() {

        Documento documentoMemorandoComisionTemp;
        String idDocEnGestorDocumental;

        documentoMemorandoComisionTemp = this.buscarDocumentoMemorandoComision();
        if (documentoMemorandoComisionTemp == null) {
            this.addMensajeWarn(
                "No se encontró el memorando comisión, por favor verifique que ya se haya generado y radicado");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        idDocEnGestorDocumental = documentoMemorandoComisionTemp.getIdRepositorioDocumentos();

        this.reportsService.consultarReporteDeGestorDocumental(idDocEnGestorDocumental);

    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna un documento del memorando de comisión asociado a la comisión
     *
     * @return TramiteDocumento
     * @author javier.aponte
     */
    private Documento buscarDocumentoMemorandoComision() {

        Documento memorandoComision = null;

        if (this.selectedComisionTramite.getComision().getMemorandoDocumentoId() != null) {
            memorandoComision = this.getGeneralesService().buscarDocumentoPorId(
                this.selectedComisionTramite.getComision().getMemorandoDocumentoId());
        }

        return memorandoComision;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ComisionTramite getSelectedComisionTramite() {
        return selectedComisionTramite;
    }

    public void setSelectedComisionTramite(ComisionTramite selectedComision) {
        this.selectedComisionTramite = selectedComision;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del evento se selección en la tabla de trámites para la actividad
     */
    public void fooSelect(SelectEvent se) {
        visualizacionBontonExportar();
        visualizacionBotonDevolverPorComisionModificada();
        visualizacionBotonAvanzarProceso();
    }

    public void fooUnselect(UnselectEvent se) {
        visualizacionBontonExportar();
        visualizacionBotonDevolverPorComisionModificada();
        visualizacionBotonAvanzarProceso();
    }
//--------------------------------------------------------------------------------------------------

    public void visualizacionBontonExportar() {

        if (this.selectedTramites != null && this.selectedTramites.length > 0) {
            this.banderaBotonExportar = true;
        } else {
            this.banderaBotonExportar = false;
        }
    }
//--------------------------------------------------------------------------------------------------

    public StreamedContent getDocumentoStream() {
        File file;
        URL url;
        try {
            url = new URL(this.selectedTramiteDocumentacion.getDocumentoSoporte().
                getIdRepositorioDocumentos());
        } catch (MalformedURLException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        try {
            InputStream is = new FileInputStream(file);
            String mymeType = new MimetypesFileTypeMap().getContentType(file);
            return new DefaultStreamedContent(is,
                mymeType, file.getName());

        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public TramiteDocumentacion getSelectedTramiteDocumentacion() {
        return selectedTramiteDocumentacion;
    }

    public void setSelectedTramiteDocumentacion(
        TramiteDocumentacion selectedTramiteDocumentacion) {
        this.selectedTramiteDocumentacion = selectedTramiteDocumentacion;
    }

    public StreamedContent getDocumentosAsociadosATramite() {
        this.descargarDocumentos();
        return documentosAsociadosATramite;
    }

    public void setDocumentosAsociadosATramite(
        StreamedContent documentosAsociadosATramite) {
        this.documentosAsociadosATramite = documentosAsociadosATramite;
    }

    public void descargar() {
//TODO :: ?? :: Implementar la descarga a dispositivo movil.

        this.addMensajeInfo("OJO: no se está haciendo nada en este botón");
    }

//------------------------------------------------------------------------------------------
    public String avanzarProcesos() {
        for (Tramite t : this.selectedTramites) {
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            UsuarioDTO usuarioEjecutor;
            if (t.getFuncionarioEjecutor() == null) {
                usuarioEjecutor = this.getGeneralesService().
                    getCacheUsuario(this.usuario.getLogin());
            } else {
                usuarioEjecutor = this.getGeneralesService().getCacheUsuario(t.
                    getFuncionarioEjecutor());
            }
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(usuarioEjecutor);
            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral.setTransicion(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_AL_SNC);
            Actividad actividad = this.tareasPendientesMB.buscarActividadPorIdTramite(t.getId());

            this.getProcesosService().avanzarActividad(usuarioEjecutor, actividad.getId(),
                solicitudCatastral);

            this.tramitesEjecucion.remove(t);
        }
        UtilidadesWeb.removerManagedBean("ejecucionTramitesTerreno");
        this.tareasPendientesMB.init();
        return "index";
    }

//------------------------------------------------------------------------------------------
    public String terminar() {
        return null;
    }
//------------------------------------------------------------------------------------------

    public String cerrar() {
        UtilidadesWeb.removerManagedBean("ejecucionTramitesTerreno");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del link que tiene el número de memorando de la comisión. Baja del gestor documental
     * el archivo correspondiente al memorando del último estado de la comisión.
     *
     * @author pedro.garcia
     */
    public void alistarMemorandoParaVisualizacion() {

        ComisionEstado comisionUltimoEstado;
        String idDocEnGD;

        this.documentoMemorandoComision = null;

        //D: en el init de este MB se definió en la posición 0 de la lista de estados de la comisión
        //  al último estado de la comisión
        comisionUltimoEstado = this.selectedComisionTramite.getComision().getComisionEstados().
            get(0);

        if (comisionUltimoEstado != null) {
            //D: el documento asociado al último estado no debería ser null porque en la interfaz se
            //  validó eso para no pintar el link cuyo action trae a este método
            idDocEnGD = comisionUltimoEstado.getMemorandoDocumento().getIdRepositorioDocumentos();
            this.documentoMemorandoComision =
                this.reportsService.consultarReporteDeGestorDocumental(idDocEnGD);
        }

    }

    /**
     * Método encargado de descargar los documentos del trámite o del predio, seleccionados por el
     * usuario
     *
     * @author javier.aponte
     */
    public void descargarDocumentos() {

        List<String> rutaDocumentosADescargar = new ArrayList<String>();

        ReporteDTO documentoDTO;

        for (TramiteDocumentacion tramDocumentacion : this.selectedDocumentacionesTramite) {

            if (tramDocumentacion.getDocumentoSoporte() != null &&
                 tramDocumentacion.getDocumentoSoporte().getIdRepositorioDocumentos() != null &&
                 !tramDocumentacion.getDocumentoSoporte().getIdRepositorioDocumentos().trim().
                    isEmpty()) {
                documentoDTO = this.reportsService.
                    consultarReporteDeGestorDocumental(tramDocumentacion.getDocumentoSoporte().
                        getIdRepositorioDocumentos());
                if (documentoDTO != null) {
                    rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                }

            }

        }

        for (Predio predioTemp : this.getSelectedTramite().getPredios()) {

            if (predioTemp.isImprimirFichaPredial()) {

                this.fichaPredialDigital = this.getConservacionService().
                    buscarDocumentoPorPredioIdAndTipoDocumento(predioTemp.getId(),
                        ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());

                if (this.fichaPredialDigital != null) {
                    documentoDTO =
                        this.reportsService.consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(
                            this.fichaPredialDigital.getIdRepositorioDocumentos());

                    if (documentoDTO != null) {
                        rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                    }

                }

            }
            if (predioTemp.isImprimirCartaCatastral()) {

                this.cartaCatastralUrbana = this.getConservacionService().
                    buscarDocumentoPorNumeroPredialAndTipoDocumento(predioTemp.getNumeroPredial().
                        substring(0, 17),
                        ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

                if (this.cartaCatastralUrbana != null) {
                    documentoDTO =
                        this.reportsService.consultarReporteDeGestorDocumental(
                            this.cartaCatastralUrbana.getIdRepositorioDocumentos());

                    if (documentoDTO != null) {
                        rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                    }

                }
            }
        }

        if (this.getSelectedTramite().isQuinta() && this.descargarCartaCatastralQuinta) {
            this.cartaCatastralUrbana = this.getConservacionService().
                buscarDocumentoPorNumeroPredialAndTipoDocumento(this.getSelectedTramite().
                    getNumeroPredial().substring(0, 17),
                    ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

            if (this.cartaCatastralUrbana != null) {
                documentoDTO = this.reportsService.consultarReporteDeGestorDocumental(
                    this.cartaCatastralUrbana.getIdRepositorioDocumentos());

                if (documentoDTO != null) {
                    rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                }

            }
        }

        if (!rutaDocumentosADescargar.isEmpty()) {

            String rutaZipDocumentos = this.getGeneralesService().crearZipAPartirDeRutaDeDocumentos(
                rutaDocumentosADescargar);

            if (rutaZipDocumentos != null) {
                InputStream stream;
                File archivoDocumentosAsociadosATramite = new File(rutaZipDocumentos);
                if (!archivoDocumentosAsociadosATramite.exists()) {
                    LOGGER.warn("Error el archivo de origen no existe");
                    this.addMensajeError("Ocurrió un error al descargar los archivos");
                    return;
                }

                try {
                    stream = new FileInputStream(archivoDocumentosAsociadosATramite);
                    this.documentosAsociadosATramite = new DefaultStreamedContent(stream,
                        Constantes.TIPO_MIME_ZIP, "Documentos_tramite.zip");
                } catch (FileNotFoundException e) {
                    LOGGER.error(
                        "Archivo no encontrado, no se puede descargar los documentos asociados al trámite",
                        e);
                }
            }

        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Maneja la activación del botón "Devolver por comisión modificada". </br>
     * Este se debe activar si todos los trámites seleccionados tienen la comisión en estado
     * {@link EComisionEstado#CANCELADA}
     */
    public void visualizacionBotonDevolverPorComisionModificada() {

        if (this.selectedTramites != null && this.selectedTramites.length > 0) {
            this.mostrarBotonDevolverXComisCancelada = true;

            for (Tramite tramite : this.selectedTramites) {
                if (!tramite.getComisionTramite().getComision().getEstado().equals(
                    EComisionEstado.CANCELADA.getCodigo())) {
                    this.mostrarBotonDevolverXComisCancelada = false;
                    break;
                }
            }
        } else {
            this.mostrarBotonDevolverXComisCancelada = false;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Maneja la activación del botón "Avanzar proceso". </br>
     * Este se debe activar si todos los trámites seleccionados tienen la comisión en estado
     * {@link EComisionEstado#EN_EJECUCION}
     */
    public void visualizacionBotonAvanzarProceso() {

        if (this.selectedTramites != null && this.selectedTramites.length > 0) {
            this.mostrarBotonAvanzarProceso = true;

            for (Tramite tramite : this.selectedTramites) {
                if (!tramite.getComisionTramite().getComision().getEstado().equals(
                    EComisionEstado.EN_EJECUCION.getCodigo())) {
                    this.mostrarBotonAvanzarProceso = false;
                    break;
                }
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón para "devolver por comisión modificada".
     *
     * @author pedro.garcia
     */
    public void devolverPorComisionCancelada() {

        boolean error = false;

        SolicitudCatastral solicitudCatastral, scRespuesta;
        List<UsuarioDTO> usuarios;
        String transicion;
        String observaciones =
            "Se devuelve de actividad por retiro del trámite de la comisión, o por " +
            "cancelación de una comisión";

        for (Tramite tramite : this.selectedTramites) {

            solicitudCatastral = new SolicitudCatastral();

            //D: mover el trámite
            transicion = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_CONSERVACION);
            if (usuarios == null || usuarios.isEmpty()) {
                LOGGER.
                    warn("No se encontraron usuarios con el rol " + ERol.RESPONSABLE_CONSERVACION);
                error = true;
                continue;
            }

            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral.setTransicion(transicion);
            solicitudCatastral.setObservaciones(observaciones);

            scRespuesta = this.getProcesosService().avanzarActividadPorIdProceso(
                tramite.getProcesoInstanciaId(), solicitudCatastral);

            if (scRespuesta == null) {
                LOGGER.warn("Error en Alistar información al mover el trámite " + tramite.getId());
                error = true;
            }

            //javier.aponte CC-NO-CO-034 Si la comisión está cancelada entonces elimina las relaciones de comisión trámite
            // y establece el trámite en comisionado NO
            tramite = this.generalMB.
                eliminarRelacionComisionTramite(tramite, this.usuario);

            this.tramitesEjecucion.remove(tramite);
        }
        this.selectedTramites = null;

        if (error) {
            this.addMensajeError(
                "Ocurrió un error moviendo los trámites. Consulte con el administrador del sistema");
        } else {
            this.addMensajeInfo("Los trámites se movieron a la actividad " +
                ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR);
            this.mostrarBotonDevolverXComisCancelada = false;
        }

    }

//end of class    
}
