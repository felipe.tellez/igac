package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoAdicion;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB para el caso de uso CU-SA-AC-084 Consultar Adiciones del Contrato Activo del Avaluador Externo
 *
 * @cu CU-SA-AC-84
 *
 * @author felipe.cadena
 *
 */
@Component("consultaAdicionesContratoActivo")
@Scope("session")
public class ConsultaAdicionesContratoActivoMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -2628425907644754748L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaAdicionesContratoActivoMB.class);
    /**
     * Información acerca del avaluador
     */
    private VProfesionalAvaluosContrato avaluador;
    /**
     * Contrato activo del avaluador
     */
    private ProfesionalAvaluosContrato contrato;
    /**
     * Valor total de las adiciones del cotrato
     */
    private Double valorTotalAdiciones;
    /**
     * Valor total de las adiciones del cotrato
     */
    private Date fechaFinalConAdiciones;
    /**
     * Lista de adiciones relacionadas al contrato
     */
    private List<ProfesionalContratoAdicion> adicionesContrato;

    // --------getters-setters----------------
    public VProfesionalAvaluosContrato getAvaluador() {
        return this.avaluador;
    }

    public void setAvaluador(VProfesionalAvaluosContrato avaluador) {
        this.avaluador = avaluador;
    }

    public Date getFechaFinalConAdiciones() {
        return fechaFinalConAdiciones;
    }

    public void setFechaFinalConAdiciones(Date fechaFinalConAdiciones) {
        this.fechaFinalConAdiciones = fechaFinalConAdiciones;
    }

    public Double getValorTotalAdiciones() {
        return valorTotalAdiciones;
    }

    public void setValorTotalAdiciones(Double valorTotalAdiciones) {
        this.valorTotalAdiciones = valorTotalAdiciones;
    }

    public ProfesionalAvaluosContrato getContrato() {
        return this.contrato;
    }

    public void setContrato(ProfesionalAvaluosContrato contrato) {
        this.contrato = contrato;
    }

    public List<ProfesionalContratoAdicion> getAdicionesContrato() {
        return this.adicionesContrato;
    }

    public void setAdicionesContrato(List<ProfesionalContratoAdicion> adicionesContrato) {
        this.adicionesContrato = adicionesContrato;
    }

    /**
     * Método para hacer el llamado de este caso de uso desde el CU-SA-AC-051
     */
    public void cargarDesdeCU051(VProfesionalAvaluosContrato avaluador) {
        LOGGER.debug("Cargando datos desde caso de uso externo...");

        FiltroDatosConsultaProfesionalAvaluos fa = new FiltroDatosConsultaProfesionalAvaluos();
        fa.setNumeroIdentificacion(avaluador.getNumeroIdentificacion());
        List<VProfesionalAvaluosContrato> la = this.getAvaluosService().
            buscarAvaluadoresPorFiltro(fa);

        if (!la.isEmpty()) {
            this.avaluador = la.get(0);
        }
        this.contrato = this.getAvaluosService().
            obtenerContratoAvaluadorPorId(this.avaluador.getContratoId());

        this.buscarAdiciones();
        this.calcularFechaFinalConAdiciones();

    }

    /**
     * Método para bucar las adiciones del contrato activo del avaluador
     *
     * @author felipe.cadena
     */
    public void buscarAdiciones() {

        this.adicionesContrato = this.getAvaluosService().
            buscarAdicionesPorIdProfesionalContrato(this.contrato.getId());
        this.valorTotalAdiciones = this.getAvaluosService().
            obtenerTotalAdicionesPorIdProfesionalContrato(this.contrato.getId());

    }

    /**
     * Método para calcular la fecha fina del contrato incluyendo las adiciones.
     *
     * @author felipe.cadena
     *
     */
    public void calcularFechaFinalConAdiciones() {
        LOGGER.debug("Calculando fecha fin ...");

        if (!this.adicionesContrato.isEmpty()) {
            Date faux = null;
            faux = this.adicionesContrato.get(0).getFechaHasta();
            for (int i = 1; i < this.adicionesContrato.size(); i++) {
                Date fAdicion = this.adicionesContrato.get(i).getFechaHasta();
                if (fAdicion.after(faux)) {
                    faux = fAdicion;
                }
            }

            if (faux.after(contrato.getFechaHasta())) {
                this.fechaFinalConAdiciones = faux;
            } else {
                this.fechaFinalConAdiciones = contrato.getFechaHasta();
            }
        }

        LOGGER.debug("Fecha calculada");

    }
}
