/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 *
 * @author juanfelipe.garcia
 */
public class HistoricoProyeccionViaGubernativaDTO implements java.io.Serializable {

    private static long serialVersionUID = 3956227729553241949L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    // Fields
    private String tipoProyeccion;
    private Tramite tramiteInfo;

    // Constructors
    /** default constructor */
    public HistoricoProyeccionViaGubernativaDTO() {
    }

    public String getTipoProyeccion() {
        return tipoProyeccion;
    }

    public void setTipoProyeccion(String tipoProyeccion) {
        this.tipoProyeccion = tipoProyeccion;
    }

    public Tramite getTramiteInfo() {
        return tramiteInfo;
    }

    public void setTramiteInfo(Tramite tramiteInfo) {
        this.tramiteInfo = tramiteInfo;
    }

}
