/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.Consignacion;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 *
 * Managed bean del cu Consultar Avalúos Aprobados Asociados al Contrato o Consignación
 *
 * @author pedro.garcia
 * @cu CU-SA-AC-087
 */
@Component("consultaAvaluosAprobados")
@Scope("session")
public class ConsultaAvaluosAprobadosMB extends SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaAvaluosAprobadosMB.class);

    /**
     * variables con las que se diferencia el cu desde donde éste es invocado
     */
    private final int CU_SA_AC_008 = 1;
    private final int CU_SA_AC_086 = 2;
    private int extendedUC;

    /**
     * objeto que se recibe como parámetro para mostrar los avalúos asociados a ese contrato cuando
     * se llega del cu-sa-ac-008
     */
    private ContratoInteradministrativo currentContract;

    /**
     * objeto que se recibe como parámetro para mostrar los avalúos asociados al contrato al que se
     * le hizo esta Consignacion, cuando se llega del cu-sa-ac-086
     */
    private Consignacion currentConsignacion;

    /**
     * lista de los avalúos en aprobados asociados al contrato o consignación actual
     */
    private ArrayList<Avaluo> avaluosAprobadosAsociados;

    private int numeroAvaluosAprobados;

    /**
     * avalúo del que se van a visualizar los predios y que se selecciona indirectamente al dar
     * click en el botón de 'información predial' de la tabla
     */
    private Avaluo selectedAvaluoForInfoPredios;

    /**
     * contiene el valor ejecutado del contrato seleccionado
     */
    private Double valorEjecutado;

//-----------------    getters  y setters   -----------------
    public Avaluo getSelectedAvaluoForInfoPredios() {
        return this.selectedAvaluoForInfoPredios;
    }

    public void setSelectedAvaluoForInfoPredios(Avaluo selectedAvaluoForInfoPredios) {
        this.selectedAvaluoForInfoPredios = selectedAvaluoForInfoPredios;
    }

    public int getNumeroAvaluosAprobados() {
        return this.numeroAvaluosAprobados;
    }

    public void setNumeroAvaluosAprobados(int numeroAvaluosAprobados) {
        this.numeroAvaluosAprobados = numeroAvaluosAprobados;
    }
//---------------------------------------------------------------------------------------------

    public Consignacion getCurrentConsignacion() {
        return this.currentConsignacion;
    }

    public void setCurrentConsignacion(Consignacion currentConsignacion) {

        if (this.currentConsignacion == null ||
            (this.currentConsignacion != null && this.currentConsignacion.getId() !=
            currentConsignacion.getId())) {
            this.currentConsignacion = currentConsignacion;

            this.avaluosAprobadosAsociados = consultarAvaluosAprobadosPorConsignacion();
            this.numeroAvaluosAprobados = this.avaluosAprobadosAsociados.size();
        }

        this.currentConsignacion = currentConsignacion;
    }
//---------------------------------------------------------------------------------------------

    public int getCU_SA_AC_008() {
        return this.CU_SA_AC_008;
    }

    public int getCU_SA_AC_086() {
        return this.CU_SA_AC_086;
    }

    public int getExtendedUC() {
        return this.extendedUC;
    }

    public void setExtendedUC(int extendedUC) {
        this.extendedUC = extendedUC;
    }

    public ArrayList<Avaluo> getAvaluosAprobadosAsociados() {
        return this.avaluosAprobadosAsociados;
    }

    public void setAvaluosAprobadosAsociados(ArrayList<Avaluo> avaluosAprobadosAsociados) {
        this.avaluosAprobadosAsociados = avaluosAprobadosAsociados;
    }

    public ContratoInteradministrativo getCurrentContract() {
        return this.currentContract;
    }

    public void setCurrentContract(ContratoInteradministrativo currentContract) {
        this.currentContract = currentContract;
    }

    public Double getValorEjecutado() {
        return this.valorEjecutado;
    }

    //-------------------------     methods   ------------------------------
//--------------------------------------------------------------------------------------------------
    /**
     * Hace la consulta e los avalúos aprobados pertenecientes a un contrato
     */
    private ArrayList<Avaluo> consultarAvaluosAprobadosPorContrato() {

        ArrayList<Avaluo> answer = null;
        FiltroDatosConsultaAvaluo filtro;

        filtro = new FiltroDatosConsultaAvaluo();

        filtro.setAprobado(ESiNo.SI.getCodigo());
        answer = (ArrayList<Avaluo>) this.getAvaluosService().
            consultarAvaluosDeContrato(this.currentContract.getId(), filtro);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace la consulta e los avalúos aprobados relacionados a una consignación
     */
    private ArrayList<Avaluo> consultarAvaluosAprobadosPorConsignacion() {

        ArrayList<Avaluo> answer = null;
        FiltroDatosConsultaAvaluo filtro;

        filtro = new FiltroDatosConsultaAvaluo();

        filtro.setAprobado(ESiNo.SI.getCodigo());
        answer = (ArrayList<Avaluo>) this.getAvaluosService().consultarAvaluosDeContrato(
            this.currentConsignacion.getContratoInteradministrativo().getId(), filtro);

        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Carga los datos necesarios cuando este mb se llama desdel el que maneja el cu 008
     *
     * @param contratoSeleccionado
     */
    public void cargarDesdeCU008(ContratoInteradministrativo currentContract) {

        this.extendedUC = this.CU_SA_AC_008;

        if (currentContract != null && currentContract.getId() != null) {
            this.currentContract = currentContract;

            this.avaluosAprobadosAsociados = consultarAvaluosAprobadosPorContrato();
            this.numeroAvaluosAprobados = this.avaluosAprobadosAsociados.size();
            this.calcularValorEjecutado();
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Carga los datos necesarios cuando este mb se llama desdel el que maneja el cu 086
     *
     * @param contratoSeleccionado
     */
    public void cargarDesdeCU086(Consignacion consignacion) {

        this.extendedUC = this.CU_SA_AC_086;

        if (this.currentConsignacion == null ||
            (this.currentConsignacion != null &&
            this.currentConsignacion.getId() != currentConsignacion.getId())) {
            this.currentConsignacion = consignacion;

            this.avaluosAprobadosAsociados = consultarAvaluosAprobadosPorConsignacion();
            this.numeroAvaluosAprobados = this.avaluosAprobadosAsociados.size();
            this.calcularValorEjecutado();
        }

    }

    // -------------------------------------------------------------------
    /**
     * Método que calcula el valor ejecutado del contrato seleccionado
     *
     * @author christian.rodriguez
     */
    private void calcularValorEjecutado() {
        this.valorEjecutado = this.getAvaluosService()
            .calcularValorEjecutadoPAContrato(this.currentContract.getId());
    }

//end of class
}
