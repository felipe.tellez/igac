/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.util;

import java.util.Comparator;
import javax.faces.model.SelectItem;

/**
 *
 * @author felipe.cadena
 */
public class SelectItemComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        SelectItem s1 = (SelectItem) o1;
        SelectItem s2 = (SelectItem) o2;

        return s1.getLabel().compareTo(s2.getLabel());
    }

}
