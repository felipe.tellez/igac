/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util.converters;

import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.util.Constantes;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter para los objetos CalificacionAnexo para poder ser usados como valor en los items de los
 * combos
 *
 * @author pedro.garcia
 */
@FacesConverter(value = "calificacionAnexoConverter",
    forClass = co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo.class)
public class CalificacionAnexoConverter implements Converter {

//--------------------------------------------------------------------------------------------------
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        CalificacionAnexo answer;
        UsoConstruccion usoConstruccionTemp;

        answer = new CalificacionAnexo();
        usoConstruccionTemp = new UsoConstruccion();
        String[] objectAttributes;
        objectAttributes = value.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setId(Long.parseLong(objectAttributes[0]));
        usoConstruccionTemp.setId(Long.parseLong(objectAttributes[1]));
        answer.setUsoConstruccion(usoConstruccionTemp);
        answer.setDescripcion(objectAttributes[2]);
        answer.setPuntos(Short.parseShort(objectAttributes[3]));

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object o) {
        return ((CalificacionAnexo) o).toString();
    }

}
