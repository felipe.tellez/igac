package co.gov.igac.snc.web.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.entity.conservacion.RepControlReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Tarea que se encarga de ejecutar un reporte de tipo RepControlReporte
 *
 * @author Juan Carlos
 *
 */
public class EjecutarReporteJobTask extends AbstractLocator implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = -7332773578521079455L;
    public static Logger LOGGER = LoggerFactory.getLogger(EjecutarReporteJobTask.class);

    private RepReporteEjecucion job;

    /**
     *
     * @param job a ejecutar
     */
    public EjecutarReporteJobTask(RepReporteEjecucion job) {
        this.job = job;
    }

    /**
     * Método encargado de enviar la ejecución del job pendiente
     */
    public void run() {
        //TODO EJECUTAR EL MÉTODO QUE COMPLETA LA EJECUCIÓN DEL REPORTE ejm: lo envia jasperserver, cuando termina actualiza la bd y envía email...
        //this.getGeneralesService().reenviarJobGeograficoEsperando(this.job.getId());
        this.getGeneralesService().reenviarJobReportesTerminado(job.getId());
    }

}
