package co.gov.igac.snc.web.controller.conservacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.util.ELogAccionEvento;
import co.gov.igac.snc.vo.ErrorVO;
import co.gov.igac.snc.vo.ParametroEditorVO;
import co.gov.igac.snc.vo.ResultadoVO;
import co.gov.igac.snc.web.controller.BaseController;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Servicio Rest para la consulta de datos del sistema SNC
 *
 */
@Controller
@RequestMapping(value = "/general/**")
public class GeneralController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = -5621225470092732149L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneralController.class);

    /**
     * Obtiene la lista de tareas del usuario autenticado en el sistema
     *
     * @param modelMap
     * @return
     *
     * @modified felipe.cadena - 11-10-2013 - Se adiciona el registro de las consultas del servicio
     * rest
     */
    @Deprecated
    @RequestMapping(value = "/tareas/geograficas/**")
    public void listarTareasGeograficas(ModelMap modelMap) {
        //Se registra el acceso al servicio 
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());
        try {
            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
            ResultadoVO vo = this.getTramiteService().obtenerTareasGeograficas(usuario);
            modelMap.addAttribute("resultado", vo);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error en la generacion de la lista de tareas ");
            modelMap.addAttribute("Error", error);
        }
    }

    /**
     * Obtiene la lista de tareas del usuario autenticado en el sistema
     *
     * @param modelMap
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/tareas/geograficas2/**")
    public void listarTareasGeograficas2(ModelMap modelMap) {

        //Se registra el acceso al servicio 
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());

        try {
            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
            ResultadoVO vo = this.getTramiteService().obtenerTareasGeograficas2(usuario);
            modelMap.addAttribute("resultado", vo);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error en la generacion de la lista de tareas ");
            modelMap.addAttribute("Error", error);
        }
    }

    /**
     * Obtiene la lista de tareas del usuario autenticado en el sistema
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/tareas/geograficas3/**")
    public void listarTareasGeograficas3(ModelMap modelMap) {

        //Se registra el acceso al servicio 
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());

        try {
            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
            ResultadoVO vo = this.getTramiteService().obtenerTareasGeograficasPaginado(usuario);
            modelMap.addAttribute("resultado", vo);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error en la generacion de la lista de tareas ");
            modelMap.addAttribute("Error", error);
            LOGGER.error(error.getMensajeError(), e);
        }
    }

    /**
     * Registra el acceso al servicio y la acción dependiendo el metodo invocado
     *
     * @param nombreMetodo Nombre del metodo desde que se invoco.
     */
    public void registrarAccion(String nombreMetodo) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.
            currentRequestAttributes();
        HttpServletRequest hsr = attr.getRequest();
        LogAcceso la = UtilidadesWeb.obtenerInfoSesionDesktop(hsr);
        Long accesoId = this.getGeneralesService().registrarAcceso(la);
        LogAccion accion = new LogAccion(accesoId,
            ELogAccionEvento.ACCION.toString(),
            "Servicio rest",
            nombreMetodo,
            hsr.getRequestURI());
        this.getGeneralesService().registrarlogAccion(accion);

    }

    /**
     * Servicio parametros editor
     *
     * @author andres.eslava
     */
    @RequestMapping(value = "/tareas/obtenerParametros/**")
    public void obtenerParametrosEditor(ModelMap modelMap) {
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());
        try {
            List<ParametroEditorVO> listaParametros = this.getGeneralesService().
                obtenerParametrosEditor();
            for (ParametroEditorVO p : listaParametros) {
                modelMap.addAttribute("parametro", p);
            }
        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error en la obtencion del parametro");
            modelMap.addAttribute("Error", error);
            LOGGER.error(error.getMensajeError(), e);
        }
    }
}
