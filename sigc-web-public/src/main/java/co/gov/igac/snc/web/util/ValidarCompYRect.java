/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EComponenteConstruccionCom;
import co.gov.igac.snc.persistence.util.EErroresDatosRectificar;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPredioTipo;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipoCalificacion;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.Constantes;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.LoggerFactory;

/**
 * Clase para realizar las validaciones cuando complementaciones y rectificaciones
 *
 * @author felipe.cadena
 */
public class ValidarCompYRect extends SNCManagedBean {

    private enum ECampoRC implements Serializable {

        PERSONA_TIPO_IDENTIFICACION(1),
        PERSONA_NUMERO_IDENTIFICACION(2),
        PERSONA_PRIMER_APELLIDO(3),
        PERSONA_SEGUNDO_APELLIDO(4),
        PERSONA_PRIMER_NOMBRE(5),
        PERSONA_SEGUNDO_NOMBRE(6),
        PERSONA_RAZON_SOCIAL(7),
        PERSONA_SIGLA(8),
        //FICHA_MATRIZ_TOTAL_UNIDADES_PRIVADAS(9),
        //FICHA_MATRIZ_TORRE_PISOS(10),
        //UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION(11),
        FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA(12),
        FICHA_MATRIZ_AREA_TOTAL_TERRENO_COMUN(13),
        //FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_PRIVADA(14),
        UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA(14),
        FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_COMUN(15),
        //PREDIO_AREA_TERRENO(16),
        //PREDIO_AREA_CONSTRUCCION(17),
        PERSONA_PREDIO_PARTICIPACION(18),
        PERSONA_PREDIO_TIPO(19),
        PREDIO_NUMERO_REGISTRO(20),
        PREDIO_TIPO(21),
        //PREDIO_ZONA_ZONA_FISICA(22),
        //PREDIO_ZONA_ZONA_GEOECONOMICA(23),
        UNIDAD_CONSTRUCCION_USO_ID(24),
        UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION_COMP(25),
        UNIDAD_CONSTRUCCION_PISO_UBICACION(26),
        UNIDAD_CONSTRUCCION_TOTAL_PISOS_UNIDAD(27),
        UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES_COMP(28),
        UNIDAD_CONSTRUCCION_TOTAL_BANIOS_COMP(29),
        UNIDAD_CONSTRUCCION_TOTAL_LOCALES_COMP(30),
        UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION_COMP(31),
        UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION(32),
        PREDIO_DESTINO(33),
        PREDIO_DIRECCION_DIRECCION(34),
        FICHA_MATRIZ_PREDIO_COEFICIENTE(35),
        PREDIO_DIRECCION_PRINCIPAL(36),
        FOTO_COMP(37),
        PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION(38),
        PERSONA_PREDIO_PROPIEDAD_VALOR(39),
        PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO(40),
        //UNIDAD_CONSTRUCCION_AREA_UNIDAD_CONSTRUIDA(41),
        PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA(41),
        PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO(42),
        PERSONA_PREDIO_PROPIEDAD_MUNICIPIO(43),
        PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO(44),
        PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO(45),
        PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO(46),
        UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION(47),
        PREDIO_ZONA_ZONA_FISICA(48),
        PREDIO_ZONA_ZONA_GEOECONOMICA(49),
        UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION(50),
        UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES(51),
        UNIDAD_CONSTRUCCION_TOTAL_BANIOS(52),
        UNIDAD_CONSTRUCCION_TOTAL_LOCALES(53),
        //UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA(54),             
        UNIDAD_CONSTRUCCION_DIMENSION(54),
        FOTO(55),
        PERSONA_TIPO_IDENTIFICACION_COMP(56),
        PERSONA_NUMERO_IDENTIFICACION_COMP(57),
        PERSONA_PRIMER_APELLIDO_COMP(58),
        PERSONA_SEGUNDO_APELLIDO_COMP(59),
        PERSONA_PRIMER_NOMBRE_COMP(60),
        PERSONA_SEGUNDO_NOMBRE_COMP(61),
        PERSONA_RAZON_SOCIAL_COMP(62),
        PERSONA_PREDIO_PARTICIPACION_COMP(63),
        PERSONA_SIGLA_COMP(64),
        PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION_COMP(65),
        PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO_COMP(66),
        PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO_COMP(67),
        PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA_COMP(68),
        PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO_COMP(69),
        PERSONA_PREDIO_PROPIEDAD_MUNICIPIO_COMP(70),
        PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO_COMP(71),
        PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO_COMP(72),
        PREDIO_NUMERO_REGISTRO_COMP(73),
        PREDIO_TIPO_COMP(74),
        UNIDAD_CONSTRUCCION_USO_ID_COMP(75),
        PREDIO_DESTINO_COMP(76),
        PREDIO_DIRECCION_DIRECCION_COMP(77),
        FICHA_MATRIZ_PREDIO_COEFICIENTE_COMP(78),
        PREDIO_DIRECCION_PRINCIPAL_COMP(79),
        PERSONA_PREDIO_PROPIEDAD_VALOR_COMP(80),
        UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION_COMP(81);
        private Integer id;

        ECampoRC(Integer id) {
            this.id = id;
        }

        public Integer getId() {
            return id;
        }
    }
    private static final org.slf4j.Logger LOGGER = LoggerFactory
        .getLogger(ValidarCompYRect.class);

    private PPredio predio;
    private String mensajeError;
    private List<TramiteRectificacion> rectificaciones;

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    /**
     * Método para validar las complementaciones y rectificaciones realizadas sobre un predio.
     *
     * @author felipe.cadena
     * @param predio -Predio con los datos actualizados para realizar las validaciones.
     * @return
     */
    public PPredio validar(PPredio predio, Long idTramite) {

        this.rectificaciones = this.getTramiteService().obtenerTramiteRectificacionesDeTramite(
            idTramite);

        this.predio = predio;

        Method m;
        Boolean resultado;

        for (TramiteRectificacion dato : rectificaciones) {
            try {
                m = this.getClass().getDeclaredMethod("validar_" + dato.getDatoRectificar().getId());
                resultado = (Boolean) m.invoke(this);

                if (!resultado) {
                    return null;
                }
            } catch (Exception ex) {
                LOGGER.debug(dato.getDatoRectificar().getNombre() +
                    " >>Dato sin acciones requeridas");
            }
        }

        return predio;
    }

    //PERSONA_TIPO_IDENTIFICACION
    public boolean validar_1() {

        PPersona per;
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {

            per = pp.getPPersona();

            if (per.getTipoIdentificacion().equals(EPersonaTipoIdentificacion.NIT.getCodigo()) &&
                 (per.getRazonSocial() == null || per.getRazonSocial().isEmpty())) {

                String name = "";
                if (per.getPrimerNombre() != null) {
                    name = name.concat(" ").concat(per.getPrimerNombre());
                    per.setPrimerNombre("");
                }
                if (per.getSegundoNombre() != null) {
                    name = name.concat(" ").concat(per.getSegundoNombre());
                    per.setSegundoNombre("");
                }
                if (per.getPrimerApellido() != null) {
                    name = name.concat(" ").concat(per.getPrimerApellido());
                    per.setPrimerApellido("");
                }
                if (per.getSegundoApellido() != null) {
                    name = name.concat(" ").concat(per.getSegundoApellido());
                    per.setSegundoApellido("");
                }
                per.setRazonSocial(name);
            }

            if (!per.getTipoIdentificacion().equals(EPersonaTipoIdentificacion.NIT.getCodigo()) &&
                 (per.getRazonSocial() != null || !per.getRazonSocial().isEmpty())) {

                per.setPrimerApellido(per.getRazonSocial());
                per.setRazonSocial("");
                per.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
            } else {
                per.setPrimerApellido("");
                per.setPrimerNombre("");
                per.setSegundoApellido("");
                per.setSegundoNombre("");
                per.setRazonSocial(per.getNombre());
                per.setTipoPersona(EPersonaTipoPersona.JURIDICA.getCodigo());
            }
        }

        return true;
    }

    //PERSONA_NUMERO_IDENTIFICACION
    public boolean validar_2() {

        PPersona per;
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            per = pp.getPPersona();

            if (per.getNumeroIdentificacion().equals(Constantes.NIT_LA_NACION) &&
                 per.getTipoIdentificacion().equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                per.setRazonSocial(Constantes.RAZON_SOCIAL_LA_NACION);
                continue;
            }
            if (!per.getTipoIdentificacion().equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {

//felipe.cadena :: Se comenta para que no realizar esta validacion segun incidencia #6909               
//                if (per.getNumeroIdentificacion().equals("0")
//                        || per.getNumeroIdentificacion().length() < 2) {
//                    this.mensajeError = EErroresDatosRectificar.CEDULA_INVALIDA.getMensaje();
//                    return false;
//                }
            } else {
                if (!this.validarNit(per.getNumeroIdentificacion(), per.getDigitoVerificacion())) {
                    this.mensajeError = EErroresDatosRectificar.NIT_INVALIDO.getMensaje();
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validar_18() {
        Double total = 0d;
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            if (pp.getParticipacion() == null || pp.getParticipacion() == 0d) {
                this.mensajeError = EErroresDatosRectificar.PORCENTAJE_PARTICIPACION.getMensaje();
                return false;
            }
            total += pp.getParticipacion();
        }

        if (!total.equals(100d)) {
            this.mensajeError = EErroresDatosRectificar.TOTAL_PARTICIPACION.getMensaje();
            return false;
        }
        return true;
    }

    public boolean validar_39() {
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {
                if (ppp.getValor() == null || ppp.getValor() == 0d) {
                    this.mensajeError = EErroresDatosRectificar.VALOR_COMPRA_CERO.getMensaje();
                    return false;
                }
            }

        }
        return true;
    }

    public boolean validar_44() {
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {
                if (ppp.getNumeroTitulo() == null || ppp.getNumeroTitulo().equals("0")) {
                    this.mensajeError = EErroresDatosRectificar.NUMERO_TITULO_CERO.getMensaje();
                    return false;
                }
            }

        }
        return true;
    }

    public boolean validar_45() {
        Date fechaSys = new Date();
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {
                if (ppp.getFechaTitulo() == null || ppp.getFechaTitulo().after(fechaSys)) {
                    this.mensajeError = EErroresDatosRectificar.FECHA_TITULO_POSTERIOR.getMensaje();
                    return false;
                }
                if (!this.getGeneralesService().esDiaHabil(ppp.getFechaTitulo())) {
                    this.mensajeError = EErroresDatosRectificar.FECHA_TITULO_NO_HABIL.getMensaje();
                    return false;
                }
            }

        }
        return true;
    }

    public boolean validar_46() {
        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {
                if (!this.getGeneralesService().esDiaHabil(ppp.getFechaRegistro())) {
                    this.mensajeError = EErroresDatosRectificar.FECHA_REGISTRO_NO_HABIL.getMensaje();
                    return false;
                }
            }

        }
        return true;
    }

    public boolean validar_41() {

        for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
            for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {
                if (ppp.getFechaTitulo() == null ||
                     ppp.getEntidadEmisora().length() < 3 ||
                     this.buscarCaracteresIlegales(ppp.getEntidadEmisora())) {
                    this.mensajeError = EErroresDatosRectificar.ENTIDAD_EMISORA_INVALIDA.
                        getMensaje();
                    return false;
                }
            }

        }
        return true;
    }

    public boolean validar_21() {

        if (this.predio.getTipo().equals(EPredioTipo.VACANTE.getCodigo())) {
            this.mensajeError = EErroresDatosRectificar.TIPO_PREDIO_INVALIDO.getMensaje();
            return false;
        }
        return true;
    }

    public boolean validar_20() {

        if (this.predio.getNumeroRegistro() == null ||
             this.predio.getNumeroRegistro().isEmpty()) {
            this.mensajeError = EErroresDatosRectificar.NUMERO_REGISTRO_INVALIDO.getMensaje();
            return false;
        }
        return true;
    }

    public boolean validar_34() {

        if (this.predio.isPredioRural()) {
            for (PPredioDireccion dir : this.predio.getPPredioDireccions()) {
                if (dir.getDireccion().length() < 5
                        && !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(
                        dir.getCancelaInscribe())) {
                    this.mensajeError = EErroresDatosRectificar.DIRECCION_INVALIDA.getMensaje();
                    return false;
                }
            }
        }
        return true;
    }

    //Validacion baños y cocinas rectificacion
    //felipe.cadena::14/11/2014::#10365::Control de cambios CC-NP-CO-070 Se cambia el nombre del metodo lo cual deshabilita la validacion
    public boolean validar_32_desactivado() {

        for (PUnidadConstruccion uc : this.predio.getPUnidadConstruccions()) {
            if (uc.getTipoCalificacion().equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.
                getCodigo()) ||
                 uc.getTipoCalificacion().equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.
                    getCodigo())) {
                for (PUnidadConstruccionComp ucc : uc.getPUnidadConstruccionComps()) {
                    if (ucc.getComponente().equals(EComponenteConstruccionCom.BANIO.getCodigo())) {
                        if (ucc.getElementoCalificacion().equals("MOBILIARIO")) {
                            if (!ucc.getDetalleCalificacion().equals("POBRE")) {
                                this.mensajeError = EErroresDatosRectificar.BANIO_INVALIDO.
                                    getMensaje();
                                return false;
                            }
                        }
                    }
                    if (ucc.getComponente().equals(EComponenteConstruccionCom.COCINA.getCodigo())) {
                        if (ucc.getElementoCalificacion().equals("MOBILIARIO")) {
                            if (!ucc.getDetalleCalificacion().equals("POBRE")) {
                                this.mensajeError = EErroresDatosRectificar.COCINA_INVALIDO.
                                    getMensaje();
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    public boolean validar_25() {
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getTotalPisosConstruccion() < 1) {
                this.mensajeError = EErroresDatosRectificar.TOTAL_PISOS_CERO.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_28() {
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getTotalHabitaciones() < 1) {
                this.mensajeError = EErroresDatosRectificar.TOTAL_HABITACIONES_CERO.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_29() {
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getTotalBanios() < 1) {
                this.mensajeError = EErroresDatosRectificar.TOTAL_BANIOS_CERO.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_30() {
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getTotalLocales() < 1) {
                this.mensajeError = EErroresDatosRectificar.TOTAL_LOCALES_CERO.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_14() {
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getAreaConstruida() < 1) {
                this.mensajeError = EErroresDatosRectificar.TOTAL_AREA_CONSTRUIDA_CERO.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_53() {
        return this.validar_30();
    }

    public boolean validar_52() {
        return this.validar_29();
    }

    public boolean validar_31() {
        return this.validar_47();
    }

    public boolean validar_54() {
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getAreaConstruida() < 1) {
                this.mensajeError = EErroresDatosRectificar.DIMENSION_CERO.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_47() {
        Calendar c = Calendar.getInstance();
        for (PUnidadConstruccion uc : this.predio.getPUnidadesConstruccionConvencional()) {
            if (uc.getAnioConstruccion() > c.get(Calendar.YEAR)) {
                this.mensajeError = EErroresDatosRectificar.ANIO_POSTERIOR.getMensaje();
                return false;
            }
        }
        return true;
    }

    public boolean validar_56() {
        return this.validar_1();
    }

    public boolean validar_57() {
        return this.validar_2();
    }

    public boolean validar_63() {
        return this.validar_18();
    }

    public boolean validar_80() {
        return this.validar_39();
    }

    public boolean validar_66() {
        return this.validar_44();
    }

    public boolean validar_68() {
        return this.validar_41();
    }

    public boolean validar_71() {
        return this.validar_45();
    }

    public boolean validar_74() {
        return this.validar_21();
    }

    public boolean validar_73() {
        return this.validar_20();
    }

    //Validacion baños y cocinas complementacion
    //felipe.cadena::14/11/2014::#10365::Control de cambios CC-NP-CO-070 Se cambia el nombre del metodo lo cual deshabilita la validacion
    public boolean validar_81_desactivado() {
        return this.validar_32_desactivado();
    }

    public boolean validar_50() {
        return this.validar_25();
    }

    public boolean validar_51() {
        return this.validar_28();
    }

    public boolean validar_72() {
        return this.validar_46();
    }

    public boolean validar_12() {
        return true;
    }

    /**
     * Método para calcular el digito de verificación de un NIT
     *
     * @param nit
     * @param digitoVerificacion
     * @return
     */
    public boolean validarNit(String nit, String digitoVerificacion) {

        int[] primos = Constantes.NUMEROS_DIGITO_VERIFICACION;
        int[] digitos = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int total = 0;
        int digitoCalculado = 0;
        int digitoIngresado = Integer.valueOf(digitoVerificacion);

        StringBuilder nitSB = new StringBuilder(nit);
        nitSB = nitSB.reverse();

        for (int i = 0; i < nitSB.length(); i++) {
            digitos[i] = Integer.valueOf(nitSB.charAt(i) - 48);
        }

        for (int i = 0; i < primos.length; i++) {
            total += primos[i] * digitos[i];
        }

        total = total % 11;

        if (total <= 1) {
            digitoCalculado = total;
        } else {
            digitoCalculado = 11 - total;
        }

        if (digitoCalculado == digitoIngresado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método para determinar si una cadena tiene caracteres ilegales
     *
     * @author felipe.cadena
     * @param campo
     * @return true - Si la cadena tiene caracteres ilegales false - Si la cadena solo posee letras
     * y
     */
    public boolean buscarCaracteresIlegales(String campo) {
        Pattern p = Pattern.compile("[^A-Za-z0-9.-áéíóúñ]+");
        Matcher m = p.matcher(campo);
        StringBuffer sb = new StringBuffer();
        boolean resultado = m.find();
        boolean caracteresIlegales = false;

        while (resultado) {
            caracteresIlegales = true;
            m.appendReplacement(sb, "");
            resultado = m.find();
        }

        return caracteresIlegales;
    }
}
