/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.menu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaAreaCapturaOfertaEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-001 Establecer área o municipio para ofertas
 *
 * @author pedro.garcia
 * @cu CU-TV-0F-001
 * @version 2.0
 *
 *
 */
@Component("establecimientoAreasOfertas")
@Scope("session")
public class EstablecimientoAreasOfertasMB extends SNCManagedBean {

    /**
     * serial auto-generado
     */
    private static final long serialVersionUID = -517976981166965075L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EstablecimientoAreasOfertasMB.class);

    /**
     * mb para el cc de selección de departamento y municipio
     */
    @Autowired
    private CombosDeptosMunisMB combosDMZ;

    /**
     * lista de áreas de ofertas que están nen la bd
     */
    private List<AreaCapturaOferta> areasOfertas;

    /**
     * área de oferta seleccionada de la tabla
     */
    private AreaCapturaOferta[] areasOfertasSeleccionadas;

    private Date fechaActual;

    private boolean areaConInformacionGIS;

    /**
     * variable que guarda los datos del usuario que usa la aplicación
     */
    private UsuarioDTO usuario;

    /**
     * Variable auxiliar usada para integración con el visor
     */
    private String usuarioNombre;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Esta variable es para el parametro de las capas del mapa
     */
    private String moduleLayer;

    /**
     * Esta variable es para el parametro de las herramientas del mapa
     */
    private String moduleTools;

    /**
     * valor del campo hidden que se usa en al interacción con el mapa para crear los registros en
     * la tabla AREA_CAPTURA_DETALLE
     */
    private String idOfertaMapa;

    /**
     * indica si el usuario que ingresa el el coordinador del GIT avalúos
     */
    private boolean banderaEsCoordinador;

    /**
     * código de la territorial del usuario actual
     */
    private String territorialCod;

    /**
     * Objeto que guarda el área que se cree. Se necesita como variable de clase para poder obtener
     * el id de proceso y actualizar el área captura oferta, ya que es mejor garantizar que se
     * guarda en bd
     */
    private AreaCapturaOferta nuevaAreaCapturaOferta;

    /**
     * objetos de tipo Departamento, Municipio y Zona para la creacion del área
     */
    private Departamento departamento;
    private Municipio municipio;
    private Zona zona;

    // ------------- methods ----------------------
    public Date getFechaActual() {
        return this.fechaActual;
    }

    public void setFechaActual(Date fechaActual) {
        this.fechaActual = fechaActual;
    }

    public AreaCapturaOferta[] getAreasOfertasSeleccionadas() {
        return this.areasOfertasSeleccionadas;
    }

    public void setAreasOfertasSeleccionadas(
        AreaCapturaOferta[] areasOfertasSeleccionadas) {
        this.areasOfertasSeleccionadas = areasOfertasSeleccionadas;
    }

    public List<AreaCapturaOferta> getAreasOfertas() {
        return this.areasOfertas;
    }

    public void setAreasOfertas(ArrayList<AreaCapturaOferta> areasOfertas) {
        this.areasOfertas = areasOfertas;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public UsuarioDTO getLoggedInUser() {
        return this.usuario;
    }

    public void setLoggedInUser(UsuarioDTO loggedInUser) {
        this.usuario = loggedInUser;
    }

    public String getIdOfertaMapa() {
        return this.idOfertaMapa;
    }

    public void setIdOfertaMapa(String idOfertaMapa) {
        this.idOfertaMapa = idOfertaMapa;
    }

    public boolean isBanderaEsCoordinador() {
        return this.banderaEsCoordinador;
    }

    public void setAreaConInformacionGIS(boolean areaConInformacionGIS) {
        this.areaConInformacionGIS = areaConInformacionGIS;
    }

    public boolean isAreaConInformacionGIS() {
        return this.areaConInformacionGIS;
    }

    public String getUsuarioNombre() {
        return this.usuario.getNombreCompleto();
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public AreaCapturaOferta getNuevaAreaCapturaOferta() {
        return this.nuevaAreaCapturaOferta;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on EstablecimientoAreasOfertasMB init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.fechaActual = new Date(System.currentTimeMillis());
        this.banderaEsCoordinador = false;

        this.territorialCod = this.usuario.getCodigoEstructuraOrganizacional();

        if (!this.usuarioEsCoordinadorGIT()) {
            this.combosDMZ.setEstructuraTerritorialCod(this.territorialCod);
        }

        if (this.territorialCod == null) {
            this.territorialCod = Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL;
            this.usuario
                .setCodigoTerritorial(Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        this.cargarAreasVigentes(this.territorialCod);
        this.inicializarVariablesVisorGIS();

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Determina si el usuario activo es coordinador GIT, dependiendo de estro se habilitaran las
     * opciones de nueva y visor gis
     *
     * @author christian.rodriguez
     * @return verdadero si el usuario activo es COORDINADOR_GIT, falso si no
     */
    private boolean usuarioEsCoordinadorGIT() {
        String[] rolesUsuario = usuario.getRoles();
        for (String rol : rolesUsuario) {
            if (rol.equalsIgnoreCase(ERol.COORDINADOR_GIT_AVALUOS.toString())) {
                this.banderaEsCoordinador = true;
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que carga las áreas captura oferta vigentes para la territorial especificada
     *
     * @author christian.rodriguez
     * @param territorialCod código de la territorial
     */
    private void cargarAreasVigentes(String territorialCod) {
        this.areasOfertas = this.getAvaluosService()
            .buscarAreasCapturaVigentesPorEstrucOrg(territorialCod);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author lorena.salamanca
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.OFERTAS_INMOBILIARIAS.getCodigo();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * método action del botón "guardar" en la ventana flotante de creación de área
     *
     * @modified rodrigo.hernandez - uso de departamento, municipio y zona para la creación del área
     */
    public void guardarAreaOferta() {

        this.nuevaAreaCapturaOferta = new AreaCapturaOferta();

        // D: se toman los datos del mb de selección de depto, muni y zona
        // Se cargan datos de un departamento a partir de su codigo
        this.departamento = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(
                this.combosDMZ.getSelectedDepartamentoCod());

        // Se cargan datos de un municipio a partir de su codigo
        this.municipio = this.getGeneralesService().getCacheMunicipioByCodigo(
            this.combosDMZ.getSelectedMunicipioCod());

        // Se cargan datos de una zona a partir de su codigo
        if (this.combosDMZ.getSelectedZonaCod() != null &&
             !this.combosDMZ.getSelectedZonaCod().isEmpty()) {
            this.zona = this.getGeneralesService().getCacheZonaByCodigo(
                this.combosDMZ.getSelectedZonaCod());
        }

        this.nuevaAreaCapturaOferta.setDepartamento(departamento);
        this.nuevaAreaCapturaOferta.setMunicipio(municipio);
        this.nuevaAreaCapturaOferta.setZona(zona);
        this.nuevaAreaCapturaOferta.setFecha(this.fechaActual);
        this.nuevaAreaCapturaOferta
            .setEstado(EOfertaAreaCapturaOfertaEstado.VIGENTE.toString());
        this.nuevaAreaCapturaOferta.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());

        if (this.nuevaAreaCapturaOferta.getEstructuraOrganizacionalCod() == null) {
            this.nuevaAreaCapturaOferta
                .setEstructuraOrganizacionalCod(
                    Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        if (this.areaConInformacionGIS) {
            this.nuevaAreaCapturaOferta
                .setObservaciones("Área creada usando visor SIG");
        } else {
            this.nuevaAreaCapturaOferta
                .setObservaciones("Área creada manualmente");
        }

        this.nuevaAreaCapturaOferta.setUsuarioLog(this.usuario.getLogin());
        this.nuevaAreaCapturaOferta.setFechaLog(this.fechaActual);

        this.nuevaAreaCapturaOferta = this.getAvaluosService()
            .guardarAreaCapturaOferta(this.nuevaAreaCapturaOferta,
                this.usuario);

        this.addMensajeInfo("Creada el área de captura con id " +
             this.nuevaAreaCapturaOferta.getId() + ".");
        if (!this.banderaEsCoordinador) {
            this.addMensajeInfo(
                "Si no puede visualizar el área intente usando el botón 'Recargar áreas'.");
        }

        this.idOfertaMapa = String.valueOf(this.nuevaAreaCapturaOferta.getId());

        // D: hacer que se vuelvan a consultar las áreas que se muestran en la
        // tabla para reflejar
        // el cambio
        this.cargarAreasVigentes(this.territorialCod);

        // D: crear la instancia del proceso
        if (this.forwardProcess()) {
            this.addMensajeError("No se pudo crear la instancia del proceso para esta área");
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar varias áreas y darle el botón de eliminar. Borra las áreas de
     * la base
     *
     * @author christian.rodriguez
     * @modified pedro.garcia eliminación de las instancias de proceso relacionadas
     */
    public void eliminarAreas() {

        if (this.areasOfertasSeleccionadas.length != 0) {
            boolean operacionExistosa;
            for (AreaCapturaOferta areaCapturaOferta : this.areasOfertasSeleccionadas) {
                operacionExistosa = this.getAvaluosService()
                    .borrarAreaCapturaOferta(areaCapturaOferta.getId());

                if (!operacionExistosa) {
                    this.addMensajeInfo("El área " + areaCapturaOferta.getId() +
                         " tiene asignados recolectores y no " +
                         "pueden ser eliminadas");
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");
                } else {
                    this.eliminarInstanciasProceso();
                }
            }

            // D: hacer que se vuelvan a consultar las áreas que se muestran en
            // la tabla para reflejar
            // el cambio
            this.cargarAreasVigentes(territorialCod);

            this.eliminarInstanciasProceso();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" en la ventana principal del cu
     *
     * @return
     */
    public String cerrarVentanaCU() {

        UtilidadesWeb.removerManagedBeans();

        return ConstantesNavegacionWeb.INDEX;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * crea la instancia del proceso
     *
     * @author pedro.garcia
     * @return true si ocurrio un error al crear la instancia de proceso
     */
    private boolean forwardProcess() {

        LOGGER.debug("Creando proceso Ofertas");

        boolean error = false;
        List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
        DivisionAdministrativa municipioBPM, departamentoBPM, zonaBPM;
        OfertaInmobiliaria ofertaInmobiliariaBPM;
        String idProceso = "";

        listaUsuarios.add(this.usuario);

        // D: objeto OfertaInmobiliaria es el objeto de negocio del proceso
        ofertaInmobiliariaBPM = new OfertaInmobiliaria();
        ofertaInmobiliariaBPM.setIdentificador(new Long(this.idOfertaMapa));
        municipioBPM = new DivisionAdministrativa(this.municipio.getCodigo(),
            this.municipio.getNombre());
        departamentoBPM = new DivisionAdministrativa(
            this.departamento.getCodigo(), this.departamento.getNombre());

        if (this.zona != null) {
            zonaBPM = new DivisionAdministrativa(this.zona.getCodigo(),
                this.zona.getNombre());
            ofertaInmobiliariaBPM.setZona(zonaBPM);
        }

        ofertaInmobiliariaBPM.setMunicipio(municipioBPM);
        ofertaInmobiliariaBPM.setDepartamento(departamentoBPM);
        ofertaInmobiliariaBPM
            .setObservaciones("creación de área captura ofertas");
        ofertaInmobiliariaBPM
            .setTransicion(ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);
        ofertaInmobiliariaBPM.setUsuarios(listaUsuarios);
        ofertaInmobiliariaBPM.setTerritorial(this.usuario
            .getDescripcionEstructuraOrganizacional());
        ofertaInmobiliariaBPM.setFuncionario(this.usuario.getNombreCompleto());

        try {
            idProceso = this.getProcesosService().crearProceso(
                ofertaInmobiliariaBPM);

            this.doDatabaseStatesUpdate(idProceso);

            LOGGER.debug("proceso creado con id " + idProceso);
        } catch (Exception ex) {
            LOGGER.error("Error creando la instancia del proceso: " +
                 ex.getMessage());
            error = true;
        }

        // D: actualizar el área captura oferta con el id de proceso.
        // Se hace en una segunda vuelta para garantizar que queda guardada
        // antes de crear la
        // instancia del proceso
        try {
            this.nuevaAreaCapturaOferta.setProcesoInstanciaId(idProceso);
            this.nuevaAreaCapturaOferta = this.getAvaluosService()
                .guardarAreaCapturaOferta(this.nuevaAreaCapturaOferta,
                    this.usuario);
        } catch (Exception ex) {
            LOGGER.error("Error actualizando el AreaCapturaOferta con id " +
                 this.nuevaAreaCapturaOferta.getId());
        }

        return error;

    }

    // -----------------------------------------------------------------------
    /**
     * Actualiza un area captura oferta asociandole un id de proceso
     *
     * @author christian.rodriguez
     * @param idProceso id del proceso a asociar
     */
    private void doDatabaseStatesUpdate(String idProceso) {

        AreaCapturaOferta aco = this.getAvaluosService()
            .buscarAreaCapturaOfertaPorId(new Long(this.idOfertaMapa));

        if (aco != null) {
            aco.setProcesoInstanciaId(idProceso);
            this.getAvaluosService()
                .guardarAreaCapturaOferta(aco, this.usuario);
        }

        this.cargarAreasVigentes(this.territorialCod);

    }

    // -------------------------------------------------------------------------------------------------
    /**
     * elimina las instancias de proceso relacionadas con áreas que se eliminan
     *
     * @author pedro.garcia
     */
    private void eliminarInstanciasProceso() {

        String procesoId;

        if (this.areasOfertasSeleccionadas.length != 0) {
            for (AreaCapturaOferta aco : this.areasOfertasSeleccionadas) {
                procesoId = aco.getProcesoInstanciaId();
                try {
                    this.getProcesosService().cancelarProcesoPorIdProceso(
                        procesoId,
                        "por eliminación del área de captura de ofertas");
                } catch (Exception ex) {
                    LOGGER.error("Error cancelando la instancia de proceso " +
                         procesoId + ": " + ex.getMessage());
                    this.addMensajeError("Ocurrió un error cancelando alguna instancia del " +
                         "proceso.");
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Listener que recarga las áreas asociadas a la territorial del usuario
     *
     * @author christian.rodriguez
     */
    public void recargarAreas() {
        this.cargarAreasVigentes(this.territorialCod);
    }

    /**
     * Listener que reinicializa el objeto del area seleccionada
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaCreacionAreaSIG() {
        this.nuevaAreaCapturaOferta = new AreaCapturaOferta();
        this.combosDMZ.resetAll();
    }

    // end of class
}
