package co.gov.igac.snc.web.util;

/**
 *
 * @author juan.mendez
 *
 */
public class MonitoringAgent {

    // TODO esto debería ser un parámetro
    private boolean isEnabled;

    private static MonitoringAgent instance = null;

    /**
     *
     */
    protected MonitoringAgent() {
        // Exists only to defeat instantiation.
    }

    /**
     *
     * @return
     */
    public static MonitoringAgent getInstance() {
        if (instance == null) {
            instance = new MonitoringAgent();
            // TODO esto debería ser parametrizado
            instance.isEnabled = true;
        }
        return instance;
    }

    /**
     *
     * @return
     */
    public boolean isEnabled() {
        return isEnabled;
    }
}
