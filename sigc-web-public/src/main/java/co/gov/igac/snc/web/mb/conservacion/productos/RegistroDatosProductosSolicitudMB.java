package co.gov.igac.snc.web.mb.conservacion.productos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.ProcesoDeProductosCatastrales;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralError;
import co.gov.igac.snc.persistence.entity.tramite.ProductoFormato;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EArchivoAnexoFormato;
import co.gov.igac.snc.persistence.util.ECertEspDatoCertificar;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EProCatDetConDestinoA;
import co.gov.igac.snc.persistence.util.ERegPreCriterioBusqueda;
import co.gov.igac.snc.persistence.util.ERegPreNivelInformacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ProductoInconsistenciaDTO;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el registro de datos de productos solicitados
 *
 * @author javier.aponte
 */
@Component("registroDatosPS")
@Scope("session")
public class RegistroDatosProductosSolicitudMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -3865342860839390196L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RegistroDatosProductosSolicitudMB.class);

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    private long idSolicitudDeTrabajo;

    /**
     * Solicitud seleccionada en el árbol de tareas
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Lista de los documentos soportes de los productos catastrales solicitados
     */
    private List<Documento> documentosSoportesPC;

    /**
     * Lista que contiene los productos catastrales asociados a la solicitud
     */
    private List<ProductoCatastral> productosCatastralesAsociados;

    /**
     * Variable que contiene el producto catastral seleccionado
     */
    private ProductoCatastral productoCatastralSeleccionado;

    /**
     * Variable que contiene el nuevo producto catastral detalle que se desea adicionar
     */
    private ProductoCatastralDetalle nuevoProductoCatastralDetalle;

    /**
     * Variable que contiene el producto catastral detalle seleccionado
     */
    private ProductoCatastralDetalle productoCatastralDetalleSeleccionado;

    /**
     * Lista que contiene del DTO que contiene los datos del producto inconsistencia
     */
    private List<ProductoInconsistenciaDTO> productoInconsistenciasDTO;

    /**
     * Variable que contiene el DTO de producto inconsistencia seleccionado
     */
    private ProductoInconsistenciaDTO productoInconsistenciasDTOSeleccionado;

    //Variables para la pantalla que consulta los datos
    private String nombrePantalla;

    private String nombreFormularioPantalla;

    /**
     * Variable para saber si se activa el botón de enviar a ajustes en la solicitud del producto
     */
    private boolean activarBotonEnviarAAjustesSolicitudProducto;

    private boolean activarBotonIngresarModificarDatosP;

    private boolean activarBotonAceptarRegistrosPrediales;

    private boolean activarNivelDeInformacion;

    private boolean activarSeccionPersonaNatural;

    private boolean activarSeccionPersonaJuridica;

    private boolean activarSeccionMasivo;

    //Banderas para saber de que parte del proceso fue devuelto
    private boolean vieneDeIngresarSolicitudDeProductos;

    private boolean vieneDeRevisarProductosGenerados;

    private boolean vieneDeRegistrarCorreccionesAProductosEntregados;

    /**
     * Variable para saber que está en el caso de uso 005
     */
    private boolean isActividadRevisarInformacionDeProductosAGenerar;

    /**
     * Objeto que contiene las partes del número predial
     */
    private NumeroPredialPartes numeroPredialPartes;

    /**
     * Objeto que contiene las partes del número predial inicial cuando la consulta es por rango
     */
    private NumeroPredialPartes numeroPredialPartesInicial;

    /**
     * Objeto que contiene las partes del número predial final cuando la consulta es por rango
     */
    private NumeroPredialPartes numeroPredialPartesFinal;

    //----------------------------------Variables para las pantallas de registros prediales----------
    /**
     * Variable donde se almacena el criterio de búsqueda seleccionado en la pantalla de registros
     * prediales
     */
    private String criterioBusqueda;

    /**
     * Variable que almacena si en la pantalla de registros prediales se seleccionó la opción de con
     * datos de propietarios, ó con datos extendidos
     */
    private boolean datosAMostrar;

    /**
     * Variable nombre de la pantalla a abrir desde la pantalla de registros prediales
     */
    private String pantallaAAbrirDesdeRegistrosPrediales;

    /**
     * Variable nombre del formulario de la pantalla a abrir desde la pantalla de registros
     * prediales
     */
    private String formularioAActualizarDesdeRegistrosPrediales;

    /**
     * Código del departamento seleccionado
     */
    private String selectedDepartamentoCodigo;

    /**
     * Variable del departamento seleccionado
     */
    private Departamento selectedDepartamento;

    /**
     * Código del municipio seleccionado
     */
    private String selectedMunicipioCodigo;

    /**
     * Variable del municipio seleccionado
     */
    private Municipio selectedMunicipio;

    /**
     * Variable para saber si se requiere la información a la fecha actual
     */
    private boolean informacionALaFechaActual;

    /**
     * Variable que almacena el nivel de información que requiere el usuario
     */
    private String nivelDeInformacion;

    /**
     * Variable encargada de almacenar las personas asociadas a los productos
     */
    private List<ProductoCatastralDetalle> productosCatastralesDetallesAsociadasAProductos;

    private String descripcionTramiteCatastral;

    private String descripcionTramiteCatastralOtro;

    private String descripcionTramiteCatastralNota;

    //------------------------Variables para la pantalla de certificados especiales
    /**
     * {@link Array} usado para almacenar los datos a certificar seleccionados.
     */
    private String[] datosACertificarSelected;

    private boolean activarUltimoActoAdministrativo;
    private boolean activarAvaluosAnteriores;
    private boolean activarDestinoEconomico;
    private boolean activarNumeroPredialAnterior;
    private boolean activarPrediosColindantes;
    private boolean activarJustificacionDelDerechoDePropiedadOPosesion;
    private boolean activarZonaGeoeconomica;
    private boolean activarCotas;

//------------------------------Variables para la pantalla de destino de certificados catastrales----
    private boolean activarConDestinoA;

    private boolean activarDestinoOtro;

    private String destinoCertificadoCatastral;

    /**
     * Variable nombre de la pantalla a abrir desde la pantalla de destino certificados catastrales
     */
    private String pantallaAAbrirDesdeDestinoCertificadosCatastrales;

    /**
     * Variable nombre del formulario de la pantalla a abrir desde la pantalla de destino
     * certificados catastrales
     */
    private String formularioAActualizarDesdeDestinoCertificadosCatastrales;

//---------------------Variables para la pantalla de consulta de datos por predio
    private boolean activarSeleccionDepartamentoMunicipio;

    private String tipoDeSeleccionConsultaPredio;

    private boolean activarSeccionPrediosManzanas;

    private boolean activarTramiteCatastralOtro;

    private boolean activarDepuracionGeoOtro;

    private boolean activarEnviarTramiteCatastral;

    private boolean activarTotalRegistros;

    private String tipoDeSeleccionConsultaPersona;

    //---------------------Variables para la pantalla de consulta de predios masivamente
    /**
     * Mensaje del boton que permite cargar archivos masivamente
     */
    private String mensajeBotonCargaArchivoMasivamente;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteProducto;

    /**
     * Nombre del archivo de soporte del bloqueo masivo
     */
    private String nombreDocumentoProductoMasivo;

    /**
     * Documento temporal para la previsualización del archivo de bloqueo masivo
     */
    private Documento documentoTemporalProductoMasivo;

    /**
     * Archivo resultado cargado
     */
    private File archivoMasivoResultado;

    /**
     * Documento archivo anexo soporte masivo
     */
    private DocumentoArchivoAnexo documentoArchivoAnexo;

    /**
     * Documento temporal a visualizar
     */
    private Documento documentoTemporalAVisualizar;

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    private StreamedContent fileTempDownload;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

//---------------------Variables para la pantalla de consulta de datos por predio	
    private String tipoDeBusquedaResolucionesDeConservacion;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Variable para mostrar el aviso cuando la cantidad de productos generados es diferente a la
     * cantidad de productos solicitados
     */
    private String aviso;

    /**
     * Bandera para saber si hay productos catastrales asociados a la solicitud
     */
    private boolean banderaNoExisteProductoCatastralAsociado;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;

    /**
     * Orden apra la lista de Municipios
     */
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * indican si los combos están ordenados por nombre (si no, lo están por código)
     */
    private boolean municipiosOrderedByNombre;
    private boolean departamentosOrderedByNombre;

    private boolean usuarioDelegado;
    private List<Municipio> municipiosDelegados;

// -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init RegistroDatosProductosSolicitudMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        //felipe.cadena::22-12-2016::Ajustes para delegacion
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.municipiosDelegados = this.getGeneralesService().obtenerMunicipiosDelegados();

        try {

            this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

            //D: se obtiene el id del trámite
            this.idSolicitudDeTrabajo = this.tareasPendientesMB.getInstanciaSeleccionada().
                getIdObjetoNegocio();

            this.solicitudSeleccionada = this.getTramiteService().
                buscarSolicitudConSolicitantesSolicitudPorId(this.idSolicitudDeTrabajo);

            this.documentosSoportesPC = this.getTramiteService().buscarDocumentacionPorSolicitudId(
                this.idSolicitudDeTrabajo);

            this.productosCatastralesAsociados = this.getGeneralesService().
                buscarProductosCatastralesPorSolicitudId(this.idSolicitudDeTrabajo);

            this.banderaNoExisteProductoCatastralAsociado = false;
            if (this.productosCatastralesAsociados == null || this.productosCatastralesAsociados.
                isEmpty()) {

                this.banderaNoExisteProductoCatastralAsociado = true;

            }

            this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
            this.nuevoProductoCatastralDetalle.setPcdFinAprobado(ESiNo.NO.getCodigo());

            //Inicializa banderas para identificar de que parte del proceso viene
            this.vieneDeIngresarSolicitudDeProductos = false;
            this.vieneDeRevisarProductosGenerados = false;
            this.vieneDeRegistrarCorreccionesAProductosEntregados = false;

            //Establece en true de la parte del proceso de donde viene
            this.establecerVariablesDeProcesos();

            this.isActividadRevisarInformacionDeProductosAGenerar = false;

            //Se reclama la actividad
            this.reclamarActividad(this.solicitudSeleccionada);

            //Variables carga masiva predios
            this.activarBotonEnviarAAjustesSolicitudProducto = false;

            this.mensajeBotonCargaArchivoMasivamente = "Cargar archivo";

            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
            this.selectedDepartamentoCodigo = null;
            this.selectedMunicipioCodigo = null;

            // Inicialización de departamento
            if (this.usuarioDelegado) {
                this.departamentos = (ArrayList<Departamento>) this.getGeneralesService().
                    getDepartamentoByCodigoEstructuraOrganizacional(this.usuario.
                        getCodigoTerritorial());
            } else {
                this.departamentos = (ArrayList<Departamento>) this
                    .getGeneralesService().getCacheDepartamentosPorPais(
                        Constantes.COLOMBIA);
            }

            this.updateMunicipiosItemList();

            //Bandera para resoluciones de conservación
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }

    }

    /**
     * Método encargado de establecer las variables asociadas a procesos
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez 22/09/2014 Se agregan nuevos filtros para realizar la consulta de
     * lista de actividades
     */
    public void establecerVariablesDeProcesos() {

        Map<EParametrosConsultaActividades, String> filtros =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        filtros.put(EParametrosConsultaActividades.ID_PRODUCTOS_CATASTRALES,
            this.solicitudSeleccionada.getId().toString());
        filtros.put(EParametrosConsultaActividades.NUMERO_SOLICITUD, this.solicitudSeleccionada.
            getNumero());
        filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
        filtros.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, this.usuario.getLogin());

        List<Actividad> actividades = null;

        try {
            actividades = this.getProcesosService().consultarListaActividades(filtros);

            if (actividades != null && !actividades.isEmpty()) {

                //Las actividades las devuelve el process organizadas por fecha, por lo tanto la primera en devolver
                //es la actividad de donde viene
                if (actividades.size() > 1) {
                    if (ProcesoDeProductosCatastrales.ACT_PRODUCTOS_INGRESAR_AJUSTAR_SOLICITUD_PRODUCTO.
                        equals(actividades.get(actividades.size() - 2).getNombre())) {
                        this.vieneDeIngresarSolicitudDeProductos = true;
                    } else if (ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REVISAR_PRODUCTOS_GENERADOS.
                        equals(actividades.get(actividades.size() - 2).getNombre())) {
                        this.vieneDeRevisarProductosGenerados = true;
                    } else if (ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_CORRECCIONES_PRODUCTOS_ENTREGADOS.
                        equals(actividades.get(actividades.size() - 2).getNombre())) {
                        this.vieneDeRegistrarCorreccionesAProductosEntregados = true;
                    }
                }

            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error consultando la lista de actividades por el número de solicitud: " +
                this.solicitudSeleccionada.getNumero(), ex);
        }

    }

    /**
     * listener del cambio en el combo de departamentos
     */
    public void onChangeDepartamentos() {
        this.selectedMunicipio = null;
        this.updateMunicipiosItemList();
    }

    /**
     * action del commandlink para ordenar la lista de departamentos
     *
     * @author leidy.gonzalez
     */
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.updateDepartamentosItemList();
    }

    /**
     * Metodo que actualiza la lista de Departamentos
     *
     * @author leidy.gonzalez
     */
    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de municipios
     *
     * @author leidy.gonzalez
     */
    public void cambiarOrdenMunicipios() {

        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.updateMunicipiosItemList();
    }

    /**
     * actualiza la lista de municipios según el departamento seleccionado
     *
     * @author leidy.gonzalez
     */
    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        boolean munDelegado = false;
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));

        if (this.selectedDepartamentoCodigo != null) {
            if (this.usuarioDelegado) {
                this.municipios = new ArrayList<Municipio>();
                for (Municipio mun : this.municipiosDelegados) {
                    if (mun.getDepartamento().getCodigo().equals(this.selectedDepartamentoCodigo)) {
                        this.municipios.add(mun);
                    }
                }
            } else {
                List<Municipio> municipiosTotal = (ArrayList<Municipio>) this.getGeneralesService()
                    .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
                        this.selectedDepartamentoCodigo);
                this.municipios = new ArrayList<Municipio>();
                for (Municipio municipio : municipiosTotal) {
                    for (Municipio munDel : this.municipiosDelegados) {
                        if (munDel.getCodigo().equals(municipio.getCodigo())) {
                            munDelegado = true;
                            break;
                        }
                        if (!munDelegado) {
                            this.municipios.add(municipio);
                        }
                    }

                }

            }

            if (this.municipios != null && !this.municipios.isEmpty()) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    Collections.sort(this.municipios);
                } else {
                    Collections.sort(this.municipios, Municipio.getComparatorNombre());
                }

                for (Municipio municipio : this.municipios) {
                    if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                        this.municipiosItemList.add(new SelectItem(municipio
                            .getCodigo(), municipio.getCodigo3Digitos() +
                             "-" + municipio.getNombre()));
                    } else {
                        this.municipiosItemList.add(new SelectItem(
                            municipio.getCodigo(), municipio.getNombre()));
                    }
                }
            }
        }

    }

    //--------------------------------------------------------------------------------------------------
    @Override
    public boolean validateProcess() {
        return false;
    }

    @Override
    public void setupProcessMessage() {

    }

    @Override
    public void doDatabaseStatesUpdate() {

        for (ProductoCatastral pc : this.productosCatastralesAsociados) {

            this.getGeneralesService().guardarActualizarProductoCatastralAsociadoASolicitud(pc);

        }

    }

    //--------------------------------------MÉTODOS-----------------------------------------
    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public EOrden getOrdenDepartamentos() {
        return ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public ArrayList<Departamento> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(ArrayList<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public ArrayList<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ArrayList<Municipio> municipios) {
        this.municipios = municipios;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public boolean isActivarSeccionMasivo() {
        return activarSeccionMasivo;
    }

    public void setActivarSeccionMasivo(boolean activarSeccionMasivo) {
        this.activarSeccionMasivo = activarSeccionMasivo;
    }

    public StreamedContent getFileTempDownload() {
        controladorDescargaArchivos();
        return fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public Documento getDocumentoTemporalAVisualizar() {
        return documentoTemporalAVisualizar;
    }

    public void setDocumentoTemporalAVisualizar(
        Documento documentoTemporalAVisualizar) {
        this.documentoTemporalAVisualizar = documentoTemporalAVisualizar;
    }

    public DocumentoArchivoAnexo getDocumentoArchivoAnexo() {
        return documentoArchivoAnexo;
    }

    public void setDocumentoArchivoAnexo(DocumentoArchivoAnexo documentoArchivoAnexo) {
        this.documentoArchivoAnexo = documentoArchivoAnexo;
    }

    public Documento getDocumentoTemporalProductoMasivo() {
        return documentoTemporalProductoMasivo;
    }

    public void setDocumentoTemporalProductoMasivo(
        Documento documentoTemporalProductoMasivo) {
        this.documentoTemporalProductoMasivo = documentoTemporalProductoMasivo;
    }

    public Documento getDocumentoSoporteProducto() {
        return documentoSoporteProducto;
    }

    public void setDocumentoSoporteProducto(Documento documentoSoporteProducto) {
        this.documentoSoporteProducto = documentoSoporteProducto;
    }

    public String getNombreDocumentoProductoMasivo() {
        return nombreDocumentoProductoMasivo;
    }

    public void setNombreDocumentoProductoMasivo(
        String nombreDocumentoProductoMasivo) {
        this.nombreDocumentoProductoMasivo = nombreDocumentoProductoMasivo;
    }

    public String getDescripcionTramiteCatastralOtro() {
        return descripcionTramiteCatastralOtro;
    }

    public void setDescripcionTramiteCatastralOtro(
        String descripcionTramiteCatastralOtro) {
        this.descripcionTramiteCatastralOtro = descripcionTramiteCatastralOtro;
    }

    public String getDescripcionTramiteCatastralNota() {
        return descripcionTramiteCatastralNota;
    }

    public void setDescripcionTramiteCatastralNota(
        String descripcionTramiteCatastralNota) {
        this.descripcionTramiteCatastralNota = descripcionTramiteCatastralNota;
    }

    public String getDescripcionTramiteCatastral() {
        return descripcionTramiteCatastral;
    }

    public void setDescripcionTramiteCatastral(String descripcionTramiteCatastral) {
        this.descripcionTramiteCatastral = descripcionTramiteCatastral;
    }

    public String getMensajeCargaArchivo() {
        return mensajeBotonCargaArchivoMasivamente;
    }

    public void setMensajeCargaArchivo(String mensajeCargaArchivo) {
        this.mensajeBotonCargaArchivoMasivamente = mensajeCargaArchivo;
    }

    public boolean isActivarEnviarTramiteCatastral() {
        return activarEnviarTramiteCatastral;
    }

    public void setActivarEnviarTramiteCatastral(
        boolean activarEnviarTramiteCatastral) {
        this.activarEnviarTramiteCatastral = activarEnviarTramiteCatastral;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Documento> getDocumentosSoportesPC() {
        return documentosSoportesPC;
    }

    public void setDocumentosSoportesPC(List<Documento> documentosSoportesPC) {
        this.documentosSoportesPC = documentosSoportesPC;
    }

    public List<ProductoCatastral> getProductosCatastralesAsociados() {
        return productosCatastralesAsociados;
    }

    public void setProductosCatastralesAsociados(
        List<ProductoCatastral> productosCatastralesAsociados) {
        this.productosCatastralesAsociados = productosCatastralesAsociados;
    }

    public ProductoCatastral getProductoCatastralSeleccionado() {
        return productoCatastralSeleccionado;
    }

    public void setProductoCatastralSeleccionado(
        ProductoCatastral productoCatastralSeleccionado) {
        this.productoCatastralSeleccionado = productoCatastralSeleccionado;
    }

    public boolean isVieneDeIngresarSolicitudDeProductos() {
        return vieneDeIngresarSolicitudDeProductos;
    }

    public void setVieneDeIngresarSolicitudDeProductos(
        boolean vieneDeIngresarSolicitudDeProductos) {
        this.vieneDeIngresarSolicitudDeProductos = vieneDeIngresarSolicitudDeProductos;
    }

    public boolean isVieneDeRevisarProductosGenerados() {
        return vieneDeRevisarProductosGenerados;
    }

    public void setVieneDeRevisarProductosGenerados(
        boolean vieneDeRevisarProductosGenerados) {
        this.vieneDeRevisarProductosGenerados = vieneDeRevisarProductosGenerados;
    }

    public boolean isVieneDeRegistrarCorreccionesAProductosEntregados() {
        return vieneDeRegistrarCorreccionesAProductosEntregados;
    }

    public void setVieneDeRegistrarCorreccionesAProductosEntregados(
        boolean vieneDeRegistrarCorreccionesAProductosEntregados) {
        this.vieneDeRegistrarCorreccionesAProductosEntregados =
            vieneDeRegistrarCorreccionesAProductosEntregados;
    }

    public boolean isActividadRevisarInformacionDeProductosAGenerar() {
        return isActividadRevisarInformacionDeProductosAGenerar;
    }

    public void setActividadRevisarInformacionDeProductosAGenerar(
        boolean isActividadRevisarInformacionDeProductosAGenerar) {
        this.isActividadRevisarInformacionDeProductosAGenerar =
            isActividadRevisarInformacionDeProductosAGenerar;
    }

    public String getNombrePantalla() {
        return nombrePantalla;
    }

    public void setNombrePantalla(String nombrePantalla) {
        this.nombrePantalla = nombrePantalla;
    }

    public String getNombreFormularioPantalla() {
        return nombreFormularioPantalla;
    }

    public void setNombreFormularioPantalla(String nombreFormularioPantalla) {
        this.nombreFormularioPantalla = nombreFormularioPantalla;
    }

    public boolean isActivarBotonIngresarModificarDatosP() {
        return activarBotonIngresarModificarDatosP;
    }

    public void setActivarBotonIngresarModificarDatosP(
        boolean activarBotonIngresarModificarDatosP) {
        this.activarBotonIngresarModificarDatosP = activarBotonIngresarModificarDatosP;
    }

    public boolean isActivarBotonEnviarAAjustesSolicitudProducto() {
        return activarBotonEnviarAAjustesSolicitudProducto;
    }

    public void setActivarBotonEnviarAAjustesSolicitudProducto(
        boolean activarBotonEnviarAAjustesSolicitudProducto) {
        this.activarBotonEnviarAAjustesSolicitudProducto =
            activarBotonEnviarAAjustesSolicitudProducto;
    }

    public NumeroPredialPartes getNumeroPredialPartes() {
        return numeroPredialPartes;
    }

    public void setNumeroPredialPartes(NumeroPredialPartes numeroPredialPartes) {
        this.numeroPredialPartes = numeroPredialPartes;
    }

    public NumeroPredialPartes getNumeroPredialPartesInicial() {
        return numeroPredialPartesInicial;
    }

    public void setNumeroPredialPartesInicial(
        NumeroPredialPartes numeroPredialPartesInicial) {
        this.numeroPredialPartesInicial = numeroPredialPartesInicial;
    }

    public NumeroPredialPartes getNumeroPredialPartesFinal() {
        return numeroPredialPartesFinal;
    }

    public void setNumeroPredialPartesFinal(
        NumeroPredialPartes numeroPredialPartesFinal) {
        this.numeroPredialPartesFinal = numeroPredialPartesFinal;
    }

    public ProductoCatastralDetalle getNuevoProductoCatastralDetalle() {
        return nuevoProductoCatastralDetalle;
    }

    public void setNuevoProductoCatastralDetalle(
        ProductoCatastralDetalle nuevoProductoCatastralDetalle) {
        this.nuevoProductoCatastralDetalle = nuevoProductoCatastralDetalle;
    }

    public ProductoCatastralDetalle getProductoCatastralDetalleSeleccionado() {
        return productoCatastralDetalleSeleccionado;
    }

    public void setProductoCatastralDetalleSeleccionado(
        ProductoCatastralDetalle productoCatastralDetalleSeleccionado) {
        this.productoCatastralDetalleSeleccionado = productoCatastralDetalleSeleccionado;
    }

    public List<ProductoInconsistenciaDTO> getProductoInconsistenciasDTO() {
        return productoInconsistenciasDTO;
    }

    public void setProductoInconsistenciasDTO(
        List<ProductoInconsistenciaDTO> productoInconsistenciasDTO) {
        this.productoInconsistenciasDTO = productoInconsistenciasDTO;
    }

    public ProductoInconsistenciaDTO getProductoInconsistenciasDTOSeleccionado() {
        return productoInconsistenciasDTOSeleccionado;
    }

    public void setProductoInconsistenciasDTOSeleccionado(
        ProductoInconsistenciaDTO productoInconsistenciasDTOSeleccionado) {
        this.productoInconsistenciasDTOSeleccionado = productoInconsistenciasDTOSeleccionado;
    }

    public String getCriterioBusqueda() {
        return criterioBusqueda;
    }

    public void setCriterioBusqueda(String criterioBusqueda) {
        this.criterioBusqueda = criterioBusqueda;
    }

    public boolean getDatosAMostrar() {
        return datosAMostrar;
    }

    public void setDatosAMostrar(boolean datosAMostrar) {
        this.datosAMostrar = datosAMostrar;
    }

    public boolean isActivarBotonAceptarRegistrosPrediales() {
        return activarBotonAceptarRegistrosPrediales;
    }

    public void setActivarBotonAceptarRegistrosPrediales(
        boolean activarBotonAceptarRegistrosPrediales) {
        this.activarBotonAceptarRegistrosPrediales = activarBotonAceptarRegistrosPrediales;
    }

    public boolean isActivarNivelDeInformacion() {
        return activarNivelDeInformacion;
    }

    public void setActivarNivelDeInformacion(boolean activarNivelDeInformacion) {
        this.activarNivelDeInformacion = activarNivelDeInformacion;
    }

    public boolean isActivarSeccionPersonaNatural() {
        return activarSeccionPersonaNatural;
    }

    public void setActivarSeccionPersonaNatural(boolean activarSeccionPersonaNatural) {
        this.activarSeccionPersonaNatural = activarSeccionPersonaNatural;
    }

    public boolean isActivarSeccionPersonaJuridica() {
        return activarSeccionPersonaJuridica;
    }

    public void setActivarSeccionPersonaJuridica(
        boolean activarSeccionPersonaJuridica) {
        this.activarSeccionPersonaJuridica = activarSeccionPersonaJuridica;
    }

    public String getPantallaAAbrirDesdeRegistrosPrediales() {
        return pantallaAAbrirDesdeRegistrosPrediales;
    }

    public void setPantallaAAbrirDesdeRegistrosPrediales(
        String pantallaAAbrirDesdeRegistrosPrediales) {
        this.pantallaAAbrirDesdeRegistrosPrediales = pantallaAAbrirDesdeRegistrosPrediales;
    }

    public String getFormularioAActualizarDesdeRegistrosPrediales() {
        return formularioAActualizarDesdeRegistrosPrediales;
    }

    public void setFormularioAActualizarDesdeRegistrosPrediales(
        String formularioAActualizarDesdeRegistrosPrediales) {
        this.formularioAActualizarDesdeRegistrosPrediales =
            formularioAActualizarDesdeRegistrosPrediales;
    }

    public String getSelectedDepartamentoCodigo() {
        return selectedDepartamentoCodigo;
    }

    public void setSelectedDepartamentoCodigo(String selectedDepartamentoCodigo) {
        this.selectedDepartamentoCodigo = selectedDepartamentoCodigo;
        Departamento dptoSeleccionado = null;
        if (selectedDepartamentoCodigo != null && this.departamentos != null) {
            for (Departamento depto : this.departamentos) {
                if (selectedDepartamentoCodigo.equals(depto.getCodigo())) {
                    dptoSeleccionado = depto;
                    break;
                }
            }
        }
        this.selectedDepartamento = dptoSeleccionado;
        this.selectedMunicipioCodigo = null;
    }

    public String getSelectedMunicipioCodigo() {
        return selectedMunicipioCodigo;
    }

    public void setSelectedMunicipioCodigo(String selectedMunicipioCodigo) {
        this.selectedMunicipioCodigo = selectedMunicipioCodigo;
        Municipio municipioSelected = null;
        if (selectedMunicipioCodigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (selectedMunicipioCodigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.selectedMunicipio = municipioSelected;
    }

    public boolean isInformacionALaFechaActual() {
        return informacionALaFechaActual;
    }

    public void setInformacionALaFechaActual(boolean informacionALaFechaActual) {
        this.informacionALaFechaActual = informacionALaFechaActual;
    }

    public String getNivelDeInformacion() {
        return nivelDeInformacion;
    }

    public void setNivelDeInformacion(String nivelDeInformacion) {
        this.nivelDeInformacion = nivelDeInformacion;
    }

    public List<ProductoCatastralDetalle> getProductosCatastralesDetallesAsociadasAProductos() {
        return productosCatastralesDetallesAsociadasAProductos;
    }

    public void setProductosCatastralesDetallesAsociadasAProductos(
        List<ProductoCatastralDetalle> productosCatastralesDetallesAsociadasAProductos) {
        this.productosCatastralesDetallesAsociadasAProductos =
            productosCatastralesDetallesAsociadasAProductos;
    }

    public String[] getDatosACertificarSelected() {
        return datosACertificarSelected;
    }

    public void setDatosACertificarSelected(String[] datosACertificarSelected) {
        this.datosACertificarSelected = datosACertificarSelected;
    }

    public boolean isActivarUltimoActoAdministrativo() {
        return activarUltimoActoAdministrativo;
    }

    public void setActivarUltimoActoAdministrativo(
        boolean activarUltimoActoAdministrativo) {
        this.activarUltimoActoAdministrativo = activarUltimoActoAdministrativo;
    }

    public boolean isActivarAvaluosAnteriores() {
        return activarAvaluosAnteriores;
    }

    public void setActivarAvaluosAnteriores(boolean activarAvaluosAnteriores) {
        this.activarAvaluosAnteriores = activarAvaluosAnteriores;
    }

    public boolean isActivarDestinoEconomico() {
        return activarDestinoEconomico;
    }

    public void setActivarDestinoEconomico(boolean activarDestinoEconomico) {
        this.activarDestinoEconomico = activarDestinoEconomico;
    }

    public boolean isActivarNumeroPredialAnterior() {
        return activarNumeroPredialAnterior;
    }

    public void setActivarNumeroPredialAnterior(boolean activarNumeroPredialAnterior) {
        this.activarNumeroPredialAnterior = activarNumeroPredialAnterior;
    }

    public boolean isActivarPrediosColindantes() {
        return activarPrediosColindantes;
    }

    public void setActivarPrediosColindantes(boolean activarPrediosColindantes) {
        this.activarPrediosColindantes = activarPrediosColindantes;
    }

    public boolean isActivarJustificacionDelDerechoDePropiedadOPosesion() {
        return activarJustificacionDelDerechoDePropiedadOPosesion;
    }

    public void setActivarJustificacionDelDerechoDePropiedadOPosesion(
        boolean activarJustificacionDelDerechoDePropiedadOPosesion) {
        this.activarJustificacionDelDerechoDePropiedadOPosesion =
            activarJustificacionDelDerechoDePropiedadOPosesion;
    }

    public boolean isActivarZonaGeoeconomica() {
        return activarZonaGeoeconomica;
    }

    public void setActivarZonaGeoeconomica(boolean activarZonaGeoeconomica) {
        this.activarZonaGeoeconomica = activarZonaGeoeconomica;
    }

    public boolean isActivarCotas() {
        return activarCotas;
    }

    public void setActivarCotas(boolean activarCotas) {
        this.activarCotas = activarCotas;
    }

    public boolean isActivarConDestinoA() {
        return activarConDestinoA;
    }

    public void setActivarConDestinoA(boolean activarConDestinoA) {
        this.activarConDestinoA = activarConDestinoA;
    }

    public boolean isActivarDestinoOtro() {
        return activarDestinoOtro;
    }

    public void setActivarDestinoOtro(boolean activarDestinoOtro) {
        this.activarDestinoOtro = activarDestinoOtro;
    }

    public boolean isActivarTramiteCatastralOtro() {
        return activarTramiteCatastralOtro;
    }

    public void setActivarTramiteCatastralOtro(boolean activarTramiteCatastralOtro) {
        this.activarTramiteCatastralOtro = activarTramiteCatastralOtro;
    }

    public boolean isActivarDepuracionGeoOtro() {
        return activarDepuracionGeoOtro;
    }

    public void setActivarDepuracionGeoOtro(boolean activarDepuracionGeoOtro) {
        this.activarDepuracionGeoOtro = activarDepuracionGeoOtro;
    }

    public String getDestinoCertificadoCatastral() {
        return destinoCertificadoCatastral;
    }

    public void setDestinoCertificadoCatastral(String destinoCertificadoCatastral) {
        this.destinoCertificadoCatastral = destinoCertificadoCatastral;
    }

    public String getPantallaAAbrirDesdeDestinoCertificadosCatastrales() {
        return pantallaAAbrirDesdeDestinoCertificadosCatastrales;
    }

    public void setPantallaAAbrirDesdeDestinoCertificadosCatastrales(
        String pantallaAAbrirDesdeDestinoCertificadosCatastrales) {
        this.pantallaAAbrirDesdeDestinoCertificadosCatastrales =
            pantallaAAbrirDesdeDestinoCertificadosCatastrales;
    }

    public String getFormularioAActualizarDesdeDestinoCertificadosCatastrales() {
        return formularioAActualizarDesdeDestinoCertificadosCatastrales;
    }

    public void setFormularioAActualizarDesdeDestinoCertificadosCatastrales(
        String formularioAActualizarDesdeDestinoCertificadosCatastrales) {
        this.formularioAActualizarDesdeDestinoCertificadosCatastrales =
            formularioAActualizarDesdeDestinoCertificadosCatastrales;
    }

    public Departamento getSelectedDepartamento() {
        return selectedDepartamento;
    }

    public void setSelectedDepartamento(Departamento selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public Municipio getSelectedMunicipio() {
        return selectedMunicipio;
    }

    public void setSelectedMunicipio(Municipio selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public boolean isActivarSeleccionDepartamentoMunicipio() {
        return activarSeleccionDepartamentoMunicipio;
    }

    public void setActivarSeleccionDepartamentoMunicipio(
        boolean activarSeleccionDepartamentoMunicipio) {
        this.activarSeleccionDepartamentoMunicipio = activarSeleccionDepartamentoMunicipio;
    }

    public String getTipoDeSeleccionConsultaPredio() {
        return tipoDeSeleccionConsultaPredio;
    }

    public void setTipoDeSeleccionConsultaPredio(
        String tipoDeSeleccionConsultaPredio) {
        this.tipoDeSeleccionConsultaPredio = tipoDeSeleccionConsultaPredio;
    }

    public String getTipoDeSeleccionConsultaPersona() {
        return tipoDeSeleccionConsultaPersona;
    }

    public void setTipoDeSeleccionConsultaPersona(
        String tipoDeSeleccionConsultaPersona) {
        this.tipoDeSeleccionConsultaPersona = tipoDeSeleccionConsultaPersona;
    }

    public boolean isActivarSeccionPrediosManzanas() {
        return activarSeccionPrediosManzanas;
    }

    public void setActivarSeccionPrediosManzanas(
        boolean activarSeccionPrediosManzanas) {
        this.activarSeccionPrediosManzanas = activarSeccionPrediosManzanas;
    }

    public String getTipoDeBusquedaResolucionesDeConservacion() {
        return tipoDeBusquedaResolucionesDeConservacion;
    }

    public void setTipoDeBusquedaResolucionesDeConservacion(
        String tipoDeBusquedaResolucionesDeConservacion) {
        this.tipoDeBusquedaResolucionesDeConservacion = tipoDeBusquedaResolucionesDeConservacion;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public boolean isBanderaNoExisteProductoCatastralAsociado() {
        return banderaNoExisteProductoCatastralAsociado;
    }

    public void setBanderaNoExisteProductoCatastralAsociado(
        boolean banderaNoExisteProductoCatastralAsociado) {
        this.banderaNoExisteProductoCatastralAsociado = banderaNoExisteProductoCatastralAsociado;
    }

    /**
     * Método encargado de determinar que pantalla llamar dependiendo del tipo de producto
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez 15/09/2014 Se modifica el código que genera la carta catastral
     * urbana(del 80 al 5)
     */
    public void llamarPantallaConsultaDatos() {

        //Codigos de los productos asociados a consulta datos por predio
        Pattern patronConsultaDatosPorPredio = Pattern.compile("30|20|5|6|7|8|5");
        Matcher matConsultaDatosPorPredio = patronConsultaDatosPorPredio.matcher(
            this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Codigos de los productos asociados a registros prediales
        Pattern patronRegistrosPrediales = Pattern.compile("32|33|1598|1599|36|37|1595|38");
        Matcher matRegistrosPrediales = patronRegistrosPrediales.matcher(
            this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Codigos de los productos asociados a resoluciones
        Pattern patronResoluciones = Pattern.compile("35|1600");
        Matcher matResoluciones = patronResoluciones.matcher(this.productoCatastralSeleccionado.
            getProducto().getCodigo());

        //Códigos de los productos asociados a certificados catastrales nacionales
        Pattern patronCertificadosCatastralesNacionales = Pattern.compile("25|26|27|28|29|30|31");
        Matcher matCertificadosCatastralesNacionales = patronCertificadosCatastralesNacionales.
            matcher(this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Inicializa variables de las pantallas
        this.activarSeccionPersonaJuridica = false;
        this.activarSeccionPersonaNatural = false;
        this.activarSeccionMasivo = false;

        if (matConsultaDatosPorPredio.matches()) {
            this.nombrePantalla = "consultarPorDatosPredioWV";
            this.nombreFormularioPantalla = ":consultarPorDatosPredioForm";
        } else if (matRegistrosPrediales.matches()) {
            this.nombrePantalla = "registrosPredialesWV";
            this.nombreFormularioPantalla = ":registrosPredialesForm";
        } else if (matResoluciones.matches()) {
            this.nombrePantalla = "resolucionesWV";
            this.nombreFormularioPantalla = ":resolucionesConservacionForm";
        } else if (matCertificadosCatastralesNacionales.matches()) {

            this.nombrePantalla = "consultarPorDatosPersonaWV";
            this.nombreFormularioPantalla = ":consultarPorDatosPersonaForm";

            this.activarSeccionPersonaJuridica = true;
            this.activarSeccionPersonaNatural = true;

            this.activarConDestinoA = false;
            this.activarDestinoOtro = false;
            if (this.productoCatastralSeleccionado.getProducto().getCodigo().equals("26")) {
                this.nuevoProductoCatastralDetalle.setConDestinoA(
                    EProCatDetConDestinoA.LIBRETA_MILITAR.getNombre());
            } else if (this.productoCatastralSeleccionado.getProducto().getCodigo().equals("27")) {
                this.nuevoProductoCatastralDetalle.setConDestinoA(
                    EProCatDetConDestinoA.SUBSIDIO_DE_VIVIENDA.getNombre());
            } else if (this.productoCatastralSeleccionado.getProducto().getCodigo().equals("28")) {
                this.nuevoProductoCatastralDetalle.setConDestinoA(
                    EProCatDetConDestinoA.CENTRO_EDUCATIVO_OFICIAL.getNombre());
            } else {
                this.activarConDestinoA = true;
                this.nombrePantalla = "destinoCertificadosCatastralesWV";
                this.nombreFormularioPantalla = ":destinoCertificadosCatastralesForm";
            }

            if (this.productoCatastralSeleccionado.getProducto().getCodigo().equals("29")) {
                this.pantallaAAbrirDesdeDestinoCertificadosCatastrales = "consultarPorDatosPredioWV";
                this.formularioAActualizarDesdeDestinoCertificadosCatastrales =
                    ":consultarPorDatosPredioForm";
            } else if (this.productoCatastralSeleccionado.getProducto().getCodigo().equals("31")) {
                this.pantallaAAbrirDesdeDestinoCertificadosCatastrales =
                    "certificadoCatastralEspecialWV";
                this.formularioAActualizarDesdeDestinoCertificadosCatastrales =
                    ":certificadoCatastralEspecialForm";
            } else {
                this.pantallaAAbrirDesdeDestinoCertificadosCatastrales =
                    "consultarPorDatosPersonaWV";
                this.formularioAActualizarDesdeDestinoCertificadosCatastrales =
                    ":consultarPorDatosPersonaForm";
            }

        }

        if (this.nombrePantalla != null && !this.nombrePantalla.trim().isEmpty()) {
            this.activarBotonIngresarModificarDatosP = true;
        }

    }

    /**
     * Método encargado de determinar que pantalla llamar dependiendo del tipo de producto en la
     * pantalla de revisar información de productos a entregar
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se agregan codigos asociados a las resoluciones para ser
     * visualizados en la pantalla de Revisar Información de Productos a Generar.
     */
    public void llamarPantallaConsultaDatosDesdeRevisarInformacion() {

        this.productosCatastralesDetallesAsociadasAProductos = this.getGeneralesService().
            buscarProductosCatastralesDetallePorProductoCatastralId(
                this.productoCatastralSeleccionado.getId());

        //Codigos de los productos asociados a consulta datos por predio
        Pattern patronConsultaDatosPorPredio = Pattern.compile("30|29|20|5|6|7|8|5");
        Matcher matConsultaDatosPorPredio = patronConsultaDatosPorPredio.matcher(
            this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Codigos de los productos asociados a registros prediales
        Pattern patronRegistrosPrediales = Pattern.compile("32|33|1598|1599|36|37|1595|38");
        Matcher matRegistrosPrediales = patronRegistrosPrediales.matcher(
            this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Códigos de los productos asociados a certificados catastrales nacionales
        Pattern patronCertificadosCatastralesNacionales = Pattern.compile("25|26|27|28");
        Matcher matCertificadosCatastralesNacionales = patronCertificadosCatastralesNacionales.
            matcher(this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Códigos de los productos asociados a certificados catastrales especial
        Pattern patronCertificadosCatastralEspecial = Pattern.compile("31");
        Matcher matCertificadosCatastralEspecial = patronCertificadosCatastralEspecial.matcher(
            this.productoCatastralSeleccionado.getProducto().getCodigo());

        //Codigos de los productos asociados a resoluciones
        Pattern patronResoluciones = Pattern.compile("35|1600");
        Matcher matResoluciones = patronResoluciones.matcher(this.productoCatastralSeleccionado.
            getProducto().getCodigo());

        //Inicializa variables de las pantallas
        this.activarSeccionPersonaJuridica = false;
        this.activarSeccionPersonaNatural = false;

        if (matConsultaDatosPorPredio.matches()) {
            this.nombrePantalla = "consultarPorDatosPredioWV";
            this.nombreFormularioPantalla = ":consultarPorDatosPredioForm";
        } else if (matRegistrosPrediales.matches()) {

            if (this.productosCatastralesDetallesAsociadasAProductos != null &&
                !this.productosCatastralesDetallesAsociadasAProductos.isEmpty()) {
                if (this.productosCatastralesDetallesAsociadasAProductos.get(0).
                    getNumeroPredialInicial() != null) {
                    this.nombrePantalla = "consultarPorDatosPredioWV";
                    this.nombreFormularioPantalla = ":consultarPorDatosPredioForm";
                } else if (this.productosCatastralesDetallesAsociadasAProductos.get(0).
                    getTipoIdentificacion() != null &&
                    this.productosCatastralesDetallesAsociadasAProductos.get(0).
                        getNumeroIdentificacion() != null) {
                    this.nombrePantalla = "consultarPorDatosPersonaWV";
                    this.nombreFormularioPantalla = ":consultarPorDatosPersonaForm";
                }
            }
        } else if (matCertificadosCatastralesNacionales.matches()) {

            this.nombrePantalla = "consultarPorDatosPersonaWV";
            this.nombreFormularioPantalla = ":consultarPorDatosPersonaForm";

            this.activarSeccionPersonaJuridica = true;
            this.activarSeccionPersonaNatural = true;

        } else if (matCertificadosCatastralEspecial.matches()) {
            this.nombrePantalla = "certificadoCatastralEspecialWV";
            this.nombreFormularioPantalla = ":certificadoCatastralEspecialForm";
        } else if (matResoluciones.matches()) {
            this.nombrePantalla = "resolucionesWV";
            this.nombreFormularioPantalla = ":resolucionesConservacionForm";
        }

        if (this.nombrePantalla != null && !this.nombrePantalla.trim().isEmpty()) {
            this.activarBotonIngresarModificarDatosP = true;
        }

    }

    /**
     * Método encargado de cambiar el destino a de la pantalla de descripción de trámite catastral
     *
     * @author javier.aponte
     */
    public void cambioDescripcionTramiteCatastralSel() {

        this.activarTramiteCatastralOtro = false;
        if (Constantes.OTRO.equalsIgnoreCase(this.descripcionTramiteCatastral)) {
            this.activarTramiteCatastralOtro = true;
        }

    }

    /**
     * Método encargado de cambiar el destino a de la pantalla de depuración geográfica
     *
     * @author javier.aponte
     */
    public void cambioDepuracionGeograficaSel() {

        this.activarDepuracionGeoOtro = false;
        if (Constantes.OTRO.equalsIgnoreCase(this.nuevoProductoCatastralDetalle.
            getDepuracionGeoDescripcion())) {
            this.activarDepuracionGeoOtro = true;
        }

    }

    /**
     * Método encargado de cambiar el destino a de la pantalla de destino certificados catastrales
     *
     * @author javier.aponte
     */
    public void cambioDestinoASel() {

        this.activarDestinoOtro = false;
        if (EProCatDetConDestinoA.OTRO.getNombre().equals(this.destinoCertificadoCatastral)) {
            this.activarDestinoOtro = true;
        }

    }

    /**
     * Método encargado de determinar que pantalla llamar dependiendo del tipo de producto
     *
     * @author javier.aponte
     */
    public void eliminarLlamadoPantallaConsultaDatos() {

        this.nombrePantalla = "";
        this.nombreFormularioPantalla = "";

        this.activarBotonIngresarModificarDatosP = false;

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de ingresar o modificar datos de productos
     *
     * @author javier.aponte
     */
    public void ingresarModificarDatosDeProductos() {

        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

        this.nuevoProductoCatastralDetalle.setProductoCatastral(this.productoCatastralSeleccionado);

        this.productosCatastralesDetallesAsociadasAProductos = this.getGeneralesService().
            buscarProductosCatastralesDetallePorProductoCatastralId(
                this.productoCatastralSeleccionado.getId());

        this.inicializarVariablesDePantallas();
    }

    /**
     * Método que se ejecuta cuando se oprime el botón de revisar información de productos a generar
     *
     * @author javier.aponte
     */
    public void revisarInformacionDeProductosAGenerar() {

        if (this.productosCatastralesAsociados == null || this.productosCatastralesAsociados.
            isEmpty()) {
            this.addMensajeError(
                "Ocurrió un error al consultar los datos de los productos catastrales asociados");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                Constantes.ERROR_REQUEST_CONTEXT);
            return;
        }

        this.inicializarVariablesDePantallas();

    }

    private void inicializarVariablesDePantallas() {

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartesInicial = new NumeroPredialPartes();
        this.numeroPredialPartesFinal = new NumeroPredialPartes();

        //Inicializa las variables de la pantalla de registros prediales
        this.selectedDepartamentoCodigo = new String();
        this.selectedMunicipioCodigo = new String();
        this.criterioBusqueda = new String();
        this.informacionALaFechaActual = false;
        this.datosAMostrar = false;

        this.nivelDeInformacion = null;
        this.activarBotonAceptarRegistrosPrediales = false;
        this.activarBotonIngresarModificarDatosP = false;

        this.activarUltimoActoAdministrativo = false;
        this.activarAvaluosAnteriores = false;
        this.activarDestinoEconomico = false;
        this.activarNumeroPredialAnterior = false;
        this.activarPrediosColindantes = false;
        this.activarJustificacionDelDerechoDePropiedadOPosesion = false;
        this.activarZonaGeoeconomica = false;
        this.activarCotas = false;

        this.activarConDestinoA = false;

        this.activarSeleccionDepartamentoMunicipio = true;
        this.activarSeccionPrediosManzanas = true;

        this.activarTramiteCatastralOtro = false;
        this.activarDepuracionGeoOtro = false;

        this.datosACertificarSelected = null;

        this.destinoCertificadoCatastral = new String();

        this.aviso = new String();
        this.activarBotonEnviarAAjustesSolicitudProducto = false;

        this.productoInconsistenciasDTO = new ArrayList<ProductoInconsistenciaDTO>();

        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.
            getManagedBean("combosDeptosMunis");

        combosDeptosMunisMB.setSelectedDepartamentoCod(new String());
        combosDeptosMunisMB.setSelectedMunicipioCod(new String());

        this.activarEnviarTramiteCatastral = false;

        this.activarNivelDeInformacion = false;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("registroDatosPS");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método encargado de guardar los productos catastrales detalle a los que se les registraron
     * inconsistencias
     *
     * @author javier.aponte
     */
    public void guardarProductoCatastralDetalleInconsistencias() {

        StringBuilder inconsistencias = new StringBuilder();

        for (ProductoInconsistenciaDTO pi : this.productoInconsistenciasDTO) {
            if (this.productoInconsistenciasDTOSeleccionado.getTramiteCatastralDescripcion() != null) {
                pi.setTramiteCatastralDescripcion(this.productoInconsistenciasDTOSeleccionado.
                    getTramiteCatastralDescripcion());
                inconsistencias.append(pi.getNumeroPredial() + " - " + pi.
                    getTramiteCatastralDescripcion() + "\n");
            } else {
                inconsistencias.append(pi.getNumeroPredial());
            }

        }

        ProductoCatastralError productoCatastralError = new ProductoCatastralError();

        productoCatastralError.setInconsistencias(inconsistencias.toString());
        productoCatastralError.setProductoCatastralId(this.productoCatastralSeleccionado.getId());
        productoCatastralError.setFechaLog(new Date());
        productoCatastralError.setUsuarioLog(this.usuario.getLogin());

        this.getGeneralesService().guardarActualizarProductoCatastralError(productoCatastralError);

        if (this.productoInconsistenciasDTOSeleccionado.getTramiteCatastralDescripcion() != null) {

            this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                this.productoInconsistenciasDTOSeleccionado.getTramiteCatastralDescripcion());

        }

        limpiarCamposRegistrarInconsistencia();

    }

    public void limpiarCamposRegistrarInconsistencia() {

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartesInicial = new NumeroPredialPartes();
        this.productoInconsistenciasDTO = new ArrayList<ProductoInconsistenciaDTO>();

    }

    public void limpiarCamposDepartamentoYMunicipio() {

        this.selectedDepartamento = null;
        this.selectedDepartamentoCodigo = null;
        this.selectedMunicipio = null;
        this.selectedMunicipioCodigo = null;

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de aceptar en la pantalla de registros
     * prediales
     *
     * @author javier.aponte
     */
    public void aceptarRegistrosPrediales() {

        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.
            getManagedBean("combosDeptosMunis");

        if (combosDeptosMunisMB.getSelectedDepartamentoCod() == null ||
             combosDeptosMunisMB.getSelectedDepartamentoCod().isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_007.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_007.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                Constantes.ERROR_REQUEST_CONTEXT);
            return;
        }
        if (combosDeptosMunisMB.getSelectedMunicipioCod() == null ||
             combosDeptosMunisMB.getSelectedMunicipioCod().isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_007.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_007.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                Constantes.ERROR_REQUEST_CONTEXT);
            return;
        }

        this.seleccionDepartamentoYMunicipio();

        /*
         * @modified by hector.arias :: 07/06/2017:: #20009 :: Se completa el codigo del
         * departamento y municipio cuando no se selecciona ningún nivel de información para generar
         * registros 1 y 2
         */
        if (this.nivelDeInformacion == null || !this.nivelDeInformacion.isEmpty()) {
            this.numeroPredialPartes.setDepartamentoCodigo(selectedDepartamento.getCodigo());
            this.numeroPredialPartes.setMunicipioCodigo(selectedMunicipio.getCodigo().
                substring(2, 5));
            this.numeroPredialPartesInicial.setDepartamentoCodigo(selectedDepartamento.getCodigo());
            this.numeroPredialPartesInicial.setMunicipioCodigo(selectedMunicipio.getCodigo().
                substring(2, 5));
            this.numeroPredialPartesFinal.setDepartamentoCodigo(selectedDepartamento.getCodigo());
            this.numeroPredialPartesFinal.setMunicipioCodigo(selectedMunicipio.getCodigo().
                substring(2, 5));
        }

        if (ERegPreNivelInformacion.MUNICIPIO.getCodigo().equals(this.nivelDeInformacion)) {
            if (this.selectedDepartamentoCodigo == null || this.selectedDepartamentoCodigo.isEmpty() ||
                 this.selectedMunicipioCodigo == null || this.selectedDepartamentoCodigo.isEmpty()) {
                this.addMensajeError(
                    "Si seleccionó nivel de información Municipio, debe seleccionar" +
                    "el departamento y el municipio");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
                return;
            }
        }

        if (ERegPreNivelInformacion.MUNICIPIO.getCodigo().equals(this.nivelDeInformacion) ||
             ERegPreNivelInformacion.ZONA.getCodigo().equals(this.nivelDeInformacion) ||
             ERegPreNivelInformacion.SECTOR.getCodigo().equals(this.nivelDeInformacion) ||
             ERegPreNivelInformacion.MANZANA.getCodigo().equals(this.nivelDeInformacion)) {

            this.numeroPredialPartes.setDepartamentoCodigo(this.selectedDepartamentoCodigo);
            this.numeroPredialPartes.setMunicipioCodigo((this.selectedMunicipioCodigo != null &&
                !this.selectedMunicipioCodigo.isEmpty()) ? this.selectedMunicipioCodigo.substring(2,
                5) : "");

            this.numeroPredialPartes.setPredio("0000");
            this.numeroPredialPartes.setCondicionPropiedad("0");
            this.numeroPredialPartes.setEdificio("00");
            this.numeroPredialPartes.setPiso("00");
            this.numeroPredialPartes.setUnidad("0000");

            this.numeroPredialPartesInicial.setDepartamentoCodigo(this.selectedDepartamentoCodigo);
            this.numeroPredialPartesInicial.setMunicipioCodigo(
                (this.selectedMunicipioCodigo != null && !this.selectedMunicipioCodigo.isEmpty()) ?
                this.selectedMunicipioCodigo.substring(2, 5) : "");

            this.numeroPredialPartesInicial.setPredio("0000");
            this.numeroPredialPartesInicial.setCondicionPropiedad("0");
            this.numeroPredialPartesInicial.setEdificio("00");
            this.numeroPredialPartesInicial.setPiso("00");
            this.numeroPredialPartesInicial.setUnidad("0000");

            this.numeroPredialPartesFinal.setDepartamentoCodigo(this.selectedDepartamentoCodigo);
            this.numeroPredialPartesFinal.setMunicipioCodigo(
                (this.selectedMunicipioCodigo != null && !this.selectedMunicipioCodigo.isEmpty()) ?
                this.selectedMunicipioCodigo.substring(2, 5) : "");

            this.numeroPredialPartesFinal.setPredio("9999");
            this.numeroPredialPartesFinal.setCondicionPropiedad("9");
            this.numeroPredialPartesFinal.setEdificio("99");
            this.numeroPredialPartesFinal.setPiso("99");
            this.numeroPredialPartesFinal.setUnidad("9999");

        }

        if (ERegPreNivelInformacion.MUNICIPIO.getCodigo().equals(this.nivelDeInformacion)) {

            this.numeroPredialPartes.setTipoAvaluo("00");
            this.numeroPredialPartes.setSectorCodigo("00");
            this.numeroPredialPartes.setComunaCodigo("00");
            this.numeroPredialPartes.setBarrioCodigo("00");
            this.numeroPredialPartes.setManzanaVeredaCodigo("0000");

            String numeroPredial = this.numeroPredialPartes.assembleNumeroPredialFromSegments();
            this.nuevoProductoCatastralDetalle.setNumeroPredial(numeroPredial);

            this.numeroPredialPartesInicial.setTipoAvaluo("00");
            this.numeroPredialPartesInicial.setSectorCodigo("00");
            this.numeroPredialPartesInicial.setComunaCodigo("00");
            this.numeroPredialPartesInicial.setBarrioCodigo("00");
            this.numeroPredialPartesInicial.setManzanaVeredaCodigo("0000");

            numeroPredial = this.numeroPredialPartesInicial.assembleNumeroPredialFromSegments();
            this.nuevoProductoCatastralDetalle.setNumeroPredialInicial(numeroPredial);

            this.numeroPredialPartesFinal.setTipoAvaluo("99");
            this.numeroPredialPartesFinal.setSectorCodigo("99");
            this.numeroPredialPartesFinal.setComunaCodigo("99");
            this.numeroPredialPartesFinal.setBarrioCodigo("99");
            this.numeroPredialPartesFinal.setManzanaVeredaCodigo("9999");

            numeroPredial = this.numeroPredialPartesFinal.assembleNumeroPredialFromSegments();
            this.nuevoProductoCatastralDetalle.setNumeroPredialFinal(numeroPredial);

            this.productosCatastralesDetallesAsociadasAProductos.add(
                this.nuevoProductoCatastralDetalle);
            this.activarSeccionPrediosManzanas = false;

        } else if (ERegPreNivelInformacion.ZONA.getCodigo().equals(this.nivelDeInformacion)) {

            this.numeroPredialPartes.setSectorCodigo("00");
            this.numeroPredialPartes.setComunaCodigo("00");
            this.numeroPredialPartes.setBarrioCodigo("00");
            this.numeroPredialPartes.setManzanaVeredaCodigo("0000");

            this.numeroPredialPartesInicial.setSectorCodigo("00");
            this.numeroPredialPartesInicial.setComunaCodigo("00");
            this.numeroPredialPartesInicial.setBarrioCodigo("00");
            this.numeroPredialPartesInicial.setManzanaVeredaCodigo("0000");

            this.numeroPredialPartesFinal.setSectorCodigo("99");
            this.numeroPredialPartesFinal.setComunaCodigo("99");
            this.numeroPredialPartesFinal.setBarrioCodigo("99");
            this.numeroPredialPartesFinal.setManzanaVeredaCodigo("9999");

        } else if (ERegPreNivelInformacion.SECTOR.getCodigo().equals(this.nivelDeInformacion)) {

            this.numeroPredialPartes.setComunaCodigo("00");
            this.numeroPredialPartes.setBarrioCodigo("00");
            this.numeroPredialPartes.setManzanaVeredaCodigo("0000");

            this.numeroPredialPartesInicial.setComunaCodigo("00");
            this.numeroPredialPartesInicial.setBarrioCodigo("00");
            this.numeroPredialPartesInicial.setManzanaVeredaCodigo("0000");

            this.numeroPredialPartesFinal.setComunaCodigo("99");
            this.numeroPredialPartesFinal.setBarrioCodigo("99");
            this.numeroPredialPartesFinal.setManzanaVeredaCodigo("9999");

        }

        if (this.selectedDepartamentoCodigo != null && !this.selectedDepartamentoCodigo.isEmpty()) {
            this.nuevoProductoCatastralDetalle.
                setDepartamentoCodigo(this.selectedDepartamentoCodigo);
            this.activarSeleccionDepartamentoMunicipio = false;
        }
        if (this.selectedMunicipioCodigo != null && !this.selectedMunicipioCodigo.isEmpty()) {
            this.nuevoProductoCatastralDetalle.setMunicipioCodigo(this.selectedMunicipioCodigo);
        }

        this.nuevoProductoCatastralDetalle.setFechaLog(new Date());
        this.nuevoProductoCatastralDetalle.setUsuarioLog(this.usuario.getLogin());

    }

    /**
     * Método que limpia los campos de registros prediales inmediatamente despues de oprimir el
     * boton aceptar
     *
     * @author javier.aponte
     */
    public void limpiarCamposRegistrosPrediales() {
        this.selectedDepartamento = new Departamento();
        this.selectedDepartamentoCodigo = "";
        this.selectedMunicipio = new Municipio();
        this.selectedMunicipioCodigo = "";
        this.criterioBusqueda = "";
        this.productoCatastralDetalleSeleccionado.setFormato("");
        this.nuevoProductoCatastralDetalle.setDepartamentoCodigo("");
        this.nuevoProductoCatastralDetalle.setMunicipioCodigo("");
    }

    /**
     * Método que se ejecuta cuando se oprime el botón de cerrar en la pantalla de registros
     * prediales y los dialogos de criterios de busqueda
     *
     * @author leidy.gonzalez
     */
    /*
     * @modified by leidy.gonzalez :: 21/01/2015:: #11167 :: Se elimina inicializacion de tabla de
     * productos asociados a solicitud debido a que si se cierra el dialogo y aun no se a enviado el
     * producto no encuentra datos para avanzar el proceso
     */
    public void cerrarDialogo() {

        this.productoCatastralSeleccionado = new ProductoCatastral();
        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

    }

    /**
     * Método para saber el departamento y municipio seleccionados por el usuario
     *
     * @author javier.aponte
     */
    private void seleccionDepartamentoYMunicipio() {
        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.
            getManagedBean("combosDeptosMunis");

        this.selectedDepartamentoCodigo = combosDeptosMunisMB.getSelectedDepartamentoCod();

        this.selectedDepartamento = combosDeptosMunisMB.getSelectedDepartamento();

        this.selectedMunicipioCodigo = combosDeptosMunisMB.getSelectedMunicipioCod();

        this.selectedMunicipio = combosDeptosMunisMB.getSelectedMunicipio();
    }

    /**
     * Método que se ejecuta cuando se cambia la selección en el criterio de búsqueda en la pantalla
     * de registros prediales
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se realiza validacion para que cuando el producto ha sido
     * generado visualice un mensaje de advertencia con que tipo de criterio de busqueda se genero y
     * no intente generarlo por otro debido a que los criterios de busueda son excluyentes.
     */
    public void cambiarCriterioDeBusqueda() {

        //Inicializa variables de las pantallas
        this.activarBotonAceptarRegistrosPrediales = false;
        this.activarNivelDeInformacion = false;
        this.activarSeccionPersonaJuridica = false;
        this.activarSeccionPersonaNatural = false;

        if (this.criterioBusqueda != null && !this.criterioBusqueda.isEmpty()) {

            this.activarBotonAceptarRegistrosPrediales = true;

            if (this.productoCatastralSeleccionado.getProductoGenerar() != null &&
                 this.productoCatastralSeleccionado.getProductoGenerar()
                    .equals(ESiNo.SI.getCodigo())) {
                if (this.productosCatastralesDetallesAsociadasAProductos != null &&
                    !this.productosCatastralesDetallesAsociadasAProductos.isEmpty()) {
                    for (ProductoCatastralDetalle pcd
                        : this.productosCatastralesDetallesAsociadasAProductos) {
                        if (pcd.getNumeroIdentificacion() != null) {

                            this.addMensajeWarn(
                                "El producto catastral seleccionado ya ha sido generado por el tipo de criterio de busqueda: nombre de propietario o documento de identidad");
                            this.pantallaAAbrirDesdeRegistrosPrediales =
                                "consultarPorDatosPersonaWV";
                            this.formularioAActualizarDesdeRegistrosPrediales =
                                ":consultarPorDatosPersonaForm";

                            this.criterioBusqueda =
                                ERegPreCriterioBusqueda.POR_DOCUMENTO_DE_IDENTIDAD.getCodigo();

                            if (pcd.getPrimerNombre() != null || pcd.getRazonSocial() != null) {
                                this.activarSeccionPersonaJuridica = true;
                                this.activarSeccionPersonaNatural = true;
                                this.criterioBusqueda =
                                    ERegPreCriterioBusqueda.POR_NOMBRE_DE_PROPIETARIO.getCodigo();
                            }
                            break;

                        } else if (pcd.getNumeroPredial() != null) {

                            this.addMensajeWarn(
                                "El producto catastral seleccionado ya ha sido generado por el tipo de criterio de busqueda:predio");
                            this.pantallaAAbrirDesdeRegistrosPrediales = "consultarPorDatosPredioWV";
                            this.formularioAActualizarDesdeRegistrosPrediales =
                                ":consultarPorDatosPredioForm";
                            this.activarNivelDeInformacion = true;
                            this.criterioBusqueda = ERegPreCriterioBusqueda.POR_PREDIO.getCodigo();
                            break;

                        }

                    }
                }
            } else {
                if (ERegPreCriterioBusqueda.POR_PREDIO.getCodigo().equals(this.criterioBusqueda)) {

                    this.pantallaAAbrirDesdeRegistrosPrediales = "consultarPorDatosPredioWV";
                    this.formularioAActualizarDesdeRegistrosPrediales =
                        ":consultarPorDatosPredioForm";
                    this.activarNivelDeInformacion = true;
                }
                if (ERegPreCriterioBusqueda.POR_NOMBRE_DE_PROPIETARIO.getCodigo().equals(
                    this.criterioBusqueda) ||
                     ERegPreCriterioBusqueda.POR_DOCUMENTO_DE_IDENTIDAD.getCodigo().equals(
                        this.criterioBusqueda)) {
                    this.pantallaAAbrirDesdeRegistrosPrediales = "consultarPorDatosPersonaWV";
                    this.formularioAActualizarDesdeRegistrosPrediales =
                        ":consultarPorDatosPersonaForm";
                }

                if (ERegPreCriterioBusqueda.POR_NOMBRE_DE_PROPIETARIO.getCodigo().equals(
                    this.criterioBusqueda)) {
                    this.activarSeccionPersonaJuridica = true;
                    this.activarSeccionPersonaNatural = true;
                }
            }

        }

    }

    /**
     * Método que se ejecuta cuando se cambia la selección en el tipo de búsqueda en la pantalla de
     * resoluciones
     *
     * @author leidy.gonzalez
     */
    public void cambiarTipoDeBusquedaResoluciones() {

        //Inicializa variables de las pantallas
        if (this.tipoDeBusquedaResolucionesDeConservacion != null &&
            !this.tipoDeBusquedaResolucionesDeConservacion.isEmpty()) {
            if (this.nuevoProductoCatastralDetalle != null) {
                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
            }

        }

    }

    /**
     * Accion ejecutada sobre el radio button asociado al tipo de persona
     *
     * @author javier.aponte
     */
    public void tipoPersonaSel() {
        this.nuevoProductoCatastralDetalle.setTipoIdentificacion(null);
        if (this.nuevoProductoCatastralDetalle.isPersonaJuridica()) {
            this.nuevoProductoCatastralDetalle.setTipoIdentificacion(EPersonaTipoIdentificacion.NIT.
                getCodigo());
        }
    }

    /**
     * Método encargado de asociar productos catastrales detalles a productos solicitados
     *
     * @author javier.aponte
     * @modified leidy.gonzalez Se agrega verificación de ingreso de comillas sencillas en la
     * lectura del archivo para ser eliminadas en la acrga de este.
     */
    public void asociarProductosCatastralesDetallesAProductos(
        ProductoCatastralDetalle nuevoProductoDetalle) {

        if (this.productosCatastralesDetallesAsociadasAProductos == null ||
            this.productosCatastralesDetallesAsociadasAProductos.size() == 0) {
            this.productosCatastralesDetallesAsociadasAProductos =
                new ArrayList<ProductoCatastralDetalle>();
        }

        nuevoProductoDetalle.setFechaLog(new Date());
        nuevoProductoDetalle.setUsuarioLog(this.usuario.getLogin());

        if (!this.productosCatastralesDetallesAsociadasAProductos.isEmpty()) {
            for (ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos) {
                //Validar que el número de identificación no se halla ingresado ya, en el caso que se ingresen datos
                //de persona
                if (nuevoProductoDetalle.getNumeroIdentificacion() != null &&
                     nuevoProductoDetalle.getNumeroIdentificacion().equals(pcd.
                        getNumeroIdentificacion())) {
                    this.addMensajeError(
                        "Este número de identificación ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                    return;
                } //Validar que el número predial no se halla ingresado ya, en el caso que se ingresen datos
                //de predio
                else if (nuevoProductoDetalle.getNumeroPredial() != null &&
                     nuevoProductoDetalle.getNumeroPredial().equals(pcd.getNumeroPredial())) {
                    this.addMensajeError(
                        "Este número predial ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                    return;
                }
                String comillas = " '";
                if (pcd.getPrimerApellido() != null && pcd.getPrimerApellido().equals(comillas)) {
                    pcd.setPrimerApellido(null);
                }

                if (pcd.getSegundoApellido() != null && pcd.getSegundoApellido().equals(comillas)) {
                    pcd.setSegundoApellido(null);
                }
                if (pcd.getPrimerNombre() != null && pcd.getPrimerNombre().equals(comillas)) {
                    pcd.setPrimerNombre(null);
                }
                if (pcd.getSegundoNombre() != null && pcd.getSegundoNombre().equals(comillas)) {
                    pcd.setSegundoNombre(null);
                }

            }
        }

        if (nuevoProductoDetalle.getNumeroIdentificacion() != null && nuevoProductoDetalle.
            getNumeroIdentificacion().equals(Constantes.CERO)) {
            this.addMensajeError(
                "El número de identificación con valor 0 no puede ser ingresado, por favor verifique los datos e intente nuevamente");
            return;
        }

        if (nuevoProductoDetalle.getNumeroPredialFinal() != null ||
             nuevoProductoDetalle.getNumeroPredialInicial() != null ||
             nuevoProductoDetalle.getNumeroPredial() != null ||
             nuevoProductoDetalle.getTipoIdentificacion() != null ||
             nuevoProductoDetalle.getNumeroIdentificacion() != null) {

            nuevoProductoDetalle.setProductoCatastral(this.productoCatastralSeleccionado);
            this.productosCatastralesDetallesAsociadasAProductos.add(nuevoProductoDetalle);

        }

        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

        inicializarDatosArchivoProductoMasivo();
    }

    /**
     * Asociar resoluciones a productos
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se agregan validaciones depentiendo del tipo de busqueda de la
     * resolución, se cargan campos Departamento y Municipio para ser visualizados en la tabla de
     * resoluciones asociadas a la solicitud.
     * @modified by leidy.gonzalez 19/08/2014 Se agregan validaciones para que la fecha y numero de
     * resolucion inicial no sean mayores a el final.
     */
    public void asociarResolucionesAProducto() {

        List<Tramite> resolucionesTramite = new ArrayList<Tramite>();
        String codigoMun;

        this.nuevoProductoCatastralDetalle.setFechaLog(new Date());
        this.nuevoProductoCatastralDetalle.setUsuarioLog(this.usuario.getLogin());

        if (this.selectedDepartamentoCodigo != null && !this.selectedDepartamentoCodigo.isEmpty() &&
             this.selectedMunicipioCodigo != null && !this.selectedMunicipioCodigo.isEmpty()) {
            this.nuevoProductoCatastralDetalle.
                setDepartamentoCodigo(this.selectedDepartamentoCodigo);
            this.nuevoProductoCatastralDetalle.setMunicipioCodigo(this.selectedMunicipioCodigo);
        }

        if (this.nuevoProductoCatastralDetalle.getDepartamentoCodigo() == null &&
             this.nuevoProductoCatastralDetalle.getMunicipioCodigo() == null) {

            if (this.selectedDepartamentoCodigo != null && this.selectedMunicipioCodigo != null) {
                this.nuevoProductoCatastralDetalle.setDepartamentoCodigo(
                    this.selectedDepartamentoCodigo);
                this.nuevoProductoCatastralDetalle.setMunicipioCodigo(this.selectedMunicipioCodigo.
                    substring(2, 5));
            }
        }

        //felipe.cadena::02-01-2017::validaciones delegacion
        if (nuevoProductoCatastralDetalle.getDepartamentoCodigo() == null ||
             nuevoProductoCatastralDetalle.getDepartamentoCodigo().isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_007.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_007.getMensajeTecnico());
            return;
        }
        if (nuevoProductoCatastralDetalle.getMunicipioCodigo() == null ||
             nuevoProductoCatastralDetalle.getMunicipioCodigo().isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_007.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_007.getMensajeTecnico());
            return;
        }

        if (nuevoProductoCatastralDetalle.getMunicipioCodigo().length() == 5) {
            codigoMun = (nuevoProductoCatastralDetalle.getMunicipioCodigo());
        } else {
            codigoMun = (nuevoProductoCatastralDetalle.getDepartamentoCodigo() +
                nuevoProductoCatastralDetalle.getMunicipioCodigo());
        }
        if (this.usuarioDelegado) {

            List<Municipio> municipiosList;

            boolean munExiste = false;
            municipiosList = this.getGeneralesService().buscarMunicipioPorIdOficina(
                this.usuario.getCodigoTerritorial());

            for (Municipio mun : municipiosList) {
                if (mun.getCodigo().equals(codigoMun)) {
                    munExiste = true;
                    break;
                }
            }

            if (!munExiste) {
                this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_009.toString());
                LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_009.getMensajeTecnico());
                return;
            }

        } else {
            for (Municipio mun : this.municipiosDelegados) {
                if (mun.getCodigo().equals(codigoMun)) {
                    this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_010.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_010.getMensajeTecnico());
                    return;
                }
            }
        }

        boolean hayDatosFaltantes = true;

        for (ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos) {
            //Validar que el número de resolución inicial no se halla ingresado ya
            if (this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial() != null &&
                 this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial().equals(pcd.
                    getNumeroResolucionInicial())) {
                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                this.addMensajeError(
                    "Este número de resolución ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                return;
            } //Validar que la fecha de resolución inicial no se halla ingresado ya
            else if (this.nuevoProductoCatastralDetalle.getFechaResolucionInicial() != null &&
                 this.nuevoProductoCatastralDetalle.getFechaResolucionInicial().equals(pcd.
                    getFechaResolucionInicial())) {
                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                this.addMensajeError(
                    "Esta fecha de resolución ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                return;
            }
        }

        //Por número de resolución individual
        if (this.tipoDeBusquedaResolucionesDeConservacion != null) {
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("1")) {
                if (this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial() != null &&
                    !this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial().isEmpty()) {
                    hayDatosFaltantes = false;

                    //consultar resolucion generada
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucion(this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas para el numero de resolucion ingresado");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango de fechas establecido");

                        if (this.nuevoProductoCatastralDetalle.getDepartamentoCodigo() != null &&
                             this.nuevoProductoCatastralDetalle.getMunicipioCodigo() != null) {

                            this.nuevoProductoCatastralDetalle.setMunicipioCodigo(
                                this.nuevoProductoCatastralDetalle.getDepartamentoCodigo() +
                                this.nuevoProductoCatastralDetalle.getMunicipioCodigo());
                        }
                    }

                }
            }
            //Por fecha de resolución individual
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("2")) {
                if (this.nuevoProductoCatastralDetalle.getFechaResolucionInicial() != null) {
                    hayDatosFaltantes = false;

                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucion(this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas para esta fecha ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en la fecha ingresada");
                    }
                }
            }
            //Por rango de número de resolución
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("3")) {
                if (this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial() != null &&
                    this.nuevoProductoCatastralDetalle.getNumeroResolucionFinal() != null) {

                    hayDatosFaltantes = false;

                    //comparar que año de resolucion final no sea mayor a año de resolucion inicial 
                    /* if(this.nuevoProductoCatastralDetalle.getAnioFinal() >
                     * this.nuevoProductoCatastralDetalle.getAnioInicial()){
                     * this.addMensajeError("Año de resolución inicial debe ser menor o igual a año
                     * de resolución final"); return;
				} */
                    int comparaNumeroResolucion = this.nuevoProductoCatastralDetalle.
                        getNumeroResolucionInicial().compareTo(this.nuevoProductoCatastralDetalle.
                            getNumeroResolucionFinal());

                    if (comparaNumeroResolucion > 0) {
                        this.addMensajeError(
                            "El número de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    //consultar resoluciones que fueron generadas
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucionFinalEInicial(
                            this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas para este rango de resoluciones ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango establecido");
                    }
                }
            }
            //Por rango de fecha de resolución
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("4")) {
                if (this.nuevoProductoCatastralDetalle.getFechaResolucionInicial() != null &&
                    this.nuevoProductoCatastralDetalle.getFechaResolucionFinal() != null) {

                    hayDatosFaltantes = false;

                    if (this.nuevoProductoCatastralDetalle.getFechaResolucionFinal().before(
                        this.nuevoProductoCatastralDetalle.getFechaResolucionInicial())) {
                        this.addMensajeError(
                            "La fecha de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }
                    //consultar resoluciones que fueron generadas
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucionFinalEInicial(
                            this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas para este rango de fechas ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango de fechas establecido");
                    }
                }

            }

            //Por número de resolución individual de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("5")) {
                if (this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial() != null &&
                    !this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial().isEmpty()) {
                    hayDatosFaltantes = false;
                    this.nuevoProductoCatastralDetalle.setPcdFinAprobado(ESiNo.SI.getCodigo());

                    //consultar resoluciones que fueron generadas que se encuentran en aplicar cambios por numero de resolucion
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucionAplicaCambios(
                            this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para el numero de resoluciòn ingresado");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango de fechas establecido");

                        if (this.nuevoProductoCatastralDetalle.getDepartamentoCodigo() != null &&
                             this.nuevoProductoCatastralDetalle.getMunicipioCodigo() != null) {
                            this.nuevoProductoCatastralDetalle.setMunicipioCodigo(
                                this.nuevoProductoCatastralDetalle.getDepartamentoCodigo() +
                                this.nuevoProductoCatastralDetalle.getMunicipioCodigo());
                        }
                    }
                }

            }
            //Por fecha de resolución individual de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("6")) {
                if (this.nuevoProductoCatastralDetalle.getFechaResolucionInicial() != null) {
                    hayDatosFaltantes = false;
                    this.nuevoProductoCatastralDetalle.setPcdFinAprobado(ESiNo.SI.getCodigo());

                    //consultar resoluciones que fueron generadas en aplicar cambios por fecha de resolucion
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucionAplicaCambios(
                            this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para la fecha de resoluciòn ingresado");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en la fecha ingresada para aplicar cambios");
                    }
                }
            }
            //Por rango de número de resolución de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("7")) {
                if (this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial() != null &&
                     this.nuevoProductoCatastralDetalle.getNumeroResolucionFinal() != null) {

                    hayDatosFaltantes = false;
                    this.nuevoProductoCatastralDetalle.setPcdFinAprobado(ESiNo.SI.getCodigo());

                    // comparar que año de resolucion final no sea mayor a año
                    // de resolucion inicial
                    /*
                     * if(this.nuevoProductoCatastralDetalle.getAnioFinal() >
                     * this.nuevoProductoCatastralDetalle.getAnioInicial()){ this.addMensajeError(
                     * "Año de resolución inicial debe ser menor o igual a año de resolución final"
                     * ); return; }
                     */
                    int comparaNumeroResolucion = this.nuevoProductoCatastralDetalle.
                        getNumeroResolucionInicial().compareTo(
                            this.nuevoProductoCatastralDetalle.getNumeroResolucionFinal());

                    if (comparaNumeroResolucion > 0) {
                        this.addMensajeError(
                            "El número de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    // consultar resoluciones que fueron generadas en el rango de resoluciones ingresadas y se encuentran en aplicar cambios
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
                            this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para este rango de resoluciones ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango establecido para aplicar cambios");
                    }
                }
            }
            //Por rango de fecha de resolución de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("8")) {
                if (this.nuevoProductoCatastralDetalle.getFechaResolucionInicial() != null &&
                    this.nuevoProductoCatastralDetalle.getFechaResolucionFinal() != null) {

                    hayDatosFaltantes = false;
                    this.nuevoProductoCatastralDetalle.setPcdFinAprobado(ESiNo.SI.getCodigo());

                    if (this.nuevoProductoCatastralDetalle.getFechaResolucionFinal().before(
                        this.nuevoProductoCatastralDetalle.getFechaResolucionInicial())) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "La fecha de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    // consultar resoluciones que fueron generadas en el rango de fechas ingresadas y se encuentran en aplicar cambios
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
                            this.nuevoProductoCatastralDetalle);

                    if (resolucionesTramite.size() == 0) {
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para este rango de fechas ingresados");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango de fechas establecido para aplicar cambios");
                    }
                }

            }

        } else {
            this.addMensajeError("Debe seleccionar un criterio de busqueda");
            return;
        }

        if (hayDatosFaltantes) {
            this.addMensajeError("Por favor ingrese todos los datos e intente nuevamente");
            return;
        } else {
            this.nuevoProductoCatastralDetalle.setProductoCatastral(
                this.productoCatastralSeleccionado);
            this.productosCatastralesDetallesAsociadasAProductos.add(
                this.nuevoProductoCatastralDetalle);
        }

        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
        this.selectedDepartamentoCodigo = null;
        this.selectedMunicipioCodigo = null;

    }

    /**
     * Método para validar y asociar el número predial al producto, luego de validar el número
     * predial llama al método de asociar productos catastrales detalles a productos
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez :: 12534 :: 12-05-2015 :: Se valida que para insertar un predio
     * al producto catastral detalle que va a generar R1 y R2 este predio no tenga los siguientes
     * valores en su numero predial:condicion de propiedad 8 o 9, edificio 00, piso 00, unidad 000 y
     * que no se encuentre CANCELADO. Las antriores condiciones para tipoDeSeleccionConsultaPredio
     * individual y por rangos.
     */
    public void asociarNumeroPredialAProducto() {

        Predio predio;
        Predio predioInicial;
        Predio predioFinal;

        if ((this.tipoDeSeleccionConsultaPredio != null && !this.tipoDeSeleccionConsultaPredio.
            trim().isEmpty() &&
             "1".equals(this.tipoDeSeleccionConsultaPredio)) ||
             this.tipoDeSeleccionConsultaPredio == null) {

            String numeroPredial = this.numeroPredialPartes.assembleNumeroPredialFromSegments();

            if (numeroPredial == null || numeroPredial.isEmpty()) {

                this.addMensajeError("Debe ingresar el número predial completo");
                return;

            }

            if ((this.numeroPredialPartes.getCondicionPropiedad().equals("8") ||
                this.numeroPredialPartes.getCondicionPropiedad().equals("9")) &&
                 this.numeroPredialPartes.getEdificio().equals("00") &&
                 this.numeroPredialPartes.getPiso().equals("00") &&
                 this.numeroPredialPartes.getUnidad().equals("0000")) {

                this.addMensajeError(
                    "El nùmero predial ingresado es un PH o Condominio por lo tanto no puede generar R1 y/o R2");
                return;
            } else {

                predio = this.getConservacionService().getPredioByNumeroPredial(
                    numeroPredial);
                if (predio == null) {
                    this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_004.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_004.getMensajeTecnico());
                    this.limpiarCamposPredioPartes();
                    return;
                }
                if (predio.getEstado().equals(EPredioEstado.CANCELADO.getCodigo())) {

                    this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_003.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_003.getMensajeTecnico());
                    return;

                } else {
                    this.nuevoProductoCatastralDetalle.setNumeroPredial(numeroPredial);
                }
            }

        } else if (this.tipoDeSeleccionConsultaPredio != null &&
            !this.tipoDeSeleccionConsultaPredio.trim().isEmpty() &&
             "2".equals(this.tipoDeSeleccionConsultaPredio)) {

            String numeroPredialInicial = this.numeroPredialPartesInicial.
                assembleNumeroPredialFromSegments();

            if (numeroPredialInicial == null || numeroPredialInicial.isEmpty()) {

                this.addMensajeError("Debe ingresar el número predial inicial completo");
                return;

            }

            String numeroPredialFinal = this.numeroPredialPartesFinal.
                assembleNumeroPredialFromSegments();

            if (numeroPredialFinal == null || numeroPredialFinal.isEmpty()) {

                this.addMensajeError("Debe ingresar el número predial final completo");
                return;

            }

            Pattern patronRegistrosPrediales = Pattern.compile("32|33|1598|1599|36|37|1595|38");
            Matcher matRegistrosPrediales = patronRegistrosPrediales.matcher(
                this.productoCatastralSeleccionado.getProducto().getCodigo());

            if (matRegistrosPrediales.matches()) {
                if ("PDF".equalsIgnoreCase(this.productoCatastralSeleccionado.getFormato())) {

                    Object[] resultado = new Object[2];
                    List<Object> parametros = new ArrayList<Object>();

                    parametros.add(numeroPredialInicial);
                    parametros.add(numeroPredialFinal);

                    resultado = this.getGeneralesService().contarRegistrosPrediales(parametros);

                    //Cantidad de registros por propietario
                    if ((Integer.valueOf(String.valueOf(resultado[1]))).intValue() > 10000) {
                        this.addMensajeError(
                            "El rango devuelve demasiados registros para generar el PDF, por favor" +
                            " cambie los criterios de búsqueda e intente nuevamente");
                        return;
                    }
                }
            }

            if ((this.numeroPredialPartesFinal.getCondicionPropiedad().equals("8") ||
                this.numeroPredialPartesFinal.getCondicionPropiedad().equals("9")) &&
                 this.numeroPredialPartesFinal.getEdificio().equals("00") &&
                 this.numeroPredialPartesFinal.getPiso().equals("00") &&
                 this.numeroPredialPartesFinal.getUnidad().equals("0000")) {

                this.addMensajeError(
                    "El nùmero predial final es un PH o Condominio por lo tanto no puede generar R1 y/o R2");
                return;
            }

            if ((this.numeroPredialPartesInicial.getCondicionPropiedad().equals("8") ||
                this.numeroPredialPartesInicial.getCondicionPropiedad().equals("9")) &&
                 this.numeroPredialPartesInicial.getEdificio().equals("00") &&
                 this.numeroPredialPartesInicial.getPiso().equals("00") &&
                 this.numeroPredialPartesInicial.getUnidad().equals("0000")) {

                this.addMensajeError(
                    "El nùmero predial inicial es un PH o Condominio por lo tanto no puede generar R1 y/o R2");
                return;
            }

            /*
             * @modified by hector.arias :: 07/06/2017:: #20009 :: Se completa el codigo del
             * departamento y municipio cuando no se selecciona ningún nivel de información para
             * generar registros 1 y 2
             */
            if (this.nivelDeInformacion == null || !this.nivelDeInformacion.isEmpty()) {
                this.numeroPredialPartes.setDepartamentoCodigo(selectedDepartamento.getCodigo());
                this.numeroPredialPartes.setMunicipioCodigo(selectedMunicipio.getCodigo().substring(
                    2, 5));
            }

            String numPredialFinalZona = numeroPredialPartesFinal.getTipoAvaluo();
            String numPredialInicialZona = numeroPredialPartesInicial.getTipoAvaluo();
            int resultZona = numPredialFinalZona.compareTo(numPredialInicialZona);

            String numPredialFinalSector = numeroPredialPartesFinal.getSectorCodigo();
            String numPredialInicialSector = numeroPredialPartesInicial.getSectorCodigo();
            int resultSector = numPredialFinalSector.compareTo(numPredialInicialSector);

            String numPredialFinalComuna = numeroPredialPartesFinal.getComunaCodigo();
            String numPredialInicialComuna = numeroPredialPartesInicial.getComunaCodigo();
            int resultComuna = numPredialFinalComuna.compareTo(numPredialInicialComuna);

            String numPredialFinalBarrio = numeroPredialPartesFinal.getBarrioCodigo();
            String numPredialInicialBarrio = numeroPredialPartesInicial.getBarrioCodigo();
            int resultBarrio = numPredialFinalBarrio.compareTo(numPredialInicialBarrio);

            String numPredialFinalManzana = numeroPredialPartesFinal.getManzanaVeredaCodigo();
            String numPredialInicialManzana = numeroPredialPartesInicial.getManzanaVeredaCodigo();
            int resultManzana = numPredialFinalManzana.compareTo(numPredialInicialManzana);

            String numPredialFinalPredio = numeroPredialPartesFinal.getPredio();
            String numPredialInicialPredio = numeroPredialPartesInicial.getPredio();
            int resultPredio = numPredialFinalPredio.compareTo(numPredialInicialPredio);

            String numPredialFinalCondicionPropiedad = numeroPredialPartesFinal.
                getCondicionPropiedad();
            String numPredialInicialCondicionPropiedad = numeroPredialPartesInicial.
                getCondicionPropiedad();
            int resultPropiedad = numPredialFinalCondicionPropiedad.compareTo(
                numPredialInicialCondicionPropiedad);

            String numPredialFinalEdificio = numeroPredialPartesFinal.getEdificio();
            String numPredialInicialEdificio = numeroPredialPartesInicial.getEdificio();
            int resultEdificio = numPredialFinalEdificio.compareTo(numPredialInicialEdificio);

            String numPredialFinalPiso = numeroPredialPartesFinal.getPiso();
            String numPredialInicialPiso = numeroPredialPartesInicial.getPiso();
            int resultPiso = numPredialFinalPiso.compareTo(numPredialInicialPiso);

            String numPredialFinalUnidad = numeroPredialPartesFinal.getUnidad();
            String numPredialInicialUnidad = numeroPredialPartesInicial.getUnidad();
            int resultUnidad = numPredialFinalUnidad.compareTo(numPredialInicialUnidad);

            if (resultZona >= 0 &&
                 resultSector >= 0 &&
                 resultComuna >= 0 &&
                 resultBarrio >= 0 &&
                 resultManzana >= 0 &&
                 resultPredio >= 0 &&
                 resultPropiedad >= 0 &&
                 resultEdificio >= 0 &&
                 resultPiso >= 0 &&
                 resultUnidad >= 0) {

                predioInicial = this.getConservacionService().getPredioByNumeroPredial(
                    numeroPredialInicial);

                if (predioInicial != null && predioInicial.getEstado().equals(
                    EPredioEstado.CANCELADO.getCodigo())) {

                    this.addMensajeError(
                        "El nùmero predial inicial ingresado se encuentra cancelado, por lo tal no se puede generar R1 y/o R2");
                    return;

                } else {

                    this.nuevoProductoCatastralDetalle.setNumeroPredial(numeroPredialInicial);
                    this.nuevoProductoCatastralDetalle.setNumeroPredialInicial(numeroPredialInicial);

                }

                predioFinal = this.getConservacionService().getPredioByNumeroPredial(
                    numeroPredialFinal);

                if (predioFinal != null && predioFinal.getEstado().equals(EPredioEstado.CANCELADO.
                    getCodigo())) {

                    this.addMensajeError(
                        "El nùmero predial final ingresado se encuentra cancelado, por lo tal no se puede generar R1 y/o R2");
                    return;

                } else {
                    this.nuevoProductoCatastralDetalle.setNumeroPredialFinal(numeroPredialFinal);
                }

                List<String> rangoNPrediales = this.getGeneralesService().
                    obtenerNumerosPredialesEntreRangoPredial(numeroPredialInicial,
                        numeroPredialFinal, false);

                for (String string : rangoNPrediales) {
                    ProductoCatastralDetalle pcd = new ProductoCatastralDetalle();
                    pcd.setNumeroPredial(string);
                    this.asociarProductosCatastralesDetallesAProductos(pcd);
                }
                this.limpiarCamposPredioPartes();
                return;

            } else {

                this.addMensajeError(
                    "El número de predio inicial no debe ser mayor al número de predio final");
            }

        } else if (this.tipoDeSeleccionConsultaPredio != null &&
            !this.tipoDeSeleccionConsultaPredio.trim().isEmpty() &&
             "3".equals(this.tipoDeSeleccionConsultaPredio)) {

        }

        this.asociarProductosCatastralesDetallesAProductos(this.nuevoProductoCatastralDetalle);

        this.limpiarCamposPredioPartes();

    }

    /**
     * Metodo encargado de la inicialización de datos de socumento de archivo masivo
     *
     * @author leidy.gonzalez
     */
    public void inicializarDatosArchivoProductoMasivo() {
        this.documentoSoporteProducto = new Documento();
        this.nombreDocumentoProductoMasivo = null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo validador de carga de archivo en masa para predio
     *
     * @author leidy.gonzalez
     * @modified by leidy.gonzalez 30/07/2014 Se ingresan return para cuando exista un error no siga
     * corriendo el programa
     */
    public void validadorArchivoPredio(FileUploadEvent eventoMasa) {

        FileOutputStream fileOutputStream = null;
        this.documentoTemporalProductoMasivo = new Documento();
        this.nombreDocumentoProductoMasivo = eventoMasa.getFile().getFileName();

        this.documentoTemporalProductoMasivo
            .setArchivo(this.nombreDocumentoProductoMasivo);
        try {
            this.archivoMasivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                this.nombreDocumentoProductoMasivo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoMasivoResultado);

            byte[] buffer = new byte[Math.round(eventoMasa.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoMasa.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            this.lecturaArchivoMasivoPredio(eventoMasa);

        } catch (IOException e) {
            LOGGER.error("ERROR: " + e, e);
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }

    }

    /**
     * Verifica que el numero predial sea valido en longitud y este relacionado al municipio y
     * departamenteo seleccionado
     *
     * @param numero
     * @return
     */
    private boolean validarNumeroPredial(String numero) {
        boolean valido = true;
        this.seleccionDepartamentoYMunicipio();

        if (numero.length() != 30) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_005.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_005.getMensajeTecnico());
            return false;
        }

        if (this.selectedMunicipioCodigo == null || this.selectedMunicipioCodigo.isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_007.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_007.getMensajeTecnico());
            return false;
        }
        if (!numero.startsWith(this.selectedMunicipioCodigo)) {
            this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_006.toString());
            LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_006.getMensajeTecnico());
            return false;
        }

        return valido;

    }

    /**
     * Método de lectura del archivo de predio masivo
     *
     * @author leidy.gonzalez
     * @modified by leidy.gonzalez 30/07/2014 Se ingresan return para cuando exista un error no siga
     * corriendo el programa
     */
    @SuppressWarnings("rawtypes")
    private void lecturaArchivoMasivoPredio(FileUploadEvent eventoMasa) {

        String sCadena;
        List<String> numerosCargados = new ArrayList<String>();

        try {

            if (eventoMasa.getFile().getContentType().equals(EArchivoAnexoFormato.ARCHIVO_PLANO.
                getValor()) ||
                 eventoMasa.getFile().getFileName().endsWith(EArchivoAnexoFormato.ARCHIVO_PLANO.
                    getExtension())) {

                BufferedReader bf = new BufferedReader(new FileReader(
                    this.archivoMasivoResultado));

                int contadorErrores = 0;

                while ((sCadena = bf.readLine()) != null) {

                    this.nuevoProductoCatastralDetalle.setFechaLog(new Date());
                    this.nuevoProductoCatastralDetalle.setUsuarioLog(this.usuario.getLogin());

                    if (!this.validarNumeroPredial(sCadena)) {
                        return;
                    }
                    numerosCargados.add(sCadena);
                    this.nuevoProductoCatastralDetalle.setNumeroPredial(sCadena);

                    this.productosCatastralesDetallesAsociadasAProductos.add(
                        nuevoProductoCatastralDetalle);
                    this.nuevoProductoCatastralDetalle
                        .setProductoCatastral(this.productoCatastralSeleccionado);
                    this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

                }

                if (!this.getConservacionService().validarExistenciaYActivosDePredios(
                    numerosCargados)) {
                    this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_008.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_008.getMensajeTecnico());
                    this.productosCatastralesDetallesAsociadasAProductos.clear();
                    return;
                }

                this.documentoArchivoAnexo = new DocumentoArchivoAnexo();

                this.documentoArchivoAnexo.
                    setFormato(EArchivoAnexoFormato.ARCHIVO_PLANO.getCodigo());
                this.documentoArchivoAnexo.setArchivo(eventoMasa.getFile()
                    .getFileName());

                if (contadorErrores == 0) {
                    String mensaje = "El documento masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                }
            } else if (eventoMasa.getFile().getContentType().equals(
                EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                 eventoMasa.getFile().getContentType().equals(
                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX.getValor()) ||
                 eventoMasa.getFile().getFileName().endsWith(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.
                    getExtension()) ||
                 eventoMasa.getFile().getFileName().endsWith(
                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX.getExtension())) {

                FileInputStream fis = new FileInputStream(
                    this.archivoMasivoResultado);
                XSSFWorkbook workbook = new XSSFWorkbook(fis);
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rowsX = sheet.rowIterator();
                Object numeroPredial[] = null;
                int contadorErrores = 0;

                while (rowsX.hasNext()) {
                    XSSFRow row = (XSSFRow) rowsX.next();
                    Iterator cells = row.cellIterator();
                    numeroPredial = new Object[35];
                    int c = 0;

                    while (cells.hasNext()) {

                        XSSFCell cell = (XSSFCell) cells.next();

                        numeroPredial[c] = cell;

                        c++;

                    }

                    String numero = numeroPredial[0].toString();

                    if (!this.validarNumeroPredial(numero)) {
                        return;
                    }
                    numerosCargados.add(numero);
                    this.nuevoProductoCatastralDetalle.setFechaLog(new Date());
                    this.nuevoProductoCatastralDetalle.setUsuarioLog(this.usuario.getLogin());
                    this.nuevoProductoCatastralDetalle.setNumeroPredial(numero);

                    this.documentoArchivoAnexo = new DocumentoArchivoAnexo();
                    this.documentoArchivoAnexo.setFormato(eventoMasa.getFile().getContentType());

                    this.productosCatastralesDetallesAsociadasAProductos.add(
                        nuevoProductoCatastralDetalle);
                    this.nuevoProductoCatastralDetalle
                        .setProductoCatastral(this.productoCatastralSeleccionado);
                    this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

                }

                if (!this.getConservacionService().validarExistenciaYActivosDePredios(
                    numerosCargados)) {
                    this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_008.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_008.getMensajeTecnico());
                    this.productosCatastralesDetallesAsociadasAProductos.clear();
                    return;
                }

                if (eventoMasa.getFile().getContentType().equals(
                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                     eventoMasa.getFile().getFileName().endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getExtension())) {
                    this.documentoArchivoAnexo.setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.
                        getCodigo());
                } else {
                    this.documentoArchivoAnexo.setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX.
                        getCodigo());
                }
                this.documentoArchivoAnexo.setArchivo(eventoMasa.getFile().getFileName());

                if (contadorErrores == 0) {
                    String mensaje = "El documento masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                }
            }
        } catch (IOException e) {
            String mensaje = "El archivo no contiene la estructura adecuada";
            LOGGER.error(mensaje + " " + e, e);
            this.nombreDocumentoProductoMasivo = null;

            this.addMensajeError(mensaje);
            return;
        } catch (Exception e) {
            String mensaje = "Ocurrio un error al leer el archivo!";
            LOGGER.error(mensaje + " " + e, e);
            this.nombreDocumentoProductoMasivo = null;

            this.addMensajeError(mensaje);
            return;
        }

        guardarDocumentoSoporteProducto();

    }

    /**
     * Metodo validador de carga de archivo en masa para persona
     *
     * @author leidy.gonzalez
     */
    public void validadorArchivoPersona(FileUploadEvent eventoMasa) {

        FileOutputStream fileOutputStream = null;
        this.documentoTemporalProductoMasivo = new Documento();
        this.nombreDocumentoProductoMasivo = eventoMasa.getFile().getFileName();
        String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

        this.documentoTemporalProductoMasivo
            .setArchivo(this.nombreDocumentoProductoMasivo);
        try {
            this.archivoMasivoResultado = new File(
                directorioTemporal,
                this.nombreDocumentoProductoMasivo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoMasivoResultado);

            byte[] buffer = new byte[Math.round(eventoMasa.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoMasa.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            lecturaArchivoMasivoPersona(eventoMasa);

        } catch (IOException e) {
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.LOGGER.error(mensaje + " " + e, e);
            this.addMensajeError(mensaje);
        }

    }

    /**
     * Método de lectura del archivo de personas masivo
     *
     * @author leidy.gonzalez modified by leidy.gonzalez Se crean validaciones de longitud para cada
     * uno de los campos ingresados en el archivo masivo. Cada vez que se genere un error en la
     * lectura del archivo se inicializa la lista productosCatastralesDetallesAsociadasAProductos y
     * se remueve lo que haya sido leido hasta el momento.
     */
    @SuppressWarnings("rawtypes")
    private void lecturaArchivoMasivoPersona(FileUploadEvent eventoMasa) {

        String sCadena;
        String separador;
        boolean formatoCorrecto;

        try {

            if (eventoMasa.getFile().getContentType().equals(EArchivoAnexoFormato.ARCHIVO_PLANO.
                getValor()) ||
                 eventoMasa.getFile().getFileName().endsWith(EArchivoAnexoFormato.ARCHIVO_PLANO.
                    getExtension())) {

                BufferedReader bf = new BufferedReader(new FileReader(this.archivoMasivoResultado));
                separador = Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO;
                int contadorErrores = 0;
                String partes[] = null;

                while ((sCadena = bf.readLine()) != null) {

                    partes = sCadena.split(separador);

                    formatoCorrecto = validarEstructuraArchivoPersonaMasivo(partes);

                    if (formatoCorrecto == true) {

                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

                        this.nuevoProductoCatastralDetalle.setFechaLog(new Date());
                        this.nuevoProductoCatastralDetalle.setUsuarioLog(this.usuario.getLogin());

                        if (partes[0] != null && partes[0].length() <= 30) {
                            this.nuevoProductoCatastralDetalle.setTipoPersona(partes[0]);
                        } else {
                            contadorErrores++;
                            String mensaje = "El tipo de persona" +
                                 partes[0] +
                                 " no debe tener una longitud mayor a 30 campos";
                            this.addMensajeError(mensaje);
                            this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                this.productosCatastralesDetallesAsociadasAProductos);
                            this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                            return;
                        }

                        if (partes[1] != null && partes[1].length() <= 30) {
                            this.nuevoProductoCatastralDetalle.setTipoIdentificacion(partes[1]);
                        } else {
                            contadorErrores++;
                            String mensaje = "El tipo de identificación" +
                                 partes[0] +
                                 " no debe tener una longitud mayor a 30 campos";
                            this.addMensajeError(mensaje);
                            this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                this.productosCatastralesDetallesAsociadasAProductos);
                            this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                            return;
                        }

                        if (partes[2] != null && partes[2].length() <= 50) {
                            this.nuevoProductoCatastralDetalle.setNumeroIdentificacion(partes[2]);
                        } else {
                            contadorErrores++;
                            String mensaje =
                                "Para la persona identificada con número identificación" +
                                 partes[0] +
                                 " no debe tener una longitud mayor a 50 campos";
                            this.addMensajeError(mensaje);
                            this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                this.productosCatastralesDetallesAsociadasAProductos);
                            this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                            return;
                        }

                        if (this.nuevoProductoCatastralDetalle.getTipoPersona().equals(
                            EPersonaTipoPersona.JURIDICA.toString())) {

                            if (partes.length == 6) {

                                if (partes[3] != null && partes[3].length() <= 2) {
                                    this.nuevoProductoCatastralDetalle.setDigitoVerificacion(
                                        partes[3]);
                                } else {
                                    contadorErrores++;
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                        
                                        " El digito de verificación no debe tener una longitud mayor a 2 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }

                                if (partes[4] != null && partes[4].length() <= 100) {
                                    this.nuevoProductoCatastralDetalle.setRazonSocial(partes[4]);
                                } else {
                                    contadorErrores++;
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                        
                                        " La razón social no debe tener una longitud mayor a 100 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }

                                if (partes[5] != null && partes[5].length() <= 50) {
                                    this.nuevoProductoCatastralDetalle.setSigla(partes[5]);
                                } else {
                                    contadorErrores++;
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                         " La sigla no debe tener una longitud mayor a 50 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El archivo asociado no contiene el formato correcto, ninguno de los campos ingresados debe ser nulo";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        if (this.nuevoProductoCatastralDetalle.getTipoPersona().equals(
                            EPersonaTipoPersona.NATURAL.toString())) {

                            if (partes.length == 7) {

                                if (partes[3] != null && partes[3].length() <= 100) {
                                    this.nuevoProductoCatastralDetalle.setPrimerApellido(partes[3]);
                                } else {
                                    contadorErrores++;
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                        
                                        " El primer apellido no debe tener una longitud mayor a 100 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }

                                if (partes[4] != null && partes[4].length() <= 100) {
                                    this.nuevoProductoCatastralDetalle.setSegundoApellido(partes[4]);
                                } else {
                                    contadorErrores++;
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                        
                                        " El segundo apellido no debe tener una longitud mayor a 100 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }

                                if (partes[5] != null && partes[5].length() <= 100) {
                                    this.nuevoProductoCatastralDetalle.setPrimerNombre(partes[5]);
                                } else {
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                        
                                        " El primer nombre no debe tener una longitud mayor a 100 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }

                                if (partes[6] != null && partes[6].length() <= 100) {
                                    this.nuevoProductoCatastralDetalle.setSegundoNombre(partes[6]);
                                } else {
                                    String mensaje =
                                        "Para la persona identificada con número identificación " +
                                         partes[2] +
                                        
                                        " El segundo nombre no debe tener una longitud mayor a 100 campos";
                                    this.addMensajeError(mensaje);
                                    this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                        this.productosCatastralesDetallesAsociadasAProductos);
                                    this.nuevoProductoCatastralDetalle =
                                        new ProductoCatastralDetalle();
                                    return;
                                }
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El archivo asociado no contiene el formato correcto, ninguno de los campos ingresados debe ser nulo";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        this.documentoArchivoAnexo = new DocumentoArchivoAnexo();
                        this.documentoArchivoAnexo.setFormato(EArchivoAnexoFormato.ARCHIVO_PLANO.
                            getCodigo());
                        this.documentoArchivoAnexo.setArchivo(eventoMasa.getFile().getFileName());

                        this.productosCatastralesDetallesAsociadasAProductos.add(
                            this.nuevoProductoCatastralDetalle);
                        this.nuevoProductoCatastralDetalle.setProductoCatastral(
                            this.productoCatastralSeleccionado);

                    } else {

                        contadorErrores++;
                        String mensaje = "El archivo asociado " +
                             partes[0] +
                             " no contiene el formato adecuado." +
                             " Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                            this.productosCatastralesDetallesAsociadasAProductos);
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        return;
                    }
                    this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                }

                if (contadorErrores == 0) {
                    String mensaje = "El documento masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                }

            } else if (eventoMasa.getFile().getContentType().equals(
                EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                 eventoMasa.getFile().getContentType().equals(
                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX.getValor()) ||
                 eventoMasa.getFile().getFileName().endsWith(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.
                    getExtension()) ||
                 eventoMasa.getFile().getFileName().endsWith(
                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX.getExtension())) {

                FileInputStream fis = new FileInputStream(this.archivoMasivoResultado);
                XSSFWorkbook workbook = new XSSFWorkbook(fis);
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rowsX = sheet.rowIterator();
                int contadorErrores = 0;
                Object partes[] = null;

                while (rowsX.hasNext()) {
                    XSSFRow row = (XSSFRow) rowsX.next();
                    Iterator cells = row.cellIterator();
                    int c = 0;
                    partes = new Object[7];

                    while (cells.hasNext()) {

                        XSSFCell cell = (XSSFCell) cells.next();

                        partes[c] = cell;
                        c++;

                    }
                    this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                    this.nuevoProductoCatastralDetalle.setFechaLog(new Date());
                    this.nuevoProductoCatastralDetalle.setUsuarioLog(this.usuario.getLogin());

                    String tipoPersona = partes[0].toString();

                    if (tipoPersona != null && tipoPersona.length() <= 30) {

                        this.nuevoProductoCatastralDetalle.setTipoPersona(tipoPersona);
                    } else {
                        contadorErrores++;
                        String mensaje = "El tipo de persona" +
                             partes[0] +
                             " no debe tener una longitud mayor a 30 campos";
                        this.addMensajeError(mensaje);
                        this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                            this.productosCatastralesDetallesAsociadasAProductos);
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        return;
                    }

                    String tipoIdentificacion = partes[1].toString();

                    if (tipoIdentificacion != null && tipoIdentificacion.length() <= 30) {

                        this.nuevoProductoCatastralDetalle.setTipoIdentificacion(tipoIdentificacion);
                    } else {
                        contadorErrores++;
                        String mensaje = "El tipo de identificación" +
                             partes[1] +
                             "no debe tener una longitud mayor a 30";
                        this.addMensajeError(mensaje);
                        this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                            this.productosCatastralesDetallesAsociadasAProductos);
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        return;
                    }

                    String numeroIdentificacion = partes[2].toString();

                    if (numeroIdentificacion != null && numeroIdentificacion.length() <= 50) {

                        this.nuevoProductoCatastralDetalle.setNumeroIdentificacion(
                            numeroIdentificacion);
                    } else {
                        contadorErrores++;
                        String mensaje = "Para la persona identificada con número identificación " +
                             partes[2] +
                             " este campo no debe tener una longitud mayor a 50 campos";
                        this.addMensajeError(mensaje);
                        this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                            this.productosCatastralesDetallesAsociadasAProductos);
                        this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                        return;
                    }

                    if (this.nuevoProductoCatastralDetalle.getTipoPersona().equals(
                        EPersonaTipoPersona.JURIDICA.toString())) {

                        if (partes[3] != null) {
                            if (partes[3].toString().length() <= 4) {
                                String digitoVerificacion = partes[3].toString();

                                this.nuevoProductoCatastralDetalle.setDigitoVerificacion(
                                    digitoVerificacion);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El digito de verificación no debe tener una longitud mayor a 4 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        if (partes[4] != null) {
                            if (partes[4].toString().length() <= 100) {
                                String razonSocial = partes[4].toString();
                                this.nuevoProductoCatastralDetalle.setRazonSocial(razonSocial);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                     "La razon social no debe tener una longitud mayor a 100 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        if (partes[5] != null) {
                            if (partes[5].toString().length() <= 50) {
                                String sigla = partes[5].toString();

                                this.nuevoProductoCatastralDetalle.setSigla(sigla);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                     " La sigla no debe tener una longitud mayor a 50 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }
                    }

                    if (this.nuevoProductoCatastralDetalle.getTipoPersona().equals(
                        EPersonaTipoPersona.NATURAL.toString())) {

                        if (partes[3] != null) {
                            if (partes[3].toString().length() <= 100) {
                                String primerApellido = partes[3].toString();
                                this.nuevoProductoCatastralDetalle
                                    .setPrimerApellido(primerApellido);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El primer apellido no debe tener una longitud mayor a 100 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        if (partes[4] != null) {
                            if (partes[4].toString().length() <= 100) {
                                String segundoApellido = partes[4].toString();
                                this.nuevoProductoCatastralDetalle
                                    .setSegundoApellido(segundoApellido);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El segundo apellido no debe tener una longitud mayor a 100 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        if (partes[5] != null) {
                            if (partes[5].toString().length() <= 100) {
                                String primerNombre = partes[5].toString();
                                this.nuevoProductoCatastralDetalle
                                    .setPrimerNombre(primerNombre);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El primer nombre no debe tener una longitud mayor a 100 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }

                        }

                        if (partes[6] != null) {
                            if (partes[6].toString().length() <= 100) {
                                String segundoNombre = partes[6].toString();
                                this.nuevoProductoCatastralDetalle
                                    .setSegundoNombre(segundoNombre);
                            } else {
                                contadorErrores++;
                                String mensaje =
                                    "Para la persona identificada con número identificación " +
                                     partes[2] +
                                    
                                    " El segundo nombre no debe tener una longitud mayor a 100 campos";
                                this.addMensajeError(mensaje);
                                this.productosCatastralesDetallesAsociadasAProductos.removeAll(
                                    this.productosCatastralesDetallesAsociadasAProductos);
                                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();
                                return;
                            }
                        }

                    }

                    this.productosCatastralesDetallesAsociadasAProductos.add(
                        this.nuevoProductoCatastralDetalle);
                    this.nuevoProductoCatastralDetalle.setProductoCatastral(
                        this.productoCatastralSeleccionado);

                    this.documentoArchivoAnexo = new DocumentoArchivoAnexo();
                    this.documentoArchivoAnexo.setFormato(eventoMasa.getFile()
                        .getContentType());
                    if (eventoMasa.getFile().getContentType().equals(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                         eventoMasa.getFile().getFileName().endsWith(
                            EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getExtension())) {
                        this.documentoArchivoAnexo.setFormato(
                            EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getCodigo());
                    } else {
                        this.documentoArchivoAnexo
                            .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX.getCodigo());
                    }
                    this.documentoArchivoAnexo.setArchivo(eventoMasa.getFile().getFileName());

                }

                this.nuevoProductoCatastralDetalle = new ProductoCatastralDetalle();

                if (contadorErrores == 0) {
                    String mensaje = "El documento masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                }

            }
        } catch (IOException e) {
            String mensaje = "El archivo no contiene la estructura adecuada!";
            this.LOGGER.error(mensaje + " " + e, e);
            this.nombreDocumentoProductoMasivo = null;
            this.addMensajeError(mensaje);
        } catch (Exception e) {
            String mensaje = "Ocurrio un error al leer el archivo!";
            this.LOGGER.error(mensaje + " " + e, e);
            this.nombreDocumentoProductoMasivo = null;
            this.addMensajeError(mensaje);
            return;
        }
        guardarDocumentoSoporteProducto();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para validar la estructura basica del archivo de bloqueo masivo
     *
     * @author leidy.gonzalez
     */
    private boolean validarEstructuraArchivoPersonaMasivo(String[] partes) {

        if (partes[0] != null || partes[1] != null || partes[2] != null) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de la descarga de un archivo temporal de office
     *
     * @author leidy.gonzalez
     */
    public void controladorDescargaArchivos() {

        if (this.documentoTemporalAVisualizar != null &&
             this.documentoTemporalAVisualizar.getArchivo() != null &&
             !this.documentoTemporalAVisualizar.getArchivo().isEmpty()) {

            File fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.documentoTemporalAVisualizar.getArchivo());
            InputStream stream;

            try {

                stream = new FileInputStream(fileTemp);
                cargarDocumentoTemporalPrev();
                this.fileTempDownload = new DefaultStreamedContent(stream,
                    this.tipoMimeDocTemporal,
                    this.documentoTemporalAVisualizar.getArchivo());

            } catch (FileNotFoundException e) {
                String mensaje = "Ocurrió un error al cargar al archivo." +
                     " Por favor intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización Se
     * adiciono un atributo tipoMimeDocumento para la clase de previsualización de documento y este
     * debe ser el que se consuma
     *
     * @author leidy.gonzalez
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoTemporalAVisualizar
                .getArchivo());
    }

    /**
     * Método encargado de guardar el documento soporte
     *
     * @author leidy.gonzalez
     */
    public void guardarDocumentoSoporteProducto() {

        this.documentoSoporteProducto = new Documento();
        String fileSeparator = System.getProperty("file.separator");

        if (this.nombreDocumentoProductoMasivo != null &&
            !this.nombreDocumentoProductoMasivo.trim().isEmpty()) {

            this.documentoSoporteProducto.setArchivo(this.nombreDocumentoProductoMasivo);
            this.documentoSoporteProducto.setEstado(EDocumentoEstado.CARGADO.getCodigo());

            TipoDocumento tipoDocumento = new TipoDocumento();

            if (ERegPreCriterioBusqueda.POR_PREDIO.getCodigo().equals(this.criterioBusqueda)) {

                tipoDocumento.setId(ETipoDocumento.DOC_MASIVO_PRODUCTO_PREDIO.getId());

            } else if (ERegPreCriterioBusqueda.POR_DOCUMENTO_DE_IDENTIDAD.getCodigo().equals(
                this.criterioBusqueda) || ERegPreCriterioBusqueda.POR_NOMBRE_DE_PROPIETARIO.
                    getCodigo().equals(this.criterioBusqueda)) {

                tipoDocumento.setId(ETipoDocumento.DOC_MASIVO_PRODUCTO_PERSONA.getId());
            }

            this.documentoSoporteProducto.setTipoDocumento(tipoDocumento);
            tipoDocumento = this.getGeneralesService().buscarTipoDocumentoPorId(
                documentoSoporteProducto.getTipoDocumento().getId());
            this.documentoSoporteProducto.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            this.documentoSoporteProducto.setUsuarioCreador(this.usuario.getLogin());
            this.documentoSoporteProducto.setUsuarioLog(this.usuario.getLogin());
            this.documentoSoporteProducto.setFechaDocumento(new Date());
            this.documentoSoporteProducto.setBloqueado(ESiNo.NO.getCodigo());
            this.documentoSoporteProducto.setFechaLog(new Date());

        } else {

            this.documentoSoporteProducto.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            this.documentoSoporteProducto.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
            this.documentoSoporteProducto.setBloqueado(ESiNo.NO.getCodigo());
            this.documentoSoporteProducto.setFechaLog(new Date());
            this.documentoSoporteProducto.setFechaDocumento(new Date());
            this.documentoSoporteProducto.setUsuarioCreador(this.usuario.getLogin());
            this.documentoSoporteProducto.setUsuarioLog(this.usuario.getLogin());

            TipoDocumento tipoDocumento = new TipoDocumento();

            if (ERegPreCriterioBusqueda.POR_PREDIO.getCodigo().equals(this.criterioBusqueda)) {

                tipoDocumento.setId(ETipoDocumento.DOC_MASIVO_PRODUCTO_PREDIO.getId());

            } else if (ERegPreCriterioBusqueda.POR_DOCUMENTO_DE_IDENTIDAD.getCodigo().equals(
                this.criterioBusqueda) || ERegPreCriterioBusqueda.POR_NOMBRE_DE_PROPIETARIO.
                    getCodigo().equals(this.criterioBusqueda)) {

                tipoDocumento.setId(ETipoDocumento.DOC_MASIVO_PRODUCTO_PERSONA.getId());
            }
        }

        DocumentoSolicitudDTO datosGestorDocumental =
            DocumentoSolicitudDTO.crearArgumentoCargarSolicitudAvaluoComercial(
                this.solicitudSeleccionada.getNumero(), this.solicitudSeleccionada.getFecha(), "",
                UtilidadesWeb.obtenerRutaTemporalArchivos() + fileSeparator +
                documentoSoporteProducto.getArchivo());

        // Subo el archivo a Alfresco y actualizo el documento
        this.getGeneralesService().guardarDocumentosSolicitudAvaluoAlfresco(
            this.usuario, datosGestorDocumental, documentoSoporteProducto);

        this.getTramiteService().guardarYactualizarDocumento(this.documentoSoporteProducto);

    }

    /**
     * Metodo encargado de limpiar los campos del Numero Predial que se asocio al producto
     *
     * @author leidy.gonzalez
     */
    public void limpiarCamposPredioPartes() {

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartesInicial = new NumeroPredialPartes();
        this.numeroPredialPartesFinal = new NumeroPredialPartes();

        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.
            getManagedBean("combosDeptosMunis");

        if ((this.tipoDeSeleccionConsultaPredio != null && !this.tipoDeSeleccionConsultaPredio.
            trim().isEmpty() &&
             "1".equals(this.tipoDeSeleccionConsultaPredio)) ||
             this.tipoDeSeleccionConsultaPredio == null) {
            this.numeroPredialPartes.setDepartamentoCodigo(combosDeptosMunisMB.
                getSelectedDepartamentoCod());
            this.numeroPredialPartes.setMunicipioCodigo(combosDeptosMunisMB.
                getSelectedMunicipioCod().substring(2));
        } else if (this.tipoDeSeleccionConsultaPredio != null &&
            !this.tipoDeSeleccionConsultaPredio.trim().isEmpty() &&
             "2".equals(this.tipoDeSeleccionConsultaPredio)) {
            this.numeroPredialPartesInicial.setDepartamentoCodigo(combosDeptosMunisMB.
                getSelectedDepartamentoCod());
            this.numeroPredialPartesInicial.setMunicipioCodigo(combosDeptosMunisMB.
                getSelectedMunicipioCod().substring(2));
            this.numeroPredialPartesFinal.setDepartamentoCodigo(combosDeptosMunisMB.
                getSelectedDepartamentoCod());
            this.numeroPredialPartesFinal.setMunicipioCodigo(combosDeptosMunisMB.
                getSelectedMunicipioCod().substring(2));
        }
    }

    /**
     * Método encargado de eliminar productos catastrales detalles asociados a productos
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se valida que al eliminar un producto catastral en pantalla
     * tambien se elimine en la Base de Datos
     * @modified by leidy.gonzalez 30/07/2014 Se elimina producto catrastal de la Base de Datos.
     */
    public void eliminarProductosCatastralesDetallesAsociadasAProductos() {
        if (this.productoCatastralDetalleSeleccionado == null) {
            this.addMensajeError("Debe seleccionar el producto a ser eliminado");
        } else {
            this.productosCatastralesDetallesAsociadasAProductos.remove(
                this.productoCatastralDetalleSeleccionado);
            this.getGeneralesService().eliminarProductoCatastralDetalle(
                this.productoCatastralDetalleSeleccionado);
        }

        this.productoCatastralSeleccionado.setProductoGenerar(null);

        this.doDatabaseStatesUpdate();

        this.limpiarCamposDepartamentoYMunicipio();
    }

    /**
     * Método encargado de eliminar productos catastrales detalles asociados a productos
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se valida que al eliminar un producto catastral en pantalla
     * tambien se elimine en la Base de Datos
     * @modified by leidy.gonzalez 30/07/2014 Se elimina producto catrastal de la Base de Datos.
     */
    public void editarResolucionesAsociadasAProductos() {
        List<Tramite> resolucionesTramite = new ArrayList<Tramite>();

        /* if(this.selectedDepartamentoCodigo != null && this.selectedMunicipioCodigo != null){
         * this.productoCatastralDetalleSeleccionado.setDepartamentoCodigo(this.selectedDepartamentoCodigo);
         * this.productoCatastralDetalleSeleccionado.setMunicipioCodigo(this.selectedMunicipioCodigo.substring(2,5));
    	} */
        boolean hayDatosFaltantes = true;

        /* for(ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos){
         * //Validar que el número de resolución inicial no se halla ingresado ya
         * if(this.nuevoProductoCatastralDetalle.getNumeroResolucionInicial() != null &&
         * this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial().equals(pcd.getNumeroResolucionInicial())){
         * this.addMensajeError("Este número de resolución ya ha sido ingresado, por favor verifique
         * los datos e intente nuevamente"); return; } //Validar que la fecha de resolución inicial
         * no se halla ingresado ya else
         * if(this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial() != null &&
         * this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial().equals(pcd.getFechaResolucionInicial())){
         * this.addMensajeError("Esta fecha de resolución ya ha sido ingresado, por favor verifique
         * los datos e intente nuevamente"); return; }
		} */
        //Por número de resolución individual
        if (this.tipoDeBusquedaResolucionesDeConservacion != null) {
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("1")) {
                if (this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial() != null &&
                    !this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial().
                        isEmpty()) {
                    hayDatosFaltantes = false;

                    //consultar resolucion generada
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucion(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas para el numero de resolucion ingresado");
                        return;
                    }
                }
            }
            //Por fecha de resolución individual
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("2")) {
                if (this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial() != null) {
                    hayDatosFaltantes = false;

                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucion(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas para esta fecha ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en la fecha ingresada");
                    }
                }
            }
            //Por rango de número de resolución
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("3")) {
                if (this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial() != null &&
                    this.productoCatastralDetalleSeleccionado.getNumeroResolucionFinal() != null) {

                    hayDatosFaltantes = false;

                    //comparar que año de resolucion final no sea mayor a año de resolucion inicial 
                    /* if(this.nuevoProductoCatastralDetalle.getAnioFinal() >
                     * this.nuevoProductoCatastralDetalle.getAnioInicial()){
                     * this.addMensajeError("Año de resolución inicial debe ser menor o igual a año
                     * de resolución final"); return;
				} */
                    int comparaNumeroResolucion = this.productoCatastralDetalleSeleccionado.
                        getNumeroResolucionInicial().compareTo(
                            this.productoCatastralDetalleSeleccionado.getNumeroResolucionFinal());

                    if (comparaNumeroResolucion > 0) {
                        this.addMensajeError(
                            "El número de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    //consultar resoluciones que fueron generadas
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucionFinalEInicial(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas para este rango de resoluciones ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango establecido");
                    }
                }
            }
            //Por rango de fecha de resolución
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("4")) {
                if (this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial() != null &&
                    this.productoCatastralDetalleSeleccionado.getFechaResolucionFinal() != null) {

                    hayDatosFaltantes = false;

                    if (this.productoCatastralDetalleSeleccionado.getFechaResolucionFinal().before(
                        this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial())) {
                        this.addMensajeError(
                            "La fecha de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    //consultar resoluciones que fueron generadas
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucionFinalEInicial(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas para este rango de fechas ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango de fechas establecido");
                    }
                }

            }
            //Por número de resolución individual de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("5")) {
                if (this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial() != null &&
                    !this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial().
                        isEmpty()) {
                    hayDatosFaltantes = false;

                    //consultar resoluciones que fueron generadas que se encuentran en aplicar cambios por numero de resolucion
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucionAplicaCambios(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para el numero de resoluciòn ingresado");
                        return;
                    }
                }

            }
            //Por fecha de resolución individual de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("6")) {
                if (this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial() != null) {
                    hayDatosFaltantes = false;

                    //consultar resoluciones que fueron generadas en aplicar cambios por fecha de resolucion
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucionAplicaCambios(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para la fecha de resoluciòn ingresado");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en la fecha ingresada para aplicar cambios");
                    }
                }
            }
            //Por rango de número de resolución de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("7")) {
                if (this.productoCatastralDetalleSeleccionado.getNumeroResolucionInicial() != null &&
                     this.productoCatastralDetalleSeleccionado.getNumeroResolucionFinal() != null) {

                    hayDatosFaltantes = false;

                    // comparar que año de resolucion final no sea mayor a año
                    // de resolucion inicial
                    /*
                     * if(this.nuevoProductoCatastralDetalle.getAnioFinal() >
                     * this.nuevoProductoCatastralDetalle.getAnioInicial()){ this.addMensajeError(
                     * "Año de resolución inicial debe ser menor o igual a año de resolución final"
                     * ); return; }
                     */
                    int comparaNumeroResolucion = this.productoCatastralDetalleSeleccionado.
                        getNumeroResolucionInicial().compareTo(
                            this.productoCatastralDetalleSeleccionado.getNumeroResolucionFinal());

                    if (comparaNumeroResolucion > 0) {
                        this.addMensajeError(
                            "El número de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    // consultar resoluciones que fueron generadas en el rango de resoluciones ingresadas y se encuentran en aplicar cambios
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para este rango de resoluciones ingresadas");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango establecido para aplicar cambios");
                    }
                }
            }
            //Por rango de fecha de resolución de Trámite Aplicado
            if (this.tipoDeBusquedaResolucionesDeConservacion.equals("8")) {
                if (this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial() != null &&
                    this.productoCatastralDetalleSeleccionado.getFechaResolucionFinal() != null) {

                    hayDatosFaltantes = false;

                    if (this.productoCatastralDetalleSeleccionado.getFechaResolucionFinal().before(
                        this.productoCatastralDetalleSeleccionado.getFechaResolucionInicial())) {
                        this.addMensajeError(
                            "La fecha de resolución inicial no puede ser mayor a la final, por favor verifique los datos e intente nuevamente");
                        return;
                    }

                    // consultar resoluciones que fueron generadas en el rango de fechas ingresadas y se encuentran en aplicar cambios
                    resolucionesTramite = this.getTramiteService().
                        consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
                            this.productoCatastralDetalleSeleccionado);

                    if (resolucionesTramite.size() == 0) {
                        this.addMensajeError(
                            "No existen resoluciones generadas en aplicar cambios para este rango de fechas ingresados");
                        return;
                    } else {
                        int cantidadTramites = resolucionesTramite.size();

                        this.addMensajeInfo("Existen cantidad de:    " + cantidadTramites +
                            "    resoluciones, en el rango de fechas establecido para aplicar cambios");
                    }
                }

            }

        } else {
            this.addMensajeError("Debe seleccionar un criterio de busqueda");
            return;
        }

        if (hayDatosFaltantes) {
            this.addMensajeError("Por favor ingrese todos los datos e intente nuevamente");
            return;
        }

        this.productoCatastralDetalleSeleccionado = new ProductoCatastralDetalle();
        this.updateDepartamentosItemList();
        this.updateMunicipiosItemList();
    }

    /**
     * Método que se ejecuta cuando se oprime el botón de generar producto
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez se actualiza en base de Datos el campo producto a generar del
     * producto catastral seleccionado
     * @modified by leidy.gonzalez 12/08/2014 Se agrega filtro de departamento y municipio al
     * producto catastral detalle a generar cuando este es seleccionado en registros prediales.
     * @modified by leidy.gonzalez 29/08/2014 Se agrega validación para generar productos por rangos
     * para ficha, carta y certificado.
     * @modified by leidy.gonzalez 15/09/2014 Se modifica el código que genera la carta catastral
     * urbana(del 80 al 5)
     */
    public void generarProducto() {

        Object[] resultado = new Object[2];
        List<Object> parametros = new ArrayList<Object>();

        Pattern patronRegistrosPrediales = Pattern.compile("32|33|1598|1599|36|37|1595|38");
        Matcher matRegistrosPrediales = patronRegistrosPrediales.matcher(
            this.productoCatastralSeleccionado.getProducto().getCodigo());

        Pattern patronRegistrosPredialesConDatosPropietario = Pattern.
            compile("33|1598|1599|1595|38");
        Matcher matRegistrosPredialesConDatosPropietario =
            patronRegistrosPredialesConDatosPropietario.matcher(this.productoCatastralSeleccionado.
                getProducto().getCodigo());

        Pattern patronResoluciones = Pattern.compile("1600|35");
        Matcher matResoluciones = patronResoluciones.matcher(this.productoCatastralSeleccionado.
            getProducto().getCodigo());

        Pattern patronProductosPDF = Pattern.compile("5|30|20");
        Matcher matProductosPDF = patronProductosPDF.matcher(this.productoCatastralSeleccionado.
            getProducto().getCodigo());

        //Guarda los productos catastrales detalle 
        for (ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos) {

            if (this.selectedDepartamentoCodigo != null && !this.selectedDepartamentoCodigo.
                isEmpty()) {
                pcd.setDepartamentoCodigo(this.selectedDepartamentoCodigo);
                if (this.selectedMunicipioCodigo != null && this.selectedMunicipioCodigo.isEmpty()) {
                    pcd.setMunicipioCodigo(this.selectedMunicipioCodigo);
                }

            }

            if (matRegistrosPrediales.matches()) {

                if (pcd.getNumeroPredialInicial() == null || pcd.getNumeroPredialFinal().isEmpty()) {
                    pcd.setNumeroPredialInicial(pcd.getNumeroPredial());
                    pcd.setNumeroPredialFinal(pcd.getNumeroPredial());
                }

                if ("PDF".equalsIgnoreCase(this.productoCatastralSeleccionado.getFormato())) {

                    parametros.clear();

                    parametros.add(pcd.getNumeroPredialInicial());
                    parametros.add(pcd.getNumeroPredialFinal());

                    resultado = this.getGeneralesService().contarRegistrosPrediales(parametros);

                    //Cantidad de registros por propietario
                    if ((Integer.valueOf(String.valueOf(resultado[1]))).intValue() > 10000) {
                        this.addMensajeError("El rango " + pcd.getNumeroPredialInicial() + " - " +
                            pcd.getNumeroPredialFinal() +
                            " devuelve demasiados registros para generar el PDF, por favor" +
                            " cambie los criterios de búsqueda e intente nuevamente");
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                            Constantes.ERROR_REQUEST_CONTEXT);
                        return;
                    }
                }

                this.getGeneralesService().guardarProductoCatastralDetalle(pcd);
            }

            if (matProductosPDF.matches()) {

                if (this.tipoDeSeleccionConsultaPredio != null &&
                     !this.tipoDeSeleccionConsultaPredio.trim().isEmpty() &&
                     "2".equals(this.tipoDeSeleccionConsultaPredio)) {
                    this.getGeneralesService().
                        guardarNumerosPredialesEntreRangosEnProductoCatastralDetalle(pcd);
                    this.getGeneralesService().eliminarProductoCatastralDetalle(pcd);
                } else {
                    this.getGeneralesService().guardarProductoCatastralDetalle(pcd);
                }

            } else if (matResoluciones.matches()) {
                this.getGeneralesService().guardarProductoCatastralDetalle(pcd);
            }

            if (this.productoCatastralSeleccionado != null) {
                if (this.datosAMostrar) {
                    this.productoCatastralSeleccionado.setDatosExtendidos(ESiNo.SI.getCodigo());
                }

                if (matRegistrosPredialesConDatosPropietario.matches()) {
                    this.productoCatastralSeleccionado.setDatosPropietarios(ESiNo.SI.getCodigo());
                }

                this.productoCatastralSeleccionado.setProductoGenerar(ESiNo.SI.getCodigo());

                this.getGeneralesService().guardarActualizarProductoCatastralAsociadoASolicitud(
                    this.productoCatastralSeleccionado);
            }

        }

        if (this.productosCatastralesDetallesAsociadasAProductos.size() !=
            this.productoCatastralSeleccionado.getCantidad().intValue() && !matRegistrosPrediales.
            matches()) {
            this.aviso = "La cantidad de productos solicitados en el(los) documento(s)" +
                "soporte(s) es diferente a la cantidad de los productos generados";
            this.activarBotonEnviarAAjustesSolicitudProducto = true;
        }

    }

    /**
     * Método encargado de definir si se activa la opción de revisar información de productos a
     * generar
     *
     * @author javier.aponte
     */
    public boolean isActivarRevisarInformacionProductosAGenerar() {

        boolean answer = true;
        if (this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.
            isEmpty()) {
            for (ProductoCatastral pc : this.productosCatastralesAsociados) {
                if (pc.getProductoGenerar() == null || ESiNo.NO.getCodigo().equals(pc.
                    getProductoGenerar())) {
                    return false;
                }
            }
        }
        return answer;
    }

    /**
     * Método encargado de avanzar el proceso a revisar información de productos a generar
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez:: 17844:: 27/07/2016:: Se ajusta lista de usuarios con el rol de
     * control de calidad de productos.
     */
    public String avanzarProcesoARevisarInformacionProductosAGenerar() {

        //Avanzar proceso
        String mensaje = null;

        this.consultarNumeroPredialPorPredio();

        this.doDatabaseStatesUpdate();

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            List<UsuarioDTO> controlCalidadProductos = this.obtenerControladorCalidadProductos();

            //Se valida que exista control de calidad productos
            if (controlCalidadProductos == null) {
                this.
                    addMensajeError("No existe usuarios con el rol control de calidad de productos");
                return null;
            }

            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REVISAR_INFORMACION_PRODUCTO_A_GENERAR,
                controlCalidadProductos));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("Avanzando la actividad a registrar ajustar productos trámite catastral");

            this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);

        } catch (Exception e) {
            mensaje = "Ocurrió un error al avanzar el proceso";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
        }

        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }

    }

    /**
     * Método encargado de consultar el numero predial por predio
     *
     * @author leidy.gonzalez
     */
    /*
     * @modified by leidy.gonzalez :: 21/01/2015:: #11167 :: Se valida que para poder consultar si
     * existe el predio se debieron haber agregado los campos a la lista de productos asociados a la
     * solicitud
     */
    public Predio consultarNumeroPredialPorPredio() {

        Predio p = null;
        StringBuilder mensaje = new StringBuilder("");

        if (this.productosCatastralesDetallesAsociadasAProductos != null &&
             !this.productosCatastralesDetallesAsociadasAProductos
                .isEmpty()) {

            for (ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos) {

                if (pcd.getNumeroPredial() != null) {

                    p = this.getConservacionService().getPredioByNumeroPredial(
                        pcd.getNumeroPredial());

                    if (p == null) {

                        mensaje.append("El Predio con número predial: " +
                             pcd.getNumeroPredial() +
                             " no existe en la base de datos");
                    }
                }
            }

            if (mensaje != null && !mensaje.toString().trim().isEmpty()) {
                ProductoCatastralError errorPredio = new ProductoCatastralError();
                errorPredio
                    .setProductoCatastralId(this.productoCatastralSeleccionado
                        .getId());
                errorPredio.setInconsistencias(mensaje.toString());
                errorPredio.setFechaLog(new Date());
                errorPredio.setUsuarioLog(this.usuario.getLogin());

                this.getGeneralesService()
                    .guardarActualizarProductoCatastralError(errorPredio);
            }
        }

        return p;
    }

    /**
     * Método encargado de avanzar el proceso a ingresar solicitud de productos a generar
     *
     * @author javier.aponte
     */
    public String avanzarProcesoAIngresarSolicitudProductos() {

        //Avanzar proceso
        String mensaje = null;

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(this.usuario);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_INGRESAR_AJUSTAR_SOLICITUD_PRODUCTO,
                usuarios));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("Avanzando la actividad a ingresar o ajustar productos trámite catastral");

            this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);

        } catch (Exception e) {
            mensaje = "Ocurrió un error al avanzar el proceso";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
        }

        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }

    }

    /**
     * Método encargado de avanzar el proceso a revisar productos generados
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez:: 17844:: 27/07/2016:: Se realiza ajuste en la lista de usuarios
     * con el rol de control de calidad de productos.
     */
    public String avanzarProcesoARevisarProductosGenerados() {

        //Avanzar proceso
        String mensaje = null;

        this.realizarGeneracionDeProducto();

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            List<UsuarioDTO> controladorDeCalidad = this.obtenerControladorCalidadProductos();

            //Se valida que exista el controlador de calidad
            if (controladorDeCalidad == null) {
                this.addMensajeError(
                    "No existen usuarios con el rol de control de calidad de productos");
                return null;
            }

            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REVISAR_PRODUCTOS_GENERADOS,
                controladorDeCalidad));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("Avanzando la actividad a revisar productos generados");

            this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);

        } catch (Exception e) {
            mensaje = "Ocurrió un error al avanzar el proceso";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
        }

        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }
    }

    /**
     * Método encargado de realizar la generación del producto, dependiendo del tipo de producto
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez 15/09/2014 Se modifica el código que genera la carta catastral
     * urbana(del 80 al 5)
     */
    private void realizarGeneracionDeProducto() {

        ReporteDTO producto;
        String rutaProducto;

        Pattern patronCertificadosCatastralesNacionales;
        Matcher matCertificadosCatastralesNacionales;

        Pattern patronCertificadoEspecial;
        Matcher matCertificadoEspecial;

        Pattern patronCertificadoPlanoPredialCatastral;
        Matcher matCertificadoPlanoPredialCatastral;

        Pattern patronFichaPredialDigital;
        Matcher matFichaPredialDigital;

        Pattern patronCartaCatastralUrbana;
        Matcher matCartaCatastralUrbana;

        EReporteServiceSNC enumeracionProducto = null;
        Map<String, String> parameters;

        //Códigos de los productos asociados a certificados catastrales nacionales
        patronCertificadosCatastralesNacionales = Pattern.compile("25|26|27|28");

        //Código del certificado plano predial catastral 
        patronCertificadoPlanoPredialCatastral = Pattern.compile("30");

        //Código de la ficha predial digital
        patronFichaPredialDigital = Pattern.compile("20");

        //Código de la carta catastral urbana
        patronCartaCatastralUrbana = Pattern.compile("5");

        //Llama la generación de todos los productos catastrales
        for (ProductoCatastral pcTemp : this.productosCatastralesAsociados) {

            matCertificadosCatastralesNacionales = patronCertificadosCatastralesNacionales.matcher(
                pcTemp.getProducto().getCodigo());

            matFichaPredialDigital = patronFichaPredialDigital.matcher(pcTemp.getProducto().
                getCodigo());

            matCertificadoPlanoPredialCatastral = patronCertificadoPlanoPredialCatastral.matcher(
                pcTemp.getProducto().getCodigo());

            matCartaCatastralUrbana = patronCartaCatastralUrbana.matcher(pcTemp.getProducto().
                getCodigo());

            this.productosCatastralesDetallesAsociadasAProductos = this.getGeneralesService().
                buscarProductosCatastralesDetallePorProductoCatastralId(pcTemp.getId());

            //Los certificados catastrales son productos que se envían a generar de manera sincronica, porque se generan muy rápido
            if (matCertificadosCatastralesNacionales.matches()) {

                for (ProductoCatastralDetalle pcdTemp
                    : this.productosCatastralesDetallesAsociadasAProductos) {

                    if (pcdTemp.getNumeroPredial() != null) {
                        enumeracionProducto =
                            EReporteServiceSNC.CERTIFICADO_NACIONAL_POR_PROPIETARIO_POR_PREDIO;
                    } else if (pcdTemp.getNumeroIdentificacion() != null) {
                        enumeracionProducto =
                            EReporteServiceSNC.CERTIFICADO_NACIONAL_POR_PROPIETARIO_POR_PERSONA;
                    }

                    parameters = new HashMap<String, String>();

                    parameters.put("PRODUCTO_CATASTRAL_DETALLE_ID", String.valueOf(pcdTemp.getId()));

                    producto = reportsService.generarReporte(parameters,
                        enumeracionProducto, this.usuario);

                    rutaProducto = this.getGeneralesService().
                        guardarProductoGeneradoEnGestorDocumental(
                            producto.getRutaCargarReporteAAlfresco(),
                            this.usuario.getCodigoEstructuraOrganizacional());

                    if (producto != null) {
                        pcdTemp.setRutaProductoEjecutado(rutaProducto);

                        this.getGeneralesService().guardarProductoCatastralDetalle(pcdTemp);
                    }

                }
            }

            //Generar la ficha predial digital
            if (matFichaPredialDigital.matches()) {

                for (ProductoCatastralDetalle pcdTemp
                    : this.productosCatastralesDetallesAsociadasAProductos) {

                    this.getGeneralesService().generarFichaPredialDigitalProductos(pcdTemp,
                        this.usuario);

                }

            }

            //Generar el certificado plano predial catastral
            if (matCertificadoPlanoPredialCatastral.matches()) {

                for (ProductoCatastralDetalle pcdTemp
                    : this.productosCatastralesDetallesAsociadasAProductos) {

                    this.getGeneralesService().generarCertificadoPlanoPredialCatastralProductos(
                        pcdTemp, this.usuario);

                }

            }

            //Generar la carta catastral urbana
            if (matCartaCatastralUrbana.matches()) {

                for (ProductoCatastralDetalle pcdTemp
                    : this.productosCatastralesDetallesAsociadasAProductos) {

                    this.getGeneralesService().generarCartaCatastralUrbanaProductos(pcdTemp,
                        this.usuario);

                }

            }

            //Códigos de los productos asociados a certificados catastrales nacionales
            patronCertificadoEspecial = Pattern.compile("31");
            matCertificadoEspecial = patronCertificadosCatastralesNacionales.matcher(pcTemp.
                getProducto().getCodigo());

            //Los certificados catastrales son productos que se envían a generar de manera sincronica, porque se generan muy rápido
            if (matCertificadoEspecial.matches()) {

                //Arreglar para el certificado catastral
            }
        }
    }

    /**
     * Método que se ejecuta cuando se selecciona un dato a certificar en la pantalla de certificado
     * especial
     *
     * @author javier.aponte
     */
    public void activarTextosDatosACertificar() {

        this.activarUltimoActoAdministrativo = false;
        this.activarAvaluosAnteriores = false;
        this.activarDestinoEconomico = false;
        this.activarNumeroPredialAnterior = false;
        this.activarPrediosColindantes = false;
        this.activarJustificacionDelDerechoDePropiedadOPosesion = false;
        this.activarZonaGeoeconomica = false;
        this.activarCotas = false;

        for (String dc : this.datosACertificarSelected) {
            if (ECertEspDatoCertificar.ULTIMO_ACTO_ADMINISTRATIVO.getCodigo().equals(dc)) {
                this.activarUltimoActoAdministrativo = true;
            }
            if (ECertEspDatoCertificar.AVALUOS_ANTERIORES.getCodigo().equals(dc)) {
                this.activarAvaluosAnteriores = true;
            }
            if (ECertEspDatoCertificar.DESTINO_ECONOMICO.getCodigo().equals(dc)) {
                this.activarDestinoEconomico = true;
            }
            if (ECertEspDatoCertificar.NUMERO_PREDIAL_ANTERIOR.getCodigo().equals(dc)) {
                this.activarNumeroPredialAnterior = true;
            }
            if (ECertEspDatoCertificar.PREDIOS_COLINDANTES.getCodigo().equals(dc)) {
                this.activarPrediosColindantes = true;
            }
            if (ECertEspDatoCertificar.JUSTIFICACION_DEL_DERECHO_DE_PROPIEDAD_O_POSESION.getCodigo().
                equals(dc)) {
                this.activarJustificacionDelDerechoDePropiedadOPosesion = true;
            }
            if (ECertEspDatoCertificar.ZONA_GEOECONOMICA.getCodigo().equals(dc)) {
                this.activarZonaGeoeconomica = true;
            }
            if (ECertEspDatoCertificar.COTAS.getCodigo().equals(dc)) {
                this.activarCotas = true;
            }
        }

    }

    /**
     * Método que se ejecuta cuando se oprime el botón aceptar en la pantalla de destino de
     * certificados catastrales
     *
     * @author javier.aponte
     */
    public void aceptarDestinoCertificadosCatastrales() {

        this.nuevoProductoCatastralDetalle.setConDestinoA(this.destinoCertificadoCatastral);

    }

    /**
     * Método encargado de obtener el controlado de calidad de productos
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez:: 17844:: 27/07/2016:: Se realiza ajuste en la lista de usuarios
     * con el rol de control de calidad de productos.
     */
    private List<UsuarioDTO> obtenerControladorCalidadProductos() {

        List<UsuarioDTO> controladoresCalidadProductos = new ArrayList<UsuarioDTO>();

        if (this.usuario.getDescripcionUOC() != null) {
            controladoresCalidadProductos = this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    usuario.getDescripcionUOC(),
                    ERol.CONTROL_CALIDAD_PRODUCTOS);
        }

        //si no existe Control calidad productos en la UOC se buscan en la territorial.
        if (controladoresCalidadProductos.isEmpty()) {
            controladoresCalidadProductos = this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    usuario.getDescripcionTerritorial(),
                    ERol.CONTROL_CALIDAD_PRODUCTOS);
        }

        if (controladoresCalidadProductos != null && !controladoresCalidadProductos.isEmpty()) {
            return controladoresCalidadProductos;
        }

        return null;

    }

    /**
     * Método encargado de registrar inconsistencias
     *
     * @author javier.aponte
     */
    public void asociarInconsistenciaAProductoCatastral() {

        String numeroPredial = this.numeroPredialPartes.assembleNumeroPredialFromSegments();

        if (numeroPredial == null || numeroPredial.isEmpty()) {

            this.addMensajeError("Debe ingresar el número predial completo");
            return;

        }

        for (ProductoInconsistenciaDTO pi : this.productoInconsistenciasDTO) {
            if (numeroPredial.equals(pi.getNumeroPredial())) {
                this.addMensajeError(
                    "Este número predial ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                return;
            }
        }

        if (this.productoInconsistenciasDTO == null) {
            this.productoInconsistenciasDTO = new ArrayList<ProductoInconsistenciaDTO>();
        }

        ProductoInconsistenciaDTO productoInconsistenciaDTO = new ProductoInconsistenciaDTO();

        productoInconsistenciaDTO.setNumeroPredial(numeroPredial);

        this.productoInconsistenciasDTO.add(productoInconsistenciaDTO);

    }

    /**
     * Mètodo que se ejecuta cuando se oprime aceptar en la pantalla de descripción de trámite
     * catastral
     *
     * @author javier.aponte
     * @modified leidy.gonzalez
     */
    public void aceptarRegistroTramiteCatastral() {

        if (descripcionTramiteCatastral == null || descripcionTramiteCatastral.isEmpty()) {

            this.addMensajeError("Debe ingresar la descripcion del tramite catastral");
            return;

        }

        if (this.descripcionTramiteCatastralOtro != null && !this.descripcionTramiteCatastralOtro.
            trim().isEmpty()) {
            this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                this.descripcionTramiteCatastralOtro + "\n");
            this.activarEnviarTramiteCatastral = true;

            if (this.descripcionTramiteCatastralNota != null &&
                !this.descripcionTramiteCatastralNota.trim().isEmpty()) {
                this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                    this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() +
                    "\n" + this.descripcionTramiteCatastralNota + "\n");
                this.activarEnviarTramiteCatastral = true;
            }
        } else if (this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() ==
            null) {
            if (this.descripcionTramiteCatastral != null &&
                !this.descripcionTramiteCatastral.trim().isEmpty()) {
                this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                    this.descripcionTramiteCatastral + "\n");
                this.activarEnviarTramiteCatastral = true;
            }

            if (this.descripcionTramiteCatastralNota != null &&
                !this.descripcionTramiteCatastralNota.trim().isEmpty()) {
                this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                    this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() +
                    "\n" + this.descripcionTramiteCatastralNota + "\n");
                this.activarEnviarTramiteCatastral = true;
            }

            if (this.productoInconsistenciasDTOSeleccionado != null) {
                this.productoInconsistenciasDTOSeleccionado.setTramiteCatastralDescripcion(
                    this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion());
            }
        } else {
            if (!this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion().
                contains(this.descripcionTramiteCatastral)) {

                if (this.descripcionTramiteCatastral != null && !this.descripcionTramiteCatastral.
                    trim().isEmpty()) {
                    this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                        this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() +
                        "\n" + this.descripcionTramiteCatastral + "\n");
                    this.activarEnviarTramiteCatastral = true;
                }

                if (this.descripcionTramiteCatastralNota != null &&
                    !this.descripcionTramiteCatastralNota.trim().isEmpty()) {
                    this.productoCatastralDetalleSeleccionado.setTramiteCatastralDescripcion(
                        this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() +
                        "\n" + this.descripcionTramiteCatastralNota + "\n");
                    this.activarEnviarTramiteCatastral = true;
                }

                if (this.productoInconsistenciasDTOSeleccionado != null) {
                    this.productoInconsistenciasDTOSeleccionado.setTramiteCatastralDescripcion(
                        this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() +
                        "\n");
                }
            } else {
                this.addMensajeError("Este trámite catastral ya se encuentra asociado al predio");
                return;
            }
        }
    }

    /**
     * Mètodo que se ejecuta cuando se oprime aceptar en la pantalla de descripción de depuración
     * geográfica
     *
     * @author javier.aponte
     */
    public void aceptarRegistroDepuracionGeografica() {

        if (this.productoCatastralDetalleSeleccionado.getDepuracionGeoOtro() != null) {
            this.productoInconsistenciasDTOSeleccionado.setDepuracionGeoDescripcion(
                this.productoCatastralDetalleSeleccionado.getDepuracionGeoOtro());
        }

        this.productoCatastralDetalleSeleccionado.setDepuracionGeoDescripcion(
            this.productoCatastralDetalleSeleccionado.getDepuracionGeoDescripcion() + " " +
            this.productoCatastralDetalleSeleccionado.getDepuracionGeoNota());

        if (this.productoInconsistenciasDTOSeleccionado != null) {
            this.productoInconsistenciasDTOSeleccionado.setDepuracionGeoDescripcion(
                this.productoCatastralDetalleSeleccionado.getDepuracionGeoDescripcion());
        }

    }

    /**
     * Método encargado de obtener el responsable de conservación o director territorial asociados
     * al trámite según corresponda
     *
     * @author javier.aponte
     * @return UsuarioDTO
     */
    private UsuarioDTO obtenerResponsableConservacion() {

        List<UsuarioDTO> responsablesConservacion;

        responsablesConservacion = (List<UsuarioDTO>) this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION);

        return responsablesConservacion.get(0);
    }

    /**
     * Método encargado de obtener el responsable de conservación o director territorial asociados
     * al trámite según corresponda
     *
     * @author leidy.gonzalez
     * @return UsuarioDTO
     */
    private UsuarioDTO obtenerDirectorTerritorial() {

        List<UsuarioDTO> directorTerritorial;

        directorTerritorial = (List<UsuarioDTO>) this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionTerritorial(),
                ERol.DIRECTOR_TERRITORIAL);

        return directorTerritorial.get(0);
    }

    /**
     * Método encargado de obtener el responsable de conservación o director territorial asociados
     * al trámite según corresponda
     *
     * @author leidy.gonzalez
     * @return UsuarioDTO
     */
    private UsuarioDTO obtenerResponsableAtencionUsuario() {

        List<UsuarioDTO> responsablesConservacion;

        responsablesConservacion = (List<UsuarioDTO>) this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_ATENCION_USUARIO);

        return responsablesConservacion.get(0);
    }

    /**
     * Método que se ejecuta cuando se oprime el boton enviar Tramite catastral
     *
     * @author leidy.gonzalez
     * @return UsuarioDTO
     */
    public void enviarTramiteCatastral() {

        if (this.productoCatastralDetalleSeleccionado.getNumeroPredial() != null) {

            List<String> destinatariosList = new ArrayList<String>();

            UsuarioDTO responsableConservacion = this.obtenerResponsableConservacion();
            // Responsable de conservación de la UOC.
            if (responsableConservacion != null &&
                responsableConservacion.getEmail() != null &&
                !responsableConservacion.getEmail().trim().isEmpty()) {
                destinatariosList.add(responsableConservacion.getEmail());
            }

            String[] destinatarios = destinatariosList
                .toArray(new String[destinatariosList.size()]);

            // Remitente
            String remitente = this.usuario.getEmail();

            // Asunto y el contenido del correo electrónico.
            String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_REGISTRO_TRAMITE_CATASTRAL_PRODUCTOS;

            // Plantilla de contenido
            String codigoPlantillaContenido =
                EPlantilla.MENSAJE_COMUNICACION_REGISTRO_TRAMITEL_PRODUCTOS
                    .getCodigo();

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaContenido = new String[3];
            parametrosPlantillaContenido[0] = this.obtenerResponsableConservacion().
                getNombreCompleto();
            parametrosPlantillaContenido[1] = this.obtenerDirectorTerritorial().getNombreCompleto();
            parametrosPlantillaContenido[2] = this.obtenerResponsableAtencionUsuario().
                getNombreCompleto();

            // Cargar parámetros en el managed bean del componente.
            VisualizacionCorreoActualizacionMB visualCorreoMB =
                (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                    .getManagedBean("visualizacionCorreoActualizacion");
            visualCorreoMB.inicializarParametros(asunto, destinatarios,
                remitente, null, codigoPlantillaContenido,
                null,
                parametrosPlantillaContenido, null);

            // Envio de correo electrónico
            visualCorreoMB.enviarCorreo();
        }
        this.activarEnviarTramiteCatastral = false;
    }

    /**
     * Mètodo encargado de determinar si se muestra el total de registros en la pantalla de consulta
     * de datos por predio o datos por persona
     *
     * @author leidy.gonzalez
     */
    public boolean isActivarTotalRegistros() {
        for (ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos) {
            if (pcd.getNumeroPredialFinal() != null) {
                return false;
            }
        }
        if (nivelDeInformacion != null) {
            return false;
        }
        return true;
    }

    /**
     * Método que gestiona la activación del boton de Enviar tramite Catastral sobre la lista de
     * productos asociados a la solicitud
     *
     * @author leidy.gonzalez
     */
    public void activarSeleccionProductosAsociadosSolicitud() {
        if (this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() != null) {
            this.activarEnviarTramiteCatastral = true;
        }
    }

    public void setActivarTotalRegistros(boolean activarTotalRegistros) {
        this.activarTotalRegistros = activarTotalRegistros;
    }

    /**
     * Método que gestiona la desactivación del boton de Enviar tramite Catastral sobre la lista de
     * productos asociados a la solicitud
     *
     * @author leidy.gonzalez
     */
    public void desactivarSeleccionProductosAsociadosSolicitud() {
        if (this.productoCatastralDetalleSeleccionado.getTramiteCatastralDescripcion() == null) {
            this.activarEnviarTramiteCatastral = false;
        }
    }

    /**
     * Método que permite visualizar el formato a escoger de acuerdo a el producto catastral
     * seleccionado
     *
     * @author leidy.gonzalez
     */
    public List<SelectItem> getProductoFormato() {

        List<SelectItem> lista = new ArrayList<SelectItem>();

        List<ProductoFormato> listProductoFormato = this.getGeneralesService()
            .getCacheFormatoPorProducto(
                this.productoCatastralSeleccionado.getProducto()
                    .getId());

        lista.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (listProductoFormato != null && !listProductoFormato.isEmpty()) {
            for (ProductoFormato productoFormato : listProductoFormato) {
                lista.add(new SelectItem(productoFormato.getFormato()));
            }
        }

        return lista;
    }

    public void adicionarResolucionProducto() {

    }

    /**
     * Listener para la seleccion de departamentos
     *
     * @author felipe.cadena
     */
    public void onDeptoSelectedListener() {

        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.
            getManagedBean("combosDeptosMunis");
        combosDeptosMunisMB.onChangeDepartamentosListener();

        this.numeroPredialPartesInicial.setDepartamentoCodigo(combosDeptosMunisMB.
            getSelectedDepartamentoCod());
        this.numeroPredialPartesFinal.setDepartamentoCodigo(combosDeptosMunisMB.
            getSelectedDepartamentoCod());
        this.numeroPredialPartes.setDepartamentoCodigo(combosDeptosMunisMB.
            getSelectedDepartamentoCod());

        if (!combosDeptosMunisMB.getMunicipios().isEmpty()) {
            combosDeptosMunisMB.setSelectedMunicipio(combosDeptosMunisMB.getMunicipios().get(0));
            combosDeptosMunisMB.setSelectedMunicipioCod(combosDeptosMunisMB.getMunicipios().get(0).
                getCodigo());
            this.onMunSelectedListener();
        }
    }

    /**
     * Listener para la seleccion de departamentos
     *
     * @author felipe.cadena
     */
    public void onMunSelectedListener() {

        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.
            getManagedBean("combosDeptosMunis");
        combosDeptosMunisMB.onChangeMunicipiosListener();

        this.numeroPredialPartesInicial.setMunicipioCodigo(combosDeptosMunisMB.
            getSelectedMunicipioCod().substring(2, 5));
        this.numeroPredialPartesFinal.setMunicipioCodigo(combosDeptosMunisMB.
            getSelectedMunicipioCod().substring(2, 5));
        this.numeroPredialPartes.setMunicipioCodigo(combosDeptosMunisMB.getSelectedMunicipioCod().
            substring(2, 5));

    }

    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author felipe.cadena
     */
    private boolean reclamarActividad(Solicitud solicitudReclamar) {
        Actividad actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        List<Actividad> actividades;

        if (actividad == null) {
            actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (actividades != null && !actividades.isEmpty()) {
                for (Actividad actTemp : actividades) {
                    if (actTemp.getIdObjetoNegocio() == solicitudReclamar.getId()) {
                        actividad = actTemp;
                        break;
                    }
                }
            }
        }

        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.usuario.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                        solicitudReclamar.getNumero()));
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.usuario);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                    solicitudReclamar.getNumero()));
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }
    }

    /**
     * Método que puede ser llamado desde la interfaz web, encargado de asociar productos
     * catastrales detalles a productos solicitados
     *
     * @author juan.cruz
     */
    public void asociarProductosCatastralesDetallesAProductos() {
        this.asociarProductosCatastralesDetallesAProductos(this.nuevoProductoCatastralDetalle);
    }

//end of class
}
