/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import co.gov.igac.generales.dto.UsuarioDTO;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import javax.faces.model.SelectItem;

/**
 *
 * Managed bean del cu Reasignar interventor contrato
 *
 * @author christian.rodriguez
 * @modified felipe.cadena
 * @cu CU-SA-AC-090
 */
@Component("reasignarInterventorContrato")
@Scope("session")
public class ReasignacionInterventorContratoMB extends SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReasignacionInterventorContratoMB.class);
    /**
     * {@link FiltroDatosConsultaAvaluo} filtro usado para la consulta
     */
    private FiltroDatosConsultaContratoInteradministrativo filtro;
    /**
     * Lista con los {@link ContratoInteradministrativo} encontrados al realizar la busqueda
     */
    private List<ContratoInteradministrativo> contratosEncontrados;
    /**
     * Contrato seleccionado para reasignacion de interventor
     */
    private ContratoInteradministrativo[] contratoSeleccionado;
    /**
     * Lista de los posibles intenvetores
     */
    private List<ProfesionalAvaluo> interventores;
    /**
     * Lista de los intenvetores para la busqueda
     */
    private List<SelectItem> interventoresMenu;
    /**
     * Interventor seleccionado para reasignación
     */
    private ProfesionalAvaluo interventorSeleccionado;
    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que determina si se está haciendo una busqueda por numero de contrato o por los datos
     * del interventor
     */
    private boolean banderaBusquedaPorNumeroContrato;

    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaContratoInteradministrativo getFiltro() {
        return this.filtro;
    }

    public List<ContratoInteradministrativo> getContratosEncontrados() {
        return this.contratosEncontrados;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<SelectItem> getInterventoresMenu() {
        return this.interventoresMenu;
    }

    public void setInterventoresMenu(List<SelectItem> interventoresMenu) {
        this.interventoresMenu = interventoresMenu;
    }

    public ContratoInteradministrativo[] getContratoSeleccionado() {
        return this.contratoSeleccionado;
    }

    public void setContratoSeleccionado(
        ContratoInteradministrativo contratoSeleccionado[]) {
        this.contratoSeleccionado = contratoSeleccionado;
    }

    public ProfesionalAvaluo getInterventorSeleccionado() {
        return this.interventorSeleccionado;
    }

    public void setInterventorSeleccionado(
        ProfesionalAvaluo interventorSeleccionado) {
        this.interventorSeleccionado = interventorSeleccionado;
    }

    public List<ProfesionalAvaluo> getInterventores() {
        return this.interventores;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaBusquedaPorNumeroContrato() {
        return this.banderaBusquedaPorNumeroContrato;
    }

    public void setBanderaBusquedaPorNumeroContrato(
        boolean banderaBusquedaPorNumeroContrato) {
        this.banderaBusquedaPorNumeroContrato = banderaBusquedaPorNumeroContrato;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de ReasignacionInterventorContratoMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        this.banderaBusquedaPorNumeroContrato = true;

        this.filtro = new FiltroDatosConsultaContratoInteradministrativo();
        this.filtro.setActivo(true);
        this.cargarListaInterventores();

    }

    /**
     * Método para cargar la lista de interventores que se selecciona para la busqueda
     *
     * @author felipe.cadena
     */
    public void cargarListaInterventores() {

        this.interventoresMenu = new ArrayList<SelectItem>();

        FiltroDatosConsultaProfesionalAvaluos filtroInteventor =
            new FiltroDatosConsultaProfesionalAvaluos();
        filtroInteventor.setTipoVinculacion(
            EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA.getCodigo());
        filtroInteventor.setTerritorialCodigoProfesional(this.usuario.getCodigoTerritorial());
        this.interventores = this.getAvaluosService().
            buscarProfesionalesAvaluosPorFiltro(filtroInteventor);

        for (ProfesionalAvaluo pa : this.interventores) {
            this.interventoresMenu.add(new SelectItem(pa.getId(), pa.getNombreCompleto()));
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que realiza la busqueda de los {@link ContratoInteradministrativo} segun los
     * atributos del {@link FiltroDatosConsultaContratoInteradministrativo}
     *
     * @author christian.rodriguez
     */
    public void buscar() {
        if (this.filtro != null) {

            this.filtro.setActivo(true);
            this.contratosEncontrados = this.getAvaluosService()
                .buscarContratosInteradministrativosPorFiltros(null,
                    this.filtro);
        }

        if (this.contratosEncontrados.isEmpty() && this.banderaBusquedaPorNumeroContrato) {
            this.addMensajeInfo("No existe el No. de contrato ingresado");
        }
        if (this.contratosEncontrados.isEmpty() && !this.banderaBusquedaPorNumeroContrato) {
            this.addMensajeInfo("No existen contratos interadministrativos " +
                 "activos asociados al interventor ingresado");
        }

    }

    public void limpiarCamposBusqueda() {
        this.filtro.setIdInterventor(null);
        this.filtro.setNumeroContrato(null);
        this.filtro.setVigencia(null);
        this.filtro.setNumeroDocumentoInterventor(null);
    }

    // ----------------------------------------------------------------------
    /**
     * Listener encargado de reasignar el interventor del {@link ContratoInteradministrativo}
     *
     * @author christian.rodriguez
     */
    public void asignarInterventor() {
        LOGGER.debug("Asignando interventores...");

        if (this.contratoSeleccionado != null &&
             this.interventorSeleccionado != null) {
            for (ContratoInteradministrativo cia : contratoSeleccionado) {
                cia.setInterventor(this.interventorSeleccionado);
                this.getAvaluosService()
                    .guardarActualizarContratoInteradminsitrativo(
                        cia, null, null);
            }

        } else {
            this.addMensajeError(
                "Debe seleccionar un interventor y un contrato para realizar la asignación.");
        }
        this.contratosEncontrados = new ArrayList<ContratoInteradministrativo>();
        this.cargarListaInterventores();

        LOGGER.debug("Interventores asignados");

    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }
}
