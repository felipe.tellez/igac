package co.gov.igac.snc.web.util;

import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.web.mb.generales.GeneralMB;

public class PaisConverter implements Converter {

    @Autowired
    GeneralMB generalMB;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
        String value) {
        if (!StringUtils.isEmpty(value)) {
            List<Pais> paises = generalMB
                .getPaises();
            for (Pais pais : paises) {
                if (pais.getCodigo().equals(value)) {
                    return pais;
                }
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            Pais pais = (Pais) value;
            return pais.getCodigo();
        }
        return null;
    }

}
