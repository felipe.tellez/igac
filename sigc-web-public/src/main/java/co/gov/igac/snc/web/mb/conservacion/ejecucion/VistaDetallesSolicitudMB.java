/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para la vista de los detalles de un trámite
 *
 * @author juan.agudelo
 */
@Component("vistaDetallesSolicitud")
@Scope("session")
public class VistaDetallesSolicitudMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 1123123123123123L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VistaDetallesSolicitudMB.class);

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    // ---- variables de la visualización de los detalles de la solicitud ----
    /**
     * Solicitud seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Número predial para la vista de detalle
     */
    private String numeroPredial;

    // --------- methods -------------------
    public Solicitud getSolicitudSeleccionada() {
        return this.solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;

        if (this.solicitudSeleccionada != null &&
             this.solicitudSeleccionada.getId() != null) {
            this.solicitudSeleccionada = this.getTramiteService()
                .buscarSolicitudFetchTramitesBySolicitudId(this.solicitudSeleccionada
                    .getId());
        }

    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

//------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("en el init de VistaDetallesSolicitudMB ");
    }

    // -------------------------------------------------------------------------------------------------
    public void buscarPredioPorNumeroPredial() {
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb
            .getManagedBean("consultaPredio");
        mb001.cleanAllMB();
        if (this.numeroPredial != null) {
            Predio auxPredio = this.getConservacionService()
                .getPredioByNumeroPredial(this.numeroPredial);
            this.consultaPredioMB.setSelectedPredioId(auxPredio.getId());
            mb001.setSelectedPredio1(auxPredio);
        }
    }

}
