/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipoCalificacion;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * Clase que se usará como tipo de los objetos (nodos) que se adicionan al treetable de "descripción
 * de unidad de construcción", cuando la unidad de construcción es de tipo convencional
 *
 * @author pedro.garcia
 */
public class CalificacionConstruccionTreeNode {

    private CalificacionConstruccion calificacionConstruccion;
    private ArrayList<SelectItem> detallesCalificacionItemList;

    /**
     * 1(residencial), 2(comercial), 3(industrial)
     */
    private String tipoCalificacion;

    private Integer selectedDetalleValue;

    private CalificacionConstruccion selectedDetalle;

    /**
     * bandera para saber si en este nodo se debe mostrar ese icono como un link para hacer el
     * upload de la imagen
     */
    private boolean mostrarIconoSubirImagen;

    /**
     * rutas que se necesita manejar para ubicar la imagen del componente
     */
    private String rutaArchivoCMS;

    private String rutaArchivoLocal;

    /**
     * guarda la url web del archivo temporal con la imagen del componente de calificación. Es
     * necesaria para poder mostrar la imagen en un p:graphicImage como el que se usa cuando se ha
     * cargado una imagen y se quiere visualizar en ese mismo momento. Se usa porque no funcionó
     * armar el value de ese componente con un DefaultStreamedContent
     */
    private String urlArchivoWebTemporal;

//------------ methods  -----------
//--------------------------------------------------------------------------------------------------
    public CalificacionConstruccionTreeNode() {

    }

    public CalificacionConstruccionTreeNode(String tipoCalificacionP) {
        this.tipoCalificacion = tipoCalificacionP;
    }
//--------------------------------------------------------------------------------------------------

    public String getUrlArchivoWebTemporal() {
        return this.urlArchivoWebTemporal;
    }

    public void setUrlArchivoWebTemporal(String urlArchivoWebTemporal) {
        this.urlArchivoWebTemporal = urlArchivoWebTemporal;
    }

    public void setRutaArchivoLocal(String rutaArchivoLocal) {
        this.rutaArchivoLocal = rutaArchivoLocal;
    }

    public String getRutaArchivoLocal() {
        return rutaArchivoLocal;
    }

    public boolean isMostrarIconoSubirImagen() {
        return mostrarIconoSubirImagen;
    }

    public void setMostrarIconoSubirImagen(boolean mostrarIconoSubirImagen) {
        this.mostrarIconoSubirImagen = mostrarIconoSubirImagen;
    }

    public void setRutaArchivoCMS(String rutaArchivoCMS) {
        this.rutaArchivoCMS = rutaArchivoCMS;
    }

    public String getRutaArchivoCMS() {
        return rutaArchivoCMS;
    }

    public CalificacionConstruccion getSelectedDetalle() {
        return selectedDetalle;
    }

    public void setSelectedDetalle(CalificacionConstruccion selectedDetalle) {
        this.selectedDetalle = selectedDetalle;
    }

    public Integer getSelectedDetalleValue() {
        return selectedDetalleValue;
    }

    public void setSelectedDetalleValue(Integer selectedDetalleValue) {
        this.selectedDetalleValue = selectedDetalleValue;
    }

    public CalificacionConstruccion getCalificacionConstruccion() {
        return calificacionConstruccion;
    }

    public void setCalificacionConstruccion(CalificacionConstruccion calificacionConstruccion) {
        this.calificacionConstruccion = calificacionConstruccion;
    }

    public String getTipoCalificacion() {
        return tipoCalificacion;
    }

    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    public ArrayList<SelectItem> getDetallesCalificacionItemList() {
        return detallesCalificacionItemList;
    }

    public void setDetallesCalificacionItemList(ArrayList<SelectItem> detallesCalificacionItemList) {
        this.detallesCalificacionItemList = detallesCalificacionItemList;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma el combo de detalles de calificación para este elemento
     *
     * @param elementos
     */
    public void armarComboDetalleCalificacion(List<CalificacionConstruccion> elementos) {

        SelectItem selectItem;
        Integer puntos;
        int idTipo = 0;

        this.detallesCalificacionItemList = new ArrayList<SelectItem>();

        CalificacionConstruccion defaultValue = new CalificacionConstruccion();
        defaultValue.setId(0L);

        if (this.tipoCalificacion.
            compareToIgnoreCase(
                EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) == 0) {
            idTipo = 2;
        } else if (this.tipoCalificacion.
            compareToIgnoreCase(
                EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) == 0) {
            idTipo = 3;
        } else if (this.tipoCalificacion.
            compareToIgnoreCase(
                EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo()) == 0) {
            idTipo = 1;
        }

        boolean isTheFirst = true;
        for (CalificacionConstruccion ccTemp : elementos) {
            puntos = getPuntos(idTipo, ccTemp);

            //D: si en la tabla los puntos son negativos quiere decir que no aplica para ese tipo de
            //   calificación
            if (puntos.intValue() >= 0) {
                ccTemp.setPuntos(puntos.intValue());
                if (isTheFirst) {
                    this.selectedDetalle = ccTemp;
                    defaultValue.setPuntos(puntos.intValue());

                    //D: se agregó esto porque ahora es necesario que aparezca la opción de seleccionar
                    selectItem = new SelectItem(defaultValue, "Seleccione...");
                    this.detallesCalificacionItemList.add(selectItem);

                    isTheFirst = false;
                }
                selectItem = new SelectItem(ccTemp, ccTemp.getDetalleCalificacion());
                this.detallesCalificacionItemList.add(selectItem);
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Llama a la función adecuada de un objeto tipo CalificacionConstruccion para obtener los
     * puntos
     *
     * @param idTipo coincide con el de la enumeración EUnidadConstruccionTipoCalificacion
     * @param element
     * @return
     */
    private Integer getPuntos(int idTipo, CalificacionConstruccion element) {

        Integer answer = null;

        switch (idTipo) {
            case 1: {
                answer = element.getPuntosResidencial();
                break;
            }
            case 2: {
                answer = element.getPuntosComercial();
                break;
            }
            case 3: {
                answer = element.getPuntosIndustrial();
                break;
            }
            default:
                answer = new Integer(0);
        }
        return answer;
    }

//end of class
}
