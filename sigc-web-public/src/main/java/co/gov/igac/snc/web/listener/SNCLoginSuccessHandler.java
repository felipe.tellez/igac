/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.listener;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.web.locator.SNCServiceLocator;
import co.gov.igac.snc.util.EEstadoSesion;
import co.gov.igac.snc.web.util.EAtributosSesion;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

/**
 * Clase para registrar los accesos al sistema, funciona como un listener que se activa cuando un
 * usuario se autentica con credenciales correctas.
 *
 * @author felipe.cadena
 */
public class SNCLoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private IContextListener contexto;
    private static Log LOGGER = LogFactory.getLog(SessionCounterListener.class);
    private boolean tieneRol;
    /**
     * Este es el metodo que se ejecuta cada que un usuario se autentica, si solo se permite una
     * sesion por usuario simultanea, cuando el usuario intente autenticarse por segunda vez se
     * redireccionara a la pagina de login y no se realizara ningun registro de entrada.
     *
     * @author felipe.cadena
     * @param request
     * @param response
     * @param authentication
     * @throws ServletException
     * @throws IOException
     *
     * @modified felipe.cadena :: 17/10/2014 ::Se permiten hasta tres sesiones abiertas de un solo
     * usuario
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
        Authentication authentication) throws ServletException, IOException {
        
        LogAcceso accesoActual = UtilidadesWeb.obtenerInfoSesion(request, authentication);
        ConexionUsuario conexionActual = UtilidadesWeb.obtenerInfoConexion(request, authentication,
            accesoActual);
        conexionActual.setUsuario(conexionActual.getUsuario().toLowerCase());
        accesoActual.setEstadoSesion(EEstadoSesion.INICIADA.toString());
        accesoActual.setLogin(accesoActual.getLogin().toLowerCase());
        String loginUser = accesoActual.getLogin().toLowerCase();
        
        List<String> rolesSNC = this.getGeneralesService().obtenerListaNombresGrupo("SI");
        ArrayList<String> rolesSNCUsuario = new ArrayList<String>();
        
        boolean consultaSnc = false;
        boolean secretariaSnc = false;
        tieneRol=false;
        UsuarioDTO user = this.getGeneralesService().getCacheUsuario(loginUser);
        
        boolean entraPorActualizacion = 
            (user.getDescripcionTerritorial().equals("DLG_BARRANQUILLA")||
            user.getDescripcionTerritorial().equals("QUINDIO"));
        
        for (String rol : user.getRoles()) {
            //#75433::juan.cruz:: Se filtran solo roles SNC            
            if(rolesSNC.contains(rol.toUpperCase())){
                rolesSNCUsuario.add(rol);
            }            
            if (ERol.CONSULTA_SNC.getRol().equalsIgnoreCase(rol)) {
                consultaSnc = true;
            }
            if (ERol.SECRETARIA_CONSERVACION.getRol().equalsIgnoreCase(rol)) {
                secretariaSnc = true;
            }
            if (ERol.RESPONSABLE_ACTUALIZACION.getRol().equalsIgnoreCase(rol) ||
                    ERol.RESPONSABLE_SIG.getRol().equalsIgnoreCase(rol)) {
                setTieneRol(true);
            }
        }
        
        user.setRoles(rolesSNCUsuario.toArray(new String[0]));
        
        //parametro para determinar si se permiten varias sesiones simultaneas del mismo usuario 
        Parametro accesoPermitido = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.ACCESO_PERMITIDO.toString());
        Parametro accesoSoloSecretaria = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.SOLO_SECRETARIA.toString());
        //parametro para determinar si se permiten varias sesiones simultaneas del mismo usuario 
        Parametro maxAccesoParam = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.MAX_ACCESOS_SIMULTANEOS.toString());
        long maxNumeroAcceso = maxAccesoParam.getValorNumero().longValue();
        LogAcceso accesoPrevio = this.getGeneralesService().obtenerLogAccesoPorUsuario(loginUser);
        long numeroAccesos = this.getGeneralesService().obtenerNumeroConexionesPorUsuario(loginUser);

        if (accesoPrevio != null &&
             (numeroAccesos >= maxNumeroAcceso) &&
             !accesoActual.getMaquinaCliente().contains(EAtributosSesion.SNC_DESKTOP.getValor()) &&
             !accesoPrevio.getMaquinaCliente().contains(EAtributosSesion.SNC_DESKTOP.getValor()) ||
             accesoPermitido.getValorCaracter().equals(ESiNo.NO.getCodigo()) ||
            (!secretariaSnc && accesoSoloSecretaria.getValorCaracter().equals(ESiNo.SI.getCodigo()))
            ||
            (accesoSoloSecretaria.getValorCaracter().equals(ESiNo.SI.getCodigo()) && entraPorActualizacion )
            ) {

            //Se define parametro para que no se registre salida para los casos de usuario recurrente
            request.getSession().setAttribute(EAtributosSesion.USUARIO_RECURRENTE.getValor(), true);
            response.sendRedirect(request.getContextPath() + "/logout");

        } else {
            if (esCierreDeSistema() && !esUsuarioActualizacionPermitido(user)) {
                request.getSession().setAttribute(EAtributosSesion.USUARIO_RECURRENTE.getValor(), true);
                response.sendRedirect(request.getContextPath() + "/logout");
            }else{
                //Se registra el acceso y se redirecciona a las lista de tareas.
                ConexionUsuario cu = this.getGeneralesService().guardarActualizarConexionUsuario(
                        conexionActual);
                this.getGeneralesService().registrarAcceso(accesoActual);
                this.setDefaultTargetUrl("/tareas/listaTareas.jsf");
                //Se define el i de sesion en bd
                request.getSession().setAttribute(EAtributosSesion.ID_CONEXION.getValor(), cu.getId());
                super.onAuthenticationSuccess(request, response, authentication);
            }
        }        
    }

    /**
     * Se necesita obtener el servicio para persistir el registro de entrada.
     *
     * @author felipe.cadena
     * @return
     */
    private IGenerales getGeneralesService() {
        IGenerales service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getGeneralesService();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }
    
    private boolean esCierreDeSistema(){
        Calendar calActual = Calendar.getInstance();
        Calendar calInicioCierre = Calendar.getInstance();
        Calendar calFinCierre = Calendar.getInstance();
        Parametro intervaloCierre = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.INTERVALO_CIERRE_SISTEMA.toString());
        String[] strIntervalo = intervaloCierre.getValorCaracter().split(":");
        String[] stInicio = strIntervalo[0].split("-");
        String[] stFin = strIntervalo[1].split("-");
        calInicioCierre.set(Integer.valueOf(stInicio[0]), 
                Integer.valueOf(stInicio[1])-1, Integer.valueOf(stInicio[2]), 
                Integer.valueOf(stInicio[3]), Integer.valueOf(stInicio[4]));
        calFinCierre.set(Integer.valueOf(stFin[0]), 
                Integer.valueOf(stFin[1])-1, Integer.valueOf(stFin[2]), 
                Integer.valueOf(stFin[3]), Integer.valueOf(stFin[4]));
        LOGGER.info(calActual.compareTo(calInicioCierre));
        LOGGER.info(calActual.compareTo(calFinCierre));
        LOGGER.info(calActual.compareTo(calInicioCierre) >= 0 && 
                calActual.compareTo(calFinCierre) <=0);
        return calActual.compareTo(calInicioCierre) >= 0 && 
                calActual.compareTo(calFinCierre) <=0;
    }
    
    private boolean esUsuarioActualizacionPermitido(UsuarioDTO user){
        Parametro paramTieneAct = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.TIENE_ACCESO_ACTUALIZACION.toString());
        String[] territorialesAct = paramTieneAct.getValorCaracter().split(",");
        boolean tieneTerritorial = false;
        for (String str : territorialesAct) {
            if(str.contains(user.getDescripcionTerritorial())){
                tieneTerritorial = true;
            }
        }
        
        return tieneRol & tieneTerritorial;
    }

    /**
     * @return the tieneRol
     */
    public boolean isTieneRol() {
        return tieneRol;
    }

    /**
     * @param tieneRol the tieneRol to set
     */
    public void setTieneRol(boolean tieneRol) {
        this.tieneRol = tieneRol;
    }
    
    
}
