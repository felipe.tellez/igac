package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("consultaJustDerProp")
@Scope("session")
public class ConsultaJustificacionDerechoPropiedadMB extends SNCManagedBean
    implements Serializable {

    private static final long serialVersionUID = -8436611868391451222L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaJustificacionDerechoPropiedadMB.class);

    private Predio predio;
    private List<PersonaPredioPropiedad> derechosPropiedad;

    /**
     * id en el repositorio de documentos del documento soporte que se va a visualizar
     */
    private String idDocumentoSoporteEnRepo;

    /**
     * url del documento soporte una vez se ha recuperado del gestor documental
     */
    private String urlDocumentoSoporteJP;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

//--------------------------------------------------------------------------------------------------
    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public Predio getPredio() {
        return this.predio;
    }

    public String getIdDocumentoSoporteEnRepo() {
        return this.idDocumentoSoporteEnRepo;
    }

    public void setIdDocumentoSoporteEnRepo(String idDocumentoSoporteenRepo) {
        this.idDocumentoSoporteEnRepo = idDocumentoSoporteenRepo;
    }

    public String getUrlDocumentoSoporteJP() {
        return this.urlDocumentoSoporteJP;
    }

    public void setUrlDocumentoSoporteJP(String urlDocumentoSoporteJP) {
        this.urlDocumentoSoporteJP = urlDocumentoSoporteJP;
    }

//	public void getPredioByNumeroPredial(String numPredial) {
//		this.predio = this.getConservacionService()
//				.getPredioFetchDerechosPropiedadByNumeroPredial(numPredial);
//	}
    public void getPredioById(Long predioId) {
        this.predio = this.getConservacionService().getPredioFetchPersonasById(predioId);
    }

    public List<PersonaPredioPropiedad> getDerechosPropiedad() {
        return this.derechosPropiedad;
    }

    public void setDerechosPropiedad(
        List<PersonaPredioPropiedad> derechosPropiedad) {
        this.derechosPropiedad = derechosPropiedad;
    }
//--------------------------------------------------------------------------------------------------    

    @PostConstruct
    public void init() {
        this.predio = new Predio();
        this.derechosPropiedad = new ArrayList<PersonaPredioPropiedad>();

        // Se obtiene un predio específico, para pruebas.
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");

        Long predioId = mb001.getSelectedPredioId();
        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioId != null) {
            if (!esPredioOrigen) {
                this.getPredioById(predioId);
            } else {
                this.predio = mb001.getSelectedPredio1();
            }
        }

        if (this.predio != null && this.predio.getPersonaPredios() != null) {
            for (PersonaPredio perPre : this.predio.getPersonaPredios()) {
                List<PersonaPredioPropiedad> personaPredioPropiedades = perPre
                    .getPersonaPredioPropiedads();
                for (PersonaPredioPropiedad perPrePro : personaPredioPropiedades) {
                    this.derechosPropiedad.add(perPrePro);
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link para ver el documento soporte de una justificación de propiedad.
     *
     * @author pedro.garcia
     */
    public void traerDocumentoSoporteJP() {

        LOGGER.debug("consultando documento soporte justificación propiedad ");
        ReporteDTO reporteDocumento;
        if (this.idDocumentoSoporteEnRepo != null) {
            reporteDocumento = this.reportsService.consultarReporteDeGestorDocumental(
                this.idDocumentoSoporteEnRepo);
            this.urlDocumentoSoporteJP = reporteDocumento.getUrlWebReporte();
        }

    }

//end of class    
}
