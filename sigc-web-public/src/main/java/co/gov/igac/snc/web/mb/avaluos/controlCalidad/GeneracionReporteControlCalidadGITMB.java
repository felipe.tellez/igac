/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EDatosBaseCorreoElectronico;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del CU-SA-AC-065 Generar Reporte Control Calidad GIT de avaluos Territorial se llama
 * desde el CU 018 cuando se encuentra en control de calidad GIT
 *
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-065
 */
@Component("generacionReporteControlCalidadGIT")
@Scope("session")
public class GeneracionReporteControlCalidadGITMB extends SNCManagedBean {

    private static final long serialVersionUID = 6354951188456812512L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneracionReporteControlCalidadGITMB.class);
    /**
     * Avalúo asociado a la consulta de controles de calidad
     */
    private Avaluo avaluo;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;
    /**
     * Revisión asociada al oficio remisorio
     */
    private ControlCalidadAvaluoRev revisionCC;
    /**
     * Fecha en que se realiza el reporte
     */
    private Date fechaReporte;
    /**
     * Documento que representa el oficio generado.
     */
    private Documento oficioRemisorio;
    /**
     * predios relacionados al avalúo
     */
    private ArrayList<Predio> prediosAvaluo;
    /**
     * DTO del reporte del oficio
     */
    private ReporteDTO oficioDto;
    /**
     * Director de la territorial donde se realiza el tramite
     */
    private UsuarioDTO directorTerritorial;
    /**
     * Coordinador GIT de avalúos de la territorial en la que se realizó el avaluo
     */
    private UsuarioDTO coordinadorGITAvaluos;
    /**
     * Profesional que está realizando el control de calidad a
     */
    private ProfesionalAvaluo profesionalControlCalidad;
    /**
     * la territorial donde se está realizando el avalúo.
     */
    private EstructuraOrganizacional territorialAvaluo;
    /**
     * Orden de practica asociada al avalúo
     */
    private OrdenPractica ordenPractica;
    /**
     * Bandera para determinar que el oficio ya fue generado.
     */
    private Boolean banderaOficioGenerado;
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    protected IContextListener contexto;
    // --------------------------  Visor GIS --------------------------
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;
    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    // ----------------------------Métodos SET y GET----------------------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ReporteDTO getOficioDto() {
        return oficioDto;
    }

    public void setOficioDto(ReporteDTO oficioDto) {
        this.oficioDto = oficioDto;
    }

    public Boolean getBanderaOficioGenerado() {
        return banderaOficioGenerado;
    }

    public void setBanderaOficioGenerado(Boolean banderaOficioGenerado) {
        this.banderaOficioGenerado = banderaOficioGenerado;
    }

    public OrdenPractica getOrdenPractica() {
        return ordenPractica;
    }

    public void setOrdenPractica(OrdenPractica ordenPractica) {
        this.ordenPractica = ordenPractica;
    }

    public ProfesionalAvaluo getProfesionalControlCalidad() {
        return profesionalControlCalidad;
    }

    public void setProfesionalControlCalidad(ProfesionalAvaluo profesionalControlCalidad) {
        this.profesionalControlCalidad = profesionalControlCalidad;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return directorTerritorial;
    }

    public void setDirectorTerritorial(UsuarioDTO directorTerritorial) {
        this.directorTerritorial = directorTerritorial;
    }

    public UsuarioDTO getCoordinadorGITAvaluos() {
        return coordinadorGITAvaluos;
    }

    public void setCoordinadorGITAvaluos(UsuarioDTO coordinadorGITAvaluos) {
        this.coordinadorGITAvaluos = coordinadorGITAvaluos;
    }

    public EstructuraOrganizacional getTerritorialAvaluo() {
        return territorialAvaluo;
    }

    public void setTerritorialAvaluo(EstructuraOrganizacional territorialAvaluo) {
        this.territorialAvaluo = territorialAvaluo;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public Documento getOficioRemisorio() {
        return oficioRemisorio;
    }

    public void setOficioRemisorio(Documento oficioRemisorio) {
        this.oficioRemisorio = oficioRemisorio;
    }

    public ArrayList<Predio> getPrediosAvaluo() {
        return prediosAvaluo;
    }

    public void setPrediosAvaluo(ArrayList<Predio> prediosAvaluo) {
        this.prediosAvaluo = prediosAvaluo;
    }

    public Date getFechaReporte() {
        return this.fechaReporte;
    }

    public void setFechaReporte(Date fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public ControlCalidadAvaluoRev getRevisionCC() {
        return revisionCC;
    }

    public void setRevisionCC(ControlCalidadAvaluoRev revisionCC) {
        this.revisionCC = revisionCC;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaOficioRemisorioTerritorialMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.oficioRemisorio = new Documento();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
    }

    /**
     * Método para cargar el coordinador del git avaluos y el director territorial
     *
     * @author felipe.cadena
     */
    private void cargarCoordinadores() {
        List<UsuarioDTO> directoresTerritorial = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.territorialAvaluo.getNombre(),
                ERol.DIRECTOR_TERRITORIAL);

        if (!directoresTerritorial.isEmpty()) {
            this.directorTerritorial = directoresTerritorial.get(0);
        }

        List<UsuarioDTO> coordinadoresGITAvaluos = this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                ERol.COORDINADOR_GIT_AVALUOS);

        if (!coordinadoresGITAvaluos.isEmpty()) {
            this.coordinadorGITAvaluos = coordinadoresGITAvaluos.get(0);
        }

    }

    /**
     * Método para llamar este caso de uso desde otros CU
     *
     * @author felipe.cadena
     */
    public void cargarDesdeOtrosMB(ControlCalidadAvaluoRev revision) {

        this.revisionCC = this.getAvaluosService().obtenerControlCalidadAvaluoRevPorId(
            revision.getId());
        this.revisionCC = revision;
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(
            this.revisionCC.getControlCalidadAvaluo().getAvaluoId());
        this.profesionalControlCalidad = this.getAvaluosService().obtenerProfesionalAvaluoPorId(
            this.revisionCC.getControlCalidadAvaluo()
                .getProfesionalAvaluos().getId());
        this.fechaReporte = new Date();
        this.prediosAvaluo = (ArrayList<Predio>) this.getAvaluosService()
            .obtenerPrediosDeAvaluo(this.avaluo.getId());

        this.territorialAvaluo = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                String.valueOf(this.avaluo
                    .getEstructuraOrganizacionalCod()));
        this.ordenPractica = this.getAvaluosService().
            consultarOrdenPracticaPorAvaluoId(this.avaluo.getId());

        this.cargarCoordinadores();
        this.banderaOficioGenerado = false;

    }

    /**
     * Método para mostrar la vista previa del oficio remisorio
     *
     * @author felipe.cadena
     */
    public void verVistaPrevia() {
        LOGGER.debug("Generando oficio...");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        previsualizacionDocMB.setReporteDTO(this.oficioDto);
        LOGGER.debug("Oficio generado");
    }

    /**
     * Método para generar el oficio remisorio
     *
     * @author felipe.cadena
     */
    public void generarOficio() {
        LOGGER.debug("En ...#generarOficio");

        // TODO :: felipe.cadena :: Cambiar la url del reporte cuando esté definido
        // Nombre del reporte
        String urlReporte = EReporteServiceSNC.REPORTE_PRUEBA
            .getUrlReporte();

        // Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("NUMERO_RADICADO", "");

        try {
            this.oficioDto = this.reportsService.
                generarReporte(parameters, EReporteServiceSNC.REPORTE_PRUEBA, usuario);
        } catch (Exception ex) {
            LOGGER.error("error generando reporte: " + ex.getMessage());
            this.addMensajeError("Error al generar el reporte");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER,
                ex, urlReporte);
        }
    }

    /**
     * Método para confirmar el oficio por correo al coordinador del GIT de avalúos
     *
     * @author felipe.cadena
     */
    public void confirmarOficio() {
        LOGGER.debug("Confirmando oficio");

        this.generarOficio();
        this.generarDocumento();

        this.banderaOficioGenerado = true;

        LOGGER.debug("Oficio confirmado");
    }

    /**
     * Método para generar el documento del oficio desde el servidor de reo¿portes
     *
     * @author felipe.cadena
     *
     */
    private void generarDocumento() {

        TipoDocumento tipoDocumentoOP = new TipoDocumento();
        tipoDocumentoOP.setId(ETipoDocumento.OFICIOS_REMISORIOS
            .getId());
        tipoDocumentoOP.setNombre(ETipoDocumento.OFICIOS_REMISORIOS
            .getNombre());

        // this.oficioRemisorio = new Documento();
        this.oficioRemisorio.setArchivo(this.oficioDto.getArchivoReporte().getName());
        this.oficioRemisorio.setTipoDocumento(tipoDocumentoOP);
        this.oficioRemisorio.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.oficioRemisorio.setBloqueado(ESiNo.NO.getCodigo());
        this.oficioRemisorio.setUsuarioCreador(this.usuario.getLogin());
        this.oficioRemisorio.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        this.oficioRemisorio.setFechaLog(new Date(System.currentTimeMillis()));
        this.oficioRemisorio.setUsuarioLog(this.usuario.getLogin());
        this.oficioRemisorio.setDescripcion("Reporte control calidad GIT " +
             this.avaluo.getSecRadicado());

    }

    /**
     * Método para guardar el documento del oficio en alfresco y asignarlo a la revisión de control
     * de calidad correspondiente.
     *
     * @author felipe.cadena
     * @param tipoDocumento {@link TipoDocumento} del documento a guardar
     * @return
     */
    private void guardarDocumentoAlfresco(TipoDocumento tipoDocumento) {

        Solicitud solTemp = this.avaluo.getTramite().getSolicitud();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(
                solTemp.getNumero(), solTemp.getFecha(),
                tipoDocumento.getNombre(),
                this.oficioDto.getArchivoReporte().getAbsolutePath());

        this.oficioRemisorio = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, this.oficioRemisorio);

        this.revisionCC.setSoporteDocumento(oficioRemisorio);
        this.getAvaluosService().guardarActualizarControlCalidadAValuoRev(revisionCC, usuario);

        if (this.oficioRemisorio == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

    }

    /**
     * Método para guardar la información del oficio en base de datos y en alfresco.
     *
     * @author felipe.cadena
     *
     */
    public void guardarReporteDefinitivamente() {

        this.guardarDocumentoAlfresco(oficioRemisorio.getTipoDocumento());

        this.oficioRemisorio = this.getGeneralesService().guardarDocumento(
            this.oficioRemisorio);

        TramiteDocumento td = new TramiteDocumento();
        td.setDocumento(this.oficioRemisorio);
        td.setTramite(this.avaluo.getTramite());
        td.setFechaLog(new Date());
        td.setfecha(new Date());
        td.setUsuarioLog(this.usuario.getLogin());

        td = this.getTramiteService().actualizarTramiteDocumento(
            td);
        if (td == null) {
            this.addMensajeError("Error al relacionar el documento al tramite");
        }

    }

    /**
     * Método para enviar el oficio por correo al coordinador del GIT de avalúos
     *
     * @author felipe.cadena
     */
    public void enviarOficio() {

        this.guardarReporteDefinitivamente();

        List<String> destinatarios = new ArrayList<String>();
        List<String> destinatariosCC = new ArrayList<String>();

        for (ProfesionalAvaluo pa : this.avaluo.getAvaluadores()) {
            destinatariosCC.add(pa.getCorreoElectronico());
        }

        UsuarioDTO usuarioDirectorTerritorial = this
            .getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(
                this.territorialAvaluo.getNombre(),
                ERol.DIRECTOR_TERRITORIAL).get(0);
        UsuarioDTO usuarioCoordinadorGITAvaluos = this
            .getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(
                Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                ERol.COORDINADOR_GIT_AVALUOS).get(0);

        String nombresDestinatarios = "";
        String nombreProfesional;

        for (ProfesionalAvaluo pa : this.avaluo.getAvaluadores()) {
            destinatarios.add(pa.getCorreoElectronico());
        }

        if (this.avaluo.getEstructuraOrganizacionalCod().
            equals(Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL)) {
            nombreProfesional = this.usuario.getNombreCompleto();
            destinatarios.add(usuarioDirectorTerritorial.getLogin().concat(
                EDatosBaseCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC
                    .getDato()));
        } else {
            nombreProfesional = this.directorTerritorial.getNombreCompleto();
        }

        for (String string : destinatarios) {
            nombresDestinatarios = nombresDestinatarios.concat(string + "; ");
        }

        Object[] datosCorreo = new Object[6];
        datosCorreo[0] = nombresDestinatarios;//destinatarios
        datosCorreo[1] = this.avaluo.getTramite().getTipoTramiteCadenaCompleto();//tipo tramite
        datosCorreo[2] = this.avaluo.getSecRadicado();//sec. radicado
        datosCorreo[3] = nombreProfesional;//profesional
        datosCorreo[4] = this.avaluo.getSolicitante().getNombreCompleto();//solicitante
        datosCorreo[5] = this.coordinadorGITAvaluos.getNombreCompleto();//coordinador GIT

        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};

        if (this.oficioRemisorio != null) {
            adjuntos[0] = this.oficioDto.getArchivoReporte().getAbsolutePath();
            adjuntosNombres[0] = this.oficioDto.getArchivoReporte().getName();
        }
        LOGGER.debug("Url archivo " + adjuntos[0]);

        boolean notificacionExitosa = this
            .getGeneralesService()
            .enviarCorreoElectronicoConCuerpo(
                EPlantilla.MENSAJE_REPORTE_CONTROL_CALIDAD_GIT_AVALUOS,
                destinatarios, destinatariosCC, null, datosCorreo, adjuntos,
                adjuntosNombres);

        if (notificacionExitosa) {
            this.addMensajeInfo("Se envio exitosamente el oficio por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo enviar el Reporte control calidad GIT.");
            context.addCallbackParam("error", "error");
        }

//TODO :: felipe.cadena::mover el tramite para asignación de control de calidad en sede central.
    }

    /**
     * Método para reiniciar la variables cuando se cancela la generación del reporte.
     *
     * @author felipe.cadena
     */
    public void cancelarGenaracionOficio() {
        LOGGER.debug("cancelando oficio...");

        this.oficioDto = null;
        this.oficioRemisorio = null;

        LOGGER.debug("Oficio cancelado...");
    }
}
