package co.gov.igac.snc.web.locator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.excepciones.ExcepcionSNC;

/**
 * On a method call, handler is responsible for invoking the target method
 *
 * @author juan.mendez
 * @version 2.0
 *
 */
public class RemoteProxyHandler implements InvocationHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteProxyHandler.class);

    public static final int RETRY_LIMIT = 3;

    protected Object businessObject;

    /**
     *
     * @param businessObject
     */
    public RemoteProxyHandler(Object businessObject) {
        this.businessObject = businessObject;
    }

    /**
     *
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        try {
            result = method.invoke(businessObject, args);

        } catch (InvocationTargetException e) {
            if (e.getTargetException() instanceof ExcepcionSNC) {
                throw e.getTargetException();
            }
        }
        return result;

    }

}
