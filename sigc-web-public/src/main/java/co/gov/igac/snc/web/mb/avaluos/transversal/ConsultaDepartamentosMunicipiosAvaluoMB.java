package co.gov.igac.snc.web.mb.avaluos.transversal;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * MB para caso de uso CU-SA-AC-093 Consultar Departamentos y Municipios de un Avaluo
 *
 * @author rodrigo.hernandez
 *
 */
@Component("consultarDptoMunpioAvaluo")
@Scope("session")
public class ConsultaDepartamentosMunicipiosAvaluoMB extends SNCManagedBean {

    /**
     * Serial ID generado
     */
    private static final long serialVersionUID = 297583064013776644L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaDepartamentosMunicipiosAvaluoMB.class);

    /**
     * Lista de predios de un avaluo
     */
    private List<Municipio> listaMunicipiosAvaluo;

    /**
     * Sec. Radicado del Avalúo
     */
    private String secRadicado;

    /**
     * Id del avaluo
     */
    private Long idAvaluo;

    private Avaluo avaluoSeleccionado;

    public List<Municipio> getListaMunicipiosAvaluo() {
        return listaMunicipiosAvaluo;
    }

    public void setListaMunicipiosAvaluo(List<Municipio> listaMunicipiosAvaluo) {
        this.listaMunicipiosAvaluo = listaMunicipiosAvaluo;
    }

    public String getSecRadicado() {
        return secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    public Long getIdAvaluo() {
        return idAvaluo;
    }

    public void setIdAvaluo(Long idAvaluo) {
        this.idAvaluo = idAvaluo;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio ConsultaDepartamentosMunicipiosAvaluoMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin ConsultaDepartamentosMunicipiosAvaluoMB#init");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio ConsultaDepartamentosMunicipiosAvaluoMB#iniciarDatos");

        this.listaMunicipiosAvaluo = new ArrayList<Municipio>();

        LOGGER.debug("Fin ConsultaDepartamentosMunicipiosAvaluoMB#iniciarDatos");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para buscar los departamentos y municipios de un avaluo
     *
     * @author rodrigo.hernandez
     */
    private void buscarDepartamentosMunicipiosAvaluo() {
        LOGGER.debug(
            "Inicio ConsultaDepartamentosMunicipiosAvaluoMB#buscarDepartamentosMunicipiosAvaluo");

        listaMunicipiosAvaluo = this.getAvaluosService()
            .buscarDepartamentosMunicipiosAvaluo(this.idAvaluo);

        LOGGER.debug(
            "Fin ConsultaDepartamentosMunicipiosAvaluoMB#buscarDepartamentosMunicipiosAvaluo");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que se llama desde otros MB para consultar departamentos y municipios de un avalúo
     *
     * @param idAvaluo - Id del Avaluo
     *
     * @param secRadicado - Sec. Radicado del Avalúo
     *
     * @param listaDepartamentosMunicipios - Lista que se envía desde el MB que llama a este método
     *
     * @author rodrigo.hernandez
     */
    public void cargarDeptosMunisAvaluo() {
        LOGGER.debug(
            "Inicio ConsultaDepartamentosMunicipiosAvaluoMB#consultarDepartamentosMunicipiosAvaluo");

        this.idAvaluo = this.avaluoSeleccionado.getId();
        this.secRadicado = this.avaluoSeleccionado.getSecRadicado();

        this.buscarDepartamentosMunicipiosAvaluo();

        LOGGER.debug(
            "Fin ConsultaDepartamentosMunicipiosAvaluoMB#consultarDepartamentosMunicipiosAvaluo");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión de la consulta terminar </br>
     * </br> - Llamado desde la pagina
     * <b>consultarDepartamentosMunicipiosAvaluo.xhtml</b></br> - action para botón <b>Cerrar</b>
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaDepartamentosMunicipiosAvaluoMB#cerrar");

        UtilidadesWeb.removerManagedBean("consultarDptoMunpioAvaluo");

        LOGGER.debug("finalizando ConsultaDepartamentosMunicipiosAvaluoMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

}
