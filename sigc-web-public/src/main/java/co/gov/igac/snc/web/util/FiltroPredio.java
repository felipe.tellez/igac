/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util;

/**
 * Filtro para los campos de la consulta de predios
 *
 * @author pedro.garcia
 *
 */
public class FiltroPredio {

    private String territorialId;
    private String departamentoId;
    private String municipioId;

    /**
     * segmentos del número predial
     */
    private String numeroPredialS1;
    private String numeroPredialS2;
    private String numeroPredialS3;
    private String numeroPredialS4;
    private String numeroPredialS5;
    private String numeroPredialS6;
    private String numeroPredialS7;
    private String numeroPredialS8;
    private String numeroPredialS9;
    private String numeroPredialS10;
    private String numeroPredialS11;
    private String numeroPredialS12;

    private String direccionPredio;
    private String numeroPredialAnterior;
    private String numeroPredial;
    private String nombrePropietario;
    private String identificacionPropietario;
    private String usoConstruccionId;
    private String destinoPredioId;
    private String areaTerreno;
    private String areaTerrenoConstr;

    public String getTerritorialId() {
        return territorialId;
    }

    public void setTerritorialId(String territorialId) {
        this.territorialId = territorialId;
    }

    public String getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

//-------------------------------------------------------------------------------------------------
    public FiltroPredio() {
        this.departamentoId = "";
        this.municipioId = "";
        this.territorialId = "";
    }

    /**
     * @return the numeroPredialS1
     */
    public String getNumeroPredialS1() {
        return numeroPredialS1;
    }

    /**
     * @param numeroPredialS1 the numeroPredialS1 to set
     */
    public void setNumeroPredialS1(String numeroPredialS1) {
        this.numeroPredialS1 = numeroPredialS1;
    }

    /**
     * @return the numeroPredialS2
     */
    public String getNumeroPredialS2() {
        return numeroPredialS2;
    }

    /**
     * @param numeroPredialS2 the numeroPredialS2 to set
     */
    public void setNumeroPredialS2(String numeroPredialS2) {
        this.numeroPredialS2 = numeroPredialS2;
    }

    /**
     * @return the numeroPredialS3
     */
    public String getNumeroPredialS3() {
        return numeroPredialS3;
    }

    /**
     * @param numeroPredialS3 the numeroPredialS3 to set
     */
    public void setNumeroPredialS3(String numeroPredialS3) {
        this.numeroPredialS3 = numeroPredialS3;
    }

    /**
     * @return the numeroPredialS4
     */
    public String getNumeroPredialS4() {
        return numeroPredialS4;
    }

    /**
     * @param numeroPredialS4 the numeroPredialS4 to set
     */
    public void setNumeroPredialS4(String numeroPredialS4) {
        this.numeroPredialS4 = numeroPredialS4;
    }

    /**
     * @return the numeroPredialS5
     */
    public String getNumeroPredialS5() {
        return numeroPredialS5;
    }

    /**
     * @param numeroPredialS5 the numeroPredialS5 to set
     */
    public void setNumeroPredialS5(String numeroPredialS5) {
        this.numeroPredialS5 = numeroPredialS5;
    }

    /**
     * @return the numeroPredialS6
     */
    public String getNumeroPredialS6() {
        return numeroPredialS6;
    }

    /**
     * @param numeroPredialS6 the numeroPredialS6 to set
     */
    public void setNumeroPredialS6(String numeroPredialS6) {
        this.numeroPredialS6 = numeroPredialS6;
    }

    /**
     * @return the numeroPredialS7
     */
    public String getNumeroPredialS7() {
        return numeroPredialS7;
    }

    /**
     * @param numeroPredialS7 the numeroPredialS7 to set
     */
    public void setNumeroPredialS7(String numeroPredialS7) {
        this.numeroPredialS7 = numeroPredialS7;
    }

    /**
     * @return the numeroPredialS8
     */
    public String getNumeroPredialS8() {
        return numeroPredialS8;
    }

    /**
     * @param numeroPredialS8 the numeroPredialS8 to set
     */
    public void setNumeroPredialS8(String numeroPredialS8) {
        this.numeroPredialS8 = numeroPredialS8;
    }

    /**
     * @return the numeroPredialS9
     */
    public String getNumeroPredialS9() {
        return numeroPredialS9;
    }

    /**
     * @param numeroPredialS9 the numeroPredialS9 to set
     */
    public void setNumeroPredialS9(String numeroPredialS9) {
        this.numeroPredialS9 = numeroPredialS9;
    }

    /**
     * @return the numeroPredialS10
     */
    public String getNumeroPredialS10() {
        return numeroPredialS10;
    }

    /**
     * @param numeroPredialS10 the numeroPredialS10 to set
     */
    public void setNumeroPredialS10(String numeroPredialS10) {
        this.numeroPredialS10 = numeroPredialS10;
    }

    /**
     * @return the numeroPredialS11
     */
    public String getNumeroPredialS11() {
        return numeroPredialS11;
    }

    /**
     * @param numeroPredialS11 the numeroPredialS11 to set
     */
    public void setNumeroPredialS11(String numeroPredialS11) {
        this.numeroPredialS11 = numeroPredialS11;
    }

    /**
     * @return the numeroPredialS12
     */
    public String getNumeroPredialS12() {
        return numeroPredialS12;
    }

    /**
     * @param numeroPredialS12 the numeroPredialS12 to set
     */
    public void setNumeroPredialS12(String numeroPredialS12) {
        this.numeroPredialS12 = numeroPredialS12;
    }

    /**
     * @return the direccionPredio
     */
    public String getDireccionPredio() {
        return direccionPredio;
    }

    /**
     * @param direccionPredio the direccionPredio to set
     */
    public void setDireccionPredio(String direccionPredio) {
        this.direccionPredio = direccionPredio;
    }

    /**
     * @return the numeroPredialAnterior
     */
    public String getNumeroPredialAnterior() {
        return numeroPredialAnterior;
    }

    /**
     * @param numeroPredialAnterior the numeroPredialAnterior to set
     */
    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    /**
     * @return the numeroPredial
     */
    public String getNumeroPredial() {
        return numeroPredial;
    }

    /**
     * @param numeroPredial the numeroPredial to set
     */
    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    /**
     * @return the nombrePropietario
     */
    public String getNombrePropietario() {
        return nombrePropietario;
    }

    /**
     * @param nombrePropietario the nombrePropietario to set
     */
    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    /**
     * @return the identificacionPropietario
     */
    public String getIdentificacionPropietario() {
        return identificacionPropietario;
    }

    /**
     * @param identificacionPropietario the identificacionPropietario to set
     */
    public void setIdentificacionPropietario(String identificacionPropietario) {
        this.identificacionPropietario = identificacionPropietario;
    }

    /**
     * @return the usoConstruccionId
     */
    public String getUsoConstruccionId() {
        return usoConstruccionId;
    }

    /**
     * @param usoConstruccionId the usoConstruccionId to set
     */
    public void setUsoConstruccionId(String usoConstruccionId) {
        this.usoConstruccionId = usoConstruccionId;
    }

    /**
     * @return the destinoPredioId
     */
    public String getDestinoPredioId() {
        return destinoPredioId;
    }

    /**
     * @param destinoPredioId the destinoPredioId to set
     */
    public void setDestinoPredioId(String destinoPredioId) {
        this.destinoPredioId = destinoPredioId;
    }

    /**
     * @return the areaTerreno
     */
    public String getAreaTerreno() {
        return areaTerreno;
    }

    /**
     * @param areaTerreno the areaTerreno to set
     */
    public void setAreaTerreno(String areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    /**
     * @return the areaTerrenoConstr
     */
    public String getAreaTerrenoConstr() {
        return areaTerrenoConstr;
    }

    /**
     * @param areaTerrenoConstr the areaTerrenoConstr to set
     */
    public void setAreaTerrenoConstr(String areaTerrenoConstr) {
        this.areaTerrenoConstr = areaTerrenoConstr;
    }
}
