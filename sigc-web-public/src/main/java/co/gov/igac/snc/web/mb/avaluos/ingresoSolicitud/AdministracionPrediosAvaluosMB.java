package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.util.ECondicionJuridica;
import co.gov.igac.snc.persistence.util.ECondicionJuridicaNumPredial;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioZonaUnidadOrganica;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.EUnidadMedidaAreaTerreno;
import co.gov.igac.snc.util.EUnidadMedidaConstruccion;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.util.ConversorUnidadesArea;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB para caso de uso CU-SA-AC-046 Administrar Predios de Avalúo
 *
 * @author ariel.ortiz
 * @modified rodrigo.hernandez - Organización de código Adicion de funcionalidades faltantes +
 * Validacion de municipio para realizar busqueda de predio
 *
 * Corrección a funcionalidades + Adicionar predio + Cargar predios asociados a la solicitud
 *
 */
@Component("adminPrediosAvaluos")
@Scope("session")
public class AdministracionPrediosAvaluosMB extends SNCManagedBean {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -1750847579144637324L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionPrediosAvaluosMB.class);

    @Autowired
    private GeneralMB generalMB;

    // ------------------Atributos---------------------------------
    /**
     * Usuario loggeado en la sesión
     */
    private UsuarioDTO usuario;

    /**
     * Predio que actualmente se está modificando o creando
     */
    private SolicitudPredio predioSeleccionado;

    /**
     * Variables que muestran las opciones de los ComboBox
     */
    private List<SelectItem> departamentosCombo;
    private List<SelectItem> municipiosCombo;
    private List<SelectItem> circuloRegistralCombo;
    private List<SelectItem> tiposAvaluoPredialCombo;

    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;
    private List<CirculoRegistral> circulosRegistrales;

    /**
     * Variables que recogen la opcion seleccionada de los combos
     */
    private String departamentoCodSeleccionado;
    private String municipioCodSeleccionado;
    private String unidadMedidaAreaTerrenoSeleccionada;
    private String unidadMedidaConstruccionSeleccionada;

    /**
     * Variable para indicar si se va a realizar una modificacion masiva
     */
    private boolean banderaModificacionMasiva;

    /**
     * Variable para indicar si se va a usar la matricula antigua
     */
    private boolean banderaMatriculaAntigua;

    /**
     * Variable para indicar si el municipio pertenece a catastro descentralizado
     */
    private boolean banderaMunicipioDescentralizado;

    /**
     * Se activa para decir que se está editando un predio, si está en false es porque se está
     * creando uno nuevo
     */
    private boolean banderaEsEdicion;

    /**
     * bandera que indica el modo de lectura de los datos</br> <b>true: </b>Modo lectura </br>
     * <b>false: </b>Modo edición
     *
     */
    private boolean banderaModoLecturaHabilitado;

    /**
     * Bandera que indica si la persona es Natural o Juridica</br> <b>true:
     * </b>La persona es Natural </br> <b>false: </b>La persona es Juridica
     *
     */
    private boolean banderaSolicitanteEsPersonaNatural;

    /**
     * Bandera que indica si este MB es llamado desde DiligenciamientoSolicitudMB
     * (CU-SA-AC-121)</br> <b>true: </b>Es llamado
     * </br> <b>false: </b>No es llamado
     *
     */
    private boolean banderaEsCargadoDesdeMB121;

    /**
     * Bandera que indica si este MB es llamado desde (CU-SA-AC-48 - Crear Avalúo Asociado a la
     * Radicación)</br> <b>true: </b>Es llamado </br>
     * <b>false: </b>No es llamado
     *
     */
    private boolean banderaEsCargadoDesdeMB48;

    /**
     * Bandera que indica si se muestran todos los registro de la tabla de predios o se muestran
     * únicamente los principales
     */
    private boolean banderaTablaExpandidaPredio;

    /**
     * Variable para almacenar el codigo territorial del usuario
     */
    private String codigoTerritorial;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * Contiene la solicitud que es recibida desde el caso de uso principal
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Predios asociados a la solicitud
     */
    private List<SolicitudPredio> prediosSolicitud;

    /**
     * Predios seleccionados para modificación, eliminación o asociación de contactos
     */
    private SolicitudPredio[] prediosSeleccionados;

    /**
     * Solicitante asociado a la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Variable para almacenar el nombre del solicitante
     */
    private String nombreSolicitante;

    /**
     * Variable para almacenar el documento del solicitante
     */
    private String documentoSolicitante;

    // ------------------Getters & Setters-------------------------
    public List<SelectItem> getDepartamentosCombo() {
        return this.departamentosCombo;
    }

    public List<SelectItem> getMunicipiosCombo() {
        return this.municipiosCombo;
    }

    public String getDepartamentoCodSeleccionado() {
        return departamentoCodSeleccionado;
    }

    public void setDepartamentoCodSeleccionado(
        String departamentoCodSeleccionado) {
        this.departamentoCodSeleccionado = departamentoCodSeleccionado;
    }

    public String getMunicipioCodSeleccionado() {
        return municipioCodSeleccionado;
    }

    public void setMunicipioCodSeleccionado(String municipioCodSeleccionado) {
        this.municipioCodSeleccionado = municipioCodSeleccionado;
    }

    public List<SelectItem> getCirculoRegistralCombo() {
        return circuloRegistralCombo;
    }

    public void setCirculoRegistralCombo(List<SelectItem> circuloRegistralCombo) {
        this.circuloRegistralCombo = circuloRegistralCombo;
    }

    public List<SelectItem> getTiposAvaluoPredialCombo() {
        return tiposAvaluoPredialCombo;
    }

    public void setTiposAvaluoPredialCombo(
        List<SelectItem> tiposAvaluoPredialCombo) {
        this.tiposAvaluoPredialCombo = tiposAvaluoPredialCombo;
    }

    public boolean isBanderaModificacionMasiva() {
        return banderaModificacionMasiva;
    }

    public void setBanderaModificacionMasiva(boolean banderaModificacionMasiva) {
        this.banderaModificacionMasiva = banderaModificacionMasiva;
    }

    public boolean isBanderaMatriculaAntigua() {
        return banderaMatriculaAntigua;
    }

    public void setBanderaMatriculaAntigua(boolean banderaMatriculaAntigua) {
        this.banderaMatriculaAntigua = banderaMatriculaAntigua;
    }

    public SolicitudPredio getPredioSeleccionado() {
        return predioSeleccionado;
    }

    public void setPredioSeleccionado(SolicitudPredio predioSeleccionado) {
        this.predioSeleccionado = predioSeleccionado;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        // TODO :: christian.rodriguez :: usar 'this'. Revisar todos los getters
        // :: pedro.garcia
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(
        FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public String getUnidadMedidaAreaTerrenoSeleccionada() {
        return this.unidadMedidaAreaTerrenoSeleccionada;
    }

    public void setUnidadMedidaAreaTerrenoSeleccionada(
        String unidadMedidaAreaTerrenoSeleccionada) {
        this.unidadMedidaAreaTerrenoSeleccionada = unidadMedidaAreaTerrenoSeleccionada;
    }

    public String getUnidadMedidaConstruccionSeleccionada() {
        return unidadMedidaConstruccionSeleccionada;
    }

    public void setUnidadMedidaConstruccionSeleccionada(
        String unidadMedidaConstruccionSeleccionada) {
        this.unidadMedidaConstruccionSeleccionada = unidadMedidaConstruccionSeleccionada;
    }

    public List<SolicitudPredio> getPrediosSolicitud() {
        return prediosSolicitud;
    }

    public void setPrediosSolicitud(List<SolicitudPredio> prediosSolicitud) {
        this.prediosSolicitud = prediosSolicitud;
    }

    public SolicitudPredio[] getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(SolicitudPredio[] prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public SolicitanteSolicitud getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public boolean isBanderaEsEdicion() {
        return banderaEsEdicion;
    }

    public void setBanderaEsEdicion(boolean banderaEsEdicion) {
        this.banderaEsEdicion = banderaEsEdicion;
    }

    public boolean isBanderaModoLecturaHabilitado() {
        return banderaModoLecturaHabilitado;
    }

    public void setBanderaModoLecturaHabilitado(
        boolean banderaModoLecturaHabilitado) {
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
    }

    public boolean isBanderaSolicitanteEsPersonaNatural() {
        return banderaSolicitanteEsPersonaNatural;
    }

    public void setBanderaSolicitanteEsPersonaNatural(
        boolean banderaSolicitanteEsPersonaNatural) {
        this.banderaSolicitanteEsPersonaNatural = banderaSolicitanteEsPersonaNatural;
    }

    public boolean isBanderaEsCargadoDesdeMB121() {
        return banderaEsCargadoDesdeMB121;
    }

    public void setBanderaEsCargadoDesdeMB121(boolean banderaEsCargadoDesdeMB121) {
        this.banderaEsCargadoDesdeMB121 = banderaEsCargadoDesdeMB121;
    }

    public boolean isBanderaEsCargadoDesdeMB48() {
        return banderaEsCargadoDesdeMB48;
    }

    public void setBanderaEsCargadoDesdeMB48(boolean banderaEsCargadoDesdeMB48) {
        this.banderaEsCargadoDesdeMB48 = banderaEsCargadoDesdeMB48;
    }

    public boolean isBanderaTablaExpandidaPredio() {
        return banderaTablaExpandidaPredio;
    }

    public void setBanderaTablaExpandidaPredio(
        boolean banderaTablaExpandidaPredio) {
        this.banderaTablaExpandidaPredio = banderaTablaExpandidaPredio;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getDocumentoSolicitante() {
        return documentoSolicitante;
    }

    public void setDocumentoSolicitante(String documentoSolicitante) {
        this.documentoSolicitante = documentoSolicitante;
    }

    // -------------------------------------Métodos---------------------------------------
    @PostConstruct
    public void init() {

        this.iniciarBanderasDeLlamadosDeMB();

    }

    // ------------------------------------------------------------------------------------
    /**
     * Inicializar las variables requeridas por el caso de uso (Dah)
     *
     * @author christian.rodriguez
     */
    private void inicializarVariables() {

        this.codigoTerritorial = this.usuario.getCodigoTerritorial();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();

        this.predioSeleccionado = new SolicitudPredio();

        // Se valida si el solicitante es persona Natural
        if (this.banderaSolicitanteEsPersonaNatural) {
            // Se inicia el dato de nombre del solicitante
            this.nombreSolicitante = this.solicitante.getNombreCompleto();
            // Se inicia el dato de documento del solicitante
            this.documentoSolicitante = this.solicitante
                .getNumeroIdentificacion();
        } else {
            // Se inicia el dato de nombre del solicitante
            this.nombreSolicitante = this.solicitante.getSigla() != null ? this.solicitante
                .getRazonSocial() + "-" + this.solicitante.getSigla() :
                 this.solicitante.getRazonSocial() + "";
            // Se inicia el dato de documento del solicitante
            this.documentoSolicitante = this.solicitante
                .getNumeroIdentificacion() +
                 "-" +
                 this.solicitante.getDigitoVerificacion();
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Inicializa los combos que se van a suar en la pantalla de Adicionar/Modificar predio
     *
     * @author christian.rodriguez
     */
    private void inicializarCombos() {

        this.cargarComboDepartamentos();
        this.cargarComboMunicipios();

    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar las banderas que indican desde que MB es llamado este MB
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderasDeLlamadosDeMB() {
        this.banderaEsCargadoDesdeMB121 = false;
        this.banderaEsCargadoDesdeMB48 = false;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de Departamentos
     *
     * @author rodrigo.hernandez
     * @modified christian.rodriguez
     */
    private void cargarComboDepartamentos() {

        this.departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorTerritorial(
                usuario.getCodigoTerritorial());

        this.departamentosCombo = this.generalMB
            .getListaCodigoYDepartamentoPorTerritorial(this.codigoTerritorial);

        this.actualizarComboDepartamento();
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de Municipios
     *
     * @author rodrigo.hernandez
     * @modified christian.rodriguez
     */
    private void cargarComboMunicipios() {
        if (this.departamentoCodSeleccionado != null &&
             !this.departamentoCodSeleccionado.isEmpty()) {
            this.municipiosCombo = this.generalMB
                .getListaCodigoYMunicipioPorDepartamento(this.departamentoCodSeleccionado);
        }

        this.actualizarComboMunicipio();
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de circulosRegistrales de Municipios
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboCirculosRegistrales() {
        SelectItem item = null;

        // TODO::rodrigo.hernandez::revisar datos para circulos registrales.
        // Faltan datos en la BD. Tabla
        // IGAC_GENERALES.CIRCULO_REGISTRAL_MUNICIPIO
        this.circuloRegistralCombo = new ArrayList<SelectItem>();

        if (this.municipioCodSeleccionado != null &&
             !this.municipioCodSeleccionado.isEmpty()) {
            this.circulosRegistrales = this.getGeneralesService()
                .getCacheCirculosRegistralesPorMunicipio(
                    this.municipioCodSeleccionado);

            if (this.circulosRegistrales.size() > 0) {
                for (CirculoRegistral cr : this.circulosRegistrales) {
                    item = new SelectItem(cr.getCodigo(), cr.getNombre());
                    this.circuloRegistralCombo.add(item);
                }

                this.banderaMatriculaAntigua = false;
            } else {
                String mensaje = "El municipio seleccionado no tiene círculo registral asociado";
                this.addMensajeError(mensaje);

                this.circuloRegistralCombo = new ArrayList<SelectItem>();
                this.circuloRegistralCombo.add(new SelectItem("",
                    "No hay circulos registrales disponibles"));

                // Se deshabilita el combo de matriculas inmobiliarias
                this.banderaMatriculaAntigua = true;
            }
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que actualiza los municipios en cada cambio de departamento.
     *
     * @author rodrigo.hernandez
     * @modified christian.rodriguez
     */
//TODO :: rodrigo.hernandez :: si actualiza los municipios, ¿por qué se llama actualizarComboDepartamento? :: pedro.garcia
    private void actualizarComboDepartamento() {

        this.departamentosCombo = new ArrayList<SelectItem>();

        Collections
            .sort(this.departamentos, Departamento.getComparatorNombre());

        for (Departamento departamento : this.departamentos) {
            this.departamentosCombo.add(new SelectItem(
                departamento.getCodigo(), departamento.getNombre()));
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que actualiza los circulos registrales en cada cambio de municipio.
     *
     * @author rodrigo.hernandez
     * @modified christian.rodriguez
     */
    private void actualizarComboMunicipio() {
        this.municipiosCombo = new ArrayList<SelectItem>();

        if (this.departamentoCodSeleccionado != null) {
            this.municipios = new ArrayList<Municipio>();
            this.municipios = (ArrayList<Municipio>) this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoCodSeleccionado);

            Collections.sort(this.municipios, Municipio.getComparatorNombre());

            for (Municipio municipio : this.municipios) {
                this.municipiosCombo.add(new SelectItem(municipio.getCodigo(),
                    municipio.getNombre()));
            }
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Listener que ejecuta el método que actualiza los departamentos disponibles para la seleccion.
     * Tambien setea el código del departamento en el componente del número predial
     *
     * @author christian.rodriguez
     */
//TODO :: christian.rodriguez :: si es un listener, ¿por qué no se llama así? (seguir estándar). ¿de quién es listener? :: pedro.garcia
    public void departamentoCambio() {

        this.actualizarComboMunicipio();
        this.datosConsultaPredio
            .setNumeroPredialS1(this.departamentoCodSeleccionado);
    }

    // ------------------------------------------------------------------------------------
    /**
     * Actualiza el tipo de unidad de medida para el area de terreno. Si el tipo de predio es rural
     * entonces se selecciona la unidad "HECTAREA". En otro caso se selecciona "METRO CUADRADO"
     *
     * @author christian.rodriguez
     */
    public void tipoAvaluoPredioCambio() {

        if (this.predioSeleccionado != null &&
             this.predioSeleccionado.getTipoAvaluoPredio() != null) {

            if (this.predioSeleccionado.getTipoAvaluoPredio().equals(
                ETipoAvaluo.RURAL.getCodigo())) {
                this.unidadMedidaAreaTerrenoSeleccionada = EUnidadMedidaAreaTerreno.HECTAREA
                    .getCodigo();
            } else {
                this.unidadMedidaAreaTerrenoSeleccionada = EUnidadMedidaAreaTerreno.METRO_CUADRADO
                    .getCodigo();
            }
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Actualiza el campo condición jurídica. Si el campo numero 22 del numero predial es 6,7,8 o 9
     * se marca como "PROPIEDAD HORIZONTAL" en los demas casos se marca como "NO PROPIEDAD
     * HORIZONTAL"
     *
     * @author christian.rodriguez
     */
    public void condicionPropiedadCambio() {

        if (this.datosConsultaPredio != null &&
             this.datosConsultaPredio.getNumeroPredialS9() != null) {

            if (ECondicionJuridicaNumPredial.PROPIEDAD_HORIZONTAL.getCodigo()
                .contains(this.datosConsultaPredio.getNumeroPredialS9())) {
                this.predioSeleccionado
                    .setCondicionJuridica(ECondicionJuridica.PROPIEDAD_HORIZONTAL
                        .getCodigo());
            } else {
                this.predioSeleccionado
                    .setCondicionJuridica(ECondicionJuridica.NO_PROPIEDAD_HORIZONTAL
                        .getCodigo());
            }
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Listener que ejecuta el método que actualiza los departamentos disponibles para la seleccion.
     * Tambien setea el código del municipio en el componente del número predial
     *
     * @author christian.rodriguez
     */
    public void municipioCambio() {

        if (!this.banderaModificacionMasiva) {
            this.cargarComboCirculosRegistrales();
        }

        if (!this.municipioCodSeleccionado.isEmpty()) {
            this.datosConsultaPredio
                .setNumeroPredialS2(this.municipioCodSeleccionado
                    .substring(2));
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Listener que busca el predio asociado al número predial ingresado. Si no se encuentra ningún
     * predio se muestra error
     *
     * @author rodrigo.hernandez
     * @modified christian.rodriguez
     */
    public void buscarPredio() {
        LOGGER.debug("Iniciando AdministracionPrediosAvaluosMB#buscarPredio");

        this.datosConsultaPredio.assembleNumeroPredialFromSegments();

        if (!this.predioExisteEnLista(this.prediosSolicitud,
            this.datosConsultaPredio.getNumeroPredial())) {

            boolean banderaValidacionNumeroPredial = this
                .validarNumeroPredial();
            String mensaje = "";

            if (banderaValidacionNumeroPredial) {

                this.banderaMunicipioDescentralizado = this
                    .getGeneralesService()
                    .municipioPerteneceACatastroDescentralizado(
                        this.municipioCodSeleccionado);

                if (this.banderaMunicipioDescentralizado) {
                    // TODO::rodrigo.hernandez::buscar predios en catastro no
                    // centralizado
                } else {

                    Predio predioTemporal = this.getConservacionService()
                        .buscarPredioPorNumeroPredialParaAvaluoComercial(
                            this.predioSeleccionado.getNumeroPredial());

                    if (predioTemporal != null) {

                        this.cargarPredioSeleccionadoConPredio(predioTemporal);
                    } else {

                        mensaje = "No se encontró ningún predio con el número predial digitado";
                        this.addMensajeWarn(mensaje);
                    }
                }
            } else {
                mensaje = "Número predial inválido";
                this.addMensajeError(mensaje);
            }

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeError("Ya existe un predio asociado con el mismo número predial. " +
                 "Si desea editarlo use el botón de editar");
        }

        LOGGER.debug("Finalizando AdministracionPrediosAvaluosMB#buscarPredio");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que asocia y guarda los predios seleccionados a la solicitud actual
     *
     * @author christian.rodriguez
     * @modified rodrigo.hernandez - ajuste para asignar Solicitud a SolicitudPredio
     */
//TODO :: christian.rodriguez :: ¿es action de algún botón o link? :: pedro.garcia
    public void guardarPredio() {

        if (!this.banderaModificacionMasiva) {
            this.prediosSeleccionados = new SolicitudPredio[1];

            this.datosConsultaPredio.assembleNumeroPredialFromSegments();

            this.predioSeleccionado.setNumeroPredial(this.datosConsultaPredio
                .getNumeroPredial());

            this.prediosSeleccionados[0] = this.predioSeleccionado;
        }

        if (this.prediosSeleccionados != null) {

            for (SolicitudPredio predio : this.prediosSeleccionados) {

//TODO :: christian.rodriguez :: no es necesario ir a bd a consultar el departamento y el municipio :: pedro.garcia
                predio.setDepartamento(this.obtenerDepartamentoSeleccionado());
                predio.setMunicipio(this.obtenerMunicipioSeleccionado());

                predio.setUsuarioLog(this.usuario.getLogin());
                predio.setFechaLog(new Date());
                predio.setSolicitud(this.solicitudSeleccionada);

                String direccionNormalizada = this.getGeneralesService()
                    .obtenerDireccionNormalizada(predio.getDireccion());
                predio.setDireccion(direccionNormalizada);

                Double areaTerreno = predio.getAreaTerreno();
                if (predio.getTipoAvaluoPredio().equalsIgnoreCase(
                    ETipoAvaluo.RURAL.getCodigo())) {
                    predio.setAreaTerreno(ConversorUnidadesArea
                        .convertirAHectarea(areaTerreno,
                            this.unidadMedidaAreaTerrenoSeleccionada));
                } else {
                    predio.setAreaTerreno(ConversorUnidadesArea
                        .convertirAMetroCuadrado(areaTerreno,
                            this.unidadMedidaAreaTerrenoSeleccionada));
                }
            }

            if (this.banderaEsEdicion ||
                 !this.predioExisteEnLista(this.prediosSolicitud,
                    this.predioSeleccionado.getNumeroPredial())) {

                this.prediosSolicitud = this.getTramiteService()
                    .guardarActualizarSolicitudesPredios(
                        Arrays.asList(this.prediosSeleccionados));
            } else {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeError("Ya existe un predio asociado con el mismo número predial. " +
                     "Si desea editarlo use el botón de editar");
            }

            this.cargarPrediosAsociadosASolicitud();
        }
    }

    private boolean predioExisteEnLista(List<SolicitudPredio> predios,
        String numeroPredial) {

        if (predios != null) {
            for (SolicitudPredio predioTemp : predios) {

                if (predioTemp.getNumeroPredial().equals(numeroPredial)) {
                    return true;
                }
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Listener que inicia el proceso de edición de predios. Determina si es una modificación masiva
     * o no dependiendo del número de predios seleccionados
     *
     * @author christian.rodriguez
     */
    public void editarPredios() {

        if (this.prediosSeleccionados != null &&
             this.prediosSeleccionados.length > 1) {
            this.banderaModificacionMasiva = true;
        } else {
            this.banderaModificacionMasiva = false;
        }

        this.predioSeleccionado = this.prediosSeleccionados[0];

        this.departamentoCodSeleccionado = this.predioSeleccionado
            .getDepartamento().getCodigo();
        this.actualizarComboMunicipio();
        this.municipioCodSeleccionado = this.predioSeleccionado.getMunicipio()
            .getCodigo();

        if (!this.banderaModificacionMasiva) {
            this.cargarComboCirculosRegistrales();
        }

        this.datosConsultaPredio
            .disassembleNumeroPredialInSegments(this.predioSeleccionado
                .getNumeroPredial());

        this.unidadMedidaConstruccionSeleccionada = EUnidadMedidaConstruccion.METRO_CUADRADO
            .getCodigo();
        this.unidadMedidaAreaTerrenoSeleccionada = EUnidadMedidaAreaTerreno.METRO_CUADRADO
            .getCodigo();

        this.banderaEsEdicion = true;

    }

    // ------------------------------------------------------------------------------------
    /**
     * Elimina los predios seleccionados
     *
     * @author christian.rodriguez
     */
    public void eliminarPredios() {

        if (this.prediosSeleccionados != null &&
             this.prediosSeleccionados.length > 0) {

            List<SolicitudPredio> prediosAEliminar = new ArrayList<SolicitudPredio>();

            for (SolicitudPredio predio : this.prediosSeleccionados) {

                if (predio.getAvaluoPredioId() == null) {
                    prediosAEliminar.add(predio);
                } else {
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");

                    this.addMensajeError("El predio " +
                         predio.getNumeroPredial() +
                         " no se puede eliminar por estar asociado a un avalúo.");

                }
            }
            if (!prediosAEliminar.isEmpty()) {
                this.getTramiteService().eliminarSolicitudPredios(
                    prediosAEliminar);
                this.cargarPrediosAsociadosASolicitud();
            }
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Recupera un objeto con el Departamento asociado al código de municipio seleccionado
     *
     * @author christian.rodriguez
     * @return departamento seleccionado
     */
    private Departamento obtenerDepartamentoSeleccionado() {

        if (this.departamentoCodSeleccionado != null &&
             !this.departamentoCodSeleccionado.isEmpty()) {

            Departamento depto = this.getGeneralesService()
                .getCacheDepartamentoByCodigo(
                    this.departamentoCodSeleccionado);
            return depto;
        }
        return null;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Recupera un objeto con el Municipio asociado al código de municipio seleccionado
     *
     * @author christian.rodriguez
     * @return municipio seleccionado
     */
    private Municipio obtenerMunicipioSeleccionado() {

        if (this.departamentoCodSeleccionado != null &&
             !this.departamentoCodSeleccionado.isEmpty()) {

            Municipio muni = this.getGeneralesService()
                .getCacheMunicipioByCodigo(this.municipioCodSeleccionado);
            return muni;
        }
        return null;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método auxiliar para cargar al predio seleccionado (SolicitudPredio) los datos de un predio
     * (Predio) recuperado generalmente de la BD por el método buscar
     *
     * @author christian.rodriguez
     * @param predioTemporal Predio con los datos a copiar en el solicitud predio seleccionado
     */
    private void cargarPredioSeleccionadoConPredio(Predio predioTemporal) {

        if (this.predioSeleccionado != null) {

            this.predioSeleccionado.setAreaConstruccion(predioTemporal
                .getAreaConstruccion());
            this.predioSeleccionado.setAreaTerreno(predioTemporal
                .getAreaTerreno());
            this.predioSeleccionado.setDireccion(predioTemporal
                .getDireccionPrincipal());
            this.predioSeleccionado.setNumeroRegistro(predioTemporal
                .getNumeroRegistro());
            this.predioSeleccionado.setTipoAvaluoPredio(predioTemporal
                .getTipoAvaluoPredio());
            this.predioSeleccionado.setPredioId(predioTemporal.getId());
            this.predioSeleccionado.setCirculoRegistral(predioTemporal
                .getCirculoRegistral().getCodigo());

            if (predioTemporal.getCirculoRegistral() != null) {
                this.predioSeleccionado
                    .setCirculoRegistral(predioTemporal
                        .getCirculoRegistral().getCodigo() != null ? predioTemporal
                                .getCirculoRegistral().getCodigo() : "");
            } else {
                this.predioSeleccionado.setCirculoRegistral("");
            }

            if (predioTemporal.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_7.getCodigo()) ||
                 predioTemporal.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_8.getCodigo()) ||
                 predioTemporal.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_9.getCodigo())) {
                this.predioSeleccionado
                    .setCondicionJuridica(ECondicionJuridica.PROPIEDAD_HORIZONTAL
                        .getCodigo());
            } else {
                this.predioSeleccionado
                    .setCondicionJuridica(ECondicionJuridica.NO_PROPIEDAD_HORIZONTAL
                        .getCodigo());
            }

            if (predioTemporal.getZonaUnidadOrganica().equals(
                EPredioZonaUnidadOrganica.RURAL.getCodigo()) ||
                 this.predioSeleccionado.getTipoAvaluoPredio().equals(
                    ETipoAvaluo.RURAL.getCodigo())) {
                this.unidadMedidaAreaTerrenoSeleccionada = EUnidadMedidaAreaTerreno.HECTAREA
                    .getCodigo();
            } else {
                this.unidadMedidaAreaTerrenoSeleccionada = EUnidadMedidaAreaTerreno.METRO_CUADRADO
                    .getCodigo();
            }
        }

    }

    // ------------------------------------------------------------------------------------
    /**
     * Valida que el número predial ingresado este correctamente conformado en sus campos
     *
     * @author rodrigo.hernandez
     * @return true si es valido, false en otro caso
     */
    private boolean validarNumeroPredial() {
        boolean result = true;
        boolean banderaValidacion = this.datosConsultaPredio
            .validateSegmentsNumeroPredial();

        if (banderaValidacion) {

            result = true;

            this.predioSeleccionado.setNumeroPredial(this.datosConsultaPredio
                .getNumeroPredial());

            if (this.datosConsultaPredio.getNumeroPredialS1() == null ||
                 this.datosConsultaPredio.getNumeroPredialS1().equals(
                    "00") ||
                 this.datosConsultaPredio.getNumeroPredialS2() == null ||
                 this.datosConsultaPredio.getNumeroPredialS2().equals(
                    "000")) {

                this.predioSeleccionado.setNumeroPredial("");
                result = false;
            }
        } else {
            this.predioSeleccionado.setNumeroPredial("");
            result = false;
        }

        return result;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Listener que reinicializa la variable de predio seleccionado
     *
     * @author christian.rodriguez
     */
    public void adicionarPredio() {

        this.predioSeleccionado = new SolicitudPredio();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();

        this.departamentoCodSeleccionado = "";
        this.municipioCodSeleccionado = "";

        // Valores predeterminados
        this.unidadMedidaConstruccionSeleccionada = EUnidadMedidaConstruccion.METRO_CUADRADO
            .getCodigo();
        this.unidadMedidaAreaTerrenoSeleccionada = EUnidadMedidaAreaTerreno.METRO_CUADRADO
            .getCodigo();

        this.banderaModificacionMasiva = false;
        this.banderaEsEdicion = false;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Carga desde la base de datos los solicitudpredio asociados a la solicitud
     *
     * @author christian.rodriguez
     */
    public void cargarPrediosAsociadosASolicitud() {

        this.prediosSolicitud = this.getTramiteService()
            .buscarSolicitudesPredioBySolicitudId(
                this.solicitudSeleccionada.getId());
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar variables desde DiligenciamientoSolicitudMB
     *
     * @author rodrigo.hernandez
     *
     * @param solicitud - Solicitud en curso
     * @param listaPredios - Lista de predios asociados a la solicitud
     * @param banderaModoLecturaHabilitado - bandera que indica el modo de lectura de los datos</br>
     * <b>true: </b>Modo lectura </br> <b>false: </b>Modo edición
     * </br> </br>
     * @param usuario - Usuario en sesión
     */
    public void cargarVariablesExternas(Solicitud solicitud,
        SolicitanteSolicitud solicitante,
        List<SolicitudPredio> listaPredios,
        boolean banderaModoLecturaHabilitado, UsuarioDTO usuario) {

        LOGGER.debug("Iniciando AdministracionPrediosAvaluosMB#cargarVariablesExternas");

        this.prediosSeleccionados = new SolicitudPredio[]{};

        this.solicitudSeleccionada = solicitud;
        this.solicitante = solicitante;
        this.prediosSolicitud = listaPredios;
        this.usuario = usuario;
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
        this.banderaSolicitanteEsPersonaNatural = this
            .esSolicitanteSeleccionadoPersonaNatural(solicitante);
        this.banderaEsCargadoDesdeMB121 = true;

        this.inicializarVariables();
        this.inicializarCombos();

        LOGGER.debug("Finalizando AdministracionPrediosAvaluosMB#cargarVariablesExternas");

    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para realizar validaciones necesarias para el almacenamiento de la información
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public boolean validarInfo() {
        boolean result = true;
        String mensaje = "";

        // Se valida que existan predios asociados a la solicitud
        if (this.prediosSolicitud.size() == 0) {
            mensaje = "No hay predios para seleccionar";
            this.addMensajeError(mensaje);
            result = false;

        } else {
            // Se valida si los predios de la solicitud tienen un contacto
            // asociado
            for (SolicitudPredio predio : this.prediosSolicitud) {
                if (predio.getContacto() == null) {
                    mensaje = "Hay al menos un predio sin un contacto asociado";
                    this.addMensajeError(mensaje);
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para validar si el solicitante es Persona Natural
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true: </b>Es persona natural </br> <b>false: </b>Es persona juridica </br>
     */
    private boolean esSolicitanteSeleccionadoPersonaNatural(
        SolicitanteSolicitud solicitante) {
        boolean result = true;

        if (solicitante.getTipoPersona().equals(
            EPersonaTipoPersona.JURIDICA.getCodigo())) {
            result = false;
        }

        return result;
    }
}
