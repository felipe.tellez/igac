package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.util.Date;
import java.util.List;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoCesion;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * MB para CU-SA-AC-051 - Administrar información de contrato activo del avaluador
 *
 * @author rodrigo.hernandez
 *
 */
//TODO :: rodrigo.hernandez :: 11-10-2012 :: ¿un MB sin init?. Al menos con una línea de debug. Por estándar :: pedro.garcia
@Component("adminInfoContratoActivoAv")
@Scope("session")
public class AdminInfoContratoActivoAvaluadorMB extends SNCManagedBean {

    private static final long serialVersionUID = 7604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdminInfoContratoActivoAvaluadorMB.class);

    /**
     * ManagedBean del caso de uso 83
     */
    private ConsultaPagosParafiscalesAvaluadorExternoMB consultaPagosParafiscalesMB;

    /**
     * ManagedBean del caso de uso 84
     */
    private ConsultaAdicionesContratoActivoMB consultaAdicionesContratoMB;

    /**
     * ManagedBean del caso de uso 82
     */
    private AdminPagosContratoAvaluadorExternoMB adminPagosContratosMB;

    /**
     * ManagedBean del caso de uso 85
     */
    private AdministracionValoresComprometidosMB adminValoresCompromeditosMB;

    private VProfesionalAvaluosContrato avaluadorSeleccionado;

    private ProfesionalAvaluosContrato contrato;

    private List<ProfesionalContratoCesion> listaCesionesContrato;

    /**
     * Variable que almacena el valor del contrato MÁS las adiciones
     */
    private Double valorContratoAdiciones;

    /**
     * Variable que almacena el valor de las adiciones del contrato
     */
    private Double valorAdiciones;

    /**
     * Variable que almacena el valor del saldo por ejecutar del contrato
     */
    private Double saldoPorEjecutar;

    /**
     * Variable que almacena el valor ejecutado del contrato
     */
    private Double valorEjecutado;

    /**
     * Variable que almacena la fecha final del contrato con adiciones
     */
    private Date fechaFinalContratoAdiciones;

    private String dummy;

    // ----- Setters y getters ------------------
    public VProfesionalAvaluosContrato getAvaluadorSeleccionado() {
        return avaluadorSeleccionado;
    }

    public void setAvaluadorSeleccionado(VProfesionalAvaluosContrato avaluadorSeleccionado) {
        this.avaluadorSeleccionado = avaluadorSeleccionado;
    }

    public String getDummy() {
        return dummy;
    }

    public void setDummy(String dummy) {
        this.dummy = dummy;
    }

    public ProfesionalAvaluosContrato getContrato() {
        return contrato;
    }

    public void setContrato(ProfesionalAvaluosContrato contrato) {
        this.contrato = contrato;
    }

    public Double getValorContratoAdiciones() {
        return valorContratoAdiciones;
    }

    public void setValorContratoAdiciones(Double valorContratoAdiciones) {
        this.valorContratoAdiciones = valorContratoAdiciones;
    }

    public Double getValorAdiciones() {
        return valorAdiciones;
    }

    public void setValorAdiciones(Double valorAdiciones) {
        this.valorAdiciones = valorAdiciones;
    }

    public Double getSaldoPorEjecutar() {
        return saldoPorEjecutar;
    }

    public void setSaldoPorEjecutar(Double saldoPorEjecutar) {
        this.saldoPorEjecutar = saldoPorEjecutar;
    }

    public Double getValorEjecutado() {
        return valorEjecutado;
    }

    public void setValorEjecutado(Double valorEjecutado) {
        this.valorEjecutado = valorEjecutado;
    }

    public Date getFechaFinalContratoAdiciones() {
        return fechaFinalContratoAdiciones;
    }

    public void setFechaFinalContratoAdiciones(Date fechaFinalContratoAdiciones) {
        this.fechaFinalContratoAdiciones = fechaFinalContratoAdiciones;
    }

    public List<ProfesionalContratoCesion> getListaCesionesContrato() {
        return listaCesionesContrato;
    }

    public void setListaCesionesContrato(List<ProfesionalContratoCesion> listaCesionesContrato) {
        this.listaCesionesContrato = listaCesionesContrato;
    }

    // -------------------------------------------------------------------------------
    /**
     * Método para calcular el valor del contrato MAS sus adiciones
     *
     * @author rodrigo.hernandez
     */
    private void obtenerValorContratoAdiciones() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#obtenerValorContratoAdiciones");

        this.valorContratoAdiciones = this.contrato.getValor() + this.valorAdiciones;

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#obtenerValorContratoAdiciones");
    }

    // -------------------------------------------------------------------------------
    /**
     * Método para calcular el valor de las adiciones del contrato
     *
     * @author rodrigo.hernandez
     */
    private void obtenerValorAdiciones() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#obtenerValorAdiciones");

        this.valorAdiciones = this.getAvaluosService()
            .obtenerTotalAdicionesPorIdProfesionalContrato(this.contrato.getId());

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#obtenerValorAdiciones");
    }

    // -------------------------------------------------------------------------------
    /**
     * Método para calcular el saldo por ejecutar del contrato
     *
     * @author rodrigo.hernandez
     */
    private void obtenerSaldoPorEjecutar() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#obtenerSaldoPorEjecutar");

        this.saldoPorEjecutar = this.valorContratoAdiciones - this.valorEjecutado;

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#obtenerSaldoPorEjecutar");
    }

    // -------------------------------------------------------------------------------
    /**
     * Método para calcular el valor ejecutado del contrato
     *
     * @author rodrigo.hernandez
     */
    private void obtenerValorEjecutado() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#obtenerValorEjecutado");

        this.valorEjecutado = this.getAvaluosService().calcularValorEjecutadoPAContrato(
            this.contrato.getId());

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#obtenerValorEjecutado");
    }

    // -------------------------------------------------------------------------------
    /**
     * Método para calcular la fecha final del contrato teniendo en cuenta sus adiciones
     *
     * @author rodrigo.hernandez
     */
    private void obtenerFechaFinalContratoAdiciones() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#obtenerFechaFinalContratoAdiciones");

        this.fechaFinalContratoAdiciones = this.getAvaluosService()
            .calcularFechaFinalPAContratoAdiciones(this.contrato.getId());

        // Se valida que la fecha consultada no sea null
        if (this.fechaFinalContratoAdiciones != null) {
            // Se compara la fecha consultada es menor que la fecha_hasta del
            // contrato
            if (this.contrato.getFechaHasta().after(this.fechaFinalContratoAdiciones)) {
                this.fechaFinalContratoAdiciones = this.contrato.getFechaHasta();
            }
        } // La fecha puede ser null ya que no tiene adiciones al contrato
        else {
            this.fechaFinalContratoAdiciones = this.contrato.getFechaHasta();
        }

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#obtenerFechaFinalContratoAdiciones");
    }

    // --------------------------------------------------------------------------------------------------------
    /**
     * Método para cargar la información del contrato
     *
     * @author rodrigo.hernandez
     */
    private void cargarInformacionContrato() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#cargarInformacionContrato");

        this.contrato = this.getAvaluosService().obtenerContratoAvaluadorPorId(
            this.avaluadorSeleccionado.getContratoId());

        this.obtenerFechaFinalContratoAdiciones();
        this.obtenerValorAdiciones();
        this.obtenerValorContratoAdiciones();

        this.obtenerValorEjecutado();
        this.obtenerSaldoPorEjecutar();

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#cargarInformacionContrato");
    }

    public void cargarListaCesionesContrato() {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#cargarListaCesionesContrato");

        this.listaCesionesContrato = this.getAvaluosService().obtenerCesionesDeContratoActivo(
            this.contrato.getId());

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#cargarListaCesionesContrato");
    }

    // --------------------------------------------------------------------------------------------------------
    /**
     * Método que carga informacion del avaluador </br> <i><b>Llamado desde MB del CU-SA-AC-35
     * Consultar Avaluadores Externos</b></i>
     *
     * @param avaluador
     */
    public void cargarDesdeCU35(VProfesionalAvaluosContrato avaluador) {
        LOGGER.debug("Inicio AdminInfoContratoActivoAvaluadorMB#cargarDesdeCU35");

        this.avaluadorSeleccionado = avaluador;
        this.cargarInformacionContrato();

        LOGGER.debug("Fin AdminInfoContratoActivoAvaluadorMB#cargarDesdeCU35");
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el caso de uso 86 para ver los pagos parafiscales asociados al avaluador y
     * el contrato seleccionados
     *
     * @author christian.rodriguez
     */
    public void cargarPagosParafiscales() {

        if (this.contrato != null && this.avaluadorSeleccionado != null) {

            this.consultaPagosParafiscalesMB =
                (ConsultaPagosParafiscalesAvaluadorExternoMB) UtilidadesWeb
                    .getManagedBean("consultaPagosParafiscales");
            this.consultaPagosParafiscalesMB.cargarDesdeOtrosMB(this.contrato,
                this.avaluadorSeleccionado.getProfesionalAvaluosId());

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("Debe seleccionar un contrato y un avaluador.");
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el caso de uso 85 para adminsitrar los valores comprometidos del contrato
     *
     * @author christian.rodriguez
     */
    public void cargarAdminValoresCompromeditosEjecucion() {

        if (this.contrato != null && this.avaluadorSeleccionado != null) {

            this.adminValoresCompromeditosMB = (AdministracionValoresComprometidosMB) UtilidadesWeb
                .getManagedBean("administrarValoresComprometidos");
            this.adminValoresCompromeditosMB
                .cargarDesdeCU051(this.avaluadorSeleccionado);

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("Debe seleccionar un contrato y un avaluador.");
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el caso de uso 82 para adminsitrar los pagos del contrato
     *
     * @author christian.rodriguez
     */
    public void cargarAdminPagosContrato() {

        if (this.contrato != null && this.avaluadorSeleccionado != null) {

            this.adminPagosContratosMB = (AdminPagosContratoAvaluadorExternoMB) UtilidadesWeb
                .getManagedBean("adminPagosContratoAvaluador");
            this.adminPagosContratosMB
                .cargarDesdeOtrosMB(this.contrato,
                    this.avaluadorSeleccionado
                        .getProfesionalAvaluosId(), false);

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("Debe seleccionar un contrato y un avaluador.");
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el caso de uso 84 para ver las adiciones realizadas a un contrato
     *
     * @author christian.rodriguez
     */
    public void cargarAdicionesContrato() {

        if (this.avaluadorSeleccionado != null) {

            this.consultaAdicionesContratoMB = (ConsultaAdicionesContratoActivoMB) UtilidadesWeb
                .getManagedBean("consultaAdicionesContratoActivo");
            this.consultaAdicionesContratoMB.cargarDesdeCU051(this.avaluadorSeleccionado);

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("Debe seleccionar un contrato y un avaluador.");
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el caso de uso 84 para ver las adiciones realizadas a un contrato
     *
     * @author christian.rodriguez
     */
    public void cargarConsultaContratoActivo() {

        if (this.avaluadorSeleccionado != null) {

            this.consultaAdicionesContratoMB = (ConsultaAdicionesContratoActivoMB) UtilidadesWeb
                .getManagedBean("consultaAdicionesContratoActivo");
            this.consultaAdicionesContratoMB
                .cargarDesdeCU051(this.avaluadorSeleccionado);

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("Debe seleccionar un contrato y un avaluador.");
        }
    }

}
