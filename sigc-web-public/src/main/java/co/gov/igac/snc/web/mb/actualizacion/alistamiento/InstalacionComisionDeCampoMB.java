package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoRecursoHumano;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 *
 * @author javier.barajas
 * @version 1.0
 * @cu 216 Instalacion Comision Creacion de metodos para cargar los datos en la tabla de enventos
 * asistentes asi como la creacion del actualizacion y los asistentes a la instalacion tambien se
 * genera el acta de instalacion de comisión
 *
 */
@Component("instalarComisionDeCampo")
@Scope("session")
public class InstalacionComisionDeCampoMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(InstalacionComisionDeCampoMB.class);

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Variable maneja criterios de unificacion
     */
    private String criteriosUnificacion;
    /**
     * Variable para alamacenar la identificacion del invitado
     */
    private String identificacionInvitado;
    /**
     * Variable para alamacenar el cargo del invitado
     */
    private String cargoInvitado;

    /**
     * Variable para alamacenar institucion del invitado
     */
    private String institucionInvitado;
    /**
     * Variable para alamacenar la lista de responsable de actualizacion
     */
    private List<ActualizacionResponsable> responsablesActualizacion;

    /**
     * Variable para alamacenar la Actualizacion
     */
    private Actualizacion actualizacionSeleccionada;

    /**
     * Variable para alamacenar Actualizacion Evento
     */
    private ActualizacionEvento actualizacionEventoSelecionada;

    /**
     * Variable para alamacenar la seleccioin del Evento Asistente
     */
    private EventoAsistente asistenteSeleccionado;

    /**
     * Variable para alamacenar la lista de Recurso Humano
     */
    private List<RecursoHumano> recursoHumanoActualizacion;

    /**
     * Variable para alamacenar el usuario de la aplicacion
     */
    private UsuarioDTO usuario;

    /**
     * Variable para alamacenar la fecha de la instalacion de la comision
     */
    private Date fechaComision;

    /**
     * Variable para alamacenar la lista de Recurso Humano
     */
    private String nombreInvitado;
    /**
     * Lista asistentes nuevos
     */
    private List<EventoAsistente> listaAsistentesNuevos;

    /**
     * lista asistentes para carga en la tabla
     */
    private List<EventoAsistente> listaAsistentesTabla;

    /**
     * Arreglo de select item para seleccionar el cargo
     */
    private ArrayList<SelectItem> tipoCargoV;

    /**
     * Manejo de reportes
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Variable usada en el pickList
     */
    private DualListModel<String> funcionariosAsistentes;

    /**
     * Lista asistentes invitados para carga el el picklist
     */
    private List<String> listaAsistentesInvitados;
    /**
     * Lista asistentes invitados para carga el el picklist
     */
    private List<String> listaAsistentes;
    /**
     * Bandera para agregar asistentes
     */
    private boolean banderaAgregarAsistente;

    /**
     * Documento donde se guarda el acta de la instalación
     */
    private Documento documento;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDTO;

    /**
     * Variable de contexto
     */
    @Autowired
    private IContextListener context;

    @PostConstruct
    public void init() {

        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 24/10/12
        // Obtener la actualizacióin del arbol de tareas.
        this.setActualizacionSeleccionada(this.getActualizacionService()
            .recuperarActualizacionPorId(450L));

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.listaAsistentesInvitados = new ArrayList<String>();
        this.listaAsistentes = new ArrayList<String>();
        this.banderaAgregarAsistente = false;
        //this.actualizacionSeleccionada = this.getActualizacionService().getActualizacionConResponsables(450L);
        this.responsablesActualizacion = this.actualizacionSeleccionada.
            getActualizacionResponsables();
        this.listaAsistentesNuevos = new ArrayList<EventoAsistente>();
        this.listaAsistentesTabla = new ArrayList<EventoAsistente>();
        this.cargarAsistentesTabla();
        this.funcionariosAsistentes = new DualListModel<String>(this.listaAsistentes,
            this.listaAsistentesInvitados);
        //this.funcionariosYContratistas = new DualListModel<RecursoHumano>(this.sourceFuncionariosYContratistas,this.targetFuncionariosYContratistas);
        this.tipoCargoV = new ArrayList<SelectItem>();
        this.tipoCargoV.add(new SelectItem(ETipoRecursoHumano.CONTRATISTA.getCodigo(),
            ETipoRecursoHumano.CONTRATISTA.name()));
        this.tipoCargoV.add(new SelectItem(ETipoRecursoHumano.FUNCIONARIO.getCodigo(),
            ETipoRecursoHumano.FUNCIONARIO.name()));
    }

    /* --------------------------------------------------------------------------------------------------------------
     * Getters y Setters
     *
     * --------------------------------------------------------------------------------------------------------------
     */
    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public ArrayList<SelectItem> getTipoCargoV() {
        return tipoCargoV;
    }

    public void setTipoCargoV(ArrayList<SelectItem> tipoCargoV) {

        this.tipoCargoV = tipoCargoV;
    }

    public EventoAsistente getAsistenteSeleccionado() {
        return this.asistenteSeleccionado;
    }

    public void setAsistenteSeleccionado(EventoAsistente asistenteSeleccionado) {
        this.asistenteSeleccionado = asistenteSeleccionado;
    }

    public List<EventoAsistente> getListaAsistentesTabla() {
        return listaAsistentesTabla;
    }

    public void setListaAsistentesTabla(List<EventoAsistente> listaAsistentesTabla) {
        this.listaAsistentesTabla = listaAsistentesTabla;
    }

    public ActualizacionEvento getActualizacionEventoSelecionada() {
        return this.actualizacionEventoSelecionada;
    }

    public void setActualizacionEventoSelecionada(
        ActualizacionEvento actualizacionEventoSelecionada) {
        this.actualizacionEventoSelecionada = actualizacionEventoSelecionada;
    }

    public List<RecursoHumano> getRecursoHumanoActualizacion() {
        return this.recursoHumanoActualizacion;
    }

    public void setRecursoHumanoActualizacion(
        List<RecursoHumano> recursoHumanoActualizacion) {
        this.recursoHumanoActualizacion = recursoHumanoActualizacion;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return this.actualizacionSeleccionada;
    }

    public void setActualizacionSeleccionada(Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public List<String> getListaAsistentes() {
        return this.listaAsistentes;
    }

    public void setListaAsistentes(List<String> listaAsistentes) {
        this.listaAsistentes = listaAsistentes;
    }

    public String getCriteriosUnificacion() {

        return this.criteriosUnificacion;
    }

    public List<ActualizacionResponsable> getResponsablesActualizacion() {
        return this.responsablesActualizacion;
    }

    public void setResponsablesActualizacion(
        List<ActualizacionResponsable> responsablesActualizacion) {
        this.responsablesActualizacion = responsablesActualizacion;
    }

    public void setCriteriosUnificacion(String criteriosUnificacion) {
        this.criteriosUnificacion = criteriosUnificacion;
    }

    public String getIdentificacionInvitado() {
        return identificacionInvitado;
    }

    public void setIdentificacionInvitado(String identificacionInvitado) {
        this.identificacionInvitado = identificacionInvitado;
    }

    public String getCargoInvitado() {
        return cargoInvitado;
    }

    public void setCargoInvitado(String cargoInvitado) {
        this.cargoInvitado = cargoInvitado;
    }

    public String getInstitucionInvitado() {
        return institucionInvitado;
    }

    public void setInstitucionInvitado(String institucionInvitado) {
        this.institucionInvitado = institucionInvitado;
    }

    public Date getFechaComision() {
        return fechaComision;
    }

    public void setFechaComision(Date fechaComision) {
        this.fechaComision = fechaComision;
    }

    public DualListModel<String> getFuncionariosAsistentes() {
        return funcionariosAsistentes;
    }

    public void setFuncionariosAsistentes(DualListModel<String> funcionariosAsistentes) {
        this.funcionariosAsistentes = funcionariosAsistentes;
    }

    public List<String> getListaAsistentesInvitados() {
        return listaAsistentesInvitados;
    }

    public void setListaAsistentesInvitados(List<String> listaAsistentesInvitados) {
        this.listaAsistentesInvitados = listaAsistentesInvitados;
    }

    public String getNombreInvitado() {
        return nombreInvitado;
    }

    public void setNombreInvitado(String nombreInvitado) {
        this.nombreInvitado = nombreInvitado;
    }

    public boolean isBanderaAgregarAsistente() {
        return banderaAgregarAsistente;
    }

    public void setBanderaAgregarAsistente(boolean banderaAgregarAsistente) {
        this.banderaAgregarAsistente = banderaAgregarAsistente;
    }

    public ReportesUtil getReportsService() {
        return reportsService;
    }

    public void setReportsService(ReportesUtil reportsService) {
        this.reportsService = reportsService;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    /*
     * ----------------------------------------------------------------------------------------------------------
     * METODOS
     * ----------------------------------------------------------------------------------------------------------
     */
    /**
     * Radica el crea y guarda los objetos para guardar el acta y generala
     *
     * @author javier.barajas
     */
    public void cargarActa() {

        LOGGER.debug("--------------ENTRO CARGAR ACTA");
        this.documento = new Documento();

        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.MEMORANDO_DE_AMPLIACION_DE_COMISION.getId());
        this.documento.setTipoDocumento(tipoDocumento);
        this.documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.documento.setBloqueado(ESiNo.NO.getCodigo());
        this.documento.setUsuarioCreador(this.usuario.getLogin());
        this.documento.setUsuarioLog(this.usuario.getLogin());
        this.documento.setFechaLog(new Date(System.currentTimeMillis()));
        this.documento.setFechaDocumento(this.fechaComision);
        this.documento.setArchivo("TEMPORAL");
        this.documento.setEstructuraOrganizacionalCod(this.usuario.
            getCodigoEstructuraOrganizacional());

        try {
            this.documento = this.getGeneralesService().guardarDocumento(this.documento);

        } catch (Exception e) {
            LOGGER.debug("Error al cargar el acta de instalación de campo");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo cargar el acta de la instalación de campo",
                "No se pudo cargar el acta de la instalación de campo"));
        }
        this.actualizacionEventoSelecionada = new ActualizacionEvento();
        this.actualizacionEventoSelecionada.setNombre(
            "Instalacion de comision de campo y asistentes invitados");
        this.actualizacionEventoSelecionada.setActualizacion(actualizacionSeleccionada);
        this.actualizacionEventoSelecionada.setTipo("INSTALACION COMISION");

        this.actualizacionEventoSelecionada.setDescripcion(
            "Instalacion de comision de campo y generacion de acta de asistentes  para la " +
             this.actualizacionSeleccionada.getTipoProceso() + " en el Municipio de" +
             this.actualizacionSeleccionada.getMunicipio().getNombre());
        this.actualizacionEventoSelecionada.setEventoAsistentes(this.listaAsistentesTabla);

        this.actualizacionEventoSelecionada.setActaDocumentoId(this.documento.getId());
        this.actualizacionEventoSelecionada.setFecha(getFechaComision());
        this.actualizacionEventoSelecionada.setUsuarioLog(this.usuario.
            getCodigoEstructuraOrganizacional());
        this.actualizacionEventoSelecionada.setFechaLog(new Date(System.currentTimeMillis()));
        long actualizacionId = this.actualizacionSeleccionada.getId();
        try {
            this.actualizacionEventoSelecionada = this.getActualizacionService().
                cargarActaEnComisionDeCampo(actualizacionId, actualizacionEventoSelecionada,
                    this.documento);
        } catch (Exception e) {
            LOGGER.debug("Error al cargar el acta de instalación de campo");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo cargar el acta de la instalación de campo",
                "No se pudo cargar el acta de la instalación de campo"));
        }
        for (EventoAsistente ea : this.listaAsistentesTabla) {
            ea.setActualizacionEvento(this.actualizacionEventoSelecionada);
            ea.setUsuarioLog(this.usuario.getLogin());
            ea.setFechaLog(new Date(System.currentTimeMillis()));
        }
        try {
            this.listaAsistentesTabla = this.getActualizacionService().
                guardarYactualizarEventoAsistentes(this.listaAsistentesTabla);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "SE GUARDO EXITOSAMENTE TODOS LOS ASISTENTES Y EL EVENTO",
                "SE GUARDO EXITOSAMENTE TODOS LOS ASISTENTES Y EL EVENTO"));
        } catch (Exception e) {
            LOGGER.debug("Error al cargar los asistentes al evento");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudieron guardar los asistentes al evento",
                "No se pudieron guardar los asistentes al evento"));
        }
        this.banderaAgregarAsistente = false;
    }

    /**
     * Consulta a la capa de negocios los asistentes al evento
     *
     * @author javier.barajas
     */
    public void cargarAsistentesTabla() {

        for (ActualizacionResponsable ar : this.responsablesActualizacion) {
            this.recursoHumanoActualizacion = this.getActualizacionService().
                getRecursoHumanoByResponsableActId(ar.getId());

            for (RecursoHumano rh : this.recursoHumanoActualizacion) {
                this.listaAsistentes.add(rh.getNombre());
            }
        }
    }

    /**
     * Carga la tabla de asistentes invitados
     *
     * @author javier.barajas
     */
    public void cargarAsistentesInvitadosTabla() {
        EventoAsistente asistentetemp = new EventoAsistente();
        for (String av : this.funcionariosAsistentes.getTarget()) {

            for (RecursoHumano rh : this.recursoHumanoActualizacion) {
                if (rh.getNombre().equals(av)) {
                    asistentetemp.setNombre(rh.getNombre());
                    asistentetemp.setDocumentoIdentificacion(rh.getDocumentoIdentificacion());
                    asistentetemp.setCargo(rh.getCargo());
                    asistentetemp.setInstitucion("IGAC");
                    this.listaAsistentesTabla.add(asistentetemp);
                    asistentetemp = new EventoAsistente();
                }
            }

            LOGGER.debug("TAMAÑO DE LA LISTA INVITADOS TABLA" + this.listaAsistentesTabla.size());

        }
        LOGGER.debug("TAMAÑO LISTA INVITADOS" + this.listaAsistentesTabla.size());

    }

    /**
     * Agrea un asistente invitado a ala tabla
     *
     * @author javier.barajas
     */
    public void agregarInvitado() {
        EventoAsistente asistentetemp = new EventoAsistente();
        LOGGER.debug("ANTES");
        for (EventoAsistente ea : this.listaAsistentesTabla) {
            LOGGER.debug("CARGO" + ea.getCargo());
        }

        //this.listaAsistentesTabla.clear();
        //this.cargarAsistentesTabla();
        if (!listaAsistentesTabla.contains(this.nombreInvitado)) {
            //listaAsistentes.add(this.nombreInvitado);
            asistentetemp.setNombre(this.nombreInvitado);
            asistentetemp.setDocumentoIdentificacion(this.identificacionInvitado);
            asistentetemp.setCargo(this.cargoInvitado);
            asistentetemp.setInstitucion(this.institucionInvitado);
            asistentetemp.setUsuarioLog(this.usuario.getLogin());
            asistentetemp.setFechaLog(new Date(System.currentTimeMillis()));
            //this.listaAsistentesNuevos.add(asistentetemp);
            this.listaAsistentesTabla.add(asistentetemp);
            LOGGER.debug("DESPUES");
            for (EventoAsistente ea : this.listaAsistentesTabla) {
                LOGGER.debug("CARGO" + ea.getCargo());
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "El asistente al evento ya esta en la tabla",
                "El asistente al evento ya esta en la tabla"));
        }

    }

    /**
     * Elimina el asistente invitado
     *
     * @author javier.barajas
     */
    public void quitarInvitado() {
        //this.listaAsistentesNuevos.remove(this.asistenteSeleccionado);
        this.listaAsistentesTabla.remove(this.asistenteSeleccionado);
        //this.listaAsistentes.remove(this.asistenteSeleccionado);

    }

    /**
     * Funcion evalua si se muestra o no asistente
     *
     * @author javier.barajas
     */
    public void mostrarAgregarAsistente() {
        if (banderaAgregarAsistente == false) {
            this.banderaAgregarAsistente = true;
            this.cargarAsistentesInvitadosTabla();
            //LOGGER.debug("TAMAÑO DE LOS ASISTENTES INVITADOS"+this.listaAsistentesInvitados.size());
            //LOGGER.debug("TAMAÑO DE LOS ASISTENTES INVITADOS dual list"+this.funcionariosAsistentes.getTarget().size());

        } else {
            this.banderaAgregarAsistente = false;
        }
    }

    public IContextListener getContext() {
        return context;
    }

    public void setContext(IContextListener context) {
        this.context = context;
    }

    /**
     * Genera el reporte del acta
     *
     * @author javier.barajas
     */
    public void generarActa() {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.INSTALACION_COMISION;
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("ACTUALIZACION_ID", "" + this.actualizacionSeleccionada.getId());
        parameters.put("CRITERIOS_UNIFICACION", this.criteriosUnificacion);
        parameters.put("ACTULIZACION_EVENTO_ID", "" + this.actualizacionEventoSelecionada.getId());
        this.reporteDTO = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);
        try {
            // SubE el archivo a Alfresco y actualizo el documento

            this.documento.setArchivo(this.reporteDTO.getRutaCargarReporteAAlfresco());
            this.documento = this.getGeneralesService().actualizarDocumento(this.documento);
            try {
                this.documento = this.getActualizacionService().
                    guardarDocumentoDeActualizacionEnAlfresco(this.documento,
                        this.actualizacionSeleccionada, this.usuario);
            } catch (Exception e) {
                LOGGER.error("No se pudo guardar el documento en Alfresco");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN, "No se pudo guardar el documento en Alfresco",
                    "No se pudo guardar el documento en Alfresco"));
            }
            ActualizacionDocumento docActualizacion = new ActualizacionDocumento();
            docActualizacion.setActualizacion(this.actualizacionSeleccionada);
            docActualizacion.setFechaLog(new Date(System.currentTimeMillis()));
            docActualizacion.setFueGenerado(true);
            docActualizacion.setDocumento(this.documento);
            docActualizacion.setUsuarioLog(this.usuario.getLogin());

            try {
                docActualizacion = this.getActualizacionService().
                    guardarYActualizarActualizacionDocumento(docActualizacion);
            } catch (Exception e) {
                LOGGER.error("No se pudo guardar el documento en Actualizacion Documento");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "No se pudo guardar el documento en Actualizacion Documento",
                    "No se pudo guardar el documento en Actualizacion Documento"));
            }

            System.out.println("-------------url" + documento.getId());
        } catch (Exception e) {
            LOGGER.error("No se pudo guardar el documento en Alfresco");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo guardar el documento en Alfresco",
                "No se pudo guardar el documento en Alfresco"));
        }
    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("instalarComisionDeCampo");
        this.tareasPendientesMB.init();
        //this.init();
        return "index";
    }

}
