package co.gov.igac.snc.web.components;

import java.util.List;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;

/**
 *
 * Interfaz local usada para definir los métodos que avanzan el proceso en conservación
 * distinguiendo del ambiente en donde se encuentre.
 *
 * @author david.cifuentes
 */
public interface IAvanzarProcesoConservacion {

    /**
     * Método usado para avanzar el proceso en conservación de la actividad comunicarAutoInteresado.
     *
     * @author david.cifuentes
     *
     * @param usuario
     * @param actividad
     * @param tramite
     * @param entidadesOGruposARealizarPruebas
     *
     * @throws ExcepcionSNC
     */
    public void avanzarProcesoComunicarAutoInteresado(UsuarioDTO usuario, Actividad actividad,
        Tramite tramite,
        List<TramitePruebaEntidad> entidadesOGruposARealizarPruebas)
        throws ExcepcionSNC;

    /**
     * Método que avanza el proceso en el subproceso de determinar procedencia de la solicitud
     *
     * @param usuario
     * @param actividad
     *
     * @throws ExcepcionSNC
     *
     * @author javier.aponte
     */
//TODO :: javier.aponte :: mejorar la documentación. 
    // 1. determinar procedencia de la solicitud no es subproceso
    // 2. ¿a qué actividad lo avanza?
    public void avanzarProcesoDepurarInformacionGeografica(UsuarioDTO usuarioActual,
        UsuarioDTO usuarioDestino,
        Actividad actividad) throws ExcepcionSNC;

    /**
     * Métodos para la inyección manual de los servicios de soporte
     *
     * @param tramiteService
     */
    public void setTramiteService(ITramite tramiteService);

    public void setGeneralesService(IGenerales generalesService);

    public void setProcesoService(IProcesos procesoService);

}
