/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.util;

import co.gov.igac.generales.dto.UsuarioDTO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Managed bean para ser usado en las interfaces donde se necesita pintar listas de selección de
 * departamentos y municipios que estén interrelacionadas y con ordenamientos por código y por
 * nombre.
 *
 * Para ser usada dentro de otro MB se debe inyectar, y así se pueden obtener el departamento y
 * municipio seleccionados
 *
 * OJO: USAR SOLO CUANDO LOS VALORES DE LOS COMBOS DEBAN SER REQUERIDOS Y SE ARMEN EN CASCADA.
 *
 * @author pedro.garcia
 */
/*
 * @ruined juan.agudelo
 */
@Component("combosDeptosMunis")
@Scope("session")
public class CombosDeptosMunisMB extends SNCManagedBean {

    private static final long serialVersionUID = 5179769835166965222L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CombosDeptosMunisMB.class);

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<EstructuraOrganizacional> territoriales;
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;
    private ArrayList<Zona> zonas;
    private ArrayList<Sector> sectores;
    private ArrayList<Comuna> comunas;
    private ArrayList<Barrio> barrios;
    private ArrayList<ManzanaVereda> manzanas;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> territorialesItemList;
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;
    private ArrayList<SelectItem> zonasItemList;
    private ArrayList<SelectItem> sectoresItemList;
    private ArrayList<SelectItem> comunasItemList;
    private ArrayList<SelectItem> barriosItemList;
    private ArrayList<SelectItem> manzanasItemList;

    /**
     * Orden que se usa para mostrar las listas de departamento y municipio y etc
     */
    private EOrden ordenTerritoriales;
    private EOrden ordenDepartamentos;
    private EOrden ordenMunicipios;
    private EOrden ordenZonas;
    private EOrden ordenSectores;
    private EOrden ordenComunas;
    private EOrden ordenBarrios;
    private EOrden ordenManzanas;

    /**
     * códigos de departamento, municipio y zona seleccionados
     */
    private String selectedDepartamentoCod;
    private String selectedMunicipioCod;
    private String selectedZonaCod;
    private String selectedSectorCod;
    private List<String> selectedSectoresCods;
    private String selectedComunaCod;
    private String selectedBarrioCod;
    private String selectedManzanaCod;

    private Departamento selectedDepartamento;
    private Municipio selectedMunicipio;
    private Zona selectedZona;
    private Sector selectedSector;
    private Comuna selectedComuna;
    private Barrio selectedBarrio;
    private ManzanaVereda selectedManzana;

    private String estructuraTerritorialCod;

    // ---------- flags --------------
    /**
     * indican si los combos están ordenados por nombre (si no, lo están por código)
     */
    private boolean territorialesOrderedByNombre;
    private boolean municipiosOrderedByNombre;
    private boolean departamentosOrderedByNombre;
    private boolean zonasOrderedByNombre;
    private boolean sectorOrderedByNombre;
    private boolean comunaOrderedByNombre;
    private boolean barrioOrderedByNombre;
    private boolean manzanaOrderedByNombre;

    /**
     * Usuario necesario para identificar entidades delegadas
     */
    private UsuarioDTO usuario;
    private boolean usuarioDelegado;
    private List<Municipio> municipiosDelegados;

    // ------------- methods -------------------------
    public ArrayList<SelectItem> getZonasItemList() {
        return this.zonasItemList;
    }

    public void setZonasItemList(ArrayList<SelectItem> zonasItemList) {
        this.zonasItemList = zonasItemList;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public Departamento getSelectedDepartamento() {
        return this.selectedDepartamento;
    }

    public EOrden getOrdenZonas() {
        return this.ordenZonas;
    }

    public ArrayList<SelectItem> getComunasItemList() {
        return this.comunasItemList;
    }

    public void setComunasItemList(ArrayList<SelectItem> comunasItemList) {
        this.comunasItemList = comunasItemList;
    }

    public ArrayList<SelectItem> getBarriosItemList() {
        return this.barriosItemList;
    }

    public void setBarriosItemList(ArrayList<SelectItem> barriosItemList) {
        this.barriosItemList = barriosItemList;
    }

    public ArrayList<SelectItem> getManzanasItemList() {
        return this.manzanasItemList;
    }

    public void setManzanasItemList(ArrayList<SelectItem> manzanasItemList) {
        this.manzanasItemList = manzanasItemList;
    }

    public EOrden getOrdenComunas() {
        return this.ordenComunas;
    }

    public void setOrdenComunas(EOrden ordenComunas) {
        this.ordenComunas = ordenComunas;
    }

    public EOrden getOrdenBarrios() {
        return this.ordenBarrios;
    }

    public void setOrdenBarrios(EOrden ordenBarrios) {
        this.ordenBarrios = ordenBarrios;
    }

    public EOrden getOrdenManzanas() {
        return this.ordenManzanas;
    }

    public void setOrdenManzanas(EOrden ordenManzanas) {
        this.ordenManzanas = ordenManzanas;
    }

    public String getSelectedComunaCod() {
        return this.selectedComunaCod;
    }

    public void setSelectedComunaCod(String codigo) {
        this.selectedComunaCod = codigo;

        Comuna comunaSelected = null;
        if (codigo != null && this.comunas != null) {
            for (Comuna comuna : this.comunas) {
                if (codigo.equals(comuna.getCodigo().substring(
                    comuna.getCodigo().length() - 2,
                    comuna.getCodigo().length()))) {
                    comunaSelected = comuna;
                    break;
                }
            }
        }
        this.selectedComuna = comunaSelected;

        this.selectedBarrioCod = null;
        updateBarriosItemList();

        this.selectedManzanaCod = null;
        updateManzanasItemList();
    }

    public String getSelectedBarrioCod() {
        return this.selectedBarrioCod;
    }

    public void setSelectedBarrioCod(String codigo) {
        this.selectedBarrioCod = codigo;

        Barrio barrioSelected = null;
        if (codigo != null && this.barrios != null) {
            for (Barrio barrio : this.barrios) {
                if (codigo.equals(barrio.getCodigo())) {
                    barrioSelected = barrio;
                    break;
                }
            }
        }
        this.selectedBarrio = barrioSelected;

        this.selectedManzanaCod = null;
        updateManzanasItemList();
    }

    public String getSelectedManzanaCod() {
        return this.selectedManzanaCod;
    }

    public void setSelectedManzanaCod(String codigo) {
        this.selectedManzanaCod = codigo;

        ManzanaVereda manzanaSelected = null;
        if (codigo != null && this.manzanas != null) {
            for (ManzanaVereda manzana : this.manzanas) {
                if (codigo.equals(manzana.getCodigo())) {
                    manzanaSelected = manzana;
                    break;
                }
            }
        }
        this.selectedManzana = manzanaSelected;
    }

    public Comuna getSelectedComuna() {
        return this.selectedComuna;
    }

    public void setSelectedComuna(Comuna selectedComuna) {
        this.selectedComuna = selectedComuna;
    }

    public Barrio getSelectedBarrio() {
        return this.selectedBarrio;
    }

    public void setSelectedBarrio(Barrio selectedBarrio) {
        this.selectedBarrio = selectedBarrio;
    }

    public ManzanaVereda getSelectedManzana() {
        return this.selectedManzana;
    }

    public void setSelectedManzana(ManzanaVereda selectedManzana) {
        this.selectedManzana = selectedManzana;
    }

    public boolean isSectorOrderedByNombre() {
        return this.sectorOrderedByNombre;
    }

    public void setSectorOrderedByNombre(boolean sectorOrderedByNombre) {
        this.sectorOrderedByNombre = sectorOrderedByNombre;
    }

    public boolean isComunaOrderedByNombre() {
        return this.comunaOrderedByNombre;
    }

    public void setComunaOrderedByNombre(boolean comunaOrderedByNombre) {
        this.comunaOrderedByNombre = comunaOrderedByNombre;
    }

    public boolean isBarrioOrderedByNombre() {
        return this.barrioOrderedByNombre;
    }

    public void setBarrioOrderedByNombre(boolean barrioOrderedByNombre) {
        this.barrioOrderedByNombre = barrioOrderedByNombre;
    }

    public boolean isManzanaOrderedByNombre() {
        return this.manzanaOrderedByNombre;
    }

    public void setManzanaOrderedByNombre(boolean manzanaOrderedByNombre) {
        this.manzanaOrderedByNombre = manzanaOrderedByNombre;
    }

    public void setOrdenZonas(EOrden ordenZonas) {
        this.ordenZonas = ordenZonas;
    }

    public EOrden getOrdenSectores() {
        return this.ordenSectores;
    }

    public void setOrdenSectores(EOrden ordenSectores) {
        this.ordenSectores = ordenSectores;
    }

    public void setSelectedDepartamento(Departamento selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public Municipio getSelectedMunicipio() {
        return this.selectedMunicipio;
    }

    public void setSelectedMunicipio(Municipio selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    // ---------------------------------------------------------------------
    public String getSelectedMunicipioCod() {
        return this.selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String codigo) {
        this.selectedMunicipioCod = codigo;

        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.selectedMunicipio = municipioSelected;
    }

    // --------------------------------------------------------------------------------
    public String getSelectedDepartamentoCod() {
        return this.selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String codigo) {

        this.selectedDepartamentoCod = codigo;
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento departamento : this.departamentos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        this.selectedDepartamento = dptoSeleccionado;
    }

    // ---------------------------------------------------------------------
    public String getSelectedZonaCod() {
        return this.selectedZonaCod;
    }

    /*
     * @modified pedro.garcia el código de zona puede llegar de longitud 2 (el código mismo) o de 7
     * (el número predial hasta la zona), por eso la búsqueda del código en la lista de zonas debe
     * tener eso en cuenta
     */
    public void setSelectedZonaCod(String codigo) {

        if (codigo != null) {
            this.selectedZonaCod = codigo;

            Zona zonaSelected = null;
            String tempCodigoZona, codigoAComparar;
            boolean compararPorCod;

            // D: define si se va a comparar por 2 o por 7 caracteres
            compararPorCod = (codigo.length() == 2) ? true : false;

            if (codigo != null && this.zonas != null) {
                for (Zona zona : this.zonas) {
                    tempCodigoZona = zona.getCodigo();
                    codigoAComparar = (compararPorCod) ? tempCodigoZona.substring(tempCodigoZona
                        .length() - 2) : tempCodigoZona;
                    if (codigo.equals(codigoAComparar)) {
                        zonaSelected = zona;
                        this.selectedZonaCod = zona.getCodigo();
                        break;
                    }
                }
            }
            this.selectedZona = zonaSelected;

            this.selectedSectorCod = null;
            this.selectedComunaCod = null;
            this.selectedBarrioCod = null;
            this.selectedManzanaCod = null;
        }
    }

    // ---------------------------------------------------------------------------------
    public List<String> getSelectedSectoresCods() {
        return this.selectedSectoresCods;
    }

    public void setSelectedSectoresCods(List<String> selectedSectoresCods) {
        this.selectedSectoresCods = selectedSectoresCods;
    }

    // ---------------------------------------------------------------------------------
    public String getSelectedSectorCod() {
        return this.selectedSectorCod;
    }

    public void setSelectedSectorCod(String codigo) {
        this.selectedSectorCod = codigo;

        Sector sectorSelected = null;
        if (codigo != null && this.sectores != null) {
            for (Sector sector : this.sectores) {
                if (codigo.equals(sector.getCodigo())) {
                    sectorSelected = sector;
                    break;
                }
            }
        }
        this.selectedSector = sectorSelected;

        this.selectedComunaCod = null;
        updateComunasItemList();

        this.selectedBarrioCod = null;
        updateBarriosItemList();

        this.selectedManzanaCod = null;
    }

//----------------------------------------------------------------------------------
    public ArrayList<EstructuraOrganizacional> getTerritoriales() {
        return this.territoriales;
    }

    public void setTerritoriales(ArrayList<EstructuraOrganizacional> territoriales) {
        this.territoriales = territoriales;
    }

    public ArrayList<Departamento> getDepartamentos() {
        return this.departamentos;
    }

    public void setDepartamentos(ArrayList<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public ArrayList<Municipio> getMunicipios() {
        return this.municipios;
    }

    public void setMunicipios(ArrayList<Municipio> municipios) {
        this.municipios = municipios;
    }

//-----------------------------------------------------------------------------------
    public ArrayList<SelectItem> getTerritorialesItemList() {
        if (this.territorialesItemList == null) {
            this.updateTerritorialesItemList();
        }

        return this.territorialesItemList;
    }

    public void setTerritorialesItemList(ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }

        return this.departamentosItemList;
    }

    public ArrayList<SelectItem> getDepartamentosItemListDelegada() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }

        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    // -----------------------------------------------------------------------------------
    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public EOrden getOrdenTerritoriales() {
        return this.ordenTerritoriales;
    }

    public void setOrdenTerritoriales(EOrden ordenTerritoriales) {
        this.ordenTerritoriales = ordenTerritoriales;
    }

    public EOrden getOrdenDepartamentos() {
        return this.ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public EOrden getOrdenMunicipios() {
        return this.ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public boolean isTerritorialesOrderedByNombre() {
        return this.territorialesOrderedByNombre;
    }

    public void setTerritorialesOrderedByNombre(boolean territorialesOrderedByNombre) {
        this.territorialesOrderedByNombre = territorialesOrderedByNombre;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return this.municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return this.departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(
        boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public boolean isZonasOrderedByNombre() {
        return this.zonasOrderedByNombre;
    }

    public void setZonasOrderedByNombre(boolean zonasOrderedByNombre) {
        this.zonasOrderedByNombre = zonasOrderedByNombre;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de CombosDeptosMunisMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        //felipe.cadena::22-12-2016::Ajustes para delegacion
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();

        this.ordenTerritoriales = EOrden.NOMBRE;
        this.ordenDepartamentos = EOrden.NOMBRE;
        this.territorialesOrderedByNombre = true;
        this.departamentosOrderedByNombre = true;
        this.ordenMunicipios = EOrden.NOMBRE;
        this.municipiosOrderedByNombre = true;
        this.ordenZonas = EOrden.NOMBRE;
        this.zonasOrderedByNombre = true;
        this.ordenSectores = EOrden.NOMBRE;
        this.sectorOrderedByNombre = true;
        this.ordenComunas = EOrden.NOMBRE;
        this.comunaOrderedByNombre = true;
        this.ordenBarrios = EOrden.NOMBRE;
        this.barrioOrderedByNombre = true;
        this.ordenManzanas = EOrden.NOMBRE;
        this.manzanaOrderedByNombre = true;

        this.estructuraTerritorialCod = "";

        if (this.usuarioDelegado) {
            this.departamentos = (ArrayList<Departamento>) this.getGeneralesService().
                getDepartamentoByCodigoEstructuraOrganizacional(this.usuario.getCodigoTerritorial());
        } else {
            this.departamentos = (ArrayList<Departamento>) this
                .getGeneralesService().buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
                    Constantes.COLOMBIA);
        }
        this.municipiosDelegados = this.getGeneralesService().obtenerMunicipiosDelegados();
        this.updateMunicipiosItemList();

        this.territoriales = (ArrayList<EstructuraOrganizacional>) this.getGeneralesService().
            getCacheTerritoriales();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de territoriales
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenTerritoriales() {

        if (this.ordenTerritoriales.equals(EOrden.CODIGO)) {
            this.ordenTerritoriales = EOrden.NOMBRE;
            this.territorialesOrderedByNombre = true;
        } else {
            this.ordenTerritoriales = EOrden.CODIGO;
            this.territorialesOrderedByNombre = false;
        }

        this.updateTerritorialesItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de departamentos
     */
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.updateDepartamentosItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de municipios
     */
    public void cambiarOrdenMunicipios() {

        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.updateMunicipiosItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de zonas
     */
    public void cambiarOrdenZonas() {

        if (this.ordenZonas.equals(EOrden.CODIGO)) {
            this.ordenZonas = EOrden.NOMBRE;
            this.zonasOrderedByNombre = true;
        } else {
            this.ordenZonas = EOrden.CODIGO;
            this.zonasOrderedByNombre = false;
        }

        this.updateZonasItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de sectores
     */
    public void cambiarOrdenSectores() {

        if (this.ordenSectores.equals(EOrden.CODIGO)) {
            this.ordenSectores = EOrden.NOMBRE;
            this.sectorOrderedByNombre = true;
        } else {
            this.ordenSectores = EOrden.CODIGO;
            this.sectorOrderedByNombre = false;
        }

        this.updateSectoresItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de comunas
     */
    public void cambiarOrdenComunas() {

        if (this.ordenComunas.equals(EOrden.CODIGO)) {
            this.ordenComunas = EOrden.NOMBRE;
            this.comunaOrderedByNombre = true;
        } else {
            this.ordenComunas = EOrden.CODIGO;
            this.comunaOrderedByNombre = false;
        }

        this.updateComunasItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de barrios
     */
    public void cambiarOrdenBarrios() {

        if (this.ordenBarrios.equals(EOrden.CODIGO)) {
            this.ordenBarrios = EOrden.NOMBRE;
            this.barrioOrderedByNombre = true;
        } else {
            this.ordenBarrios = EOrden.CODIGO;
            this.barrioOrderedByNombre = false;
        }

        this.updateBarriosItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de manzanas
     */
    public void cambiarOrdenManzanas() {

        if (this.ordenManzanas.equals(EOrden.CODIGO)) {
            this.ordenManzanas = EOrden.NOMBRE;
            this.manzanaOrderedByNombre = true;
        } else {
            this.ordenManzanas = EOrden.CODIGO;
            this.manzanaOrderedByNombre = false;
        }

        this.updateManzanasItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * actualiza la lista de departamentos según el órden escogido
     *
     * @author pedro.garcia
     */
    public void updateTerritorialesItemList() {

        this.territorialesItemList = new ArrayList<SelectItem>();
        this.territorialesItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));

        if (this.ordenTerritoriales.equals(EOrden.CODIGO)) {
            Collections.sort(this.territoriales);
        } else {
            Collections.sort(this.territoriales, EstructuraOrganizacional.getComparatorNombre());
        }

        for (EstructuraOrganizacional territorial : this.territoriales) {
            if (this.ordenTerritoriales.equals(EOrden.CODIGO)) {
                this.territorialesItemList.add(new SelectItem(territorial
                    .getCodigo(), territorial.getCodigo() + "-" +
                     territorial.getNombre()));
            } else {
                this.territorialesItemList.add(new SelectItem(territorial
                    .getCodigo(), territorial.getNombre()));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * actualiza la lista de departamentos según el órden escogido
     */
    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos, Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * actualiza la lista de municipios según el departamento seleccionado
     *
     */
    public void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        boolean munDelegado = false;

        if (!this.usuarioDelegado) {
            this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
        }
        if (this.selectedDepartamentoCod != null) {
            if (this.usuarioDelegado) {
                this.municipios = new ArrayList<Municipio>();
                for (Municipio mun : this.municipiosDelegados) {
                    if (mun.getDepartamento().getCodigo().equals(this.selectedDepartamentoCod)) {
                        this.municipios.add(mun);
                    }
                }

                this.selectedMunicipioCod = this.municipios.get(0).getCodigo();
                this.selectedMunicipio = this.municipios.get(0);
            } else {
                List<Municipio> municipiosTotal = (ArrayList<Municipio>) this.getGeneralesService()
                    .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
                        this.selectedDepartamentoCod);
                this.municipios = new ArrayList<Municipio>();
                for (Municipio municipio : municipiosTotal) {
                    munDelegado = false;
                    for (Municipio munDel : this.municipiosDelegados) {
                        if (munDel.getCodigo().equals(municipio.getCodigo())) {
                            munDelegado = true;
                            break;
                        }
                    }
                    if (!munDelegado) {
                        this.municipios.add(municipio);
                    }
                }

            }

            if (this.municipios != null && !this.municipios.isEmpty()) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    Collections.sort(this.municipios);
                } else {
                    Collections.sort(this.municipios, Municipio.getComparatorNombre());
                }

                for (Municipio municipio : this.municipios) {
                    if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                        this.municipiosItemList.add(new SelectItem(municipio
                            .getCodigo(), municipio.getCodigo3Digitos() +
                             "-" + municipio.getNombre()));
                    } else {
                        this.municipiosItemList.add(new SelectItem(
                            municipio.getCodigo(), municipio.getNombre()));
                    }
                }
            }
        } else {
            this.municipios = null;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * actualiza la lista de zonas según el municipio seleccionado
     *
     */
    public void updateZonasItemList() {
        
        this.zonas = null;
        this.selectedZona = null;
        this.selectedZonaCod = null;

        if (this.selectedMunicipioCod != null) {
            this.zonasItemList = new ArrayList<SelectItem>();
            this.zonasItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

            this.zonas = (ArrayList<Zona>) this.getGeneralesService()
                .getZonasFromMunicipio(this.selectedMunicipioCod);

            if (this.zonas != null && !this.zonas.isEmpty()) {
                if (this.ordenZonas.equals(EOrden.CODIGO)) {
                    Collections.sort(this.zonas);
                } else {
                    Collections.sort(this.zonas, Zona.getComparatorNombre());
                }

                for (Zona zona : this.zonas) {
                    if (this.ordenZonas.equals(EOrden.CODIGO)) {
                        this.zonasItemList.add(new SelectItem(zona.getCodigo(),
                            zona.getCodigo3Digitos() + "-" + zona.getNombre()));
                    } else {
                        this.zonasItemList.add(new SelectItem(zona.getCodigo(), zona.getNombre()));
                    }
                }
            }
        } else {
            this.zonasItemList = null;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de departamentos
     */
    public void onChangeDepartamentosListener() {
        this.selectedMunicipio = null;
        this.updateMunicipiosItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de municipios
     */
    public void onChangeMunicipiosListener() {
        this.selectedZonaCod = null;
        this.updateZonasItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de zonas
     */
    public void onChangeZonaListener() {
        this.selectedSectorCod = null;
        updateSectoresItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de sectores
     */
    public void onChangeSectorListener() {
        this.selectedComunaCod = null;
        updateComunasItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de comunas
     */
    public void onChangeComunaListener() {
        this.selectedBarrioCod = null;
        updateBarriosItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de barrio
     */
    public void onChangeBarrioListener() {
        // Método dummie ejecutado como listener en el cambio del combo de
        // barrio, es requerido por el composite pero para este caso puntual no
        // ejecuta ningúna acción
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio en el combo de barrio
     */
    public void onChangeManzanaListener() {
        // Método dummie ejecutado como listener en el cambio del combo de
        // manzana, es requerido por el composite pero para este caso puntual no
        // ejecuta ningúna acción
    }
//--------------------------------------------------------------------------------------------------

    /**
     * actualiza la lista de sectores según la zona seleccionado
     *
     */
    private void updateSectoresItemList() {

        if (this.selectedZonaCod != null) {
            this.sectoresItemList = new ArrayList<SelectItem>();
            this.sectoresItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

            this.sectores = (ArrayList<Sector>) this.getGeneralesService()
                .getSectoresFromZona(this.selectedZonaCod);

            if (this.sectores != null && !this.sectores.isEmpty()) {
                if (this.ordenSectores.equals(EOrden.CODIGO)) {
                    Collections.sort(this.sectores);
                } else {
                    Collections.sort(this.sectores,
                        Sector.getComparatorNombre());
                }

                for (Sector sector : this.sectores) {
                    if (this.ordenSectores.equals(EOrden.CODIGO)) {
                        this.sectoresItemList.add(new SelectItem(sector
                            .getCodigo(), sector.getCodigo3Digitos() + "-" +
                             sector.getNombre()));
                    } else {
                        this.sectoresItemList.add(new SelectItem(sector
                            .getCodigo(), sector.getNombre()));
                    }
                }
            }
        } else {
            this.sectoresItemList = null;
        }

    }

//-------------------------------------------------------------------------
    /**
     * actualiza la lista de comunas según el sector seleccionado
     *
     */
    private void updateComunasItemList() {

        if (this.selectedSectorCod != null) {
            this.comunasItemList = new ArrayList<SelectItem>();
            this.comunasItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

            this.comunas = (ArrayList<Comuna>) this.getGeneralesService()
                .getComunasFromSector(this.selectedSectorCod);

            if (this.comunas != null && !this.comunas.isEmpty()) {
                if (this.ordenComunas.equals(EOrden.CODIGO)) {
                    Collections.sort(this.comunas);
                } else {
                    Collections.sort(this.comunas, Comuna.getComparatorNombre());
                }

                for (Comuna comuna : this.comunas) {
                    if (this.ordenComunas.equals(EOrden.CODIGO)) {
                        this.comunasItemList.add(new SelectItem(comuna
                            .getCodigo(), comuna.getCodigo3Digitos() + "-" +
                             comuna.getNombre()));
                    } else {
                        this.comunasItemList.add(new SelectItem(comuna
                            .getCodigo(), comuna.getNombre()));
                    }
                }
            }
        } else {
            this.comunasItemList = null;
        }

    }

    // -------------------------------------------------------------------------
    /**
     * actualiza la lista de barrios según el sector seleccionado
     *
     */
    private void updateBarriosItemList() {

        if (this.selectedComunaCod != null) {
            this.barriosItemList = new ArrayList<SelectItem>();
            this.barriosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

            this.barrios = (ArrayList<Barrio>) this.getGeneralesService()
                .getBarriosFromComuna(this.selectedComunaCod);

            if (this.barrios != null && !this.barrios.isEmpty()) {
                if (this.ordenBarrios.equals(EOrden.CODIGO)) {
                    Collections.sort(this.barrios);
                } else {
                    Collections.sort(this.barrios, Barrio.getComparatorNombre());
                }

                for (Barrio barrio : this.barrios) {
                    if (this.ordenBarrios.equals(EOrden.CODIGO)) {
                        this.barriosItemList.add(new SelectItem(barrio
                            .getCodigo(), barrio.getCodigo3Digitos() + "-" +
                             barrio.getNombre()));
                    } else {
                        this.barriosItemList.add(new SelectItem(barrio
                            .getCodigo(), barrio.getNombre()));
                    }
                }
            }
        } else {
            this.barriosItemList = null;
        }

    }

    // -------------------------------------------------------------------------
    /**
     * actualiza la lista de manzanas según el sector seleccionado
     *
     */
    private void updateManzanasItemList() {

        if (this.selectedBarrioCod != null) {
            this.manzanasItemList = new ArrayList<SelectItem>();
            this.manzanasItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

            this.manzanas = (ArrayList<ManzanaVereda>) this
                .getGeneralesService().getManzanaVeredaFromBarrio(
                    this.selectedBarrioCod);

            if (this.manzanas != null && !this.manzanas.isEmpty()) {
                if (this.ordenManzanas.equals(EOrden.CODIGO)) {
                    Collections.sort(this.manzanas);
                } else {
                    Collections.sort(this.manzanas, ManzanaVereda.getComparatorNombre());
                }

                for (ManzanaVereda manzana : this.manzanas) {
                    if (this.ordenManzanas.equals(EOrden.CODIGO)) {
                        this.manzanasItemList.add(new SelectItem(manzana
                            .getCodigo(), manzana.getCodigo3Digitos() + "-" +
                             manzana.getNombre()));
                    } else {
                        this.manzanasItemList.add(new SelectItem(manzana
                            .getCodigo(), manzana.getNombre()));
                    }
                }
            }
        } else {
            this.manzanasItemList = null;
        }
    }

    public ArrayList<Zona> getZonas() {
        return zonas;
    }

    public void setZonas(ArrayList<Zona> zonas) {
        this.zonas = zonas;
    }

    public Zona getSelectedZona() {
        return selectedZona;
    }

    public void setSelectedZona(Zona selectedZona) {
        this.selectedZona = selectedZona;
    }

    public Sector getSelectedSector() {
        return selectedSector;
    }

    public void setSelectedSector(Sector selectedSector) {
        this.selectedSector = selectedSector;
    }

    public ArrayList<Sector> getSectores() {
        return sectores;
    }

    public void setSectores(ArrayList<Sector> sectores) {
        this.sectores = sectores;
    }

    public ArrayList<SelectItem> getSectoresItemList() {
        return sectoresItemList;
    }

    public void setSectoresItemList(ArrayList<SelectItem> sectoresItemList) {
        this.sectoresItemList = sectoresItemList;
    }

    public String getEstructuraTerritorialCod() {
        return estructuraTerritorialCod;
    }

    /**
     * Carga los departamentos de la territorial enviada como argumento
     *
     * @author christian.rodriguez
     * @param estructuraTerritorialCod código de la territorial
     */
    public void setEstructuraTerritorialCod(String estructuraTerritorialCod) {
        this.departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorTerritorial(
                estructuraTerritorialCod);

        this.estructuraTerritorialCod = estructuraTerritorialCod;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * pone todas las listas de selección en null
     *
     * @author pedro.garcia
     */
    public void resetSelectionLists() {

        this.departamentosItemList = null;
        this.municipiosItemList = null;
        this.zonasItemList = null;
        this.sectoresItemList = null;
        this.comunasItemList = null;
        this.barriosItemList = null;
        this.manzanasItemList = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * pone todas los valores de códigos seelccionados en ""
     *
     * @author pedro.garcia
     */
    public void resetSelectedCodesValues() {

        this.selectedDepartamentoCod = "";
        this.selectedMunicipioCod = "";
        this.selectedZonaCod = "";
        this.selectedZona = null;
        this.selectedSectorCod = "";
        this.selectedComunaCod = "";
        this.selectedBarrioCod = "";
        this.selectedManzanaCod = "";
    }
//--------------------------------------------------------------------------------------------------

    /**
     * resetea todo
     */
    public void resetAll() {
        this.resetSelectionLists();
        this.resetSelectedCodesValues();
    }

//end of class
}
