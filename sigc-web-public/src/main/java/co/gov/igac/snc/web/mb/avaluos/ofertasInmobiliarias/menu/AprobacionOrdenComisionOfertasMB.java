/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.menu;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaComisionEstado;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-003 Aprobar Orden Comisión Ofertas
 *
 * @author christian.rodriguez
 * @cu CU-TV-0F-003
 * @version 2.0
 */
@Component("aprobacionOrdenComisionOfertas")
@Scope("session")
public class AprobacionOrdenComisionOfertasMB extends OrdenComisionOfertasMB {

    /**
     * auto generated serial
     */
    private static final long serialVersionUID = 5656168997679537655L;

    // -------------- methods
//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on AprobacionOrdenComisionOfertasMB init");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        this.extraerCodigoEstructuraOrganizacional();

        this.estadoInicialComisiones = EOfertaComisionEstado.POR_APROBAR.getCodigo();

        this.almacenarIdsActividadesArbol();

        this.cargarListaComisiones();

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado cuando se aprueba una comisión. Se enecargar de cambiar el estado de la
     * comision seleccioanda a APROBADA y actualizarla en la base de datos
     *
     * Tambien ejecuta los métodos de process.
     *
     * @author christian.rodriguez
     */
    public void aprobarComisiones() {

        if (this.selectedComisionesPorAprobar != null) {

            for (ComisionOferta comision : this.getSelectedComisionesPorAprobar()) {
                comision.setEstado(EOfertaComisionEstado.APROBADA.getCodigo());
            }

            if (!this.forwardProcess()) {
                this.addMensajeError("Ocurrió un error avanzando alguna de las actividades del " +
                    "proceso");
            }

            this.cargarListaComisiones();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see OrdenComisionOfertasMB#guardarComisionOferta()
     * @author christian.rodriguez
     */
    @Override
    public void guardarComisionOferta() {
        super.guardarComisionOferta();
    }

//-------------------------------------- Metodos para integración con process ------------
    private boolean validateProcess() {
        return true;
    }

//-------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @return false si no hubo ningún error avanzando las actividades
     */
    private boolean forwardProcess() {

        LOGGER.debug("Moviendo el proceso para las regiones asociadas a la comisión");

        boolean ok = true;
        ArrayList<RegionCapturaOferta> regionesToForward, tempRegionesToForward;
        List<UsuarioDTO> listaUsuarios;
        UsuarioDTO usuarioDestinoActividad;
        OfertaInmobiliaria ofertaBPM;
        String idActividad, transition;

        if (!this.validateProcess()) {
            return false;
        }

        if (!this.doDatabaseStatesUpdate()) {
            return false;
        }

        //D: buscar las regiones que estén asociadas con cada comisión que se va a aprobar para
        //   tener los datos de los procesos que se deben avanzar
        regionesToForward = new ArrayList<RegionCapturaOferta>();
        for (ComisionOferta comision : this.selectedComisionesPorAprobar) {
            tempRegionesToForward = (ArrayList<RegionCapturaOferta>) this.getAvaluosService().
                obtenerRegionesCapturaOfertaDeComisionNF(comision.getId());

            if (tempRegionesToForward != null && !tempRegionesToForward.isEmpty()) {
                regionesToForward.addAll(tempRegionesToForward);
            }
        }

        transition = ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO;
        listaUsuarios = new ArrayList<UsuarioDTO>();

        for (RegionCapturaOferta rco : regionesToForward) {

            usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                rco.getAsignadoRecolectorId());

            listaUsuarios.add(usuarioDestinoActividad);
            ofertaBPM = new OfertaInmobiliaria();
            ofertaBPM.setUsuarios(listaUsuarios);
            ofertaBPM.setTransicion(transition);
            ofertaBPM.setTerritorial(this.estructuraOrganizacionalUsuario);
            idActividad = this.mapaActividadRegion.get(rco.getId());

            if (idActividad != null) {
                try {
                    this.getProcesosService().avanzarActividad(idActividad, ofertaBPM);
                } catch (Exception ex) {
                    LOGGER.error("Error moviendo actividad con id " + idActividad + " a la " +
                        "actividad " + transition + " : " + ex.getMessage());
                    ok = false;
                    break;
                }
            }
        }

        return ok;

    }

// end of class
}
