/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.time.DateUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.impl.procesador.ProcesadorDocumentosImpl;
import co.gov.igac.sigc.documental.interfaces.IProcesadorDocumentos;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.ERolesEjecutorComisionDepuracion;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.EventoScheduleComisionesDTO;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para la asignación de comisiones para los trámites
 *
 * @author pedro.garcia
 * @cu CU-CO-A-04
 * @cu CU-NP-CO-213
 */
/*
 * @modified pedro.garcia 12-06-2013 adición de funcionalidad para el manejo de comisiones de
 * Depuración @modified david.cifuentes Debido a la adición del caso de uso 175 - Aprobar comisiones
 * se modificaron los métodos doDatabaseStatesUpdate y setupProcessMessage.
 */
@Component("manejoComisionesTramites")
@Scope("session")
public class ManejoComisionesTramitesMB extends ProcessFlowManager {

    /**
     * Serial
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ManejoComisionesTramitesMB.class);

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * managed bean para pintar los trámites de una comisión y permitir el retiro de éstos
     */
    @Autowired
    private TramitesDeComisionMB tramitesDeComisionMB;

    /**
     * variable del contexto
     */
    @Autowired
    private IContextListener contextService;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites (de terreno) a
     * los que no se les ha asignado una comisión
     */
    private LazyDataModel<Tramite> lazyTramitesPorComisionar;

    /**
     * variable donde se cargan las comisiones para las que aún no se ha generado memorando
     */
    private LazyDataModel<VComision> lazyComisionesPorGenerarMemo;

    /**
     * Trámites seleccionados de la tabla de trámites por asignar comisión
     */
    private Tramite[] selectedTramites;

    /**
     * Trámite de trabajo para las funcionalidades que funcionan con un solo trámite y sin
     * seleccionar una fila de la tabla de trámites (que tiene selección múltiple). Se debe usar
     * porque no funciona usar como target de un setPropertyActionListener algo como:
     * selectedTramites[0]
     */
    private Tramite selectedTramite;

    /**
     * Comisiones (VComision) seleccionadas de la tabla de comisiones sin memo
     */
    private VComision[] selectedComisiones;

    /**
     * comisión que se selecciona en la modal para comisionar trámites
     */
    private VComision selectedComision;

    /**
     * comisión que se selecciona para generar memorando de comisión
     */
    private VComision selectedComisionParaMemo;

    /**
     * selección de comisión en la modal para ver detalle o asignarle un trámite
     */
    private VComision selectedComisionTramite;

    /**
     * id de la comision seleccionada
     */
    private String selectedComisionId;

    /**
     * variable para guardar lo datos de una nueva comisión
     */
    private Comision nuevaComision;

    /**
     * variable que guarda los datos del usuario que usa la aplicación
     */
    private UsuarioDTO loggedInUser;

    /**
     * Listado de trámites asociados a alguna comisión que se seleccione
     */
    private List<Tramite> tramitesAsociadosComision;

    /**
     * Trámites seleccionados de la tabla de trámites asociados a una comisión
     */
    private Tramite[] selectedTramitesComision;

    private String idTerritorial;

    /**
     * modelo para el schedule de comisiones
     */
    private DefaultScheduleModel comisionesScheduleModel;

    /**
     * lista de ejecutores de la comisión seleccionada
     */
    private List<VEjecutorComision> ejecutoresComision;

    /**
     * Listado de trámites asociados a un ejecutor para alguna comisión que se seleccione
     */
    private List<Tramite> tramitesEjecutorComision;

    /**
     * ejecutor de comisión seleccionado (para ver sus trámites)
     */
    private VEjecutorComision selectedEjecutorComision;

    /**
     * objeto ReporteDTO en el que se recupera el memorando de la comisión.
     */
    private ReporteDTO documentoMemorandoComisionDTO;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String manzanaCodigo;
    private String prediosParaZoom;

//-----------   banderas   ------------------
    /**
     * indica si este MB va a manejar las comisiones del subproceso de depuración
     */
    private boolean isSubprocesoDepuracion;

    private boolean comisionFueModificada;

    /**
     * se usa como parámetro en la página de ajustes de tiemnpos de comisión para determinar si se
     * está trabajando con una comisión nueva
     */
    private boolean esNuevaComisionParaAjusteTiempos;

    /**
     * se usa para determinar si la ventana desde la que se abre la ventana de creación de nueva
     * comisión debe cerrarse: si la creación fue cancelada, no debe cerrarse
     */
    private boolean canceladaNuevaComision;

    /**
     * flag para saber si debo habilitar el botón "Crear comisión" en la ventana "Creación de nueva
     * comisión". Se debe deshabilitar luego de que se crea una comisión para evitar que se vuelva a
     * hacer la operación con los mismos trámites asociados
     */
    private boolean disableCrearComisionButton;

    /**
     * para evitar que el botón de "guardar cambios a la comisión" esté activo cuando la comisión se
     * cancela por retiro de todos los ejecutores
     */
    private boolean permitirGuardarCambiosComision;

    /**
     * indica si la comisión con la que se está trabajando es una nueva. Si no es nueva, es una
     * VComision
     */
    private boolean currentComisionIsNew;

//-------------    
    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    /**
     * la misma lista de ids de trámites que son el objeto de negocio de las instancias de
     * actividades del árbol, pero como List que es la que se usa en loe métodos de consulta;
     */
    private List<Long> idsTramitesActividades_L;

    private String numeroComisionSeleccionada;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteMemorandoComision;

    /**
     * Lista de usuarios con el rol de director territorial.
     */
    private List<UsuarioDTO> directoresTerritoriales;

//---------------------------------  methods     ------
    public boolean isIsSubprocesoDepuracion() {
        return this.isSubprocesoDepuracion;
    }

    public void setIsSubprocesoDepuracion(boolean isSubprocesoDepuracion) {
        this.isSubprocesoDepuracion = isSubprocesoDepuracion;
    }

    public ReporteDTO getDocumentoMemorandoComisionDTO() {
        return this.documentoMemorandoComisionDTO;
    }

    public void setDocumentoMemorandoComisionDTO(ReporteDTO documentoMemorandoComisionDTO) {
        this.documentoMemorandoComisionDTO = documentoMemorandoComisionDTO;
    }

    public Tramite getSelectedTramite() {
        return this.selectedTramite;
    }

    public void setSelectedTramite(Tramite selectedTramite) {
        this.selectedTramite = selectedTramite;
    }

    public boolean isPermitirGuardarCambiosComision() {
        return this.permitirGuardarCambiosComision;
    }

    public void setPermitirGuardarCambiosComision(boolean permitirGuardarCambiosComision) {
        this.permitirGuardarCambiosComision = permitirGuardarCambiosComision;
    }
//--------------------------------------------------------------------------

    public VComision getSelectedComisionParaMemo() {
        return this.selectedComisionParaMemo;
    }

    /**
     * Si se escoge una nueva comisión para generarle memorando, se deben definir algunas variables
     * de manera acorde.
     *
     * @param selectedComisionParaMemo
     */
    public void setSelectedComisionParaMemo(VComision selectedComisionParaMemo) {

        if (this.selectedComisionParaMemo != null && selectedComisionParaMemo != null &&
            this.selectedComisionParaMemo.getId() != selectedComisionParaMemo.getId()) {
        }

        this.selectedComisionParaMemo = selectedComisionParaMemo;
    }
//--------------------------------------------------------------------------

    public VEjecutorComision getSelectedEjecutorComision() {
        return this.selectedEjecutorComision;
    }

    public void setSelectedEjecutorComision(VEjecutorComision selectedEjecutorComision) {
        this.selectedEjecutorComision = selectedEjecutorComision;
    }

    public List<Tramite> getTramitesEjecutorComision() {
        return this.tramitesEjecutorComision;
    }

    public void setTramitesEjecutorComision(List<Tramite> tramitesEjecutorComision) {
        this.tramitesEjecutorComision = tramitesEjecutorComision;
    }

    public ReporteDTO getReporteMemorandoComision() {
        return this.reporteMemorandoComision;
    }

    public void setReporteMemorandoComision(ReporteDTO reporteMemorandoComision) {
        this.reporteMemorandoComision = reporteMemorandoComision;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * En este método se hace la búsqueda de ejecutores de la comisión que se haya seleccionado Solo
     * se hace si la comisión seleccionada es diferente a la anterior, o si la comisión fue
     * modificada
     *
     * @return
     */
    public List<VEjecutorComision> getEjecutoresComision() {

        String tempNumeroComision;

        if (this.selectedComision.getNumero() != null) {
            tempNumeroComision = this.selectedComision.getNumero();

            if (!tempNumeroComision.equalsIgnoreCase(this.numeroComisionSeleccionada)) {
                this.numeroComisionSeleccionada = tempNumeroComision;
                this.ejecutoresComision =
                    this.getTramiteService().buscarEjecutoresPorNumeroComision(tempNumeroComision);
            } else if (this.comisionFueModificada) {
                this.ejecutoresComision =
                    this.getTramiteService().buscarEjecutoresPorNumeroComision(tempNumeroComision);
                this.comisionFueModificada = false;
            }
        } else {
            this.numeroComisionSeleccionada = "";
        }
        return this.ejecutoresComision;
    }

    public void setEjecutoresComision(List<VEjecutorComision> ejecutoresComision) {
        this.ejecutoresComision = ejecutoresComision;
    }
//--------------------------------------------------------------------------------------------------

    public boolean isEsNuevaComisionParaAjusteTiempos() {
        return this.esNuevaComisionParaAjusteTiempos;
    }

    public void setEsNuevaComisionParaAjusteTiempos(boolean esNuevaComisionParaAjusteTiempos) {
        this.esNuevaComisionParaAjusteTiempos = esNuevaComisionParaAjusteTiempos;
    }

//--------------------------------------------------------------------------------------------------
    public Date getCurrentDate() {
        return new Date();
    }
//--------------------------------------------------------------------------------------------------

    public DefaultScheduleModel getComisionesScheduleModel() {
        return comisionesScheduleModel;
    }

    public void setComisionesScheduleModel(DefaultScheduleModel comisionesScheduleModel) {
        this.comisionesScheduleModel = comisionesScheduleModel;
    }

    public void setDisableCrearComisionButton(boolean disableCrearComisionButton) {
        this.disableCrearComisionButton = disableCrearComisionButton;
    }

    public boolean isDisableCrearComisionButton() {
        return this.disableCrearComisionButton;
    }

    public List<Tramite> getTramitesAsociadosComision() {
        return this.tramitesAsociadosComision;
    }

    public void setTramitesAsociadosComision(List<Tramite> tramitesAsociadosComision) {
        this.tramitesAsociadosComision = tramitesAsociadosComision;
    }

    public Tramite[] getSelectedTramitesComision() {
        return this.selectedTramitesComision;
    }

    public void setSelectedTramitesComision(Tramite[] selectedTramitesComision) {
        this.selectedTramitesComision = selectedTramitesComision;
    }

    public UsuarioDTO getLoggedInUser() {
        return this.loggedInUser;
    }

    public void setLoggedInUser(UsuarioDTO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public Comision getNuevaComision() {
        return this.nuevaComision;
    }

    public void setNuevaComision(Comision nuevaComision) {
        this.nuevaComision = nuevaComision;
    }

    public VComision getSelectedComision() {
        return selectedComision;
    }

    /**
     * Debe cambiar el valor de la bandera que se usa para saber si muestra el botón de radicación
     * del memorando, ya que pudo haberse radicado uno antes y no se devuelve al valor que debe
     * tener para mostrar el botón.
     *
     * OJO: no se sabe porqué ejecuta este método al cerrar la ventana del cu. Cuando no quedan
     * comisiones en la tabla en parámetro llega en null y se revienta.
     *
     * @param selectedComision
     */
    public void setSelectedComision(VComision selectedVComision) {

        this.permitirGuardarCambiosComision = true;

        this.selectedComision = selectedVComision;
    }
//------------------------------------------------------------------------

    public String getSelectedComisionId() {
        return this.selectedComisionId;
    }

    public void setSelectedComisionId(String selectedComisionId) {
        this.selectedComisionId = selectedComisionId;
    }

    public VComision getSelectedComisionTramite() {
        return this.selectedComisionTramite;
    }

    public void setSelectedComisionTramite(VComision selectedComisionTramite) {
        this.selectedComisionTramite = selectedComisionTramite;
    }

    public VComision[] getSelectedComisiones() {
        return this.selectedComisiones;
    }

    public void setSelectedComisiones(VComision[] selectedComisiones) {
        this.selectedComisiones = selectedComisiones;
    }

    public LazyDataModel<Tramite> getLazyTramitesPorComisionar() {
        return this.lazyTramitesPorComisionar;
    }

    public void setLazyTramitesPorComisionar(LazyDataModel<Tramite> lazyTramitesPorComisionar) {
        this.lazyTramitesPorComisionar = lazyTramitesPorComisionar;
    }

    public Tramite[] getSelectedTramites() {
        return this.selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public LazyDataModel<VComision> getLazyComisionesPorGenerarMemo() {
        return this.lazyComisionesPorGenerarMemo;
    }

    public void setLazyComisionesPorGenerarMemo(
        LazyDataModel<VComision> lazyComisionesPorGenerarMemo) {
        this.lazyComisionesPorGenerarMemo = lazyComisionesPorGenerarMemo;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("entra al init del ManejoComisionesTramitesMB");

        //D: en el atributo opcionActual está la ruta completa (macroproceso, proceso, subproceso, 
        //   actividad) de la actividad
        if (this.tareasPendientesMB.getOpcionActual().contains(
            ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION) ||
            this.tareasPendientesMB.getOpcionActual().contains(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION_EJECUTOR)) {
            this.isSubprocesoDepuracion = true;
        }

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "manejoComisionesTramites");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();
        this.idTerritorial = this.loggedInUser.getCodigoEstructuraOrganizacional();

        //D: se obtienen los ids de los trámites objetivo a partir del árbol de tareas       
        this.idsTramitesActividades =
            this.tareasPendientesMB.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        this.idsTramitesActividades_L = new ArrayList<Long>();

        for (int i = 0; i < this.idsTramitesActividades.length; i++) {
            this.idsTramitesActividades_L.add(this.idsTramitesActividades[i]);
        }

        //------- init de las cosas para el schedule de comisiones
        this.setComisionesScheduleModel(new DefaultScheduleModel());

        //D: llenar los eventos del schedule. Se deben mostrar todos los tipos de comisión.
        cargarEventosComisiones();

        // Inicialización del visor
        this.applicationClientName = this.contextService.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";

        LOGGER.debug("... finalizó init.");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public ManejoComisionesTramitesMB() {

        this.lazyTramitesPorComisionar = new LazyDataModel<Tramite>() {

            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();

                populateTramitesLazyly(answer, first, pageSize, sortField, sortOrder.toString(),
                    filters);

                return answer;
            }
        };

        //OJO: si se pone esto en el getter de la propiedad genera una excepción de división por 0
        this.lazyComisionesPorGenerarMemo = new LazyDataModel<VComision>() {

            @Override
            public List<VComision> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<VComision> answer = new ArrayList<VComision>();

                populateComisionesLazyly(answer, first, pageSize, filters);

                return answer;

            }
        };

    }
//--------------------------------------------------------------------------------------------------

    /**
     * hace la búsqueda de trámites a los que se les debe asignar comisión
     */
    private void populateTramitesLazyly(List<Tramite> answer, int first, int pageSize,
        String sortField, String sortOrder, Map<String, String> filters) {

        List<Tramite> rows;
        int size, count;

        if (this.isSubprocesoDepuracion) {
            rows = this.getTramiteService().buscarTramitesPorComisionar(this.idsTramitesActividades,
                this.idTerritorial, EComisionTipo.DEPURACION.getCodigo(), sortField, sortOrder,
                filters, first, pageSize);
        } else {
            rows = this.getTramiteService().buscarTramitesPorComisionar(this.idsTramitesActividades,
                this.idTerritorial, EComisionTipo.CONSERVACION.getCodigo(), sortField, sortOrder,
                filters, first, pageSize);
        }

        for (Tramite t : rows) {
            List<TramiteDocumento> documentosPruebas = this.getTramiteService()
                .obtenerTramiteDocumentosDePruebasPorTramiteId(t.getId());
            if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                t.setDocumentosPruebas(documentosPruebas);
            }
            t.setDepartamento(this.getGeneralesService().getDepartamentoByCodigo(
                t.getNumeroRadicacion().substring(0, 2)));
            t.setMunicipio(this.getGeneralesService().getCacheMunicipioByCodigo(
                t.getNumeroRadicacion().substring(0, 5)));
        }

        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }

        //D: se cuentan los de la bd asignar el tamaño del datamodel
        if (this.isSubprocesoDepuracion) {
            count = this.getTramiteService().
                contarTramitesPorComisionar(this.idsTramitesActividades, EComisionTipo.DEPURACION.
                    getCodigo());
        } else {
            count = this.getTramiteService().
                contarTramitesPorComisionar(this.idsTramitesActividades, EComisionTipo.CONSERVACION.
                    getCodigo());
        }

        this.lazyTramitesPorComisionar.setRowCount(count);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * hace la búsqueda de comisiones (VComision) a las que no se las ha generado memorando
     */
    private void populateComisionesLazyly(List<VComision> answer, int first, int pageSize,
        Map<String, String> filters) {

        List<VComision> rows;
        int size, count;

        if (this.isSubprocesoDepuracion) {
            rows = this.getTramiteService().buscarComisionesSinMemo(this.idTerritorial,
                this.idsTramitesActividades_L, EComisionTipo.DEPURACION.getCodigo(), filters,
                first, pageSize);
            count = this.getTramiteService().contarComisionesSinMemo(this.idTerritorial,
                this.idsTramitesActividades_L, EComisionTipo.DEPURACION.getCodigo());
        } else {
            rows = this.getTramiteService().buscarComisionesSinMemo(this.idTerritorial,
                this.idsTramitesActividades_L, EComisionTipo.CONSERVACION.getCodigo(), filters,
                first, pageSize);
            count = this.getTramiteService().contarComisionesSinMemo(this.idTerritorial,
                this.idsTramitesActividades_L, EComisionTipo.CONSERVACION.getCodigo());
        }

        if (rows != null) {
            size = rows.size();
            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }

        this.lazyComisionesPorGenerarMemo.setRowCount(count);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * acción para el botón "Comisionar" en la pantalla principal de manejo comisiones trámites
     *
     * @author pedro.garcia
     */
    /*
     * @modified pedro.garcia 23-05-2012 ya no se valida la regla chimba de que no se pueden
     * comisionar trámites de diferente ejecutor. Se deja el método para no romper el flujo y para
     * posibles validaciones.
     */
    public void comisionarTramites() {

        LOGGER.debug("entra al método comisionarTramites del ManejoComisionesTramitesMB");

        boolean okToComisionar;

        okToComisionar = true;
        if (!okToComisionar) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace nada. Solo para obligar a la aplicación a que venga hasta el servidor
     *
     * @param event
     */
    public void onComisionSelectListener(SelectEvent event) {
        LOGGER.debug("comisión seleccionada");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método action para el botón "Crear nueva" de la página "Comisionar trámites".
     */
    public void crearComisionInit() {

        this.nuevaComision = new Comision();
        this.currentComisionIsNew = true;

        if (this.isSubprocesoDepuracion) {
            this.nuevaComision.setTipo(EComisionTipo.DEPURACION.getCodigo());

            //D: se inicializan los campos 'duracion' y 'duracionTotal'
            this.calcularDuracionesComision(2, false);

        } else {
            this.nuevaComision.setTipo(EComisionTipo.CONSERVACION.getCodigo());

            //D: se inicializan los campos 'duracion' y 'duracionTotal'
            this.calcularDuracionesComision(1, true);
        }

        this.disableCrearComisionButton = false;
        this.canceladaNuevaComision = false;

        this.nuevaComision.setDuracionAjuste(0.0);
        this.nuevaComision.setConductor("");
        this.nuevaComision.setPlacaVehiculo("");
        this.nuevaComision.setObjeto("");
        this.nuevaComision.setObservaciones("");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método action para el botón "Crear comisión" de la página "Creación de nueva comisión"
     */
    /*
     * D: la comisión que se crea debe estar relacionada con al menos un trámite. Si llegó a este
     * punto hay por lo menos un trámite seleccionado. Al crear la comisión se debe también
     * actualizar el estado de los trámites relacionados
     */
    public void crearComision() {

        LOGGER.debug("creando nueva comisión ...");

        Date currentDate = new Date();
        List<ComisionTramite> comisionTramiteRels;
        ComisionTramite tempComisionTramite;
        String numeroNuevaComision;
        Object[] numeracionAnswer;
        GregorianCalendar calCurrentDate, calFechaInicComision, calFechaFinComision;

        //N: hay que hacer la comparación de fecha actual con fecha inicio de comisión así porque
        // el currentDate queda con la hora actual y el que trae el componmente tiene hora 00:00 
        calCurrentDate = new GregorianCalendar();
        calFechaInicComision = new GregorianCalendar();
        calFechaFinComision = new GregorianCalendar();
        calCurrentDate.setTime(currentDate);
        calFechaInicComision.setTime(this.nuevaComision.getFechaInicio());

        //D: request context para enviar errores
        RequestContext context = RequestContext.getCurrentInstance();

        //D: validar que la fecha inicial sea mayor o igual que la actual
        //OJO: hay que preguntar, adicionalmente, si no son el mismo día, porque la fecha que viene
        //     del componente primefaces tiene hora = 00:00:00 y esa sería menor que la fecha actual
        //     (que es creada con hora)
        if (calFechaInicComision.before(calCurrentDate) &&
            !DateUtils.isSameDay(calFechaInicComision, calCurrentDate)) {
            this.addMensajeError(
                "La fecha inicial de la comisión no puede ser menor que la fecha " +
                "actual!.");
            context.addCallbackParam("error", "pailander en las fechas de comisión");
            return;
        }

        //D: validar que la fecha final de la comisión sea mayor o igual que la fecha inicial.
        //   Ambas fechas pueden ser el mismo día
        calFechaFinComision.setTime(this.nuevaComision.getFechaFin());
        if (calFechaFinComision.before(calFechaInicComision) &&
            !DateUtils.isSameDay(calFechaInicComision, calFechaFinComision)) {
            this.addMensajeError("La fecha final de la comisión no puede ser menor que la fecha " +
                "de inicio de la misma!.");
            context.addCallbackParam("error", "pailander en las fechas de comisión");
            return;
        }

        comisionTramiteRels = new ArrayList<ComisionTramite>();
        this.nuevaComision.setFechaLog(currentDate);
        this.nuevaComision.setUsuarioLog(this.loggedInUser.getLogin() + ": insert comisión.");

        //D: armar objetos de la relación COMISION_TRAMITE y asignárselos a la nueva comisión
        for (Tramite currentTramite : this.selectedTramites) {
            tempComisionTramite = new ComisionTramite();
            tempComisionTramite.setComision(this.nuevaComision);
            tempComisionTramite.setTramite(currentTramite);
            //N: se supone que los trámites que trajo aquí ya fueron manipulados para agregarles el
            //   tiempo de duración (desde la consulta)
            tempComisionTramite.setDuracion(currentTramite.getDuracionTramite());
            tempComisionTramite.setRealizada(ESiNo.NO.getCodigo());
            tempComisionTramite.setFechaLog(currentDate);
            tempComisionTramite.setUsuarioLog(this.loggedInUser.getLogin() +
                ": insert relación comisión-trámite");

            //D: si se trata de comisiones de depuración, el ejecutor o topógrafo viene en el campo 
            // transient de Tramite que se adicionó para ello
            if (this.isSubprocesoDepuracion) {

                tempComisionTramite.setEjecutor(currentTramite.getEjecutorDepuracion());
                tempComisionTramite.setEjecutorNombre(currentTramite.getNombreEjecutorDepuracion());

                Actividad actividadActual = tareasPendientesMB.buscarActividadPorIdTramite(
                    currentTramite.getId());
                if (actividadActual.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION)) {
                    tempComisionTramite.setEjecutorRol(ERolesEjecutorComisionDepuracion.TOPOGRAFO.
                        getNombre());
                } else {
                    tempComisionTramite.setEjecutorRol(ERolesEjecutorComisionDepuracion.EJECUTOR.
                        getNombre());
                }

            } else {
                //D: si no es comisión de Depuración, el rol es siempre 'ejecutor'
                tempComisionTramite.setEjecutorRol(ERolesEjecutorComisionDepuracion.EJECUTOR.
                    getNombre());
                tempComisionTramite.setEjecutor(currentTramite.getFuncionarioEjecutor());
                tempComisionTramite.setEjecutorNombre(currentTramite.getNombreFuncionarioEjecutor());
            }

            comisionTramiteRels.add(tempComisionTramite);
        }
        this.nuevaComision.setComisionTramites(comisionTramiteRels);

        numeracionAnswer = this.getGeneralesService().
            generarNumeracion(ENumeraciones.NUMERACION_TYA, this.idTerritorial, "", "", 0);

        //D: en el [0] viene como string sin '-'
        numeroNuevaComision = (String) numeracionAnswer[0];
        this.nuevaComision.setNumero(numeroNuevaComision);
        this.nuevaComision.setEstado(EComisionEstado.CREADA.name());

        this.nuevaComision =
            this.getTramiteService().insertarComision(this.nuevaComision, this.loggedInUser);

        //D: insertar el registro correspondiente el la tabla COMISION_ESTADO
        ComisionEstado nuevaComisionEstado =
            this.armarNuevaComisionEstado(this.nuevaComision,
                EComisionEstadoMotivo.CREACION.getCodigo());

        try {
            this.getTramiteService().insertarComisionEstado(nuevaComisionEstado, this.loggedInUser);
        } catch (Exception e) {
            LOGGER.error("Error al insertar una ComisionEstado: " + e.getMessage());
        }

        //D: actualizar el estado de los trámites que quedaron asociados a la comisión
        for (Tramite currentTramite : this.selectedTramites) {
            //currentTramite.setEstado(ETramiteEstado.CON_COMISION_ASIGNADA.getCodigo());
            currentTramite.setComisionado(ESiNo.SI.toString());
        }
        this.getTramiteService().actualizarTramites(this.selectedTramites);

        this.addMensajeInfo(
            "La comisión fue creada y los trámites seleccionados fueron asociados a ésta");

        this.selectedTramites = null;
        this.disableCrearComisionButton = true;
        this.currentComisionIsNew = false;

    }
//-------------------------------------------------------------------------------------------------

    public void doNothing() {
        LOGGER.debug("doing nothing");
    }
//-------------------------------------------------------------------------------------------------

    /**
     * action del botón cancelar en la ventana de crear nueva comisión
     */
    public void anularCrearComision() {
        LOGGER.debug("se anula creación de nueva comisión");

        this.nuevaComision = null;
        this.canceladaNuevaComision = true;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * action del botón "Adicionar a comisión seleccionada" en la ventana de comisionar trámites
     *
     * @version 2.0
     */
    public void adicionarTramitesAComision() {

        LOGGER.debug("entra a ManejoComisionesTramitesMB#adicionarTramitesAComision ...");

        List<ComisionTramite> newRows;
        ComisionTramite newRow;
        Comision comision;
        Date currentDate;
        Double nuevaDuracion;
        boolean cumpleRegla, error;

        newRows = new ArrayList<ComisionTramite>();
        comision = new Comision();
        currentDate = new Date();
        error = false;

        comision.setId(this.selectedComisionTramite.getId());

        //D: insertar registros en la tabla COMISION_TRAMITE
        for (Tramite selectedTramite_1 : this.selectedTramites) {
            newRow = new ComisionTramite();
            newRow.setComision(comision);
            newRow.setTramite(selectedTramite_1);
            newRow.setRealizada(ESiNo.NO.getCodigo());
            newRow.setDuracion(selectedTramite_1.getDuracionTramite());
            newRow.setFechaLog(currentDate);
            newRow.setEjecutor(selectedTramite_1.getFuncionarioEjecutor());
            newRow.setEjecutorNombre(selectedTramite_1.getNombreFuncionarioEjecutor());
            newRow.setUsuarioLog(this.loggedInUser.getLogin() +
                ": insert de registro para trámites adicionales a la comisión");
            newRows.add(newRow);
        }

        try {
            this.getTramiteService().insertarRegistrosComisionTramite(newRows);
        } catch (Exception ex) {
            LOGGER.error("error insertando registros de ComisionTramite: " + ex.getMessage());
            error = true;
            return;
        }

        //D: actualizar el estado de los trámites que quedaron asociados a la comisión
        for (Tramite currentTramite : this.selectedTramites) {
            //currentTramite.setEstado(ETramiteEstado.CON_COMISION_ASIGNADA.getCodigo());
            currentTramite.setComisionado(ESiNo.SI.toString());

        }

        try {
            this.getTramiteService().actualizarTramites(this.selectedTramites);
            this.addMensajeInfo("Trámite(s) adicionado a la comisión");
        } catch (Exception ex) {
            LOGGER.error("Error actualizando los trámites que se van a asociar a una comisión " +
                "existente: " + ex.getMessage());
            error = true;
            return;
        }

        //D: recalcular duración comisión
        nuevaDuracion = this.getTramiteService().obtenerDuracionComision2(
            this.selectedComisionTramite.getId());
        this.selectedComisionTramite.setDuracion(nuevaDuracion);

        //D: actualizar comisión
        try {
            this.getTramiteService().actualizarComision(this.loggedInUser,
                this.selectedComisionTramite);
        } catch (Exception ex) {
            LOGGER.error("Error actualizando la comisión existente a la que se asocian trámites" +
                ex.getMessage());
            error = true;
            return;
        }

        //D: validar regla de tiempos de comisión:
        cumpleRegla = UtilidadesWeb.validarReglaDuracionComision(
            this.selectedComisionTramite.getDuracion(),
            this.selectedComisionTramite.getDuracionTotal());

        if (!cumpleRegla) {
            this.addMensajeWarn(
                "Existe diferencia entre el tiempo de la comisión y la duración de la misma.");
        }

        if (error) {
            this.addMensajeError("Ocurrió un error en la adición de trámites a la comisión.");
        }

        this.selectedComisionTramite = null;
        this.selectedTramites = null;
        this.comisionFueModificada = true;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * listener del evento de cerrar la ventana en que se asocian trámites a una comisión (existente
     * o nueva)
     *
     * @param event
     */
    public void modalsCloseListener(CloseEvent event) {
        LOGGER.debug("modal cerrada");

        this.selectedComisionTramite = null;
        this.nuevaComision = null;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * action del botón "Modificar datos de comisión" del tab de "modificación de comisión"
     */
    public void modificarComisionInit() {
        LOGGER.debug("entra a modificar comisión");

    }
//-------------------------------------------------------------------------------------------------

    /**
     * action del botón "Guardar cambios" de la modal de detalles de la comisión
     */
    /*
     * @modified by leidy.gonzalez::#17288::09/06/2016 Se valida que la fecha de modificacion de la
     * comision final no se mayor que la inicial
     */
    public void modificarComision() {
        RequestContext context = RequestContext.getCurrentInstance();

        if (this.selectedComision.getFechaInicio() != null && this.selectedComision.getFechaFin() !=
            null &&
             this.selectedComision.getFechaFin().before(this.selectedComision.getFechaInicio())) {
            this.addMensajeError("La fecha final de la comisión no puede ser menor que la fecha " +
                "de inicio de la misma!.");
            context.addCallbackParam("error", "pailander en las fechas de comisión");
            return;
        }

        this.getTramiteService().actualizarComision(this.loggedInUser, this.selectedComision);
        this.addMensajeInfo("Comisión modificada");
        this.selectedComision = null;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * adiciona eventos al schedule de comisiones según la consulta de comisiones por el estado dado
     *
     * @param estadoComision
     */
    /*
     * @modified pedro.garcia Se hacen todas las consultas a la capa de negocio allá Se tiene en
     * cuenta el tipo de comisión
     */
    private void cargarEventosScheduleComisionesPorEstado(String tipoComision,
        String estadoComision, String estiloEvento) {

        List<EventoScheduleComisionesDTO> eventos;
        DefaultScheduleEvent eventoComision;

        eventos = this.getTramiteService().armarEventosScheduleComisiones(
            this.idTerritorial, tipoComision, estadoComision);

        for (EventoScheduleComisionesDTO evento : eventos) {
            eventoComision = new DefaultScheduleEvent();
            eventoComision.setTitle(evento.getTitulo());
            eventoComision.setStartDate(evento.getFechaInicio());
            eventoComision.setEndDate(evento.getFechaFin());
            eventoComision.setAllDay(true);
            eventoComision.setStyleClass(estiloEvento);

            this.comisionesScheduleModel.addEvent(eventoComision);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "retirar ejecutor" de la ventana detalle de la comisión
     *
     * retira todos los trámites de ese ejecutor que estén en esa comisión
     *
     * @author pedro.garcia
     * @version 2.0
     */
    public void retirarEjecutor() {

        LOGGER.debug("retirando ejecutor de la comisión...");
        Double nuevaDuracion;
        boolean cumpleRegla, cancelarComision = false;

        //D: si estoy retirabdo un ejecutor y la comisión solo tenía uno, entonces se cancela
        if (this.ejecutoresComision.size() <= 1) {
            this.addMensajeInfo(
                "* Se retiró el último ejecutor de la comisión!. Esta queda en estado " +
                EComisionEstado.CANCELADA.toString());
            this.addMensajeInfo("Por favor cierre esta ventana para continuar.");
            cancelarComision = true;
        }

        //N: hay que hacer esto porque puede que la variable que contiene los tramites de un ejecutor
        //   en una comisión sea null o que contenga tramites de otro ejecutor
        consultarTramitesEjecutorComision();

        //D: eliminar las relaciones comisión-trámite
        ArrayList<Long> idsTramites = new ArrayList<Long>();
        for (Tramite tr : this.tramitesEjecutorComision) {
            idsTramites.add(tr.getId());
        }
        this.getTramiteService().eliminarTramiteComisionPorTramitesYComision(
            idsTramites, this.selectedComision.getId());

        //D: como el mf jpa chilla al tratar de actualizar el trámite porque los elementos de la lista
        //   comisionTramites fueron borrados, sin hacer caso de que no hay cascade para ese atributo,
        //   se borran a mano.
        //   Además se marcan esos trámites como no comisionados
        for (Tramite t : this.tramitesEjecutorComision) {
            t.setComisionTramites(null);
            t.setComisionado(ESiNo.NO.getCodigo());
        }

        //D: actualizar trámites.
        //N: se eliminó el cascade de los trámites para el atributo de las relaciones con comision_tramite
        //   porque si se eliminaban a mano esos registros y se hacía luego update del trámite, se 
        //   volvían a crear
        try {
            this.getTramiteService().actualizarTramites(this.tramitesEjecutorComision);
        } catch (Exception ex) {
            LOGGER.error("Error actualizando los trámites retirados de una comisión debido al " +
                "retiro de su ejecutor: " + ex.getMessage());
        }

        //D: recalcular duración comisión. Solo es necesario hacerlo si la comisión no se va a cancelar.
        // Tampoco debe hacerse para las comisiones de Depuración porque la duración no depende de
        // los trámites
        if (!cancelarComision && !this.isSubprocesoDepuracion) {
            nuevaDuracion = this.getTramiteService().
                obtenerDuracionComision2(this.selectedComision.getId());
            this.selectedComision.setDuracion(nuevaDuracion);

            //D: validar regla de tiempos de comisión:
            cumpleRegla = UtilidadesWeb.validarReglaDuracionComision(
                this.selectedComision.getDuracion(), this.selectedComision.getDuracionTotal());
            if (!cumpleRegla) {
                this.addMensajeWarn(
                    "Existe diferencia entre el tiempo de la comisión y la duración de la misma.");
            }
        }

        this.addMensajeInfo("Ejecutor retirado de la comisión. El cambio ya fue guardado.");

        //D: si es necesario, se cambia el estado de la comisión
        if (cancelarComision) {
            this.selectedComision.setEstado(EComisionEstado.CANCELADA.getCodigo());

            //D: si cambia de estado hay que insertar el respectivo registro
            ComisionEstado nuevaComisionEstado =
                this.armarNuevaComisionEstado(this.selectedComision,
                    EComisionEstadoMotivo.RETIRO_TODOS_LOS_EJECUTORES.getCodigo());

            try {
                this.getTramiteService().insertarComisionEstado(nuevaComisionEstado,
                    this.loggedInUser);
            } catch (Exception ex) {
                LOGGER.error("Error insertando el registro de cambio de estado de la comisión " +
                    "luego del retiro de un ejecutor: " + ex.getMessage());
            }

        }

        //D: actualizar comisión
        try {
            this.getTramiteService().actualizarComision(this.loggedInUser, this.selectedComision);
            this.comisionFueModificada = true;
        } catch (Exception ex) {
            LOGGER.error("Error actualizando la comisión luego del retiro de un ejecutor: " +
                ex.getMessage());
        }

        if (cancelarComision) {
            this.permitirGuardarCambiosComision = false;
        }

        this.selectedEjecutorComision = null;

        LOGGER.debug("... terminó de eliminar trámites de ejecutor");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" en la ventana de ajustes de tiempos de la comisión cuando es una
     * comisión seleccionada o una nueva
     *
     * @author pedro.garcia
     * @deprecated desde 16-09-2013 porque se cambiaron reglas para los tiempos de las comisiones.
     * No se usa.
     */
    public void ajustarTiemposComision() {

        LOGGER.debug("on ManejoComisiones#ajustarTiemposComision");

        Double tiempoAjusteComision, duracionTotal;
        boolean ajusteOK = true;

        //D: validación de que la duración tenga valor válido    	
        if (this.esNuevaComisionParaAjusteTiempos) {
            tiempoAjusteComision = this.nuevaComision.getDuracionAjuste();
        } else {
            tiempoAjusteComision = this.selectedComision.getDuracionAjuste();
        }

        if ((tiempoAjusteComision.floatValue() % 0.5) != 0) {
            this.addMensajeError("El valor del ajuste de la duración de la comisión debe ser un " +
                "número positivo múltiplo de 0.5!");
            ajusteOK = false;
        } else {
            if (this.esNuevaComisionParaAjusteTiempos) {
                duracionTotal =
                    this.nuevaComision.getDuracion() + this.nuevaComision.getDuracionAjuste();
                duracionTotal = Utilidades.roundToDecimals(duracionTotal, 2);
                this.nuevaComision.setDuracionTotal(duracionTotal);
                this.nuevaComision.setDuracionAjuste(tiempoAjusteComision);
            } else {
                duracionTotal =
                    this.selectedComision.getDuracion() + this.selectedComision.getDuracionAjuste();
                duracionTotal = Utilidades.roundToDecimals(duracionTotal, 2);
                this.selectedComision.setDuracionTotal(duracionTotal);
                this.selectedComision.setDuracionAjuste(tiempoAjusteComision);
            }
            this.addMensajeInfo("Ajuste de tiempo exitoso!. Cierre esta ventana para continuar");
        }

        if (!ajusteOK) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander en los tiempos de ajuste");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link que se pone en el número de trámites de cada ejecutor de una comisión
     */
    public void consultarTramitesEjecutorComision() {
        this.tramitesEjecutorComision = this
            .getTramiteService()
            .buscarTramitesDeEjecutorEnComision(
                this.selectedEjecutorComision
                    .getIdFuncionarioEjecutor(),
                this.selectedComision.getId());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * método que se llama al dar click en el botón "radicar memorando de comisión" de la ventana
     * "memorando de comisión"
     *
     * @author pedro.garcia
     */
    /*
     * Ahora la acción de este botón sólo debe avanzar el proceso @modified javier.aponte @modified
     * pedro.garcia 12-07-2013 el memo se genera siempre
     */
    public void avanzarProcesoAAprobarComision() {

        this.generarMemorandoComision();

        this.forwardThisProcess();

        this.selectedComisionParaMemo = null;

    }

    /**
     * Mètodo encargado de generara los memorandos de comisión
     *
     * @author javier.aponte
     */
    public void generarMemorandoComision() {

        List<String> rutasMemorandos = new ArrayList<String>();

        this.ejecutoresComision = this.getTramiteService()
            .buscarEjecutoresPorNumeroComision(
                this.selectedComisionParaMemo.getNumero());

        UsuarioDTO ejecutorSeleccionado;

        for (VEjecutorComision ec : this.ejecutoresComision) {

            ejecutorSeleccionado = this.getGeneralesService().getCacheUsuario(ec.
                getIdFuncionarioEjecutor());

            if (ejecutorSeleccionado != null) {

                /** ------ GENERAR MEMORANDO ------ * */
                rutasMemorandos.add(this.generarMemoComision(ejecutorSeleccionado));
            }

        }

        if (!rutasMemorandos.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
            this.reporteMemorandoComision =
                this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

    }

//----------   implementación de metodos del ProcessFlowManager
    /**
     * No hay nada que validar para radicar el memorando de una comisión
     *
     * @return
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }

//------------------------------------------------------------------------------------
    /**
     * @see ProcessFlowManager#setupProcessMessage()
     *
     * Aunque en este caso se está usando el template idividualProcessCU (debido a que el movimiento
     * del proceso se hace cuando se radica el memo de una comisión), en realidad es masivo porque
     * puede haber varios trámites involucrados en la comisión.
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        Actividad activity;
        ActivityMessageDTO messageDTO, messageDTODefault;
        String observaciones = "", processTransition;
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;
        List<ActivityMessageDTO> processMessages;
        List<UsuarioDTO> usuariosDestinoPorRol;
        UsuarioDTO usuarioDestinoActividad;

        // D: no debería ser null
        if (this.selectedComisionParaMemo == null) {
            return;
        }

        // D: se buscan los trámites asociados a la comisión a la que se va a generar memorando,
        // ya que estos son los que se deben mover en el bpm
        this.tramitesAsociadosComision = this.getTramiteService().buscarTramitesDeComision(
            this.selectedComisionParaMemo.getId(), this.selectedComisionParaMemo.getTipoComision());

        // D: las observaciones que se dan en el template individualProcessCU se
        // setean en el atributo 'message' del ProcessFlowManager. En el caso de actividades masivas
        // este mensaje no se usa, pero hay que sacar el texto de observaciones de alli para colocarlo
        // en cada mensaje de la lista
        if (this.getMessage() != null) {
            messageDTODefault = this.getMessage();
            observaciones = messageDTODefault.getComment();
        }

        try {
            processMessages = new ArrayList<ActivityMessageDTO>();
            if (this.tramitesAsociadosComision != null) {
                for (Tramite currentTramite : this.tramitesAsociadosComision) {
                    messageDTO = new ActivityMessageDTO();
                    messageDTO.setUsuarioActual(this.loggedInUser);
                    solicitudCatastral = new SolicitudCatastral();
                    usuariosActividad = new ArrayList<UsuarioDTO>();
                    usuariosDestinoPorRol = new ArrayList<UsuarioDTO>();

                    activity = this.tareasPendientesMB
                        .buscarActividadPorIdTramite(currentTramite.getId());
                    messageDTO.setComment(observaciones);
                    messageDTO.setActivityId(activity.getId());

                    // Transición
                    if (this.isSubprocesoDepuracion) {
                        processTransition = ProcesoDeConservacion.ACT_DEPURACION_APROBAR_COMISION;
                    } //D: si es una revisión de avalúo, una mutación de cuarta o
                    //si la solicitud es de tipo vía gubernativa (trámites de tipo 'recurso x' o queja)
                    else if (currentTramite.getTipoTramite().equals(
                        ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo()) ||
                         (currentTramite.getClaseMutacion() != null &&
                        currentTramite.getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo())) ||
                         currentTramite.isViaGubernativa()) {
                        processTransition = ProcesoDeConservacion.ACT_EJECUCION_APROBAR_COMISION;
                    } else {
                        processTransition = ProcesoDeConservacion.ACT_ASIGNACION_APROBAR_COMISION;
                    }

                    solicitudCatastral.setTransicion(processTransition);

                    // Siempre existira un director para la territorial
                    usuariosDestinoPorRol = getTramiteService().
                        buscarFuncionariosPorRolYTerritorial(
                            this.loggedInUser.getDescripcionTerritorial(), ERol.DIRECTOR_TERRITORIAL);

                    if (usuariosDestinoPorRol == null || usuariosDestinoPorRol.isEmpty()) {
                        this.addMensajeError(
                            "No fue posible recuperar la información del director territorial.");
                        return;
                    }

                    // No debería pasar que ésta lista encuentre vacía o nula,
                    // siempre es uno.
                    usuarioDestinoActividad = usuariosDestinoPorRol.get(0);

                    usuariosActividad.add(usuarioDestinoActividad);
                    solicitudCatastral.setUsuarios(usuariosActividad);

                    // D: no es necesario hacer esto si los datos no cambian en
                    // el trámite. Por eso en
                    // la consulta que se hace para los trámites no se hace
                    // fetch de la solicitud
                    // solicitudCatastral.setNumeroRadicacion(currentTramite.getNumeroRadicacion());
                    // solicitudCatastral.setNumeroSolicitud(currentTramite.getSolicitud().getNumero());
                    messageDTO.setSolicitudCatastral(solicitudCatastral);
                    processMessages.add(messageDTO);
                }

                if (!processMessages.isEmpty()) {
                    this.setMasiveMessages(processMessages);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error en setupProcessMessage");
            this.addMensajeError("Error al avanzar el proceso.");
        }

    }
//------------------------------------------------------------------------------------

    /**
     * 1. Actualiza el estado de la comisión a la que se le generó y radicó el memorando 2. registra
     * el cambio de estado de la comisión 3. Actualiza el campo comisionado de los trámites
     * involucrados en la comisión
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        //D: 1.        
        if (this.selectedComisionParaMemo != null) {
            this.selectedComisionParaMemo.setEstado(EComisionEstado.POR_APROBAR.getCodigo());
            //OJO: se llama el que recibe una VComision

            try {
                this.getTramiteService().actualizarComision(this.loggedInUser,
                    this.selectedComisionParaMemo);
            } catch (Exception ex) {
                LOGGER.error("Excepción actualizando comisión: " + ex.getMessage());
                return;
            }

            // 2.
            ComisionEstado nuevaComisionEstado =
                this.armarNuevaComisionEstado(this.selectedComisionParaMemo,
                    EComisionEstadoMotivo.PENDIENTE_DE_APROBACION.getCodigo());

            try {
                this.getTramiteService().insertarComisionEstado(nuevaComisionEstado,
                    this.loggedInUser);
            } catch (Exception ex) {
                LOGGER.error("Excepción insertando nuevo estado de comisión: " + ex.getMessage());
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * en el caso en que el llamado al BPM se debe hacer desde un botón que no está definido en
     * alguno de los template de procesos (porque por ejemplo hay al mismo tiempo dos botones que
     * mueven la actividad a diferentes transiciones), se debe hacer el llamado a los métodos como
     * si se estuviera implementando alguno de los métodos que avanzan el proceso del ProcessMB
     *
     * @author pedro.garcia
     */
    private void forwardThisProcess() {

        boolean ok = false;
        if (this.validateProcess()) {

            this.setupProcessMessage();
            // D: se hace primero lo del process para tratar de que no queden
            // los trámites en estado
            // inconsistente
            try {
                super.forwardProcess();
                ok = true;
            } catch (Exception e) {
                LOGGER.error("Error en procesos al mover trámites desde comisionar: " +
                     e.getMessage());
                this.addMensajeError("Error al avanzar el proceso.");
                return;
            }

            if (ok) {
                this.doDatabaseStatesUpdate();
            }
        }

        //OJO: no se debe hacer esto aquí porque al dar click en "radicar" NO se debe quitar el MB
        //     de la sesión para permitir que se refresque el reporte y que quede habilitado el botón "imprimir"
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        //Utilidades.removerManagedBean("manejoComisionesTramites");		
        //D: forzar el refrezco del árbol de tareas
        //this.tareasPendientesMB.init();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna la cadena que se usa como default action. Hubo que crear un método porque no funcionó
     * pasar un string como parámetro para el action de un botón en el template.
     *
     * Se definió una regla de navegación con esta
     *
     * @author pedro.garcia
     * @return
     */
    public String getDefaultAction() {
        return ConstantesNavegacionWeb.COMISIONAR_TRAMITES_TERRENO;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón cerrar de la ventana principal
     */
    public String cerrarVentanaCU() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        // ya no hace falta porque al llamar el init de tareasPendientes se llama a otro método que
        // limpia todos los mb de la sesión
        //Utilidades.removerManagedBean("manejoComisionesTramites");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma un objeto ComisionEstado con los datos necesarios
     *
     * PRE: los datos de la comisión asociada ya han sido definidos
     *
     * @author pedro.garcia
     * @return
     */
    private ComisionEstado armarNuevaComisionEstado(Comision comision, String motivo) {

        ComisionEstado answer = null;
        Date currentDate;

        if (this.nuevaComision == null) {
            return answer;
        }

        currentDate = new Date(System.currentTimeMillis());
        answer = new ComisionEstado();
        answer.setFecha(currentDate);
        answer.setFechaInicio(comision.getFechaInicio());
        answer.setFechaFin(comision.getFechaFin());
        answer.setComision(comision);
        answer.setEstado(comision.getEstado());
        answer.setFechaLog(currentDate);
        answer.setMotivo(motivo);
        answer.setUsuarioLog(this.loggedInUser.getLogin());

        return answer;
    }
//---------------

    private ComisionEstado armarNuevaComisionEstado(VComision comision, String motivo) {

        ComisionEstado answer;
        Date currentDate;
        Comision comisionTemp;

        currentDate = new Date(System.currentTimeMillis());
        answer = new ComisionEstado();
        answer.setFecha(currentDate);
        answer.setFechaInicio(comision.getFechaInicio());
        answer.setFechaFin(comision.getFechaFin());
        answer.setEstado(comision.getEstado());
        answer.setFechaLog(currentDate);
        answer.setMotivo(motivo);
        //D: no se guarda documento porque el que interesa es el de cuando se aprueba la comisión
        //answer.setMemorandoDocumento(this.documentoMemorandoComision);
        answer.setUsuarioLog(this.loggedInUser.getLogin());

        comisionTemp = new Comision();
        comisionTemp.setId(comision.getId());
        answer.setComision(comisionTemp);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método invocado para retirar los trámites de una comisión.
     */
    public void retirarTramitesComision() {
        this.tramitesDeComisionMB.retirarTramitesComision();
        this.comisionFueModificada = true;
        this.selectedComision = null;
    }
//--------------------------------------------------------------------------------------------------

    public void cierreVentanaNuevaComisionListener() {

        if (this.canceladaNuevaComision) {
            //D: se hace esto para que al cancelar la creación de la comisión -en la ventana
            //  correspondiente- no se cierre la ventana que abrió esa ventana
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "don't close me");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * genera el reporte que contiene el memorando de comisión. Genera el reporte sin el número de
     * radicado
     *
     * @author javier.aponte
     */
    public String generarMemoComision(UsuarioDTO ejecutorSeleccionado) {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.MEMORANDO_COMISION;
        if (isSubprocesoDepuracion) {
            enumeracionReporte = EReporteServiceSNC.MEMORANDO_COMISION_DEPURACION;
        }

        Map<String, String> parameters = new HashMap<String, String>();

        UsuarioDTO directorTerritorial = null;

        parameters.put("COMISION_ID", String.valueOf(this.selectedComisionParaMemo.getId()));
        if (!isSubprocesoDepuracion) {
            parameters.put("FUNCIONARIO_EJECUTOR", ejecutorSeleccionado.getLogin());
        }

        if (this.directoresTerritoriales == null) {
            this.directoresTerritoriales =
                (List<UsuarioDTO>) this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.loggedInUser.getDescripcionTerritorial(),
                    ERol.DIRECTOR_TERRITORIAL);
        }

        if (this.directoresTerritoriales != null && !this.directoresTerritoriales.isEmpty() &&
             this.directoresTerritoriales.get(0) != null) {
            directorTerritorial = this.directoresTerritoriales.get(0);
        }

        if (directorTerritorial != null && directorTerritorial.getNombreCompleto() != null) {
            parameters.put("NOMBRE_DIRECTOR_TERRITORIAL", directorTerritorial.getNombreCompleto());
        }

        this.reporteMemorandoComision = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.loggedInUser);

        return this.reporteMemorandoComision.getRutaCargarReporteAAlfresco();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor de fecha final de la comisión. Se debe recalcular la duración
     * total de la comisión que depende de la fecha final y la inicial. Para las comisiones de
     * Depuración la duración es igual a la duración total.
     *
     * @author pedro.garcia
     */
    public void cambioFechaFinalComisionListener(DateSelectEvent dsEvent) {

        //N: en este punto la fecha final no ha sido definida en el value del componente (aquí significa
        //  que this.nuevaComision.getFechaFin() es null), por lo que  hay que tomarla del evento.
        // Esto se debe a que el listener se ejecuta primero que el setter de la propiedad que contiene
        // el valor del componente que se obliga a procesar (definido con el atributo 'process')
        LOGGER.debug("seleccionó fecha final: " + dsEvent.getDate());

        if (this.currentComisionIsNew) {
            this.nuevaComision.setFechaFin(dsEvent.getDate());
        } else {
            this.selectedComision.setFechaFin(dsEvent.getDate());
        }

        if (this.isSubprocesoDepuracion) {
            this.calcularDuracionesComision(2, false);
        } else {
            this.calcularDuracionesComision(1, false);
        }

    }
//------------------

    /**
     * listener del cambio de valor de fecha inicial de la comisión. Se usa para las comisiones de
     * de Depuración.
     *
     * @author pedro.garcia
     */
    /*
     * En el momento en que se cambia la fecha final, cuando se ejecuta el listener de esa acción,
     * la fecha inicial puede estar en null, porque no se ha seleccionado, o porque no se ha
     * procesado (ya que el listener se ejecuta primero que el setter de la propiedad). Debido a
     * esto, se fuerza a que al cambiar seleccionar la fecha inicial se venga al MB para que al
     * momento de calcular la duración de la comisión de depuración, esta esté disponible.
     */
    public void cambioFechaInicialComisionListener(DateSelectEvent dsEvent) {

        //N: en este punto la fecha no se ha sido definida en el value del componente (aquí significa
        //  que this.nuevaComision.getFechaFin() es null), por lo que  hay que tomarla del evento
        LOGGER.debug("seleccionó fecha inicial: " + dsEvent.getDate());

        if (this.currentComisionIsNew) {
            this.nuevaComision.setFechaInicio(dsEvent.getDate());
        } else {
            this.selectedComision.setFechaInicio(dsEvent.getDate());
        }

        if (this.isSubprocesoDepuracion) {
            this.calcularDuracionesComision(2, false);
        } else {
            this.calcularDuracionesComision(1, false);
        }

    }
//--------------------------------------------------------------------------------------------------    

    /** --------------------------------------------------------------------- */
    /**
     * Listener sobre la selección de una comisión, sirve para el 'visor GIS', realiza el set de los
     * números prediales a dibujar en el visor asociados a los trámites de la comision seleccionada
     *
     * @author javier.aponte
     *
     */
    public void cargarPrediosVisorSelectListener() {

        String numerosPrediales = "";
        if (this.selectedComision != null) {

            List<Long> idsComisiones = new ArrayList<Long>();

            idsComisiones.add(this.selectedComision.getId());

            if (!idsComisiones.isEmpty()) {

                // Búsqueda de los trámites asociados a la comisión, los cuales
                // avanzaran en el proceso.
                List<Tramite> tramitesComisionesSeleccionadas = this
                    .getTramiteService().buscarTramitesDeComisiones(
                        idsComisiones);

                // Búsqueda de los predios de los trámites.
                if (tramitesComisionesSeleccionadas != null &&
                     !tramitesComisionesSeleccionadas.isEmpty()) {
                    List<Long> idsTramites = new ArrayList<Long>();
                    for (Tramite t : tramitesComisionesSeleccionadas) {
                        // Se recuperan los id's de os trámites para encontrar
                        // sus predios asociadios.

                        idsTramites.add(t.getId());
                    }

                    if (!idsTramites.isEmpty()) {

                        List<Predio> prediosAsociadosALosTramites = this
                            .getConservacionService()
                            .buscarPrediosPorListaDeIdsTramite(idsTramites);

                        // Set de los predios a visualizar en el visor.
                        if (prediosAsociadosALosTramites != null &&
                             !prediosAsociadosALosTramites.isEmpty()) {

                            numerosPrediales = new String();
                            for (Predio p : prediosAsociadosALosTramites) {
                                numerosPrediales += p.getNumeroPredial() + ",";
                            }
                            this.prediosParaZoom = numerosPrediales.substring(0,
                                numerosPrediales.length() - 1);
                        }
                    }
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * Listener sobre la desselección de una comisión, sirve para el 'visor GIS', limpia los números
     * prediales a dibujar en el visor asociados a los trámites de la comision seleccionada
     *
     * @author javier.aponte
     * @param event
     */
    public void cargarPrediosVisorUnSelectListener(UnselectEvent event) {
        this.prediosParaZoom = null;
    }

    /**
     * Obtiene los predios de la seleccion de la tabla para hacerles zoom en el visor GIS
     *
     * @author lorena.salamanca
     * @param event
     */
    public void obtenerPrediosParaZoomSelectListener(SelectEvent event) {
        String codigo;
        this.prediosParaZoom = "";
        for (int k = 0; k < this.selectedTramites.length; k++) {
            if (this.selectedTramites[k].isQuinta()) {
                codigo = this.selectedTramites[k].getNumeroPredial();
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }
            if (this.selectedTramites[k].isSegundaEnglobe()) {
                List<Long> idTramites = new LinkedList<Long>();
                idTramites.add(this.selectedTramites[k].getId());
                List<String> codigosSegunda;
                codigosSegunda = this.getTramiteService().getNumerosPredialesByTramiteId(idTramites);
                codigo = codigosSegunda.toString().replace("[", "");
                codigo = codigo.replace("]", "");
                codigo = codigo.replace(" ", "");
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }

            if (this.selectedTramites[k].isSegundaEnglobe() == false &&
                this.selectedTramites[k].isQuinta() == false) {
                codigo = this.selectedTramites[k].getPredio().getNumeroPredial();
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------		

    private boolean contienePredio(String codigo) {
        return this.prediosParaZoom.contains(codigo);
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * Retira los predios de la seleccion de la tabla para retirarlos del zoom del visor GIS
     *
     * @author lorena.salamanca
     * @param event
     */
    public void eliminarPrediosParaZoomUnselectListener(UnselectEvent event) {
        for (int k = 0; k < this.selectedTramites.length; k++) {
            String codigo;
            codigo = this.selectedTramites[k].getNumeroPredial();
            this.prediosParaZoom = this.prediosParaZoom.replace(codigo, "");
            this.prediosParaZoom = this.prediosParaZoom.replace(",,", ",");

            if (this.prediosParaZoom.charAt(this.prediosParaZoom.length() - 1) == ',') {
                this.prediosParaZoom = this.prediosParaZoom.substring(0,
                    this.prediosParaZoom.length() - 1);
            }
            if (this.prediosParaZoom.charAt(0) == ',') {
                this.prediosParaZoom = this.prediosParaZoom.substring(1,
                    this.prediosParaZoom.length());
            }
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Listener del cierre de la ventana de modificación de la comisión. Se usa para garantizar que
     * se debe seleccionar de nuevo una comisión para que no queden valores pegados de la comisión
     * seleccionada anteriormente, que han sido modificados en el flujo de esa ventana, pero que no
     * se guardaron en bd
     *
     * @author pedro.garcia
     */
    public void cerrarVentanaModificacionComisionListener() {
        this.selectedComision = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * calcula y asigna las duraciones (duración y duración total) de las comisiones según su tipo
     *
     * @author pedro.garcia 16-09-2013
     * @param tipoComision
     * @param recalcularDuracion indica si se debe recalcular el campo 'duracion' para las
     * comisiones de Conservación que es donde depende de los trámites. Se usa para evitar cálculos
     * costosos innecesarios. Se debe usar como true cuando se está creando la comisión. Si es true,
     * la variable this.selectedTramites debe haber sido inicializada.
     */
    /*
     * @modified by leidy.gonzalez::#17288:: 11/05/2016:: Se realiza validacion para verificar que
     * la fecha final de la coision no sea menor que la inicial, tanto cuando la comision es nueva o
     * se esta modificando
     */
    private void calcularDuracionesComision(int tipoComision, boolean recalcularDuracion) {

        HashMap<String, Double> duracionTramitesPorEjecutor;
        String ejecutor;
        Double currentDTE;
        double maxTemporal, duracion, duracionTotal;
        Comision tempComision;
        boolean cumpleRegla;

        duracion = 0.0;
        tempComision = new Comision();

        //N: se asignan los valores involucrados a una comisión temporal para modificarlos y luego
        //  usarlos en la comisión que se está modificando
        if (this.currentComisionIsNew) {

            if (this.nuevaComision.getFechaInicio() != null && this.nuevaComision.getFechaFin() !=
                null &&
                 this.nuevaComision.getFechaFin().before(this.nuevaComision.getFechaInicio())) {
                this.addMensajeWarn("La fecha final de la comision es menor que la fecha inicial.");
            }

            tempComision.setFechaInicio(
                (this.nuevaComision.getFechaInicio() != null) ? this.nuevaComision.getFechaInicio() :
                null);
            tempComision.setFechaFin(
                (this.nuevaComision.getFechaFin() != null) ? this.nuevaComision.getFechaFin() : null);
            tempComision.setDuracion(
                (this.nuevaComision.getDuracion() != null) ? this.nuevaComision.getDuracion() : 0.0);
            tempComision.setDuracionTotal(
                (this.nuevaComision.getDuracionTotal() != null) ? this.nuevaComision.
                getDuracionTotal() : 0.0);
        } else {

            if (this.selectedComision.getFechaInicio() != null && this.selectedComision.
                getFechaFin() != null &&
                 this.selectedComision.getFechaFin().before(this.selectedComision.getFechaInicio())) {
                this.addMensajeWarn("La fecha final de la comision es menor que la fecha inicial.");
            }

            tempComision.setFechaInicio(
                (this.selectedComision.getFechaInicio() != null) ? this.selectedComision.
                getFechaInicio() : null);
            tempComision.setFechaFin(
                (this.selectedComision.getFechaFin() != null) ? this.selectedComision.getFechaFin() :
                null);
            tempComision.setDuracion(
                (this.selectedComision.getDuracion() != null) ? this.selectedComision.getDuracion() :
                0.0);
            tempComision.setDuracionTotal(
                (this.selectedComision.getDuracionTotal() != null) ? this.selectedComision.
                getDuracionTotal() : 0.0);
        }

        switch (tipoComision) {
            // de Conservación
            case 1: {
                if (recalcularDuracion) {
                    duracionTramitesPorEjecutor = new HashMap<String, Double>();
                    for (Tramite tramite : this.selectedTramites) {
                        ejecutor = tramite.getFuncionarioEjecutor();
                        if (duracionTramitesPorEjecutor.containsKey(ejecutor)) {
                            currentDTE = duracionTramitesPorEjecutor.get(ejecutor);
                            currentDTE = currentDTE + tramite.getDuracionTramite();
                            duracionTramitesPorEjecutor.put(ejecutor, currentDTE);
                        } else {
                            duracionTramitesPorEjecutor.put(ejecutor, tramite.getDuracionTramite());
                        }
                    }

                    for (Double duracionTramites : duracionTramitesPorEjecutor.values()) {
                        maxTemporal = duracionTramites;
                        if (maxTemporal > duracion) {
                            duracion = maxTemporal;
                        }
                    }

                    tempComision.setDuracion(duracion);
                }

                break;
            }
            // de Depuración
            case 2: {
                duracion = (tempComision.getFechaInicio() != null && tempComision.getFechaFin() !=
                    null) ?
                        UtilidadesWeb.calcularNumeroDiasEntreFechas(
                            tempComision.getFechaInicio(), tempComision.getFechaFin()) :
                        0.0;
                tempComision.setDuracion(duracion);

                break;
            }

            default: {
                break;
            }

        }

        if (tempComision.getFechaInicio() != null && tempComision.getFechaFin() != null) {
            duracionTotal = UtilidadesWeb.calcularNumeroDiasEntreFechas(
                tempComision.getFechaInicio(), tempComision.getFechaFin());
            //N: hay una regla que dice que se le sume 0.5 días
            duracionTotal += 0.5;
            tempComision.setDuracionTotal(duracionTotal);

            cumpleRegla = UtilidadesWeb.validarReglaDuracionComision(
                tempComision.getDuracion(), tempComision.getDuracionTotal());
            if (!cumpleRegla) {
                this.addMensajeWarn(
                    "Existe diferencia entre la duración total de la comisión y la duración de los trámites.");
            }
        }

        if (this.currentComisionIsNew) {
            this.nuevaComision.setFechaInicio(
                (tempComision.getFechaInicio() != null) ? tempComision.getFechaInicio() : null);
            this.nuevaComision.setFechaFin(
                (tempComision.getFechaFin() != null) ? tempComision.getFechaFin() : null);
            this.nuevaComision.setDuracion(
                (tempComision.getDuracion() != null) ? tempComision.getDuracion() : 0.0);
            this.nuevaComision.setDuracionTotal(
                (tempComision.getDuracionTotal() != null) ? tempComision.getDuracionTotal() : 0.0);
        } else {
            this.selectedComision.setFechaInicio(
                (tempComision.getFechaInicio() != null) ? tempComision.getFechaInicio() : null);
            this.selectedComision.setFechaFin(
                (tempComision.getFechaFin() != null) ? tempComision.getFechaFin() : null);
            this.selectedComision.setDuracion(
                (tempComision.getDuracion() != null) ? tempComision.getDuracion() : 0.0);
            this.selectedComision.setDuracionTotal(
                (tempComision.getDuracionTotal() != null) ? tempComision.getDuracionTotal() : 0.0);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "cerrar" an la pantalla de modificación de una comisión. Se usa para poner
     * en null los elementos seleccionados para que no queden seleccionados.
     *
     * @author pedro.garcia
     */
    public void limpiarSeleccionadosModifComision() {

        this.selectedComision = null;
        this.selectedEjecutorComision = null;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * llama al método que carga los eventos de la programción de comisiones con los diferentes
     * valores según los estados que se requiere mostrar.
     *
     * Actualmente debe mostrar las comisiones en los estados creada : cuando se crea pero no se ha
     * aprobado por_ejecutar : cuando se aprueba en_ejecucion y las comisiones de todos los tipos
     * (actualmente depuración y conservación)
     *
     * @author pedro.garcia
     */
    private void cargarEventosComisiones() {

        String estadoComision, estiloEventoSchedule, tipoComisionConservacion, tipoComisionDepuracion;

        tipoComisionConservacion = EComisionTipo.CONSERVACION.getCodigo();
        tipoComisionDepuracion = EComisionTipo.DEPURACION.getCodigo();

        //D: los estilos para los diferentes eventos están definidos en el css 
        estadoComision = EComisionEstado.CREADA.getCodigo();
        estiloEventoSchedule = "comisionesCreadas";
        cargarEventosScheduleComisionesPorEstado(tipoComisionConservacion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.POR_EJECUTAR.getCodigo();
        estiloEventoSchedule = "comisionesPorEjecutar";
        cargarEventosScheduleComisionesPorEstado(tipoComisionConservacion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.EN_EJECUCION.getCodigo();
        estiloEventoSchedule = "comisionesEnEjecucion";
        cargarEventosScheduleComisionesPorEstado(tipoComisionConservacion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.CREADA.getCodigo();
        estiloEventoSchedule = "comisionesCreadas";
        cargarEventosScheduleComisionesPorEstado(tipoComisionDepuracion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.POR_EJECUTAR.getCodigo();
        estiloEventoSchedule = "comisionesPorEjecutar";
        cargarEventosScheduleComisionesPorEstado(tipoComisionDepuracion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.EN_EJECUCION.getCodigo();
        estiloEventoSchedule = "comisionesEnEjecucion";
        cargarEventosScheduleComisionesPorEstado(tipoComisionDepuracion, estadoComision,
            estiloEventoSchedule);

    }

//end of class
}
