package co.gov.igac.snc.web.mb.generales;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;

import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @description Managed bean para parametrizar el cargue de las plantillas de las secciones de
 * encabezado, pie de página y marca de agua, para que los reportes de las delegadas, territoriales
 * y/o unidades operativas tengan su propio formato de reportes generados en el SNC..
 *
 * @cu CU-NP-CO 191 Parametrizar el cargue de formato de documento
 *
 * @version 1.0
 *
 * @author david.cifuentes
 */
@Component("parametrizacionCarguePlantilla")
@Scope("session")
public class ParametrizacionCarguePlantillaDocumentoMB extends SNCManagedBean
    implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -7311846763503557925L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ParametrizacionCarguePlantillaDocumentoMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /** ---------------------------------- */
    /** ---------- CONSTANTES ------------ */
    /** ---------------------------------- */
    private static final String WATER_MARK = "WATER MARK";

    private static final String SEPARADOR_CARPETA = System
        .getProperty("file.separator");

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable usada para capturar el código de una territorial seleccionada de la lista de
     * territoriales.
     */
    private String territorialSeleccionadaCodigo;

    /**
     * Lista de select items de las territoriales
     */
    private ArrayList<SelectItem> territorialesItemList;

    /**
     * Variable usada para capturar el código de una UOC seleccionada de la lista de UOCs de la
     * territorial.
     */
    private String unidadOperativaSeleccionadaCodigo;

    /**
     * Lista de select items de las UOC.
     */
    private List<SelectItem> unidadesOperativasCatastroItemList;

    /**
     * Fecha de la plantilla de la territorial o UOC
     */
    private Date fechaPlantilla;

    /**
     * Variable para almacenar la sección seleccionada del reporte
     */
    private String seccionSeleccionada;

    /**
     * Objeto que contiene los datos del reporte generico o en firme
     */
    private ReporteDTO reportePlantilla;

    /**
     * Objeto que contiene los datos del reporte temporal
     */
    private ReporteDTO reporteTemporal;

    /**
     * URLs por donde se puede visualizar la secciones de la plantilla
     */
    private String urlWebCabecera;
    private String urlWebMarcaDeAgua;
    private String urlWebPieDePagina;

    /**
     * Archivos de las secciones de la plantilla
     */
    private File archivoCabecera;
    private File archivoMarcaDeAgua;
    private File archivoPieDePagina;

    /**
     * Variable para visualizar las secciones de una plantilla que se encuentra en firme para una
     * territorial o UOC, o para la plantilla genérica.
     */
    private PlantillaSeccionReporte plantilla;

    /**
     * Nombre de la sección seleccionada
     */
    private String seccionNombre;

    /**
     * Variable para verificar si existe o no una plantilla en firme y así habilitar las opciones de
     * visualización de plantilla genérica.
     */
    private boolean plantillaGenericaBool;

    /**
     * determina si un usuario pertenece a una entidad delegada
     */
    private boolean usuarioDelegado;
    
    /**
     * determina si un usuario pertenece a una entidad delegada
     */
    private boolean usuarioHabilitado;

    /**
     * Habilita la opcion de guardar cambios en la plantilla
     */
    private boolean banderaPonerEnFirme;
    private boolean plantillaHabilitada;

    /**
     * Variable para deshabilitar la selección de la UOC cuando el usuario es una UOC debido que
     * esta se precarga.
     */
    private boolean deshabilitarSeleccionUOC;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - CierreAnioParaMunicipiosConservacionMB");
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.usuarioHabilitado = MenuMB.getMenu().isUsuarioHabilitado();
        this.banderaPonerEnFirme = false;
        this.plantillaHabilitada = false;

        // Inicialización de variables
        this.territorialSeleccionadaCodigo = null;
        this.unidadOperativaSeleccionadaCodigo = null;
        this.urlWebCabecera = null;
        this.urlWebMarcaDeAgua = null;
        this.urlWebPieDePagina = null;

        this.territorialesItemList = new ArrayList<SelectItem>();
        this.unidadesOperativasCatastroItemList = new ArrayList<SelectItem>();

        // Territoriales
        if (usuarioDelegado || usuarioHabilitado) {
            EstructuraOrganizacional delegada = this.getGeneralesService().
                obtenerTerritorialPorCodigo(this.getUsuario().getCodigoTerritorial());
            this.territorialesItemList.add(new SelectItem(delegada
                .getCodigo(), delegada.getNombre()));
        } else {
            for (EstructuraOrganizacional territorial : this.getGeneralesService()
                .getCacheTerritoriales()) {
                if (this.getUsuario().getCodigoTerritorial().equals(territorial.getCodigo())) {
                    this.territorialesItemList.add(new SelectItem(territorial
                        .getCodigo(), territorial.getNombre()));
                }
            }
        }
        if (this.usuario.getCodigoUOC() != null &&
            !this.usuario.getCodigoUOC().isEmpty()) {
            this.plantilla = this.getGeneralesService().
                buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(this.usuario.
                    getCodigoUOC());
            this.deshabilitarSeleccionUOC = true;
        } else {
            this.plantilla = this.getGeneralesService().
                buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(this.usuario.
                    getCodigoTerritorial());
            this.deshabilitarSeleccionUOC = false;
        }

        // Plantilla genérica
        if (this.plantilla == null) {
            this.plantillaGenericaBool = true;
        }
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public boolean isBanderaPonerEnFirme() {
        return banderaPonerEnFirme;
    }

    public void setBanderaPonerEnFirme(boolean banderaPonerEnFirme) {
        this.banderaPonerEnFirme = banderaPonerEnFirme;
    }

    public boolean isPlantillaHabilitada() {
        return plantillaHabilitada;
    }

    public void setPlantillaHabilitada(boolean plantillaHabilitada) {
        this.plantillaHabilitada = plantillaHabilitada;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return territorialesItemList;
    }

    public void setTerritorialesItemList(
        ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public ReporteDTO getReportePlantilla() {
        return reportePlantilla;
    }

    public void setReportePlantilla(ReporteDTO reportePlantilla) {
        this.reportePlantilla = reportePlantilla;
    }

    public File getArchivoCabecera() {
        return archivoCabecera;
    }

    public void setArchivoCabecera(File archivoCabecera) {
        this.archivoCabecera = archivoCabecera;
    }

    public File getArchivoMarcaDeAgua() {
        return archivoMarcaDeAgua;
    }

    public void setArchivoMarcaDeAgua(File archivoMarcaDeAgua) {
        this.archivoMarcaDeAgua = archivoMarcaDeAgua;
    }

    public File getArchivoPieDePagina() {
        return archivoPieDePagina;
    }

    public void setArchivoPieDePagina(File archivoPieDePagina) {
        this.archivoPieDePagina = archivoPieDePagina;
    }

    public boolean isDeshabilitarSeleccionUOC() {
        return deshabilitarSeleccionUOC;
    }

    public void setDeshabilitarSeleccionUOC(boolean deshabilitarSeleccionUOC) {
        this.deshabilitarSeleccionUOC = deshabilitarSeleccionUOC;
    }

    public Date getFechaPlantilla() {
        return fechaPlantilla;
    }

    public void setFechaPlantilla(Date fechaPlantilla) {
        this.fechaPlantilla = fechaPlantilla;
    }

    public String getUrlWebCabecera() {
        return urlWebCabecera;
    }

    public void setUrlWebCabecera(String urlWebCabecera) {
        this.urlWebCabecera = urlWebCabecera;
    }

    public ReporteDTO getReporteTemporal() {
        return reporteTemporal;
    }

    public void setReporteTemporal(ReporteDTO reporteTemporal) {
        this.reporteTemporal = reporteTemporal;
    }

    public boolean isPlantillaGenericaBool() {
        return plantillaGenericaBool;
    }

    public void setPlantillaGenericaBool(boolean plantillaGenericaBool) {
        this.plantillaGenericaBool = plantillaGenericaBool;
    }

    public PlantillaSeccionReporte getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(PlantillaSeccionReporte plantilla) {
        this.plantilla = plantilla;
    }

    public String getUrlWebMarcaDeAgua() {
        return urlWebMarcaDeAgua;
    }

    public void setUrlWebMarcaDeAgua(String urlWebMarcaDeAgua) {
        this.urlWebMarcaDeAgua = urlWebMarcaDeAgua;
    }

    public String getUrlWebPieDePagina() {
        return urlWebPieDePagina;
    }

    public void setUrlWebPieDePagina(String urlWebPieDePagina) {
        this.urlWebPieDePagina = urlWebPieDePagina;
    }

    public String getUnidadOperativaSeleccionadaCodigo() {
        return unidadOperativaSeleccionadaCodigo;
    }

    public void setUnidadOperativaSeleccionadaCodigo(
        String unidadOperativaSeleccionadaCodigo) {
        this.unidadOperativaSeleccionadaCodigo = unidadOperativaSeleccionadaCodigo;
    }

    public String getSeccionNombre() {
        return seccionNombre;
    }

    public void setSeccionNombre(String seccionNombre) {
        this.seccionNombre = seccionNombre;
    }

    public String getSeccionSeleccionada() {
        return seccionSeleccionada;
    }

    public void setSeccionSeleccionada(String seccionSeleccionada) {
        this.seccionSeleccionada = seccionSeleccionada;
    }

    public List<SelectItem> getUnidadesOperativasCatastroItemList() {
        return unidadesOperativasCatastroItemList;
    }

    public void setUnidadesOperativasCatastroItemList(
        List<SelectItem> unidadesOperativasCatastroItemList) {
        this.unidadesOperativasCatastroItemList = unidadesOperativasCatastroItemList;
    }

    /** ---------------------------------- */
    /** ------------ MÉTODOS ------------- */
    /** ---------------------------------- */
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @author david.cifuentes
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {

        EstructuraOrganizacional estructuraOrganizacional, answer = null;
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritoriales()) {
            if (eo.getCodigo().equals(codigoTerritorial)) {
                answer = eo;
                break;
            }
        }
        if (answer != null) {
            estructuraOrganizacional = this
                .getGeneralesService()
                .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
            if (estructuraOrganizacional != null) {
                return estructuraOrganizacional;
            } else {
                return answer;
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el llamado a los métodos de cargue de las UOCs y los departamentos, y
     * realiza el set de la UOC seleccionada.
     *
     * @author david.cifuentes
     * @return
     */
    public void cargarUOC() {

        this.unidadesOperativasCatastroItemList.clear();
        if (this.territorialSeleccionadaCodigo != null) {

            if (!this.usuarioDelegado && !this.usuarioHabilitado) {
                this.cargarListaUOC();
                this.cargarUOCSeleccionada();
            }

            // Cargue de plantilla
            this.cargarPlantillaParametrizada();

            // Generación del reporte temporal con la nueva sección
            //this.generarPlantillaTemporal();
        } else {
            if (this.territorialSeleccionadaCodigo == null) {
                this.addMensajeError(ECodigoErrorVista.PARAMS_DOCUMENTOS_001.toString());
                LOGGER.error(ECodigoErrorVista.PARAMS_DOCUMENTOS_001.getMensajeTecnico());
                this.addErrorCallback();
                this.plantillaHabilitada = false;
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el set por default de la UOC del usuario.
     *
     * @author david.cifuentes
     */
    public void cargarUOCSeleccionada() {
        if (this.usuario.isUoc()) {
            this.unidadOperativaSeleccionadaCodigo = this.usuario
                .getCodigoUOC();
        } else {
            this.unidadOperativaSeleccionadaCodigo = null;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Mètodo que realiza el cargue de unidades operativas de catastro cuando se ha seleccionado una
     * territorial
     *
     * @author david.cifuentes
     */
    public void cargarListaUOC() {

        EstructuraOrganizacional territorial = this
            .buscarTerritorial(this.territorialSeleccionadaCodigo);

        this.unidadesOperativasCatastroItemList = new ArrayList<SelectItem>();
        this.unidadOperativaSeleccionadaCodigo = null;

        if (!territorial.isDireccionGeneral()) {
            List<EstructuraOrganizacional> unidadesOperativasCatastro = this
                .getGeneralesService()
                .getCacheEstructuraOrganizacionalPorTipoYPadre(
                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                    this.territorialSeleccionadaCodigo);

            for (EstructuraOrganizacional eo : unidadesOperativasCatastro) {
                this.unidadesOperativasCatastroItemList.add(new SelectItem(eo
                    .getCodigo(), eo.getNombre()));
            }
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza la consulta de la plantilla parametrizada para una territorial o UOC.
     *
     * @author david.cifuentes
     */
    public void cargarPlantillaParametrizada() {

        try {

            this.archivoCabecera = null;
            this.archivoMarcaDeAgua = null;
            this.archivoPieDePagina = null;

            if (this.territorialSeleccionadaCodigo == null) {
                this.addMensajeError(ECodigoErrorVista.PARAMS_DOCUMENTOS_001.toString());
                LOGGER.error(ECodigoErrorVista.PARAMS_DOCUMENTOS_001.getMensajeTecnico());
                this.addErrorCallback();
                this.plantillaHabilitada = false;
                return;
            }

            this.plantillaHabilitada = true;

            String estructuraOrganizacionalCodigo = null;

            if (this.unidadOperativaSeleccionadaCodigo != null &&
                !this.unidadOperativaSeleccionadaCodigo.isEmpty()) {
                estructuraOrganizacionalCodigo = this.unidadOperativaSeleccionadaCodigo;

                this.usuario
                    .setCodigoTerritorial(this.territorialSeleccionadaCodigo);
                this.usuario
                    .setCodigoUOC(this.unidadOperativaSeleccionadaCodigo);
            } else {
                estructuraOrganizacionalCodigo = this.usuario.getCodigoTerritorial();
            }

            // Consulta de plantilla
            PlantillaSeccionReporte plantillaVigente = this
                .getGeneralesService()
                .buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(
                    estructuraOrganizacionalCodigo);

            if (this.validarSeccionesPlantilla() && plantillaVigente != null) {

                this.fechaPlantilla = this.plantilla.getFechaEnFirme();

                // Generación del reporte con la plantilla en firme
                EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.PLANTILLA;
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("URL_CABECERA", plantillaVigente.getUrlCabecera());
                parameters.put("URL_MARCA_DE_AGUA", plantillaVigente.getUrlMarcaDeAgua());
                parameters.put("URL_PIE_DE_PAGINA", plantillaVigente.getUrlPieDePagina());

                // Generación de reporte en firme
                this.reportePlantilla = reportsService.generarReporte(
                    parameters, enumeracionReporte, this.usuario);

                this.reporteTemporal = reportsService.generarReporte(
                    parameters, enumeracionReporte, this.usuario);

                if (this.reportePlantilla != null) {
                    this.plantillaGenericaBool = false;
                } else {
                    this.plantillaGenericaBool = true;
                }
            } else {

                this.plantillaGenericaBool = true;
                this.fechaPlantilla = null;
                this.reportePlantilla = null;
                this.urlWebCabecera = null;
                this.urlWebPieDePagina = null;
                this.urlWebMarcaDeAgua = null;
                this.plantilla = null;

                // Generación del reporte temporal para edición
                this.generarPlantillaTemporal();
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza la validación de las URLs de las imágenes asociadas a la plantilla
     * seleccionada
     *
     * @author david.cifuentes
     */
    public boolean validarSeccionesPlantilla() throws Exception {

        boolean actualizarPlantillaBool = false;

        if (this.plantilla != null) {
            // Validar las urls de las secciones
            if (this.plantilla.getUrlCabecera() != null) {

                // Se realizó la publicación de la url pero no funciona
                // adecuadamente y se debe publicar en web nuevamente
                if (!this.validarUrl(this.plantilla.getUrlCabecera())) {

                    if (this.plantilla.getRutaCabecera() != null) {
                        // Descarga del FTP
                        String rutaTempLocal = this.getGeneralesService()
                            .descargarArchivoDeAlfrescoATemp(
                                this.plantilla.getRutaCabecera());

                        if (rutaTempLocal == null || rutaTempLocal.isEmpty()) {
                            LOGGER.error(
                                "Ocurrió un errror al descargar el documento de alfresco: " +
                                "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                            throw (new Exception(
                                "Ocurrió un errror al descargar el documento de alfresco: " +
                                "descargarArchivoDeAlfrescoATemp retorno null o vacio"));
                        }

                        // Publicación en web
                        String urlTempWeb = this.getGeneralesService()
                            .publicarArchivoEnWeb(rutaTempLocal);

                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        actualizarPlantillaBool = true;
                    } else {
                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        actualizarPlantillaBool = true;
                    }
                }
            }

            if (this.plantilla.getUrlMarcaDeAgua() != null) {

                // Se realizó la publicación de la url pero no funciona
                // adecuadamente y se debe publicar en web nuevamente
                if (!this.validarUrl(this.plantilla.getUrlMarcaDeAgua())) {

                    if (this.plantilla.getRutaMarcaDeAgua() != null) {
                        // Descarga del FTP
                        String rutaTempLocal = this.getGeneralesService()
                            .descargarArchivoDeAlfrescoATemp(
                                this.plantilla.getRutaMarcaDeAgua());

                        if (rutaTempLocal == null || rutaTempLocal.isEmpty()) {
                            LOGGER.error(
                                "Ocurrió un errror al descargar el documento de alfresco: " +
                                "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                            throw (new Exception(
                                "Ocurrió un errror al descargar el documento de alfresco: " +
                                "descargarArchivoDeAlfrescoATemp retorno null o vacio"));
                        }

                        // Publicación en web
                        String urlTempWeb = this.getGeneralesService()
                            .publicarArchivoEnWeb(rutaTempLocal);

                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        actualizarPlantillaBool = true;
                    } else {
                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        actualizarPlantillaBool = true;
                    }
                }
            }

            if (this.plantilla.getUrlPieDePagina() != null) {

                // Se realizó la publicación de la url pero no funciona
                // adecuadamente y se debe publicar en web nuevamente
                if (!this.validarUrl(this.plantilla.getUrlPieDePagina())) {

                    if (this.plantilla.getRutaPieDePagina() != null) {
                        // Descarga del FTP
                        String rutaTempLocal = this.getGeneralesService()
                            .descargarArchivoDeAlfrescoATemp(
                                this.plantilla.getRutaPieDePagina());

                        if (rutaTempLocal == null || rutaTempLocal.isEmpty()) {
                            LOGGER.error(
                                "Ocurrió un errror al descargar el documento de alfresco: " +
                                "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                            throw (new Exception(
                                "Ocurrió un errror al descargar el documento de alfresco: " +
                                "descargarArchivoDeAlfrescoATemp retorno null o vacio"));
                        }

                        // Publicación en web
                        String urlTempWeb = this.getGeneralesService()
                            .publicarArchivoEnWeb(rutaTempLocal);

                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        actualizarPlantillaBool = true;
                    } else {
                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        actualizarPlantillaBool = true;
                    }
                }
            }

            // Actualización de plantilla
            if (actualizarPlantillaBool) {
                this.plantilla = this.getGeneralesService()
                    .guardarPlantillaSeccionReporte(this.plantilla);
            }
            return true;
        }
        return false;
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza el cargue de la plantilla generica
     *
     * @author david.cifuentes
     */
    public void cargarPlantillaGenerica() {

        try {
            EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.PLANTILLA;
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("URL_CABECERA", null);
            parameters.put("URL_MARCA_DE_AGUA", null);
            parameters.put("URL_PIE_DE_PAGINA", null);
            this.reportePlantilla = reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);

            if (this.reportePlantilla != null) {
                this.plantillaGenericaBool = true;
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza el cargue de la plantilla vigente para la entidad autenticada
     *
     * @author felipe.cadena
     */
    public void cargarPlantillaVigente() {

        try {
            EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.PLANTILLA;
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("URL_CABECERA", this.plantilla.getUrlCabecera());
            parameters.put("URL_MARCA_DE_AGUA", this.plantilla.getUrlMarcaDeAgua());
            parameters.put("URL_PIE_DE_PAGINA", this.plantilla.getUrlPieDePagina());
            this.reportePlantilla = reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);

            if (this.reportePlantilla != null) {
                this.plantillaGenericaBool = true;
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza la carga de una sección del reporte
     *
     * @param eventoCarga
     */
    public void cargarSeccion(FileUploadEvent eventoCarga) {

        this.banderaPonerEnFirme = false;
        if (this.territorialSeleccionadaCodigo == null) {
            this.addMensajeError(ECodigoErrorVista.PARAMS_DOCUMENTOS_001.toString());
            LOGGER.error(ECodigoErrorVista.PARAMS_DOCUMENTOS_001.getMensajeTecnico());
            this.addErrorCallback();
            return;
        }
        if (this.seccionSeleccionada == null || this.seccionSeleccionada.isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.PARAMS_DOCUMENTOS_002.toString());
            LOGGER.error(ECodigoErrorVista.PARAMS_DOCUMENTOS_002.getMensajeTecnico());
            this.addErrorCallback();
            return;
        }

        try {
            File archivoTemporal = new File(FileUtils.getTempDirectory()
                .getAbsolutePath(), eventoCarga.getFile().getFileName());

            FileOutputStream fileOutputStream = new FileOutputStream(
                archivoTemporal);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            // Publicación en web de la sección cargada
            if (archivoTemporal != null && archivoTemporal.exists()) {
                this.reporteTemporal = this.reportsService
                    .generarReporteDesdeUnFile(archivoTemporal);
            }

            if (this.reporteTemporal != null) {

                this.seccionNombre = null;
                if (this.plantilla != null) {
                    if (this.urlWebCabecera == null) {
                        this.urlWebCabecera = this.plantilla.getUrlCabecera();
                    }
                    if (this.urlWebMarcaDeAgua == null) {
                        this.urlWebMarcaDeAgua = this.plantilla.getUrlMarcaDeAgua();
                    }
                    if (this.urlWebPieDePagina == null) {
                        this.urlWebPieDePagina = this.plantilla.getUrlPieDePagina();
                    }
                } else {
                    this.plantilla = new PlantillaSeccionReporte();
                }

                // Cargue de la url en web de la sección
                if ("HEADER".equals(this.seccionSeleccionada)) {
                    this.urlWebCabecera = this.reporteTemporal
                        .getUrlWebReporte();
                    this.seccionNombre = "Encabezado";
                    this.archivoCabecera = archivoTemporal;

                } else if (WATER_MARK.equals(this.seccionSeleccionada)) {
                    this.urlWebMarcaDeAgua = this.reporteTemporal
                        .getUrlWebReporte();
                    this.seccionNombre = "Marca de agua";
                    this.archivoMarcaDeAgua = archivoTemporal;

                } else if ("FOOTER".equals(this.seccionSeleccionada)) {
                    this.urlWebPieDePagina = this.reporteTemporal
                        .getUrlWebReporte();
                    this.seccionNombre = "Pie de página";
                    this.archivoPieDePagina = archivoTemporal;
                }

                if ((this.plantilla.getUrlCabecera() == null || this.plantilla.getUrlCabecera().
                    isEmpty()) &&
                    (this.urlWebCabecera == null || urlWebCabecera.isEmpty())) {
                    if ((this.urlWebMarcaDeAgua == null || this.urlWebMarcaDeAgua.isEmpty()) &&
                        (this.urlWebPieDePagina == null || this.urlWebPieDePagina.isEmpty())) {
                        this.banderaPonerEnFirme = true;
                    }else {
                        this.banderaPonerEnFirme = true;
                    }
                } else {
                    this.banderaPonerEnFirme = true;
                }

                // Generación del reporte temporal con la nueva sección
                this.generarPlantillaTemporal();

                this.addMensajeInfo("La sección " + this.seccionNombre +
                    " se cargó correctamente.");
            } else {
                this.addMensajeError("La sección no pudo ser cargada.");
            }

        } catch (IOException e) {
            e.printStackTrace();
            this.addMensajeError("La sección no pudo ser cargada.");
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que reemplaza los parámetros de la sección y realiza el llamado al método que genera
     * el reporte vacio para la previsualización de la plantilla.
     *
     * @author david.cifuentes
     */
    public void generarPlantillaTemporal() {
        try {

            EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.PLANTILLA;
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("URL_CABECERA", this.urlWebCabecera);
            parameters.put("URL_MARCA_DE_AGUA", this.urlWebMarcaDeAgua);
            parameters.put("URL_PIE_DE_PAGINA", this.urlWebPieDePagina);

            if (this.unidadOperativaSeleccionadaCodigo != null) {
                this.usuario
                    .setCodigoTerritorial(this.territorialSeleccionadaCodigo);
                this.usuario
                    .setCodigoUOC(this.unidadOperativaSeleccionadaCodigo);
            } else {
                this.usuario
                    .setCodigoTerritorial(this.territorialSeleccionadaCodigo);
            }

            this.reporteTemporal = reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que elimina el valor de las urls de las imágenes de las secciones.
     *
     * @author david.cifuentes
     */
    public void eliminarSeccion() {

        this.seccionNombre = null;
        this.banderaPonerEnFirme = false;

        if (this.seccionSeleccionada == null || this.seccionSeleccionada.isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.PARAMS_DOCUMENTOS_002.toString());
            LOGGER.error(ECodigoErrorVista.PARAMS_DOCUMENTOS_002.getMensajeTecnico());
            this.addErrorCallback();
            return;
        }

        if ("HEADER".equals(this.seccionSeleccionada)) {
            this.urlWebCabecera = null;
            this.seccionNombre = "Encabezado";
        } else if (WATER_MARK.equals(this.seccionSeleccionada)) {
            this.urlWebMarcaDeAgua = null;
            this.seccionNombre = "Marca de agua";
        } else if ("FOOTER".equals(this.seccionSeleccionada)) {
            this.urlWebPieDePagina = null;
            this.seccionNombre = "Pie de página";
        }

        if (this.urlWebCabecera == null ||
            this.urlWebCabecera.isEmpty()) {
            if ((this.urlWebMarcaDeAgua == null || this.urlWebMarcaDeAgua.isEmpty()) &&
                (this.urlWebPieDePagina == null || this.urlWebPieDePagina.isEmpty())) {
                this.banderaPonerEnFirme = true;
            }
        } else {
            this.banderaPonerEnFirme = true;
        }

        // Generación de plantilla temporal para su previsualización
        this.generarPlantillaTemporal();

        this.addMensajeInfo("Se realizó la eliminación de la sección " + this.seccionNombre +
            " satisfactoriamente.");
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza la actualización en base de datos de una plantilla
     *
     * @author david.cifuentes
     */
    /*
     * @modificado por leidy.gonzalez::#23686::23/02/2017 Se elimina almacenamiento de la URL en la
     * tabla PLANTILLA SECCION REPORTE
     */
    public void ponerEnFirme() {

        String rutaCabecera, rutaMarcaDeAgua, rutaPieDePagina;

        try {
            String estructuraOrganizacionalCod = null;
            if (this.plantilla == null || this.plantilla.getId() == null) {
                this.plantilla = new PlantillaSeccionReporte();
                if (this.urlWebCabecera != null && !this.urlWebCabecera.isEmpty()) {
                    //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                    rutaCabecera = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebCabecera,
                        this.archivoCabecera);
                    this.plantilla.setRutaCabecera(rutaCabecera);
                    this.plantilla.setUrlCabecera(this.urlWebCabecera);
                }
                if (this.urlWebMarcaDeAgua != null && !this.urlWebMarcaDeAgua.isEmpty()) {
                    //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                    rutaMarcaDeAgua = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebMarcaDeAgua,
                        this.archivoMarcaDeAgua);
                    this.plantilla.setRutaMarcaDeAgua(rutaMarcaDeAgua);
                    this.plantilla.setUrlMarcaDeAgua(this.urlWebMarcaDeAgua);
                }
                if (this.urlWebPieDePagina != null && !this.urlWebPieDePagina.isEmpty()) {
                    //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                    rutaPieDePagina = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebPieDePagina,
                        this.archivoPieDePagina);
                    this.plantilla.setRutaPieDePagina(rutaPieDePagina);
                    this.plantilla.setUrlPieDePagina(this.urlWebPieDePagina);
                }
            } else {
                // Verificar las secciones que se actualizaron

                // Cabecera
                if (this.plantilla.getUrlCabecera() != null) {
                    if (this.urlWebCabecera != null && !this.urlWebCabecera.equals(this.plantilla.
                        getUrlCabecera())) {

                        // Reemplazar la nueva sección y eliminar del FTP la antigua
                        this.getGeneralesService().borrarDocumentoEnAlfresco(this.plantilla.
                            getRutaCabecera());
                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        rutaCabecera = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebCabecera,
                            this.archivoCabecera);
                        this.plantilla.setRutaCabecera(rutaCabecera);
                        this.plantilla.setUrlCabecera(this.urlWebCabecera);
                    }
                } else if (this.urlWebCabecera != null) {
                    //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                    rutaCabecera = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebCabecera,
                        this.archivoCabecera);
                    this.plantilla.setRutaCabecera(rutaCabecera);
                    this.plantilla.setUrlCabecera(this.urlWebCabecera);
                }

                // Marca de agua
                if (this.plantilla.getUrlMarcaDeAgua() != null) {
                    if (this.urlWebMarcaDeAgua != null && !this.urlWebMarcaDeAgua.equals(
                        this.plantilla.getUrlMarcaDeAgua())) {
                        // Reemplazar la nueva sección y eliminar del FTP la antigua
                        this.getGeneralesService().borrarDocumentoEnAlfresco(this.plantilla.
                            getRutaMarcaDeAgua());
                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        rutaMarcaDeAgua = this.guardarArchivoFTPDesdeArchivoWeb(
                            this.urlWebMarcaDeAgua, this.archivoMarcaDeAgua);
                        this.plantilla.setRutaMarcaDeAgua(rutaMarcaDeAgua);
                        this.plantilla.setUrlMarcaDeAgua(this.urlWebMarcaDeAgua);
                    }

                } else if (this.urlWebMarcaDeAgua != null) {
                    //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                    rutaMarcaDeAgua = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebMarcaDeAgua,
                        this.archivoMarcaDeAgua);
                    this.plantilla.setRutaMarcaDeAgua(rutaMarcaDeAgua);
                    this.plantilla.setUrlMarcaDeAgua(this.urlWebMarcaDeAgua);
                }

                // Pie de pagina
                if (this.plantilla.getUrlPieDePagina() != null) {
                    if (this.urlWebPieDePagina != null && !this.urlWebPieDePagina.equals(
                        this.plantilla.getUrlPieDePagina())) {
                        // Reemplazar la nueva sección y eliminar del FTP la antigua
                        this.getGeneralesService().borrarDocumentoEnAlfresco(this.plantilla.
                            getRutaPieDePagina());
                        //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                        rutaPieDePagina = this.guardarArchivoFTPDesdeArchivoWeb(
                            this.urlWebPieDePagina, this.archivoPieDePagina);
                        this.plantilla.setRutaPieDePagina(rutaPieDePagina);
                        this.plantilla.setUrlPieDePagina(this.urlWebPieDePagina);
                    }
                } else if (this.urlWebPieDePagina != null) {
                    //Se elimina almacenamiento de URL en la Entidad: PLANTILLA SECCION REPORTE
                    rutaPieDePagina = this.guardarArchivoFTPDesdeArchivoWeb(this.urlWebPieDePagina,
                        this.archivoPieDePagina);
                    this.plantilla.setRutaPieDePagina(rutaPieDePagina);
                    this.plantilla.setUrlPieDePagina(this.urlWebPieDePagina);
                }
            }

            if (this.unidadOperativaSeleccionadaCodigo != null ||
                this.territorialSeleccionadaCodigo != null) {

                if (this.unidadOperativaSeleccionadaCodigo != null) {
                    estructuraOrganizacionalCod = this.unidadOperativaSeleccionadaCodigo;
                } else {
                    estructuraOrganizacionalCod = this.territorialSeleccionadaCodigo;
                }

                // Actualización de campos
                this.plantilla
                    .setEstructuraOrganizacionalCod(estructuraOrganizacionalCod);
                this.plantilla
                    .setFechaEnFirme(new Date(System.currentTimeMillis()));
                this.plantilla.setFechaLog(new Date(System.currentTimeMillis()));
                this.plantilla.setUsuarioLog(this.usuario.getLogin());

                // Consulta de plantilla
                this.plantilla = this.getGeneralesService()
                    .guardarPlantillaSeccionReporte(this.plantilla);

                this.addMensajeInfo("Se puso en firme la plantilla de manera satisfactoria.");

                this.banderaPonerEnFirme = false;
                this.generarPlantillaTemporal();

            } else {
                this.addMensajeError(
                    "Se debe seleccionar una territorial o UOC para poner en firme la plantilla.");
            }

        } catch (Exception e) {
            this.addMensajeError(
                "Ocurrió un error al poner en firme la plantilla, por favor intente más tarde.");
            LOGGER.error(e.getMessage());
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza la carga en el FTP de un archivo publicado cargado desde web
     *
     * @author david.cifuentes
     * @param urlArchivoWeb
     * @param archivo
     * @return
     */
    public String guardarArchivoFTPDesdeArchivoWeb(String urlArchivoWeb,
        File archivo) {

        String rutaFTP = null;

        if (urlArchivoWeb != null && archivo != null) {

            String rutaTemporal = null;
            try {

                URL url = new URL(urlArchivoWeb);
                URLConnection urlCon = url.openConnection();
                InputStream is = urlCon.getInputStream();
                FileOutputStream fos = new FileOutputStream(
                    UtilidadesWeb.obtenerRutaTemporalArchivos() +
                    SEPARADOR_CARPETA + archivo.getName());

                byte[] array = new byte[1000];
                int leido = is.read(array);
                while (leido > 0) {
                    fos.write(array, 0, leido);
                    leido = is.read(array);
                }
                is.close();
                fos.close();
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }

            String estructuraOrganizacionalCod = null;
            if (this.unidadOperativaSeleccionadaCodigo != null ||
                this.territorialSeleccionadaCodigo != null) {

                if (this.unidadOperativaSeleccionadaCodigo != null) {
                    estructuraOrganizacionalCod = this.unidadOperativaSeleccionadaCodigo;
                } else {
                    estructuraOrganizacionalCod = this.territorialSeleccionadaCodigo;
                }

                try {
                    IDocumentosService servicioDocumental;
                    DocumentoVO documentoVo;

                    // D: se carga el archivo en la carpeta temporal del sistema
                    rutaTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos() +
                        SEPARADOR_CARPETA + archivo.getName();
                    servicioDocumental = DocumentalServiceFactory.getService();
                    documentoVo = servicioDocumental
                        .cargarDocumentoWorkspaceEstructuraOrganizacional(
                            estructuraOrganizacionalCod, rutaTemporal);
                    if (documentoVo != null) {
                        rutaFTP = documentoVo.getIdSistemaDocumental();
                    }
                } catch (ExcepcionSNC e) {
                    LOGGER.error("Error en guardarArchivoFTPDesdeArchivoWeb: " + e.getMessage());
                    if (SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.
                        getCodigoDeExcepcion().equals(e.getCodigoDeExcepcion())) {
                        this.addMensajeError(
                            "Ocurrió un error almacenando el documento, por favor intente más tarde.");
                    } else {
                        this.addMensajeError(
                            "Ocurrió un error guardando la plantilla, por favor intente más tarde.");
                    }
                } catch (Exception e) {
                    e.getMessage();
                    String mensaje = "Ocurrió un error, por favor intente más tarde.";
                    this.addMensajeError(mensaje);
                }
            }

        }
        return rutaFTP;
    }

    // -------------------------------------------------------------- //
    /**
     * Método que realiza la validación de que una URL responde y no es un link roto
     *
     * @author david.cifuentes
     * @param url
     * @return
     */
    public boolean validarUrl(String url) {

        try {
            URL u = new URL(url);
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();

            if (huc.getResponseCode() == 404) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("ERROR - Url no válida: " + url);
            return false;
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método usado para verificar el envío de la sección al momento de cambiar en el menú de
     * secciones.
     */
    public void cambioNombreSeccion() {
        LOGGER.error(this.seccionSeleccionada);

        if ("HEADER".equals(this.seccionSeleccionada)) {
            this.seccionNombre = "Encabezado";
        } else if (WATER_MARK.equals(this.seccionSeleccionada)) {
            this.seccionNombre = "Marca de agua";
        } else if ("FOOTER".equals(this.seccionSeleccionada)) {
            this.seccionNombre = "Pie de página";
        }
    }

    // -------------------------------------------------------------- //
    /**
     * Método cerrar que forza realiza un llamado al método que remueve los managed bean de la
     * sesión y forza el init de tareasPendientesMB.
     *
     * @return
     */
    public String cerrar() {
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    // Fin de la clase
}
