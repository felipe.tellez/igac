package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IParametroDAO;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import java.util.List;

@Stateless
public class ParametroDAOBean extends GenericDAOWithJPA<Parametro, Long> implements IParametroDAO {

    public Parametro getParametroPorNombre(String nombre) {
        Query query = this.entityManager.createQuery(
            "Select p from Parametro p where p.nombre = :nombre");
        query.setParameter("nombre", nombre);
        return (Parametro) query.getSingleResult();
    }

    /**
     * @see IParametroDAO#getParametroPorPrefijo(java.lang.String)
     */
    @Override
    public List<Parametro> getParametroPorPrefijo(String prefijo) {
        String sql = "select param from Parametro param where param.nombre like :prefijo";
        Query query = this.entityManager.createQuery(sql);
        query.setParameter("prefijo", prefijo + "%");
        return (List<Parametro>) query.getResultList();
    }

}
