package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IProductoFormatoDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoFormato;

/**
 * Implementación de los servicios de persistencia del objeto ProductoFormato.
 *
 * @author javier.barajas
 */
@Stateless
public class ProductoFormatoDAOBean extends GenericDAOWithJPA<ProductoFormato, Long> implements
    IProductoFormatoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProductoFormatoDAOBean.class);

    /**
     * Método que retorna una lista de {ProductoFormato} buscandolos por id del producto.
     *
     * @author leidy.gonzalez
     * @param productoId
     * @return
     */
    public List<ProductoFormato> findByIdProducto(Long productoId) {
        try {
            String queryToExecute = "SELECT pf FROM ProductoFormato pf" +
                " WHERE pf.producto.id = :productoId";
            Query query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("productoId", productoId);

            return query.getResultList();
        } catch (Exception e) {
            LOGGER.debug("DominioDAOBean#findByCodigo -> Error: " + e.getMessage());
            return null;
        }

    }

}
