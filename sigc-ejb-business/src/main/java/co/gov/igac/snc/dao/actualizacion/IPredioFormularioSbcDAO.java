package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.PredioFormularioSbc;

/**
 * Servicios de persistencia para el objeto PredioFormularioSbc
 *
 * @author javier.barajas
 */
@Local
public interface IPredioFormularioSbcDAO extends IGenericJpaDAO<PredioFormularioSbc, Long> {

}
