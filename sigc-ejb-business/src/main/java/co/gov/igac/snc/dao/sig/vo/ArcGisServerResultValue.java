/**
 *
 */
package co.gov.igac.snc.dao.sig.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author juan.mendez
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArcGisServerResultValue implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8585090961078423136L;

    private String paramName;
    private String value;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
