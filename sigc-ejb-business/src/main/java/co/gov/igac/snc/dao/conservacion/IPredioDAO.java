/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.ListDetallesVisorVO;

@Local
public interface IPredioDAO extends IGenericJpaDAO<Predio, Long> {

    /**
     * @author fabio.navarrete Retorna un predio por el número predial especificado
     * @param numPredial número predial del predio a obtener
     * @return
     */
    public Predio getPredioByNumeroPredial(String numPredial);

    /**
     * Metodo para obtener predios asociados a una raiz de un numero predial
     *
     * @author felipe.cadena
     * @param raizNumPredial
     * @return
     */
    public List<Predio> obtenerPorRaizNumeroPredial(String raizNumPredial);

    /**
     * @author fabio.navarrete Retorna un predio por el número predial especificado y además obtiene
     * los datos de la tabla persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se
     * requiera inicializar los datos personPredio asociados al predio.
     * @param numPredial número predial del predio a obtener
     * @return
     */
    public Predio getPredioFetchPersonasByNumeroPredial(String numPredial);

    /**
     * Retorna un predio por el id de un predio especificado y además obtiene los datos de la tabla
     * persona_predio asociados a éste. </br>
     * Sólo debe usarse cuando REALMENTE se requiera inicializar los datos personPredio asociados al
     * predio.
     *
     * @author juan.agudelo
     * @param predioId
     * @return
     */
    public Predio getPredioFetchPersonasById(Long predioId);

    /**
     * Retorna la lista de Predios que cumplen con los criterios de búsqueda que vienen dados en el
     * parámetro
     *
     * @author pedro.garcia
     * @param datosConsultaPredio Objeto cuyos valores son los que se van a usar en la búsqueda de
     * Predios
     * @param from índice del primer resultado (de la lista de resultados) que se va a devolver.
     * @param howMany máximo número de registros que se van a devolver. Si = -1 => se devuelven
     * todos
     * @return Lista de objetos. Cada uno es el nombre del departamento, del municipio, y el predio
     * como tal
     */
    public List<Object[]> searchForPredios(FiltroDatosConsultaPredio datosConsultaPredio, int from,
        int howMany);

    /**
     * Retorna la lista de Predios que cumplen con los criterios de búsqueda que vienen dados en el
     * parámetro
     *
     * @author pedro.garcia
     * @param datosConsultaPredio Objeto cuyos valores son los que se van a usar en la búsqueda de
     * Predios
     * @param from índice del primer resultado (de la lista de resultados) que se va a devolver.
     * @param howMany máximo número de registros que se van a devolver. Si = -1 => se devuelven
     * todos
     * @return Lista de objetos. Cada uno es el nombre del departamento, del municipio, y el predio
     * como tal
     */
//	public List<Predio> searchForPredios2(FiltroDatosConsultaPredio datosConsultaPredio, int from, int howMany);
    /**
     * Retorna un predio por el número predial especificado y además obtiene los datos de la tabla
     * predio_avaluo_catastral asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos predioAvaluoCatastral asociados al predio.
     *
     * @param numPredial número predial del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchAvaluosByNumeroPredial(String numPredial);

    /**
     * @author fabio.navarrete Retorna un predio por su número predial y además obtiene los datos de
     * la tabla persona_predio_propiedad asociados.
     * @param numPredial número predial del predio a obtener.
     * @return
     */
    public Predio getPredioFetchDerechosPropiedadByNumeroPredial(String numPredial);

    //TODO:Documentar Método -> Recordar adjuntar autor
    public Predio getPredioById(Long id);

    /**
     * @author fabio.navarrete Retorna un predio con los datos de ubicación asociados según el
     * número predial ingresado
     * @param numPredial número predial del predio a obtener
     * @return Predio requerido con las listas inicializadas
     */
    public Predio getPredioFetchDatosUbicacionByNumeroPredial(String numPredial);

    /**
     * @author juan.agudelo Retorna un predio por el número predial especificado y además obtiene
     * los datos de la tabla departamento, municipio y persona_predio asociados a éste.
     * @param numPredial número predial del predio a obtener
     * @return Predio requerido con las listas inicializadas
     */
    public Predio getPredioDatosFetchPersonasByNumeroPredial(String numPredial);

    /**
     * Retorna el número de Predios que cumplen con los criterios de búsqueda que vienen dados en el
     * parámetro
     *
     * @author pedro.garcia
     * @param datosConsultaPredio Objeto cuyos valores son los que se van a usar en la búsqueda de
     * Predios
     * @return
     */
    public int searchForPrediosCount(FiltroDatosConsultaPredio datosConsultaPredio);

    /**
     * Retorna un predio por el identificador del predio especificado y además obtiene los datos de
     * la tabla predio_avaluo_catastral asociados a éste. Sólo debe usarse cuando REALMENTE se
     * requiera inicializar los datos predioAvaluoCatastral asociados al predio.
     *
     * @param predioId identificador del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchAvaluosByPredioId(Long predioId);

    /**
     * Retorna un predio por el identificador del predio especificado y además obtiene los datos de
     * la tabla persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos personaPredio asociados al predio.
     *
     * @param predioId
     * @return
     */
    public Predio findPredioFetchPersonasByPredioId(Long predioId);

    /**
     * Busca un predio dado un número predial. OJO: por cuestiones de migración de BD puede ser que
     * dos predios tengan el mismo número predial, pero solo debería ser uno, así que se hace la
     * consulta como si trajera una lista, pero solo se tiene en cuenta el primero
     *
     * @author pedro.garcia
     * @param numeroPredial
     * @return
     */
    public Predio findByNumeroPredial(String numeroPredial);

    /**
     * Metodo que recupera un predio a partir del numero predial busqueda no nativa
     *
     * @param numeroPredial
     * @return
     */
    public Predio getPredioPorNumeroPredial(String numeroPredial);

    /**
     * Valida si un predio existe teniendo en cuenta su número predial
     *
     * @author juan.mendez
     * @param numeroPredial
     * @return
     */
    public boolean existePredioPorNumeroPredial(String numeroPredial);

    /**
     * Metodo que retorna la lista de predios cuyo numero predial comienza por cierto codigo
     *
     * @param codigo
     * @author franz.gamba
     */
    public List<Predio> getPrediosPorCodigo(String codigo);

    /**
     * Metodo que retorna la lista de predios correspondientes a una matricula inmobiliaria de un
     * municipio
     *
     * @param municipioCod
     * @param matriculaInm
     * @author franz.gamba
     */
    public List<Predio> getPrediosPorMunicipioYMatricula(String municipioCod, String matriculaInm);

    /**
     * Metodo que retorna la lista de predios correspondientes a una direccion de un municipio
     *
     * @param municipioCod
     * @param matriculaInm
     * @author franz.gamba
     */
    public List<Predio> getPrediosPorMunicipioYDireccion(String municipioCod, String direccion);

    /**
     * @author fabio.navarrete
     * @return
     */
    public Set<Predio> findAllReturnSet();

    /**
     * Metodo que retorna la lista de predios cuyos numeros prediales se encuentran en la seleccion
     * del visor
     *
     * @param numPrediales
     * @author franz.gamba
     */
    public List<Predio> getPrediosPorNumerosPrediales(String numPrediales);

    /**
     * Metodo que retorna la lista de predios cuyos numeros prediales se encuentran en la seleccion
     * del visor y a su vez una lista con los numeros prediale sque nos e encontraron
     *
     * @param numPrediales
     * @author franz.gamba
     */
    public ListDetallesVisorVO getPrediosPorNumerosPredialesVisor(String numPrediales);

    /**
     * Metodo que recibe una direccion y retorna un {@link String} con la dirección normalizada
     * conforme a las covenciones.
     *
     * @author david.cifuentes
     * @param direccion
     * @return direccion normalizada
     */
    public String normalizarDireccion(String direccion);

    /**
     * Metodo que retorna un arreglo de tipo double con las áreas totales de terreno y
     * deconstrucción catastrales en la posición 0 está el área total de terreno y en la posicion 1
     * está el área total construida
     *
     * @author javier.aponte
     * @param numeroPredial
     * @return
     */
    public double[] getAreasCatastralesPorNumeroPredial(String numeroPredial);

    /**
     * Método que busca predios por FiltroDatosConsultaPrediosBloqueo
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPredioBloqueo
     * @return
     */
    public List<Predio> buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo);

    /**
     * Método encargado de traer la lista de pisos según el edificio dado. El método retorna datos
     * si el predio de dicho terreno tiene condición de propiedad Propiedad Horizontal PH
     *
     * @param numeroPredial
     * @return
     * @author fabio.navarrete
     */
    public List<String> findPisosEdificio(String numeroPredial);

    /**
     * Método encargado de traer la lista de terrenos(Predios) según la manzana o vereda ingresada.
     *
     * @param numeroPredial
     * @return
     * @author fabio.navarrete
     * @modified pedro.garcia 26-06-2012 adición de parámetro que define la condición de propiedad
     * que restringe la búsqueda de números de terreno
     */
    public List<String> findTerrenosManzana(String numeroPredial);

    /**
     * Método encargado de traer la lista de edificios según el terreno (Predio) dado. El método
     * retorna datos si el predio de dicho terreno tiene condición de propiedad Propiedad Horizontal
     * PH
     *
     * @param numeroPredial
     * @return
     * @author fabio.navarrete
     */
    public List<String> findEdificiosTerreno(String numeroPredial);

    /**
     * Método que busca predios en un rango por FiltroDatosConsultaPrediosBloqueo inicial y final
     *
     * @author juan.agudelo
     * @param filtroCPredioB
     * @param filtroFinalCPredioB
     * @return
     */
    public List<Predio> buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB,
        FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB);

    /**
     * Método que realiza un conteo de la cantidad de predios nacionales.
     *
     * @author david.cifuentes
     * @return
     */
    public Long contarPrediosNacionales();

    /**
     * Busca un predio dado su id haciendo fetch de los datos relacionados con la ubicación del
     * predio (pestaña "ubicación" en detalles del predio)
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId id del predio buscado
     * @return
     */
    public Predio getPredioFetchDatosUbicacionById(Long predioId);

    /**
     *
     * Metodo encargado de buscar un predio que no se encuentre en tramite alguno, para utilizarlo
     * en la siguiente prueba
     *
     * @author fredy.wilches
     * @version 2.0
     * @param codigoEstructuraOrganizacional
     * @return
     */
    public Predio buscarPredioLibreTramite(Integer codigoEstructuraOrganizacional,
        String codigoMunicipio);

    /**
     * Método que recupera una lista de predios búscando por números prediales
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numerosPredialesColindantes
     * @return
     */
    public List<Predio> getPrediosByNumerosPrediales(
        List<String> numerosPredialesColindantes);

    /**
     * Método que revisa si un predio se encuentra cancelado por predioId o numeroPredial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param predio
     * @return
     */
    public boolean isPredioCanceladoByPredioIdOrNumeroPredial(Long predioId,
        String numeroPredial);

    /**
     * Método que devuelve los predios asociados a una región de captura para ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @param idRegionOferta identificador de la región de captura de ofertas
     * @param idRecolector identificador del recoelctor asociado a la región de captura de ofertas
     * @return predios asociados a la región de captura de ofertas
     */
    public List<Predio> buscarPrediosPorRegionCapturaOferta(Long idRegionCaptura,
        String idRecolector);

    /**
     * obtiene la listan de números de terreno que están en una manzana y asociados a las
     * condiciones de propiedad dadas
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param manzana (17 primeras posiciones del número predial)
     * @param condicionesPropiedad
     * @return
     */
    public List<String> getTerrenoNumbersFromManzanaAndPropertyCondition(
        String manzana, List<String> condicionesPropiedad);

    /**
     * obtiene la lista de números de edificio o torre que se encuentran asociados al número de
     * terreno dado
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param terreno numero predial hasta el componente terreno (21 primeras posiciones del número
     * predial)
     * @return
     */
    public List<String> getEdificioTorreNumbersFromTerreno(String terreno);

    /**
     * obtiene la lista de números de piso que se encuentran asociados al número de edificio o torre
     * dado
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param edificioTorre numero predial hasta el componente 'edificio o torre' (24 primeras
     * posiciones del número predial)
     *
     * @return
     */
    public List<String> getPisoNumbersFromEdificioTorre(String edificioTorre);

    /**
     * Dice si existe un predio con el número predial dado y que tenga alguno de los estados dados
     *
     * @author pedro.garcia
     * @param numeroPredial
     * @param listaEstados lista de estados con los que se filtra la búsqueda. Si es null, no se
     * filtra por ese criterio
     * @version 2.0
     * @return true si existe al menos un predio con el número predial dado
     */
    public boolean existsPredio(String numeroPredial, String... listaEstados);

    /**
     * Busca los predios con el número predial dado sin hacer fetch de nada excepto del departamento
     * y municipio (para mostrar nombres)
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param numeroPredial
     * @param listaEstados opcional.
     * @return
     */
    /*
     * NO MODIFICAR
     */
    public List<Predio> lookForPrediosByNumPredialYEstadosNF(
        String numeroPredial, String... listaEstados);

    /**
     * Carga aleatoriamente un predio que inicia por un código determinado
     *
     * @param codeStartsWith
     * @return
     */
    public Predio loadRandomObject(String codeStartsWith);

    public Predio getPredioFetchDatosUbicacionParaMovilesById(Long predioId);

    /**
     * Obtiene las unidades de construccion relacionadas con un Predio. Hace fetch de los
     * UsoConstruccion
     *
     * @author pedro.garcia
     * @version 2.0
     * @param predioId
     * @return
     */
    public List<UnidadConstruccion> getUnidadesConstruccion(Long predioId);

    /**
     * Busca los predios segun el numero de manzana
     *
     * @author javier.barajas
     * @param numero de manzana
     * @return Lista de predios
     */
    public List<Predio> obtenerPrediosPorNumeroManzana(String numeroManzana);

    /**
     * Metodo para buscar por numero predial anterior o actual usado en IPER
     *
     * @author fredy.wilches
     * @param numeroPredial
     * @return
     */
    public Predio getPredioByNumeroPredialAnteriorOActual(String numeroPredial);

    /**
     * Metodo para buscar por una lista de numeros prediales los datos fiscos para generar reportes
     *
     * @author javier.barajas
     * @param List<numeroPredial>, valores de paginacion
     * @return List<numeroPredial>
     */
    public List<Object[]> getDatosFisicosByNumeroPredial(List<String> numeroPredial, int first,
        int pageSize);

    /**
     * Metodo para buscar por una lista de numeros prediales los datos Ficha Matriz para generar
     * reportes
     *
     * @author javier.barajas
     * @param List<numeroPredial>, valores de paginacion
     * @return List<numeroPredial>
     */
    public List<Object[]> getReporteFichaMatriz(List<String> numeroPredial, int first, int pageSize);

    /**
     * Metodo para buscar predios para la generacion de formularios SBC
     *
     * @cu 204
     * @author javier.barajas
     * @param String cadena de busqueda
     * @return List<Predio>
     */
    public List<Predio> getBuscaPredioGenereacionFormularioSBC(String cadenaBusqueda);

    /**
     * Retorna todos los predios que no estan incluidos en un reconocimiento predial
     *
     * @return
     * @author andres.eslava
     */
    public List<Predio> getPrediosLibreActualizacionReconocimientoYSaldoConservacion(
        String codigoManzana);

    /**
     * Obtiene un predio con los datos asociados necesarios para realizar las validaciones de los
     * tramites de rectificación y complementación
     *
     * @author felipe.cadena
     * @param predioId
     * @return
     */
    public Predio obtenerParaValidacionesRectificacionById(Long predioId);

    /**
     * Obtiene los predios asociados a un PH
     *
     * @author felipe.cadena
     * @param terreno numero predial hasta el componente terreno (21 primeras posiciones del número
     * predial)
     * @return
     */
    public List<Predio> obtenerPrediosPH(String terreno);

    /**
     * Obtiene la informacion completa de los predio asociado a un tramite de englobe
     *
     * @param idTramite
     * @return lista de predios asociados al tramite
     * @author andres.eslava
     */
    public List<Predio> obtenerPrediosCompletosEnglobePorTramiteId(Long tramiteId);

    /**
     * Obtiene el predio con direcciones y construcciones por el numero predial.
     *
     * @param numeroPredial numero predial del predio a buscar
     * @return returna el predio con sus direcciones y construcciones.
     *
     * @author andres.eslava
     */
    public Predio obtenerPredioCompletoPorNumeroPredial(String numeroPredial);

    /**
     * Obtiene predio por filtro de consulta catastral.
     *
     * @param filtro filtro que identifica los datos requeridos para la consulta sobre los predios
     * existentes
     * @return prediso que coinciden de acuerdo a los parametros de busqueda
     * @author andres.eslava
     */
    public List<Predio> obtenerPredioFiltroConsultaCatastralWebService(
        FiltroConsultaCatastralWebService filtro);

    /**
     * Busca los predios segun el numero de manzana
     *
     * @author javier.aponte
     * @param numero de manzana
     * @return Primer predio
     */
    public Predio obtenerPrimerPredioPorNumeroManzana(String numeroManzana);

    /**
     * Busca los predios segun el numero predial
     *
     * @author felipe.cadena
     * @param numero de terreno
     * @return List<Predio>
     */
    public List<Predio> obtenerPrediosPorNumerosPrediales(List<String> numeroTerreno);

    /**
     * Busca un {@link Predio} con todas sus listas dependientes
     *
     * @author david.cifuentes
     * @param idPredio
     * @return Predio
     */
    public Predio buscarPredioCompletoPorPredioId(Long idPredio);

    /**
     * Detemina si un predio tiene mejoras asociadas.
     *
     * @author pedro.garcia
     *
     * @param numeroPredioEnNumeroPredial segmento del número predial que contiene el número de
     * predio
     * @return true si alguno de los predios que compartan el número de predio tiene condición de
     * propiedad 5 o 6
     */
    public boolean tieneMejorasPredio(String numeroPredioEnNumeroPredial);

    /**
     * Retorna un predio y obtiene los datos de la tabla predio_avaluo_catastral asociados a éste.
     * Sólo debe usarse para la consulta del detalle del avalúo en la consulta de detalles del
     * predio.
     *
     * @param predioId id del predio que se consulta
     * @return
     * @author pedro.garcia
     */
    public Predio getDetalleAvaluosById(Long predioId);

    /**
     * Método que devuelve una lista con los números prediales que estén entre el número predial
     * inicial y el número predial final que se pasan como parámetros
     *
     * @param numeroPredialInicial
     * @param numeroPredialFinal
     * @return Lista con los números prediales que se encuentran entre el rango
     * @author javier.aponte
     */
    public List<String> getNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal(
        String numeroPredialInicial,
        String numeroPredialFinal, boolean completo);

    /**
     * Método que realiza la suma de los avalúos de terreno de los predios, las construcciones
     * convencionales y las no convncionales asociados a la ficha matriz proyectada.
     *
     * @author felipe.cadena
     *
     * @param idFicha
     * @return -- Arreglo de tres posiciones con los datos de: <br/>
     * - Total avaluo Predios <br/>
     * - Total avaluo construcciones Convencionales <br/>
     * - Total avaluo construcciones No Convencionales <br/>
     */
    public double[] calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long idFicha);

    /**
     * Consulta los predios asociados a un numero de resgitro y circulo registral determinado
     *
     * @author felipe.cadena
     * @param circuloRegistralCodigo
     * @param numeroRegistro
     * @return
     */
    public List<Predio> buscarPorMatricula(String circuloRegistralCodigo, String numeroRegistro);

    /**
     * Obtiene los predios que han sido modificados por un tramite geografico
     *
     * @param codigoManzana numero predial de 17 caracteres
     * @return lista de predios
     * @author andres.eslava
     */
    public List obtenerPrediosEditadosGeograficamenteSNC(String codigoManzana) throws Exception;

    /**
     * Consulta los predios cancelados de una Ficha Matriz verificando hasta Condicion de propiedad
     *
     * @author lorena.salamanca
     * @param numeroPredialFichaMatriz
     * @return
     */
    public List<Predio> obtenerPrediosCanceladosQuintaCondominioPH(String numeroPredialFichaMatriz);

    /**
     * Valida si el conjunto de predios del parametro se existen el la BD y estan en estado activo
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarExistenciaYActivos(List<String> nPrediales);

    /**
     * Obtiene las mejoras asociadas a un numero predial dado
     *
     *
     * @author felipe.cadena
     *
     * @param numeroPredial
     * @return
     */
    public List<Predio> obtenerMejorasPredio(String numeroPredial);

    /**
     * Determina la existencia de las matriculas dadas en los predios en firme
     *
     * @author felipe.cadena
     *
     * @param matriculas
     * @param municipioCodigo
     * @return
     */
    public List<Predio> validarExistenciaMatriculasPorMunicipio(List<String> matriculas,
        String municipioCodigo);

    /**
     * Determina la existencia de los predios asociados a los numeros prediales dados
     *
     * @author felipe.cadena
     * @param nPrediales
     * @return
     */
    public List<Predio> consultaPorNumeroPredial(List<String> nPrediales);

    /**
     * Retorna predios asociados a los ids dados
     *
     * @author felipe.cadena
     * @param predioIds
     * @return
     */
    public List<Predio> consultaPorPredioIds(List<Long> predioIds);

    /**
     * Consulta la cantidad de Predios que existen dentro del rango de numero predial
     *
     * @author leidy.gonzalez
     * @param numeroPredial
     * @param numeroPredialFinal
     * @return
     */
    public Integer contarPrediosPorRangoNumeroPredial(String numeroPredial,
        String numeroPredialFinal);

    /**
     * obtiene la listan de números de terreno que están en una manzana
     *
     * @author leidy.gonzalez
     *
     * @param manzana (17 primeras posiciones del número predial)
     * @return
     */
    public List<String> obtenerNumerosTerrenoPorPredioHastaManzana(String predioAManzana);
}
