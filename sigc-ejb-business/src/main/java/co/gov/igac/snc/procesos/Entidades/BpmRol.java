/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_ROL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmRol.findAll", query = "SELECT b FROM BpmRol b"),
    @NamedQuery(name = "BpmRol.findById", query = "SELECT b FROM BpmRol b WHERE b.id = :id"),
    @NamedQuery(name = "BpmRol.findByRol", query = "SELECT b FROM BpmRol b WHERE b.rol = :rol"),
    @NamedQuery(name = "BpmRol.findByListRol", query = "SELECT b FROM BpmRol b WHERE b.rol in(:rol)"),
    @NamedQuery(name = "BpmRol.findByObservaciones", query = "SELECT b FROM BpmRol b WHERE b.observaciones = :observaciones")})
public class BpmRol implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ROL")
    private String rol;
    @Column(name = "OBSERVACIONES")
    private String observaciones;

    public BpmRol() {
    }

    public BpmRol(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmRol)) {
            return false;
        }
        BpmRol other = (BpmRol) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmRol[ id=" + id + " ]";
    }
    
}
