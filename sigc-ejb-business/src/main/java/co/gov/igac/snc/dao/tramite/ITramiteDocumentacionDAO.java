package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;

@Local
public interface ITramiteDocumentacionDAO extends
    IGenericJpaDAO<TramiteDocumentacion, Long> {

    /**
     * Método que retorna una lista de TramiteDocumentacion asociada a un trámite específico,
     * ingresando el id del trámite.
     *
     * @param tramiteId
     * @return
     * @author fabio.navarrete
     */
    public List<TramiteDocumentacion> findByTramiteId(Long tramiteId);

    /**
     * Metodo que actualiza un a lista de TramiteDocumentacions y carga datos adicionales
     *
     * @author lorena.salamanca
     * @param tramiteDocumentacions
     * @return
     */
    public List<TramiteDocumentacion> updateMultipleCompleto(
        List<TramiteDocumentacion> tramiteDocumentacions);

    /**
     * Método para buscar la lista de TramiteDocumentacions asociada a una lista de Tramites
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitud
     *
     * @return
     */
    public List<TramiteDocumentacion> buscarTramiteDocumentacionsPorTramitesIds(
        List<Long> listaIdsTramites);
}
