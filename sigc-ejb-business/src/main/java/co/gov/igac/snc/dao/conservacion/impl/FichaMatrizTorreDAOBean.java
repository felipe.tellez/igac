/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizTorreDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los métodos para los servicios de persistencia del objeto
 * {@link FichaMatrizPredio} .
 *
 * @author david.cifuentes
 */
@Stateless
public class FichaMatrizTorreDAOBean extends
    GenericDAOWithJPA<FichaMatrizTorre, Long> implements
    IFichaMatrizTorreDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FichaMatrizTorreDAOBean.class);

    // -------------------------------------------------------------------------
    /**
     * @see IFichaMatrizTorreDAO#existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long numeroTorre,
     * Long idFichaMatriz)
     * @author david.cifuentes
     */
    @Override
    public boolean existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(
        Long numeroTorre, Long idFichaMatriz) {

        Long resultado = 0L;

        try {
            Query q = entityManager.createQuery("SELECT COUNT(fmt.id) FROM FichaMatrizTorre fmt " +
                "JOIN fmt.fichaMatriz fm " +
                "WHERE fmt.torre = :numeroTorre " +
                "AND fm.id = :idFichaMatriz ");
            q.setParameter("numeroTorre", numeroTorre);
            q.setParameter("idFichaMatriz", idFichaMatriz);

            resultado = (Long) q.getSingleResult();
            if (resultado != null && resultado.intValue() > 0) {
                return true;
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010
                .getExcepcion(LOGGER, e, e.getMessage(),
                    "FichaMatrizTorreDAOBean#existeTorreEnFirmePorIdFichaMatrizYNumeroTorre");
        }
        return false;
    }

    /**
     * TODO:Documentar Método -> Recordar adjuntar autor
     */
    @Implement
    @Override
    public FichaMatrizTorre getFichaMatrizTorreById(Long id) {
        LOGGER.debug("getPredioByIdPredio");
        FichaMatrizTorre fichaMatrizTorre = this.entityManager.find(FichaMatrizTorre.class, id);
        return fichaMatrizTorre;
    }

    /**
     * @see IFichaMatrizTorreDAO#buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long numeroTorre,
     * Long idFichaMatriz)
     * @author leidy.gonzalez
     */
    @Override
    public FichaMatrizTorre buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
        Long numeroTorre, Long idFichaMatriz) {

        FichaMatrizTorre resultado = new FichaMatrizTorre();

        try {
            Query q = entityManager.createQuery("SELECT fmt FROM FichaMatrizTorre fmt " +
                "JOIN fmt.fichaMatriz fm " +
                "WHERE fmt.torre = :numeroTorre " +
                "AND fm.id = :idFichaMatriz ");
            q.setParameter("numeroTorre", numeroTorre);
            q.setParameter("idFichaMatriz", idFichaMatriz);

            resultado = (FichaMatrizTorre) q.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010
                .getExcepcion(LOGGER, e, e.getMessage(),
                    "FichaMatrizTorreDAOBean#buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre");
        }
        return resultado;
    }

    // end of class
}
