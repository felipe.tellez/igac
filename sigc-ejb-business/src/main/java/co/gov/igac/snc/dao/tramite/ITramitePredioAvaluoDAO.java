package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;

/**
 * Esta clase representa los predios que hacen parte del trámite de un avalúo.
 *
 * @author jamir.avila
 *
 */
@Local
public interface ITramitePredioAvaluoDAO {

    /**
     * Crea un predio y lo vincula con un avalúo.
     *
     * @param tramitePredioAvaluo objeto con los datos del predio a vincular.
     * @return el objeto de transporte con los datos del predio.
     */
    public TramitePredioAvaluo adicionarPredioAAvaluo(TramitePredioAvaluo tramitePredioAvaluo);

    /**
     * Remueve el vínculo entre un predio y un avalúo.
     *
     * @param idTramitePredioAvaluo identificador de la relación trámite - predio de avalúo a
     * remover.
     */
    public void removerPredioDeAvaluo(Long idTramitePredioAvaluo);

    /**
     * Actualiza los datos de un predio que pertenece a un avalúo.
     *
     * @param predioAvaluo objeto con los datos actualizados del predio.
     */
    public void actualizarPredioDeAvaluo(TramitePredioAvaluo predioAvaluo);

    /**
     * Método que carga los predios asociados a un trámite de avaluos especifico.
     *
     * @author ariel.ortiz
     * @param tramiteId
     * @return
     */
    public List<TramitePredioAvaluo> buscarPrediosPorTramiteId(Long tramiteId);
}
