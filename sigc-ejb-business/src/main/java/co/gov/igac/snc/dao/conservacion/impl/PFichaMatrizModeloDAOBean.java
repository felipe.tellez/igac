package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizModeloDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;

/**
 * Implementación de los servicios de persistencia del objeto {@link PFichaMatrizModelo}.
 *
 * @author david.cifuentes
 */
@Stateless
public class PFichaMatrizModeloDAOBean extends
    GenericDAOWithJPA<PFichaMatrizModelo, Long> implements
    IPFichaMatrizModeloDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PFichaMatrizModeloDAOBean.class);

    /**
     * @author david.cifuentes
     * @see IPFichaMatrizModeloDAO#cargarModeloDeConstruccion(Long)
     */
    @Override
    public PFichaMatrizModelo cargarModeloDeConstruccion(
        Long pFichaMatrizModeloId) {
        PFichaMatrizModelo answer = null;

        String sql = "SELECT DISTINCT pfmm FROM PFichaMatrizModelo pfmm" +
            " LEFT JOIN FETCH pfmm.PFmModeloConstruccions pfmc" +
            " LEFT JOIN FETCH pfmc.usoConstruccion " +
            " WHERE pfmm.id = :pFichaMatrizModeloId";

        Query query = this.entityManager.createQuery(sql);
        query.setParameter("pFichaMatrizModeloId", pFichaMatrizModeloId);
        try {
            answer = (PFichaMatrizModelo) query.getSingleResult();

            if (answer != null && answer.getPFmModeloConstruccions() != null) {
                answer.getPFmModeloConstruccions().size();
                if (answer.getPFmModeloConstruccions().size() > 0) {
                    for (PFmModeloConstruccion pfmmc : answer.getPFmModeloConstruccions()) {
                        pfmmc.getPFmConstruccionComponentes().size();
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;
    }
}
