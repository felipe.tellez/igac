package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioZona;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoPredioZona
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IAvaluoPredioZonaDAO extends IGenericJpaDAO<AvaluoPredioZona, Long> {

    /**
     * Método para determinar si existen zonas asociadas a un avalúo o a los predio s del avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public Boolean calcularZonasPreviasAvaluo(Long idAvaluo);

    /**
     * Hace la consulta por el id del Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioZona> consultarPorAvaluo(long avaluoId);

//end of interface
    /**
     * Método para eliminar la zonas previas que esten relacionadas al avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public boolean eliminarZonasPreviasAvaluo(Long idAvaluo);

}
