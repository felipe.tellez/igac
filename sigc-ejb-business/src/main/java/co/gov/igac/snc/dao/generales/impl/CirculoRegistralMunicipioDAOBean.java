/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.CirculoRegistralMunicipio;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ICirculoRegistralMunicipioDAO;

/**
 *
 * @author javier.aponte
 */
@Stateless
public class CirculoRegistralMunicipioDAOBean extends GenericDAOWithJPA<CirculoRegistralMunicipio, String>
    implements ICirculoRegistralMunicipioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        CirculoRegistralMunicipioDAOBean.class);

    /**
     * @see ICirculoRegistralMunicipioDAO#findByCodigoCirculoRegistral(String)
     * @author javier.aponte
     */
    public List<CirculoRegistralMunicipio> findByCodigoCirculoRegistral(
        String codigoCirculoRegistral) {

        LOGGER.debug("on CirculoRegistralMunicipioDAOBean#findByCodigoCirculoRegistral...");
        List<CirculoRegistralMunicipio> answer = null;
        String queryString;
        Query query;

        queryString = " SELECT crm from CirculoRegistralMunicipio crm " +
            "JOIN FETCH crm.municipio m JOIN FETCH m.departamento d WHERE crm.circuloRegistral.codigo = :codigo";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("codigo", codigoCirculoRegistral);

        try {
            answer = query.getResultList();
            LOGGER.debug("... ok.");
        } catch (Exception e) {
            LOGGER.error("... error. " + e.getMessage());
        }

        return answer;
    }

}
