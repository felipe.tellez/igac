package co.gov.igac.snc.dao.conservacion;

import java.math.BigDecimal;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.TmpBloqueoMasivo;

@Local
public interface ITmpBloqueoDAO extends IGenericJpaDAO<TmpBloqueoMasivo, Long> {

    public BigDecimal generarSecuenciaTmpBloqueoMasivo();
}
