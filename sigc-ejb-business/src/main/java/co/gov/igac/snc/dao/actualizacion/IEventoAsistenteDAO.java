package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;

/**
 * Servicios de persistencia para el objeto EventoAsistente
 *
 * @author franz.gamba
 */
@Local
public interface IEventoAsistenteDAO extends IGenericJpaDAO<EventoAsistente, Long> {

    /**
     * Método que trae el Asistentes al evento de Actualizacion asignado a una evento de
     * actualizacion
     *
     * @cu 216 Instalacion de Comision de Campo
     * @param actualizacionEventoId
     * @return
     * @version 1.0
     * @author javier.barajas
     */
    public List<EventoAsistente> obtenerAsistenteEventoporActualizacionEvento(
        Long actualizacionEventoId);

}
