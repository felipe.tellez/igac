/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.CodigoHomologado;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ICodigoHomologadoDAO;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.ESistema;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author fredy.wilches
 */
@Stateless
public class CodigoHomologadoDAOBean extends GenericDAOWithJPA<CodigoHomologado, Long> implements
    ICodigoHomologadoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodigoHomologadoDAOBean.class);

    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.dao.generales.ICodigoHomologadoDAO#obtenerCodigoHomologado(co.gov.igac.snc.persistence.util.ECodigoHomologado,
     * co.gov.igac.snc.persistence.util.ESistema, co.gov.igac.snc.persistence.util.ESistema,
     * java.lang.String, java.util.Date)
     */
    @Override
    public String obtenerCodigoHomologado(ECodigoHomologado codigo, ESistema origen,
        ESistema destino, String valor, Date fecha) {

        Query query;
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd");

        String queryToExecute = "SELECT FNC_HOMOLOGAR_CODIGO('" + codigo.toString() + "', '" +
            origen.toString() + "', '" + destino.toString() + "', '" + valor +
            (fecha != null ? "', to_date('" + formatoDeFecha.format(fecha) + "','yyyy/mm/dd')" :
                "'") + ") from dual";

        LOGGER.debug(queryToExecute);

        query = entityManager.createNativeQuery(queryToExecute);
        String resultStr = null;
        try {
            Object result = query.getSingleResult();
            if (result != null) {
                resultStr = result.toString();
            }
        } catch (NoResultException ex) {
            LOGGER.error(ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, ex, ex.
                getMessage(),
                "La consulta de código homologado no obtuvo resultados");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultStr;
    }
//--------------------------------------------------------------------------------------------------	

    @Override
    public String obtenerCodigo(ECodigoHomologado codigo, ESistema origen, ESistema destino,
        String valor, Date fecha) {

        Query query;
        String queryToExecute =
            "SELECT VALOR_SISTEMA_1 FROM CODIGO_HOMOLOGADO WHERE NOMBRE_CODIGO = '" + codigo.
                toString() + "' AND SISTEMA_1 = '" + origen.toString() +
            "' AND SISTEMA_2 = '" + destino.toString() + "' AND VALOR_SISTEMA_2 = '" + valor + "'";

        LOGGER.debug(queryToExecute);

        query = entityManager.createNativeQuery(queryToExecute);
        String resultStr = null;
        try {
            Object result = query.getSingleResult();
            if (result != null) {
                resultStr = result.toString();
            }
        } catch (NoResultException ex) {
            LOGGER.error(ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, ex, ex.
                getMessage(),
                "La consulta de código homologado no obtuvo resultados");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultStr;
    }

}
