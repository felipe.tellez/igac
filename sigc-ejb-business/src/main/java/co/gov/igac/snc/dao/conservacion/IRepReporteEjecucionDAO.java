package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte control reporte
 *
 * @author javier.aponte
 */
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.util.FiltroDatosConsultaReportesRadicacion;
import co.gov.igac.snc.util.FiltroGenerarReportes;

@Local
public interface IRepReporteEjecucionDAO extends IGenericJpaDAO<RepReporteEjecucion, Long> {

    /**
     * Método encargado de obtener Jobs de reporte que se encuentran en estado Iniciado
     *
     * @return Retorna la Lista de reportes en estado Iniciado
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> obtenerJobsReporteEjecucionTerminado(Long cantidad);

    /**
     * Método encargado de buscar los reporte que fueron ejecutados por el usuario logeado
     *
     * @return Retorna la Lista de reportes que pertenecen al usuario
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> buscarReporteEjecucionPorUsuario(String usuarioLogin);

    /**
     * Método que llama al procedimiento que consulta los reportes historicos de cierre anual
     * generados para un departamento o municipio.
     *
     * @author david.cifuentes
     *
     * @param codigoDepartamento
     * @param codigoMunicipio
     * @return
     */
    public List<RepReporteEjecucion> buscarReporteEjecucionCierreAnualHistoricos(
        String codigoDepartamento, String codigoMunicipio);

    public List<RepReporteEjecucion> buscarReportePorDepartamentoYMunicipio(Long tipoReporte,
        String codigoDepartamento, String codigoMunicipio);

    /**
     * Método que consulta un reporte por el codigo generado
     *
     * @author leidy.gonzalez
     *
     * @param datosConsultaPredio
     * @param selectedDepartamentoCod
     * @param territorial
     */
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicos(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedDepartamentoCod, String selectedMunicipioCod, String territorial, String uoc);

    /**
     * Método que realiza la busqueda de {@link RepReporteEjecucion} que cumplen una serie de
     * condiciones enviadas como parámetros, está pensada para los reportes de radicación
     *
     * @param datosConsultaReportesRadicacion
     * @return
     */
    public List<RepReporteEjecucion> buscarReportesRadicacionGenerados(
        FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion);

    /**
     * Método encargado de obtener Jobs de reporte
     *
     * @param idReporteConsultar
     * @return Retorna la Lista de reportes
     * @author leidy.gonzalez
     */
    //public RepReporteEjecucion buscarReportesPorIds(Long idReporteConsultar);
    /**
     * Método que consulta un reporte por el id Reporte
     *
     * @author leidy.gonzalez
     *
     * @param idReporteConsultar
     * @return
     */
    public RepReporteEjecucion buscarReportePorId(Long idReporteConsultar);

    /**
     * Metodo para obtener una lista de ejecuciones por id
     *
     * @author felipe.cadena
     *
     * @param idReporteConsultar
     * @return
     */
    public List<RepReporteEjecucion> buscarReportesPorIds(List<Long> idReporteConsultar);

    /**
     * Método que consulta un reporte por el codigo generado
     *
     * @author leidy.gonzalez
     *
     * @param idReporteConsultar
     * @return
     */
    public List<RepReporteEjecucion> buscarReportesPorCodigo(String idReporteConsultar);

    public RepReporteEjecucion buscarReportePorCodigo(String codigoReporte);

    /**
     * Método que consulta un reporte por el id Reporte
     *
     * @author leidy.gonzalez
     *
     * @param idReporteConsultar
     * @return
     */
    public List<RepReporteEjecucion> buscarReportesPorId(Long idReporteConsultar);

    /**
     * Método que consulta un reporte por los filtros ingresados omitiendo el municipio
     *
     * @author leidy.gonzalez
     *
     * @param datosConsultaPredio
     * @param selectedDepartamentoCod
     * @param territorial
     */
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicosSinMuncipio(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedDepartamentoCod, String territorial, String uoc);

}
