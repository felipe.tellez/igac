package co.gov.igac.snc.dao.tramite.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.dao.tramite.IVSolicitudPredioDAO;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class VSolicitudPredioDAOBean extends
    GenericDAOWithJPA<VSolicitudPredio, Long> implements
    IVSolicitudPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitanteSolicitudDAOBean.class);

    /**
     * @modify fabio.navarrete
     * @author david.cifuentes
     * @see ISolicitanteSolicitudDAO#buscarSolicitudPorFiltros(VSolicitudPredio
     * datosFiltroSolicitud)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VSolicitudPredio> buscarSolicitudPorFiltros(
        VSolicitudPredio datosFiltroSolicitud, int desde, int cantidad) {

        LOGGER.debug("Entra en VSolicitudPredioDAOBean#buscarSolicitudPorFiltros");

        if (desde < 0) {
            desde = 0;
        }

        List<VSolicitudPredio> answer = new ArrayList<VSolicitudPredio>();
        // TODO fabio.navarrete :: 02-08-2011 :: Implementar el query con
        // criteria, si algún día se cambia a jpa 2.0 :: fabio.navarrete
        /*
         * CriteriaBuilder queryBuilder = em.getCriteriaBuilder(); CriteriaQuery qdef =
         * queryBuilder.createQuery();
         */
        StringBuilder query = new StringBuilder();
        // LEFT JOIN FETCH vsp.tramite

        query.append("SELECT vsp FROM VSolicitudPredio vsp WHERE 1=1 ");

        if (datosFiltroSolicitud.getTipoIdentificacion() != null &&
            datosFiltroSolicitud.getTipoIdentificacion().compareTo("") != 0) {
            query.append(" AND vsp.tipoIdentificacion LIKE :tipoIdentificacion");
        }
        if (datosFiltroSolicitud.getNumeroIdentificacion() != null &&
            datosFiltroSolicitud.getNumeroIdentificacion().compareTo("") != 0) {
            query.append(" AND vsp.numeroIdentificacion = :numeroIdentificacion");
        }
        if (datosFiltroSolicitud.getDigitoVerificacion() != null &&
            datosFiltroSolicitud.getDigitoVerificacion().compareTo("") != 0) {
            query.append(" AND vsp.digitoVerificacion = :digitoVerificacion");
        }
        if (datosFiltroSolicitud.getPrimerNombre() != null &&
            datosFiltroSolicitud.getPrimerNombre().compareTo("") != 0) {
            query.append(" AND UPPER(vsp.primerNombre) LIKE :primerNombre");
        }
        if (datosFiltroSolicitud.getSegundoNombre() != null &&
            datosFiltroSolicitud.getSegundoNombre().compareTo("") != 0) {
            query.append(" AND UPPER(vsp.segundoNombre) LIKE :segundoNombre");
        }
        if (datosFiltroSolicitud.getPrimerApellido() != null &&
            datosFiltroSolicitud.getPrimerApellido().compareTo("") != 0) {
            query.append(" AND UPPER(vsp.primerApellido) LIKE :primerApellido");
        }
        if (datosFiltroSolicitud.getSegundoApellido() != null &&
            datosFiltroSolicitud.getSegundoApellido().compareTo("") != 0) {
            query.append(" AND UPPER(vsp.segundoApellido) LIKE :segundoApellido");
        }

        if (datosFiltroSolicitud.getNumeroPredial() != null &&
            datosFiltroSolicitud.getNumeroPredial().compareTo("") != 0) {
            query.append(" AND vsp.numeroPredial LIKE :numeroPredial");
        }

        Query q = entityManager.createQuery(query.toString());

        if (datosFiltroSolicitud.getTipoIdentificacion() != null &&
            datosFiltroSolicitud.getTipoIdentificacion().compareTo("") != 0) {
            q.setParameter("tipoIdentificacion",
                datosFiltroSolicitud.getTipoIdentificacion());
        }
        if (datosFiltroSolicitud.getNumeroIdentificacion() != null &&
            datosFiltroSolicitud.getNumeroIdentificacion().compareTo("") != 0) {
            q.setParameter("numeroIdentificacion",
                datosFiltroSolicitud.getNumeroIdentificacion());
        }
        if (datosFiltroSolicitud.getDigitoVerificacion() != null &&
            datosFiltroSolicitud.getDigitoVerificacion().compareTo("") != 0) {
            q.setParameter("digitoVerificacion",
                datosFiltroSolicitud.getDigitoVerificacion());
        }
        if (datosFiltroSolicitud.getPrimerNombre() != null &&
            datosFiltroSolicitud.getPrimerNombre().compareTo("") != 0) {
            q.setParameter("primerNombre", datosFiltroSolicitud
                .getPrimerNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getSegundoNombre() != null &&
            datosFiltroSolicitud.getSegundoNombre().compareTo("") != 0) {
            q.setParameter("segundoNombre", datosFiltroSolicitud
                .getSegundoNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getPrimerApellido() != null &&
            datosFiltroSolicitud.getPrimerApellido().compareTo("") != 0) {
            q.setParameter("primerApellido", datosFiltroSolicitud
                .getPrimerApellido().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getSegundoApellido() != null &&
            datosFiltroSolicitud.getSegundoApellido().compareTo("") != 0) {
            q.setParameter("segundoApellido", datosFiltroSolicitud
                .getSegundoApellido().toUpperCase() + "%");
        }

        if (datosFiltroSolicitud.getNumeroPredial() != null &&
            datosFiltroSolicitud.getNumeroPredial().compareTo("") != 0) {
            q.setParameter("numeroPredial",
                datosFiltroSolicitud.getNumeroPredial());
        }

        q.setFirstResult(desde);
        if (cantidad > 0) {
            q.setMaxResults(cantidad);
        }

        try {
            answer = q.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("VSolicitudPredioDAOBean#buscarSolicitudPorFiltros: " +
                "La consulta de solicitanteSolicitud no devolvió resultados");
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de solicitantes Solicitud: " +
                ex.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVSolicitudPredioDAO#findSolicitudesPaginadasByFiltro(VSolicitudPredio, int...)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<VSolicitudPredio> findSolicitudesPaginadasByFiltro(
        VSolicitudPredio datosFiltroSolicitud, String sortField, String sortOrder,
        Map<String, String> filters,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("Entra en VSolicitudPredioDAOBean#findSolicitudesPaginadasByFiltro");

        List<VSolicitudPredio> answer;
        String where = "";
        Map<String, Object> valoresFiltros = new HashMap<String, Object>();
        if (filters != null) {
            for (String campo : filters.keySet()) {
                if (campo.equals("numeroRadicacionSolicitud")) {
                    where += " AND vsp." + campo + " like :" + campo;
                    valoresFiltros.put(campo, "%" + filters.get(campo) + "%");
                }
                if (campo.equals("numeroRadicacionTramite")) {
                    where += " AND vsp." + campo + " like :" + campo;
                    valoresFiltros.put(campo, "%" + filters.get(campo) + "%");
                }
            }
        }

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT vsp FROM VSolicitudPredio vsp WHERE 1=1 ");

        query.append(criteriosDeBusqueda(datosFiltroSolicitud));

        if (valoresFiltros.size() > 0) {
            query.append(where);
        }

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            query.append(" ORDER BY t.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

        Query q = this.entityManager.createQuery(query.toString());

        if (datosFiltroSolicitud.getTipoIdentificacion() != null &&
            datosFiltroSolicitud.getTipoIdentificacion().compareTo("") != 0) {
            q.setParameter("tipoIdentificacion",
                datosFiltroSolicitud.getTipoIdentificacion());
        }
        if (datosFiltroSolicitud.getNumeroIdentificacion() != null &&
            datosFiltroSolicitud.getNumeroIdentificacion().compareTo("") != 0) {
            q.setParameter("numeroIdentificacion",
                datosFiltroSolicitud.getNumeroIdentificacion());
        }
        if (datosFiltroSolicitud.getDigitoVerificacion() != null &&
            datosFiltroSolicitud.getDigitoVerificacion().compareTo("") != 0) {
            q.setParameter("digitoVerificacion",
                datosFiltroSolicitud.getDigitoVerificacion());
        }
        if (datosFiltroSolicitud.getPrimerNombre() != null &&
            datosFiltroSolicitud.getPrimerNombre().compareTo("") != 0) {
            q.setParameter("primerNombre", datosFiltroSolicitud
                .getPrimerNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getSegundoNombre() != null &&
            datosFiltroSolicitud.getSegundoNombre().compareTo("") != 0) {
            q.setParameter("segundoNombre", datosFiltroSolicitud
                .getSegundoNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getPrimerApellido() != null &&
            datosFiltroSolicitud.getPrimerApellido().compareTo("") != 0) {
            q.setParameter("primerApellido", datosFiltroSolicitud
                .getPrimerApellido().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getSegundoApellido() != null &&
            datosFiltroSolicitud.getSegundoApellido().compareTo("") != 0) {
            q.setParameter("segundoApellido", datosFiltroSolicitud
                .getSegundoApellido().toUpperCase() + "%");
        }

        if (datosFiltroSolicitud.getNumeroPredial() != null &&
            datosFiltroSolicitud.getNumeroPredial().compareTo("") != 0) {
            q.setParameter("numeroPredial",
                datosFiltroSolicitud.getNumeroPredial());
        }

        if (valoresFiltros.size() > 0) {
            for (String campo : valoresFiltros.keySet()) {
                q.setParameter(campo, valoresFiltros.get(campo));
                LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
            }
        }

        try {
            answer = findInRangeUsingQuery(q, rowStartIdxAndCount);
        } catch (IndexOutOfBoundsException iobe) {
            answer = new ArrayList<VSolicitudPredio>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
     * Método que arma el query para busqueda y contedo de solicitudes páginadas con base en el
     * filtro de solicitud
     *
     * @author juan.agudelo
     * @version 2.0
     * @param datosFiltroSolicitud
     * @return
     */
    private String criteriosDeBusqueda(VSolicitudPredio datosFiltroSolicitud) {

        StringBuilder sbTmp = new StringBuilder();

        if (datosFiltroSolicitud.getTipoIdentificacion() != null &&
            datosFiltroSolicitud.getTipoIdentificacion().compareTo("") != 0) {
            sbTmp.append(" AND vsp.tipoIdentificacion LIKE :tipoIdentificacion");
        }
        if (datosFiltroSolicitud.getNumeroIdentificacion() != null &&
            datosFiltroSolicitud.getNumeroIdentificacion().compareTo("") != 0) {
            sbTmp.append(" AND vsp.numeroIdentificacion = :numeroIdentificacion");
        }
        if (datosFiltroSolicitud.getDigitoVerificacion() != null &&
            datosFiltroSolicitud.getDigitoVerificacion().compareTo("") != 0) {
            sbTmp.append(" AND vsp.digitoVerificacion = :digitoVerificacion");
        }
        if (datosFiltroSolicitud.getPrimerNombre() != null &&
            datosFiltroSolicitud.getPrimerNombre().compareTo("") != 0) {
            sbTmp.append(" AND UPPER(vsp.primerNombre) LIKE :primerNombre");
        }
        if (datosFiltroSolicitud.getSegundoNombre() != null &&
            datosFiltroSolicitud.getSegundoNombre().compareTo("") != 0) {
            sbTmp.append(" AND UPPER(vsp.segundoNombre) LIKE :segundoNombre");
        }
        if (datosFiltroSolicitud.getPrimerApellido() != null &&
            datosFiltroSolicitud.getPrimerApellido().compareTo("") != 0) {
            sbTmp.append(" AND UPPER(vsp.primerApellido) LIKE :primerApellido");
        }
        if (datosFiltroSolicitud.getSegundoApellido() != null &&
            datosFiltroSolicitud.getSegundoApellido().compareTo("") != 0) {
            sbTmp.append(" AND UPPER(vsp.segundoApellido) LIKE :segundoApellido");
        }

        if (datosFiltroSolicitud.getNumeroPredial() != null &&
            datosFiltroSolicitud.getNumeroPredial().compareTo("") != 0) {
            sbTmp.append(" AND vsp.numeroPredial LIKE :numeroPredial");
        }

        return sbTmp.toString();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVSolicitudPredioDAO#countSolicitudesPaginadasByFiltro(VSolicitudPredio)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Integer countSolicitudesPaginadasByFiltro(
        VSolicitudPredio datosFiltroSolicitud) {
        LOGGER.debug("Entra en VSolicitudPredioDAOBean#countSolicitudesPaginadasByFiltro");

        Integer answer;

        StringBuilder query = new StringBuilder();

        query.append("SELECT COUNT (vsp) FROM VSolicitudPredio vsp WHERE 1=1 ");

        query.append(criteriosDeBusqueda(datosFiltroSolicitud));

        Query q = entityManager.createQuery(query.toString());

        if (datosFiltroSolicitud.getTipoIdentificacion() != null &&
            datosFiltroSolicitud.getTipoIdentificacion().compareTo("") != 0) {
            q.setParameter("tipoIdentificacion",
                datosFiltroSolicitud.getTipoIdentificacion());
        }
        if (datosFiltroSolicitud.getNumeroIdentificacion() != null &&
            datosFiltroSolicitud.getNumeroIdentificacion().compareTo("") != 0) {
            q.setParameter("numeroIdentificacion",
                datosFiltroSolicitud.getNumeroIdentificacion());
        }
        if (datosFiltroSolicitud.getDigitoVerificacion() != null &&
            datosFiltroSolicitud.getDigitoVerificacion().compareTo("") != 0) {
            q.setParameter("digitoVerificacion",
                datosFiltroSolicitud.getDigitoVerificacion());
        }
        if (datosFiltroSolicitud.getPrimerNombre() != null &&
            datosFiltroSolicitud.getPrimerNombre().compareTo("") != 0) {
            q.setParameter("primerNombre", datosFiltroSolicitud
                .getPrimerNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getSegundoNombre() != null &&
            datosFiltroSolicitud.getSegundoNombre().compareTo("") != 0) {
            q.setParameter("segundoNombre", datosFiltroSolicitud
                .getSegundoNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getPrimerApellido() != null &&
            datosFiltroSolicitud.getPrimerApellido().compareTo("") != 0) {
            q.setParameter("primerApellido", datosFiltroSolicitud
                .getPrimerApellido().toUpperCase() + "%");
        }
        if (datosFiltroSolicitud.getSegundoApellido() != null &&
            datosFiltroSolicitud.getSegundoApellido().compareTo("") != 0) {
            q.setParameter("segundoApellido", datosFiltroSolicitud
                .getSegundoApellido().toUpperCase() + "%");
        }

        if (datosFiltroSolicitud.getNumeroPredial() != null &&
            datosFiltroSolicitud.getNumeroPredial().compareTo("") != 0) {
            q.setParameter("numeroPredial",
                datosFiltroSolicitud.getNumeroPredial());
        }

        try {
            answer = ((Long) q.getSingleResult()).intValue();
        } catch (NoResultException nr) {
            answer = 0;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

}
