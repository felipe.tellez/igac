package co.gov.igac.snc.dao.conservacion.impl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.persistence.util.ERepReporteEjecucionEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaReportesRadicacion;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import com.lowagie.text.pdf.PdfFormXObject;
import java.math.BigDecimal;

@Stateless
public class RepReporteEjecucionDAOBean extends GenericDAOWithJPA<RepReporteEjecucion, Long> implements 
		IRepReporteEjecucionDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepReporteEjecucionDAOBean.class);
	
	/**
	 * @see RepReporteEjecucion#obtenerJobsReporteEjecucionTerminado
	 * @author leidy.gonzalez
	 */
	@Override
	public List<RepReporteEjecucion> obtenerJobsReporteEjecucionTerminado(Long cantidad) {
		List<RepReporteEjecucion> answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre " +
				"where rre.estado = :estado "+
				"and rre.resultadoGeneracion = :resultadoGeneracion";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("estado", ERepReporteEjecucionEstado.EN_PROCESO.getCodigo());
            query.setParameter("resultadoGeneracion", ERepReporteEjecucionEstado.TERMINADO.getCodigo());
            
            if(cantidad != null && cantidad > 0){
                query.setMaxResults(cantidad.intValue());
            }
            
			answer = (ArrayList<RepReporteEjecucion>) query.getResultList();
			
			for(RepReporteEjecucion temp : answer){
				Hibernate.initialize(temp.getRepReporte());
				
				if (temp.getTerritorial()!=null && !temp.getTerritorial().getCodigo().isEmpty()) {
					Hibernate.initialize(temp.getTerritorial().getCodigo());
				}

				if (temp.getTerritorial()!=null && !temp.getTerritorial().getNombre().isEmpty()) {
					Hibernate.initialize(temp.getTerritorial().getNombre());
				}
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#obtenerJobsControlReporteTerminado: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}

		return answer;
	}
	
	/**
	 * @see RepReporteEjecucion#buscarReporteEjecucionPorUsuario(String usuarioLogin)
	 * @author leidy.gonzalez
	 */
	@Override
	public List<RepReporteEjecucion> buscarReporteEjecucionPorUsuario(String usuarioLogin) {
		List<RepReporteEjecucion> answer = null;
		
		Query query;
		String queryToExecute;

		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre where rre.usuario = :usuario";

		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("usuario", usuarioLogin);
            
			answer = (ArrayList<RepReporteEjecucion>) query.getResultList();
			
			for(RepReporteEjecucion temp : answer){
				Hibernate.initialize(temp.getRepReporte());
				
				if (!temp.getTerritorial().getCodigo().isEmpty()) {
					Hibernate.initialize(temp.getTerritorial().getCodigo());
				}

				if (!temp.getTerritorial().getNombre().isEmpty()) {
					Hibernate.initialize(temp.getTerritorial().getNombre());
				}
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReporteEjecucionPorUsuario: "
					+ "La consulta de reporte control reporte no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}

		return answer;
	}

	/**
	 * @author david.cifuentes
	 * @see IRepReporteEjecucionDAO#buscarReporteEjecucionCierreAnualHistoricos(String,
	 *      String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RepReporteEjecucion> buscarReporteEjecucionCierreAnualHistoricos(
			String codigoDepartamento, String codigoMunicipio) {
		
		List<RepReporteEjecucion> answer = null;
		Query query;
		String queryToExecute;
		List<String> categoriasCierreAnual = new ArrayList<String>();
		categoriasCierreAnual.add(ERepReporteCategoria.CIERRE_ANUAL_DEPARTAMENTAL.getCategoria());
		categoriasCierreAnual.add(ERepReporteCategoria.CIERRE_ANUAL_GENERAL.getCategoria());
		categoriasCierreAnual.add(ERepReporteCategoria.CIERRE_ANUAL_MUNICIPAL.getCategoria());
		categoriasCierreAnual.add(ERepReporteCategoria.CIERRE_ANUAL_PROCESO.getCategoria());

		if(codigoMunicipio != null && !codigoMunicipio.trim().isEmpty()){
			queryToExecute = "SELECT rre FROM RepReporteEjecucion rre "
					+ "JOIN FETCH rre.repReporte rr "
					+ "WHERE rre.departamentoCodigo = :codigoDepartamento "
					+ "AND rre.municipioCodigo = :codigoMunicipio "
					+ "AND rr.categoria IN (:categoriasCierreAnual)";
			query = this.entityManager.createQuery(queryToExecute);
			query.setParameter("codigoDepartamento", codigoDepartamento);
			query.setParameter("codigoMunicipio", codigoMunicipio);
			query.setParameter("categoriasCierreAnual", categoriasCierreAnual);
		}else{
			queryToExecute = "SELECT rre FROM RepReporteEjecucion rre "
					+ "JOIN FETCH rre.repReporte rr "
					+ "WHERE rre.departamentoCodigo = :codigoDepartamento "
					+ "AND rr.categoria IN (:categoriasCierreAnual)";
			query = this.entityManager.createQuery(queryToExecute);
			query.setParameter("codigoDepartamento", codigoDepartamento);
			query.setParameter("categoriasCierreAnual", categoriasCierreAnual);
		}
		
		try {

			answer = (ArrayList<RepReporteEjecucion>) query.getResultList();

			if(answer != null){
				for (RepReporteEjecucion temp : answer) {
					Hibernate.initialize(temp.getRepReporte());
				}
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReporteEjecucionCierreAnualHistoricos: "
					+ "La consulta de reporte control reporte no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}

		return answer;
	}

	/**
	 * @see IRepReporteEjecucionDAO#buscarReportesRadicacionGenerados(FiltroDatosConsultaReportesRadicacion)
	 * @author david.cifuentes
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RepReporteEjecucion> buscarReportesRadicacionGenerados(
			FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion) {

		StringBuilder query = new StringBuilder();

		query.append("SELECT DISTINCT rre" 
				+ " FROM RepReporteEjecucion rre"
				+ " JOIN rre.repReporteEjecucionDetalles rred" 
				+ " WHERE 1=1");
		
		if (datosConsultaReportesRadicacion.getActividad() != null
				&& !datosConsultaReportesRadicacion.getActividad().isEmpty()) {
			query.append(" AND rred.actividad = :actividad");
		}
		if (datosConsultaReportesRadicacion.getClasificacion() != null
				&& !datosConsultaReportesRadicacion.getClasificacion().isEmpty()) {
			query.append(" AND rred.clasificacion = :clasificacion");
		}
		if (datosConsultaReportesRadicacion.getDepartamentoCodigo() != null
				&& !datosConsultaReportesRadicacion.getDepartamentoCodigo().isEmpty()) {
			query.append(" AND rre.departamentoCodigo = :departamentoCodigo");
		}
		if (datosConsultaReportesRadicacion.getDiasTranscurridos() != null
				&& !datosConsultaReportesRadicacion.getDiasTranscurridos().isEmpty()) {
			query.append(" AND rred.diasTranscurridos = :diasTranscurridos");
		}
		if (datosConsultaReportesRadicacion.getFechaFin() != null) {
			query.append(" AND rred.fechaFinal BETWEEN :fechaFin1 AND :fechaFin2");
		}
		if (datosConsultaReportesRadicacion.getFechaInicio() != null) {
			query.append(" AND rred.fechaInicial BETWEEN :fechaInicio1 AND :fechaInicio2");
		}
		if (datosConsultaReportesRadicacion.getFormato() != null
				&& !datosConsultaReportesRadicacion.getFormato().isEmpty()) {
			query.append(" AND rre.formato = :formato");
		}
		if (datosConsultaReportesRadicacion.getCodigoReporte() != null
				&& !datosConsultaReportesRadicacion.getCodigoReporte().isEmpty()) {
			query.append(" AND rre.codigoReporte = :codigoReporte");
		}
		if (datosConsultaReportesRadicacion.getFuncionario() != null
				&& !datosConsultaReportesRadicacion.getFuncionario().isEmpty()) {
			query.append(" AND rred.nombreFuncionario = :nombreFuncionario");
		}
		if (datosConsultaReportesRadicacion.getMunicipioCodigo() != null
				&& !datosConsultaReportesRadicacion.getMunicipioCodigo()
							.isEmpty()) {
			query.append(" AND rre.municipioCodigo = :municipioCodigo");
		}
		if (datosConsultaReportesRadicacion.getRol() != null
				&& !datosConsultaReportesRadicacion.getRol().isEmpty()) {
			query.append(" AND rred.rol = :rol");
		}
		if (datosConsultaReportesRadicacion.getSubproceso() != null
				&& !datosConsultaReportesRadicacion.getSubproceso().isEmpty()) {
			query.append(" AND rred.subproceso = :subproceso");
		}
		if (datosConsultaReportesRadicacion.getTerritorialCodigo() != null
				&& !datosConsultaReportesRadicacion.getTerritorialCodigo().isEmpty()) {
			query.append(" AND rre.territorial.codigo = :territorialCodigo");
		}
		if (datosConsultaReportesRadicacion.getTipoReporteId() != null) {
			query.append(" AND rre.repReporte.id = :tipoReporteId");
		}
		if (datosConsultaReportesRadicacion.getUOCCodigo() != null
				&& !datosConsultaReportesRadicacion.getUOCCodigo().isEmpty()) {
			query.append(" AND rre.UOC.codigo = :UOCCodigo");
		}
		
		// Query
		Query q = this.entityManager.createQuery(query.toString());
		
		if (datosConsultaReportesRadicacion.getActividad() != null
				&& !datosConsultaReportesRadicacion.getActividad().isEmpty()) {
			q.setParameter("actividad", datosConsultaReportesRadicacion.getActividad());
		}
		if (datosConsultaReportesRadicacion.getClasificacion() != null
				&& !datosConsultaReportesRadicacion.getClasificacion().isEmpty()) {
			q.setParameter("clasificacion", datosConsultaReportesRadicacion.getClasificacion());
		}
		if (datosConsultaReportesRadicacion.getDepartamentoCodigo() != null
				&& !datosConsultaReportesRadicacion.getDepartamentoCodigo().isEmpty()) {
			q.setParameter("departamentoCodigo", datosConsultaReportesRadicacion.getDepartamentoCodigo());
		}
		if (datosConsultaReportesRadicacion.getDiasTranscurridos() != null
				&& !datosConsultaReportesRadicacion.getDiasTranscurridos().isEmpty()) {
			q.setParameter("diasTranscurridos", datosConsultaReportesRadicacion.getDiasTranscurridos());
		}
		if (datosConsultaReportesRadicacion.getFechaFin() != null) {
			q.setParameter("fechaFin1",datosConsultaReportesRadicacion.getFechaFin());
			Date fechaFin = datosConsultaReportesRadicacion.getFechaFin();
			Calendar c = Calendar.getInstance();
			c.setTime(fechaFin);
			c.add(Calendar.DATE, 1);
			q.setParameter("fechaFin2", c.getTime());
		}
		if (datosConsultaReportesRadicacion.getFechaInicio() != null) {
			q.setParameter("fechaInicio1",datosConsultaReportesRadicacion.getFechaInicio());
			Date fechaInicio = datosConsultaReportesRadicacion.getFechaInicio();
			Calendar c = Calendar.getInstance();
			c.setTime(fechaInicio);
			c.add(Calendar.DATE, 1);
			q.setParameter("fechaInicio2", c.getTime());
		}
		if (datosConsultaReportesRadicacion.getFormato() != null
				&& !datosConsultaReportesRadicacion.getFormato().isEmpty()) {
			q.setParameter("formato", datosConsultaReportesRadicacion.getFormato());
		}
		if (datosConsultaReportesRadicacion.getCodigoReporte() != null
				&& !datosConsultaReportesRadicacion.getCodigoReporte().isEmpty()) {
			q.setParameter("codigoReporte", datosConsultaReportesRadicacion.getCodigoReporte());
		}
		if (datosConsultaReportesRadicacion.getFuncionario() != null
				&& !datosConsultaReportesRadicacion.getFuncionario().isEmpty()) {
			q.setParameter("nombreFuncionario", datosConsultaReportesRadicacion.getFuncionario());
		}
		if (datosConsultaReportesRadicacion.getMunicipioCodigo() != null
				&& !datosConsultaReportesRadicacion.getMunicipioCodigo()
							.isEmpty()) {
			q.setParameter("municipioCodigo", datosConsultaReportesRadicacion.getMunicipioCodigo());
		}
		if (datosConsultaReportesRadicacion.getRol() != null
				&& !datosConsultaReportesRadicacion.getRol().isEmpty()) {
			q.setParameter("rol", datosConsultaReportesRadicacion.getRol());
		}
		if (datosConsultaReportesRadicacion.getSubproceso() != null
				&& !datosConsultaReportesRadicacion.getSubproceso().isEmpty()) {
			q.setParameter("subproceso", datosConsultaReportesRadicacion.getSubproceso());
		}
		if (datosConsultaReportesRadicacion.getTerritorialCodigo() != null
				&& !datosConsultaReportesRadicacion.getTerritorialCodigo().isEmpty()) {
			q.setParameter("territorialCodigo", datosConsultaReportesRadicacion.getTerritorialCodigo());
		}
		if (datosConsultaReportesRadicacion.getTipoReporteId() != null) {
			q.setParameter("tipoReporteId", datosConsultaReportesRadicacion.getTipoReporteId());
		}
		if (datosConsultaReportesRadicacion.getUOCCodigo() != null
				&& !datosConsultaReportesRadicacion.getUOCCodigo().isEmpty()) {
			q.setParameter("UOCCodigo", datosConsultaReportesRadicacion.getUOCCodigo());
		}
		
		try {

			List<RepReporteEjecucion> answer = (ArrayList<RepReporteEjecucion>) q.getResultList();

			if(answer != null && !answer.isEmpty()){
				for (RepReporteEjecucion rep : answer) {
					Hibernate.initialize(rep.getRepReporte());
					Hibernate.initialize(rep.getRepReporteEjecucionDatos());
					Hibernate.initialize(rep.getRepReporteEjecucionDetalles());
					if(rep.getTerritorial() != null){
						Hibernate.initialize(rep.getTerritorial());
					}
					if(rep.getUOC() != null){
						Hibernate.initialize(rep.getUOC());
					}
				}
				return answer;
			}
		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportesRadicacionGenerados: "
					+ "La consulta de reportes de radicación no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		return null;
	}
	
	/**
	 * @see RepReporteEjecucion#buscarReportePorId(Long idReporteConsultar)
	 * @author leidy.gonzalez
	 */
	@Override
	public RepReporteEjecucion buscarReportePorId(Long idReporteConsultar){
		RepReporteEjecucion answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre " +
				"JOIN FETCH rre.repReporte rr " +
				"where rre.id = :idReporteConsultar ";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idReporteConsultar", idReporteConsultar);
            
			answer = (RepReporteEjecucion) query.getSingleResult();
			
			if(answer != null && answer.getRepReporte() != null){
				Hibernate.initialize(answer.getRepReporte());
				Hibernate.initialize(answer.getRepReporte().getId());
				Hibernate.initialize(answer.getRepReporte().getNombre());
				Hibernate.initialize(answer.getRepReporte().getUrlReporteJasper());
				
				if (answer.getTerritorial() != null){
				
					if (!answer.getTerritorial().getCodigo().isEmpty()) {
						Hibernate.initialize(answer.getTerritorial().getCodigo());
					}

					if (!answer.getTerritorial().getNombre().isEmpty()) {
						Hibernate.initialize(answer.getTerritorial().getNombre());
					}
				}
				
				if (answer.getUOC() != null){
					if (!answer.getUOC().getCodigo().isEmpty()) {
						Hibernate.initialize(answer.getUOC().getCodigo());
					}

					if (!answer.getUOC().getNombre().isEmpty()) {
						Hibernate.initialize(answer.getUOC().getNombre());
					}
				}
				
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorId: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		
		return answer;
	}
	/**
	 * @see RepReporteEjecucion#buscarReportePorId(List<Long> idReporteConsultar)
	 * @author felipe.cadena
	 */
	@Override
	public List<RepReporteEjecucion> buscarReportesPorIds(List<Long> idReporteConsultar){
		List<RepReporteEjecucion> answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre " +
				"JOIN FETCH rre.repReporte rr " +
				"where rre.id in :idsReporteConsultar ";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idsReporteConsultar", idReporteConsultar);
            
			answer =  query.getResultList();
            
            for (RepReporteEjecucion repReporteEjecucion : answer) {
                Hibernate.initialize(repReporteEjecucion.getRepReporte());
            }

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorId: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		
		return answer;
	}
        
        /**
	 * @see RepReporteEjecucion#buscarReportePorId(Long idReporteConsultar)
	 * @author leidy.gonzalez
	 */
	@Override
	public List<RepReporteEjecucion> buscarReportesPorId(Long idReporteConsultar){
		List<RepReporteEjecucion> answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre " +
				"where rre.id = :idReporteConsultar ";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idReporteConsultar", idReporteConsultar);
            
			answer = (ArrayList<RepReporteEjecucion>) query.getResultList();
			
			for(RepReporteEjecucion temp : answer){
				Hibernate.initialize(temp.getRepReporte());
				Hibernate.initialize(temp.getRepReporte().getId());
				Hibernate.initialize(temp.getRepReporte().getNombre());
				
				if (temp.getTerritorial() != null){
					
					if (!temp.getTerritorial().getCodigo().isEmpty()) {
						Hibernate.initialize(temp.getTerritorial().getCodigo());
					}

					if (!temp.getTerritorial().getNombre().isEmpty()) {
						Hibernate.initialize(temp.getTerritorial().getNombre());
					}
				}
				
				if (temp.getUOC() != null){
					if (!temp.getUOC().getCodigo().isEmpty()) {
						Hibernate.initialize(temp.getUOC().getCodigo());
					}

					if (!temp.getUOC().getNombre().isEmpty()) {
						Hibernate.initialize(temp.getUOC().getNombre());
					}
				}
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorId: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		
		return answer;
	}
        
        /**
	 * @see RepReporteEjecucion#buscarReportePorId(String idReporteConsultar)
	 * @author leidy.gonzalez
	 */
	@Override
	public List<RepReporteEjecucion> buscarReportesPorCodigo(String idReporteConsultar){
		List<RepReporteEjecucion> answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre " +
				"where rre.codigoReporte like :idReporteConsultar ";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idReporteConsultar", idReporteConsultar);
            
			answer = (ArrayList<RepReporteEjecucion>) query.getResultList();
			
			for(RepReporteEjecucion temp : answer){
				Hibernate.initialize(temp.getRepReporte());
				Hibernate.initialize(temp.getRepReporte().getId());
				Hibernate.initialize(temp.getRepReporte().getNombre());
				Hibernate.initialize(temp.getRepReporte().getUrlReporteJasper());
				Hibernate.initialize(temp.getRepReporteEjecucionDatos());
				Hibernate.initialize(temp.getRepReporteEjecucionDetalles());
				
				if(temp.getTerritorial() != null){
					Hibernate.initialize(temp.getTerritorial());
					
					if(!temp.getTerritorial().getCodigo().isEmpty()){
						Hibernate.initialize(temp.getTerritorial().getCodigo());
					}
					
					if(!temp.getTerritorial().getNombre().isEmpty()){
						Hibernate.initialize(temp.getTerritorial().getNombre());
					}
				}
				if (temp.getUOC() != null){
					if (!temp.getUOC().getCodigo().isEmpty()) {
						Hibernate.initialize(temp.getUOC().getCodigo());
					}

					if (!temp.getUOC().getNombre().isEmpty()) {
						Hibernate.initialize(temp.getUOC().getNombre());
					}
				}
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorId: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		
		return answer;
	}
	
	/**
	 * @see RepReporteEjecucion#buscarReportePorCodigo(String codigoReporte)
	 * @author leidy.gonzalez
	 */
	@Override
	public RepReporteEjecucion buscarReportePorCodigo(String codigoReporte){
		RepReporteEjecucion answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporteEjecucion rre " +
				"JOIN FETCH rre.repReporte rr " +
				"where rre.codigoReporte = :codigoReporte ";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("codigoReporte", codigoReporte);
            
			answer = (RepReporteEjecucion) query.getSingleResult();
			
			if(answer != null && answer.getRepReporte() != null){
				Hibernate.initialize(answer.getRepReporte());
				Hibernate.initialize(answer.getRepReporte().getId());
				Hibernate.initialize(answer.getRepReporte().getNombre());
				Hibernate.initialize(answer.getRepReporte().getUrlReporteJasper());
				Hibernate.initialize(answer.getRepReporteEjecucionDatos());
				Hibernate.initialize(answer.getRepReporteEjecucionDetalles());
				
				if(answer.getTerritorial() != null){
					Hibernate.initialize(answer.getTerritorial());
					
					if (!answer.getTerritorial().getCodigo().isEmpty()) {
						Hibernate.initialize(answer.getTerritorial().getCodigo());
					}

					if (!answer.getTerritorial().getNombre().isEmpty()) {
						Hibernate.initialize(answer.getTerritorial().getNombre());
					}
				}
				
				
				
				if (answer.getUOC() != null){
					if (!answer.getUOC().getCodigo().isEmpty()) {
						Hibernate.initialize(answer.getUOC().getCodigo());
					}

					if (!answer.getUOC().getNombre().isEmpty()) {
						Hibernate.initialize(answer.getUOC().getNombre());
					}
				}
				
			}

		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorCodigo: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		
		return answer;
	}

    /**
    * @see RepReporteEjecucion#buscarReportePorDepartamentoYMunicipio(Long tipoReporte, String codigoDepartamento, String codigoMunicipio)
    * @author leidy.gonzalez
    */
    @Override
    public List<RepReporteEjecucion> buscarReportePorDepartamentoYMunicipio(Long tipoReporte, String codigoDepartamento, String codigoMunicipio){
           List<RepReporteEjecucion> answer = null;

           return answer;
    }

    /**
    * @see RepReporteEjecucion#buscarReportePorFiltrosBasicos(FiltroGenerarReportes datosConsultaPredio,String selectedDepartamentoCod, String territorial, String uoc, RepConsultaPredial configuracionAvanzada)
    * @author leidy.gonzalez
    */
    @Override
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicos(FiltroGenerarReportes datosConsultaPredio,
            String selectedDepartamentoCod, String selectedMunicipioCod, String territorial, String uoc) {

        List<RepReporteEjecucion> answer = null;
        int contadorRegistrosMultiplesAnzada = 0;
        Query query;
        StringBuilder queryToExecute = new StringBuilder();

    
        queryToExecute.append("SELECT rre FROM RepReporteEjecucion rre "
                    + " JOIN FETCH rre.repReporte rr "
                    + " JOIN FETCH rre.repReporteEjecucionDetalles rred"
                    + " WHERE rr.id = :tipoInforme "
                    + " AND rre.formato = :formato "
                    + " AND rre.fechaCorte = :fechaCorte "
                    + " AND rre.vigencia = :anioReporte ");
       

        //Consulta Avanzada-Parametros
        
        if (selectedDepartamentoCod != null && !selectedDepartamentoCod.isEmpty()) {

            queryToExecute.append(" AND rre.departamentoCodigo = :departamento");

        }
        
        if (selectedMunicipioCod != null && !selectedMunicipioCod.isEmpty()) {

             queryToExecute.append(" AND rre.municipioCodigo = :selectedMunicipioCod");

         }

        if (uoc != null && !uoc.isEmpty()) {

            queryToExecute.append(" AND rre.UOC.codigo = :uocCodigo");
            
        }else if (territorial != null && !territorial.isEmpty()){
            
            queryToExecute.append(" AND rre.territorial.codigo = :territorial");
        }

        if (datosConsultaPredio.getNumeroPredial() != null
                && !datosConsultaPredio.getNumeroPredial().isEmpty()) {

            queryToExecute.append(" AND rred.numeroPredialDesde = :numeroPredial");

        }

        if (datosConsultaPredio.getNumeroPredialFinal() != null
                && !datosConsultaPredio.getNumeroPredialFinal().isEmpty()) {

            queryToExecute.append(" AND rred.numeroPredialHasta = :numeroPredialFinal");

        }

        if (datosConsultaPredio.getTipoIdentificacionPropietario() != null
                && !datosConsultaPredio.getTipoIdentificacionPropietario().isEmpty()) {

            queryToExecute.append(" AND rred.tipoIdentificacion = :tipoIdentificacion");

        }

        if (datosConsultaPredio.getIdentificacionPropietario() != null
                && !datosConsultaPredio.getIdentificacionPropietario().isEmpty()) {

            queryToExecute.append(" AND rred.numeroIdentificacion = :numeroIdentificacion");

        }

        if (datosConsultaPredio.getModoAdquisicionId() != null
                && !datosConsultaPredio.getModoAdquisicionId().isEmpty()) {

            queryToExecute.append(" AND rred.modoAdquisicion = :modoAdquisicion");

        }

        if (datosConsultaPredio.getTipoTitulo() != null
                && !datosConsultaPredio.getTipoTitulo().isEmpty()) {

            queryToExecute.append(" AND rred.tipoTitulo = :tipoTitulo");

        }

        if (datosConsultaPredio.getEntidad() != null && !datosConsultaPredio.getEntidad().isEmpty()) {

            queryToExecute.append(" AND rred.entidad = :entidad");
        }

        if (datosConsultaPredio.getDireccion() != null
                && !datosConsultaPredio.getDireccion().isEmpty()) {

            queryToExecute.append(" AND rred.direccion = :direccion");

        }

        if (datosConsultaPredio.getNumeroRegistro() != null
                && !datosConsultaPredio.getNumeroRegistro().isEmpty()) {

            queryToExecute.append(" AND rred.matriculaInmobiliaria = :matriculaInmobiliaria");

        }
        
        if (datosConsultaPredio.getNumeroRegistroFinal() != null
                && !datosConsultaPredio.getNumeroRegistroFinal().isEmpty()) {
            queryToExecute.append(" AND rred.matriculaInmobiliariaHasta = :matriculaInmobiliariaHasta");
        }

        if (datosConsultaPredio.getTipoPredioId() != null
                && !datosConsultaPredio.getTipoPredioId().isEmpty()) {

            queryToExecute.append(" AND rred.predioTipo = :tipoPredio");
        }

        if (datosConsultaPredio.getDestinoPredioId() != null
                && !datosConsultaPredio.getDestinoPredioId().isEmpty()) {

            queryToExecute.append(" AND rred.predioDestino = :destinoEconomico");

        }

        if (datosConsultaPredio.getAreaTerrenoMin() != null) {

            queryToExecute.append(" AND rred.areaTerrenoDesde = :areaTerrenoDesde");
        }
        
        if (datosConsultaPredio.getAreaTerrenoMax() != null) {
            
            queryToExecute.append(" AND rred.areaTerrenoHasta = :areaTerrenoHasta");
        }

        if (datosConsultaPredio.getAreaConstruidaMin() != null) {
            
            queryToExecute.append(" AND rred.areaConstruccionDesde = :areaConstruccionDesde");
                
        }
        if (datosConsultaPredio.getAreaConstruidaMax() != null) {
            
            queryToExecute.append(" AND rred.areaConstruccionHasta = :areaConstruccionHasta");
            
        }

        if (datosConsultaPredio.getAvaluoDesde() != null) {
            
            queryToExecute.append(" AND rred.avaluoDesde = :avaluoDesde");
            
        }
        if (datosConsultaPredio.getAvaluoHasta() != null) {
            
            queryToExecute.append(" AND rred.avaluoHasta = :avaluoHasta");
            
        }

        
        if (datosConsultaPredio.getZonaFisicaId() != null
                && !datosConsultaPredio.getZonaFisicaId().isEmpty()) {

            queryToExecute.append(" AND rred.zonaFisica in (:zonaFisica)");

        }

        if (datosConsultaPredio.getZonaEconomicaId() != null
                && !datosConsultaPredio.getZonaEconomicaId().isEmpty()) {

            queryToExecute.append(" AND rred.zonaGeoeconomica in (:zonaGeoeconomica)");

        }

        if (datosConsultaPredio.getUsoConstruccionId() != null
                && !datosConsultaPredio.getUsoConstruccionId().isEmpty()) {

            queryToExecute.append(" AND rred.construccionUso in (:usoConstruccion)");
        }

        if (datosConsultaPredio.getFechaInicioBloqueo() != null) {

            queryToExecute.append(" AND rred.fechaInicioBloqueo = :fechaInicioBloqueo");

        }

        if (datosConsultaPredio.getFechaDesbloqueo() != null) {

            queryToExecute.append(" AND rred.fechaDesbloqueo = :fechaDesbloqueo");

        }
        
        if (datosConsultaPredio.getTipoPersona() != null 
                && !datosConsultaPredio.getTipoPersona().isEmpty()) {
            
            queryToExecute.append(" AND rred.tipoPersona = :tipoPersona");
        }

		if (datosConsultaPredio.getConsolidado() != null
				&& !datosConsultaPredio.getConsolidado().isEmpty()) {

			queryToExecute.append(" AND rred.consolidado = :consolidado");
		}

		if (datosConsultaPredio.getIncluirUocs() != null
				&& !datosConsultaPredio.getIncluirUocs().isEmpty()) {

			queryToExecute.append(" AND rred.incluirUocs = :incluirUocs");
		}

		if (datosConsultaPredio.getNacional() != null
				&& !datosConsultaPredio.getNacional().isEmpty()) {

			queryToExecute.append(" AND rred.nacional = :nacional");
		}

        try {

            query = this.entityManager.createQuery(queryToExecute.toString());

            query.setParameter("formato", datosConsultaPredio.getFormato());
            query.setParameter("tipoInforme", datosConsultaPredio.getCodigoTipoInforme());
            query.setParameter("fechaCorte", datosConsultaPredio.getTipoDeFechaCorte());
            query.setParameter("anioReporte", datosConsultaPredio.getAnoReporte());


            //Consulta Avanzada-Parametros

            if (selectedDepartamentoCod != null && !selectedDepartamentoCod.isEmpty()) {

                query.setParameter("departamento", selectedDepartamentoCod);

            }

            if (selectedMunicipioCod != null && !selectedMunicipioCod.isEmpty()) {

                query.setParameter("selectedMunicipioCod", selectedMunicipioCod);

            }

            if (uoc != null && !uoc.isEmpty()) {
                query.setParameter("uocCodigo", uoc);
            } else if (territorial != null && !territorial.isEmpty()){
                query.setParameter("territorial", territorial);
            }


            if (datosConsultaPredio.getNumeroPredial() != null
                    && !datosConsultaPredio.getNumeroPredial().isEmpty()) {

                query.setParameter("numeroPredial", datosConsultaPredio.getNumeroPredial());

            }
            
            if (datosConsultaPredio.getNumeroPredialFinal() != null
                    && !datosConsultaPredio.getNumeroPredialFinal().isEmpty()) {

                query.setParameter("numeroPredialFinal", datosConsultaPredio.getNumeroPredialFinal());

            }

            if (datosConsultaPredio.getTipoIdentificacionPropietario() != null
                    && !datosConsultaPredio.getTipoIdentificacionPropietario().isEmpty()) {

                query.setParameter("tipoIdentificacion", datosConsultaPredio.getTipoIdentificacionPropietario());

            }

            if (datosConsultaPredio.getIdentificacionPropietario() != null
                    && !datosConsultaPredio.getIdentificacionPropietario().isEmpty()) {

                query.setParameter("numeroIdentificacion", datosConsultaPredio.getIdentificacionPropietario());

            }

            if (datosConsultaPredio.getModoAdquisicionId() != null
                    && !datosConsultaPredio.getModoAdquisicionId().isEmpty()) {
                
                String modoAdquisicion = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String modoAdquisicionTemp : datosConsultaPredio.getModoAdquisicionId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        modoAdquisicion = modoAdquisicion + "," + "'"
                                + modoAdquisicionTemp + "'";

                    } else {
                        modoAdquisicion = "'" + modoAdquisicionTemp + "'";
                    }
                }

                query.setParameter("modoAdquisicion", modoAdquisicion);

            }

            if (datosConsultaPredio.getTipoTitulo() != null
                    && !datosConsultaPredio.getTipoTitulo().isEmpty()) {
                
                String tipoTitulo = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String tipoTituloTemp : datosConsultaPredio.getTipoTitulo()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        tipoTitulo = tipoTitulo + "," + "'"
                                + tipoTituloTemp + "'";

                    } else {
                        tipoTitulo = "'" + tipoTituloTemp + "'";
                    }
                }

                query.setParameter("tipoTitulo", tipoTitulo);

            }

            if (datosConsultaPredio.getEntidad() != null
                    && !datosConsultaPredio.getEntidad().isEmpty()) {

                query.setParameter("entidad", datosConsultaPredio.getEntidad());

            }

            if (datosConsultaPredio.getDireccion() != null
                    && !datosConsultaPredio.getDireccion().isEmpty()) {

                query.setParameter("direccion", datosConsultaPredio.getDireccion());

            }

            if (datosConsultaPredio.getNumeroRegistro() != null
                    && !datosConsultaPredio.getNumeroRegistro().isEmpty()) {

                query.setParameter("matriculaInmobiliaria", datosConsultaPredio.getNumeroRegistro());

            }
            
            if (datosConsultaPredio.getNumeroRegistroFinal() != null
                    && !datosConsultaPredio.getNumeroRegistroFinal().isEmpty()) {
                query.setParameter("matriculaInmobiliariaHasta", datosConsultaPredio.getNumeroRegistroFinal());
            }

            if (datosConsultaPredio.getTipoPredioId() != null
                    && !datosConsultaPredio.getTipoPredioId().isEmpty()) {

                String tipoPredio = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String tipoPredioTemp : datosConsultaPredio.getTipoPredioId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        tipoPredio = tipoPredio + "," + "'"
                                + tipoPredioTemp + "'";

                    } else {
                        tipoPredio = "'" + tipoPredioTemp + "'";
                    }
                }

                query.setParameter("tipoPredio", tipoPredio);

            }

            if (datosConsultaPredio.getDestinoPredioId() != null
                    && !datosConsultaPredio.getDestinoPredioId().isEmpty()) {
                
                String destinoPredio = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String destinoPredioTemp : datosConsultaPredio.getDestinoPredioId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        destinoPredio = destinoPredio + "," + "'"
                                + destinoPredioTemp + "'";

                    } else {
                        destinoPredio = "'" + destinoPredioTemp + "'";
                    }


                }

                query.setParameter("destinoEconomico", destinoPredio);

            }



            if (datosConsultaPredio.getAreaTerrenoMin() != null) {
                    query.setParameter("areaTerrenoDesde", datosConsultaPredio.getAreaTerrenoMin());
                }
            if (datosConsultaPredio.getAreaTerrenoMax() != null) {
                    query.setParameter("areaTerrenoHasta", datosConsultaPredio.getAreaTerrenoMax());
                }


            if (datosConsultaPredio.getAreaConstruidaMin() != null) {
                    query.setParameter("areaConstruccionDesde", datosConsultaPredio.getAreaConstruidaMin());
                }
            if (datosConsultaPredio.getAreaConstruidaMax() != null) {
                    query.setParameter("areaConstruccionHasta", datosConsultaPredio.getAreaConstruidaMax());
                }


            if (datosConsultaPredio.getAvaluoDesde() != null) {
                query.setParameter("avaluoDesde", datosConsultaPredio.getAvaluoDesde());
            }
            if (datosConsultaPredio.getAvaluoHasta() != null) {
                query.setParameter("avaluoHasta", datosConsultaPredio.getAvaluoHasta());
            }


            if (datosConsultaPredio.getZonaFisicaId() != null
                    && !datosConsultaPredio.getZonaFisicaId().isEmpty()) {
                
                String zonaFisica = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String zonaFisicaTemp : datosConsultaPredio.getZonaFisicaId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        zonaFisica = zonaFisica + "," + "'"
                                + zonaFisicaTemp + "'";

                    } else {
                        zonaFisica = "'" + zonaFisicaTemp + "'";
                    }


                }

                query.setParameter("zonaFisica", zonaFisica);

            }

            if (datosConsultaPredio.getZonaEconomicaId() != null
                    && !datosConsultaPredio.getZonaEconomicaId().isEmpty()) {
                
                String zonaEconomica = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String zonaEconomicaTemp : datosConsultaPredio.getZonaEconomicaId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        zonaEconomica = zonaEconomica + "," + "'"
                                + zonaEconomicaTemp + "'";

                    } else {
                        zonaEconomica = "'" + zonaEconomicaTemp + "'";
                    }


                }

                query.setParameter("zonaGeoeconomica", zonaEconomica);

            }

            if (datosConsultaPredio.getUsoConstruccionId()!= null
                    && !datosConsultaPredio.getUsoConstruccionId().isEmpty()) {
                
                String usoConstruccion = " ";
                contadorRegistrosMultiplesAnzada = 0;


                for (String usoConstruccionTemp : datosConsultaPredio.getUsoConstruccionId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        usoConstruccion = usoConstruccion + "," + "'"
                                + usoConstruccionTemp + "'";

                    } else {
                        usoConstruccion = "'" + usoConstruccionTemp + "'";
                    }


                }

                query.setParameter("usoConstruccion", usoConstruccion);

            }

            if (datosConsultaPredio.getFechaInicioBloqueo() != null) {

                query.setParameter("fechaInicioBloqueo", datosConsultaPredio.getFechaInicioBloqueo());

            }

            if (datosConsultaPredio.getFechaDesbloqueo() != null) {

                query.setParameter("fechaDesbloqueo", datosConsultaPredio.getFechaDesbloqueo());

            }
            
            if (datosConsultaPredio.getTipoPersona() != null
                    && !datosConsultaPredio.getTipoPersona().isEmpty()) {
                
                query.setParameter("tipoPersona", datosConsultaPredio.getTipoPersona());
            }


			if (datosConsultaPredio.getConsolidado() != null
					&& !datosConsultaPredio.getConsolidado().isEmpty()) {

				query.setParameter("consolidado", datosConsultaPredio.getConsolidado());
			}

			if (datosConsultaPredio.getIncluirUocs() != null
					&& !datosConsultaPredio.getIncluirUocs().isEmpty()) {

				query.setParameter("incluirUocs", datosConsultaPredio.getIncluirUocs());
			}

			if (datosConsultaPredio.getNacional() != null
					&& !datosConsultaPredio.getNacional().isEmpty()) {

				query.setParameter("nacional", datosConsultaPredio.getNacional());
			}

            answer = (List<RepReporteEjecucion>) query.getResultList();

            for (RepReporteEjecucion temp : answer) {
                Hibernate.initialize(temp.getRepReporte());
                Hibernate.initialize(temp.getRepReporte().getId());
                Hibernate.initialize(temp.getRepReporte().getNombre());

                if (temp.getTerritorial() != null) {
                    Hibernate.initialize(temp.getTerritorial());

                    if (!temp.getTerritorial().getCodigo().isEmpty()) {
                        Hibernate.initialize(temp.getTerritorial().getCodigo());
                    }

                    if (!temp.getTerritorial().getNombre().isEmpty()) {
                        Hibernate.initialize(temp.getTerritorial().getNombre());
                    }
                }

                if (temp.getUOC() != null) {
                    if (!temp.getUOC().getCodigo().isEmpty()) {
                        Hibernate.initialize(temp.getUOC().getCodigo());
                    }

                    if (!temp.getUOC().getNombre().isEmpty()) {
                        Hibernate.initialize(temp.getUOC().getNombre());
                    }
                }
            }

        } catch (NoResultException nrEx) {
            LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorFiltrosBasicos: "
                    + "La consulta de reporte de ejecucion no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                    LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
    * @see RepReporteEjecucion#buscarReportePorFiltrosBasicos(FiltroGenerarReportes datosConsultaPredio,String selectedDepartamentoCod, String territorial, String uoc, RepConsultaPredial configuracionAvanzada)
    * @author leidy.gonzalez
    */
   @Override
   public List<RepReporteEjecucion> buscarReportePorFiltrosBasicosSinMuncipio(FiltroGenerarReportes datosConsultaPredio,
                   String selectedDepartamentoCod, String territorial, String uoc){
           List<RepReporteEjecucion> answer = null;

           Query query;
           String queryToExecute;

           if(uoc != null && !uoc.isEmpty()){

                   queryToExecute = "SELECT rre FROM RepReporteEjecucion rre "
                                   + "JOIN FETCH rre.repReporte rr "
                                   + "WHERE rr.id = : tipoInforme "
                                   + "AND rre.formato = :formato "
                                   + "AND rre.municipioCodigo is null "
                                   + "AND rre.departamentoCodigo = :departamento "
                                   + "AND rre.UOC.codigo = :uocCodigo "
                                   + "AND rre.fechaCorte = :fechaCorte "
                                   + "AND rre.vigencia = :anioReporte ";
           }else{
                   queryToExecute = "SELECT rre FROM RepReporteEjecucion rre "
                                   + "JOIN FETCH rre.repReporte rr "
                                   + "WHERE rr.id = :tipoInforme"
                                   + " AND rre.formato = :formato "
                                   + "AND rre.municipioCodigo is null "
                                   + " AND rre.departamentoCodigo = :departamento "
                                   + " AND rre.territorial.codigo = :territorial "
                                   + " AND rre.fechaCorte = :fechaCorte "
                                   + " AND rre.vigencia = :anioReporte ";
           }

           try {

       query = this.entityManager.createQuery(queryToExecute);
       query.setParameter("formato", datosConsultaPredio.getFormato());
       query.setParameter("tipoInforme",datosConsultaPredio.getCodigoTipoInforme());
       query.setParameter("departamento",selectedDepartamentoCod);

       if(uoc != null && !uoc.isEmpty()){
           query.setParameter("uocCodigo",uoc);
       }else{
           query.setParameter("territorial",territorial);
       }

       query.setParameter("fechaCorte",datosConsultaPredio.getTipoDeFechaCorte());
       query.setParameter("anioReporte",datosConsultaPredio.getAnoReporte());


      answer = (ArrayList<RepReporteEjecucion>) query.getResultList();


			for (RepReporteEjecucion temp : answer) {
				Hibernate.initialize(temp.getRepReporte());
				Hibernate.initialize(temp.getRepReporte().getId());
				Hibernate.initialize(temp.getRepReporte().getNombre());

				if(temp.getTerritorial() != null){
					Hibernate.initialize(temp.getTerritorial());
					
					if (!temp.getTerritorial().getCodigo().isEmpty()) {
						Hibernate.initialize(temp.getTerritorial().getCodigo());
					}

					if (!temp.getTerritorial().getNombre().isEmpty()) {
						Hibernate.initialize(temp.getTerritorial().getNombre());
					}
				}
				
				
				
				if (temp.getUOC() != null){
					if (!temp.getUOC().getCodigo().isEmpty()) {
						Hibernate.initialize(temp.getUOC().getCodigo());
					}

					if (!temp.getUOC().getNombre().isEmpty()) {
						Hibernate.initialize(temp.getUOC().getNombre());
					}
				}
           }

           } catch (NoResultException nrEx) {
                   LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorFiltrosBasicos: "
                                   + "La consulta de reporte de ejecucion no devolvió resultados");
           } catch (Exception e) {
                   throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                                   LOGGER, e, e.getMessage());
           }

           return answer;
   }

}
