/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.Componente;
import javax.ejb.Local;

/**
 * interface para el DAO de Componente
 *
 * @author felipe.cadena
 */
@Local
public interface IComponenteDAO extends IGenericJpaDAO<Componente, Long> {

    public Componente obtenerComponentePorNombre(String nombre);

}
