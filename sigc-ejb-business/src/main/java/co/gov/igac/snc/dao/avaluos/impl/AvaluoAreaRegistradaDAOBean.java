package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoAreaRegistradaDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoObservacionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAreaRegistrada;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoObservacionDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoAreaRegistradaDAOBean extends GenericDAOWithJPA<AvaluoAreaRegistrada, Long>
    implements IAvaluoAreaRegistradaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoAreaRegistradaDAOBean.class);

    /**
     * @see IAvaluoAreaRegistradaDAO#buscarPorAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public List<AvaluoAreaRegistrada> buscarPorAvaluo(Long idAvaluo) {

        List<AvaluoAreaRegistrada> resultado = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT aar " +
                " FROM AvaluoAreaRegistrada aar " +
                " WHERE aar.avaluo.id = :avaluo ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluo", idAvaluo);
            resultado = query.getResultList();

            for (AvaluoAreaRegistrada avaluoAreaRegistrada : resultado) {
                Hibernate.initialize(avaluoAreaRegistrada.getAvaluoPredioConstruccion());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

    /**
     * @see IAvaluoAreaRegistradaDAO#buscarPorAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public AvaluoAreaRegistrada buscarPorIdConAtributos(Long id) {

        AvaluoAreaRegistrada resultado = null;

        try {

            resultado = this.findById(id);
            Hibernate.initialize(resultado);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

}
