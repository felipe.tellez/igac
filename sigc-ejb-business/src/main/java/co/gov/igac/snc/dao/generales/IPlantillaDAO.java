package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;

/**
 * Declaración de los servicios de persistencia de las entidades Plantilla.
 *
 * @author jamir.avila.
 *
 */
@Local
public interface IPlantillaDAO extends IGenericJpaDAO<Plantilla, Long> {

    /**
     * Recupera el objeto Plantilla que corresponda con el código especificado.
     *
     * @param codigo código de la plantilla a recuperar.
     * @return el objeto Plantilla correspondiente o null en caso de que no haya ninguna
     * correspondencia.
     * @throws ExcepcionSNC en caso de que se presente una situacion anormal.
     */
    public Plantilla recuperarPorCodigo(String codigo) throws ExcepcionSNC;
}
