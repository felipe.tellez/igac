package co.gov.igac.snc.dao.tramite.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IProductoCatastralErrorDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralError;

/**
 * Implementación de los servicios de persistencia del objeto ProductoCatastralError.
 *
 * @author javier.aponte
 */
@Stateless
public class ProductoCatastralErrorDAOBean extends GenericDAOWithJPA<ProductoCatastralError, Long>
    implements IProductoCatastralErrorDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProductoCatastralErrorDAOBean.class);

}
