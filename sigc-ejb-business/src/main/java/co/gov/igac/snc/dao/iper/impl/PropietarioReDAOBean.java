/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.iper.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.iper.IPropietarioReDAO;
import co.gov.igac.snc.persistence.entity.i11n.PropietarioRe;

/**
 * Implementación de los métodos de bd de la tabla COLINDANTE
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Stateless
public class PropietarioReDAOBean extends GenericDAOWithJPA<PropietarioRe, Long>
    implements IPropietarioReDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropietarioReDAOBean.class);

//--------------------------------------------------------------------------------------------------
//end of class
}
