/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos sobre la tabla TRAMITE_PREDIO_ENGLOBE que relaciona
 * trámites con predios (para los casos de englobe)
 *
 * @author pedro.garcia
 * @deprecated Porque la tabla se llama TRAMITE_DETALLE_PREDIO. Usar el entity y las clases
 * asociadas TramiteDetallePredio
 */
@Deprecated
@Local
public interface ITramitePredioEnglobeDAO extends IGenericJpaDAO<TramitePredioEnglobe, Long> {

    /**
     * retorna los registros relacionados con un trámite
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<TramitePredioEnglobe> getByTramiteId(Long idTramite);

    /**
     *
     * retorna los numeros prediales relacionados con uno o varios trámites
     *
     * @author lorena.salamanca
     * @param tramiteIds
     * @return
     */
    public List<String> getNumerosPredialesByTramiteId(List<Long> tramiteIds);

}
