/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.io.FileUtils;
import org.hibernate.collection.internal.PersistentBag;
import org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.interfaces.IGestionDocumentalService;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteMovilesDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioInconsistencia;
import co.gov.igac.snc.persistence.entity.conservacion.PredioServidumbre;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.ReferenciaCartografica;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.SingleValueConverter;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.CollectionConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 *
 */
@Stateless
public class TramiteMovilesDAOBean extends GenericDAOWithJPA<Tramite, Long>
    implements ITramiteMovilesDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteMovilesDAOBean.class);

    @EJB
    private IDocumentoDAO remoteDocumentoDao;

    @EJB
    private IComisionTramiteDAO remoteComisionDAO;

    @EJB
    private ISolicitanteSolicitudDAO remoteSolicitanteDAO;

    @EJB
    private ITramiteDAO remoteTramiteDAO;

    @EJB
    private IPredioDAO remotePredioDAO;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    public void setRemoteComisionDAO(IComisionTramiteDAO remoteComisionDAO) {
        this.remoteComisionDAO = remoteComisionDAO;
    }

    public void setRemotePredioDAO(IPredioDAO remotePredioDAO) {
        this.remotePredioDAO = remotePredioDAO;
    }

    /*
     * (non-Javadoc)
     *
     * @see co.gov.igac.snc.dao.tramite.ITramiteMovilesDAO#obtenerFichaPredialCompleta
     * (java.lang.Long)
     */
    @Override
    public String obtenerFichaPredialCompleta(Long idTramite) {
        String rutaArchivoZip = null;
        try {
            Tramite tramite = findById(idTramite);
            List<String> codigosPredios = new ArrayList<String>();

            if (tramite.getPredio() != null) {
                codigosPredios.add(tramite.getPredio().getNumeroPredial());
            }
            if (tramite.getPredios() != null) {
                for (Predio predio : tramite.getPredios()) {
                    codigosPredios.add(predio.getNumeroPredial());
                }
            }

            //@modified by juanfelipe.garcia :: 25-11-2013 :: se dejo comentariado por cambio en el gestor documental
            //                                                  validar con la nueva versión y ajustar a lo requerido
//			IDocumentosService servicio = DocumentalServiceFactory.getService();
//			List<String> idsGestorDocumental = new ArrayList<String>();
//			for (String numeroPredial : codigosPredios) {
//				idsGestorDocumental.add(servicio.urlFichaPredial(numeroPredial,
//						FileUtils.getTempDirectoryPath()));
//			}
            // TODO: CON LOS IDS DEL GESTOR DOCUMENTAL , RECUPERAR LOS PDFS DE
            // LAS FICHAS (TANTO LAS ESCANEADAS COMO LAS DIGITALES
            // TODO: Generar el documento final con el siguiente método
            // IDocumentosService ( servicio.convertirYAdicionarMarcaDeAgua )
            UsuarioDTO usuario = new UsuarioDTO();
            IDocumentosService service = DocumentalServiceFactory.getService();
            // service.convertirYAdicionarMarcaDeAgua(usuario, arg1, arg2);

            //TODO para generar el archivo zip utilizar la clase co.gov.igac.snc.dao.util.ZipUtil 
            //    
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return rutaArchivoZip;
    }

    /**
     * (non-Javadoc)
     *
     * @see co.gov.igac.snc.dao.tramite.ITramiteMovilesDAO#getXmlTramiteByTramiteId(java.lang.Long)
     */
    public String getXmlTramiteByTramiteId(Long idTramite) {
        String rutaArchivo = null;
        try {
            String[] propertiesToInitialize = {"comisionTramites", "solicitanteTramites"};
            Tramite tramite = findById(idTramite, propertiesToInitialize);
            for (SolicitanteTramite sol : tramite.getSolicitanteTramites()) {
                sol.getTramite().getId();
            }

            //Nota: Utilizan XStream para convertir directamente la clase java en xml.
            //		(con todo el código que hicieron tal vez hubiera sido más práctico y rápido
            //		crear el xml manualmente con dom4j
            XStream xstream = new XStream(new DomDriver("UTF-8"));

            // Crear alias para los objetos y campos.
            xstream.alias("tramite", Tramite.class);
            xstream.aliasType("list", PersistentBag.class);
            xstream.aliasType("solicitante", SolicitanteTramite.class);
            xstream.aliasType("comision", ComisionTramite.class);
            xstream.aliasSystemAttribute(null, "class");
            xstream.aliasSystemAttribute(null, "reference");

            // Colecciones implicitas de la clase.
            xstream.addImplicitCollection(Tramite.class, "comisionTramites");
            xstream.addImplicitCollection(Tramite.class, "solicitanteTramites");

            // Aca se limpian las persistence bag para que no salgan los
            // atributos en el xml
            xstream.registerConverter(new CollectionConverter(xstream.getMapper()) {
                public boolean canConvert(Class clazz) {
                    return PersistentBag.class == clazz;
                }
            });

            xstream.registerConverter(new Converter() {
                @Override
                public boolean canConvert(Class clazz) {
                    return clazz.equals(Tramite.class);
                }

                @Override
                public void marshal(Object value, HierarchicalStreamWriter writer,
                    MarshallingContext context) {
                    Tramite tramite = (Tramite) value;
                    List<SolicitanteTramite> sols = tramite.getSolicitanteTramites();

                    // Campo id Tramite
                    writer.startNode("id");
                    if (tramite.getId() != null) {
                        writer.setValue(String.valueOf(tramite.getId()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo clase mutación
                    writer.startNode("claseMutacion");
                    if (tramite.getClaseMutacion() != null) {
                        writer.setValue(String.valueOf(tramite.getClaseMutacion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo clasificación
                    writer.startNode("clasificacion");
                    if (tramite.getClasificacion() != null) {
                        writer.setValue(String.valueOf(tramite.getClasificacion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo fechaRadicacion
                    writer.startNode("fechaRadicacion");
                    if (tramite.getFechaRadicacion() != null) {
                        writer.setValue(String.valueOf(tramite.getFechaRadicacion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo fuente
                    writer.startNode("fuente");
                    if (tramite.getFuente() != null) {
                        writer.setValue(String.valueOf(tramite.getFuente()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo numeroRadicacion
                    writer.startNode("numeroRadicacion");
                    if (tramite.getNumeroRadicacion() != null) {
                        writer.setValue(String.valueOf(tramite.getNumeroRadicacion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo observacionesRadicacion
                    writer.startNode("observacionesRadicacion");
                    if (tramite.getObservacionesRadicacion() != null) {
                        writer.setValue(String.valueOf(tramite.getObservacionesRadicacion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo subtipo
                    writer.startNode("subtipo");
                    if (tramite.getSubtipo() != null) {
                        writer.setValue(String.valueOf(tramite.getSubtipo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo tipoTramite
                    writer.startNode("tipoTramite");
                    if (tramite.getTipoTramite() != null) {
                        writer.setValue(String.valueOf(tramite.getTipoTramite()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo omitidoNuevo
                    writer.startNode("omitidoNuevo");
                    if (tramite.getOmitidoNuevo() != null) {
                        writer.setValue(String.valueOf(tramite.getOmitidoNuevo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo areaConstruccion
                    writer.startNode("areaConstruccion");
                    if (tramite.getAreaConstruccion() != null) {
                        writer.setValue(String.valueOf(tramite.getAreaConstruccion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo areaTerreno
                    writer.startNode("areaTerreno");
                    if (tramite.getAreaTerreno() != null) {
                        writer.setValue(String.valueOf(tramite.getAreaTerreno()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo numeroPredial
                    writer.startNode("numeroPredial");
                    if (tramite.getNumeroPredial() != null) {
                        writer.setValue(String.valueOf(tramite.getNumeroPredial()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    for (SolicitanteTramite sol : sols) {
                        writer.startNode("solicitante");
                        // Campo id Solicitante
                        writer.startNode("id");
                        if (sol.getId() != null) {
                            writer.setValue(String.valueOf(sol.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tramite
                        writer.startNode("tramite");
                        if (sol.getTramite().getId() != null) {
                            writer.setValue(String.valueOf(sol.getTramite()
                                .getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipoPersona
                        writer.startNode("tipoPersona");
                        if (sol.getTipoPersona() != null) {
                            writer.setValue(String.valueOf(sol.getTipoPersona()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipoIdentificacion
                        writer.startNode("tipoIdentificacion");
                        if (sol.getTipoIdentificacion() != null) {
                            writer.setValue(String.valueOf(sol
                                .getTipoIdentificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo numeroIdentificacion
                        writer.startNode("numeroIdentificacion");
                        if (sol.getNumeroIdentificacion() != null) {
                            writer.setValue(String.valueOf(sol
                                .getNumeroIdentificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo primerNombre
                        writer.startNode("primerNombre");
                        if (sol.getPrimerNombre() != null) {
                            writer.setValue(String.valueOf(sol.getPrimerNombre()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo segundoNombre
                        writer.startNode("segundoNombre");
                        if (sol.getSegundoNombre() != null) {
                            writer.setValue(String.valueOf(sol.getSegundoNombre()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo primerApellido
                        writer.startNode("primerApellido");
                        if (sol.getPrimerApellido() != null) {
                            writer.setValue(String.valueOf(sol.getPrimerApellido()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo segundoApellido
                        writer.startNode("segundoApellido");
                        if (sol.getSegundoApellido() != null) {
                            writer.setValue(String.valueOf(sol.getSegundoApellido()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo razonSocial
                        writer.startNode("razonSocial");
                        if (sol.getRazonSocial() != null) {
                            writer.setValue(String.valueOf(sol.getRazonSocial()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo sigla
                        writer.startNode("sigla");
                        if (sol.getSigla() != null) {
                            writer.setValue(String.valueOf(sol.getSigla()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo direccion
                        writer.startNode("direccion");
                        if (sol.getDireccion() != null) {
                            writer.setValue(String.valueOf(sol.getDireccion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo telefonoPrincipal
                        writer.startNode("telefonoPrincipal");
                        if (sol.getTelefonoPrincipal() != null) {
                            writer.setValue(String.valueOf(sol.getTelefonoPrincipal()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo telefonoCelular
                        writer.startNode("telefonoCelular");
                        if (sol.getTelefonoCelular() != null) {
                            writer.setValue(String.valueOf(sol.getTelefonoCelular()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();
                        writer.endNode();
                    }

                }

                @Override
                public Object unmarshal(HierarchicalStreamReader arg0,
                    UnmarshallingContext arg1) {
                    return null;
                }
            });

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            xstream.toXML(tramite);
            String xml = outputStream.toString("UTF-8");
            LOGGER.debug(xml);
            File outputFile = File.createTempFile("Tramite_", ".xml");
            FileOutputStream xmlFile = new FileOutputStream(outputFile);
            xstream.toXML(tramite, xmlFile);
            rutaArchivo = outputFile.getAbsolutePath();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return rutaArchivo;
    }

    /**
     *
     * Función que crea un .xml con la información de un predio
     */
    public String getXmlPredioByPredioId(Long idPredio) {
        String rutaArchivo = null;
        try {

            Predio predio = this.remotePredioDAO.getPredioFetchDatosUbicacionParaMovilesById(
                idPredio);
            predio.getPredioDireccions().size();
            predio.getPredioInconsistencias().size();
            predio.getPersonaPredios().size();
            predio.getPredioServidumbres().size();
            predio.getUnidadConstruccions().size();
            predio.getPredioZonas().size();
            predio.getReferenciaCartograficas().size();

            XStream xstream = new XStream(new DomDriver("UTF-8"));

            // Crear alias para los objetos y campos.
            xstream.alias("predio", Predio.class);
            xstream.aliasType("list", PersistentBag.class);
            xstream.aliasType("inconsistencis", PredioInconsistencia.class);
            xstream.aliasType("direccion", PredioDireccion.class);
            xstream.aliasType("circuloRegistral", CirculoRegistral.class);
            xstream.aliasType("servidumbre", PredioServidumbre.class);
            xstream.aliasType("unidadConstruccion", UnidadConstruccion.class);
            xstream.aliasType("referenciaCartografica",
                ReferenciaCartografica.class);
            xstream.aliasType("unidadConstruccionComponentes",
                UnidadConstruccionComp.class);
            xstream.aliasType("zona", PredioZona.class);
            xstream.aliasType("personasPredio", PersonaPredio.class);
            xstream.aliasType("personasPredioPropiedad",
                PersonaPredioPropiedad.class);
            xstream.aliasSystemAttribute(null, "class");

            // Colecciones implicitas de la clase Predio
            xstream.addImplicitCollection(Predio.class, "predioDireccions");
            xstream.addImplicitCollection(Predio.class,
                "referenciaCartograficas");
            xstream.addImplicitCollection(Predio.class, "predioInconsistencias");
            xstream.addImplicitCollection(Predio.class, "personaPredios");
            xstream.addImplicitCollection(Predio.class, "predioServidumbres");
            xstream.addImplicitCollection(Predio.class, "unidadConstruccions");
            xstream.addImplicitCollection(Predio.class, "predioZonas");
            xstream.addImplicitCollection(PersonaPredio.class,
                "personaPredioPropiedads");
            xstream.addImplicitCollection(Persona.class, "personaPredios");
            xstream.addImplicitCollection(Persona.class, "personaBloqueos");
            xstream.addImplicitCollection(UnidadConstruccion.class,
                "unidadConstruccionComps");

            // Aca se limpian las persistence bag para que no salgan los
            // atributos en el xml
            xstream.registerConverter(new CollectionConverter(xstream
                .getMapper()) {
                public boolean canConvert(Class clazz) {
                    return PersistentBag.class == clazz;
                }

                public boolean canConvertJS(Class clazz) {
                    return JavassistLazyInitializer.class == clazz;
                }

            });

            xstream.registerConverter(new Converter() {
                public boolean canConvert(Class clazz) {
                    return clazz.equals(Predio.class);
                }

                @Override
                public void marshal(Object value,
                    HierarchicalStreamWriter writer,
                    MarshallingContext context) {

                    Predio predio = (Predio) value;
                    List<PredioDireccion> dirs = predio.getPredioDireccions();
                    List<ReferenciaCartografica> refs = predio.getReferenciaCartograficas();
                    List<PredioInconsistencia> incons = predio.getPredioInconsistencias();
                    List<PersonaPredio> personas = predio.getPersonaPredios();
                    List<PredioServidumbre> servis = predio.getPredioServidumbres();
                    List<PredioZona> zonas = predio.getPredioZonas();
                    List<UnidadConstruccion> unidades = predio.getUnidadConstruccions();

                    // Campo id Predio
                    writer.startNode("id");
                    if (predio.getId() != null) {
                        writer.setValue(String.valueOf(predio.getId()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo tipoAvaluo
                    writer.startNode("tipoAvaluo");
                    if (predio.getTipoAvaluo() != null) {
                        writer.setValue(String.valueOf(predio.getTipoAvaluo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo sectorCodigo
                    writer.startNode("sectorCodigo");
                    if (predio.getSectorCodigo() != null) {
                        writer.setValue(String.valueOf(predio.getSectorCodigo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo barrioCodigo
                    writer.startNode("barrioCodigo");
                    if (predio.getBarrioCodigo() != null) {
                        writer.setValue(String.valueOf(predio.getBarrioCodigo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo manzanaCodigo
                    writer.startNode("manzanaCodigo");
                    if (predio.getManzanaCodigo() != null) {
                        writer.setValue(String.valueOf(predio.getManzanaCodigo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo numeroPredial
                    writer.startNode("numeroPredial");
                    if (predio.getNumeroPredial() != null) {
                        writer.setValue(String.valueOf(predio.getNumeroPredial()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo numeroPredialAnterior
                    writer.startNode("numeroPredialAnterior");
                    if (predio.getNumeroPredialAnterior() != null) {
                        writer.setValue(String.valueOf(predio.getNumeroPredialAnterior()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo destino
                    writer.startNode("destino");
                    if (predio.getDestino() != null) {
                        writer.setValue(String.valueOf(predio.getDestino()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo tipo
                    writer.startNode("tipo");
                    if (predio.getTipo() != null) {
                        writer.setValue(String.valueOf(predio.getTipo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo areaTerreno
                    writer.startNode("areaTerreno");
                    if (predio.getAreaTerreno() != null) {
                        writer.setValue(String.valueOf(predio.getAreaTerreno()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo codigo del circulo registral
                    writer.startNode("circuloRegistral");
                    if (predio.getCirculoRegistral().getCodigo() != null) {
                        writer.setValue(String.valueOf(predio.getCirculoRegistral().getCodigo()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo numeroRegistro
                    writer.startNode("numeroRegistro");
                    if (predio.getNumeroRegistro() != null) {
                        writer.setValue(String.valueOf(predio.getNumeroRegistro()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo numeroRegistroAnterior
                    writer.startNode("numeroRegistroAnterior");
                    if (predio.getNumeroRegistro() != null) {
                        writer.setValue(String.valueOf(predio.getNumeroRegistroAnterior()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo tipoCatastro
                    writer.startNode("tipoCatastro");
                    if (predio.getTipoCatastro() != null) {
                        writer.setValue(String.valueOf(predio.getTipoCatastro()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo estado
                    writer.startNode("estado");
                    if (predio.getEstado() != null) {
                        writer.setValue(String.valueOf(predio.getEstado()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo estrato
                    writer.startNode("estrato");
                    if (predio.getEstrato() != null) {
                        writer.setValue(String.valueOf(predio.getEstrato()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo chip
                    writer.startNode("chip");
                    if (predio.getChip() != null) {
                        writer.setValue(String.valueOf(predio.getChip()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo areaConstruccion
                    writer.startNode("areaConstruccion");
                    if (predio.getAreaConstruccion() != null) {
                        writer.setValue(String.valueOf(predio.getAreaConstruccion()));
                    } else {
                        writer.setValue("");
                    }
                    writer.endNode();

                    // Campo inscripcionCatastral
                    writer.startNode("inscripcionCatastral");
                    writer.setValue("01/01/1900");
                    writer.endNode();

                    //Direcciones del predio
                    for (PredioDireccion dir : dirs) {
                        writer.startNode("direccion");

                        // Campo id Direccion
                        writer.startNode("id");
                        if (dir.getId() != null) {
                            writer.setValue(String.valueOf(dir.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (dir.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo direccion
                        writer.startNode("dir");
                        if (dir.getDireccion() != null) {
                            writer.setValue(String.valueOf(dir.getDireccion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo principal
                        writer.startNode("principal");
                        if (dir.getPrincipal() != null) {
                            writer.setValue(String.valueOf(dir.getPrincipal()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        writer.endNode();
                    }

                    //Referencias cartograficas
                    for (ReferenciaCartografica refCarto : refs) {
                        writer.startNode("referenciaCartografica");

                        // Campo id
                        writer.startNode("id");
                        if (refCarto.getId() != null) {
                            writer.setValue(String.valueOf(refCarto.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (predio.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo plancha
                        writer.startNode("plancha");
                        if (refCarto.getPlancha() != null) {
                            writer.setValue(String.valueOf(refCarto.getPlancha()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo vuelo
                        writer.startNode("vuelo");
                        if (refCarto.getVuelo() != null) {
                            writer.setValue(String.valueOf(refCarto.getVuelo()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo foto
                        writer.startNode("foto");
                        if (refCarto.getFoto() != null) {
                            writer.setValue(String.valueOf(refCarto.getFoto()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        writer.endNode();
                    }
                    //Inconsistencas
                    for (PredioInconsistencia inc : incons) {
                        writer.startNode("inconsistencia");

                        // Campo id
                        writer.startNode("id");
                        if (inc.getId() != null) {
                            writer.setValue(String.valueOf(inc.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (predio.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipo
                        writer.startNode("tipo");
                        if (inc.getTipo() != null) {
                            writer.setValue(String.valueOf(inc.getTipo()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo estado
                        writer.startNode("estado");
                        if (inc.getEstado() != null) {
                            writer.setValue(String.valueOf(inc.getEstado()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        writer.endNode();
                    }
                    //Personas
                    for (PersonaPredio pers : personas) {

                        Persona p = pers.getPersona();
                        List<PersonaPredioPropiedad> ppp = pers.getPersonaPredioPropiedads();

                        writer.startNode("persona");

                        // Campo id
                        writer.startNode("id");
                        if (p.getId() != null) {
                            writer.setValue(String.valueOf(p.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (predio.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipoPersona
                        writer.startNode("tipoPersona");
                        if (p.getTipoPersona() != null) {
                            writer.setValue(String.valueOf(p.getTipoPersona()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipoIdentificacion
                        writer.startNode("tipoIdentificacion");
                        if (p.getTipoIdentificacion() != null) {
                            writer.setValue(String.valueOf(p.getTipoIdentificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo numeroIdentificacion
                        writer.startNode("numeroIdentificacion");
                        if (p.getNumeroIdentificacion() != null) {
                            writer.setValue(String.valueOf(p.getNumeroIdentificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo digito de verificacion
                        writer.startNode("digitoVerificacion");
                        if (p.getDigitoVerificacion() != null) {
                            writer.setValue(String.valueOf(p.getDigitoVerificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo  Primer nombre
                        writer.startNode("primerNombre");
                        if (p.getPrimerNombre() != null) {
                            writer.setValue(String.valueOf(p.getPrimerNombre()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo segundo nombre
                        writer.startNode("segundoNombre");
                        if (p.getSegundoNombre() != null) {
                            writer.setValue(String.valueOf(p.getSegundoNombre()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo primer apellido
                        writer.startNode("primerApellido");
                        if (p.getPrimerApellido() != null) {
                            writer.setValue(String.valueOf(p.getPrimerApellido()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo segundo apellido
                        writer.startNode("segundoApellido");
                        if (p.getSegundoApellido() != null) {
                            writer.setValue(String.valueOf(p.getSegundoApellido()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo razonSocial
                        writer.startNode("razonSocial");
                        if (p.getRazonSocial() != null) {
                            writer.setValue(String.valueOf(p.getRazonSocial()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo sigla
                        writer.startNode("sigla");
                        if (p.getSigla() != null) {
                            writer.setValue(String.valueOf(p.getSigla()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipo
                        writer.startNode("tipo");
                        if (pers.getTipo() != null) {
                            writer.setValue(String.valueOf(pers.getTipo()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo participacion
                        writer.startNode("participacion");
                        if (pers.getParticipacion() != null) {
                            writer.setValue(String.valueOf(pers.getParticipacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        for (PersonaPredioPropiedad propiedad : ppp) {

                            // Campo Entidad emisora
                            writer.startNode("entidadEmisora");
                            if (propiedad.getEntidadEmisora() != null) {
                                writer.setValue(String.valueOf(propiedad.getEntidadEmisora()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo Departamento
                            writer.startNode("departamentoCodigo");
                            if (propiedad.getDepartamento() != null) {
                                writer.setValue(String.valueOf(propiedad.getDepartamento()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo Municipio
                            writer.startNode("municipioCodigo");
                            if (propiedad.getMunicipio() != null) {
                                writer.setValue(String.valueOf(propiedad.getMunicipio()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo Fecha de Registro
                            writer.startNode("fechaRegistro");
                            if (propiedad.getFechaRegistro() != null) {
                                writer.setValue(String.valueOf(propiedad.getFechaRegistro()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo Número de Registro
                            writer.startNode("numeroRegistro");
                            if (propiedad.getNumeroRegistro() != null) {
                                writer.setValue(String.valueOf(propiedad.getNumeroRegistro()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo Modo de Adquisición
                            writer.startNode("modoAdquisicion");
                            if (propiedad.getModoAdquisicion() != null) {
                                writer.setValue(String.valueOf(propiedad.getModoAdquisicion()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo valor de Adquisición
                            writer.startNode("valor");
                            if (propiedad.getValor() != null) {
                                writer.setValue(String.valueOf(propiedad.getValor()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo tipo de título
                            writer.startNode("tipoTitulo");
                            if (propiedad.getTipoTitulo() != null) {
                                writer.setValue(String.valueOf(propiedad.getTipoTitulo()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo número de título
                            writer.startNode("numeroTitulo");
                            if (propiedad.getNumeroTitulo() != null) {
                                writer.setValue(String.valueOf(propiedad.getNumeroTitulo()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo fecha de título
                            writer.startNode("fechaTitulo");
                            if (propiedad.getFechaTitulo() != null) {
                                writer.setValue(String.valueOf(propiedad.getFechaTitulo()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                        }

                        writer.endNode();

                    }
                    //Servidumbre
                    for (PredioServidumbre ser : servis) {
                        writer.startNode("servidumbre");

                        // Campo id
                        writer.startNode("id");
                        if (ser.getId() != null) {
                            writer.setValue(String.valueOf(ser.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (predio.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo servidumbre
                        writer.startNode("serv");
                        if (ser.getServidumbre() != null) {
                            writer.setValue(String.valueOf(ser.getServidumbre()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        writer.endNode();
                    }
                    //Zonas
                    for (PredioZona zona : zonas) {
                        writer.startNode("zona");

                        // Campo id
                        writer.startNode("id");
                        if (zona.getId() != null) {
                            writer.setValue(String.valueOf(zona.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (predio.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo zona física
                        writer.startNode("zonaFisica");
                        if (zona.getZonaFisica() != null) {
                            writer.setValue(String.valueOf(zona.getZonaFisica()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo zona geoeconomica
                        writer.startNode("zonaEconomica");
                        if (zona.getZonaGeoeconomica() != null) {
                            writer.setValue(String.valueOf(zona.getZonaGeoeconomica()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo area zona
                        writer.startNode("area");
                        if (zona.getArea() != null) {
                            writer.setValue(String.valueOf(zona.getArea()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        writer.endNode();
                    }
                    //Unidades
                    for (UnidadConstruccion unidad : unidades) {
                        writer.startNode("unidadConstruccion");

                        List<UnidadConstruccionComp> ucc = unidad.getUnidadConstruccionComps();

                        // Campo id
                        writer.startNode("id");
                        if (unidad.getId() != null) {
                            writer.setValue(String.valueOf(unidad.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo idPredio
                        writer.startNode("idPredio");
                        if (predio.getId() != null) {
                            writer.setValue(String.valueOf(predio.getId()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo unidad
                        writer.startNode("unidad");
                        if (unidad.getUnidad() != null) {
                            writer.setValue(String.valueOf(unidad.getUnidad()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipoCalificacion
                        writer.startNode("tipoCalificacion");
                        if (unidad.getTipoCalificacion() != null) {
                            writer.setValue(String.valueOf(unidad.getTipoCalificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Total habitaciones
                        writer.startNode("totalHabitaciones");
                        if (unidad.getTotalHabitaciones() != null) {
                            writer.setValue(String.valueOf(unidad.getTotalHabitaciones()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Total Baños
                        writer.startNode("totalBanios");
                        if (unidad.getTotalBanios() != null) {
                            writer.setValue(String.valueOf(unidad.getTotalBanios()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Total Locales
                        writer.startNode("totalLocales");
                        if (unidad.getTotalLocales() != null) {
                            writer.setValue(String.valueOf(unidad.getTotalLocales()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Piso de ubicación
                        writer.startNode("pisoUbicacion");
                        if (unidad.getPisoUbicacion() != null) {
                            writer.setValue(String.valueOf(unidad.getPisoUbicacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Pisos de la construcción
                        writer.startNode("totalPisosConstruccion");
                        if (unidad.getTotalPisosConstruccion() != null) {
                            writer.setValue(String.valueOf(unidad.getTotalPisosConstruccion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Pisos de la unidad
                        writer.startNode("totalPisosUnidad");
                        if (unidad.getTotalPisosUnidad() != null) {
                            writer.setValue(String.valueOf(unidad.getTotalPisosUnidad()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo uso
                        writer.startNode("usoConstruccion");
                        if (unidad.getUsoConstruccion() != null) {
                            writer.setValue(String.valueOf(unidad.getUsoConstruccion().getNombre()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo Total de Puntos
                        writer.startNode("totalPuntaje");
                        if (unidad.getTotalPuntaje() != null) {
                            writer.setValue(String.valueOf(unidad.getTotalPuntaje()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo AreaConstruida
                        writer.startNode("areaConstruida");
                        if (unidad.getAreaConstruida() != null) {
                            writer.setValue(String.valueOf(unidad.getAreaConstruida()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipificacion
                        writer.startNode("tipificacion");
                        if (unidad.getTipificacion() != null) {
                            writer.setValue(String.valueOf(unidad.getTipificacion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo tipo dominio
                        writer.startNode("tipoDominio");
                        if (unidad.getTipoDominio() != null) {
                            writer.setValue(String.valueOf(unidad.getTipoDominio()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        // Campo año de la construcción
                        writer.startNode("anioConstruccion");
                        if (unidad.getAnioConstruccion() != null) {
                            writer.setValue(String.valueOf(unidad.getAnioConstruccion()));
                        } else {
                            writer.setValue("");
                        }
                        writer.endNode();

                        for (UnidadConstruccionComp u : ucc) {
                            writer.startNode("UnidadConstruccionComp");

                            // Campo id
                            writer.startNode("id");
                            if (unidad.getId() != null) {
                                writer.setValue(String.valueOf(u.getId()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo idPredio
                            writer.startNode("idPredio");
                            if (predio.getId() != null) {
                                writer.setValue(String.valueOf(predio.getId()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo unidad
                            writer.startNode("idUnidad");
                            if (unidad.getId() != null) {
                                writer.setValue(String.valueOf(unidad.getId()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo Componente
                            writer.startNode("componente");
                            if (u.getComponente() != null) {
                                writer.setValue(String.valueOf(u.getComponente()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo elemento calificacion
                            writer.startNode("elementoCalificacion");
                            if (u.getElementoCalificacion() != null) {
                                writer.setValue(String.valueOf(u.getElementoCalificacion()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo detalle calificacion
                            writer.startNode("detalleCalificacion");
                            if (u.getDetalleCalificacion() != null) {
                                writer.setValue(String.valueOf(u.getDetalleCalificacion()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            // Campo puntos
                            writer.startNode("puntos");
                            if (u.getPuntos() != null) {
                                writer.setValue(String.valueOf(u.getPuntos()));
                            } else {
                                writer.setValue("");
                            }
                            writer.endNode();

                            writer.endNode();

                        }

                        writer.endNode();
                    }

                }

                @Override
                public Object unmarshal(HierarchicalStreamReader arg0,
                    UnmarshallingContext arg1) {
                    // TODO Auto-generated method stub
                    return null;
                }
            });

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            xstream.toXML(predio);
            String xml = outputStream.toString("UTF-8");
            LOGGER.debug(xml);

            File outputFile = File.createTempFile("Predio_", ".xml");
            FileOutputStream xmlFile = new FileOutputStream(outputFile);
            xstream.toXML(predio, xmlFile);
            rutaArchivo = outputFile.getAbsolutePath();
            LOGGER.debug("Ruta archivo con info de predio(s): " + rutaArchivo);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return rutaArchivo;
    }

    /**
     *
     * Esta funcion devuelve en un .xml el extent del predio o predios y un .png con la ortofoto de
     * los mismos.
     *
     * @author lorena.salamanca
     * @param UsuarioDTO, ArcgisServerTokenDTO, String
     */
    public String generarOrtofotoDeTramite(UsuarioDTO usuario, ArcgisServerTokenDTO token,
        String identificadoresPredios) {
        /*
         * //TODO este método debe ser refactorizado.. en teoría debería retornar dos cosas: //	el
         * xml con el extent y la imagen... en el momento no está retornando nada String rutaArchivo
         * = null; try { String extentPredios =
         * SigDAO.getInstance(logMensajeDAO).obtenerExtentDePredios(identificadoresPredios, token,
         * usuario); if(extentPredios != null && extentPredios != ""){ XStream xstream = new
         * XStream(new DomDriver("UTF-8")); FileOutputStream xmlFile; File outputFile =
         * File.createTempFile("ExtentPredios_",".xml"); xmlFile = new FileOutputStream(outputFile);
         * xstream.toXML(extentPredios, xmlFile); LOGGER.debug("Extent de predios: " +
         * extentPredios); String rutaArchivoExtent = outputFile.getAbsolutePath();
         * LOGGER.debug("Ruta archivo con extent de predios: " + rutaArchivoExtent); //rutaArchivo =
         * SigDAO.getInstance(logMensajeDAO).obtenerOrtofotoDeExtent(extentPredios, token, usuario);
         * LOGGER.debug("Ruta archivo con ortofoto de predios: " + rutaArchivo); } } catch
         * (FileNotFoundException e) { throw
         * SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage()); }
         * catch (IOException e) { throw
         * SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage()); }
         * return rutaArchivo;
         *
         */
        throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
            "Servicio No Implementado. La invocación debe realizarse de forma asincrónica");
    }

    // end of class
}
