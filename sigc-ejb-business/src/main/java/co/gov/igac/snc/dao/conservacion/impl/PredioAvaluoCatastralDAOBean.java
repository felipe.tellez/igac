/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioAvaluoCatastralDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;

/**
 *
 * @author juan.agudelo
 * @version 2.0
 */
@Stateless
public class PredioAvaluoCatastralDAOBean extends
    GenericDAOWithJPA<PredioAvaluoCatastral, Long> implements
    IPredioAvaluoCatastralDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioAvaluoCatastralDAOBean.class);

    // -------------------------------------------------------------------------
    /**
     * @see IPredioAvaluoCatastralDAO#findValorAvaluoByNumeroPredial(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Double findValorAvaluoByNumeroPredial(String numeroPredial) {

        LOGGER.debug("executing PredioDAOBean#findValorAvaluoByNumeroPredial");

        Query query;
        String q;

        q = "SELECT pac.valorTotalAvaluoCatastral " +
            " FROM PredioAvaluoCatastral pac" + " JOIN pac.predio p" +
            " WHERE p.numeroPredial = :numeroPredial" +
            " ORDER BY pac.vigencia DESC";

        query = entityManager.createQuery(q);
        query.setParameter("numeroPredial", numeroPredial);
        query.setMaxResults(1);
        try {

            Double answer = (Double) query.getSingleResult();
            return answer;

        } catch (NoResultException ne) {
            return -1.0;
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioAvaluoCatastralDAO#obtenerHistoricoPorPredio(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<PredioAvaluoCatastral> obtenerHistoricoPorPredio(Long predioId) {

        List<PredioAvaluoCatastral> answer = null;
        Query query;
        String queryString;

        queryString = "" +
            "SELECT pac FROM PredioAvaluoCatastral pac WHERE pac.predio.id = :predioId_p";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("predioId_p", predioId);

        try {
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioAvaluoCatastralDAOBean#obtenerHistoricoPorPredio");
        }

        return answer;
    }

// end of class
}
