/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class ComisionTramiteDAOBean extends GenericDAOWithJPA<ComisionTramite, Long>
    implements IComisionTramiteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionTramiteDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IComisionTramiteDAO#deleteRow(java.lang.Long, java.lang.Long)
     */
    @Implement
    @Override
    public void deleteRow(Long tramiteId, Long comisionId) {

        LOGGER.debug("ComisionTramiteDAOBean#deleteRow");
        Query query;

        try {
            query = this.entityManager.createNamedQuery("deleteByTramiteAndComision");

            query.setParameter("tramiteIdParam", tramiteId);
            query.setParameter("comisionIdParam", comisionId);

            query.executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en ComisionTramiteDAOBean#deleteRow: " +
                ex.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#deleteMultipleCT(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    @Deprecated
    public void deleteMultipleCT(List<Tramite> tramites) {

        LOGGER.debug("on ComisionTramiteDAOBean#deleteMultipleCT ...");

        Long idTramite = 0l;
        int deleted = 0;
        List<ComisionTramite> comisionTramiteRelations;

        try {
            for (Tramite t : tramites) {
                comisionTramiteRelations = t.getComisionTramites();

                //N: se puede hacer esto porque no debería suceder que un mismo trámite esté en
                //   varias comisiones
                this.deleteMultiple(comisionTramiteRelations);
            }
        } catch (Exception e) {
            LOGGER.error("... error borrando comision_tramite con id trámite = " + idTramite +
                " :" + e.getMessage());
        }

        LOGGER.debug("... finished. Deleted " + deleted + " rows");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#getComisionDuration(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Double getComisionDuration(Long idComision) {

        LOGGER.debug("on ComisionTramiteDAOBean#getComisionDuration ...");

        Double answer = 0.0;
        BigDecimal tempAnswer;
        String queryString;
        Query query;
        Object obj;

        //N: jpql
        queryString = "SELECT MAX(ct.duracion) " +
            " FROM ComisionTramite ct WHERE ct.comision.id = :idComisionP";
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idComisionP", idComision);
            obj = query.getSingleResult();
            tempAnswer = (obj != null) ? (BigDecimal) obj : new BigDecimal(0.0);
            answer = Double.valueOf(tempAnswer.doubleValue());

        } catch (Exception e) {
            LOGGER.error("... error obteniendo duración comisión :" + e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#getComisionDuration2
     * @author pedro.garcia Este aplica la nueva regla para hallar la duración de la comisión
     * (definida en mayo de 2012)
     *
     * duracion = max(sum(dt,e1,c), sum(dt,e2,c), ..., sum(dt,en,c))
     *
     * donde sum(dt,ex,c) es la sumatoria de las duraciones de los trámites (dt) del ejecutor x (ex)
     * que están comisionados en la comisión c
     *
     * @param idComision
     * @return
     */
    @Implement
    @Override
    public Double getComisionDuration2(Long idComision) throws ExcepcionSNC {

        Double answer = 0.0;
        BigDecimal tempAnswer;
        String queryString;
        Query query;
        Object obj;

        //N: jpql
        queryString = "SELECT MAX(duration) " +
            " FROM( " +
            " SELECT SUM(ct.duracion) AS duration, t.funcionario_ejecutor " +
            " FROM comision_tramite ct, tramite t " +
            " WHERE ct.comision_id = :idComisionP AND ct.tramite_id = t.id " +
            " GROUP BY t.funcionario_ejecutor)";

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.setParameter("idComisionP", idComision);
            obj = query.getSingleResult();
            tempAnswer = (obj != null) ? (BigDecimal) obj : new BigDecimal(0.0);
            answer = Double.valueOf(tempAnswer.doubleValue());
        } catch (Exception e) {
            LOGGER.error("... error obteniendo duración comisión :" + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "ComisionTramiteDAOBean#getComisionDuration");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#buscarComisionesTramitePorIdTramite(java.lang.Long)
     */
    /*
     * @modified pedro.garcia - documentación. - Paso intermedio de asignación de variable resultado
     * - fetch de la comisión
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ComisionTramite> buscarComisionesTramitePorIdTramite(
        Long idTramite) {

        String errorMsg;
        List<ComisionTramite> answer;
        Query query = this.entityManager
            .createQuery("SELECT ct" +
                " FROM ComisionTramite ct" +
                " JOIN FETCH ct.tramite t" +
                " JOIN FETCH ct.comision c" +
                " WHERE t.id = :idTramite");
        query.setParameter("idTramite", idTramite);
        try {
            answer = query.getResultList();
            return answer;
        } catch (Exception e) {
            errorMsg = "Error buscando comisiones trámite por id de trámite";
            LOGGER.error(errorMsg + idTramite);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ComisionTramiteDAOBean#buscarComisionesTramitePorIdTramite");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#deleteRowsByComisionId(java.lang.Long)
     * @author pedro.garcia
     * @version 2.0
     */
    @Implement
    @Override
    public void deleteRowsByComisionId(Long idComision) {

        String queryString, errorMsg;
        Query query;

        queryString = "DELETE FROM ComisionTramite ct WHERE ct.comision.id = :idComisionP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idComisionP", idComision);

            query.executeUpdate();
        } catch (Exception ex) {
            errorMsg = "Error borrando relaciones ComisionTramite de la comisión ";
            LOGGER.error(errorMsg + idComision);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionTramiteDAOBean#deleteRowsByComisionId");
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IComisionTramiteDAO#buscarComisionesTramitePorIdTramite(java.lang.Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ComisionTramite> findComisionesTramiteByComisionId(
        Long comisionId) {

        List<ComisionTramite> answer;
        Query query = this.entityManager.createQuery("SELECT ct" +
            " FROM ComisionTramite ct" +
            " JOIN FETCH ct.tramite t" +
            " JOIN FETCH ct.comision c" +
            " WHERE c.id = :comisionId");

        query.setParameter("comisionId", comisionId);
        try {
            answer = query.getResultList();
        } catch (IndexOutOfBoundsException e) {
            answer = new ArrayList<ComisionTramite>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#contarComisionesEfectivasDeTramite(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarComisionesEfectivasDeTramite(Long tramiteId, String tipoComision) {

        int answer = 0;
        Query query;
        String queryString;
        Long howMany;

        queryString = "" +
            "SELECT COUNT(c.id) " +
            "FROM ComisionTramite ct LEFT JOIN ct.comision c " +
            "WHERE c.estado = '" + EComisionEstado.FINALIZADA.getCodigo() + "' " +
            "AND c.tipo = :tipoComision_p " +
            "AND ct.tramite.id = :tramiteId_p";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("tipoComision_p", tipoComision);
        query.setParameter("tramiteId_p", tramiteId.longValue());

        try {
            howMany = (Long) query.getSingleResult();
            if (howMany != null) {
                answer = howMany.intValue();
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionTramiteDAOBean#contarComisionesEfectivasDeTramite");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#consultarComisionesEfectivasDeTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<ComisionTramite> consultarComisionesEfectivasDeTramite(Long idTramite) {

        List<ComisionTramite> answer = null;
        Query query;
        String queryString;

        queryString = "" +
            "SELECT ct " +
            "FROM ComisionTramite ct LEFT JOIN FETCH ct.comision c " +
            "LEFT JOIN FETCH ct.tramite tram LEFT JOIN FETCH tram.solicitud " +
            "WHERE c.estado = '" + EComisionEstado.FINALIZADA.getCodigo() + "' " +
            "AND ct.tramite.id = :tramiteId_p";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("tramiteId_p", idTramite.longValue());

        try {
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionTramiteDAOBean#consultarComisionesEfectivasDeTramite");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IComisionTramiteDAO#deleteByTramiteIdsAndComisionId(java.util.List, java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void deleteByTramiteIdsAndComisionId(List<Long> idsTramites, Long idComision)
        throws ExcepcionSNC {

        String queryString;
        Query query;
        int deleted = 0;

        queryString = "" +
            "DELETE FROM ComisionTramite ct " +
            "WHERE ct.tramite.id IN(:idsTram_p) " +
            "AND ct.comision.id = :idComision_p ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idsTram_p", idsTramites);
            query.setParameter("idComision_p", idComision);

            deleted = query.executeUpdate();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                getExcepcion(LOGGER, e, "ComisionTramiteDAOBeandeleteByTramiteIdsAndComisionId",
                    "ComisionTramite con tramites id", Arrays.toString(idsTramites.toArray()));
        }
        LOGGER.debug("... finished. Deleted " + deleted + " rows");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionTramiteDAO#buscarPorIdComision(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<ComisionTramite> buscarPorIdComision(Long idComision) {

        List<ComisionTramite> answer = null;
        Query query;
        String queryString;

        queryString = "" +
            "SELECT ct " +
            "FROM ComisionTramite ct LEFT JOIN FETCH ct.comision c " +
            "WHERE c.id = :comisionId_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("comisionId_p", idComision.longValue());
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionTramiteDAOBean#buscarPorIdComision");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IComisionTramiteDAO#buscarComisionesTramiteNoRealizadasPorIdTramite(java.lang.Long)
     * @author javier.aponte
     */
    @Override
    public List<ComisionTramite> buscarComisionesTramiteNoRealizadasPorIdTramite(
        Long idTramite) {

        String errorMsg;
        List<ComisionTramite> answer;
        Query query = this.entityManager
            .createQuery("SELECT ct" +
                " FROM ComisionTramite ct" +
                " JOIN FETCH ct.tramite t" +
                " JOIN FETCH ct.comision c" +
                " WHERE t.id = :idTramite" +
                " AND ct.realizada = :realizada");
        query.setParameter("idTramite", idTramite);
        query.setParameter("realizada", ESiNo.NO.getCodigo());
        try {
            answer = query.getResultList();
            return answer;
        } catch (Exception e) {
            errorMsg = "Error buscando comisiones trámite por id de trámite";
            LOGGER.error(errorMsg + idTramite);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ComisionTramiteDAOBean#buscarComisionesTramitePorIdTramite");
        }

    }

//end of class    
}
