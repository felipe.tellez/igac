/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IDetalleCapturaOfertaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class DetalleCapturaOfertaDAOBean extends
    GenericDAOWithJPA<DetalleCapturaOferta, Long> implements
    IDetalleCapturaOfertaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DetalleCapturaOfertaDAOBean.class);

    /**
     * @author ariel.ortiz
     *
     */
    @Override
    public List<String> cargarDetallesAreasPorRecolector(String idRecolector, Long idRegion) {

        List<String> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT dco.manzanaVeredaCodigo" +
            " FROM DetalleCapturaOferta dco, RegionCapturaOferta rco" +
            " WHERE dco.regionCapturaOferta.id = rco.id" +
            " AND dco.regionCapturaOferta.id = :idRegion" +
            " AND rco.asignadoRecolectorId = :idRecolector";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", idRecolector);
            query.setParameter("idRegion", idRegion);

            answer = (List<String>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    @Override
    public List<String> cargarManzanasPorRegionesAsociadas(
        List<Long> regionCapturaOfertaIdList) {

        List<String> answer = null;
        List<String> returnAnswer = null;

        for (Long id : regionCapturaOfertaIdList) {

            String queryString;
            Query query;

            queryString = "SELECT dco.manzanaVeredaCodigo" +
                " FROM DetalleCapturaOferta dco" +
                " WHERE dco.regionCapturaOferta.id = :idRegion";

            LOGGER.debug(queryString);

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("idRegion", id);

                answer = (List<String>) query.getResultList();

            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010
                    .getExcepcion(LOGGER, e, e.getMessage());
            }

            if (!answer.isEmpty()) {
                returnAnswer.addAll(answer);
            }
        }
        return returnAnswer;

    }

    @Override
    public List<DetalleCapturaOferta> cargarDetallesCapturaOfertaPorArea(Long idArea) {
        List<DetalleCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT dco" +
            " FROM DetalleCapturaOferta dco " +
            " WHERE dco.areaCapturaOferta.id = :idArea";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idArea", idArea);

            answer = (List<DetalleCapturaOferta>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

}
