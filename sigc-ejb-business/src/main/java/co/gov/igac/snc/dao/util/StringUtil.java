package co.gov.igac.snc.dao.util;

import java.text.Normalizer;

public class StringUtil {

    /**
     * getting rid of accents and converting them to regular letters
     * http://stackoverflow.com/questions/3322152/java-getting-rid-of-accents-and-converting-them-to-regular-letters
     *
     * @param string
     * @return
     */
    public static String flattenToAscii(String string) {
        StringBuilder sb = new StringBuilder(string.length());
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        for (char c : string.toCharArray()) {
            if (c <= '\u007F') {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
