package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;

/**
 * Dao para acceder a la entidad LogMensaje
 *
 * @author juan.mendez
 *
 */
@Local
public interface ILogMensajeDAO extends IGenericJpaDAO<LogMensaje, Long> {

}
