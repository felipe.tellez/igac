package co.gov.igac.snc.dao.util;

import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.EZonaCierreVigencia;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Dao para querys nativos del sistema
 *
 * @author juan.mendez
 *
 */
public class QueryNativoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(QueryNativoDAO.class);

    /**
     * Normaliza una dirección
     *
     * @author juan.mendez
     */
    public static String normalizarDireccion(EntityManager entityManager, String direccion) {
        String direccionNormalizada = null;
        try {
            direccion = direccion.toLowerCase()
                .replace("á", "a")
                .replace("é", "e")
                .replace("í", "i")
                .replace("ó", "o")
                .replace("ú", "u")
                .replace("ñ", "n");
            String sqlQuery = "select " + EProcedimientoAlmacenadoFuncion.FN_NORMALIZAR +
                "('D','C','00','000', '" +
                direccion + "') FROM DUAL";
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            direccionNormalizada = (String) result;
            LOGGER.debug("direccion:" + direccion);
            LOGGER.debug("Resultado Normalizado:" + direccionNormalizada);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return direccionNormalizada;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna una fecha contando el número de días hábiles a partir de una fecha inicial
     *
     * @author juan.mendez
     * @param entityManager
     * @param fechaInicial
     * @param numeroDias
     * @return
     */
    public static Date calcularFechaConDiasHorasHabiles(EntityManager entityManager,
        Date fechaInicial,
        int numeroDias) {
        Date fechaSiguiente = null;
        try {
            String convertDateOracle = "  to_date('" +
                (fechaInicial.getYear() + 1900) + "/" +
                (fechaInicial.getMonth() + 1) + "/" +
                (fechaInicial.getDate()) + " " +
                (fechaInicial.getHours()) + ":" +
                (fechaInicial.getMinutes()) + ":" +
                (fechaInicial.getSeconds()) +
                "', 'yyyy/mm/dd HH24:MI:SS') ";
            //select FNC_SUMAR_DIAS_HABILES (to_date('2011/11/01', 'yyyy/mm/dd'),15) from dual 
            //select  to_char( FNC_SUMAR_DIAS_HABILES (to_date('2011/11/01 8:00:00', 'yyyy/mm/dd HH24:MI:SS'),10)  
            //,'yyyy/mm/dd HH:MI:SS'  ) 		from dual 

            String sqlQuery = "SELECT " + EProcedimientoAlmacenadoFuncion.FNC_SUMAR_DIAS_HABILES +
                "( " + convertDateOracle + "," + numeroDias + " ) FROM DUAL";
            //LOGGER.debug("sqlQuery:" + sqlQuery);
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            fechaSiguiente = (Date) result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return fechaSiguiente;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Calcula el número de dias habiles entre dos fechas dadas
     *
     * @author felipe.cadena
     *
     * @param entityManager
     * @param fechaDesde
     * @param fechaHasta
     * @return
     */
    public static int calcularDiasHabilesEntreDosFechas(EntityManager entityManager, Date fechaDesde,
        Date fechaHasta) {
        BigDecimal numeroDias;
        try {
            String fechaDesdeOracle = "  to_date('" +
                (fechaDesde.getYear() + 1900) + "/" +
                (fechaDesde.getMonth() + 1) + "/" +
                (fechaDesde.getDate()) + " " +
                "', 'yyyy/mm/dd') ";
            String fechaHastaOracle = "  to_date('" +
                (fechaHasta.getYear() + 1900) + "/" +
                (fechaHasta.getMonth() + 1) + "/" +
                (fechaHasta.getDate()) + " " +
                "', 'yyyy/mm/dd') ";

            String sqlQuery = "SELECT " + EProcedimientoAlmacenadoFuncion.FNC_OBTENER_DIAS_HABILES +
                "( " + fechaDesdeOracle + "," + fechaHastaOracle + ",'0' ) FROM DUAL";
            LOGGER.debug("sqlQuery:" + sqlQuery);
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            numeroDias = (BigDecimal) result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return numeroDias.intValue();
    }

    /**
     * Determina si una fecha dad corresponde a un dia habil.
     *
     * @author felipe.cadena
     *
     * @param entityManager
     * @param fecha
     * @return
     */
    public static boolean esDiaHabil(EntityManager entityManager, Date fecha) {

        String resultado;
        try {
            String fechaOracle = "  to_date('" +
                (fecha.getYear() + 1900) + "/" +
                (fecha.getMonth() + 1) + "/" +
                (fecha.getDate()) + " " +
                "', 'yyyy/mm/dd') ";

            String sqlQuery = "SELECT " + EProcedimientoAlmacenadoFuncion.FNC_ES_DIA_HABIL +
                "( " + fechaOracle + ",'0' ) FROM DUAL";
            LOGGER.debug("sqlQuery:" + sqlQuery);
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            resultado = (String) result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        if (resultado.equals(ESiNo.NO.getCodigo())) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @param entityManager
     * @param numeroPredial20
     * @return
     */
    public static String convertirNumeroPredialDe20A30(EntityManager entityManager,
        String numeroPredial20) {
        String numeroPredial30 = null;
        try {
            String sqlQuery = "select " +
                EProcedimientoAlmacenadoFuncion.FNC_CONVERTIR_PREDIAL20_A_30 + "('" +
                numeroPredial20 + "') FROM DUAL";
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            numeroPredial30 = (String) result;
            LOGGER.debug("numeroPredial20:" + numeroPredial20);
            LOGGER.debug("numeroPredial30:" + numeroPredial30);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return numeroPredial30;
    }

    /**
     * Retorna el ultimo año de actualizaciòn de una zona
     *
     * @author felipe.cadena
     *
     * @param entityManager
     * @param zona -- numero predial hasta zona
     * @return
     */
    public static Integer anioUltimaActualizacion(EntityManager entityManager, String zona) {

        BigDecimal resultado;
        try {
            String zonaFinal = zona.substring(0, 5);
            String codigoZona = zona.substring(5, 7);

            if (!codigoZona.equals(EZonaCierreVigencia.RURAL.getCodigo()) &&
                !codigoZona.equals(EZonaCierreVigencia.URBANA.getCodigo())) {
                codigoZona = EZonaCierreVigencia.CORREGIMIENTOS.getCodigo();
            }

            zonaFinal = zonaFinal.concat(codigoZona);

            String sqlQuery = "SELECT " +
                EProcedimientoAlmacenadoFuncion.FNC_ANIOULTIMA_ACTUALIZACION +
                "('" + zonaFinal + "') FROM DUAL";
            LOGGER.debug("sqlQuery:" + sqlQuery);
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            resultado = (BigDecimal) result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        if (resultado == null) {
            return null;
        } else {
            return resultado.intValue();
        }

    }

    /**
     * Retorna el porcentaje de incremento del avaluo de un predio para una vigencia
     *
     * @author felipe.cadena
     *
     * @param entityManager
     * @param vigencia
     * @param predioId
     * @return
     */
    public static Double obtenerPorcentajeIncremento(EntityManager entityManager, Date vigencia,
        Long predioId) {

        BigDecimal resultado;
        try {

            String fechaOracle = "  to_date('" +
                (vigencia.getYear() + 1900) + "/" +
                (vigencia.getMonth() + 1) + "/" +
                (vigencia.getDate()) + " " +
                "', 'yyyy/mm/dd') ";

            BigDecimal predio = BigDecimal.valueOf(predioId);
            String sqlQuery = "SELECT " +
                EProcedimientoAlmacenadoFuncion.CALCULARPCTINCREMENTOPREDIO +
                "(" + fechaOracle + ", '" + predio + "') FROM DUAL";
            LOGGER.debug("sqlQuery:" + sqlQuery);
            Query query = entityManager.createNativeQuery(sqlQuery);
            Object result = query.getSingleResult();
            resultado = (BigDecimal) result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        if (resultado == null) {
            return null;
        } else {
            return resultado.doubleValue();
        }

    }

    /**
     * Retorna la fecha de creacion del predio en el SNC
     *
     * @author leidy.gonzalez
     */
    public static Date consultarFechaCreacionPredio(EntityManager entityManager, Long pPredioId) {

        Date fecha = new Date();

        try {
            BigDecimal predio = BigDecimal.valueOf(pPredioId);

            String sqlQuery = "SELECT " +
                EProcedimientoAlmacenadoFuncion.FECHA_INSCRIPCION_FICHA_MATRIZ +
                "('" + predio + "') FROM DUAL";

            LOGGER.debug("sqlQuery:" + sqlQuery);
            Query query = entityManager.createNativeQuery(sqlQuery);

            Object result = query.getSingleResult();
            fecha = (Date) result;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        if (fecha == null) {
            return null;
        } else {
            return fecha;
        }
    }

}
