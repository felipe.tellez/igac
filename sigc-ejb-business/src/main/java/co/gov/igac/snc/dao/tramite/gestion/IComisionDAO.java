/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion;

import java.util.List;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de una Comisión
 *
 * @author pedro.garcia
 */
@Local
public interface IComisionDAO extends IGenericJpaDAO<Comision, Long> {

    /**
     * Actualiza los datos de una comisión. Se recibe un VComision, pero hay que guardarlo como
     * Comision
     *
     * @author pedro.garcia
     * @param loggedInUser
     * @param selectedComision
     */
    public void updateAsVComision(UsuarioDTO loggedInUser, VComision selectedComision);

    /**
     * Actualiza los datos de una comisión
     *
     * @author juan.agudelo
     * @param loggedInUser
     * @param comision
     */
    public Comision updateComision(UsuarioDTO loggedInUser, Comision comision);

    /**
     * Método que busca las comisiones que tengan un estado igual al parametro estado, tal que los
     * trámites que pertenecen a esa comisión sean para un predio que este en la territorial del
     * parametro que se le pasa
     *
     * @author javier.aponte
     * @param idTerritorial
     * @param estado
     * @return
     */
    public List<Comision> findComisionesPorIdTerritorialAndEstadoComision(
        String territorialId, String estadoComision);

    /**
     * Método que retorna la lista de ejecutores asociados a una comision
     *
     * @param idComision Id de la comisión asociada a los ejecutores
     * @param territorialId Id de la territorial asociada al funcionario del sistema
     * @param estadoComision estado de la comisión
     * @return
     */
    public List<String> findFuncionariosEjecutoresByComision(Long idComision,
        String territorialId, String estadoComision);

    /**
     * Metodo que busca la primer comision para usarla en los test y que no quede quemada
     *
     * @author fredy.wilches
     * @return Comision
     */
    public Comision getComisionParaTests();

    /**
     * Método que busca las comisiones que sean del tipo y estado dados, tal que los trámites que
     * pertenecen a esa comisión sean para un predio que esté en la territorial dada
     *
     * Basado en el método findComisionesPorIdTerritorialAndEstadoComision
     *
     * @author pedro.garcia
     *
     * @param idTerritorial
     * @param tipoComision
     * @param estado
     * @return
     */
    public List<Comision> findComisionesPorTerritorialYTipoYEstado(
        String territorialId, String tipoComision, String estadoComision);

    /**
     * Actualiza los datos en la tabla
     *
     * @author pedro.garcia
     *
     * @param usuarioActual
     * @param comision
     * @return true si todo resulta bien
     */
    public boolean updateComision2(UsuarioDTO usuarioActual, Comision comision);

    /**
     * Consulta las comisiones activas (estados: por ejecutar, en ejecución, aplazada, suspendida)
     * del tipo dado en las que está involucrado un trámite
     *
     * @param idTramite
     * @param tipoComision si es null, no se hace distinción del tipo de comisión
     * @return
     */
    public List<Comision> consultarActivasPorTramite(Long idTramite, String tipoComision);

//end of interface    
}
