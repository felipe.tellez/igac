package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoLiquidacionCostoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoLiquidacionCosto;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Clase que contiene los queries sobre la tabla AVALUO_LIQUIDACION_COSTO en SNC_AVALUOS
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class AvaluoLiquidacionCostoDAOBean extends
    GenericDAOWithJPA<AvaluoLiquidacionCosto, Long> implements
    IAvaluoLiquidacionCostoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoDAOBean.class);

    /**
     * @see IAvaluoLiquidacionCostoDAO#calcularValorEjecutadoAvaluosAprobadosPorContratoId(Long)
     * boolean)
     *
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public Double calcularValorEjecutadoAvaluosAprobadosPorContratoId(
        Long idContrato) {

        LOGGER.debug("Inicio AvaluoLiquidacionCostoDAOBean#" +
            "calcularValorEjecutadoAvaluosAprobadosPorContratoId");
        Double result = 0D;
        String queryString = "";
        Query query;

        queryString = "SELECT SUM (alc.totalHonorariosIgac)" +
            " FROM" +
            " AvaluoLiquidacionCosto alc," +
            " Avaluo ava," +
            " AvaluoAsignacionProfesional aap," +
            " ProfesionalAvaluosContrato pac," +
            " Tramite tra" +
            " WHERE" +
            " alc.avaluo.id = ava.id" +
            " AND aap.avaluo.id = ava.id" +
            " AND ava.aprobado = '" + ESiNo.SI.getCodigo() + "'" +
            " AND aap.profesionalAvaluoContratoId = pac.id" +
            " AND pac.id = :idContrato" +
            " AND ava.tramite.id = tra.id";
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);

            result = Double
                .parseDouble(String.valueOf(query.getSingleResult()));

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER,
                    e,
                    "AvaluoLiquidacionCostoDAOBean#" +
                    "calcularValorEjecutadoAvaluosAprobadosPorContratoId");
        }

        LOGGER.debug("Fin AvaluoLiquidacionCostoDAOBean#" +
            "calcularValorEjecutadoAvaluosAprobadosPorContratoId");

        return result;
    }

}
