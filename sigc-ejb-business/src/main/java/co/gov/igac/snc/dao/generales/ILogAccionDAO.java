/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import javax.ejb.Local;

/**
 * Dao para acceder a la entidad LogAcceso
 *
 * @author felipe.cadena
 */
@Local
public interface ILogAccionDAO extends IGenericJpaDAO<LogAccion, Long> {

}
