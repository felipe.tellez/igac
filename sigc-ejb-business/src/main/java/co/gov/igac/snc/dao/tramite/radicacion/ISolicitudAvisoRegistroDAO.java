/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudAvisoRegistro;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de una SolicitudAvisoRegistro
 *
 * @author pedro.garcia
 */
@Local
public interface ISolicitudAvisoRegistroDAO extends IGenericJpaDAO<SolicitudAvisoRegistro, Long> {

}
