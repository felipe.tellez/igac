package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IVProfesionalAvaluosCargaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosCarga;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase que contiene los queries sobre la vista v_profesional_avaluos_carga en SNC_AVALUOS
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class VProfesionalAvaluosCargaDAOBean extends
    GenericDAOWithJPA<VProfesionalAvaluosCarga, Long> implements
    IVProfesionalAvaluosCargaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfesionalAvaluo.class);

    /**
     * @see IProfesionalAvaluoDAO#buscarProfesionalesPorNombre(String)
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VProfesionalAvaluosCarga> buscarCargaAvaluadoresPorNombreYterritorial(
        String nombreProfesional, String codigoTerritorial) {
        LOGGER.debug(
            "on VProfesionalAvaluosCargaDAOBean#buscarCargaAvaluadoresPorNombreYterritorial ");
        List<VProfesionalAvaluosCarga> resultado = null;

        String queryString;
        Query query;

        queryString = "SELECT pa " +
            " FROM VProfesionalAvaluosCarga pa " +
            " WHERE CONCAT (UPPER(pa.primerNombre), " +
            " UPPER(pa.segundoNombre), UPPER(pa.primerApellido), " +
            " UPPER(pa.segundoApellido)) " +
            " LIKE CONCAT('%',:nombreAvaluador,'%')" +
            " AND pa.profesionalActivo = 'SI'" +
            " AND pa.territorialCodigo = :codigoTerritorial";

        LOGGER.debug(queryString);
        try {
            query = this.entityManager.createQuery(queryString);

            nombreProfesional = nombreProfesional.replace(" ", "%");
            query.setParameter("nombreAvaluador", nombreProfesional.toUpperCase());
            query.setParameter("codigoTerritorial", codigoTerritorial);
            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "VProfesionalAvaluosCargaDAOBean#buscarCargaAvaluadoresPorNombreYterritorial");

        }
        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVProfesionalAvaluosCargaDAO#consultarPorTerritorial(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<VProfesionalAvaluosCarga> consultarPorTerritorial(String codigoTerritorial) {

        LOGGER.debug("on VProfesionalAvaluosCargaDAOBean#consultarPorTerritorial");

        List<VProfesionalAvaluosCarga> answer = null;
        String queryString;
        Query query;

        queryString =
            " SELECT vpac FROM VProfesionalAvaluosCarga vpac " +
            " WHERE vpac.territorialCodigo = :codTerritorial_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("codTerritorial_p", codigoTerritorial);
            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "VProfesionalAvaluosCargaDAOBean#consultarPorTerritorial");
        }

        return answer;
    }

//end of class
}
