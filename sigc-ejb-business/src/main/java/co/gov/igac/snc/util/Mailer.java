package co.gov.igac.snc.util;

import java.io.File;
import java.util.Properties;

import javax.naming.Context;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;

/**
 * Singleton para el envío de correo electrónico
 *
 * @author fredy.wilches
 *
 */
public class Mailer {

    private static final Logger LOGGER = LoggerFactory.getLogger(Mailer.class);

    private static Mailer instance;

    /**
     * Constructor privado para el Singleton
     */
    private Mailer() {

    }

    /**
     *
     * @return @throws ExcepcionSNC
     */
    public static synchronized Mailer getInstance() throws ExcepcionSNC {
        if (instance == null) {
            instance = new Mailer();
        }
        return instance;
    }

    /**
     * Envío de correo electrónico al administrador del sistema Utilizado para enviar notificaciones
     * de errores
     *
     * @param subject
     * @param body
     * @param fileAttachments
     * @param titleAttachments
     * @throws ExcepcionSNC
     */
    public void sendEmailToSysAdmin(String subject, String body) throws ExcepcionSNC {
        try {
            Properties contextProps = new Properties();
            contextProps.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.as.naming.InitialContextFactory");
            InitialContext ictx = new InitialContext(contextProps);
            Session session = (Session) ictx.lookup("java:/SNCMiddlewareMail");
            Properties props = session.getProperties();
            String emailSysAdmin = props.getProperty("mail.from");
            sendEmail(emailSysAdmin, subject, body, null, null);
        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100013.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

    /**
     * Envío de correo electrónico. </br>
     * OJO: este solo envía a una dirección de correo de tipo TO
     *
     * @param email
     * @param subject
     * @param body
     * @param fileAttachments
     * @param titleAttachments
     * @throws MessagingException
     * @throws NamingException
     */
    public void sendEmail(String email, String subject, String body, String[] fileAttachments,
        String[] titleAttachments)
        throws ExcepcionSNC {
        LOGGER.debug("sendMsg");

        InternetAddress[] address;
        try {
            // @Resource(lookup="java:/MiddlewareMagicMail")
            // private Session mailSession;

            Properties contextProps = new Properties();
            contextProps.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.as.naming.InitialContextFactory");
            InitialContext ictx = new InitialContext(contextProps);

            Session session = (Session) ictx.lookup("java:/SNCMiddlewareMail");
            Properties props = session.getProperties();

            String host = props.getProperty("mail.smtp.host");
            int port = Integer.parseInt(props.getProperty("mail.smtp.port"));
            String username = props.getProperty("mail.user");
            String password = props.getProperty("mail.password");
            String from = props.getProperty("mail.from");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            address = InternetAddress.parse(email, false);
            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(subject);

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setContent(body, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            if (fileAttachments != null && fileAttachments.length > 0) {

                for (int i = 0; i < fileAttachments.length; i++) {
                    File f = new File(fileAttachments[i]);
                    messageBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(fileAttachments[i]);
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    if (titleAttachments[i] != null && !titleAttachments[i].trim().isEmpty()) {
                        messageBodyPart.setFileName(titleAttachments[i]);
                    } else {
                        messageBodyPart.setFileName(f.getName());
                    }
                    multipart.addBodyPart(messageBodyPart);
                }
            }

            message.setContent(multipart);

            Transport transport = session.getTransport("smtp");
            transport.connect(host, port, username, password);

            Transport.send(message);
            LOGGER.debug("Correo enviado satisfactoriamente a " + email);
        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100013.getExcepcion(LOGGER, e, e.
                getMessage());
        } catch (MessagingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100013.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Envío de correo electrónico.
     *
     * @author pedro.garcia
     * @param emailsTO lista de direcciones typo TO
     * @param emailsCC lista de direcciones typo CC
     * @param emailsBCC lista de direcciones typo BCC
     * @param subject
     * @param body
     * @param fileAttachments
     * @param titleAttachments
     */
    /*
     * Se hace casi lo mismo que en el otro sendEmail, pero se usan destinatarios tipo CC y BCC. No
     * se factorizó por pereza
     */
    public void sendEmail(List<String> emailsTO, List<String> emailsCC, List<String> emailsBCC,
        String subject,
        String body, String[] fileAttachments, String[] titleAttachments) throws ExcepcionSNC {

        LOGGER.debug("on Mailer#sendEmail");

        Properties contextProps = new Properties();
        InitialContext ictx;
        Session session;
        Properties props;
        String host, username, password, from;
        int port;
        Message message;
        Transport transport;
        MimeBodyPart messageBodyPart;
        Multipart multipart;
        InternetAddress[] addresses;

        try {

            contextProps.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.jboss.as.naming.InitialContextFactory");
            ictx = new InitialContext(contextProps);

            session = (Session) ictx.lookup("java:/SNCMiddlewareMail");
            props = session.getProperties();

            host = props.getProperty("mail.smtp.host");
            port = Integer.parseInt(props.getProperty("mail.smtp.port"));
            username = props.getProperty("mail.user");
            password = props.getProperty("mail.password");
            from = props.getProperty("mail.from");

            message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setSubject(subject);

            if (emailsTO != null && !emailsTO.isEmpty()) {
                for (String address : emailsTO) {
                    addresses = InternetAddress.parse(address);
                    message.addRecipient(Message.RecipientType.TO, addresses[0]);
                }
            }

            if (emailsCC != null && !emailsCC.isEmpty()) {
                for (String address : emailsCC) {
                    addresses = InternetAddress.parse(address);
                    message.addRecipient(Message.RecipientType.CC, addresses[0]);
                }
            }

            if (emailsBCC != null && !emailsBCC.isEmpty()) {
                for (String address : emailsBCC) {
                    addresses = InternetAddress.parse(address);
                    message.addRecipient(Message.RecipientType.BCC, addresses[0]);
                }
            }

            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html");

            multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            if (fileAttachments != null && fileAttachments.length > 0) {

                for (int i = 0; i < fileAttachments.length; i++) {
                    File f = new File(fileAttachments[i]);
                    messageBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(fileAttachments[i]);
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    if (titleAttachments[i] != null && !titleAttachments[i].trim().isEmpty()) {
                        messageBodyPart.setFileName(titleAttachments[i]);
                    } else {
                        messageBodyPart.setFileName(f.getName());
                    }
                    multipart.addBodyPart(messageBodyPart);
                }
            }

            message.setContent(multipart);

            transport = session.getTransport("smtp");
            transport.connect(host, port, username, password);

            Transport.send(message);

        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100013.getExcepcion(LOGGER, e, e.
                getMessage());
        } catch (MessagingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100013.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

}
