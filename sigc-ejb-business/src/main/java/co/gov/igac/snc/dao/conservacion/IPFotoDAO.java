package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import java.util.List;

/**
 *
 * @author juan.agudelo
 */
@Local
public interface IPFotoDAO extends IGenericJpaDAO<PFoto, Long> {

    /**
     * Método usado para almacenar un Documento y una PFotos vincualdos
     *
     * @author juan.agudelo
     * @param pFoto PFoto
     */
    public PFoto guardarPFoto(PFoto pFoto);

    /**
     * Metodo para guardar una lista de objetos PFoto y sus Documento vinculados
     *
     * @author juan.agudelo
     * @param pFotosList
     */
    public void guardarPFotos(List<PFoto> pFotosList);

    /**
     * busca los registros relacionados con una unidad de construcción dada
     *
     * @author pedro.garcia
     * @param unidadConstruccionId
     * @return
     */
    public List<PFoto> findByUnidadConstruccionIdFetchDocumento(Long unidadConstruccionId);

    /**
     * Actualiza los registros de la tabla asociados a una unidad de construcción. Lo que hace es
     * borrar los registros existentes e insertar los nuevos. El id de la unidad de construcción
     * viene como atributo de los objetos de la lista.
     *
     * OJO: solo debe usarse en el caso en que se crean PFotos para PUnidadesConstruccion
     *
     * @author pedro.garcia
     * @param pFotosList PRE: la lista es no vacía
     */
    public void updateDataByUnidadConstruccion(List<PFoto> pFotosList);

    /**
     * borra los registros de la tabla que tengan como id de unidad de construcción el que se pasa
     * como parámetro
     *
     * @param unidadConstruccionId
     * @return
     */
    public int deleteByUnidadConstruccion(Long unidadConstruccionId);

    /**
     * Método para guardar una lista de objetos PFoto y sus Documento vinculados Se diferencia del
     * método guardarPFotos en que este usa persist
     *
     * @author pedro.garcia
     * @param pFotosList
     */
    public void persistPFotos(List<PFoto> pFotosList);

}
