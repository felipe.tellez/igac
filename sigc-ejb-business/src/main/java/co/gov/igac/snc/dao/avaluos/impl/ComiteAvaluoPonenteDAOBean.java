package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IComiteAvaluoPonenteDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoPonente;

/**
 * @see IComiteAvaluoPonenteDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ComiteAvaluoPonenteDAOBean extends
    GenericDAOWithJPA<ComiteAvaluoPonente, Long> implements IComiteAvaluoPonenteDAO {
}
