package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IVProfesionalAvaluosContratoDAO;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;

/**
 * Clase que contiene los queries sobre la vista v_profesional_avaluos_contrato en SNC_AVALUOS
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class VProfesionalAvaluosContratoDAOBean extends
    GenericDAOWithJPA<VProfesionalAvaluosContrato, Long> implements
    IVProfesionalAvaluosContratoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VProfesionalAvaluosContratoDAOBean.class);

    /**
     * @see
     * IVProfesionalAvaluosContratoDAO#buscarAvaluadoresPorFiltro(FiltroDatosConsultaProfesionalAvaluos)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<VProfesionalAvaluosContrato> buscarAvaluadoresPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtroDatos) {

        LOGGER.debug("inicio VProfesionalAvaluosContratoDAOBean#buscarAvaluadoresPorFiltro");

        List<VProfesionalAvaluosContrato> result = null;
        StringBuilder queryString = new StringBuilder();
        Query query = null;

        queryString.append("SELECT vpac" + " FROM VProfesionalAvaluosContrato vpac" +
            " WHERE 1=1");

        if (filtroDatos.getTipoIdentificacion() != null &&
            !filtroDatos.getTipoIdentificacion().isEmpty()) {
            queryString.append(" AND vpac.tipoIdentificacion = :tipoId");
        }

        if (filtroDatos.getNumeroIdentificacion() != null &&
            !filtroDatos.getNumeroIdentificacion().isEmpty()) {
            queryString.append(" AND vpac.numeroIdentificacion = :numeroId");
        }

        if (filtroDatos.getPrimerApellido() != null &&
            !filtroDatos.getPrimerApellido().isEmpty()) {
            queryString.append(" AND vpac.primerApellido = :primerApellido");
        }

        if (filtroDatos.getPrimerNombre() != null &&
            !filtroDatos.getPrimerNombre().isEmpty()) {
            queryString.append(" AND vpac.primerNombre = :primerNombre");
        }

        if (filtroDatos.getSegundoApellido() != null &&
            !filtroDatos.getSegundoApellido().isEmpty()) {
            queryString.append(" AND vpac.segundoApellido = :segundoApellido");
        }

        if (filtroDatos.getSegundoNombre() != null &&
            !filtroDatos.getSegundoNombre().isEmpty()) {
            queryString.append(" AND vpac.segundoNombre = :segundoNombre");
        }

        if (filtroDatos.getProfesion() != null &&
            !filtroDatos.getProfesion().isEmpty()) {
            queryString.append(" AND vpac.profesion = :profesion");
        }

        if (filtroDatos.getTerritorialContrato() != null &&
            !filtroDatos.getTerritorialContrato().isEmpty()) {
            queryString.append(" AND vpac.territorialCodigo = :territorial");
        }

        if (filtroDatos.getContratoActivo() != null &&
            !filtroDatos.getContratoActivo().isEmpty()) {
            queryString.append(" AND vpac.activo = :contratoActivo");
        }

        String queryStr = queryString.toString();
        query = this.entityManager.createQuery(queryStr);

        if (filtroDatos.getTipoIdentificacion() != null &&
            !filtroDatos.getTipoIdentificacion().isEmpty()) {
            query.setParameter("tipoId", filtroDatos.getTipoIdentificacion());
        }

        if (filtroDatos.getNumeroIdentificacion() != null &&
            !filtroDatos.getNumeroIdentificacion().isEmpty()) {
            query.setParameter("numeroId",
                filtroDatos.getNumeroIdentificacion());
        }

        if (filtroDatos.getPrimerApellido() != null &&
            !filtroDatos.getPrimerApellido().isEmpty()) {
            query.setParameter("primerApellido",
                filtroDatos.getPrimerApellido());
        }

        if (filtroDatos.getPrimerNombre() != null &&
            !filtroDatos.getPrimerNombre().isEmpty()) {
            query.setParameter("primerNombre", filtroDatos.getPrimerNombre());
        }

        if (filtroDatos.getSegundoApellido() != null &&
            !filtroDatos.getSegundoApellido().isEmpty()) {
            query.setParameter("segundoApellido",
                filtroDatos.getSegundoApellido());
        }

        if (filtroDatos.getSegundoNombre() != null &&
            !filtroDatos.getSegundoNombre().isEmpty()) {
            query.setParameter("segundoNombre", filtroDatos.getSegundoNombre());
        }

        if (filtroDatos.getProfesion() != null &&
            !filtroDatos.getProfesion().isEmpty()) {
            query.setParameter("profesion", filtroDatos.getProfesion());
        }

        if (filtroDatos.getTerritorialContrato() != null &&
            !filtroDatos.getTerritorialContrato().isEmpty()) {
            query.setParameter("territorial", filtroDatos.getTerritorialContrato());
        }

        if (filtroDatos.getContratoActivo() != null &&
            !filtroDatos.getContratoActivo().isEmpty()) {
            query.setParameter("contratoActivo", filtroDatos.getContratoActivo());
        }

        result = query.getResultList();

        LOGGER.debug("fin VProfesionalAvaluosContratoDAOBean#buscarAvaluadoresPorFiltro");

        return result;
    }
}
