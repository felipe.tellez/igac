package co.gov.igac.snc.dao.tramite.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ISolicitanteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.ESolicitanteRelacionNoti;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class SolicitanteDAOBean extends GenericDAOWithJPA<Solicitante, Long>
    implements ISolicitanteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitanteDAOBean.class);

//--------------------------------------------------------------------------------------------------    
    @Override
    public Solicitante buscarSolicitantePorTipoYNumeroDeDocumento(
        String numeroDocumento, String tipoDocumento) {
        LOGGER.debug("Buscando a solicitante dado su tipo y numero de documento");
        LOGGER.debug(numeroDocumento + "\t" + tipoDocumento);

        Solicitante answer = null;

        String sqlQuery = "SELECT s FROM Solicitante s" +
            " LEFT JOIN FETCH s.direccionPais" +
            " LEFT JOIN FETCH s.direccionDepartamento" +
            " LEFT JOIN FETCH s.direccionMunicipio" +
            " WHERE s.tipoIdentificacion = :tipoIdentificacion AND" +
            " s.numeroIdentificacion = :numeroIdentificacion";
        try {
            Query q = entityManager.createQuery(sqlQuery);
            q.setParameter("tipoIdentificacion", tipoDocumento);
            q.setParameter("numeroIdentificacion", numeroDocumento);

            answer = (Solicitante) q.getSingleResult();
            return answer;
        } catch (NoResultException nrEx) {
            LOGGER.info("La consulta de solicitantes no devolvió resultados");
            return null;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "SolicitanteDAOBean#buscarSolicitantePorTipoYNumeroDeDocumento");
        } finally {
            return answer;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     */
    @Override
    public Solicitante findSolicitanteByTipoIdentAndNumDocFetchRelacions(
        String tipoDocumento, String numeroDocumento) {

        Solicitante answer = null;

        String sqlQuery = "SELECT s FROM Solicitante s" +
            " LEFT JOIN FETCH s.direccionPais" +
            " LEFT JOIN FETCH s.direccionDepartamento" +
            " LEFT JOIN FETCH s.direccionMunicipio" +
            " WHERE s.tipoIdentificacion = :tipoIdentificacion AND" +
            " s.numeroIdentificacion = :numeroIdentificacion";

        Query q = entityManager.createQuery(sqlQuery);
        q.setParameter("tipoIdentificacion", tipoDocumento);
        q.setParameter("numeroIdentificacion", numeroDocumento);

        try {
            answer = (Solicitante) q.getSingleResult();
        } catch (NoResultException nrEx) {
            LOGGER.info("La consulta no devolvió solicitantes");
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "SolicitanteDAOBean#findSolicitanteByTipoIdentAndNumDocFetchRelacions");
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que realiza la búsqueda de solicitantes regresando una lista de tipo
     * {@link Solicitante} en caso de que haya más de uno
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Solicitante> findSolicitanteByTipoIdentAndNumDocFetchRelacionsList(
        String tipoDocumento, String numeroDocumento) {

        List<Solicitante> answer = null;

        String sqlQuery = "SELECT s FROM Solicitante s" +
            " LEFT JOIN FETCH s.direccionPais" +
            " LEFT JOIN FETCH s.direccionDepartamento" +
            " LEFT JOIN FETCH s.direccionMunicipio" +
            " WHERE s.tipoIdentificacion = :tipoIdentificacion AND" +
            " s.numeroIdentificacion = :numeroIdentificacion";

        Query q = entityManager.createQuery(sqlQuery);
        q.setParameter("tipoIdentificacion", tipoDocumento);
        q.setParameter("numeroIdentificacion", numeroDocumento);

        try {
            answer = q.getResultList();
        } catch (NoResultException nrEx) {
            LOGGER.info("La consulta no devolvió solicitantes", nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "SolicitanteDAOBean#findSolicitanteByTipoIdentAndNumDocFetchRelacions");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author david.cifuentes
     * @see ISolicitanteDAO#buscarSolicitantesPorFiltro(FiltroDatosConsultaSolicitante, boolean,
     * String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Solicitante> buscarSolicitantesPorFiltro(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante, boolean cargarPaisDeptoYMuni,
        String filtroAdicionalJPQL) {
        LOGGER.debug("Entra en SolicitanteDAOBean#buscarSolicitantePorNombre");

        StringBuilder query = new StringBuilder();

        query.append("SELECT s FROM Solicitante s ");

        if (cargarPaisDeptoYMuni) {
            query.append(" LEFT JOIN FETCH s.direccionPais ");
            query.append(" LEFT JOIN FETCH s.direccionDepartamento ");
            query.append(" LEFT JOIN FETCH s.direccionMunicipio ");
        }

        query.append(" WHERE 1=1 ");

        if (datosFiltroSolicitante.getPrimerNombre() != null &&
            !datosFiltroSolicitante.getPrimerNombre().equals("")) {
            query.append(" AND UPPER(s.primerNombre) LIKE :primerNombre");
        }
        if (datosFiltroSolicitante.getSegundoNombre() != null &&
            !datosFiltroSolicitante.getSegundoNombre().equals("")) {
            query.append(" AND UPPER(s.segundoNombre) LIKE :segundoNombre");
        }
        if (datosFiltroSolicitante.getPrimerApellido() != null &&
            !datosFiltroSolicitante.getPrimerApellido().equals("")) {
            query.append(" AND UPPER(s.primerApellido) LIKE :primerApellido");
        }
        if (datosFiltroSolicitante.getSegundoApellido() != null &&
            !datosFiltroSolicitante.getSegundoApellido().equals("")) {
            query.append(" AND UPPER(s.segundoApellido) LIKE :segundoApellido");
        }
        if (datosFiltroSolicitante.getDigitoVerificacion() != null &&
            !datosFiltroSolicitante.getDigitoVerificacion().equals("")) {
            query.append(" AND s.digitoVerificacion = :digitoVerificacion");
        }

        if (datosFiltroSolicitante.getRazonSocial() != null &&
            !datosFiltroSolicitante.getRazonSocial().equals("")) {
            query.append(" AND UPPER(s.razonSocial) LIKE :razonSocial");
        }

        if (datosFiltroSolicitante.getTipoIdentificacion() != null &&
            !datosFiltroSolicitante.getTipoIdentificacion().equals("")) {
            query.append(" AND s.tipoIdentificacion = :tipoIdentificacion");
        }

        if (datosFiltroSolicitante.getNumeroIdentificacion() != null &&
            !datosFiltroSolicitante.getNumeroIdentificacion().equals("")) {
            query.append(" AND s.numeroIdentificacion = :numeroIdentificacion");
        }

        if (datosFiltroSolicitante.getTipoPersona() != null &&
            !datosFiltroSolicitante.getTipoPersona().isEmpty()) {
            query.append(" AND UPPER(s.tipoPersona) LIKE :tipoPersona");
        }

        if (datosFiltroSolicitante.getSigla() != null &&
            !datosFiltroSolicitante.getSigla().isEmpty()) {
            query.append(" AND UPPER(s.sigla) LIKE :sigla");
        }

        if (filtroAdicionalJPQL != null && !filtroAdicionalJPQL.isEmpty()) {
            query.append(filtroAdicionalJPQL);
        }

        Query q = this.entityManager.createQuery(query.toString());

        if (datosFiltroSolicitante.getPrimerNombre() != null &&
            !datosFiltroSolicitante.getPrimerNombre().equals("")) {
            q.setParameter("primerNombre", datosFiltroSolicitante
                .getPrimerNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitante.getSegundoNombre() != null &&
            !datosFiltroSolicitante.getSegundoNombre().equals("")) {
            q.setParameter("segundoNombre", datosFiltroSolicitante
                .getSegundoNombre().toUpperCase() + "%");
        }
        if (datosFiltroSolicitante.getPrimerApellido() != null &&
            !datosFiltroSolicitante.getPrimerApellido().equals("")) {
            q.setParameter("primerApellido", datosFiltroSolicitante
                .getPrimerApellido().toUpperCase() + "%");
        }
        if (datosFiltroSolicitante.getSegundoApellido() != null &&
            !datosFiltroSolicitante.getSegundoApellido().equals("")) {
            q.setParameter("segundoApellido", datosFiltroSolicitante
                .getSegundoApellido().toUpperCase() + "%");
        }
        if (datosFiltroSolicitante.getDigitoVerificacion() != null &&
            !datosFiltroSolicitante.getDigitoVerificacion().equals("")) {
            q.setParameter("digitoVerificacion",
                datosFiltroSolicitante.getDigitoVerificacion());
        }

        if (datosFiltroSolicitante.getRazonSocial() != null &&
            !datosFiltroSolicitante.getRazonSocial().equals("")) {
            q.setParameter("razonSocial", datosFiltroSolicitante
                .getRazonSocial().toUpperCase() + "%");
        }

        if (datosFiltroSolicitante.getTipoIdentificacion() != null &&
            !datosFiltroSolicitante.getTipoIdentificacion().equals("")) {
            q.setParameter("tipoIdentificacion",
                datosFiltroSolicitante.getTipoIdentificacion());
        }

        if (datosFiltroSolicitante.getNumeroIdentificacion() != null &&
            !datosFiltroSolicitante.getNumeroIdentificacion().equals("")) {
            q.setParameter("numeroIdentificacion",
                datosFiltroSolicitante.getNumeroIdentificacion());
        }

        if (datosFiltroSolicitante.getTipoPersona() != null &&
            !datosFiltroSolicitante.getTipoPersona().isEmpty()) {
            q.setParameter("tipoPersona", datosFiltroSolicitante.getTipoPersona());
        }

        if (datosFiltroSolicitante.getSigla() != null &&
            !datosFiltroSolicitante.getSigla().isEmpty()) {
            q.setParameter("sigla", datosFiltroSolicitante.getSigla());
        }

        try {
            return (List<Solicitante>) q.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("SolicitanteDAOBean#buscarSolicitantePorNombre: " +
                "La consulta de solicitantes no devolvió resultados");
            return null;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de solicitantes: " +
                ex.getMessage());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * ISolicitanteDAO#buscarSolicitantesPorFiltroConContratoInteradministrativo(FiltroDatosConsultaSolicitante)
     * @author christian.rodriguez
     */
    @Override
    public List<Solicitante> buscarSolicitantesPorFiltroConContratoInteradministrativo(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante) {

        List<Solicitante> solicitantesConContrato = new ArrayList<Solicitante>();

        String filtroAdicionalJPQL =
            " AND s.id IN (SELECT ci.entidadSolicitante.id FROM ContratoInteradministrativo ci)  ";

        solicitantesConContrato = this.buscarSolicitantesPorFiltro(datosFiltroSolicitante, true,
            filtroAdicionalJPQL);

        return solicitantesConContrato;
    }

    /**
     * @see ISolicitanteDAO#obtenerSolicitantesPropietariosTramiteMasivo(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<Solicitante> obtenerSolicitantesPropietariosTramiteMasivo(Long tramiteId) {
        List<Solicitante> solicitantes = new ArrayList<Solicitante>();

        TreeMap<Long, String> direcciones = new TreeMap<Long, String>();
        TreeMap<Long, Long> personaPredio = new TreeMap<Long, Long>();

        try {
            String queryPrevio = "SELECT s FROM Solicitante s" +
                " LEFT JOIN FETCH s.direccionDepartamento d" +
                " LEFT JOIN FETCH s.direccionMunicipio m" +
                " WHERE s.tramiteId = :tramiteId" +
                " AND s.relacionNotificacion = :relacion";

            Query qPrevio = this.entityManager.createQuery(queryPrevio);

            qPrevio.setParameter("tramiteId", tramiteId);
            qPrevio.setParameter("relacion", ESolicitanteRelacionNoti.PROPIETARIO.getValor());
            solicitantes = qPrevio.getResultList();

            //Si ya existen los solicitantes no se procesa nada adicional
            if (solicitantes != null && !solicitantes.isEmpty()) {
                return solicitantes;
            }

            //Se obtienen las direcciones de los predios
            String query = "SELECT ppp.id,ppd.direccion  FROM P_PERSONA_PREDIO PPP " +
                " INNER JOIN P_PERSONA PP ON PP.ID= PPP.PERSONA_ID" +
                " INNER JOIN P_PREDIO p ON p.ID = PPP.PREDIO_ID " +
                " INNER JOIN P_PREDIO_DIRECCION PPD ON PPD.PREDIO_ID = P.ID" +
                " WHERE ppp.PREDIO_ID IN  " +
                " (SELECT pp2.id FROM  p_predio pp2 where pp2.tramite_id = :tramiteId) " +
                " AND ppd.principal = 'SI'" +
                " AND P.ORIGINAL_TRAMITE = 'NO'";

            Query q = this.entityManager.createNativeQuery(query);

            q.setParameter("tramiteId", tramiteId);
            List<Object> answer = (List<Object>) q.getResultList();

            LOGGER.debug(answer.toString());

            for (Object object : answer) {
                Object[] item = (Object[]) object;
                Long idPredio = ((BigDecimal) item[0]).longValue();
                String direccion = ((String) item[1]);
                direcciones.put(idPredio, direccion);
            }

            //Se obtienen las personas
            String queryPersona = "SELECT PP.*  FROM P_PERSONA_PREDIO PPP " +
                " INNER JOIN P_PERSONA PP ON PP.ID= PPP.PERSONA_ID" +
                " INNER JOIN P_PREDIO p ON p.ID = PPP.PREDIO_ID " +
                " WHERE ppp.PREDIO_ID IN  " +
                " (SELECT pp2.id FROM  p_predio pp2 where pp2.tramite_id = :tramiteId) " +
                " AND P.ORIGINAL_TRAMITE = 'NO'";

            Query qPersona = this.entityManager.createNativeQuery(queryPersona, PPersona.class);

            qPersona.setParameter("tramiteId", tramiteId);
            List<PPersona> personas = (List<PPersona>) qPersona.getResultList();

            //se obtienen los predios de las personas
            String queryPredios = "SELECT PPP.*  FROM P_PERSONA_PREDIO PPP " +
                " INNER JOIN P_PERSONA PP ON PP.ID= PPP.PERSONA_ID" +
                " INNER JOIN P_PREDIO p ON p.ID = PPP.PREDIO_ID " +
                " WHERE ppp.PREDIO_ID IN  " +
                " (SELECT pp2.id FROM  p_predio pp2 where pp2.tramite_id = :tramiteId) " +
                " AND P.ORIGINAL_TRAMITE = 'NO'";

            Query qPredios = this.entityManager.
                createNativeQuery(queryPredios, PPersonaPredio.class);

            qPredios.setParameter("tramiteId", tramiteId);
            List<PPersonaPredio> predios = (List<PPersonaPredio>) qPredios.getResultList();

            for (int i = 0; i < personas.size(); i++) {

                PPersona pPersona = personas.get(i);

                //Asigna la direccion del predio si el propietario no tiene direccion originalmente
                if (pPersona.getDireccion() == null) {
                    pPersona.setDireccion(direcciones.get(predios.get(i).getId()));
                }
                Solicitante s = new Solicitante(pPersona);

                //Marcar solicitante como propietario y asignar tramite id
                s.setTipoSolicitante(ESolicitanteTipoSolicitant.PRIVADA.getCodigo());
                s.setTramiteId(tramiteId);
                s.setRelacionNotificacion(ESolicitanteRelacionNoti.PROPIETARIO.getValor());
                //Si ya existe un solicitante con el mismo numero de identificacion y tipo de documento no lo agrega
                boolean contieneSolicitante = false;
                for (Solicitante sol : solicitantes) {
                    if (sol.getNumeroIdentificacion().equals(s.getNumeroIdentificacion()) &&
                        sol.getTipoIdentificacion().equals(s.getTipoIdentificacion())) {

                        contieneSolicitante = true;
                        break;
                    }
                }

                if (!contieneSolicitante) {
                    solicitantes.add(s);
                }
            }

            this.persistMultiple(solicitantes);

            //Recuperar ids de solicitantes nuevos
            String queryPost = "SELECT s FROM Solicitante s" +
                " LEFT JOIN FETCH s.direccionDepartamento d" +
                " LEFT JOIN FETCH s.direccionMunicipio m" +
                " WHERE s.tramiteId = :tramiteId" +
                " AND s.relacionNotificacion = :relacion";

            Query qPost = this.entityManager.createQuery(queryPost);

            qPost.setParameter("tramiteId", tramiteId);
            qPost.setParameter("relacion", ESolicitanteRelacionNoti.PROPIETARIO.getValor());
            solicitantes = qPost.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerSolicitantesPropietariosTramiteMasivo");
        }

        return solicitantes;
    }

    /**
     * @see ISolicitanteDAO#obtenerSolicitantesPropietariosCond0Y5(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<Solicitante> obtenerSolicitantesPropietariosEV(Long tramiteId) {
        List<Solicitante> solicitantes = new ArrayList<Solicitante>();

        TreeMap<Long, String> direcciones = new TreeMap<Long, String>();
        TreeMap<Long, Long> personaPredio = new TreeMap<Long, Long>();
        try {
            String queryPrevio = "SELECT s FROM Solicitante s" +
                " LEFT JOIN FETCH s.direccionDepartamento d" +
                " LEFT JOIN FETCH s.direccionMunicipio m" +
                " WHERE s.tramiteId = :tramiteId" +
                " AND s.relacionNotificacion = :relacion";

            Query qPrevio = this.entityManager.createQuery(queryPrevio);

            qPrevio.setParameter("tramiteId", tramiteId);
            qPrevio.setParameter("relacion", ESolicitanteRelacionNoti.PROPIETARIO.getValor());
            solicitantes = qPrevio.getResultList();

            //Si ya existen los solicitantes no se procesa nada adicional
            if (solicitantes != null && !solicitantes.isEmpty()) {
                return solicitantes;
            }

            //Se obtienen las direcciones de los predios
            String query = "SELECT ppp.id,ppd.direccion  FROM P_PERSONA_PREDIO PPP " +
                " INNER JOIN P_PERSONA PP ON PP.ID= PPP.PERSONA_ID" +
                " INNER JOIN P_PREDIO p ON p.ID = PPP.PREDIO_ID " +
                " INNER JOIN P_PREDIO_DIRECCION PPD ON PPD.PREDIO_ID = P.ID" +
                " WHERE ppp.PREDIO_ID IN  " +
                " (SELECT pp2.id FROM  p_predio pp2 where pp2.tramite_id = :tramiteId) " +
                " AND ppd.principal = 'SI'";

            Query q = this.entityManager.createNativeQuery(query);

            q.setParameter("tramiteId", tramiteId);

            List<Object> answer = (List<Object>) q.getResultList();

            LOGGER.debug(answer.toString());

            for (Object object : answer) {
                Object[] item = (Object[]) object;
                Long idPredio = ((BigDecimal) item[0]).longValue();
                String direccion = ((String) item[1]);
                direcciones.put(idPredio, direccion);
            }

            //Se obtienen las personas
            String queryPersona = "SELECT PP.*  FROM P_PERSONA_PREDIO PPP " +
                " INNER JOIN P_PERSONA PP ON PP.ID= PPP.PERSONA_ID" +
                " INNER JOIN P_PREDIO p ON p.ID = PPP.PREDIO_ID " +
                " WHERE ppp.PREDIO_ID IN  " +
                " (SELECT pp2.id FROM  p_predio pp2 where pp2.tramite_id = :tramiteId) ";

            Query qPersona = this.entityManager.createNativeQuery(queryPersona, PPersona.class);

            qPersona.setParameter("tramiteId", tramiteId);
            List<PPersona> personas = (List<PPersona>) qPersona.getResultList();

            //se obtienen los predios de las personas
            String queryPredios = "SELECT PPP.*  FROM P_PERSONA_PREDIO PPP " +
                " INNER JOIN P_PERSONA PP ON PP.ID= PPP.PERSONA_ID" +
                " INNER JOIN P_PREDIO p ON p.ID = PPP.PREDIO_ID " +
                " WHERE ppp.PREDIO_ID IN  " +
                " (SELECT pp2.id FROM  p_predio pp2 where pp2.tramite_id = :tramiteId) ";

            Query qPredios = this.entityManager.
                createNativeQuery(queryPredios, PPersonaPredio.class);

            qPredios.setParameter("tramiteId", tramiteId);
            List<PPersonaPredio> predios = (List<PPersonaPredio>) qPredios.getResultList();

            for (int i = 0; i < personas.size(); i++) {

                PPersona pPersona = personas.get(i);

                //Asigna la direccion del predio si el propietario no tiene direccion originalmente
                if (pPersona.getDireccion() == null) {
                    pPersona.setDireccion(direcciones.get(predios.get(i).getId()));
                }
                Solicitante s = new Solicitante(pPersona);

                //Marcar solicitante como propietario y asignar tramite id
                s.setTipoSolicitante(ESolicitanteTipoSolicitant.PRIVADA.getCodigo());
                s.setTramiteId(tramiteId);
                s.setRelacionNotificacion(ESolicitanteRelacionNoti.PROPIETARIO.getValor());
                //Si ya existe un solicitante con el mismo numero de identificacion y tipo de documento no lo agrega
                boolean contieneSolicitante = false;
                for (Solicitante sol : solicitantes) {
                    if (sol.getNumeroIdentificacion().equals(s.getNumeroIdentificacion()) &&
                        sol.getTipoIdentificacion().equals(s.getTipoIdentificacion())) {

                        contieneSolicitante = true;
                        break;
                    }
                }

                if (!contieneSolicitante) {
                    solicitantes.add(s);
                }
            }

            this.persistMultiple(solicitantes);

            //Recuperar ids de solicitantes nuevos
            String queryPost = "SELECT s FROM Solicitante s" +
                " LEFT JOIN FETCH s.direccionDepartamento d" +
                " LEFT JOIN FETCH s.direccionMunicipio m" +
                " WHERE s.tramiteId = :tramiteId" +
                " AND s.relacionNotificacion = :relacion";

            Query qPost = this.entityManager.createQuery(queryPost);

            qPost.setParameter("tramiteId", tramiteId);
            qPost.setParameter("relacion", ESolicitanteRelacionNoti.PROPIETARIO.getValor());
            solicitantes = qPost.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerSolicitantesPropietariosTramiteMasivo");
        }

        return solicitantes;
    }

}
