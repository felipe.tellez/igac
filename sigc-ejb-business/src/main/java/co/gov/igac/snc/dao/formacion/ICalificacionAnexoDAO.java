/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.formacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface para el DAO de CalificacionAnexo
 *
 * @author pedro.garcia
 */
@Local
public interface ICalificacionAnexoDAO extends IGenericJpaDAO<CalificacionAnexo, Long> {

    /**
     * Retorna la lista de registros donde el id del uso de construcción sea el dado
     *
     * @param usoConstruccionId
     * @return
     */
    public List<CalificacionAnexo> findByUsoConstruccionId(Long usoConstruccionId);

    /**
     * Método que recupera todos los registros de calificación anexo
     *
     * @author javier.aponte
     * @return
     */
    public List<CalificacionAnexo> getAllCalificacionAnexo();
}
