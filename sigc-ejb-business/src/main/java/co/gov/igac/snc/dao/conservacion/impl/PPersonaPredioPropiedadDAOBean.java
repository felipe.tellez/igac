package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioPropiedadDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class PPersonaPredioPropiedadDAOBean extends
    GenericDAOWithJPA<PPersonaPredioPropiedad, Long> implements
    IPPersonaPredioPropiedadDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPersonaPredioPropiedadDAOBean.class);

    /**
     * @author juan.agudelo
     * @see IPPredioDAO#buscarPPredioFetchPPersonaPredioPorId(Long)
     */
    @Override
    public PPersonaPredioPropiedad buscarPPersonaPredioPropiedadPorId(Long id) {
        String query = "SELECT p3" + " FROM PPersonaPredioPropiedad p3 " +
            " JOIN FETCH p3.PPersonaPredio p2" + " JOIN FETCH p2.PPredio" +
            " WHERE p3.id = :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("id", id);
        try {
            PPersonaPredioPropiedad p = (PPersonaPredioPropiedad) q
                .getSingleResult();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    // --------------------------------------------------------------------------------------
    @Override
    public void gestionarSolicitud(
        List<PPersonaPredioPropiedad> gestionarSolicitud) {

        try {
            for (PPersonaPredioPropiedad p3 : gestionarSolicitud) {
                persist(p3);
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(LOGGER, e,
                "PPersonaPredioPropiedad");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPersonaPredioPropiedadDAO#findByPPersonaPredioId(Long)
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPersonaPredioPropiedad> findByPPersonaPredioId(Long idPPersonaPredio) {
        String sql = "SELECT pppp FROM PPersonaPredioPropiedad pppp" +
            " JOIN FETCH pppp.PPersonaPredio" +
            " LEFT JOIN FETCH pppp.departamento" +
            " LEFT JOIN FETCH pppp.municipio" +
            " LEFT JOIN FETCH pppp.documentoSoporte ds" +
            " LEFT JOIN FETCH ds.tipoDocumento" +
            " WHERE pppp.PPersonaPredio.id = :idPPersonaPredio";
        Query q = this.entityManager.createQuery(sql).setParameter("idPPersonaPredio",
            idPPersonaPredio);
        return q.getResultList();
    }

    // -----------------------------------------//
    /**
     * @see IPPersonaPredioPropiedadDAO#buscarTitulosPorPPersonaPredio
     * @author fredy.wilches
     */
    public List<PPersonaPredioPropiedad> buscarTitulosPorPPersonaPredio(PPersonaPredio pp) {

        List<PPersonaPredioPropiedad> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT pppp FROM PPersonaPredioPropiedad pppp" +
            " LEFT JOIN FETCH pppp.PPersonaPredio ppp" +
            " LEFT JOIN FETCH pppp.documentoSoporte ds " +
            " LEFT JOIN FETCH ds.tipoDocumento " +
            " LEFT JOIN FETCH pppp.departamento " +
            " LEFT JOIN FETCH pppp.municipio " +
            " JOIN FETCH ppp.PPredio pp" +
            " WHERE ppp.id = :idPPersonaPredio";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPersonaPredio", pp.getId());
            answer = query.getResultList();

        } catch (Exception e) {
            LOGGER.error("PPersonaPredioPropiedadDAOBean#buscarTitulosPorPPersonaPredio error: " +
                e.getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IPPersonaPredioPropiedadDAO#consultarPorPPersonaPredioId(Long)
     * @author leidy.gonzalez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPersonaPredioPropiedad> consultarPorPPersonaPredioId(Long idPPersonaPredio) {
        String sql = "SELECT pppp FROM PPersonaPredioPropiedad pppp" +
            " JOIN FETCH pppp.PPersonaPredio" +
            " LEFT JOIN FETCH pppp.departamento" +
            " LEFT JOIN FETCH pppp.municipio" +
            " LEFT JOIN FETCH pppp.documentoSoporte ds" +
            " LEFT JOIN FETCH ds.tipoDocumento" +
            " WHERE pppp.PPersonaPredio.id = :idPPersonaPredio" +
            " AND pppp.modoAdquisicion <> :modoAdquisicion";
        Query q = this.entityManager.createQuery(sql).setParameter("idPPersonaPredio",
            idPPersonaPredio);

        q.setParameter("modoAdquisicion", "MIGRACION");
        return q.getResultList();
    }

}
