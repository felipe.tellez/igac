/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVPredioBloqueadoHoyDAO;
import co.gov.igac.snc.persistence.entity.vistas.VPredioBloqueadoHoy;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class VPredioBloqueadoHoyDAOBean extends
    GenericDAOWithJPA<VPredioBloqueadoHoy, Long> implements
    IVPredioBloqueadoHoyDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPredioBloqueadoHoyDAOBean.class);

    // end of class
}
