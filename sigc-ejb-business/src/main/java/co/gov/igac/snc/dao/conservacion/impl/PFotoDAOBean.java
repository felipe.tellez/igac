package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;

import co.gov.igac.generales.dto.PUnidadConstruccionDto;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFotoDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class PFotoDAOBean extends GenericDAOWithJPA<PFoto, Long> implements IPFotoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PFotoDAOBean.class);

    @EJB
    private ITipoDocumentoDAO tipoDocumentoDaoService;

    /**
     * Permite guardar una Pfoto con el documento asociado
     *
     * @author fredy.wilches
     * @param pFoto
     * @param unidadConstruccion
     */
    @Override
    public PFoto guardarPFoto(PFoto pFoto) {
        Documento doc = pFoto.getDocumento();

        doc.setTipoDocumento(this.tipoDocumentoDaoService.
            findById(ETipoDocumento.FOTOGRAFIA.getId()));
        doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioCreador(pFoto.getUsuarioLog());
        doc.setUsuarioLog(pFoto.getUsuarioLog());
        doc.setFechaLog(pFoto.getFechaLog());
        try {
            //this.documentoService.ingresarDocumento(doc);
            pFoto.setDocumento(doc);
            return this.update(pFoto);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPFotoDAO#guardarPFotos(List)
     */
    @Implement
    @Override
    public void guardarPFotos(List<PFoto> pFotosList) {

        TipoDocumento tipoDocumento;

        try {
            tipoDocumento = this.tipoDocumentoDaoService.findById(ETipoDocumento.FOTOGRAFIA.getId());

            for (int i = 0; i < pFotosList.size(); i++) {
                pFotosList
                    .get(i)
                    .getDocumento()
                    .setTipoDocumento(tipoDocumento);

                this.update(pFotosList.get(i));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "PFotoDAOBean#guardarPFotos");

        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPFotoDAO#findByUnidadConstruccionIdFetchDocumento(java.lang.Long)
     */
    @Override
    public List<PFoto> findByUnidadConstruccionIdFetchDocumento(Long unidadConstruccionId) {

        LOGGER.info("PFotoDAOBean#findByUnidadConstruccionIdFetchDocumento");
        List<PFoto> answer = null;
        Query query;

        query = this.entityManager.createNamedQuery("findByUnidadConstruccionIdFetchDocumento");
        query.setParameter("ucIdParam", unidadConstruccionId);

        try {
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PFotoDAOBean#findByUnidadConstruccionIdFetchDocumento");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPFotoDAO#updateDataByUnidadConstruccion(java.util.List)
     */
    @Override
    public void updateDataByUnidadConstruccion(List<PFoto> pFotosList) {

        LOGGER.info("PFotoDAOBean#updateDataByUnidadConstruccion");

        List<PFoto> toDelete;
        Query query;

        try {
            query = this.entityManager.createNamedQuery("findByUnidadConstruccionId");
            query.setParameter("ucIdParam", pFotosList.get(0).getPUnidadConstruccion().getId());

            toDelete = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL_NAMED_QUERY.getExcepcion(
                LOGGER, ex, "PFoto.findByUnidadConstruccionId",
                "PFotoDAOBean#updateDataByUnidadConstruccion");
        }

        try {
            //D: como se definió que el atributo 'documento' de PFoto tuviera todas las operaciones
            //   en cascada, se borran también esos registros
            removeMultiple(toDelete);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                getExcepcion(
                    LOGGER, ex, "PFotoDAOBean#updateDataByUnidadConstruccion", "PFoto", "?");
        }

        try {
            //D: hay que forzar a que los id de las PFoto y sus Documento sean null para que, una vez
            //   borrados, se puedan insertar sin problema. Si los id no eran null se lanza una excepción
            for (PFoto tempPFoto : pFotosList) {
                tempPFoto.setId(null);
                tempPFoto.getDocumento().setId(null);
            }
            //D:  ... y luego se insertan los nuevos en ambas tablas
            persistPFotos(pFotosList);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(
                LOGGER, ex, "PFoto");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * borra todos los registros que tengan como id de unidad de construcción el que se pasa como
     * parámetro
     *
     * @param unidadConstruccionId
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public int deleteByUnidadConstruccion(Long unidadConstruccionId) {

        Query query;
        int answer = 0;

        query = this.entityManager.createNamedQuery("deleteByUnidadConstruccionId");
        query.setParameter("ucIdParam", unidadConstruccionId);

        // N: here, it's not needed to create a transaction because is't passed
        // from ... somewhere
        answer = query.executeUpdate();

        return answer;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * @see IPFotoDAO#persistPFotos(List)
     */
    @Override
    public void persistPFotos(List<PFoto> pFotosList) {

        TipoDocumento tipoDocumento;

        try {
            tipoDocumento = this.tipoDocumentoDaoService.findById(ETipoDocumento.FOTOGRAFIA.getId());
            for (int i = 0; i < pFotosList.size(); i++) {
                pFotosList.get(i).getDocumento().setTipoDocumento(tipoDocumento);
                this.persist(pFotosList.get(i));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

//end of class
}
