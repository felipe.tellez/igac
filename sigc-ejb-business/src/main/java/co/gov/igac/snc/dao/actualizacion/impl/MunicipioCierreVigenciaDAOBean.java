package co.gov.igac.snc.dao.actualizacion.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IMunicipioCierreVigenciaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.util.EEstadoCierreMunicipio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto {@link MunicipioCierreVigencia}.
 *
 * @author david.cifuentes
 */
@Stateless(mappedName = "MunicipioCierreVigenciaDAO")
public class MunicipioCierreVigenciaDAOBean extends
    GenericDAOWithJPA<MunicipioCierreVigencia, Long> implements
    IMunicipioCierreVigenciaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MunicipioCierreVigenciaDAOBean.class);

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IMunicipioCierreVigencia#
     * consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios (DecretoAvaluoAnual
     * decretoAvaluoAnual, List<String>
     * codigosMunicipios)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<MunicipioCierreVigencia> consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
        DecretoAvaluoAnual decretoAvaluoAnual,
        List<String> codigosMunicipios) throws ExcepcionSNC {

        LOGGER.debug(
            "MunicipioCierreVigenciaDAOBean#consultarMunicipiosCierreVigenciaPorListaCodigosMunicipios");

        List<MunicipioCierreVigencia> answer = null;
        String queryString;
        Query query;

        try {
            queryString = "SELECT DISTINCT mcv FROM MunicipioCierreVigencia mcv" +
                " LEFT JOIN FETCH mcv.zonaCierreVigencias" +
                " LEFT JOIN FETCH mcv.decretoAvaluoAnual daa" +
                " WHERE mcv.municipioCodigo IN (:codigosMunicipios) " +
                " AND daa.id = :idDecretoAvaluoAnual";

            query = entityManager.createQuery(queryString);
            query.setParameter("codigosMunicipios", codigosMunicipios);
            query.setParameter("idDecretoAvaluoAnual",
                decretoAvaluoAnual.getId());

            answer = query.getResultList();

        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "ERROR: MunicipioCierreVigenciaDAOBean#consultarMunicipiosCierreVigenciaPorListaCodigosMunicipios: " +
                e.getMessage());
            throw (e);
        }
        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IMunicipioCierreVigencia# validarExistenciaMunicipiosPorDecreto (DecretoAvaluoAnual
     * decretoAvaluoAnual, UsuarioDTO usuario)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void validarExistenciaMunicipiosPorDecreto(
        DecretoAvaluoAnual decretoAvaluoAnual, UsuarioDTO usuario)
        throws ExcepcionSNC {

        LOGGER.debug("MunicipioCierreVigenciaDAOBean#validarExistenciaMunicipiosPorDecreto");

        String queryString;
        Query query;
        long conteoMCVDecreto = 0;

        try {

            // La vigencia del municipio_cierre_vigencia debe ser un año menos
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(decretoAvaluoAnual.getVigencia());
            calendar.add(Calendar.YEAR, -1);
            String fechaMenosUnAnio = formatoFecha.format(calendar.getTime());
            Date vigencia = formatoFecha.parse(fechaMenosUnAnio);

            // Revisión de la existencia de municipios asociados al decreto en
            // la tabla MunicipioCierreVigencia.
            queryString = "SELECT COUNT (mcv) FROM MunicipioCierreVigencia mcv " +
                " WHERE decretoAvaluoAnual.id = :idDecretoAvaluoAnual";

            query = entityManager.createQuery(queryString);
            query.setParameter("idDecretoAvaluoAnual",
                decretoAvaluoAnual.getId());

            conteoMCVDecreto = (Long) query.getSingleResult();

            // Si existen registros para éste decreto, se debe verificar cuales 
            // no existen y se crean en caso de que haya alguno. En caso de que no
            // existan, se deben insertar todos los municipios en la tabla
            // MunicipioCierreVigencia asociandoles el decreto actual.
            if (conteoMCVDecreto > 0) {

                long conteoMunicipios = 0;

                queryString = "SELECT COUNT (m) FROM Municipio m ";
                query = entityManager.createQuery(queryString);
                conteoMunicipios = (Long) query.getSingleResult();

                if (conteoMCVDecreto == conteoMunicipios) {
                    return;
                } else {
                    // Creación de municipios no existentes
                    queryString = "INSERT INTO municipio_cierre_vigencia " +
                        " (municipio_codigo, vigencia, fecha_log, usuario_log, resolucion_actualizacion_id, resolucion_formacion_id, decreto_avaluo_anual_id, estado_cierre_conservacion, estado_cierre_actualizacion) " +

                        " SELECT m.codigo, :vigencia, :fechaLog, :userLog, :resolucionActualizacion, :resolucionFormacion, :decretoId, :estadoConservacion, :estadoActualizacion " +

                        " FROM Municipio m WHERE m.codigo NOT IN (SELECT municipio_codigo FROM municipio_cierre_vigencia " +
                        " WHERE decreto_avaluo_anual_id = :idDecretoAvaluoAnual)";

                    query = entityManager.createNativeQuery(queryString);

                    // Parámetros
                    query.setParameter("vigencia", vigencia);
                    query.setParameter("fechaLog", new Date());
                    query.setParameter("userLog", usuario.getLogin());
                    query.setParameter("resolucionActualizacion", new Long(0));
                    query.setParameter("resolucionFormacion", new Long(0));
                    query.setParameter("decretoId", decretoAvaluoAnual.getId());
                    query.setParameter("estadoConservacion", EEstadoCierreMunicipio.SIN_EJECUTAR.
                        getEstado());
                    query.setParameter("estadoActualizacion", EEstadoCierreMunicipio.SIN_EJECUTAR.
                        getEstado());
                    query.setParameter("idDecretoAvaluoAnual", decretoAvaluoAnual.getId());

                    query.executeUpdate();
                }

            } else {
                // IMPORTANTE: Esto sólo sucederá una vez al año, una vez
                // insertados los municipios, se trabajará sobre estos durante
                // el resto del año.

                /**
                 * Descomentar en el caso que se quiera probar éste método con la prueba unitaria.
                 *
                 * entityManager.getTransaction().begin();
                 */
                queryString = "SELECT m FROM Municipio m ";
                query = entityManager.createQuery(queryString);
                List<Municipio> municipios = (List<Municipio>) query.getResultList();

                if (municipios != null && !municipios.isEmpty()) {
                    for (Municipio m : municipios) {
                        MunicipioCierreVigencia mcv = new MunicipioCierreVigencia();
                        mcv.setMunicipioCodigo(m.getCodigo());
                        mcv.setResolucionActualizacionId(0L);
                        mcv.setResolucionFormacionId(0L);
                        mcv.setActualizacionTotal(ESiNo.NO.getCodigo());
                        mcv.setFormacionTotal(ESiNo.NO.getCodigo());
                        mcv.setDecretoAvaluoAnual(decretoAvaluoAnual);
                        mcv.setVigencia(vigencia);
                        mcv.setEstadoCierreActualizacion(EEstadoCierreMunicipio.SIN_EJECUTAR.
                            getEstado());
                        mcv.setEstadoCierreConservacion(EEstadoCierreMunicipio.SIN_EJECUTAR.
                            getEstado());
                        mcv.setFechaLog(new Date());
                        mcv.setUsuarioLog(usuario.getLogin());
                        entityManager.persist(mcv);
                    }
                }

                /**
                 * Descomentar en el caso que se quiera probar éste método con la prueba unitaria.
                 *
                 * entityManager.getTransaction().commit();
                 */
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "ERROR: MunicipioCierreVigenciaDAOBean#validarExistenciaMunicipiosPorDecreto: " +
                e.getMessage());
            throw (e);
        } catch (ParseException e) {
            LOGGER.error(
                "ERROR: MunicipioCierreVigenciaDAOBean#validarExistenciaMunicipiosPorDecreto: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_NEGOCIO_0001
                .getExcepcion(LOGGER, e, e.getMessage());
        }
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IMunicipioCierreVigencia# guardarListaMunicipioCierreVigencias(
     * List<MunicipioCierreVigencia> listaMunicipioCierreVigencia)
     */
    @Override
    public boolean guardarListaMunicipioCierreVigencias(
        List<MunicipioCierreVigencia> listaMunicipioCierreVigencia) {

        LOGGER.debug("MunicipioCierreVigenciaDAOBean#guardarListaMunicipioCierreVigencias");
        try {
            this.updateMultiple(listaMunicipioCierreVigencia);
            LOGGER.debug("OK");
            return true;

        } catch (Exception e) {
            LOGGER.error(
                "ERROR: MunicipioCierreVigenciaDAOBean#guardarListaMunicipioCierreVigencias: " +
                e.getMessage());
            return false;
        }
    }

}
