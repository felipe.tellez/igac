package co.gov.igac.snc.dao.generales;

import java.util.List;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.dao.IGenericJpaDAO;

import javax.ejb.Local;

@Local
public interface IDepartamentoDAO extends IGenericJpaDAO<Departamento, String> {

    public List<Departamento> findByPais(String codigoPais);

    /**
     * Retorna la lista de departamentos que pertenecen a una entidad territorial
     *
     * @param entidadTerritorialCode Código de la entidad territorial
     * @return
     */
    public List<Departamento> findByEntidadTerritorial(String entidadTerritorialCode);

    /**
     * Método encargado de obtener un departamento a partir de su código.
     *
     * @param codigo Código del departamento
     * @return Departamento con el código que se recibe como parámetro.
     */
    public Departamento getDepartamentoByCodigo(String codigo);

    /**
     * Retorna una lista de {@link Departamentos} asociados a un {@link Pais} exceptuando los
     * departamentos con catastro centralizado (Antioquia, Bogotá)
     *
     * @param codigoPais {@link String} Código del país
     * @return
     * @author david.cifuentes
     */
    public List<Departamento> buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
        String codigoPais);

    /**
     * Metodo que retorna los departamentos de una UOC
     *
     * @param codigoUoc
     * @return
     */
    public List<Departamento> buscarDepartamentosByUOC(String codigoUoc);

}
