package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class PFichaMatrizPredioDAOBean extends
    GenericDAOWithJPA<PFichaMatrizPredio, Long> implements
    IPFichaMatrizPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PFichaMatrizPredioDAOBean.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<PFichaMatrizPredio> findByPFichaMatrizId(Long pFichaMatrizId) {
        try {
            String sql = "SELECT pfmp FROM PFichaMatrizPredio pfmp" +
                " WHERE pfmp.PFichaMatriz.id = :pFichaMatrizId";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("pFichaMatrizId", pFichaMatrizId);
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Error en la búsqueda de ficha matriz: " +
                e.getMessage());
            return null;
        }
    }

    /**
     * @see IPFichaMatrizPredioDAO#obtenerPFichaMatrizPredioPorNumeroPredial(java.lang.String)
     * @author juanfelipe.garcia
     * @param numeroPredial
     * @return
     */
    @Override
    public PFichaMatrizPredio obtenerPFichaMatrizPredioPorNumeroPredial(String numeroPredial) {

        LOGGER.
            debug("executing PFichaMatrizPredioDAOBean#obtenerPFichaMatrizPredioPorNumeroPredial");

        PFichaMatrizPredio resultado = null;

        String q1 = "SELECT fmp FROM PFichaMatrizPredio fmp" +
            " WHERE fmp.numeroPredial = :numeroPredial";
        try {

            Query q = entityManager.createQuery(q1);
            q.setParameter("numeroPredial", numeroPredial);

            resultado = (PFichaMatrizPredio) q.getSingleResult();

            resultado.getPFichaMatriz().getId();
            resultado.getPFichaMatriz().getPFichaMatrizModelos().size();
            resultado.getPFichaMatriz().getPFichaMatrizTorres().size();
            resultado.getPFichaMatriz().getPFichaMatrizPredios().size();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PFichaMatrizPredioDAOBean#obtenerPFichaMatrizPredioPorNumeroPredial");
        }
        return resultado;
    }

    @Override
    public void eliminar(PFichaMatrizPredio fichaMatrizPredio) {
        LOGGER.debug("executing PFichaMatrizPredioDAOBean#eliminar");

        String q1 = "DELETE FROM P_FICHA_MATRIZ_PREDIO WHERE ID = " + fichaMatrizPredio.getId();
        try {
            Query q = entityManager.createNativeQuery(q1);
            q.executeUpdate();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PFichaMatrizPredioDAOBean#eliminar");
        }
    }

    /**
     * @see IPFichaMatrizPredioDAO#buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(String,
     * Long)
     * @author david.cifuentes
     * @param cancelaInscribe
     * @param fichaMatrizId
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(
        String cancelaInscribe, Long fichaMatrizId) {

        try {
            String sql = "SELECT pfmp FROM PFichaMatrizPredio pfmp" +
                " WHERE pfmp.PFichaMatriz.id = :fichaMatrizId" +
                " AND pfmp.cancelaInscribe = :cancelaInscribe";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("fichaMatrizId", fichaMatrizId);
            query.setParameter("cancelaInscribe", cancelaInscribe);

            return query.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Error en la búsqueda del PFichaMatrizPredio: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see IPFichaMatrizPredioDAO#buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(String, Long)
     * @author david.cifuentes
     * @param estadoPredio
     * @param fichaMatrizId
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(
        String estadoPredio, Long fichaMatrizId) {

        //felipe.cadena::Se consideran los predio que no tienen estado definido
        boolean noEstado = false;
        if ("".equals(estadoPredio) || estadoPredio == null) {
            noEstado = true;
        }

        try {
            String sql = "SELECT pfmp FROM PFichaMatrizPredio pfmp" +
                " WHERE pfmp.PFichaMatriz.id = :fichaMatrizId";

            if (noEstado) {
                sql += " AND pfmp.estado is null";
            } else {
                sql += " AND pfmp.estado = :estadoPredio";
            }

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("fichaMatrizId", fichaMatrizId);
            if (!noEstado) {
                query.setParameter("estadoPredio", estadoPredio);
            }

            List<PFichaMatrizPredio> answer = query.getResultList();
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Error en la búsqueda del PFichaMatrizPredio: " + e.getMessage());
        }
        return null;
    }

    /**
     * @see IPFichaMatrizPredioDAO#obtenerPFichaMatrizPredioPorNumeroPredial(java.lang.String)
     * @author leidy.gonzalez
     * @param numeroPredial
     * @return
     */
    @Override
    public List<PFichaMatrizPredio> obtenerPFichaMatrizPredioContieneNumeroPredial(
        String numeroPredial) {

        LOGGER.debug(
            "executing PFichaMatrizPredioDAOBean#obtenerPFichaMatrizPredioContieneNumeroPredial");

        List<PFichaMatrizPredio> resultado = null;

        String q1 = "SELECT fmp FROM PFichaMatrizPredio fmp" +
            " WHERE fmp.numeroPredial LIKE :numeroPredial";
        try {

            Query q = entityManager.createQuery(q1);
            q.setParameter("numeroPredial", numeroPredial + "%");

            resultado = (List<PFichaMatrizPredio>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PFichaMatrizPredioDAOBean#obtenerPFichaMatrizPredioPorNumeroPredial");
        }
        return resultado;
    }
}
