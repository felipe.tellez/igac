package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramitePruebaEntidadDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author fabio.navarrete
 *
 */
@Stateless
public class TramitePruebaEntidadDAOBean extends
    GenericDAOWithJPA<TramitePruebaEntidad, Long> implements ITramitePruebaEntidadDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramitePruebaEntidadDAOBean.class);

    /**
     * @see ITramiteDAO#findTramitePruebaEntidadByTramiteId(Long)
     * @author franz.gamba
     */
    /*
     * @modified by leidy.gonzalez:: #16501:: 14/04/2016:: Se agrega Join en la consulta de tramite
     * prueba con la estructura organizacional.
     */
    @Override
    public List<TramitePruebaEntidad> findTramitePruebaEntidadByTramiteId(Long tramiteId) {

        Query query = this.entityManager
            .createQuery("SELECT tpe FROM TramitePruebaEntidad tpe " +
                " LEFT JOIN FETCH tpe.direccionMunicipio " +
                " LEFT JOIN FETCH tpe.direccionDepartamento " +
                " LEFT JOIN FETCH tpe.solicitante " +
                " LEFT JOIN FETCH tpe.estructuraOrganizacional " +
                " JOIN tpe.tramitePrueba tp " +
                " JOIN tp.tramite t " +
                " WHERE t.id = :tramiteId");

        List<TramitePruebaEntidad> entidads = null;

        try {
            query.setParameter("tramiteId", tramiteId);
            entidads = (List<TramitePruebaEntidad>) query.getResultList();
            for (TramitePruebaEntidad tpe : entidads) {
                if (tpe.getDireccionDepartamento() != null &&
                    tpe.getDireccionDepartamento().getNombre() != null &&
                    tpe.getDireccionMunicipio() != null &&
                    tpe.getDireccionMunicipio().getNombre() != null &&
                    tpe.getDireccionPais() != null &&
                    tpe.getDireccionPais().getNombre() != null) {

                    tpe.getDireccionDepartamento().getNombre();
                    tpe.getDireccionMunicipio().getNombre();
                    tpe.getDireccionPais().getNombre();

                    if (tpe.getSolicitante() != null) {
                        Hibernate.initialize(tpe.getSolicitante());
                    }

                    if (tpe.getEstructuraOrganizacional() != null) {
                        Hibernate.initialize(tpe.getEstructuraOrganizacional());
                    }

                    if (tpe.getRazonSocial() != null) {
                        Hibernate.initialize(tpe.getRazonSocial());
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("TramiteDAOBean#findTramitePruebaEntidadByTramiteId error: " +
                e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramitePruebaEntidadDAOBean#findTramitePruebaEntidadByTramiteId");
        }
        return entidads;
    }

}
