
/*
 * Proyecto SNC 2016
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioTerrenoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredioTerreno;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Operaciones de base de datos de la clase FichaMatrizPredioTerreno
 *
 * @author felipe.cadena
 */
@Stateless
public class FichaMatrizPredioTerrenoDAOBean extends GenericDAOWithJPA<FichaMatrizPredioTerreno, Long>
    implements IFichaMatrizPredioTerrenoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        FichaMatrizPredioTerrenoDAOBean.class);

}
