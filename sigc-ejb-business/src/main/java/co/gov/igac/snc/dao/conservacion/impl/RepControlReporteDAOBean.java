package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IReporteControlReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepControlReporte;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class RepControlReporteDAOBean extends GenericDAOWithJPA<RepControlReporte, Long> implements
    IReporteControlReporteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepControlReporteDAOBean.class);

    /**
     * @see IRepControlReporteDAO#buscarReporteControlReportePorUsuario(String usuarioLogin)
     * @author javier.aponte
     */
    @Override
    public List<RepControlReporte> buscarReporteControlReportePorUsuario(String usuarioLogin) {
        List<RepControlReporte> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT rcr FROM RepControlReporte rcr where rcr.usuario = :usuario";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("usuario", usuarioLogin);

            answer = (ArrayList<RepControlReporte>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("RepControlReporteDAOBean#buscarReporteControlPorUsuarioAndEstado: " +
                "La consulta de reporte control reporte no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

}
