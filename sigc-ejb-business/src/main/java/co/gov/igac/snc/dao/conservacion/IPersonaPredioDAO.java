package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;

public interface IPersonaPredioDAO extends IGenericJpaDAO<PersonaPredio, Long> {

    /**
     * Método encargado de ubicar un objeto de tipo PersonaPredio asociado a un Predio identificado
     * mediante su número predial.
     *
     * @param numPredial Número predial que identifica al predio.
     * @return
     * @author fabio.navarrete
     */
    public PersonaPredio getPersonaPredioById(Long idPersonaPredio);

    /**
     * Método encargado de ubicar un objeto de tipo PersonaPredio asociado a un Predio identificado
     * mediante su número predial. Inicializa además las propiedades asociadas a este predio.
     *
     * @param numPredial Número predial que identifica al predio.
     * @return
     * @author fabio.navarrete
     */
    public PersonaPredio getPersonaPredioFetchPropiedadsById(Long idPersonaPredio);

    /**
     * Método que retorna los PersonaPredios asociados a un predio
     *
     * @author franz.gamba
     * @param predioId
     * @return
     */
    public List<PersonaPredio> getPersonaPrediosByPredioId(Long predioId);

    /**
     * Método que retorna el conteo de cuantos persona predios pueden haber por persona
     *
     * @param personaId
     * @return
     */
    public Long countPersonaPrediosByPersonaId(Long personaId);

    /**
     * Método que retorna los PersonaPredios asociados a un persona
     *
     * @author leidy.gonzalez
     * @param personaId
     * @return
     */
    public List<PersonaPredio> getPersonaPrediosByPersonaId(Long personaId);

}
