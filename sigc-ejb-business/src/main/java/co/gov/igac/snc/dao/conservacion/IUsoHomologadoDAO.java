package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.UsoHomologado;
import javax.ejb.Local;

/*
 * Proyecto SNC 2017
 */
/**
 * Interfaz para la clase UsoHomologado
 *
 * @author @author felipe.cadena
 */
@Local
public interface IUsoHomologadoDAO extends IGenericJpaDAO<UsoHomologado, Long> {

}
