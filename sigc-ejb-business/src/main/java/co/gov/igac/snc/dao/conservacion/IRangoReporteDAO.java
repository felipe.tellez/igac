/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RangoReporte;
import javax.ejb.Local;

/**
 * Interfaz para metodos de acceso de datos de la entiad RangoReporte
 *
 * @author felipe.cadena
 */
@Local
public interface IRangoReporteDAO extends IGenericJpaDAO<RangoReporte, Long> {

}
