package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAsignacionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAsignacionDAO
 *
 * @author felipe.cadena
 */
@Stateless
public class AsignacionDAOBean extends GenericDAOWithJPA<Asignacion, Long> implements
    IAsignacionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AsignacionDAOBean.class);

    /**
     * @see AsignacionDAOBean#consultarAsignacionActualAvaluo(Long, String)
     * @author felipe.cadena
     */
    @Override
    public Asignacion consultarAsignacionActualAvaluo(Long idAvaluo,
        String actividad) {

        Asignacion resultado = null;

        String queryString;
        Query query;

        try {
            queryString = "SELECT a " +
                " FROM AvaluoAsignacionProfesional aap " +
                " JOIN aap.asignacion a " +
                " WHERE " +
                " aap.avaluo.id = :avaluoId " +
                " AND aap.fechaHasta IS NULL " +
                " AND aap.actividad = :actividad ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluoId", idAvaluo);
            query.setParameter("actividad", actividad);

            resultado = (Asignacion) query.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }
}
