package co.gov.igac.snc.dao.vistas.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVTramitePredioDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VTramitePredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.HashMap;

/**
 * Implementación de los servicios de persistencia del objeto {@link VTramitePredio}.
 *
 * @author david.cifuentes
 */
@Stateless
public class VTramitePredioDAOBean extends
    GenericDAOWithJPA<VTramitePredio, Long> implements IVTramitePredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VTramitePredioDAOBean.class);

    @Override
    // ---------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IVTramitePredio#buscarTramitesPorFiltros( String estadoTramite, String
     * numeroSolicitudBusqueda, String numeroRadicacionBusqueda, String numeroPredialBusqueda,
     * FiltroDatosConsultaSolicitante solicitanteBusqueda)
     */
    @SuppressWarnings("unchecked")
    public List<VTramitePredio> buscarTramitesPorFiltros(String estadoTramite,
        String numeroSolicitudBusqueda, String numeroRadicacionBusqueda,
        FiltroDatosConsultaPredio datosConsultaPredio,
        FiltroDatosConsultaSolicitante solicitanteBusqueda) {

        List<VTramitePredio> vTramitePredioResult = null;
        StringBuilder q = new StringBuilder();
        StringBuilder where = new StringBuilder();

        where.append(" WHERE 1 = 1 ");

        // Filtro por estado del trámite
        if (estadoTramite != null && !estadoTramite.trim().isEmpty()) {
            where.append(" AND t.estado = :estadoTramite ");
        }

        // Filtro número de solicitud
        if (numeroSolicitudBusqueda != null &&
            !numeroSolicitudBusqueda.trim().isEmpty()) {
            where.append(" AND t.solicitud.numero LIKE :numeroSolicitudBusqueda ");
        }

        // Filtro número de radicación
        if (numeroRadicacionBusqueda != null &&
            !numeroRadicacionBusqueda.trim().isEmpty()) {
            where.append(" AND t.numeroRadicacion LIKE :numeroRadicacionBusqueda ");
        }

        // Filtro número predial
//		Comentado para que la busqueda se pueda hacer por cualquier campo del número predial
//		if (numeroPredialBusqueda != null
//				&& !numeroPredialBusqueda.trim().isEmpty()) {
//			//where.append(" AND (p.numeroPredial = :numeroPredialBusqueda OR ptpe.numeroPredial = :numeroPredialBusqueda ) ");
//			where.append(" AND ("
//					+ " vtp.predio.id IN"
//					+ " ( SELECT pre.id FROM Predio pre WHERE pre.numeroPredial = :numeroPredialBusqueda)"
//					+ " AND t.id = vtp.tramite.id )");
//		}
        if (datosConsultaPredio != null) {
            if (datosConsultaPredio.getNumeroPredialS1() != null &&
                !datosConsultaPredio.getNumeroPredialS1().isEmpty()) {
                where.append(" AND vtp.predio.departamento.codigo like :departamentoCodigo");
            }

            if (datosConsultaPredio.getNumeroPredialS2() != null &&
                !datosConsultaPredio.getNumeroPredialS2().isEmpty()) {
                where.append(" AND vtp.predio.municipio.codigo like :municipioCodigo");
            }

            if (datosConsultaPredio.getNumeroPredialS3() != null &&
                !datosConsultaPredio.getNumeroPredialS3().isEmpty()) {
                where.append(" AND vtp.predio.tipoAvaluo = :tipoAvaluo");
            }

            if (datosConsultaPredio.getNumeroPredialS4() != null &&
                !datosConsultaPredio.getNumeroPredialS4().isEmpty()) {
                where.append(" AND vtp.predio.sectorCodigo = :sectorCodigo");
            }

            if (datosConsultaPredio.getNumeroPredialS5() != null &&
                !datosConsultaPredio.getNumeroPredialS5().isEmpty()) {
                where.append(" AND vtp.predio.barrioCodigo LIKE :comunaCodigo");
            }

            if (datosConsultaPredio.getNumeroPredialS6() != null &&
                !datosConsultaPredio.getNumeroPredialS6().isEmpty()) {
                where.append(" AND vtp.predio.barrioCodigo LIKE :barrioCodigo");
            }

            if (datosConsultaPredio.getNumeroPredialS7() != null &&
                !datosConsultaPredio.getNumeroPredialS7().isEmpty()) {
                where.append(" AND vtp.predio.manzanaCodigo = :manzanaCodigo");
            }

            if (datosConsultaPredio.getNumeroPredialS8() != null &&
                !datosConsultaPredio.getNumeroPredialS8().isEmpty()) {
                where.append(" AND vtp.predio.predio = :predio");
            }

            if (datosConsultaPredio.getNumeroPredialS9() != null &&
                !datosConsultaPredio.getNumeroPredialS9().isEmpty()) {
                where.append(" AND vtp.predio.condicionPropiedad = :condicionPropiedad");
            }

            if (datosConsultaPredio.getNumeroPredialS10() != null &&
                !datosConsultaPredio.getNumeroPredialS10().isEmpty()) {
                where.append(" AND vtp.predio.edificio = :edificio");
            }

            if (datosConsultaPredio.getNumeroPredialS11() != null &&
                !datosConsultaPredio.getNumeroPredialS11().isEmpty()) {
                where.append(" AND vtp.predio.piso = :piso");
            }

            if (datosConsultaPredio.getNumeroPredialS12() != null &&
                !datosConsultaPredio.getNumeroPredialS12().isEmpty()) {
                where.append(" AND vtp.predio.unidad = :unidad");
            }

            // Filtro por territorial
            if (datosConsultaPredio.getTerritorialId() != null &&
                !datosConsultaPredio.getTerritorialId().trim().isEmpty()) {
                where.append(
                    " AND (eos.estructuraOrganizacional.codigo = :codigoTerritorialBusqueda OR eos.estructuraOrganizacional.estructuraOrganizacionalCod = :codigoTerritorialBusqueda) ");
            }
        }
        // Filtro solicitante
        if (solicitanteBusqueda != null) {

            // Filtro por tipo de identificación
            if (solicitanteBusqueda.getTipoIdentificacion() != null &&
                !solicitanteBusqueda.getTipoIdentificacion().trim()
                    .isEmpty()) {
                where.append(" AND ( st.tipoIdentificacion = :tipoIdentificacion " +
                    " OR ss.tipoIdentificacion = :tipoIdentificacion ) ");
            }

            // Filtro por número de identificación
            if (solicitanteBusqueda.getNumeroIdentificacion() != null &&
                !solicitanteBusqueda.getNumeroIdentificacion().trim()
                    .isEmpty()) {
                where.append(" AND ( st.numeroIdentificacion = :numeroIdentificacion " +
                    " OR ss.numeroIdentificacion = :numeroIdentificacion ) ");
            }

            // Filtro por el dígito de verificación
            if (solicitanteBusqueda.getDigitoVerificacion() != null &&
                !solicitanteBusqueda.getDigitoVerificacion().trim()
                    .isEmpty()) {
                where.append(" AND ( st.digitoVerificacion = :digitoVerificacion " +
                    " OR ss.digitoVerificacion = :digitoVerificacion ) ");
            }

            // Filtro por primer nombre
            if (solicitanteBusqueda.getPrimerNombre() != null &&
                !solicitanteBusqueda.getPrimerNombre().trim().isEmpty()) {
                where.append(" AND ( UPPER(st.primerNombre) = :primerNombre " +
                    " OR UPPER(ss.primerNombre) = :primerNombre ) ");
            }

            // Filtro por segundo nombre
            if (solicitanteBusqueda.getSegundoNombre() != null &&
                !solicitanteBusqueda.getSegundoNombre().trim().isEmpty()) {
                where.append(" AND ( UPPER(st.segundoNombre) = :segundoNombre " +
                    " OR UPPER(ss.segundoNombre) = :segundoNombre ) ");
            }

            // Filtro por primer apellido
            if (solicitanteBusqueda.getPrimerApellido() != null &&
                !solicitanteBusqueda.getPrimerApellido().trim()
                    .isEmpty()) {
                where.append(" AND ( UPPER(st.primerApellido) = :primerApellido " +
                    " OR UPPER(ss.primerApellido) = :primerApellido ) ");
            }

            // Filtro por segundo apellido
            if (solicitanteBusqueda.getSegundoApellido() != null &&
                !solicitanteBusqueda.getSegundoApellido().trim()
                    .isEmpty()) {
                where.append(" AND ( UPPER(st.segundoApellido) = :segundoApellido " +
                    " OR UPPER(ss.segundoApellido) = :segundoApellido ) ");
            }
        }

        q.append("SELECT DISTINCT vtp FROM VTramitePredio vtp " +
            " LEFT JOIN FETCH vtp.tramite t " +
            " LEFT JOIN FETCH t.municipio m" +
            " LEFT JOIN FETCH t.departamento d" +
            " LEFT JOIN t.municipio.jurisdiccions eos" +
            " LEFT JOIN FETCH vtp.predio predio " +
            " LEFT JOIN FETCH t.predio p " +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe " +
            " LEFT JOIN FETCH tpe.predio ptpe " +
            " LEFT JOIN FETCH t.solicitud s " +
            " LEFT JOIN s.solicitanteSolicituds ss " +
            " LEFT JOIN t.solicitanteTramites st " + where);

        try {
            Query query = this.entityManager.createQuery(q.toString());
            LOGGER.debug("SQL : " + q.toString());

            if (numeroSolicitudBusqueda != null &&
                !numeroSolicitudBusqueda.trim().isEmpty()) {
                query.setParameter("numeroSolicitudBusqueda",
                    "%" + numeroSolicitudBusqueda + "%");
            }

            if (estadoTramite != null && !estadoTramite.trim().isEmpty()) {
                query.setParameter("estadoTramite", estadoTramite);
            }

            if (numeroRadicacionBusqueda != null &&
                !numeroRadicacionBusqueda.trim().isEmpty()) {
                query.setParameter("numeroRadicacionBusqueda",
                    "%" + numeroRadicacionBusqueda + "%");
            }
            if (datosConsultaPredio != null) {

                if (datosConsultaPredio.getNumeroPredialS1() != null &&
                    !datosConsultaPredio.getNumeroPredialS1().isEmpty()) {
                    query.setParameter("departamentoCodigo",
                        datosConsultaPredio.getNumeroPredialS1());
                }

                if (datosConsultaPredio.getNumeroPredialS2() != null &&
                    !datosConsultaPredio.getNumeroPredialS2().isEmpty()) {
                    query.setParameter("municipioCodigo",
                        datosConsultaPredio.getNumeroPredialS1() +
                        datosConsultaPredio.getNumeroPredialS2());
                }

                if (datosConsultaPredio.getNumeroPredialS3() != null &&
                    !datosConsultaPredio.getNumeroPredialS3().isEmpty()) {
                    query.setParameter("tipoAvaluo",
                        datosConsultaPredio.getNumeroPredialS3());
                }

                if (datosConsultaPredio.getNumeroPredialS4() != null &&
                    !datosConsultaPredio.getNumeroPredialS4().isEmpty()) {
                    query.setParameter("sectorCodigo",
                        datosConsultaPredio.getNumeroPredialS4());
                }

                if (datosConsultaPredio.getNumeroPredialS5() != null &&
                    !datosConsultaPredio.getNumeroPredialS5().isEmpty()) {
                    query.setParameter("comunaCodigo",
                        datosConsultaPredio.getNumeroPredialS5() + "%");
                }

                if (datosConsultaPredio.getNumeroPredialS6() != null &&
                    !datosConsultaPredio.getNumeroPredialS6().isEmpty()) {
                    query.setParameter("barrioCodigo", "%" +
                        datosConsultaPredio.getNumeroPredialS6());
                }

                if (datosConsultaPredio.getNumeroPredialS7() != null &&
                    !datosConsultaPredio.getNumeroPredialS7().isEmpty()) {
                    query.setParameter("manzanaCodigo",
                        datosConsultaPredio.getNumeroPredialS7());
                }

                if (datosConsultaPredio.getNumeroPredialS8() != null &&
                    !datosConsultaPredio.getNumeroPredialS8().isEmpty()) {
                    query.setParameter("predio",
                        datosConsultaPredio.getNumeroPredialS8());
                }

                if (datosConsultaPredio.getNumeroPredialS9() != null &&
                    !datosConsultaPredio.getNumeroPredialS9().isEmpty()) {
                    query.setParameter("condicionPropiedad",
                        datosConsultaPredio.getNumeroPredialS9());
                }

                if (datosConsultaPredio.getNumeroPredialS10() != null &&
                    !datosConsultaPredio.getNumeroPredialS10().isEmpty()) {
                    query.setParameter("edificio",
                        datosConsultaPredio.getNumeroPredialS10());
                }

                if (datosConsultaPredio.getNumeroPredialS11() != null &&
                    !datosConsultaPredio.getNumeroPredialS11().isEmpty()) {
                    query.setParameter("piso",
                        datosConsultaPredio.getNumeroPredialS11());
                }

                if (datosConsultaPredio.getNumeroPredialS12() != null &&
                    !datosConsultaPredio.getNumeroPredialS12().isEmpty()) {
                    query.setParameter("unidad",
                        datosConsultaPredio.getNumeroPredialS12());
                }

                if (datosConsultaPredio.getTerritorialId() != null &&
                    !datosConsultaPredio.getTerritorialId().trim().isEmpty()) {
                    query.setParameter("codigoTerritorialBusqueda", datosConsultaPredio.
                        getTerritorialId());
                }
            }

            if (solicitanteBusqueda != null) {

                if (solicitanteBusqueda.getTipoIdentificacion() != null &&
                    !solicitanteBusqueda.getTipoIdentificacion().trim()
                        .isEmpty()) {
                    query.setParameter("tipoIdentificacion",
                        solicitanteBusqueda.getTipoIdentificacion());
                }
                if (solicitanteBusqueda.getNumeroIdentificacion() != null &&
                    !solicitanteBusqueda.getNumeroIdentificacion()
                        .trim().isEmpty()) {
                    query.setParameter("numeroIdentificacion",
                        solicitanteBusqueda.getNumeroIdentificacion());
                }
                if (solicitanteBusqueda.getDigitoVerificacion() != null &&
                    !solicitanteBusqueda.getDigitoVerificacion().trim()
                        .isEmpty()) {
                    query.setParameter("digitoVerificacion",
                        solicitanteBusqueda.getDigitoVerificacion());
                }
                if (solicitanteBusqueda.getPrimerNombre() != null &&
                    !solicitanteBusqueda.getPrimerNombre().trim()
                        .isEmpty()) {
                    query.setParameter("primerNombre", solicitanteBusqueda
                        .getPrimerNombre().toUpperCase());
                }
                if (solicitanteBusqueda.getSegundoNombre() != null &&
                    !solicitanteBusqueda.getSegundoNombre().trim()
                        .isEmpty()) {
                    query.setParameter("segundoNombre", solicitanteBusqueda
                        .getSegundoNombre().toUpperCase());
                }
                if (solicitanteBusqueda.getPrimerApellido() != null &&
                    !solicitanteBusqueda.getPrimerApellido().trim()
                        .isEmpty()) {
                    query.setParameter("primerApellido", solicitanteBusqueda
                        .getPrimerApellido().toUpperCase());
                }
                if (solicitanteBusqueda.getSegundoApellido() != null &&
                    !solicitanteBusqueda.getSegundoApellido().trim()
                        .isEmpty()) {
                    query.setParameter("segundoApellido", solicitanteBusqueda
                        .getSegundoApellido().toUpperCase());
                }
            }

            vTramitePredioResult = query.getResultList();
            for (VTramitePredio vptIterator : vTramitePredioResult) {
                vptIterator.getTramite().getTramites().size();
                vptIterator.getTramite().getSolicitanteTramites().size();
                vptIterator.getTramite().getSolicitud()
                    .getSolicitanteSolicituds().size();
            }

            return vTramitePredioResult;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ExcepcionSNC(
                "Error en el método buscarTramitesPorFiltros.",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), e);
        }
    }

    // ----------------------------------------------------------------- //
    /**
     * @see IVTramitePredio#consultaDeTramitesPorFiltros(FiltroDatosConsultaTramite filtrosTramite,
     * FiltroDatosConsultaPredio filtrosPredio, FiltroDatosConsultaSolicitante filtrosSolicitante)
     *
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia :: 22-07-2013 :: cambio de tipo de dato respueata para mejorar
     * comparacione en las otras capas
     *
     * @modified by juanfelipe.garcia :: 20-09-2013 :: adición de condición de búsqueda por
     * territorial
     */
    @SuppressWarnings("unchecked")
    @Override
    public HashMap<Long, Tramite> consultaDeTramitesPorFiltros(
        FiltroDatosConsultaTramite filtrosTramite,
        FiltroDatosConsultaPredio filtrosPredio,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String codigoMunicipioTerritorialSeleccionada,
        int maxResults) {

        List<VTramitePredio> vTramitePredioResult = null;
        HashMap<Long, Tramite> resultado = new HashMap<Long, Tramite>();
        StringBuilder q = new StringBuilder();
        StringBuilder where = new StringBuilder();
//		String estadoTramite = ETramiteEstado.RECHAZADO.getCodigo();
        boolean isMutacionDeCuarta = false;
        try {

            where.append(" WHERE 1 = 1 ");
//			if (codigoMunicipioTerritorialSeleccionada != null
//					&& !codigoMunicipioTerritorialSeleccionada.trim().isEmpty())
//			{
//				where.append(" AND SUBSTR(t.numeroRadicacion,1, 5) = " + "'" + codigoMunicipioTerritorialSeleccionada + "'");
//			}
            // FILTROS DEL TRÁMITE
            if (filtrosTramite != null) {

                if (filtrosTramite.getClaseMutacion() != null &&
                    !filtrosTramite.getClaseMutacion().trim().isEmpty() &&
                    filtrosTramite.getClaseMutacion().equals("4")) {
                    isMutacionDeCuarta = true;
                }

                // Filtro del número de solicitud
                if (filtrosTramite.getNumeroSolicitud() != null &&
                    !filtrosTramite.getNumeroSolicitud().trim().isEmpty()) {
                    where.append(" AND t.solicitud.numero LIKE :numeroSolicitudBusqueda ");
                }

                // Filtro número de radicación
                if (filtrosTramite.getNumeroRadicacion() != null &&
                    !filtrosTramite.getNumeroRadicacion().trim().isEmpty()) {
                    where.append(" AND t.numeroRadicacion LIKE :numeroRadicacionBusqueda ");
                }

                // Filtro por fecha de radicación
                if (filtrosTramite.getFechaRadicacion() != null) {
                    where.append(
                        " AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2 ");
                }

                // Filtro por tipo de solicitud
                if (filtrosTramite.getTipoSolicitud() != null &&
                    !filtrosTramite.getTipoSolicitud().trim().isEmpty() &&
                    !isMutacionDeCuarta) {
                    where.append(" AND t.solicitud.tipo = :tipo ");
                }

                // Filtro por tipo de trámite
                if (filtrosTramite.getTipoTramite() != null &&
                    !filtrosTramite.getTipoTramite().trim().isEmpty()) {
                    where.append(" AND t.tipoTramite = :tipoTramite ");
                }

                // Filtro por clase de mutación
                if (filtrosTramite.getClaseMutacion() != null &&
                    !filtrosTramite.getClaseMutacion().trim().isEmpty()) {
                    where.append(" AND t.claseMutacion = :claseMutacion ");
                }

                // Filtro por subtipo
                if (filtrosTramite.getSubtipo() != null &&
                    !filtrosTramite.getSubtipo().trim().isEmpty()) {
                    where.append(" AND t.subtipo = :subtipo ");
                }

                // Filtro por estado del trámite
                //juanfelipe:garcia :: 30/05/2013 :: si viene en 0, se deja la logica original del metodo
                //si el valor es diferente, busca por el estado del filtro (CU 181)
                if (filtrosTramite.getEstadoTramite() != null) {
                    where.append(" AND t.estado in :estadoTramite ");
                }

                // Filtro por territorial
                if (filtrosTramite.getCodidoUnidadOperativa() != null &&
                    !filtrosTramite.getCodidoUnidadOperativa().trim().isEmpty()) {
                    where.append(
                        " AND eos.estructuraOrganizacional.codigo = :codigoTerritorialBusqueda ");
                } else {
                    if (filtrosTramite.getTerritorialId() != null &&
                        !filtrosTramite.getTerritorialId().trim().isEmpty()) {
                        where.append(
                            " AND (eos.estructuraOrganizacional.codigo = :codigoTerritorialBusqueda OR eos.estructuraOrganizacional.estructuraOrganizacionalCod = :codigoTerritorialBusqueda) ");
                    }
                }
                
             // Filtro por funcionario ejecutor
                if (filtrosTramite.getFuncionarioEjecutor() != null &&
                    !filtrosTramite.getFuncionarioEjecutor().trim().isEmpty()) {
                    where.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor ");
                }

            }

            // FILTROS DEL PREDIO
            if (filtrosPredio != null) {
                if (filtrosPredio.getNumeroPredialS1() != null && !filtrosPredio.
                    getNumeroPredialS1().isEmpty()) {//08
                    where.append(" AND vtp.predio.departamento.codigo = :departamentoCodigo");
                }
                if (filtrosPredio.getNumeroPredialS2() != null && !filtrosPredio.
                    getNumeroPredialS2().isEmpty()) {//001
                    where.append(" AND vtp.predio.municipio.codigo = :municipioCodigo");
                }
                if (filtrosPredio.getNumeroPredialS3() != null && !filtrosPredio.
                    getNumeroPredialS3().isEmpty()) {//01
                    where.append(" AND vtp.predio.tipoAvaluo = :tipoAvaluo");
                }
                if (filtrosPredio.getNumeroPredialS4() != null && !filtrosPredio.
                    getNumeroPredialS4().isEmpty()) {//01
                    where.append(" AND vtp.predio.sectorCodigo = :sectorCodigo");
                }
                if (filtrosPredio.getNumeroPredialS5() != null &&
                    !filtrosPredio.getNumeroPredialS5().isEmpty() &&
                    filtrosPredio.getNumeroPredialS6() != null &&
                    !filtrosPredio.getNumeroPredialS6().isEmpty()) {//00 00
                    where.append(" AND vtp.predio.barrioCodigo = :barrioCodigo");
                }
                if (filtrosPredio.getNumeroPredialS7() != null && !filtrosPredio.
                    getNumeroPredialS7().isEmpty()) {//0003
                    where.append(" AND vtp.predio.manzanaCodigo = :manzanaCodigo");
                }
                if (filtrosPredio.getNumeroPredialS8() != null && !filtrosPredio.
                    getNumeroPredialS8().isEmpty()) {//0003
                    where.append(" AND vtp.predio.predio = :predio");
                }
                if (filtrosPredio.getNumeroPredialS9() != null && !filtrosPredio.
                    getNumeroPredialS9().isEmpty()) {//5
                    where.append(" AND vtp.predio.condicionPropiedad = :condicionPropiedad");
                }
                if (filtrosPredio.getNumeroPredialS10() != null && !filtrosPredio.
                    getNumeroPredialS10().isEmpty()) {//00
                    where.append(" AND vtp.predio.edificio = :edificio");
                }
                if (filtrosPredio.getNumeroPredialS11() != null && !filtrosPredio.
                    getNumeroPredialS11().isEmpty()) {//00
                    where.append(" AND vtp.predio.piso = :piso");
                }
                if (filtrosPredio.getNumeroPredialS12() != null && !filtrosPredio.
                    getNumeroPredialS12().isEmpty()) {//001
                    where.append(" AND vtp.predio.unidad = :unidad");
                }
            }

            // FILTROS DEL SOLICITANTE
            if (filtrosSolicitante != null) {

                // Filtro por tipo de identificación
                if (filtrosSolicitante.getTipoIdentificacion() != null &&
                    !filtrosSolicitante.getTipoIdentificacion().trim().isEmpty()) {
                    where.append(" AND ( st.tipoIdentificacion = :tipoIdentificacion " +
                        " OR ss.tipoIdentificacion = :tipoIdentificacion ) ");
                }

                // Filtro por número de identificación
                if (filtrosSolicitante.getNumeroIdentificacion() != null &&
                    !filtrosSolicitante.getNumeroIdentificacion().trim().isEmpty()) {

                    where.append(" AND ( st.numeroIdentificacion = :numeroIdentificacion " +
                        " OR ss.numeroIdentificacion = :numeroIdentificacion ) ");
                }

                // Filtro por el dígito de verificación
                if (filtrosSolicitante.getDigitoVerificacion() != null &&
                    !filtrosSolicitante.getDigitoVerificacion().trim()
                        .isEmpty()) {

                    where.append(" AND ( st.digitoVerificacion = :digitoVerificacion " +
                        " OR ss.digitoVerificacion = :digitoVerificacion ) ");
                }

                // Filtro por primer nombre
                if (filtrosSolicitante.getPrimerNombre() != null &&
                    !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {

                    where.append(" AND ( UPPER(st.primerNombre) = :primerNombre " +
                        " OR UPPER(ss.primerNombre) = :primerNombre ) ");
                }

                // Filtro por segundo nombre
                if (filtrosSolicitante.getSegundoNombre() != null &&
                    !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {

                    where.append(" AND ( UPPER(st.segundoNombre) = :segundoNombre " +
                        " OR UPPER(ss.segundoNombre) = :segundoNombre ) ");
                }

                // Filtro por primer apellido
                if (filtrosSolicitante.getPrimerApellido() != null &&
                    !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {

                    where.append(" AND ( UPPER(st.primerApellido) = :primerApellido " +
                        " OR UPPER(ss.primerApellido) = :primerApellido ) ");
                }

                // Filtro por segundo apellido
                if (filtrosSolicitante.getSegundoApellido() != null &&
                    !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {

                    where.append(" AND ( UPPER(st.segundoApellido) = :segundoApellido " +
                        " OR UPPER(ss.segundoApellido) = :segundoApellido ) ");
                }

                // Filtro por razón social
                if (filtrosSolicitante.getRazonSocial() != null &&
                    !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {

                    where.append(" AND ( UPPER(st.razonSocial) = :razonSocial " +
                        " OR UPPER(ss.razonSocial) = :razonSocial ) ");
                }
            }

            q.append("SELECT DISTINCT vtp FROM VTramitePredio vtp " +
                " LEFT JOIN FETCH vtp.tramite t " +
                " LEFT JOIN FETCH t.municipio m" +
                " LEFT JOIN t.municipio.jurisdiccions eos" +
                " LEFT JOIN FETCH t.departamento d" +
                " LEFT JOIN FETCH vtp.predio predio " //					+ " LEFT JOIN FETCH t.predio p "
                +
                 " LEFT JOIN FETCH t.tramitePredioEnglobes tpe " //					+ " LEFT JOIN FETCH tpe.predio ptpe "
                +
                 " LEFT JOIN FETCH t.solicitud s " +
                " LEFT JOIN FETCH t.resultadoDocumento trd " +
                " LEFT JOIN s.solicitanteSolicituds ss " +
                " LEFT JOIN t.solicitanteTramites st ");
            q.append(where);
            q.append(" ORDER BY t.id DESC");

            Query query = this.entityManager.createQuery(q.toString());
            LOGGER.debug("SQL : " + q.toString());

            if (filtrosTramite != null) {

                if (filtrosTramite.getCodidoUnidadOperativa() != null &&
                    !filtrosTramite.getCodidoUnidadOperativa().trim().isEmpty()) {
                    query.setParameter("codigoTerritorialBusqueda", filtrosTramite.
                        getCodidoUnidadOperativa());
                } else {
                    if (filtrosTramite.getTerritorialId() != null &&
                        !filtrosTramite.getTerritorialId().trim().isEmpty()) {
                        query.setParameter("codigoTerritorialBusqueda", filtrosTramite.
                            getTerritorialId());
                    }
                }

                if (filtrosTramite.getNumeroSolicitud() != null &&
                    !filtrosTramite.getNumeroSolicitud().trim().isEmpty()) {
                    query.setParameter("numeroSolicitudBusqueda", "%" + filtrosTramite.
                        getNumeroSolicitud() + "%");
                }

                if (filtrosTramite.getNumeroRadicacion() != null &&
                    !filtrosTramite.getNumeroRadicacion().trim().isEmpty()) {
                    query.setParameter("numeroRadicacionBusqueda", "%" + filtrosTramite.
                        getNumeroRadicacion() + "%");
                }

                if (filtrosTramite.getFechaRadicacion() != null) {
                    query.setParameter("fechaRadicacion1", filtrosTramite.getFechaRadicacion());

                }
                //refs #7478 - ajuste en la consulta por fechas
                if (filtrosTramite.getFechaRadicacionFinal() != null) {
                    Date hasta = filtrosTramite.getFechaRadicacionFinal();
                    Calendar c = Calendar.getInstance();
                    c.setTime(hasta);
                    c.add(Calendar.DAY_OF_MONTH, 1);
                    query.setParameter("fechaRadicacion2", c.getTime());

                } else if (filtrosTramite.getFechaRadicacion() != null &&
                    filtrosTramite.getFechaRadicacionFinal() == null) {
                    Date hasta = filtrosTramite.getFechaRadicacion();
                    Calendar c = Calendar.getInstance();
                    c.setTime(hasta);
                    c.add(Calendar.DATE, 1);
                    query.setParameter("fechaRadicacion2", c.getTime());
                }

                if (filtrosTramite.getTipoSolicitud() != null &&
                    !filtrosTramite.getTipoSolicitud().trim().isEmpty() &&
                    !isMutacionDeCuarta) {
                    query.setParameter("tipo", filtrosTramite.getTipoSolicitud());
                }

                if (filtrosTramite.getTipoTramite() != null &&
                    !filtrosTramite.getTipoTramite().trim().isEmpty()) {
                    query.setParameter("tipoTramite", filtrosTramite.getTipoTramite());
                }

                if (filtrosTramite.getClaseMutacion() != null &&
                    !filtrosTramite.getClaseMutacion().trim().isEmpty()) {
                    query.setParameter("claseMutacion", filtrosTramite.getClaseMutacion());
                }

                if (filtrosTramite.getSubtipo() != null &&
                    !filtrosTramite.getSubtipo().trim().isEmpty()) {
                    query.setParameter("subtipo", filtrosTramite.getSubtipo());
                }

                //juanfelipe:garcia :: 30/05/2013 :: ajuste para manejar el estado del tramite 
                //cuando es cancelado, finalizado o archivado (CU - 181)
                if (filtrosTramite.getEstadoTramite() != null &&
                    !filtrosTramite.getEstadoTramite().trim().isEmpty()) {
                    query.setParameter("estadoTramite", Arrays.asList(filtrosTramite.
                        getEstadoTramite().split(",")));
                }
                
             // Filtro por funcionario ejecutor
                if (filtrosTramite.getFuncionarioEjecutor() != null &&
                    !filtrosTramite.getFuncionarioEjecutor().trim().isEmpty()) {
                	query.setParameter("funcionarioEjecutor",filtrosTramite.getFuncionarioEjecutor());
                }

                
            }

            if (filtrosPredio != null) {
                if (filtrosPredio.getNumeroPredialS1() != null &&
                    !filtrosPredio.getNumeroPredialS1().isEmpty()) {
                    query.setParameter("departamentoCodigo", filtrosPredio.getNumeroPredialS1());
                }

                if (filtrosPredio.getNumeroPredialS2() != null &&
                    !filtrosPredio.getNumeroPredialS2().isEmpty()) {
                    query.setParameter("municipioCodigo", filtrosPredio.getNumeroPredialS1() +
                        filtrosPredio.getNumeroPredialS2());
                }

                if (filtrosPredio.getNumeroPredialS3() != null &&
                    !filtrosPredio.getNumeroPredialS3().isEmpty()) {
                    query.setParameter("tipoAvaluo", filtrosPredio.getNumeroPredialS3());
                }

                if (filtrosPredio.getNumeroPredialS4() != null &&
                    !filtrosPredio.getNumeroPredialS4().isEmpty()) {
                    query.setParameter("sectorCodigo", filtrosPredio.getNumeroPredialS4());
                }
                //refs #6559 - ajuste en la consulta por número predial
                if (filtrosPredio.getNumeroPredialS5() != null &&
                    !filtrosPredio.getNumeroPredialS5().isEmpty() &&
                    filtrosPredio.getNumeroPredialS6() != null &&
                    !filtrosPredio.getNumeroPredialS6().isEmpty()) {
                    query.setParameter("barrioCodigo", filtrosPredio.getNumeroPredialS5() +
                        filtrosPredio.getNumeroPredialS6());
                }

                if (filtrosPredio.getNumeroPredialS7() != null &&
                    !filtrosPredio.getNumeroPredialS7().isEmpty()) {
                    query.setParameter("manzanaCodigo", filtrosPredio.getNumeroPredialS7());
                }

                if (filtrosPredio.getNumeroPredialS8() != null &&
                    !filtrosPredio.getNumeroPredialS8().isEmpty()) {
                    query.setParameter("predio", filtrosPredio.getNumeroPredialS8());
                }

                if (filtrosPredio.getNumeroPredialS9() != null &&
                    !filtrosPredio.getNumeroPredialS9().isEmpty()) {
                    query.setParameter("condicionPropiedad", filtrosPredio.getNumeroPredialS9());
                }

                if (filtrosPredio.getNumeroPredialS10() != null &&
                    !filtrosPredio.getNumeroPredialS10().isEmpty()) {
                    query.setParameter("edificio", filtrosPredio.getNumeroPredialS10());
                }

                if (filtrosPredio.getNumeroPredialS11() != null &&
                    !filtrosPredio.getNumeroPredialS11().isEmpty()) {
                    query.setParameter("piso", filtrosPredio.getNumeroPredialS11());
                }

                if (filtrosPredio.getNumeroPredialS12() != null &&
                    !filtrosPredio.getNumeroPredialS12().isEmpty()) {
                    query.setParameter("unidad", filtrosPredio.getNumeroPredialS12());
                }
            }

            if (filtrosSolicitante != null) {

                if (filtrosSolicitante.getTipoIdentificacion() != null &&
                    !filtrosSolicitante.getTipoIdentificacion().trim()
                        .isEmpty()) {
                    query.setParameter("tipoIdentificacion", filtrosSolicitante.
                        getTipoIdentificacion());
                }
                if (filtrosSolicitante.getNumeroIdentificacion() != null &&
                    !filtrosSolicitante.getNumeroIdentificacion()
                        .trim().isEmpty()) {
                    query.setParameter("numeroIdentificacion", filtrosSolicitante.
                        getNumeroIdentificacion());
                }
                if (filtrosSolicitante.getDigitoVerificacion() != null &&
                    !filtrosSolicitante.getDigitoVerificacion().trim().isEmpty()) {
                    query.setParameter("digitoVerificacion", filtrosSolicitante.
                        getDigitoVerificacion());
                }
                if (filtrosSolicitante.getPrimerNombre() != null &&
                    !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {
                    query.setParameter("primerNombre", filtrosSolicitante.getPrimerNombre().
                        toUpperCase());
                }
                if (filtrosSolicitante.getSegundoNombre() != null &&
                    !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {
                    query.setParameter("segundoNombre", filtrosSolicitante.getSegundoNombre().
                        toUpperCase());
                }
                if (filtrosSolicitante.getPrimerApellido() != null &&
                    !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {
                    query.setParameter("primerApellido", filtrosSolicitante.getPrimerApellido().
                        toUpperCase());
                }
                if (filtrosSolicitante.getSegundoApellido() != null &&
                    !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {
                    query.setParameter("segundoApellido", filtrosSolicitante.getSegundoApellido().
                        toUpperCase());
                }
                if (filtrosSolicitante.getRazonSocial() != null &&
                    !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {
                    query.setParameter("razonSocial", filtrosSolicitante.getRazonSocial().
                        toUpperCase());
                }
            }
            
            vTramitePredioResult = query
                .getResultList();

            for (VTramitePredio vptIterator : vTramitePredioResult) {
                resultado.put(vptIterator.getTramite().getId(), vptIterator.getTramite());
                if (resultado.size() >= maxResults) {
                    break;
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

}
