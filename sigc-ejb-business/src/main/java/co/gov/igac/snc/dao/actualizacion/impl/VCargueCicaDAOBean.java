package co.gov.igac.snc.dao.actualizacion.impl;
/*
 * Proyecto SNC 2019
 */

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.persistence.entity.actualizacion.VCargueCica;
import co.gov.igac.snc.util.FiltroCargueDTO;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Persistencia asociada a las clase VCargueCica
 *
 * @author felipe.cadena
 */
@Stateless
public class VCargueCicaDAOBean extends GenericDAOWithJPA<VCargueCica, Long> implements IVCargueCicaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(VCargueCicaDAOBean.class);

    /**
     * @author felipe.cadena
     * @see IVCargueCicaDAO#buscarPrediosProyectados(List<String>)
     */
    @Override
    public List<VCargueCica> buscarPrediosProyectados(List<String> predios) {
        List<VCargueCica> resultado = null;

        String queryString = "SELECT cv FROM VCargueCica cv ";

        List<Object> objectPredios = new ArrayList<Object>(predios);



        String condicion  = "WHERE 1=1 ";

        condicion += " AND ("+ Utilidades.compileOrListCondition(objectPredios,
                800, "cv.numeroPredialCica") +")";

        queryString += condicion;


        try {
            Query query = this.entityManager.createQuery(queryString.toString());

            query = Utilidades.compileOrListParams(objectPredios,
                    800, query);
            resultado = query.getResultList();

        } catch (NoResultException e) {
            return new ArrayList<VCargueCica>();
        } catch (Exception e) {
            LOGGER.error("error on IVCargueCicaDAO#buscarPrediosProyectados: " + e.
                    getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                    "IVCargueCicaDAO#buscarPrediosProyectados");
        }

        return resultado;
    }

    /**
     * @author felipe.cadena
     * @see IVCargueCicaDAO#buscarResumenPredios(FiltroCargueDTO)
     */
    @Override
    public List<Object[]> buscarResumenPredios(FiltroCargueDTO filtroCargueDTO) {
        List<Object[]> resultado = null;

        String queryString = "SELECT tipo_avaluo, sector_codigo, manzana_codigo, count(*) FROM V_CARGUE_CICA " +
                " WHERE MUNICIPIO_CODIGO = "+filtroCargueDTO.getMunicipioCodigo()+" AND VIGENCIA = "+ filtroCargueDTO.getVigencia() +
                " GROUP BY  tipo_avaluo, sector_codigo, manzana_codigo";

        try {
            Query query = this.entityManager.createNativeQuery(queryString);

            resultado = query.getResultList();

        } catch (Exception e) {
            LOGGER.error("error on IVCargueCicaDAO#buscarResumenPredios: " + e.
                    getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                    "IVCargueCicaDAO#buscarResumenPredios");
        }

        return resultado;
    }

    /**
     * @author felipe.cadena
     * @see IVCargueCicaDAO#buscarRangosFiltros(FiltroCargueDTO)
     */
    @Override
    public Object[] buscarRangosFiltros(FiltroCargueDTO filtroCargueDTO) {
        Object[] resultado = null;

        String queryString = "SELECT MUNICIPIO_CODIGO, VIGENCIA, " +
                "min(area_terreno_inicial) ati_min, " +
                "max(area_terreno_inicial) ati_max, " +
                "min(area_terreno_final) atf_min, " +
                "max(area_terreno_final) atf_max, " +
                "min(area_terreno_variacion) atv_min," +
                "max(area_terreno_variacion) atv_max," +
                "min(area_construccion_inicial) aci_min, " +
                "max(area_construccion_inicial) aci_max, " +
                "min(area_construccion_final) acf_min, " +
                "max(area_construccion_final) acf_max, " +
                "min(area_construccion_variacion) acv_min, " +
                "max(area_construccion_variacion) acv_max, " +
                "min(avaluo_inicial) avi_min, " +
                "max(avaluo_inicial) avi_max, " +
                "min(avaluo_final) avf_min, " +
                "max(avaluo_final) avf_max, " +
                "min(avaluo_variacion) avv_min, " +
                "max(avaluo_variacion) avv_max " +
                "FROM V_CARGUE_CICA " +
                " WHERE MUNICIPIO_CODIGO = "+filtroCargueDTO.getMunicipioCodigo()+" AND VIGENCIA = "+ filtroCargueDTO.getVigencia() +
                " GROUP BY  MUNICIPIO_CODIGO, VIGENCIA ";

        try {
            Query query = this.entityManager.createNativeQuery(queryString);

            resultado = (Object[]) query.getSingleResult();

        } catch (NoResultException e) {
           return null;
        } catch (Exception e) {
            LOGGER.error("error on IVCargueCicaDAO#buscarRangosFiltros: " + e.
                    getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                    "IVCargueCicaDAO#buscarRangosFiltros");
        }

        return resultado;
    }

    /**
     * @author felipe.cadena
     * @see IVCargueCicaDAO#buscarPrediosAsociadosCargue(List)
     */
    @Override
    public List<VCargueCica> buscarPrediosAsociadosCargue(List<VCargueCica> predios) {
        List<VCargueCica> resultado = null;

        List<Long> tramiteIds = new ArrayList<Long>();

        for (VCargueCica predio:predios) {
            tramiteIds.add(predio.getTramiteId());
        }

        String queryString = "SELECT vc FROM VCargueCica vc \n" +
                "WHERE vc.tramiteId in :tramiteIds";

        try {
            Query query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIds",tramiteIds);

            resultado = query.getResultList();

        } catch (NoResultException e) {
            return new ArrayList<VCargueCica>();
        } catch (Exception e) {
            LOGGER.error("error on IVCargueCicaDAO#buscarPrediosAsociadosCargue: " + e.
                    getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                    "IVCargueCicaDAO#buscarPrediosAsociadosCargue");
        }

        return resultado;
    }

    /**
     * @author felipe.cadena
     * @see IVCargueCicaDAO#buscarProyeccionCargueCica(FiltroCargueDTO)
     */
    @Override
    public List<VCargueCica> buscarProyeccionCargueCica(FiltroCargueDTO filtroCargueDTO) {


        return (List<VCargueCica>)this.buscarProyeccionCicaImp(filtroCargueDTO, false);
    }

    /**
     * @author felipe.cadena
     * @see IVCargueCicaDAO#countProyeccionCargueCica(FiltroCargueDTO)
     */
    @Override
    public int countProyeccionCargueCica(FiltroCargueDTO filtroCargueDTO) {


        return (Integer)this.buscarProyeccionCicaImp(filtroCargueDTO, true);
    }

    private Object buscarProyeccionCicaImp(FiltroCargueDTO filtroCargueDTO, boolean isCount){
        LOGGER.debug("en TramiteDAOBean#findTramitesDeterminarProcedenciaSolicitud");

        List<VCargueCica> answer1;
        Integer answer2;
        Map<String, String> filters =filtroCargueDTO.getFilters();

        Object answer = null;
        StringBuilder queryString, idTramiteIn;
        Query query;

        queryString = new StringBuilder();
        idTramiteIn = new StringBuilder();


        if (!isCount) {
            queryString.append("SELECT cv FROM VCargueCica cv " +
                    " WHERE 1 = 1 ");
        } else {
            // N: esta, para dejarla más liviana se hace de forma diferente
            queryString.append("SELECT count(cv) " +
                    " FROM VCargueCica cv WHERE 1 = 1 ");

        }

        //D: revisar filtros de búsqueda.
        //N: el operador sql que se use depende del modo de emparejamiento definido en el xhtml (ej: si filterMatchMode="exact" => se usa '=')

        if (filtroCargueDTO.getVigencia()>0) {
            queryString.append(" AND cv.vigencia = :vigencia ");
        }
        if (filtroCargueDTO.getMunicipioCodigo()!=null && !filtroCargueDTO.getMunicipioCodigo().isEmpty()) {
            queryString.append(" AND cv.municipioCodigo = :municipioCodigo ");
        }

        if (filters.containsKey("numeroPredialSnc")) {
            queryString.append(" AND cv.numeroPredialSnc LIKE :numeroPredialSnc ");
        }
        if (filters.containsKey("numeroPredialCica")) {
            queryString.append(" AND cv.numeroPredialCica LIKE :numeroPredialCica ");
        }
        //area terreno ---
        if (filtroCargueDTO.getAreaTerrenoInicial().getValorInicial()!=null) {
            queryString.append(" AND cv.areaTerrenoInicial > :areaTerrenoInicialV1 ");
        }
        if (filtroCargueDTO.getAreaTerrenoInicial().getValorFinal()!=null) {
            queryString.append(" AND cv.areaTerrenoInicial < :areaTerrenoInicialV2 ");
        }
        if (filtroCargueDTO.getAreaTerrenoFinal().getValorInicial()!=null) {
            queryString.append(" AND cv.areaTerrenoFinal > :areaTerrenoFinalV1 ");
        }
        if (filtroCargueDTO.getAreaTerrenoFinal().getValorFinal()!=null) {
            queryString.append(" AND cv.areaTerrenoFinal < :areaTerrenoFinalV2 ");
        }
        if (filtroCargueDTO.getAreaTerrenoVariacion().getValorInicial()!=null) {
            queryString.append(" AND cv.areaTerrenoVariacion > :areaTerrenoVariacionV1 ");
        }
        if (filtroCargueDTO.getAreaTerrenoVariacion().getValorFinal()!=null) {
            queryString.append(" AND cv.areaTerrenoVariacion < :areaTerrenoVariacionV2 ");
        }

        //area construccion ---
        if (filtroCargueDTO.getAreaConstruccionInicial().getValorInicial()!=null) {
            queryString.append(" AND cv.areaConstruccionInicial > :areaConstruccionInicialV1 ");
        }
        if (filtroCargueDTO.getAreaConstruccionInicial().getValorFinal()!=null) {
            queryString.append(" AND cv.areaConstruccionInicial < :areaConstruccionInicialV2 ");
        }
        if (filtroCargueDTO.getAreaConstruccionFinal().getValorInicial()!=null) {
            queryString.append(" AND cv.areaConstruccionFinal > :areaConstruccionFinalV1 ");
        }
        if (filtroCargueDTO.getAreaConstruccionFinal().getValorFinal()!=null) {
            queryString.append(" AND cv.areaConstruccionFinal < :areaConstruccionFinalV2 ");
        }
        if (filtroCargueDTO.getAreaConstruccionVariacion().getValorInicial()!=null) {
            queryString.append(" AND cv.areaConstruccionVariacion > :areaConstruccionVariacionV1 ");
        }
        if (filtroCargueDTO.getAreaConstruccionVariacion().getValorFinal()!=null) {
            queryString.append(" AND cv.areaConstruccionVariacion < :areaConstruccionVariacionV2 ");
        }

        //avaluos  ---
        if (filtroCargueDTO.getAvaluoInicial().getValorInicial()!=null) {
            queryString.append(" AND cv.avaluoInicial > :avaluoInicialV1 ");
        }
        if (filtroCargueDTO.getAvaluoInicial().getValorFinal()!=null) {
            queryString.append(" AND cv.avaluoInicial < :avaluoInicialV2 ");
        }
        if (filtroCargueDTO.getAvaluoFinal().getValorInicial()!=null) {
            queryString.append(" AND cv.avaluoFinal > :avaluoFinalV1 ");
        }
        if (filtroCargueDTO.getAvaluoFinal().getValorFinal()!=null) {
            queryString.append(" AND cv.avaluoFinal < :avaluoFinalV2 ");
        }
        if (filtroCargueDTO.getAvaluoVariacion().getValorInicial()!=null) {
            queryString.append(" AND cv.avaluoVariacion > :avaluoVariacionV1 ");
        }
        if (filtroCargueDTO.getAvaluoVariacion().getValorFinal()!=null) {
            queryString.append(" AND cv.avaluoVariacion < :avaluoVariacionV2 ");
        }


        queryString.append(idTramiteIn);

        //D: se agrega el posible ordenamiento al query
        //OJO: se asume que si va a contar no se necesita ordenamiento
        if (!isCount) {
            if (filtroCargueDTO.getSortField() != null && !filtroCargueDTO.getSortField().equals("") && !filtroCargueDTO.getSortOrder().equals("UNSORTED")) {
                queryString.append(" ORDER BY cv.").append(filtroCargueDTO.getSortField()).
                        append(filtroCargueDTO.getSortOrder().equals("DESCENDING") ? " DESC" : " ASC");
            }
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());

            if (filtroCargueDTO.getVigencia()>0) {
                query.setParameter("vigencia", Long.valueOf(filtroCargueDTO.getVigencia()));
            }
            if (filtroCargueDTO.getMunicipioCodigo()!=null && !filtroCargueDTO.getMunicipioCodigo().isEmpty()) {
                query.setParameter("municipioCodigo", filtroCargueDTO.getMunicipioCodigo());
            }

            if (filters.containsKey("numeroPredialSnc")) {
                query.setParameter("numeroPredialSnc", "%" + filters.get("numeroPredialSnc") + "%");
            }
            if (filters.containsKey("numeroPredialCica")) {
                query.setParameter("numeroPredialCica", "%" + filters.get("numeroPredialCica") + "%");
            }

            //area terreno ---
            if (filtroCargueDTO.getAreaTerrenoInicial().getValorInicial()!=null) {
                query.setParameter("areaTerrenoInicialV1", filtroCargueDTO.getAreaTerrenoInicial().getValorInicial());
            }
            if (filtroCargueDTO.getAreaTerrenoInicial().getValorFinal()!=null) {
                query.setParameter("areaTerrenoInicialV2", filtroCargueDTO.getAreaTerrenoInicial().getValorFinal());
            }
            if (filtroCargueDTO.getAreaTerrenoFinal().getValorInicial()!=null) {
                query.setParameter("areaTerrenoFinalV1", filtroCargueDTO.getAreaTerrenoFinal().getValorInicial());
            }
            if (filtroCargueDTO.getAreaTerrenoFinal().getValorFinal()!=null) {
                query.setParameter("areaTerrenoFinalV2", filtroCargueDTO.getAreaTerrenoFinal().getValorFinal());
            }
            if (filtroCargueDTO.getAreaTerrenoVariacion().getValorInicial()!=null) {
                query.setParameter("areaTerrenoVariacionV1", filtroCargueDTO.getAreaTerrenoVariacion().getValorInicial());
            }
            if (filtroCargueDTO.getAreaTerrenoVariacion().getValorFinal()!=null) {
                query.setParameter("areaTerrenoVariacionV2", filtroCargueDTO.getAreaTerrenoVariacion().getValorFinal());
            }

            //area construccion ---
            if (filtroCargueDTO.getAreaConstruccionInicial().getValorInicial()!=null) {
                query.setParameter("areaConstruccionInicialV1", filtroCargueDTO.getAreaConstruccionInicial().getValorInicial());
            }
            if (filtroCargueDTO.getAreaConstruccionInicial().getValorFinal()!=null) {
                query.setParameter("areaConstruccionInicialV2", filtroCargueDTO.getAreaConstruccionInicial().getValorFinal());
            }
            if (filtroCargueDTO.getAreaConstruccionFinal().getValorInicial()!=null) {
                query.setParameter("areaConstruccionFinalV1", filtroCargueDTO.getAreaConstruccionFinal().getValorInicial());
            }
            if (filtroCargueDTO.getAreaConstruccionFinal().getValorFinal()!=null) {
                query.setParameter("areaConstruccionFinalV2", filtroCargueDTO.getAreaConstruccionFinal().getValorFinal());
            }
            if (filtroCargueDTO.getAreaConstruccionVariacion().getValorInicial()!=null) {
                query.setParameter("areaConstruccionVariacionV1", filtroCargueDTO.getAreaConstruccionVariacion().getValorInicial());
            }
            if (filtroCargueDTO.getAreaConstruccionVariacion().getValorFinal()!=null) {
                query.setParameter("areaConstruccionVariacionV2", filtroCargueDTO.getAreaConstruccionVariacion().getValorFinal());
            }

            //avaluos ---
            if (filtroCargueDTO.getAvaluoInicial().getValorInicial()!=null) {
                query.setParameter("avaluoInicialV1", filtroCargueDTO.getAvaluoInicial().getValorInicial());
            }
            if (filtroCargueDTO.getAvaluoInicial().getValorFinal()!=null) {
                query.setParameter("avaluoInicialV2", filtroCargueDTO.getAvaluoInicial().getValorFinal());
            }
            if (filtroCargueDTO.getAvaluoFinal().getValorInicial()!=null) {
                query.setParameter("avaluoFinalV1", filtroCargueDTO.getAvaluoFinal().getValorInicial());
            }
            if (filtroCargueDTO.getAvaluoFinal().getValorFinal()!=null) {
                query.setParameter("avaluoFinalV2", filtroCargueDTO.getAvaluoFinal().getValorFinal());
            }
            if (filtroCargueDTO.getAvaluoVariacion().getValorInicial()!=null) {
                query.setParameter("avaluoVariacionV1", filtroCargueDTO.getAvaluoVariacion().getValorInicial());
            }
            if (filtroCargueDTO.getAvaluoVariacion().getValorFinal()!=null) {
                query.setParameter("avaluoVariacionV2", filtroCargueDTO.getAvaluoVariacion().getValorFinal());
            }
            

            if (!isCount) {
                answer1 = (List<VCargueCica>) super.findInRangeUsingQuery(query, filtroCargueDTO.getFirst(), filtroCargueDTO.getPageSize());

                answer = answer1;
            } else {
                answer2 = Integer.valueOf(query.getSingleResult().toString());
                answer = answer2;
            }
        } catch (NoResultException e) {
            return new ArrayList<VCargueCica>();
        } catch (Exception e) {
            LOGGER.error("error on IVCargueCicaDAO#buscarProyeccionCicaImp: " + e.
                    getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                    "IVCargueCicaDAO#buscarProyeccionCicaImp");
        }

        return answer;
    }


}