/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITipoSolicitudTramiteDAO;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class TipoSolicitudTramiteDAOBean extends GenericDAOWithJPA<TipoSolicitudTramite, Long>
    implements ITipoSolicitudTramiteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TipoSolicitudTramiteDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITipoSolicitudTramiteDAO#findByTipoSolicitud(java.lang.String)
     */
    @Implement
    public List<TipoSolicitudTramite> findByTipoSolicitud(String tipoSolicitud) {

        LOGGER.debug("on TipoSolicitudTramiteDAOBean#findByTipoSolicitud ...");
        List<TipoSolicitudTramite> answer = null;
        Query query;

        try {
            query = this.entityManager.createNamedQuery("findByTipoSolicitud");
            query.setParameter("tipoSolP", tipoSolicitud);
            answer = query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;
    }

//end of class
}
