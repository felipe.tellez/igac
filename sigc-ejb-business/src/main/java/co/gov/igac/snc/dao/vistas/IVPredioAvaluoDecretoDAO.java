package co.gov.igac.snc.dao.vistas;

import java.util.ArrayList;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VPredioAvaluoDecreto;

/**
 * Interfaz para los servicios de persistencia del objeto {@link VPredioAvaluoDecreto}.
 *
 * @author david.cifuentes
 */
@Local
public interface IVPredioAvaluoDecretoDAO extends
    IGenericJpaDAO<VPredioAvaluoDecreto, Long> {

    /**
     * Método que busca los avaluos históricos de un pPredio por su id.
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    /*
     * @modified by leidy.gonzalez :: #12528::26/05/2015::CU_187
     */
    public ArrayList<VPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredio(
        Long pPredioId);
}
