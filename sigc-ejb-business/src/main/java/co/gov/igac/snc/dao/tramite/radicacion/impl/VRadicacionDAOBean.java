package co.gov.igac.snc.dao.tramite.radicacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.IVRadicacionDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.VRadicacion;
import co.gov.igac.snc.util.FiltroDatosSolicitud;

/**
 * Funcionalidad para acceder a las radicaciones y solicitudes.
 *
 * @author jamir.avila
 */
@Stateless
public class VRadicacionDAOBean extends GenericDAOWithJPA<TramiteRectificacion, Long> implements
    IVRadicacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudDAOBean.class);

    /**
     * @see IVRadicacionDAO#buscarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud, int, int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VRadicacion> buscarSolicitudesCotizacionAvaluos(
        FiltroDatosSolicitud filtro, int desde, int cantidad) {
        LOGGER.info("VRadicacionDAOBean::buscarSolicitudesCotizacionAvaluos()");

        if (desde < 0) {
            desde = 0;
        }

        StringBuilder sql = new StringBuilder("SELECT v FROM VRadicacion v WHERE 1 = 1 ");

        preSolicitudesCotizacionAvaluos(filtro, sql);
        Query query = entityManager.createQuery(sql.toString());
        postSolicitudesCotizacionAvaluos(filtro, query);

        query.setFirstResult(desde);
        if (cantidad > 0) {
            query.setMaxResults(cantidad);
        }

        List<VRadicacion> resultado = null;
        try {
            resultado = (List<VRadicacion>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return resultado;
    }

    /**
     * @see IVRadicacionDAO#buscarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud)
     */
    @Override
    public long contarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro) {
        LOGGER.info("VRadicacionDAOBean::contarSolicitudesCotizacionAvaluos()");
        StringBuilder sql = new StringBuilder(
            "SELECT COUNT(*) AS cantidad FROM VRadicacion v WHERE 1 = 1 ");

        preSolicitudesCotizacionAvaluos(filtro, sql);
        Query query = entityManager.createQuery(sql.toString());
        postSolicitudesCotizacionAvaluos(filtro, query);

        Long resultado = (Long) query.getSingleResult();
        return resultado.longValue();
    }

    /**
     * Método auxiliar para la construcción del filtro. Se invoca antes de la creación del objeto
     * Query.
     *
     * @param filtro objeto con los datos del filtro.
     * @param sql expresión SQL.
     */
    private void preSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro,
        StringBuilder sql) {
        LOGGER.info("VRadicacionDAOBean::preSolicitudesCotizacionAvaluos()");

        if (filtro.getNumero() != null && filtro.getNumero().trim().length() > 0) {
            sql.append("   AND v.numeroRadicacion = :numeroRadicacion ");
        }
        if (filtro.getFechaInicial() != null) {
            sql.append("   AND v.fechaRadicacion >= :fechaInicial ");
        }
        if (filtro.getFechaFinal() != null) {
            sql.append("   AND v.fechaRadicacion <= :fechaFinal ");
        }
        if (filtro.getRadicacionInicial() != null && filtro.getRadicacionInicial().trim().length() >
            0) {
            sql.append("   AND v.numeroRadicacionInicial = :numeroRadicacionInicial ");
        }
        if (filtro.getSolicitanteId() != null) {
            sql.append("   AND v.personaSolicitanteId = :personaSolicitanteId ");
        }
        if (filtro.getTipoPersona() != null && filtro.getTipoPersona().trim().length() > 0) {
            sql.append("   AND v.tipoPersona = :tipoPersona ");
        }
        if (filtro.getPaisCodigo() != null && filtro.getPaisCodigo().trim().length() > 0) {
            sql.append("   AND v.direccionPaisCodigo = :direccionPaisCodigo");
        }
        if (filtro.getDepartamentoCodigo() != null &&
            filtro.getDepartamentoCodigo().trim().length() > 0) {
            sql.append("   AND v.direccionDepartamentoCodigo = :direccionDepartamentoCodigo ");
        }
        if (filtro.getMunicipioCodigo() != null && filtro.getDepartamentoCodigo().trim().length() >
            0) {
            sql.append("   AND v.direccionMunicipioCodigo = :direccionMunicipioCodigo");
        }
    }

    /**
     * Método auxiliar para el llenado de los parámetros del filtro. Se invoca después de la
     * creación del objeto Query.
     *
     * @param filtro objeto con los datos del filtro.
     * @param sql expresión SQL.
     */
    private void postSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro,
        Query query) {
        LOGGER.info("VRadicacionDAOBean::postSolicitudesCotizacionAvaluos()");

        if (filtro.getNumero() != null && filtro.getNumero().trim().length() > 0) {
            query.setParameter("numeroRadicacion", filtro.getNumero());
        }
        if (filtro.getFechaInicial() != null) {
            query.setParameter("fechaInicial", filtro.getFechaInicial());
        }
        if (filtro.getFechaFinal() != null) {
            query.setParameter("fechaFinal", filtro.getFechaFinal());
        }
        if (filtro.getRadicacionInicial() != null && filtro.getRadicacionInicial().trim().length() >
            0) {
            query.setParameter("numeroRadicacionInicial", filtro.getRadicacionInicial());
        }
        if (filtro.getSolicitanteId() != null) {
            query.setParameter("personaSolicitanteId", filtro.getSolicitanteId());
        }
        if (filtro.getTipoPersona() != null && filtro.getTipoPersona().trim().length() > 0) {
            query.setParameter("tipoPersona", filtro.getTipoPersona());
        }
        if (filtro.getPaisCodigo() != null && filtro.getPaisCodigo().trim().length() > 0) {
            query.setParameter("direccionPaisCodigo", filtro.getPaisCodigo());
        }
        if (filtro.getDepartamentoCodigo() != null &&
            filtro.getDepartamentoCodigo().trim().length() > 0) {
            query.setParameter("direccionDepartamentoCodigo", filtro.getDepartamentoCodigo());
        }
        if (filtro.getMunicipioCodigo() != null && filtro.getMunicipioCodigo().trim().length() > 0) {
            query.setParameter("direccionMunicipioCodigo", filtro.getMunicipioCodigo());
        }
    }
}
