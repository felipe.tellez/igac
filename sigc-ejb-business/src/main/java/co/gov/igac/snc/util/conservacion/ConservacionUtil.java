package co.gov.igac.snc.util.conservacion;

import java.util.Date;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;

/**
 * Clase encargada de mantener las utilidades multipropósito, NO involucradas con transacciones u
 * operaciones relacionadas directamente con acceso a datos. Creada para NO involucrar operaciones
 * sencillas en la implementación de los EJBs.
 *
 * @author fabio.navarrete
 *
 */
public class ConservacionUtil {

    /**
     * Retorna una ficha matriz con los valores por defecto (ceros) asociada a su vez con el predio
     * ingresado como parámetro
     *
     * @param pPredio
     * @param usuario
     * @return
     * @author fabio.navarrete
     */
    public static PFichaMatriz createBlankPFichaMatriz(PPredio pPredio,
        UsuarioDTO usuario) {
        PFichaMatriz fichaMatriz = new PFichaMatriz();
        fichaMatriz.setAreaTotalConstruidaComun(0.0);
        fichaMatriz.setAreaTotalConstruidaPrivada(0.0);
        fichaMatriz.setAreaTotalTerrenoComun(0.0);
        fichaMatriz.setAreaTotalTerrenoPrivada(0.0);
        fichaMatriz.setValorTotalAvaluoCatastral(0.0);
        fichaMatriz.setTotalUnidadesPrivadas(0L);
        fichaMatriz.setTotalUnidadesSotanos(0L);
        // Como es una ficha matriz nueva en proyección, se debe estar
        // inscribiendo
        fichaMatriz.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
            .getCodigo());
        // Se asocia el predio
        fichaMatriz.setPPredio(pPredio);
        fichaMatriz.setFechaLog(new Date());
        fichaMatriz.setUsuarioLog(usuario.getLogin());
        return fichaMatriz;
    }
}
