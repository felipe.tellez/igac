package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoCesion;

/**
 * interfaz para los métodos de bd de la tabla AREA_PROFESIONAL_CONTRATO_CESION
 *
 * @author rodrigo.hernandez
 * @version 2.0
 */
@Local
public interface IProfesionalContratoCesionDAO extends
    IGenericJpaDAO<ProfesionalContratoCesion, Long> {

    /**
     * Método para consultar la lista de cesiones asociadas a un contrato activo
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato - Id del contrato activo
     * @return
     */
    public List<ProfesionalContratoCesion> obtenerCesionesDeContratoActivo(Long idContrato);

}
