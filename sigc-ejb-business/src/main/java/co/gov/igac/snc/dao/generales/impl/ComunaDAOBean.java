package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IComunaDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;

@Stateless
public class ComunaDAOBean extends GenericDAOWithJPA<Comuna, String> implements IComunaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComunaDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<Comuna> findBySector(String sectorId) {
        Query query = this.entityManager.createQuery(
            "SELECT DISTINCT m FROM Comuna m WHERE m.sector.codigo = :sectorId");
        query.setParameter("sectorId", sectorId);
        return query.getResultList();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IMunicipioDAO#getComunaByCodigo(String)
     */
    public Comuna getComunaByCodigo(String codigo) {
        Query q = entityManager.createNamedQuery("comuna.findComunaByCodigo");
        q.setParameter("codigo", codigo);
        Comuna m = (Comuna) q.getSingleResult();
        return m;
    }
}
