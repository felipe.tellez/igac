package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFmConstruccionComponente;

/**
 * Servicios de persistencia del objeto {@link PFmConstruccionComponente}.
 *
 * @author david.cifuentes
 */
@Local
public interface IPFmConstruccionComponenteDAO extends
    IGenericJpaDAO<PFmConstruccionComponente, Long> {

    /**
     * Actualiza los datos de la tabla P_FM_CONSTRUCCION_COMPONENTE para la unidad del modelo de
     * construcción que viene como atributo de los objetos de la lista de objetos a insertar. La
     * actualización consiste en borrar todos los registros anteriores que estén relacionados con
     * esa unidad de construcción e insertar los nuevos. Se borran, y no se actualizan, porque es
     * más fácil debido a la forma como se arma el árbol de componentes-elementos-detalles
     *
     * @author david.cifuentes
     * @param List <PFmConstruccionComponente> Lista de objetos que se van a actualizar
     */
    public void actualizarPFmConstruccionComponenteRegistros(
        List<PFmConstruccionComponente> newPFmConstruccionComponenteList);

    /**
     * Elimina los registros de la tabla P_FM_CONSTRUCCION_COMPONENTE que correspondan al
     * PFmModeloConstruccion con el id suministrado.
     *
     * @author david.cifuentes
     * @param pFmModeloConstruccionId
     */
    public void borrarPFmConstruccionComponenteRegistros(
        Long pFmModeloConstruccionId);
}
