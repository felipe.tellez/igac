package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;

@Local
public interface ITramiteDocumentoDAO extends IGenericJpaDAO<TramiteDocumento, Long> {

    /**
     * Método que busca un {@link TramiteDocumento} con soporte en Alfresco por su id.
     *
     * @author david.cifuentes
     * @param idTramite
     * @return
     */
    public TramiteDocumento buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(Long idTramite);

    /**
     * Obtiene los registros (haciendo fetch del Documento) donde el trámite es el dado, y para los
     * cuales el Documento relacionado es del tipo dado
     *
     * @author pedro.garcia
     * @param tramiteId
     * @param idTipoDocumento
     * @return
     */
    public List<TramiteDocumento> findByTramiteAndTipoDocumento(long tramiteId, long idTipoDocumento);

    /**
     * Obtiene los registros (haciendo fetch del Documento) donde el trámite es el dado, y para los
     * cuales el Documento relacionado es un documento de pruebas
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<TramiteDocumento> findByTramiteDocumentosDePruebasAsociadas(long tramiteId);

    /**
     * Método que retorna el numero de trámite documentos que estan asociados a pruebas con una
     * entidad
     *
     * @author franz.gamba
     * @return
     */
    public Integer countDocumentosPruebasPorEntidad(Long tramiteId, String numeroIdentificacion);

    /**
     * Método que retorna los trámite documentos que estan asociados a pruebas con una entidad.
     *
     * @param tramiteId
     * @param numeroIdentificacion
     * @author franz.gamba
     * @return
     */
    public List<TramiteDocumento> getDocumentosPruebasDeEntidad(Long tramiteId,
        String numeroIdentificacion);

    /**
     * Obtiene (debería existir) el TramiteDocumento que se crea en el momento de comunicar la
     * notificación de resolución a una persona y que se va a modificar cuando se registra la
     * notificación
     *
     * PRE: el documento debe existir. Si no, hay un error en el proceso
     *
     * @author pedro.garcia
     *
     * @param idTramite id del trámite
     * @param docPersonaNotificacion documento de la persona a notificar
     * @return
     */
    public TramiteDocumento getForRegistroNotificacion(Long idTramite, String docPersonaNotificacion);

    /**
     * Método que retorna una lista de tramiteDocumentos asociados a un trámite mediante el id del
     * tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<TramiteDocumento> findTramiteDocumentosByTramiteId(Long tramiteId);

    /**
     * Método que obtiene una lista de tramiteDocumentos asociados a un documento
     *
     * @param documentoId
     * @author leidy.gonzalez
     */
    public List<TramiteDocumento> consultaTramiteDocumentosPorDocumentoId(Long documentoId);

    /**
     * Método que obtiene un tramiteDocumento asociados a un idTramite, docPersonaNotificacion y
     * nombrePersonaNotif
     *
     * @param documentoId
     * @author leidy.gonzalez
     */
    public TramiteDocumento obtenerDocNotificacion(Long idTramite,
        String docPersonaNotificacion, String nombrePersonaNotif);

    /**
     * Obtiene (debería existir) la lista de TramiteDocumentos que se crea en el momento de
     * comunicar la notificación de resolución a una persona y que se va a modificar cuando se
     * registra la notificación
     *
     * PRE: el documento debe existir. Si no, hay un error en el proceso
     *
     * @author leidy.gonzalez
     *
     * @param idTramite id del trámite
     * @param List<String> docsPersonaNotificacion documento de la persona a notificar
     * @return
     */
    public List<TramiteDocumento> obtenerTramiteDocumentosParaRegistrosNotificacion(
        Long idTramite, List<String> docsPersonaNotificacion);

    /**
     * Método que obtiene una Lista de tramiteDocumentos de Notificacion y Autorizacion asociados a
     * un idTramite, docPersonaNotificacion y nombrePersonaNotif
     *
     * @param idTramite
     * @param docPersonaNotificacion
     * @param nombrePersonaNotif
     * @author leidy.gonzalez
     */
    public List<TramiteDocumento> obtenerDocsComunicacionYAutorizacion(
        Long idTramite, String docPersonaNotificacion, String nombrePersonaNotif);

//end of interface
}
