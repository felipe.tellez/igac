package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialManzana;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialManzanaPK;

/**
 * @author fredy.wilches
 */
@Local
public interface IEstadisticaGerencialManzanaDAO extends
    IGenericJpaDAO<EstadisticaGerencialManzana, EstadisticaGerencialManzanaPK> {

    public List<Object[]> consultaPorMunicipio(Long anio, String codigoMunicipio, String zona);

}
