/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IPlantillaSeccionReporteDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import java.util.List;

/**
 * Implementación del DAO para PlantillaSeccionReporte
 *
 * @author david.cifuentes
 *
 */
@Stateless
public class PlantillaSeccionReporteDAOBean extends
    GenericDAOWithJPA<PlantillaSeccionReporte, Long> implements
    IPlantillaSeccionReporteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PlantillaSeccionReporteDAOBean.class);

    /**
     * @see
     * IPlantillaSeccionReporteDAO#buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(String)
     * @author david.cifuentes
     */
    @Override
    public PlantillaSeccionReporte buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo) {

        PlantillaSeccionReporte resultado = null;
        String queryString =
            "SELECT psr FROM PlantillaSeccionReporte psr WHERE psr.estructuraOrganizacionalCod = :estructuraOrganizacionalCodigo";

        try {
            Query query = entityManager.createQuery(queryString);
            query.setParameter("estructuraOrganizacionalCodigo",
                estructuraOrganizacionalCodigo);

            resultado = (PlantillaSeccionReporte) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC(
                "PlantillaSeccionReporteDAOBean#buscarPlantillaSeccionReportePorCodEstructuraOrganizacional",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }

        return resultado;
    }

    /**
     * @see
     * IPlantillaSeccionReporteDAO#buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean verificarUrlSeccionesPlantillaReportes() {

        try {
            String queryString = "SELECT psr FROM PlantillaSeccionReporte psr " +
                " WHERE (psr.urlCabecera IS NULL AND psr.rutaCabecera IS NOT NULL) " +
                " OR (psr.urlMarcaDeAgua IS NULL AND psr.rutaMarcaDeAgua IS NOT NULL) " +
                " OR (psr.urlPieDePagina IS NULL AND psr.rutaPieDePagina IS NOT NULL) ";

            Query query = entityManager.createQuery(queryString);
            List<PlantillaSeccionReporte> plantillas = query.getResultList();

            if (plantillas != null && !plantillas.isEmpty()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC(
                "PlantillaSeccionReporteDAOBean#verificarUrlSeccionesPlantillaReportes",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
    }
}
