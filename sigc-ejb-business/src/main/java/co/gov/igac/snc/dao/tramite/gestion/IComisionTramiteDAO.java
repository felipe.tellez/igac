/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de una Comision_Tramite
 *
 * @author pedro.garcia
 */
@Local
public interface IComisionTramiteDAO extends IGenericJpaDAO<ComisionTramite, Long> {

    /**
     * Borra un registro de la tabla COMISION_TRAMITE donde la comisión y el trámite sean los dados
     *
     * @author pedro.garcia
     * @param tramiteId
     * @param comisionId
     */
    public void deleteRow(Long tramiteId, Long comisionId);

    /**
     * Elimina los registros que tengan como id de trámite los id de los trámites que vienen en la
     * lista
     *
     * N: No se llama deleteMultiple como quise ponerle porque presenta un problema: en la
     * implementación dice que ya existe un método con la misma firma (el de GenericDAOWithJPA), y
     * si se usa @Override dice que los dos métodos tienen el mismo erasure. Esto sucede porque para
     * el compilador es lo mismo List<Lola> que List<T>, because all instances of a generic class
     * have the same run-time class (List in your case), regardless of their actual type parameters.
     *
     * @author pedro.garcia
     * @param tramites
     * @version 2.0
     * @deprecated este ya no sirve porque ahora hay comisiones de depuración se debe usar
     * deleteByTramiteIdsAndComisionId
     */
    @Deprecated
    public void deleteMultipleCT(List<Tramite> tramites);

    /**
     * Elimina los registros que tengan como id de trámite alguno de los ids que vienen en la lista,
     * y como id de comisión el que viene como parámetro.
     *
     * @autor pedro.garcia
     *
     * @param idsTramites
     * @param idComision
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     */
    public void deleteByTramiteIdsAndComisionId(List<Long> idsTramites, Long idComision) throws
        ExcepcionSNC;

    /**
     * obtiene la duración de una comisión a partir de las relaciones entre comisión y trámites. Se
     * calcula como el máximo de las duraciones de los trámites asociados con ella
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param idComision
     * @return
     */
    public Double getComisionDuration(Long idComision);

    /**
     * Obtiene la duración de una comisión a partir de las relaciones entre comisión y trámites. Se
     * deja como método diferente de getComisionDuration porque aplica la nueva regla y está en sql
     * nativo
     *
     * duracion = max(sum(dt,e1,c), sum(dt,e2,c), sum(dt,en,c))
     *
     * donde sum(dt,ex,c) es la sumatoria de las duraciones de los trámites (dt) del ejecutor x (ex)
     * que están comisionados en la comisión c
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param idComision
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     * @return
     */
    public Double getComisionDuration2(Long idComision) throws ExcepcionSNC;

    /**
     * Método que busca en comision trámite por el id del trámite y devuelve una lista de tipo
     * comisión trámite con información de las comisiones asociadas al trámite
     *
     * @author javier.aponte
     */
    public List<ComisionTramite> buscarComisionesTramitePorIdTramite(Long idTramite);

    /**
     * Elimina los registros de la tabla que están asociados a la comisión con el id dado.
     *
     * @pedro.garcia
     * @version 2.0
     *
     * @param idComision
     */
    public void deleteRowsByComisionId(Long idComision);

    /**
     * Busca las ComisionTramite por id de comisión
     *
     * @author juan.agudelo
     * @param comisionId
     * @return
     */
    List<ComisionTramite> findComisionesTramiteByComisionId(Long comisionId);

    /**
     * Cuenta las comisiones efectivas (que han llegado al estado 'finalizada') en las que ha estado
     * el trámite con el id dado
     *
     * @author pedro.garcia
     * @param tramiteId
     * @param tipoComision tipo de comisión que se va a usar para el cálculo del número de
     * comisiones en las que ha estado el trámite
     * @return
     */
    public int contarComisionesEfectivasDeTramite(Long tramiteId, String tipoComision);

    /**
     * Consulta los registros correspondientes al trámite con id dado, para los cuales la comisión
     * fue efectiva, es decir, llegó al estado 'finalizada'
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<ComisionTramite> consultarComisionesEfectivasDeTramite(Long idTramite);

    /**
     * Busca los registros que estén relacionados con el id de comisión dado
     *
     * @author pedro.garcia
     * @param idComision
     * @return
     */
    public List<ComisionTramite> buscarPorIdComision(Long idComision);

    /**
     * Método que busca en comision trámite por el id del trámite y devuelve una lista de tipo
     * comisión trámite con información de las comisiones asociadas al trámite que no han sido
     * realizadas
     *
     * se supone que la lista retornada solo debería un elemento pues un trámite solo debe estar
     * asociado a una comision sin realizar en un instante de tiempo determinado
     *
     * @author javier.aponte
     */
    public List<ComisionTramite> buscarComisionesTramiteNoRealizadasPorIdTramite(Long idTramite);

//end of interface        
}
