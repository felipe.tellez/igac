package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;

/**
 * Servicios de persistencia para el objeto ProductoCatastral
 *
 * @author javier.barajas
 */
@Local
public interface IProductoCatastralDAO extends IGenericJpaDAO<ProductoCatastral, Long> {

    /**
     * Metodo para guardar la generacion de productos catastrales
     *
     * @param List<ProductoCatastral>
     * @return List<ProductoCatastral @cu CU-TV- PR-0001 INGRESAR SOLICITUD PRODUCTOS CATASTRALES
     * @version 1.0 @author javier.barajas
     *
     */
    public List<ProductoCatastral> guardarYactualizarProductosCatastrales(
        List<ProductoCatastral> listaProductos);

    /**
     * Metodo para consultar producto catastral por id
     *
     * @param Long productoCatastralId
     * @return PorductoCatastral
     * @cu CU-TV-PR-0002 SOLICITUD DE PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public ProductoCatastral consultarProductosCatastralesporId(Long productoCatastralId);

    /**
     * Metodo para guardar y actualizar un producto catastral
     *
     * @param ProductoCatastral
     * @return ProductoCatastral
     * @cu CU-TV-PR-0001 INGRESAR SOLICITUD PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public ProductoCatastral guardarYactualizarProductoCatastral(ProductoCatastral producto);

    /**
     * Método encargado de buscar en la base de datos los productos catastrales asociados a una
     * solicitud
     *
     * @param solicitudId id de la solicitud
     * @return lista de productos catastrales asociados a la solicitud
     * @author javier.aponte
     */
    public List<ProductoCatastral> buscarProductosCatastralesPorSolicitudId(Long solicitudId);

    /**
     * Obtiene la lista de jobs que completaron su ejecución en la base de datos y que tienen
     * pendiente ser completados en Java
     *
     * @return Lista de productos catastrales de jobs pendientes en SNC
     * @author javier.aponte
     */
    public List<ProductoCatastral> obtenerJobsPendientesEnSNC(Long cantidad);

    /**
     * Actualiza el estado de un job ( Maneja su propia transacción )
     *
     * @param job
     * @param nuevoEstado
     * @author javier.aponte
     */
    public void actualizarEstado(ProductoCatastral job, String nuevoEstado);

}
