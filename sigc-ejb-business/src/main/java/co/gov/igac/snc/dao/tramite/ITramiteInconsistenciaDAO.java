/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface para las operaciones DB de la entidad TramiteInconsistencia
 *
 * @author felipe.cadena
 */
@Local
public interface ITramiteInconsistenciaDAO extends IGenericJpaDAO<TramiteInconsistencia, Long> {

    /**
     * Metodo para consultar las inconsistencias asociadas un tramite
     *
     * @author felipe.cadena
     *
     * @param idTramite
     * @return
     */
    public List<TramiteInconsistencia> buscarPorIdTramite(Long idTramite);

    /**
     * Bucar las inconsistencias por el tramite Id haciendo un filtro por el campo depurada
     *
     * @param TramiteId identificador del tramite.
     * @param condicionDepurada cadena representa la condicion sobre la propiedad depurada de la
     * inconsistencia Ej. SI,NO,CC
     *
     * @author andres.eslava
     */
    public List<TramiteInconsistencia> buscarDepuradaByTramiteId(Long TramiteId,
        String condicionDepurada);

    /**
     * Procesa y guarda las inconsistencias asociadas a un tramite en el xml enviado
     *
     * @author felipe.cadena
     * @param XMLInconsistencias
     * @param tramite
     * @return
     */
    public Boolean guardarInconsistenciasAsincronicas(Tramite tramite, String XMLInconsistencias);

	/**
	 * Elimina las inconsistencias de un tramite
	 * @param idTramite
	 * @author vsocarras
	 * @return 
	 */
	boolean eliminarInconsistenciasPorTramite(Long idTramite);
}
