/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;

/**
 *
 * @author pedro.garcia
 * @modified juan.agudelo ubicación en el paquete correspondiente al esquema de DB
 */
@Local
public interface IEstructuraOrganizacionalDAO extends
    IGenericJpaDAO<EstructuraOrganizacional, String> {

    public List<EstructuraOrganizacional> getDependencias(EEstructuraOrganizacional tipo,
        String codigoPadre);

    /**
     * Retorna la lista de las Estructuras Organizacionales que sean Territoriales
     *
     * @return
     */
    public List<EstructuraOrganizacional> findTerritorialesAll();

    /**
     * hace la consulta para saber si un código de municipio corresponde a una entidad territorial
     *
     * @author pedro.garcia
     * @param codigoMunicipio
     * @param idTerritorial
     * @return
     */
    public boolean findOutIfIsSede(String codigoMunicipio, String idTerritorial);

    /**
     * Retorna la lista de todas las Territoriales incluyendo la dirección general
     *
     * @return
     * @author fabio.navarrete
     */
    public List<EstructuraOrganizacional> findTerritorialesAndDireccionGeneral();

    /**
     * Retorna las subdirecciones y oficinas (EstructuraOrganizacional) de tipo Subdireccion y
     * Oficina Apoyo asociadas a una EstructuraOrganizacional específica
     *
     * @param codigoPadre
     * @return
     * @author fabio.navarrete
     */
    public List<EstructuraOrganizacional> findSubdireccionesOficinas(
        String codigoPadre);

    /**
     * Retorna los grupos de trabajo (GIT) asociados a una estructura organizacional específica
     *
     * @param estructuraOrganizacionalCodigo
     * @return
     * @author fabio.navarrete
     */
    public List<EstructuraOrganizacional> findGruposTrabajo(
        String estructuraOrganizacionalCodigo);

    /**
     * Retorna la lista de municipios para una territorial con los municipios asociados a las
     * unidades operativas de catastro
     *
     * @author juan.agudelo
     * @param codigoEO
     * @return
     */
    public List<String> findMunicipiosTerritorialByCodigoExtructuraOrganizacional(
        String codigoEO);

    /**
     * Retorna la descripcion de la estructura organizacional correspondiente a un municipio
     *
     * @author franz.gamba
     * @param municipioCod
     * @return
     */
    public String findEstructuraOrgByMunicipioCod(String municipioCod);

    /**
     * Retorna el codigo de la estructura organizacional superior a la correspondiente al codigo del
     * municipio
     *
     * @author franz.gamba
     * @param municipioCod
     * @return
     */
    public String findEstructuraOrgSuperiorByMunicipioCod(String municipioCod);

    /**
     * Retorna la lista de códigos de estructura organizacional asociados a un código de estructura
     * organizacional
     *
     * @author juan.agudelo
     * @version 2.0
     * @param codigo
     * @return
     */
    public List<String> getCodigosEOAsociadosByCodigoEstructuraOrganizacional(
        String codigo);

    /**
     * Método que búsca una {@link EstructuraOrganizacional} por su código, retornandola con su
     * {@link Municipio}, {@link Departamento} y {@link Pais}.
     *
     * @author david.cifuentes
     */
    public EstructuraOrganizacional buscarEstructuraOrganizacionalPorCodigo(
        String codigoEstructuraOrganizacional);

    /**
     * Obtiene las UOC relacionadas a una territorial
     *
     * @author felipe.cadena
     * @param codigoTerritorial
     * @return
     */
    public List<EstructuraOrganizacional> buscarUOCporCodigoTerritorial(String codigoTerritorial);

    /**
     * Obtiene las EstructuraOrganizacionales por nombre
     *
     * @author dumar.penuela
     * @param oUC
     * @param territorial
     * @return
     */
    public List<EstructuraOrganizacional> buscarEstructuraOrganizacionalByNombre(String oUC,
        String territorial);

    /**
     * MÃ©todo que realiza la busqueda de una estructura organizacional por un cÃ³digo de municipio
     *
     * @param codigo
     * @return
     */
    public String buscarEstructuraOrgPorMunicipioCod(String codigo);

}
