package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionResponsableDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionResponsable.
 *
 * @author franz.gamba
 */
@Stateless
public class ActualizacionResponsableDAOBean extends GenericDAOWithJPA<ActualizacionResponsable, Long>
    implements IActualizacionResponsableDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionResponsableDAOBean.class);

}
