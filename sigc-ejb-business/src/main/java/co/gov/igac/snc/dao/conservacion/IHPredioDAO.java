package co.gov.igac.snc.dao.conservacion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;

@Local
public interface IHPredioDAO extends IGenericJpaDAO<HPredio, Long> {

    public List<HPredio> findHPredioListByConsecutivoCatastral(BigDecimal consecutivoCatastral);

    /**
     * Método encargado de buscar una lista de h_predios por el id del trámite y que tengan en la
     * etiqueta cancela_inscribe el valor de I o M y que el tipo historia sea P
     *
     * @param idTramite
     * @return Lista de historico de predios
     * @author javier.aponte
     */
    public List<HPredio> buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(
        Long idTramite);

    /**
     * Método que consulta un {@link HPredio} por su número predial
     *
     * @author david.cifuentes
     * @param numeroPredial
     * @param estado
     * @return
     */
    public HPredio buscarHPredioPorNumeroPredialYEstado(String numeroPredial, String estado);

    /**
     * Método que consulta el último {@link HPredio} por su número predial y un estado cancela
     * inscribe envíado como parámetro
     *
     * @author david.cifuentes
     * @param numeroPredial
     * @param estadoCancelaInscribe
     * @return
     */
    public HPredio buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(
        String numeroPredial, String estadoCancelaInscribe);

    /**
     * Consulta la cantidad de Predios que existen dentro del rango de numero predial en historicos
     *
     * @author leidy.gonzalez
     * @param numeroPredial
     * @param numeroPredialFinal
     * @return
     */
    public Integer contarPrediosPorRangoNumeroPredialHistorico(String numeroPredial,
        String numeroPredialFinal);

    /**
     * Traer el estado del predio al momento de ser originado el numeroPredial
     *
     * @param numeroPredial
     *
     * @author juan.cruz
     * @return
     */
    public HPredio obtenerEstadoOrigenNumeroPredial(String numeroPredial);

    /**
     * Traer el listado de los predios "tipo_historia" Origen segun el tramite id.
     *
     * @param tramiteId
     *
     * @author juan.cruz
     * @return
     */
    public List<HPredio> obtenerPrediosOrigenPorTramiteId(Long tramiteId);

    /**
     * Trae toda la información necesaria del hPredio para llenar las pestañas de detalles del
     * predio
     *
     * @param tramiteId
     *
     * @author juan.cruz
     * @return
     */
    public HPredio obtenerHPredioCompletoPorId(Long id);
}
