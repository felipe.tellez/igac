/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.fachadas;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Id;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import co.gov.igac.generales.dto.PUnidadConstruccionDto;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.business.conservacion.IAplicarCambios;
import co.gov.igac.snc.business.conservacion.IConsultaPPredio;
import co.gov.igac.snc.business.conservacion.IConsultaPredio;
import co.gov.igac.snc.business.conservacion.IProyectarConservacion;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.conservacion.IAxMunicipioDAO;
import co.gov.igac.snc.dao.conservacion.IColindanteDAO;
import co.gov.igac.snc.dao.conservacion.IEntidadDAO;
import co.gov.igac.snc.dao.conservacion.IEstadisticaGerencialManzanaDAO;
import co.gov.igac.snc.dao.conservacion.IEstadisticaGerencialPredioDAO;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioDAO;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizTorreDAO;
import co.gov.igac.snc.dao.conservacion.IFotoDAO;
import co.gov.igac.snc.dao.conservacion.IHFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IHFichaMatrizPredioDAO;
import co.gov.igac.snc.dao.conservacion.IHPersonaPredioPropiedadDAO;
import co.gov.igac.snc.dao.conservacion.IHPredioDAO;
import co.gov.igac.snc.dao.conservacion.IListadoPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizModeloDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizPredioTerrenoDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizTorreDAO;
import co.gov.igac.snc.dao.conservacion.IPFmConstruccionComponenteDAO;
import co.gov.igac.snc.dao.conservacion.IPFmModeloConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.IPFotoDAO;
import co.gov.igac.snc.dao.conservacion.IPManzanaVeredaDAO;
import co.gov.igac.snc.dao.conservacion.IPModeloConstruccionFotoDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioPropiedadDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioAvaluoCatastralDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDireccionDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioServidumbreDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.IPReferenciaCartograficaDAO;
import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionCompDAO;
import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.IPersonaBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IPersonaDAO;
import co.gov.igac.snc.dao.conservacion.IPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPersonasBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IPredioAvaluoCatastralDAO;
import co.gov.igac.snc.dao.conservacion.IPredioBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDireccionDAO;
import co.gov.igac.snc.dao.conservacion.IPredioInconsistenciaDAO;
import co.gov.igac.snc.dao.conservacion.IPredioLinderoDAO;
import co.gov.igac.snc.dao.conservacion.IPredioPropietarioHisDAO;
import co.gov.igac.snc.dao.conservacion.IPredioServidumbreDAO;
import co.gov.igac.snc.dao.conservacion.IPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.IPrediosBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IRangoReporteDAO;
import co.gov.igac.snc.dao.conservacion.IRepConfigParametroReporteDAO;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionDAO;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionDetalleDAO;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionUsuarioDAO;
import co.gov.igac.snc.dao.conservacion.IRepUsuarioRolReporteDAO;
import co.gov.igac.snc.dao.conservacion.IReporteControlReporteDAO;
import co.gov.igac.snc.dao.conservacion.IReporteDatosReporteDAO;
import co.gov.igac.snc.dao.conservacion.ITipificacionUnidadConstDAO;
import co.gov.igac.snc.dao.conservacion.ITmpBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IUnidadConstruccionCompDAO;
import co.gov.igac.snc.dao.conservacion.IUnidadConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.IUsoConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.IUsoHomologadoDAO;
import co.gov.igac.snc.dao.conservacion.IVUsoConstruccionZonaDAO;
import co.gov.igac.snc.dao.formacion.ICalificacionAnexoDAO;
import co.gov.igac.snc.dao.formacion.ICalificacionConstruccionDAO;
import co.gov.igac.snc.dao.formacion.IDecretoAvaluoAnualDAO;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IHorarioAtencionTerritorialDAO;
import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.sig.vo.ProductoCatastralResultado;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDetallePredioDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteMovilesDAO;
import co.gov.igac.snc.dao.tramite.ITramiteTareaDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.dao.vistas.IVPropiedadTierraDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.jms.IIntegracionLocal;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.persistence.entity.conservacion.Colindante;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.HPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFmConstruccionComponente;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.conservacion.PModeloConstruccionFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioServidumbre;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioInconsistencia;
import co.gov.igac.snc.persistence.entity.conservacion.PredioPropietarioHis;
import co.gov.igac.snc.persistence.entity.conservacion.PredioServidumbre;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.RangoReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepConsultaPredial;
import co.gov.igac.snc.persistence.entity.conservacion.RepControlReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionDetalle;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.conservacion.TmpBloqueoMasivo;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.UsoHomologado;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.persistence.entity.vistas.VPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.util.EColindanteTipo;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EDatoRectificarTipo;
import co.gov.igac.snc.persistence.util.EDias;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.persistence.util.IProyeccionObject;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.DivisionAdministrativaDTO;
import co.gov.igac.snc.util.ErrorUtil;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaParametrizacionReporte;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaReportesRadicacion;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.HorarioTerritorialDTO;
import co.gov.igac.snc.util.JustificacionPropiedadDTO;
import co.gov.igac.snc.util.MunicipioManzanaDTO;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.util.TotalManzanaDTO;
import co.gov.igac.snc.util.TotalPredialDTO;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.conservacion.ConservacionUtil;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;
import co.gov.igac.snc.vo.EstadisticasRegistroVO;
import co.gov.igac.snc.vo.HistoricoAvaluosVO;
import co.gov.igac.snc.vo.ListDetallesVisorVO;
import co.gov.igac.snc.vo.PredioInfoVO;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

@Stateless
//@Interceptors(TimeInterceptor.class) 
public class ConservacionBean implements IConservacion, IConservacionLocal {

    /**
     *
     */
    @SuppressWarnings("unused")
    private static final long serialVersionUID = 788578817708515822L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConservacionBean.class);

    /**
     * Interfaces locales de servicios
     */
    @EJB
    private ISolicitudDAO solicitudDao;

    @EJB
    private ITramite tramiteService;

    @EJB
    private ITramiteDetallePredioDAO tramiteDetallePredioService;

    @EJB
    private IProcesos procesosService;

    @EJB
    private IIntegracionLocal integracionService;

    @EJB
    private IGenerales generalesService;

    @EJB
    private IAplicarCambios aplicarCambios;

    @EJB
    private ITransaccionalLocal transaccionalService;

    /**
     * Interfaces locales de acceso a datos (Dao)
     */
    @EJB
    private IDocumentoDAO documentoDao;

    @EJB
    private ITipoDocumentoDAO tipoDocumentoDao;

    @EJB
    private IPredioAvaluoCatastralDAO predioAvaluoCatastralDao;

    @EJB
    private ITramiteDAO tramiteDao;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;

    @EJB
    private IConsultaPredio consultaPredioDao;

    @EJB
    private IConsultaPPredio consultaPPredioDao;

    @EJB
    private IPredioDAO predioDao;

    @EJB
    private IColindanteDAO colindanteDao;

    @EJB
    private IPersonaPredioDAO personaPredioDao;

    @EJB
    private IPPredioDireccionDAO pPredioDireccionDao;

    @EJB
    private IPredioDireccionDAO predioDireccionDao;

    @EJB
    private IPPredioServidumbreDAO pPredioServidumbreDao;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private IPredioServidumbreDAO predioServidumbreDao;

    @EJB
    private IHPredioDAO hPredioDao;

    @EJB
    private IPredioBloqueoDAO predioBloqueoDao;

    @EJB
    private IPrediosBloqueoDAO prediosBloqueoDao;

    @EJB
    private IPersonasBloqueoDAO personasBloqueoDao;

    @EJB
    private IPersonaBloqueoDAO personaBloqueoDao;

    @EJB
    private IUnidadConstruccionDAO unidadConstruccionDao;

    private IUnidadConstruccionCompDAO unidadConstruccionCompDao;

    @EJB
    private IUsoConstruccionDAO usoConstruccionDao;

    @EJB
    private IVUsoConstruccionZonaDAO vUsoConstruccionZonaDao;

    @EJB
    private IPPredioDAO pPredioDao;

    @EJB
    private ICalificacionConstruccionDAO calificacionConstruccionDao;

    @EJB
    private IPPersonaPredioPropiedadDAO pPersonaPredioPropiedadDao;

    @EJB
    private IHPersonaPredioPropiedadDAO hPersonaPredioPropiedadDao;

    @EJB
    private IProyectarConservacion proyectarConservacionDao;

    @EJB
    private IPFotoDAO pFotoDao;

    @EJB
    private IAxMunicipioDAO axMunicipioDAO;

    @EJB
    private IPModeloConstruccionFotoDAO pModeloConstruccionFotoDao;

    @EJB
    private IFotoDAO fotoDao;

    @EJB
    private IPredioZonaDAO predioZonaDAO;

    @EJB
    private IPersonaDAO personaDao;

    @EJB
    private IPPredioAvaluoCatastralDAO pPredioAvaluoCatastralDao;

    @EJB
    private IPUnidadConstruccionDAO pUnidadConstruccionDao;

    @EJB
    private IPUnidadConstruccionCompDAO pUnidadConstruccionCompDao;

    @EJB
    private ITipificacionUnidadConstDAO tipificacionUnidadConstruccionDao;

    @EJB
    private ICalificacionAnexoDAO calificacionAnexoDao;

    @EJB
    private IFichaMatrizDAO fichaMatrizDao;

    @EJB
    private IPFichaMatrizDAO pFichaMatrizDao;

    @EJB
    private IPFichaMatrizTorreDAO pFichaMatrizTorreDao;

    @EJB
    private IFichaMatrizPredioDAO fichaMatrizPredioDao;

    @EJB
    private IFichaMatrizTorreDAO fichaMatrizTorreDao;

    @EJB
    private IPFichaMatrizPredioDAO pFichaMatrizPredioDao;

    @EJB
    private IPPersonaPredioDAO pPersonaPredioDao;

    @EJB
    private IPPredioZonaDAO pPredioZonaDao;

    @EJB
    private IHorarioAtencionTerritorialDAO horarioAtencionDao;

    @EJB
    private IVPropiedadTierraDAO vPropiedadTierraDao;

    @EJB
    private IPReferenciaCartograficaDAO pReferenciaCartograficaDao;

    @EJB
    private IPPersonaDAO pPersonaDao;

    @EJB
    private ITipificacionUnidadConstDAO tipificacionUnidadConstDao;

    @EJB
    private IPFichaMatrizModeloDAO pFichaMatrizModeloDao;

    @EJB
    private IPFmConstruccionComponenteDAO pFmConstruccionComponenteDao;

    @EJB
    private ITmpBloqueoDAO bloqueoMasivoService;

    @EJB
    private IPredioBloqueoDAO predioBloqueService;

    @EJB
    private IPManzanaVeredaDAO manzanaVeredaService;

    @EJB
    private IPFmModeloConstruccionDAO pFmModeloConstruccionDao;

    @EJB
    private ITmpBloqueoDAO tmpBloqueoService;

    @EJB
    private IPredioPropietarioHisDAO predioPropietarioHisDao;

    @EJB
    private IConservacion conservacionService;

    @EJB
    private ITramiteMovilesDAO tramiteMovilesDao;

    @EJB
    private IPredioInconsistenciaDAO predioInconsistenciaDao;

    @EJB
    private IEstadisticaGerencialPredioDAO estadisticaGerenciaPredioDao;

    @EJB
    private IEstadisticaGerencialManzanaDAO estadisticaGerenciaManzanaDao;

    @EJB
    private IProductoCatastralJobDAO productoCatastralJobDAO;

    @EJB
    private IJurisdiccionDAO jurisdiccionDao;

    @EJB
    private IReporteControlReporteDAO reporteControlReporteDao;

    @EJB
    private IReporteDatosReporteDAO reporteDatosReporteDao;

    @EJB
    private IDepartamentoDAO departamentoDao;

    @EJB
    private IMunicipioDAO municipioDao;

    @EJB
    private ITramiteTareaDAO tramiteTareaDao;

    @EJB
    private IRepReporteEjecucionDAO repReporteEjecucionDAO;

    @EJB
    private IRepReporteEjecucionDetalleDAO repReporteEjecucionDetalleDAO;

    @EJB
    private IListadoPredioDAO listadoPredioDao;

    @EJB
    private IFichaMatrizPredioDAO fichaMatrizPredioDAO;

    @EJB
    private IFormacion formacionService;

    @EJB
    private IHFichaMatrizPredioDAO hFichaMatrizPredioDao;

    @EJB
    private IDecretoAvaluoAnualDAO decretoAvaluoAnualDAO;

    @EJB
    private IPredioLinderoDAO predioLinderoDAO;

    @EJB
    private IRepUsuarioRolReporteDAO repUsuarioRolReporteDAO;

    @EJB
    private ITramiteDocumentoDAO tramiteDocumentoDao;

    // --------------------------------------------------------------------------------------------------
    @EJB
    private IEntidadDAO entidadDao;

    @EJB
    private IPredioBloqueoDAO predioBloqueoDAOBean;

    @EJB
    private IUsoHomologadoDAO usoHomologadoDAO;

    @EJB
    private IRepReporteEjecucionUsuarioDAO ejecucionUsuarioDAO;

    @EJB
    private IRepConfigParametroReporteDAO repConfigParametroReporteDAO;

    @EJB
    private IHFichaMatrizDAO hFichaMatrizDAO;

    @EJB
    private IPFichaMatrizPredioTerrenoDAO fichaMatrizPredioTerrenoDao;

    @EJB
    private IRangoReporteDAO rangoReporteDAO;

    /**
     * @see IConservacion#getPredioByNumeroPredial(String)
     */
    @Override
    public Predio getPredioByNumeroPredial(String idPredio) {
        LOGGER.debug("Ingreso al método: getPredioByNumeroPredial en ConservacionBean");
        return this.predioDao.getPredioByNumeroPredial(idPredio);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IConservacion#getPredioFetchPersonasById(Id)
     */
    @Override
    public Predio getPredioFetchPersonasById(Long predioId) {

        LOGGER.debug("en ConservacionBean#getPredioFetchPersonasById ");
        Predio answer = null;

        try {
            answer = this.consultaPredioDao.getPredioFetchPersonasById(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#getPredioFetchPersonasById: " + ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#findHPredioListByConsecutivoCatastral(BigDecimal)
     */
    @Override
    public List<HPredio> findHPredioListByConsecutivoCatastral(
        BigDecimal consecutivoCatastral) {
        List<HPredio> answer;
        answer = this.hPredioDao
            .findHPredioListByConsecutivoCatastral(consecutivoCatastral);
        LOGGER.debug(answer.size() + "ConservacionBean");

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    private Predio findPredioByNumeroPredial(String numeroPredial) {
        return this.predioDao.getPredioByNumeroPredial(numeroPredial);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#getPredioServidumbreByPredio(Predio)
     */
    @Override
    public List<PredioServidumbre> getPredioServidumbreByPredio(Predio predio) {
        return predioServidumbreDao.getPredioServidumbreByPredio(predio);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#consultarPredios(Predio)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Object[]> searchPredios(FiltroDatosConsultaPredio datosConsultaPredio,
        int from, int howMany) {

        List<Object[]> answer = null;

        answer = this.predioDao.searchForPredios(datosConsultaPredio, from, howMany);
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public UnidadConstruccion findUnidadConstruccionByPredioANDUnidad(
        Predio predio, String unidad) {
        return unidadConstruccionDao
            .findUnidadConstruccionByPredioANDUnidad(predio, unidad);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#findUnidadConstruccionCompByUnidadConstruccion(UnidadConstruccion)
     */
    @Override
    public List<UnidadConstruccionComp> findUnidadConstruccionCompByUnidadConstruccion(
        UnidadConstruccion unidad) {

        try {
            return this.unidadConstruccionCompDao
                .findUnidadConstruccionCompByUnidadConstruccion(unidad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#findUnidadConstruccionCompByUnidadConstruccion: " +
                ex.getMensaje());
        }

        return null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#getPredioBloqueo(java.lang.String)
     */
    @Override
    public PredioBloqueo getPredioBloqueo(String numeroPredial) {

        PredioBloqueo answer = null;

        try {
            answer = this.predioBloqueoDao.findCurrentPredioBloqueo(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#getPredioBloqueo: " + ex.getMensaje());
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#obtenerPredioBloqueosPorNumeroPredial(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<PredioBloqueo> obtenerPredioBloqueosPorNumeroPredial(
        String numeroPredial) {
        return this.prediosBloqueoDao
            .getPredioBloqueosByNumeroPredial(numeroPredial);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#contarPersonasBloqueo(FiltroDatosConsultaPersonasBloqueo)
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public Integer contarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo) {

        Integer cpb = null;

        try {
            cpb = this.predioBloqueoDao.contarPrediosBloqueo(filtroDatosConsultaPredioBloqueo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#contarPrediosBloqueo: " + ex.getMensaje());
        }

        return cpb;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IConservacion#buscarPrediosBloqueo(FiltroDatosConsultaPrediosBloqueo, int...)
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<PredioBloqueo> buscarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo,
        final int... rowStartIdxAndCount) {

        List<PredioBloqueo> pb = this.prediosBloqueoDao
            .buscarPrediosBloqueo(filtroDatosConsultaPredioBloqueo,
                rowStartIdxAndCount);

        return pb;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IConservacion#actualizarPrediosBloqueo(UsuarioDTO, List)
     */
    @Implement
    @Override
    public ResultadoBloqueoMasivo actualizarPrediosBloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloqueados) {

        return this.prediosBloqueoDao.actualizarPrediosBloqueo(usuario,
            prediosBloqueados);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#buscarPersonasBloqueo(FiltroDatosConsultaPersonasBloqueo)
     */
    @Override
    public List<PersonaBloqueo> buscarPersonasBloqueo(
        FiltroDatosConsultaPersonasBloqueo filtroDatosConsultaPersonasBloqueo) {

        List<PersonaBloqueo> pb = null;
        try {
            pb = this.personaBloqueoDao
                .buscarPersonasBloqueo(filtroDatosConsultaPersonasBloqueo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPersonasBloqueo: " + ex.getMensaje());
        }
        return pb;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#actualizarPersonasBloqueo(UsuarioDTO, List)
     */
    @Override
    public List<PersonaBloqueo> actualizarPersonasBloqueo(UsuarioDTO usuario,
        List<PersonaBloqueo> personasBloqueadas) {

        List<PersonaBloqueo> answer = null;

        try {
            answer = this.personaBloqueoDao.actualizarPersonasBloqueo(usuario, personasBloqueadas);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#actualizarPersonasBloqueo: " + ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see
     * IConservacionLocal#buscarPrediosCriterios(co.gov.igac.snc.util.FiltroGenerarReportes,int,
     * int...)
     * @author javier.barajas
     *
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Object[]> buscarPrediosCriterios(
        FiltroGenerarReportes datosConsultaPredio, int datosADesplegar,
        int... idPrimeraFilaYCuantos) {

        List<Object[]> answer = null;

        try {
            answer = this.sncProcedimientoDao.buscarPrediosPorCriteriosRangoNumeroPredial(
                datosConsultaPredio, false, datosADesplegar, idPrimeraFilaYCuantos);
            LOGGER.debug("Tamaño objeto: " + answer.size());

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en buscarPredios: " + ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     * @see IConservacion#getPredioFetchAvaluosByNumeroPredial(String)
     */
    @Implement
    @Override
    public Predio getPredioFetchAvaluosByNumeroPredial(String numPredial) {
        // return
        // predioService.getPredioFetchAvaluosByNumeroPredial(numPredial);
        return consultaPredioDao
            .getPredioFetchAvaluosByNumeroPredial(numPredial);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#findPersonaBloqueoByPersonaId(java.lang.Long)
     */
    @Implement
    @Override
    public PersonaBloqueo findPersonaBloqueoByPersonaId(Long idPersona) {

        PersonaBloqueo answer = null;
        try {
            answer = this.personaBloqueoDao.findCurrentPersonaBloqueoByPersonaId(idPersona);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("excepción en ConservacionBean#findPersonaBloqueoByPersonaId: " +
                ex.getMensaje());
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#obtenerPersonaBloqueosPorPersonaId(Long)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<PersonaBloqueo> obtenerPersonaBloqueosPorPersonaId(Long idPersona) {

        try {
            return this.personaBloqueoDao.getPersonaBloqueosByPersonaId(idPersona);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPersonaBloqueosPorPersonaId: " +
                ex.getMensaje());
        }
        return null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#obtenerUsosConstruccion()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<UsoConstruccion> obtenerUsosConstruccion() {
        List<UsoConstruccion> answer = null;

        try {
            answer = this.usoConstruccionDao.findAllOrderedByColumn("nombre");
        } catch (Exception ex) {
            LOGGER.error("excepción consultando los usos de construcción: " + ex.getMessage());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#obtenerVUsosConstruccionZonas()
     * @author fredy.wilches
     */
    @Implement
    @Override
    public List<VUsoConstruccionZona> obtenerVUsosConstruccionZona(String zona) {
        List<VUsoConstruccionZona> answer = null;

        try {
            answer = this.vUsoConstruccionZonaDao.buscarVUsosConstruccionPorZona(zona);
        } catch (Exception ex) {
            LOGGER.error("excepción consultando los v usos de construcción por zona: " + ex.
                getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#findTramitesByPredio(java.lang.String)
     */
    @Implement
    @Override
    public List<Tramite> getTramitesByNumeroPredialPredio(String numeroPredial) {
        return this.tramiteDao.getTramitesByNumeroPredialPredio(numeroPredial);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#getFichaMatrizByNumeroPredialPredio
     */
    @Override
    public FichaMatriz getFichaMatrizByNumeroPredialPredio(String numeroPredial) {
        return this.fichaMatrizDao
            .getFichaMatrizByNumeroPredialPredio(numeroPredial);
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#getPredioFetchDerechosPropiedadByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchDerechosPropiedadByNumeroPredial(
        String numPredial) {
        return this.consultaPredioDao
            .getPredioFetchDerechosPropiedadByNumeroPredial(numPredial);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#obtenerPredioPorId(java.lang.Long)
     * @author pedro.garcia OJO: NO modificar
     */
    @Implement
    @Override
    public Predio obtenerPredioPorId(Long predioId) {

        LOGGER.debug("on ConservacionBean#obtenerPredioPorId ..");
        Predio answer = null;

        try {
            answer = this.predioDao.getPredioById(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción consultando el predio con id = " + predioId);
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @see IConservacion#ObtenerPredioConDatosUbicacionPorNumeroPredial(String)
     * @modified pedro.garcia 14-05-2012 mejoras. try y catch
     */
    @Override
    public Predio obtenerPredioConDatosUbicacionPorNumeroPredial(String numPredial) {

        Predio answer = null;
        try {
            answer = this.consultaPredioDao.
                getPredioFetchDatosUbicacionByNumeroPredial(numPredial);
        } catch (Exception ex) {
            LOGGER.error("error en IConservacion#ObtenerPredioConDatosUbicacionPorNumeroPredial: " +
                ex.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see
     * IConservacion#contarResultadosBusquedaPredios(co.gov.igac.snc.util.FiltroDatosConsultaPredio)
     * @author pedro.garcia
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public int contarResultadosBusquedaPredios(
        FiltroDatosConsultaPredio datosConsultaPredio) {
        return this.predioDao.searchForPrediosCount(datosConsultaPredio);
    }

    /**
     * @author fabio.navarrete
     * @see IConservacion#obtenerPPredioPorIdConAvaluos(Long)
     */
    @Override
    public PPredio obtenerPPredioPorIdConAvaluos(Long idPredio) {
        return this.proyectarConservacionDao
            .findPPredioByIdFetchAvaluos(idPredio);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#obtenerTodosCalificacionConstruccion()
     * @author pedro.garcia
     */
    @Override
    public List<CalificacionConstruccion> obtenerTodosCalificacionConstruccion() {

        List<CalificacionConstruccion> answer = null;

        // D: hay que ordenarlos por la columna "componente" para que el armado del treenode
        // funcione bien. El ordenamiento adicional es por presentación
        List<String> columNames;
        columNames = new ArrayList<String>();

        /*
         * Ordenamiento modificado por FGWL, peticion funcional. Funcionales aseguran el orden por
         * id componente, elemento, detalleCalificacion
         */
        columNames.add("id");

        try {
            answer = this.calificacionConstruccionDao.findAllOrderedByColumns(columNames);
        } catch (Exception ex) {
            LOGGER.error("Error en ConservacionBean#obtenerTodosCalificacionConstruccion: " +
                ex.getMessage());
        }

        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#actualizarPPredio(PPredio)
     */
    @Override
    public PPredio actualizarPPredio(PPredio predioSeleccionado) {
        try {
            PPredio auxPredio = pPredioDao.update(predioSeleccionado);
            predioSeleccionado.setId(auxPredio.getId());
            return predioSeleccionado;
        } catch (RuntimeException rtex) {
            return null;
        }
    }

    /**
     * @author javier.aponte
     * @see IConservacion#obtenerPPredioPorIdTraerDirecciones(Long)
     */
    @Override
    public PPredio obtenerPPredioPorIdTraerDirecciones(Long idPredio) {
        return this.pPredioDao.findPPredioByIdFetchDirecciones(idPredio);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#gestionarJustificacion(PPersonaPredioPropiedad)
     */
    @Override
    public void gestionarJustificacion(
        List<PPersonaPredioPropiedad> justificacion) {

        try {
            this.pPersonaPredioPropiedadDao.gestionarSolicitud(justificacion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#gestionarJustificacion: " + ex.getMensaje());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#getPPredioFetchPPersonaPredioPorID(Long)
     */
    @Override
    public PPredio getPPredioFetchPPersonaPredioPorId(Long id) {
        return this.consultaPPredioDao
            .getPPredioFetchPPersonaPredioPorId(id);
    }

    /**
     * @author fredy.wilches
     *
     * Metodo que consulta pPredio pero lo trae con todas sus colecciones dependientes.
     *
     * @modified david.cifuentes Asociación de las pUnidadConstrucción.
     *
     * @throws Exception
     */
    @Override
    public PPredio obtenerPPredioCompletoById(Long idPredio) throws Exception {
        PPredio pPredio = this.pPredioDao.findPPredioCompletoById(idPredio);
        // Asociar las PUnidadConstruccion
        List<PUnidadConstruccion> pUnidadConstruccions = this.pUnidadConstruccionDao
            .buscarUnidadesDeConstruccionPorPPredioId(idPredio, true);
        if (pUnidadConstruccions != null && !pUnidadConstruccions.isEmpty()) {
            pPredio.setPUnidadConstruccions(pUnidadConstruccions);
        }
        return pPredio;
    }

    /**
     * @author fredy.wilches Metodo que consulta pPredio pero lo trae con todas sus colecciones
     * dependientes.
     *
     * @modified david.cifuentes Asociación de las pUnidadConstruccion.
     *
     * @throws Exception
     */
    @Override
    public PPredio obtenerPPredioCompletoByIdyTramite(Long idPredio, Long idTramite) throws
        Exception {
        PPredio pPredio = this.pPredioDao.findPPredioCompletoByIdyTramite(idPredio, idTramite);
        // Asociar las PUnidadConstruccion
        List<PUnidadConstruccion> pUnidadConstruccions = this.pUnidadConstruccionDao
            .buscarUnidadesDeConstruccionPorPPredioId(idPredio, true);
        if (pUnidadConstruccions != null && !pUnidadConstruccions.isEmpty()) {
            pPredio.setPUnidadConstruccions(pUnidadConstruccions);
        }
        return pPredio;
    }

    /**
     * @author fredy.wilches Metodo que consulta pPredio, buscando por el id del tramite pero lo
     * trae con todas sus colecciones dependientes. A usarse unicamente en quinta nuevo
     * @throws Exception
     */
    @Override
    public PPredio obtenerPPredioCompletoByIdTramite(Long idTramite) throws Exception {
        PPredio pPredio = this.pPredioDao.findPPredioCompletoByIdTramite(idTramite);
        // Asociar las PUnidadConstruccion
        List<PUnidadConstruccion> pUnidadConstruccions = this.pUnidadConstruccionDao
            .buscarUnidadesDeConstruccionPorPPredioId(pPredio.getId(), true);
        if (pUnidadConstruccions != null && !pUnidadConstruccions.isEmpty()) {
            pPredio.setPUnidadConstruccions(pUnidadConstruccions);
        }
        return pPredio;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#normalizarDireccion(String)
     */
    @Override
    public String normalizarDireccion(String direccion) {
        return predioDao.normalizarDireccion(direccion);
    }

    /**
     * @author fabio.navarrete
     * @see IConservacion#eliminarProyeccion(Object)
     */
    @Override
    public IProyeccionObject eliminarProyeccion(UsuarioDTO usuario, IProyeccionObject objP) {
        return proyectarConservacionDao.deleteProyeccion(usuario, objP);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#PPredioDirecciones(predioDirecciones)
     */
    @Override
    public void borrarPPredioDirecciones(PPredioDireccion[] predioDirecciones) {
        int i;
        for (i = 0; i < predioDirecciones.length; i++) {
            this.pPredioDireccionDao.delete(predioDirecciones[i]);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#PPredioServidumbres(predioServidumbres)
     */
    @Override
    public void borrarPPredioServidumbres(
        PPredioServidumbre[] predioServidumbres) {
        int i;
        for (i = 0; i < predioServidumbres.length; i++) {
            this.pPredioServidumbreDao.delete(predioServidumbres[i]);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#borrarPPredioDireccion(PPredioDireccion)
     */
    /*
     * @modified by leidy.gonzalez:: 11-05-2015:: 11117:: Se modifica manera de actualizar la
     * servidumbre
     */
    @Override
    public PPredioServidumbre adicionarServidumbre(PPredioServidumbre servidumbreAdicional) {
        PPredioServidumbre pPredioServidumbre;
        pPredioServidumbre = this.pPredioServidumbreDao.update(servidumbreAdicional);

        return pPredioServidumbre;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fredy.wilches
     * @see IConservacionLocal#guardarPFoto(PFoto pFoto, PUnidadConstruccionDto unidadConstruccion)
     */
    @Override
    public PFoto guardarPFoto(PFoto pFoto, UsuarioDTO usuario) {

        Documento documentoGuardado;
        String rutaLocal;

        if (null == pFoto.getDocumento().getRutaArchivoLocal() ||
            pFoto.getDocumento().getRutaArchivoLocal().isEmpty()) {
            LOGGER.debug("La ruta local de la PFoto es vacía o nula");
            return pFoto;
        }

        rutaLocal = pFoto.getDocumento().getRutaArchivoLocal();
        documentoGuardado = new Documento();

        try {
            documentoGuardado = this.documentoDao.almacenarDocumentoFotoEnGestorDocumental(
                pFoto.getDocumento(), usuario, rutaLocal);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "No se pudo guardar en el gestor documental la foto correspondiente a la PFoto." +
                "En ConservacionBean#guardarPFoto. Excepción: " + ex.getMessage());
        }

        pFoto.setDocumento(documentoGuardado);
        pFoto = this.pFotoDao.guardarPFoto(pFoto);

        return pFoto;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     * @see IConservacion#guardarProyeccion(Object)
     */
    @Override
    public Object guardarProyeccion(UsuarioDTO usuario, IProyeccionObject objP) {
        return proyectarConservacionDao.saveProyeccion(usuario, objP);
    }

    /**
     * @author fabio.navarrete
     * @see IConservacion#obtenerPUnidadConstruccionPorId(Long)
     */
    @Override
    public IModeloUnidadConstruccion obtenerPUnidadConstruccionPorId(Long id) {
        return this.pUnidadConstruccionDao.findById(id);
    }

    /**
     * @author fabio.navarrete
     * @see IConservacion#actualizarPPredioAvaluoCatastral(PPredioAvaluoCatastral)
     */
    @Override
    public PPredioAvaluoCatastral actualizarPPredioAvaluoCatastral(
        PPredioAvaluoCatastral ppaval) {
        return this.pPredioAvaluoCatastralDao.update(ppaval);
    }

    /**
     * * @author jonathan.chacon
     * @param ppaval
     * @return
     */
    @Override
    public PPredioAvaluoCatastral guardarPPredioAvaluoCatastral(
        PPredioAvaluoCatastral ppaval) {
        return this.pPredioAvaluoCatastralDao.persistRE(ppaval);
    }

    /**
     * Eliminar un avaluo de un ppredio
     */
    @Override
    public void eliminarPPredioAvaluoCatastral(PPredioAvaluoCatastral avaluo) {
        this.pPredioAvaluoCatastralDao.delete(avaluo);
    }

    /**
     * Eliminar un avaluo de un ppredio todas las vigencias
     *
     * @author jonathan.chacon
     * @param avaluo
     */
    @Override
    public void eliminarPPredioAvaluoCatastralCompleto(List<PPredioAvaluoCatastral> avaluo) {
        this.pPredioAvaluoCatastralDao.deleteMultiple(avaluo);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#obtenerTipificacionUnidadConstrPorPuntos(int)
     * @author pedro.garcia
     */
    @Override
    public TipificacionUnidadConst obtenerTipificacionUnidadConstrPorPuntos(int puntos) {

        TipificacionUnidadConst answer = null;

        try {
            answer = this.tipificacionUnidadConstruccionDao.findByPoints(puntos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerTipificacionUnidadConstrPorPuntos: " +
                ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#insertarPUnidadConstruccionCompRegistros(List<PUnidadConstruccionComp>)
     */
    /*
     * @modified david.cifuentes Corrección de implementación.
     */
    @Override
    public List<PUnidadConstruccionComp> insertarPUnidadConstruccionCompRegistros(
        List<PUnidadConstruccionComp> newUnidadesConstruccionComp) {

        LOGGER.debug(
            "ConservacionBean#insertarPUnidadConstruccionCompRegistros insertando registros");
        this.pUnidadConstruccionCompDao.persistMultiple(newUnidadesConstruccionComp);
        String[] criterio = new String[]{"id", "=", "" + newUnidadesConstruccionComp.get(0).
            getPUnidadConstruccion().getId()};
        List<String[]> criterios = new ArrayList<String[]>();
        criterios.add(criterio);
        return pUnidadConstruccionCompDao.findByCriteria(criterios);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IConservacionLocal#buscarFotografiasPredio(Long)
     */
    @Implement
    @Override
    public List<Foto> buscarFotografiasPredio(Long predioId) {

        List<Foto> answer;
        answer = this.fotoDao.buscarFotografiasPredio(predioId);
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerCalificacionesAnexoPorIdUsoConstruccion(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<CalificacionAnexo> obtenerCalificacionesAnexoPorIdUsoConstruccion(
        Long selectedUsoUnidadConstruccionId) {

        List<CalificacionAnexo> answer = null;
        answer = this.calificacionAnexoDao
            .findByUsoConstruccionId(selectedUsoUnidadConstruccionId);
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#actualizarPUnidadConstruccionCompRegistros(java.util.List)
     */
    /*
     * @modified david.cifuentes Corrección de implementación.
     */
    @Override
    public List<PUnidadConstruccionComp> actualizarPUnidadConstruccionCompRegistros(
        List<PUnidadConstruccionComp> unidadesConstruccionComp) {

        this.pUnidadConstruccionCompDao.updateByUnidadConstruccionId(unidadesConstruccionComp);
        String[] criterio = new String[]{"id", "=", "" + unidadesConstruccionComp.get(0).
            getPUnidadConstruccion().getId()};
        List<String[]> criterios = new ArrayList<String[]>();
        criterios.add(criterio);
        List<PUnidadConstruccionComp> componentesResult = pUnidadConstruccionCompDao.findByCriteria(
            criterios);
        return componentesResult;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#obtenerPredioConAvaluosPorId(String)
     * @author fabio.navarrete
     */
    @Override
    public Predio obtenerPredioConAvaluosPorId(Long predioId) {

        Predio answer = null;
        try {
            answer = this.consultaPredioDao.getPredioFetchAvaluosByPredioId(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPredioConAvaluosPorId: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#borrarPUnidadConstruccionCompRegistros(java.lang.Long)
     */
    @Implement
    @Override
    public void borrarPUnidadConstruccionCompRegistros(Long unidadConstruccionId) {

        try {
            this.pUnidadConstruccionCompDao.deleteByUnidadConstruccionId(unidadConstruccionId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#borrarPUnidadConstruccionCompRegistros: " +
                ex.getMensaje());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarPPrediosPorTramiteId(Long)
     */
    @Override
    public List<PPredio> buscarPPrediosPorTramiteId(Long tramiteId) {
        return this.pPredioDao.findPPrediosByTramiteId(tramiteId);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#guardarPFotos(List)
     * @author juan.agudelo
     */
    /*
     * modified pedro.garcia 03-05-2013 captura de excepciones de negocio
     */
    @Implement
    @Override
    public void guardarPFotos(List<PFoto> pFotosList) {
        try {
            this.pFotoDao.guardarPFotos(pFotosList);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en ConservacionBean#guardarPFotos: " + ex.getMensaje());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @see IConservacion#obtenerPredioConPersonasPorPredioId(Long)
     * @modified pedro.garcia organización de código. Manejo de excepciones
     */
    @Override
    public Predio obtenerPredioConPersonasPorPredioId(Long predioId) {

        Predio answer = null;

        try {
            answer = this.consultaPredioDao.findPredioFetchPersonasByPredioId(predioId);
            if (answer != null) {
                Hibernate.initialize(answer.getUnidadConstruccions());
                for (UnidadConstruccion unidadConstruccion : answer.getUnidadConstruccions()) {
                    Hibernate.initialize(unidadConstruccion.getUsoConstruccion());
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en ConservacionBean#obtenerPredioConPersonasPorPredioId: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarPFotosUnidadConstruccion(java.lang.Long)
     * @author pedro.garcia
     */
    @Override
    @Implement
    public List<PFoto> buscarPFotosUnidadConstruccion(Long unidadConstruccionId,
        boolean cargarEnWebTemporal) {

        List<PFoto> pfotosList = null;
        IDocumentosService documentalService;
        DocumentoVO documentoVo;

        pfotosList = this.pFotoDao.findByUnidadConstruccionIdFetchDocumento(unidadConstruccionId);

        if (cargarEnWebTemporal) {
            documentalService = DocumentalServiceFactory.getService();
            for (PFoto foto : pfotosList) {
                if (foto.getDocumento().getIdRepositorioDocumentos() != null) {
                    documentoVo = documentalService.cargarDocumentoPrivadoEnWorkspacePreview(
                        foto.getDocumento().getIdRepositorioDocumentos());

                    //D: asigna la ruta en la que queda disponible el archivo al Documento relacionado con la PFoto
                    foto.getDocumento().setRutaArchivoWeb(documentoVo.getUrlPublico());
                } else {
                    foto.getDocumento().setRutaArchivoWeb(null);
                }
            }
        }
        return pfotosList;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fredy.wilches
     * @see IConservacion#buscarImagenesTramite(Tramite t)
     */
    @Override
    public List<Documento> buscarImagenesTramite(Tramite t) {
        List<Documento> documentos = this.documentoDao.buscarImagenesTramite(t);

        for (Documento d : documentos) {
            if (d.getIdRepositorioDocumentos() != null &&
                !d.getIdRepositorioDocumentos().trim().isEmpty()) {
                String path = this.documentoDao
                    .descargarOficioDeGestorDocumentalATempLocalMaquina(d
                        .getIdRepositorioDocumentos());
                IDocumentosService service = DocumentalServiceFactory
                    .getService();

                /*
                 * refs #4729 :: juanfelipe.garcia :: 15/05/2013 :: cambio de metodo en Documental
                 * Se carga el documento de la temporal local, a la carpeta publica de alfresco para
                 * poder verlo por http
                 */
                String rutaTemp = new File(path).getAbsolutePath();
                DocumentoVO documentoVO = service.cargarDocumentoWorkspacePreview(rutaTemp);
                String rutaPreview = documentoVO.getUrlPublico();
                d.setArchivo(rutaPreview);

            }
        }
        return documentos;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarPModeloConstruccionFotosPorIdUnidad(Long, boolean)
     * @author david.cifuentes
     */
    @Override
    public List<PModeloConstruccionFoto> buscarPModeloConstruccionFotosPorIdUnidad(
        Long unidadDelModeloId, boolean cargarEnWebTemporal) {

        List<PModeloConstruccionFoto> pfotosUnidadDelModeloList = null;
        IDocumentosService documentalService;
        DocumentoVO documentoVo;

        pfotosUnidadDelModeloList = this.pModeloConstruccionFotoDao
            .findByUnidadDelModeloIdFetchDocumento(unidadDelModeloId);

        if (cargarEnWebTemporal && pfotosUnidadDelModeloList != null &&
            !pfotosUnidadDelModeloList.isEmpty()) {
            documentalService = DocumentalServiceFactory.getService();
            for (PModeloConstruccionFoto foto : pfotosUnidadDelModeloList) {
                if (foto.getFotoDocumento().getIdRepositorioDocumentos() != null) {
                    documentoVo = documentalService
                        .cargarDocumentoPrivadoEnWorkspacePreview(foto
                            .getFotoDocumento()
                            .getIdRepositorioDocumentos());

                    // D: asigna la ruta en la que queda disponible el archivo
                    // al Documento relacionado con la PFoto
                    foto.getFotoDocumento().setRutaArchivoWeb(
                        documentoVo.getUrlPublico());
                } else {
                    foto.getFotoDocumento().setRutaArchivoWeb(null);
                }
            }
        }
        return pfotosUnidadDelModeloList;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#actualizarPFotosUnidadConstruccion(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void actualizarPFotosUnidadConstruccion(List<PFoto> pFotosList) {

        try {
            this.pFotoDao.updateDataByUnidadConstruccion(pFotosList);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#actualizarPFotosUnidadConstruccion: " +
                ex.getMensaje());
        }
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#contarPrediosBloqueoByNumeroPredial(String)
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public Integer contarPrediosBloqueoByNumeroPredial(String numeroPredial) {
        return this.prediosBloqueoDao
            .contarPrediosBloqueoByNumeroPredial(numeroPredial);
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#buscarPrediosBloqueoByNumeroPredial(String, int...)
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<PredioBloqueo> buscarPrediosBloqueoByNumeroPredial(
        String numeroPredial, final int... rowStartIdxAndCount) {
        return this.prediosBloqueoDao.buscarPrediosBloqueoByNumeroPredial(
            numeroPredial, rowStartIdxAndCount);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#buscarPrediosBloqueoByNumeroPredial(String, int...)
     */
    @Override
    public List<PredioBloqueo> actualizarPrediosDesbloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloqueados) {
        return this.prediosBloqueoDao.actualizarPrediosDesbloqueo(usuario,
            prediosBloqueados);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#buscarPersonaBloqueoByNumeroIdentificacion(String)
     */
    @Override
    public List<PersonaBloqueo> buscarPersonaBloqueoByNumeroIdentificacion(
        String numeroIdentificacion, String tipoIdentificacion) {
        return this.personaBloqueoDao
            .buscarPersonaBloqueoByNumeroIdentificacion(
                numeroIdentificacion, tipoIdentificacion);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#buscarPersonaByNombreDocumentoDV(Persona)
     */
    @Override
    public Persona buscarPersonaByNombreDocumentoDV(Persona personaDatos) {
        return this.personaDao.buscarPersonaByNombreDocumentoDV(personaDatos);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#actualizarPersonaDesbloqueo(UsuarioDTO, List)
     */
    @Override
    public List<PersonaBloqueo> actualizarPersonaDesbloqueo(UsuarioDTO usuario,
        List<PersonaBloqueo> personaBloqueada) {

        try {
            return this.personasBloqueoDao.actualizarPersonasDesbloqueo(usuario, personaBloqueada);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("Error en ConservacionBean#actualizarPersonaDesbloqueo: " + ex.getMensaje());
        }

        return null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#actualizarPrediosBloqueoM(UsuarioDTO, ArrayList)
     */
    @Override
    public List<PredioBloqueo> actualizarPrediosBloqueoM(UsuarioDTO usuario,
        ArrayList<PredioBloqueo> bloqueoMasivo) {
        List<PredioBloqueo> pb = this.prediosBloqueoDao
            .actualizarPrediosBloqueoM(usuario, bloqueoMasivo);

        return pb;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#actualizarPersonaBloqueoM(UsuarioDTO, ArrayList)
     */
    @Override
    public List<PersonaBloqueo> actualizarPersonaBloqueoM(UsuarioDTO usuario,
        ArrayList<PersonaBloqueo> bloqueoMasivo) {
        List<PersonaBloqueo> pb = this.personasBloqueoDao
            .actualizarPersonaBloqueoM(usuario, bloqueoMasivo);

        return pb;
    }
//--------------------------------------------------------------------------------------------------
    //TODO :: el que lo hizo :: 21-10-11 :: poner autor y documentar :: juan.agudelo

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<UsoConstruccion> buscarUsosConstruccionPorNumerosPredialesPredio(
        List<String> numerosPrediales) {
        return this.usoConstruccionDao
            .buscarUsosConstruccionPorNumerosPredialesPredio(numerosPrediales);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#getPredioPorNumeroPredial(String)
     *
     */
    @Implement
    @Override
    public Predio getPredioPorNumeroPredial(String numeroPredial) {

        try {
            return this.predioDao.getPredioPorNumeroPredial(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#getPredioPorNumeroPredial: " + ex.getMensaje());
        }

        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 22-08-2012 Manejo de excepciones
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPorTramiteId(Long tramiteId) {

        List<Predio> answer = null;

        try {
            answer = this.tramiteDao.buscarPrediosPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPrediosPorTramiteId: " + ex.getMensaje());
        }
        return answer;
    }

    // ------------------------------------------------- //
    /**
     * @see IConservacion#buscarPrediosPorListaDeIdsTramite(List<Long>)
     * @author david.cifuentes
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPorListaDeIdsTramite(List<Long> idsTramites) {

        List<Predio> answer = null;

        try {
            answer = this.tramiteDao
                .buscarPrediosPorListaDeIdsTramite(idsTramites);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPrediosPorTramiteId: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPanelAvaluoPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 15-08-2012. Orden. Manejo de excepciones.
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPanelAvaluoPorTramiteId(Long tramiteId) {

        LOGGER.debug("en ConservacionBean#buscarPrediosPanelAvaluoPorTramiteId");

        List<Predio> answer = null;

        try {
            answer = this.tramiteDao.buscarPrediosPanelAvaluoPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPrediosPanelAvaluoPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPanelJPropiedadPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 22-08-2012. Orden. Manejo de excepciones.
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPanelJPropiedadPorTramiteId(Long tramiteId) {

        List<Predio> answer = null;

        try {
            answer = this.tramiteDao.buscarPrediosPanelJPropiedadPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPrediosPanelJPropiedadPorTramiteId: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPanelPropietariosPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPanelPropietariosPorTramiteId(
        Long tramiteId) {
        return this.tramiteDao.buscarPrediosPanelPropietariosPorTramiteId(tramiteId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPanelFichaMatrizPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPanelFichaMatrizPorTramiteId(Long tramiteId) {
        return this.tramiteDao.buscarPrediosPanelFichaMatrizPorTramiteId(tramiteId);
    }

    /**
     * @see IConservacion#buscarPPrediosPanelFichaMatrizPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public List<PPredio> buscarPPrediosPanelFichaMatrizPorTramiteId(Long tramiteId) {
        return this.pPredioDao.buscarPPrediosPanelFichaMatrizPorTramiteId(tramiteId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPanelUbicacionPredioPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPanelUbicacionPredioPorTramiteId(
        Long tramiteId) {
        return this.tramiteDao.buscarPrediosPanelUbicacionPredioPorTramiteId(tramiteId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPPrediosPanelAvaluoPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 14-08-2012 manejo de excepciones. Orden.
     */
    @Implement
    @Override
    public List<PPredio> buscarPPrediosPanelAvaluoPorTramiteId(Long tramiteId) {

        LOGGER.debug("en ConservacionBean#buscarPPrediosPanelAvaluoPorTramiteId");

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPrediosPanelAvaluoPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPrediosPanelAvaluoPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPPersonasPorTipoNumeroIdentificacion(String, String)
     */
    @Override
    public List<PPersona> buscarPPersonasPorTipoNumeroIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion) {
        try {
            List<PPersona> ppersonas = new ArrayList<PPersona>();
            List<Persona> personas = this.personaDao
                .findByTipoNumeroIdentificacion(tipoIdentificacion,
                    numeroIdentificacion);
            if (personas != null) {
                for (Persona persona : personas) {
                    PPersona ppersona = new PPersona();
                    //BeanUtils.copyProperties(ppersona, persona);
                    /* Mapper mapper = new DozerBeanMapper(); ppersona = mapper.map(persona,
                     * PPersona.class); */
                    ppersona.incorporarDatosPersona(persona);
                    ppersonas.add(ppersona);
                }
            }
            return ppersonas;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ExcepcionSNC("Buscar Persona por número de identificación", ESeveridadExcepcionSNC.ERROR,
                "Error en el proceso de búsqueda de Personas", e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPPrediosPanelJPropiedadPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo de excepciones
     */
    @Override
    public List<PPredio> buscarPPrediosPanelJPropiedadPorTramiteId(Long tramiteId) {
        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPrediosPanelJPropiedadPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPrediosPanelJPropiedadPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPPrediosPanelPropietariosPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo de excepciones
     */
    @Override
    public List<PPredio> buscarPPrediosPanelPropietariosPorTramiteId(Long tramiteId) {

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPrediosPanelPropietariosPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPrediosPanelPropietariosPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#eliminarPPersonaPredios(List)
     */
    @Override
    public void eliminarPPersonaPredios(List<PPersonaPredio> pPersonaPredios) {
        this.pPersonaPredioDao.deleteMultiple(pPersonaPredios);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#guardarActualizarPPredio(PPredio)
     */
    @Override
    public PPredio guardarActualizarPPredio(PPredio pPredio) {
        try {
            if (pPredio.getPPersonaPredios() != null) {
                for (PPersonaPredio ppp : pPredio.getPPersonaPredios()) {
                    ppp.getPPersona().setPrimerNombre(ppp.getPPersona().getPrimerNombre() != null ?
                        ppp.getPPersona().getPrimerNombre() : null);
                    ppp.getPPersona().setSegundoNombre(
                        ppp.getPPersona().getSegundoNombre() != null ? ppp.getPPersona().
                        getSegundoNombre() : null);
                    ppp.getPPersona().setPrimerApellido(ppp.getPPersona().getPrimerApellido() !=
                        null ? ppp.getPPersona().getPrimerApellido() : null);
                    ppp.getPPersona().setSegundoApellido(ppp.getPPersona().getSegundoApellido() !=
                        null ? ppp.getPPersona().getSegundoApellido() : null);
                    ppp.getPPersona().setRazonSocial(ppp.getPPersona().getRazonSocial() != null ?
                        ppp.getPPersona().getRazonSocial() : null);
                    ppp.getPPersona().setSigla(ppp.getPPersona().getSigla() != null ? ppp.
                        getPPersona().getSigla() : null);
                    //@modified by leidy.gonzalez::#12893:: Se quita el set a PPersona ya que redunda y duplica informacion
                    this.pPersonaDao.update(ppp.getPPersona());
                }
            }
            PPredio auxPPredio = this.pPredioDao.update(pPredio);
            pPredio.setId(auxPPredio.getId());

            auxPPredio = this.pPredioDao.findPPredioCompletoById(auxPPredio
                .getId());
            if (auxPPredio != null) {
                pPredio = auxPPredio;
            }

        } catch (Exception e) {
            LOGGER.error("error en ConservacionBean#guardarActualizarPPredio: " +
                e.getMessage());
        }
        return pPredio;
    }

    /**
     * @see IConservacion#eliminarPPersonaPredio(PPersonaPredio)
     */
    @Override
    public void eliminarPPersonaPredio(PPersonaPredio pPersonaPredio) {
        this.pPersonaPredioDao.delete(pPersonaPredio);
    }

    /**
     * @see IConservacion#reemplazarPropietarios(PPredio, List)
     */
    @Override
    public PPredio reemplazarPropietarios(PPredio pPredio,
        List<PPersonaPredio> pPersonaPredios) {
        return this.pPersonaPredioDao.replacePropietarios(pPredio, pPersonaPredios);
    }

    //TODO :: el que lo hizo :: 21-10-11 :: poner autor y documentar :: juan.agudelo
    @Override
    public PPersonaPredio buscarPPersonaPredioPorId(Long id) {
        return this.pPersonaPredioDao.findById(id);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPPrediosPanelUbicacionPredioPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 16-08-2012 Orden. Manejo de excepciones
     */
    @Implement
    @Override
    public List<PPredio> buscarPPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId) {

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPrediosPanelUbicacionPredioPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarPPrediosPanelUbicacionPredioPorTramiteId" +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que retorna los predios cuyo numero predial inicia por un codigo
     *
     * @author franz.gamba
     * @modified juan.agudelo modificación por Dead store in answer
     */
    @Override
    public List<Predio> getPrediosPorCodigo(String codigo) {
        return this.predioDao.getPrediosPorCodigo(codigo);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPersonasById(Long)
     * @author juan.agudelo
     */
    @Override
    public Persona buscarPersonasById(Long personaId) {
        try {
            return this.personaDao.findById(personaId);
        } catch (Exception ex) {
            LOGGER.error("Error en ConservacionBean#buscarPersonasById: " + ex.getMessage());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Metodo que retorna predios segun su municipio y matricula inmobiliaria
     *
     * @author franz.gamba
     * @modified juan.agudelo modificación por Dead store in answer
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPorMunicipioYMatricula(
        String municipioCod, String matriculaInm) {
        return this.predioDao.getPrediosPorMunicipioYMatricula(
            municipioCod, matriculaInm);
    }

    /**
     * @see IConservacion#proyectarJustificacionDerechoPropiedad(UsuarioDTO, List)
     */
    @Override
    public void proyectarJustificacionDerechoPropiedad(UsuarioDTO usuario,
        List<PPersonaPredioPropiedad> personaPredioPropiedadInscritos) {
        try {
            for (PPersonaPredioPropiedad ppp : personaPredioPropiedadInscritos) {
                this.guardarProyeccion(usuario, ppp);
                //this.pPersonaPredioService.update(ppp.getPPersonaPredio());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC(
                "algúnCódigo",
                ESeveridadExcepcionSNC.ERROR,
                "Error al almacenar la proyección de la justificación del derecho de propiedad",
                e);
        }
    }

    //---------------------------------------------------------------------------------------------
    /**
     *
     * @modified juan.agudelo modificación por Dead store in answer
     */
    // TODO franz.gamba :: 28-11-11 :: Poner el nombre del autor y Documentar
    // método :: juan.agudelo
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPorMunicipioYDireccion(
        String municipioCod, String direccion) {
        return this.predioDao.getPrediosPorMunicipioYDireccion(
            municipioCod, direccion);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#eliminarPPredioDireccion(PPredioDireccion)
     */
    @Override
    public void eliminarPPredioDireccion(PPredioDireccion pPredioDireccion) {
        this.pPredioDireccionDao.delete(pPredioDireccion);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarFotosPredioPorComponenteDeConstruccion(Long, String)
     * @author juan.agudelo
     */
    @Override
    public List<String> buscarFotosPredioPorComponenteDeConstruccion(
        Long predioId, String componenteConstruccion) {

        return this.fotoDao.buscarFotosPredioPorComponenteDeConstruccion(
            predioId, componenteConstruccion);
    }

    /**
     * @see IConservacion#guardarFotos(List)
     * @author juan.agudelo
     */
    @Override
    public List<Foto> guardarFotos(List<Foto> fotos) {
        return this.fotoDao.guardarFotos(fotos);
    }

    /**
     * @see IConservacion#guardarActualizarPFichaMatriz(PFichaMatriz)
     * @author fabio.navarrete
     */
    @Override
    public PFichaMatriz guardarActualizarPFichaMatriz(PFichaMatriz fichaMatriz) {
        PFichaMatriz pFichaMatrizAux = this.pFichaMatrizDao.update(fichaMatriz);
        fichaMatriz.setId(pFichaMatrizAux.getId());
        if (pFichaMatrizAux.getId() != null) {
            fichaMatriz.setPFichaMatrizTorres(this.pFichaMatrizTorreDao
                .findByFichaMatrizId(fichaMatriz.getId()));
        }
        return fichaMatriz;
    }

    /**
     * @see IConservacion#buscarPFichaMatrizPorPredioId(Long)
     * @author fabio.navarrete
     */
    @Override
    public PFichaMatriz buscarPFichaMatrizPorPredioId(Long idPPredio) {
        return this.pFichaMatrizDao.findByPredioId(idPPredio);
    }

    /**
     *
     */
    @Override
    public PFichaMatrizTorre guardarActualizarPFichaMatrizTorre(
        PFichaMatrizTorre pFichaMatrizTorre) {
        PFichaMatrizTorre pfmt = this.pFichaMatrizTorreDao.update(pFichaMatrizTorre);
        pFichaMatrizTorre.setId(pfmt.getId());
        return pFichaMatrizTorre;
    }

    @Override
    public List<PFichaMatrizTorre> guardarActualizarPFichaMatrizTorres(
        List<PFichaMatrizTorre> pFichaMatrizTorres) {
        return this.pFichaMatrizTorreDao.updateMultipleReturn(pFichaMatrizTorres);
    }

    /**
     * @see IConservacion#guardarPPredioDireccion(PPredioDireccion)
     * @author david.cifuentes
     */
    @Override
    public PPredioDireccion guardarPPredioDireccion(PPredioDireccion direccion) {
        return this.pPredioDireccionDao.update(direccion);
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarZonasHomogeneasPorPPredioId(Long)
     * @author david.cifuentes
     * @modified juan.agudelo modificación por Dead store in answer
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<PPredioZona> buscarZonasHomogeneasPorPPredioId(Long idPPredio) {
        return this.pPredioZonaDao.buscarZonasHomogeneasPorPPredioId(idPPredio);
    }

    /**
     * @see IConservacion#buscarZonasHomogeneasPorPPredioIdYEstadoP(Long, String)
     * @author felipe.cadena
     */
    @Override
    public List<PPredioZona> buscarZonasHomogeneasPorPPredioIdYEstadoP(Long idPPredio, String estado) {

        List<PPredioZona> zonas = new ArrayList<PPredioZona>();
        try {
            zonas = this.pPredioZonaDao.buscarZonasPorPPredioIdYEstadoP(idPPredio, estado);
        } catch (Exception e) {
            LOGGER.error("Error al consultar zonas");
        }
        return zonas;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#buscarPrediosPorNumerosPrediales
     * @author franz.gamba
     *
     * @modified juan.agudelo modificación por Dead store in answer
     */
    @Override
    //TODO :: usar try y catch de excepciones SNC (para este y los demás métodos) :: pedro.garcia
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPorNumerosPrediales(String numPrediales) {
        return this.predioDao.getPrediosPorNumerosPrediales(numPrediales);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @seeIConservacion#buscarPrediosPorNumerosPredialesVisor
     * @author franz.gamba
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ListDetallesVisorVO buscarPrediosPorNumerosPredialesVisor(String numPrediales) {
        return this.predioDao.getPrediosPorNumerosPredialesVisor(numPrediales);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#eliminarPFichaMatrizTorre(PFichaMatrizTorre)
     * @author fabio.navarrete
     */
    @Override
    public void eliminarPFichaMatrizTorre(
        PFichaMatrizTorre pFichaMatrizTorre) {
        this.pFichaMatrizTorreDao.delete(pFichaMatrizTorre);

    }

    //---------------------------------------------------------------------------------------------
    /**
     * @modified juan.agudelo modificación por Dead store in answer
     * @author franz.gamba
     */
    @Override
    public List<EstadisticasRegistroVO> obtenerEstadisticasConcentracionPropiedad(
        String departamentoCodigo, String municipioCodigo,
        String zonaUnidadOrganica) {
        return this.vPropiedadTierraDao.getEstadisticas(departamentoCodigo,
            municipioCodigo, zonaUnidadOrganica);
    }

    /**
     * @see IConservacion#buscarHPersonaPredioPropiedadPorPredioId(Long)
     * @author fabio.navarrete
     */
    @Override
    public List<HPersonaPredioPropiedad> buscarHPersonaPredioPropiedadPorPredioId(Long predioId) {
        return this.hPersonaPredioPropiedadDao.findByPredioId(predioId);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#actualizarFichaMatriz(PFichaMatriz)
     */
    @Override
    public PFichaMatriz actualizarFichaMatriz(PFichaMatriz fichaMatriz) {
        try {
            PFichaMatriz answer = pFichaMatrizDao.update(fichaMatriz);
            return answer;
        } catch (RuntimeException rtex) {
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#guardarActualizarPPredioDirecciones(List<PPredioDireccion>)
     */
    @Override
    public List<PPredioDireccion> actualizarPPredioDirecciones(
        List<PPredioDireccion> pPredioDirecciones) {

        List<PPredioDireccion> resultado = null;
        try {
            resultado = this.pPredioDireccionDao.updateMultipleReturn(pPredioDirecciones);

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#buscarUltimoNumeroPredialDisponible(String)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String buscarUltimoNumeroPredialDisponible(String numeroPredialManzana) {

        String numeroRadicadoResultado = null;
        try {
            numeroRadicadoResultado = sncProcedimientoDao.generarNumeroPredio(numeroPredialManzana);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return numeroRadicadoResultado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#actualizarPFichaMatrizPredios(PFichaMatrizPredio[])
     */
    public void actualizarPFichaMatrizPredios(PFichaMatrizPredio[] prediosFichaMatriz) {
        int i;
        for (i = 0; i < prediosFichaMatriz.length; i++) {
            this.pFichaMatrizPredioDao.update(prediosFichaMatriz[i]);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#guardarPrediosFichaMatriz(List<PFichaMatrizPredio)
     */
    @Override
    public List<PFichaMatrizPredio> guardarPrediosFichaMatriz(
        List<PFichaMatrizPredio> prediosFichaMatriz) {
        List<PFichaMatrizPredio> answer = null;
        answer = this.pFichaMatrizPredioDao.updateMultipleReturn(prediosFichaMatriz);
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#guardarPPredios(List<PFichaMatrizPredio)
     */
    @Override
    public List<PPredio> guardarPPredios(List<PPredio> pPredios) {

        List<PPredio> answer = null;
        try {
            answer = this.pPredioDao.updateMultipleReturn(pPredios);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#guardarPPredios: " + ex.getMensaje());
        }
        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#eliminarPrediosFichaMatriz(List<PFichaMatrizPredio)
     */
    @Override
    public void eliminarPrediosFichaMatriz(List<PFichaMatrizPredio> prediosFichaMatriz) {

        for (PFichaMatrizPredio pfmp : prediosFichaMatriz) {
            List<PFichaMatrizPredio> pFichaMatrizPredios = this.pFichaMatrizPredioDao
                .findByPFichaMatrizId(pfmp.getPFichaMatriz().getId());
            pfmp.getPFichaMatriz().setPFichaMatrizPredios(pFichaMatrizPredios);
            int idx = -1;

            for (int i = 0; i < pfmp.getPFichaMatriz().getPFichaMatrizPredios()
                .size(); i++) {
                if (pfmp.getPFichaMatriz().getPFichaMatrizPredios().get(i)
                    .getId().equals(pfmp.getId())) {
                    idx = i;
                    break;
                }
            }

            if (idx != -1) {
                pfmp.getPFichaMatriz().getPFichaMatrizPredios().remove(idx);
            }
        }
        this.pFichaMatrizPredioDao.deleteMultiple(prediosFichaMatriz);
    }

//end of class
    /**
     * @see IConservacion#guardarActualizarPPersonaPredios(List)
     * @author fabio.navarrete
     */
    @Override
    public List<PPersonaPredio> guardarActualizarPPersonaPredios(
        List<PPersonaPredio> pPersonaPredios) {
        for (PPersonaPredio pPersonaPredio : pPersonaPredios) {
            PPersonaPredio ppp = this.pPersonaPredioDao
                .update(pPersonaPredio);
            pPersonaPredio.setId(ppp.getId());
        }
        return pPersonaPredios;
    }

    /**
     * @see IConservacion#buscarPPersonaPrediosPorPredioId(Long)
     * @author fabio.navarrete
     */
    @Override
    public List<PPersonaPredio> buscarPPersonaPrediosPorPredioId(Long idPPredio) {
        return this.pPersonaPredioDao.findByPPredioId(idPPredio);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#guardarJustificacionesPropiedad(JustificacionPropiedadDTO
     * JustificacionPropiedadDTO, PPersonaPredio[] propietariosNuevosSeleccionados, UsuarioDTO
     * usuario)
     * @author fredy.wilches
     */
    @Override
    public JustificacionPropiedadDTO guardarJustificacionesPropiedad(
        JustificacionPropiedadDTO justificacionSeleccionada,
        PPersonaPredio[] propietariosNuevosSeleccionados,
        UsuarioDTO usuario) {
        /*
         * Se recorren los seleccionados por el usuario en la UI
         */
        for (PPersonaPredio pp : propietariosNuevosSeleccionados) {
            boolean existia = false;
            for (PPersonaPredioPropiedad ppp : justificacionSeleccionada.getJustificaciones()) {
                /*
                 * Si ya existia se actualiza
                 */
                if (pp.getId().equals(ppp.getPPersonaPredio().getId())) {
                    pp = this.guardarActualizarPPersonaPredio(pp);
                    ppp.setPPersonaPredio(pp);
                    ppp.setTipoTitulo(justificacionSeleccionada.getTipoTitulo());
                    ppp.setEntidadEmisora(justificacionSeleccionada.getEntidadEmisora());
                    ppp.setDepartamento(justificacionSeleccionada.getDepartamento());
                    ppp.setMunicipio(justificacionSeleccionada.getMunicipio());
                    ppp.setNumeroTitulo(justificacionSeleccionada.getNumeroTitulo());
                    ppp.setFechaTitulo(justificacionSeleccionada.getFechaTitulo());
                    ppp.setFechaRegistro(justificacionSeleccionada.getFechaRegistro());
                    ppp.setDocumentoSoporte(justificacionSeleccionada.getDocumentoSoporte());
                    ppp.setModoAdquisicion(justificacionSeleccionada.getModoAdquisicion());
                    ppp.setValor(justificacionSeleccionada.getValor());
                    ppp.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
                    ppp.setCancelaInscribe(justificacionSeleccionada.getCancelaInscribe());
                    ppp.setUsuarioLog(justificacionSeleccionada.getUsuarioLog());
                    this.guardarActualizarPPersonaPredioPropiedad(ppp);
                    existia = true;
                    break;
                }
            }
            /*
             * Si no existia se adiciona
             */
            if (!existia) {
                PPersonaPredioPropiedad ppp = justificacionSeleccionada.
                    crearPPersonaPredioPropiedad(pp, usuario);
                ppp = this.guardarActualizarPPersonaPredioPropiedad(ppp);
                justificacionSeleccionada.adicionarJustificacion(ppp);
            }
        }
        /*
         * Se recorren los que ya existian, y sino existe entre los seleccionados por el usuario en
         * la UI, se eliminan
         */
        List<PPersonaPredioPropiedad> eliminadas = new ArrayList<PPersonaPredioPropiedad>();
        if (propietariosNuevosSeleccionados.length > 0) {
            PPredio predio = propietariosNuevosSeleccionados[0].getPPredio();

            for (PPersonaPredioPropiedad ppp : justificacionSeleccionada.getJustificaciones()) {
                if (ppp.getPPersonaPredio().getPPredio().getId().equals(predio.getId())) {
                    boolean existe = false;
                    for (PPersonaPredio pp : propietariosNuevosSeleccionados) {
                        if (ppp.getPPersonaPredio().getId().equals(pp.getId())) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {
                        eliminadas.add(ppp);
                        this.eliminarPPersonaPredioPropiedad(ppp);
                    }
                }
            }
            for (PPersonaPredioPropiedad ppp : eliminadas) {
                justificacionSeleccionada.getJustificaciones().remove(ppp);
            }

        }
        return justificacionSeleccionada;
    }

    /* public List<PPersonaPredio> guardarJustificacionesPropiedad( List<PPersonaPredio>
     * pPersonaPredios, PPersonaPredioPropiedad pPersonaPredioPropiedad, UsuarioDTO usuario) { for
     * (PPersonaPredio pPersonaPredio : pPersonaPredios) { PPersonaPredioPropiedad pppp = new
     * PPersonaPredioPropiedad( pPersonaPredioPropiedad); pppp.setUsuarioLog(usuario.getLogin());
     * pppp.setFechaLog(new Date()); pppp.setPPersonaPredio(pPersonaPredio);
     *
     * pPersonaPredio.setUsuarioLog(usuario.getLogin()); pPersonaPredio.setFechaLog(new Date());
     * pPersonaPredio = this .guardarActualizarPPersonaPredio(pPersonaPredio);
     *
     * pppp = this.guardarActualizarPPersonaPredioPropiedad(pppp); if
     * (!pPersonaPredio.getPPersonaPredioPropiedads().contains(pppp)) {
     * pPersonaPredio.getPPersonaPredioPropiedads().add(pppp); }
     *
     * }
     * return pPersonaPredios; } */
    /**
     * @see IConservacion#guardarActualizarPPersonaPredioPropiedad(PPersonaPredioPropiedad)
     * @author fabio.navarrete
     */
    @Override
    public PPersonaPredioPropiedad guardarActualizarPPersonaPredioPropiedad(
        PPersonaPredioPropiedad pPersonaPredioPropiedad) {
        PPersonaPredioPropiedad pPersonaPredioPropiedadAux = this.pPersonaPredioPropiedadDao
            .update(pPersonaPredioPropiedad);
        pPersonaPredioPropiedad.setId(pPersonaPredioPropiedadAux.getId());
        return pPersonaPredioPropiedad;
    }

    /**
     * @see IConservacion#guardarActualizarPPersonaPredio(PPersonaPredio)
     * @author fabio.navarrete
     */
    @Override
    public PPersonaPredio guardarActualizarPPersonaPredio(
        PPersonaPredio pPersonaPredio) {
        pPersonaPredio.setPPersona(pPersonaDao.findById(pPersonaPredio
            .getPPersona().getId()));
        PPersonaPredio pPersonaPredioAux = this.pPersonaPredioDao
            .update(pPersonaPredio);
        pPersonaPredio.setId(pPersonaPredioAux.getId());
        pPersonaPredio
            .setPPersonaPredioPropiedads(this.pPersonaPredioPropiedadDao
                .findByPPersonaPredioId(pPersonaPredio.getId()));
        return pPersonaPredio;
    }

    //TODO :: el que lo hizo :: 21-10-11 :: poner autor y documentar :: juan.agudelo
//TODO :: juan.agudelo :: 24-02-2012 :: un todo dirigido a 'el que lo hizo'????. :: pedro.garcia
    @Override
    public void eliminarPPersonaPredioPropiedad(
        PPersonaPredioPropiedad pPersonaPredioPropiedad) {
        this.pPersonaPredioPropiedadDao.delete(pPersonaPredioPropiedad);
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see co.gov.igac.snc.fachadas.IConservacion#enviarACoordinador(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public void enviarACoordinador(Actividad actividad, UsuarioDTO usuario) {
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        solicitudCatastral.setUsuarios(this.tramiteService
            .buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionEstructuraOrganizacional(),
                ERol.COORDINADOR));
        solicitudCatastral.setTransicion(ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);
        procesosService.avanzarActividad(usuario, actividad.getId(), solicitudCatastral);
    }
//--------------------------------------------------------------------------------------------------		

    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.fachadas.IConservacion#enviarAGenerarResolucion(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public void enviarAGenerarResolucion(Actividad actividad, UsuarioDTO usuario) {
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(usuario);
        solicitudCatastral.setUsuarios(usuarios);
        solicitudCatastral.setTransicion(ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION);
        procesosService.avanzarActividad(actividad.getId(), solicitudCatastral);
    }

    /**
     *
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.fachadas.IConservacionLocal#enviarAModificacionInformacionGeografica(co.gov.igac.snc.apiprocesos.tareas.comun.Actividad,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public void enviarAModificacionInformacionGeografica(Actividad actividad,
        UsuarioDTO usuario) {
        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(actividad.
            getIdObjetoNegocio());
        if (tramite.isQuinta()) {
            List<PPredio> prediosResultantes = this.conservacionService.
                buscarPprediosResultantesPorTramiteId(tramite.getId());
            if (prediosResultantes.size() > 0) {
                integracionService.generarReplicaParaTramite(tramite, actividad, usuario,
                    prediosResultantes.get(0));
            }
        } else {
            integracionService.generarReplicaParaTramite(tramite, actividad, usuario);
        }
    }

    /**
     *
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.fachadas.IConservacionLocal#enviarAModificacionInformacionGeografica(co.gov.igac.snc.apiprocesos.tareas.comun.Actividad,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    /**
     * @modified juan.mendez 2014/04/03 Se modificó la firma del método de SigDao. Ya no requiere el
     * parámetro token.
     */
    @Override
    public void enviarAModificacionInformacionGeograficaSincronica(Actividad actividad,
        UsuarioDTO usuario) {

        //Se carga el tramite con los datos necesarios
        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(actividad.
            getIdObjetoNegocio());
        //felipe.cadena::ACCU::Se cambio la manera de genera la replica de edicion
        //Valida si el tramite ya tiene replica
        Long replicaOriginalId = ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId();
        List<TramiteDocumento> td = tramite.getTramiteDocumentos();
        boolean noTieneReplicaFlag = true;
        for (TramiteDocumento tdTemp : td) {
            if (tdTemp.getDocumento().getTipoDocumento().getId().equals(replicaOriginalId)) {
                noTieneReplicaFlag = false;
            }
        }

        //Si el tramite no tiene replica se inicia el trabajo 
        if (noTieneReplicaFlag) {
            this.generarReplicaEdicion(tramite, usuario, 0);

        } else {
            //Se avanza la actividad con el usuario correspondiente
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(usuario);
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setUsuarios(usuarios);

            if (actividad.getNombre().equals(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR);

            } else if (actividad.getNombre().equals(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION);

            } else if (actividad.getNombre().equals(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_TOPOGRAFIA)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA);

            } else if (actividad.getNombre().equals(
                ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA);

            } else {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA);

            }
            procesosService.avanzarActividad(actividad.getId(), solicitudCatastral);
        }

    }

    /**
     * @see IConservacion#envairARevisionProyeccionDeResoucion(String, UsuarioDTO)
     */
    @Override
    public void enviarARevisionProyeccionDeResolucion(Actividad actividad, UsuarioDTO usuario) {

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();

        solicitudCatastral.setUsuarios(this.tramiteService
            .buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION));
        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION);

        procesosService.avanzarActividad(actividad.getId(), solicitudCatastral);

    }

    /**
     * @see IConservacion#obtenerPPrediosCompletosPorIds(List)
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> obtenerPPrediosCompletosPorIds(List<Long> predioIds) {
        return this.pPredioDao.findByIds(predioIds);
    }

    /**
     * @see IConservacion#obtenerPPrediosCompletosPorIdsPHCondominio(List)
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> obtenerPPrediosCompletosPorIdsPHCondominio(List<Long> predioIds) {
        return this.pPredioDao.findByIdsPHCondominio(predioIds);
    }

    /**
     * @see IConservacion#getAreasCatastralesPorNumeroPredial(String numeroPredial)
     * @author javier.aponte
     */
    @Override
    public double[] getAreasCatastralesPorNumeroPredial(String numeroPredial) {
        return this.predioDao.getAreasCatastralesPorNumeroPredial(numeroPredial);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#eliminarReferenciaCartografica(List<PReferenciaCartografica)
     */
    @Override
    public void eliminarReferenciaCartografica(PReferenciaCartografica referenciaCartografica) {
        this.pReferenciaCartograficaDao.delete(referenciaCartografica);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#guardarPReferenciaCartografica(PReferenciaCartografica)
     */
    @Override
    public PReferenciaCartografica guardarPReferenciaCartografica(
        PReferenciaCartografica referenciaCartografica) {
        PReferenciaCartografica answer = this.pReferenciaCartograficaDao.update(
            referenciaCartografica);
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IConservacion#buscarPersonaPorNumeroYTipoIdentificacion(String, String)
     * @author juan.agudelo
     */
    @Override
    public List<Persona> buscarPersonaPorNumeroYTipoIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion) {
        return this.personaDao.buscarPersonaPorNumeroYTipoIdentificacion(
            tipoIdentificacion, numeroIdentificacion);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#bloquearMasivamentePersonas(UsuarioDTO, ArrayList)
     */
    @Override
    public ResultadoBloqueoMasivo bloquearMasivamentePersonas(
        UsuarioDTO usuario, ArrayList<PersonaBloqueo> bloqueoMasivo) {

        try {
            return this.personasBloqueoDao
                .bloquearMasivamentePersonas(usuario, bloqueoMasivo);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("Error en ConservacionBean#bloquearMasivamentePersonas: " + ex.getMensaje());
        }

        return null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IConservacion#bloquearMasivamentePredios(UsuarioDTO, ArrayList)
     */
    @Override
    public ResultadoBloqueoMasivo bloquearMasivamentePredios(
        UsuarioDTO usuario, ArrayList<PredioBloqueo> bloqueoMasivo) {

        try {
            return this.prediosBloqueoDao.bloquearMasivamentePredios(usuario, bloqueoMasivo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#bloquearMasivamentePredios: " + ex.getMensaje());
        }

        return null;

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IConservacion#buscarPPrediosCompletosPorTramiteId(Long)
     * @author fredy.wilches
     */
    @Override
    public List<PPredio> buscarPPrediosCompletosPorTramiteId(Long idTramite) {
        return this.pPredioDao.findPPrediosCompletosByTramiteId(idTramite);
    }

    /**
     * @see IConservacion#buscarPPrediosCompletosPorTramiteId(Long)
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> buscarPPrediosCompletosPorTramiteIdPHCondominio(
        Long idTramite) {
        return this.pPredioDao
            .findPPrediosCompletosByTramiteIdPHCondominio(idTramite);
    }

    @Override
    public void eliminarProyecciones(UsuarioDTO usuarioDto,
        List<IProyeccionObject> proyeccionObjects) {
        for (IProyeccionObject proyObj : proyeccionObjects) {
            this.proyectarConservacionDao.deleteProyeccion(usuarioDto, proyObj);
        }
    }

    /**
     * @see IConservacion#guardarPPersonaPredio(UsuarioDTO, PPersonaPredio)
     * @author fabio.navarrete
     */
    @Override
    public PPersonaPredio guardarPPersonaPredio(UsuarioDTO usuario,
        PPersonaPredio pPersonaPredio) {

        pPersonaPredio.getPPersona().setFechaLog(new Date());
        pPersonaPredio.getPPersona().setUsuarioLog(usuario.getLogin());
        pPersonaPredio.setFechaLog(new Date());
        pPersonaPredio.setUsuarioLog(usuario.getLogin());
        pPersonaPredio.setPPersona(this.pPersonaDao
            .update(pPersonaPredio.getPPersona()));
        /* if (pPersonaPredio.getId() != null && pPersonaPredio.getPPersona() != null &&
         * !pPersonaPredio.getPPersona().getPPersonaPredios() .contains(pPersonaPredio)) {
         * pPersonaPredio.getPPersona().getPPersonaPredios() .add(pPersonaPredio); } */

//        pPersonaPredio
//				.setId(((PPersonaPredio) this.proyectarConservacionDao
//						.saveProyeccion(usuario, pPersonaPredio))
//						.getId());
        return this.pPersonaPredioDao.update(pPersonaPredio);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see
     * IConservacion#buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(FiltroDatosConsultaPrediosBloqueo)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo) {
        return this.predioDao
            .buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(filtroDatosConsultaPredioBloqueo);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarPersonaPorNombrePartesTipoDocNumDoc(PersonaBloqueo)
     * @author juan.agudelo
     */
    @Override
    public Persona buscarPersonaPorNombrePartesTipoDocNumDoc(
        Persona persona) {
        return this.personaDao
            .buscarPersonaPorNombrePartesTipoDocNumDoc(persona);
    }

    /**
     * @see IConservacion#buscarPisosEdificio(String)
     * @author fabio.navarrete
     */
    @Override
    public List<String> buscarPisosEdificio(String numeroPredial) {
        return this.predioDao.findPisosEdificio(numeroPredial);
    }

    /**
     * @see IConservacion#buscarTerrenosManzana(String)
     * @author fabio.navarrete
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<String> buscarTerrenosManzana(String numeroPredial) {
        return this.predioDao.findTerrenosManzana(numeroPredial);
    }

    /**
     * @see IConservacion#buscarEdificiosTerreno(String)
     * @author fabio.navarrete
     */
    @Override
    public List<String> buscarEdificiosTerreno(String numeroPredial) {
        return this.predioDao.findEdificiosTerreno(numeroPredial);
    }

    /**
     * @see IConservacion#guardarPPredioYFichaMatriz(PPredio, PFichaMatriz)
     * @author fabio.navarrete
     * @modified david.cifuentes
     */
    @Override
    public PFichaMatriz guardarPPredioYFichaMatriz(PPredio pPredio,
        UsuarioDTO usuario) {
        if (pPredio.getFechaInscripcionCatastral() != null) {
            pPredio.setFechaInscripcionCatastral(new Date(System.currentTimeMillis()));
        }
        PPredio pPredioAuxiliar = this.pPredioDao.update(pPredio);
        if (pPredioAuxiliar != null) {
            pPredio.setId(pPredioAuxiliar.getId());
            pPredio.setPPredioDireccions(pPredioAuxiliar.getPPredioDireccions());
        }
        PFichaMatriz fichaMatriz = ConservacionUtil.createBlankPFichaMatriz(
            pPredio, usuario);
        fichaMatriz.setId(this.pFichaMatrizDao.update(fichaMatriz).getId());
        return fichaMatriz;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarPersonaPorNombrePartesTipoDocNumDocTemp(PersonaBloqueo)
     * @author juan.agudelo
     */
    @Override
    public List<Persona> buscarPersonaPorNombrePartesTipoDocNumDocTemp(Persona persona) {
        return this.personaDao
            .buscarPersonaPorNombrePartesTipoDocNumDocTemp(persona);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
     * FiltroDatosConsultaPrediosBloqueo, FiltroDatosConsultaPrediosBloqueo)
     * @author juan.agudelo
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB,
        FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB) {
        return this.predioDao
            .buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
                filtroCPredioB, filtroFinalCPredioB);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#contarPrediosNacionales()
     * @author david.cifuentes
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long contarPrediosNacionales() {
        return this.predioDao.contarPrediosNacionales();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @see IConservacionLocal#obtenerPredioConDatosUbicacionPorId(Long)
     */
    @Implement
    @Override
    public Predio obtenerPredioConDatosUbicacionPorId(Long predioId) {

        LOGGER.debug("on ConservacionBean#obtenerPredioConDatosUbicacionPorId ...");

        Predio answer = null;
        try {
            answer = this.consultaPredioDao.getPredioFetchDatosUbicacionById(predioId);
        } catch (Exception ex) {
            LOGGER.error(" ... excepción: " + ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, ex,
                "ConservacionBean#obtenerPredioConDatosUbicacionPorId");
        }
        return answer;
    }

    /**
     * @author fredy.wilches
     * @see IConservacionLocal#buscarPPersonaPredioPropiedadPorPPersonaPredio (PPersonaPredio pp)
     */
    @Override
    public List<PPersonaPredioPropiedad> buscarPPersonaPredioPropiedadPorPPersonaPredio(
        PPersonaPredio pp) {
        List<PPersonaPredioPropiedad> answer = null;
        try {
            answer = this.pPersonaPredioPropiedadDao.buscarTitulosPorPPersonaPredio(pp);
        } catch (Exception ex) {
            LOGGER.error("Error: " + ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, ex);
        }
        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#buscarPredios(co.gov.igac.snc.util.FiltroDatosConsultaPredio, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Object[]> buscarPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, int... idPrimeraFilaYCuantos) {

        List<Object[]> answer = null;

        try {
            answer = this.sncProcedimientoDao.buscarPrediosPorCriterios(
                datosConsultaPredio, false, idPrimeraFilaYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en buscarPredios: " + ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#contarPredios(co.gov.igac.snc.util.FiltroDatosConsultaPredio, int[])
     * @author pedro.garcia
     *
     */
    /*
     * Para los niveles de transaccionalidad ver Transaction Annotations:
     * http://tomee.apache.org/transaction-annotations.html Performance:
     * http://stackoverflow.com/questions/10796172/what-do-i-get-from-setting-this-transactionattributetype-not-supported
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public int contarPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, int... idPrimeraFilaYCuantos) {

        int answer = 0;
        List<Object[]> spAnswer;
        BigDecimal tempCount;

        try {
            spAnswer = this.sncProcedimientoDao.buscarPrediosPorCriterios(
                datosConsultaPredio, true, idPrimeraFilaYCuantos);
            if (spAnswer != null && !spAnswer.isEmpty()) {
                tempCount = (BigDecimal) spAnswer.get(0)[0];
                answer = tempCount.intValue();
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en contarPredios: " + ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#obtenerTipificacionesUnidadConstruccion()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<TipificacionUnidadConst> obtenerTipificacionesUnidadConstruccion() {
        List<TipificacionUnidadConst> answer = null;

        try {
            answer = this.tipificacionUnidadConstDao.findAll();
        } catch (Exception ex) {
            LOGGER.error("excepción consultando las tipificaciones de unidad de construcción: " +
                ex.getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacionLocal#cargarModeloDeConstruccion(Long)
     */
    @Override
    public PFichaMatrizModelo cargarModeloDeConstruccion(Long pFichaMatrizModeloId) {
        PFichaMatrizModelo answer = null;
        try {
            answer = this.pFichaMatrizModeloDao.cargarModeloDeConstruccion(pFichaMatrizModeloId);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e);
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacionLocal#guardarModeloDeConstruccion(PFichaMatrizModelo)
     */
    @Override
    public PFichaMatrizModelo guardarModeloDeConstruccion(
        PFichaMatrizModelo pFichaMatrizModelo) {
        PFichaMatrizModelo answer = null;
        try {
            answer = this.pFichaMatrizModeloDao.update(pFichaMatrizModelo);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(
                LOGGER, e);
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see
     * IConservacionLocal#insertarPFmConstruccionComponenteRegistros(List<PFmConstruccionComponente>
     * )
     */
    @Override
    public void insertarPFmConstruccionComponenteRegistros(
        List<PFmConstruccionComponente> newPFmConstruccionComponenteList) {
//TODO :: david.cifuentes :: hacer manejo de excepciones copmo se definió :: pedro.garcia   
        this.pFmConstruccionComponenteDao
            .persistMultiple(newPFmConstruccionComponenteList);
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see null null null null null null null null null     IConservacionLocal#actualizarPFmConstruccionComponenteRegistros(List<
	 *      PFmConstruccionComponente> )
     */
    @Override
    public void actualizarPFmConstruccionComponenteRegistros(
        List<PFmConstruccionComponente> newPFmConstruccionComponenteList) {
//TODO :: david.cifuentes :: hacer manejo de excepciones copmo se definió :: pedro.garcia           
        this.pFmConstruccionComponenteDao
            .actualizarPFmConstruccionComponenteRegistros(newPFmConstruccionComponenteList);
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacionLocal#borrarPFmConstruccionComponenteRegistros(Long)
     */
    @Override
    public void borrarPFmConstruccionComponenteRegistros(
        Long pFmModeloConstruccionId) {
//TODO :: david.cifuentes :: hacer manejo de excepciones copmo se definió :: pedro.garcia           
        this.pFmConstruccionComponenteDao
            .borrarPFmConstruccionComponenteRegistros(pFmModeloConstruccionId);
    }

    /**
     * @author david.cifuentes
     * @see IConservacionLocal#guardarPModeloConstruccionFotos(List<PModeloConstruccionFoto>)
     */
    @Override
    public void guardarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> listPFotosUnidadDelModelo) {
//TODO :: david.cifuentes :: hacer manejo de excepciones copmo se definió :: pedro.garcia           
        this.pModeloConstruccionFotoDao.guardarPModeloConstruccionFotos(listPFotosUnidadDelModelo);
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacionLocal#actualizarPModeloConstruccionFotos(List<PModeloConstruccionFoto>)
     */
    @Override
    public void actualizarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> pModeloConstruccionFotoList) {
//TODO :: david.cifuentes :: hacer manejo de excepciones copmo se definió :: pedro.garcia           
        this.pModeloConstruccionFotoDao.actualizarPModeloConstruccionFotos(
            pModeloConstruccionFotoList);
    }

//-------------------------------------------------------------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see IConservacion#guardarPModeloConstruccionFoto(PModeloConstruccionFoto
     * pModeloConstruccionFoto, UsuarioDTO usuario)
     */
    @Override
    public PModeloConstruccionFoto guardarPModeloConstruccionFoto(
        PModeloConstruccionFoto pModeloConstruccionFoto, UsuarioDTO usuario) {

        Documento documentoGuardado;
        String rutaLocal;

        if (null == pModeloConstruccionFoto.getFotoDocumento()
            .getRutaArchivoLocal() ||
            pModeloConstruccionFoto.getFotoDocumento()
                .getRutaArchivoLocal().isEmpty()) {
            LOGGER.debug("La ruta local de la PModeloConstruccionFoto es vacía o nula");
            return pModeloConstruccionFoto;
        }

        rutaLocal = pModeloConstruccionFoto.getFotoDocumento()
            .getRutaArchivoLocal();
        documentoGuardado = new Documento();

        try {
            documentoGuardado = this.documentoDao
                .almacenarDocumentoFotoEnGestorDocumental(
                    pModeloConstruccionFoto.getFotoDocumento(),
                    usuario, rutaLocal);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "No se pudo guardar en el gestor documental la foto correspondiente a la PFoto." +
                "En ConservacionBean#guardarPFoto. Excepción: " +
                ex.getMessage());
        }

        pModeloConstruccionFoto.setFotoDocumento(documentoGuardado);
        pModeloConstruccionFoto = this.pModeloConstruccionFotoDao
            .guardarPModeloConstruccionFoto(pModeloConstruccionFoto);

        return pModeloConstruccionFoto;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#guardarIdsBloqueoMasivo(List)
     */
    @Override
    public List<TmpBloqueoMasivo> guardarActualizarIdsBloqueoMasivo(
        List<TmpBloqueoMasivo> tmpBloqueoMasivo) {

        LOGGER.debug("en ConservacionBean#guardarActualizarIdsBloqueoMasivo");

        List<TmpBloqueoMasivo> answer = null;

        try {
            answer = this.bloqueoMasivoService.updateMultipleReturn(tmpBloqueoMasivo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#guardarActualizarIdsBloqueoMasivo: " +
                ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#borrarIdsBloqueoMasivo(List)
     */
    @Override
    public void borrarIdsBloqueoMasivo(
        List<TmpBloqueoMasivo> tmpBloqueoMasivo) {

        LOGGER.debug("en ConservacionBean#borrarIdsBloqueoMasivo");

        try {
            this.bloqueoMasivoService.deleteMultiple(tmpBloqueoMasivo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#borrarIdsBloqueoMasivo: " +
                ex.getMensaje());
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#buscarUnidadDelModeloDeConstruccionPorSuId(Long idPFmModeloConstruccion)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public PFmModeloConstruccion buscarUnidadDelModeloDeConstruccionPorSuId(
        Long idPFmModeloConstruccion) {
//TODO :: david.cifuentes :: hacer manejo de excepciones copmo se definió :: pedro.garcia           
        return this.pFmModeloConstruccionDao
            .buscarUnidadDelModeloDeConstruccionPorSuId(idPFmModeloConstruccion);
    }

    // -------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#buscarPrediosColindantes(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosColindantesPorNumeroPredial(
        String numeroPredial) {

        List<String> numerosPredialesColindantes = this.transaccionalService.
            findPrediosColindantesByNumeroPredialNuevaTransaccion(numeroPredial);
        if (numerosPredialesColindantes != null &&
            !numerosPredialesColindantes.isEmpty()) {
            return this.transaccionalService.
                getPrediosByNumerosPredialesNuevaTransaccion(numerosPredialesColindantes);
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#generarSecuenciaTmpBloqueoMasivo()
     */
    @Override
    public BigDecimal generarSecuenciaTmpBloqueoMasivo() {

        return this.tmpBloqueoService.generarSecuenciaTmpBloqueoMasivo();
    }

    // -------------------------------------------------------------------------
    /**
     * @see IConservacion#esPredioCanceladoPorPredioIdONumeroPredial(Long, String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public boolean esPredioCanceladoPorPredioIdONumeroPredial(Long predioId,
        String numeroPredial) {

        return this.predioDao.isPredioCanceladoByPredioIdOrNumeroPredial(
            predioId, numeroPredial);
    }

    /**
     * Método encargado de buscar el historico de justificaciones de un predio.
     *
     * @author fredy.wilches
     * @version 2.0
     * @param predio Predio asociado a las justificaciones a obtener.
     * @return Retorna la Lista de justificaciones historicas asociadas a un predio
     */
    @Override
    public List<PredioPropietarioHis> getPredioPropietarioHisByPredio(Long predioId) {
        return this.predioPropietarioHisDao.getPredioPropietarioHisByPredio(predioId);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IConservacion#buscarUnidadDeConstruccionPorSuId(Long idPUnidadConstruccion)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public PUnidadConstruccion buscarUnidadDeConstruccionPorSuId(
        Long idPUnidadConstruccion) {
        return this.pUnidadConstruccionDao
            .buscarUnidadDeConstruccionPorSuId(idPUnidadConstruccion);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerNumsTerrenoPorCondPropiedadEnManzana(java.lang.String,
     * java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<String> obtenerNumsTerrenoPorCondPropiedadEnManzana(
        String manzana, List<String> condicionesPropiedad) {

        LOGGER.debug("en ConservacionBean#obtenerNumsTerrenoPorCondPropiedadEnManzana");

        List<String> answer = null;

        if (manzana.length() < 17) {
            return answer;
        } else if (manzana.length() > 17) {
            manzana = manzana.substring(0, 17);
        }
        try {
            answer = this.predioDao.getTerrenoNumbersFromManzanaAndPropertyCondition(
                manzana, condicionesPropiedad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerNumsTerrenoPorCondPropiedadEnManzana: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerNumsEdificioTorrePorNumeroTerreno(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<String> obtenerNumsEdificioTorrePorNumeroTerreno(String terreno) {

        LOGGER.debug("en ConservacionBean#obtenerNumsEdificioTorrePorNumeroTerreno");

        List<String> answer = null;

        //D: se toman las 21 primeras posiciones del número
        if (terreno.length() < 21) {
            return answer;
        } else if (terreno.length() > 21) {
            terreno = terreno.substring(0, 21);
        }
        try {
            answer = this.predioDao.getEdificioTorreNumbersFromTerreno(terreno);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerNumsEdificioTorrePorNumeroTerreno: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerNumsPisoPorNumeroEdificioTorre(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<String> obtenerNumsPisoPorNumeroEdificioTorre(String edificioTorre) {

        LOGGER.debug("en ConservacionBean#obtenerNumsPisoPorNumeroEdificioTorre");

        List<String> answer = null;

        //D: se toman las 22 primeras posiciones del número
        if (edificioTorre.length() < 22) {
            return answer;
        } else if (edificioTorre.length() > 22) {
            edificioTorre = edificioTorre.substring(0, 22);
        }
        try {
            answer = this.predioDao.getPisoNumbersFromEdificioTorre(edificioTorre);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerNumsPisoPorNumeroEdificioTorre: " +
                ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#existePredio(java.lang.String, java.util.List<java.lang.String>[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean existePredio(String numeroPredial, String... listaEstados) {

        LOGGER.debug("en ConservacionBean#existePredio");
        boolean answer = false;

        try {
            answer = this.predioDao.existsPredio(numeroPredial, listaEstados);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#existePredio: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#buscarPrediosPorNumPredialYEstadosNF(java.lang.String,
     * java.lang.String[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosPorNumPredialYEstadosNF(
        String numeroPredial, String... listaEstados) {

        LOGGER.debug("en ConservacionBean#buscarPrediosPorNumPredialYEstadosNF");
        List<Predio> answer = null;

        try {
            answer = this.predioDao.lookForPrediosByNumPredialYEstadosNF(
                numeroPredial, listaEstados);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPrediosPorNumPredialYEstadosNF: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#generarNumeroUnidadPorTerreno(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String generarNumeroUnidadPorTerreno(String npHastaCondProp) {

        LOGGER.debug("en ConservacionBean#generarNumeroUnidadPorTerreno");

        String answer = null;

        if (npHastaCondProp.length() < 22) {
            return answer;
        }

        try {
            answer = this.sncProcedimientoDao.generarNumeroMejoraTE(npHastaCondProp);
            answer = answer.substring(26);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en ConservacionBean#generarNumeroUnidadPorTerreno: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#generarNumeroUnidadPorPiso(java.lang.String, boolean)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String generarNumeroUnidadPorPiso(String piso, boolean esMejora) {

        LOGGER.debug("en ConservacionBean#generarNumeroUnidadPorPiso");

        String answer = null;

        if (piso.length() < 26) {
            return answer;
        }

        try {
            if (esMejora) {
                answer = this.sncProcedimientoDao.generarNumeroMejoraPH(piso);
            } else {
                answer = this.sncProcedimientoDao.generarNumeroUnidad(piso);
            }
            answer = answer.substring(26);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en ConservacionBean#generarNumeroUnidadPorPiso: " +
                ex.getMensaje());
        }

        return answer;

    }

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.IConservacionLocal#buscarPprediosResultantesPorTramiteId(java.lang.Long)
     */
    @Override
    public List<PPredio> buscarPprediosResultantesPorTramiteId(Long tramiteId) {
        return this.pPredioDao.findPPrediosResultantesByTramiteId(tramiteId);
    }

    /**
     * @see IConservacionLocal#buscarPredioPorNumeroPredialParaAvaluoComercial(String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public Predio buscarPredioPorNumeroPredialParaAvaluoComercial(
        String numeroPredial) {
        LOGGER.debug("Iniciando ConservacionBean#buscarPredioPorNumeroPredialParaAvaluoComercial");

        Predio predio = null;
        try {
            predio = this.findPredioByNumeroPredial(numeroPredial);

            // Se valida que el predio exista
            if (predio != null) {
                predio.getPredioDireccions().size();
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Excepción en ConservacionBean#buscarPredioPorNumeroPredialParaAvaluoComercial: " +
                ex.getMensaje());
        }

        LOGGER.debug("Finalizando ConservacionBean#buscarPredioPorNumeroPredialParaAvaluoComercial");

        return predio;
    }

    /**
     * @see IConservacionLocal#getPredioAvaluoCatastralById
     *
     * @author fredy.wilches
     */
    @Override
    public PredioAvaluoCatastral getPredioAvaluoCatastralById(Long id) {
        return this.predioAvaluoCatastralDao.findById(id);
    }

    //-------------------------- //
    /**
     * @see IConservacionLocal#calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long)
     * @author david.cifuentes
     */
    @Override
    public Double calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long idTramite) {
        return this.pPredioDao.calcularSumaAvaluosTotalesDePrediosFichaMatriz(idTramite);
    }

    //-------------------------- //
    /**
     * @author franz.gamba
     * @see IConservacionLocal#buscarPersonaPorTipoYNumeroDeIdentificacion(String, String)
     */
    @Override
    public Persona buscarPersonaPorTipoYNumeroDeIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion) {

        return this.personaDao.getPersonabyTipoIdentificacionNumeroIdentificacion(
            tipoIdentificacion, numeroIdentificacion);
    }

    //-------------------------- //
    /**
     * @see IConservacionLocal#buscarUnidadesDeConstruccionPorPPredioId(Long)
     * @author david.cifuentes
     * @modifiedBy fredy.wilches
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionPorPPredioId(
        Long idPPredio, boolean incluirCanceladas) {

        List<PUnidadConstruccion> answer = null;
        try {
            answer = this.pUnidadConstruccionDao
                .buscarUnidadesDeConstruccionPorPPredioId(idPPredio,
                    incluirCanceladas);
        } catch (Exception e) {
            LOGGER.error("Error en List<PUnidadConstruccion>");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#buscarPPrediosPorTramiteId
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<PPredio> buscarPPrediosPorTramiteId(Long idTramite, List<String> cmiValuesToUse) {

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPorTramiteId(idTramite, cmiValuesToUse);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPPrediosPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerPPredioDatosUbicacion(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerPPredioDatosUbicacion(Long ppredioId) {

        PPredio answer = null;

        try {
            answer = this.pPredioDao.obtenerConDatosUbicacion(ppredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPPredioDatosPanelUbicacion: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerPPredioDatosJustificPropiedad(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerPPredioDatosJustificPropiedad(Long ppredioId) {

        PPredio answer = null;

        try {
            answer = this.pPredioDao.obtenerConDatosJustificacionPropiedad(ppredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPPredioDatosJustificPropiedad: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerPPredioDatosAvaluo(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerPPredioDatosAvaluo(Long ppredioId) {

        LOGGER.debug("en ConservacionBean#obtenerPPredioDatosAvaluo");

        PPredio answer = null;

        try {
            answer = this.pPredioDao.obtenerConDatosAvaluo(ppredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPPredioDatosAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerPPredioDatosPropietarios(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerPPredioDatosPropietarios(Long ppredioId) {

        LOGGER.debug("en ConservacionBean#obtenerPPredioDatosPropietarios");

        PPredio answer = null;

        try {
            answer = this.pPredioDao.obtenerConDatosPropietarios(ppredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPPredioDatosPropietarios: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerPPredioDatosFichaMatriz(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerPPredioDatosFichaMatriz(Long ppredioId) {

        LOGGER.debug("en ConservacionBean#obtenerPPredioDatosFichaMatriz ");

        PPredio answer = null;

        try {
            answer = this.pPredioDao.obtenerConDatosFichaMatriz(ppredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPPredioDatosFichaMatriz: " +
                ex.getMensaje());
        }

        return answer;

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IConservacionLocal#obtenerPPrediosPorTramiteIdActAvaluos(java.lang.Long)
     */
    @Override
    public List<PPredio> obtenerPPrediosPorTramiteIdActAvaluos(Long tramiteId) {

        LOGGER.debug("en ConservacionBean#obtenerPPrediosPorTramiteIdActAvaluos ");

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.getPPrediosByTramiteIdActualizarAvaluo(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPPrediosPorTramiteIdActAvaluos: " +
                ex.getMensaje());
        }

        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author.franz.gamba
     * @see IConservacionLocal#buscarPersonaPredioPorPredioId(Long)
     */
    @Override
    public List<PersonaPredio> buscarPersonaPredioPorPredioId(Long predioId) {

        LOGGER.debug("en ConservacionBean#buscarPersonaPredioPorPredioId ");
        List<PersonaPredio> answer = null;

        try {
            answer = this.personaPredioDao.getPersonaPrediosByPredioId(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPersonaPredioPorPredioId: " +
                ex.getMensaje());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IConservacionLocal#buscarPrediosColindantesPorServicioWeb(UsuarioDTO, String)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosColindantesPorServicioWeb(
        UsuarioDTO usuario, String numPredial) {
        LOGGER.debug("en ConservacionBean#buscarPrediosColindantesPorServicioWeb");

        List<Predio> prediosColindantes = null;

        List<String> colindantes = null;
        try {
            colindantes = SigDAOv2.getInstance().obtenerColindantesPredio(numPredial, usuario,
                "null");
        } catch (ExcepcionSNC e) {
            LOGGER.info("No existe información espacial para este predio en la base de datos");
            return null;
        }

        if (colindantes != null && colindantes.size() > 0) {
            prediosColindantes = new ArrayList<Predio>();
            //se eliminan los colindantes anteriores para evitar redundancias.
            List<Colindante> colAnteriores = this.transaccionalService.
                buscarColindantesNumeroPredialNuevaTransaccion(numPredial);
            this.transaccionalService.deleteMultipleColindantesNuevaTransaccion(colAnteriores);

            for (String numeroP : colindantes) {
                if (!numeroP.equals(numPredial)) {

                    Predio predio = this.transaccionalService.
                        findPredioByNumeroPredialNuevaTransaccion(numeroP);
                    prediosColindantes.add(predio);

                    Colindante colindante = new Colindante();
                    colindante.setFechaLog(new Date());
                    colindante.setNumeroPredial(numPredial);
                    colindante.setNumeroPredialColindante(numeroP);
                    colindante.setTipo(EColindanteTipo.PREDIO.getCodigo());
                    colindante.setUsuarioLog(usuario.getLogin());
                    this.transaccionalService.updateColindanteNuevaTransaccion(colindante);
                }
            }
        }
        return prediosColindantes;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @author felipe.cadena
     * @see IConservacionLocal#buscarPrediosColindantesMejorasPorServicioWeb(UsuarioDTO, String)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosColindantesMejorasPorServicioWeb(
        UsuarioDTO usuario, String numPredial) {
        LOGGER.debug("en ConservacionBean#buscarPrediosColindantesMejorasPorServicioWeb");

        List<Predio> prediosColindantes = null;
        //TODO :: felipe.cadena :: validar que la funcionalidad sea la correcta
        List<String> colindantes = SigDAOv2.getInstance().obtenerColindantesConstruccion(numPredial,
            usuario, "null");

        if (colindantes != null && colindantes.size() > 0) {
            prediosColindantes = new ArrayList<Predio>();

            for (String numeroP : colindantes) {
                if (!numeroP.equals(numPredial)) {
                    prediosColindantes.add(this
                        .findPredioByNumeroPredial(numeroP));
                }
            }
        }
        return prediosColindantes;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IConservacionLocal#conteoPersonaPrediosPorPersonaId(Long)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long conteoPersonaPrediosPorPersonaId(Long personaId) {
        Long answer = null;
        try {
            answer = this.personaPredioDao.countPersonaPrediosByPersonaId(personaId);
        } catch (Exception e) {
            LOGGER.error("error en ConservacionBean#conteoPersonaPrediosPorPersonaId: " +
                e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IConservacionLocal#actualizarPPersona(PPersona)
     */
    @Override
    public PPersona actualizarPPersona(PPersona persona) {
        PPersona answer = null;
        try {
            answer = this.pPersonaDao.update(persona);
        } catch (Exception e) {
            LOGGER.error("error en ConservacionBean#conteoPersonaPrediosPorPersonaId: " +
                e.getMessage());
        }
        return answer;
    }

    /**
     * @author ariel.ortiz
     * @see IConservacionLocal#obtenerXmlPredioPorPredioId(idPredio)
     */
    @Override
    public void obtenerXmlPredioPorPredioId(Long idPredio) {

        try {
            this.tramiteMovilesDao.getXmlPredioByPredioId(idPredio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * @see IGeneralesLocal#guardarFichaPredialGenerada(Documento documento, UsuarioDTO usuario)
     * @author javier.aponte
     */
    @Override
    public Documento guardarFichaPredialGenerada(Documento documento, UsuarioDTO usuario) {

        Documento resultado = null;

        try {
            resultado = this.documentoDao.almacenarFichaPredialDigitalGestorDocumental(documento,
                usuario, documento.getArchivo());
        } catch (Exception e) {
            LOGGER.error("Se produjo un error al almacenar la ficha predial digital:" + e.
                getMessage());
        }

        return resultado;
    }

    /**
     * @see IGeneralesLocal#guardarCertificadoPlanoPredialCatastral(Documento documento, UsuarioDTO
     * usuario)
     * @author javier.aponte
     */
    public Documento guardarCertificadoPlanoPredialCatastral(Documento documento, UsuarioDTO usuario) {

        Documento resultado = null;

        try {
            resultado = this.documentoDao.almacenarFichaPredialDigitalGestorDocumental(documento,
                usuario, documento.getArchivo());
        } catch (Exception e) {
            LOGGER.error(
                "Se produjo un error al almacenar el certificado plano predial catastral:" + e.
                    getMessage());
        }

        return resultado;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IConservacionLocal#obtenerUnidadesConstruccionsPorNumeroPredial(String)
     */
    @Override
    public List<UnidadConstruccion> obtenerUnidadesConstruccionsPorNumeroPredial(
        String numPred) {
        LOGGER.debug("en ConservacionBean#obtenerUnidadesConstruccionsPorNumeroPredial ");

        List<UnidadConstruccion> answer = null;

        try {
            answer = this.unidadConstruccionDao.getUnidadsConstruccionByNumeroPredial(numPred);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en ConservacionBean#obtenerUnidadesConstruccionsPorNumeroPredial: " +
                ex.getMensaje());
        }

        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IConservacionLocal#obtenerPUnidadesConstruccionsPorNumeroPredial(String)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstruccionsPorNumeroPredial(
        String numPred) {
        LOGGER.debug("en ConservacionBean#obtenerPUnidadesConstruccionsPorNumeroPredial ");

        List<PUnidadConstruccion> answer = null;

        try {
            answer = this.pUnidadConstruccionDao.getPUnidadsConstruccionByNumeroPredial(numPred);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en ConservacionBean#obtenerPUnidadesConstruccionsPorNumeroPredial: " +
                ex.getMensaje());
        }

        return answer;
    }

    // ------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#guardarListaPUnidadConstruccion(List<PUnidadConstruccion>)
     * @author david.cifuentes
     */
    @Override
    public List<PUnidadConstruccion> guardarListaPUnidadConstruccion(
        List<PUnidadConstruccion> pUnidadConstruccions) {

        List<PUnidadConstruccion> answer = null;

        try {
            answer = this.pUnidadConstruccionDao
                .updateMultipleReturn(pUnidadConstruccions);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#guardarListaPUnidadConstruccion: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IConservacion#actualizarListaUnidadConstruccionComp(List<PUnidadConstruccionComp>)
     * @author david.cifuentes
     */
    @Override
    public List<PUnidadConstruccionComp> actualizarListaUnidadConstruccionComp(
        List<PUnidadConstruccionComp> unidadesConstruccionComp) {

//TODO :: david.cifuentes :: hacer manejo de excepciones según el estándar :: pedro.garcia   
        if (unidadesConstruccionComp != null &&
            !unidadesConstruccionComp.isEmpty() &&
            unidadesConstruccionComp.get(0).getPUnidadConstruccion() != null) {
            this.pUnidadConstruccionCompDao
                .deleteByUnidadConstruccionId(unidadesConstruccionComp
                    .get(0).getPUnidadConstruccion().getId());
        }
        List<PUnidadConstruccionComp> componentesResult = this.pUnidadConstruccionCompDao
            .updateMultipleReturn(unidadesConstruccionComp);

        return componentesResult;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#borrarPFotosDeLaPUnidadConstruccion(Long)
     * @author david.cifuentes
     */
    @Override
    public void borrarPFotosDeLaPUnidadConstruccion(Long unidadConstruccionId) {
        try {

            List<String[]> criterios = new ArrayList<String[]>();
            String[] criterio = new String[3];

            criterio[0] = "PUnidadConstruccion.id";
            criterio[1] = "number";
            criterio[2] = unidadConstruccionId.toString();
            criterios.add(criterio);
            this.pFotoDao.deleteByExactCriteria(criterios);
        } catch (Exception e) {
            LOGGER.error("error on ConservacionBean#borrarPFotosDeLaPUnidadConstruccion: " +
                e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#borrarPModeloConstruccionFotosDelPFmModeloConstruccion(Long)
     * @author david.cifuentes
     */
    @Override
    public void borrarPModeloConstruccionFotosDelPFmModeloConstruccion(Long pFmModeloConstruccionId) {

        try {
            List<String[]> criterios = new ArrayList<String[]>();
            String[] criterio = new String[3];

            criterio[0] = "PFmModeloConstruccion.id";
            criterio[1] = "number";
            criterio[2] = pFmModeloConstruccionId.toString();
            criterios.add(criterio);
            this.pModeloConstruccionFotoDao.deleteByExactCriteria(criterios);
        } catch (Exception e) {
            LOGGER.error("error on ConservacionBean#borrarPFotosDeLaPUnidadConstruccion: " +
                e.getMessage());
        }
    }

    /**
     * @see IConservacionLocal#buscarCalificacionConstruccionPorId(Long)
     * @author david.cifuentes
     */
    @Override
    public CalificacionAnexo buscarCalificacionConstruccionPorId(
        Long calificacionAnexoId) {
        return this.calificacionAnexoDao.findById(calificacionAnexoId);
    }

    /**
     * @see IConservacion#buscarDatosFisicos(List<Predio>, int, int)
     * @author javier.barajas
     */
    @Override
    public List<Object[]> buscarDatosFisicos(List<String> listaPredios,
        int first, int pageSize) {

        return this.predioDao.getDatosFisicosByNumeroPredial(listaPredios, first, pageSize);
    }

    @Override
    public List<Object[]> buscarFichaMatrizReportes(List<String> listaPredios,
        int first, int pageSize) {

        return this.predioDao.getReporteFichaMatriz(listaPredios, first, pageSize);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see co.gov.igac.snc.fachadas.IConservacionLocal#obtenerValorUnidadTerreno(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public Double obtenerValorUnidadTerreno(String zonaCodigo, String destino,
        String zonaGeoEconomica, Long predioId) {

        Double answer = null;
        try {
            answer = this.sncProcedimientoDao.obtenerValorUnidadTerreno(zonaCodigo, destino,
                zonaGeoEconomica, predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerValorUnidadTerreno: " + ex.getMensaje());
        }

        return answer;
    }
    
    
  //--------------------------------------------------------------------------------------------------

    /**
     * @author vsocarras
     * @see co.gov.igac.snc.fachadas.IConservacionLocal#obtenerValorUnidadTerreno(java.lang.String,
     * java.lang.String, java.lang.String,Timestamp)
     */
    @Override
    public Double obtenerValorUnidadTerreno(String zonaCodigo, String destino,
        String zonaGeoEconomica, Long predioId,Timestamp vigencia) {

        Double answer = null;
        try {
            answer = this.sncProcedimientoDao.obtenerValorUnidadTerreno(zonaCodigo, destino,
                zonaGeoEconomica, predioId,vigencia );
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerValorUnidadTerreno: " + ex.getMensaje());
        }

        return answer;
    }
    
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#obtenerListaPPredioAvaluoCatastralPorIdPPredio(Long)
     * @author david.cifuentes
     */
    @Override
    public List<PPredioAvaluoCatastral> obtenerListaPPredioAvaluoCatastralPorIdPPredio(
        Long pPredioId) {
        return this.pPredioAvaluoCatastralDao
            .obtenerListaPPredioAvaluoCatastralPorIdPPredio(pPredioId);
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IConservacion#actualizarCartaCatastralUrbana(Long, UsuarioDTO, boolean)
     * @author javier.aponte
     */
    @Override
    public boolean actualizarCartaCatastralUrbana(Long tramiteId, UsuarioDTO usuario,
        boolean reemplazar) {

        boolean answer = true;

        //Parametro condicion propiedad validas para generar
        Parametro condicionPropiedadValida = this.generalesService.getCacheParametroPorNombre(
            EParametro.VALIDA_GENERACION_FICHA_PREDIAL_CARTA_CATASTRAL.toString());

        StringBuilder numerosPrediales = new StringBuilder();

        Tramite tramite = this.tramiteService.buscarTramiteConResolucionSimple(tramiteId);

        if (tramite.getPredio() != null && !condicionPropiedadValida.getValorCaracter().contains(
            tramite.getPredio().getCondicionPropiedad())) {
        	numerosPrediales.append(tramite.getPredio().getNumeroPredial()).append(
                Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);

        }
        
        if (tramite.isQuinta() && tramite.getNumeroPredial() != null && tramite.getPredio()!=null) {
            if (!numerosPrediales.toString().contains(tramite.getNumeroPredial()) &&
                !condicionPropiedadValida.getValorCaracter().contains(tramite.getPredio().
                    getCondicionPropiedad())) {
                numerosPrediales.append(tramite.getNumeroPredial());
            }
        }

        if (numerosPrediales.length() > 0) {
            if (numerosPrediales.toString().endsWith(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD)) {
                numerosPrediales.deleteCharAt(numerosPrediales.length() - 1);
            }

            //javier.aponte :: refs#15532 :: Si no se debe reemplazar la carta entonces
            //se genera sólo para aquellos predios para los cuales no exista :: 18/01/2016
            if (!reemplazar) {

                Documento cartaTemp;
                StringBuilder numerosPredialesSinCarta = new StringBuilder();

                for (String numeroPredial : numerosPrediales.toString().split(
                    Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD)) {

                    cartaTemp = this.documentoDao.buscarDocumentoPorNumeroPredialAndTipoDocumento(
                        numeroPredial.substring(0, 17), ETipoDocumento.CARTA_CATASTRAL_URBANA.
                        getId());
                    if (cartaTemp == null) {
                        numerosPredialesSinCarta.append(numeroPredial +
                            Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                    }
                }

                if (numerosPredialesSinCarta.toString().endsWith(
                    Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD)) {
                    numerosPredialesSinCarta.deleteCharAt(numerosPredialesSinCarta.length() - 1);
                }

                //Se envia petición para crear el job para  carta catastral urbana
                //felipe.cadena::#16096::23-02-2016::Solo se invoca el metodo si existe por lo menos un predio para generar carta
                if (!numerosPredialesSinCarta.toString().isEmpty()) {
                    answer = this.generarCartaCatastralUrbana(numerosPredialesSinCarta.toString(),
                        usuario, tramite.getId(), true);
                }

            } else {

                //Se envia petición para crear el job para  carta catastral urbana
                answer = this.generarCartaCatastralUrbana(numerosPrediales.toString(), usuario,
                    tramite.getId(), true);

            }

        }
        return answer;

    }

//--------------------------------------------------------------------------------------------------	
    /**
     * Para los niveles de transaccionalidad ver Transaction Annotations
     * http://tomee.apache.org/transaction-annotations.html
     *
     * @see IConservacion#generarCartaCatastralUrbana(String, UsuarioDTO, Long ){
     * @author javier.aponte
     */
    @Override
    public boolean generarCartaCatastralUrbana(String numerosPrediales, UsuarioDTO usuario,
        Long tramiteId, boolean generarEnPDF) {

        LOGGER.debug("en ConservacionBean#generarCartaCatastralUrbana ");
        boolean answer = true;

        try {
            String numerosPredialesPredio[] = numerosPrediales.toString().split(",");

            EstructuraOrganizacional eo;
            Departamento departamento;
            Municipio municipio;
            String territorial;
            String nombreDepartamento;
            String nombreMunicipio;

            for (String numeroPredial : numerosPredialesPredio) {
                eo = this.jurisdiccionDao.getEstructuraOrganizacionalByMunicipioCod(numeroPredial.
                    substring(0, 5));
                territorial = eo.getNombre().trim();

                departamento = this.departamentoDao.getDepartamentoByCodigo(numeroPredial.substring(
                    0, 2));
                municipio = this.municipioDao.getMunicipioByCodigo(numeroPredial.substring(0, 5));

                nombreDepartamento = departamento.getNombre().trim().replace("(",
                    Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
                nombreMunicipio = municipio.getNombre().trim().replace("(",
                    Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);

                nombreDepartamento = nombreDepartamento.split(
                    Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)[0];
                nombreMunicipio = nombreMunicipio.split(
                    Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)[0];
                //v1.1.9
                Tramite tramite = new Tramite();
                tramite.setId(tramiteId);
                SigDAOv2.getInstance().generarCCUAsync(this.productoCatastralJobDAO, numeroPredial.
                    substring(0, 17), generarEnPDF,
                    tramite, null, usuario);
            }
        } catch (ExcepcionSNC e) {
            answer = false;
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * Para los niveles de transaccionalidad ver Transaction Annotations
     * http://tomee.apache.org/transaction-annotations.html
     *
     * @see IConservacion#generarFichaPredialDigital(String, UsuarioDTO ){
     * @author javier.aponte
     */
    @Override
    public boolean generarFichaPredialDigital(String numerosPrediales, UsuarioDTO usuario,
        Long tramiteId) {

        LOGGER.debug("en ConservacionBean#generarFichaPredialDigital ");
        boolean answer = true;

        String numerosPredialesPredio[] = numerosPrediales.toString().split(",");

        try {
            for (String numeroPredial : numerosPredialesPredio) {
                Tramite tramite = new Tramite();
                tramite.setId(tramiteId);
                SigDAOv2.getInstance().generarFPDAsync(this.productoCatastralJobDAO, numeroPredial,
                    tramite, null, usuario);
            }
        } catch (ExcepcionSNC e) {
            answer = false;
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#actualizarFichaPredialDigital(Long, UsuarioDTO, boolean)
     * @author javier.aponte
     */
    @Override
    public boolean actualizarFichaPredialDigital(Long tramiteId, UsuarioDTO usuario,
        boolean reemplazar) {

        boolean answer = true;

        StringBuilder numerosPrediales;

        //Parametro condicion propiedad validas para generar
        Parametro condicionPropiedadValida = this.generalesService.getCacheParametroPorNombre(
            EParametro.VALIDA_GENERACION_FICHA_PREDIAL_CARTA_CATASTRAL.toString());

        //Código para la generación de la ficha predial digital
        numerosPrediales = new StringBuilder();

        Tramite tramite = this.tramiteService.buscarTramiteConResolucionSimple(tramiteId);

        if (tramite.getPredio() != null) {

            if (tramite.isSegundaDesenglobe()) {

                if (ETramiteEstado.FINALIZADO_APROBADO.getCodigo().equals(tramite.getEstado())) {

                    List<HPredio> prediosHistoricos = null;
                    prediosHistoricos = this.hPredioDao.
                        buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(tramite.
                            getId());
                    if (prediosHistoricos != null && !prediosHistoricos.isEmpty()) {

                        FichaMatriz fichaMatriz = null;
                        FichaMatrizPredio fichaMatrizPredio;

                        for (HPredio predioTemp : prediosHistoricos) {

                            //Para predio con condición 5 0 6
                            if (EPredioCondicionPropiedad.CP_5.getCodigo().equals(predioTemp.
                                getCondicionPropiedad()) ||
                                EPredioCondicionPropiedad.CP_6.getCodigo().equals(predioTemp.
                                    getCondicionPropiedad())) {

                                String numeroPredialParaMejora = predioTemp.getNumeroPredial().
                                    substring(0, 21) + "000000000";

                                if (!numerosPrediales.toString().contains(numeroPredialParaMejora) &&
                                    !condicionPropiedadValida.getValorCaracter().contains(
                                        predioTemp.getCondicionPropiedad())) {
                                    numerosPrediales.append(numeroPredialParaMejora).append(
                                        Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                                }

                                break;

                            } //Para condición 8 y 9 se genera la ficha predial digital para la ficha matriz
                            else if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(predioTemp.
                                getCondicionPropiedad()) ||
                                EPredioCondicionPropiedad.CP_9.getCodigo().equals(predioTemp.
                                    getCondicionPropiedad())) {

                                try {
                                    if (predioTemp.getNumeroPredial().endsWith("00000000")) {
                                        fichaMatriz = this.fichaMatrizDao.
                                            obtenerFichaMatrizPorPredioId(predioTemp.getId());
                                    } else {
                                        fichaMatrizPredio = this.fichaMatrizPredioDAO.
                                            obtenerFichaMatrizPredioPorNumeroPredial(predioTemp.
                                                getNumeroPredial());
                                        if (fichaMatrizPredio != null) {
                                            fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                                        }
                                    }
                                } catch (ExcepcionSNC e) {
                                    throw SncBusinessServiceExceptions.EXCEPCION_100001.
                                        getExcepcion(LOGGER, e, e.getMessage());
                                }

                                if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                                    if (!numerosPrediales.toString().contains(fichaMatriz.
                                        getPredio().getNumeroPredial()) &&
                                        !condicionPropiedadValida.getValorCaracter().contains(
                                            fichaMatriz.getPredio().getCondicionPropiedad())) {
                                        numerosPrediales.append(fichaMatriz.getPredio().
                                            getNumeroPredial()).append(
                                                Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                                    }
                                }

                                break;

                            } else {

                                if (!numerosPrediales.toString().contains(predioTemp.
                                    getNumeroPredial()) && !condicionPropiedadValida.
                                        getValorCaracter().contains(predioTemp.
                                            getCondicionPropiedad())) {
                                    numerosPrediales.append(predioTemp.getNumeroPredial()).append(
                                        Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                                }

                            }

                        }
                    }

                } else {

                    if (!numerosPrediales.toString().
                        contains(tramite.getPredio().getNumeroPredial()) &&
                        !condicionPropiedadValida.getValorCaracter().contains(tramite.getPredio().
                            getCondicionPropiedad())) {
                        numerosPrediales.append(tramite.getPredio().getNumeroPredial()).append(
                            Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                    }

                }

            } else {

                //Para predio con condición 5 0 6
                if (tramite.getPredio().isMejora()) {

                    String numeroPredialParaMejora = tramite.getPredio().getNumeroPredial().
                        substring(0, 21) + "000000000";

                    if (!numerosPrediales.toString().contains(numeroPredialParaMejora) &&
                        !condicionPropiedadValida.getValorCaracter().contains(tramite.getPredio().
                            getCondicionPropiedad())) {
                        numerosPrediales.append(numeroPredialParaMejora).append(
                            Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                    }

                } //Para condición 8 y 9 se genera la ficha predial digital para la ficha matriz
                else if (tramite.getPredio().isCondicionPropiedadRara()) {

                    FichaMatriz fichaMatriz = null;
                    FichaMatrizPredio fichaMatrizPredio;
                    try {

                        if (tramite.getPredio().getNumeroPredial().endsWith("00000000")) {
                            fichaMatriz = this.fichaMatrizDao.obtenerFichaMatrizPorPredioId(tramite.
                                getPredio().getId());
                        } else {
                            fichaMatrizPredio = this.fichaMatrizPredioDAO.
                                obtenerFichaMatrizPredioPorNumeroPredial(tramite.getPredio().
                                    getNumeroPredial());
                            if (fichaMatrizPredio != null) {
                                fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                            }
                        }
                    } catch (ExcepcionSNC e) {
                        throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e,
                            e.getMessage());
                    }

                    if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                        if (!numerosPrediales.toString().contains(fichaMatriz.getPredio().
                            getNumeroPredial()) && !condicionPropiedadValida.getValorCaracter().
                                contains(fichaMatriz.getPredio().getCondicionPropiedad())) {
                            numerosPrediales.append(fichaMatriz.getPredio().getNumeroPredial()).
                                append(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                        }
                    }

                } else {
                    if (!numerosPrediales.toString().
                        contains(tramite.getPredio().getNumeroPredial()) &&
                        !condicionPropiedadValida.getValorCaracter().contains(tramite.getPredio().
                            getCondicionPropiedad())) {
                        numerosPrediales.append(tramite.getPredio().getNumeroPredial()).append(
                            Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                    }
                }
            }

        }

        if (tramite.isQuinta() && tramite.getNumeroPredial() != null) {

            List<HPredio> prediosHistoricos = null;
            prediosHistoricos = this.hPredioDao.
                buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(tramite.getId());
            if (prediosHistoricos != null && !prediosHistoricos.isEmpty()) {
                for (HPredio predioTemp : prediosHistoricos) {
                    if (!numerosPrediales.toString().contains(predioTemp.getNumeroPredial()) &&
                        !condicionPropiedadValida.getValorCaracter().contains(predioTemp.
                            getCondicionPropiedad())) {
                        numerosPrediales.append(predioTemp.getNumeroPredial()).append(
                            Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
                    }
                }
            }
        }

        if (numerosPrediales.length() > 0) {

            if (numerosPrediales.toString().endsWith(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD)) {
                numerosPrediales.deleteCharAt(numerosPrediales.length() - 1);
            }

            //javier.aponte :: refs#15532 :: Si no se debe reemplazar la ficha entonces
            //se genera sólo para aquellos predios para los cuales no exista :: 18/01/2016
            if (!reemplazar) {

                String numerosPredialesConFormatoSQL = "'" + numerosPrediales.toString().replaceAll(
                    Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD, "','") + "'";

                String numerosPredialesSinFicha = this.documentoDao.
                    obtenerNumerosPredialesSinFichaPredialDigital(numerosPredialesConFormatoSQL);

                //Se envia petición para crear el job para  ficha predial digital
                answer = this.generarFichaPredialDigital(numerosPredialesSinFicha, usuario, tramite.
                    getId());
            } else {

                //Se envia petición para crear el job para  ficha predial digital
                answer = this.generarFichaPredialDigital(numerosPrediales.toString(), usuario,
                    tramite.getId());

            }
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------	

    @Override
    public List<Predio> buscarPredioGenereacionFormularioSBCporCadena(String cadenaBusqueda) {
        try {
            return this.predioDao.getBuscaPredioGenereacionFormularioSBC(cadenaBusqueda);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IConservacionLocal#obtenerPpredioDirecionesPorPredioId(java.lang.Long)
     */
    @Override
    public List<PPredioDireccion> obtenerPpredioDirecionesPorPredioId(
        Long predioId) {
        try {
            return this.pPredioDireccionDao.getPPredioDireccionesByPredioId(predioId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IConservacion#buscarDocumentoPorPredioIdAndTipoDocumento(predioId, idTipoDocumento){
     * @author javier.aponte
     */
    @Override
    public Documento buscarDocumentoPorPredioIdAndTipoDocumento(Long predioId, Long idTipoDocumento) {

        LOGGER.debug("en ConservacionBean#buscarDocumentoPorPredioIdAndTipoDocumento");
        Documento answer = null;

        try {
            answer = this.documentoDao.buscarDocumentoPorPredioIdAndTipoDocumento(predioId,
                idTipoDocumento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarDocumentoPorPredioIdAndTipoDocumento: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IConservacion#buscarDocumentoPorNumeroPredialAndTipoDocumento(numeroPredial,
     * idTipoDocumento){
     * @author javier.aponte
     */
    @Override
    public Documento buscarDocumentoPorNumeroPredialAndTipoDocumento(String numeroPredial,
        Long idTipoDocumento) {

        LOGGER.debug("en ConservacionBean#buscarDocumentoPorNumeroPredialAndTipoDocumento");
        Documento answer = null;

        try {
            answer = this.documentoDao.
                buscarDocumentoPorNumeroPredialAndTipoDocumento(numeroPredial, idTipoDocumento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en ConservacionBean#buscarDocumentoPorNumeroPredialAndTipoDocumento: " +
                ex.getMensaje());
        }

        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#getPPredioByNumeroPredial(String)
     * @author javier.aponte
     */
    @Override
    public PPredio getPPredioByNumeroPredial(String numeroPredial) {
        LOGGER.debug("Ingreso al método: getPPredioByNumeroPredial en ConservacionBean");
        return this.pPredioDao.getPPredioByNumeroPredial(numeroPredial);
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerPredioValidacionesRectificacionById(String)
     */
    @Override
    public Predio obtenerPredioValidacionesRectificacionById(Long idPredio) {

        LOGGER.debug("en ConservacionBean#obtenerPredioValidacionesRectificacionById");
        Predio answer = null;

        try {
            answer = this.predioDao.obtenerParaValidacionesRectificacionById(idPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPredioValidacionesRectificacionById: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#buscarFotoPorIdUnidadConstruccionYTipo(Long, String)
     */
    @Override
    public List<Foto> buscarFotoPorIdUnidadConstruccionYTipo(Long unidadId, String codigoTipoFoto) {

        LOGGER.debug("en ConservacionBean#buscarFotoPorIdUnidadConstruccionYTipo");
        List<Foto> answer = null;

        try {
            answer = this.fotoDao.buscarPorIdUnidadConstruccionYTipo(unidadId, codigoTipoFoto);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarFotoPorIdUnidadConstruccionYTipo: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#borrarPFotos(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void borrarPFotos(List<PFoto> listaPFotosABorrar) {

        List<Documento> listaDocumentosABorrar;

        //D: lista de ids de documentos que se van a borrar del gestor documental
        ArrayList<String> listaIDGDABorrar;

        listaDocumentosABorrar = new ArrayList<Documento>();
        listaIDGDABorrar = new ArrayList<String>();

        for (PFoto pfoto : listaPFotosABorrar) {
            listaDocumentosABorrar.add(pfoto.getDocumento());
            listaIDGDABorrar.add(pfoto.getDocumento().getIdRepositorioDocumentos());
        }

        //D: se borran los registros de PFoto
        try {
            this.pFotoDao.deleteMultiple(listaPFotosABorrar);
        } catch (Exception ex1) {
            LOGGER.error("Error borrando registros de PFotos: " + ex1.getMessage());
        }

        //D: se borran los registros de Documento asociados a las PFoto
        try {
            this.documentoDao.deleteMultiple(listaDocumentosABorrar);
        } catch (Exception ex2) {
            LOGGER.error("Error borrando registros de PFotos: " + ex2.getMessage());
        }

        //D: se borran del gestor documental los archivos correspondientes a los Documento
        try {
            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            servicioDocumental.borrarDocumentos(listaIDGDABorrar);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error eliminando documentos del gestor documental: " + ex.getMensaje());
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IConservacionLocal#obtenerHistoricoAvaluoPredio(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<PredioAvaluoCatastral> obtenerHistoricoAvaluoPredio(Long predioId) {

        List<PredioAvaluoCatastral> answer = null;

        try {
            answer = this.predioAvaluoCatastralDao.obtenerHistoricoPorPredio(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error obteniendo el histórico del avalúo para un predio: " +
                ex.getMensaje());
        }

        return answer;
    }

    // ------------------------------------------------------ //
    /**
     * @see IConservacionLocal#guardarPFotoUnidadDelModelo(PModeloConstruccionFoto
     * pModeloConstruccionFoto, UsuarioDTO usuario)
     * @author david.cifuentes
     */
    @Override
    public PModeloConstruccionFoto guardarPFotoUnidadDelModelo(
        PModeloConstruccionFoto pModeloConstruccionFoto, UsuarioDTO usuario) {

        Documento documentoGuardado;
        String rutaLocal;

        if (null == pModeloConstruccionFoto.getFotoDocumento()
            .getRutaArchivoLocal() ||
            pModeloConstruccionFoto.getFotoDocumento()
                .getRutaArchivoLocal().isEmpty()) {
            LOGGER.debug("La ruta local de la PModeloConstruccionFoto es vacía o nula");
            return pModeloConstruccionFoto;
        }

        rutaLocal = pModeloConstruccionFoto.getFotoDocumento()
            .getRutaArchivoLocal();
        documentoGuardado = new Documento();

        try {
            documentoGuardado = this.documentoDao
                .almacenarDocumentoFotoEnGestorDocumental(
                    pModeloConstruccionFoto.getFotoDocumento(),
                    usuario, rutaLocal);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("No se pudo guardar en el gestor documental la foto correspondiente" +
                " a la PModeloConstruccionFoto." +
                "En ConservacionBean#guardarPFotoUnidadDelModelo. Excepción: " +
                ex.getMessage());
        }

        pModeloConstruccionFoto.setFotoDocumento(documentoGuardado);
        pModeloConstruccionFoto = this.pModeloConstruccionFotoDao
            .guardarPModeloConstruccionFoto(pModeloConstruccionFoto);

        return pModeloConstruccionFoto;
    }

    // ------------------------------------------------------ //
    /**
     * @see IConservacionLocal#borrarPModeloConstruccionFotos(List<
     *      PModeloConstruccionFoto> listaPModeloConstFotoABorrar)
     * @author david.cifuentes
     */
    @Override
    public void borrarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> listaPModeloConstFotoABorrar) {

        List<Documento> listaDocumentosABorrar;

        // D: lista de ids de documentos que se van a borrar del gestor
        // documental
        ArrayList<String> listaIDGDABorrar;

        listaDocumentosABorrar = new ArrayList<Documento>();
        listaIDGDABorrar = new ArrayList<String>();

        for (PModeloConstruccionFoto pModeloConstruccionFoto : listaPModeloConstFotoABorrar) {
            listaDocumentosABorrar.add(pModeloConstruccionFoto
                .getFotoDocumento());
            listaIDGDABorrar.add(pModeloConstruccionFoto.getFotoDocumento()
                .getIdRepositorioDocumentos());
        }

        // D: se borran los registros de PModeloConstruccionFoto
        try {
            this.pModeloConstruccionFotoDao
                .deleteMultiple(listaPModeloConstFotoABorrar);
        } catch (Exception ex1) {
            LOGGER.error("Error borrando registros de PModeloConstruccionFotos: " +
                ex1.getMessage());
        }

        // D: se borran los registros de Documento asociados a las PFoto
        try {
            this.documentoDao.deleteMultiple(listaDocumentosABorrar);
        } catch (Exception ex2) {
            LOGGER.error("Error borrando registros de PModeloConstruccionFoto: " +
                ex2.getMessage());
        }
        // D: se borran del gestor documental los archivos correspondientes a
        // las Documento
        try {
            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();
            servicioDocumental.borrarDocumentos(listaIDGDABorrar);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error eliminando documentos del gestor documental: " +
                ex.getMensaje());
        }
    }
//--------------------------------------------------------------------------------------------------

    // -------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#determinarColindancia(List<Predio>, Predio, UsuarioDTO, boolean)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> determinarColindancia(List<Predio> predios, Predio predioBase,
        UsuarioDTO usuario, boolean validarColindantesWS) {

        List<Predio> colindantes;
        List<Predio> PredioNoColindantes = new ArrayList<Predio>();
        int[][] matrizColindancia;
        HashMap<Long, List<Predio>> prediosColindantes = new HashMap<Long, List<Predio>>();

        //Si se quiere validar solo un predio se asume colindante.
        if (predios.size() <= 1 && (predios.size() > 0 && predios.get(0).getNumeroPredial().equals(
            predioBase.getNumeroPredial()))) {
            return PredioNoColindantes;
        }

        //Se agrega el predio base para determinar la colindancia entre todo el grupo de predios
        predios.add(0, predioBase);

        for (Predio predio : predios) {
            colindantes = this.buscarPrediosColindantesPorNumeroPredial(predio.getNumeroPredial());
            if (colindantes == null && validarColindantesWS) {
                try {
                    colindantes = this.buscarPrediosColindantesPorServicioWeb(usuario, predio.
                        getNumeroPredial());
                } catch (ExcepcionSNC e) {
                    continue;
                }
            }
            if (colindantes != null) {
                prediosColindantes.put(predio.getId(), colindantes);
            }
        }

        //se calcula la matriz de colindancia
        matrizColindancia = this.calcularMatrizColindancia(predios, prediosColindantes);

        //se calcula la matriz de colindancia relativa
        matrizColindancia = this.calcularMatrizColindanciaRealativa(matrizColindancia);

        int len = matrizColindancia.length;

        //si el valor es cero quiere decir que el predio no es colindadnte con el predio Base
        for (int i = 0; i < len; i++) {
            if (matrizColindancia[i][0] == 0) {
                PredioNoColindantes.add(predios.get(i));
            }
        }
        return PredioNoColindantes;
    }

    /**
     * @see IConservacionLocal#determinarColindancia(List<Predio>, UsuarioDTO)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> determinarColindanciaMejoras(List<Predio> predios, UsuarioDTO usuario) {

        List<Predio> colindantes;
        List<Predio> PredioNoColindantes = new ArrayList<Predio>();
        int[][] matrizColindancia;
        HashMap<Long, List<Predio>> prediosColindantes = new HashMap<Long, List<Predio>>();

        //Si se quiere validar solo un predio se asume colindante.
        if (predios.size() <= 1) {
            return PredioNoColindantes;
        }

        for (Predio predio : predios) {
            try {
                colindantes = this.buscarPrediosColindantesMejorasPorServicioWeb(usuario, predio.
                    getNumeroPredial());
            } catch (ExcepcionSNC e) {
                continue;
            }

            prediosColindantes.put(predio.getId(), colindantes);
        }

        //se calcula la matriz de colindancia
        matrizColindancia = this.calcularMatrizColindancia(predios, prediosColindantes);

        //se calcula la matriz de colindancia relativa
        matrizColindancia = this.calcularMatrizColindanciaRealativa(matrizColindancia);

        int len = matrizColindancia.length;

        //si el valor es cero quiere decir que el predio no es colindadnte con el predio Base
        for (int i = 0; i < len; i++) {
            if (matrizColindancia[i][0] == 0) {
                PredioNoColindantes.add(predios.get(i));
            }
        }
        return PredioNoColindantes;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * Retorna una matriz de colindancia entre un grupo de predios.
     *
     * @author felipe.cadena
     *
     * @param predios Mapa con los predios y sus colindantes
     */
    private int[][] calcularMatrizColindancia(List<Predio> predios,
        HashMap<Long, List<Predio>> colindantes) {

        int len = predios.size();
        int[][] colindancia = new int[len][len];
        List<Predio> colindantesP;

        for (int i = 0; i < len; i++) {
            colindantesP = colindantes.get(predios.get(i).getId());
            if (colindantesP == null) {
                continue;
            }
            for (int j = 0; j < len; j++) {
                Predio p = predios.get(j);
                if (i == j || colindantesP.contains(p)) {
                    colindancia[i][j] = 1;
                } else {
                    colindancia[i][j] = 0;
                }
            }
        }

        return colindancia;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Calcula los caminos de colindancia de un grupo de predios.
     *
     * @param matrizCaminos
     * @return
     */
    private int[][] calcularMatrizColindanciaRealativa(int[][] matrizAdjacencia) {
        int len = matrizAdjacencia[0].length;
        int[][] matrizCaminos = new int[len][len];
        System.arraycopy(matrizAdjacencia, 0, matrizCaminos, 0, len);
        for (int k = 0; k < len; k++) {
            for (int i = 0; i < len; i++) {
                if (matrizCaminos[i][k] == 0) {
                    continue;
                }
                for (int j = 0; j < len; j++) {
                    if (matrizCaminos[i][k] == 1 &&
                        matrizCaminos[k][j] == 1) {
                        matrizCaminos[i][j] = 1;
                    }
                }
            }
        }
        return matrizCaminos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#reversarProyeccionPredio(java.lang.Long, java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public void reversarProyeccionPredio(Long idTramite, Long idPredio) {

        try {
            this.sncProcedimientoDao.reversarProyeccionPredio(idTramite, idPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Reversando proyeccion predio " +
                ex.getMensaje());
        }

    }

    /**
     * @see IConservacionLocal#obtenerPrediosPH(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> obtenerPrediosPH(String terreno) {

        List<Predio> resultado = null;

        try {
            resultado = this.predioDao.obtenerPrediosPH(terreno);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("obtenerPrediosPHoCondominio " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    @Override
    public FichaMatriz obtenerFichaMatrizOrigen(String numeroPredial) {

        FichaMatriz resultado = null;

        try {
            resultado = this.fichaMatrizDao.obtenerPorNumeroPredial(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("obtenerFichaMatrizOrigen " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IConservacionLocal#obtenerPrediosInconsistenciaPorIdPredioId(String idPredio)
     * @author javier.aponte
     */
    @Override
    public List<PredioInconsistencia> obtenerPrediosInconsistenciaPorIdPredio(Long idPredio) {

        List<PredioInconsistencia> resultado = null;

        try {
            resultado = this.predioInconsistenciaDao.findByPredioId(idPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en obtenerPrediosInconsistenciaPorIdPredioId " +
                ex.getMensaje());
        }

        return resultado;
    }
    //-------------------------------------------------------------------------------------------------   

    /**
     * Método encargado de consultar la información para el módulo gerencial en cuanto a predios se
     * refiere
     *
     * @param anio
     * @param codigoDepartamento
     * @param codigoMunicipio
     * @param zona
     * @return
     */
    @Override
    public List<DivisionAdministrativaDTO> consultaEstadisticaGerencialPredio(Long anio,
        String codigoDepartamento, String codigoMunicipio, String zona) {

        List<DivisionAdministrativaDTO> resultado = new ArrayList<DivisionAdministrativaDTO>();

        if (codigoMunicipio == null || codigoMunicipio.equals("")) {

            if (codigoDepartamento == null || codigoDepartamento.equals("")) {
                //Consulta pais
                List<Object[]> datos = this.estadisticaGerenciaPredioDao.consultaPorPais(anio, zona);
                DivisionAdministrativaDTO dto = null;
                String dpto = null;
                boolean nuevo;
                for (Object[] e : datos) {
                    nuevo = false;
                    if (dpto == null || !e[0].equals(dpto)) {
                        nuevo = true;
                        dto = new DivisionAdministrativaDTO("");
                        dto.setDivisionAdministrativa((String) e[0]);
                        dpto = (String) e[0];
                    }
                    dto.getDatos().add(new TotalPredialDTO(((BigDecimal) e[1]).intValue(),
                        (String) e[2], (BigDecimal) e[3], (BigDecimal) e[4], (BigDecimal) e[5],
                        (BigDecimal) e[6], (BigDecimal) e[7]));
                    if (nuevo) {
                        resultado.add(dto);
                    }
                }
            } else {

                // Consulta por dpto
                List<Object[]> datos = this.estadisticaGerenciaPredioDao.consultaPorDepartamento(
                    anio, codigoDepartamento, zona);
                DivisionAdministrativaDTO dto = null;
                String mun = null;
                boolean nuevo;
                for (Object[] e : datos) {
                    nuevo = false;
                    if (mun == null || !e[0].equals(mun)) {
                        nuevo = true;
                        dto = new DivisionAdministrativaDTO("");
                        dto.setDivisionAdministrativa((String) e[0]);
                        mun = (String) e[0];
                    }
                    dto.getDatos().add(new TotalPredialDTO(((BigDecimal) e[1]).intValue(),
                        (String) e[2], (BigDecimal) e[3], (BigDecimal) e[4], (BigDecimal) e[5],
                        (BigDecimal) e[6], (BigDecimal) e[7]));
                    if (nuevo) {
                        resultado.add(dto);
                    }

                }

            }
        } else {
            // Consulta por municipio
            List<Object[]> datos = this.estadisticaGerenciaPredioDao.consultaPorMunicipio(anio,
                codigoMunicipio, zona);
            DivisionAdministrativaDTO dto = new DivisionAdministrativaDTO("");
            for (Object[] e : datos) {
                /* dto.setDivisionAdministrativa(e.getId().getMunicipio().getNombre());
                 * dto.getDatos().add(new TotalPredialDTO(e.getId().getAnio(), e.getId().getZona(),
                 * e.getCantidadPredios(), e.getCantidadPropietarios(), e.getAreaTerreno(),
                 * e.getAreaConstruccion(), e.getValorAvaluo())); */
                dto.setDivisionAdministrativa((String) e[0]);
                dto.getDatos().add(
                    new TotalPredialDTO(((BigDecimal) e[1]).intValue(), (String) e[2],
                        (BigDecimal) e[3], (BigDecimal) e[4], (BigDecimal) e[5], (BigDecimal) e[6],
                        (BigDecimal) e[7]));
            }
            resultado.add(dto);
        }

        return resultado;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método encargado de consultar la información para el módulo gerencial en cuanto a manzanas se
     * refiere
     *
     * @param anio
     * @param codigoMunicipio
     * @param zona
     * @return
     */
    @Override
    public List<MunicipioManzanaDTO> consultaEstadisticaGerencialManzana(Long anio,
        String codigoMunicipio, String zona) {

        List<MunicipioManzanaDTO> resultado = new ArrayList<MunicipioManzanaDTO>();

        List<Object[]> datos = this.estadisticaGerenciaManzanaDao.consultaPorMunicipio(anio,
            codigoMunicipio, zona);
        MunicipioManzanaDTO dto = new MunicipioManzanaDTO("");
        for (Object[] e : datos) {
            /* dto.setDivisionAdministrativa(e.getId().getMunicipio().getNombre());
             * dto.getDatos().add(new TotalPredialDTO(e.getId().getAnio(), e.getId().getZona(),
             * e.getCantidadPredios(), e.getCantidadPropietarios(), e.getAreaTerreno(),
             * e.getAreaConstruccion(), e.getValorAvaluo())); */
            dto.setDivisionAdministrativa((String) e[0]);
            dto.getDatos().add(new TotalManzanaDTO(((BigDecimal) e[1]).intValue(), (String) e[2],
                (BigDecimal) e[3], (BigDecimal) e[4], (BigDecimal) e[5], (BigDecimal) e[6],
                (BigDecimal) e[7], (BigDecimal) e[8], (BigDecimal) e[9], (BigDecimal) e[10],
                (BigDecimal) e[11], (BigDecimal) e[12], (BigDecimal) e[13], (BigDecimal) e[14],
                (BigDecimal) e[15], (BigDecimal) e[16], (BigDecimal) e[17], (BigDecimal) e[18]));
        }
        resultado.add(dto);
        return resultado;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#generarReplicaTramite(Actividad ,UsuarioDTO, boolean);
     * @author felipe.cadena
     */
    @Override
    public void generarReplicaDeConsultaTramiteYAvanzarProceso(Actividad actividad,
        UsuarioDTO usuario, UsuarioDTO responsableSIG, boolean depuracion) {
        //Se consulta la informacion del tramite
        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(actividad.
            getIdObjetoNegocio());

        //Valida si el tramite ya tiene replica
        Long replicaOriginalId = ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId();
        List<TramiteDocumento> td = tramite.getTramiteDocumentos();
        boolean noTieneReplicaFlag = true;
        for (TramiteDocumento tdTemp : td) {
            if (tdTemp.getDocumento().getTipoDocumento().getId().equals(replicaOriginalId)) {
                noTieneReplicaFlag = false;
            }
        }

        //Se avanza la actividad con el usuario correspondiente
        if (depuracion) {

            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(responsableSIG);
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setUsuarios(usuarios);

            solicitudCatastral.setTransicion(
                ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL);
            this.procesosService.avanzarActividad(usuario, actividad.getId(), solicitudCatastral);
        }

        //Si el tramite no tiene replica se inicia el trabajo 
        if (noTieneReplicaFlag) {

            //Se transfiere la actividad al usuario tiempos muertos
            UsuarioDTO usuarioTiempoMuertos = this.generalesService.getCacheUsuario(
                ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());
            List<Actividad> actividadesNuevas = this.procesosService.
                getActividadesPorIdObjetoNegocio(tramite.getId());

            if (!actividadesNuevas.isEmpty()) {
                this.procesosService.transferirActividadConservacion(actividadesNuevas.get(0).
                    getId(), usuarioTiempoMuertos);
            }

            StringBuilder prediosSb = new StringBuilder("");

            List<Predio> predios = tramite.getPredios();

            if (tramite.isQuinta()) {
                prediosSb.append(tramite.getNumeroPredial());
            } else {
                if (tramite.getPredio() != null) {
                    prediosSb.append(tramite.getPredio().getNumeroPredial()).append(",");
                }

                for (Predio predio : predios) {
                    if (!(("5".equals(predio.getCondicionPropiedad()) || "6".equals(predio.
                        getCondicionPropiedad())))) {
                        if (tramite.getPredio() != null) {
                            if (predio.getId().equals(tramite.getPredio().getId())) {
                                continue;
                            }
                        }
                        prediosSb.append(predio.getNumeroPredial()).append(",");
                    }
                }
            }

            if (depuracion) {
                usuario = responsableSIG;
            }

            SigDAOv2.getInstance().exportarDatosAsync(
                this.productoCatastralJobDAO,
                tramite,
                prediosSb.toString(),
                tramite.getTipoTramite(),
                ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_FILEGEODB,
                usuario);

        }
    }
//--------------------------------------------------------------------------------------------------    
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IConservacionLocal#buscaPrediosConsultaCatastralWebService
     */
    @Override
    public List<Predio> buscaPrediosConsultaCatastralWebService(
        FiltroConsultaCatastralWebService filtro, Long numeroPagina) throws Exception {
        LOGGER.debug("INICIA.....ConservacionBean#buscaPrediosConsultaCatastralWebService");
        List<Predio> predios = new ArrayList<Predio>();
        try {
            Object[] resultadoSP = this.sncProcedimientoDao.buscarPrediosWS(filtro, numeroPagina);

            //andres.eslava::generacion de predios.
            if (resultadoSP != null && resultadoSP[0] != null) {
                List<Object[]> objetosPredio = (List<Object[]>) resultadoSP[0];
                for (Object[] predio : objetosPredio) {

                    Predio unPredio = new Predio();
                    Departamento unDepartamento = new Departamento();
                    Municipio unMunicipio = new Municipio();
                    List<PredioDireccion> direcciones = new ArrayList<PredioDireccion>();
                    PredioDireccion predioDireccion = new PredioDireccion();

                    unPredio.setId(Long.valueOf(String.valueOf(predio[0])));
                    unPredio.setNumeroPredial(String.valueOf(predio[5]));
                    unPredio.setNumeroPredialAnterior(String.valueOf(predio[6]));
                    unPredio.setDestino(String.valueOf(predio[7]));
                    unPredio.setNumeroRegistro(String.valueOf(predio[8]));
                    unPredio.setAreaTerreno(Double.valueOf(String.valueOf(predio[9])));
                    unPredio.setAreaConstruccion(Double.valueOf(String.valueOf(predio[10])));
                    unPredio.setAvaluoCatastral(Double.valueOf(String.valueOf(predio[11])));

                    //TODO::andres.eslava::02/10/2013::direccionDebe agregarlo el SP en la ultima posicion.::andres.eslava
                    //andres.eslava::Agrega informacion de la direccion 
                    predioDireccion.setDireccion(String.valueOf(predio[12]));
                    direcciones.add(predioDireccion);
                    unPredio.setPredioDireccions(direcciones);

                    //Agrega la informacion del departamento.        
                    unDepartamento.setCodigo(String.valueOf(predio[1]));
                    unDepartamento.setNombre(String.valueOf(predio[2]));
                    unPredio.setDepartamento(unDepartamento);

                    //Agrega la informacion del municipio
                    unMunicipio.setCodigo(String.valueOf(predio[3]));
                    unMunicipio.setNombre(String.valueOf(predio[4]));
                    unPredio.setMunicipio(unMunicipio);

                    //andres.eslava::busqueda de propietarios.
                    if (resultadoSP[1] != null) {
                        List<Object[]> objetosPersona = (List<Object[]>) resultadoSP[1];
                        List<PersonaPredio> personasPredios = new ArrayList<PersonaPredio>();
                        for (Object[] persona : objetosPersona) {
                            if (Long.valueOf(String.valueOf(persona[0])).equals(unPredio.getId())) {

                                //andres.eslava entidades requeridad para asociar una persona a un predio
                                Persona unaPersona = new Persona();
                                PersonaPredio unaPersonaPredio = new PersonaPredio();

                                unaPersona.setTipoIdentificacion(String.valueOf(persona[1]));
                                unaPersona.setNumeroIdentificacion(String.valueOf(persona[2]));
                                unaPersona.setNombre(String.valueOf(String.valueOf(persona[3])));

                                unaPersonaPredio.setPredio(unPredio);
                                unaPersonaPredio.setPersona(unaPersona);

                                personasPredios.add(unaPersonaPredio);
                            }
                        }
                        unPredio.setPersonaPredios(personasPredios);
                    }

                    //andres.eslava busqueda de zonas
                    if (resultadoSP[2] != null) {
                        List<Object[]> objetosZonas = (List<Object[]>) resultadoSP[2];
                        List<PredioZona> predioZonas = new ArrayList<PredioZona>();
                        for (Object[] zona : objetosZonas) {
                            if (unPredio.getId().equals(Long.valueOf(String.valueOf(zona[0])))) {
                                PredioZona predioZona = new PredioZona();
                                predioZona.setZonaFisica(String.valueOf(zona[1]));
                                predioZona.setZonaGeoeconomica(String.valueOf(zona[2]));
                                predioZona.setArea(Double.valueOf(String.valueOf(zona[3])));
                                predioZonas.add(predioZona);
                            }
                        }
                        unPredio.setPredioZonas(predioZonas);
                    }

                    //andres.eslava busqueda de construcciones
                    if (resultadoSP[3] != null) {
                        List<Object[]> objetosConstruccion = (List<Object[]>) resultadoSP[3];
                        List<UnidadConstruccion> construcciones =
                            new ArrayList<UnidadConstruccion>();
                        for (Object[] construccion : objetosConstruccion) {
                            if (unPredio.getId().equals(Long.
                                valueOf(String.valueOf(construccion[0])))) {
                                UnidadConstruccion unaConstruccion = new UnidadConstruccion();

                                UsoConstruccion uso = new UsoConstruccion();

                                if (construccion[1] != null) {
                                    unaConstruccion.setUnidad(String.valueOf(construccion[1]));
                                }
                                if (construccion[2] != null) {
                                    unaConstruccion.setTipoConstruccion(String.valueOf(
                                        construccion[2]));
                                }
                                if (construccion[3] != null) {
                                    uso.setNombre(String.valueOf(construccion[3]));
                                    unaConstruccion.setUsoConstruccion(uso);
                                }
                                if (construccion[5] != null) {
                                    unaConstruccion.setAreaConstruida(Double.valueOf(String.valueOf(
                                        construccion[5])));
                                }
                                if (construccion[6] != null) {
                                    unaConstruccion.setTotalBanios(Integer.valueOf(String.valueOf(
                                        construccion[6])));
                                }
                                if (construccion[7] != null) {
                                    unaConstruccion.setTotalHabitaciones(Integer.valueOf(String.
                                        valueOf(construccion[7])));
                                }
                                if (construccion[8] != null) {
                                    unaConstruccion.setTotalLocales(Integer.valueOf(String.valueOf(
                                        construccion[8])));
                                }
                                if (construccion[9] != null) {
                                    unaConstruccion.setTotalPisosUnidad(Integer.valueOf(String.
                                        valueOf(construccion[9])));
                                }
                                if (construccion[10] != null) {
                                    unaConstruccion.setTotalPuntaje(Double.valueOf(String.valueOf(
                                        construccion[10])));
                                }
                                construcciones.add(unaConstruccion);
                            }
                        }
                        unPredio.setUnidadConstruccions(construcciones);
                    }
                    //andres.eslava adiciona cada predio construido con la info SP    
                    LOGGER.debug(unPredio.getNumeroPredial());

                    predios.add(unPredio);
                }//Fin For predios          
            }// Fin condicion resultado SP no nulo ni vacio
            return predios;
        } catch (Exception e) {
//TODO :: andres.eslava :: 16-09-2013 :: en los catch se usa error como nivel del logger :: pedro.garcia
            LOGGER.debug("Error en... ConservacionBean#buscaPrediosConsultaCatastralWebService");
//TODO :: andres.eslava :: 16-09-2013 :: no usar printStackTrace() :: pedro.garcia
            e.printStackTrace();
//TODO :: andres.eslava :: 16-09-2013 :: hacer manejo de errores como se definió en correo enviado :: pedro.garcia            
            throw new Exception();
        } finally {
            LOGGER.debug("FINALIZA.....ConservacionBean#buscaPrediosConsultaCatastralWebService");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Es la que hace el trabajo real de la segunda parte de la aplicación de cambios
     *
     * @author pedro.garcia
     * @param idTramite
     * @param usuario
     * @return
     */
    private boolean aplicarCambiosDeProyeccionAP2Helper(Long idTramite, UsuarioDTO usuario) {

        boolean answer;
        String message, idActividad;
        ArrayList<Actividad> listaActividades;
        Tramite tramite;

        answer = false;
        message = "";

        try {

            try {
                tramite = this.tramiteDao.buscarTramiteConResolucionSimple(idTramite);
            } catch (Exception ex) {
                message = "Error en ConservacionBean#aplicarCambiosDeProyeccionS buscando el " +
                    "trámite con id " + idTramite.toString();
                LOGGER.error(message);
                throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                    getExcepcion(LOGGER, ex, "Tramite", idTramite.toString());
            }

            //D: APLICA CAMBIOS EN LA BD
            LOGGER.debug("::::::::::::::::::::::::APLICACION CAMBIOS ALFANUMERICOS T=" + tramite.
                getId() + ":::::::::::::::::::::::::::");
            LOGGER.debug("AC USER:" + usuario.getLogin());
            LOGGER.debug("Inicia sincronización de la Base de datos Alfanumérica...");
            LOGGER.debug(
                ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
            Object mensajes[] = this.sncProcedimientoDao.confirmarProyeccion(idTramite);
            if (Utilidades.hayErrorEnEjecucionSP(mensajes)) {

                message =
                    "Ocurrió un Error al confirmar la proyección en la base de datos para el " +
                    "trámite con id " + idTramite.toString() + ": " + ErrorUtil.getError(mensajes);
                LOGGER.error(message);
                LOGGER.debug("::::::::::::::::::::::::ERROR APLICACION CAMBIOS ALFANUMERICOS T=" +
                    tramite.getId() + ":::::::::::::::::::::::::::");
                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                    getExcepcion(LOGGER, new Exception(""),
                        EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION.toString());
            }

            //D: MOVER EL PROCESO
            LOGGER.debug("::::::::::::::::::::::::APLICACION CAMBIOS PROCESS T=" + tramite.getId() +
                ":::::::::::::::::::::::::::");
            LOGGER.debug("AC USER:" + usuario.getLogin());
            LOGGER.debug(
                "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
            String processTransition = "";
            SolicitudCatastral solicitudCatastral;
            List<UsuarioDTO> usuariosActividad;

            solicitudCatastral = new SolicitudCatastral();
            usuariosActividad = new ArrayList<UsuarioDTO>();
            usuariosActividad.add(usuario);
            processTransition = ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
            solicitudCatastral.setTransicion(processTransition);
            solicitudCatastral.setUsuarios(usuariosActividad);

            try {
                listaActividades = (ArrayList<Actividad>) this.procesosService.
                    getActividadesPorIdObjetoNegocio(idTramite,
                        ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);
            } catch (ExcepcionSNC ex) {
                message = "Ocurrió un error al consultar la lista de actividades para el trámite " +
                    "con id " + idTramite;
                LOGGER.debug("::::::::::::::::::::::::ERROR APLICACION CAMBIOS ALFANUMERICOS T=" +
                    tramite.getId() + ":::::::::::::::::::::::::::");
                LOGGER.debug(message);
                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(LOGGER,
                    ex, "ConservacionBean#aplicarCambiosDeProyeccionS", "avanzarActividad");
            }

            if (listaActividades != null && !listaActividades.isEmpty() &&
                listaActividades.get(0).getId() != null &&
                !listaActividades.get(0).getId().isEmpty()) {

                idActividad = listaActividades.get(0).getId();
                try {
                    this.procesosService.avanzarActividad(usuario, idActividad, solicitudCatastral);
                } catch (Exception ex) {
                    message =
                        "Ocurrió un error en el BPM al tratar de avanzar la actividad del trámite " +
                        "con id " + idTramite;
                    LOGGER.debug(
                        "::::::::::::::::::::::::ERROR APLICACION CAMBIOS ALFANUMERICOS T=" +
                        tramite.getId() + ":::::::::::::::::::::::::::");
                    LOGGER.debug(message);
                    throw new ExcepcionSNC("Error en el BPM", ex.getMessage(), message);
                }

            } else {

                //felipe.cadena:: #8912 :: Se consulta por numero de radicacion para los tramites asociados.
                Map filtros = new HashMap<Object, Object>();
                filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION, tramite.
                    getNumeroRadicacion());
                filtros.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD,
                    ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);
                filtros.
                    put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
                List<Actividad> actividades = this.procesosService.
                    consultarListaActividades(filtros);

                if (actividades != null && !actividades.isEmpty() &&
                    actividades.get(0).getId() != null &&
                    !actividades.get(0).getId().isEmpty()) {

                    idActividad = actividades.get(0).getId();
                    try {
                        this.procesosService.avanzarActividad(usuario, idActividad,
                            solicitudCatastral);
                    } catch (Exception ex) {
                        message =
                            "Ocurrió un error en el BPM al tratar de avanzar la actividad del trámite " +
                            "con id " + idTramite;
                        LOGGER.debug(
                            "::::::::::::::::::::::::ERROR APLICACION CAMBIOS ALFANUMERICOS T=" +
                            tramite.getId() + ":::::::::::::::::::::::::::");
                        LOGGER.debug(message);
                        throw new ExcepcionSNC("Error en el BPM", ex.getMessage(), message);
                    }

                } else {

                    message =
                        "No se encontró la instancia de actividad correspondiente al trámite " +
                        idTramite + " en la actividad " +
                        ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
                    LOGGER.debug(
                        "::::::::::::::::::::::::ERROR APLICACION CAMBIOS ALFANUMERICOS T=" +
                        tramite.getId() + ":::::::::::::::::::::::::::");
                    LOGGER.debug(message);
                    throw new ExcepcionSNC("No se encontró la actividad del trámite", "Error",
                        message);
                }
            }

            //D: ACTUALIZAR EL TRAMITE Y LA RESOLUCION
            // Cuando el trámite es de quinta actualizar el predio del trámite
            if (tramite.isQuinta() || tramite.isQuintaNuevo() || tramite.isQuintaOmitido() ||
                tramite.isQuintaOmitidoNuevo()) {

                List<PPredio> prediosDelTramite = this.conservacionService
                    .buscarPPrediosPorTramiteId(tramite.getId());
                if (prediosDelTramite != null && !prediosDelTramite.isEmpty()) {
                    Predio predioSeleccionado = this.conservacionService.
                        obtenerPredioPorId(prediosDelTramite.get(0).getId());
                    if (predioSeleccionado != null) {
                        tramite.setPredio(predioSeleccionado);
                        tramite.setPredioId(predioSeleccionado.getId());
                    }
                }
            }

            LOGGER.debug("::::::::::::::::::::::::APLICACION CAMBIOS ACTUALIZACION DATOS T=" +
                tramite.getId() + ":::::::::::::::::::::::::::");
            LOGGER.debug("AC USER:" + usuario.getLogin());
            LOGGER.debug(
                "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
            //D: se actualiza en trámite
            tramite.setEstado(ETramiteEstado.FINALIZADO_APROBADO.getCodigo());
            tramite.setUsuarioLog(usuario.getLogin());
            tramite.setFechaLog(new Date());

            tramite.getResultadoDocumento().setEstado(EDocumentoEstado.RESOLUCION_EN_FIRME.
                getCodigo());

            tramite = this.tramiteService.guardarActualizarTramite2(tramite);

            Documento docResp;
            docResp = this.tramiteService.guardarYactualizarDocumento(tramite.
                getResultadoDocumento());
            if (docResp == null) {
                message = "Error en la actualización del documento resultado del trámite con id " +
                    tramite.getId().toString();
                throw new ExcepcionSNC("Error en Documento", "Error", message);
            }

            //Se hace el llamado para actualizar la ficha predial digital
            boolean fichaOK = false;
            fichaOK = this.actualizarFichaPredialDigital(tramite.getId(), usuario, true);
            if (!fichaOK) {
                message =
                    "Ocurrió un error en la generación o actualización de la ficha predial digital" +
                    " del trámite con id " + tramite.getId().toString();
                throw new ExcepcionSNC("Error en la ficha predial digital", "Error", message);
            }

            //Se hace el llamado para actualizar la carta catastral urbana
            if (tramite.isTramiteGeografico()) {
                fichaOK = this.actualizarCartaCatastralUrbana(tramite.getId(), usuario, true);
                if (!fichaOK) {
                    message =
                        "Ocurrió un error en la generación o actualización de la carta catastral urbana" +
                        " del trámite con id " + tramite.getId().toString();
                    throw new ExcepcionSNC("Error en la carta catastral urbana", "Error", message);
                }
            }

            answer = true;

        } catch (ExcepcionSNC ex) {
            ErrorUtil.notificarErrorPorCorreoElectronico(ex, usuario.getLogin(), idTramite, message);
        }

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see
     * IConservacionLocal#aplicarCambiosDeProyeccionAP2(co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob)
     * @author pedro.garcia
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean aplicarCambiosDeProyeccionAP2(ProductoCatastralJob registroTrabajoFinalizado) {

        LOGGER.debug("En ConservacionBean#aplicarCambiosDeProyeccionAP2 ...");

        boolean answer;
        UsuarioDTO usuario = null;
        String loginUsuario;
        Long idTramite;
        Actividad actividadTramite = new Actividad();
        boolean isDepuracion;

        answer = true;

        loginUsuario = registroTrabajoFinalizado.getLoginUsuarioDeCodigo();
        idTramite = registroTrabajoFinalizado.getIdTramiteUsuarioDeCodigo();
        //Se obtienen los datos de la actividad inicial
        actividadTramite.setNombre(registroTrabajoFinalizado.getNombreActividadDeCodigo());
        actividadTramite.setId(registroTrabajoFinalizado.getTaskIdProcesoDeCodigo());

        Tramite tramite = this.tramiteDao.buscarTramiteConResolucionSimple(idTramite);
        tramite.setActividadActualTramite(actividadTramite);

        //N: no estoy muy de acuerdo con mezclar los beans de fachada, pero otros ya lo hicieron.
        usuario = this.generalesService.getCacheUsuario(loginUsuario);

        //felipe.cadena::se determina la actividad de procedencia basado en la actividad que tiene el job.
        if (actividadTramite.getNombre() != null) {
            LOGGER.debug("Evita la consulta de process TID:" + idTramite);
            isDepuracion = actividadTramite.getNombre().startsWith(Constantes.DEPURACION);
        } else {
            LOGGER.debug(
                "Consulta la actividad en process para determinar proceso de inicio TID: " +
                idTramite);
            isDepuracion = this.determinarSiEsDepuracion(idTramite);
        }
        if (isDepuracion) {
            try {
                this.aplicarCambios.aplicarCambiosDepuracion(tramite, usuario, 2);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
            }
        } else {
            try {
                this.aplicarCambios.aplicarCambiosConservacion(tramite, usuario, 1);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
            } finally {
                //this.productoCatastralJobDAO.update(registroTrabajoFinalizado);

            }
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Determina si el tramite para aplicar cambios es de depuración
     *
     * @author felipe.cadena
     * @param idTramite
     * @return
     */
    private boolean determinarSiEsDepuracion(Long idTramite) {

        List<Actividad> actividades = this.procesosService.getActividadesPorIdObjetoNegocio(
            idTramite);

        if (actividades != null && !actividades.isEmpty()) {
            Actividad act = actividades.get(0);
            if (act.getNombre().equals(
                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) ||
                act.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) ||
                act.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {
                return true;
            } else {
                return false;
            }
        } else {
            LOGGER.error("La lista de actividades para el trámite " + idTramite.toString() +
                " resultó nula o vacía");
            return false;
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     *
     *
     * @see IConservacionLocal#guardarDocumentoReplica
     * @author felipe.cadena
     */
    @Override
    public boolean guardarDocumentoReplica(ProductoCatastralJob job) {
        LOGGER.debug("guardarDocumentoReplica");
        try {

            this.aplicarCambios.generarReplicaEdicion(null, null, 3, job);
            return true;

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje());
            return false;

        }

    }

//--------------------------------------------------------------------------------------------------    
    /**
     *
     *
     * @see IConservacionLocal#guardarDocumentoReplica
     * @author felipe.cadena
     */
    @Override
    public boolean guardarDocumentoReplicaConsulta(ProductoCatastralJob job) {
        LOGGER.debug("guardarDocumentoReplicaConsulta");
        try {

            this.aplicarCambios.generarReplicaConsulta(null, null, null, 3, job);
            return true;

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje());
            return false;

        }

    }

    //--------------------------------------------------------------------------------------------------    
    /**
     *
     *
     * @see IConservacionLocal#reenviarJobGeneracionReplica
     * @author felipe.cadena
     */
    @Override
    public void reenviarJobGeneracionReplica(ProductoCatastralJob job) {

        Long tramiteId = null;
        String usuariologin = null;

        // recupero los datos del job
        Map<String, String> paramsCodigoJob = this.obtenerParametrosXMLJob(job.getCodigo(),
            "//parameters/parameter");

        tramiteId = Long.valueOf(paramsCodigoJob.get("numeroTramite"));
        usuariologin = paramsCodigoJob.get("usuario");

        try {

            Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(tramiteId);
            UsuarioDTO usuario = this.generalesService.getCacheUsuario(usuariologin);

            StringBuilder codigosPredios = new StringBuilder("");

            if (tramite.isQuinta()) {
                List<PPredio> prediosResultantes = this.conservacionService.
                    buscarPprediosResultantesPorTramiteId(tramite.getId());
                if (prediosResultantes.size() > 0) {

                    codigosPredios.append(prediosResultantes.get(0).getNumeroPredial()).append(",");
                }
            } else {
                List<Predio> predios = tramite.getPredios();

                for (Predio predio : predios) {
                    if (!((EPredioCondicionPropiedad.CP_5.getCodigo().equals(predio.
                        getCondicionPropiedad()) ||
                        EPredioCondicionPropiedad.CP_6.getCodigo().equals(predio.
                            getCondicionPropiedad())))) {
                        codigosPredios.append(predio.getNumeroPredial()).append(",");
                    }
                }
            }
            LOGGER.debug("prediosSb:" + codigosPredios.toString());

            SigDAOv2.getInstance().exportarDatosAsync(
                this.productoCatastralJobDAO,
                tramite,
                codigosPredios.toString(),
                tramite.getTipoTramite(), ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_FILEGEODB,
                usuario);

        } catch (ExcepcionSNC e) {
            LOGGER.error("error en ConservacionBean#reenviarJobGeneracionReplica: " +
                e.getMensaje());

        }
    }

    //-------------------------------------------------------------------------------------------------    
    /**
     * Recupera los parametros XML del job
     *
     * @author felipe.cadena
     * @param campoXML
     * @param path
     * @return
     */
    private Map<String, String> obtenerParametrosXMLJob(String campoXML, String path) {

        Map<String, String> params = new HashMap<String, String>();
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(new ByteArrayInputStream(campoXML.getBytes("UTF-8")));

            XPathFactory factory = XPathFactory.newInstance();

            XPath xpath = factory.newXPath();
            XPathExpression expr = xpath.compile(path + "/name | " + path + "/Name");

            Object result = expr.evaluate(doc, XPathConstants.NODESET);

            NodeList nodesName = (NodeList) result;

            expr = xpath.compile(path + "/value | " + path + "/Value");

            result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodesValue = (NodeList) result;

            for (int i = 0; i < nodesName.getLength(); i++) {
                params.put(nodesName.item(i).getTextContent(), nodesValue.item(i).getTextContent());

            }

        } catch (Exception e) {
            LOGGER.error("error en ConservacionBean#obtenerParametrosXMLJob: " +
                e.getMessage());
        }
        return params;
    }
    //-------------------------------------------------------------------------------------------------

    // ------------------------------------------------ //
    /**
     *
     * @see IFormacionLocal#recalcularAvaluosDecretoParaUnPredio(Long tramiteId, Long predioId, Date
     * fechaDelCalculo)
     * @author javier.aponte
     */
    public Object[] recalcularAvaluosDecretoParaUnPredio(Long tramiteId,
        Long predioId, Date fechaDelCalculo, UsuarioDTO usuario) {
        Object[] errors = null;
        try {
            errors = sncProcedimientoDao.reproyectarAvaluos(tramiteId,
                predioId, fechaDelCalculo, usuario);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IConservacionLocal#generarReportePredialDatosBasicos(FiltroGenerarReportes, UsuarioDTO)
     * @author javier.aponte
     * @modified by leidy.gonzalez
     */
    @Override
    public Object[] generarReportePredialDatosBasicos(RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            answer = sncProcedimientoDao.generarReportePredialDatosBasicos(repReporteEjecucion);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacionLocal#buscarReporteControlReportePorUsuario(String)
     * @author javier.aponte
     */
    @Override
    public List<RepControlReporte> buscarReporteControlReportePorUsuario(String usuarioLogin) {

        List<RepControlReporte> answer = null;
        try {
            answer = this.reporteControlReporteDao.buscarReporteControlReportePorUsuario(
                usuarioLogin);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar reporteControlPorUsuario" + e.getMessage());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#contarRegistrosReporteDatosReportePorControlReporteId(Long )
     * @author javier.aponte
     */
    @Override
    public Integer contarRegistrosReporteDatosReportePorControlReporteId(Long idControlReporte) {

        Integer numeroRegistros = null;
        try {
            numeroRegistros = this.reporteDatosReporteDao.
                contarRegistrosReporteDatosReportePorControlReporteId(idControlReporte);
        } catch (Exception e) {
            LOGGER.debug(
                "Ocurrió un error al consultar contarRegistrosReporteDatosReportePorControlReporteId" +
                e.getMessage());
        }
        return numeroRegistros;
    }
    //--------------------------------------------------------------------------------------------------    

    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IConservacionLocal#procesarFichaPredialDigital
     * @author javier.aponte
     * @modified by leidy.gonzalez 25/08/2014 Incidencia: 9215 Se elimina envi� de correo a el
     * usuario logueado cuando se genera una inconsistencia al procesar la ficha predial digital
     */
    /*
     * @modified by leidy.gonzalez:: 19/05/2016 :: #15922 :: Se ajusta reenvio de job para ficha
     * predial cuando falle la generacion de tal.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public String procesarFichaPredialDigital(ProductoCatastralJob job, int contador) {

        LOGGER.debug("procesarFichaPredialDigital");
        String answer = ProductoCatastralJob.SNC_TERMINADO.toString();

        Parametro maxEnvioJobFichaPredial = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_FICHA_Y_CARTA_CATASTRAL.toString());

        //v1.1.9
        List<HPredio> prediosHistoricos = null;
        try {
            contador++;

            UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());
            IDocumentosService documentosService = DocumentalServiceFactory.getService();

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService.
                buscarEstructuraOrganizacionalPorCodigo(usuario.getCodigoEstructuraOrganizacional());

            String urlReporte = EReporteServiceSNC.FICHA_PREDIAL_DIGITAL.getUrlReporte();
            Reporte reporte = new Reporte(usuario, estructuraOrganizacional);
            reporte.setUrl(urlReporte);

            reporte.setEstructuraOrganizacional(estructuraOrganizacional);

            List<ProductoCatastralResultado> resultadosFPD = job.
                obtenerResultadosParaProductosCatatrales();
            if (resultadosFPD == null || resultadosFPD.isEmpty()) {
                String message = "El servidor de productos no retornó resultados.";
                throw SncBusinessServiceExceptions.EXCEPCION_100018.getExcepcion(LOGGER, null,
                    message);
            }

            List<String> numerosPrediales = job.obtenerParametroNumerosPrediales();
            for (String numeroPredial : numerosPrediales) {
                String publicUrlFPD = null;
                String publicUrlMPD = null;
                String escalaFichaPredialDigital = null;

                for (ProductoCatastralResultado resultado : resultadosFPD) {
                    if (resultado.getCodigo().equals(numeroPredial)) {
                        if (resultado.getTipoProducto().equals(
                            ProductoCatastralJob.TIPO_IMAGEN_FPD_PRINCIPAL)) {
                            //se mueve el documento de una ruta privada del gestor documental a una publica para poder verlo por http (cambio en la arquitectura documental 24/06/2013)
                            DocumentoVO documentoVO = documentosService.
                                cargarDocumentoPrivadoEnWorkspacePreview(resultado.getIdArchivo());
                            publicUrlFPD = documentoVO.getUrlPublico();
                            escalaFichaPredialDigital = resultado.getEscala();
                            continue;
                        }
                        if (resultado.getTipoProducto().equals(
                            ProductoCatastralJob.TIPO_IMAGEN_FPD_MANZANA)) {
                            //se mueve el documento de una ruta privada del gestor documental a una publica para poder verlo por http (cambio en la arquitectura documental 24/06/2013)
                            DocumentoVO documentoVO = documentosService.
                                cargarDocumentoPrivadoEnWorkspacePreview(resultado.getIdArchivo());
                            publicUrlMPD = documentoVO.getUrlPublico();
                            continue;
                        }
                    }
                }
                LOGGER.debug("publicUrlFPD: " + publicUrlFPD);
                LOGGER.debug("publicUrlMPD: " + publicUrlMPD);

                Predio predio = this.predioDao.getPredioByNumeroPredial(numeroPredial);
                List<Foto> fotosFachadaPredio = this.fotoDao.buscarFotografiaPorIdPredioAndFotoTipo(
                    predio.getId(), EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo());

                String urlWebFotoFachada = null;
                if (fotosFachadaPredio != null && !fotosFachadaPredio.isEmpty() &&
                    fotosFachadaPredio.get(0).getDocumento() != null) {
                    DocumentoVO documentoVO = documentosService.
                        cargarDocumentoPrivadoEnWorkspacePreview(fotosFachadaPredio.get(0).
                            getDocumento().getIdRepositorioDocumentos());
                    urlWebFotoFachada = documentoVO.getUrlPublico();
                }

                reporte.addParameter("ESCALA", escalaFichaPredialDigital);
                reporte.addParameter("FOTO_LOCALIZACION_GEOGRAFICA", publicUrlFPD);
                reporte.addParameter("FOTO_LOCALIZACION_MANZANA", publicUrlMPD);
                if (urlWebFotoFachada != null) {
                    reporte.addParameter("FOTO_FACHADA", urlWebFotoFachada);
                }
                reporte.addParameter("COPIA_USO_RESTRINGIDO", "true");

                Tramite tramite = null;
                Long tramiteId = job.getTramiteId();
                if (tramiteId != null) {
                    tramite = this.tramiteService.buscarTramiteConResolucionSimple(tramiteId);
                }
                if (tramite != null) {
                    if (!tramite.isSegundaDesenglobe()) {

                        if (tramite.getPredio() != null && tramite.getPredio().isMejora()) {
                            this.generarArchivoFichaPredialDigitalPorPredio(tramite.getPredio().
                                getNumeroPredial(), usuario, reporte);
                        } else {
                            this.generarArchivoFichaPredialDigitalPorPredio(predio.
                                getNumeroPredial(), usuario, reporte);
                        }

                    } else if (tramite.isSegundaDesenglobe()) {

                        if (predio.isCondicionPropiedadRara()) {
                            prediosHistoricos = this.hPredioDao.
                                buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(
                                    tramite.getId());

                            if (prediosHistoricos != null && !prediosHistoricos.isEmpty()) {
                                for (HPredio predioTemp : prediosHistoricos) {
                                    if (!predioTemp.getNumeroPredial().endsWith("00000000")) {
                                        this.crearRegistroProductoCatastralJobFPD(job.
                                            getUsuarioLog(), predioTemp.getNumeroPredial(),
                                            tramiteId, reporte);
                                    }
                                }
                            }
                        } else {
                            if (tramite.getPredio() != null && tramite.getPredio().isMejora()) {
                                this.generarArchivoFichaPredialDigitalPorPredio(tramite.getPredio().
                                    getNumeroPredial(), usuario, reporte);
                            } else {
                                this.generarArchivoFichaPredialDigitalPorPredio(predio.
                                    getNumeroPredial(), usuario, reporte);
                            }
                        }
                    }
                } else {
                    this.generarArchivoFichaPredialDigitalPorPredio(predio.getNumeroPredial(),
                        usuario, reporte);
                }
            }
        } catch (ExcepcionSNC e) {

            LOGGER.debug("ProcesarFichaPredialDigital:" + e.getMensaje(), e);
            answer = e.getMensaje();

            //Se reenvia job de ficha para garantizar que cuendo llegue a actividades posteriores
            //al menos un 80, 90% de fichas se hayan geenrado
            if (contador <= maxEnvioJobFichaPredial.getValorNumero()) {
                this.procesarFichaPredialDigital(job, contador);
                this.productoCatastralJobDAO.update(job);
            }

        } catch (Exception ex) {

            LOGGER.debug("ProcesarFichaPredialDigital:" + ex.getMessage(), ex);
            answer = ex.getMessage();

            //Se reenvia job de ficha para garantizar que cuendo llegue a actividades posteriores
            //al menos un 80, 90% de fichas se hayan geenrado
            if (contador <= maxEnvioJobFichaPredial.getValorNumero()) {
                this.procesarFichaPredialDigital(job, contador);
                this.productoCatastralJobDAO.update(job);
            }

        }
        return answer;
    }

    /**
     * Método para generar el archivo de la ficha predial digital por predio
     *
     * @author javier.aponte
     */
    private void generarArchivoFichaPredialDigitalPorPredio(String numeroPredial, UsuarioDTO usuario,
        Reporte reporte) {

        Predio predio;
        Documento documento;
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());
        File archivoFichaPredialDigital = null;
        IReporteService reportesService = ReporteServiceFactory.getService();

        predio = this.predioDao.getPredioByNumeroPredial(numeroPredial);

        if (predio != null) {
            documento = this.documentoDao.buscarDocumentoPorPredioIdAndTipoDocumento(predio.getId(),
                ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());

            if (documento == null) {
                documento = new Documento();
                documento.setTipoDocumento(tipoDocumento);
                documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                documento.setBloqueado(ESiNo.NO.getCodigo());
            }

            reporte.addParameter("NUMERO_PREDIAL", numeroPredial);

            archivoFichaPredialDigital = reportesService.getReportAsFile(reporte,
                new LogMensajesReportesUtil(this.generalesService, usuario));

            if (archivoFichaPredialDigital != null) {
                LOGGER.debug(
                    "TareaGenerarFichaPredialDigitalAction: Reporte ficha predial digital generado exitosamente" +
                    archivoFichaPredialDigital.getAbsolutePath());

                documento.setArchivo(archivoFichaPredialDigital.getPath());
                documento.setUsuarioCreador(usuario.getLogin());
                documento.setUsuarioLog(usuario.getLogin());
                documento.setFechaLog(new Date(System.currentTimeMillis()));
                documento.
                    setEstructuraOrganizacionalCod(usuario.getCodigoEstructuraOrganizacional());
                documento.setPredioId(predio.getId());

                // Se sube el archivo al gestor documental y se actualiza el documento
                documento = this.conservacionService.guardarFichaPredialGenerada(documento, usuario);

                if (documento == null || documento.getIdRepositorioDocumentos() == null ||
                    documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                    LOGGER.error(
                        "Ocurrió un error al guardar la ficha predial en el sistema de gestión documental");
                } else {
                    LOGGER.debug("ProcesarFichaPredialDigital: La ficha predial del predio: " +
                        numeroPredial + " se generó exitosamente");
                }
            }
        } else {
            LOGGER.debug("ProcesarFichaPredialDigital: Ocurrió un error al consultar el predio: " +
                numeroPredial + " en la base de datos");
        }

    }

    /**
     * Método encargado de crear un nuevo registro en la tabla producto catastral job, estos jobs
     * están en espera de que sean ejecutados en jasperserver
     *
     * @author javier.aponte
     * @param job productoCatastralJob
     * @param numeroPredial
     * @param tramiteId
     */
    private void crearRegistroProductoCatastralJobFPD(String usuario, String numeroPredial,
        Long tramiteId, Reporte reporte) {

        ProductoCatastralJob pcjTemp = new ProductoCatastralJob();

        pcjTemp.setCodigo(numeroPredial + Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO + String.
            valueOf(tramiteId));
        pcjTemp.setEstado(ProductoCatastralJob.JPS_ESPERANDO);
        pcjTemp.setFechaLog(new Date());
        pcjTemp.setTipoProducto(ProductoCatastralJob.SIG_JOB_IMAGEN_FICHA_PREDIAL_DIGITAL);
        pcjTemp.setUsuarioLog(usuario);
        pcjTemp.setTramiteId(tramiteId);

        Map<String, String> parametros = reporte.getParams();

        StringBuilder resultado = new StringBuilder();
        resultado.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        resultado.append("<resultado>");
        resultado.append("<parametro ");
        resultado.append("escala=\"" + parametros.get("ESCALA") + "\" ");
        resultado.append("fotoLocalizacionGeografica=\"" + parametros.get(
            "FOTO_LOCALIZACION_GEOGRAFICA") + "\" ");
        resultado.append(
            "fotoLocalizacionManzana=\"" + parametros.get("FOTO_LOCALIZACION_MANZANA") + "\" ");
        if (parametros.get("FOTO_FACHADA") != null) {
            resultado.append("fotoFachada=\"" + parametros.get("FOTO_FACHADA") + "\" ");
        } else {
            resultado.append("fotoFachada=\"\" ");
        }
        resultado.append("copiaUsoRestringido=\"" + parametros.get("COPIA_USO_RESTRINGIDO") + "\">");
        resultado.append("</parametro>");
        resultado.append("</resultado>");

        pcjTemp.setResultado(resultado.toString());

        this.productoCatastralJobDAO.update(pcjTemp);

    }

    /**
     *
     *
     * @see IConservacionLocal#procesarFichaPredialDigital
     * @author javier.aponte
     * @modified by leidy.gonzalez 25/08/2014 Incidencia: 9215 Se elimina envi� de correo a el
     * usuario logueado cuando se genera una inconsistencia al procesar la carta catastral urbana
     */
    /*
     * @modified by leidy.gonzalez:: 19/05/2016 :: #15922 :: Se ajusta reenvio de job para carta
     * catastral cuando falle la generacion de tal.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public String procesarCartaCatastralUrbana(ProductoCatastralJob job, int contador) {
        LOGGER.debug("procesarCartaCatastralUrbana");
        String answer = ProductoCatastralJob.SNC_TERMINADO.toString();
        Parametro maxEnvioJobCartaCatastral = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_FICHA_Y_CARTA_CATASTRAL.toString());

        try {
            contador++;

            UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

            try {
                String numManzana = job.obtenerParametroCodigoManzana().get(0);
                LOGGER.debug("numManzana: " + numManzana);

                Predio predio = this.predioDao.obtenerPrimerPredioPorNumeroManzana(numManzana);

                Documento documento = this.documentoDao.
                    buscarDocumentoPorNumeroPredialAndTipoDocumento(predio.getNumeroPredial().
                        substring(0, 17),
                        ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

                List<ProductoCatastralResultado> resultadoCartaCatastralUrbana = job.
                    obtenerResultadosParaProductosCatatrales();
                if (resultadoCartaCatastralUrbana == null || resultadoCartaCatastralUrbana.isEmpty()) {
                    String message = "El servidor de productos no retornó resultados.";
                    throw SncBusinessServiceExceptions.EXCEPCION_100018.getExcepcion(LOGGER, null,
                        message);
                }

                if (documento == null) {
                    documento = new Documento();
                    documento.setTipoDocumento(tipoDocumento);
                    documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                    documento.setBloqueado(ESiNo.NO.getCodigo());
                }

                String rutaCarpetaTemporalImagenCCU = this.generalesService.
                    descargarArchivoDeAlfrescoATemp(resultadoCartaCatastralUrbana.get(0).
                        getIdArchivo());

                documento.setArchivo(rutaCarpetaTemporalImagenCCU);
                documento.setUsuarioCreador(usuario.getLogin());
                documento.setUsuarioLog(usuario.getLogin());
                documento.setFechaLog(new Date(System.currentTimeMillis()));
                documento.
                    setEstructuraOrganizacionalCod(usuario.getCodigoEstructuraOrganizacional());
                documento.setPredioId(predio.getId());
                documento.setNumeroPredial(predio.getNumeroPredial().substring(0, 17) +
                    "0000000000000");

                // Subo el archivo a Alfresco y actualizo el documento
                documento = this.conservacionService.guardarFichaPredialGenerada(documento, usuario);

                if (documento == null || documento.getIdRepositorioDocumentos() == null ||
                    documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                    String causa =
                        "Ocurrió un error al guardar la carta catastral urbana en el sistema de gestión documental";
                    answer = causa;
                    LOGGER.error(causa);
                } else {
                    LOGGER.debug(
                        "TareaGenerarCartaCatastralUrbanaAction: La carta catastral urbana del predio: " +
                        numManzana + " se generó exitosamente");
                }
            } catch (ExcepcionSNC e) {
                LOGGER.error(e.getMessage(), e);
                answer = e.getMessage();

                //Se reenvia job de carta para garantizar que cuendo llegue a actividades posteriores
                //al menos un 80, 90% de cartas se hayan geenrado
                if (contador <= maxEnvioJobCartaCatastral.getValorNumero()) {
                    this.procesarCartaCatastralUrbana(job, contador);
                }
            }

        } catch (ExcepcionSNC e) {

            //Se reenvia job de carta para garantizar que cuendo llegue a actividades posteriores
            //al menos un 80, 90% de cartas se hayan geenrado
            if (contador <= maxEnvioJobCartaCatastral.getValorNumero()) {
                this.procesarCartaCatastralUrbana(job, contador);
            }

            LOGGER.error(e.getMensaje());
            answer = e.getMensaje();
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------    
    /**
     *
     *
     * @see IConservacionLocal#guardarPlanoPredialRural(String, String, String)
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean guardarPlanoPredialRural(String rutaArchivo, String numeroManzana,
        String usuarioLogin) {
        LOGGER.debug("guardarPlanoPredialRural");
        boolean resultadoOk = true;
        try {
            UsuarioDTO usuario = this.generalesService.getCacheUsuario(usuarioLogin);

            Predio predio;
            Documento documento;

            TipoDocumento tipoDocumento = new TipoDocumento();
            //El plano predial rural lo guardamos como carta catatral urbana
            tipoDocumento.setId(ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

            String rutaCarpetaTemporalImagenCCU;

            try {

                String numManzana = numeroManzana;
                LOGGER.debug("numerosPrediales: " + numManzana);

                predio = this.predioDao.obtenerPrimerPredioPorNumeroManzana(numManzana);

                documento = this.documentoDao.buscarDocumentoPorNumeroPredialAndTipoDocumento(
                    predio.getNumeroPredial().substring(0, 17),
                    ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

                if (documento == null) {
                    documento = new Documento();
                    documento.setTipoDocumento(tipoDocumento);
                    documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                    documento.setBloqueado(ESiNo.NO.getCodigo());
                }

                rutaCarpetaTemporalImagenCCU = rutaArchivo;

                documento.setArchivo(rutaCarpetaTemporalImagenCCU);
                documento.setUsuarioCreador(usuario.getLogin());
                documento.setUsuarioLog(usuario.getLogin());
                documento.setFechaLog(new Date(System.currentTimeMillis()));
                documento.
                    setEstructuraOrganizacionalCod(usuario.getCodigoEstructuraOrganizacional());
                documento.setPredioId(predio.getId());
                documento.setNumeroPredial(predio.getNumeroPredial().substring(0, 17) +
                    "0000000000000");

                // Subo el archivo a Alfresco y actualizo el documento
                documento = this.conservacionService.guardarFichaPredialGenerada(documento, usuario);

                if (documento == null || documento.getIdRepositorioDocumentos() == null ||
                    documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                    String causa =
                        "Ocurrió un error al guardar el plano predial rural en el sistema de gestión documental";
                    LOGGER.error(causa);
                    ErrorUtil.notificarErrorPorCorreoElectronico(null, usuario.getLogin(), null,
                        causa);
                } else {
                    LOGGER.debug(
                        "TareaGenerarCartaCatastralUrbanaAction: El plano predial rural del predio: " +
                        numManzana + " se generó exitosamente");
                }
            } catch (ExcepcionSNC e) {
                LOGGER.error(e.getMessage(), e);
                ErrorUtil.notificarErrorPorCorreoElectronico(e, usuario.getLogin(), null, e.
                    getMessage());
                resultadoOk = false;
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje());
            resultadoOk = false;
        }
        return resultadoOk;
    }

    /**
     * @see IConservacionLocal#obtenerPrediosPorNumerosPrediales(java.util.List)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> obtenerPrediosPorNumerosPrediales(List<String> numeros) {
        List<Predio> resultado = null;

        try {
            resultado = this.predioDao.obtenerPrediosPorNumerosPrediales(numeros);

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje());
        }

        return resultado;
    }

    //--------------------------------------------------------------------------//
    /**
     * @see IConservacionLocal#buscarPrediosCompletosPorTramiteId(Long)
     * @author david.cifuentes
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Predio> buscarPrediosCompletosPorTramiteId(Long tramiteId) {

        List<Predio> answer = null;
        try {
            answer = this.tramiteDao.buscarPrediosPorTramiteId(tramiteId);
            if (answer != null && !answer.isEmpty()) {
                List<Predio> predios = new ArrayList<Predio>();
                for (Predio p : answer) {
                    p = this.predioDao.buscarPredioCompletoPorPredioId(p
                        .getId());
                    if (p != null) {
                        predios.add(p);
                    }
                }
                if (!predios.isEmpty()) {
                    answer = predios;
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPrediosCompletosPorTramiteId: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * @see
     * IConservacionLocal#finalizarCambiosDepuracion(co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public UsuarioDTO finalizarCambiosDepuracion(ProductoCatastralJob registroTrabajoFinalizado) {

        LOGGER.debug("En ConservacionBean#aplicarCambiosDeProyeccionAP2 ...");

        String message;
        UsuarioDTO usuario;
        String loginUsuario;
        Long idTramite;
        Tramite tramite;

        loginUsuario = registroTrabajoFinalizado.getLoginUsuarioDeCodigo();
        idTramite = registroTrabajoFinalizado.getIdTramiteUsuarioDeCodigo();

        usuario = this.generalesService.getCacheUsuario(loginUsuario);
        tramite = this.tramiteDao.buscarTramiteConResolucionSimple(idTramite);

        //-----mover el proceso----------//
        List<Actividad> listaActividades;
        Actividad actividadTramite;

        String processTransition = "";
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuariosActividad = new ArrayList<UsuarioDTO>();

        //obtener actividad
        try {
            listaActividades = (ArrayList<Actividad>) this.procesosService.
                getActividadesPorIdObjetoNegocio(idTramite);
        } catch (ExcepcionSNC ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(LOGGER,
                ex, "ConservacionBean#finalizarCambiosDepuracion", "avanzarActividad");
        }

        if (listaActividades != null && !listaActividades.isEmpty() &&
            listaActividades.get(0).getId() != null &&
            !listaActividades.get(0).getId().isEmpty()) {

            //determinar actividad de procedencia de la depuración
            actividadTramite = listaActividades.get(0);
            String actividadPrevia = actividadTramite.getObservacion();

            if (actividadPrevia.equals(
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE)) {
                usuariosActividad = this.tramiteService.buscarFuncionariosPorRolYTerritorial(
                    usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);
            } else {
                usuariosActividad = new ArrayList<UsuarioDTO>();
                usuariosActividad.add(this.generalesService.
                    getCacheUsuario(tramite.getFuncionarioEjecutor()));
            }

            processTransition = actividadPrevia;
            solicitudCatastral.setUsuarios(usuariosActividad);
            solicitudCatastral.setTransicion(processTransition);

            //avanzar la actividad
            try {
                this.procesosService.avanzarActividad(usuario, actividadTramite.getId(),
                    solicitudCatastral);
            } catch (Exception ex) {
                message =
                    "Ocurrió un error en el BPM al tratar de avanzar la actividad del trámite " +
                    "con id " + idTramite;
                throw new ExcepcionSNC("Error en el BPM", ex.getMessage(), message);
            }

        } else {
            message = "No se encontró la instancia de actividad correspondiente al trámite " +
                idTramite + " en la actividad " +
                ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
            throw new ExcepcionSNC("No se encontró la actividad del trámite", "Error", message);
        }

        //---------Borrar documentos replicas geograficas-------------//
        List<TramiteDocumento> docsTramite = this.tramiteService.
            obtenerTramiteDocumentosDeTramitePorTipoDoc(idTramite,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());
        docsTramite.addAll(this.tramiteService.
            obtenerTramiteDocumentosDeTramitePorTipoDoc(idTramite,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId()));

        for (TramiteDocumento tramiteDocumento : docsTramite) {
            this.tramiteService.eliminarTramiteDocumentoConDocumentoAsociado(tramiteDocumento);
        }

        return usuariosActividad.get(0);

    }

    /**
     * Método encargado de cambiar el tipo de documento de la copia de bd geográfica original y de
     * la final por copia de bd geográfica original depuración y final depuración
     *
     * @author javier.aponte
     * @param idTramite
     * @param usuarioActividad
     */
    @Override
    public void cambiarTipoDocumentoCopiaBdGeografica(Long idTramite) {

        Documento documentoTemp;

        TipoDocumento tdCopiaBDOriginalDepuracion = this.tipoDocumentoDao.findById(
            ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL_DEPURACION.getId());
        TipoDocumento tdCopiaBDFinalDepuracion = this.tipoDocumentoDao.findById(
            ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL_DEPURACION.getId());

        List<TramiteDocumento> copiasBDGeograficaOriginal = this.tramiteService.
            obtenerTramiteDocumentosDeTramitePorTipoDoc(idTramite,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());
        if (copiasBDGeograficaOriginal == null || copiasBDGeograficaOriginal.isEmpty()) {
            LOGGER.error("Ocurrió un error grave no existe la replica original");
            return;
        }
        List<TramiteDocumento> copiasBDGeograficaOriginalDepuracion = this.tramiteService.
            obtenerTramiteDocumentosDeTramitePorTipoDoc(idTramite,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL_DEPURACION.getId());
        String ultimoRegistroCopiaBDGeograficaOriginalDepuracion;
        int indiceUltimoRegistroCopiaBDGeograficaOriginalDepuracion;

        List<TramiteDocumento> copiasBDGeograficaFinal = this.tramiteService.
            obtenerTramiteDocumentosDeTramitePorTipoDoc(idTramite,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());
        if (copiasBDGeograficaFinal == null || copiasBDGeograficaFinal.isEmpty()) {
            LOGGER.error("Ocurrió un error grave no existe la replica final");
            return;
        }
        List<TramiteDocumento> copiasBDGeograficaFinalDepuracion = this.tramiteService.
            obtenerTramiteDocumentosDeTramitePorTipoDoc(idTramite,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL_DEPURACION.getId());
        String ultimoRegistroCopiaBDGeograficaFinalDepuracion;
        int indiceUltimoRegistroCopiaBDGeograficaFinalDepuracion;

        int indiceTemp = 0;
        if (copiasBDGeograficaOriginalDepuracion != null && !copiasBDGeograficaOriginalDepuracion.
            isEmpty()) {
            for (TramiteDocumento tdTemp : copiasBDGeograficaOriginalDepuracion) {
                indiceUltimoRegistroCopiaBDGeograficaOriginalDepuracion = (Integer.valueOf(tdTemp.
                    getDocumento().getObservaciones().replaceAll("Copia No. ", ""))).intValue();
                if (indiceUltimoRegistroCopiaBDGeograficaOriginalDepuracion > indiceTemp) {
                    indiceTemp = indiceUltimoRegistroCopiaBDGeograficaOriginalDepuracion;
                }
            }
        } else {
            indiceTemp = 0;
        }
        indiceTemp++;
        ultimoRegistroCopiaBDGeograficaOriginalDepuracion = "Copia No. " + indiceTemp;

        documentoTemp = copiasBDGeograficaOriginal.get(0).getDocumento();
        documentoTemp.setTipoDocumento(tdCopiaBDOriginalDepuracion);
        documentoTemp.setObservaciones(ultimoRegistroCopiaBDGeograficaOriginalDepuracion);
        this.generalesService.actualizarDocumento(documentoTemp);

        indiceTemp = 0;
        if (copiasBDGeograficaFinalDepuracion != null && !copiasBDGeograficaFinalDepuracion.
            isEmpty()) {
            for (TramiteDocumento tdTemp : copiasBDGeograficaFinalDepuracion) {
                indiceUltimoRegistroCopiaBDGeograficaFinalDepuracion = (Integer.valueOf(tdTemp.
                    getDocumento().getObservaciones().replaceAll("Copia No. ", ""))).intValue();
                if (indiceUltimoRegistroCopiaBDGeograficaFinalDepuracion > indiceTemp) {
                    indiceTemp = indiceUltimoRegistroCopiaBDGeograficaFinalDepuracion;
                }
            }
        } else {
            indiceTemp = 0;
        }
        indiceTemp++;
        ultimoRegistroCopiaBDGeograficaFinalDepuracion = "Copia No. " + indiceTemp;
        documentoTemp = copiasBDGeograficaFinal.get(0).getDocumento();
        documentoTemp.setTipoDocumento(tdCopiaBDFinalDepuracion);
        documentoTemp.setObservaciones(ultimoRegistroCopiaBDGeograficaFinalDepuracion);
        this.generalesService.actualizarDocumento(documentoTemp);

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacionLocal#tieneMejorasPredio(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean tieneMejorasPredio(String numeroPredioEnNumeroPredial) {

        LOGGER.debug("En ConservacionBean#tieneMejorasPredio ");

        boolean answer = false;

        try {
            answer = this.predioDao.tieneMejorasPredio(numeroPredioEnNumeroPredial);
        } catch (ExcepcionSNC sncEx) {
            LOGGER.error("Error en ConservacionBean#tieneMejorasPredio: " + sncEx.getMensaje());
        }

        return answer;
    }

    //--------------------------------------------------------------------------------------------------    
    /**
     * @see IGenerales#obtenerVigenciaUltimaActualizacionPorZona(java.util.Date)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public Date obtenerVigenciaUltimaActualizacionPorZona(String zona) {

        Date answer;
        LOGGER.debug("on ConservacionBean#obtenerVigenciaUltimaActualizacionPorZona ...");

        Calendar c = Calendar.getInstance();

        //D: se usa cualquiera de los EJB que implementen IGenericJpaDAO para obtener el entity manager
        Integer year = QueryNativoDAO.anioUltimaActualizacion(this.documentoDao.getEntityManager(),
            zona);

        if (year == null) {
            LOGGER.info("No existes fecha de actualizaciòn para la zona " + zona);
            return null;
        }

        c.set(year, 0, 1);

        answer = c.getTime();

        LOGGER.debug("... finished ConservacionBean#obtenerVigenciaUltimaActualizacionPorZona.");
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConservacion#obtenerPredioConDetalleAvaluoPorId(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Predio obtenerPredioConDetalleAvaluoPorId(Long predioId) {

        Predio answer = null;
        try {
            answer = this.consultaPredioDao.consultarDetalleAvaluoPredioByPredioId(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPredioConAvaluosPorId: " +
                ex.getMensaje());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------	
    /**
     * Método para copiar del predio actual, los propietarios a todos los predios del desenglobe
     *
     * @param predioOrigen
     * @param predios
     * @param usuario
     */
    public List<PPredio> copiarPropietarios(PPredio predioOrigen, List<PPredio> predios,
        String usuarioLog) {
        for (PPredio predio : predios) {
            if (!predio.isEsPredioFichaMatriz() && !predioOrigen.getId().equals(predio.getId())) {
                for (PPersonaPredio pp : predioOrigen.getPPersonaPredios()) {
                    boolean existe = false;
                    for (PPersonaPredio ppp : predio.getPPersonaPredios()) {
                        if (pp.getPPersona().getNumeroIdentificacion().equals(ppp.getPPersona().
                            getNumeroIdentificacion())) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {

                        PPersonaPredio pppn = new PPersonaPredio();
                        pppn.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                        pppn.setFechaInscripcionCatastral(new Date());
                        pppn.setFechaLog(new Date());
                        //Se crea una nueva persona, con los mismos datos de la P_PERSONA_PREDIO a copiar
                        if (pp.getPPersona() != null) {
                            PPersona pper = pp.getPPersona();
                            pper.setId(null);
                            pper.setFechaLog(new Date(System.currentTimeMillis()));
                            pper = this.pPersonaDao.update(pper);

                            //Se adiciona la nueva persona a la P_PERSONA_PREDIO a copiar
                            pppn.setPPersona(pper);
                        }

                        pppn.setTipo(pp.getTipo());
                        pppn.setUsuarioLog(usuarioLog);
                        pppn.setPPredio(predio);
                        this.pPersonaPredioDao.update(pppn);

                        predio.getPPersonaPredios().add(pppn);

                    }
                }
            }
        }
        return predios;
    }

    /**
     * @see IConservacion#rectificarZonas(java.lang.Long)
     * @author felipe.cadena
     */
    /*
     * @modified juan.mendez 2014/04/03 Se modificó la firma del método de SigDao. Ya no requiere el
     * parámetro token. @modifies felipe.cadena::22-07-2015::Se agrega
     */
    @Override
    public List<PPredioZona> rectificarZonas(Long idTramite, UsuarioDTO usuario, boolean sincronico) {

        Tramite tramite = this.tramiteDao.getDocumentacionCompletaTramite(idTramite);
        List<PPredio> prediosProyectados = new ArrayList<PPredio>();
        Documento copiaConsulta = null;
        List<PPredioZona> zonas = new LinkedList<PPredioZona>();

        //se obtiene la replica de consulta
        for (TramiteDocumento td : tramite.getTramiteDocumentos()) {
            if (td.getDocumento().getTipoDocumento().getId().
                equals(ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId())) {
                copiaConsulta = td.getDocumento();
            }
        }

        //felipe.cadena::23-07-2015::Para el caso de los PH solo se calculan las zonas de la ficha matriz
        if (tramite.getPredio().getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.
            getCodigo())) {
            try {
                PPredio predioProyectado = this.obtenerPPredioCompletoById(tramite.getPredio().
                    getId());
                prediosProyectados.add(predioProyectado);
            } catch (Exception ex) {
                LOGGER.error("No existe predios proyectados para el tramite.");
                return null;
            }
        } else {
            prediosProyectados = this.conservacionService.buscarPPrediosCompletosPorTramiteId(
                idTramite);
        }

        if (copiaConsulta == null) {
            LOGGER.error("No existe copia de consulta para calcular las zonas.");
            return null;
        }
        if (prediosProyectados == null || prediosProyectados.isEmpty()) {
            LOGGER.error("No existe predios proyectados para el tramite.");
            return null;
        }

        if (!sincronico) {

            SigDAOv2.getInstance().obtenerZonasAsync(this.productoCatastralJobDAO,
                prediosProyectados, copiaConsulta.getIdRepositorioDocumentos(),
                usuario, tramite, null);
        } else {
            // se obtienen las zonas desde la geodatabase
            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos =
                SigDAOv2.getInstance().obtenerZonasHomogeneas(prediosProyectados, copiaConsulta.
                    getIdRepositorioDocumentos(), usuario);

            for (String numPredio : datos.keySet()) {
                List<ZonasFisicasGeoeconomicasVO> zn = datos.get(numPredio);

                this.tramiteService.actualizarZonasPorPredio(usuario, datos, tramite);
                zonas = this.buscarZonasHomogeneasPorNumeroPredialPPredio(numPredio);

            }

        }

        return zonas;
    }

    /**
     * @see IConservacion#eliminarZona(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public void eliminarZona(PPredioZona pZona) {

        try {
            this.pPredioZonaDao.delete(pZona);
        } catch (Exception e) {
            LOGGER.error("Error al eliminar la zona");
        }
    }

    /**
     * @see IConservacion#eliminarZona(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public PPredioZona guardarActualizarZona(PPredioZona pZona) {

        PPredioZona result = null;
        try {
            result = this.pPredioZonaDao.update(pZona);
        } catch (Exception e) {
            LOGGER.error("Error al actualizar la zona");
        }
        return result;
    }

    /**
     * @see IConservacion#obtenerPPredioAvaluosCatastral(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public PPredioAvaluoCatastral obtenerPPredioAvaluosCatastral(Long id) {

        //felipe.cadena::redmine #7538
        PPredioAvaluoCatastral ppac = null;
        try {
            ppac = this.pPredioAvaluoCatastralDao.findById(id);
        } catch (Exception e) {
            LOGGER.error("Error al consultar Predio avaluo catastral");
        }
        return ppac;
    }

    /**
     * @see IConservacion#buscarFotografiaPorIdPredioAndFotoTipo(Long, String)
     * @author javier.aponte
     */
    @Implement
    @Override
    public List<Foto> buscarFotografiaPorIdPredioAndFotoTipo(Long predioId, String codigoTipoFoto) {

        List<Foto> answer = null;

        try {

            answer = this.fotoDao.buscarFotografiaPorIdPredioAndFotoTipo(predioId, codigoTipoFoto);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarFotografiaPorIdPredioAndFotoTipo: " +
                predioId + ex.getMensaje());
        }
        return answer;
    }

    @Implement
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosPorTramiteTipoEstado(Long tramiteId,
        String tipo, String estado) {

        List<ProductoCatastralJob> resultados = new ArrayList<ProductoCatastralJob>();

        try {
            resultados = this.productoCatastralJobDAO.obtenerJobsGeograficosPorTramiteTipoEstado(
                tramiteId, tipo, estado);
        } catch (ExcepcionSNC es) {
            LOGGER.debug("Error al consultar jobs del tramite " + tramiteId);

        }

        return resultados;

    }

    /**
     * @see IConservacion#obtenerZonasUltimaVigenciaPorIdPredio(java.lang.Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<PredioZona> obtenerZonasUltimaVigenciaPorIdPredio(Long predioId) {

        List<PredioZona> answer = null;

        try {

            answer = this.predioZonaDAO.buscarPorPPredioId(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerZonasUltimaVigenciaPorIdPredio: " +
                predioId + ex.getMensaje());
        }
        return answer;

    }

    /**
     * @see IConservacion#buscarFichasEnProcesoDeCarga( String)
     * @author javier.aponte
     */
    @Implement
    @Override
    public List<PFichaMatriz> buscarFichasEnProcesoDeCarga(String usuario) {
        List<PFichaMatriz> resultado = null;

        try {
            resultado = this.pFichaMatrizDao.obtenerFichasEnProcesoDeCarga(usuario);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarFichasEnProcesoDeCarga con usuario :" +
                usuario + ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IConservacion#buscarFichaPorNumeroPredialProyectada(PFichaMatriz)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public PFichaMatriz buscarFichaPorNumeroPredialProyectada(String numeroPredial) {

        PFichaMatriz pfm = null;

        try {
            pfm = this.pFichaMatrizDao.findByNumeroPredial(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarFichaPorNumeroPredial con predio :" +
                numeroPredial + ex.getMensaje());
            pfm = null;
        }

        return pfm;
    }

    /**
     * @see IConservacion#buscarPrediosPorRaizNumPredial(PFichaMatriz)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosPorRaizNumPredial(String raizNumPredial) {

        List<Predio> resultado = null;

        try {
            resultado = this.predioDao.obtenerPorRaizNumeroPredial(raizNumPredial);
        } catch (ExcepcionSNC es) {
            LOGGER.error(
                "Error al obtener los predios para la raiz : " + raizNumPredial + " me : " + es.
                    getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#proyectarFichaMigrada(java.lang.String, java.util.Date, java.lang.String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public Object[] proyectarFichaMigrada(String numeroPredial, Date fecha, String usuario) {

        Object[] result = null;

        try {
            result = this.sncProcedimientoDao.proyectarFichaMigrada(numeroPredial, fecha, usuario);
        } catch (ExcepcionSNC es) {
            LOGGER.error("Error en la proyeccion de la ficha migrada");
        }

        return result;
    }

    /**
     * @see IConservacion#reversarFichaMigrada(java.lang.Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public Object[] reversarProyeccionFichaMigrada(Long numeroPredial) {

        Object[] result = null;

        try {
            result = this.sncProcedimientoDao.reversarProyeccionFichaMigrada(numeroPredial);
        } catch (ExcepcionSNC es) {
            LOGGER.error("Error en reversar la proyeccion de la ficha migrada");
        }

        return result;
    }

    /**
     * @see IConservacion#reversarFichaMigrada(java.lang.Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public Object[] confirmarProyeccionFichaMigrada(Long numeroPredial) {

        Object[] result = null;

        try {
            result = this.sncProcedimientoDao.confirmarProyeccionFichaMigrada(numeroPredial);
        } catch (ExcepcionSNC es) {
            LOGGER.error("Error en confirmar la proyeccion de la ficha migrada");
        }

        return result;
    }

    @Implement
    @Override
    public boolean eliminarPunidadConstruccion(PUnidadConstruccion puc) {

        try {
            this.pUnidadConstruccionDao.delete(puc);
            return true;
        } catch (Exception es) {
            LOGGER.error("Error al eliminar construccion proyectada" + es.getMessage());
            return false;
        }

    }

    /**
     * @see IConservacion#guardarPPersonaPredio2(UsuarioDTO, PPersonaPredio)
     * @author juanfelipe.garcia
     */
    @Override
    public PPersonaPredio guardarPPersonaPredio2(UsuarioDTO usuario,
        PPersonaPredio pPersonaPredio) {
        try {
            pPersonaPredio.getPPersona().setFechaLog(new Date());
            pPersonaPredio.getPPersona().setUsuarioLog(usuario.getLogin());
            pPersonaPredio.setFechaLog(new Date());
            pPersonaPredio.setUsuarioLog(usuario.getLogin());

            pPersonaPredio = this.pPersonaPredioDao.update(pPersonaPredio);

            return pPersonaPredio;
        } catch (Exception e) {
            LOGGER.error("Error al guardar PPersonaPredio");
            return null;
        }
    }

    /**
     *
     * @see IConservacionBean#obtenerManzanasProyectadasPorTramite(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<PManzanaVereda> obtenerManzanasProyectadasPorTramite(Long tramiteId) {

        List<PManzanaVereda> resultado = null;
        try {
            resultado = this.manzanaVeredaService.obtenerPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerManzanasProyectadasPorTramite: " +
                tramiteId + "  -  - " + ex.getMensaje());
        }
        return resultado;
    }

    /**
     *
     * @see
     * IConservacionBean#guardarActualizarPManzanaVereda(co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda)
     * @author felipe.cadena
     */
    @Override
    public PManzanaVereda guardarActualizarPManzanaVereda(PManzanaVereda pmv) {
        PManzanaVereda resultado = null;

        try {
            resultado = this.manzanaVeredaService.update(pmv);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#guardarActualizarPManzanaVereda: " +
                pmv.getCodigo() + "  -  - " + ex.getMensaje());
        }

        return resultado;
    }

    /**
     *
     * @see IConservacion#guardarActualizarMultiplePManzanaVereda(List<PManzanaVereda>)
     * @author felipe.cadena
     */
    @Override
    public List<PManzanaVereda> guardarActualizarMultiplePManzanaVereda(List<PManzanaVereda> pmvs) {
        List<PManzanaVereda> resultado = null;

        try {
            this.manzanaVeredaService.updateMultiple(pmvs);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#guardarActualizarPManzanaVereda:  -  - " + ex.
                getMensaje());
        }

        return resultado;
    }

    /**
     *
     * @see
     * IConservacionBean#eliminarPManzanaVereda(co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda)
     * @author felipe.cadena
     */
    @Override
    public void eliminarPManzanaVereda(PManzanaVereda pmv) {

        try {
            this.manzanaVeredaService.delete(pmv);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#eliminarPManzanaVereda: " +
                pmv.getCodigo() + "  -  - " + ex.getMensaje());
        }

    }

    /**
     * @author felipe.cadena
     * @see IConservacion#generarNumeroManzana(String)
     */
    @Override
    public String generarNumeroManzana(String numeroBarrio) {

        String manzana = null;

        try {
            manzana = this.sncProcedimientoDao.generarNumeroManzana(numeroBarrio);
        } catch (ExcepcionSNC es) {
            LOGGER.error("Error al generar el numero de manzana para el barrio " + numeroBarrio);
        }
        return manzana;
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerValorUnidadConstruccionPorProcedimientoDB(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Long)
     */
    @Override
    public double[] calcularSumaAvaluosTotalesDePrediosMigradosFichaMatriz(Long idFicha) {

        double[] totalAvaluo = null;

        try {
            totalAvaluo = this.predioDao.calcularSumaAvaluosTotalesDePrediosFichaMatriz(idFicha);
        } catch (ExcepcionSNC es) {
            LOGGER.error("Error sumar los avaluos de los predios de la ficha " + idFicha);
        }
        return totalAvaluo;
    }

    /**
     * @author felipe.cadena
     * @see IConservacionLocal#obtenerValorUnidadConstruccionPorProcedimientoDB(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public Double obtenerValorUnidadConstruccionPorProcedimientoDB(String zonacodigo, String sector,
        String destino, String usoConstruccion, String zonaGeoEconomica, Long puntaje, Long predioId) {

        Double answer = null;
        try {
            answer = this.sncProcedimientoDao.
                obtenerValorUnidadConstruccion(zonacodigo, sector, destino, usoConstruccion,
                    zonaGeoEconomica, puntaje, predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#obtenerValorUnidadConstruccionPorProcedimientoDB: " + ex.
                    getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#obtenerPFichaMatrizPrediaPorNumeroPredial(java.lang.String)
     * @author juanfelipe.garcia
     */
    @Override
    public PFichaMatrizPredio obtenerPFichaMatrizPredioPorNumeroPredial(String numeroPredial) {

        PFichaMatrizPredio answer = null;
        try {
            answer = this.pFichaMatrizPredioDao.obtenerPFichaMatrizPredioPorNumeroPredial(
                numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerFichaMatrizPrediaPorNumeroPredial: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#guardarPPredio(PPredio)
     * @author leidy.gonzalez
     */
    @Override
    public PPredio guardarPPredio(PPredio pPredio) {

        PPredio auxPPredio = null;

        try {
            auxPPredio = this.pPredioDao.update(pPredio);

        } catch (Exception e) {
            LOGGER.error("error en ConservacionBean#guardarPPredio: " +
                e.getMessage());
        }
        return auxPPredio;
    }

    /**
     * @see IConservacion#obtenerUnidadesNoCanceladasByPredioId(Long)
     * @author felipe.cadena
     */
    @Override
    public List<UnidadConstruccion> obtenerUnidadesNoCanceladasByPredioId(Long predioId) {

        List<UnidadConstruccion> answer = null;

        try {
            answer = this.unidadConstruccionDao.getUnidadsConstruccionByPredioIdNoCanceladas(
                predioId);

        } catch (Exception e) {
            LOGGER.error("error en ConservacionBean#guardarActualizarPPredio: " +
                e.getMessage());
        }
        return answer;
    }

    /**
     * @see
     * IConservacionLocal#actualizarTramiteTarea(co.gov.igac.snc.persistence.entity.tramite.TramiteTarea)
     * @author felipe.cadena
     */
    @Override
    public TramiteTarea actualizarTramiteTarea(TramiteTarea tt) {

        TramiteTarea answer = null;
        try {
            answer = this.tramiteTareaDao.update(tt);
        } catch (Exception e) {
            LOGGER.info("Error al actulizar el tramite tarea");

        }

        return answer;
    }

    /**
     * @see IConservacionLocal#obtenerTramiteTareaPorIdTramite(Long)
     * @author felipe.cadena
     */
    @Override
    public List<TramiteTarea> obtenerTramiteTareaPorIdTramite(Long tramiteId, String tarea) {

        List<TramiteTarea> resultado = null;

        try {
            resultado = this.tramiteTareaDao.obtenerTareasTramite(tramiteId, tarea);
        } catch (Exception e) {
            LOGGER.error("Error al consultar las tareas del tramite");
        }

        return resultado;

    }

    /**
     * Metodo para invocar el aplicar cambios desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez::12/11/2015:: Se modifica tipo de retorno del metodo.
     */
    @Override
    public boolean aplicarCambiosConservacion(Tramite tramite, UsuarioDTO usuario, int paso) {
        boolean validaError;
        validaError = this.aplicarCambios.aplicarCambiosConservacion(tramite, usuario, paso);
        return validaError;
    }

    /**
     * Metodo para invocar el proceso aplicar cambios de depuracion desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    @Override
    public void aplicarCambiosDepuracion(Tramite tramite, UsuarioDTO usuario, int paso) {
        this.aplicarCambios.aplicarCambiosDepuracion(tramite, usuario, paso);
    }

    /**
     * Metodo para invocar el proceso aplicar cambios de actualizacion desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez:: 23-10-2015:: Se modifica el retorno del metodo.
     */
    @Override
    public boolean aplicarCambiosActualizacion(Tramite tramite, UsuarioDTO usuario, int paso) {
        boolean validaError;
        validaError = this.aplicarCambios.aplicarCambiosActualizacion(tramite, usuario, paso);
        return validaError;
    }

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param usuarioResponsableSIG -- Solo se usa para el envio a depuracion(paso 0), puede ser
     * null en los demas casos
     * @param paso
     */
    @Override
    public void generarReplicaConsulta(Tramite tramite, UsuarioDTO usuario,
        UsuarioDTO usuarioResponsableSIG, int paso) {
        this.aplicarCambios.generarReplicaConsulta(tramite, usuario, usuarioResponsableSIG, paso,
            null);
    }

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     */
    @Override
    public void generarReplicaEdicion(Tramite tramite, UsuarioDTO usuario, int paso) {
        this.aplicarCambios.generarReplicaEdicion(tramite, usuario, paso, null);
    }

    /**
     * @see IConservacionLocal#obtenerTramitesEnJobGeografico()
     * @author felipe.cadena
     */
    @Override
    public List<TramiteTarea> obtenerTramitesEnJobGeografico() {

        List<TramiteTarea> resultado = new ArrayList<TramiteTarea>();

        try {
            resultado = this.tramiteTareaDao.obtenerTramitesEnJobGeografico();
        } catch (Exception e) {
            LOGGER.error("Error al consultar las tareas del tramite");
        }

        return resultado;
    }

    /**
     * @see IConservacionLocal#obtenerProductoCatastralJobById(Long)
     * @author felipe.cadena
     */
    @Override
    public ProductoCatastralJob obtenerProductoCatastralJobById(Long jobId) {

        ProductoCatastralJob resultado = null;

        try {
            resultado = this.productoCatastralJobDAO.findById(jobId);
        } catch (Exception e) {
            LOGGER.error("Error al consultar el job por ID");
        }

        return resultado;
    }

    /**
     * @see IConservacion#actualizarPPersonaPredio(PPersonaPredio)
     * @author javier.aponte
     */
    @Override
    public PPersonaPredio actualizarPPersonaPredio(PPersonaPredio pPersonaPredio) {
        try {
            PPersonaPredio auxPPersonaPredio = this.pPersonaPredioDao.update(pPersonaPredio);
            pPersonaPredio.setId(auxPPersonaPredio.getId());
            return pPersonaPredio;
        } catch (RuntimeException rtex) {
            LOGGER.error("error en ConservacionBean#actualizarPPersonaPredio: " +
                rtex.getMessage());
            return null;
        }
    }

    /**
     * @see IConservacion#actualizarPPersonaPredioPropiedad(PPersonaPredioPropiedad)
     * @author javier.aponte
     */
    @Override
    public PPersonaPredioPropiedad actualizarPPersonaPredioPropiedad(
        PPersonaPredioPropiedad pPersonaPredioPropiedad) {
        try {
            PPersonaPredioPropiedad auxPPersonaPredioPropiedad = this.pPersonaPredioPropiedadDao.
                update(pPersonaPredioPropiedad);
            pPersonaPredioPropiedad.setId(auxPPersonaPredioPropiedad.getId());
            return pPersonaPredioPropiedad;
        } catch (RuntimeException rtex) {
            LOGGER.error("error en ConservacionBean#actualizarPPersonaPredioPropiedad: " +
                rtex.getMessage());
            return null;
        }
    }

    /**
     * @see IConservacion#guardarActualizarPPersonaPredioPropiedades(List)
     * @author javier.aponte
     */
    @Override
    public void guardarActualizarPPersonaPredioPropiedades(
        List<PPersonaPredioPropiedad> pPersonaPredioPropiedades) {

        try {
            this.pPersonaPredioPropiedadDao.updateMultiple(pPersonaPredioPropiedades);
        } catch (RuntimeException rtex) {
            LOGGER.error("error en ConservacionBean#guardarActualizarPPersonaPredioPropiedades: " +
                rtex.getMessage());
        }
    }

    /**
     * @see IConservacion#actualizarPPersonaPredioAndPPersonaPredioPropiedad(PersonaPredio[],
     * List<PPersonaPredioPropiedad>)
     * @author javier.aponte
     */
    /**
     * @see IConservacion#actualizarPPersonaPredioAndPPersonaPredioPropiedad(PersonaPredio[],
     * List<PPersonaPredioPropiedad>)
     * @author javier.aponte
     */
    @Override
    public List<PPersonaPredio> actualizarPPersonaPredioAndPPersonaPredioPropiedad(
        PersonaPredio[] propietariosVigentesSeleccionados,
        List<PPersonaPredio> pPersonaPredioPropiedades, UsuarioDTO usuario) {

        List<PPersonaPredio> answer = new ArrayList<PPersonaPredio>();
        PPersonaPredio pPersonaPredioTemp;
        try {
            answer.addAll(pPersonaPredioPropiedades);
            for (PersonaPredio pVigenteSelect : propietariosVigentesSeleccionados) {
                for (PPersonaPredio pNuevo : answer) {
                    if (pVigenteSelect.getId().equals(pNuevo.getId())) {
                        pNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                        pNuevo.setUsuarioLog(usuario.getLogin());
                        pNuevo.setFechaLog(new Date());

                        if (pNuevo.getPPersonaPredioPropiedads() != null && !pNuevo.
                            getPPersonaPredioPropiedads().isEmpty()) {
                            for (PPersonaPredioPropiedad pPersPrePro : pNuevo.
                                getPPersonaPredioPropiedads()) {
                                if (pPersPrePro.getModoAdquisicion() == null || !pPersPrePro.
                                    getModoAdquisicion().equals("MIGRACION")) {
                                    pPersPrePro.setCancelaInscribe(
                                        EProyeccionCancelaInscribe.CANCELA.getCodigo());
                                    pPersPrePro.setUsuarioLog(usuario.getLogin());
                                    pPersPrePro.setFechaLog(new Date());
                                }
                            }
                            this.guardarActualizarPPersonaPredioPropiedades(pNuevo.
                                getPPersonaPredioPropiedads());
                        }

                        pNuevo = this.actualizarPPersonaPredio(pNuevo);
                    }
                }
            }
            for (PersonaPredio pVigenteSelect : propietariosVigentesSeleccionados) {
                for (PPersonaPredio pNuevo : pPersonaPredioPropiedades) {
                    if (pVigenteSelect.getId().equals(pNuevo.getId())) {
                        pPersonaPredioTemp = (PPersonaPredio) pNuevo.clone();
                        pPersonaPredioTemp.setId(null);
                        pPersonaPredioTemp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
                            getCodigo());
                        pPersonaPredioTemp = this.actualizarPPersonaPredio(pPersonaPredioTemp);

                        answer.add(pPersonaPredioTemp);
                        break;
                    }
                }
            }
        } catch (RuntimeException rtex) {
            LOGGER.error(
                "error en ConservacionBean#actualizarPPersonaPredioAndPPersonaPredioPropiedad: " +
                rtex.getMessage());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarReporteControlReportePorUsuario(String)
     * @author leidy-gonzalez
     */
    @Override
    public List<RepReporteEjecucion> buscarReporteEjecucionPorUsuario(String usuarioLogin) {

        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO.buscarReporteEjecucionPorUsuario(usuarioLogin);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar reporteControlPorUsuario" + e.getMessage());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#actualizarRepReporteEjecucion(RepReporteEjecucion)
     */
    @Override
    public RepReporteEjecucion actualizarRepReporteEjecucion(
        RepReporteEjecucion repReporteEjecucion) {

        RepReporteEjecucion auxRepReporteEjecucion = null;

        try {
            auxRepReporteEjecucion = this.repReporteEjecucionDAO
                .update(repReporteEjecucion);

            Hibernate.initialize(auxRepReporteEjecucion.getRepReporte());
            Hibernate.initialize(auxRepReporteEjecucion.getRepReporte().getId());
            Hibernate.initialize(auxRepReporteEjecucion.getRepReporte().getNombre());

            if (!auxRepReporteEjecucion.getTerritorial().getCodigo().isEmpty()) {
                Hibernate.initialize(auxRepReporteEjecucion.getTerritorial().getCodigo());
            }

            if (!auxRepReporteEjecucion.getTerritorial().getNombre().isEmpty()) {
                Hibernate.initialize(auxRepReporteEjecucion.getTerritorial().getNombre());
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#actualizarRepReporteEjecucion: " +
                ex.getMensaje());
        }
        return auxRepReporteEjecucion;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#actualizarRepReporteEjecucion(RepReporteEjecucion)
     */
    @Override
    public RepReporteEjecucionDetalle actualizarRepReporteEjecucionDetalle(
        RepReporteEjecucionDetalle repReporteEjecucionDetalle) {

        RepReporteEjecucionDetalle auxRepReporteEjecucionDetale = null;

        try {
            auxRepReporteEjecucionDetale = this.repReporteEjecucionDetalleDAO
                .update(repReporteEjecucionDetalle);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#actualizarRepReporteEjecucion: " +
                ex.getMensaje());
        }
        return auxRepReporteEjecucionDetale;
    }

    // ---------------------------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see IConservacionLocal#buscarReporteEjecucionCierreAnualHistoricos(String, String)
     */
    @Override
    public List<RepReporteEjecucion> buscarReporteEjecucionCierreAnualHistoricos(
        String codigoDepartamento, String codigoMunicipio) {
        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO
                .buscarReporteEjecucionCierreAnualHistoricos(
                    codigoDepartamento, codigoMunicipio);
        } catch (Exception e) {
            LOGGER.debug("Error en buscarReporteEjecucionCierreAnualHistoricos: " +
                e.getMessage());
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#generarReporteCierreAnual(RepReporteEjecucion)
     * @author david.cifuentes
     */
    @Override
    public Object[] generarReporteCierreAnual(
        RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            answer = sncProcedimientoDao
                .generarReporteCierreAnual(repReporteEjecucion);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#cargarListadoPrediosPorMunicipioVigencia(String, Long)
     * @author david.cifuentes
     */
    @Override
    public String[] cargarListadoPrediosPorMunicipioVigencia(
        String municipioCodigo, Long vigenciaDecreto) {
        String[] answer = null;
        try {
            answer = listadoPredioDao.cargarListadoPrediosPorMunicipioVigencia(
                municipioCodigo, vigenciaDecreto);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#cargarListadoPrediosPorMunicipioVigencia(
     * contarListadoPredios(String[])
     * @author david.cifuentes
     */
    @Override
    public int contarListadoPredios(String[] numerosPredialesListadoPredios) {
        LOGGER.debug("ConservacionBean#contarListadoPredios");

        int answer = 0;
        try {
            answer = this.listadoPredioDao
                .contarListadoPredios(numerosPredialesListadoPredios);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#contarListadoPredios: " +
                ex.getMensaje());
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#buscarListadoPrediosCierreAnual( String[], String, String,
     * Map<String, String>, int, int)
     * @author david.cifuentes
     */
    public List<ListadoPredio> buscarListadoPrediosCierreAnual(
        String[] numerosPredialesListadoPredios, String sortField, String sortOrder,
        Map<String, String> filters, int... contadores) {

        LOGGER.debug("en ConservacionBean#buscarListadoPrediosCierreAnual");
        List<ListadoPredio> answer = null;
        try {
            answer = this.listadoPredioDao.buscarListadoPrediosCierreAnual(
                numerosPredialesListadoPredios, sortField, sortOrder, filters, contadores);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("ERROR: ConservacionBean#buscarListadoPrediosCierreAnual: " + ex.
                getMensaje());
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#buscarListadoPrediosCierreAnualPorMunicipioVigencia( String, Long,
     * String, String, Map<String, String>, int, int)
     * @author david.cifuentes
     */
    @Override
    public List<ListadoPredio> buscarListadoPrediosCierreAnualPorMunicipioVigencia(
        String municipioCodigo, Long vigenciaDecreto, String sortField,
        String sortOrder, Map<String, String> filters, int first,
        int pageSize) {
        LOGGER.debug("en ConservacionBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia");
        List<ListadoPredio> answer = null;
        try {
            answer = this.listadoPredioDao.buscarListadoPrediosCierreAnualPorMunicipioVigencia(
                municipioCodigo, vigenciaDecreto, sortField, sortOrder, filters, first, pageSize);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "ERROR: ConservacionBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia: " +
                ex.getMensaje());
        }
        return answer;
    }
    // ---------------------------------------------------------------------- //

    /**
     * @see IConservacionLocal#contarListadoPredios(String, Long, Map<String, String>)
     * @author david.cifuentes
     */
    public int contarListadoPredios(String municipioCodigo, Long vigenciaDecreto,
        Map<String, String> filters) {
        int answer = 0;
        try {
            answer = this.listadoPredioDao
                .contarListadoPredios(municipioCodigo, vigenciaDecreto, filters);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#contarListadoPredios: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#obtenerPFichaMatrizPrediaPorNumeroPredial(java.lang.String)
     * @author javier.aponte
     */
    @Override
    public FichaMatrizPredio obtenerFichaMatrizPredioPorNumeroPredial(String numeroPredial) {

        FichaMatrizPredio answer = null;
        try {
            answer = this.fichaMatrizPredioDao.obtenerFichaMatrizPredioPorNumeroPredial(
                numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerFichaMatrizPredioPorNumeroPredial: " +
                ex.getMensaje());
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long, Long)
     * @author david.cifuentes
     */
    @Override
    public boolean existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long numeroTorre,
        Long idFichaMatriz) {
        boolean existe = false;
        try {
            existe = this.fichaMatrizTorreDao.existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(
                numeroTorre, idFichaMatriz);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#existeTorreEnFirmePorIdFichaMatrizYNumeroTorre: " +
                ex.getMensaje());
        }
        return existe;
    }

    /**
     * @see IConservacion#eliminarPFichaMatrizPredio(PFichaMatrizPredio)
     * @author felipe.cadena
     */
    @Override
    public void eliminarPFichaMatrizPredio(PFichaMatrizPredio fichaMatrizPredio) {

        try {
            pFichaMatrizPredioDao.eliminar(fichaMatrizPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#eliminarPFichaMatrizPredio: " +
                ex.getMensaje());
        }
    }

    /**
     * @see IConservacion#buscarPPredioPorMatricula(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<PPredio> buscarPPredioPorMatricula(String circuloRegistralCodigo,
        String numeroRegistro) {
        List<PPredio> answer = null;
        try {
            answer = this.pPredioDao.buscarPorMatricula(circuloRegistralCodigo, numeroRegistro);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPPredioPorMatricula: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#buscarPredioPorMatricula(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> buscarPredioPorMatricula(String circuloRegistralCodigo,
        String numeroRegistro) {
        List<Predio> answer = null;
        try {
            answer = this.predioDao.buscarPorMatricula(circuloRegistralCodigo, numeroRegistro);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPredioPorMatricula: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#buscarPredioPorMatricula(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    public PPredioDireccion obtenerDireccionPrincipalFM(String numeroPredial) {
        PPredioDireccion answer = null;
        try {
            answer = this.pPredioDireccionDao.obtenerDireccionPrincipalFichaMatriz(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerDireccionPrincipalFM: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(String, Long)
     * @author david.cifuentes
     */
    @Override
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(
        String valorCancelaInscribe, Long fichaMatrizId) {
        List<PFichaMatrizPredio> answer = null;
        try {
            answer = this.pFichaMatrizPredioDao.
                buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(valorCancelaInscribe,
                    fichaMatrizId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @author david.cifuentes
     * @see IConservacion#actualizarPFichaMatrizPredios((List<PFichaMatrizPredio>)
     */
    public void actualizarListaPFichaMatrizPredios(
        List<PFichaMatrizPredio> prediosFichaMatriz) {
        this.pFichaMatrizPredioDao.updateMultiple(prediosFichaMatriz);
    }

    /**
     * @author david.cifuentes
     * @see IConservacion#obtenerFichaMatrizPorId(Long)
     */
    @Override
    public FichaMatriz obtenerFichaMatrizPorId(Long idFichaMatriz) {
        FichaMatriz answer = null;
        try {
            answer = this.fichaMatrizDao.obtenerFichaMatrizPorId(idFichaMatriz);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerFichaMatrizPorId: " +
                ex.getMensaje());
        }
        return answer;
    }

    public TreeMap<Long, Boolean> obtenerPredioModificadoPorTamite(Long idTramite,
        String nombreTablaAsociada) {

        TreeMap<Long, Boolean> resultado = null;

        try {
            resultado = this.pPredioDao.obtenerPredioModificadoPorTamite(idTramite,
                nombreTablaAsociada);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPredioModificadoPorTamite: " +
                ex.getMensaje());
        }

        return resultado;

    }

    /**
     * @see IConservacion#buscarFichaMatrizPorPredioId(Long)
     * @author javier.aponte
     */
    @Override
    public FichaMatriz buscarFichaMatrizPorPredioId(Long idPredio) {

        FichaMatriz answer = null;
        try {
            answer = this.fichaMatrizDao.findByPredioId(idPredio, true);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarFichaMatrizPorPredioId: " + ex.
                getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#actualizacionZonasHomogeneasAsync(java.lang.Long)
     */
    @Override
    public Boolean actualizacionZonasHomogeneasAsync(ProductoCatastralJob job) {
        Boolean respuesta = Boolean.FALSE;
        try {

            UsuarioDTO usuarioActual = generalesService.getCacheUsuario(job.getUsuarioLog());
            Tramite tramite = tramiteDao.findById(job.getTramiteId());
            List<Actividad> actividades = this.procesosService.getActividadesPorIdObjetoNegocio(job.
                getTramiteId());
            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> prediosZonas =
                productoCatastralJobDAO.obtenerXMLResultadoObtenerZonasHomegeneas(job.getId());

            //Caso rectificacion de tramite envio desde componente web.
            if (tramite.isRectificacionZona() || tramite.isRectificacionZonaMatriz()) {
                //TODO felipe.cadena::Caso en el que retorna por una rectificacion de zonas     
                tramiteService.actualizarZonasPorPredio(usuarioActual, prediosZonas, tramite);
                try {
                    this.tramiteService.calcularAreasUnidadesPrediales(tramite);
                } catch (Exception e) {
                    LOGGER.error(
                        "No se pudo realizar la actualización de las zonas de los PH y Condominios");
                    return Boolean.FALSE;
                }
                respuesta = Boolean.TRUE;
            } //Caso en el que retorna desde el editor.
            else {
                tramiteService.actualizarZonasPorPredio(usuarioActual, prediosZonas, tramite);

                //felipe.cadena :: #9339 :: 04/09/2014 :: Se calculan las zonas para los predios de condicion 8 o 9
                if (tramite.isTipoInscripcionPH() ||
                    tramite.isTipoInscripcionCondominio() ||
                    tramite.isDesenglobeEtapas() ||
                    tramite.isRectificacionMatriz()) {
                    try {
                        this.tramiteService.calcularAreasUnidadesPrediales(tramite);
                    } catch (Exception e) {
                        LOGGER.error(
                            "No se pudo realizar la actualización de las zonas de los PH y Condominios");
                        return Boolean.FALSE;
                    }
                }
                tramiteService.avanzarProcesoAModifInfoAlfanumerica(tramite.getId(), actividades.
                    get(0).getNombre(), usuarioActual);
                respuesta = Boolean.TRUE;
            }

            //felipe.cadena::22-17-2015::Se calculan los avaluos de los predios al terminar calculo de zonas
            List<PPredio> prediosProyectadosTramite = this.pPredioDao.buscarPorTramiteId(tramite.
                getId(), null);
            if (prediosProyectadosTramite != null && !prediosProyectadosTramite.isEmpty()) {
                for (PPredio pp : prediosProyectadosTramite) {
                    Object[] errores = this.sncProcedimientoDao.reproyectarAvaluos(tramite.getId(),
                        pp.getId(), new Date(), usuarioActual);
                    if (Utilidades.hayErrorEnEjecucionSP(errores)) {
                        LOGGER.error("Error al calcular el avaluo para el predio : " + pp.getId() +
                            "  Tramite : " + tramite.getId());
                        return Boolean.FALSE;
                    }
                }
                respuesta = Boolean.TRUE;
            } else {
                LOGGER.error("Error al recuperar los predios asociados el tramite Tramite : " +
                    tramite.getId());
                respuesta = Boolean.FALSE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return respuesta;
        }
        return respuesta;
    }

    /**
     *
     * @see IConservacionLocal#actualizacionZonasHomogeneasAsync(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public Integer contarPrediosAsociadosATramite(Long tramiteId) {

        Integer answer = 0;
        try {
            answer = this.tramiteDetallePredioService.countByTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#contarPrediosAsociadosATramite: " + ex.
                getMensaje());
        }
        return answer;

    }

    //--------------------------------------------------------------------------------------------------    
    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IConservacionLocal#procesarFichaPredialDigitalEnJasperServer
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarFichaPredialDigitalEnJasperServer(ProductoCatastralJob job) {

        LOGGER.debug("procesarFichaPredialDigitalEnJasperServer");
        boolean resultadoOk = true;

        try {

            UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService
                .buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            String urlReporte = EReporteServiceSNC.FICHA_PREDIAL_DIGITAL.getUrlReporte();

            Reporte reporte = new Reporte(usuario, estructuraOrganizacional);
            reporte.setUrl(urlReporte);

            reporte.setEstructuraOrganizacional(estructuraOrganizacional);

            int indiceSeparador = job.getCodigo().indexOf(
                Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO);
            String numeroPredial = null;

            if (indiceSeparador > 0) {

                numeroPredial = job.getCodigo().substring(0, indiceSeparador);

            }

            List<String> resultados = this.obtenerParametrosDeProductoCatastralJob(job);

            reporte.addParameter("ESCALA", resultados.get(0));
            reporte.addParameter("FOTO_LOCALIZACION_GEOGRAFICA", resultados.get(1));
            reporte.addParameter("FOTO_LOCALIZACION_MANZANA", resultados.get(2));
            if (resultados.get(3) != null && !resultados.get(3).isEmpty()) {
                reporte.addParameter("FOTO_FACHADA", resultados.get(3));
            }
            reporte.addParameter("COPIA_USO_RESTRINGIDO", resultados.get(4));

            this.generarArchivoFichaPredialDigitalPorPredio(numeroPredial, usuario, reporte);

        } catch (ExcepcionSNC e) {
            LOGGER.debug("procesarFichaPredialDigitalEnJasperServer:" + e.getMensaje(), e);
            resultadoOk = false;
        } catch (Exception ex) {
            LOGGER.debug("procesarFichaPredialDigitalEnJasperServer:" + ex.getMessage(), ex);
            resultadoOk = false;
        }
        return resultadoOk;
    }

    /**
     * Método para obtener una lista de parametros a partir de los resultados de un job
     *
     * @author javier.aponte
     */
    public List<String> obtenerParametrosDeProductoCatastralJob(
        ProductoCatastralJob productoGenerado) {
        List<String> resultados = new ArrayList<String>();
        try {
            if (productoGenerado.getResultado().contains("<?xml")) {
                LOGGER.debug(productoGenerado.getResultado());
                org.dom4j.Document document = (org.dom4j.Document) DocumentHelper.parseText(
                    productoGenerado.getResultado());
                List parametro = ((Node) document).selectNodes("//parametro");
                for (Iterator iter = parametro.iterator(); iter.hasNext();) {
                    Element productoXml = (Element) iter.next();
                    resultados.add(productoXml.attributeValue("escala"));
                    resultados.add(productoXml.attributeValue("fotoLocalizacionGeografica"));
                    resultados.add(productoXml.attributeValue("fotoLocalizacionManzana"));
                    resultados.add(productoXml.attributeValue("fotoFachada"));
                    resultados.add(productoXml.attributeValue("copiaUsoRestringido"));
                }

            } else {
                LOGGER.error(productoGenerado.getResultado());
                String message = "Ocurrió un error durante la generación del producto catastral job";
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    message);
            }
        } catch (DocumentException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultados;
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#eliminarProyeccionPredio(Long)
     */
    @Override
    public void eliminarProyeccionPredio(Long idPredio) {

        try {
            this.pPredioDao.eliminarProyeccionPredio(idPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#eliminarProyeccionPredio: " +
                ex.getMensaje());
        }
    }

    /**
     * @see IConservacion#buscarZonasHomogeneasPorPPredioIdYEstadoP(Long, String)
     * @author javier.aponte
     */
    @Override
    public List<PPredioZona> buscarZonasHomogeneasPorNumeroPredialPPredio(String numeroPredial) {

        List<PPredioZona> zonas = new ArrayList<PPredioZona>();
        try {
            zonas = this.pPredioZonaDao.buscarZonasHomogeneasPorNumeroPredialPPredio(numeroPredial);
        } catch (Exception e) {
            LOGGER.error("Error al consultar zonas por nùmero predial del p_predio");
        }
        return zonas;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#PPredioServidumbres(predioServidumbres)
     */
    @Override
    public void borrarPPredioServidumbres1(Long idServidumbre) {
        this.pPredioServidumbreDao.delete1(idServidumbre);

    }

    /**
     * @see IConservacionLocal#obtenerPrediosInvariantesDepuracion(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public List<PredioInfoVO> obtenerPrediosInvariantesDepuracion(String codigoManzana) throws
        Exception {
        LOGGER.debug("ConservacionBean#obtenerPrediosInvariantesDepuracion");
        List<PredioInfoVO> listaPrediosVO = new ArrayList<PredioInfoVO>();
        try {
            List<String> listaNumerosPrediales = this.predioDao.
                obtenerPrediosEditadosGeograficamenteSNC(codigoManzana);
            if (!listaNumerosPrediales.isEmpty() && listaNumerosPrediales != null) {
                for (String numeroPredial : listaNumerosPrediales) {
                    PredioInfoVO predioVO = new PredioInfoVO();
                    predioVO.setNumeroPredial(numeroPredial);
                    listaPrediosVO.add(predioVO);
                }
            }
            return listaPrediosVO;
        } catch (Exception e) {
            throw new Exception("Error obteniendo la lista de predios.");
        }
    }

    /**
     * @author david.cifuentes
     * @see IConservacion#findUnidadConstruccionById(Long)
     */
    @Override
    public UnidadConstruccion findUnidadConstruccionById(Long idUnidadConstruccion) {
        UnidadConstruccion answer = null;
        try {
            answer = this.unidadConstruccionDao.findById(idUnidadConstruccion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#findUnidadConstruccionById: " + ex.getMensaje());
        }
        return answer;
    }

    /**
     * @author david.cifuentes
     * @see IConservacion#buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(Long,
     * UnidadConstruccion)
     */
    @Override
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(
        Long idTramite,
        UnidadConstruccion ucPredioOriginal) {
        List<PUnidadConstruccion> answer = null;
        try {
            answer = this.pUnidadConstruccionDao.
                buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(idTramite, ucPredioOriginal);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#obtenerResolucionODecreto(java.lang.Long)
     * @author leidy.gonzalez:: #12528::26/05/2015 Se modifica por CU_187
     */
    @Override
    public ArrayList<HistoricoAvaluosVO> obtenerResolucionODecreto(Long predioId) {
        HistoricoAvaluosVO historicoAvaluosVO = new HistoricoAvaluosVO();
        ArrayList<HistoricoAvaluosVO> historicosAvaluosVO = new ArrayList<HistoricoAvaluosVO>();
        int contador = 0;

        ArrayList<Tramite> tramitesRealizados =
            (ArrayList<Tramite>) this.tramiteService.buscarTramitesFinalizadosDePredio(predioId);

        ArrayList<VPredioAvaluoDecreto> vPredioAvaluoDecreto = this.formacionService.
            buscarAvaluosCatastralesPorIdPredio(
                predioId);

        if (vPredioAvaluoDecreto != null) {
            for (VPredioAvaluoDecreto vPredioAvaluoDecretoTemp : vPredioAvaluoDecreto) {
                historicoAvaluosVO.setDecretoNumeroOResolucion(vPredioAvaluoDecretoTemp.
                    getDecretoNumero());
                historicoAvaluosVO.setDecretoFechaOResolucionFecha(vPredioAvaluoDecretoTemp.
                    getDecretoFecha());
                historicoAvaluosVO.setValorTotalAvaluoCatastralDecretoOResolucion(
                    vPredioAvaluoDecretoTemp.getValorTotalAvaluoCatastral());
                historicoAvaluosVO.setAutoavaluo(vPredioAvaluoDecretoTemp.getAutoavaluo());
                historicoAvaluosVO.setVigenciaDecretoOResolucion(vPredioAvaluoDecretoTemp.
                    getVigencia());
                historicoAvaluosVO.setResolucion(false);

                historicosAvaluosVO.add(historicoAvaluosVO);
                historicoAvaluosVO = new HistoricoAvaluosVO();
            }

        }

        if (tramitesRealizados != null) {
            for (Tramite tramiteRealizado : tramitesRealizados) {

                if (tramiteRealizado.getResultadoDocumento() != null && tramiteRealizado.
                    getResultadoDocumento().getNumeroDocumento() != null) {

                    historicoAvaluosVO.setDecretoNumeroOResolucion(tramiteRealizado.
                        getResultadoDocumento().getNumeroDocumento());
                    historicoAvaluosVO.setDecretoFechaOResolucionFecha(tramiteRealizado.
                        getResultadoDocumento().getFechaDocumento());

                    if (tramiteRealizado.getPredio().getPredioAvaluoCatastrals() != null) {
                        for (PredioAvaluoCatastral predioAvaluoCatastral : tramiteRealizado.
                            getPredio().getPredioAvaluoCatastrals()) {

                            int comparacionIgual = Double.compare(tramiteRealizado.getPredio().
                                getAvaluoCatastral(), predioAvaluoCatastral.
                                    getValorTotalAvaluoCatastral());

                            if (comparacionIgual == 0) {

                                historicoAvaluosVO.setValorTotalAvaluoCatastralDecretoOResolucion(
                                    predioAvaluoCatastral.getValorTotalAvaluoCatastral());
                                historicoAvaluosVO.setAutoavaluo(predioAvaluoCatastral.
                                    getAutoavaluo());
                                historicoAvaluosVO.setVigenciaDecretoOResolucion(
                                    predioAvaluoCatastral.getVigencia());
                                break;
                            }
                        }
                    }

                    historicoAvaluosVO.setResolucion(true);

                    for (HistoricoAvaluosVO historicoAvaluos : historicosAvaluosVO) {

                        if (historicoAvaluos.getValorTotalAvaluoCatastralDecretoOResolucion().
                            equals(historicoAvaluosVO.
                                getValorTotalAvaluoCatastralDecretoOResolucion()) &&
                            historicoAvaluos.getVigenciaDecretoOResolucion().equals(
                                historicoAvaluosVO.getVigenciaDecretoOResolucion())) {
                            contador++;
                            if (contador == 1) {
                                historicoAvaluos.setDecretoNumeroOResolucion("ACT" +
                                    historicoAvaluos.getDecretoNumeroOResolucion());
                            }
                        }
                    }

                    historicosAvaluosVO.add(historicoAvaluosVO);
                    historicoAvaluosVO = new HistoricoAvaluosVO();
                }
            }
        }
        return historicosAvaluosVO;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#crearJobGenerarResolucionesConservacion(tramite,parametros)
     */
    @Override
    public ProductoCatastralJob crearJobGenerarResolucionesConservacion(Tramite tramite,
        Map<String, String> parametros, Actividad actividad, UsuarioDTO usuarioLogeado) {
        LOGGER.debug("crearJobGenerarResolucionesConservacion para tramite: " + tramite.getId());

        ProductoCatastralJob pcjTemp = new ProductoCatastralJob();
        pcjTemp.setCodigo(actividad + Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO + String.
            valueOf(tramite.getId()));
        pcjTemp.setEstado(ProductoCatastralJob.JPS_ESPERANDO);
        pcjTemp.setFechaLog(new Date());
        pcjTemp.setTipoProducto(ProductoCatastralJob.TIPO_RESOLUCIONES_CONSERVACION);
        pcjTemp.setUsuarioLog(usuarioLogeado.getLogin());
        pcjTemp.setTramiteId(tramite.getId());

        StringBuilder resultado = new StringBuilder();
        resultado.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        resultado.append("<resultado>");
        resultado.append("<parametro ");
        resultado.append("tramiteId=\"" + parametros.get("TRAMITE_ID") + "\" ");
        resultado.append("nombreResponsableConservacion=\"" + parametros.get(
            "NOMBRE_RESPONSABLE_CONSERVACION") + "\" ");
        resultado.append("numeroResolucion=\"" + parametros.get("NUMERO_RESOLUCION") + "\" ");
        resultado.append("fechaResolucion=\"" + parametros.get("FECHA_RESOLUCION") + "\" ");
        resultado.append("fechaResolucionConFormato=\"" + parametros.get(
            "FECHA_RESOLUCION_CON_FORMATO") + "\" ");

        if (parametros.get("NOMBRE_REVISOR") != null) {
            resultado.append("nombreRevisor=\"" + parametros.get("NOMBRE_REVISOR") + "\" ");
        } else {
            resultado.append("nombreRevisor=\"\" ");
        }
        resultado.append("copiaUsoRestringido=\"" + parametros.get("COPIA_USO_RESTRINGIDO") + "\">");
        resultado.append("</parametro>");
        resultado.append("</resultado>");

        pcjTemp.setResultado(resultado.toString());

        pcjTemp = this.productoCatastralJobDAO.update(pcjTemp);

        LOGGER.debug("Job de resoluciones creado : " + pcjTemp.getId() + " y estado: " +
            pcjTemp.getEstado());

        return pcjTemp;
    }

    /**
     * Metodo que transforma de HorarioTerritorialDTO a la entidad HorarioAtencionTerritorial
     *
     * @author lorena.salamanca
     * @param horarioList
     * @param estructuraOrganizacionalCodigo
     * @param usuario
     * @return
     */
    public HorarioAtencionTerritorial horarioTerritorialDTOToHorarioAtencionTerritorial(
        List<HorarioTerritorialDTO> horarioList, String estructuraOrganizacionalCodigo,
        UsuarioDTO usuario) {
        HorarioAtencionTerritorial horarioAtencionTerritorial = new HorarioAtencionTerritorial();
        horarioAtencionTerritorial.setEstructuraOrganizacionalCod(estructuraOrganizacionalCodigo);
        horarioAtencionTerritorial.setUsuarioLog(usuario.getLogin());
        horarioAtencionTerritorial.setFechaLog(new Date());

        for (HorarioTerritorialDTO horario : horarioList) {
            if (horario.getDia().equals(EDias.LUNES.getCodigo())) {
                horarioAtencionTerritorial.setLunesHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setLunesHoraFinalManhana(horario.getHoraFinalManhana());
                horarioAtencionTerritorial.setLunesHoraInicialTarde(horario.getHoraInicialTarde());
                horarioAtencionTerritorial.setLunesHoraFinalTarde(horario.getHoraFinalTarde());
            } else if (horario.getDia().equals(EDias.MARTES.getCodigo())) {
                horarioAtencionTerritorial.setMartesHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setMartesHoraFinalManhana(horario.getHoraFinalManhana());
                horarioAtencionTerritorial.setMartesHoraInicialTarde(horario.getHoraInicialTarde());
                horarioAtencionTerritorial.setMartesHoraFinalTarde(horario.getHoraFinalTarde());
            } else if (horario.getDia().equals(EDias.MIERCOLES.getCodigo())) {
                horarioAtencionTerritorial.setMiercolesHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setMiercolesHoraFinalManhana(horario.
                    getHoraFinalManhana());
                horarioAtencionTerritorial.setMiercolesHoraInicialTarde(horario.
                    getHoraInicialTarde());
                horarioAtencionTerritorial.setMiercolesHoraFinalTarde(horario.getHoraFinalTarde());
            } else if (horario.getDia().equals(EDias.JUEVES.getCodigo())) {
                horarioAtencionTerritorial.setJuevesHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setJuevesHoraFinalManhana(horario.getHoraFinalManhana());
                horarioAtencionTerritorial.setJuevesHoraInicialTarde(horario.getHoraInicialTarde());
                horarioAtencionTerritorial.setJuevesHoraFinalTarde(horario.getHoraFinalTarde());
            } else if (horario.getDia().equals(EDias.VIERNES.getCodigo())) {
                horarioAtencionTerritorial.setViernesHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setViernesHoraFinalManhana(horario.getHoraFinalManhana());
                horarioAtencionTerritorial.setViernesHoraInicialTarde(horario.getHoraInicialTarde());
                horarioAtencionTerritorial.setViernesHoraFinalTarde(horario.getHoraFinalTarde());
            } else if (horario.getDia().equals(EDias.SABADO.getCodigo())) {
                horarioAtencionTerritorial.setSabadoHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setSabadoHoraFinalManhana(horario.getHoraFinalManhana());
                horarioAtencionTerritorial.setSabadoHoraInicialTarde(horario.getHoraInicialTarde());
                horarioAtencionTerritorial.setSabadoHoraFinalTarde(horario.getHoraFinalTarde());
            } else if (horario.getDia().equals(EDias.DOMINGO.getCodigo())) {
                horarioAtencionTerritorial.setDomingoHoraInicialManhana(horario.
                    getHoraInicialManhana());
                horarioAtencionTerritorial.setDomingoHoraFinalManhana(horario.getHoraFinalManhana());
                horarioAtencionTerritorial.setDomingoHoraInicialTarde(horario.getHoraInicialTarde());
                horarioAtencionTerritorial.setDomingoHoraFinalTarde(horario.getHoraFinalTarde());
            }
        }
        return horarioAtencionTerritorial;
    }

    /**
     * Metodo que transforma la entidad HorarioAtencionTerritorial a un HorarioTerritorialDTO
     *
     * @param horarioAtencionTerritorial
     * @return
     */
    public List<HorarioTerritorialDTO> horarioAtencionTerritorialToHorarioTerritorialDTO(
        HorarioAtencionTerritorial horarioAtencionTerritorial) {
        List<HorarioTerritorialDTO> horarioList = new ArrayList<HorarioTerritorialDTO>();

        HorarioTerritorialDTO lunesDTO = new HorarioTerritorialDTO();
        lunesDTO.setDia(EDias.LUNES.getCodigo());
        lunesDTO.setHoraInicialManhana(horarioAtencionTerritorial.getLunesHoraInicialManhana());
        lunesDTO.setHoraFinalManhana(horarioAtencionTerritorial.getLunesHoraFinalManhana());
        lunesDTO.setHoraInicialTarde(horarioAtencionTerritorial.getLunesHoraInicialTarde());
        lunesDTO.setHoraFinalTarde(horarioAtencionTerritorial.getLunesHoraFinalTarde());

        HorarioTerritorialDTO martesDTO = new HorarioTerritorialDTO();
        martesDTO.setDia(EDias.MARTES.getCodigo());
        martesDTO.setHoraInicialManhana(horarioAtencionTerritorial.getMartesHoraInicialManhana());
        martesDTO.setHoraFinalManhana(horarioAtencionTerritorial.getMartesHoraFinalManhana());
        martesDTO.setHoraInicialTarde(horarioAtencionTerritorial.getMartesHoraInicialTarde());
        martesDTO.setHoraFinalTarde(horarioAtencionTerritorial.getMartesHoraFinalTarde());

        HorarioTerritorialDTO miercolesDTO = new HorarioTerritorialDTO();
        miercolesDTO.setDia(EDias.MIERCOLES.getCodigo());
        miercolesDTO.setHoraInicialManhana(horarioAtencionTerritorial.
            getMiercolesHoraInicialManhana());
        miercolesDTO.setHoraFinalManhana(horarioAtencionTerritorial.getMiercolesHoraFinalManhana());
        miercolesDTO.setHoraInicialTarde(horarioAtencionTerritorial.getMiercolesHoraInicialTarde());
        miercolesDTO.setHoraFinalTarde(horarioAtencionTerritorial.getMiercolesHoraFinalTarde());

        HorarioTerritorialDTO juevesDTO = new HorarioTerritorialDTO();
        juevesDTO.setDia(EDias.JUEVES.getCodigo());
        juevesDTO.setHoraInicialManhana(horarioAtencionTerritorial.getJuevesHoraInicialManhana());
        juevesDTO.setHoraFinalManhana(horarioAtencionTerritorial.getJuevesHoraFinalManhana());
        juevesDTO.setHoraInicialTarde(horarioAtencionTerritorial.getJuevesHoraInicialTarde());
        juevesDTO.setHoraFinalTarde(horarioAtencionTerritorial.getJuevesHoraFinalTarde());

        HorarioTerritorialDTO viernesDTO = new HorarioTerritorialDTO();
        viernesDTO.setDia(EDias.VIERNES.getCodigo());
        viernesDTO.setHoraInicialManhana(horarioAtencionTerritorial.getViernesHoraInicialManhana());
        viernesDTO.setHoraFinalManhana(horarioAtencionTerritorial.getViernesHoraFinalManhana());
        viernesDTO.setHoraInicialTarde(horarioAtencionTerritorial.getViernesHoraInicialTarde());
        viernesDTO.setHoraFinalTarde(horarioAtencionTerritorial.getViernesHoraFinalTarde());

        HorarioTerritorialDTO sabadoDTO = new HorarioTerritorialDTO();
        sabadoDTO.setDia(EDias.SABADO.getCodigo());
        sabadoDTO.setHoraInicialManhana(horarioAtencionTerritorial.getSabadoHoraInicialManhana());
        sabadoDTO.setHoraFinalManhana(horarioAtencionTerritorial.getSabadoHoraFinalManhana());
        sabadoDTO.setHoraInicialTarde(horarioAtencionTerritorial.getSabadoHoraInicialTarde());
        sabadoDTO.setHoraFinalTarde(horarioAtencionTerritorial.getSabadoHoraFinalTarde());

        HorarioTerritorialDTO domingoDTO = new HorarioTerritorialDTO();
        domingoDTO.setDia(EDias.DOMINGO.getCodigo());
        domingoDTO.setHoraInicialManhana(horarioAtencionTerritorial.getDomingoHoraInicialManhana());
        domingoDTO.setHoraFinalManhana(horarioAtencionTerritorial.getDomingoHoraFinalManhana());
        domingoDTO.setHoraInicialTarde(horarioAtencionTerritorial.getDomingoHoraInicialTarde());
        domingoDTO.setHoraFinalTarde(horarioAtencionTerritorial.getDomingoHoraFinalTarde());

        horarioList.add(lunesDTO);
        horarioList.add(martesDTO);
        horarioList.add(miercolesDTO);
        horarioList.add(juevesDTO);
        horarioList.add(viernesDTO);
        horarioList.add(sabadoDTO);
        horarioList.add(domingoDTO);

        return horarioList;
    }

    /**
     * @author lorena.salamanca
     * @param estructuraOrganizacionalCodigo
     * @see IConservacion#buscarHorarioAtencionByEstructuraOrganizacional(String)
     */
    @Override
    public List<HorarioTerritorialDTO> buscarHorarioAtencionByEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo) {
        List<HorarioTerritorialDTO> horarioList = new ArrayList<HorarioTerritorialDTO>();
        try {
            HorarioAtencionTerritorial horarioAtencionTerritorial =
                this.horarioAtencionDao.buscarHorarioAtencionByEstructuraOrganizacional(
                    estructuraOrganizacionalCodigo);
            if (horarioAtencionTerritorial != null) {
                horarioList = horarioAtencionTerritorialToHorarioTerritorialDTO(
                    horarioAtencionTerritorial);
            } else {
                horarioList = new ArrayList<HorarioTerritorialDTO>();
                horarioList.add(new HorarioTerritorialDTO(EDias.LUNES.getCodigo()));
                horarioList.add(new HorarioTerritorialDTO(EDias.MARTES.getCodigo()));
                horarioList.add(new HorarioTerritorialDTO(EDias.MIERCOLES.getCodigo()));
                horarioList.add(new HorarioTerritorialDTO(EDias.JUEVES.getCodigo()));
                horarioList.add(new HorarioTerritorialDTO(EDias.VIERNES.getCodigo()));
                horarioList.add(new HorarioTerritorialDTO(EDias.SABADO.getCodigo()));
                horarioList.add(new HorarioTerritorialDTO(EDias.DOMINGO.getCodigo()));
            }
        } catch (Exception e) {
            LOGGER.error("Error al buscar el horario de la territorial o UOC " +
                estructuraOrganizacionalCodigo + e);
        }
        return horarioList;
    }

    /**
     * @author lorena.salamanca
     * @param estructuraOrganizacionalCodigo
     * @return
     * @see IConservacion#buscarHorarioAtencionByEstructuraOrganizacionalCodigo(String)
     */
    @Override
    public HorarioAtencionTerritorial buscarHorarioAtencionByEstructuraOrganizacionalCodigo(
        String estructuraOrganizacionalCodigo) {
        try {
            HorarioAtencionTerritorial horarioAtencionTerritorial =
                this.horarioAtencionDao.buscarHorarioAtencionByEstructuraOrganizacional(
                    estructuraOrganizacionalCodigo);
            if (horarioAtencionTerritorial != null) {
                return horarioAtencionTerritorial;
            }
        } catch (Exception e) {
            LOGGER.error("Error al buscar el horario de la territorial o UOC " +
                estructuraOrganizacionalCodigo + e);
        }
        return null;
    }

    /**
     * @author lorena.salamanca
     * @param horarioList
     * @param estructuraOrganizacionalCodigo
     * @see IConservacion#guardarActualizarHorarioAtencion(List, String, UsuarioDTO)
     */
    @Override
    public void guardarActualizarHorarioAtencion(List<HorarioTerritorialDTO> horarioList,
        String estructuraOrganizacionalCodigo, UsuarioDTO usuario) {
        try {
            HorarioAtencionTerritorial horarioAtencionTerritorial =
                horarioTerritorialDTOToHorarioAtencionTerritorial(horarioList,
                    estructuraOrganizacionalCodigo, usuario);
            this.horarioAtencionDao.update(horarioAtencionTerritorial);
        } catch (Exception e) {
            LOGGER.error("Error al guardar el horario de la territorial o UOC " +
                estructuraOrganizacionalCodigo + e);
        }
    }

    /**
     * @author lorena.salamanca
     * @param horarioList
     * @param estructuraOrganizacionalCodigo
     * @param usuario
     * @see IConservacion#eliminarHorarioAtencion(List, String, UsuarioDTO)
     */
    @Override
    public void eliminarHorarioAtencion(List<HorarioTerritorialDTO> horarioList,
        String estructuraOrganizacionalCodigo, UsuarioDTO usuario) {
        try {
            HorarioAtencionTerritorial horarioAtencionTerritorial =
                horarioTerritorialDTOToHorarioAtencionTerritorial(horarioList,
                    estructuraOrganizacionalCodigo, usuario);
            this.horarioAtencionDao.delete(horarioAtencionTerritorial);
        } catch (Exception e) {
            LOGGER.error("Error al eliminar el horario de la territorial o UOC " +
                estructuraOrganizacionalCodigo + e);
        }
    }

    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IConservacionLocal#procesarResolucionesConservacionEnJasperServer
     * @author leidy.gonzalez
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarResolucionesConservacionEnJasperServer(ProductoCatastralJob job,
        int contador) {

        LOGGER.debug("procesarResolucionesConservacionEnJasperServer");
        boolean resultadoOk = true;
        boolean errorGeneracionReporte = false;
        boolean noPrediosProyectados = false;
        Tramite tramite = new Tramite();
        EReporteServiceSNC enumeracionReporte = null;
        String nombreResponsableConservacion;

        Parametro maxEnvioJob = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());
        UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());

        try {
            contador++;

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService
                .buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            Long tramiteId = job.getTramiteId();

            //Se consulta si el Tramite tiene Predios Proyectados
            List<PPredio> pPrediosResolucion = this.pPredioDao
                .findExistingPPrediosByTramiteId(tramiteId);

            LOGGER.debug("pPrediosResolucion:" + pPrediosResolucion);

            tramite = tramiteService.buscarTramitePorId(tramiteId);

            if (!pPrediosResolucion.isEmpty() || (tramite.isViaGubernativa() &&
                pPrediosResolucion.isEmpty())) {

                LOGGER.debug("buscarTramitePorId:" + tramiteId);

                if (tramite != null && tramite.getTipoTramite() != null) {
                    if (tramite.getSubtipo() == null) {
                        tramite.setSubtipo("");
                    }
                    if (tramite.getRadicacionEspecial() == null) {
                        tramite.setRadicacionEspecial("");
                    }

                    enumeracionReporte = tramiteService.obtenerUrlReporteResoluciones(tramite.
                        getId());

                    String urlReporte = enumeracionReporte.getUrlReporte();

                    LOGGER.debug("tipo Reporte:" + urlReporte);

                    Reporte reporte = new Reporte(usuario, estructuraOrganizacional);

                    reporte.setEstructuraOrganizacional(estructuraOrganizacional);

                    reporte.setUrl(urlReporte);

                    List<String> resultados = this.
                        obtenerParametrosDeProductoCatastralJobResolucionesConservacion(job);

                    String numeroDocumento = resultados.get(2);

                    reporte.addParameter("TRAMITE_ID", (resultados.get(0) != null) ? resultados.get(
                        0) : "");
                    LOGGER.debug("TRAMITE_ID:" + resultados.get(0));
                    nombreResponsableConservacion =
                        (resultados.get(1) != null) ? resultados.get(1) : "";
                    reporte.addParameter("NOMBRE_RESPONSABLE_CONSERVACION",
                        nombreResponsableConservacion);
                    LOGGER.debug("nombreResponsableConservacion:" + resultados.get(1));
                    reporte.addParameter("NUMERO_RESOLUCION", (resultados.get(2) != null) ?
                        resultados.get(2) : "");
                    LOGGER.debug("NUMERO_RESOLUCION:" + resultados.get(2));
                    reporte.addParameter("FECHA_RESOLUCION", (resultados.get(3) != null) ?
                        resultados.get(3) : "");
                    LOGGER.debug("FECHA_RESOLUCION:" + resultados.get(3));
                    reporte.addParameter("FECHA_RESOLUCION_CON_FORMATO",
                        (resultados.get(4) != null) ? resultados.get(4) : "");
                    LOGGER.debug("FECHA_RESOLUCION_CON_FORMATO:" + resultados.get(4));

                    if (resultados.get(5) != null && !resultados.get(5).isEmpty()) {
                        reporte.addParameter("NOMBRE_REVISOR", resultados.get(5));
                        LOGGER.debug("NOMBRE_REVISOR:" + resultados.get(5));
                    }

                    if (resultados.get(6) != null && !resultados.get(6).isEmpty()) {
                        reporte.addParameter("FIRMA_USUARIO", resultados.get(6));
                        LOGGER.debug("FIRMA_USUARIO:" + resultados.get(6));
                    }

                    if (tramite.isTipoTramiteViaGubernativaModificado() ||
                        tramite.isTipoTramiteViaGubModSubsidioApelacion()) {

                        if (!tramite.getNumeroRadicacionReferencia().isEmpty()) {

                            Tramite tramiteReferencia = tramiteService.
                                buscarTramitePorNumeroRadicacion(tramite.
                                    getNumeroRadicacionReferencia());

                            LOGGER.debug("tramiteReferencia:" + tramiteReferencia);

                            if (tramiteReferencia != null) {
                                reporte.addParameter("NOMBRE_SUBREPORTE_RESUELVE", tramiteService.
                                    obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.
                                        getId()));
                            }
                        }

                    }

                    /*
                     * En este punto se realiza la generación de la firma escaneada @modified by
                     * javier.aponte 12/09/2016 #19708
                     */
                    if (nombreResponsableConservacion != null &&
                        !nombreResponsableConservacion.trim().isEmpty()) {

                        FirmaUsuario firma;
                        String documentoFirma;

                        firma = this.generalesService.buscarDocumentoFirmaPorUsuario(
                            nombreResponsableConservacion);
                        LOGGER.debug(" Consulta FIRMA:" + firma);

                        if (firma != null && nombreResponsableConservacion.equals(firma.
                            getNombreFuncionarioFirma())) {

                            documentoFirma = this.generalesService.
                                descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                                    getIdRepositorioDocumentos());
                            LOGGER.debug("Descarga documentoFirma:" + documentoFirma);

                            reporte.addParameter("FIRMA_USUARIO", documentoFirma);
                        }
                    }

                    reporte.addParameter("COPIA_USO_RESTRINGIDO", (resultados.get(6) != null) ?
                        resultados.get(6) : "");
                    LOGGER.debug("COPIA_USO_RESTRINGIDO:" + resultados.get(6));

                    LOGGER.debug("Numero resolucion:" + numeroDocumento);

                    tramite = this.generarArchivoResolucionesConservacion(usuario, reporte,
                        numeroDocumento, tramite, job, contador);
                    LOGGER.debug("Resolucion almacenada en tramite:" + tramite.getId() +
                        "con documento: " +
                        tramite.getResultado());

                } else {
                    LOGGER.error("No se encontro tramite para el job:" + job);
                    String message =
                        "Ocurrió un error durante la generación del producto catastral job para resoluciones de conservacion";
                    throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                        message);
                }

            } else {
                this.productoCatastralJobDAO.actualizarEstado(job,
                    ProductoCatastralJob.JPS_TERMINADO);
                noPrediosProyectados = true;
                return true;
            }

        } catch (ExcepcionSNC e) {

            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + e.getMensaje(), e);

            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                this.procesarResolucionesConservacionEnJasperServer(job, contador);
            } else {
                resultadoOk = false;
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
            }
        } catch (Exception ex) {
            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + ex.getMessage(), ex);

            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                errorGeneracionReporte = !this.procesarResolucionesConservacionEnJasperServer(job,
                    contador);
            } else {
                resultadoOk = false;
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                return false;
            }
        }

        if (errorGeneracionReporte) {
            return false;
        }

        if (noPrediosProyectados) {
            return true;
        }
        //Se ejecuta el Procedimiento de Aplicar Cambios
        try {

            Actividad actividad = new Actividad();
            String actividadId = null;

            int indiceSeparador = job.getCodigo().indexOf(
                Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO);

            if (indiceSeparador > 0) {
                actividadId = job.getCodigo().substring(0, indiceSeparador);
                actividad.setId(actividadId);
            }

            if (actividad != null && actividad.getId() != null &&
                job != null && job.getEstado().equals(ProductoCatastralJob.JPS_TERMINADO) &&
                tramite != null &&
                tramite.getResultadoDocumento() != null &&
                tramite.getResultadoDocumento().getId() != null) {

                LOGGER.debug("actividad:" + actividad);

                this.avanzarTramiteDeGenerarResolucionProcess(tramite, usuario, actividad);
                resultadoOk = true;

            } else {
                LOGGER.error("No se encontro actividad para el tramite_id del job:" + job);
            }

        } catch (Exception e) {

            LOGGER.error(
                "Ocurrió un error durante el Procesamiento de Aplicar Cambios o Avance de la Actividad para resoluciones de conservacion" +
                " para el tramite_id del job:" + job + ": "+e.getMessage(), e);
        }

        return resultadoOk;
    }

    /**
     * Método para obtener una lista de parametros a partir de los resultados de un job para las
     * resoluciones de conservacion
     *
     * @author leidy.gonzalez
     */
    public List<String> obtenerParametrosDeProductoCatastralJobResolucionesConservacion(
        ProductoCatastralJob productoGenerado) {
        List<String> resultados = new ArrayList<String>();
        try {
            if (productoGenerado.getResultado().contains("<?xml")) {
                LOGGER.debug(productoGenerado.getResultado());
                org.dom4j.Document document = (org.dom4j.Document) DocumentHelper.parseText(
                    productoGenerado.getResultado());
                List parametro = ((Node) document).selectNodes("//parametro");
                for (Iterator iter = parametro.iterator(); iter.hasNext();) {
                    Element productoXml = (Element) iter.next();
                    resultados.add(productoXml.attributeValue("tramiteId"));
                    resultados.add(productoXml.attributeValue("nombreResponsableConservacion"));
                    resultados.add(productoXml.attributeValue("numeroResolucion"));
                    resultados.add(productoXml.attributeValue("fechaResolucion"));
                    resultados.add(productoXml.attributeValue("fechaResolucionConFormato"));
                    resultados.add(productoXml.attributeValue("nombreRevisor"));
                    resultados.add(productoXml.attributeValue("firmaUsuario"));
                    resultados.add(productoXml.attributeValue("copiaUsoRestringido"));
                }

            } else {
                LOGGER.error(productoGenerado.getResultado());
                String message =
                    "Ocurrió un error durante la generación del producto catastral job para resoluciones de conservacion";
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    message);
            }
        } catch (DocumentException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultados;
    }

    /**
     * Método para generar el archivo de las resolciones de conservacion
     *
     * @author leidy.gonzalez
     */
    private Tramite generarArchivoResolucionesConservacion(UsuarioDTO usuario, Reporte reporte,
        String numeroDocumento, Tramite tramite, ProductoCatastralJob job, int contador) {

        Documento documento = null;
        List<TramiteDocumento> tramiteDocumentos = new ArrayList<TramiteDocumento>();
        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        File archivoResolucionesConservacion = null;
        IReporteService reportesService = ReporteServiceFactory.getService();

        List<Documento> documentos = this.documentoDao.
            buscarPorNumeroDocumentoYTipo(numeroDocumento,
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
        if (documentos != null && !documentos.isEmpty()) {
            documento = documentos.get(0);
            LOGGER.debug("documento resolucion:" + documento.getId());
        } else {

            documentos = this.documentoDao.buscarPorNumeroDocumentoYTipo(numeroDocumento,
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                    getId());

            if (documentos != null && !documentos.isEmpty()) {
                documento = documentos.get(0);
            }
        }

        if (documento == null) {
            this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
            LOGGER.error(
                "generarArchivoResolucionesConservacion#Error al consultar la resolución: " +
                numeroDocumento);
            return null;

        } else {

            //Consulta si existe TramiteDocumento Asociado al documento y si no lo crea
            tramiteDocumentos = this.tramiteDocumentoDao.consultaTramiteDocumentosPorDocumentoId(
                documento.getId());

            if (tramiteDocumentos != null && !tramiteDocumentos.isEmpty()) {
                //Se envia Registro de Tramite Documentacion Consultado
                tramiteDocumento = tramiteDocumentos.get(0);
                LOGGER.debug("tramiteDocumento:" + tramiteDocumento.getId());

            } else {
                //Se genera Registro de Tramite Documentacion por primera vez
                tramiteDocumento.setTramite(tramite);
                tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
                tramiteDocumento.setUsuarioLog(usuario.getLogin());
                LOGGER.debug("tramiteDocumento:" + tramiteDocumento);
            }

            LOGGER.debug("Parametros de reporte:" + reporte.getParametros() +
                "Url Reporte: " + reporte.getUrl());

            archivoResolucionesConservacion = reportesService.getReportAsFile(reporte,
                new LogMensajesReportesUtil(this.generalesService, usuario));

            LOGGER.debug("archivoResolucionesConservacion:" + archivoResolucionesConservacion);

            if (archivoResolucionesConservacion != null && !archivoResolucionesConservacion.
                getAbsolutePath().isEmpty() &&
                documento != null) {
                LOGGER.debug(
                    "TareaGenerarArchivoResolucionesConservacion: Reporte Resoluciones Conservacion generado exitosamente" +
                    archivoResolucionesConservacion.getAbsolutePath());

                tramite = this.aplicarCambios.guardaDocumentoTramiteResoluciones(tramite,
                    tramiteDocumento, documento, usuario, archivoResolucionesConservacion, job,
                    contador);
                LOGGER.debug("Aplica Cambios, job:" + job);

            } else {
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
                LOGGER.error("No se pudo generar el archivo de resoluciones para el tramite_id:" +
                    tramite.getId());
                return null;
            }
        }

        return tramite;
    }

    /**
     * Método para avanzar en process el tramite A Comunicar Notificacion o Aplicar Cambios
     *
     * @author leidy.gonzalez
     */
    public void avanzarTramiteDeGenerarResolucionProcess(Tramite tramite,
        UsuarioDTO usuario, Actividad actividad) {

        // Parametro Tramites Conservacion basado en las siguientes condiciones:
        // Tipo Tramite, Clase Mutacion, Subtipo, Tipo Radicacion Especial y
        // Tramite de Actualizacion
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        String notificaActualizacion;

        Parametro notificaTramitesConservacionParam = this.generalesService
            .getCacheParametroPorNombre(EParametro.TRAMITES_CONSERVACION_NOTIFICAN.toString());
        String[] listaParametrosConservacionNotifican = null;

        if (notificaTramitesConservacionParam.getValorCaracter() != null) {
            listaParametrosConservacionNotifican = notificaTramitesConservacionParam
                .getValorCaracter().split(",");
        }

        String tramitesComunicanNotificacion = "";

        if (tramite.getTipoTramite() != null) {

            int contadorTramiteNotificanRectificacion = 0;

            if (!tramite.isCancelacionMasiva()) {

                tramitesComunicanNotificacion = tramite.getTipoTramite();
            }

            if (tramite.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()) &&
                tramite.getTramiteRectificacions() != null) {

                for (TramiteRectificacion tr : tramite.getTramiteRectificacions()) {

                    if (tr.getDatoRectificar() != null && EDatoRectificarTipo.PREDIO.toString().
                        equalsIgnoreCase(tr.getDatoRectificar().getTipo())) {

                        if (tramite.isRectificacionMatriz() &&
                            EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().
                                equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

                            tramitesComunicanNotificacion = tramitesComunicanNotificacion + "-" +
                                tr.getDatoRectificar().getNombre() + "-" + tramite.
                                getRadicacionEspecial();
                        }

                        if (EDatoRectificarNombre.AREA_TERRENO.getCodigo().equalsIgnoreCase(tr.
                            getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.AREA_CONSTRUIDA.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.COEFICIENTE.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.DIMENSION.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.ZONA_FISICA.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.ZONA_GEOECONOMICA.getCodigo().equalsIgnoreCase(
                                tr.getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().
                                equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

                            //@modified by leidy.gonzalez:: #16317::08/03/2016 Modifica tramitesComunicanNotificacion para cuando son rectificacion,
                            // debido a que puede existir mas de un tipo de rectificacion a notificar
                            if (contadorTramiteNotificanRectificacion >= 1) {
                                tramitesComunicanNotificacion =
                                    tramitesComunicanNotificacion + "," + tramite.getTipoTramite() +
                                    "-" + tr.getDatoRectificar().getNombre();

                            } else {

                                tramitesComunicanNotificacion =
                                    tramitesComunicanNotificacion + "-" + tr.getDatoRectificar().
                                        getNombre();
                                contadorTramiteNotificanRectificacion++;
                            }

                        }

                    }
                    continue;
                }

            }

            if (tramite.getClaseMutacion() != null) {

                tramitesComunicanNotificacion = tramitesComunicanNotificacion +
                    "-" + tramite.getClaseMutacion();

                if ((tramite.getSubtipo() != null && !tramite.getSubtipo().equals("")) &&
                    (tramite.getRadicacionEspecial() != null && !tramite.getRadicacionEspecial().
                    equals(""))) {

                    tramitesComunicanNotificacion = tramitesComunicanNotificacion + "-" +
                        tramite.getSubtipo() + "-" +
                        tramite.getRadicacionEspecial();

                }

            }

            //@modified by leidy.gonzalez:: #16317::08/03/2016
            //Tramites de Rectificacion van a Comunicar Notificacion
            if (tramite.getTipoTramite().equals("2")) {
                String[] rectificacionSplit = tramitesComunicanNotificacion.split(",");
                String rectificanSeparado;

                for (int i = 0; i < listaParametrosConservacionNotifican.length; i++) {
                    notificaActualizacion = listaParametrosConservacionNotifican[i];

                    for (int j = 0; j < rectificacionSplit.length; j++) {
                        rectificanSeparado = rectificacionSplit[j];

                        if (rectificanSeparado.equals(notificaActualizacion)) {

                            // Avance a comunicar Notificacion Process
                            solicitudCatastral.setUsuarios(this.tramiteService
                                .buscarFuncionariosPorRolYTerritorial(
                                    usuario.getDescripcionEstructuraOrganizacional(),
                                    ERol.SECRETARIA_CONSERVACION));
                            solicitudCatastral
                                .setTransicion(
                                    ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION);

                            procesosService.avanzarActividad(usuario, actividad.getId(),
                                solicitudCatastral);

                            break;

                        }
                    }
                }

            } //Se quita que los tramites de cancelacion vayan a comunicar norificacion
            else {

                for (int i = 0; i < listaParametrosConservacionNotifican.length; i++) {
                    notificaActualizacion = listaParametrosConservacionNotifican[i];

                    if (tramitesComunicanNotificacion.equals(notificaActualizacion)) {

                        // Avance a comunicar Notificacion Process
                        solicitudCatastral.setUsuarios(this.tramiteService
                            .buscarFuncionariosPorRolYTerritorial(
                                usuario.getDescripcionEstructuraOrganizacional(),
                                ERol.SECRETARIA_CONSERVACION));
                        solicitudCatastral
                            .setTransicion(
                                ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION);

                        procesosService.avanzarActividad(usuario, actividad.getId(),
                            solicitudCatastral);

                        break;

                    }
                }

            }
            /*
             * @modified by leidy.gonzalez Se elimina validacion de cambio de estado del tramite,
             * debido a que esta se realizar en el metodo de aplicar cambios.
             */
            if (solicitudCatastral.getTransicion() == null) {

                // Si no se encuentran dentro de las
                // validaciones anteriores los tramites pasan a
                // Avanzar e aplicar Cambios
                // Si no se encuentran dentro de las
                // validaciones anteriores los tramites pasan a
                // Aplicar Cambios
                //inicia la aplicacion de cambios
                tramite.setActividadActualTramite(actividad);
                LOGGER.debug("on AplicacionCambiosMB#aplicarCambiosInicio ya tiene Actividad");

                //se marca el tramite como en aplicar cambios
                tramite.setEstadoGdb(Constantes.ACT_APLICAR_CAMBIOS);

                if (!tramite.getEstadoGdb().equals(Constantes.ACT_APLICAR_CAMBIOS)) {
                    LOGGER.error(
                        "No se pudo Actualizar el tramite");

                } else if (tramite.getEstadoGdb().equals(Constantes.ACT_APLICAR_CAMBIOS)) {

                    if (tramite.isTramiteGeografico()) {

                        conservacionService.aplicarCambiosConservacion(tramite, usuario, 0);

                    } else if (!tramite.isTramiteGeografico()) {

                        conservacionService.aplicarCambiosConservacion(tramite, usuario, 1);
                    }
                }

            }

        }
    }

    /**
     * @see IConservacion#obtenerPrediosCanceladosQuintaCondominioPH(java.lang.String)
     * @author lorena.salamanca
     * @return
     */
    @Override
    public List<Predio> obtenerPrediosCanceladosQuintaCondominioPH(
        String numeroPredialFichaMatriz) {
        return this.predioDao
            .obtenerPrediosCanceladosQuintaCondominioPH(numeroPredialFichaMatriz);
    }

    /**
     * @see IConservacionLocal#buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(String, Long)
     * @author david.cifuentes
     */
    @Override
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(
        String estadoPredio, Long fichaMatrizId) {
        List<PFichaMatrizPredio> answer = null;
        try {
            answer = this.pFichaMatrizPredioDao.buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(
                estadoPredio, fichaMatrizId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarPFichaMatrizPredioPorIdFichaYEstadoPredio: " + ex.
                    getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(String, Long)
     * @author david.cifuentes
     */
    @Override
    public List<HFichaMatrizPredio> buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(
        String estado, Long idFichaMatriz) {
        List<HFichaMatrizPredio> answer = null;
        try {
            answer = this.hFichaMatrizPredioDao.buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(
                estado, idFichaMatriz);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarHistoricoFichaMatrizPredioPorIdFichaYEstado: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarHPredioPorNumeroPredialYEstado(String, String)
     * @author david.cifuentes
     */
    @Override
    public HPredio buscarHPredioPorNumeroPredialYEstado(String numeroPredial, String estado) {
        HPredio answer = null;
        try {
            answer = this.hPredioDao.buscarHPredioPorNumeroPredialYEstado(numeroPredial, estado);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarHPredioPorNumeroPredial: " + ex.
                getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#agregarPPredio(PPredio)
     * @author lorena.salamanca
     */
    @Override
    public void agregarPPredio(PPredio pPredio) {
        this.pPredioDao.persist(pPredio);
    }

    @Override
    public List<PPredio> findPPrediosCompletosActivosByTramiteId(Long idTramite) {
        return this.pPredioDao
            .findPPrediosCompletosActivosByTramiteId(idTramite);
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#buscarPPredioPorTramite(buscarPPredioPorTramite(Tramite)
     * @author david.cifuentes
     */
    public PPredio buscarPPredioPorTramite(Tramite tramite) {
        PPredio answer = null;
        try {
            if ((tramite.getPredio() != null && !tramite.isSegunda()) ||
                tramite.isQuintaNuevo() ||
                tramite.isQuintaOmitidoNuevo()) {

                if (tramite.isQuintaNuevo() || tramite.isQuintaOmitidoNuevo()) {
                    answer = this.obtenerPPredioCompletoByIdTramite(tramite.getId());
                } else {
                    answer = this.obtenerPPredioCompletoByIdyTramite(tramite.getPredio().getId(),
                        tramite.getId());
                }
            } else if (tramite.isSegundaEnglobe()) {

                answer = this.obtenerPPredioCompletoByIdTramite(tramite.getId());
            } else if (tramite.isSegundaDesenglobe()) {

                List<PPredio> pPrediosDesenglobe = this.
                    buscarPPrediosCompletosPorTramiteIdPHCondominio(tramite.getId());

                // Se toma el primero no cancelado
                if (pPrediosDesenglobe != null && !pPrediosDesenglobe.isEmpty()) {
                    for (PPredio p : pPrediosDesenglobe) {
                        if (p.getCancelaInscribe() == null ||
                            !p.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.
                                getCodigo())) {
                            answer = p;
                            break;
                        }
                    }
                }
                if (answer != null) {
                    answer = this.obtenerPPredioCompletoById(answer.getId());
                } else if (tramite.getPredio() != null &&
                    !tramite.isSegundaDesenglobe()) {
                    answer = this.obtenerPPredioCompletoById(tramite.getPredio().getId());
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPPredioPorTramite: " + ex.getMensaje());
        } catch (Exception e) {
            LOGGER.error("Error en ConservacionBean#buscarPPredioPorTramite: " + e.getMessage());
        }
        return answer;
    }

    /**
     * @see IPPredioDAO#obtenerPredioConNumeroPredialModificadoPorTamite(java.lang.Long)
     * @author javier.aponte
     */
    public TreeMap<String, Boolean> obtenerPredioConNumeroPredialModificadoPorTamite(Long idTramite) {

        TreeMap<String, Boolean> resultado = null;

        try {
            resultado = this.pPredioDao.obtenerPredioConNumeroPredialModificadoPorTamite(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#obtenerPredioConNumeroPredialModificadoPorTamite: " +
                ex.getMensaje());
        }

        return resultado;

    }

    /**
     * @author jonathan.chacon
     * @param unidad
     * @see IConservacionLocal#actualizarPUnidadDeConstruccion( PUnidadConstruccion unidad)
     */
    @Override
    public PUnidadConstruccion actualizarPUnidadDeConstruccion(
        PUnidadConstruccion unidad) {
        return this.pUnidadConstruccionDao
            .update(unidad);
    }

    /**
     * @author jonathan.chacon
     * @param anio
     * @return
     */
    @Override
    public DecretoAvaluoAnual buscarDecretoPorVIgencia(String anio) {

        DecretoAvaluoAnual answer = new DecretoAvaluoAnual();

        try {
            answer = decretoAvaluoAnualDAO.buscarDecretoPorVigencia(anio);
        } catch (Exception e) {
            LOGGER.debug("Error en metodo buscarDecretoPorVIgencia " + e);
        }
        return answer;
    }

    /**
     * @author jonathan.chacon
     * @param vigencia
     * @param predioId
     * @return
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public double obtenerPorcentajeIncrementoVigenciaPredio(String vigencia, long predioId) {
        double answer = 0.0;
        try {

            answer = this.sncProcedimientoDao.CalcularPorcentajeIncrementoPredio(vigencia, predioId);

        } catch (Exception e) {
            LOGGER.debug("Error en metodo obtenerPorcentajeIncrementoVigenciaPredio " + e);
        }
        return answer;
    }

    //--------------------------------------------------------------------------//
    /**
     * @see IConservacionLocal#buscarPredioCompletoPorPredioId(Long)
     * @author david.cifuentes
     */
    @Override
    public Predio buscarPredioCompletoPorPredioId(Long predioId) {

        Predio answer = null;
        try {
            answer = predioDao.buscarPredioCompletoPorPredioId(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPredioCompletoPorPredioId: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#obtenerDireccionesPredio(Predio)
     * @author felipe.cadena
     */
    @Override
    public List<PredioDireccion> obtenerDireccionesPredio(Predio predio) {

        List<PredioDireccion> result = new ArrayList<PredioDireccion>();

        try {
            result = predioDireccionDao.getPredioDireccionByPredio(predio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerDireccionesPredio: " +
                ex.getMensaje());
        }
        return result;

    }

    /**
     * @see IConservacion#contarLinderosPorNumeroPredial(java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public Integer contarLinderosPorNumeroPredial(String numeroPredial) {
        LOGGER.debug("en ConservacionBean#contarLinderosPorNumeroPredial");
        int answer;
        answer = this.predioLinderoDAO.contarLinderosPorNumeroPredial(numeroPredial);

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IConservacion#actualizarPFotos(List<PFoto>)
     */
    @Override
    public List<PFoto> actualizarPFotos(List<PFoto> pFotos) {

        try {
            for (PFoto pf : pFotos) {
                pf = this.pFotoDao.update(pf);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#actualizarPFotos: " +
                ex.getMensaje());
        }
        return pFotos;
    }

    /**
     * Método que realiza la búsqueda de un predio por su id, pero no realiza fetch sobre ninguna
     * tabla, en vez de ello realiza la inicialización de los campos a través de un
     * Hibernate.inicialize
     *
     * @param idPredio
     * @return
     * @throws ExcepcionSNC
     */
    @Override
    public PPredio buscarPredioCompletoPorIdSinFetch(Long idPredio)
        throws ExcepcionSNC {
        PPredio pPredio = this.pPredioDao
            .buscarPredioCompletoPorIdSinFetch(idPredio);
        // Asociar las PUnidadConstruccion
        List<PUnidadConstruccion> pUnidadConstruccions = this.pUnidadConstruccionDao
            .buscarUnidadesDeConstruccionPorPPredioId(idPredio, true);
        if (pUnidadConstruccions != null && !pUnidadConstruccions.isEmpty()) {
            pPredio.setPUnidadConstruccions(pUnidadConstruccions);
        }
        return pPredio;
    }

    /*
     * @author javier.aponte @see IConservacion#establecerAnioYNumeroUltimaResolucion(Long
     * tramiteId, int anioUltimaResolucion, String numeroUltimaResolucion)
     */
    public Object[] establecerAnioYNumeroUltimaResolucion(Long tramiteId,
        int anioUltimaResolucion, String numeroUltimaResolucion, UsuarioDTO usuario) {
        Object[] errors = null;
        try {
            errors = sncProcedimientoDao.establecerAnioYNumeroUltimaResolucion(tramiteId,
                anioUltimaResolucion, numeroUltimaResolucion, usuario);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#buscarEntidadesPorNombreYEstado(String, String)
     * @author david.cifuentes
     */
    @Override
    public List<Entidad> buscarEntidadesPorNombreYEstado(String nombreEntidad,
        String estadoEntidad) {
        List<Entidad> answer = null;
        try {
            answer = this.entidadDao.buscarEntidadesPorNombreYEstado(nombreEntidad, estadoEntidad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarEntidadesPorNombreYEstado: " + ex);
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#buscarEntidades(String, String, String, String)
     * @author dumar.penuela
     */
    @Override
    public List<Entidad> buscarEntidades(String nombreEntidad,
        String estadoEntidad, String departamento, String municipio) {
        List<Entidad> answer = null;
        try {
            answer = this.entidadDao.buscarEntidades(nombreEntidad, estadoEntidad, departamento,
                municipio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarEntidades: " + ex);
        }
        return answer;
    }
    // ---------------------------------------------------------------------- //

    /**
     * @see IConservacionLocal#guardarEntidad(Entidad)
     * @author david.cifuentes
     */
    @Override
    public Entidad guardarEntidad(Entidad entidad) {
        Entidad answer = null;
        try {
            answer = this.entidadDao.update(entidad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#guardarEntidad: " + ex);
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#existeAsociacionEntidadBloqueo(Entidad)
     * @author david.cifuentes
     */
    @Override
    public boolean existeAsociacionEntidadBloqueo(Entidad entidad) {
        boolean answer = false;
        try {
            answer = this.predioBloqueoDao.existeAsociacionEntidadBloqueo(entidad);
            if (!answer) {
                answer = this.personaBloqueoDao.existeAsociacionEntidadBloqueo(entidad);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#existeAsociacionEntidadBloqueo: " + ex);
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#eliminarEntidad(Entidad)
     * @author david.cifuentes
     */
    @Override
    public void eliminarEntidad(Entidad entidad) {
        try {
            this.entidadDao.delete(entidad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#eliminarEntidad: " +
                ex);
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarEntidadesPorEstado(String)
     * @author david.cifuentes
     */
    @Override
    public List<Entidad> buscarEntidadesPorEstado(String estado) {
        try {
            return this.entidadDao.buscarEntidadesPorEstado(estado);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarEntidadesPorEstado: " +
                ex);
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#findEntidadById(Long)
     * @author david.cifuentes
     */
    @Override
    public Entidad findEntidadById(Long idEntidadBloqueo) {
        try {
            return this.entidadDao.findById(idEntidadBloqueo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#findEntidadById: " +
                ex);
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarEntidad(String,String,String,String,String)
     * @author david.cifuentes
     */
    @Override
    public List<PPersonaPredio> buscarPPersonaPrediosPorIdPredios(List<Long> predioIds) {
        LOGGER.debug("en ConservacionBean#buscarPPersonaPrediosPorIdPredios ");
        List<PPersonaPredio> answer = null;

        try {
            answer = this.pPersonaPredioDao.findByIdPPredios(predioIds);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPersonaPrediosPorIdPredios: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#buscarEntidad(String,String,String,String,String)
     * @author david.cifuentes
     */
    @Override
    public PPredio obtenerPPredioPorNumeroPredialConTramite(String numeroPredial) {

        PPredio answer = null;
        try {
            answer = this.pPredioDao.obtenerPorNumeroPredialConTramite(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPPredioPorNumeroPredialConTramite: " +
                ex.getMensaje(), ex);
        }
        return answer;
    }

    @Override
    public Entidad buscarEntidad(String nombreEntidad, String codDepartamento, String codMunicipio,
        String correo) {
        try {
            return this.entidadDao.buscarEntidad(nombreEntidad, codDepartamento, codMunicipio,
                correo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarEntidad: " +
                ex);
        }
        return null;
    }

    /**
     * @see IConservacion#actualizarPPersonaPredioYPropiedadCancelados(PersonaPredio[],
     * List<PPersonaPredioPropiedad>)
     * @author leidy.gonzalez
     */
    @Override
    public boolean validarExistenciaYActivosDePredios(List<String> numeros) {

        boolean resultado = false;
        try {

            resultado = this.predioDao.validarExistenciaYActivos(numeros);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#validarExistenciaYActivosDePredios: " +
                ex.getMensaje());
        }
        return resultado;

    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerAxMunicipioEnActualizacion
     */
    @Override
    public List<AxMunicipio> obtenerAxMunicipioEnActualizacion() {

        List<AxMunicipio> resultado = null;
        try {

            resultado = this.axMunicipioDAO.obtenerMunicipiosEnActualizacion();

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerAxMunicipioEnActualizacion: " +
                ex.getMensaje());
        }
        return resultado;

    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerAxMunicipioEnActualizacion
     */
    /**
     * @author felipe.cadena
     * @see IConservacion#findSolicitudByNumSolicitud(String)
     */
    @Override
    public Solicitud findSolicitudByNumSolicitud(String numeroSolicitud) {
        Solicitud sol = null;
        try {
            sol = this.solicitudDao.findSolicitudByNumSolicitud(numeroSolicitud);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#findSolicitudByNumSolicitud: " +
                ex.getMensaje(), ex);
        }
        return sol;
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerAxMunicipioPorMunicipioVigencia
     */
    @Override
    public List<AxMunicipio> obtenerAxMunicipioPorMunicipioVigencia(String municipioCodigo,
        String vigencia) {
        List<AxMunicipio> resultado = null;
        try {
            resultado = this.axMunicipioDAO.obtenerPorMunicipioVigencia(municipioCodigo, vigencia);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerAxMunicipioPorMunicipioVigencia: " +
                ex.getMensaje());
        }
        return resultado;
    }

    /* @author leidy.gonzalez @param estructuraOrganizacionalCodigo @return @see
     * IConservacion#consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(String)
     */
    @Override
    public Object[] consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(
        String estructuraOrganizacionalCodigo) {
        try {
            return this.sncProcedimientoDao.
                consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(
                    estructuraOrganizacionalCodigo);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IConservacion#buscarEntidad(String,String,String,String)
     * @author dumar.penuela
     */
    @Override
    public boolean validaExistenciaEntidad(String nombreEntidad, String estadoEntidad,
        String codDepartamento, String codMunicipio) {
        try {
            return this.entidadDao.validaExistenciaEntidad(nombreEntidad, estadoEntidad,
                codDepartamento, codMunicipio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#validaExistenciaEntidad: " +
                ex);
        }
        return false;
    }

    /**
     * @author dumar.penuela
     * @see IConservacion#validarNumeroRadicacion(String,UsuarioDTO)
     */
    @Override
    public boolean validarNumeroRadicacion(String numeroRadicacionCorrespondencia,
        UsuarioDTO usuario) {
        boolean resultado = false;
        try {
            resultado = sncProcedimientoDao.validaNumeroRadicacion(numeroRadicacionCorrespondencia,
                usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en IConservacion#validarNumeroRadicacion: " +
                e.getMensaje(), e);
        }
        return resultado;
    }

    /**
     * @see IConservacionLocal#buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(String, String)
     * @author david.cifuentes
     */
    @Override
    public HPredio buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(
        String numeroPredial, String estadoCancelaInscribe) {
        HPredio answer = null;
        try {
            answer = this.hPredioDao.buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(
                numeroPredial, estadoCancelaInscribe);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarHPredioPorNumeroPredialYEstadoCancelaInscribe: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @author dumar.penuela
     * @see IConservacion#busquedaPredioBloqueoByTipoBloqueo(String numeroPredial, String
     * tipoBloqueo)
     */
    @Override
    public PredioBloqueo busquedaPredioBloqueoByTipoBloqueo(String numeroPredial, String tipoBloqueo) {
        PredioBloqueo resultado = null;
        try {
            resultado = predioBloqueoDAOBean.busquedaPredioBloqueoByTipoBloqueo(numeroPredial,
                tipoBloqueo);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en IConservacion#busquedaPredioBloqueoByTipoBloqueo: " +
                e.getMensaje(), e);
        }
        return resultado;
    }

    /**
     * @author dumar.penuela
     * @see IConservacion#busquedaPredioBloqueoReciente(String numeroPredial)
     */
    @Override
    public PredioBloqueo busquedaPredioBloqueoReciente(String numeroPredial) {
        PredioBloqueo resultado = null;
        try {
            resultado = predioBloqueoDAOBean.busquedaPredioBloqueoReciente(numeroPredial);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en IConservacion#busquedaPredioBloqueoReciente: " +
                e.getMensaje(), e);
        }
        return resultado;
    }

    /**
     * @author dumar.penuela
     * @see IConservacion#getPredioDesBloqueosByNumeroPredial(String numeroPredial)
     */
    @Override
    public List<PredioBloqueo> getPredioDesBloqueosByNumeroPredial(String numeroPredial) {
        List<PredioBloqueo> resultado = null;
        try {
            resultado = predioBloqueoDAOBean.getPredioDesBloqueosByNumeroPredial(numeroPredial);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en IConservacion#getPredioDesBloqueosByNumeroPredial: " +
                e.getMensaje(), e);
        }
        return resultado;
    }

    /**
     * @author dumar.penuela
     * @see IConservacion#busquedaPredioDesBloqueoReciente(String numeroPredial)
     */
    @Override
    public PredioBloqueo busquedaPredioDesBloqueoReciente(String numeroPredial) {
        PredioBloqueo resultado = null;
        try {
            resultado = predioBloqueoDAOBean.busquedaPredioDesBloqueoReciente(numeroPredial);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en IConservacion#busquedaPredioDesBloqueoReciente: " +
                e.getMensaje(), e);
        }
        return resultado;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacionLocal#getPersonaPrediosByPersonaId(Long)
     */
    @Override
    public List<PersonaPredio> getPersonaPrediosByPersonaId(Long personaId) {

        LOGGER.debug("en ConservacionBean#getPersonaPrediosByPersonaId ");
        List<PersonaPredio> answer = null;

        try {
            answer = this.personaPredioDao.getPersonaPrediosByPersonaId(personaId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPersonaPredioPorPredioId: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacionLocal#buscarPPersonasPredioContandoPredios(Long)
     */
    @Override
    public List<PPersonaPredio> buscarPPersonasPredioContandoPredios(List<Long> predioIds) {

        LOGGER.debug("en ConservacionBean#buscarPPersonasPredioContandoPredios ");
        List<PPersonaPredio> answer = null;

        try {
            answer = this.pPersonaPredioDao.buscarPPersonasPredioContandoPredios(predioIds);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPersonasPredioContandoPredios: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#actualizarPPersonaPredioYPropiedadCancelados(PersonaPredio[],
     * List<PPersonaPredioPropiedad>)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPersonaPredio> actualizarPPersonaPredioYPropiedadCancelados(
        List<PPersonaPredio> pPersonaPredioPropiedades, UsuarioDTO usuario,
        PPredio predioSeleccionado) {

        PPersonaPredio pPersonaPredioTemp = new PPersonaPredio();
        List<PPersonaPredio> pPersonaPredioPropiedadesN = new ArrayList<PPersonaPredio>();

        try {

            for (PPersonaPredio pVigenteSelect : pPersonaPredioPropiedades) {

                pPersonaPredioTemp = (PPersonaPredio) pVigenteSelect.clone();

                pPersonaPredioTemp.setId(null);
                pPersonaPredioTemp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
                    getCodigo());
                pPersonaPredioTemp.setPPredio(predioSeleccionado);
                pPersonaPredioTemp.setUsuarioLog(usuario.getLogin());
                pPersonaPredioTemp.setFechaLog(new Date());
                pPersonaPredioTemp = this.actualizarPPersonaPredio(pPersonaPredioTemp);

                pPersonaPredioPropiedadesN.add(pPersonaPredioTemp);

            }
        } catch (RuntimeException rtex) {
            LOGGER.error(
                "error en ConservacionBean#actualizarPPersonaPredioYPropiedadCancelados: " +
                rtex.getMessage());
        }
        return pPersonaPredioPropiedadesN;
    }

    /**
     * @author felipe.cadena
     * @see IConservacionLocal#obtenerBloqueosPredio(Long)
     */
    @Override
    public List<PredioBloqueo> obtenerBloqueosPredio(Long predioId, String estadoBloqueo) {

        LOGGER.debug("en ConservacionBean#obtenerBloqueosPredio ");
        List<PredioBloqueo> answer = null;
        try {
            answer = this.predioBloqueService.obtenerBloqueosPredio(predioId, estadoBloqueo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerBloqueosPredio: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#consultarFechaCreacionPredio(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Override
    public Date consultarFechaCreacionPredio(Long pPredioId) {

        Date answer = null;
        try {
            //D: se usa cualquiera de los EJB que implementen IGenericJpaDAO para obtener el entity manager
            answer = QueryNativoDAO.consultarFechaCreacionPredio(this.documentoDao.
                getEntityManager(), pPredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#consultarFechaCreacionPredio: " +
                ex.getMensaje(), ex);
        }
        return answer;
    }

    /**
     * @see IConservacion#consultarFechaCreacionPredio(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<PPersonaPredioPropiedad> consultarJustificacionesPorPropetario(Long personaPredioId) {

        List<PPersonaPredioPropiedad> result = null;
        try {
            result = this.pPersonaPredioPropiedadDao.findByPPersonaPredioId(personaPredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#consultarJustificacionesPorPropetario: " +
                ex.getMensaje(), ex);
        }

        return result;

    }

    /**
     * @see
     * IConservacion#guardarJustificacionesPropiedadPrediosNoCancelados(JustificacionPropiedadDTO
     * JustificacionPropiedadDTO, PPersonaPredio[] propietariosNuevosSeleccionados, UsuarioDTO
     * usuario)
     * @author leidy.gonzalez
     */
    @Override
    public JustificacionPropiedadDTO guardarJustificacionesPropiedadPrediosNoCancelados(
        JustificacionPropiedadDTO justificacionSeleccionada,
        JustificacionPropiedadDTO justificacionSeleccionadaSinModificar,
        List<PPersonaPredio> listPersonasPredioSinCanc,
        UsuarioDTO usuario) {
        /*
         * Se recorren los seleccionados por el usuario en la UI
         */
        PPersonaPredioPropiedad ppp = new PPersonaPredioPropiedad();

        for (PPersonaPredio pp : listPersonasPredioSinCanc) {

            if (justificacionSeleccionadaSinModificar == null) {

                //Consulta persona_predio_propiedad con persona_predio_id
                List<PPersonaPredioPropiedad> listPersonasPredioPropiedad =
                    this.pPersonaPredioPropiedadDao.consultarPorPPersonaPredioId(pp.getId());

                if (listPersonasPredioPropiedad == null &&
                    listPersonasPredioPropiedad.isEmpty()) {

                    ppp.setId(null);
                }
                ppp.setPPersonaPredio(pp);
                ppp.setTipoTitulo(justificacionSeleccionada.getTipoTitulo());
                ppp.setEntidadEmisora(justificacionSeleccionada.getEntidadEmisora());
                ppp.setDepartamento(justificacionSeleccionada.getDepartamento());
                ppp.setMunicipio(justificacionSeleccionada.getMunicipio());
                ppp.setNumeroTitulo(justificacionSeleccionada.getNumeroTitulo());
                ppp.setFechaTitulo(justificacionSeleccionada.getFechaTitulo());
                ppp.setFechaRegistro(justificacionSeleccionada.getFechaRegistro());
                ppp.setDocumentoSoporte(justificacionSeleccionada.getDocumentoSoporte());
                ppp.setModoAdquisicion(justificacionSeleccionada.getModoAdquisicion());
                ppp.setValor(justificacionSeleccionada.getValor());
                ppp.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
                ppp.setCancelaInscribe(justificacionSeleccionada.getCancelaInscribe());
                ppp.setUsuarioLog(justificacionSeleccionada.getUsuarioLog());
                ppp.setFechaLog(new Date());

                this.guardarActualizarPPersonaPredioPropiedad(ppp);

            } else {

                for (PPersonaPredioPropiedad pPersonaPredioPropiedad : pp.
                    getPPersonaPredioPropiedads()) {

                    pPersonaPredioPropiedad.setPPersonaPredio(pp);
                    pPersonaPredioPropiedad.setTipoTitulo(justificacionSeleccionada.getTipoTitulo());
                    pPersonaPredioPropiedad.setEntidadEmisora(justificacionSeleccionada.
                        getEntidadEmisora());
                    pPersonaPredioPropiedad.setDepartamento(justificacionSeleccionada.
                        getDepartamento());
                    pPersonaPredioPropiedad.setMunicipio(justificacionSeleccionada.getMunicipio());
                    pPersonaPredioPropiedad.setNumeroTitulo(justificacionSeleccionada.
                        getNumeroTitulo());
                    pPersonaPredioPropiedad.setFechaTitulo(justificacionSeleccionada.
                        getFechaTitulo());
                    pPersonaPredioPropiedad.setFechaRegistro(justificacionSeleccionada.
                        getFechaRegistro());
                    pPersonaPredioPropiedad.setDocumentoSoporte(justificacionSeleccionada.
                        getDocumentoSoporte());
                    pPersonaPredioPropiedad.setModoAdquisicion(justificacionSeleccionada.
                        getModoAdquisicion());
                    pPersonaPredioPropiedad.setValor(justificacionSeleccionada.getValor());
                    pPersonaPredioPropiedad.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
                    pPersonaPredioPropiedad.setUsuarioLog(justificacionSeleccionada.getUsuarioLog());
                    pPersonaPredioPropiedad.setFechaLog(new Date());

                    this.guardarActualizarPPersonaPredioPropiedad(pPersonaPredioPropiedad);
                }
            }

            pp.setFechaInscripcionCatastral(justificacionSeleccionada.getFechaTitulo());
            this.guardarActualizarPPersonaPredio(pp);

            pp.getPPredio().setFechaInscripcionCatastral(
                justificacionSeleccionada.getFechaTitulo());
            this.guardarActualizarPPredio(pp.getPPredio());

        }

        return justificacionSeleccionada;
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerUsosHomologados
     */
    @Override
    public List<UsoHomologado> obtenerUsosHomologados() {

        List<UsoHomologado> resultado = null;
        try {

            resultado = this.usoHomologadoDAO.findAll();

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerUsosHomologados: " +
                ex.getMensaje());
        }
        return resultado;

    }

    /**
     * @see IConservacion#buscarPPersonaPrediosPorPredioIdInscritos(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPersonaPredio> buscarPPersonaPrediosPorPredioIdInscritos(Long idPPredio) {
        return this.pPersonaPredioDao.findByPPredioIdCancelaInscritos(idPPredio);
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#obtenerPrediosPorFichaEstado
     */
    @Override
    public List<PFichaMatrizPredio> obtenerPrediosPorFichaEstado(String estado, Long idFicha) {

        List<PFichaMatrizPredio> resultado = null;
        try {

            resultado = this.pFichaMatrizPredioDao.
                buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(estado, idFicha);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerPrediosPorFichaEstado: " +
                ex.getMensaje());
        }
        return resultado;

    }

    /**
     * @see IConservacion#guardarJustificacionesPropiedad(JustificacionPropiedadDTO
     * JustificacionPropiedadDTO, PPersonaPredio[] propietariosNuevosSeleccionados, UsuarioDTO
     * usuario)
     * @author leidy.gonzalez
     */
    @Override
    public JustificacionPropiedadDTO guardarJustificacionesPropiedadCancelacionMasiva(
        JustificacionPropiedadDTO justificacionSeleccionada,
        JustificacionPropiedadDTO justificacionSeleccionadaSinModificar,
        PPersonaPredio[] propietariosNuevosSeleccionados,
        UsuarioDTO usuario) {

        PPersonaPredioPropiedad ppp = new PPersonaPredioPropiedad();
        /*
         * Se recorren los seleccionados por el usuario en la UI
         */
        for (PPersonaPredio pp : propietariosNuevosSeleccionados) {

            if (justificacionSeleccionadaSinModificar == null) {
                ppp.setId(null);
                ppp.setPPersonaPredio(pp);
                ppp.setTipoTitulo(justificacionSeleccionada.getTipoTitulo());
                ppp.setEntidadEmisora(justificacionSeleccionada.getEntidadEmisora());
                ppp.setDepartamento(justificacionSeleccionada.getDepartamento());
                ppp.setMunicipio(justificacionSeleccionada.getMunicipio());
                ppp.setNumeroTitulo(justificacionSeleccionada.getNumeroTitulo());
                ppp.setFechaTitulo(justificacionSeleccionada.getFechaTitulo());
                ppp.setFechaRegistro(justificacionSeleccionada.getFechaRegistro());
                ppp.setDocumentoSoporte(justificacionSeleccionada.getDocumentoSoporte());
                ppp.setModoAdquisicion(justificacionSeleccionada.getModoAdquisicion());
                ppp.setValor(justificacionSeleccionada.getValor());
                ppp.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
                ppp.setCancelaInscribe(justificacionSeleccionada.getCancelaInscribe());
                ppp.setUsuarioLog(justificacionSeleccionada.getUsuarioLog());
                ppp.setFechaLog(new Date());

                this.guardarActualizarPPersonaPredioPropiedad(ppp);

            } else {

                for (PPersonaPredioPropiedad pPersonaPredioPropiedad : pp.
                    getPPersonaPredioPropiedads()) {

                    pPersonaPredioPropiedad.setPPersonaPredio(pp);
                    pPersonaPredioPropiedad.setTipoTitulo(justificacionSeleccionada.getTipoTitulo());
                    pPersonaPredioPropiedad.setEntidadEmisora(justificacionSeleccionada.
                        getEntidadEmisora());
                    pPersonaPredioPropiedad.setDepartamento(justificacionSeleccionada.
                        getDepartamento());
                    pPersonaPredioPropiedad.setMunicipio(justificacionSeleccionada.getMunicipio());
                    pPersonaPredioPropiedad.setNumeroTitulo(justificacionSeleccionada.
                        getNumeroTitulo());
                    pPersonaPredioPropiedad.setFechaTitulo(justificacionSeleccionada.
                        getFechaTitulo());
                    pPersonaPredioPropiedad.setFechaRegistro(justificacionSeleccionada.
                        getFechaRegistro());
                    pPersonaPredioPropiedad.setDocumentoSoporte(justificacionSeleccionada.
                        getDocumentoSoporte());
                    pPersonaPredioPropiedad.setModoAdquisicion(justificacionSeleccionada.
                        getModoAdquisicion());
                    pPersonaPredioPropiedad.setValor(justificacionSeleccionada.getValor());
                    pPersonaPredioPropiedad.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
                    pPersonaPredioPropiedad.setCancelaInscribe(justificacionSeleccionada.
                        getCancelaInscribe());
                    pPersonaPredioPropiedad.setUsuarioLog(justificacionSeleccionada.getUsuarioLog());
                    pPersonaPredioPropiedad.setFechaLog(new Date());

                    this.guardarActualizarPPersonaPredioPropiedad(pPersonaPredioPropiedad);
                }
            }

            pp.setFechaInscripcionCatastral(justificacionSeleccionada.getFechaTitulo());
            this.guardarActualizarPPersonaPredio(pp);

            pp.getPPredio().setFechaInscripcionCatastral(
                justificacionSeleccionada.getFechaTitulo());
            this.guardarActualizarPPredio(pp.getPPredio());
        }

        return justificacionSeleccionada;

    }

    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IConservacionLocal#procesarResolucionesCancelacionMasivaEnJasperServer
     * @author leidy.gonzalez
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarResolucionesCancelacionMasivaEnJasperServer(ProductoCatastralJob job,
        Tramite tramite, int contador) {

        LOGGER.debug("procesarResolucionesCancelacionMasivaEnJasperServer");
        boolean resultadoOk = true;
        boolean errorGeneracionReporte = false;
        boolean noPrediosProyectados = false;
        EReporteServiceSNC enumeracionReporte = null;
        String nombreResponsableConservacion;

        Parametro maxEnvioJob = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());
        UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());

        try {
            contador++;

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService
                .buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            //Se consulta si el Tramite tiene Predios Proyectados
            List<PPredio> pPrediosResolucion = this.pPredioDao
                .findExistingPPrediosByTramiteId(tramite.getId());

            if (!pPrediosResolucion.isEmpty()) {

                enumeracionReporte = EReporteServiceSNC.MODELO_RESOLUCION_CANCELACION_MASIVA;;

                String urlReporte = enumeracionReporte.getUrlReporte();

                LOGGER.debug("tipo Reporte cancelacion:" + urlReporte);

                Reporte reporte = new Reporte(usuario, estructuraOrganizacional);

                reporte.setEstructuraOrganizacional(estructuraOrganizacional);

                reporte.setUrl(urlReporte);

                List<String> resultados = this.
                    obtenerParametrosDeProductoCatastralJobResolucionesConservacion(job);

                String numeroDocumento = resultados.get(2);

                reporte.addParameter("TRAMITE_ID", (resultados.get(0) != null) ? resultados.get(0) :
                    "");
                LOGGER.debug("TRAMITE_ID:" + resultados.get(0));
                nombreResponsableConservacion = (resultados.get(1) != null) ? resultados.get(1) : "";
                reporte.addParameter("NOMBRE_RESPONSABLE_CONSERVACION",
                    nombreResponsableConservacion);
                LOGGER.debug("nombreResponsableConservacion:" + resultados.get(1));
                reporte.addParameter("NUMERO_RESOLUCION", (resultados.get(2) != null) ? resultados.
                    get(2) : "");
                LOGGER.debug("NUMERO_RESOLUCION:" + resultados.get(2));
                reporte.addParameter("FECHA_RESOLUCION", (resultados.get(3) != null) ? resultados.
                    get(3) : "");
                LOGGER.debug("FECHA_RESOLUCION:" + resultados.get(3));
                reporte.addParameter("FECHA_RESOLUCION_CON_FORMATO", (resultados.get(4) != null) ?
                    resultados.get(4) : "");
                LOGGER.debug("FECHA_RESOLUCION_CON_FORMATO:" + resultados.get(4));

                if (resultados.get(5) != null && !resultados.get(5).isEmpty()) {
                    reporte.addParameter("NOMBRE_REVISOR", resultados.get(5));
                    LOGGER.debug("NOMBRE_REVISOR:" + resultados.get(5));
                }

                if (resultados.get(6) != null && !resultados.get(6).isEmpty()) {
                    reporte.addParameter("FIRMA_USUARIO", resultados.get(6));
                    LOGGER.debug("FIRMA_USUARIO:" + resultados.get(6));
                }

                if (nombreResponsableConservacion != null &&
                    !nombreResponsableConservacion.trim().isEmpty()) {

                    FirmaUsuario firma;
                    String documentoFirma;

                    firma = this.generalesService.buscarDocumentoFirmaPorUsuario(
                        nombreResponsableConservacion);
                    LOGGER.debug(" Consulta FIRMA:" + firma);

                    if (firma != null && nombreResponsableConservacion.equals(firma.
                        getNombreFuncionarioFirma())) {

                        documentoFirma = this.generalesService.
                            descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                                getIdRepositorioDocumentos());
                        LOGGER.debug("Descarga documentoFirma:" + documentoFirma);

                        reporte.addParameter("FIRMA_USUARIO", documentoFirma);
                    }
                }

                reporte.addParameter("COPIA_USO_RESTRINGIDO", (resultados.get(6) != null) ?
                    resultados.get(6) : "");
                LOGGER.debug("COPIA_USO_RESTRINGIDO:" + resultados.get(6));

                LOGGER.debug("Numero resolucion:" + numeroDocumento);

                tramite = this.generarArchivoResolucionesConservacion(usuario, reporte,
                    numeroDocumento, tramite, job, contador);
                LOGGER.debug("Resolucion almacenada en tramite:" + tramite.getId() +
                    "con documento: " +
                    tramite.getResultado());

            } else {
                this.productoCatastralJobDAO.actualizarEstado(job,
                    ProductoCatastralJob.JPS_TERMINADO);
                noPrediosProyectados = true;
                return true;
            }

        } catch (ExcepcionSNC e) {

            LOGGER.debug("procesarResolucionesCancelacionMasivaEnJasperServer:" + e.getMensaje(), e);

            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                this.procesarResolucionesCancelacionMasivaEnJasperServer(job, tramite, contador);
            } else {
                resultadoOk = false;
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
            }
        } catch (Exception ex) {
            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + ex.getMessage(), ex);

            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                errorGeneracionReporte = !this.procesarResolucionesCancelacionMasivaEnJasperServer(
                    job, tramite, contador);
            } else {
                resultadoOk = false;
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                return false;
            }
        }

        if (errorGeneracionReporte) {
            return false;
        }

        if (noPrediosProyectados) {
            return true;
        }
        //Se ejecuta el Procedimiento de Aplicar Cambios
        try {

            Actividad actividad = new Actividad();
            String actividadId = null;

            int indiceSeparador = job.getCodigo().indexOf(
                Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO);

            if (indiceSeparador > 0) {
                actividadId = job.getCodigo().substring(0, indiceSeparador);
                actividad.setId(actividadId);
            }

            if (actividad != null && actividad.getId() != null &&
                job != null && job.getEstado().equals(ProductoCatastralJob.JPS_TERMINADO) &&
                tramite != null &&
                tramite.getResultadoDocumento() != null &&
                tramite.getResultadoDocumento().getId() != null) {

                LOGGER.debug("actividad:" + actividad);

                this.avanzarTramiteDeGenerarResolucionProcess(tramite, usuario, actividad);
                resultadoOk = true;

            } else {
                LOGGER.error("No se encontro actividad para el tramite_id del job:" + job);
            }

        } catch (Exception e) {

            LOGGER.error("Ocurrió un error durante el Procesamiento de Aplicar Cambios " +
                "o Avance de la Actividad para resoluciones de Cancelacion Masiva" +
                " para el tramite_id del job:" + job);
        }

        return resultadoOk;
    }

    /**
     * @see IConservacionLocal#buscarReportePorFiltros(Long,String,String)
     * @author leidy.gonzalez
     */
    @Override
    public List<RepReporteEjecucion> buscarReportePorDepartamentoYMunicipio(Long tipoReporte,
        String codigoDepartamento, String codigoMunicipio) {

        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO
                .buscarReportePorDepartamentoYMunicipio(tipoReporte, codigoDepartamento,
                    codigoMunicipio);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar reporteControlPorUsuario" +
                e.getMessage(), e);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarReportePorId(Long)
     * @author leidy-gonzalez
     */
    @Override
    public List<RepReporteEjecucion> buscarReportesPorCodigo(String idReporteConsultar) {

        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO
                .buscarReportesPorCodigo(idReporteConsultar);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportePorId" +
                e.getMessage(), e);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarReportePorCodigo(Long)
     * @author leidy.gonzalez
     */
    @Override
    public RepReporteEjecucion buscarReportePorCodigo(String codigoReporte) {
        try {
            RepReporteEjecucion answer = this.repReporteEjecucionDAO
                .buscarReportePorCodigo(codigoReporte);
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportePorCodigo" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IConservacionLocal#buscarSubscripcionPorUsuarioIdReporte(String,Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<RepReporteEjecucionUsuario> buscarSubscripcionPorUsuarioIdReporte(String usuario,
        Long idReporte) {
        try {
            List<RepReporteEjecucionUsuario> answer = this.ejecucionUsuarioDAO
                .obtenerPorUsuarioIdReporte(usuario, idReporte);
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarSubscripcionPorUsuarioIdReporte" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IConservacionLocal#buscarReportePorFiltrosBasicos(Long)
     * @author leidy-gonzalez
     */
    @Override
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicos(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedDepartamentoCod, String selectedMunicipioCod, String territorial, String uoc) {

        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO
                .buscarReportePorFiltrosBasicos(datosConsultaPredio, selectedDepartamentoCod,
                    selectedMunicipioCod, territorial, uoc);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportePorFiltrosBasicos" +
                e.getMessage(), e);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarReportePorFiltrosAvanzados(FiltroGenerarReportes
     * datosConsultaPredio, Long idReporteEjecucion, RepConsultaPredial configuracionAvanzada)
     * @author leidy-gonzalez
     */
    @Override
    public List<RepReporteEjecucionDetalle> buscarReportePorFiltrosAvanzados(
        FiltroGenerarReportes datosConsultaPredio, String selectedMunicipioCod,
        Long idReporteEjecucion, RepConfigParametroReporte configuracionAvanzada) {

        List<RepReporteEjecucionDetalle> answer = null;
        try {
            answer = this.repReporteEjecucionDetalleDAO
                .buscarReportePorFiltrosAvanzados(datosConsultaPredio, selectedMunicipioCod,
                    idReporteEjecucion, configuracionAvanzada);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportePorFiltrosAvanzados" +
                e.getMessage(), e);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarReportePorId(Long)
     * @author leidy-gonzalez
     */
    @Override
    public List<RepReporteEjecucion> buscarReportePorId(Long idReporteConsultar) {

        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO
                .buscarReportesPorId(idReporteConsultar);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportePorId" +
                e.getMessage(), e);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarListaRepUsuarioRolReportePorUsuario(UsuarioDTO)
     * @author david.cifuentes
     */
    @Override
    public List<RepUsuarioRolReporte> buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
        UsuarioDTO usuario, String categoria) {
        List<RepUsuarioRolReporte> answer = null;
        try {
            answer = this.repUsuarioRolReporteDAO.
                buscarListaRepUsuarioRolReportePorUsuarioYCategoria(usuario, categoria);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en ConservacionBean#buscarListaRepUsuarioRolReportePorUsuarioYCategoria: " +
                ex.getMensaje(), ex);
        }
        return answer;
    }

    /**
     * @see
     * IConservacionLocal#buscarReportePorFiltrosBasicosSinMuncipio(FiltroGenerarReportes,String,String,String)
     * @author leidy-gonzalez
     */
    @Override
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicosSinMuncipio(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedDepartamentoCod, String territorial, String uoc) {

        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO
                .buscarReportePorFiltrosBasicosSinMuncipio(datosConsultaPredio,
                    selectedDepartamentoCod, territorial, uoc);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportePorFiltrosBasicos" +
                e.getMessage(), e);
        }
        return answer;
    }

    /**
     * @see IConservacion#contarPrediosPorRangoNumeroPredial(java.lang.String,java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public Integer contarPrediosPorRangoNumeroPredial(String numeroPredial,
        String numeroPredialFinal) {
        LOGGER.debug("en ConservacionBean#contarPrediosPorRangoNumeroPredial");
        int answer;
        answer = predioDao.contarPrediosPorRangoNumeroPredial(numeroPredial, numeroPredialFinal);

        return answer;
    }

    /**
     * @see IConservacion#contarPrediosPorRangoNumeroPredial(java.lang.String,java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public Integer contarPrediosPorRangoNumeroPredialHistorico(String numeroPredial,
        String numeroPredialFinal) {
        LOGGER.debug("en ConservacionBean#contarPrediosPorRangoNumeroPredial");
        int answer;
        answer = this.hPredioDao.contarPrediosPorRangoNumeroPredialHistorico(numeroPredial,
            numeroPredialFinal);

        return answer;
    }

    /**
     * @see IConservacionLocal#generarReporteRadicacion(RepReporteEjecucion)
     * @author david.cifuentes
     */
    @Override
    public Object[] generarReporteRadicacion(RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            answer = sncProcedimientoDao
                .generarReporteRadicacion(repReporteEjecucion);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#actualizarRepReporteEjecucionUsuario(RepReporteEjecucionUsuario)
     */
    @Override
    public RepReporteEjecucionUsuario actualizarRepReporteEjecucionUsuario(
        RepReporteEjecucionUsuario repReporteEjecucionUsuario) {
        RepReporteEjecucionUsuario auxRepReporteEjecucion = null;

        try {
            auxRepReporteEjecucion = this.ejecucionUsuarioDAO
                .update(repReporteEjecucionUsuario);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#actualizarRepReporteEjecucionUsuario: " +
                ex.getMensaje());
        }
        return auxRepReporteEjecucion;
    }

    /**
     * @see IConservacionLocal#buscarParametrizacionReporteUsuarioCompletaPorId(Long)
     * @author david.cifuentes
     */
    @Override
    public RepUsuarioRolReporte buscarParametrizacionReporteUsuarioCompletaPorId(
        Long id) {
        try {
            RepUsuarioRolReporte answer = this.repUsuarioRolReporteDAO
                .buscarParametrizacionReporteUsuarioCompletaPorId(id);
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar eliminarRepUsuarioRolReporte" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see
     * IConservacionLocal#buscarParametrizacionReporteUsuario(FiltroDatosConsultaParametrizacionReporte)
     * @author david.cifuentes
     */
    @Override
    public List<RepUsuarioRolReporte> buscarParametrizacionReporteUsuario(
        FiltroDatosConsultaParametrizacionReporte datosConsultaParametrizacion) {

        List<RepUsuarioRolReporte> answer = null;
        try {
            answer = this.repUsuarioRolReporteDAO.buscarParametrizacionReporteUsuario(
                datosConsultaParametrizacion);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarParametrizacionReporteUsuario" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarSubscripcionPorUsuario(Long)
     * @author felipe.cadena
     */
    @Override
    public List<RepReporteEjecucionUsuario> buscarSubscripcionPorUsuario(String usuario) {
        try {
            List<RepReporteEjecucionUsuario> answer = this.ejecucionUsuarioDAO
                .obtenerPorUsuario(usuario);
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar RepReporteEjecucionUsuario" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    @Override
    public List<RepReporteEjecucion> buscarReportesPorIds(List<Long> idsReportes) {
        try {
            List<RepReporteEjecucion> answer = this.repReporteEjecucionDAO.buscarReportesPorIds(
                idsReportes);
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar RepReporteEjecucion" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IConservacionLocal#eliminarRepUsuarioRolReporte(List<Long>)
     * @author david.cifuentes
     */
    @Override
    public boolean eliminarRepUsuarioRolReporte(List<Long> idsParametrizaciones) {
        try {
            for (Long idRepUsuarioRolReporte : idsParametrizaciones) {
                this.repUsuarioRolReporteDAO.eliminarRepUsuarioRolReportePorId(
                    idRepUsuarioRolReporte);
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar eliminarRepUsuarioRolReporte" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IConservacionLocal#guardarRepUsuarioRolReporte(RepUsuarioRolReporte)
     * @author david.cifuentes
     */
    @Override
    public RepUsuarioRolReporte guardarRepUsuarioRolReporte(
        RepUsuarioRolReporte reporteUsuario) {
        try {
            RepUsuarioRolReporte repUsuarioRolReporte = this.repUsuarioRolReporteDAO.update(
                reporteUsuario);
            return repUsuarioRolReporte;
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar guardarRepUsuarioRolReporte" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#descargarOficioDeGestorDocumentalATempLocalMaquina(String idRepositorio)
     */
    @Override
    public String descargarOficioDeGestorDocumentalATempLocalMaquina(String idRepositorio) {

        String path;

        try {

            path = this.documentoDao
                .descargarOficioDeGestorDocumentalATempLocalMaquina(idRepositorio);

        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar guardarRepUsuarioRolReporte" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return path;
    }

    /**
     * Metodo para consultar la configuracion de los reportes prediales por tipo de informe, para
     * visualizar en
     *
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public List<RepConfigParametroReporte> consultaConfiguracionReportePredial(Long idReporte) {

        LOGGER.debug("en TramiteBean#consultaConfiguracionReportePredial");

        List<RepConfigParametroReporte> answer = null;
        try {
            answer = this.repConfigParametroReporteDAO.
                consultaConfiguracionReportePredial(idReporte);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#consultaConfiguracionReportePredial: " + ex.
                getMensaje(), ex);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarReportesRadicacionGenerados
     * (FiltroDatosConsultaReportesRadicacion)
     * @author david.cifuentes
     */
    @Override
    public List<RepReporteEjecucion> buscarReportesRadicacionGenerados(
        FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion) {
        List<RepReporteEjecucion> answer = null;
        try {
            answer = this.repReporteEjecucionDAO.buscarReportesRadicacionGenerados(
                datosConsultaReportesRadicacion);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar buscarReportesRadicacionGenerados" +
                e.getMessage());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#isPredioMigradoCobol
     * @author juan.cruz
     */
    @Override
    public boolean isPredioMigradoCobol(String numeroPredial) {

        HPredio origenPredio = this.hPredioDao.obtenerEstadoOrigenNumeroPredial(numeroPredial);
        // Se debe validar primero que tipo de valor es "CancelaInscribe" para identificar que valor devolver
        if (origenPredio == null) {
            return true;
        }
        if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().
            equals(origenPredio.getCancelaInscribe())) {
            return false;
        } else if (EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(origenPredio.
            getCancelaInscribe())) {
            if (origenPredio.getTramiteId() != null) {
                Tramite tramiteOrigen = this.tramiteDao.findById(origenPredio.getTramiteId());
                //se verifica que la clase de mutación del tramite no sea segunda ni quinta 
                if (tramiteOrigen != null &&
                    (EMutacionClase.SEGUNDA.getCodigo().equals(tramiteOrigen.getClaseMutacion()) ||
                    EMutacionClase.QUINTA.getCodigo().equals(tramiteOrigen.getClaseMutacion()))) {
                    //Si no manteiene el numero predial es migrado de Cobol
                    return ESiNo.NO.getCodigo().equals(tramiteOrigen.getMantieneNumeroPredial());
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * @see IConservacionLocal#buscarPrimerProyeccionNumeroPredial(String)
     * @author juan.cruz
     */
    @Override
    public HPredio buscarPrimerProyeccionNumeroPredial(String numeroPredial) {
        HPredio answer = null;
        try {
            answer = this.hPredioDao.obtenerEstadoOrigenNumeroPredial(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPrimerProyeccionNumeroPredial: " + ex.
                getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#buscarPrediosOrigenDeTramite(Long)
     * @author juan.cruz
     */
    @Implement
    @Override
    public List<HPredio> buscarPrediosOrigenDeTramite(Long tramiteId) {
        LOGGER.debug("en TramiteBean#buscarPrediosOrigenDeTramite");
        try {
            List<HPredio> prediosOrigen = this.hPredioDao.
                obtenerPrediosOrigenPorTramiteId(tramiteId);

            List<Long> hPrediosId = new ArrayList<Long>();
            for (HPredio unPredio : prediosOrigen) {
                hPrediosId.add(unPredio.getId());
            }
            //buscar hfichasMatriz segun listados de hPrediosId
            List<HFichaMatriz> fichasMatriz = new ArrayList<HFichaMatriz>();
            if (hPrediosId.size() > 0) {
                fichasMatriz = this.hFichaMatrizDAO.
                    buscarFichasMatrizporListadoprediosId(hPrediosId);
            }
            //Se agregan los hFichaMatriz a cada uno de los hpredios correspondientes.
            for (HFichaMatriz unaHFichaMatriz : fichasMatriz) {
                for (HPredio unHPredio : prediosOrigen) {
                    if (unHPredio.getId().equals(unaHFichaMatriz.getHPredio().getId())) {
//                        Set<HFicha
//                        unHPredio.setHFichaMatrizs(unaHFichaMatriz);
                        break;
                    }
                }
            }

            return prediosOrigen;

        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("error en TramiteBean#buscarPrediosOrigenDeTramite: " + ex.getMensaje(), ex);
            return null;
        }

    }

    /**
     * @author juan.cruz
     * @see IConservacionLocal#obtenerHPredioOrigenDetallado(Long)
     */
    @Implement
    @Override
    public HPredio obtenerHPredioOrigenDetallado(Long predioId) {

        LOGGER.debug("on ConservacionBean#obtenerHPredioOrigenDetallado ...");

        HPredio answer = null;
        try {
            answer = this.hPredioDao.obtenerHPredioCompletoPorId(predioId);
        } catch (Exception ex) {
            LOGGER.error(" ... excepción: " + ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, ex,
                "ConservacionBean#obtenerHPredioOrigenDetallado");
        }
        return answer;
    }

    /**
     * @see IConservacion#buscarFichaMatrizPorPredioIdSinAsociaciones(Long)
     * @author felipe.cadena
     */
    @Override
    public FichaMatriz buscarFichaMatrizPorPredioIdSinAsociaciones(Long idPredio) {

        FichaMatriz answer = null;
        try {
            answer = this.fichaMatrizDao.findByPredioId(idPredio, false);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarFichaMatrizPorPredioId: " + ex.
                getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#buscarMejorasPorNumeroPredial(Long)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> buscarMejorasPorNumeroPredial(String numeroPredial) {

        List<Predio> resultado = new ArrayList<Predio>();
        try {
            resultado = this.predioDao.obtenerMejorasPredio(numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarMejorasPorNumeroPredial: " + ex.
                getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#buscarPrediosPanelAvaluoPorTramiteId(List<Long>)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPPredioPorIdPredios(List<Long> predioIds) {

        LOGGER.debug("en ConservacionBean#buscarPPredioPorIdPredios");

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPredioPorIdPredios(predioIds);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPredioPorIdPredios: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#buscarPrediosPanelAvaluoPorTramiteId(List<Long>)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> validarExistenciaMatriculasPorMunicipio(List<String> matriculas,
        String municipioCodigo) {

        LOGGER.debug("en ConservacionBean#buscarPPredioPorIdPredios");

        List<Predio> answer = null;

        try {
            answer = this.predioDao.validarExistenciaMatriculasPorMunicipio(matriculas,
                municipioCodigo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPredioPorIdPredios: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#buscarHistoricoZonas(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredioZona> buscarZonasVigentes(Long idPPredio) {

        List<PPredioZona> resultado = new ArrayList<PPredioZona>();
        try {
            resultado = this.pPredioZonaDao.buscarTodasZonasPorPPredioId(idPPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarHistoricoZonas: " + ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#consultaPrediosPorNumeroPredial(List<String>)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> consultaPrediosPorNumeroPredial(List<String> nPrediales) {

        List<Predio> answer = null;

        try {
            answer = this.predioDao.consultaPorNumeroPredial(nPrediales);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPredioPorIdPredios: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#consultaPrediosPorNumeroPredial(List<String>)
     * @author felipe.cadena
     */
    @Override
    public List<PPredio> consultaPPrediosPorNumeroPredial(List<String> nPrediales) {

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.consultaPorNumeroPredial(nPrediales, null, null);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPredioPorIdPredios: " +
                    ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#consultaPrediosPorNumeroPredial(List<String>)
     * @author felipe.cadena
     */
    @Override
    public List<PPredio> consultaPPrediosPorNumeroPredial(List<String> nPrediales, String tipoTramite, Boolean equal) {

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.consultaPorNumeroPredial(nPrediales, tipoTramite, equal);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPredioPorIdPredios: " +
                    ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#obtenerPrediosCompletosPorTramiteId(Long)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> obtenerPrediosCompletosPorTramiteId(Long tramiteId) {

        List<Predio> answer = null;

        try {
            answer = this.predioDao.obtenerPrediosCompletosEnglobePorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPrediosCompletosPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#buscarZonasPorPPredioIdYVigencia(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredioZona> buscarZonasPorPPredioIdYVigencia(Long idPPredio, Date vigencia) {

        List<PPredioZona> resultado = new ArrayList<PPredioZona>();
        try {
            resultado = this.pPredioZonaDao.buscarZonasPorPPredioIdYVigencia(idPPredio, vigencia);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarHistoricoZonas: " + ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#obtenerPrediosCompletosPorTramiteId(Long)
     * @author felipe.cadena
     */
    @Override
    public List<PFichaMatrizPredio> buscarFichaMatrizPredioPorPFichaMatrizId(Long fichaId) {

        List<PFichaMatrizPredio> answer = null;

        try {
            answer = this.pFichaMatrizPredioDao
                .findByPFichaMatrizId(fichaId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarFichaMatrizPredioPorPFichaMatrizId: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacionLocal#buscarUnidConstPorTramiteYDifNumFichaMatrizEV (Long,String)
     * @author leidy.gonzalez
     */
    @Override
    public List<PUnidadConstruccion> buscarUnidConstPorTramiteYDifNumFichaMatrizEV(
        Long idTramite, String numeroFichaMatriz) {

        List<PUnidadConstruccion> pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                buscarUnidConstPorTramiteYDifNumFichaMatrizEV(
                    idTramite, numeroFichaMatriz);
        } catch (Exception e) {
            LOGGER.debug(
                "Ocurrió un error al consultar buscarUnidConstPorTramiteYDifNumFichaMatrizEV" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacionLocal#obtenerPUnidadsConstruccionPorIdPredio (Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadsConstruccionPorIdPredio(
        Long idPred) {

        List<PUnidadConstruccion> pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                obtenerPUnidadsConstruccionPorIdPredio(
                    idPred);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar obtenerPUnidadsConstruccionPorIdPredio" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacionLocal#calculoZonasFisicasYGeoeconomicas(Date, String)
     * @author leidy.gonzalez
     */
    @Override
    public Object[] calculoZonasFisicasYGeoeconomicas(Date vigencia, String zona) {
        try {
            return this.sncProcedimientoDao.calculoZonasFisicasYGeoeconomicas(vigencia, zona);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IConservacionLocal#obtenerPredioPorId(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public FichaMatrizTorre obtenerFichaMatrizTorrePorId(Long id) {

        LOGGER.debug("on ConservacionBean#obtenerFichaMatrizTorrePorId ..");
        FichaMatrizTorre answer = null;

        try {
            answer = this.fichaMatrizTorreDao.getFichaMatrizTorreById(id);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("ExcepciÃ³n consultando la FichaMatrizTorre con id = " + id);
        }

        return answer;
    }

    /**
     * @see IConservacionLocal#obtenerPUnidadesConstConvPorIdPredio(java.lang.Long)
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstConvPorIdPredio(
        Long idPred) {

        List<PUnidadConstruccion> pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                obtenerPUnidadesConstConvPorIdPredio(
                    idPred);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar obtenerPUnidadsConstruccionPorIdPredio" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacionLocal#obtenerPUnidadesConstNoConvPorIdPredio(java.lang.Long)
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstNoConvPorIdPredio(
        Long idPred) {

        List<PUnidadConstruccion> pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                obtenerPUnidadesConstNoConvPorIdPredio(
                    idPred);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar obtenerPUnidadsConstruccionPorIdPredio" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacion#obtenerFichaMatrizTorrePorFichaId(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PFichaMatrizTorre> obtenerFichaMatrizTorrePorFichaId(Long fichaMatrizId) {

        List<PFichaMatrizTorre> pFichaMatriz = null;

        try {
            pFichaMatriz = this.pFichaMatrizTorreDao.buscarPFichaMatrizTorreporFichaMatrizId(
                fichaMatrizId);
        } catch (Exception e) {
            LOGGER.debug("Ocurrió un error al consultar obtenerFichaMatrizTorrePorFichaId" +
                e.getMessage());
        }
        return pFichaMatriz;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#actualizarListaPFichaMatrizPredio((PFichaMatrizPredio)
     */
    @Override
    public void actualizarListaPFichaMatrizPredio(
        PFichaMatrizPredio predioFichaMatriz) {
        this.pFichaMatrizPredioDao.update(predioFichaMatriz);
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacion#actualizarListaPFichaMatrizPredioTerreno((PFichaMatrizPredio)
     */
    @Override
    public void actualizarListaPFichaMatrizPredioTerreno(
        PFichaMatrizPredioTerreno predFichaMatrizPredTerr) {
        this.fichaMatrizPredioTerrenoDao.update(predFichaMatrizPredTerr);
    }

    /**
     * @see IConservacionLocal#buscarUnidConstOriginalesCanceladasPorTramite (Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PUnidadConstruccion> buscarUnidConstOriginalesCanceladasPorTramite(
        Long idTramite) {

        List<PUnidadConstruccion> pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                buscarUnidConstOriginalesCanceladasPorTramite(
                    idTramite);
        } catch (Exception e) {
            LOGGER.debug(
                "Ocurrió un error al consultar buscarUnidConstOriginalesCanceladasPorTramite" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacionLocal#buscarUnidConstOriginalesPorIdPredio (Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PUnidadConstruccion> buscarUnidConstOriginalesPorIdPredio(
        Long idPredio) {

        List<PUnidadConstruccion> pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                buscarUnidConstOriginalesPorIdPredio(
                    idPredio);
        } catch (Exception e) {
            LOGGER.debug("OcurriÃ³ un error al consultar buscarUnidConstOriginalesPorIdPredio" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacion#buscarZonasNoCanceladaPorPPredioId(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredioZona> buscarZonasNoCanceladaPorPPredioId(Long idPPredio) {

        List<PPredioZona> resultado = new ArrayList<PPredioZona>();
        try {
            resultado = this.pPredioZonaDao.buscarZonasNoCanceladaPorPPredioId(idPPredio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarZonasNoCanceladaPorPPredioId: " + ex.
                getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#buscarZonasTempPorIdTramite(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredioZona> buscarZonasTempPorIdTramite(Long idTram) {

        List<PPredioZona> resultado = new ArrayList<PPredioZona>();
        try {
            resultado = this.pPredioZonaDao.buscarZonasTempPorIdTramite(idTram);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("Error en ConservacionBean#buscarZonasTempPorIdTramite: " + ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacionLocal#guardarListaPPredioZona(List<PPredioZona>)
     * @author leidy.gonzalez
     */
    @Override
    public void guardarListaPPredioZona(List<PPredioZona> pPredioZona) {

        try {
            this.pPredioZonaDao
                .updateMultipleReturn(pPredioZona);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#guardarListaPPredioZona: " +
                ex.getMensaje());
        }
    }

    /**
     * @author felipe.cadena
     * @see IConservacion#eliminarFichaMatrizPredioTerrenoMul(predFichaMatrizPredTerr)
     */
    @Override
    public void eliminarFichaMatrizPredioTerrenoMul(
        List<PFichaMatrizPredioTerreno> predFichaMatrizPredTerr) {

        try {
            this.fichaMatrizPredioTerrenoDao.deleteMultiple(predFichaMatrizPredTerr);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#eliminarFichaMatrizPredioTerrenoMul: " +
                ex.getMensaje(), ex);
        }
    }

    /**
     * @see IConservacion#buscarUnidadDeConstruccionPorUnidadConstruccionId(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PUnidadConstruccionComp> buscarUnidadDeConstruccionPorUnidadConstruccionId(
        Long unidadConstruccionId) {

        List<PUnidadConstruccionComp> resultado = new ArrayList<PUnidadConstruccionComp>();
        try {
            resultado = this.pUnidadConstruccionCompDao.
                buscarUnidadDeConstruccionPorUnidadConstruccionId(unidadConstruccionId);

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarUnidadDeConstruccionPorUnidadConstruccionId: " +
                ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#insertarMultiplePUnidadConstruccionComp(List<PUnidadConstruccionComp>)
     * @author leidy.gonzalez
     */
    @Override
    public void insertarMultiplePUnidadConstruccionComp(
        List<PUnidadConstruccionComp> newUnidadesConstruccionComp) {

        LOGGER.debug(
            "ConservacionBean#insertarPUnidadConstruccionCompRegistros insertando registros");

        try {
            this.pUnidadConstruccionCompDao
                .updateMultiple(newUnidadesConstruccionComp);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#guardarListaPPredioZona: " +
                ex.getMensaje());
        }

    }

    /**
     * @see IConservacion#buscarPprediosCond9PorIdTramite(idTramite)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPprediosCond9PorIdTramite(Long idTramite) {

        List<PPredio> resultado = new ArrayList<PPredio>();
        try {
            resultado = this.pPredioDao.
                buscarPprediosCond9PorIdTramite(idTramite);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#buscarPprediosCond9PorIdTramite: " + ex.
                getMensaje());
        }
        return resultado;
    }

    /**
     * @see IConservacion#redondearNumeroAMil(numero)
     * @author leidy.gonzalez
     */
    @Override
    public void redondearNumeroAMil(double numero) {

        try {
            Double avaluo = Utilidades.redondearNumeroAMil(numero);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#redondearNumeroAMil: " + ex.getMensaje());
        }
    }

    /**
     * @see IConservacion#obtenerPrediosProyectadosAsociadosAFicha(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> obtenerPrediosProyectadosAsociadosAFicha(Long fichaTramId) {

        LOGGER.debug("en ConservacionBean#obtenerPrediosProyectadosAsociadosAFicha");

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.obtenerPrediosProyectadosAsociadosAFicha(fichaTramId);;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPrediosProyectadosAsociadosAFicha: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#buscarPPrediosModPorTramiteId(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPPrediosModPorTramiteId(Long tramId, Long fichaMatrizId) {

        LOGGER.debug("en ConservacionBean#buscarPPrediosModPorTramiteId");

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPrediosModPorTramiteId(tramId, fichaMatrizId);;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#buscarPPrediosModPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#obtenerFichaMatrizTramiteEV(String)
     * @author leidy.gonzalez
     */
    @Override
    public PFichaMatriz obtenerFichaMatrizTramiteEV(String numeroPredial) {

        LOGGER.debug("en ConservacionBean#obtenerFichaMatrizTramiteEV");

        PFichaMatriz answer = null;

        try {
            answer = this.pFichaMatrizDao.obtenerFichaMatrizTramiteEV(numeroPredial);;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerFichaMatrizTramiteEV: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(Long tramId) {

        LOGGER.debug("en ConservacionBean#buscarPPrediosDifFichaMatrizCon0Y5NoCancelados");

        List<PPredio> answer = null;

        try {
            answer = this.pPredioDao.buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(tramId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en ConservacionBean#buscarPPrediosDifFichaMatrizCon0Y5NoCancelados: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IConservacion#guardarJustificacionesPropiedadEV (JustificacionPropiedadDTO
     * JustificacionPropiedadDTO, PPersonaPredio[] propietariosNuevosSeleccionados, UsuarioDTO
     * usuario)
     * @author leidy.gonzalez
     */
    @Override
    public JustificacionPropiedadDTO guardarJustificacionesPropiedadEV(
        JustificacionPropiedadDTO justificacionSeleccionada,
        PPersonaPredio[] propietariosNuevosSeleccionados,
        UsuarioDTO usuario) {
        /*
         * Se recorren los seleccionados por el usuario en la UI
         */
        for (PPersonaPredio pp : propietariosNuevosSeleccionados) {

            PPersonaPredioPropiedad ppp = justificacionSeleccionada.crearPPersonaPredioPropiedad(pp,
                usuario);
            ppp = this.guardarActualizarPPersonaPredioPropiedad(ppp);
            justificacionSeleccionada.adicionarJustificacion(ppp);

        }
        return justificacionSeleccionada;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IConservacionLocal#buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long, Long)
     * @author leidy.gonzalez
     */
    @Override
    public FichaMatrizTorre buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
        Long numeroTorre, Long idFichaMatriz) {
        FichaMatrizTorre answer = null;
        try {
            answer = this.fichaMatrizTorreDao.buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
                numeroTorre, idFichaMatriz);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacion#obtenerZonasPorIdPredio(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PredioZona> obtenerZonasPorIdPredio(Long predioId) {

        List<PredioZona> answer = null;

        try {

            answer = this.predioZonaDAO.obtenerZonasPorIdPredio(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerZonasPorIdPredio: " +
                predioId + ex.getMensaje());
        }
        return answer;

    }

    /**
     * @see IConservacion#buscarPredioPorMatricula(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Override
    public PPredioDireccion obtenerDireccionPrincipalFichaMatrizId(Long predioId) {
        PPredioDireccion answer = null;
        try {
            answer = this.pPredioDireccionDao.obtenerDireccionPrincipalFichaMatrizId(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerDireccionPrincipalFichaMatrizId: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#obtenerPUnidadsConstruccionPorIdPredioYUnidad (Long,String)
     * @author leidy.gonzalez
     */
    @Override
    public PUnidadConstruccion obtenerPUnidadsConstruccionPorIdPredioYUnidad(
        Long idPred, String unidad) {

        PUnidadConstruccion pUnidadesConstruccion = null;

        try {
            pUnidadesConstruccion = this.pUnidadConstruccionDao.
                obtenerPUnidadsConstruccionPorIdPredioYUnidad(
                    idPred, unidad);
        } catch (Exception e) {
            LOGGER.debug(
                "Ocurrio un error al consultar obtenerPUnidadsConstruccionPorIdPredioYUnidad" +
                e.getMessage());
        }
        return pUnidadesConstruccion;

    }

    /**
     * @see IConservacion#consultaPrediosPorNumeroPredial(List<String>)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> consultarPrediosPorPredioIds(List<Long> predioIds) {

        List<Predio> answer = null;

        try {
            answer = this.predioDao.consultaPorPredioIds(predioIds);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#consultarPrediosPorPredioIds: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * Metodo para consultar la configuracion de los reportes por categoria
     *
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<RepConfigParametroReporte> consultaConfiguracionReportePorCat(String categoria) {

        LOGGER.debug("en ConservacionBean#consultaConfiguracionReportePorCat");

        List<RepConfigParametroReporte> answer = null;
        try {
            answer = this.repConfigParametroReporteDAO.obtenerConfiguracionPorCategoria(categoria);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#consultaConfiguracionReportePorCat: " + ex.
                getMensaje(), ex);
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#generarReporteEstadistico(RepReporteEjecucion)
     * @author felipe.cadena
     */
    @Override
    public Object[] generarReporteEstadistico(RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            answer = sncProcedimientoDao
                .generarReporteEstadistico(repReporteEjecucion);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#guardarRangoReporteMultiple(List)
     * @author felipe.cadena
     */
    @Override
    public void guardarRangoReporteMultiple(List<RangoReporte> rangos) {

        try {
            rangoReporteDAO.updateMultiple(rangos);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see IConservacion#obtenerPFichaMatrizPredioContieneNumeroPredial(java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public List<PFichaMatrizPredio> obtenerPFichaMatrizPredioContieneNumeroPredial(
        String numeroPredial) {

        List<PFichaMatrizPredio> answer = null;
        try {
            answer = this.pFichaMatrizPredioDao.obtenerPFichaMatrizPredioContieneNumeroPredial(
                numeroPredial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#obtenerPFichaMatrizPredioContieneNumeroPredial: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see IConservacionLocal#obtenerNumerosTerrenoPorPredioHastaManzana(java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public List<String> obtenerNumerosTerrenoPorPredioHastaManzana(String predioAManzana) {

        LOGGER.debug("en ConservacionBean#obtenerNumsTerrenoPorCondPropiedadEnManzana");

        List<String> answer = null;

        if (predioAManzana.length() < 17) {
            return answer;
        } else if (predioAManzana.length() > 17) {
            predioAManzana = predioAManzana.substring(0, 17);
        }
        try {
            answer = this.predioDao.obtenerNumerosTerrenoPorPredioHastaManzana(predioAManzana);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ConservacionBean#obtenerNumerosTerrenoPorPredioHastaManzana: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacionLocal#obtenerUnidadesConstruccionPorListaPredioId(List<Long>)
     */
    @Override
    public List<UnidadConstruccion> obtenerUnidadesConstruccionPorListaId(
        List<Long> idPUnidadMod) {
        LOGGER.debug("en ConservacionBean#obtenerUnidadesConstruccionPorListaId ");

        List<UnidadConstruccion> answer = null;

        try {
            answer = this.unidadConstruccionDao.obtenerUnidadesConstruccionPorListaId(idPUnidadMod);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerUnidadesConstruccionPorListaId: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacionLocal#obtenerPUnidadConstruccionPorIds(List<Long>)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadConstruccionPorIds(
        List<Long> id) {
        LOGGER.debug("en ConservacionBean#obtenerPUnidadConstruccionPorIds ");

        List<PUnidadConstruccion> answer = null;

        try {
            answer = this.pUnidadConstruccionDao.obtenerPUnidadConstruccionPorIds(id);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPUnidadConstruccionPorIds: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacionLocal#obtenerZonasPorIds(List<Long>)
     */
    @Override
    public List<PredioZona> obtenerZonasPorIds(List<Long> id) {
        LOGGER.debug("en ConservacionBean#obtenerZonasPorIds ");

        List<PredioZona> answer = null;

        try {
            answer = this.predioZonaDAO.obtenerZonasPorIds(id);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerZonasPorIds: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IConservacionLocal#obtenerPZonasPorIds(List<Long>)
     */
    @Override
    public List<PPredioZona> obtenerPZonasPorIds(List<Long> id) {
        LOGGER.debug("en ConservacionBean#obtenerZonasPorIds ");

        List<PPredioZona> answer = null;

        try {
            answer = this.pPredioZonaDao.obtenerPZonasPorIds(id);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en ConservacionBean#obtenerPZonasPorIds: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * Método para verificar si el tramite Comunica Notificacion
     *
     * @author leidy.gonzalez
     */
    public boolean verificaTramiteComunica(Tramite tramite) {
        LOGGER.debug("on AplicacionCambiosMB#verificaTramiteComunica");

        // Parametro Tramites Conservacion basado en las siguientes condiciones:
        // Tipo Tramite, Clase Mutacion, Subtipo, Tipo Radicacion Especial y
        // Tramite de Actualizacion
        String notificaActualizacion;
        boolean tramiteNotifica = false;

        Parametro notificaTramitesConservacionParam = this.generalesService
            .getCacheParametroPorNombre(EParametro.TRAMITES_CONSERVACION_NOTIFICAN.toString());
        String[] listaParametrosConservacionNotifican = null;

        if (notificaTramitesConservacionParam.getValorCaracter() != null) {
            listaParametrosConservacionNotifican = notificaTramitesConservacionParam
                .getValorCaracter().split(",");
        }

        String tramitesComunicanNotificacion = "";

        if (tramite.getTipoTramite() != null) {

            int contadorTramiteNotificanRectificacion = 0;

            if (!tramite.isCancelacionMasiva()) {

                tramitesComunicanNotificacion = tramite.getTipoTramite();
            }

            if (tramite.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()) &&
                tramite.getTramiteRectificacions() != null) {

                for (TramiteRectificacion tr : tramite.getTramiteRectificacions()) {

                    if (tr.getDatoRectificar() != null && EDatoRectificarTipo.PREDIO.toString().
                        equalsIgnoreCase(tr.getDatoRectificar().getTipo())) {

                        if (tramite.isRectificacionMatriz() &&
                            EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().
                                equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

                            tramitesComunicanNotificacion = tramitesComunicanNotificacion + "-" +
                                tr.getDatoRectificar().getNombre() + "-" + tramite.
                                getRadicacionEspecial();
                        }

                        if (EDatoRectificarNombre.AREA_TERRENO.getCodigo().equalsIgnoreCase(tr.
                            getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.AREA_CONSTRUIDA.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.COEFICIENTE.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.DIMENSION.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.ZONA_FISICA.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.ZONA_GEOECONOMICA.getCodigo().equalsIgnoreCase(
                                tr.getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().
                                equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

                            //@modified by leidy.gonzalez:: #16317::08/03/2016 Modifica tramitesComunicanNotificacion para cuando son rectificacion,
                            // debido a que puede existir mas de un tipo de rectificacion a notificar
                            if (contadorTramiteNotificanRectificacion >= 1) {
                                tramitesComunicanNotificacion =
                                    tramitesComunicanNotificacion + "," + tramite.getTipoTramite() +
                                    "-" + tr.getDatoRectificar().getNombre();

                            } else {

                                tramitesComunicanNotificacion =
                                    tramitesComunicanNotificacion + "-" + tr.getDatoRectificar().
                                        getNombre();
                                contadorTramiteNotificanRectificacion++;
                            }

                        }

                    }
                    continue;
                }

            }

            if (tramite.getClaseMutacion() != null) {

                tramitesComunicanNotificacion = tramitesComunicanNotificacion +
                    "-" + tramite.getClaseMutacion();

                if ((tramite.getSubtipo() != null && !tramite.getSubtipo().equals("")) &&
                    (tramite.getRadicacionEspecial() != null && !tramite.getRadicacionEspecial().
                    equals(""))) {

                    tramitesComunicanNotificacion = tramitesComunicanNotificacion + "-" +
                        tramite.getSubtipo() + "-" +
                        tramite.getRadicacionEspecial();

                }

            }

            //@modified by leidy.gonzalez:: #16317::08/03/2016
            //Tramites de Rectificacion van a Comunicar Notificacion
            if (tramite.getTipoTramite().equals("2")) {
                String[] rectificacionSplit = tramitesComunicanNotificacion.split(",");
                String rectificanSeparado;

                for (int i = 0; i < listaParametrosConservacionNotifican.length; i++) {
                    notificaActualizacion = listaParametrosConservacionNotifican[i];

                    for (int j = 0; j < rectificacionSplit.length; j++) {
                        rectificanSeparado = rectificacionSplit[j];

                        if (rectificanSeparado.equals(notificaActualizacion)) {

                            tramiteNotifica = true;

                        }
                    }
                }

            } //Se quita que los tramites de cancelacion vayan a comunicar norificacion
            else {

                for (int i = 0; i < listaParametrosConservacionNotifican.length; i++) {
                    notificaActualizacion = listaParametrosConservacionNotifican[i];

                    if (tramitesComunicanNotificacion.equals(notificaActualizacion)) {

                        tramiteNotifica = true;

                    }
                }

            }

        }

        return tramiteNotifica;
    }

    /**
     * Método para avanzar en process el tramite A Comunicar Notificacion
     *
     * @author leidy.gonzalez
     */
    public boolean avanzarTramiteAComunicarNotificacion(Tramite tramite,
        UsuarioDTO usuario, Actividad actividad) {
        LOGGER.debug("on AplicacionCambiosMB#avanzarTramiteAComunicarNotificacion");

        boolean avanzoAComunicar = false;

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();

        // Avance a comunicar Notificacion Process
        solicitudCatastral.setUsuarios(this.tramiteService
            .buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionEstructuraOrganizacional(),
                ERol.SECRETARIA_CONSERVACION));
        solicitudCatastral
            .setTransicion(
                ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION);

        try {

            solicitudCatastral = procesosService.avanzarActividad(usuario, actividad.getId(),
                solicitudCatastral);

        } catch (Exception e) {

            LOGGER.error(
                "Ocurrió un error a avanzar el tramite:" + tramite.getId() +
                " a la actividad de Comunicar Notificacion", e);
        }

        if (solicitudCatastral != null) {
            avanzoAComunicar = true;
        } else {
            avanzoAComunicar = false;
        }

        return avanzoAComunicar;
    }

    /**
     * Método que envia a procesar el tramite a Aplicar Cambios
     *
     * @author leidy.gonzalez
     */
    public boolean procesarTramiteAplicarCambiosConservacion(Tramite tramite,
        UsuarioDTO usuario, Actividad actividad) {

        boolean aplicoCambios = false;

        tramite.setActividadActualTramite(actividad);
        LOGGER.debug("on AplicacionCambiosMB#procesarTramiteAplicarCambiosConservacion");

        //se marca el tramite como en aplicar cambios
        tramite.setEstadoGdb(Constantes.ACT_APLICAR_CAMBIOS);
        tramite = tramiteService.guardarActualizarTramite2(tramite);

        try {

            if (tramite.isTramiteGeografico()) {

                aplicoCambios = conservacionService.aplicarCambiosConservacion(tramite, usuario, 0);

            } else if (!tramite.isTramiteGeografico()) {

                aplicoCambios = conservacionService.aplicarCambiosConservacion(tramite, usuario, 1);
            }

        } catch (Exception e) {

            LOGGER.error(
                "Ocurrió un error a aplicar cambios del tramite:" + tramite.getId() +
                " en la actividad de Generar Resolucion", e);
        }
        return aplicoCambios;
    }
}
