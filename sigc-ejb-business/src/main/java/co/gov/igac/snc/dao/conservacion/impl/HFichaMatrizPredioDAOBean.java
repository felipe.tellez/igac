package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IHFichaMatrizPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatrizPredio;

@Stateless
public class HFichaMatrizPredioDAOBean extends
    GenericDAOWithJPA<HFichaMatrizPredio, Long> implements
    IHFichaMatrizPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(HFichaMatrizPredioDAOBean.class);

    /**
     * @see IHFichaMatrizPredioDAOBean#buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(String,
     * Long)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<HFichaMatrizPredio> buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(
        String estado, Long fichaMatrizId) {

        List<HFichaMatrizPredio> answer = new ArrayList<HFichaMatrizPredio>();
        try {

            // Se consultan los históricos de fecha más reciente
            String sqlHistoricosRecientes =
                "SELECT hfmp.id, hfmp.numero_predial, hfmp.h_predio_id, MAX(hfmp.fecha_log)" +
                " FROM h_ficha_matriz_predio hfmp " +
                " WHERE hfmp.ficha_matriz_id = :fichaMatrizId " +
                " AND hfmp.estado = :estado " +
                " AND hfmp.h_predio_id IN " +
                " (SELECT MAX(h_predio_id) " +
                "	FROM h_ficha_matriz_predio " +
                " WHERE ficha_matriz_id = hfmp.ficha_matriz_id " +
                " AND numero_predial = hfmp.numero_predial)" +
                " GROUP BY hfmp.numero_predial, hfmp.h_predio_id, hfmp.id ";

            Query queryHistoricosRecientes = this.entityManager.createNativeQuery(
                sqlHistoricosRecientes);

            queryHistoricosRecientes.setParameter("fichaMatrizId", fichaMatrizId);
            queryHistoricosRecientes.setParameter("estado", estado);
            List<Object[]> resultado = queryHistoricosRecientes.getResultList();

            if (resultado != null && !resultado.isEmpty()) {
                for (Object object : resultado) {

                    Object[] item = (Object[]) object;
                    Long hfmpId = ((BigDecimal) item[0]).longValue();
                    Long hfmpHPredioId = ((BigDecimal) item[2]).longValue();
                    if (hfmpId != null && hfmpHPredioId != null) {

                        // Consulta del HFichaMatrizPredio
                        String sql =
                            "SELECT DISTINCT * FROM HFichaMatrizPredio WHERE id = :id AND HPredio.id = :hPredioId ";

                        Query query = this.entityManager.createQuery(sql);
                        query.setParameter("id", hfmpId);
                        query.setParameter("hPredioId", hfmpHPredioId);
                        List<HFichaMatrizPredio> answerTemp = query.getResultList();

                        if (answerTemp != null) {
                            answer.addAll(answerTemp);
                        }
                    }
                }
            }
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Error en la búsqueda del HFichaMatrizPredio: " + e.getMessage());
            return null;
        }
    }

}
