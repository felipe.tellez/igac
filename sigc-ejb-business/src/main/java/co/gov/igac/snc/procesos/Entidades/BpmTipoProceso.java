/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_TIPO_PROCESO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmTipoProceso.findAll", query = "SELECT b FROM BpmTipoProceso b"),
    @NamedQuery(name = "BpmTipoProceso.findByProceso", query = "SELECT b FROM BpmTipoProceso b WHERE b.proceso = :proceso"),
    @NamedQuery(name = "BpmTipoProceso.findById", query = "SELECT b FROM BpmTipoProceso b WHERE b.id = :id"),
    @NamedQuery(name = "BpmTipoProceso.findByMapaProceso", query = "SELECT b FROM BpmTipoProceso b WHERE b.mapaProceso = :mapaProceso")})
public class BpmTipoProceso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "PROCESO")
    private String proceso;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "MAPA_PROCESO")
    private String mapaProceso;

    public BpmTipoProceso() {
    }

    public BpmTipoProceso(BigDecimal id) {
        this.id = id;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getMapaProceso() {
        return mapaProceso;
    }

    public void setMapaProceso(String mapaProceso) {
        this.mapaProceso = mapaProceso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmTipoProceso)) {
            return false;
        }
        BpmTipoProceso other = (BpmTipoProceso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmTipoProceso[ id=" + id + " ]";
    }
    
}
