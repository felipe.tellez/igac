package co.gov.igac.snc.dao.sig.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerResponseErrorDTO;
import co.gov.igac.snc.util.exceptions.RemoteServiceException;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Manejo de la respuesta de Arcgis Server
 *
 * @author juan.mendez
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArcgisServerResponse implements Serializable {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ArcgisServerResponse.class);

    /**
     *
     */
    private static final long serialVersionUID = 4290907874289848440L;

    public static final String ESRI_JOB_SUBMITTED = "esriJobSubmitted";
    public static final String ESRI_JOB_EXECUTING = "esriJobExecuting";
    public static final String ESRI_JOB_FAILED = "esriJobFailed";
    public static final String ESRI_JOB_WAITING = "esriJobWaiting";

    private String jobId;
    private String jobStatus;
    private ArcgisServerResponseErrorDTO error;
    private ArcGisServerMessage[] messages;
    private ArcGisServerResult results;

    public ArcGisServerResult getResults() {
        return results;
    }

    public void setResults(ArcGisServerResult results) {
        this.results = results;
    }

    public ArcgisServerResponseErrorDTO getError() {
        return error;
    }

    public void setError(ArcgisServerResponseErrorDTO error) {
        this.error = error;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public ArcGisServerMessage[] getMessages() {
        return messages;
    }

    public void setMessages(ArcGisServerMessage[] messages) {
        this.messages = messages;
    }

    /**
     * Valida los mensajes de error del servidor (Errores de conexión)
     *
     * @param resultMap
     */
    public void validateServerError() throws RemoteServiceException {
        if (error != null) {
            String message = error.getMessage();
            LOGGER.error(message);
            if (message.contains("Unable to get geoprocessing service info for service")) {
                message =
                    "Error al intentar ejecutar el toolbox. El servicio no está publicado o no se encuentra disponible : ( " +
                    message + " )";
            } else if (message.contains("Error submitting task") || message.contains(
                "Please check your parameters")) {
                message =
                    "Error al enviar el Job. El toolbox no está publicado, se encuentra detenido o no se encuentra disponible : ( " +
                    message + " )";
            } else if (message.contains("Invalid URL")) {
                message =
                    "Error al ejecutar el toolbox. El servicio no está publicado en el servidor : ( " +
                    message + " )";
            }
            StringBuffer msg = new StringBuffer("CN010020");
            msg.append("Error de ejecución de Servicio Remoto. ").append(message);
            throw new RemoteServiceException(msg.toString());
        }
    }

    /**
     * Valida si ocurrió un error durante la ejecución del job
     *
     * @throws RemoteServiceException
     */
    public void validateJobError() throws RemoteServiceException {
        //LOGGER.debug(this.toString());
        //LOGGER.debug(this.jobStatus);
        if (this.jobStatus == null || this.jobStatus.equals(ArcgisServerResponse.ESRI_JOB_FAILED)) {
            String lastMessageDescription = null;
            if (messages != null) {
                ArcGisServerMessage lastMessage = messages[messages.length - 1];
                lastMessageDescription = lastMessage.getDescription();
            }
            //throw SncBusinessServiceExceptions.EXCEPCION_100008.getExcepcion(LOGGER,null,lastMessageDescription, this.jobId);
            StringBuffer msg = new StringBuffer("CN010020");
            msg.append("Error de ejecución de Servicio Remoto. ").append(lastMessageDescription);
            throw new RemoteServiceException(msg.toString());
        }
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Verifica si el job se está ejecutando
     *
     * @return
     */
    public boolean isJobRunning() {
        boolean running = false;
        if (this.jobStatus.equals(ArcgisServerResponse.ESRI_JOB_SUBMITTED) ||
            this.jobStatus.equals(ArcgisServerResponse.ESRI_JOB_EXECUTING) ||
            this.jobStatus.equals(ArcgisServerResponse.ESRI_JOB_WAITING)) {
            running = true;
        }
        return running;
    }

}
