package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IUsoHomologadoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.UsoHomologado;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Proyecto SNC 2017
 */
/**
 * Metodos de acceso a datos para la clase UsoHomologado
 *
 * @author felipe.cadena
 */
@Stateless
public class UsoHomologadoDAOBean extends GenericDAOWithJPA<UsoHomologado, Long> implements
    IUsoHomologadoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsoHomologadoDAOBean.class);

}
