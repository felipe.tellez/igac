package co.gov.igac.snc.fachadas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.dao.conservacion.IPPersonaDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioPropiedadDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.ICodigoHomologadoDAO;
import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.snc.dao.generales.IPaisDAO;
import co.gov.igac.snc.dao.generales.IParametroDAO;
import co.gov.igac.snc.dao.iper.IInfInterrelacionadaDAO;
import co.gov.igac.snc.dao.iper.IInfInterrelacionadaReDAO;
import co.gov.igac.snc.dao.iper.IPropietarioReDAO;
import co.gov.igac.snc.dao.tramite.IModeloResolucionDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteTextoResolucionDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionada;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionadaRe;
import co.gov.igac.snc.persistence.entity.i11n.Propietario;
import co.gov.igac.snc.persistence.entity.i11n.PropietarioRe;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESistema;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;
import co.gov.igac.snc.util.constantes.ConstantesResolucionesTramites;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.Random;

@Stateless
//@Interceptors(TimeInterceptor.class)
public class InterrelacionBean implements IInterrelacion, Serializable {

    /**
     * @author fredy.wilches
     */
    private static final long serialVersionUID = -6903219491637196277L;

    private static final Logger LOGGER = LoggerFactory.getLogger(InterrelacionBean.class);

    @EJB
    private IInfInterrelacionadaDAO dao;

    @EJB
    private IInfInterrelacionadaReDAO interrelacionadaReDao;

    @EJB
    private IPropietarioReDAO propietarioReDao;

    @EJB
    private ISolicitudDAO solicitudDao;

    @EJB
    private ITramiteDAO tramiteDao;

    @EJB
    private IPaisDAO paisDao;

    @EJB
    private IJurisdiccionDAO jurisdiccionDao;

    @EJB
    private ISolicitanteDAO solicitanteDao;

    @EJB
    private ISolicitanteSolicitudDAO solicitanteSolicitudDao;

    @EJB
    private IPredioDAO predioDao;

    @EJB
    private IGeneralesLocal generalesService;

    @EJB
    private IConservacionLocal conservacionService;

    @EJB
    private ITramiteLocal tramiteService;

    @EJB
    private IProcesosLocal procesosService;

    @EJB
    private ICodigoHomologadoDAO codigoHomologadoDao;

    @EJB
    private IPPersonaPredioDAO personaPredioDAO;

    @EJB
    private IPPersonaPredioPropiedadDAO personaPredioPropiedadDAO;

    @EJB
    private IPPersonaDAO personaDAO;

    @EJB
    private IModeloResolucionDAO modeloResolucionDao;

    @EJB
    private ITramiteTextoResolucionDAO tramiteTextoResolucionDao;

    @EJB
    private IParametroDAO parametroDao;

    private static final SimpleDateFormat formatoFechaTituloResolucion = new SimpleDateFormat(
        Constantes.FORMATO_FECHA_TITULO_RESOLUCION);

    public Solicitud procesarPendientes() {

        LOGGER.debug("Inicio InterrelacionBean:procesarPendientes");

        List<InfInterrelacionada> tramites = dao.getTramitesPrimeraPendientes();
        // Deben haber tramites pendientes para crear una solicitud

        Solicitud s = new Solicitud();
        if (tramites != null && tramites.size() > 0) {

            LOGGER.debug("Tramites " + tramites.size());

            s.setTramites(new ArrayList<Tramite>());
            s.setFolios(0);
            s.setAnexos(0);
            s.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
            s.setAsunto("Trámites catastrales IPER");
            s.setObservaciones("");
            s.setTipoDocumentoCorrespondencia("998");
            s.setSistemaEnvio("96");

            // TODO: Clara
            //s.setSistemaEnvio(sistemaEnvio);
            SolicitanteSolicitud ss = new SolicitanteSolicitud();
            String tipoIdentificacionIper = this.parametroDao.getParametroPorNombre(
                EParametro.TIPO_IDENTIFICACION_SOLICITANTE_IPER.toString()).getValorCaracter();

            Solicitante solicitante = this.solicitanteDao.
                buscarSolicitantePorTipoYNumeroDeDocumento(this.parametroDao.getParametroPorNombre(
                    EParametro.DOCUMENTO_SOLICITANTE_IPER.toString()).getValorCaracter(),
                    tipoIdentificacionIper);
            if (solicitante == null) {
                solicitante = new Solicitante();
                solicitante.setId(1836L);
            }
            solicitante.setUsuarioLog("IPER");
            solicitante.setFechaLog(new Date());
            ss.setNotificacionEmail(" ");
            ss.setUsuarioLog("IPER");
            ss.setFechaLog(new Date());

            solicitante.setTipoPersona(Constantes.TIPO_PERSONA_IPER);
            ss.setTipoPersona(Constantes.TIPO_PERSONA_IPER);

            solicitante.setTipoIdentificacion(tipoIdentificacionIper);
            ss.setTipoIdentificacion(tipoIdentificacionIper);

            solicitante.setNumeroIdentificacion("999999999");
            ss.setNumeroIdentificacion("999999999");

            solicitante.setRazonSocial("Interrelación catastro y registro - IPER");
            ss.setRazonSocial("Interrelación catastro y registro - IPER");

            solicitante.setSigla("IPER");
            ss.setSigla("IPER");

            Pais p = paisDao.findById(Constantes.COLOMBIA);
            solicitante.setDireccionPais(p);
            ss.setDireccionPais(p);

            solicitante.setRelacion(ESolicitanteSolicitudRelac.INTERMEDIARIO.toString());
            ss.setRelacion(ESolicitanteSolicitudRelac.INTERMEDIARIO.toString());

            solicitante = this.solicitanteDao.update(solicitante);
            ss.setSolicitante(solicitante);
            ss.setSolicitud(s);

            List<SolicitanteSolicitud> soliSoli = new ArrayList<SolicitanteSolicitud>();

            s.setEstado(ESolicitudEstado.RECIBIDA.getCodigo());

            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setCodigoTerritorial("5000"); // CENTRAL, SUB_CATASTRO
            usuario.setDescripcionTerritorial("SUB_CATASTRO");
            usuario.setLogin("IPER");
            soliSoli.add(ss);
            s.setFecha(new Date());
            s.setFinalizado(ESiNo.SI.getCodigo());
            s.setUsuarioLog("SNC-IPER");
            s.setFechaLog(new Date());

            s.setSolicitanteSolicituds(soliSoli);

            Object[] resultado = this.obtenerNumeroSolicitud(s, usuario);
            if (resultado != null && resultado.length > 0 && !((new BigDecimal(-1)).equals(
                resultado[0]))) {
                s.setNumero(resultado[5].toString());
            } else {
                LOGGER.error("Solicitud no numerada por error en la radicación con correspondencia");
                LOGGER.error("" + resultado[1]);
                LOGGER.error("" + resultado[4]);
                Random generadorAleatoreo = new Random();
                s.setNumero(String.valueOf(generadorAleatoreo.nextLong()));

            }
            s.getSolicitanteSolicituds().get(0).setSolicitud(s);
            s = this.solicitudDao.update(s);

            ss.setSolicitud(s);
            ss.setId(s.getSolicitanteSolicituds().get(0).getId());

            //ss=solicitanteSolicitudDao.update(ss);
            //soliSoli.add(ss);
            LOGGER.debug("Solicitud " + s);

            for (InfInterrelacionada i : tramites) {

                boolean automatico = true;
                boolean procesar = true;

                Predio predio = this.existePredio(i.getNumeroCatastro());

                Tramite t = new Tramite();
                t.setObservacionesRadicacion(" ");
                if (predio == null) {
                    automatico = false;
                    t.setObservacionesRadicacion("Predio no se encuentra en la base del SNC");
                } else {
                    if (predio.isEstaBloqueado()) {
                        automatico = false;
                        t.setObservacionesRadicacion("Predio se encuentra bloqueado");
                    } else {
                        for (PersonaPredio pp : predio.getPersonaPredios()) {
                            Persona per = pp.getPersona();
                            if (per.getBloqueada()) {
                                automatico = false;
                                t.setObservacionesRadicacion(
                                    "Predio tiene propietario(s) bloqueado(s)");
                                break;
                            }
                        }
                        if (automatico) {
                            boolean propietariosIguales = this.sonPropietariosIguales(predio.
                                getPersonaPredios(), i.getPropietarios());
                            if (propietariosIguales) {
                                automatico = false;
                                t.setObservacionesRadicacion("Trámite posiblemente ya elaborado");
                            } else {
                                for (Propietario pro : i.getPropietarios()) {
                                    if (pro.getTipoDocumento() == null ||
                                        pro.getNumeroDocumento() == null || ((pro.getNombre1() ==
                                        null || pro.getApellido1() == null) &&
                                        pro.getRazonSocial() == null)) {
                                        automatico = false;
                                        t.setObservacionesRadicacion(
                                            "Trámite con información incompleta");
                                        break;
                                    }
                                }
                                if (i.getInstrumento() == null || i.getOficinaOrigenInstrumento() ==
                                    null || i.getCodigoDepartamento() == null || i.
                                    getCodigoMunicipio() == null || i.getNumeroInstrumento() == null ||
                                    i.getFechaInstrumento() == null) {
                                    automatico = false;
                                    t.setObservacionesRadicacion(
                                        "Trámite con información incompleta");
                                }
                                if (automatico) {
                                    List<Tramite> tramitesPredio = tramiteService.
                                        buscarTramitesPendientesDePredio(predio.getId());
                                    if (tramitesPredio != null && tramitesPredio.size() > 0) {
                                        procesar = false;
                                    }
                                }
                            }

                        }
                    }
                }
                if (procesar) {
                    t.setSolicitud(s);
                    t.setTipoTramite(ETramiteTipoTramite.MUTACION.getCodigo());
                    t.setEstado(ETramiteEstado.RECIBIDO.getCodigo());
                    t.setArchivado(ESiNo.NO.getCodigo());
                    t.setUsuarioLog("IPER");
                    t.setFechaLog(new Date());
                    t.setFechaRadicacion(new Date());
                    t.setClaseMutacion(EMutacionClase.PRIMERA.getCodigo());
                    t.setPredio(predio);

                    LOGGER.debug("Tramite " + t);
                }

                if (automatico) {
                    if (procesar) {
                        resultado = this.generalesService.generarNumeracion(
                            ENumeraciones.NUMERACION_RADICACION_CATASTRAL,
                            "0", predio.getDepartamento().getCodigo(), predio.getMunicipio().
                            getCodigo(), 0);
                        t.setNumeroRadicacion(resultado[0].toString());
                    }
                } else {

                }

                if (procesar) {

                    tramiteDao.persist(t);
                    //t.setId(t1.getId());
                    t.setDepartamento(predio.getDepartamento());
                    t.setMunicipio(predio.getMunicipio());
                    LOGGER.debug("Tramite " + t);

                    SolicitudCatastral sc = new SolicitudCatastral();
                    sc.setIdentificador(t.getId());
                    sc.setNumeroSolicitud(s.getNumero());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(t.getFechaRadicacion());
                    sc.setFechaRadicacion(cal);
                    sc.setNumeroRadicacion(t.getNumeroRadicacion());

                    sc.setNumeroPredial(t.getPredio().getNumeroPredial());
                    String jurisdiccion = jurisdiccionDao.
                        getEstructuraOrganizacionalIdByMunicipioCod(i.getCodigoDepartamento() + i.
                            getCodigoMunicipio());
                    String eo = codigoHomologadoDao.obtenerCodigoHomologado(
                        ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL, ESistema.SNC, ESistema.LDAP,
                        jurisdiccion, null);
                    sc.setTerritorial(eo);
                    sc.setTipoTramite(t.getTipoTramiteCadenaCompleto());

                    if (automatico) {

                        // POR USUARIO
                        // TODO: FALTA ACT_VALIDACION_GENERAR_RESOLUCION_IPER
                        sc.setRol(ERol.RESPONSABLE_CONSERVACION.getDistinguishedName());
                        sc.setTransicion(
                            ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION_IPER);
                        sc.setUsuarios(LdapDAO.getUsuariosEstructuraRol2(eo,
                            ERol.RESPONSABLE_CONSERVACION));
                        t.setFuncionarioEjecutor(Constantes.FUNCIONARIO_EJECUTOR_IPER);
                        i.setEstado("En proceso");

                        // TODO : proyectar, inscripciones y decretos, resolucion?
                        // TODO : poner nuevos propietarios
                    } else {
                        // POR ROL
                        sc.setRol(ERol.RESPONSABLE_CONSERVACION.getDistinguishedName());
                        sc.setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

                        i.setAutomatica(ESiNo.NO.getCodigo());

                        // TODO : como es manual donde pone los propietarios?
                    }
                    dao.update(i);

                    LOGGER.debug("Solicitud catastral " + sc);

                    t.setProcesoInstanciaId(this.procesosService.crearProceso(sc));
                    t.setPredioId(predio.getId());
                    t.setInfInterrelacionada(i);
                    Tramite t1 = tramiteDao.update(t);

                    TramiteTextoResolucion tramiteTextoResolucion = new TramiteTextoResolucion();
                    tramiteTextoResolucion.setFechaLog(new Date());
                    tramiteTextoResolucion.setTramite(t);
                    tramiteTextoResolucion.setModeloResolucion(this.
                        reemplazarParametrosTextoConsiderandoResuelve(t, usuario));
                    tramiteTextoResolucion.setTitulo("RESOLUCIÓN PRIMERA");
                    tramiteTextoResolucion.setConsiderando(tramiteTextoResolucion.
                        getModeloResolucion().getTextoConsiderando());
                    tramiteTextoResolucion.setResuelve(tramiteTextoResolucion.getModeloResolucion().
                        getTextoResuelve());
                    tramiteTextoResolucion.setFechaLog(new Date());
                    tramiteTextoResolucion.setUsuarioLog(usuario.getLogin());

                    tramiteTextoResolucionDao.persist(tramiteTextoResolucion);

                    s.getTramites().add(t);

                    LOGGER.debug(" *********** TRAMITE IPER ********** " + t.getId());

                    // TODO : cambiar estado en INF_INTERRELACIONADA
                }

                InfInterrelacionadaRe res = new InfInterrelacionadaRe(i, s, t, predio, automatico,
                    procesar);
                interrelacionadaReDao.update(res);
                for (Propietario pr : i.getPropietarios()) {
                    PropietarioRe proR = new PropietarioRe(pr);
                    proR.setInfInterrelacionadaRNciId(res.getNciId());
                    propietarioReDao.persist(proR);
                }

            }
            if (s.getTramites().size() > 0) {
                s = this.solicitudDao.update(s);
            }
        }
        return s;
    }

    public Object[] obtenerNumeroSolicitud(Solicitud solicitud, UsuarioDTO usuario) {

        if (solicitud.getSolicitanteSolicituds() == null || solicitud.getSolicitanteSolicituds().
            isEmpty()) {
            throw SncBusinessServiceExceptions.EXCEPCION_NEGOCIO_0001.getExcepcion(LOGGER);
        }

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Territorial=compania
        parametros.add("SC_SNC"); // Usuario
        parametros.add("ER"); // tipo correspondencia
        parametros.add(solicitud.getTipo()); // tipo contenido solicitud.getTipo()
        parametros.add(new BigDecimal(solicitud.getFolios()));
        parametros.add(new BigDecimal(solicitud.getAnexos()));
        parametros.add(solicitud.getAsunto()); // Asunto
        parametros.add(solicitud.getObservaciones()); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add(solicitud.getSistemaEnvio()); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("1"); // presentacion, 6 internet
        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Destino lugar
        parametros.add(""); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(""); // Direccion destino
        parametros.add(""); // Tipo identificacion destino
        parametros.add(""); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getTipoIdentificacion()); // Solicitante tipo documento
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getNumeroIdentificacion()); // Solicitante  identificacion
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getNombreCompleto()); //
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getTelefonoPrincipal()); //
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getCorreoElectronico()); //

        Object[] resultado = this.generalesService.radicarEnCorrespondencia(parametros);
        return resultado;
    }

    public Predio existePredio(String numeroPredial) {
        return predioDao.getPredioByNumeroPredialAnteriorOActual(numeroPredial);
    }

    public boolean sonPropietariosIguales(List<PersonaPredio> pps, List<Propietario> ps) {
        if (pps.size() == ps.size()) {
            List<Persona> personasIper = new ArrayList<Persona>();
            for (Propietario p : ps) {
                Persona per = new Persona(p.getTipoDocumento(), p.getNumeroDocumento(), p.
                    getNombre1(), p.getNombre2(), p.getApellido1(), p.getApellido2(), p.
                    getRazonSocial());
                personasIper.add(per);
            }
            for (PersonaPredio pp : pps) {
                Persona p = pp.getPersona();
                boolean esta = false;
                for (Persona p2 : personasIper) {
                    if (p.equals(p2)) {
                        esta = true;
                        break;
                    }
                }
                if (!esta) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }

    public void proyectarPropietarios(Solicitud s) {
        for (Tramite t : s.getTramites()) {
            try {
                PPredio pp = this.conservacionService.obtenerPPredioCompletoByIdTramite(t.getId());
                for (PPersonaPredio ppp : pp.getPPersonaPredios()) {

                    ppp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                    ppp.getPPersona().setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
                        getCodigo());
                    personaPredioDAO.update(ppp);

                    for (PPersonaPredioPropiedad pppp : ppp.getPPersonaPredioPropiedads()) {
                        pppp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                        personaPredioPropiedadDAO.update(pppp);
                    }

                }
                for (Propietario pro : t.getInfInterrelacionada().getPropietarios()) {

                    PPersona pper = new PPersona(pro);
                    pper.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    personaDAO.persist(pper);

                    PPersonaPredio ppp = new PPersonaPredio();
                    ppp.setPPersona(pper);
                    ppp.setPPredio(pp);
                    ppp.setTipo("PROPIETARIO");
                    ppp.setFechaLog(new Date());
                    ppp.setUsuarioLog("IPER");
                    ppp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    personaPredioDAO.persist(ppp);

                    PPersonaPredioPropiedad pppp = new PPersonaPredioPropiedad();
                    pppp.setPPersonaPredio(ppp);
                    pppp.setTipo("Derecho"); // Tomado del dominio PERSONA_PREDIO_PROPIEDAD_TI
                    pppp.setEntidadEmisora("Registro"); // Tomado del dominio DERECHO_PROPIEDAD_ENTIDAD_E
                    //pppp.setDepartamento(departamento) Se depende de que IPER lo tenga
                    ///pppp.setMunicipio(municipio) Se depende de que IPER convierta a codigo DANE
                    //pppp.setFalsaTradicion(falsaTradicion)
                    pppp.setFechaRegistro(t.getInfInterrelacionada().getFechaInstrumento());
                    pppp.setNumeroRegistro(t.getInfInterrelacionada().getNumeroInstrumento());
                    pppp.setModoAdquisicion(t.getInfInterrelacionada().getModoAdquisicion()); // Tomado del dominio DERECHO_PROPIEDAD_MODO_ADQU

                    pppp.setFechaLog(new Date());
                    pppp.setUsuarioLog("IPER");
                    pppp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    personaPredioPropiedadDAO.persist(pppp);

                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }

        }
    }

    public ModeloResolucion reemplazarParametrosTextoConsiderandoResuelve(Tramite t,
        UsuarioDTO usuario) {

        List<ModeloResolucion> modelos = modeloResolucionDao.findModelosResolucionByTramite(t.
            getTipoTramite(), t.getClaseMutacion());
        ModeloResolucion modeloResolucionSeleccionado = modelos.get(0);

        String tipoTramite = t.getTipoTramite();

        Locale colombiaLocale = new Locale("ES", "es_CO");

        String estructuraOrganizacional = null;
        String responsableConservacion = null;

        Object parametrosTextoConsiderando[] = null;
        Object parametrosTextoResuelve[] = null;

        if (usuario.isTerritorial()) {
            estructuraOrganizacional = ConstantesResolucionesTramites.DIRECCION_TERRITORIAL +
                usuario.getDescripcionEstructuraOrganizacional()
                    .toUpperCase();
            responsableConservacion =
                ConstantesResolucionesTramites.RESPONSABLE_DIRECCION_TERRITORIAL;
        }
        if (usuario.isUoc()) {
            estructuraOrganizacional = ConstantesResolucionesTramites.UNIDAD_OPERATIVA +
                usuario.getDescripcionEstructuraOrganizacional()
                    .replaceAll("UOC_", "").toUpperCase();
            responsableConservacion = ConstantesResolucionesTramites.RESPONSABLE_UNIDAD_OPERATIVA;
        }

        StringBuilder solicitantes = new StringBuilder();
        StringBuilder identificaciones = new StringBuilder();
        StringBuilder condiciones = new StringBuilder();
        StringBuilder radicar = new StringBuilder();

        for (SolicitanteSolicitud solicitanteSolicitud : t
            .getSolicitud().getSolicitanteSolicituds()) {
            solicitantes
                .append(solicitanteSolicitud
                    .getNombreCompleto()
                    .substring(
                        0,
                        solicitanteSolicitud.getNombreCompleto()
                            .length() - 1).toUpperCase() +
                    ", ");
            identificaciones.append(solicitanteSolicitud
                .getTipoIdentificacion() +
                ". No. " +
                solicitanteSolicitud.getNumeroIdentificacion() + ", ");
            condiciones.append(solicitanteSolicitud.getRelacion().toUpperCase() +
                ", ");
        }

        StringBuilder senorIdentificacion = new StringBuilder();
        if (t.getSolicitud().getSolicitanteSolicituds().size() > 1) {
            senorIdentificacion.append(ConstantesResolucionesTramites.SENIORES +
                solicitantes +
                ConstantesResolucionesTramites.IDENTIFICADOS +
                identificaciones +
                ConstantesResolucionesTramites.CONDICIONES + condiciones +
                ConstantesResolucionesTramites.RESPECTIVAMENTE);
            radicar.append(ConstantesResolucionesTramites.RADICARON);
        }
        if (t.getSolicitud().getSolicitanteSolicituds().size() == 1) {
            senorIdentificacion.append(ConstantesResolucionesTramites.SENIOR +
                solicitantes +
                ConstantesResolucionesTramites.IDENTIFICADO +
                identificaciones +
                ConstantesResolucionesTramites.CONDICION + condiciones);
            radicar.append(ConstantesResolucionesTramites.RADICO);
        }

        radicar.append(" " + ConstantesResolucionesTramites.NUMERO_RADICADO +
            t.getNumeroRadicacion() + ", ");

        List<Dominio> tiposSolicitud = this.generalesService
            .getCacheDominioPorNombre(EDominio.SOLICITUD_TIPO);
        String tipoSolicitudValor = null;
        for (Dominio tmp : tiposSolicitud) {
            if (tmp.getCodigo().equals(t.getSolicitud().getTipo())) {
                tipoSolicitudValor = tmp.getValor().toUpperCase();
                break;
            }
        }
        String documentosJustificativos = this.formarDocumentosJustificativos(t);

        if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) ||
            tipoTramite.equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {

            parametrosTextoConsiderando = new Object[8];
            parametrosTextoResuelve = new Object[5];

            parametrosTextoConsiderando[0] = estructuraOrganizacional;
            parametrosTextoConsiderando[1] = responsableConservacion;
            parametrosTextoConsiderando[2] = senorIdentificacion;
            parametrosTextoConsiderando[3] = (t.getPredio() == null) ? "" :
                t.getPredio().getNumeroPredial();
            parametrosTextoConsiderando[4] = (t.getMunicipio() == null) ? "" :
                t.getMunicipio().getNombre().toUpperCase();
            parametrosTextoConsiderando[5] = radicar;
            parametrosTextoConsiderando[6] = tipoSolicitudValor;
            parametrosTextoConsiderando[7] = documentosJustificativos;

            parametrosTextoResuelve[0] = responsableConservacion;
            parametrosTextoResuelve[1] = 2;
            parametrosTextoResuelve[2] = 3;
            parametrosTextoResuelve[3] = 4;
            parametrosTextoResuelve[4] = 5;

        }
        if (parametrosTextoConsiderando != null) {
            modeloResolucionSeleccionado
                .setTextoConsiderando(new MessageFormat(
                    modeloResolucionSeleccionado
                        .getTextoConsiderando(), colombiaLocale)
                    .format(parametrosTextoConsiderando));
        }
        if (parametrosTextoResuelve != null) {
            modeloResolucionSeleccionado
                .setTextoResuelve(new MessageFormat(
                    modeloResolucionSeleccionado
                        .getTextoResuelve(), colombiaLocale)
                    .format(parametrosTextoResuelve));
        }
        return modeloResolucionSeleccionado;

    }

    private String formarDocumentosJustificativos(Tramite t) {

        String answer = "";
        PPredio predioSeleccionado;
        try {
            predioSeleccionado = this.conservacionService.obtenerPPredioCompletoByIdTramite(t.
                getId());

            List<PPersonaPredioPropiedad> pPersonaPredioPropiedad =
                new ArrayList<PPersonaPredioPropiedad>();

            boolean agregarDocumentoJustificativo;

            StringBuilder documentosJustificativos = new StringBuilder();

            if (predioSeleccionado != null &&
                predioSeleccionado.getPPersonaPredios() != null) {
                for (PPersonaPredio persPred : predioSeleccionado
                    .getPPersonaPredios()) {
                    for (PPersonaPredioPropiedad ppp : persPred
                        .getPPersonaPredioPropiedads()) {
                        if (ppp.getCancelaInscribe() != null &&
                            (ppp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.INSCRIBE
                                    .getCodigo()) || ppp
                                .getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.MODIFICA
                                        .getCodigo()))) {

                            agregarDocumentoJustificativo = true;
                            for (PPersonaPredioPropiedad pppTemp : pPersonaPredioPropiedad) {
                                if (pppTemp.getId() != ppp.getId() &&
                                    pppTemp.getNumeroTitulo() != null && pppTemp.getFechaTitulo() !=
                                    null &&
                                    pppTemp.getNumeroTitulo().equals(ppp.getNumeroTitulo()) &&
                                    pppTemp.getFechaTitulo().equals(ppp.getFechaTitulo())) {
                                    agregarDocumentoJustificativo = false;
                                }
                            }

                            if (agregarDocumentoJustificativo && ppp.getTipoTitulo() != null && ppp.
                                getNumeroTitulo() != null) {
                                documentosJustificativos.append(((ppp.getTipoTitulo() != null) ?
                                    ppp.getTipoTitulo().toUpperCase() : "") +
                                    " NO. " +
                                    ((ppp.getNumeroTitulo() != null) ? ppp.getNumeroTitulo() : "") +
                                    " DE FECHA " +
                                    ((ppp.getFechaTitulo() != null) ? formatoFechaTituloResolucion.
                                    format(ppp.getFechaTitulo()).toUpperCase() : "") +
                                    ", " +
                                    ConstantesResolucionesTramites.NOTARIA +
                                    " " +
                                    (ppp.getEntidadEmisora() == null ? "" : ppp
                                    .getEntidadEmisora().toUpperCase()) +
                                    " DE " +
                                    (ppp.getMunicipio() == null ? "" : ppp.getMunicipio()
                                    .getNombre().toUpperCase()) +
                                    ", " +
                                    (ppp.getDepartamento() == null ? "" : ppp
                                    .getDepartamento().getNombre().toUpperCase()) + ", " +
                                    ConstantesResolucionesTramites.MATRICULA_INMOBILIARIA +
                                    ", " +
                                    (predioSeleccionado.getNumeroRegistro() != null ?
                                    predioSeleccionado.getNumeroRegistro() : "") +
                                    ",<br>"
                                );
                                pPersonaPredioPropiedad.add(ppp);
                            }
                        }
                    }
                }
            }

            if (documentosJustificativos.length() > 5) {
                answer = documentosJustificativos.delete(documentosJustificativos.length() - 5,
                    documentosJustificativos.length()).toString();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return answer;
    }

    public List<InfInterrelacionadaRe> getTramites(FiltroDatosConsultaTramitesIper filtro) {
        return this.interrelacionadaReDao.getTramites(filtro);
    }
}
