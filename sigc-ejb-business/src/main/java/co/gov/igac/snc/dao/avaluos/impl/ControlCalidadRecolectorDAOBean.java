/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadRecolectorDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadRecolector;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author ariel.ortiz
 * @version 2.0
 */
@Stateless
public class ControlCalidadRecolectorDAOBean extends
    GenericDAOWithJPA<ControlCalidadRecolector, Long> implements
    IControlCalidadRecolectorDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadRecolectorDAOBean.class);

    @Override
    public List<ControlCalidadRecolector> buscarControlCalidadRecolectorPorIdControl(
        Long idControl) {

        LOGGER.debug(
            "entrando a ControlCalidadRecolectorDAOBean#buscarControlCalidadRecolectorPorIdControl ...");

        List<ControlCalidadRecolector> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "select ccr from ControlCalidadRecolector ccr" +
            " WHERE ccr.controlCalidad.id = :idControl";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idControl", idControl);

            queryAnswer = (List<ControlCalidadRecolector>) query
                .getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        LOGGER.debug(
            "saliendo de ControlCalidadRecolectorDAOBean#buscarControlCalidadRecolectorPorIdControl ...");

        return queryAnswer;

    }

}
