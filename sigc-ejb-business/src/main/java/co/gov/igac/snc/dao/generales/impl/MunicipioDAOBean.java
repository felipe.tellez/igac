package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.persistence.util.EMunicipioActualizacionEstado;
import java.util.ArrayList;

@Stateless
public class MunicipioDAOBean extends GenericDAOWithJPA<Municipio, String> implements IMunicipioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(MunicipioDAOBean.class);

//--------------------------------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    @Override
    public List<Municipio> findByDepartamento(String departamentoId) {
        Query query = this.entityManager.createQuery(
            "Select m from Municipio m where m.departamento.codigo = :departamentoId");
        query.setParameter("departamentoId", departamentoId);
        return query.getResultList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IMunicipioDAO#findByDepartamentoNQ(java.lang.String)
     * @modified pedro.garcia para que el query los retornara ordenados por nombre
     */
    @Implement
    @Override
    public List<Municipio> findByDepartamentoNQ(String departamentoId) {

        Query query;
        List<Municipio> answer = null;
        String queryString;

        queryString = "SELECT m FROM Municipio m WHERE m.departamento.codigo = :departamentoId " +
            "ORDER BY m.nombre";

        query = this.entityManager.createQuery(queryString, Municipio.class);
        query.setParameter("departamentoId", departamentoId);

        try {
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Excepción en búsqueda de municipios por departamento: " + ex.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IMunicipioDAO#getMunicipioByCodigo(String)
     */
    public Municipio getMunicipioByCodigo(String codigo) {
        Query q = entityManager.createNamedQuery("municipio.findMunicipioByCodigo");
        q.setParameter("codigo", codigo);
        Municipio m = (Municipio) q.getSingleResult();
        return m;
    }

    // ---------------------------------------------------------------------------------------------- //
    /**
     * @see IMunicipioDAO#buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(String)
     */
    @Override
    public List<Municipio> buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
        String departamentoCodigo) {

        String sqlQuery = "SELECT m FROM Municipio m, Jurisdiccion j" +
            " WHERE m.codigo = j.municipio.codigo AND m.departamento.codigo = :departamentoCodigo ";

        Query query = this.entityManager.createQuery(sqlQuery);
        query.setParameter("departamentoCodigo", departamentoCodigo);
        return query.getResultList();
    }

    /**
     * @see IMunicipioDAO#findByEntidadTerritorial(String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Municipio> findByEntidadTerritorial(String entidadTerritorialCode) {
        Query query;
        String queryString;
        List<Municipio> answer;

        queryString = "SELECT * FROM MUNICIPIO M" +
            " WHERE M.CODIGO IN ( " +
            " SELECT J.MUNICIPIO_CODIGO FROM JURISDICCION J" +
            " WHERE J.ESTRUCTURA_ORGANIZACIONAL_COD = :territorialId" +
            " )";

        query = this.entityManager.createNativeQuery(queryString, Municipio.class);
        query.setParameter("territorialId", entidadTerritorialCode);
        answer = (List<Municipio>) query.getResultList();

        return answer;

    }

    /**
     * @see IMunicipioDAO#obtenerDelegados()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Municipio> obtenerDelegados() {
        Query query;
        String queryString;
        List<Municipio> answer;

        queryString = "SELECT M.* FROM MUNICIPIO M " +
            "INNER JOIN JURISDICCION J ON J.MUNICIPIO_CODIGO = M.CODIGO " +
            "INNER JOIN ESTRUCTURA_ORGANIZACIONAL EO ON EO.CODIGO = J.ESTRUCTURA_ORGANIZACIONAL_COD " +
            "WHERE EO.TIPO = 'DELEGACION'";

        query = this.entityManager.createNativeQuery(queryString, Municipio.class);
        answer = (List<Municipio>) query.getResultList();

        return answer;

    }
    
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Municipio> obtenerHabilitados(){
        Query query;
        String queryString;
        List<Municipio> answer;

        queryString = "SELECT M.* FROM MUNICIPIO M " +
            "INNER JOIN JURISDICCION J ON J.MUNICIPIO_CODIGO = M.CODIGO " +
            "INNER JOIN ESTRUCTURA_ORGANIZACIONAL EO ON EO.CODIGO = J.ESTRUCTURA_ORGANIZACIONAL_COD " +
            "WHERE EO.TIPO = 'HABILITADA'";

        query = this.entityManager.createNativeQuery(queryString, Municipio.class);
        answer = (List<Municipio>) query.getResultList();

        return answer;
    }

    /**
     *
     * @author felipe.cadena
     * @see IMunicipioDAO#obtenerMunicipiosActualizacionPorDepto(java.lang.String)
     */
    @Override
    public List<Municipio> obtenerMunicipiosActualizacionPorDepto(String deptoCodigo) {

        List<Municipio> resultado = new ArrayList<Municipio>();

        Query query;
        String queryString;

        queryString = "SELECT DISTINCT M.* FROM MUNICIPIO M " +
            "INNER JOIN AX_MUNICIPIO MA ON MA.MUNICIPIO_CODIGO = M.CODIGO " +
            "WHERE  MA.DEPARTAMENTO_CODIGO = :deptoCodigo ";

        query = this.entityManager.createNativeQuery(queryString, Municipio.class);
        query.setParameter("deptoCodigo", deptoCodigo);
        resultado = (List<Municipio>) query.getResultList();

        return resultado;

    }

    /**
     * @see IMunicipioDAO#validaMunicipioDepartamento(String,String)
     * @dumar.penuela verifica la exitencia de un municipio en un departamento
     */
    @Override
    public Boolean validaMunicipioDepartamento(String departamentoId, String municipioId) {

        Query query;
        Boolean answer = false;
        String queryString;

        Municipio municipio = null;

        queryString =
            "SELECT m FROM Municipio m WHERE m.departamento.codigo = :departamentoId AND m.codigo= :municipioId";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("departamentoId", departamentoId);
        query.setParameter("municipioId", municipioId);

        try {
            municipio = (Municipio) query.getSingleResult();

            if (municipio != null) {
                answer = true;
            }

        } catch (Exception ex) {
            LOGGER.error(
                "Excepción en búsqueda de validación de la existencia de un municipio en un departamento: " +
                ex.getMessage(), ex);
        }
        return answer;
    }

    /**
     * @see IMunicipioDAO#buscarMunicipiosSNCNoDelegada()
     * @author felipe.cadena
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Municipio> buscarMunicipiosSNCNoDelegada() {
        Query query;
        String queryString;
        List<Municipio> answer = null;

        queryString = "select m.* from municipio m " +
                "inner join jurisdiccion j on m.codigo = j.municipio_codigo " +
                "inner join estructura_organizacional eo on j.estructura_organizacional_cod = eo.codigo " +
                "inner join municipio_complemento mc on mc.municipio_codigo = m.codigo " +
                "where  " +
                "tipo <> 'DELEGACION' " +
                "and mc.con_informacion_gdb = 'SI'";

        try {

            query = this.entityManager.createNativeQuery(queryString, Municipio.class);
            answer = (List<Municipio>) query.getResultList();
            return answer;


        }catch (Exception e) {
            LOGGER.error(
                    "RepUsuarioRolReporteDAOBean#buscarParametrizacionReporteUsuarioCompletaPorId:" +
                            "Error al consultar el RepUsuarioRolReporte del SNC");

        }

       return answer;

    }

}
