package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.ITmpBloqueoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.TmpBloqueoMasivo;

@Stateless
public class TmpBloqueoDAOBean extends GenericDAOWithJPA<TmpBloqueoMasivo, Long>
    implements ITmpBloqueoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TmpBloqueoDAOBean.class);

    @Override
    public BigDecimal generarSecuenciaTmpBloqueoMasivo() {

        Query query = entityManager.createNativeQuery(
            "SELECT BLOQUEO_MASIVO_ID_SEQ.NextVal FROM DUAL");
        BigDecimal secuencia = (BigDecimal) query.getResultList().get(0);

        return secuencia;

    }
}
