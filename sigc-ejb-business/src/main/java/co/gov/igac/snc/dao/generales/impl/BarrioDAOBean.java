package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IBarrioDAO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto {@link Barrio}.
 */
@Stateless
public class BarrioDAOBean extends GenericDAOWithJPA<Barrio, String> implements IBarrioDAO {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(BarrioDAOBean.class);

// --------------------------------------------------------------------------------------------------
    /**
     * @see IBarrioDAO#findByComuna(String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Barrio> findByComuna(String comunaId) {
        Query query = this.entityManager
            .createQuery("SELECT DISTINCT m FROM Barrio m WHERE m.comuna.codigo = :comunaId");
        query.setParameter("comunaId", comunaId);
        return query.getResultList();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IBarrioDAO#getBarrioByCodigo(String)
     */
    @Override
    public Barrio getBarrioByCodigo(String codigo) {
        Query q = entityManager.createNamedQuery("barrio.findBarrioByCodigo");
        q.setParameter("codigo", codigo);
        Barrio m = (Barrio) q.getSingleResult();
        return m;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IBarrioDAO#numeroBarriosMunicipio(String)
     * @author javier.barajas
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Barrio> numeroBarriosMunicipio(String codigoMunicipio) {
        Query query = this.entityManager
            .createQuery("SELECT DISTINCT m FROM Barrio m " +
                "WHERE m.codigo LIKE :codigoMunicipio");

        try {
            query.setParameter("codigoMunicipio", codigoMunicipio + "%");
            return query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "BarrioDAOBean#numeroBarriosMunicipio");
        }

    }

}
