/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface ITipificacionUnidadConstDAO extends IGenericJpaDAO<TipificacionUnidadConst, Long> {

    /**
     * Obtiene el registro de la tabla que corresponda a los puntos dados
     *
     * @author pedro.garcia
     * @param puntos
     * @return
     */
    public TipificacionUnidadConst findByPoints(int puntos);

}
