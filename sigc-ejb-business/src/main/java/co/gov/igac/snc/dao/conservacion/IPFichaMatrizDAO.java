package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import java.util.List;

@Local
public interface IPFichaMatrizDAO extends IGenericJpaDAO<PFichaMatriz, Long> {

    /**
     * Método que consulta una proyección de ficha matriz según el id del PPredio al cual está
     * asociada
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public PFichaMatriz findByPredioId(Long idPPredio);

    /**
     * Busca la ficha matriz de un predio relacionado al numero predial dado.
     *
     * @author felipe.cadena
     * @param numPredial
     * @return
     */
    public PFichaMatriz findByNumeroPredial(String numPredial);

    /**
     * Obtiene las fichas proyectadas ingresadas al sistema por medio de una opción de menú.
     *
     * @author felipe.cadena
     * @param usuario
     * @return
     */
    public List<PFichaMatriz> obtenerFichasEnProcesoDeCarga(String usuario);

    /**
     * Metodo que retorna todas las fichas matrices asociadas a un tramite.
     *
     * @author felipe.cadena
     *
     *
     * @param tramiteId
     * @return
     */
    public List<PFichaMatriz> obtenerFichasAsociadasATramite(Long tramiteId);

    /**
     * Metodo que retorna las ficha matriz asociadas a un tramite de englobe virtual.
     *
     * @author leidy.gonzalez
     *
     * @param numeroPredial
     * @return
     */
    public PFichaMatriz obtenerFichaMatrizTramiteEV(String numeroPredial);

}
