package co.gov.igac.snc.dao.tramite;

import java.util.List;
import javax.ejb.Local;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;

/**
 *
 * @author fredy.wilches
 *
 */
@Local
public interface ISolicitanteSolicitudDAO extends IGenericJpaDAO<SolicitanteSolicitud, Long> {

    public List<SolicitanteSolicitud> getSolicitantesBySolicitud(Long solicitudId);

    /**
     * obtiene las filas donde coincida el id de la solicitud. El query hace fetch del municipio,
     * departamento y país.
     *
     * @author pedro.garcia
     * @param solicitudId
     * @return
     */
    public List<SolicitanteSolicitud> getSolicitantesBySolicitud2(Long solicitudId);

    /**
     * Retorna una lista de solicitanteSolicitud a partir del id de la solicitud
     *
     * @param id
     * @return
     * @author unknown
     * @documented fabio.navarrete
     */
    public List<SolicitanteSolicitud> findBySolicitudId(Long solicitudId);

    /**
     *
     * Metodo que retorna n objetos de SolicitanteSolicitud para utilizarlos en pruebas
     *
     * @author fredy.wilches
     * @return
     */
    public List<SolicitanteSolicitud> getSolicitanteSolicitudAleatorio();

    /**
     * Método que retorna los solicitantesSolicitud asociados a una solicitud filtrados por relación
     *
     * @author christian.rodriguez
     * @return lista de solicitanteSolicitud
     */
    public List<SolicitanteSolicitud> getSolicitanteSolicitudByIdSolicitudRelacion(
        Long idSolicitud, String solicitanteSolicitudEstado);
}
