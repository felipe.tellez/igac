package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IConvenioEntidadDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ConvenioEntidad;

/**
 * Implementación de los servicios de persistencia del objeto ConvenioEntidad.
 *
 * @author david.cifuentes
 */
@Stateless
public class ConvenioEntidadDAOBean extends
    GenericDAOWithJPA<ConvenioEntidad, Long> implements IConvenioEntidadDAO {

    /**
     * Servicio de bitácora.
     */
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDAOBean.class);

}
