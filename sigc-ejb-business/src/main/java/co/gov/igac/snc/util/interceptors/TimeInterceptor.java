package co.gov.igac.snc.util.interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ejemplo de interceptor que mide el tiempo de ejecución de un método
 *
 * @author juan.mendez
 *
 */
public class TimeInterceptor {

    private static final Logger log = LoggerFactory
        .getLogger(TimeInterceptor.class);

    @AroundInvoke
    public Object logCall(InvocationContext ctx) throws Exception {
        String target = ctx.getTarget().getClass().getName();
        //String beanClassName = ctx.getClass().getName();
        String businessMethodName = ctx.getMethod().getName();
        String targetMethod = target + "." + businessMethodName;
        long startTime = System.currentTimeMillis();
        //log.debug("Invoking " + targetMethod);
        //log.debug("target " + target);
        try {
            return ctx.proceed();
        } finally {
            //log.debug("Exiting " + target);
            long totalTime = System.currentTimeMillis() - startTime;
            log.debug("End Business Method:" + targetMethod + " Time:" + totalTime + "ms");
        }
        // return ctx.proceed();
    }
}
