/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.formacion;

import java.util.Date;
import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.TablaTerreno;
import javax.ejb.Local;

/**
 * Interface para el DAO de TablaTerrenoDAO
 *
 * @author fredy.wilches
 */
@Local
public interface ITablaTerrenoDAO extends IGenericJpaDAO<TablaTerreno, Long> {

    public List<TablaTerreno> findByZonaVigencia(String zona, Date vigencia);
}
