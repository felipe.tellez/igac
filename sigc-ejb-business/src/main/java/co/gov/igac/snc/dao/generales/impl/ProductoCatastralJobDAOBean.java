package co.gov.igac.snc.dao.generales.impl;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

@Stateless
public class ProductoCatastralJobDAOBean extends GenericDAOWithJPA<ProductoCatastralJob, Long>
    implements
    IProductoCatastralJobDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductoCatastralJobDAOBean.class);

    @EJB
    private ITramiteDAO tramiteDAOService;

    /**
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobGenerarDatosEdicion(java.lang.String,
     * java.lang.String, java.lang.String, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public ProductoCatastralJob crearJobGenerarDatosEdicion(Tramite tramite,
        String identificadoresPredios,
        String tipoMutacion, UsuarioDTO usuario) {
        LOGGER.debug("crearJobGenerarDatosEdicion");
        String paramsAsXML = paramsJobExportarDatosAsXML(tramite.getId().toString(),
            identificadoresPredios,
            tipoMutacion, usuario, null, null, null);
        ProductoCatastralJob producto = crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION,
            ProductoCatastralJob.AGS_ESPERANDO, paramsAsXML, tramite, null);
        LOGGER.debug(producto.toString());
        return producto;
    }

    /**
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobGenerarDatosEdicion(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public ProductoCatastralJob crearJobExportarDatosEdicion(Tramite tramite,
        String identificadoresPredios,
        String tipoMutacion, String formato, UsuarioDTO usuario) {
        LOGGER.debug("crearJobGenerarDatosEdicion");
        String paramsAsXML = paramsJobExportarDatosAsXML(tramite.getId().toString(),
            identificadoresPredios,
            tipoMutacion, usuario, formato, null, null);
        ProductoCatastralJob producto = crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS,
            ProductoCatastralJob.AGS_ESPERANDO, paramsAsXML, tramite, null);
        LOGGER.debug(producto.toString());
        return producto;
    }

    /**
     * @param numeroTramite identificador del tramite
     * @param identificadoresPredios predios relacionado por los JOBS
     * @param tipoMutacion tipo de mutacion es un campo del tramite
     * @param usuario usuario relacionado a la actividad que lo invoca
     * @param formato requerido de salida del trabajo asincronico
     * @param URLReplica URL direccion de la replica geografica
     * @return xml con la estructura de parametros requeridos por los distintos de JOB
     *
     * @return cadena que representa el XML generado
     *
     * @modified andres.eslav::01/Jul/2015::adicion parametro url::refs #13126 #13127
     */
    /*
     * @modified andres.eslav::01/Jul/2015::adicion parametro url::refs #13126 #13127 @modified
     * andres.eslava::23/Sept/2015::Se agrega soporte a radicaciones especiales
     */
    private String paramsJobExportarDatosAsXML(String numeroTramite, String identificadoresPredios,
        String tipoMutacion, UsuarioDTO usuario, String formato, String URLReplica,
        String radicacionEspecial) {
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("numeroTramite");
        parameter.addElement("value").addText(numeroTramite);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("identificadoresPredios");
        parameter.addElement("value").addText(identificadoresPredios);

        if (tipoMutacion != null &&
            !StringUtils.isBlank(tipoMutacion)) {
            parameter = parameters.addElement("parameter");
            parameter.addElement("name").addText("tipoMutacion");
            parameter.addElement("value").addText(tipoMutacion);
        }

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("usuario");
        parameter.addElement("value").addText(usuario.getLogin());

        if (formato != null) {
            parameter = parameters.addElement("parameter");
            parameter.addElement("name").addText("formato");
            parameter.addElement("value").addText(formato);
        }

        if (URLReplica != null) {
            parameter = parameters.addElement("parameter");
            parameter.addElement("name").addText("URLReplica");
            parameter.addElement("value").addText(URLReplica);
        }

        if (radicacionEspecial != null) {
            parameter = parameters.addElement("parameter");
            parameter.addElement("name").addText("radicacionEspecial");
            parameter.addElement("value").addText(radicacionEspecial);
        }

        return root.asXML();
    }
    
    /**
     * 
     * @param ubicacionArchivo
     * @param validacionZonas
     * @param validacionPredios
     * @param usuario
     * @return 
     * 
     * @hector.arias
     */
    private String paramsJobCargueActualizaciónAsXML(String enviroment, String ubicacionArchivo,
        String validacionZonas, String validacionPredios, UsuarioDTO usuario, String departamentoSeleccionado, String municipioSeleccionado) {
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("numeroTramite");

        parameter = parameters.addElement("parameter");
        parameter.addElement("enviroment").addText(enviroment);
        parameter.addElement("ubicacionArchivoGDB").addText(ubicacionArchivo);
        parameter.addElement("validarZonas").addText(validacionZonas);
        parameter.addElement("validarPredios").addText(validacionPredios);
        parameter.addElement("value").addText(usuario.getLogin());
        parameter.addElement("codigoDepartamento").addText(departamentoSeleccionado);
        parameter.addElement("codigoMunicipio").addText(municipioSeleccionado);

        return root.asXML();
    }

    /**
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobFinalizarEdicionTramite(java.lang.String,
     * java.lang.String, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public ProductoCatastralJob crearJobFinalizarEdicionTramite(Tramite tramite,
        String idArchivoSistemaDocumental, UsuarioDTO usuario) {
        LOGGER.debug("crearJobGenerarDatosEdicion");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("numeroTramite");
        parameter.addElement("value").addText(tramite.getId().toString());

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("idArchivoSistemaDocumental");
        parameter.addElement("value").addText(idArchivoSistemaDocumental);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("usuario");
        parameter.addElement("value").addText(usuario.getLogin());

        if (tramite.getActividadActualTramite() != null &&
            !StringUtils.isBlank(tramite.getActividadActualTramite().getNombre())) {

            String actClean = tramite.getActividadActualTramite().getNombre();
            actClean = StringUtils.stripAccents(actClean);
            parameter = parameters.addElement("parameter");
            parameter.addElement("name").addText("actividadProceso");
            parameter.addElement("value").addText(actClean);
        }

        if (tramite.getActividadActualTramite() != null &&
            !StringUtils.isBlank(tramite.getActividadActualTramite().getId())) {
            parameter = parameters.addElement("parameter");
            parameter.addElement("name").addText("taskIdProceso");
            parameter.addElement("value").addText(tramite.getActividadActualTramite().getId());

        }

        return crearJobGeoprocesamiento(usuario, ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, null);
    }

    /*
     * @Override public ProductoCatastralJob crearJobGenerarProductosDatos(String
     * identificadorProducto, String formato, String codigosEntidadesGeograficas, UsuarioDTO
     * usuario) { LOGGER.debug("crearJobGenerarDatosEdicion"); Document parametros =
     * DocumentHelper.createDocument(); Element root = parametros.addElement("SigJob"); Element
     * parameters = root.addElement("parameters");
     *
     * Element parameter = parameters.addElement("parameter");
     * parameter.addElement("name").addText("identificadorProducto");
     * parameter.addElement("value").addText(identificadorProducto);
     *
     * parameter = parameters.addElement("parameter");
     * parameter.addElement("name").addText("formato");
     * parameter.addElement("value").addText(formato);
     *
     * parameter = parameters.addElement("parameter");
     * parameter.addElement("name").addText("codigosEntidadesGeograficas");
     * parameter.addElement("value").addText(codigosEntidadesGeograficas);
     *
     * parameter = parameters.addElement("parameter");
     * parameter.addElement("name").addText("usuario");
     * parameter.addElement("value").addText(usuario.getLogin());
     *
     * return crearJobGeoprocesamiento(usuario,
     * ProductoCatastralJob.SIG_JOB_GENERAR_PRODUCTOS_DATOS, ProductoCatastralJob.AGS_ESPERANDO,
     * root.asXML(), null); }
     */
 /*
     * //--------------------------------------------------------------------------------------------------
     * @Override public ProductoCatastralJob obtenerSiguienteTrabajoParaAplicarCambios() {
     * LOGGER.debug("obtenerSiguienteTrabajoParaAplicarCambios"); Query query =
     * this.entityManager.createQuery("select count(id) from ProductoCatastralJob " + "where
     * tipoProducto = :tipoProducto and estado = :estado "); query.setParameter("tipoProducto",
     * ProductoCatastralJob.TIPO_JOB_APLICAR_CAMBIOS); query.setParameter("estado",
     * ProductoCatastralJob.ESTADO_EN_EJECUCION); Long conteo = (Long) query.getSingleResult(); if
     * (conteo > 0) { throw SncBusinessServiceExceptions.EXCEPCION_100017.getExcepcion(LOGGER, null,
     * "Ya se encuentra ejecutando un proceso de aplicación de cambios"); }
     *
     * ProductoCatastralJob siguienteAProcesar = null;
     *
     * query = entityManager.createQuery("select p from ProductoCatastralJob p " + "where
     * tipoProducto = :tipoProducto and estado = :estado order by id asc");
     * query.setParameter("tipoProducto", ProductoCatastralJob.TIPO_JOB_APLICAR_CAMBIOS);
     * query.setParameter("estado", ProductoCatastralJob.ESTADO_ESPERANDO); query.setMaxResults(1);
     * @SuppressWarnings("unchecked") List<ProductoCatastralJob> resultados = query.getResultList();
     * if (resultados.isEmpty()) { throw
     * SncBusinessServiceExceptions.EXCEPCION_100017.getExcepcion(LOGGER, null, "No existen trabajos
     * de aplicación de cambios para ejecutar"); } siguienteAProcesar = resultados.get(0);
     *
     * // Marca la ejecución del job en la Base de datos //Nota: LA actualización del registro se
     * hace en el servidor GIS
     * //siguienteAProcesar.setEstado(ProductoCatastralJob.ESTADO_EN_EJECUCION);
     * //this.update(siguienteAProcesar);
     *
     * return siguienteAProcesar; }
     */
    /**
     *
     * @see co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#obtenerJobsPendientesEnSNC()
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsPendientesEnSNC() {
        LOGGER.debug("obtenerJobsPendientesEnSNC");
        Query query = entityManager.createQuery("select p from ProductoCatastralJob p " +
            "where estado = :estado order by id asc");
        query.setParameter("estado", ProductoCatastralJob.AGS_TERMINADO);
        List<ProductoCatastralJob> resultados = query.getResultList();
        return resultados;
    }

    /**
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#obtenerJobsPendientesEnSNC(java.lang.Long)
     */
    /*
     * @modified pedro.garcia 08-11-2013 manejo de excepciones
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsPendientesEnSNC(Long cantidad) {
        LOGGER.debug("obtenerJobsPendientesEnSNC");

        List<ProductoCatastralJob> resultados = null;
        String queryString;

        queryString = "SELECT p FROM ProductoCatastralJob p " +
            "WHERE estado = :estado ORDER BY id ASC";
        Query query = this.entityManager.createQuery(queryString);

        try {
            query.setParameter("estado", ProductoCatastralJob.AGS_TERMINADO);
            if (cantidad > 0) {
                query.setMaxResults(cantidad.intValue());
            }
            query.setFirstResult(0);
            resultados = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ProductoCatastralJobDAOBean#obtenerJobsPendientesEnSNC");
        } finally {
            return resultados;
        }
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<ProductoCatastralJob> obtenerJobsSIGConErrores() {
        LOGGER.debug("obtenerJobsSIGConErrores");
        Query query = entityManager.createQuery("select p from ProductoCatastralJob p " +
            "where estado = :estado order by id asc");
        query.setParameter("estado", ProductoCatastralJob.AGS_ERROR);
        @SuppressWarnings("unchecked")
        List<ProductoCatastralJob> resultados = query.getResultList();
        return resultados;
    }

    @Override
    public List<ProductoCatastralJob> obtenerJobsSNCConErrores() {
        LOGGER.debug("obtenerJobsSNCConErrores");
        Query query = entityManager.createQuery("select p from ProductoCatastralJob p " +
            "where estado = :estado order by id asc");
        query.setParameter("estado", ProductoCatastralJob.SNC_ERROR);
        @SuppressWarnings("unchecked")
        List<ProductoCatastralJob> resultados = query.getResultList();
        return resultados;
    }

    /**
     * @see IProductoCatastralJobDAO#obtenerJobsGeograficosEsperando(int, int)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosEsperando(int horas, int cantidadJobs,
        int numeroReintentos) {
        LOGGER.debug("obtenerJobsGeograficosEsperando");
		String sql =  "SELECT * "
                            + "from ("
                            + "select p.*"
                            + "from producto_catastral_job  p "
                            + "where estado = 'AGS_ESPERANDO' "
                            + "AND fecha_log < (sysdate - interval '"+horas+"' hour) "
                            + "And (p.numero_envio is null or P.Numero_Envio < '"+numeroReintentos+"') "
                            + "AND (P.TIPO_PRODUCTO ='"+ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS+"' OR "
                            + "P.TIPO_PRODUCTO='"+ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION+"' OR "
                            + "P.TIPO_PRODUCTO ='"+ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION+"' OR " 
                            + "P.TIPO_PRODUCTO ='"+ProductoCatastralJob.SIG_JOB_IMAGEN_PREDIAL+"') "                            
                            + "ORDER BY fecha_log asc"
                            + ")"
                            + " where rownum < '"+cantidadJobs+"'";
        Query query = entityManager.createNativeQuery(sql, ProductoCatastralJob.class);
        List<ProductoCatastralJob> slqResults = query.getResultList();
        return slqResults;
    }

    /**
     * @see IProductoCatastralJobDAO#obtenerJobsGeograficosError(int, int)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosError(int horas, int cantidadJobs,
        int numeroReintentos) {
        LOGGER.debug("obtenerJobsGeograficosError");
        String sql = "SELECT * " +
            "from (" +
            "select p.*" +
            "from producto_catastral_job  p " +
            "where estado = 'AGS_ERROR' " +
            "AND fecha_log < (sysdate - interval '" + horas + "' hour) " +
            "And (p.numero_envio is null or P.Numero_Envio < '" + numeroReintentos + "') " +
            "AND (p.TIPO_PRODUCTO ='" + ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS + "' OR " +
            "P.TIPO_PRODUCTO='" + ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION + "' OR " +
            "P.TIPO_PRODUCTO ='" + ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION + "')" +
            "ORDER BY fecha_log asc" +
            ")" +
            " where rownum < '" + cantidadJobs + "'";
        Query query = entityManager.createNativeQuery(sql, ProductoCatastralJob.class);
        List<ProductoCatastralJob> slqResults = query.getResultList();
        return slqResults;
    }
    
    /**
    * @see IProductoCatastralJobDAO#obtenerJobsGeograficosImagenPredialTerminado(int, int)      
    */
	@Override
	public List<ProductoCatastralJob>  obtenerJobsGeograficosImagenPredialTerminado(int horas,int cantidadJobs, int numeroReintentos) {
		LOGGER.debug("obtenerJobsGeograficosImagenPredialTerminado");
                String sql =  "SELECT * "
                    + "from ("
                    + "select p.*"
                    + "from producto_catastral_job  p "
                    + "where estado = 'AGS_TERMINADO' "
                    + " AND fecha_log < (sysdate - interval '"+horas+"' hour) "
                    + " AND (p.numero_envio is null or P.Numero_Envio < '"+numeroReintentos+"') "
                    + " AND (p.TIPO_PRODUCTO ='"+ProductoCatastralJob.SIG_JOB_IMAGEN_PREDIAL+"')"                            
                    + " ORDER BY fecha_log asc"
                    + ")"
                    + " where rownum < '"+cantidadJobs+"'";
		Query query = entityManager.createNativeQuery(sql,ProductoCatastralJob.class);
		List<ProductoCatastralJob> slqResults = query.getResultList();             
		return slqResults;
	}
    
    @Override
    public void reprogramarEjecucionJob(ProductoCatastralJob job) {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @modified felipe.cadena :: 29/05/2014 :: Se agrega el parametro tramite
     *
     * @param usuario
     * @param tipo
     * @param estado
     * @param paramsAsXml
     * @param tramite
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ProductoCatastralJob crearJobGeoprocesamiento(UsuarioDTO usuario, String tipo,
        String estado, String paramsAsXml, Tramite tramite,
        ProductoCatastralDetalle productoCatastralDetalle) {
        LOGGER.debug("crearJobGeoprocesamiento");
        ProductoCatastralJob job = new ProductoCatastralJob();
        job.setEstado(estado);
        job.setTipoProducto(tipo);
        job.setCodigo(paramsAsXml);
        job.setUsuarioLog(usuario.getLogin());
        job.setResultado("");
        if (tramite != null) {
            job.setTramiteId(tramite.getId());
        }
        if (productoCatastralDetalle != null) {
            job.setProductoCatastralDetalleId(productoCatastralDetalle.getId());
        }
        job.setNumeroEnvio(0);
        job = this.entityManager.merge(job);
        this.entityManager.flush();
        LOGGER.debug(job.toString());
        return job;
    }

    /**
     * Actualiza el estado de un job ( Maneja su propia transacción )
     *
     * @param job
     * @param nuevoEstado
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @Override
    public void actualizarEstado(ProductoCatastralJob job, String nuevoEstado) {
        LOGGER.debug("actualizarEstado job: " + job + "  ,\n nuevoEstado:" + nuevoEstado);
        LOGGER.debug("Estado Actual:" + job.getEstado());
        //ProductoCatastralJob persistentJob = this.findById(job.getId());
        job.setEstado(nuevoEstado);
        job = this.entityManager.merge(job);
        LOGGER.debug("Nuevo Estado:" + job.getEstado());
    }

    /**
     * Actualiza el resultado de un job ( Maneja su propia transacción )
     *
     * @param job
     * @param nuevoEstado
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @Override
    public void actualizarResultado(ProductoCatastralJob job, String nuevoResultado) {
        LOGGER.debug("actualizarResultado: job: " + job + " ,\n nuevoResultado:" + nuevoResultado);
        LOGGER.debug("Resultado Actual:" + job.getResultado());

        job.setResultado(nuevoResultado);
        job = this.entityManager.merge(job);

        LOGGER.debug("Nuevo resultado : " + job.getResultado());
    }

    /**
     * Actualiza atributos del objeto Nota: Solo funcionó con la anotación
     * TransactionAttributeType.REQUIRED
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @Override
    public ProductoCatastralJob actualizar(ProductoCatastralJob job, String nuevoEstado,
        String nuevoResultado) {
        LOGGER.debug("actualizarResultado: job: " + job);
        LOGGER.debug("Estado Actual:" + job.getEstado());
        LOGGER.debug("Resultado Actual:" + job.getResultado());
        LOGGER.debug("Numero Envio:" + job.getNumeroEnvio());

        //job = this.findById(job.getId());
        if (nuevoEstado != null) {
            job.setEstado(nuevoEstado);
        }
        if (nuevoResultado != null) {
            job.setResultado(nuevoResultado);
        }

        job = this.entityManager.merge(job);
        LOGGER.debug("Nuevo Estado:" + job.getEstado());
        LOGGER.debug("Nuevo resultado : " + job.getResultado());

        return job;

    }

    /**
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobEliminarVersionSDE(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public ProductoCatastralJob crearJobEliminarVersionSDE(String nombreVersion, UsuarioDTO usuario,
        Tramite tramite) {
        LOGGER.debug("crearJobEliminarVersionSDE");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("nombreVersion");
        parameter.addElement("value").addText(nombreVersion);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("usuario");
        parameter.addElement("value").addText(usuario.getLogin());

        return crearJobGeoprocesamiento(usuario, ProductoCatastralJob.SIG_JOB_ELIMINAR_VERSION_SDE,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, null);
    }

    /**
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobExportarImagen(java.lang.String,
     * java.lang.String, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public ProductoCatastralJob crearJobExportarImagen(String codigo, String tipoImagen,
        UsuarioDTO usuario) {
        LOGGER.debug("crearJobExportarImagen");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("codigo");
        parameter.addElement("value").addText(codigo);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("tipoImagen");
        parameter.addElement("value").addText(tipoImagen);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("usuario");
        parameter.addElement("value").addText(usuario.getLogin());

        return crearJobGeoprocesamiento(usuario, ProductoCatastralJob.SIG_JOB_EXPORTAR_IMAGEN,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), null, null);
    }
    
    /**
     * 
     * @param tipoProducto
     * @param usuarioLog
     * @return 
     */
    public ProductoCatastralJob obtenerJobsCargueActualizacion(String tipoProducto, String usuarioLog){
        
        try {
            String qString = "select max(pcj) FROM ProductoCatastralJob pcj" +
                " WHERE pcj.tipoProducto = :tipoProducto AND pcj.usuarioLog = :usuarioLog";

            Query query = this.entityManager.createQuery(qString);
            query.setParameter("tipoProducto", tipoProducto);
            query.setParameter("usuarioLog", usuarioLog);
            
            ProductoCatastralJob pcj = (ProductoCatastralJob) query.getSingleResult();
            
            return pcj;
            
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "ProductoCatastralJob#obtenerJobsCargueActualizacion");
        }
    }

    /**
     * @author felipe.cadena
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#obtenerJobsGeograficosPorTramiteTipoEstado(java.lang.Long,
     * java.lang.String, java.lang.String)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosPorTramiteTipoEstado(Long tramiteId,
        String tipoJob, String estadoJob) {

        List<ProductoCatastralJob> resultado = new ArrayList<ProductoCatastralJob>();
        LOGGER.debug("obtenerJobsGeograficosPorTramiteTipoEstado");

        try {
            String qString = "SELECT pcj FROM ProductoCatastralJob pcj" +
                " WHERE pcj.tramiteId = :tramiteId ";
            if (tipoJob != null) {
                qString = qString + "AND pcj.tipoProducto = :tipoProducto ";
            }
            if (estadoJob != null) {
                qString = qString + "AND pcj.estado = :estado ";
            }

            Query query = this.entityManager.createQuery(qString);
            query.setParameter("tramiteId", tramiteId);
            if (tipoJob != null) {
                query.setParameter("tipoProducto", tipoJob);
            }
            if (estadoJob != null) {
                query.setParameter("estado", estadoJob);
            }
            resultado = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "ProductoCatastralJob#obtenerJobsGeograficosPorTramiteTipoEstado");
        }
        return resultado;

    }

    /**
     * @author felipe.cadena
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#obtenerPorEstadoTramiteIdTipo(java.util.List,
     * java.lang.Long, java.lang.String)
     */
    @Override
    public List<ProductoCatastralJob> obtenerPorEstadoTramiteIdTipo(List<String> estados,
        Long tramiteId, String tipoJob) {

        List<ProductoCatastralJob> resultado = new ArrayList<ProductoCatastralJob>();
        LOGGER.debug("obtenerPorEstadoTramiteIdTipo");

        try {
            String qString = "SELECT pcj FROM ProductoCatastralJob pcj" +
                " WHERE pcj.tramiteId = :tramiteId ";

            if (estados != null) {
                qString = qString + "AND pcj.estado in :estados ";
            }
            if (tipoJob != null) {
                qString = qString + "AND pcj.tipoProducto = :tipoProducto ";
            }

            qString = qString + " ORDER BY pcj.id";

            Query query = this.entityManager.createQuery(qString);
            query.setParameter("tramiteId", tramiteId);
            if (estados != null) {
                query.setParameter("estados", estados);
            }
            if (tipoJob != null) {
                query.setParameter("tipoProducto", tipoJob);
            }
            resultado = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "ProductoCatastralJob#obtenerPorEstadoTramiteIdTipo");
        }
        return resultado;
    }

    /**
     * @author felipe.cadena
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#obtenerUltimoJobPorTramite(java.lang.Long,
     * java.lang.String)
     */
    /*
     * @modified by leidy.gonzalez:: 15/10/2015 :: 14043
     */
    @Override
    public ProductoCatastralJob obtenerUltimoJobPorTramite(Long tramiteId, String tipo) {

        ProductoCatastralJob catastralJob = null;

        LOGGER.debug("Entra en ProductoCatastralJobDAOBean#obtenerUltimoJobPorTramite");

        StringBuilder query = new StringBuilder();
        try {

            String qString = "SELECT MAX(v.fechaLog) FROM ProductoCatastralJob v " +
                " WHERE v.tramiteId = :tramiteId ";
            if (tipo != null) {
                qString = qString + "AND v.tipoProducto = :tipoProducto ";
            }

            Query qFecha = entityManager.createQuery(qString.toString());

            qFecha.setParameter("tramiteId", tramiteId);
            if (tipo != null) {
                qFecha.setParameter("tipoProducto", tipo);
            }

            Date maxFecha = (Date) qFecha.getSingleResult();

            query.append("SELECT v " +
                "FROM ProductoCatastralJob v " +
                "WHERE v.tramiteId = :tramiteId " +
                " AND v.fechaLog = :fecha");

            Query q = entityManager.createQuery(query.toString());
            q.setParameter("tramiteId", tramiteId);
            q.setParameter("fecha", maxFecha);

            catastralJob = (ProductoCatastralJob) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return catastralJob;

    }

    /**
     * @see IProductoCatastralJobDAO#obtenerJobsEsperandoUnoPorManzana(int)
     */
    public List<ProductoCatastralJob> obtenerJobsEsperandoUnoPorManzana(long cantidad) throws
        Exception {
        List<ProductoCatastralJob> jobEsperando = new LinkedList<ProductoCatastralJob>();
        String sql = "select * from(" +
            "select identificador_job, codigo_manzana, estado from (" +
            "      select identificador_job,codigo_manzana,estado, row_number() over (partition by codigo_manzana order by identificador_job asc) rn" +
            "      from (" +
            "            with manzanas_pendientes as (" +
            "              Select  distinct(substr(pp.numero_predial,0,17)) as codigo_manzana" +
            "              From  Producto_Catastral_Job Pcj    inner    Join P_Predio Pp           On Pp.tramite_id =Pcj.Tramite_Id" +

            "              where Pcj.Tipo_Producto='SIG_JOB_FINALIZAR_EDICION' and  pcj.estado IN ('AGS_ESPERANDO', 'AGS_EN_EJECUCION','AGS_ERROR')" +
            "            )" +
            "            select distinct pcj.id as identificador_job, m.codigo_manzana, pcj.estado" +
            "            from  Producto_Catastral_Job Pcj " +
            "                    inner Join P_Predio Pp  On Pp.tramite_id =Pcj.Tramite_Id" +
            "                    inner join manzanas_pendientes m on substr(pp.numero_predial,0,17) = m.codigo_manzana" +

            "            where   Pcj.Tipo_Producto='SIG_JOB_FINALIZAR_EDICION'  and   pcj.estado  IN ('AGS_ESPERANDO', 'AGS_EN_EJECUCION','AGS_ERROR')" +
            "      )  job_pendientes" +
            ") Jobs_Ordenados" +
            " Where Rn = 1" +
            ")Where Rownum < " + cantidad +
            " and estado in ('AGS_ESPERANDO','AGS_ERROR')";

        Query query = entityManager.createNativeQuery(sql);

        try {
            List<Object> sqlResults = query.getResultList();
            for (Object row : sqlResults) {
                Object[] registro = (Object[]) row;
                //Verifica el estado del Job
                String jobEstado = (String) registro[2];
                if (jobEstado.equals(ProductoCatastralJob.AGS_ESPERANDO) || jobEstado.equals(
                    ProductoCatastralJob.AGS_ERROR)) {
                    jobEsperando.add(this.findById(Long.valueOf(String.valueOf(registro[0]))));
                }
            }
        } catch (Exception e) {
            throw new Exception("Error obteniendo los jobs de aplicar cambios geograficos");
        }

        return jobEsperando;
    }

    /**
     * @see IProductoCatastralJobDAO#crearJobValidacionInconsistencias(java.lang.String,
     * co.gov.igac.snc.persistence.entity.tramite.Tramite, co.gov.igac.generales.dto.UsuarioDTO,
     * java.lang.String)
     */
    /*
     * @modified andres.eslava::se agrega soporte a radicaciones especiales
     */
    @Override
    public ProductoCatastralJob crearJobValidacionInconsistencias(String identificadoresPredios,
        Tramite tramite, UsuarioDTO usuario, String tipoMutacion, String radicacionEspecial) {
        LOGGER.debug("ProductoCatastralJobDAOBean.crearJobValidacionInconsistencias ... INICIA");
        String paramsAsXML = paramsJobExportarDatosAsXML(tramite.getId().toString(),
            identificadoresPredios, tipoMutacion, usuario, null, null, radicacionEspecial);
        ProductoCatastralJob producto = crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_VALIDAR_INCONSISTENCIAS,
            ProductoCatastralJob.AGS_ESPERANDO, paramsAsXML, tramite, null);
        LOGGER.debug("ID Producto Catastral Job Generado" + producto.toString());
        LOGGER.debug("ProductoCatastralJobDAOBean.crearJobValidacionInconsistencias ... FIN");
        return producto;
    }
    
    /**
     * @see IProductoCatastralJobDAO#crearJobValidacionInconsistenciasActualizacion(java.lang.String,
     * java.lang.String, java.lang.String, co.gov.igac.generales.dto.UsuarioDTO)
     */
    /*
     * @modified hector.arias
     */
    @Override
    public ProductoCatastralJob crearJobValidacionInconsistenciasActualizacion(String enviroment, String ubicacionArchivo,
        String validacionZonas, String validacionPredios, UsuarioDTO usuario, String departamentoSeleccionado, String municipioSeleccionado) {
        LOGGER.debug("ProductoCatastralJobDAOBean.crearJobValidacionInconsistenciasActualizacion ... INICIA");
        String paramsAsXML = paramsJobCargueActualizaciónAsXML(enviroment, ubicacionArchivo,
            validacionZonas, validacionPredios, usuario, departamentoSeleccionado, municipioSeleccionado);
        ProductoCatastralJob producto = crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_CARGUE_ACTUALIZACION,
            ProductoCatastralJob.AGS_ESPERANDO, paramsAsXML, null, null);
        LOGGER.debug("ID Producto Catastral Job Generado" + producto.toString());
        LOGGER.debug("ProductoCatastralJobDAOBean.crearJobValidacionInconsistenciasActualizacion ... FIN");
        return producto;
    }

    /**
     * @see IProductoCatastralJobDAO#crearJobValidacionInconsistencias(java.lang.String,
     * co.gov.igac.snc.persistence.entity.tramite.Tramite, co.gov.igac.generales.dto.UsuarioDTO,
     * java.lang.String)
     */
    @Override
    public ProductoCatastralJob crearJobObtenerZonasHomogeneas(String identificadoresPredios,
        Tramite tramite, UsuarioDTO usuario, String tipoMutacion, String URLReplica) {
        LOGGER.debug("ProductoCatastralJobDAOBean#crearJobObtenerZonasHomogeneas ... INICIA");
        String paramsAsXML = paramsJobExportarDatosAsXML(tramite.getId().toString(),
            identificadoresPredios, tipoMutacion, usuario, null, URLReplica, null);
        ProductoCatastralJob producto = crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS,
            ProductoCatastralJob.AGS_ESPERANDO, paramsAsXML, tramite, null);
        LOGGER.debug("ID Producto Catastral Job Generado" + producto.toString());
        LOGGER.debug("ProductoCatastralJobDAOBean#crearJobObtenerZonasHomogeneas ... FIN");
        return producto;
    }

    /**
     * @see IProductoCatastralJobDAO#obtenerXMLResultadoObtenerZonasHomegeneas(java.lang.Long)
     */
    @Override
    public HashMap<String, List<ZonasFisicasGeoeconomicasVO>> obtenerXMLResultadoObtenerZonasHomegeneas(
        Long idJob) {
        LOGGER.debug(
            "ProductoCatastralJobDAOBean#obtenerXMLResultadoObtenerZonasHomegeneas ... INICIA");

        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos =
            new HashMap<String, List<ZonasFisicasGeoeconomicasVO>>();
        ProductoCatastralJob job = this.findById(idJob);
        String resultadoJob = job.getResultado();

        try {

            Document xmlDocument = DocumentHelper.parseText(resultadoJob);
            if (xmlDocument != null) {
                List<Node> zonasResponse = xmlDocument.selectNodes("//Zonas");
                for (Node zonaCdata : zonasResponse) {
                    String cdata = zonaCdata.getText();
                    //LOGGER.debug("cdata " + cdata);
                    SAXReader reader = new SAXReader();
                    Document zonasHomogeneas = reader.read(new StringReader(cdata));
                    List<Node> zonas = zonasHomogeneas.selectNodes("//zonas");
                    for (Node zona : zonas) {
                        Element element = (Element) zona;
                        ZonasFisicasGeoeconomicasVO ZfgVO = new ZonasFisicasGeoeconomicasVO();
                        String numeroPredial = element.attributeValue("numeroPredial");
                        LOGGER.debug("numeroPredial " + numeroPredial);
                        String codigoZHF = element.attributeValue("codigoZHF");
                        LOGGER.debug("codigoZHF " + codigoZHF);
                        String codigoZHG = element.attributeValue("codigoZHG");
                        LOGGER.debug("codigoZHG " + codigoZHG);
                        String area = element.attributeValue("area");
                        LOGGER.debug("area " + area);
                        ZfgVO.setNumeroPredial(numeroPredial);
                        ZfgVO.setCodigoFisica(codigoZHF);
                        ZfgVO.setCodigoGeoeconomica(codigoZHG);
                        ZfgVO.setArea(area);
                        if (datos.get(numeroPredial) == null) {
                            List<ZonasFisicasGeoeconomicasVO> zonasList =
                                new ArrayList<ZonasFisicasGeoeconomicasVO>();
                            datos.put(numeroPredial, zonasList);
                        }
                        datos.get(numeroPredial).add(ZfgVO);
                    }
                }

            }
            return datos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /*
     * Obtiene el producto catastral job a partir del objeto en el xml @author andres.eslava
     */
    private ZonasFisicasGeoeconomicasVO obtenerZonasProductoCatastralJobVOXML(
        org.w3c.dom.Element predioZonaXML) {

        ZonasFisicasGeoeconomicasVO ZonasVO = new ZonasFisicasGeoeconomicasVO();
        ZonasVO.setNumeroPredial(
            predioZonaXML.getElementsByTagName("Value").item(0).getFirstChild().getNodeValue());
        ZonasVO.setArea(predioZonaXML.getElementsByTagName("Value").item(1).getFirstChild().
            getNodeValue());
        ZonasVO.setCodigoGeoeconomica(predioZonaXML.getElementsByTagName("Value").item(2).
            getFirstChild().getNodeValue());
        ZonasVO.setCodigoFisica(predioZonaXML.getElementsByTagName("Value").item(3).getFirstChild().
            getNodeValue());
        return ZonasVO;

    }

    /**
     * @see IProductoCatastralJobDAO#obtenerUltimoJobSinTerminarGeograficamente()
     * @return
     */
    @Override
    public ProductoCatastralJob obtenerUltimoJobObtenerZonasSinTerminarGeograficamente(
        Long idTramite) throws Exception {
        try {
            String sql = "select max(p) from ProductoCatastralJob p where p.estado in('" +
                ProductoCatastralJob.AGS_ERROR + "','" +
                ProductoCatastralJob.AGS_EN_EJECUCION + "','" +
                ProductoCatastralJob.SNC_ERROR + "','" +
                ProductoCatastralJob.AGS_ESPERANDO + "')" +
                "and p.tipoProducto = '" + ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS + "'" +
                "and p.tramiteId =:idTramite";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idTramite", idTramite);
            ProductoCatastralJob pcj = (ProductoCatastralJob) query.getSingleResult();
            return pcj;
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error obteniendo el ultimo job de obtener zonas");
        }
    }

    /**
     * @see IProductoCatastralJobDAO#obtenerJobsJasperServerEsperando(int)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsJasperServerEsperando(int cantidadJobs) {

        LOGGER.debug("obtenerJobsJasperServerEsperando");

        List<ProductoCatastralJob> resultados = null;
        String queryString;

        queryString = "SELECT p FROM ProductoCatastralJob p " +
            "WHERE estado = :estado ORDER BY id ASC";
        Query query = this.entityManager.createQuery(queryString);

        try {
            query.setParameter("estado", ProductoCatastralJob.JPS_ESPERANDO);
            if (cantidadJobs > 0) {
                query.setMaxResults(cantidadJobs);
            }
            query.setFirstResult(0);
            resultados = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ProductoCatastralJobDAOBean#obtenerJobsJasperServerEsperando");
        } finally {
            return resultados;
        }

    }

    /**
     * Obtiene el ultimo job de un tipo en particular para un tramite
     *
     * @author leidy.gonzalez
     * @param idTramites
     * @param tipo
     * @return
     */
    @Override
    public List<ProductoCatastralJob> obtenerUltimoJobPorIdTramites(List<Long> idTramites,
        String tipo) {

        List<ProductoCatastralJob> catastralJob = null;

        LOGGER.debug("Entra en ProductoCatastralJobDAOBean#obtenerUltimoJobPorIdTramites");

        Query query;
        int intervalSize = 500;
        ArrayList<List<Long>> splitIds = null;
        String queryToExecute = null;

        if (idTramites.size() < intervalSize) {

            queryToExecute = "SELECT v " +
                "FROM ProductoCatastralJob v " +
                "WHERE v.tramiteId IN (:tramiteId) " +
                "AND v.tipoProducto = :tipoProducto ";

        } else {

            int idxStart = 0;
            int idxFinal = intervalSize;

            int intervalsNum = idTramites.size() / intervalSize;

            if ((idTramites.size() % intervalSize) != 0) {
                intervalsNum++;
            }

            splitIds = new ArrayList<List<Long>>();

            for (int i = 1; i <= intervalsNum; i++) {
                if (idxFinal < idTramites.size()) {
                    splitIds.add(idTramites.subList(idxStart, idxFinal));
                } else {
                    splitIds.add(idTramites.subList(idxStart, idTramites.size()));
                }
                idxStart = idxFinal;
                idxFinal = idxFinal + intervalSize;

            }

            queryToExecute = "SELECT v " +
                "FROM ProductoCatastralJob v " +
                "WHERE v.tipoProducto = :tipoProducto " +
                "AND (";

            for (int i = 0; i < intervalsNum; i++) {
                queryToExecute += " v.tramiteId IN (:tramite" + i + "s)";

                if (i != (intervalsNum - 1)) {
                    queryToExecute += " OR ";
                } else {
                    queryToExecute += ")";
                }
            }
        }

        try {
            query = this.entityManager.createQuery(queryToExecute);

            if (idTramites.size() < intervalSize) {

                query.setParameter("tramiteId", idTramites);
                query.setParameter("tipoProducto", tipo);

            } else {
                int k = 0;
                for (List<Long> idsInterval : splitIds) {

                    query.setParameter("tramite" + k++ + "s", idsInterval);
                    query.setParameter("tipoProducto", tipo);

                }
            }
            catastralJob = (ArrayList<ProductoCatastralJob>) query.getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return catastralJob;

    }

    /**
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobGenerarCartaCatastralUrbanaConservacion(java.lang.String,
     * boolean, java.lang.Long, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public ProductoCatastralJob crearJobGenerarCartaCatastralUrbana(String codigoManzana,
        boolean generarEnPDF,
        Tramite tramite, ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {
        LOGGER.debug("crearJobGenerarDatosEdicion");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("codigoManzana");
        parameter.addElement("value").addText(codigoManzana);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("generarEnPDF");
        parameter.addElement("value").addText(Boolean.toString(generarEnPDF));

        return crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_IMAGEN_CARTA_CATASTRAL_URBANA,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, productoCatastralDetalle);
    }

    /**
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobGenerarFichaPredialDigital(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    public ProductoCatastralJob crearJobGenerarFichaPredialDigital(String codigoPredio,
        Tramite tramite,
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {
        LOGGER.debug("crearJobGenerarDatosEdicion");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("codigoPredio");
        parameter.addElement("value").addText(codigoPredio);

        return crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_IMAGEN_FICHA_PREDIAL_DIGITAL,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, productoCatastralDetalle);
    }

        public ProductoCatastralJob crearJobGenerarImagenPredial(String codigoPredio,Tramite tramite,
			ProductoCatastralDetalle productoCatastralDetalle,  UsuarioDTO usuario){
		LOGGER.debug("crearJobGenerarImagenPredial");
		Document parametros = DocumentHelper.createDocument();
		Element root = parametros.addElement("SigJob");
		Element parameters = root.addElement("parameters");
		Element parameter1 = parameters.addElement("parameter");
		parameter1.addElement("name").addText("codigoPredio");
		parameter1.addElement("value").addText(codigoPredio);
        Element parameter2 = parameters.addElement("parameter");
        parameter2.addElement("name").addText("enviar_mail");
		parameter2.addElement("value").addText("SI");
        Element parameter3 = parameters.addElement("parameter");
        parameter3.addElement("name").addText("email_notificacion");
		parameter3.addElement("value").addText(usuario.getEmail());
		
		return crearJobGeoprocesamiento(usuario, ProductoCatastralJob.SIG_JOB_IMAGEN_PREDIAL,
				ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, null);
	}
	
    /**
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO#crearJobGenerarCertificadoPlanoPredial(java.lang.String,
     * co.gov.igac.snc.persistence.entity.tramite.Tramite,
     * co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    public ProductoCatastralJob crearJobGenerarCertificadoPlanoPredial(String codigoPredio,
        Tramite tramite,
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {
        LOGGER.debug("crearJobGenerarCertificadoPlanoPredial");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("codigoPredio");
        parameter.addElement("value").addText(codigoPredio);

        return crearJobGeoprocesamiento(usuario,
            ProductoCatastralJob.SIG_JOB_IMAGEN_CERTIFICADO_PLANO_PREDIAL,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, productoCatastralDetalle);
    }

    /**
     * @see
     * IProductoCatastralJobDAO#buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(Long)
     * @return
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public ProductoCatastralJob buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(
        Long idProductoCatastralDetalle) {

        LOGGER.debug("buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId");

        String queryString = "SELECT p FROM ProductoCatastralJob p " +
            " WHERE productoCatastralDetalleId = :idProductoCatastralDetalle " +
            "ORDER BY id DESC";
        Query query = this.entityManager.createQuery(queryString);

        try {
            query.setParameter("idProductoCatastralDetalle", idProductoCatastralDetalle);
            List<ProductoCatastralJob> jobs = query.getResultList();
            if (jobs != null && !jobs.isEmpty()) {
                return jobs.get(0);
            } else {
                return null;
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ProductoCatastralJobDAOBean#buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId");
        }
    }

    /**
     * @see IProductoCatastralJobDAO#crearJobActualizarColindancia
     * @param predios
     * @param usuario
     * @param tramite
     * @return
     */
    @Override
    public ProductoCatastralJob crearJobActualizarColindancia(String predios, UsuarioDTO usuario,
        Tramite tramite) {
        LOGGER.debug("crearJobActualizarColindancia");
        Document parametros = DocumentHelper.createDocument();
        Element root = parametros.addElement("SigJob");
        Element parameters = root.addElement("parameters");

        Element parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("predios");
        parameter.addElement("value").addText(predios);

        parameter = parameters.addElement("parameter");
        parameter.addElement("name").addText("usuario");
        parameter.addElement("value").addText(usuario.getLogin());

        return crearJobGeoprocesamiento(usuario, ProductoCatastralJob.SIG_JOB_ACTUALIZAR_COLINDANTES,
            ProductoCatastralJob.AGS_ESPERANDO, root.asXML(), tramite, null);
    }

    //FIN DAO
}
