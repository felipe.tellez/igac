package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IAjusteManzanaAreaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.AjusteManzanaArea;

@Stateless
public class AjusteManazanaAreaDAOBean extends GenericDAOWithJPA<AjusteManzanaArea, Long> implements
    IAjusteManzanaAreaDAO {

}
