/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface IUsoConstruccionDAO extends IGenericJpaDAO<UsoConstruccion, Long> {

    public List<UsoConstruccion> buscarUsosConstruccionPorNumerosPredialesPredio(
        List<String> numerosPrediales);
}
