package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoParticipante;

/**
 *
 * Interfaz para los métodos de bd de la tabla COMITE_AVALUO_PARTICIPANTE
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IComiteAvaluoParticipanteDAO extends IGenericJpaDAO<ComiteAvaluoParticipante, Long> {

}
