package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IConvenioDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementación de los servicios de persistencia del objeto Convenio.
 *
 * @author jamir.avila.
 */
@Stateless(mappedName = "ConvenioDAO")
public class ConvenioDAOBean extends GenericDAOWithJPA<Convenio, Long> implements IConvenioDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDAOBean.class);

    /**
     * Constructor por omisión.
     *
     * @autor jamir.avila.
     */
    public ConvenioDAOBean() {
    }

    /**
     * {@link #recuperarConveniosPorIdActualizacion(int)}
     */
    @Override
    public List<Convenio> recuperarConveniosPorIdActualizacion(long idActualizacion)
        throws ExcepcionSNC {
        LOGGER.debug("idActualizacion: " + idActualizacion);
        Query query = entityManager.createNamedQuery("recuperarConveniosPorIdActualizacion");
        query.setParameter("idActualizacion", idActualizacion);

        @SuppressWarnings("unchecked")
        List<Convenio> convenios = (List<Convenio>) query.getResultList();
        return convenios;
    }

}
