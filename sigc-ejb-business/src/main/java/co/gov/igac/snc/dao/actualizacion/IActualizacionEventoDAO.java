package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;

/**
 * Servicios de persistencia para el objeto ActualizacionEvento
 *
 * @author franz.gamba
 */
@Local
public interface IActualizacionEventoDAO extends IGenericJpaDAO<ActualizacionEvento, Long> {

}
