package co.gov.igac.snc.dao.vistas.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVPPredioAvaluoDecretoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto {@link VPPredioAvaluoDecreto}.
 *
 * @author david.cifuentes
 */
@Stateless
public class VPPredioAvaluoDecretoDAOBean extends
    GenericDAOWithJPA<VPPredioAvaluoDecreto, Long> implements
    IVPPredioAvaluoDecretoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPPredioAvaluoDecretoDAOBean.class);

    /**
     * @see IVPPredioAvaluoDecretoDAO#buscarAvaluosCatastralesPorIdPPredio(Long pPredioId)
     * @author david.cifuentes
     * @modified by javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(
        Long pPredioId, Date fechaDelCalculo) {
        List<VPPredioAvaluoDecreto> predioAvaluosCatastrales = null;
        try {
            Query query = this.entityManager
                .createQuery("SELECT vppad FROM VPPredioAvaluoDecreto vppad" +
                    " JOIN FETCH vppad.PPredio pp" +
                    " WHERE pp.id = :pPredioId" +
                    " AND vppad.fechaInscripcionCatastral >= :fechaDelCalculo" +
                    " AND (vppad.cancelaInscribe = :modifica" +
                    " OR vppad.cancelaInscribe = :inscribe)" +
                    " ORDER BY vppad.vigencia DESC");

            query.setParameter("pPredioId", pPredioId);
            query.setParameter("fechaDelCalculo", fechaDelCalculo);
            query.setParameter("modifica", EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            query.setParameter("inscribe", EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            predioAvaluosCatastrales = (List<VPPredioAvaluoDecreto>) query
                .getResultList();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predioAvaluosCatastrales;
    }

    /**
     * @see IVPPredioAvaluoDecretoDAO#buscarAvaluosCatastralesPorIdPPredio(Long)
     * @author david.cifuentes
     * @modified christian.rodriguez
     * @modified by javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredio(
        Long pPredioId) {
        List<VPPredioAvaluoDecreto> predioAvaluosCatastrales = null;
        try {

            Query query = this.entityManager
                .createQuery("SELECT vppad " +
                    " FROM VPPredioAvaluoDecreto vppad " +
                    " JOIN FETCH vppad.PPredio pp " +
                    " WHERE pp.id = :pPredioId " +
                    " AND (vppad.cancelaInscribe = :modifica " +
                    " OR vppad.cancelaInscribe = :inscribe " +
                    " OR vppad.cancelaInscribe = :temp) " +
                    " ORDER BY vppad.vigencia DESC");

            query.setParameter("pPredioId", pPredioId);
            query.setParameter("modifica", EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            query.setParameter("inscribe", EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            query.setParameter("temp", EProyeccionCancelaInscribe.TEMP.getCodigo());
            predioAvaluosCatastrales = (List<VPPredioAvaluoDecreto>) query.getResultList();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predioAvaluosCatastrales;
    }
}
