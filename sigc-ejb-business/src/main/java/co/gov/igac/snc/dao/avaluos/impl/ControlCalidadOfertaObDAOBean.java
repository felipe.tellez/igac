package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadOfertaObDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOfertaOb;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IControlCalidadOfertaObDAO
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class ControlCalidadOfertaObDAOBean extends
    GenericDAOWithJPA<ControlCalidadOfertaOb, Long> implements
    IControlCalidadOfertaObDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadOfertaObDAOBean.class);

    /**
     * @see IControlCalidadOfertaObDAO#cargarObservacionesPorControlCalidadOfertaId(Long)
     */
    @Override
    public List<ControlCalidadOfertaOb> cargarObservacionesPorControlCalidadOfertaId(
        Long controlCalidadOfertaId) {

        LOGGER.debug(
            "on ControlCalidadOfertaObDAOBean#cargarObservacionesPorControlCalidadOfertaId ...");

        List<ControlCalidadOfertaOb> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT ccoob FROM ControlCalidadOfertaOb ccoob" +
            " WHERE ccoob.controlCalidadOferta.id = :idcco" +
            " ORDER BY ccoob.fecha ASC";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idcco", controlCalidadOfertaId);

            queryAnswer = query.getResultList();

            for (ControlCalidadOfertaOb ccob : queryAnswer) {
                ccob.getControlCalidadOferta().getOfertaInmobiliariaId().getOfertaPredios();
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return queryAnswer;

    }

    /**
     * @see IControlCalidadOfertaObDAO#cargarObservacionesPorOfertaIdY(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ControlCalidadOfertaOb> cargarObservacionesPorOfertaId(Long ofertaId) {

        LOGGER.debug("on ControlCalidadOfertaObDAOBean#cargarObservacionesPorOfertaId ...");

        List<ControlCalidadOfertaOb> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT cco " + " FROM ControlCalidadOfertaOb cco " +
            " WHERE cco.controlCalidadOferta.ofertaInmobiliariaId.id = :ofertaId " +
            " ORDER BY cco.fecha DESC";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("ofertaId", ofertaId);

            queryAnswer = (List<ControlCalidadOfertaOb>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ControlCalidadOfertaObDAOBean#cargarObservacionesPorOfertaId");
        }

        return queryAnswer;

    }

}
