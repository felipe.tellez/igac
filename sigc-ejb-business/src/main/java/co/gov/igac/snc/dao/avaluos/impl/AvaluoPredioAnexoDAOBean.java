package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioAnexoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioAnexo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoPredioAnexoDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoPredioAnexoDAOBean extends
    GenericDAOWithJPA<AvaluoPredioAnexo, Long> implements IAvaluoPredioAnexoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoPredioAnexoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoPredioAnexoDAO#consultarPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioAnexo> consultarPorAvaluo(long avaluoId) {

        LOGGER.debug("on AvaluoPredioAnexoDAOBean#consultarPorAvaluo");

        List<AvaluoPredioAnexo> answer = null;
        String queryString;
        Query query;

        queryString = "" +
            " SELECT apa FROM AvaluoPredioAnexo apa WHERE apa.avaluo.id = :idAvaluo_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo_p", avaluoId);
            answer = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "AvaluoPredioAnexoDAOBean#consultarPorAvaluo");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoPredioAnexoDAO#calcularAnexosPreviosAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Boolean calcularAnexosPreviosAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "SELECT o FROM AvaluoPredioAnexo o " +
            "JOIN o.avaluo ava WHERE ava.id = :idAvaluo";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            List<AvaluoPredioAnexo> apzs = query.getResultList();
            if (apzs == null || apzs.isEmpty()) {
                return null;
            }

            if (apzs.get(0).getAvaluoPredioId() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#calcularCultivosPreviosAvaluo");
        }
    }

    /**
     * @see IAvaluoPredioAnexoDAO#eliminarAnexosPreviosAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public boolean eliminarAnexosPreviosAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "DELETE FROM avaluo_predio_anexo ctv WHERE ctv.avaluo_id = " + idAvaluo;

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.executeUpdate();
            return true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#eliminarAnexosPreviosAvaluo");
        }
    }

//end of class
}
