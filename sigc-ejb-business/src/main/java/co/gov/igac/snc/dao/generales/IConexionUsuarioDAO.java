/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import javax.ejb.Local;

/**
 * Dao para acceder a la entidad ConexionUsuario
 *
 * @author felipe.cadena
 */
@Local
public interface IConexionUsuarioDAO extends IGenericJpaDAO<ConexionUsuario, Long> {

    /**
     * Retorna el numero de conexiones actuales de un usuario
     *
     * @author felipe.cadena
     *
     * @param usuario
     * @return
     */
    public Long obtenerNumeroConexionesPorUsuario(String usuario);

    /**
     * Retorna la conexion asociada al id de sesion
     *
     * @author felipe.cadena
     * @param idSesion
     * @return
     */
    public ConexionUsuario obtenerPorIdSesion(String idSesion);

}
