package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;

/**
 * Servicios de persistencia para el objeto ProductoCatastralDetalle
 *
 * @author javier.barajas
 */
@Local
public interface IProductoCatastralDetalleDAO extends IGenericJpaDAO<ProductoCatastralDetalle, Long> {

    /**
     * Método encargado de buscar en la base de datos los productos catastrales detalles asociados a
     * un producto catastral
     *
     * @param productoCatastralId id del producto catastrak
     * @return lista de productos catastrales detalles asociados al producto catastral
     * @author javier.aponte
     */
    public List<ProductoCatastralDetalle> buscarProductosCatastralesDetallePorProductoCatastralId(
        Long productoCatastralId);
}
