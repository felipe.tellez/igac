package co.gov.igac.snc.util;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * Clase utilitaria para la lectura del archivo .properties con la configuración para SNC
 *
 * @author juan.mendez
 *
 */
public class SNCPropertiesUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(LdapDAO.class);

    private static SNCPropertiesUtil instance;

    private ResourceBundle rs;

    /**
     *
     */
    private SNCPropertiesUtil() {
        try {
            rs = ResourceBundle.getBundle("application");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100006.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    public static synchronized SNCPropertiesUtil getInstance()
        throws ExcepcionSNC {
        if (instance == null) {
            instance = new SNCPropertiesUtil();
        }
        return instance;
    }

    /**
     *
     * @param propertyName
     * @return
     */
    public String getProperty(String propertyName) {
        String propertyValue = null;
        try {
            propertyValue = rs.getString(propertyName);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100006.getExcepcion(
                LOGGER, e, e.getMessage(), propertyName);
        }
        return propertyValue;
    }

}
