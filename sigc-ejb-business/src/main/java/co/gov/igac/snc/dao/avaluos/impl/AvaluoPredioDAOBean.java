package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IAvaluoPredioDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class AvaluoPredioDAOBean extends GenericDAOWithJPA<AvaluoPredio, Long>
    implements IAvaluoPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoPredioDAOBean.class);

    /**
     * @see IAvaluoDAO#buscarAvaluoPredioPorIdAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public List<AvaluoPredio> buscarAvaluoPredioPorIdAvaluo(Long idAvaluo) {

        List<AvaluoPredio> aps = new ArrayList<AvaluoPredio>();

        String queryString;
        Query query;

        queryString = "SELECT ap " +
            " FROM AvaluoPredio ap LEFT JOIN ap.avaluo ava" +
            " WHERE ava.id = :idAvaluo)";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            aps = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return aps;

    }

    /**
     * @see IAvaluoPredioDAO#buscarDepartamentosMunicipiosAvaluo(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<Municipio> buscarDepartamentosMunicipiosAvaluo(Long idAvaluo) {

        LOGGER.debug("Inicio AvaluoPredioDAOBean#buscarDepartamentosMunicipiosAvaluo");

        List<Municipio> result = new ArrayList<Municipio>();
        List<AvaluoPredio> resultAvaluoPredio = new ArrayList<AvaluoPredio>();
        String queryString;
        Query query;

        queryString = "SELECT ap" +
            " FROM AvaluoPredio ap" +
            " LEFT JOIN FETCH ap.municipio" +
            " LEFT JOIN FETCH ap.departamento" +
            " WHERE ap.avaluo.id =:idAvaluo" +
            " ORDER BY ap.municipio.codigo";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            resultAvaluoPredio = query.getResultList();

            for (AvaluoPredio ap : resultAvaluoPredio) {
                ap.getMunicipio();
                if (!result.contains(ap.getMunicipio())) {
                    result.add(ap.getMunicipio());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        LOGGER.debug("Fin AvaluoPredioDAOBean#buscarDepartamentosMunicipiosAvaluo");
        return result;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoPredioDAO#consultarPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredio> consultarPorAvaluo(long avaluoId) {

        LOGGER.debug("on AvaluoPredioDAOBean#consultarPorAvaluo");

        List<AvaluoPredio> answer = null;
        String queryString;
        Query query;

        queryString = "" +
            " SELECT ap FROM AvaluoPredio ap " +
            " JOIN FETCH ap.departamento " +
            " JOIN FETCH ap.municipio " +
            " WHERE ap.avaluo.id = :idAvaluo_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo_p", avaluoId);
            answer = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "AvaluoPredioDAOBean#consultarPorAvaluo");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

//end of class
}
