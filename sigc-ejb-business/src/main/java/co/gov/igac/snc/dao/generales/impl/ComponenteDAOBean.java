package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IComponenteDAO;
import co.gov.igac.snc.persistence.entity.generales.Componente;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author felipe.cadena
 */
@Stateless
public class ComponenteDAOBean extends GenericDAOWithJPA<Componente, Long> implements IComponenteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComponenteDAOBean.class);

    @Override
    public Componente obtenerComponentePorNombre(String nombre) {

        Query query;
        //String queryToExecute = "SELECT c FROM Componente c WHERE c.nombre like :nombreParam";
        String queryToExecute = "SELECT comp FROM Componente comp WHERE comp.nombre = :nombreParam";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("nombreParam", nombre);
        Componente resultStr = null;
        try {
            resultStr = (Componente) query.getSingleResult();
            Hibernate.initialize(resultStr.getComponente());
        } catch (NoResultException ex) {
            LOGGER.error(ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, ex, ex.
                getMessage(),
                "La consulta de Componente no obtuvo resultados");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultStr;

    }
}
