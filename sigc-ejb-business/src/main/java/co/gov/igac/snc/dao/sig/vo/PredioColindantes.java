package co.gov.igac.snc.dao.sig.vo;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Value object para transportar un predio con sus colindantes
 *
 *
 * @author juan.mendez
 *
 */
public class PredioColindantes {

    private String codigoPredio;
    private String[] colindantes;

    public PredioColindantes(String codigoPredio, String[] colindantes) {
        this.codigoPredio = codigoPredio;
        this.colindantes = colindantes;
    }

    public String getCodigoPredio() {
        return codigoPredio;
    }

    public void setCodigoPredio(String codigoPredio) {
        this.codigoPredio = codigoPredio;
    }

    public String[] getColindantes() {
        return colindantes;
    }

    public void setColindantes(String[] colindantes) {
        this.colindantes = colindantes;
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
