package co.gov.igac.snc.dao.tramite.radicacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.ITramiteRectificacionDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class TramiteRectificacionDAOBean extends GenericDAOWithJPA<TramiteRectificacion, Long>
    implements ITramiteRectificacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteRectificacionDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see TramiteRectificacionDAO#findTramiteRectificacionesByTramiteId
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<TramiteRectificacion> findTramiteRectificacionesByTramiteId(Long tramiteId) {

        List<TramiteRectificacion> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tr FROM TramiteRectificacion tr " +
            " WHERE tr.tramite.id = :tramiteId ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);
            answer = (List<TramiteRectificacion>) query.getResultList();
        } catch (Exception ex) {
            LOGGER.error(
                "Error on TramiteRectificacionDAOBean#findTramiteRectificacionesByTramiteId: " +
                ex.getMessage());
        }

        return answer;
    }

}
