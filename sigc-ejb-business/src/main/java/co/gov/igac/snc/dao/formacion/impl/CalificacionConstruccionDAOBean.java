/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.formacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.formacion.ICalificacionConstruccionDAO;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import javax.ejb.Stateless;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class CalificacionConstruccionDAOBean
    extends GenericDAOWithJPA<CalificacionConstruccion, Long>
    implements ICalificacionConstruccionDAO {

}
