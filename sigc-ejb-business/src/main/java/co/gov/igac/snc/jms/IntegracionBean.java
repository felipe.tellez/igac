package co.gov.igac.snc.jms;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.jms.vo.EventDetails;
import co.gov.igac.snc.jms.vo.EventType;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.interceptors.TimeInterceptor;

/**
 * Implementación de la interfaz de servicios para Integración con otros sistemas
 *
 *
 * @author juan.mendez
 *
 */
@Stateless
@Interceptors(TimeInterceptor.class)
public class IntegracionBean implements IIntegracionLocal {

    private static final Logger LOGGER = LoggerFactory.getLogger(IntegracionBean.class);

    // @Resource(mappedName = "sncQueue")
    @Resource(mappedName = "java:/queue/sncQueue")
    private Queue destinationQueue;

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    @EJB
    private IProductoCatastralJobDAO jobDao;

//--------------------------------------------------------------------------------------------------    
    /**
     * @see co.gov.igac.snc.fachadas.IIntegracionLocal#generarReplicaParaTramite(
     * co.gov.igac.snc.persistence.entity.tramite.Tramite, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    @Deprecated
    public void generarReplicaParaTramite(Tramite tramite, Actividad actividad, UsuarioDTO usuario,
        PPredio predioResultante) {
        LOGGER.debug("generarReplicaParaTramite");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            StringBuilder prediosSb = new StringBuilder("");

            if (predioResultante != null) {
                prediosSb.append(predioResultante.getNumeroPredial()).append(",");
            } else {
                List<Predio> predios = tramite.getPredios();

                for (Predio predio : predios) {
                    if (!(("5".equals(predio.getCondicionPropiedad()) || "6".equals(predio.
                        getCondicionPropiedad())))) {
                        prediosSb.append(predio.getNumeroPredial()).append(",");
                    }
                }
            }
            LOGGER.debug("prediosSb:" + prediosSb.toString());

            Message message = session.createTextMessage();
            EventType actionType = EventType.SIG_COPIA_TRABAJO_CREAR;
            message.setIntProperty(EventDetails.EVENT_TYPE, actionType.ordinal());
            message.setLongProperty(EventDetails.EVENT_TIMESTAMP, System.currentTimeMillis());
            message.setLongProperty(EventDetails.EVENT_TRAMITE_NUMERO, tramite.getId());
            message.setStringProperty(EventDetails.EVENT_PREDIOS_CODIGOS, prediosSb.toString());
            message.setStringProperty(EventDetails.EVENT_USER_LOGIN, usuario.getLogin());
            message.setStringProperty(EventDetails.EVENT_ACTIVIDAD_ID, actividad.getId());
            message.setStringProperty(EventDetails.EVENT_TIPO_TRAMITE, actividad.getTipoTramite());
            message.setStringProperty(EventDetails.EVENT_ACTIVIDAD_ACTUAL, actividad.
                getRutaActividad().getSubprocesoActividad());

            //TODO: PENDIENTE CAMBIOS DE ARQUITECTURA
            MessageProducer producer = session.createProducer(this.destinationQueue);
            producer.send(message);
            LOGGER.debug("Message sent with actionType=" + actionType);
        } catch (Exception e) {
            LOGGER.error("Error sending message", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                LOGGER.error("Problem closing JMS Connection", ex);
            }
        }
    }

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
    /**
     * @see co.gov.igac.snc.fachadas.IIntegracionLocal#generarReplicaParaTramite(
     * co.gov.igac.snc.persistence.entity.tramite.Tramite, co.gov.igac.generales.dto.UsuarioDTO,
     * PPredio predio)
     */
    @Override
    @Deprecated
    public void generarReplicaParaTramite(Tramite tramite, Actividad actividad, UsuarioDTO usuario) {
        this.generarReplicaParaTramite(tramite, actividad, usuario, null);
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * Método encargado de llamar la validación de la Geometría de predios de forma asincrónica
     *
     * @param identificadoresPredios
     * @param tramiteID
     * @param usuario
     * @author fredy.wilches
     */
    /*
     * @modified pedro.garcia 15-08-2013 No se usa el detalle que lleva el id de actividad porque ya
     * no se va a mover la actividad como parte de esta ejecución
     */
    @Implement
    @Override
    public boolean validarGeometriaPredios(String identificadoresPredios, Long tramiteId,
        UsuarioDTO usuario, String tipoTramite) {
        LOGGER.debug("validarGeometriaPredios");

        boolean answer = true;
        Connection connection = null;
        try {
            connection = this.connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Message message = session.createTextMessage();
            EventType actionType = EventType.VALIDAR_GEOMETRIA_PREDIOS;
            message.setIntProperty(EventDetails.EVENT_TYPE, actionType.ordinal());
            message.setStringProperty(EventDetails.EVENT_USER_LOGIN, usuario.getLogin());
            message.setStringProperty(EventDetails.EVENT_PREDIOS_CODIGOS, identificadoresPredios);
            message.setLongProperty(EventDetails.EVENT_TRAMITE_NUMERO, tramiteId);
            message.setStringProperty(EventDetails.EVENT_TIPO_TRAMITE, tipoTramite);
            //message.setStringProperty(EventDetails.EVENT_ACTIVIDAD_ID, actividadId);

            MessageProducer producer = session.createProducer(this.destinationQueue);
            producer.send(message);
            LOGGER.debug("Mensaje JMS enviado. Tipo = " + actionType);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            answer = false;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IIntegracionLocal#validarGeometriaUnidadesConstruccionA(java.lang.String,
     * java.lang.Long, co.gov.igac.generales.dto.UsuarioDTO)
     * @author pedro.garcia
     *
     * Es básicamente lom mismo que
     * {@link #validarGeometriaPredios(java.lang.String, java.lang.Long, co.gov.igac.generales.dto.UsuarioDTO)}
     * solo que se usa una propiedad adicional en el mensaje para diferenciar a qué método se llama
     * cuando se ejecute el Action definido.
     */
    @Implement
    @Override
    public boolean validarGeometriaUnidadesConstruccionA(String numerosPrediales,
        Long tramiteId, UsuarioDTO usuario) {
        LOGGER.debug("en IntegracionBean#validarGeometriaUnidadesConstruccionA");

        boolean answer = true;
        Connection connection = null;
        try {
            connection = this.connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Message message = session.createTextMessage();
            EventType actionType = EventType.VALIDAR_GEOMETRIA_PREDIOS;
            message.setIntProperty(EventDetails.EVENT_TYPE, actionType.ordinal());
            message.setStringProperty(EventDetails.EVENT_USER_LOGIN, usuario.getLogin());
            message.setStringProperty(EventDetails.EVENT_PREDIOS_CODIGOS, numerosPrediales);
            message.setLongProperty(EventDetails.EVENT_TRAMITE_NUMERO, tramiteId);

            //D: se setea esta propiedad para diferenciar el método que se va a ejecutar luego.
            //  Estrictamente, no se tiene en cuenta el tipo de trámite, sino que se usa como un flag para definir luego qué método llama en al Action.
            //  Se le da ese valor, porque la acción de validar geometría de uniades de construcción está asociada a los trámites de
            // cancelación de predio y de rectificación por cancelación por doble inscripción
            message.setStringProperty(EventDetails.EVENT_TIPO_TRAMITE,
                ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo());

            MessageProducer producer = session.createProducer(this.destinationQueue);
            producer.send(message);
            LOGGER.debug("Mensaje JMS enviado. Tipo = " + actionType);
        } catch (Exception e) {
            LOGGER.error(
                "Error en IntegracionBean#validarGeometriaUnidadesConstruccionA enviando el mensaje a la cola:" +
                e.getMessage(), e);
            answer = false;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return answer;
    }

}
