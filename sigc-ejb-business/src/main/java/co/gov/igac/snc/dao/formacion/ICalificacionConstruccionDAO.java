/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.formacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import javax.ejb.Local;

/**
 * Interface para el DAO de CalificacionConstruccion
 *
 * @author pedro.garcia
 */
@Local
public interface ICalificacionConstruccionDAO
    extends IGenericJpaDAO<CalificacionConstruccion, Long> {

}
