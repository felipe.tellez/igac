/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.conservacion.impl;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.business.conservacion.IAplicarCambios;
import co.gov.igac.snc.dao.conservacion.IHPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.conservacion.ITramiteDepuracionDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteTareaDAO;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.fachadas.IConservacionLocal;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EHPredioTipoHistoria;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EProcesosAsincronicos;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTareaResultado;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.ErrorUtil;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.LoggerFactory;

/**
 * Bean encargado de controlar los procesos de aplicar cambios.
 *
 * PD: las transacciones en este bean deben ser controladas explicitamente por el recurso
 * userTransaction
 *
 *
 * @author felipe.cadena
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
public class AplicarCambiosBean implements IAplicarCambios, Serializable {

    @Resource
    private UserTransaction userTransaction;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AplicarCambiosBean.class);

    @EJB
    private IGenerales generalesService;

    @EJB
    private ITramite tramiteService;

    @EJB
    private IProcesos procesosService;

    @EJB
    private IConservacion conservacionService;

    @EJB
    private IHPredioDAO hPredioDao;

    @EJB
    private ITramiteTareaDAO tramiteTareaDao;

    @EJB
    private ITramiteDAO tramiteDao;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private IProductoCatastralJobDAO productoCatastralJobDAO;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;

    @EJB
    private IDocumentoDAO documentoDao;

    @EJB
    private ITramiteDepuracionDAO tramiteDepuracionDAO;

    @EJB
    private IPPredioDAO pPredioDao;
    
    @EJB
    private ITramiteDocumentoDAO tramiteDocumentoDao;

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------         proceso aplicar cambios Depuración   -----------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO ACD # 1 <br/><br/>
     *
     * Metodo para transferir el tramite a tiempos muertos
     *
     * @author felipe.cadena
     */
    @Override
    public void transferirATiemposMuertosACD(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            "INICIO_TRANSFERENCIA_TIEMPOS_MUERTOS", 1);

        String message;

        try {

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.INICIO.toString(), null, null, 1);

            UsuarioDTO usuarioTiemposMuertos = this.generalesService.
                getCacheUsuario(ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());
            this.transferirTramite(tramite, usuarioTiemposMuertos);

            //paso existoso
            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 1);
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió al transferir el tramite a tiempos muertos";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 1);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 1);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#transferirATiemposMuertosACD",
                "finalizarEdicionTramiteGeografico");
        }

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            "FIN_TRANSFERENCIA_TIEMPOS_MUERTOS", 1);

    }

    /**
     * PASO ACD # 2 <br/><br/>
     *
     * Metodo asociado al paso 2 del proceso de aplicar cambios de depuracion. El paso 2 es
     * asincronico, asi que en este metodo solo se inicia el paso.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean enviarJobGeograficoDepuracion(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "INICIO_ENVIO_ASINCRONICO",
            2);

        String idDocumentoReplicaGDB;
        String message;
        boolean answer;

        try {
            Long replicaFinalTipoDocId = ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId();
            Documento replicaFinal = this.generalesService.
                buscarDocumentoPorTramiteIdyTipoDocumento(tramite.getId(), replicaFinalTipoDocId);

            idDocumentoReplicaGDB = "";
            if (replicaFinal.getIdRepositorioDocumentos() != null &&
                replicaFinal.getIdRepositorioDocumentos().contains(
                    Constantes.PREFIJO_IDS_DOCUMENTOS_REPOSITORIO_DOCS)) {

                idDocumentoReplicaGDB = replicaFinal.getIdRepositorioDocumentos();

            } else if (replicaFinal.getArchivo() != null &&
                replicaFinal.getArchivo().contains(
                    Constantes.PREFIJO_IDS_DOCUMENTOS_REPOSITORIO_DOCS)) {

                idDocumentoReplicaGDB = replicaFinal.getArchivo();

            }

        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error obteniendo la copia geografica final del tramite";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 2);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 2);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "AplicarCambiosBean#enviarJobGeograficoDepuracion",
                "finalizarEdicionTramiteGeograficoDepuracion");
        }

        LOGGER.debug("Inicia Sincronización de datos con ArcSDE...");
        try {
            ProductoCatastralJob job = SigDAOv2.getInstance().aplicarCambiosAsync(
                this.productoCatastralJobDAO,
                tramite, idDocumentoReplicaGDB, usuario);
            message = Constantes.PREFIJO_JOB_ID + job.getId();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 2);
            answer = true;
        } catch (ExcepcionSNC ex) {
            message =
                "Ocurrió un error ejecutando el envio del Job de la finalizacion de edición geográfica del trámite en el servidor ArcGis";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 2);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 2);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#enviarJobGeograficoDepuracion",
                "finalizarEdicionTramiteGeografico");
        }
        LOGGER.debug("Finaliza Sincronización de datos con ArcSDE.");

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "FIN_ENVIO_ASINCRONICO", 2);

        return answer;
    }

    /**
     * PASO ACD # 3 <br/><br/>
     *
     * Metodo para renombrar las replicas del proceso de depuración
     *
     * @param tramite
     * @param usuario
     *
     */
    @Override
    public void renombrarReplicasDepuracion(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "INICIO_RENOMBRAR_REPLICAS",
            3);

        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            ETramiteTareaResultado.FIN.toString(), null, null, 2);
        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 3);

        String message;

        try {

            //se elimina la version geografica
            this.eliminarVersionGeoTramite(tramite, usuario);

            this.cambiarTipoDocumentoCopiaBdGeografica(tramite.getId());
            //paso existoso
            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 3);
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error al renombrar las replicas de depuracion";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 3);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 3);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#avanzarProcesoACD",
                "avanzarActividadTramite");
        }

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "FIN_RENOMBRAR_REPLICAS", 3);

    }

    /**
     * PASO ACD # 4 <br/><br/>
     *
     * Metodo para avanzar el proceso a la actividad corespondiente una vez se termino la aplicación
     * de cambios gograficos
     *
     */
    @Override
    public UsuarioDTO avanzarProcesoACD(Tramite tramite, UsuarioDTO usuario) {

        String message;
        Long idTramite = tramite.getId();
        UsuarioDTO usuarioDestino;

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "INICIO_AVANCE_PROCESO", 4);
        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 4);

        Actividad actividadTramite;

        String processTransition = "";
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuariosActividad = new ArrayList<UsuarioDTO>();

        //obtener actividad
        try {
            actividadTramite = this.obtenerActividadActualTramite(idTramite);
        } catch (ExcepcionSNC ex) {

            message = "Ocurrió un error obteniendo la atividad del tramite";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 4);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 4);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#avanzarProcesoACD",
                "obtenerActividadTramite");
        }

        //determinar actividad de procedencia de la depuración
        String actividadPrevia = actividadTramite.getObservacion();

        if (actividadPrevia.equals(
            ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE)) {
            usuarioDestino = this.tramiteService.buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION).get(0);
        } else {
            usuarioDestino = this.generalesService.
                getCacheUsuario(tramite.getFuncionarioEjecutor());
        }

        processTransition = actividadPrevia;
        usuariosActividad.add(usuarioDestino);
        solicitudCatastral.setUsuarios(usuariosActividad);
        solicitudCatastral.setTransicion(processTransition);

        //avanzar la actividad
        try {
            this.procesosService.avanzarActividad(usuario, actividadTramite.getId(),
                solicitudCatastral);
            //paso existoso
            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 4);
        } catch (Exception ex) {
            message = "Ocurrió un error en el BPM al tratar de avanzar la actividad del trámite " +
                "con id " + idTramite;
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 4);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 4);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#avanzarProcesoACD",
                "avanzarActividadTramite");
        }

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "FIN_AVANCE_PROCESO", 4);
        return usuarioDestino;
    }

    /**
     * PASO ACD # 5 <br/><br/>
     *
     * Metodo para transferir el tramite desde tiempos muertos al usuario oiginal
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    public void transferirAUsuarioOriginalACD(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            "INICIO_TRANSFERENCIA_USUARIO_ORIGINAL", 5);
        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 5);

        String message;

        try {

            this.transferirTramite(tramite, usuario);

            //paso existoso
            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 5);
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió al transferir el tramite a tiempos muertos";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(), "ERROR", 5);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 5);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#transferirAUsuarioOriginalACD",
                "transferenciaAUsuarioOriginal");
        }

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.toString(),
            "FIN_TRANSFERENCIA_USUARIO_ORIGINAL", 5);

    }

    /**
     * ACD
     *
     * Metodo para invocar el proceso aplicar cambios de depuracion desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    @Override
    public void aplicarCambiosDepuracion(Tramite tramite, UsuarioDTO usuario, int paso) {
        try {
            if (paso == 0) {
                this.userTransaction.begin();
                this.transferirATiemposMuertosACD(tramite, usuario);
                this.userTransaction.commit();
                this.aplicarCambiosDepuracion(tramite, usuario, 1);
            } else if (paso == 1) {
                this.userTransaction.begin();
                this.enviarJobGeograficoDepuracion(tramite, usuario);
                this.userTransaction.commit();
            } else if (paso == 2) {
                //Este paso solo se puede enviar desde el regreso del paso asincronico
                this.userTransaction.begin();
                this.renombrarReplicasDepuracion(tramite, usuario);
                this.userTransaction.commit();
                this.aplicarCambiosDepuracion(tramite, usuario, 3);
            } else if (paso == 3) {
                this.userTransaction.begin();
                this.avanzarProcesoACD(tramite, usuario);
                this.userTransaction.commit();

            }

        } catch (Exception ex) {
            try {
                this.userTransaction.commit();
            } catch (Exception e) {
                LOGGER.error("Error al hacer commit de la transaccion");
            }

            LOGGER.error("Error en la aplicacion de cambios");
        }

    }

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------         proceso aplicar cambios     --------------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO ACC # 1.1 <br/><br/>
     *
     * Metodo asociado al paso 1 del proceso de aplicar cambios de conservacion. El paso 1 es
     * asincronico, asi que en este metodo solo se inicia el paso.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    public boolean enviarJobGeografico(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
            "INICIO_ENVIO_ASINCRONICO", 1);

        String idDocumentoReplicaGDB;
        String message;
        boolean answer;

        try {
            Long replicaFinalTipoDocId = ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId();
            Documento replicaFinal = this.generalesService.
                buscarDocumentoPorTramiteIdyTipoDocumento(tramite.getId(), replicaFinalTipoDocId);

            idDocumentoReplicaGDB = "";
            if (replicaFinal.getIdRepositorioDocumentos() != null &&
                replicaFinal.getIdRepositorioDocumentos().contains(
                    Constantes.PREFIJO_IDS_DOCUMENTOS_REPOSITORIO_DOCS)) {

                idDocumentoReplicaGDB = replicaFinal.getIdRepositorioDocumentos();

            } else if (replicaFinal.getArchivo() != null &&
                replicaFinal.getArchivo().contains(
                    Constantes.PREFIJO_IDS_DOCUMENTOS_REPOSITORIO_DOCS)) {

                idDocumentoReplicaGDB = replicaFinal.getArchivo();

            }

        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error obteniendo la copia geografica final del tramite";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 1);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 1);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "AplicarCambiosBean#enviarJobGeografico",
                "finalizarEdicionTramiteGeografico");
        }

        LOGGER.debug("Inicia Sincronización de datos con ArcSDE...");
        try {
            ProductoCatastralJob job = SigDAOv2.getInstance().aplicarCambiosAsync(
                this.productoCatastralJobDAO,
                tramite, idDocumentoReplicaGDB, usuario);
            message = Constantes.PREFIJO_JOB_ID + job.getId();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 1);
            answer = true;
        } catch (ExcepcionSNC ex) {
            message = ex.getMessage();
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 1);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 1);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "AplicarCambiosBean#enviarJobGeografico",
                "finalizarEdicionTramiteGeografico");
        }
        LOGGER.debug("Finaliza Sincronización de datos con ArcSDE.");

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "FIN_ENVIO_ASINCRONICO",
            1);

        return answer;
    }

    /**
     * PASO ACC # 1.2
     *
     * Metodo asociado al paso 1 del proceso de aplicar cambios de conservacion Se actualiza el
     * estado el paso, para confirmar que el job geografico fue exitoso. Se envia el resto del
     * proceso de aplicar cambios, ademas se maneja el proceso cuano se trata de un tramite de
     * Depuracion y finalmente se envia los jobs para eliminar y actualizar las versiones
     * geograficas del tramite.
     *
     *
     * @author felipe.cadena
     * @param registroTrabajoFinalizado
     * @return
     */
    @Override
    public boolean recibirJobGeografico(ProductoCatastralJob registroTrabajoFinalizado) {

        UsuarioDTO usuario = null;
        String loginUsuario;
        Long idTramite;
        boolean isDepuracion;
        boolean resultado = true;
        Tramite tramite = null;
        String message;

        loginUsuario = registroTrabajoFinalizado.getLoginUsuarioDeCodigo();
        idTramite = registroTrabajoFinalizado.getIdTramiteUsuarioDeCodigo();

        tramite = this.tramiteDao.findById(idTramite);
        usuario = this.generalesService.getCacheUsuario(loginUsuario);

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "FIN_ASINCRONICO", 1);

        //se valida si se trata de un tramite de depuración
        isDepuracion = this.determinarSiEsDepuracion(idTramite);
        if (isDepuracion) {
            try {
                this.aplicarCambiosDepuracion(tramite, usuario, 2);
            } catch (Exception ex) {
                LOGGER.error("Error al finalizar aplicar cambios depuracion.");
                resultado = false;
            }
        } else {
            try {
                //paso existoso
                message = "Se ejecuto con exito el paso 1";
                this.actualizarEstadoTramiteAC(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                    ETramiteTareaResultado.FIN.toString(), message, null, 1);
                this.tramiteDao.update(tramite);

                //se elimina la version geografica
                resultado = this.eliminarVersionGeoTramite(tramite, usuario);

                this.loggerAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "FIN", 1);

            } catch (Exception ex) {

                message = "Ocurrió un error al actualizar el estado del tramite";
                this.loggerAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 1);
                LOGGER.error(message, ex);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, ex, 1);
                LOGGER.error(ex.getMessage());
            }
        }

        this.aplicarCambiosConservacion(tramite, usuario, 2);
        return resultado;
    }

    /**
     * PASO ACC # 2 <br/><br/>
     *
     * Metodo asociado al paso 2 del proceso de aplicar cambios de conservacion. Aplica los cambios
     * alfanumericos asociados al tramite
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean aplicarCambiosAlfanumericos(Tramite tramite, UsuarioDTO usuario,
        EProcesosAsincronicos proceso) {

        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "INICIO", 2);

        if (tramite.isTramiteGeografico()) {
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.FIN.toString(), null, null, 1);
        }

        this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 2);

        String message;
        try {
            if (proceso.toString().equals(EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.
                toString())) {

                Object mensajes[] = this.sncProcedimientoDao.confirmarProyeccion(tramite.getId());
                if (Utilidades.hayErrorEnEjecucionSP(mensajes)) {
                    this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 2);
                    message =
                        "Ocurrió un Error al confirmar la proyección en la base de datos para el " +
                        "trámite con id " + tramite.getId().toString() + ": " + ErrorUtil.getError(
                        mensajes);
                    LOGGER.error(message);
                    //error paso
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, null, 2);
                    throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER);
                }
            } else if (proceso.toString().equals(
                EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION.toString())) {
                Object mensajes[] = this.sncProcedimientoDao.confirmarProyeccionActualizacion(
                    tramite.getId());
                if (Utilidades.hayErrorEnEjecucionSP(mensajes)) {
                    this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 2);
                    message =
                        "Ocurrió un Error al confirmar la proyección en la base de datos para el " +
                        "trámite con id " + tramite.getId().toString() + ": " + ErrorUtil.getError(
                        mensajes);
                    LOGGER.error(message);
                    //error paso
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, null, 2);
                    throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER);
                }
            }

            //paso existoso
            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 2);
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error al aplicar los cambios alfanumericos del tramite";
            this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 2);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 2);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#recibirJobGeografico",
                "finalizarEdicionTramiteGeografico");
        }
        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "FIN", 2);

        return true;
    }

    /**
     * PASO ACC # 3<br/><br/>
     *
     * Metodo asociado al paso 3 del proceso de aplicar cambios de conservacion. Finaliza el proceso
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean finalizarProcessAplicarCambios(Tramite tramite, UsuarioDTO usuario,
        EProcesosAsincronicos proceso) {

        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "INICIO_PROCESS", 3);
        this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 3);

        String message;
        ArrayList<Actividad> listaActividades = new ArrayList<Actividad>();
        String idActividad;

        String processTransition = "";
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuariosActividad = new ArrayList<UsuarioDTO>();
        usuariosActividad.add(usuario);
        processTransition = ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setUsuarios(usuariosActividad);

        Actividad actTramite = tramite.getActividadActualTramite();
        //felipe.cadena::Solo se consulta la actividad en process si esta no esta incluida en el tramite
        if (actTramite == null || actTramite.getId() == null) {
            try {
                listaActividades = (ArrayList<Actividad>) this.procesosService.
                    getActividadesPorIdObjetoNegocio(tramite.getId(),
                        ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);
            } catch (ExcepcionSNC ex) {
                message = "Ocurrió un error al consultar la lista de actividades para el trámite " +
                    "con id " + tramite.getId();
                this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 3);
                LOGGER.error(message, ex);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, ex, 3);

                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(LOGGER,
                    ex, "ConservacionBean#aplicarCambiosDeProyeccionS", "avanzarActividad");
            }
        } else {
            listaActividades.add(actTramite);
        }

        if (listaActividades != null && !listaActividades.isEmpty() &&
            listaActividades.get(0).getId() != null &&
            !listaActividades.get(0).getId().isEmpty()) {
            idActividad = listaActividades.get(0).getId();
            try {
                this.procesosService.avanzarActividad(usuario, idActividad, solicitudCatastral);
            } catch (Exception ex) {
                message =
                    "Ocurrió un error en el BPM al tratar de avanzar la actividad del trámite " +
                    "con id " + tramite.getId();
                this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 3);
                LOGGER.error(message, ex);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, ex, 3);

                throw new ExcepcionSNC("Error en el BPM", ex.getMessage(), message);
            }

        } else {
            message = "No se encontró la instancia de actividad correspondiente al trámite " +
                tramite.getId() + " en la actividad " +
                ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
            this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 3);
            LOGGER.error(message);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, null, 3);

            throw new ExcepcionSNC("No se encontró la actividad del trámite", "Error", message);
        }

        //paso existoso
        message = "paso existoso";
        this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
            ETramiteTareaResultado.FIN.toString(), message, null, 3);
        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "FIN", 3);

        return true;
    }

    /**
     * PASO ACC # 4<br/><br/>
     *
     * Metodo asociado al paso 4 del proceso de aplicar cambios de conservacion. Actualiza los
     * estados del tramite y la resolucion cuando se ha terminado la actividad de aplicar cambios.
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    /*
     * @modified by leidy.gonzalez Que se realizo en esta modificacion?????
     */
    @Override
    public boolean finalizarEstadosAlfanumericos(Tramite tramite, UsuarioDTO usuario,
        EProcesosAsincronicos proceso) {

        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "INICIO_ACTUALIZAR_ESTADOS",
            4);
        this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 4);
        String message;
        Documento docResp = null;

        try {
            //D: ACTUALIZAR EL TRAMITE Y LA RESOLUCION

            //se elimina la version geografica
            this.eliminarVersionGeoTramite(tramite, usuario);

            // Cuando el trámite es de quinta actualizar el predio del trámite
            if (tramite.isQuinta() || tramite.isQuintaNuevo() || tramite.isQuintaOmitido() ||
                tramite.isQuintaOmitidoNuevo()) {

                List<PPredio> prediosDelTramite = this.conservacionService
                    .buscarPPrediosPorTramiteId(tramite.getId());
                if (prediosDelTramite != null && !prediosDelTramite.isEmpty()) {
                    Predio predioSeleccionado = this.conservacionService.
                        obtenerPredioPorId(prediosDelTramite.get(0).getId());
                    if (predioSeleccionado != null) {
                        tramite.setPredio(predioSeleccionado);
                        tramite.setPredioId(predioSeleccionado.getId());
                    }
                }
            }

            //Se actualiza el estado final del tramite
            try {

                TramiteEstado tramiteEstado =
                    this.tramiteService.actualizarEstadoTramite(tramite,
                        ETramiteEstado.FINALIZADO_APROBADO, usuario);
                LOGGER.info("Tramite estado Actualizado" + tramiteEstado);
                tramite.setTramiteEstado(tramiteEstado);
                LOGGER.info("Estado Tramite" + tramite.getEstado());

                tramite.setUsuarioLog(usuario.getLogin());
                tramite.setFechaLog(new Date());

                if (tramite.getResultadoDocumento() != null) {

                    tramite.getResultadoDocumento().setEstado(EDocumentoEstado.RESOLUCION_EN_FIRME.
                        getCodigo());

                    docResp = this.tramiteService.guardarYactualizarDocumento(tramite.
                        getResultadoDocumento());

                    if (docResp == null) {
                        message =
                            "Error en la actualización del documento resultado del trámite con id " +
                            tramite.getId().toString();
                        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 4);
                        LOGGER.error(message);
                        //error paso
                        this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                            ETramiteTareaResultado.ERROR.toString(), message, null, 4);

                        throw new ExcepcionSNC("Error en Documento", "Error", message);
                    }
                }

                tramite = this.tramiteService.guardarActualizarTramite2(tramite);
            } catch (Exception ex) {
                message = "Error en la actualización del estado del tramite " +
                    tramite.getId().toString();
                this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 4);
                LOGGER.error(message, ex);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, null, 4);

                throw new ExcepcionSNC("Error en la actualización del estado del tramite", "Error",
                    message);
            }

            //paso exitoso
            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 4);
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error al actualizar el estado del tramite";
            this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 4);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 4);

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_INTERFAZ_INTEGRACION.getExcepcion(
                LOGGER, ex, "ConservacionBean#finalizarEstadosAlfanumericos",
                "finalizarEstadosAlfanumericos");
        }
        this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "FIN_ACTUALIZAR_ESTADOS", 4);

        return true;
    }

    /**
     * PASO ACC # 5.1 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se invoca el proceso
     * asincronico para la generacion de la ficha predial digital. la parte final de este paso se
     * ejecuta en : IConservacionLocal#procesarFichaPredialDigital <br/><br/>
     *
     * Nota: Los pasos 5 y 6 de aplicar cambios se envian de forma simultanea sin orden especifico
     * de finalizacion
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean generarFichaPredial(Tramite tramite, UsuarioDTO usuario) {
        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
            "INICIO_ENVIO_ASINCRONICO", 5);
        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 5);

        //Se hace el llamado para actualizar la ficha predial digital
        boolean fichaOK = false;
        String message;

        try {
            fichaOK = this.conservacionService.actualizarFichaPredialDigital(tramite.getId(),
                usuario, true);
            if (!fichaOK) {
                message =
                    "Ocurrió un error en la generación o actualización de la ficha predial digital" +
                    " del trámite con id " + tramite.getId().toString();
                this.loggerAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 5);
                LOGGER.error(message);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, null, 5);
            }
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error al enviar el job de generacion de FPD";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 5);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 5);

        }
        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "FIN_ENVIO_ASINCRONICO",
            5);
        return true;
    }

    /**
     * PASO ACC # 5.2 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se finaliza la carta
     * de la ficha predial digital <br/><br/>
     *
     *
     *
     * @author felipe.cadena
     *
     * @return
     */
    @Override
    public boolean finalizarFichaPredial(ProductoCatastralJob job) {

        boolean resultado = true;

        try {
            this.userTransaction.begin();
//        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "INICIO_ENVIO_ASINCRONICO", 5);
//        this.actualizarEstadoTramiteAC(tramite, usuario, EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
//            ETramiteTareaResultado.INICIO.toString(), null, null, 5);
            this.userTransaction.commit();

            this.userTransaction.begin();

            String answer = "";
            int contador = 0;

            answer = this.conservacionService.procesarFichaPredialDigital(job, contador);

            resultado = answer.equals(ProductoCatastralJob.SNC_TERMINADO.toString());
            this.userTransaction.commit();
        } catch (Exception e) {
            return false;
        }

        return resultado;
    }

    /**
     * PASO ACC # 6.1 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se invoca el proceso
     * asincronico para la generacion de la carta catastral. la parte final de este paso se ejecuta
     * en : IConservacionLocal#procesarCartaCatastralUrbana
     *
     * <br/><br/>
     *
     * Nota: Los pasos 5 y 6 de aplicar cambios se envian de forma simultanea sin orden especifico
     * de finalizacion
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean generarCartaCatastral(Tramite tramite, UsuarioDTO usuario) {
        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
            "INICIO_ENVIO_ASINCRONICO", 6);

        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 6);

        //Se hace el llamado para actualizar la ficha predial digital
        boolean cartaOK = false;
        String message;
        //Se hace el llamado para actualizar la carta catastral urbana 
        try {
            cartaOK = this.conservacionService.actualizarCartaCatastralUrbana(tramite.getId(),
                usuario, true);
            if (!cartaOK) {
                message =
                    "Ocurrió un error en la generación o actualización de la carta catastral urbana" +
                    " del trámite con id " + tramite.getId().toString();
                this.loggerAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 6);
                LOGGER.error(message);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, null, 6);

                throw new ExcepcionSNC("Error en la carta catastral urbana", "Error", message);
            }

        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error al enviar el job de generacion de CCU";
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 6);
            LOGGER.error(message, ex);
            //error paso
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 5);
        }

        this.loggerAplicarCambios(tramite, usuario,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "FIN_ENVIO_ASINCRONICO",
            6);
        return true;
    }

    /**
     * PASO ACC # 6.2 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se finaliza la carta
     * de la carta catastral urbana <br/><br/>
     *
     *
     *
     * @author felipe.cadena
     * @param job
     * @return
     */
    @Override
    public boolean finalizarCartaCatastral(ProductoCatastralJob job) {
        boolean resultado = true;

        try {
            this.userTransaction.begin();

//        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "INICIO_ENVIO_ASINCRONICO", 5);
//        this.actualizarEstadoTramiteAC(tramite, usuario, EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
//            ETramiteTareaResultado.INICIO.toString(), null, null, 5);
            this.userTransaction.commit();

            this.userTransaction.begin();

            String answer = "";
            int contador = 0;

            answer = this.conservacionService.procesarCartaCatastralUrbana(job, contador);

            resultado = answer.equals(ProductoCatastralJob.SNC_TERMINADO.toString());

            this.userTransaction.commit();
        } catch (Exception e) {
            return false;
        }
        return resultado;
    }

    /**
     *
     * ACC
     *
     * Metodo para invocar el aplicar cambios desde un paso determinado ACC
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez::12/11/2015:: Se modifica tipo de retorno del metodo.
     */
    @Override
    public boolean aplicarCambiosConservacion(Tramite tramite, UsuarioDTO usuario, int paso) {
        LOGGER.debug("on AplicacionCambiosBean#aplicarCambiosConservacion inicio seleccion paso: " +
            paso);
        boolean validaResultado = true;
        try {
            if (paso == 0) {
                this.userTransaction.begin();
                validaResultado = this.enviarJobGeografico(tramite, usuario);
                this.userTransaction.commit();
            } else if (paso == 1) {
                //solo se llama desde la parte asincronica      
                this.userTransaction.begin();
                validaResultado = this.aplicarCambiosAlfanumericos(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION);
                this.userTransaction.commit();
                validaResultado = this.aplicarCambiosConservacion(tramite, usuario, 2);
            } else if (paso == 2) {
                this.userTransaction.begin();
                validaResultado = this.finalizarProcessAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION);
                this.userTransaction.commit();
                validaResultado = this.aplicarCambiosConservacion(tramite, usuario, 3);
            } else if (paso == 3) {
                this.userTransaction.begin();
                validaResultado = this.finalizarEstadosAlfanumericos(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION);
                this.userTransaction.commit();
                validaResultado = this.aplicarCambiosConservacion(tramite, usuario, 4);
            } else if (paso == 4) {
                this.userTransaction.begin();
                this.generarFichaPredial(tramite, usuario);
                this.userTransaction.commit();
                this.userTransaction.begin();
                this.generarCartaCatastral(tramite, usuario);
                this.userTransaction.commit();
                this.userTransaction.begin();
                this.actualizarColindanciaPredios(tramite, usuario);
                this.userTransaction.commit();
            }

            return validaResultado;
        } catch (Exception ex) {
            //ErrorUtil.notificarErrorPorCorreoElectronico(ex, usuario.getLogin(), tramite.getId(), message);
            try {
                this.userTransaction.commit();
                return false;
            } catch (Exception e) {
                LOGGER.error("Error en la aplicación de cambios");
                return false;
            }
        }
    }

    /**
     * Metodo para iniciar la aplicacion de cambios
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param usuario
     */
    @Override
    public void aplicarCambiosInicio(Long tramiteId, UsuarioDTO usuario) {
        Tramite tramite = null;
        String message;

        try {
            //se marca el tramite como en aplicar cambios
            tramite = this.tramiteDao.buscarTramiteConResolucionSimple(tramiteId);
            tramite.setEstadoGdb(Constantes.ACT_APLICAR_CAMBIOS);
            this.tramiteDao.update(tramite);
        } catch (Exception ex) {
            message = "Error en ConservacionBean#aplicarCambiosInicio buscando el " +
                "trámite con id " + tramiteId.toString();
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 5);
            LOGGER.error(message, ex);
            throw new ExcepcionSNC("aplicarCambiosInicio", "Error", message);
        }

        //Se transfiere la actividad al usuario tiempos muertos
        UsuarioDTO usuarioTiempoMuertos = this.generalesService.getCacheUsuario(
            ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());
        this.procesosService.transferirActividadConservacion(this.obtenerActividadActualTramite(
            tramiteId).getId(), usuarioTiempoMuertos);

        //inicia la aplicacion de cambios
        if (tramite.isTramiteGeografico()) {
            this.aplicarCambiosConservacion(tramite, usuario, 0);
        } else {
            this.aplicarCambiosConservacion(tramite, usuario, 1);
        }

    }

    /**
     * Metodo para eliminar la version de la BD Geografica
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    private boolean eliminarVersionGeoTramite(Tramite tramite, UsuarioDTO usuario) {
        //D: se borra la "versión" del trámite en el servidor arcgis
        if (tramite != null) {
            try {
                SigDAOv2.getInstance().enviarJobEliminarVersion(
                    this.productoCatastralJobDAO, tramite.getMunicipio().getCodigo(), tramite,
                    usuario);

            } catch (ExcepcionSNC sncEx) {
                this.loggerAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(), "ERROR", 3);
                LOGGER.error(
                    "Ocurrió un error en el borrado de la versión del trámite en el ARCGIS sever: " +
                    sncEx.getMensaje());
                return false;
            }
            return true;
        }
        return false;
    }

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------   proceso generar replica de consulta  -----------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO GCC # 1 <br/><br/>
     *
     * Metodo para mover la actividad al proceso de depuración
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param responsableSIG
     * @param usuario
     */
    @Override
    public boolean avanzarTramiteADepuracion(Tramite tramite, UsuarioDTO responsableSIG,
        UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "INICIO", 1);
        String idActividad;

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        String message = null;

        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
            ETramiteTareaResultado.INICIO.toString(), message, null, 1);

        try {
            Actividad act = this.obtenerActividadActualTramite(tramite.getId());
            idActividad = act.getId();

            usuarios.add(responsableSIG);
            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral.setTransicion(
                ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL);
            this.procesosService.avanzarActividad(usuario, idActividad, solicitudCatastral);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 1);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "FIN", 1);
            return true;
        } catch (Exception ex) {
            message = "Error en ConservacionBean#avanzarTramiteADepuracion avanzando el " +
                "trámite con id " + tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 1);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", 1);
            LOGGER.error(message, ex);
            return false;
        }
    }

    /**
     * PASO GCC # 2 <br/><br/>
     *
     * Metodo para mover transferir la actividad al usuario tiempos muertos mientras se termina la
     * creación de la replica.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    public boolean transferirTramiteATiemposMuertosGCC(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "INICIO", 2);

        String message = null;
        try {

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 2);
            //Se transfiere la actividad al usuario tiempos muertos
            UsuarioDTO usuarioTiemposMuertos = this.generalesService.
                getCacheUsuario(ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());
            this.transferirTramite(tramite, usuarioTiemposMuertos);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 2);

        } catch (Exception ex) {
            message = "Error en ConservacionBean#transferirTramieATiemposMuertos transfiriendo " +
                "trámite con id " + tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 2);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", 2);
            LOGGER.error(message, ex);
            return false;
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "FIN", 2);
        return true;
    }

    /**
     * PASO GCC # 3.1 <br/><br/>
     *
     * Metodo asociado al envio del job para generar la replica de consulta relacionada a un tramite
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario -- es el usuario que reclamara la actividad al final de la geneacion de la
     * replica
     * @modified andres.eslava::09/Jun/2015::Modifica validacion Etapas para generar lista de
     * predios
     */
    @Override
    public void enviarJobGenerarReplicaConsulta(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "INICIO_ENVIO_ASINCRONICO", 3);

        StringBuilder prediosSb = new StringBuilder("");
        String message = null;

        try {
            List<Predio> predios = tramite.getPredios();
            // Validacion Condiciones especiales para adicion de predios
            if ((tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz()) &&
                EPredioCondicionPropiedad.CP_9.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad())) {
                prediosSb.append(tramite.getPredio().getNumeroPredial());
            } else if (tramite.isQuinta()) {
                prediosSb.append(tramite.getNumeroPredial());
            } else {
                if (tramite.getPredio() != null) {
                    prediosSb.append(tramite.getPredio().getNumeroPredial()).append(",");
                }

                for (Predio predio : predios) {
                    if (!((EPredioCondicionPropiedad.CP_5.getCodigo().equals(predio.
                        getCondicionPropiedad()) ||
                        EPredioCondicionPropiedad.CP_6.getCodigo().equals(predio.
                            getCondicionPropiedad())))) {
                        if (tramite.getPredio() != null) {
                            if (predio.getId().equals(tramite.getPredio().getId())) {
                                continue;
                            }
                        }
                        prediosSb.append(predio.getNumeroPredial()).append(",");
                    }
                }
            }
        } catch (Exception ex) {
            message =
                "Error preparando los predios para la generacion de la replica de consulta para el tramite " +
                tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 3);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", 3);
            LOGGER.error(message, ex);
            return;
        }

        try {
            ProductoCatastralJob job = SigDAOv2.getInstance().exportarDatosAsync(
                this.productoCatastralJobDAO,
                tramite,
                prediosSb.toString(),
                tramite.getTipoTramite(),
                ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_FILEGEODB,
                usuario);
            message = Constantes.PREFIJO_JOB_ID + job.getId();

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 3);

        } catch (ExcepcionSNC ex) {
            message = "Error en el envio del job para la replica de consulta del tramite " +
                tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 3);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", 3);
            LOGGER.error(message, ex);
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "FIN_ENVIO_ASINCRONICO", 3);

    }

    /**
     * PASO GCC # 4 <br/><br/>
     *
     * Guarda el registro en la db relacionado al archivo de la replica.
     *
     * @author felipe.cadena
     *
     * @param jobReplica
     */
    @Override
    public Object[] finalizarGenerarReplicaConsulta(ProductoCatastralJob jobReplica) {

        Long tramiteId;
        String usuariologin;
        String nombreReplica;
        String message;

        // recupero los datos del job
        Map<String, String> paramsCodigoJob = Utilidades.obtenerParametrosXMLJob(jobReplica.
            getCodigo(), "/SigJob/parameters/parameter");
        List<String> rutaReplica = Utilidades.obtenerAtributoXML(jobReplica.getResultado(),
            "/Response/Results/File", "documentServerId");

        //Si el no existe la ruta de la eplica.
        if (rutaReplica == null || rutaReplica.isEmpty()) {
            jobReplica.setEstado(ProductoCatastralJob.SNC_ERROR);
            return null;
        }

        tramiteId = Long.valueOf(paramsCodigoJob.get("numeroTramite"));
        usuariologin = paramsCodigoJob.get("usuario");
        nombreReplica = rutaReplica.get(0);

        UsuarioDTO usuario = this.generalesService.getCacheUsuario(usuariologin);
        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(tramiteId);

        //Si el tramite no existe se actualiza el job como error y se termina el metodo.
        if (tramite == null) {
            jobReplica.setEstado(ProductoCatastralJob.SNC_ERROR);
            return null;
        }

        message = "paso exitoso - fin proceso asincronico";
        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
            ETramiteTareaResultado.FIN.toString(), message, null, 3);

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "INICIO_RECIBO_ASINCRONICO", 4);

        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 4);

        try {
            List<TramiteDocumento> listTramitesDocumentos = new ArrayList<TramiteDocumento>();
            TramiteDocumento tramiteDocumento = new TramiteDocumento();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            tipoD.setId(ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());

            doc.setArchivo(nombreReplica);

            doc.setIdRepositorioDocumentos(nombreReplica);
            doc.setTipoDocumento(tipoD);
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());
            doc.setUsuarioCreador(usuario.getLogin());
            doc.setUsuarioLog(usuario.getLogin());
            doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
            doc.setBloqueado(ESiNo.NO.getCodigo());
            doc.setTramiteId(tramite.getId());
            doc = this.generalesService.guardarDocumento(doc);

            tramiteDocumento.setTramite(tramite);
            tramiteDocumento.setUsuarioLog(usuario.getLogin());
            tramiteDocumento.setFechaLog(new Date());
            tramiteDocumento.setfecha(new Date());

            tramiteDocumento.setDocumento(doc);
            tramiteDocumento = this.tramiteService.actualizarTramiteDocumento(tramiteDocumento);
            listTramitesDocumentos.add(tramiteDocumento);
            tramite.setTramiteDocumentos(listTramitesDocumentos);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 4);

        } catch (ExcepcionSNC ex) {
            message = "Error al crear el registro en DB para la replica de consulta del tramite " +
                tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 4);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", 4);
            LOGGER.error(message, ex);
            return null;
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "FIN_RECIBO_ASINCRONICO", 4);

        Object[] datos = new Object[2];
        datos[0] = usuario;
        datos[1] = tramite;

        return datos;

    }

    /**
     * PASO GCC # 5 <br/><br/>
     *
     * Metodo para transferir la actividad desde tiempos muertos a un usuario determinado.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    public void transferirTramiteDeTiemposMuertosAUsuarioGCC(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "INICIO", 5);

        String message = null;
        try {

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 5);

            this.transferirTramite(tramite, usuario);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 5);
        } catch (Exception ex) {
            message =
                "Error en ConservacionBean#transferirTramiteDeTiemposMuertosAUsuario transfiriendo " +
                "trámite con id " + tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 5);
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", 5);
            LOGGER.error(message, ex);
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
            toString(), "FIN", 5);
    }

    /**
     *
     * GCC Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param usuarioResponsableSIG -- Solo se usa para el envio a depuracion(paso 0), puede ser
     * null en los demas casos
     * @param paso
     * @param jobGeografico
     */
    @Override
    public void generarReplicaConsulta(Tramite tramite, UsuarioDTO usuario,
        UsuarioDTO usuarioResponsableSIG, int paso, ProductoCatastralJob jobGeografico) {

        boolean result = true;
        try {
            if (paso == 0) {
                boolean tieneCopia = this.validarExistenciaCopiaConsulta(tramite);
                if (tieneCopia) {
                    this.userTransaction.begin();
                    this.avanzarTramiteADepuracion(tramite, usuarioResponsableSIG, usuario);
                    this.userTransaction.commit();
                } else {
                    this.generarReplicaConsulta(tramite, usuarioResponsableSIG, null, 1, null);
                }

            } else if (paso == 1) {
                boolean tieneCopia = this.validarExistenciaCopiaConsulta(tramite);
                if (tieneCopia) {
                    return;
                }
                this.userTransaction.begin();
                result = this.transferirTramiteATiemposMuertosGCC(tramite, usuario);
                this.userTransaction.commit();
                if (result) {
                    this.generarReplicaConsulta(tramite, usuario, null, 2, null);
                }
            } else if (paso == 2) {
                this.userTransaction.begin();
                this.enviarJobGenerarReplicaConsulta(tramite, usuario);
                this.userTransaction.commit();
            } else if (paso == 3) {
                //Solo se puede invocar desde el recibo de proceso asincronico
                this.userTransaction.begin();
                Object[] datos = this.finalizarGenerarReplicaConsulta(jobGeografico);
                this.userTransaction.commit();
                usuario = (UsuarioDTO) datos[0];
                tramite = (Tramite) datos[1];
                List<TramiteDepuracion> tds = this.tramiteDepuracionDAO.buscarPorIdTramite(tramite.
                    getId());

                //Se valida que el tramite acaba de iniciar el proceso de depuracion, de lo contrario se saca de tiempos muertos
                if (tds == null || tds.isEmpty()) {
                    this.generarReplicaConsulta(tramite, usuario, null, 4, null);
                } else {
                    TramiteDepuracion ultimoTd = tds.get(0);

                    if (ultimoTd.getViable() == null || ultimoTd.getViable().isEmpty()) {
                        this.generarReplicaConsulta(tramite, usuario, this.obtenerResponsableSIG(
                            usuario), 0, null);
                    } else {
                        this.generarReplicaConsulta(tramite, usuario, null, 4, null);
                    }
                }
            } else if (paso == 4) {
                this.userTransaction.begin();
                this.transferirTramiteDeTiemposMuertosAUsuarioGCC(tramite, usuario);
                this.userTransaction.commit();
            }

        } catch (Exception ex) {

            try {
                this.userTransaction.commit();
            } catch (Exception e) {
                LOGGER.error("Error en el commit de la transaccion");
            }
            LOGGER.error("Error en el poceso de generacion de replica de consulta");
        }

    }

    /**
     * GCE
     *
     * Metodo para invocar la generacion de la replica de edicion desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    @Override
    public void generarReplicaEdicion(Tramite tramite, UsuarioDTO usuario, int paso,
        ProductoCatastralJob jobGeografico) {

        boolean result = true;
        try {
            if (paso == 0) {
                this.userTransaction.begin();

                //17793 :: javier.aponte :: Esto se hace para solucionar error que se queda generando
                //replicas 2000 y no genera la 97, y es debido a que en algunos casos el campo resultado
                //quedo en Error.
                if (ETramiteTareaResultado.ERROR.toString().equals(tramite.getResultado())) {
                    tramite.setResultado(null);
                }

                result = this.avanzarTramiteAModificacionGeografica(tramite, usuario);
                this.userTransaction.commit();
                if (result) {
                    this.generarReplicaEdicion(tramite, usuario, 1, null);
                }

            } else if (paso == 1) {
                this.userTransaction.begin();
                result = this.transferirTramiteATiemposMuertosGCE(tramite, usuario);
                this.userTransaction.commit();
                if (result) {
                    this.generarReplicaEdicion(tramite, usuario, 2, null);
                }
            } else if (paso == 2) {
                this.userTransaction.begin();
                this.enviarJobGenerarReplicaEdicion(tramite, usuario);
                this.userTransaction.commit();
            } else if (paso == 3) {
                //Solo se puede invocar desde el recibo de proceso asincronico
                this.userTransaction.begin();
                Object[] datos = this.finalizarGenerarReplicaEdicion(jobGeografico);
                this.userTransaction.commit();
                usuario = (UsuarioDTO) datos[0];
                tramite = (Tramite) datos[1];
                this.generarReplicaEdicion(tramite, usuario, 4, null);
            } else if (paso == 4) {
                this.userTransaction.begin();
                this.transferirTramiteDeTiemposMuertosAUsuarioGCE(tramite, usuario);
                this.userTransaction.commit();
            }

        } catch (Exception ex) {
            try {
                this.userTransaction.commit();
            } catch (Exception e) {
                LOGGER.error("Error en el commit de la transaccion");
            }
            LOGGER.error("Error en el poceso de generacion de replica de edicion");

        }

    }

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------   proceso generar replica de edicion  -----------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO GCE # 1 <br/><br/>
     *
     * Metodo para mover la actividad a la actividad de modificacion geografica
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean avanzarTramiteAModificacionGeografica(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "INICIO", 1);
        String idActividad;

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        String message = null;

        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
            ETramiteTareaResultado.INICIO.toString(), message, null, 1);

        try {
            Actividad act = this.obtenerActividadActualTramite(tramite.getId());
            idActividad = act.getId();
            String actividadActual = act.getNombre();

            solicitudCatastral.setUsuarios(usuarios);

            if (actividadActual.equals(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR);

            } else if (actividadActual.equals(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION);

            } else if (actividadActual.equals(
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_TOPOGRAFIA)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA);

            } else if (actividadActual.equals(
                ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA)) {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA);

            } else {

                solicitudCatastral.setTransicion(
                    ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA);

            }

            usuarios.add(usuario);
            solicitudCatastral.setUsuarios(usuarios);
            this.procesosService.avanzarActividad(usuario, idActividad, solicitudCatastral);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 1);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "FIN", 1);
            return true;
        } catch (Exception ex) {
            message =
                "Error en AplicarCambios#avanzarTramiteAModificacionGeografica avanzando el " +
                "trámite con id " + tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 1);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "ERROR", 1);
            LOGGER.error(message);
            return false;
        }
    }

    /**
     * PASO GCE # 2 <br/><br/>
     *
     * Metodo para mover transferir la actividad al usuario tiempos muertos mientras se termina la
     * creación de la replica.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    public boolean transferirTramiteATiemposMuertosGCE(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "INICIO", 2);

        String message = null;
        try {

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 2);
            //Se transfiere la actividad al usuario tiempos muertos
            UsuarioDTO usuarioTiemposMuertos = this.generalesService.
                getCacheUsuario(ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());
            this.transferirTramite(tramite, usuarioTiemposMuertos);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 2);
        } catch (Exception ex) {
            message = "Error en ConservacionBean#transferirTramieATiemposMuertos transfiriendo " +
                "trámite con id " + tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 2);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "ERROR", 2);
            LOGGER.error(message, ex);
            return false;
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "FIN", 2);
        return true;
    }

    /**
     * PASO GCE # 3.1 <br/><br/>
     *
     * Metodo asociado al envio del job para generar la replica de edicion relacionada a un tramite
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario -- es el usuario que reclamara la actividad al final de la generacion de la
     * replica
     */
    /*
     * @modified andres.eslava::09/Jun/2015::Modifica validacion Etapas para generar lista de
     * predios
     */
 /*
     * @modified leidy.gonzalez::19/07/2016::Modifica validacion quinta masivo para generar lista de
     * predios
     */
    @Override
    public void enviarJobGenerarReplicaEdicion(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "INICIO_ENVIO_ASINCRONICO", 3);

        StringBuilder prediosSb = new StringBuilder("");
        String message = null;

        try {
            List<Predio> predios = tramite.getPredios();
            if ((tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz() || tramite.
                isQuintaMasivo()) &&
                EPredioCondicionPropiedad.CP_9.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad())) {
                prediosSb.append(tramite.getPredio().getNumeroPredial());

            } else if (tramite.isQuintaMasivo() &&
                EPredioCondicionPropiedad.CP_8.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad())) {

                for (Predio predio : predios) {
                    if (tramite.getPredio() != null) {
                        if (predio.getId().equals(tramite.getPredio().getId())) {
                            continue;
                        }
                    }
                    prediosSb.append(predio.getNumeroPredial()).append(",");

                }

            } else if (tramite.isQuinta()) {
                //felipe.cadena::01-06-2016::#15836::Se envia el predio proyectado para
                //la creacion de la replica, si este no existe se envia el numero que se radico para casos de depuracion
                PPredio predioProyectado = this.pPredioDao.getPPredioByTramiteId(tramite.getId());
                if (predioProyectado != null) {
                    prediosSb.append(predioProyectado.getNumeroPredial());
                } else {
                    prediosSb.append(tramite.getNumeroPredial());
                }
            } else {
                if (tramite.getPredio() != null) {
                    prediosSb.append(tramite.getPredio().getNumeroPredial()).append(",");
                }

                for (Predio predio : predios) {
                    if (!((EPredioCondicionPropiedad.CP_5.getCodigo().equals(predio.
                        getCondicionPropiedad()) ||
                        EPredioCondicionPropiedad.CP_6.getCodigo().equals(predio.
                            getCondicionPropiedad())))) {
                        if (tramite.getPredio() != null) {
                            if (predio.getId().equals(tramite.getPredio().getId())) {
                                continue;
                            }
                        }
                        prediosSb.append(predio.getNumeroPredial()).append(",");
                    }
                }
            }
        } catch (Exception ex) {
            message =
                "Error preparando los predios para la generacion de la replica de consulta para el tramite " +
                tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 3);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "ERROR", 3);
            LOGGER.error(message, ex);
            return;
        }

        try {

            ProductoCatastralJob job = SigDAOv2.getInstance().exportarDatosAsync(
                this.productoCatastralJobDAO,
                tramite,
                prediosSb.toString(),
                tramite.getTipoTramite(), ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_FILEGEODB,
                usuario);
            message = Constantes.PREFIJO_JOB_ID + job.getId();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 3);

        } catch (ExcepcionSNC ex) {
            message = "Error en el envio del job para la replica de consulta del tramite " +
                tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 3);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "ERROR", 3);
            LOGGER.error(message, ex);
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "FIN_ENVIO_ASINCRONICO", 3);

    }

    /**
     * PASO GCE # 4 <br/><br/>
     *
     * Guarda el registro en la db relacionado al archivo de la replica.
     *
     * @author felipe.cadena
     *
     * @param jobReplica
     * @return
     */
    @Override
    public Object[] finalizarGenerarReplicaEdicion(ProductoCatastralJob jobReplica) {

        Long tramiteId;
        String usuariologin;
        String nombreReplica;
        String message;

        // recupero los datos del job
        Map<String, String> paramsCodigoJob = Utilidades.obtenerParametrosXMLJob(jobReplica.
            getCodigo(), "/SigJob/parameters/parameter");
        List<String> rutaReplica = Utilidades.obtenerAtributoXML(jobReplica.getResultado(),
            "/Response/Results/File", "documentServerId");

        //Si el no existe la ruta de la eplica.
        if (rutaReplica == null || rutaReplica.isEmpty()) {
            jobReplica.setEstado(ProductoCatastralJob.SNC_ERROR);
            return null;
        }

        tramiteId = Long.valueOf(paramsCodigoJob.get("numeroTramite"));
        usuariologin = paramsCodigoJob.get("usuario");
        nombreReplica = rutaReplica.get(0);

        UsuarioDTO usuario = this.generalesService.getCacheUsuario(usuariologin);
        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(tramiteId);

        //Si el tramite no existe se actualiza el job como error y se termina el metodo.
        if (tramite == null) {
            jobReplica.setEstado(ProductoCatastralJob.SNC_ERROR);
            return null;
        }

        message = "paso exitoso - fin proceso asincronico";
        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
            ETramiteTareaResultado.FIN.toString(), message, null, 3);

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "INICIO_RECIBO_ASINCRONICO", 4);

        this.actualizarEstadoTramiteAC(tramite, usuario,
            EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
            ETramiteTareaResultado.INICIO.toString(), null, null, 4);

        try {
            List<TramiteDocumento> listTramitesDocumentos = new ArrayList<TramiteDocumento>();
            TramiteDocumento tramiteDocumento = new TramiteDocumento();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            tipoD.setId(ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());

            doc.setArchivo(nombreReplica);

            doc.setIdRepositorioDocumentos(nombreReplica);
            doc.setTipoDocumento(tipoD);
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());
            doc.setUsuarioCreador(usuario.getLogin());
            doc.setUsuarioLog(usuario.getLogin());
            doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
            doc.setBloqueado(ESiNo.NO.getCodigo());
            doc.setTramiteId(tramite.getId());
            doc = this.generalesService.guardarDocumento(doc);

            tramiteDocumento.setTramite(tramite);
            tramiteDocumento.setUsuarioLog(usuario.getLogin());
            tramiteDocumento.setFechaLog(new Date());
            tramiteDocumento.setfecha(new Date());

            tramiteDocumento.setDocumento(doc);
            tramiteDocumento = this.tramiteService.actualizarTramiteDocumento(tramiteDocumento);
            listTramitesDocumentos.add(tramiteDocumento);
            tramite.setTramiteDocumentos(listTramitesDocumentos);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 4);

        } catch (ExcepcionSNC ex) {
            message = "Error al crear el registro en DB para la replica de edicion del tramite " +
                tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 4);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "ERROR", 4);
            LOGGER.error(message, ex);
            return null;
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "FIN_RECIBO_ASINCRONICO", 4);

        Object[] datos = new Object[2];
        datos[0] = usuario;
        datos[1] = tramite;

        return datos;

    }

    /**
     * PASO GCE # 5 <br/><br/>
     *
     * Metodo para transferir la actividad desde tiempos muertos a un usuario determinado.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    @Override
    public void transferirTramiteDeTiemposMuertosAUsuarioGCE(Tramite tramite, UsuarioDTO usuario) {

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "INICIO", 5);

        String message = null;
        try {

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.INICIO.toString(), message, null, 5);

            this.transferirTramite(tramite, usuario);

            message = "paso existoso";
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.FIN.toString(), message, null, 5);
        } catch (Exception ex) {
            message =
                "Error en ConservacionBean#transferirTramiteDeTiemposMuertosAUsuario transfiriendo " +
                "trámite con id " + tramite.getId().toString();
            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString(),
                ETramiteTareaResultado.ERROR.toString(), message, ex, 5);
            this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
                toString(), "ERROR", 5);
            LOGGER.error(message, ex);
        }

        this.loggerAplicarCambios(tramite, usuario, EProcesosAsincronicos.GENERAR_COPIA_EDICION.
            toString(), "FIN", 5);
    }

    /**
     * @see
     * IConservacionLocal#validarExistenciaCopiaConsulta(co.gov.igac.snc.persistence.entity.tramite.Tramite)
     * @author felipe.cadena
     *
     */
    @Override
    public boolean validarExistenciaCopiaConsulta(Tramite tramite) {

        boolean tieneCopia = false;
        Long replicaOriginalId = ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId();
        List<TramiteDocumento> td = tramite.getTramiteDocumentos();
        if (td == null) {
            return tieneCopia;
        }
        if (td.isEmpty()) {
            return tieneCopia;
        }

        for (TramiteDocumento tdTemp : td) {
            if (tdTemp.getDocumento().getTipoDocumento().getId().equals(replicaOriginalId)) {
                tieneCopia = true;
            }
        }
        return tieneCopia;
    }

    /**
     * @see
     * IConservacionLocal#validarExistenciaCopiaConsulta(co.gov.igac.snc.persistence.entity.tramite.Tramite)
     * @author felipe.cadena
     *
     */
    @Override
    public boolean validarExistenciaCopiaEdicion(Tramite tramite) {

        boolean tieneCopia = false;
        Long replicaOriginalId = ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId();
        List<TramiteDocumento> td = tramite.getTramiteDocumentos();
        if (td == null) {
            return tieneCopia;
        }
        if (td.isEmpty()) {
            return tieneCopia;
        }

        for (TramiteDocumento tdTemp : td) {
            if (tdTemp.getDocumento().getTipoDocumento().getId().equals(replicaOriginalId)) {
                tieneCopia = true;
            }
        }
        return tieneCopia;
    }

    /**
     * @see IConservacionLocal#generarReplicaDeConsultaDepuracion(Actividad ,UsuarioDTO,
     * responsableSIG);
     * @author felipe.cadena
     */
    @Override
    public void generarReplicaDeConsultaDepuracion(Long tramiteId, UsuarioDTO usuario,
        UsuarioDTO responsableSIG) {

        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(tramiteId);

        try {
            boolean tieneCopia = this.validarExistenciaCopiaConsulta(tramite);

            //paso 1
            this.avanzarTramiteADepuracion(tramite, responsableSIG, usuario);
            if (!tieneCopia) {
                //paso 2
                this.transferirTramiteATiemposMuertosGCC(tramite, usuario);
                //paso 3
                this.enviarJobGenerarReplicaConsulta(tramite, responsableSIG);
            }

        } catch (Exception e) {
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", -1);
        }

    }

    /**
     * @see IConservacionLocal#generarReplicaDeConsulta(Actividad ,UsuarioDTO);
     * @author felipe.cadena
     */
    @Override
    public void generarReplicaDeConsulta(Long tramiteId, UsuarioDTO usuario) {

        Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(tramiteId);

        try {
            boolean tieneCopia = this.validarExistenciaCopiaConsulta(tramite);

            if (!tieneCopia) {
                //paso 2
                this.transferirTramiteATiemposMuertosGCC(tramite, usuario);
                //paso 3
                this.enviarJobGenerarReplicaConsulta(tramite, usuario);
            }

        } catch (Exception e) {
            this.loggerAplicarCambios(tramite, usuario,
                EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString(), "ERROR", -1);
        }

    }

    /**
     * @see
     * IConservacionLocal#finalizarReplicaConsulta(co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob)
     * @author felipe.cadena
     */
    @Override
    public boolean finalizarReplicaConsulta(ProductoCatastralJob job) {

        try {
            //Paso 3.2
            this.finalizarGenerarReplicaConsulta(job);
            return true;
        } catch (Exception e) {
            this.loggerAplicarCambios(null, null, EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
                toString(), "ERROR", -1);
            return false;
        }

    }

    /**
     * Retorna la actividad actual de un tramite determinado se realizan 10 intentos paa obtener la
     * actividad
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public Actividad obtenerActividadActualTramite(Long tramiteId) {

        Actividad actividadTramite = null;
        ArrayList<Actividad> listaActividades;
        int intentos = 0;

        while (actividadTramite == null) {

            if (intentos == 10) {
                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(LOGGER,
                    new Exception("Error al obtener la actividad del tramite"),
                    "ConservacionBean#obtenerActividadActualTramite", "obtener actividad");
            }
            try {
                listaActividades = (ArrayList<Actividad>) this.procesosService.
                    getActividadesPorIdObjetoNegocio(tramiteId);
            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(LOGGER,
                    ex, "ConservacionBean#finalizarCambiosDepuracion", "avanzarActividad");
            }

            if (listaActividades != null && !listaActividades.isEmpty() &&
                listaActividades.get(0).getId() != null &&
                !listaActividades.get(0).getId().isEmpty()) {

                actividadTramite = listaActividades.get(0);
            }
            intentos++;
        }

        return actividadTramite;
    }

    /**
     * Actualiza el estado del tramite en el proceso de aplicar cambios
     *
     * @author felipe.cadena
     * @param tramite
     * @param tarea
     * @param estado
     * @param mensaje
     * @param tramite
     */
    private void actualizarEstadoTramiteAC(Tramite tramite, UsuarioDTO usuario, String tarea,
        String estado, String mensaje, Exception e, int paso) {
        try {
            if (tramite != null) {
                if (mensaje == null) {
                    mensaje = "Paso #" + paso + " - " + estado;
                }
                if (e != null) {
                    if (e.getCause() != null) {
                        mensaje += " EX : " + e.getCause().toString();
                    } else {
                        mensaje += " EX : " + e.toString();
                    }
                }
                if (mensaje.length() > 1990) {
                    mensaje = mensaje.substring(0, 1990);
                }

                TramiteTarea tt = new TramiteTarea(
                    tramite,
                    tarea,
                    String.valueOf(paso),
                    estado,
                    mensaje,
                    new Date(),
                    usuario.getLogin());
                if (!ETramiteTareaResultado.ERROR.toString().equals(tramite.getResultado())) {
                    tramite.setTarea(tarea);
                    tramite.setPaso(String.valueOf(paso));
                    tramite.setResultado(estado);
                }

                this.tramiteTareaDao.update(tt);
                this.tramiteDao.update(tramite);
            }
        } catch (Exception ex) {
            LOGGER.error("Error al registrar estado de tarea", ex);
        }
    }

    /**
     * @see IConservacionLocal#obtenerJobsErrorAGS()
     * @author felipe.cadena
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsErrorAGS() {

        List<ProductoCatastralJob> pcjError = null;

        try {
            pcjError = this.productoCatastralJobDAO.obtenerJobsSIGConErrores();

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al obtener los jobs con error de AGS");
        }
        return pcjError;

    }

    /**
     * @see IConservacionLocal#actualizarEstadoTramitesAGS()
     * @author felipe.cadena
     */
    @Override
    public boolean actualizarEstadoTramitesAGS(List<ProductoCatastralJob> jobs) {

        try {
            List<ProductoCatastralJob> pcjError = jobs;
            ProductoCatastralJob vpcj = new ProductoCatastralJob();
            for (ProductoCatastralJob pcj : pcjError) {
//           //vpcj = this.productoCatastralJobDAO.obtenerJobPorIdYParametro(pcj.getId(), "numeroTramite");
//            if(vpcj ==null){
//                pcj.setEstado(ProductoCatastralJob.AGS_ERROR_CANCELADO);
//                this.productoCatastralJobDAO.update(pcj);
//                continue;
//            }
                Long tramiteId = pcj.getTramiteId();
                String usuario = pcj.getUsuarioLog();
                Tramite tramite = this.tramiteService.buscarTramitePorTramiteIdProyeccion(tramiteId);

                if (tramite == null) {
                    pcj.setEstado(ProductoCatastralJob.AGS_ERROR_CANCELADO);
                    continue;
                }
                if (tramite.getResultado() != null && tramite.getResultado().equals(
                    ETramiteTareaResultado.ERROR.toString())) {
                    continue; //si el tramite ya es marcado como error no se procesa
                }

                ProductoCatastralJob ultimoPcj = this.productoCatastralJobDAO.
                    obtenerUltimoJobPorTramite(tramiteId, null);

                if (ultimoPcj.getEstado().equals(pcj.getEstado())) {
                    //actualizar tramite
                    if (!this.acutalizarEstadoTramitePorJob(pcj, tramite, usuario)) {
                        pcj.setEstado(ProductoCatastralJob.AGS_ERROR_CANCELADO);
                        this.productoCatastralJobDAO.update(pcj);
                    }
                } else {
                    pcj.setEstado(ProductoCatastralJob.AGS_ERROR_CANCELADO);
                    this.productoCatastralJobDAO.update(pcj);
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al actualizar los tramites de AGS");
            return false;
        }
        return true;
    }

    /**
     * Actualiza el estado del tramite segun el job
     *
     *
     * @author felipe.cadena
     * @param job
     * @param idTramite
     */
    private boolean acutalizarEstadoTramitePorJob(ProductoCatastralJob job, Tramite tramite,
        String usuario) {
        String tipoProducto = job.getTipoProducto();
        String paso = null;
        String tarea = null;

//TODO:: felipe.cadena:: verficar si se debe diferenciar para los tipos de productos
        if (tramite.getPaso() == null) {
            if (tipoProducto.equals(ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS.toString())) {
                paso = "3";
            } else if (tipoProducto.equals(ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION.
                toString())) {
                paso = "3";
            } else {
                paso = "1";
            }
        } else {
            paso = String.valueOf(tramite.getPaso());
        }
        if (tramite.getTarea() == null) {
            if (tipoProducto.equals(ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS.toString())) {
                tarea = EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString();
            } else if (tipoProducto.equals(ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION.
                toString())) {
                tarea = EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString();
            } else if (tipoProducto.
                equals(ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION.toString())) {
                tarea = EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString();
            } else {
                tarea = EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString();
            }
        } else {
            tarea = String.valueOf(tramite.getTarea());
        }

        tramite.setResultado(ETramiteTareaResultado.ERROR.toString());
        tramite.setTarea(tarea);
        tramite.setPaso(paso);
        TramiteTarea tt = new TramiteTarea(
            tramite,
            tarea,
            paso,
            ETramiteTareaResultado.ERROR.toString(),
            job.getResultado(),
            new Date(),
            usuario);

        this.tramiteTareaDao.update(tt);
        this.tramiteDao.update(tramite);
        return true;
    }

    /**
     * Log de info de los procesos de aplicar cambios
     *
     * @author felipe.cadena
     *
     * @param t
     * @param usuario
     * @param proceso
     * @param estado
     * @param paso
     */
    private void loggerAplicarCambios(
        Tramite t,
        UsuarioDTO usuario,
        String proceso,
        String estado,
        int paso) {

        String tid;
        String sid;
        if (t == null) {
            tid = "Tramite null";
        } else {
            tid = String.valueOf(t.getId());
        }
        if (t == null) {
            sid = "Usuario null";
        } else {
            sid = String.valueOf(t.getId());
        }

        if (estado.equals("ERROR")) {
            LOGGER.error("----------------------ERROR EN EL PROCESO DE " + proceso +
                "-----------------------");
            LOGGER.error(
                "-----------------------------------------------------------------------------------");
            LOGGER.error("***" + proceso + "::T = " + tid + "::U = " + sid + "::PASO = " + paso +
                " " + estado);
            LOGGER.error(
                "-----------------------------------------------------------------------------------");
            LOGGER.error("----------------------ERROR EN EL PROCESO DE " + proceso +
                "-----------------------");
        }

        LOGGER.info(
            "-----------------------------------------------------------------------------------");
        LOGGER.info("***" + proceso + "::T = " + tid + "::U = " + usuario.getLogin() + "::PASO = " +
            paso + " " + estado);
        LOGGER.info(
            "-----------------------------------------------------------------------------------");

    }

    /**
     * @see
     * IConservacionLocal#actualizarTramiteTarea(co.gov.igac.snc.persistence.entity.tramite.TramiteTarea)
     * @author felipe.cadena
     */
    @Override
    public TramiteTarea actualizarTramiteTarea(TramiteTarea tt) {

        TramiteTarea answer = null;
        try {
            answer = this.tramiteTareaDao.update(tt);
        } catch (Exception e) {
            LOGGER.info("Error al actulizar el tramite tarea");
        }

        return answer;
    }

    public void actualizarEstadoSigError() {

    }

    /**
     * Método encargado de cambiar el tipo de documento de la copia de bd geográfica original y de
     * la final por copia de bd geográfica original depuración y final depuración
     *
     * @author javier.aponte
     * @param idTramite
     * @param usuarioActividad
     */
    private void cambiarTipoDocumentoCopiaBdGeografica(Long idTramite) {

//TODO::felipe.cadena:: reemplazar con codigo de javier.aponte para manejo de replicas
        this.conservacionService.cambiarTipoDocumentoCopiaBdGeografica(idTramite);

    }

    /**
     * Transfiere la actividad actual de un tramite al usuario de destino
     *
     * @param t
     * @param usuarioDestino
     */
    private void transferirTramite(Tramite tramite, UsuarioDTO usuarioDestino) {
        Actividad actividadTramite;

        try {
            actividadTramite = tramite.getActividadActualTramite();
            if (actividadTramite == null || actividadTramite.getId() == null) {
                actividadTramite = this.obtenerActividadActualTramite(tramite.getId());
            }
            this.procesosService.transferirActividadConservacion(actividadTramite.getId(),
                usuarioDestino);
        } catch (ExcepcionSNC ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(LOGGER,
                ex, "ConservacionBean#transferirTramite", "transferirTramite");
        }
    }

    /**
     * Determina si el tramite para aplicar cambios es de depuración
     *
     * @author felipe.cadena
     * @param idTramite
     * @return
     */
    private boolean determinarSiEsDepuracion(Long idTramite) {

        List<Actividad> actividades = this.procesosService.getActividadesPorIdObjetoNegocio(
            idTramite);

        if (actividades != null && !actividades.isEmpty()) {
            Actividad act = actividades.get(0);
            if (act.getNombre().equals(
                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) ||
                act.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) ||
                act.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {
                return true;
            } else {
                return false;
            }
        } else {
            LOGGER.error("La lista de actividades para el trámite " + idTramite.toString() +
                " resultó nula o vacía");
            return false;
        }
    }

    /**
     *
     * ACA
     *
     * Metodo para invocar el aplicar cambios del proceso de Actualizacion desde un paso determinado
     * ACC
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez:: 23-10-2015:: Se modifica el retorno del metodo.
     */
    @Override
    public boolean aplicarCambiosActualizacion(Tramite tramite, UsuarioDTO usuario, int paso) {
        LOGGER.debug(
            "on AplicacionCambiosBean#aplicarCambiosActualizacion inicio seleccion paso: " + paso);
        boolean validaResultado = true;
        try {
            if (paso == 0) {
                //inicialmente solo se aplican cambios alfanumericos para los tramites de actualizacion
                this.userTransaction.begin();
                this.enviarJobGeografico(tramite, usuario);
                this.userTransaction.commit();
            } else if (paso == 1) {
                //solo se llama desde la parte asincronica
                this.userTransaction.begin();
                validaResultado = this.aplicarCambiosAlfanumericos(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION);
                this.userTransaction.commit();
                validaResultado = this.aplicarCambiosActualizacion(tramite, usuario, 2);
            } else if (paso == 2) {
                this.userTransaction.begin();
                validaResultado = this.finalizarProcessAplicarCambios(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION);
                this.userTransaction.commit();
                validaResultado = this.aplicarCambiosActualizacion(tramite, usuario, 3);
            } else if (paso == 3) {
                this.userTransaction.begin();
                validaResultado = this.finalizarEstadosAlfanumericos(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION);
                this.userTransaction.commit();
            }
            return validaResultado;
            //El proceso de aplicar cambios de actualizacion no invoca genaracion de carta ni de ficha

        } catch (Exception ex) {
            //ErrorUtil.notificarErrorPorCorreoElectronico(ex, usuario.getLogin(), tramite.getId(), message);
            try {
                this.userTransaction.commit();
                return false;
            } catch (Exception e) {
                LOGGER.error("Error en la aplicación de cambios");
                return false;
            }
        }

    }

    /**
     *
     * ACA Masivo
     *
     * Metodo para invocar el aplicar cambios del proceso de Actualizacion desde un paso determinado
     * ACC
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    @Override
    public void aplicarCambiosActualizacionMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        int paso) {
        LOGGER.debug(
            "on AplicacionCambiosBean#aplicarCambiosActualizacion inicio seleccion paso: " + paso);
        try {
            if (paso == 0) {
                //inicialmente solo se aplican cambios alfanumericos para los tramites de actualizacion
                this.userTransaction.begin();
                //TODO::implementar envio geografico masivo si es necesario
                //this.enviarJobGeografico(tramite, usuario);
                this.userTransaction.commit();
            } else if (paso == 1) {
                //solo se llama desde la parte asincronica
                this.userTransaction.begin();
                this.aplicarCambiosAlfanumericosMasivo(tramites, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION);
                this.userTransaction.commit();
                this.aplicarCambiosActualizacionMasivo(tramites, usuario, 2);
            } else if (paso == 2) {
                this.userTransaction.begin();
                this.finalizarProcessAplicarCambiosMasivo(tramites, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION);
                this.userTransaction.commit();
                //this.aplicarCambiosActualizacionMasivo(tramites, usuario, 3);
            } else if (paso == 3) {
                this.userTransaction.begin();
                this.finalizarEstadosAlfanumericosMasivo(tramites, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION);
                this.userTransaction.commit();
            }

            //El proceso de aplicar cambios de actualizacion no invoca genaracion de carta ni de ficha
        } catch (Exception ex) {
            //ErrorUtil.notificarErrorPorCorreoElectronico(ex, usuario.getLogin(), tramite.getId(), message);
            try {
                this.userTransaction.commit();
            } catch (Exception e) {
                LOGGER.error("Error en la aplicación de cambios");
            }
        }
    }

    /**
     * PASO ACMasivo # 2 <br/><br/>
     *
     * Metodo asociado al paso 2 del proceso de aplicar cambios. Aplica los cambios alfanumericos
     * asociados una lista de tramites
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean aplicarCambiosAlfanumericosMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        EProcesosAsincronicos proceso) {

        this.loggerAplicarCambios(null, usuario, proceso.toString(), "INICIO MASIVO", 2);

        for (Tramite tramite : tramites) {

            if (tramite.isTramiteGeografico() && !tramite.isActualizacion()) {
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.FIN.toString(), null, null, 1);
            }

            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.INICIO.toString(), null, null, 2);
        }

        String message;
        try {
            Object mensajes[] = this.sncProcedimientoDao.confirmarProyeccionMasivo(tramites);
            if (Utilidades.hayErrorEnEjecucionSP(mensajes)) {
                this.loggerAplicarCambios(null, usuario, proceso.toString(), "ERROR MASIVO", 2);
                message =
                    "Ocurrió un Error al confirmar la proyección en la base de datos para multiples tramites : " +
                    ErrorUtil.getError(mensajes);
                LOGGER.error(message);
                //error paso
                for (Tramite tramite : tramites) {
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, null, 2);
                    throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER);
                }
            }
            //paso existoso
            message = "paso existoso";
            for (Tramite tramite : tramites) {
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.FIN.toString(), message, null, 2);
            }
        } catch (ExcepcionSNC ex) {
            message = "Ocurrió un error plicar los cambios alfanumericos de los tramites";
            this.loggerAplicarCambios(null, usuario, proceso.toString(), "ERROR MASIVO", 2);
            LOGGER.error(message, ex);
            //error paso
            for (Tramite tramite : tramites) {
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, ex, 2);
            }

            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_SERVICIOS_SIG.getExcepcion(
                LOGGER, ex, "ConservacionBean#recibirJobGeografico",
                "finalizarEdicionTramiteGeografico");
        }
        this.loggerAplicarCambios(null, usuario, proceso.toString(), "FIN MASIVO", 2);

        return true;
    }

    /**
     * PASO ACMasivo # 3<br/><br/>
     *
     * Metodo asociado al paso 3 del proceso de aplicar cambios de conservacion. Finaliza el proceso
     * para los tramites de la lista
     *
     * @author felipe.cadena
     * @param tramites
     * @param usuario
     * @return
     */
    @Override
    public boolean finalizarProcessAplicarCambiosMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        EProcesosAsincronicos proceso) {

        this.loggerAplicarCambios(null, usuario, proceso.toString(), "INICIO_PROCESS", 3);
        ArrayList<Actividad> listaActividades = new ArrayList<Actividad>();

        String message;

        for (Tramite tramite : tramites) {
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.INICIO.toString(), null, null, 3);

            Actividad actTramite = tramite.getActividadActualTramite();
            //felipe.cadena::Solo se consulta la actividad en process si esta no esta incluida en el tramite
            if (actTramite == null || actTramite.getId() == null) {
                try {
                    listaActividades = (ArrayList<Actividad>) this.procesosService.
                        getActividadesPorIdObjetoNegocio(tramite.getId(),
                            ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);
                } catch (ExcepcionSNC ex) {
                    message =
                        "Ocurrió un error al consultar la lista de actividades asociadas a los tramites ";
                    this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 3);
                    LOGGER.error(message, ex);
                    //error paso
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, ex, 3);

                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(
                        LOGGER,
                        ex, "ConservacionBean#aplicarCambiosDeProyeccionS", "avanzarActividad");
                }
            } else {
                listaActividades.add(actTramite);
            }

        }

        String idActividad = null;

        String processTransition = "";
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuariosActividad = new ArrayList<UsuarioDTO>();
        usuariosActividad.add(usuario);
        processTransition = ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setUsuarios(usuariosActividad);

        if (listaActividades != null && !listaActividades.isEmpty()) {

            try {
                //this.procesosService.avanzarActividad(usuario, idActividad, solicitudCatastral);
                //TODO::felipe.cadena::Invocar metodo de avance masivo
                this.procesosService.avanzarActividad(usuario, idActividad, solicitudCatastral);
            } catch (Exception ex) {
                message = "Ocurrió un error en el BPM al tratar de avanzar los tramites masivos ";

                this.loggerAplicarCambios(null, usuario, proceso.toString(), "ERROR MASIVO", 3);
                LOGGER.error(message, ex);
                //error paso
                for (Tramite tramite : tramites) {
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, ex, 3);
                }

                throw new ExcepcionSNC("Error en el BPM", ex.getMessage(), message);
            }

        } else {
            message =
                "No se encontró la instancia de actividad correspondiente los tramites en la actividad " +
                ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
            this.loggerAplicarCambios(null, usuario, proceso.toString(), "ERROR MASIVO", 3);
            LOGGER.error(message);
            //error paso
            for (Tramite tramite : tramites) {
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, null, 3);
            }

            throw new ExcepcionSNC("No se encontró la actividad del trámite", "Error", message);
        }

        //solo se envia la solicitud pero el BPM es quien confirma la finalizacion
        this.loggerAplicarCambios(null, usuario, proceso.toString(), "FIN ENVIO MASIVO ", 3);

        return true;
    }

    /**
     * PASO ACMasivo # 4<br/><br/>
     *
     * Metodo asociado al paso 4 del proceso de aplicar cambios de conservacion. Actualiza los
     * estados del tramite y la resolucion cuando se ha terminado la actividad de aplicar cambios.
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    @Override
    public boolean finalizarEstadosAlfanumericosMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        EProcesosAsincronicos proceso) {

        this.loggerAplicarCambios(null, usuario, proceso.toString(),
            "INICIO_ACTUALIZAR_ESTADOS MASIVO", 4);

        for (Tramite tramite : tramites) {
            this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                ETramiteTareaResultado.INICIO.toString(), null, null, 4);

            String message;

            try {
                //D: ACTUALIZAR EL TRAMITE Y LA RESOLUCION

                //se elimina la version geografica
                // no se elimina para tramites de actualizacion ya que no se creo ninguna
                if (!tramite.isActualizacion()) {
                    this.eliminarVersionGeoTramite(tramite, usuario);
                }

                // Cuando el trámite es de quinta actualizar el predio del trámite
                if (tramite.isQuinta() || tramite.isQuintaNuevo() || tramite.isQuintaOmitido() ||
                    tramite.isQuintaOmitidoNuevo()) {

                    List<PPredio> prediosDelTramite = this.conservacionService
                        .buscarPPrediosPorTramiteId(tramite.getId());
                    if (prediosDelTramite != null && !prediosDelTramite.isEmpty()) {
                        Predio predioSeleccionado = this.conservacionService.
                            obtenerPredioPorId(prediosDelTramite.get(0).getId());
                        if (predioSeleccionado != null) {
                            tramite.setPredio(predioSeleccionado);
                            tramite.setPredioId(predioSeleccionado.getId());
                        }
                    }
                }

                //Se actualiza el estado final del tramite
                try {
                    TramiteEstado te = new TramiteEstado(ETramiteEstado.FINALIZADO_APROBADO.
                        getCodigo(), tramite, usuario.getLogin());
                    te = this.tramiteService.guardarActualizarTramiteEstado(te);
                    tramite.setTramiteEstado(te);
                } catch (Exception ex) {
                    message = "Error en la actualización del estado del tramite " +
                        tramite.getId().toString();
                    this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 4);
                    LOGGER.error(message, ex);
                    //error paso
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, null, 4);

                    throw new ExcepcionSNC("Error en la actualización del estado del tramite",
                        "Error", message);
                }

                tramite.setUsuarioLog(usuario.getLogin());
                tramite.setFechaLog(new Date());

                tramite.getResultadoDocumento().setEstado(EDocumentoEstado.RESOLUCION_EN_FIRME.
                    getCodigo());

                tramite = this.tramiteService.guardarActualizarTramite2(tramite);

                Documento docResp;
                docResp = this.tramiteService.guardarYactualizarDocumento(tramite.
                    getResultadoDocumento());
                if (docResp == null) {
                    message =
                        "Error en la actualización del documento resultado del trámite con id " +
                        tramite.getId().toString();
                    this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 4);
                    LOGGER.error(message);
                    //error paso
                    this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                        ETramiteTareaResultado.ERROR.toString(), message, null, 4);

                    throw new ExcepcionSNC("Error en Documento", "Error", message);
                }
                //paso exitoso
                message = "paso existoso";
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.FIN.toString(), message, null, 4);
            } catch (ExcepcionSNC ex) {
                message = "Ocurrió un error al actualizar el estado del tramite";
                this.loggerAplicarCambios(tramite, usuario, proceso.toString(), "ERROR", 4);
                LOGGER.error(message, ex);
                //error paso
                this.actualizarEstadoTramiteAC(tramite, usuario, proceso.toString(),
                    ETramiteTareaResultado.ERROR.toString(), message, ex, 4);

                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_INTERFAZ_INTEGRACION.
                    getExcepcion(
                        LOGGER, ex, "ConservacionBean#finalizarEstadosAlfanumericos",
                        "finalizarEstadosAlfanumericos");
            }

        }
        this.
            loggerAplicarCambios(null, usuario, proceso.toString(), "FIN_ACTUALIZAR_ESTADOS MASIVO",
                4);

        return true;
    }

    /**
     * Metodo que almacena el documento que se genera en las resoluciones y actualiza el tramite
     * asociandole la resolucion que se genero. antes de ingresar a la actividad de aplicar cambios.
     *
     * @author leidy.gonzalez
     * @param tramite
     * @param usuario
     * @param tramiteDocumento
     * @param documento
     * @param archivoResoluciones
     *
     * @return Tramite
     */
    @Override
    public Tramite guardaDocumentoTramiteResoluciones(Tramite tramite,
        TramiteDocumento tramiteDocumento, Documento documento,
        UsuarioDTO usuario, File archivoResoluciones, ProductoCatastralJob job, int contador) {

        Parametro maxEnvioJob = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());

        try {

            this.userTransaction.begin();

            documento.setArchivo(archivoResoluciones.getPath().toString());
            LOGGER.debug("Nombre documento:" + archivoResoluciones.getPath().toString());

            // Se sube el archivo al gestor documental y se actualiza el documento
            //documento = tramiteService.guardarDocumento(usuario, documento);
            documento = this.documentoDao.clasificarAlmacenamientoDocumentoGestorDocumental(usuario,
                documento);

            LOGGER.debug("Almacenamiento documento FTP:" + documento.getIdRepositorioDocumentos());

            if (documento.getIdRepositorioDocumentos() == null ||
                documento.getIdRepositorioDocumentos().trim().isEmpty()) {

                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
                LOGGER.error(
                    "Ocurrió un error al guardar la Resoluciones en el sistema de gestión documental");
                return null;

            } else {

                if (tramiteDocumento.getDocumento() != null &&
                    tramiteDocumento.getDocumento().getId() != null) {

                    //Si existe se actualiza registro en Base de Datos
                    tramiteDocumento.setIdRepositorioDocumentos(documento.
                        getIdRepositorioDocumentos());
                    tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                } else {
                    //Si no existe se genera registro en Base de Datos
                    tramiteDocumento.setDocumento(documento);
                    tramiteDocumento.setIdRepositorioDocumentos(documento.
                        getIdRepositorioDocumentos());
                    tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                }

                tramiteDocumento = tramiteService
                    .actualizarTramiteDocumento(tramiteDocumento);
                LOGGER.debug("Guardar tramiteDocumento:" + tramiteDocumento.getId());

                //Se almacena el documento de resoluciones asociandolo al tramite 
                tramite.setResultadoDocumento(documento);
                LOGGER.debug("Documento Almacenado en Tramite:" + tramite.getResultadoDocumento());

                tramite = tramiteService.guardarActualizarTramite(tramite);
                LOGGER.debug("Guardar tramite:" + tramite);

                //Se verifica que quedo la relacion de Documento de Resoluciones con el Tramite
                if (tramite.getResultadoDocumento() == null &&
                    tramite.getResultadoDocumento().getId() == null) {

                    this.productoCatastralJobDAO.actualizarEstado(job,
                        ProductoCatastralJob.JPS_ERROR);
                    LOGGER.debug("Error al asociar el documento de la resolución en el trámite");
                    return null;

                } else {
                    this.productoCatastralJobDAO.actualizarEstado(job,
                        ProductoCatastralJob.JPS_TERMINADO);
                    LOGGER.debug("Generacion Correcta Job con id:" + job.getId() +
                        " estado: " + job.getEstado());
                }
            }

            this.userTransaction.commit();

        } catch (Exception ex) {
            try {
                if (contador == maxEnvioJob.getValorNumero()) {
                    this.productoCatastralJobDAO.actualizarEstado(job,
                        ProductoCatastralJob.JPS_ERROR);
                    LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                }

                this.userTransaction.commit();
            } catch (Exception e) {
                LOGGER.error("Error en la guardar Documento Tramite Resoluciones");
            }
        }
        return tramite;
    }

    /**
     * Obtiene el responsable sig de la oficina del usuario, si es una UOC y no existe el rol lo
     * busca en la territorial, si no existe ninguno retorna null
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @param actividad
     * @return
     */
    private UsuarioDTO obtenerResponsableSIG(UsuarioDTO usuario) {

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        if (usuario.getDescripcionUOC() != null) {
            usuarios = this.tramiteService.buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionUOC(),
                ERol.RESPONSABLE_SIG);
        }

        //si no existe Responsable SIG en la UOC se buscan en la territorial.
        if (usuarios.isEmpty()) {
            usuarios = this.tramiteService.buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_SIG);
        }

        if (usuarios.isEmpty()) {
            return null;
        }

        return usuarios.get(0);
    }

    /**
     * @author felipe.cadena
     *
     * Determina si existe un job vigente en el momento de querer enviar otro, para evitar
     * reprocesamiento
     *
     * @param tipoJob
     *
     * @return
     */
    private boolean validarJobVigente(String tipoJob, Tramite tramite) {
        List<ProductoCatastralJob> jobs = this.conservacionService.
            obtenerJobsGeograficosPorTramiteTipoEstado(tramite.getId(), tipoJob, null);

        ProductoCatastralJob jobVigente;

        if (jobs != null) {
            if (!jobs.isEmpty()) {
                if (jobs.size() == 1) {
                    jobVigente = jobs.get(0);
                } else {
                    jobVigente = jobs.get(0);
                    for (ProductoCatastralJob productoCatastralJob : jobs) {
                        if (productoCatastralJob.getFechaLog().after(jobVigente.getFechaLog())) {
                            jobVigente = productoCatastralJob;
                        }
                    }
                }

                if (jobVigente.getEstado().equals(ProductoCatastralJob.AGS_ESPERANDO) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.AGS_EN_EJECUCION) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.AGS_TERMINADO) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.SNC_EN_EJECUCION) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.JPS_EN_EJECUCION) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.JPS_ESPERANDO)) {
                    LOGGER.warn("El trámite ya esta en proceso de " + tipoJob + " en estado : " +
                        jobVigente.getEstado());
                    return true;
                }

                if (jobVigente.getEstado().equals(ProductoCatastralJob.AGS_ERROR) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.SNC_ERROR) ||
                    jobVigente.getEstado().equals(ProductoCatastralJob.JPS_ERROR)) {
                    LOGGER.warn("El trámite presento un error en la ejecucion del job previo: " +
                        jobVigente.getEstado());
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Método para actualizar la tabla de colindancia
     *
     * @author jonathan.chacon
     * @param tramite
     * @param usuario
     * @return
     */
    private boolean actualizarColindanciaPredios(Tramite tramite, UsuarioDTO usuario) {

        //Solo para los tramites que aplican según el CC-NP-CO-131
        if (tramite != null && (tramite.isRectificacionArea() ||
            tramite.isSegunda() ||
            tramite.isQuinta() ||
            tramite.isCancelacionMasiva())) {
            try {

                //Selecciona los predios a actualizar
                //List<PPredio> pPredios= this.conservacionService.buscarPPrediosCompletosPorTramiteId(tramite.getId());
                List<HPredio> hPredios = this.hPredioDao.
                    buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(tramite.
                        getId());
                //findHPredioListByConsecutivoCatastral(BigDecimal.ONE)
                String preds = "";
                for (HPredio pPredio : hPredios) {
                    if (!pPredio.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.
                        getCodigo()) &&
                        !pPredio.getEstado().equals(EPredioEstado.CANCELADO.getCodigo()) &&
                        pPredio.getTipoHistoria().
                            equals(EHPredioTipoHistoria.PROYECCION.getCodigo())) {
                        if (pPredio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.
                            getCodigo())) {
                            //if(pPredio.isEsPredioFichaMatriz()){
                            if (pPredio.getUnidad().equals("0000")) {
                                preds += pPredio.getNumeroPredial();
                                preds += ",";
                            }
                        } else {
                            preds += pPredio.getNumeroPredial();
                            preds += ",";
                        }
                    }
                }

                ProductoCatastralJob job = SigDAOv2.getInstance().enviarJobActualizarColindancia(
                    productoCatastralJobDAO, preds, tramite, usuario);
                //Se crea el registro en la tabla tramite_tarea
                this.actualizarEstadoTramiteAC(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                    ETramiteTareaResultado.INICIO.toString(), "Paso #7 INICIO - JOB_ID: " + job.
                    getId().toString(), null, 7);

            } catch (ExcepcionSNC sncEx) {
                LOGGER.error("Ocurrió un error Atualizando la colindancia de los predios " +
                    sncEx.getMensaje());
                //Se guarda el mensaje de error en tramite_tarea
                this.actualizarEstadoTramiteAC(tramite, usuario,
                    EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                    ETramiteTareaResultado.ERROR.toString(),
                    "Ocurrió un error Atualizando la colindancia de los predios " +
                    sncEx.getMensaje(), null, 7);
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Recibe y verifica el job de colindancia y actualiza tramite_tarea en BD
     *
     * @param job
     *
     */
    @Override
    public void verificaJobActualizarColindancia(ProductoCatastralJob job, UsuarioDTO usuario) {

        try {

            Tramite tramite = this.tramiteDao.
                buscarTramitePorTramiteIdProyeccion(job.getTramiteId());

            this.actualizarEstadoTramiteAC(tramite, usuario,
                EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString(),
                ETramiteTareaResultado.FIN.toString(), "Paso #7 - FIN", null, 7);

        } catch (Exception sncEx) {
            LOGGER.error("Ocurrió un error Atualizando la colindancia de los predios " +
                sncEx.getMessage());
        }
    }

    /**
     * Determina is existe un proceso vigente de aplicar cambios
     *
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    private boolean existeProcesoVigente(Long tramiteId) {

        List<TramiteTarea> tareasAC = this.tramiteTareaDao.obtenerTareasTramite(tramiteId,
            EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.toString());

        for (TramiteTarea tramiteTarea : tareasAC) {
            if ((tramiteTarea.getPaso().equals("1") && tramiteTarea.getResultado().equals(
                ETramiteTareaResultado.FIN.toString())) ||
                tramiteTarea.getPaso().equals("2")) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Metodo que almacena el documento que se genera en las resoluciones y actualiza el tramite
     * asociandole la resolucion que se genero. antes de ingresar a la actividad de aplicar cambios.
     *
     * @author leidy.gonzalez
     * @param job
     * @param contador
     *
     * @return boolean
     */
    @Override
    public boolean generarResolucionesConservacionEnJasperServerYGuardar(ProductoCatastralJob job,
        int contador){
        
        LOGGER.debug("generarResolucionesConservacionEnJasperServerYGuardar");
        boolean resultadoOk = true;
        boolean errorGeneracionReporte = false;
        boolean noPrediosProyectados = false;
        Tramite tramite = new Tramite();
        EReporteServiceSNC enumeracionReporte = null;
        String nombreResponsableConservacion;

        Parametro maxEnvioJob = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());
        UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());
        
        try {
            
        this.userTransaction.begin();    
            
          contador++;

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService
                .buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            Long tramiteId = job.getTramiteId();

            //Se consulta si el Tramite tiene Predios Proyectados
            List<PPredio> pPrediosResolucion = this.pPredioDao
                .findExistingPPrediosByTramiteId(tramiteId);

            LOGGER.debug("pPrediosResolucion:" + pPrediosResolucion);

            tramite = tramiteService.buscarTramitePorId(tramiteId);
            LOGGER.debug("buscarTramitePorId:" + tramiteId);
            
            //Si tiene Predios Proyectados se puede generar la Resolución
            if (!pPrediosResolucion.isEmpty() || (tramite.isViaGubernativa() &&
                pPrediosResolucion.isEmpty())) {
                
                if (tramite != null && tramite.getTipoTramite() != null) {
                    if (tramite.getSubtipo() == null) {
                        tramite.setSubtipo("");
                    }
                    if (tramite.getRadicacionEspecial() == null) {
                        tramite.setRadicacionEspecial("");
                    }
                    //Obtiene la url del Reporte a generar
                    enumeracionReporte = tramiteService.obtenerUrlReporteResoluciones(tramite.
                        getId());

                    String urlReporte = enumeracionReporte.getUrlReporte();

                    LOGGER.debug("tipo Reporte:" + urlReporte);

                    Reporte reporte = new Reporte(usuario, estructuraOrganizacional);

                    reporte.setEstructuraOrganizacional(estructuraOrganizacional);

                    reporte.setUrl(urlReporte);
                    //Se obtienen los parametros del job para generar el reporte
                    List<String> resultados = new ArrayList<String>();
                    
                    try {
                        if (job.getResultado().contains("<?xml")) {
                            LOGGER.debug(job.getResultado());
                            org.dom4j.Document document = (org.dom4j.Document) DocumentHelper.
                                parseText(
                                    job.getResultado());
                            List parametro = ((Node) document).selectNodes("//parametro");
                            for (Iterator iter = parametro.iterator(); iter.hasNext();) {
                                Element productoXml = (Element) iter.next();
                                resultados.add(productoXml.attributeValue("tramiteId"));
                                resultados.add(productoXml.attributeValue(
                                    "nombreResponsableConservacion"));
                                resultados.add(productoXml.attributeValue("numeroResolucion"));
                                resultados.add(productoXml.attributeValue("fechaResolucion"));
                                resultados.add(productoXml.attributeValue(
                                    "fechaResolucionConFormato"));
                                resultados.add(productoXml.attributeValue("nombreRevisor"));
                                resultados.add(productoXml.attributeValue("firmaUsuario"));
                                resultados.add(productoXml.attributeValue("copiaUsoRestringido"));
                            }

                        } else {
                            this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                            LOGGER.error(job.getResultado());
                            String message =
                                "Ocurrió un error al obtener los parametros del producto catastral job para resoluciones de conservacion";
                            this.userTransaction.commit();
                        }
                    } catch (DocumentException e) {
                        throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e,
                            e.getMessage());
                    }

                    String numeroDocumento = resultados.get(2);

                    reporte.addParameter("TRAMITE_ID", (resultados.get(0) != null) ? resultados.get(
                        0) : "");
                    LOGGER.debug("TRAMITE_ID:" + resultados.get(0));
                    nombreResponsableConservacion =
                        (resultados.get(1) != null) ? resultados.get(1) : "";
                    reporte.addParameter("NOMBRE_RESPONSABLE_CONSERVACION",
                        nombreResponsableConservacion);
                    LOGGER.debug("nombreResponsableConservacion:" + resultados.get(1));
                    reporte.addParameter("NUMERO_RESOLUCION", (resultados.get(2) != null) ?
                        resultados.get(2) : "");
                    LOGGER.debug("NUMERO_RESOLUCION:" + resultados.get(2));
                    reporte.addParameter("FECHA_RESOLUCION", (resultados.get(3) != null) ?
                        resultados.get(3) : "");
                    LOGGER.debug("FECHA_RESOLUCION:" + resultados.get(3));
                    reporte.addParameter("FECHA_RESOLUCION_CON_FORMATO",
                        (resultados.get(4) != null) ? resultados.get(4) : "");
                    LOGGER.debug("FECHA_RESOLUCION_CON_FORMATO:" + resultados.get(4));

                    if (resultados.get(5) != null && !resultados.get(5).isEmpty()) {
                        reporte.addParameter("NOMBRE_REVISOR", resultados.get(5));
                        LOGGER.debug("NOMBRE_REVISOR:" + resultados.get(5));
                    }

                    if (resultados.get(6) != null && !resultados.get(6).isEmpty()) {
                        reporte.addParameter("FIRMA_USUARIO", resultados.get(6));
                        LOGGER.debug("FIRMA_USUARIO:" + resultados.get(6));
                    }

                    if (tramite.isTipoTramiteViaGubernativaModificado() ||
                        tramite.isTipoTramiteViaGubModSubsidioApelacion()) {

                        if (!tramite.getNumeroRadicacionReferencia().isEmpty()) {

                            Tramite tramiteReferencia = tramiteService.
                                buscarTramitePorNumeroRadicacion(tramite.
                                    getNumeroRadicacionReferencia());

                            LOGGER.debug("tramiteReferencia:" + tramiteReferencia);

                            if (tramiteReferencia != null) {
                                reporte.addParameter("NOMBRE_SUBREPORTE_RESUELVE", tramiteService.
                                    obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.
                                        getId()));
                            }
                        }

                    }

                    
                    //En este punto se realiza la generación de la firma escaneada
                    if (nombreResponsableConservacion != null &&
                        !nombreResponsableConservacion.trim().isEmpty()) {

                        FirmaUsuario firma;
                        String documentoFirma;

                        firma = this.generalesService.buscarDocumentoFirmaPorUsuario(
                            nombreResponsableConservacion);
                        LOGGER.debug(" Consulta FIRMA:" + firma);

                        if (firma != null && nombreResponsableConservacion.equals(firma.
                            getNombreFuncionarioFirma())) {

                            documentoFirma = this.generalesService.
                                descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                                    getIdRepositorioDocumentos());
                            LOGGER.debug("Descarga documentoFirma:" + documentoFirma);

                            reporte.addParameter("FIRMA_USUARIO", documentoFirma);
                        }
                    }

                    reporte.addParameter("COPIA_USO_RESTRINGIDO", (resultados.get(6) != null) ?
                        resultados.get(6) : "");
                    LOGGER.debug("COPIA_USO_RESTRINGIDO:" + resultados.get(6));

                    LOGGER.debug("Numero resolucion:" + numeroDocumento);
                    
                    //Se consulta el documento para ser modificado y almacenado en la BD
                    Documento documento = null;
                    List<TramiteDocumento> tramiteDocumentos = new ArrayList<TramiteDocumento>();
                    TramiteDocumento tramiteDocumento = new TramiteDocumento();
                    File archivoResolucionesConservacion = null;
                    IReporteService reportesService = ReporteServiceFactory.getService();

                    List<Documento> documentos = this.documentoDao.
                        buscarPorNumeroDocumentoYTipo(numeroDocumento,
                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                            getId());
                    if (documentos != null && !documentos.isEmpty()) {
                        documento = documentos.get(0);
                        LOGGER.debug("documento resolucion:" + documento.getId());
                    } else {

                        documentos = this.documentoDao.
                            buscarPorNumeroDocumentoYTipo(numeroDocumento,
                                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                getId());

                        if (documentos != null && !documentos.isEmpty()) {
                            documento = documentos.get(0);
                        }
                    }

                    if (documento == null) {
                        this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                        LOGGER.error("generarArchivoResolucionesConservacion#Error al consultar la resolución: " +
                            numeroDocumento);
                        this.userTransaction.commit();
                        resultadoOk = false;
                        return resultadoOk;

                    } else {

                        //Consulta si existe TramiteDocumento Asociado al documento y si no lo crea
                        tramiteDocumentos = this.tramiteDocumentoDao.
                            consultaTramiteDocumentosPorDocumentoId(
                                documento.getId());

                        if (tramiteDocumentos != null && !tramiteDocumentos.isEmpty()) {
                            //Se envia Registro de Tramite Documentacion Consultado
                            tramiteDocumento = tramiteDocumentos.get(0);
                            LOGGER.debug("tramiteDocumento:" + tramiteDocumento.getId());

                        } else {
                            //Se genera Registro de Tramite Documentacion por primera vez
                            tramiteDocumento.setTramite(tramite);
                            tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
                            tramiteDocumento.setUsuarioLog(usuario.getLogin());
                            LOGGER.debug("tramiteDocumento:" + tramiteDocumento);
                        }

                        LOGGER.debug("Parametros de reporte:" + reporte.getParametros() +
                            "Url Reporte: " + reporte.getUrl());
                        
                        
                        //Se llama el servicio del Jasper para generar el PDF de la Resolución
                        archivoResolucionesConservacion = reportesService.getReportAsFile(reporte,
                            new LogMensajesReportesUtil(this.generalesService, usuario));

                        LOGGER.debug("archivoResolucionesConservacion:" +
                            archivoResolucionesConservacion);

                        if (archivoResolucionesConservacion != null && !archivoResolucionesConservacion.getAbsolutePath().isEmpty()) {
                            LOGGER.debug(
                                "TareaGenerarArchivoResolucionesConservacion: Reporte Resoluciones Conservacion generado exitosamente" +
                                archivoResolucionesConservacion.getAbsolutePath());

                            //Se actualiza en BD el Tramite Documento, Documeto y Tramite
                            try {

                                documento.setArchivo(archivoResolucionesConservacion.getPath().toString());
                                LOGGER.debug("Nombre documento:" + archivoResolucionesConservacion.getPath().
                                    toString());

                                // Se sube el archivo al gestor documental y se actualiza el documento
                                //documento = tramiteService.guardarDocumento(usuario, documento);
                                documento = this.documentoDao.
                                    clasificarAlmacenamientoDocumentoGestorDocumental(usuario,
                                        documento);

                                LOGGER.debug("Almacenamiento documento FTP:" + documento.
                                    getIdRepositorioDocumentos());

                                if (documento.getIdRepositorioDocumentos() == null ||
                                    documento.getIdRepositorioDocumentos().trim().isEmpty()) {

                                    this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                                    LOGGER.error("Ocurrió un error al guardar la Resoluciones en el sistema de gestión documental");
                                    this.userTransaction.commit();
                                    resultadoOk = false;
                                    return resultadoOk;

                                } else {

                                    if (tramiteDocumento.getDocumento() != null &&
                                        tramiteDocumento.getDocumento().getId() != null) {

                                        //Si existe se actualiza registro en Base de Datos
                                        tramiteDocumento.setIdRepositorioDocumentos(documento.
                                            getIdRepositorioDocumentos());
                                        tramiteDocumento.setfecha(new Date(System.
                                            currentTimeMillis()));

                                    } else {
                                        //Si no existe se genera registro en Base de Datos
                                        tramiteDocumento.setDocumento(documento);
                                        tramiteDocumento.setIdRepositorioDocumentos(documento.
                                            getIdRepositorioDocumentos());
                                        tramiteDocumento.setfecha(new Date(System.
                                            currentTimeMillis()));

                                    }

                                    tramiteDocumento = tramiteService
                                        .actualizarTramiteDocumento(tramiteDocumento);
                                    LOGGER.debug("Guardar tramiteDocumento:" + tramiteDocumento.
                                        getId());

                                    //Se almacena el documento de resoluciones asociandolo al tramite 
                                    tramite.setResultadoDocumento(documento);
                                    LOGGER.debug("Documento Almacenado en Tramite:" + tramite.
                                        getResultadoDocumento());

                                    tramite = tramiteService.guardarActualizarTramite2(tramite);
                                    LOGGER.debug("Guardar tramite:" + tramite);

                                    //Se verifica que quedo la relacion de Documento de Resoluciones con el Tramite
                                    if (tramite.getResultadoDocumento() == null &&
                                        tramite.getResultadoDocumento().getId() == null) {

                                        this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                                        LOGGER.debug("Error al asociar el documento de la resolución en el trámite");
                                        resultadoOk = false;
                                        this.userTransaction.commit();
                                    return resultadoOk;

                                    } else {
                                        this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_TERMINADO,null);
                                        LOGGER.debug(
                                            "Generacion Correcta Job con id:" + job.getId() +
                                            " estado: " + job.getEstado());
                                        this.userTransaction.commit();
                                        resultadoOk = true;
                                    }
                                }
   

                            } catch (Exception ex) {
                                try {
                                    if (contador == maxEnvioJob.getValorNumero()) {
                                        this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                                        LOGGER.error("revisar el job:" + job +
                                            "debido a que no se pudo realizar el reintento");
                                    }

                                    this.userTransaction.commit();
                                } catch (Exception e) {
                                    LOGGER.error("Error en la guardar Documento Tramite Resoluciones");
                                }
                            }

                        } else {
                            this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                            LOGGER.error("No se pudo generar el archivo de resoluciones para el tramite_id:" +
                                tramite.getId());
                            
                            resultadoOk = false;
                            this.userTransaction.commit();
                            return resultadoOk;
                        }
                    }

                    
                } else {
                    this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_ERROR,null);
                    LOGGER.error("No se encontro tramite para el job:" + job);
                    this.userTransaction.commit();
                    
                }
                
                
                
            } else {
                this.productoCatastralJobDAO.actualizar(job,ProductoCatastralJob.JPS_TERMINADO,null);
                noPrediosProyectados = true;
                return noPrediosProyectados;
            }
            
        } catch (ExcepcionSNC e) {
            
            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + e.getMessage(), e);

            //reintento del job
            try {

                if (contador <= maxEnvioJob.getValorNumero()) {
                    this.generarResolucionesConservacionEnJasperServerYGuardar(job, contador);
                    
                } else if (contador == maxEnvioJob.getValorNumero()) {
                    resultadoOk = false;
                    this.productoCatastralJobDAO.actualizar(job, ProductoCatastralJob.JPS_ERROR,
                        null);
                    LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                    return resultadoOk;

                }
                this.userTransaction.commit();
            } catch (Exception eS) {
                
                LOGGER.error("Error en la guardar Documento Tramite Resoluciones",eS);
            }
            
        }catch (Exception ex) {
            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + ex.getMessage(), ex);

            //reintento de job
            try {

                if (contador <= maxEnvioJob.getValorNumero()) {
                    errorGeneracionReporte = !this.
                        generarResolucionesConservacionEnJasperServerYGuardar(job,
                            contador);
                } else if (contador == maxEnvioJob.getValorNumero()) {
                    resultadoOk = false;
                    this.productoCatastralJobDAO.actualizar(job, ProductoCatastralJob.JPS_ERROR,
                        null);
                    LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                    return resultadoOk;
                }
                this.userTransaction.commit();
            } catch (Exception e) {
                
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar", e);
            }
        }
        
        if (errorGeneracionReporte) {
            return false;
        }

        if (noPrediosProyectados) {
            return true;
        }
        
       return resultadoOk; 
    }
}
