package co.gov.igac.snc.fachadas;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.contenedores.Actividades;
import co.gov.igac.snc.apiprocesos.contenedores.ArbolProcesos;
import co.gov.igac.snc.apiprocesos.contenedores.ObjetoNegocio;
import co.gov.igac.snc.apiprocesos.contenedores.RutaActividad;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaContingencia;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaFrecuencia;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.protocolo.rest.APIProcesosRespuestaRest;
import co.gov.igac.snc.apiprocesos.protocolo.rest.ETipoPeticion;
import co.gov.igac.snc.apiprocesos.protocolo.rest.ParametrosServicioRest;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionActividadesUsuario;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionAvanzarActividad;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionAvanzarActividadSimple;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionCancelarProceso;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionObtenerFlujoProceso;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionObtenerObjetoNegocio;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionReclamarActividad;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionSuspenderActividad;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionTransferirActividad;
import co.gov.igac.snc.apiprocesos.protocolo.rest.RespuestaConteoActividad;
import co.gov.igac.snc.apiprocesos.protocolo.rest.RespuestaExcepcion;
import co.gov.igac.snc.apiprocesos.protocolo.rest.RespuestaFlujoEjecucionProceso;
import co.gov.igac.snc.apiprocesos.servicio.excepciones.ManejadorExcepcionesProcesos;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.ETipoActividadSNC;
import co.gov.igac.snc.comun.colecciones.Par;
import co.gov.igac.snc.comun.utilerias.Duration;
import co.gov.igac.snc.comun.utilerias.GestorArbolProcesos;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.procesos.Entidades.BpmFlujoTramite;
import co.gov.igac.snc.procesos.FlujoBpmBean;
import co.gov.igac.snc.procesos.FlujoBpmDAOBean;
import co.gov.igac.snc.util.SNCPropertiesUtil;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import javax.ejb.EJB;

@Stateless
//@Interceptors(TimeInterceptor.class)
public class ProcesosBean implements IProcesos, IProcesosLocal {

    public static final String SNC_PREFIJO_TAREAS_HUMANAS = "snc.apiprocesos.tareas.prefijo";
    private static final String URL_SERVICIO_REST_PROCESOS;
    private static final String URL_SERVICIO_REST_TAREAS;
    private static final String URL_SERV_REST_CREAR_PROCESO_CONS;
    private static final String URL_SERV_REST_CREAR_PROCESO_ACT;
    private static final String URL_SERV_REST_CREAR_PROCESO_OSMI;
    private static final String URL_SERV_REST_CREAR_PROCESO_OSMI_CONTROL_CALIDAD;
    private static final String URL_SERV_REST_CREAR_PROCESO_PRODUCTOS_CATASTRALES;
    private static final String URL_SERV_REST_CREAR_PROCESOS_ACTUALIZACION_EXPRESS;
    private static final String URL_SERV_REST_CREAR_PROCESOS_CONSERVACION;
    private static final String URL_SERV_REST_REANUDAR_PROCESOS_ACTUALIZACION_EXPRESS;

    private static final String URL_SERV_REST_OBTENER_ESTADO_PROCESO;
    private static final String URL_SERV_REST_OBTENER_ACTIVIDADES_PROCESO;
    private static final String URL_SERV_REST_CANCELAR_PROCESO_POR_ID;
    private static final String URL_SERV_REST_OBTENER_ACTIVIDADES_USUARIO;
    private static final String URL_SERV_REST_OBTENER_ACTIVIDADES_CON_USUARIO_FILTRO;
    private static final String URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION;
    private static final String URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION_SIMPLE;
    private static final String URL_SERV_REST_AVANZAR_ACTIVIDAD_ACTUALIZACION;
    private static final String URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI;
    private static final String URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI_CONTROL_CALIDAD;
    private static final String URL_SERV_REST_AVANZAR_ACTIVIDAD_PRODUCTOS_CATASTRALES;
    private static final String URL_SERV_REST_RECLAMAR_ACTIVIDAD_CONSERVACION;
    private static final String URL_SERV_REST_OBTENER_ESTADO_ACTIVIDAD;
    private static final String URL_SERV_REST_REANUDAR_ACTIVIDAD;
    private static final String URL_SERV_REST_SUSPENDER_ACTIVIDAD;
    private static final String URL_SERV_REST_OBTENER_OBJ_NEGOCIO_CONSERVACION;
    private static final String URL_SERV_REST_OBTENER_OBJ_NEGOCIO_ACTUALIZACION;
    private static final String URL_SERV_REST_OBTENER_OBJ_NEGOCIO_PROCESO;
    private static final String URL_SERV_REST_OBTENER_CONTEO_ACT_USUARIO;
    private static final String URL_SERV_REST_OBTENER_CONTEO_ACT_USUARIO_CON_FILTRO;
    private static final String URL_SERV_REST_OBTENER_ACTIVIDADES_CON_FILTRO;
    private static final String URL_SERV_REST_TRANSFERIR_ACTIVIDAD;
    private static final String URL_SERV_REST_OBTENER_FLUJO_EJECUCION_PROCESO;
    private static final String URL_SERV_REST_OBTENER_USUARIOS_ACTIVIDAD;
    private static final String URL_SERV_REST_REPLANIFICAR_ACTIVIDAD;
    
    static {
        URL_SERVICIO_REST_PROCESOS = SNCPropertiesUtil
            .getInstance()
            .getProperty(
                ParametrosServicioRest.SNC_APIPROCESOS_PROCESOS_REST_URL);
        URL_SERVICIO_REST_TAREAS = SNCPropertiesUtil.getInstance().getProperty(
            ParametrosServicioRest.SNC_APIPROCESOS_TAREAS_REST_URL);
        URL_SERV_REST_CREAR_PROCESO_CONS = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESO_CONSERVACION;
        URL_SERV_REST_CREAR_PROCESOS_ACTUALIZACION_EXPRESS = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESOS_ACTUALIZACION_EXPRESS;
        URL_SERV_REST_REANUDAR_PROCESOS_ACTUALIZACION_EXPRESS = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.REANUDAR_PROCESOS_ACTUALIZACION_EXPRESS;
        URL_SERV_REST_CREAR_PROCESO_ACT = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESO_ACTUALIZACION;
        URL_SERV_REST_CREAR_PROCESO_OSMI = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESO_OSMI;
        URL_SERV_REST_CREAR_PROCESO_OSMI_CONTROL_CALIDAD = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESO_OSMI_CONTROL_CALIDAD;
        URL_SERV_REST_CREAR_PROCESO_PRODUCTOS_CATASTRALES = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESO_PRODUCTOS_CATASTRALES;
        URL_SERV_REST_CREAR_PROCESOS_CONSERVACION = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CREAR_PROCESOS_CONSERVACION;
        URL_SERV_REST_OBTENER_ESTADO_PROCESO = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.OBTENER_ESTADO_PROCESO;
        URL_SERV_REST_OBTENER_ACTIVIDADES_PROCESO = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.OBTENER_ACTIVIDADES_PROCESO;
        URL_SERV_REST_CANCELAR_PROCESO_POR_ID = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.CANCELAR_POR_ID_PROCESO;
        URL_SERV_REST_OBTENER_ACTIVIDADES_USUARIO = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_TAREAS_POR_USUARIO;
        URL_SERV_REST_OBTENER_ACTIVIDADES_CON_USUARIO_FILTRO = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_ACTIVIDADES_CON_USUARIO_FILTRO;
        URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.AVANZAR_ACTIVIDAD_CONSERVACION;
        URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION_SIMPLE = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.AVANZAR_ACTIVIDAD_CONSERVACION_SIMPLE;
        URL_SERV_REST_AVANZAR_ACTIVIDAD_ACTUALIZACION = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.AVANZAR_ACTIVIDAD_ACTUALIZACION;
        URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.AVANZAR_ACTIVIDAD_OSMI;
        URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI_CONTROL_CALIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.AVANZAR_ACTIVIDAD_OSMI_CONTROL_CALIDAD;

        URL_SERV_REST_AVANZAR_ACTIVIDAD_PRODUCTOS_CATASTRALES = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.AVANZAR_ACTIVIDAD_PRODUCTOS;

        URL_SERV_REST_OBTENER_ACTIVIDADES_CON_FILTRO = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_ACTIVIDADES_CON_FILTRO;
        URL_SERV_REST_RECLAMAR_ACTIVIDAD_CONSERVACION = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.RECLAMAR_ACT_CONSERVACION;

        URL_SERV_REST_OBTENER_ESTADO_ACTIVIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_ESTADO_ACTIVIDAD;

        URL_SERV_REST_OBTENER_OBJ_NEGOCIO_CONSERVACION = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_OBJ_NEGOCIO_CONSERVACION;
        URL_SERV_REST_OBTENER_OBJ_NEGOCIO_ACTUALIZACION = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_OBJ_NEGOCIO_ACTUALIZACION;

        URL_SERV_REST_OBTENER_OBJ_NEGOCIO_PROCESO = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.OBTENER_OBJ_NEGOCIO_PROCESO;

        URL_SERV_REST_OBTENER_CONTEO_ACT_USUARIO = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_CONTEO_ACTIVIDADES_USUARIO;
        URL_SERV_REST_OBTENER_CONTEO_ACT_USUARIO_CON_FILTRO = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_CONTEO_ACTIVIDADES_Y_FILTRO;

        URL_SERV_REST_REANUDAR_ACTIVIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.REANUDAR_ACTIVIDAD;

        URL_SERV_REST_SUSPENDER_ACTIVIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.SUSPENDER_ACTIVIDAD;

        URL_SERV_REST_TRANSFERIR_ACTIVIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.TRANSFERIR_ACTIVIDAD;

        URL_SERV_REST_OBTENER_FLUJO_EJECUCION_PROCESO = URL_SERVICIO_REST_PROCESOS +
            ParametrosServicioRest.OBTENER_FLUJO_EJECUCION_PROCESO;

        URL_SERV_REST_OBTENER_USUARIOS_ACTIVIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.OBTENER_USUARIOS_ACTIVIDAD;

        URL_SERV_REST_REPLANIFICAR_ACTIVIDAD = URL_SERVICIO_REST_TAREAS +
            ParametrosServicioRest.REPLANIFICAR_ACTIVIDAD;
    }

    private static final String RESULTADO_SATISFACTORIO = "OK";
    private static final String ORGANIZACION = "SNC";

    @EJB
    private IGeneralesLocal generalesService;
    
    @EJB
    private FlujoBpmBean flujoBpmBean;
    
     @EJB
    private FlujoBpmDAOBean flujoBpmBeanDAO;
    
    
    public static final Logger LOGGER = LoggerFactory.getLogger(ProcesosBean.class);

    private ObjectMapper jsonMapper;

    private static List<String> rolesSNC;
    
    
    
    @PostConstruct
    public void init() {
        jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        rolesSNC = this.generalesService.obtenerListaNombresGrupo("SI");
    }

    @Override
    public String crearProceso(OfertaInmobiliaria ofertaInmobiliaria) {
        String resultado = null;
        try {

            //CREAR SERVICIO
        
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC()
                .getExcepcion(LOGGER, e,
                    "la creación del proceso de ofertas inmobiliarias.");
        }
        return resultado;
    }

    @Override
    public String crearProceso(SolicitudCatastral solicitudCatastral) {
        String resultado = null;
        SolicitudCatastral Actividad = null;
        try {                 

            //75433::juan.cruz:: Se filtran a solo roles SNC de los listados de usuarios 
           
            cambiarRolesDeActividadesUsuario(solicitudCatastral.getActividadesUsuarios());
           
            LOGGER.info("===================CREAR PROCESO CONSERVACION=================");
            Actividad = flujoBpmBean.crearProcesoConservacion(solicitudCatastral);
            resultado = Actividad.getResultado();
        
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la creación del proceso de conservación.");
        }
        return resultado;
    }

    @Override
    public String crearProceso(SolicitudProductoCatastral solicitudProductoCatastral) {
        SolicitudProductoCatastral Actividad = null;
        String resultado = null;
        try {
            LOGGER.info("===================CREAR PROCESO PRODUCTOS_CATASTRALES=================");
            Actividad = flujoBpmBean.crearProcesoProductosCatastrales(solicitudProductoCatastral);
            resultado = Actividad.getResultado();           

        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la creación del proceso de conservación.");
        }
        return resultado;
    }

    @Override
    public void crearProcesosActualizacionExpress(String municipioCodigo) {
        try {
            
            
            //CREAR SERVICIO 
        
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC()
                .getExcepcion(LOGGER, e,
                    "la creación masiva de procesos de actualización express.");
        }
    }

    @Override
    public void crearProcesosConservacion(List<SolicitudCatastral> solicitudesCatastrales) {
        try {

            //75433::juan.cruz:: Se filtran a solo roles SNC de los listado de usuarios 
            for (SolicitudCatastral unaSolicitudCatastral : solicitudesCatastrales) {
                cambiarRolesDeActividadesUsuario(unaSolicitudCatastral.getActividadesUsuarios());
            }

        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC()
                .getExcepcion(LOGGER, e,
                    "la creación masiva de procesos de actualización express.");
        }
    }

    @Override
    public void reanudarProcesosActualizacionExpress(String municipioCodigo) {
        try {
            
            
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC()
                .getExcepcion(LOGGER, e,
                    "la reanudación masiva de procesos de actualización express.");
        }
    }

    @Override
    public String crearProceso(ActualizacionCatastral actualizacionMunicipio) {
        String resultado = null;
        try {
            
            //CREAR SERVICIO
            
            
        }catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la creación del proceso de actualización.");
        }
        return resultado;
    }

    @Override
    public String obtenerEstadoProceso(String idProceso) {
        String resultado = null;
        try {
//            Client client = Client.create();
//            WebResource webResource = client
//                .resource(URL_SERV_REST_OBTENER_ESTADO_PROCESO);
//            webResource.accept(MediaType.APPLICATION_JSON_TYPE);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, idProceso);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                resultado = result.getResultado();
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener el estado del proceso.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener el estado del proceso.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener el estado del proceso.");
        }
        return resultado;
    }

    @Override
    public List<Actividad> obtenerActividadesProceso(String idProceso) {
        List<Actividad> actividades = null;
        try {
            
            actividades = flujoBpmBean.obtenerActividadesProceso(idProceso);

        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un proceso.");
        }
        return actividades;
    }

    @Override
    public String obtenerFlujoEjecucionProceso(String idActividad) {
        PeticionObtenerFlujoProceso flujoProceso = new PeticionObtenerFlujoProceso();
        flujoProceso.setIdObjeto(idActividad);
        String nombre = idActividad.replace(SNCPropertiesUtil.getInstance().getProperty(
            SNC_PREFIJO_TAREAS_HUMANAS), "");
        return obtenerImagenFlujoProceso(flujoProceso, nombre);
    }

    @Override
    public String obtenerFlujoEjecucionProceso(String mapaProceso, long idObjetoNegocio) {
        PeticionObtenerFlujoProceso flujoProceso = new PeticionObtenerFlujoProceso();
        flujoProceso.setTipoPeticion(ETipoPeticion.OBJETO_NEGOCIO.getTipo());
        flujoProceso.setMapaProceso(mapaProceso);
        flujoProceso.setIdObjeto(Long.toString(idObjetoNegocio));
        String nombre = mapaProceso.concat(Long.toString(idObjetoNegocio));
        return obtenerImagenFlujoProceso(flujoProceso, nombre);
    }

    @Override
    public boolean cancelarProcesoPorIdProceso(String idProceso, String motivo) {
        Boolean resultado = false;
        try {
            
            resultado = flujoBpmBean.cancelarProcesoPorIdProceso(idProceso, motivo);
            
        }  catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "cancelar una instancia de proceso.");
        }
        return resultado;
    }
//-------------------------------------------------------------------------------------------------

    @Override
    public ArbolProcesos consultarTareas(UsuarioDTO usuario) {
        GestorArbolProcesos constructorArbolProcesos = new GestorArbolProcesos(
            usuario.getLogin(), ORGANIZACION);
        long currentTimeMillis = System.currentTimeMillis();
        try {
                  
            LOGGER.info("INGRESA A REALIZAR CONTEO" );
            
            //flujoBpmBeanDAO.testUpdate();
            List<RespuestaConteoActividad> listaActividades = null;
            listaActividades = flujoBpmBean.obtenerConteoActividadesUsuario(usuario);     

            LOGGER.info("INGRESA CONDICIONAL IF 1 " );
            if (!listaActividades.contains(null)) {
                for (RespuestaConteoActividad conteoAct : listaActividades) {
                    // Filtrar únicamente actividades alfanuméricas
                    LOGGER.info("TIPO ACTIVIDAD: " +
                        conteoAct.getTipoActividad());
                    if (conteoAct.getTipoActividad() == null ||
                        !conteoAct.getTipoActividad().equals(
                            ETipoActividadSNC.GEOGRAFICA.toString())) {
                        Actividades actividades = new Actividades(
                            conteoAct.getRutaActividad(),
                            conteoAct.getConteo());
                        actividades.setTipoActividad(conteoAct
                            .getTipoActividad());
                        actividades.setTipoProcesamiento(conteoAct
                            .getTipoProcesamiento());
                        actividades
                            .setTransiciones(conteoAct.getTransiciones());
                        actividades
                            .setUrlCasoDeUso(conteoAct.getUrlCasoDeUso());
                        constructorArbolProcesos.addNodoActividad(actividades);
                    }
                }
            } else {
                
                LOGGER.info("ERROR ARMANDO ARBOL DE TAREAS");
            }
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un usuario.");
        } 
        LOGGER.debug("Contrucción de respuesta: " +
            (System.currentTimeMillis() - currentTimeMillis));
        return constructorArbolProcesos.getArbolProcesos();
    }

//--------------------------------------------------------------------------------------------------
    @Override
    public List<Actividad> consultarListaActividades(UsuarioDTO usuario) {
        Map<EParametrosConsultaActividades, String> condicionales =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        condicionales.put(EParametrosConsultaActividades.TERRITORIAL, usuario.
            getDescripcionEstructuraOrganizacional());
        condicionales.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
        condicionales.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, usuario.getLogin());
        return consultarListaActividades(condicionales);
    }

    @Override
    public List<Actividad> consultarListaActividadesPorActividades(UsuarioDTO usuario) {
        Map<EParametrosConsultaActividades, String> condicionales =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        condicionales.put(EParametrosConsultaActividades.TERRITORIAL, usuario.
            getDescripcionEstructuraOrganizacional());
        condicionales.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
        condicionales.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, usuario.getLogin());
        condicionales.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD,
            ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA + "," +
            ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA +
            "," + ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION + "," +
            ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR + "," +
            ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA);

        return consultarListaActividades(condicionales);
    }

    @Override
    public List<Actividad> consultarListaActividades(UsuarioDTO usuario, ERol rol) {
        Map<EParametrosConsultaActividades, String> condicionales =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        //condicionales.put(EParametrosConsultaActividades.TERRITORIAL, usuario.getDescripcionEstructuraOrganizacional());
        condicionales.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
        //condicionales.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, usuario.getLogin());
        condicionales.put(EParametrosConsultaActividades.PROPIETARIO_ACTIVIDAD, usuario.getLogin());
        //condicionales.put(EParametrosConsultaActividades.ROL_CONSERVACION, rol.getRol());
        return consultarListaActividades(condicionales);
    }

    @Override
    public List<Actividad> consultarListaActividades(UsuarioDTO usuario,
        String idActividad) {
        long currentTimeMillis = System.currentTimeMillis();
        List<Actividad> actividades = null;
        
        try {
            LOGGER.info("INGRESA A OBTENER TAREAS POR USUARIO CON LA ACTIVIDAD = " + idActividad);
            
            actividades = flujoBpmBean.obtenerTareasPorUsuario(usuario , idActividad);
            
        } catch (ExcepcionSNC e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un usuario.");
        }
        LOGGER.debug("Contrucción de respuesta: " +
            (System.currentTimeMillis() - currentTimeMillis) + ", registros: " + actividades.size());
        return actividades;
    }

    @Override
    public List<Actividad> consultarListaActividadesFiltroNombreActividad(
        UsuarioDTO usuario, String nombre) {
        long currentTimeMillis = System.currentTimeMillis();
        List<Actividad> actividades = null;
        try {

//            String json = jsonMapper.writeValueAsString(usuario);
//            Form formData = new Form();
//            formData.add(ParametrosServicioRest.USUARIO_DTO, json);
//            formData.add(ParametrosServicioRest.FILTRO, nombre);
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_ACTIVIDADES_CON_USUARIO_FILTRO);
//            ClientResponse response = webResource.type(
//                MediaType.APPLICATION_FORM_URLENCODED).post(
//                    ClientResponse.class, formData);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                actividades = jsonMapper.readValue(result.getResultado(),
//                    new TypeReference<List<Actividad>>() {
//                });
//                LOGGER.debug("Contrucción de respuesta: " +
//                    (System.currentTimeMillis() - currentTimeMillis) + ", registros: " +
//                    actividades.size());
//                return actividades;
//
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "la consulta de actividades de un usuario.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "la consulta de actividades de un usuario.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un usuario.");
        }
        return actividades;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<Actividad> consultarListaActividades(String territorial,
        String nombreActividad) {
        Map<EParametrosConsultaActividades, String> parametros =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        parametros.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD,
            nombreActividad);
        parametros.put(EParametrosConsultaActividades.TERRITORIAL, territorial);
        return consultarListaActividades(parametros);
    }

    @Override
    public List<Actividad> consultarListaActividades(
        Map<EParametrosConsultaActividades, String> parametros) {
        List<Actividad> actividades = null;
        long currentTimeMillis = System.currentTimeMillis();    
        try {
            LOGGER.info("INGRESA A LISTA ACTIVIDADES CON FILTRO = ");
            
            List<Par> filtros = new ArrayList<Par>();
            Set<EParametrosConsultaActividades> tipoFiltro = parametros
                .keySet();
            for (EParametrosConsultaActividades llave : tipoFiltro) {
                filtros.add(new Par(llave.toString(), parametros.get(llave)));
            }
            
            for (int i = 0; i < filtros.size(); i++){
                
                LOGGER.info("VALOR DE FILTROS getAtributo ========= " + filtros.get(i).getAtributo());
                LOGGER.info("VALOR DE FILTROS getValor ========= " + filtros.get(i).getValor());
            
            }         
            
            actividades = flujoBpmBean.obtenerListaActividadesConFiltro(filtros);
            
            
        } catch (ExcepcionSNC e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un usuario.");
        }
        LOGGER.debug("Contrucción de respuesta: " +
            (System.currentTimeMillis() - currentTimeMillis) + ", registros: " + actividades.size());
        return actividades;
    }

    @Override
    public List<Actividad> getActividadesPorIdObjetoNegocio(Long idObjNegocio) {
        return getActividadesPorIdObjetoNegocio(idObjNegocio, null);
    }

    @Override
    public List<Actividad> getActividadesPorIdObjetoNegocio(Long idObjNegocio,
        String nombreActividad) {
        List<Actividad> actividades = null;
        /**
         * Se modifica el método para retornar una lista con sólo la última actividad.
         *
         */
        Map<EParametrosConsultaActividades, String> parametros =
            new HashMap<EParametrosConsultaActividades, String>();
        /**
         * Para este caso se retornará la última actividad del proceso, sin tener en cuenta el
         * estado de la tarea que puede ser Finalizada, lista o reclamada.
         */
        parametros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD,
            Long.toString(idObjNegocio));

        if (nombreActividad != null && !nombreActividad.isEmpty()) {
            parametros.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD,
                nombreActividad);
        }
        actividades = consultarListaActividades(parametros);

        return actividades;

        // Se deja el código anterior para trazabilidad de cambios. Favor
        // borrarlo cuando se integren cambios
        // al trunk.
        /*
         * PeticionActividadesPorIdObjNegocio peticion = new PeticionActividadesPorIdObjNegocio();
         * peticion.setIdObjetoNegocio(idObjNegocio); if (nombreActividad != null) {
         * peticion.setNombreActividad(nombreActividad); } String json =
         * jsonMapper.writeValueAsString(peticion); Client c = Client.create(); WebResource
         * webResource = c .resource(URL_SERV_REST_OBTENER_ACTIVIDADES_POR_ID_OBJ_NEGOCIO);
         * ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
         * .post(ClientResponse.class, json); APIProcesosRespuestaRest result =
         * jsonMapper.readValue( response.getEntity(String.class), APIProcesosRespuestaRest.class);
         * if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) { actividades =
         * jsonMapper.readValue(result.getResultado(), new TypeReference<List<Actividad>>() { });
         * return actividades; } else { throw lanzarExcepcion(result); }
         */
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public SolicitudCatastral avanzarActividad(String idActividad,
        SolicitudCatastral solicitudCatastral) {

        SolicitudCatastral answer = null;

        try {
            answer = avanzarActividad(null, idActividad, solicitudCatastral);
        } catch (ExcepcionSNC sncEx) {
            LOGGER.error("Error en ProcesosBean#avanzarActividad: " + sncEx.getMensaje(), sncEx);
        }

        return answer;
    }

    @Override
    public SolicitudProductoCatastral avanzarActividad(String idActividad,
        SolicitudProductoCatastral solicitudProductoCatastral) {
        return avanzarActividad(null, idActividad, solicitudProductoCatastral);
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public ActualizacionCatastral avanzarActividad(String idActividad,
        ActualizacionCatastral actualizacionCatastral) {
        ActualizacionCatastral resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_ACTUALIZACION);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(actualizacionCatastral));
//            avanceActividad.setIdObjeto(idActividad);
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    ActualizacionCatastral.class);
                /*
                 * Esto es necesario porque en el servicio Rest de IBM se presenta un error en la
                 * serialización del objeto usuarios que hace parte de la solicitud catastral. Dicho
                 * objeto es retornado cuando se avanza la actividad en el servidor de procesos.
                 */
//                resultado.setUsuarios(actualizacionCatastral.getUsuarios());
//
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de actualización");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de actualización");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de actualización");
        }
        return resultado;
    }

    @Override
    public OfertaInmobiliaria avanzarActividad(String idActividad,
        OfertaInmobiliaria ofertaInmobiliaria) {
        return avanzarActividad(null, idActividad, ofertaInmobiliaria);
    }

    @Override
    @Deprecated
    public SolicitudCatastral avanzarActividad(String idActividad,
        String transicion, List<UsuarioDTO> usuarios, String observaciones) {
        SolicitudCatastral resultado = null;
        try {

//            for (UsuarioDTO usuario : usuarios) {
//                cambiarRolUsuario(usuario);
//            }
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION_SIMPLE);
//            PeticionAvanzarActividadSimple avanceActividadSimple =
//                new PeticionAvanzarActividadSimple();
//            avanceActividadSimple.setIdActividad(idActividad);
//            avanceActividadSimple.setTransicion(transicion);
//            avanceActividadSimple.setObservacion(observaciones);
//            avanceActividadSimple.setUsuarios(jsonMapper
//                .writeValueAsString(usuarios));
//            String peticion = jsonMapper
//                .writeValueAsString(avanceActividadSimple);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    SolicitudCatastral.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        return resultado;
    }

    @Override
    public SolicitudCatastral avanzarActividadConservacion(String idActividad,
        Duration duracion, SolicitudCatastral solicitudCatastral) {
        if (duracion != null) {

            solicitudCatastral.setDuracion(duracion);
        }
        return avanzarActividad(null, idActividad, solicitudCatastral);
    }

    @Override
    @Deprecated
    public SolicitudCatastral avanzarActividadConservacion(String idProceso,
        String idActividad, Duration duracion,
        SolicitudCatastral solicitudCatastral) {
        return avanzarActividadConservacion(idActividad, duracion,
            solicitudCatastral);
    }

    @Override
    public OfertaInmobiliaria avanzarActividadPorIdProceso(String idProceso,
        OfertaInmobiliaria ofertaInmobiliaria) {

        OfertaInmobiliaria resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//
//            avanceActividad.setIdObjeto(idProceso);
//            avanceActividad.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(ofertaInmobiliaria));
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    OfertaInmobiliaria.class);
//                /*
//                 * Este llamado del set es necesario porque en el servicio Rest de IBM se presenta
//                 * un error en la serialización del objeto usuarios que hace parte de la solicitud
//                 * catastral. Dicho objeto es retornado cuando se avanza la actividad en el servidor
//                 * de procesos.
//                 */
//                resultado.setUsuarios(ofertaInmobiliaria.getUsuarios());
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IProcesosLocal#avanzarActividad(co.gov.igac.generales.dto.UsuarioDTO, java.lang.String,
     * co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral)
     */
    @Override
    public SolicitudCatastral avanzarActividad(UsuarioDTO usuarioActActual,
        String idActividad, SolicitudCatastral solicitudCatastral) {
        SolicitudCatastral resultado = null;
        
        try {

            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();

            if (usuarioActActual != null) {
                cambiarRolUsuario(usuarioActActual);
                avanceActividad.setUsuario(jsonMapper
                    .writeValueAsString(usuarioActActual));
            }

            if (solicitudCatastral.getActividadesUsuarios() != null) {
                for (ActividadUsuarios actividadUsuario : solicitudCatastral.
                    getActividadesUsuarios()) {
                    if (actividadUsuario != null && actividadUsuario.getUsuarios() != null) {
                        for (UsuarioDTO usuario : actividadUsuario.getUsuarios()) {
                            LOGGER.info(usuario.getLogin() + " "+usuario.getNombreCompleto());
                            cambiarRolUsuario(usuario);
                        }
                    }else {
                        LOGGER.info("actividadUsuario.getUsuarios esta vacio");
                    }
                }
            }else {
                LOGGER.info("solicitudCatastral.getActividadesUsuarios esta vacia: "+solicitudCatastral);
            }
            
            

            avanceActividad.setIdObjeto(idActividad);
            
            
            resultado =  flujoBpmBean.avanzarActividadConservacion(solicitudCatastral, avanceActividad);
            
            LOGGER.info("REALIZA EL AVANCC DE LA ACTIVIDAD : METHOD avanceActividad: " + resultado);
                    
            
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
            
                
                /*
                 * Este llamado del set es necesario porque en el servicio Rest de IBM se presenta
                 * un error en la serialización del objeto usuarios que hace parte de la solicitud
                 * catastral. Dicho objeto es retornado cuando se avanza la actividad en el servidor
                 * de procesos.
                 */
                //75433::juan.cruz:: Se filtran a solo roles SNC del listado de usuarios 
                if (solicitudCatastral.getUsuarios() != null) {
                    for (UsuarioDTO usuario : solicitudCatastral.getUsuarios()) {
                        cambiarRolUsuario(usuario);
                    }
                }
                resultado.setUsuarios(solicitudCatastral.getUsuarios());


        } catch (JsonGenerationException e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        } catch (JsonMappingException e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        } catch (IOException e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        } 
        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public SolicitudProductoCatastral avanzarActividad(UsuarioDTO usuarioActActual,
        String idActividad, SolicitudProductoCatastral solicitudProductoCatastral) {
        
        SolicitudProductoCatastral resultado = null;
        try {
 
            
            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
            if (usuarioActActual != null) {
                avanceActividad.setUsuario(jsonMapper
                    .writeValueAsString(usuarioActActual));
            }
            avanceActividad.setIdObjeto(idActividad);
            avanceActividad.setObjetoNegocio(jsonMapper
                .writeValueAsString(solicitudProductoCatastral));
            
            resultado =  flujoBpmBean.avanzarActividadProductos(solicitudProductoCatastral, avanceActividad);
            
            LOGGER.info("REALIZA EL AVANCE PRODUCTO_CATASTRAL DE LA ACTIVIDAD : METHOD avanceActividad: " + resultado);
            
            resultado.setUsuarios(solicitudProductoCatastral.getUsuarios());

//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    SolicitudProductoCatastral.class);
//                /*
//                 * Este llamado del set es necesario porque en el servicio Rest de IBM se presenta
//                 * un error en la serialización del objeto usuarios que hace parte de la solicitud
//                 * catastral. Dicho objeto es retornado cuando se avanza la actividad en el servidor
//                 * de procesos.
//                 */
//                resultado.setUsuarios(solicitudProductoCatastral.getUsuarios());
//            } else {
//                throw lanzarExcepcion(result);
//            }
        } catch (JsonGenerationException e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        } catch (JsonMappingException e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        return resultado;
    }

    @Override
    public OfertaInmobiliaria avanzarActividad(
        UsuarioDTO usuarioEjecutorActActual, String idActividad,
        OfertaInmobiliaria ofertaInmobiliaria) {
        OfertaInmobiliaria resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(ofertaInmobiliaria));
//            avanceActividad.setIdObjeto(idActividad);
//            if (usuarioEjecutorActActual != null) {
//                avanceActividad.setUsuario(jsonMapper
//                    .writeValueAsString(usuarioEjecutorActActual));
//            }
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    OfertaInmobiliaria.class);
                /*
                 * Esto es necesario porque en el servicio Rest de IBM se presenta un error en la
                 * serialización del objeto usuarios que hace parte de la solicitud catastral. Dicho
                 * objeto es retornado cuando se avanza la actividad en el servidor de procesos.
                 */
//                resultado.setUsuarios(ofertaInmobiliaria.getUsuarios());
//
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de actualización");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de actualización");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de actualización");
        }
        return resultado;
    }

    @Override
    public SolicitudCatastral avanzarActividadPorIdProceso(
        UsuarioDTO usuarioEjecutorActActual, String idProceso,
        SolicitudCatastral solicitudCatastral) {
        SolicitudCatastral resultado = null;
        try {

//            if (usuarioEjecutorActActual != null) {
//                cambiarRolUsuario(usuarioEjecutorActActual);
//            }
//
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//
//            avanceActividad.setIdObjeto(idProceso);
//            avanceActividad.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(solicitudCatastral));
//            if (usuarioEjecutorActActual != null) {
//                avanceActividad.setUsuario(jsonMapper
//                    .writeValueAsString(usuarioEjecutorActActual));
//            }
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    SolicitudCatastral.class);
                /*
                 * Este llamado del set es necesario porque en el servicio Rest de IBM se presenta
                 * un error en la serialización del objeto usuarios que hace parte de la solicitud
                 * catastral. Dicho objeto es retornado cuando se avanza la actividad en el servidor
                 * de procesos.
                 */
                //75433::wilma.vega:: Se filtran a solo roles SNC del listado de usuarios 
//                if (solicitudCatastral.getUsuarios() != null) {
//                    for (UsuarioDTO usuario : solicitudCatastral.getUsuarios()) {
//                        cambiarRolUsuario(usuario);
//                    }
//                }
//                resultado.setUsuarios(solicitudCatastral.getUsuarios());
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        return resultado;
    }

    @Override
    public OfertaInmobiliaria avanzarActividadPorIdProceso(
        UsuarioDTO usuarioEjecutorActActual, String idProceso,
        OfertaInmobiliaria ofertaInmobiliaria) {
        OfertaInmobiliaria resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//
//            avanceActividad.setIdObjeto(idProceso);
//            avanceActividad.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(ofertaInmobiliaria));
//            if (usuarioEjecutorActActual != null) {
//                avanceActividad.setUsuario(jsonMapper
//                    .writeValueAsString(usuarioEjecutorActActual));
//            }
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    OfertaInmobiliaria.class);
                /*
                 * Este llamado del set es necesario porque en el servicio Rest de IBM se presenta
                 * un error en la serialización del objeto usuarios que hace parte de la solicitud
                 * catastral. Dicho objeto es retornado cuando se avanza la actividad en el servidor
                 * de procesos.
                 */
//                resultado.setUsuarios(ofertaInmobiliaria.getUsuarios());
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        return resultado;
    }

    @Override
    public SolicitudCatastral avanzarActividadPorIdProceso(String idProceso,
        SolicitudCatastral solicitudCatastral) {
        SolicitudCatastral resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_CONSERVACION);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//
//            avanceActividad.setIdObjeto(idProceso);
//            avanceActividad.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(solicitudCatastral));
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    SolicitudCatastral.class);
                /*
                 * Este llamado del set es necesario porque en el servicio Rest de IBM se presenta
                 * un error en la serialización del objeto usuarios que hace parte de la solicitud
                 * catastral. Dicho objeto es retornado cuando se avanza la actividad en el servidor
                 * de procesos.
                 */
                //75433::wilman.vega:: Se filtran a solo roles SNC del listado de usuarios 
//                if (solicitudCatastral.getUsuarios() != null) {
//                    for (UsuarioDTO usuario : solicitudCatastral.getUsuarios()) {
//                        cambiarRolUsuario(usuario);
//                    }
//                }
//                resultado.setUsuarios(solicitudCatastral.getUsuarios());
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        return resultado;
    }

    @Override
    @Deprecated
    public ActualizacionCatastral avanzarActividadActualizacion(
        String idActividad, String transicion, List<UsuarioDTO> usuarios,
        String observaciones) {
        // TODO: implementar;
        return null;
    }
//--------------------------------------------------------------------------------------------------
//TODO :: alejandro.sanchez :: documentar según el estándar del proyecto :: pedro.garcia

    @Override
    public Calendar reanudarActividad(String idActividad) {
        Calendar resultado = null;
        try {
//            Client client = Client.create();
//            WebResource webResource = client
//                .resource(URL_SERV_REST_REANUDAR_ACTIVIDAD);
//            webResource.accept(MediaType.APPLICATION_JSON_TYPE);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, idActividad);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                resultado = jsonMapper.readValue(result.getResultado(), Calendar.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener el estado de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener el estado de una actividad.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener el estado de una actividad.");
        }
        return resultado;
    }
//--------------------------------------------------------------------------------------------------
//TODO :: alejandro.sanchez :: documentar según el estándar del proyecto :: pedro.garcia    

    @Override
    public SolicitudCatastral reclamarActividad(String idActividad,
        UsuarioDTO usuario) throws ExcepcionSNC {
        SolicitudCatastral resultado = null;
        
        try {
            
            resultado = flujoBpmBean.reclamarActividadConservacion(idActividad, usuario);         
            
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "reclamar actividad de conservación");
        }
        return resultado;

    }

    @Override
    public String obtenerEstadoActividad(String idActividad) {
        String resultado = null;
        try {
//            Client client = Client.create();
//            WebResource webResource = client
//                .resource(URL_SERV_REST_OBTENER_ESTADO_ACTIVIDAD);
//            webResource.accept(MediaType.APPLICATION_JSON_TYPE);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, idActividad);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                resultado = result.getResultado();
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener el estado de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener el estado de una actividad.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener el estado de una actividad.");
        }
        return resultado;
    }

    @Override
    public SolicitudCatastral obtenerObjetoNegocioConservacion(
        String idActividad) {
        SolicitudCatastral resultado = null;
        try {

//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_OBJ_NEGOCIO_CONSERVACION);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, idActividad);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            
//            
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    SolicitudCatastral.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener objeto de negocio de una actividad.");
        }
        return resultado;
    }

    @Override
    public SolicitudCatastral obtenerObjetoNegocioConservacionPorIdProceso(String idProceso) {

        ObjetoNegocio objNegocio = obtenerObjetoNegocioPorIdProceso(idProceso,
            ProcesoDeConservacion.MAPA_PROCESO);
        if (!(objNegocio instanceof SolicitudCatastral)) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS
                .getExcepcion(LOGGER, null,
                    "El tipo de objeto retornado por el gestor de procesos es diferente " +
                    "al tipo que se esperaba.");
        }
        return (SolicitudCatastral) objNegocio;
    }

    @Override
    public ActualizacionCatastral obtenerObjetoNegocioActualizacionPorIdProceso(String idProceso) {

        ObjetoNegocio objNegocio = obtenerObjetoNegocioPorIdProceso(idProceso,
            ProcesoDeActualizacion.MAPA_PROCESO);
        if (!(objNegocio instanceof ActualizacionCatastral)) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS
                .getExcepcion(LOGGER, null,
                    "El tipo de objeto retornado por el gestor de procesos es diferente " +
                    "al tipo que se esperaba.");
        }
        return (ActualizacionCatastral) objNegocio;
    }

    @Override
    public ControlCalidad obtenerObjetoNegocioControlCalidadPorIdProceso(String idProceso) {

        ObjetoNegocio objNegocio = obtenerObjetoNegocioPorIdProceso(idProceso,
            ProcesoDeControlCalidad.MAPA_PROCESO);
        if (!(objNegocio instanceof ControlCalidad)) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS
                .getExcepcion(LOGGER, null,
                    "El tipo de objeto retornado por el gestor de procesos es diferente " +
                    "al tipo que se esperaba.");
        }
        return (ControlCalidad) objNegocio;
    }

    @Override
    public OfertaInmobiliaria obtenerObjetoNegocioOfertasInmobiliariasPorIdProceso(String idProceso) {

        ObjetoNegocio objNegocio = obtenerObjetoNegocioPorIdProceso(idProceso,
            ProcesoDeOfertasInmobiliarias.MAPA_PROCESO);
        if (!(objNegocio instanceof OfertaInmobiliaria)) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS
                .getExcepcion(LOGGER, null,
                    "El tipo de objeto retornado por el gestor de procesos es diferente " +
                    "al tipo que se esperaba.");
        }
        return (OfertaInmobiliaria) objNegocio;
    }

    @Override
    public ActualizacionCatastral obtenerObjetoNegocioActualizacion(
        String idActividad) {
        ActualizacionCatastral resultado = null;
        try {

//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_OBJ_NEGOCIO_ACTUALIZACION);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, idActividad);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    ActualizacionCatastral.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener objeto de negocio de una actividad.");
        }
        return resultado;
    }

    @Override
    public TablaFrecuencia getConteoActividadesUsuario(UsuarioDTO usuario) {
        long currentTimeMillis = System.currentTimeMillis();
        TablaFrecuencia tablaFrecuencia = null;
        try {
            
            List<RespuestaConteoActividad> listaActividades = null;
            listaActividades = flujoBpmBean.obtenerConteoActividadesUsuario(usuario);


                tablaFrecuencia = new TablaFrecuencia(usuario.getLogin());
                for (RespuestaConteoActividad conteoAct : listaActividades) {
                    RutaActividad rutaActividad = new RutaActividad(
                        conteoAct.getRutaActividad());
                    tablaFrecuencia.add(rutaActividad.getActividad(),
                        new Integer(conteoAct.getConteo()));
                }
                
            
        }catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un usuario.");
        }
        
        LOGGER.debug("Contrucción de respuesta: " +
            (System.currentTimeMillis() - currentTimeMillis));
        return tablaFrecuencia;
    }

    @Override
    public TablaContingencia getConteoActividadesConFiltro(UsuarioDTO usuario,
        String atributo) {
        long currentTimeMillis = System.currentTimeMillis();
        TablaContingencia resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_CONTEO_ACT_USUARIO_CON_FILTRO);
//            webResource.accept(MediaType.APPLICATION_FORM_URLENCODED);
//            String json = jsonMapper.writeValueAsString(usuario);
//
//            Form formData = new Form();
//            formData.add(ParametrosServicioRest.USUARIO_DTO, json);
//            formData.add(ParametrosServicioRest.FILTRO, atributo);
//            ClientResponse response = webResource.type(
//                MediaType.APPLICATION_FORM_URLENCODED).post(
//                    ClientResponse.class, formData);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    TablaContingencia.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de conservación");
        }
        LOGGER.debug("Contrucción de respuesta: " +
            (System.currentTimeMillis() - currentTimeMillis));
        return resultado;
    }

    @Override
    public SolicitudCatastral obtenerObjetoNegocioConservacion(
        String idProceso, String nombreActividad) {
        SolicitudCatastral resultado = null;
        try {
            
            resultado = flujoBpmBean.obtenerObjetoNegocioConservacion(idProceso, nombreActividad);

        }catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener objeto de negocio de una actividad.");
        }
        LOGGER.debug("Contrucción de respuesta tramite predial " + resultado.getNumeroPredial());
        return resultado;
    }

    /**
     * Implementación para obtener el objeto de negocio asociado a la actividad e instancia de
     * proceso de negocio especificadas y del ID de trámite asociado. Método utilizado para poder
     * obtener trámites asociados a un trámite padre (GLPI_39021)
     *
     * @author william.guevara
     *
     * @param idProceso Identificador de la instancia del proceso en el formato del gestor de
     * procesos.
     * @param idTramite Identificador del trámite asociado.
     * @param nombreActividad Nombre de la actividad de la que quiere recuperarse el objeto de
     * negocio.
     *
     * @return El objeto de negocio asociado a la instancia de proceso y actividad especificadas.
     */
    @Override
    public SolicitudCatastral obtenerObjetoNegocioConservacion(
        String idProceso, String idTramite, String nombreActividad) {
        SolicitudCatastral resultado = null;
        try {

//            PeticionObtenerObjetoNegocio peticion = new PeticionObtenerObjetoNegocio();
//            peticion.setIdObjeto(idProceso);
//            peticion.setIdObjetoNegocio(idTramite);
//            peticion.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
//            peticion.setNombreActividad(nombreActividad);
//            String json = jsonMapper.writeValueAsString(peticion);
//
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_OBJ_NEGOCIO_CONSERVACION);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, json);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    SolicitudCatastral.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener objeto de negocio de una actividad.");
        }
        return resultado;
    }

    /**
     *
     * @param idActividad
     * @param motivo
     * @return
     */
    @Override
    public Calendar suspenderActividad(String idActividad, Calendar fechaReanudacion, String motivo) {
        Calendar resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_SUSPENDER_ACTIVIDAD);
//
//            // Petición para suspender actividad
//            PeticionSuspenderActividad peticionSuspender = new PeticionSuspenderActividad();
//            peticionSuspender.setIdActividad(idActividad);
//            peticionSuspender.setMotivo(motivo);
//            peticionSuspender.setFechaReanudacion(fechaReanudacion);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class,
//                    jsonMapper.writeValueAsString(peticionSuspender));
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr, Calendar.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "reclamar actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "reclamar actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "reclamar actividad de conservación");
        }
        return resultado;
    }

    @Override
    public boolean transferirActividadConservacion(String idActividad,
        UsuarioDTO usuario) {
        List<String> idsActividades = new ArrayList<String>();
        idsActividades.add(idActividad);
        return transferirActividadConservacion(idsActividades, usuario);
    }

    @Override
    public boolean transferirActividadConservacion(List<String> idsActividades,
        UsuarioDTO usuario) {
        Boolean resultado = false;
        try {
            
           resultado = flujoBpmBean.transferirActividadConservacion(idsActividades, usuario);
            
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "reclamar actividad de conservación");
        }
        return resultado;
    }

    @Override
    public ControlCalidad avanzarActividad(String idActividad,
        ControlCalidad controlCalidad) {

        return avanzarActividad(null, idActividad, controlCalidad);
    }

    @Override
    public ControlCalidad avanzarActividad(UsuarioDTO usuarioEjecutorActActual,
        String idActividad, ControlCalidad controlCalidad) {
        ControlCalidad resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_AVANZAR_ACTIVIDAD_OSMI_CONTROL_CALIDAD);
//            PeticionAvanzarActividad avanceActividad = new PeticionAvanzarActividad();
//            avanceActividad.setObjetoNegocio(jsonMapper
//                .writeValueAsString(controlCalidad));
//            avanceActividad.setIdObjeto(idActividad);
//            if (usuarioEjecutorActActual != null) {
//                avanceActividad.setUsuario(jsonMapper
//                    .writeValueAsString(usuarioEjecutorActActual));
//            }
//            String peticion = jsonMapper.writeValueAsString(avanceActividad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, peticion);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    ControlCalidad.class);
                /*
                 * Esto es necesario porque en el servicio Rest de IBM se presenta un error en la
                 * serialización del objeto usuarios que hace parte de la solicitud catastral. Dicho
                 * objeto es retornado cuando se avanza la actividad en el servidor de procesos.
                 */
//                resultado.setUsuarios(controlCalidad.getUsuarios());
//
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de actualización");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "avance de actividad de actualización");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "avance de actividad de actualización");
        }
        return resultado;
    }

    @Override
    public String crearProceso(ControlCalidad controlCalidad) {
        String resultado = null;
        try {

//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_CREAR_PROCESO_OSMI_CONTROL_CALIDAD);
//            webResource.accept(MediaType.APPLICATION_JSON_TYPE);
//            String json = jsonMapper.writeValueAsString(controlCalidad);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, json);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                resultado = result.getResultado();
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC()
//                .getExcepcion(LOGGER, e,
//                    "la creación del proceso de control de calidad de ofertas inmobiliarias.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC()
//                .getExcepcion(LOGGER, e,
//                    "la creación del proceso de control de calidad de ofertas inmobiliarias.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC()
                .getExcepcion(LOGGER, e,
                    "la creación del proceso de control de calidad de ofertas inmobiliarias.");
        }
        return resultado;
    }

    private ExcepcionSNC lanzarExcepcion(APIProcesosRespuestaRest result)
        throws JsonGenerationException, JsonMappingException, IOException {
        RespuestaExcepcion respExcepcion = jsonMapper.readValue(
            result.getResultado(), RespuestaExcepcion.class);
        ExcepcionSNC excepcion = new ExcepcionSNC(
            respExcepcion.getCodigoExcepcion(),
            respExcepcion.getSeveridad(), respExcepcion.getMensaje(),
            new Throwable(respExcepcion.getTraza()));
        return excepcion;
    }

    /**
     * Método que obtiene la imagen del flujo del proceso y la publica en el directorio web.
     *
     * @author alejandro.sanchez
     *
     * @param flujoProceso
     * @param nombre
     * @return la url de la imagen a mostrar en web
     */
    private String obtenerImagenFlujoProceso(PeticionObtenerFlujoProceso flujoProceso, String nombre) {
        String urlImagen = null;
        try {
//            String json = jsonMapper.writeValueAsString(flujoProceso);
//            Client client = Client.create();
//            WebResource webResource = client.resource(URL_SERV_REST_OBTENER_FLUJO_EJECUCION_PROCESO);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN).post(
//                ClientResponse.class, json);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                RespuestaFlujoEjecucionProceso respGrafo = jsonMapper.readValue(result.
//                    getResultado(), RespuestaFlujoEjecucionProceso.class);
//                LOGGER.debug(respGrafo.getFlujoProceso());
//                try {
//                    String fileSeparator = System.getProperty("file.separator");
//                    String nombreArchivoGrafo = FileUtils.getTempDirectoryPath() + fileSeparator +
//                        nombre + ".dot";
//                    LOGGER.debug("nombreArchivoGrafo:" + nombreArchivoGrafo);
//                    FileWriter fWriter = new FileWriter(nombreArchivoGrafo);
//                    fWriter.append(respGrafo.getFlujoProceso());
//                    fWriter.flush();
//                    fWriter.close();
//                    IDocumentosService service = DocumentalServiceFactory.getService();
//                    String archivoImagen = service.generarImagenGrafo(nombreArchivoGrafo);
//                    DocumentoVO documentoVO = service.cargarDocumentoWorkspacePreview(archivoImagen);
//                    urlImagen = documentoVO.getUrlPublico();
//                    LOGGER.debug("urlImagen:" + urlImagen);
//
//                } catch (Exception e) {
//                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS.getExcepcion(
//                        LOGGER, e,
//                        "Ocurrió un error en la generación de la imagen del flujo del proceso.");
//                }
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001.getExcepcionSNC().
//                getExcepcion(LOGGER, e, "obtener el estado del proceso.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002.getExcepcionSNC().
//                getExcepcion(LOGGER, e, "obtener el estado del proceso.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003.getExcepcionSNC().
                getExcepcion(LOGGER, e, "obtener el estado del proceso.");
        }
        return urlImagen;

    }

    private ObjetoNegocio obtenerObjetoNegocioPorIdProceso(String idProceso, String mapaProceso) {
        ObjetoNegocio resultado = null;
        try {
//            PeticionObtenerObjetoNegocio peticionObjNegocio = new PeticionObtenerObjetoNegocio();
//            peticionObjNegocio.setMapaProceso(mapaProceso);
//            peticionObjNegocio.setIdObjeto(idProceso);
//            peticionObjNegocio.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
//
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_OBJ_NEGOCIO_PROCESO);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, jsonMapper.writeValueAsString(peticionObjNegocio));
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                resultado = (ObjetoNegocio) jsonMapper.readValue(result.getResultado(), Class.
//                    forName(result.getTipo()));
//            } else {
//                throw lanzarExcepcion(result);
//            }
//            return resultado;
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
//        } catch (ClassNotFoundException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener objeto de negocio de una actividad.");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCESOS
                .getExcepcion(LOGGER, null,
                    "No se encuentra el tipo de objeto retornado por el gestor" +
                    "de procesos.");
        }
        return resultado;
    }

    /**
     * Método que consulta una lista de {@link Actividad} basadas en los parámetros enviados en el
     * {@link MultiMap}.
     *
     * @see IProcesos#consultarListaActividadesPorParametrosHashTable(Hashtable<
     *      EParametrosConsultaActividades, String>)
     *
     * @nota: Copiado y ajustado para {@link MultiMap} del método null null null null null null null     consultarListaActividades(Map<EParametrosConsultaActividades,
     *        String>). Se decidió hacer otro debido al impacto que podría tener la modificación del
     * anterior a aspectos que se encuentren funcionando.
     *
     * @author david.cifuentes
     *
     * @param parametrosHashTable {@link MultiMap} en el cual sus llaves son constantes de tipo
     * {@link EParametrosConsultaActividades} y sus values los valores respectivos a buscar.
     *
     * @return Lista de objetos {@link Actividad} encontrados en el gestor de procesos.
     *
     */
    // Esto no me funcionó, lo dejo por si es útil cuando se vayan a integrar
    // los historicos, me dí cuenta que la consulta a process se hace por un
    // objeto de negocio a la vez (a la fecha), más adelante es posible que 
    // pueda ser usable.
    @Override
    public List<Actividad> consultarListaActividadesPorParametrosMultiMap(
        MultiMap<EParametrosConsultaActividades, String> parametrosMultiMap) {
        long currentTimeMillis = System.currentTimeMillis();
        List<Actividad> actividades = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_ACTIVIDADES_CON_FILTRO);
//            webResource.accept(MediaType.APPLICATION_JSON_TYPE);
//
//            List<Par> filtros = new ArrayList<Par>();
//            String llave = parametrosMultiMap.keySet().toString();
//            Collection<String> valores = parametrosMultiMap.values();
//            for (String valor : valores) {
//                filtros.add(new Par(llave, valor));
//            }
//            String filtrosStr = jsonMapper.writeValueAsString(filtros);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, filtrosStr);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                actividades = jsonMapper.readValue(result.getResultado(),
//                    new TypeReference<List<Actividad>>() {
//                });
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "la consulta de actividades de un usuario.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "la consulta de actividades de un usuario.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "la consulta de actividades de un usuario.");
        }
        LOGGER.debug("Contrucción de respuesta: " +
            (System.currentTimeMillis() - currentTimeMillis) + ", registros: " + actividades.size());
        return actividades;
    }

    @Override
    public List<UsuarioDTO> obtenerUsuariosActividad(String idActividad) {
        List<UsuarioDTO> resultado = null;
        try {
            resultado = new ArrayList<UsuarioDTO>();
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_OBTENER_USUARIOS_ACTIVIDAD);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class, idActividad);
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr,
//                    new TypeReference<List<UsuarioDTO>>() {
//                });
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener listado de usuarios de una actividad.");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "obtener listado de usuarios de una actividad.");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener listado de usuarios de una actividad.");
        }
        return resultado;
    }
    
    @Override
    public String obtenerCodTerritorial(String idActividad){
       
        String resultado = null;
        
        try {
           
            List<BpmFlujoTramite> bpmFlujoTramite  = flujoBpmBeanDAO.bpmFlujoTramiteFindById(Long.valueOf(idActividad));
            resultado = String.valueOf(bpmFlujoTramite.get(0).getCodTerritorial());
            
           
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "obtener listado de usuarios de una actividad.");
        }
        return resultado;
        
    }

    /**
     *
     * @param idActividad
     * @param motivo
     * @return
     */
    @Override
    public Calendar replanificarActividad(String idActividad, Calendar fechaReplanificacion,
        String motivo) {
        Calendar resultado = null;
        try {
//            Client c = Client.create();
//            WebResource webResource = c
//                .resource(URL_SERV_REST_REPLANIFICAR_ACTIVIDAD);
//
//            // Petición para suspender actividad
//            PeticionSuspenderActividad peticionSuspender = new PeticionSuspenderActividad();
//            peticionSuspender.setIdActividad(idActividad);
//            peticionSuspender.setMotivo(motivo);
//            peticionSuspender.setFechaReanudacion(fechaReplanificacion);
//            ClientResponse response = webResource.type(MediaType.TEXT_PLAIN)
//                .post(ClientResponse.class,
//                    jsonMapper.writeValueAsString(peticionSuspender));
//            APIProcesosRespuestaRest result = jsonMapper.readValue(
//                response.getEntity(String.class),
//                APIProcesosRespuestaRest.class);
//            if (result.getEstado().equals(RESULTADO_SATISFACTORIO)) {
//                String resultStr = result.getResultado();
//                resultado = jsonMapper.readValue(resultStr, Calendar.class);
//            } else {
//                throw lanzarExcepcion(result);
//            }
//        } catch (JsonGenerationException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40001
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "reclamar actividad de conservación");
//        } catch (JsonMappingException e) {
//            throw ManejadorExcepcionesProcesos.EXCEPCION_40002
//                .getExcepcionSNC().getExcepcion(LOGGER, e,
//                    "reclamar actividad de conservación");
        } catch (Exception e) {
            throw ManejadorExcepcionesProcesos.EXCEPCION_40003
                .getExcepcionSNC().getExcepcion(LOGGER, e,
                    "reclamar actividad de conservación");
        }
        return resultado;
    }

    /**
     * Método creado para filtrar los roles de un usuario a solo roles SNC
     *
     * @author wilman.vega
     * @param usuario
     */
    private void cambiarRolUsuario(final UsuarioDTO usuario) {
        List<String> rolesSNCUsuario = new ArrayList<String>();
        if (usuario != null && usuario.getRoles()!= null) {
            for (String rol : usuario.getRoles()) {
                if (rolesSNC.contains(rol.toUpperCase())) {
                    rolesSNCUsuario.add(rol);
                }
            }
            usuario.setRoles(rolesSNCUsuario.toArray(new String[0]));
        }
    }

    /**
     * Método creado para filtrar los roles del listado de Actividades de usuario (#75433)
     *
     * @author juan.cruz
     * @param listaActividadUsuarios
     */
    private void cambiarRolesDeActividadesUsuario(List<ActividadUsuarios> listaActividadUsuarios) {

        for (ActividadUsuarios unaActividadUsuarios : listaActividadUsuarios) {
            List<UsuarioDTO> usuarios = unaActividadUsuarios.getUsuarios();
            for (UsuarioDTO usuario : usuarios) {
                cambiarRolUsuario(usuario);
            }
        }

    }

}