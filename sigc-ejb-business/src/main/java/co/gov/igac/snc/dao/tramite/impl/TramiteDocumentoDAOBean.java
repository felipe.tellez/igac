package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import org.hibernate.Hibernate;

@Stateless
public class TramiteDocumentoDAOBean extends GenericDAOWithJPA<TramiteDocumento, Long> implements
    ITramiteDocumentoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteDocumentoDAOBean.class);

    /**
     * Método que busca un {@link TramiteDocumento} con soporte en Alfresco por su id.
     *
     * @see ITramiteDocumentoDAO#buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(idTramite)
     * @author david.cifuentes
     */
    @Override
    public TramiteDocumento buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(Long idTramite) {

        String solicitudSinRegistrar = EDocumentoEstado.SIN_REGISTRAR_SOLICITUD.getCodigo();
        String solicitudRegistrada = EDocumentoEstado.SOLICITUD_DOCS_REGISTRADA.getCodigo();
        Query query = this.entityManager.createQuery("SELECT td" +
            " FROM TramiteDocumento td" +
            " LEFT JOIN td.documento d " +
            " WHERE d.tramiteId = :idTramite " +
            " AND (d.estado = :solicitudRegistrada " +
            " OR d.estado = :solicitudSinRegistrar)");

        query.setParameter("idTramite", idTramite);
        query.setParameter("solicitudRegistrada", solicitudRegistrada);
        query.setParameter("solicitudSinRegistrar", solicitudSinRegistrar);
        query.setMaxResults(1);

        try {
            return (TramiteDocumento) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new ExcepcionSNC("Error:", ESeveridadExcepcionSNC.ERROR,
                "Falló la carga del Tramite Documento", e);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDocumentoDAO#findByTramiteAndTipoDocumento
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<TramiteDocumento> findByTramiteAndTipoDocumento(long tramiteId, long idTipoDocumento) {

        List<TramiteDocumento> answer = null;
        String queryString;
        Query query;

        queryString =
            "SELECT td FROM TramiteDocumento td JOIN td.tramite tdt JOIN td.documento tdd " +
            " JOIN tdd.tipoDocumento tddtd" +
            " WHERE tdt.id = :tramIdP AND tddtd.id = :tipoDocP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramIdP", tramiteId);
            query.setParameter("tipoDocP", idTipoDocumento);
            answer = query.getResultList();

            /*
             * @modified by leidy.gonzalez:: Se initialize TipoDocumento del TramiteDocumento
             */
            for (TramiteDocumento tramiteDocumento : answer) {

                if (tramiteDocumento.getDocumento() != null) {
                    Hibernate.initialize(tramiteDocumento.getDocumento());

                    if (tramiteDocumento.getDocumento().getTipoDocumento() != null) {
                        Hibernate.initialize(tramiteDocumento.getDocumento().getTipoDocumento());
                    }
                }

            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDocumentoDAOBean#findByTramiteAndTipoDocumento");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDocumentoDAO#findByTramiteAndTipoDocumento
     * @author pedro.garcia
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<TramiteDocumento> findByTramiteDocumentosDePruebasAsociadas(long tramiteId) {

        List<TramiteDocumento> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT td FROM TramiteDocumento td " +
            " JOIN td.tramite tdt " +
            " JOIN FETCH td.documento tdd " +
            " JOIN FETCH tdd.tipoDocumento tddtd" +
            " WHERE tdt.id = :tramIdP " +
            " AND tddtd.id IN (:tipoDocP)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramIdP", tramiteId);
            List<Long> tipos = new ArrayList<Long>();
            tipos.add(ETipoDocumento.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId());
            tipos.add(ETipoDocumento.INFORME_VISITA_REVISION_O_AUTOAVALUO.getId());
            query.setParameter("tipoDocP", tipos);
            answer = (List<TramiteDocumento>) query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on TramiteDocumentoDAOBean#findByTramiteAndTipoDocumento: " +
                ex.getMessage());
        }

        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO#countDocumentosPruebasPorEntidad(java.lang.Long,
     * java.lang.String)
     */
    @Override
    public Integer countDocumentosPruebasPorEntidad(Long tramiteId,
        String numeroIdentificacion) {
        Integer count = null;

        String query = "SELECT COUNT(td) FROM TramiteDocumento td " +
            " JOIN td.tramite t " +
            " LEFT JOIN td.documento d " +
            " LEFT JOIN d.tipoDocumento tipoDoc " +
            " WHERE td.tramite.id =:tramiteId " +
            " AND td.idPersonaNotificacion =:numeroIdentificacion " +
            " AND tipoDoc.id =:tipoDocId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);
            q.setParameter("numeroIdentificacion", numeroIdentificacion);
            q.setParameter("tipoDocId", ETipoDocumentoId.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId());

            Long conteo = (Long) q.getSingleResult();

            count = conteo.intValue();
        } catch (Exception e) {
            throw new ExcepcionSNC("Error:", ESeveridadExcepcionSNC.ERROR,
                "Falló el conteo de Tramite Documento", e);
        }
        return count;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDocumentoDAO#getDocumentosPruebasDeEntidad(Long, String)
     * @author franz.gamba
     */
    @Override
    public List<TramiteDocumento> getDocumentosPruebasDeEntidad(Long tramiteId,
        String numeroIdentificacion) {
        List<TramiteDocumento> answer = null;

        String query = "SELECT td FROM TramiteDocumento td " +
            " JOIN td.tramite t " +
            " LEFT JOIN FETCH td.documento d " +
            " LEFT JOIN FETCH d.tipoDocumento tipoDoc " +
            " WHERE td.tramite.id =:tramiteId " +
            " AND td.idPersonaNotificacion =:numeroIdentificacion " +
            " AND tipoDoc.id =:tipoDocId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);
            q.setParameter("numeroIdentificacion", numeroIdentificacion);
            q.setParameter("tipoDocId", ETipoDocumentoId.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId());

            answer = (List<TramiteDocumento>) q.getResultList();

        } catch (Exception e) {
            throw new ExcepcionSNC("Error:", ESeveridadExcepcionSNC.ERROR,
                "Falló la obtención de Tramite Documentos", e);
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDocumentoDAO#getForRegistroNotificacion(Long, null)
     * @author pedro.garcia
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public TramiteDocumento getForRegistroNotificacion(Long idTramite,
        String docPersonaNotificacion) {

        LOGGER.debug("on TramiteDocumentoDAOBean#getForRegistroNotificacion ");

        TramiteDocumento answer = null;
        List<TramiteDocumento> tempAnswer;
        Query query;
        String queryString;

        //D: debe revisar que el tipo de documento sea de comunicación de notificación,
        //   que el doc de la persona coincida con el valor del dato 'idPersonaNotificacion',
        //   que no tenga un documento de notificación asociado ni de autorización,
        //   y que coincida el id del trámite
        //Se quitaron estas partes del Query, ya no es necesaria esta validacion
        //" AND td.notificacionDocumentoId IS NULL " +
        //" AND td.autorizacionDocumentoId IS NULL " +
        queryString = "SELECT td FROM TramiteDocumento td " +
            " WHERE td.documento.tipoDocumento.id = :idTipoDocP " +
            " AND td.idPersonaNotificacion LIKE :docPersonaP " +
            " AND td.tramite.id = :tramiteIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTipoDocP", ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.
                getId());
            query.setParameter("docPersonaP", docPersonaNotificacion);
            query.setParameter("tramiteIdP", idTramite);
            tempAnswer = (List<TramiteDocumento>) query.getResultList();
            if (tempAnswer.size() > 1) {

                //D: Se puede dar el caso en el que haya dos comunicaciones de notificación asociadas a la misma persona, 
                //  esto podría pasar cuando la persona es solicitante o afectado y tambien es una entidad externa. 
                //  Para este caso es necesario que existan ambas pero no importa cual se retorne para el registro de la notificación. 
                // Debido a que al registrar la notificación se entendería que la persona se notifica tanto como
                // solicitante o afectado, y como entidad externa también.
                answer = tempAnswer.get(0);
            } else if (tempAnswer.size() == 1) {
                answer = tempAnswer.get(0);
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDocumentoDAOBean#getForRegistroNotificacion");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO#findTramiteDocumentosByTramiteId(java.lang.Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TramiteDocumento> findTramiteDocumentosByTramiteId(
        Long tramiteId) {

        List<TramiteDocumento> answer = null;
        String query = "SELECT td FROM TramiteDocumento td " +
            " LEFT JOIN FETCH td.documento d " +
            " LEFT JOIN FETCH d.tipoDocumento tipoD " +
            " WHERE td.tramite.id =:tramiteId" +
            " ORDER BY td.documento.tipoDocumento.nombre ASC";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            answer = (List<TramiteDocumento>) q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDocumentoDAOBean#findTramiteDocumentosByTramiteId");
        }
        return answer;
    }

    /*
     * @author leidy.gonzalez @see
     * co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO#consultaTramiteDocumentosPorDocumentoId(java.lang.Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TramiteDocumento> consultaTramiteDocumentosPorDocumentoId(
        Long documentoId) {

        List<TramiteDocumento> answer = null;
        String query = "SELECT td FROM TramiteDocumento td " +
            " LEFT JOIN FETCH td.documento d " +
            " WHERE td.documento.id =:documentoId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("documentoId", documentoId);

            answer = (List<TramiteDocumento>) q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDocumentoDAOBean#consultaTramiteDocumentosPorDocumentoId");
        }
        return answer;
    }

    /**
     * @see ITramiteDocumentoDAO#obtenerDocNotificacion(Long, String,String)
     * @author leidy.gonzalez
     */
    @Override
    public TramiteDocumento obtenerDocNotificacion(Long idTramite,
        String docPersonaNotificacion, String nombrePersonaNotif) {

        LOGGER.debug("on TramiteDocumentoDAOBean#obtenerDocNotificacion ");

        TramiteDocumento answer = null;
        List<TramiteDocumento> tempAnswer;
        Query query;
        String queryString;

        queryString = "SELECT td FROM TramiteDocumento td " +
            " WHERE td.documento.tipoDocumento.id = :idTipoDocP " +
            " AND td.idPersonaAutorizacion LIKE :docPersonaP " +
            " AND td.personaAutorizacion LIKE :nombrePersonaNotif " +
            " AND td.tramite.id = :tramiteIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTipoDocP", ETipoDocumento.NOTIFICACION.getId());
            query.setParameter("docPersonaP", docPersonaNotificacion);
            query.setParameter("tramiteIdP", idTramite);
            query.setParameter("nombrePersonaNotif", nombrePersonaNotif);
            tempAnswer = (List<TramiteDocumento>) query.getResultList();
            if (tempAnswer.size() > 1) {
                answer = tempAnswer.get(0);
            } else if (tempAnswer.size() == 1) {
                answer = tempAnswer.get(0);
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDocumentoDAOBean#obtenerDocNotificacion");
        }

        return answer;
    }

    /**
     * @see ITramiteDocumentoDAO#obtenerTramiteDocumentoParaRegistrosNotificacion(Long,
     * List<String>)
     * @author leidy.gonzalez
     */
    @Override
    public List<TramiteDocumento> obtenerTramiteDocumentosParaRegistrosNotificacion(
        Long idTramite, List<String> docsPersonaNotificacion) {

        LOGGER.
            debug("on TramiteDocumentoDAOBean#obtenerTramiteDocumentosParaRegistrosNotificacion ");
        List<TramiteDocumento> tempAnswer;
        Query query;
        String queryString;

        int partSize = 900;
        int len = docsPersonaNotificacion.size();
        if (len > partSize) {
            List<TramiteDocumento> resultadoTemp = new ArrayList<TramiteDocumento>();
            int res = len % partSize;
            int parts = (len / partSize) + Math.min(res, 1);
            for (int i = 0; i < parts; i++) {
                List<String> docsPersonaNotificacionAux = docsPersonaNotificacion.subList(i *
                    partSize, Math.min((i + 1) * partSize, (len)));
                resultadoTemp.addAll(this.obtenerTramiteDocumentosParaRegistrosNotificacion(
                    idTramite, docsPersonaNotificacionAux));
            }

            tempAnswer = resultadoTemp;

        } else {

            queryString = "SELECT td FROM TramiteDocumento td " +
                " WHERE td.documento.tipoDocumento.id = :idTipoDocP " +
                " AND td.idPersonaNotificacion IN (:docPersonaP) " +
                " AND td.tramite.id = :tramiteIdP";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("idTipoDocP", ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.
                    getId());
                query.setParameter("docPersonaP", docsPersonaNotificacion);
                query.setParameter("tramiteIdP", idTramite);

                tempAnswer = (List<TramiteDocumento>) query.getResultList();

            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "TramiteDocumentoDAOBean#obtenerTramiteDocumentosParaRegistrosNotificacion");
            }

        }

        return tempAnswer;
    }

    /**
     * @see ITramiteDocumentoDAO#obtenerDocsComunicacionYAutorizacion(Long, String,String)
     * @author leidy.gonzalez
     */
    @Override
    public List<TramiteDocumento> obtenerDocsComunicacionYAutorizacion(
        Long idTramite, String docPersonaNotificacion, String nombrePersonaNotif) {

        LOGGER.debug("on TramiteDocumentoDAOBean#obtenerDocsComunicacionYAutorizacion ");

        List<TramiteDocumento> tempAnswer;
        Query query;
        String queryString;

        queryString = "SELECT td FROM TramiteDocumento td " +
            " WHERE td.documento.tipoDocumento.id IN (:idTipoDocP) " +
            " AND td.idPersonaAutorizacion LIKE :docPersonaP " +
            " AND td.personaAutorizacion LIKE :nombrePersonaNotif " +
            " AND td.tramite.id = :tramiteIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTipoDocP", ETipoDocumento.DOCUMENTO_DE_NOTIFICACION.getId());
            query.setParameter("idTipoDocP",
                ETipoDocumento.DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION.getId());
            query.setParameter("docPersonaP", docPersonaNotificacion);
            query.setParameter("tramiteIdP", idTramite);
            query.setParameter("nombrePersonaNotif", nombrePersonaNotif);

            tempAnswer = (List<TramiteDocumento>) query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDocumentoDAOBean#obtenerDocsComunicacionYAutorizacion");
        }

        return tempAnswer;
    }

//end of class
}
