package co.gov.igac.snc.dao.sig;

import java.io.StringReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

class SigDAOUtil {

    static final String EXCEPTION_TYPE = "Exception";
    static final String ERROR_SNC_INFRASTRUCTURE = "ERROR_SNC_INFRASTRUCTURE";

    private static final Logger LOGGER = LoggerFactory.getLogger(SigDAOUtil.class);

    /**
     *
     */
    protected static String generateJobMessageAsXML(String name, String value) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Results");

        Element result = root.addElement("Result");
        result.addElement("Name").addText(name);
        result.addElement("Value").addText(value);

        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            LOGGER.warn(e.getMessage());
        }

        if (hostName != null) {
            result = root.addElement("Result");
            result.addElement("Name").addText("server");
            result.addElement("Value").addText(hostName);
        }
        return document.asXML();
    }

    /**
     *
     * @param identificadoresPredios
     * @param responseAsXML
     * @return
     */
    @SuppressWarnings("unchecked")
    protected static HashMap<String, List<ZonasFisicasGeoeconomicasVO>> procesarRespuestaObtenerZonasHomogeneas(
        String identificadoresPredios,
        Document xmlDocument) {
        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos =
            new HashMap<String, List<ZonasFisicasGeoeconomicasVO>>();
        if (xmlDocument != null) {
            try {
                List<Node> zonasResponse = xmlDocument.selectNodes("//Zonas");
                for (Node zonaCdata : zonasResponse) {
                    String cdata = zonaCdata.getText();
                    //LOGGER.debug("cdata " + cdata);
                    SAXReader reader = new SAXReader();
                    Document zonasHomogeneas = reader.read(new StringReader(cdata));
                    List<Node> zonas = zonasHomogeneas.selectNodes("//zonas");
                    for (Node zona : zonas) {
                        Element element = (Element) zona;
                        ZonasFisicasGeoeconomicasVO ZfgVO = new ZonasFisicasGeoeconomicasVO();
                        String numeroPredial = element.attributeValue("numeroPredial");
                        LOGGER.debug("numeroPredial " + numeroPredial);
                        String codigoZHF = element.attributeValue("codigoZHF");
                        LOGGER.debug("codigoZHF " + codigoZHF);
                        String codigoZHG = element.attributeValue("codigoZHG");
                        LOGGER.debug("codigoZHG " + codigoZHG);
                        String area = element.attributeValue("area");
                        LOGGER.debug("area " + area);
                        ZfgVO.setNumeroPredial(numeroPredial);
                        ZfgVO.setCodigoFisica(codigoZHF);
                        ZfgVO.setCodigoGeoeconomica(codigoZHG);
                        ZfgVO.setArea(area);
                        if (datos.get(numeroPredial) == null) {
                            List<ZonasFisicasGeoeconomicasVO> zonasList =
                                new ArrayList<ZonasFisicasGeoeconomicasVO>();
                            datos.put(numeroPredial, zonasList);
                        }
                        datos.get(numeroPredial).add(ZfgVO);
                    }
                }
            } catch (DocumentException e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                    getMessage());
            }
        } else {
            String errorMessage = "No se encontraron zonas homogeneas para los predios " +
                identificadoresPredios;
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, null,
                errorMessage);
        }
        return datos;
    }

    /**
     *
     * @param identificadoresPredios
     * @param responseAsXML
     * @return
     */
    @SuppressWarnings("unchecked")
    protected static List<String> procesarRespuestaColindancia(Document xmlDocument) {
        List<String> datos = new ArrayList<String>();
        if (xmlDocument != null) {
            List<Node> response = xmlDocument.selectNodes("//Colindantes");
            for (Node colindante : response) {
                Element element = (Element) colindante;
                String codigo = element.attributeValue("codigo");
                String colindantes = element.attributeValue("colindantes");
                String mejoras = element.attributeValue("mejoras");
                //LOGGER.debug("codigo " + codigo);
                //LOGGER.debug("colindantes " + colindantes);
                //LOGGER.debug("mejoras " + mejoras);
                if (colindantes != null) {
                    String[] colindantesArr = colindantes.split(",");
                    for (String codigoColindante : colindantesArr) {
                        datos.add(codigoColindante);
                    }
                }
                if (mejoras != null) {
                    String[] mejorasArr = mejoras.split(",");
                    for (String mejora : mejorasArr) {
                        datos.add(mejora);
                    }
                }
            }
        } else {
            String errorMessage = "Error en la respuesta del servidor SIG ";
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, null,
                errorMessage);
        }
        return datos;
    }

    /**
     *
     * @param xmlResponse
     * @return
     */
    @SuppressWarnings("unchecked")
    protected static Document parseXmlResponse(String xmlResponse) {
        Document xmlDocument;
        try {
            LOGGER.debug("xmlResponse:\n" + xmlResponse);
            SAXReader reader = new SAXReader();
            xmlDocument = reader.read(new StringReader(xmlResponse));
            List<Node> exceptions = xmlDocument.selectNodes("//Exception");
            for (Node exception : exceptions) {
                Element element = (Element) exception;
                String nodeType = element.attributeValue("node_type");
                LOGGER.debug("nodeType " + nodeType);
                if (nodeType.equals(EXCEPTION_TYPE)) {
                    LOGGER.error(nodeType + " : " + element.getText());
                    //únicamente lanza excepción si se detecta error de infraestructura
                    if (element.getText().indexOf(ERROR_SNC_INFRASTRUCTURE) > -1) {
                        throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER,
                            null, element.getText());
                    }
                } else {
                    LOGGER.error(nodeType + " : " + element.attributes().toString() + " : " +
                        element.getText());
                }
            }
        } catch (DocumentException e) {
            String error = "Error durante el procesamiento del mensaje xml de respuesta : " + e.
                getMessage();
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, error);
        }
        return xmlDocument;

    }

    /**
     * Procesa el XML devuelto en la validacion de inconsitencias
     *
     * @param identificadoresPredios
     * @param responseAsXML
     * @return
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    protected static HashMap<String, String> procesarRespuestasValidarInconsistencias(
        Document xmlDocument) {
        HashMap<String, String> datos = new HashMap<String, String>();
        if (xmlDocument != null) {
            try {
                List<Node> validacionResponse = xmlDocument.selectNodes("//ValidacionPredio");
                for (Node validacionPredio : validacionResponse) {
                    Element element = (Element) validacionPredio;
                    String numeroPredial = element.attributeValue("Predio");
                    LOGGER.debug("numeroPredial " + numeroPredial);
                    String inconsistencias = element.getStringValue().trim();
                    LOGGER.debug("inconsistencias " + inconsistencias);
                    datos.put(numeroPredial, inconsistencias);
                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                    getMessage());
            }
        } else {
            String errorMessage =
                "No se retorno informacion desde el componente geografico acerca de inconsistencias";
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, null,
                errorMessage);
        }
        return datos;
    }

    /**
     * Procesa el XML devuelto en la validacion de inconsitencias
     *
     * @param identificadoresPredios
     * @param responseAsXML
     * @return
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    protected static HashMap<String, String> procesarRespuestasValidarGeometriaUnidadConstruccion(
        Document xmlDocument) {
        HashMap<String, String> datos = new HashMap<String, String>();
        String numeroPredial = "";
        String valorInconsistencias = "";
        if (xmlDocument != null) {
            try {
                List<Node> resultados = xmlDocument.selectNodes("//ValidacionConstruccion");
                //Se extraen las inconsistencias del XML
                for (Node node : resultados) {
                    Element element = (Element) node;
                    numeroPredial = element.attributeValue("Construccion");
                    valorInconsistencias = element.getText();
                    valorInconsistencias = valorInconsistencias.trim();

                    if (valorInconsistencias != null && !valorInconsistencias.isEmpty()) {
                        datos.put(numeroPredial, valorInconsistencias);
                    }
                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                    getMessage());
            }
        } else {
            String errorMessage =
                "No se retorno informacion desde el componente geografico acerca de inconsistencias de construccion";
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, null,
                errorMessage);
        }
        return datos;
    }
}
