/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_VARIABLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmVariable.findAll", query = "SELECT b FROM BpmVariable b"),
    @NamedQuery(name = "BpmVariable.findById", query = "SELECT b FROM BpmVariable b WHERE b.id = :id"),
    @NamedQuery(name = "BpmVariable.findByNombre", query = "SELECT b FROM BpmVariable b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "BpmVariable.findByProcesoInstancia", query = "SELECT b FROM BpmVariable b WHERE b.procesoInstancia = :procesoInstancia"),
    @NamedQuery(name = "BpmVariable.findByValor", query = "SELECT b FROM BpmVariable b WHERE b.valor = :valor"),
    @NamedQuery(name = "BpmVariable.findByFlujoInstanciaId", query = "SELECT b FROM BpmVariable b WHERE b.flujoInstanciaId = :flujoInstanciaId")})
public class BpmVariable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "PROCESO_INSTANCIA")
    private Long procesoInstancia;
    @Column(name = "VALOR")
    private String valor;
    @Column(name = "FLUJO_INSTANCIA_ID")
    private Long flujoInstanciaId;

    public BpmVariable() {
    }

    public BpmVariable(Long id) {
        this.id = id;
    }

    public BpmVariable(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getProcesoInstancia() {
        return procesoInstancia;
    }

    public void setProcesoInstancia(Long procesoInstancia) {
        this.procesoInstancia = procesoInstancia;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Long getFlujoInstanciaId() {
        return flujoInstanciaId;
    }

    public void setFlujoInstanciaId(Long flujoInstanciaId) {
        this.flujoInstanciaId = flujoInstanciaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmVariable)) {
            return false;
        }
        BpmVariable other = (BpmVariable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmVariable[ id=" + id + " ]";
    }
    
}
