/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteReasignacionDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase para las operaciones en DB de la entidad TramiteReasignacion
 *
 * @author felipe.cadena
 */
@Stateless
public class TramiteReasignacionDAOBean extends GenericDAOWithJPA<TramiteReasignacion, Long>
    implements ITramiteReasignacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteReasignacionDAOBean.class);

}
