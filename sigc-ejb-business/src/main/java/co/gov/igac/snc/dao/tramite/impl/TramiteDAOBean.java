/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import co.gov.igac.snc.util.*;
import org.apache.commons.lang.ArrayUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IHPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioAvaluoCatastralDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentacionDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.RepConsultaPredial;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.TmpTramiteSession;
import co.gov.igac.snc.persistence.entity.tramite.TmpTramiteSessionId;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteProceso;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.DetallesDTO;
import co.gov.igac.snc.util.FiltroDatosConsultaCancelarTramites;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.TramiteConEjecutorAsignadoDTO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import javax.naming.directory.Attributes;

//TODO::pedro.garcia::eliminar todos mis métodos deprecated cuando ya no se usen definitivamente
/**
 *
 */
@Stateless
public class TramiteDAOBean extends GenericDAOWithJPA<Tramite, Long> implements ITramiteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDAOBean.class);

    @EJB
    private IDominioDAO dominioDAO;

    @EJB
    private IPredioAvaluoCatastralDAO predioAvaluoCatastralDAO;

    @EJB
    private IProcesos remoteProcesosService;

    @EJB
    private ITramiteDocumentacionDAO remoteTramiteDocumentacionService;

    @EJB
    private IPredioDAO predioDAO;

    @EJB
    private IHPredioDAO hPredioDAO;

    @EJB
    private IPPredioDAO pPredioDAO;

    @EJB
    private IDocumentoDAO documentoDAOService;

//--------------------------------------------------------------------------------------------------
    @Override
    public Tramite findTramiteByNoRadicacion(String numeroRadicacion) {
        LOGGER.debug("findTramiteByNoRadicacion");
        Query q = this.entityManager.createNamedQuery("findTramiteByNoRadicacion");

        q.setParameter("numeroRadicacion", numeroRadicacion);
        Tramite tramite = null;
        try {
            tramite = (Tramite) q.getSingleResult();
            Hibernate.initialize(tramite.getResultadoDocumento());
        } catch (Exception e) {
            throw new ExcepcionSNC("Deprecated", ESeveridadExcepcionSNC.ERROR,
                e.getMessage(), e);
        }
        return tramite;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#getTramitesByNumeroPredialPredio(java.lang.String)
     * @author pedro.garcia
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> getTramitesByNumeroPredialPredio(String numeroPredial) {
        LOGGER.debug("executing TramiteDAOBean#getTramitesByNumeroPredialPredio");

        List<Tramite> answer = null;
        Query query;
        String queryToExecute;

        // OJO: con JPQL se usan los nombres de las clases y los atributos de
        // esas clases, que corresponden al mapeo de las tablas!!!!!
        // Al usar "select new ..." se debe definir -en la clase entity
        // respectiva- un constructor
        // acorde a los campos que se traen en la consulta
        queryToExecute = "SELECT DISTINCT NEW co.gov.igac.snc.persistence.entity.tramite.Tramite(" +
            " Tr.id, Tr.predio.id, Tr.tipoTramite, Tr.numeroRadicacion, Tr.estado, Tr.claseMutacion, Tr.subtipo, " +
            " Tr.resultadoDocumento.id, Td.fecha, Tr.archivado," +
            " Tr.usuarioLog, Tr.fechaLog) " +
            " FROM Tramite Tr LEFT JOIN Tr.tramiteDocumentos Td" +
            " WHERE Tr.predio.id IN" +
            " (SELECT Pr.id FROM Predio Pr WHERE Pr.numeroPredial = :numPredial) " +
            " OR Tr.id IN ( " +
            " SELECT TPE.tramite.id FROM TramitePredioEnglobe TPE " +
            " WHERE TPE.predio.id = (SELECT Pr.id FROM Predio Pr WHERE Pr.numeroPredial = :numPredial))";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("numPredial", numeroPredial);

            answer = (ArrayList<Tramite>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("TramiteDAOBean#getTramitesByNumeroPredialPredio: " +
                "La consulta de trámites de un predio no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author fredy.wilches Retorna el listado de trámites a partir del id de la solicitud
     * @param solicitudId
     */
    /*
     * @modified by leidy.gonzalez:: #14043
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findBySolicitud(Long solicitudId) {
        // , p.numeroPredial, d.nombre, m.nombre
        List<Tramite> resultado;
        Query query = this.entityManager.createQuery("SELECT t " +
            " FROM Tramite t " + " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.departamento " + " LEFT JOIN FETCH p.municipio " +
            "where t.solicitud.id= :solicitudId");
        query.setParameter("solicitudId", solicitudId);
        resultado = (List<Tramite>) query.getResultList();

        for (Tramite tramite : resultado) {

            if (tramite.getTramiteTextoResolucions() != null) {
                Hibernate.initialize(tramite.getTramiteTextoResolucions());
            }

            if (tramite.getResultadoDocumento() != null) {
                Hibernate.initialize(tramite.getResultadoDocumento());

                if (tramite.getResultadoDocumento().getNumeroDocumento() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento().getNumeroDocumento());
                }

                if (tramite.getResultadoDocumento().getFechaDocumento() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento().getFechaDocumento());
                }
            }

            if (tramite.getTramitePredioEnglobes() != null) {
                for (TramitePredioEnglobe tpe : tramite
                    .getTramitePredioEnglobes()) {
                    if (tpe.getPredio() != null) {
                        Hibernate.initialize(tpe.getPredio());
                    }
                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getPredioDireccions() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getPredioDireccions());
                    }
                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getDireccionPrincipal() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getDireccionPrincipal());
                    }

                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getReferenciaCartograficas() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getReferenciaCartograficas());
                    }
                }
            }

            if (tramite.getDepartamento() != null) {
                Hibernate.initialize(tramite.getDepartamento());

                if (tramite.getDepartamento().getCodigo() != null) {
                    Hibernate.initialize(tramite.getDepartamento().getCodigo());
                }

                if (tramite.getDepartamento().getNombre() != null) {
                    Hibernate.initialize(tramite.getDepartamento().getNombre());
                }
            }

            if (tramite.getMunicipio() != null) {
                Hibernate.initialize(tramite.getMunicipio());
                if (tramite.getMunicipio().getNombre() != null) {
                    Hibernate.initialize(tramite.getMunicipio().getNombre());
                }
                if (tramite.getMunicipio().getCodigo() != null) {
                    Hibernate.initialize(tramite.getMunicipio().getCodigo());
                }
            }

            if (tramite.getTramiteInconsistencias() != null) {
                Hibernate.initialize(tramite.getTramiteInconsistencias());
            }
            if (tramite.getPredio() != null) {
                Hibernate.initialize(tramite.getPredio());
                if (tramite.getPredio().getPredioDireccions() != null) {
                    Hibernate.initialize(tramite.getPredio()
                        .getPredioDireccions());
                }
                if (tramite.getPredio().getDireccionPrincipal() != null) {
                    Hibernate.initialize(tramite.getPredio()
                        .getDireccionPrincipal());
                }
                if (tramite.getPredio().getReferenciaCartograficas() != null) {
                    Hibernate.initialize(tramite.getPredio()
                        .getReferenciaCartograficas());
                }
                if (tramite.getPredio().getNumeroPredial() != null) {
                    Hibernate.initialize(tramite.getPredio()
                        .getNumeroPredial());
                }
            }

        }
        return resultado;
    }

    /**
     * @author fredy.wilches Retorna el listado de trámites a partir del numero de la solicitud
     * @param numeroSolicitud
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findBySolicitud(String numeroSolicitud) {
        // , p.numeroPredial, d.nombre, m.nombre
        Query query = this.entityManager.createQuery("SELECT t FROM Tramite t" +
            " JOIN FETCH t.predio p " +
            " JOIN FETCH p.departamento" +
            " JOIN FETCH p.municipio " +
            "where t.solicitud.numero= :numeroSolicitud");
        query.setParameter("numeroSolicitud", numeroSolicitud);
        return (List<Tramite>) query.getResultList();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#buscarPredioEnglobesPorTramite(java.lang.Long)
     * @author pedro.garcia
     */
    /*
     * -> Modificado 16-11-2011 franz.gamba: se realizan las inicializaciones de departamento y
     * municipio para la consulta
     */
    @Implement
    @Override
    public List<TramiteDetallePredio> buscarPredioEnglobesPorTramite(Long tramiteId) {

        LOGGER.debug("on TramiteDAOBean#buscarPredioEnblobesPorTramite");

        List<TramiteDetallePredio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tpe " +
            " FROM TramiteDetallePredio tpe" +
            " JOIN FETCH tpe.predio" +
            " WHERE tpe.tramite.id = :idTramiteP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramiteP", tramiteId);
            answer = (List<TramiteDetallePredio>) query.getResultList();

            for (TramiteDetallePredio tpe : answer) {
                tpe.getPredio().getDepartamento().getNombre();
                tpe.getPredio().getMunicipio().getNombre();
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#buscarPredioEnblobesPorTramite");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace el query de los trámites que estén en un determinado estado de BD (diferente al estado
     * del BPM). Según el flag, devuelve solo el conteo de registros
     *
     * OJO: si no hay una codición adicional se puede uasr WHERE 1=1 y luego concatenar el IN
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param conEjecutor diferencia el query para traer los trámites que tienen o no ejecutor
     * asignado
     * @param isCount dice si lo que se quiere es contar las filas resultados
     * @param rowStartIdxAndCount
     * @return
     */
    private Object findTramitesAsignacionDeEjecutor(long[] idsTramites,
        boolean conEjecutor, boolean isCount, String sortField, String sortOrder,
        Map<String, String> filters, final int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteDAOBean#findTramitesAsignacionDeEjecutor");

        List<Tramite> answer1;
        BigInteger answer2;
        StringBuilder queryString, idTramiteIn;
        Query query;

        queryString = new StringBuilder();

        idTramiteIn = this.concatenarIdsTramites(idsTramites);

        if (!isCount) {
            queryString
                .append("SELECT distinct t FROM Tramite t LEFT JOIN FETCH t.predio p " +
                    " LEFT JOIN FETCH t.departamento LEFT JOIN FETCH t.municipio " +
                    " LEFT JOIN FETCH p.municipio m JOIN FETCH t.solicitud solicitud " +
                    " LEFT JOIN FETCH t.tramitePredioEnglobes tpe ");
        } else {
            // N: esta, para dejarla más liviana se hace de forma diferente
            queryString.append("SELECT count(t) " + " FROM Tramite t ");
        }

        if (conEjecutor) {
            queryString.append(" WHERE t.funcionarioEjecutor != null ");
        } else {
            queryString.append(" WHERE t.funcionarioEjecutor = null ");
        }

        if (filters != null) {
            for (String campo : filters.keySet()) {
                if (campo.equals("numeroRadicacion")) {
                    queryString.append(" AND t.numeroRadicacion like :numeroRadicacion");
                }
                if (campo.equals("tipoTramite")) {
                    queryString.append(" AND t.tipoTramite like :tipoTramite");
                }
                if (campo.equals("predio.municipio.nombre")) {
                    queryString.append(" AND UPPER(p.municipio.nombre) like :nombreMunicipio");
                }
                if (campo.equals("predio.numeroPredial")) {
                    queryString.append(" AND p.numeroPredial like :numeroPredial");
                }
                if (campo.equals("clasificacion")) {
                    queryString.append(" AND UPPER(t.clasificacion) like :clasificacion");
                }

                LOGGER.debug("Campo filtro " + campo);
                LOGGER.debug("Tamaño " + filters.get(campo).length());
            }
        }

        queryString.append(idTramiteIn);
        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            queryString.append(" ORDER BY t.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }
        try {
            query = this.entityManager.createQuery(queryString.toString());

            if (filters != null) {
                for (String campo : filters.keySet()) {
                    if (campo.equals("predio.municipio.nombre")) {
                        query.setParameter("nombreMunicipio",
                            "%" + filters.get(campo).toUpperCase() + "%");
                    } else if (campo.equals("predio.numeroPredial")) {
                        query.setParameter("numeroPredial", "%" + filters.get(campo).toUpperCase() +
                            "%");
                    } else {
                        query.setParameter(campo, "%" + filters.get(campo).toUpperCase() + "%");
                    }
                }
            }

            if (!isCount) {
                answer1 = (List<Tramite>) super.findInRangeUsingQuery(query,
                    rowStartIdxAndCount);

                return answer1;
            } else {
                answer2 = new BigInteger(query.getSingleResult().toString());
                return answer2;
            }
        } catch (Exception e) {
            LOGGER.error("error on TramiteDAOBean#findTramitesAsignacionDeEjecutor: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramitesAsignacionDeEjecutor");
        }
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public Tramite buscarTramiteTramitesPorTramiteId(Long tramiteId) {
        LOGGER.debug("Entra en TramiteDAOBean#buscarTramiteTramitesPorTramiteId");
        Query q = entityManager
            .createQuery("SELECT t" +
                " FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.tramite tt" +
                " LEFT JOIN FETCH t.tramiteDocumentacions td" +
                " LEFT JOIN FETCH td.departamento" +
                " LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.municipio LEFT JOIN FETCH td.tipoDocumento" +
                " WHERE t.id = :tramiteId");
        q.setParameter("tramiteId", tramiteId);

        try {
            Tramite tramite = (Tramite) q.getSingleResult();
            if (tramite.getTramite() != null) {
                fetchSubTramites(tramite);
            }
            return tramite;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

//--------------------------------------------------------------------------------------------------
    @Override
    public Tramite buscarTramitePorTramiteIdProyeccion(Long tramiteId) {
        LOGGER.debug("Entra en TramiteDAOBean#buscarTramiteTramitesPorTramiteId");
        Query q = entityManager
            .createQuery("SELECT t" +
                " FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.tramite tt" +
                " LEFT JOIN FETCH t.departamento" +
                " LEFT JOIN FETCH t.municipio" +
                " LEFT JOIN FETCH t.resultadoDocumento" +
                " LEFT JOIN FETCH t.tramiteDocumentacions td" +
                " LEFT JOIN FETCH p.departamento " +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH td.departamento" +
                " LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.municipio " +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " WHERE t.id = :tramiteId");
        q.setParameter("tramiteId", tramiteId);

        try {
            Tramite tramite = (Tramite) q.getSingleResult();
            if (tramite.getTramite() != null) {
                fetchSubTramites(tramite);
            }
            tramite.getTramiteRectificacions().size();

            tramite.getComisionTramites().size();

            for (ComisionTramite ct : tramite.getComisionTramites()) {
                Hibernate.initialize(ct.getComision());
                if (ct.getComisionTramiteDatos() != null && !ct.getComisionTramiteDatos().isEmpty()) {
                    for (ComisionTramiteDato ctd : ct.getComisionTramiteDatos()) {
                        Hibernate.initialize(ctd);
                    }
                }
            }

            tramite.getTramitePredioEnglobes().size();
            tramite.getTramites().size();
            tramite.getSolicitud().getSolicitanteSolicituds().size();
            tramite.getTramiteDocumentos().size();
            for (TramiteDocumento td : tramite.getTramiteDocumentos()) {
                td.getDocumento().getId();
                td.getDocumento().getDocumentoArchivoAnexos().size();
                td.getDocumento().getTipoDocumento().getId();
            }
            for (TramitePredioEnglobe tpe : tramite.getTramitePredioEnglobes()) {
                tpe.getPredio().getPredioDireccions().size();
                tpe.getPredio().getReferenciaCartograficas().size();
                tpe.getPredio().getDepartamento();
                tpe.getPredio().getMunicipio();
                List<UnidadConstruccion> cs = tpe.getPredio().getUnidadConstruccions();
                for (UnidadConstruccion uc : cs) {
                    uc.getUnidadConstruccionComps().size();
                }
            }
            if (tramite.getPredio() != null) {
                if (tramite.getPredio().getPredioDireccions() != null) {
                    tramite.getPredio().getPredioDireccions().size();
                }
                if (tramite.getPredio().getReferenciaCartograficas() != null) {
                    tramite.getPredio().getReferenciaCartograficas().size();
                }
                tramite.getPredio().getDepartamento();
                tramite.getPredio().getMunicipio();
            }
            if (tramite.getTramiteInconsistencias() != null) {
                tramite.getTramiteInconsistencias().size();
            }

            Hibernate.initialize(tramite.getTramiteEstados());

            Hibernate.initialize(tramite.getFichaMatrizPredioTerrenos());
            if (tramite.getFichaMatrizPredioTerrenos() != null) {
                for (PFichaMatrizPredioTerreno fmpt : tramite.getFichaMatrizPredioTerrenos()) {
                    Hibernate.initialize(fmpt.getPredio());
                    Hibernate.initialize(fmpt.getFichaMatriz());
                    Hibernate.initialize(fmpt.getPredio().getPredioDireccions());
                }

            }

            return tramite;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<Tramite> getTramitesAsociados(Long tramiteId) {
        LOGGER.debug("Entra en TramiteDAOBean#getTramitesAsociados");
        Query q = entityManager
            .createQuery("SELECT DISTINCT t" +
                " FROM Tramite t LEFT JOIN FETCH t.predio p  LEFT JOIN FETCH t.tramite tt" +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.departamento" +
                " LEFT JOIN FETCH td.municipio LEFT JOIN FETCH td.tipoDocumento" +
                " WHERE tt.id = :tramiteId");
        q.setParameter("tramiteId", tramiteId);

        try {
            List<Tramite> tramite = (List<Tramite>) q.getResultList();
            for (Tramite t : tramite) {
                if (t.getResultadoDocumento() != null) {
                    Hibernate.initialize(t.getResultadoDocumento());
                }
            }
            /*
             * if (tramite.getTramite() != null) { fetchSubTramites(tramite); }
             */
 /*
             * for(int i=0;i < tramite.size();i++){ if(tramite.get(i).getId() ==
             * tramite.get(i-1).getId()){ tramite.remove(i); } }
             */
            return tramite;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public DetallesDTO detallesPredioDeTramite(Long tramiteId) {
        LOGGER.debug("detallesPredioDeTramite");

        DetallesDTO detalle = null;

        try {
            Query q = this.entityManager
                .createQuery("SELECT t" +
                    " FROM Tramite t LEFT JOIN FETCH t.predio p  LEFT JOIN FETCH t.tramite tt" +
                    " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.departamento" +
                    " LEFT JOIN FETCH td.municipio LEFT JOIN FETCH td.tipoDocumento" +
                    " WHERE t.id = :tramiteId");

            q.setParameter("tramiteId", tramiteId);
            Tramite tramite = null;
            tramite = (Tramite) q.getSingleResult();
            Predio predio = tramite.getPredio();
            detalle = new DetallesDTO(predio);
        } catch (NoResultException e) {
            return null;
        }

        return detalle;
    }
//--------------------------------------------------------------------------------------------------

    private void fetchSubTramites(Tramite t) {
        LOGGER.debug("Entra en fetchSubTramites");
        if (t.getTramite() != null) {
            t.getTramite().getNumeroRadicacion();
            LOGGER.debug("inicializa " + t.getTramite().getNumeroRadicacion());
            fetchSubTramites(t.getTramite());
        }

    }

    /**
     * Crea un trámite correspondiente a una solicitud de cotización de avalúo comercial.
     *
     * @author jamir.avila
     * @see ITramiteDAO#crearSolicitudCotizacionAvaluo(Tramite)
     */
    @Override
    public Tramite crearSolicitudCotizacionAvaluo(Tramite tramite) {
        LOGGER.info("TramiteDAOBean#crearSolicitudCotizacionAvaluo");

        tramite.setTipoTramite("CotizaciónAvalúo");

        // Solicitud solicitud = entityManager.find(Solicitud.class,
        // tramite.getSolicitud().getId());
        // solicitud.getTramites().add(tramite);
        entityManager.persist(tramite);

        return tramite;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findByComisionId(java.lang.Long)
     * @author pedro.garcia
     */
    /*
     * @modified pedro.garcia 29-05-2012 uso de left para el join de t.predio porque puede ser nulo
     */
    @Implement
    @Override
    public List<Tramite> findByComisionId(Long comisionId) {

        List<Tramite> answer = null;
        String queryString;
        Query query;

        // D: comisionTramites es un List en Tramite, y esto funciona:
        queryString = "SELECT t FROM Tramite t " +
            "LEFT JOIN t.comisionTramites ct " +
            "LEFT JOIN ct.comision c " +
            "LEFT JOIN FETCH t.predio " +
            "JOIN FETCH t.municipio " +
            "JOIN FETCH t.departamento " +
            "WHERE ct.tramite.id = t.id " +
            "AND ct.comision.id = :comisionIdParam";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("comisionIdParam", comisionId);

            answer = (List<Tramite>) query.getResultList();

            if (answer != null) {
                for (Tramite tramiteTemp : answer) {

                    //D: traer los predios del trámite cuando es un englobe
                    if (tramiteTemp.getTramitePredioEnglobes() != null) {
                        tramiteTemp.getTramitePredioEnglobes().size();
                        for (TramitePredioEnglobe tpe : tramiteTemp.getTramitePredioEnglobes()) {

                            //N: no encontré cómo hacer que trajera el predio completo para sacarle el número predial,
                            //  y hacer que ese dato no se fuera hasta la otra capa. Si se le asigna null al predio sale error
                            Hibernate.initialize(tpe.getPredio());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#findByComisionId");
        }

        return answer;

    }

    // -------------------------------------------------------- //
    /**
     * @see ITramiteDAO#buscarTramitesDeComisiones(List<Long>)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> buscarTramitesDeComisiones(List<Long> comisionesId) {

        List<Tramite> answer = null;
        try {
            String queryString;
            Query query;

            queryString = "SELECT DISTINCT t FROM Tramite t " +
                " LEFT JOIN t.comisionTramites ct" +
                " LEFT JOIN ct.comision c " +
                " LEFT JOIN FETCH t.predio" +
                " JOIN FETCH t.municipio" +
                " JOIN FETCH t.departamento" +
                " WHERE ct.tramite.id = t.id" +
                " AND ct.comision.id IN ( :comisionesIdParam )";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("comisionesIdParam", comisionesId);

            answer = (List<Tramite>) query.getResultList();

            for (Tramite tramiteTemp : answer) {
                if (tramiteTemp.getTramitePredioEnglobes() != null) {
                    tramiteTemp.getTramitePredioEnglobes().size();
                }
            }

            return answer;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteDAO#buscarTramitesDeComisiones: " +
                ex.getMensaje());
        }
        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#contarTramitesDeEjecutorAsignado(String)
     */
    @Deprecated
    @Override
    public Integer contarTramitesDeEjecutorAsignado(String ejecutor) {

        LOGGER.debug("Entra en TramiteDAOBean#contarTramitesDeEjecutorAsignado");

        // TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir
        // este método :: juan.agudelo
        /*
         * String estadoTramite;
         *
         * estadoTramite = ETramiteEstado.FUNCIONARIO_EJECUTOR_ASIGNADO .getCodigo();
         */
        Query q = entityManager
            .createQuery("SELECT COUNT (t.funcionarioEjecutor)" +
                " FROM Tramite t" +
                " WHERE t.funcionarioEjecutor= :funcionarioEjecutor");
        // + " AND t.estado= :estado");
        q.setParameter("funcionarioEjecutor", ejecutor);
        // q.setParameter("estado", estadoTramite);

        try {
            Integer conteoTramites = (Integer.parseInt(q.getSingleResult()
                .toString()));
            LOGGER.debug("Tramites asignados al ejecutor: " + conteoTramites);
            return conteoTramites;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarTramitesDeEjecutorAsignado(String, String, int...) OJO: no cambiar
     * nunca el join fetch t.comisionTramites porque no funcionarían otras cosas
     */
    @Deprecated
    @Override
    public List<Tramite> buscarTramitesDeEjecutorAsignado(String idTerritorial,
        String ejecutor, final int... rowStartIdxAndCount) {

        LOGGER.debug("Entra en TramiteDAOBean#buscarTramitesDeEjecutorAsignado");

        // TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir
        // este método :: juan.agudelo

        /*
         * String estadoTramite; estadoTramite = ETramiteEstado.FUNCIONARIO_EJECUTOR_ASIGNADO
         * .getCodigo();
         */
        // TODO juan.agudelo hay que revisar si es necesario incluir el
        // parámetro de la territorial en la búsqueda
        Query q = entityManager
            .createQuery("SELECT t" +
                " FROM Tramite t LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.comisionTramites" +
                " JOIN FETCH t.departamento" +
                " JOIN FETCH t.municipio" +
                " WHERE t.funcionarioEjecutor= :funcionarioEjecutor" +
                " AND t.municipio.codigo IN (SELECT j.municipio.codigo FROM Jurisdiccion j " +
                " WHERE j.estructuraOrganizacional.codigo= :territorialCod)");
        // + " AND t.estado= :estado");
        q.setParameter("funcionarioEjecutor", ejecutor);
        q.setParameter("territorialCod", idTerritorial);
        // q.setParameter("estado", estadoTramite);

        try {
            List<Tramite> tramitesEjecutor = (super.findInRangeUsingQuery(q,
                rowStartIdxAndCount));

            if (tramitesEjecutor != null && tramitesEjecutor.size() > 0) {
                for (int x = 0; x < tramitesEjecutor.size(); x++) {
                    tramitesEjecutor.get(x).getTramitePredioEnglobes().size();

                    if (tramitesEjecutor.get(x).getTramitePredioEnglobes() != null) {
                        for (int y = 0; y < tramitesEjecutor.get(x)
                            .getTramitePredioEnglobes().size(); y++) {
                            tramitesEjecutor.get(x).getTramitePredioEnglobes()
                                .get(y).getPredio()
                                .getFormatedNumeroPredial();
                        }
                    }

                    if (tramitesEjecutor.get(x).getComisionTramites() != null) {
                        LOGGER.debug("ComisionTramites tamaño: " +
                            tramitesEjecutor.get(x).getComisionTramites()
                                .size());

                        if (tramitesEjecutor.get(x).getComisionTramites()
                            .size() > 0) {
                            tramitesEjecutor.get(x).getComisionTramites()
                                .get(0).getComision().getNumero();
                        }
                    }
                }
            }
            return tramitesEjecutor;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------
//TODO :: fredy.wilches :: documentar método según estándar :: pedro.garcia

    /*
     * @modified pedro.garcia 11-09-2013 manejo de excepciones
     */
 /*
     * @modified by leidy.gonzalez :: #16861 :: 13/04/2016 Se agrega Laizy de comision y
     * resultadoDocumentos asociados al tramite.
     */
    @Override
    public Tramite findById(Long tramiteId) {

        Tramite t = null;
        Query query = this.entityManager.createQuery("SELECT t " +
            " FROM Tramite t" +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE t.id= :tramiteId");

        try {
            query.setParameter("tramiteId", tramiteId);
            t = (Tramite) query.getSingleResult();
            t.getTramites().size();
            Hibernate.initialize(t.getTramiteRelacionTramitesHijos());
            Hibernate.initialize(t.getSolicitud().getSolicitanteSolicituds());

            if (t.getTramiteRectificacions() != null) {
                Hibernate.initialize(t.getTramiteRectificacions());
            }
            if (t.getSolicitanteTramites() != null) {
                Hibernate.initialize(t.getSolicitanteTramites());
            }

            if (t.getTramitePredioEnglobes() != null) {
                Hibernate.initialize(t.getTramitePredioEnglobes());
            }
            if (t.getPredios() != null) {
                Hibernate.initialize(t.getPredios());
            }
            if (t.getTramiteDocumentos() != null) {
                Hibernate.initialize(t.getTramiteDocumentos());
            }

            if (t.getTramiteInconsistencias() != null) {
                Hibernate.initialize(t.getTramiteInconsistencias());
            }

            if (t.getPredio() != null) {
                Hibernate.initialize(t.getPredio());
                if (t.getPredio().getPredioDireccions() != null) {
                    Hibernate.initialize(t.getPredio()
                        .getPredioDireccions());
                }
                if (t.getPredio().getDireccionPrincipal() != null) {
                    Hibernate.initialize(t.getPredio()
                        .getDireccionPrincipal());
                }
                if (t.getPredio().getReferenciaCartograficas() != null) {
                    Hibernate.initialize(t.getPredio()
                        .getReferenciaCartograficas());
                }
                if (t.getPredio().getNumeroPredial() != null) {
                    Hibernate.initialize(t.getPredio()
                        .getNumeroPredial());
                }
            }

            if (t.getTramitePredioEnglobes() != null) {
                for (TramitePredioEnglobe tpe : t
                    .getTramitePredioEnglobes()) {
                    if (tpe.getPredio() != null) {
                        Hibernate.initialize(tpe.getPredio());
                    }
                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getPredioDireccions() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getPredioDireccions());
                    }
                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getDireccionPrincipal() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getDireccionPrincipal());
                    }

                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getReferenciaCartograficas() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getReferenciaCartograficas());
                    }
                }
            }

            if (t.getComisionTramite() != null) {
                Hibernate.initialize(t.getComisionTramite());
            }

            if (t.getTramite() != null) {
                Hibernate.initialize(t.getTramite());
                if (t.getTramite().getComisionTramite() != null) {
                    Hibernate.initialize(t.getTramite().getComisionTramite());
                }
            }

            if (t.getResultadoDocumento() != null) {
                Hibernate.initialize(t.getResultadoDocumento());
            }
        } catch (NoResultException nre) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nre,
                "TramiteDAOBean#findById", "Tramite", tramiteId.toString());
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#findById");
        } finally {
            return t;
        }

    }

    /**
     * @see ITramiteDAO#obtenerTramitePorIdReasignacion
     * @author felipe.cadena
     */
    @Override
    public Tramite obtenerTramitePorIdReasignacion(Long tramiteId) {

        Tramite t = null;
        Query query = this.entityManager.createQuery("SELECT t " +
            " FROM Tramite t" +
            " LEFT JOIN FETCH t.departamento" +
            " LEFT JOIN FETCH t.municipio" +
            " WHERE t.id= :tramiteId");

        try {
            query.setParameter("tramiteId", tramiteId);
            t = (Tramite) query.getSingleResult();

        } catch (NoResultException nre) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nre,
                "TramiteDAOBean#findById", "Tramite", tramiteId.toString());
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#findById");
        } finally {
            return t;
        }

    }

// ---------------------------------------------------------------------------------------------
    /**
     * Metodo para consultar la configuración de las secciones para la ejecución de un trámite
     *
     * @author fredy.wilches
     */
    /*
     * @modified juan.agudelo @modified pedro.garcia 19-09-2012 Se eliminó el parámetro
     * tipoInscripcion ya que ese campo no se usa en los trámites.
     */
    @Implement
    @Override
    public TramiteSeccionDatos getConfiguracionTramite(String tipoTramite,
        String claseMutacion, String subtipo, String radicacionEspecial) {

        StringBuilder q = new StringBuilder();

        q.append("SELECT t FROM TramiteSeccionDatos t");
        q.append(" WHERE t.tipoTramite= :tipoTramite");

        if (claseMutacion != null && !claseMutacion.isEmpty()) {
            q.append(" AND t.claseMutacion= :claseMutacion ");
        }

        if (subtipo != null && !subtipo.isEmpty()) {
            q.append(" AND t.subtipo= :subtipo");
        }

        if (radicacionEspecial != null && !radicacionEspecial.isEmpty()) {
            q.append(" AND t.radicacionEspecial= :radicacionEspecial");
        } else {
            q.append(" AND t.radicacionEspecial IS NULL");
        }

        try {
            Query query = this.entityManager.createQuery(q.toString());
            query.setParameter("tipoTramite", tipoTramite);

            if (claseMutacion != null && !claseMutacion.isEmpty()) {
                query.setParameter("claseMutacion", claseMutacion);
            }

            if (subtipo != null && !subtipo.isEmpty()) {
                query.setParameter("subtipo", subtipo);
            }

            if (radicacionEspecial != null && !radicacionEspecial.isEmpty()) {
                query.setParameter("radicacionEspecial", radicacionEspecial);
            }

            return (TramiteSeccionDatos) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#getConfiguracionTramite");
        }
    }

    /**
     * @author javier.aponte Método para contar los tramites que tiene asignado un funcionario
     * ejecutor
     * @param String ejecutor
     */
    @Override
    public Integer contarTramitesDeFuncionarioEjecutor(String ejecutor) {
        LOGGER.debug("Entra en TramiteDAOBean#contarTramitesDeFuncionarioEjecutor");

        Query q = entityManager
            .createQuery("SELECT COUNT (t.funcionarioEjecutor)" +
                " FROM Tramite t" +
                " WHERE t.nombreFuncionarioEjecutor= :funcionarioEjecutor");
        q.setParameter("funcionarioEjecutor", ejecutor);

        try {
            Integer conteoTramites = (Integer.parseInt(q.getSingleResult()
                .toString()));
            LOGGER.debug("Tramites asignados al ejecutor: " + conteoTramites);
            return conteoTramites;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramiteDAO#archivarTramite(UsuarioDTO, Tramite)
     */
    @Override
    public boolean archivarTramite(UsuarioDTO usuario,
        Tramite tramiteParaArchivar) {

        boolean resultado = false;
        Tramite tramiteResultado = null;

        try {
            Tramite tramite = tramiteParaArchivar;
            tramite.setUsuarioLog(usuario.getLogin());
            tramite.setFechaLog(new Date(System.currentTimeMillis()));
            tramite.setArchivado(ESiNo.SI.getCodigo());
            tramiteResultado = update(tramite);

            if (tramiteResultado != null) {
                resultado = true;
            }

            return resultado;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return resultado;
        }
    }

// --------------------------------------------------------------------------------------------------
    /**
     * Método que consulta un texto motivo teniendo el numero de motivo.
     *
     * @see ITramiteDAO#findByNumeroMotivo(String numeroMotivo)
     * @author david.cifuentes
     */
    @Override
    public String findByNumeroMotivo(String numeroMotivo) {

        LOGGER.debug("findByNumeroMotivo");
        String answer = null;
        String queryString;
        Query query;

        queryString =
            "SELECT tm.texto FROM TextoMotivo tm WHERE tm.numero = :numero AND tm.activo = 'SI' ";

//TODO :: david.cifuentes :: 12-09-2013 :: hacer manejo de excepciones de negocio y hacer catch de esta en donde se use este método :: pedro.garcia   
        query = this.entityManager.createQuery(queryString);
        query.setParameter("numero", numeroMotivo);

        answer = (String) query.getSingleResult();

        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramite#buscarTramiteSolicitudPorId(Long)
     */
    /*
     * @modified pedro.garcia validar que el documento soporte de las documentaciones no sea null
     */
    @Override
    public Tramite buscarTramiteSolicitudPorId(Long tramiteId) {

        LOGGER.debug("Entra en TramiteDAOBean#buscarTramiteSolicitudPorId");

        Tramite tramite = null;
        Query q = entityManager.createQuery("SELECT t" + " FROM Tramite t " +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH t.tramite tt" +
            " JOIN FETCH t.departamento" + " JOIN FETCH t.municipio " +
            " JOIN FETCH t.solicitud s" +
            " LEFT JOIN FETCH t.solicitanteTramites ss" +
            " LEFT JOIN FETCH ss.solicitante sss" +
            " WHERE t.id = :tramiteId");
        q.setParameter("tramiteId", tramiteId);

        try {
            tramite = (Tramite) q.getSingleResult();
            if (tramite.getTramites() != null) {
                fetchSubTramites(tramite);
            }
            tramite.getTramiteEstados().size();
            tramite.getTramiteDocumentacions().size();
            tramite.getTramitePredioEnglobes().size();
            tramite.getSolicitud().getSolicitanteSolicituds().size();
            tramite.getSolicitanteTramites().size();

            if (tramite.getTramiteDocumentacions() != null &&
                !tramite.getTramiteDocumentacions().isEmpty()) {
                for (int i = 0; i < tramite.getTramiteDocumentacions().size(); i++) {

                    if (tramite.getTramiteDocumentacions().get(i)
                        .getDocumentoSoporte() != null) {
                        tramite.getTramiteDocumentacions().get(i)
                            .getDocumentoSoporte().getArchivo();

                        tramite.getTramiteDocumentacions().get(i)
                            .getDocumentoSoporte().getTipoDocumento()
                            .getClase();
                    }

                    if (tramite.getTramiteDocumentacions().get(i)
                        .getDepartamento() != null) {
                        tramite.getTramiteDocumentacions().get(i)
                            .getDepartamento().getNombre();
                    }
                    if (tramite.getTramiteDocumentacions().get(i)
                        .getMunicipio() != null) {
                        tramite.getTramiteDocumentacions().get(i)
                            .getMunicipio().getNombre();
                    }
                }
            }

            if (tramite.getTramitePredioEnglobes() != null &&
                !tramite.getTramitePredioEnglobes().isEmpty()) {
                for (int j = 0; j < tramite.getTramitePredioEnglobes().size(); j++) {
                    tramite.getTramitePredioEnglobes().get(j).getPredio()
                        .getNumeroPredial();
                }
            }

            if (tramite.getSolicitud().getSolicitanteSolicituds() != null &&
                tramite.getSolicitud().getSolicitanteSolicituds().size() > 0) {
                for (SolicitanteSolicitud ss : tramite.getSolicitud()
                    .getSolicitanteSolicituds()) {
                    if (ss.getSolicitante() != null) {
                        ss.getSolicitante().getId();
                    }
                }
            }

            if (tramite.getTramiteDocumentos() != null &&
                !tramite.getTramiteDocumentos().isEmpty()) {
                for (int i = 0; i < tramite.getTramiteDocumentos().size(); i++) {

                    if (tramite.getTramiteDocumentos().get(i)
                        .getDocumento() != null) {
                        tramite.getTramiteDocumentos().get(i)
                            .getDocumento().getArchivo();

                        tramite.getTramiteDocumentos().get(i)
                            .getDocumento().getTipoDocumento().getClase();
                    }
                }
            }

            Hibernate.initialize(tramite.getTramiteRectificacions());
            Hibernate.initialize(tramite.getResultadoDocumento());

            return tramite;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#buscarTramiteSolicitudPorId");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findByEjecutorAndComision(java.lang.String, java.lang.Long)
     * @author pedro.garcia
     */
    /*
     * @modified pedro.garcia 30-05-2012 tener en cuenta que el trámite puede no tener predio
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<Tramite> findByEjecutorAndComision(String idFuncionarioEjecutor, Long idComision)
        throws ExcepcionSNC {

        LOGGER.debug("on TramiteDAOBean#findByEjecutorAndComision ...");
        List<Tramite> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t JOIN FETCH t.comisionTramites cts " +
            " JOIN FETCH t.departamento JOIN FETCH t.municipio " +
            " JOIN FETCH t.departamento JOIN FETCH t.municipio LEFT JOIN FETCH t.predio" //N: este pedazo funciona (raro porque cts es una lista de comisiones) al parecer porque, al comparar por id de la comisi{on,
            // la lista trae solo un elemento
            +
             " WHERE cts.comision.id = :idComisionP AND cts.ejecutor = :idEjecutorP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idComisionP", idComision);
            query.setParameter("idEjecutorP", idFuncionarioEjecutor);
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#findByEjecutorAndComision");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#getPrediosOfTramite(java.lang.Long)
     * @author pedro.garcia
     */
    /*
     * modified by fredy.wilches
     */
    @Implement
    @Override
    public List<Predio> getPrediosOfTramite(Long tramiteId) {

        List<Predio> answer = null;
        Tramite tramite;
        Predio predioQuinta;

        try {
            tramite = this.findAndFetchPrediosById(tramiteId);
            answer = new ArrayList<Predio>();

            if (tramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                tramite.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                tramite.getSubtipo().equals(EMutacion2Subtipo.ENGLOBE.getCodigo())) {
                for (TramitePredioEnglobe tpe : tramite
                    .getTramitePredioEnglobes()) {
                    answer.add(tpe.getPredio());
                }
            } else if (tramite.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) &&
                tramite.getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo())) {
                if (tramite.getPredio() != null) {
                    answer.add(tramite.getPredio());
                } else {
                    predioQuinta = new Predio();
                    predioQuinta.setNumeroPredial(tramite.getNumeroPredial());
                    predioQuinta.setAreaConstruccion(tramite
                        .getAreaConstruccion());
                    predioQuinta.setAreaTerreno(tramite.getAreaTerreno());
                    predioQuinta.setNip("Sin registro");
                    predioQuinta.setCondicionPropiedad("Sin registro");
                    predioQuinta.setConsecutivoCatastral(BigDecimal.ZERO);
                    predioQuinta.setManzanaCodigo("");
                    predioQuinta.setSectorCodigo("");
                    predioQuinta.setZonaUnidadOrganica("");
                    answer.add(predioQuinta);
                }
            } else {
                // D: cuando es un trámite de quinta, los pocos datos del predio
                // que
                // existen están almacenados
                // como atributos del trámite. Los demás que se muestran en los
                // detalles se deben inicializar
                // para evitar nullPointerExc

                answer.add(tramite.getPredio());
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
        return answer;
    }

    /**
     * @see ITramiteDAO#getPrediosOfTramiteQuintaNuevo(java.lang.Long)
     * @author carlos.ferro
     */
    @Implement
    @Override
    public List<Predio> getPrediosOfTramiteQuintaNuevo(Long tramiteId) {

        List<Predio> answer = null;
        Tramite tramite;
        Predio predioQuinta;

        try {
            tramite = this.findAndFetchPrediosById(tramiteId);
            answer = new ArrayList<Predio>();

            if (tramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                tramite.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                tramite.getSubtipo().equals(EMutacion2Subtipo.ENGLOBE.getCodigo())) {
                for (TramitePredioEnglobe tpe : tramite
                    .getTramitePredioEnglobes()) {
                    answer.add(tpe.getPredio());
                }
            } else if (tramite.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) &&
                tramite.getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo())) {
                if (tramite.getPredio() != null) {
                    answer.add(tramite.getPredio());
                } else {
                    if (tramite.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.getCodigo())) {
                        List<HPredio> hPredios = hPredioDAO.
                            buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(
                                tramite.getId());
                        for (HPredio hPredio : hPredios) {
                            predioQuinta = predioDAO.findById(Long.valueOf(hPredio.getPredioId().getId()));
                            Hibernate.initialize(predioQuinta.getDepartamento());
                            Hibernate.initialize(predioQuinta.getMunicipio());
                            answer.add(predioQuinta);
                            answer.add(hPredio.getPredioId());
                        }
                    } else {
                        List<PPredio> pPredios = pPredioDAO.getPPrediosByTramiteId(tramiteId);
                        for (PPredio pPredio : pPredios) {
                            predioQuinta = predioDAO.findById(Long.valueOf(pPredio.getPredio()));
                            Hibernate.initialize(predioQuinta.getDepartamento());
                            Hibernate.initialize(predioQuinta.getMunicipio());
                            answer.add(predioQuinta);
                        }
                    }
                }
            } else {
                // D: cuando es un trámite de quinta, los pocos datos del predio
                // que
                // existen están almacenados
                // como atributos del trámite. Los demás que se muestran en los
                // detalles se deben inicializar
                // para evitar nullPointerExc

                answer.add(tramite.getPredio());
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * Retorna un trámite haciendo fetch de los tramite_predio_englobe y fetch de los predios en
     * estos (tramite_predio_englobe)
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    private Tramite findAndFetchPrediosById(Long tramiteId) {

        LOGGER.debug("on TramiteDAOBean#findAndFetchPrediosById ...");

        Tramite answer = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.predio tp" +
            " LEFT JOIN FETCH tp.departamento" +
            " LEFT JOIN FETCH tp.municipio " +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe " +
            " LEFT JOIN FETCH tpe.predio p" + " WHERE t.id = :idTramiteP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramiteP", tramiteId);
            answer = (Tramite) query.getSingleResult();
        } catch (NoResultException nre) {
            LOGGER.error("... error: " + nre.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0002.getExcepcion(
                LOGGER, nre, "Tramite", tramiteId);

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#getDocsOfTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Tramite getDocsOfTramite(Long tramiteId) {

        LOGGER.debug("on TramiteDAOBean#getDocsOfTramite ...");
        Tramite answer = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " JOIN FETCH t.solicitud " +
            " LEFT JOIN FETCH t.predio " +
            " LEFT JOIN FETCH t.departamento " +
            " LEFT JOIN FETCH t.municipio " +
            " LEFT JOIN FETCH t.tramiteDocumentacions documentacion " +
            " LEFT JOIN FETCH documentacion.documentoSoporte LEFT JOIN FETCH documentacion.tipoDocumento " +
            " WHERE t.id = :tramiteIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIdP", tramiteId);
            answer = (Tramite) query.getSingleResult();

            Hibernate.initialize(answer.getSolicitud().getSolicitanteSolicituds());

            for (SolicitanteSolicitud ss : answer.getSolicitud().getSolicitanteSolicituds()) {

                Hibernate.initialize(ss.getDireccionPais());
                Hibernate.initialize(ss.getDireccionDepartamento());
                Hibernate.initialize(ss.getDireccionMunicipio());

            }

            Hibernate.initialize(answer.getSolicitud().getTramites());
            Hibernate.initialize(answer.getTramites());
            Hibernate.initialize(answer.getTramiteDocumentos());
            Hibernate.initialize(answer.getTramitePredioEnglobes());
            Hibernate.initialize(answer.getPredio());
            if (answer.getPredio() != null) {
                Hibernate.initialize(answer.getPredio().getPredioDireccions());
            }

            for (TramitePredioEnglobe tdp : answer.getTramitePredioEnglobes()) {
                Hibernate.initialize(tdp.getPredio());
                Hibernate.initialize(tdp.getPredio().getPredioDireccions());
            }

        } catch (Exception e) {
            LOGGER.error("Error on TramiteDAOBean#getDocsOfTramite: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#getDocsOfTramite");
        }
        return answer;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#getDocumentacionCompletaTramite(java.lang.Long)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public Tramite getDocumentacionCompletaTramite(Long tramiteId) {

        LOGGER.debug("on TramiteDAOBean#getDocumentacionCompletaTramite ...");
        Tramite answer = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.tramiteDocumentacions documentacion " +
            " LEFT JOIN FETCH documentacion.documentoSoporte " +
            " LEFT JOIN FETCH documentacion.tipoDocumento td" +
            " LEFT JOIN FETCH t.tramitePruebas tramitePruebas " +
            " LEFT JOIN FETCH tramitePruebas.autoPruebasDocumento autoPruebasDocumento" +
            " LEFT JOIN FETCH autoPruebasDocumento.tipoDocumento tdapd" +
            " WHERE t.id = :tramiteIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIdP", tramiteId);
            answer = (Tramite) query.getSingleResult();

            Hibernate.initialize(answer.getTramites());
            Hibernate.initialize(answer.getTramiteDocumentos());

            Hibernate.initialize(answer.getComisionTramites());
            Hibernate.initialize(answer.getTramitePruebas());

        } catch (Exception e) {
            LOGGER.error("Error on TramiteDAOBean#getDocumentacionCompletaTramite: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#getDocumentacionCompletaTramite");
        }
        return answer;

    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#getTramitesByResolucion (String resolucion, String documentoEstado, String
     * documentoEstadoAdicional)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> getTramitesByResolucion(String resolucion,
        String documentoEstado, String documentoEstadoAdicional) {

        LOGGER.debug("Entra a TramiteDAOBean#getTramitesByResolucion");
        List<Tramite> answer = null;
        String claseTipoDocumento = ETipoDocumentoClase.RESOLUCION.toString();
        String queryString, where;
        Query query;

        // estado adicional
        if (documentoEstadoAdicional != null &&
            !documentoEstadoAdicional.trim().isEmpty()) {
            where = " d.estado = :documentoEstadoAdicional ";
        } else {
            where = " 1 = 1 ";
        }

        queryString = " SELECT DISTINCT t FROM Tramite t" +
            " LEFT JOIN FETCH t.resultadoDocumento d " +
            " LEFT JOIN FETCH t.tramitePredioEnglobes" +
            " JOIN FETCH d.tipoDocumento td" +
            " JOIN FETCH t.departamento" +
            " JOIN FETCH t.municipio" +
            " JOIN FETCH t.predio" +
            " WHERE d.numeroDocumento = :resolucion AND td.clase = :claseTipoDocumento " +
            " AND ( d.estado = :documentoEstado OR" + where + " )";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("resolucion", resolucion);
            query.setParameter("claseTipoDocumento", claseTipoDocumento);
            query.setParameter("documentoEstado", documentoEstado);

            if (documentoEstadoAdicional != null &&
                !documentoEstadoAdicional.trim().isEmpty()) {
                query.setParameter("documentoEstadoAdicional",
                    documentoEstadoAdicional);
            }

            answer = (List<Tramite>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarTramitesConResolucionYNumeroPredial(String resolucion, String
     * numeroPredial)
     * @author david.cifuentes
	 * */
    @SuppressWarnings({"unchecked", "deprecation"})
    @Override
    public List<Tramite> buscarTramitesConResolucionYNumeroPredialPredio(
        String resolucion, String numeroPredial) {

        LOGGER.debug("TramiteDAOBean#buscarTramitesConResolucionYNumeroPredialPredio");

        List<Tramite> answer = null;
        String queryString;
        Query query;
        String claseTipoDocumento = ETipoDocumentoClase.RESOLUCION.toString();

        queryString = "SELECT DISTINCT t FROM VTramitePredio vtp " +
            " JOIN vtp.tramite t " +
            " JOIN vtp.predio p " +
            " JOIN t.resultadoDocumento doc " +
            " JOIN FETCH t.predio pre" +
            " JOIN FETCH pre.departamento" +
            " JOIN FETCH pre.municipio" +
            " JOIN FETCH t.departamento" +
            " JOIN FETCH t.municipio" +
            " WHERE p.numeroPredial = :numPredial " +
            " AND doc.tipoDocumento.clase = :claseTipoDocumento " +
            " AND doc.numeroDocumento = :resolucion ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numPredial", numeroPredial);
            query.setParameter("claseTipoDocumento", claseTipoDocumento);
            query.setParameter("resolucion", resolucion);

            answer = (ArrayList<Tramite>) query.getResultList();

            if (answer != null) {
                if (answer.get(0).getTramitePredioEnglobes() != null && !answer.get(0).
                    getTramitePredioEnglobes().isEmpty()) {
                    answer.get(0).getTramitePredioEnglobes().size();
                    for (TramitePredioEnglobe tpe : answer.get(0).getTramitePredioEnglobes()) {
                        tpe.getPredio();
                    }
                    answer.get(0).getPredio();
                    answer.get(0).getPredios();
                }
            }
        } catch (Exception e) {
            LOGGER.error("TramiteDAOBean#buscarTramitesConResolucionYNumeroPredialPredio error: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#buscarTramitesConResolucionYNumeroPredialPredio");
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(Long
     * tramiteId)
     * @author david.cifuentes
     */
    @Override
    public Tramite buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(
        Long tramiteId) {

        LOGGER.debug("TramiteDAOBean#buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId");

        Tramite answer = null;
        String queryString;
        Query query;

//REVIEW :: david.cifuentes :: 12-09-2013 :: qué query debe quedar???. Por el nombre del método suena que sea la primera asignación de queryString, pero la que se ejecuta es la segunda     
        queryString = "SELECT t FROM Tramite t " + "JOIN FETCH t.predio tp " +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe " +
            " LEFT JOIN FETCH tpe.predio p " + "WHERE t.id = :idTramite";

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.tramiteRectificacions tr LEFT JOIN FETCH tr.datoRectificar " +
            " WHERE t.id = :tramIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramite", tramiteId);
            answer = (Tramite) query.getSingleResult();

            answer.getTramiteDocumentacions().size();

        } catch (NoResultException nre) {
            LOGGER.error(
                "TramiteDAOBean#buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId ... error: " +
                nre.getMessage());
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#getTramiteRectificaciones(java.lang.Long)
     */
    @Implement
    @Override
    public List<TramiteRectificacion> getTramiteRectificaciones(Long idTramite) {

        LOGGER.debug("on TramiteDAOBean#getTramiteRectificaciones ...");
        List<TramiteRectificacion> answer = null;
        Tramite tempTramite;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.tramiteRectificacions tr LEFT JOIN FETCH tr.datoRectificar " +
            " WHERE t.id = :tramIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramIdP", idTramite);
            tempTramite = (Tramite) query.getSingleResult();
            if (tempTramite.getTramiteRectificacions() != null) {
                answer = tempTramite.getTramiteRectificacions();
            }
            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("TramiteDAOBean#getTramiteRectificaciones error: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#getTramiteRectificaciones");
        }

        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#obtenerFuncionariosEjecutores(List<Attributes>
     * atributosEjecutores)
     * @author javier.aponte
	 * */
    @Override
    public List<SEjecutoresTramite> obtenerFuncionariosEjecutoresConNumeroDeTramitesDeTerrenoYOficinaPorFuncionarioEjecutor(
        List<UsuarioDTO> atributosEjecutores) {
        List<SEjecutoresTramite> ejecutoresTramite = new ArrayList<SEjecutoresTramite>();
        SEjecutoresTramite ejecutor;
        String nombreFuncionario;
        String idFuncionario;
        Query queryTerreno;
        Query queryOficina;

        for (UsuarioDTO atributosEjecutor : atributosEjecutores) {

            nombreFuncionario = atributosEjecutor.getNombreCompleto();
            idFuncionario = atributosEjecutor.getLogin();
            ejecutor = new SEjecutoresTramite();

            queryTerreno = entityManager
                .createQuery("SELECT COUNT (t.clasificacion)" +
                    " FROM Tramite t" +
                    " WHERE t.clasificacion = " + ETramiteClasificacion.TERRENO.toString().
                        toUpperCase() +
                    " AND t.nombreFuncionarioEjecutor = :funcionario");
            queryTerreno.setParameter("funcionario", nombreFuncionario);

            queryOficina = entityManager
                .createQuery("SELECT COUNT (t.clasificacion) " +
                    " FROM Tramite t" +
                    " WHERE t.clasificacion = " + ETramiteClasificacion.OFICINA.toString().
                        toUpperCase() +
                    " AND t.nombreFuncionarioEjecutor= :funcionario");
            queryOficina.setParameter("funcionario", idFuncionario);
            ejecutor.setFuncionario(nombreFuncionario);
            ejecutor.setFuncionarioId(idFuncionario);
            ejecutor.setTerreno((Long) queryTerreno.getSingleResult());
            ejecutor.setOficina((Long) queryOficina.getSingleResult());

            ejecutoresTramite.add(ejecutor);
        }

        return ejecutoresTramite;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findTramiteConTramitePruebaEntidadByTramiteId(Long)
     * @author franz.gamba
     */
    @Override
    public Tramite findTramiteConTramitePruebaEntidadByTramiteId(Long tramiteId) {

        Query query = this.entityManager
            .createQuery("SELECT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " JOIN FETCH p.departamento d JOIN FETCH p.municipio m" +
                " JOIN FETCH t.tramitePruebas tp" +
                " LEFT JOIN FETCH tp.tramitePruebaEntidads tpe" +
                " LEFT JOIN FETCH tpe.direccionDepartamento" +
                " LEFT JOIN FETCH tpe.direccionMunicipio" +
                " WHERE t.id= :tramiteId");
        query.setParameter("tramiteId", tramiteId);
        Tramite tramite = null;
        try {
            tramite = (Tramite) query.getSingleResult();
            if (tramite.getTramitePruebas() != null) {
                tramite.getTramitePruebas().getTramitePruebaEntidads().size();
                for (TramitePruebaEntidad tpe : tramite.getTramitePruebas().
                    getTramitePruebaEntidads()) {
                    tpe.getDireccionDepartamento().getNombre();
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramiteConTramitePruebaEntidadByTramiteId");
        }
        return tramite;
    }
    //----------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#findTramiteCargarPruebasByTramiteId(java.lang.Long)
     */
    @Override
    public Tramite findTramiteCargarPruebasByTramiteId(Long tramiteId) {

        Query query = this.entityManager
            .createQuery("SELECT t FROM Tramite t " +
                " LEFT JOIN FETCH t.predio p " +
                " LEFT JOIN FETCH p.departamento d " +
                " LEFT JOIN FETCH p.municipio m " +
                " JOIN FETCH t.solicitud " +
                " LEFT JOIN FETCH t.tramitePruebas tp " +
                " WHERE t.id =:tramiteId");
        query.setParameter("tramiteId", tramiteId);

        Tramite tramite = null;
        try {
            tramite = (Tramite) query.getSingleResult();

            if (tramite.getTramitePruebas() != null) {
                if (tramite.getTramitePruebas().getTramitePruebaEntidads() != null) {
                    tramite.getTramitePruebas().getTramitePruebaEntidads().size();
                    for (TramitePruebaEntidad tpe : tramite.getTramitePruebas().
                        getTramitePruebaEntidads()) {
                        tpe.getRazonSocial();
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("TramiteDAOBean#findTramiteCargarPruebasByTramiteId error: " +
                e.getMessage(), e);
        }
        return tramite;
    }
    //----------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findTramitePruebasByTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified by leidy.gonzalez :: #16501 :: 06/04/2016 Se inicializa nombre de la entidad del
     * solicitante
     */
    @SuppressWarnings({"deprecation"})
    @Override
    public Tramite findTramitePruebasByTramiteId(Long tramiteId) {

        Query query = this.entityManager
            .createQuery("SELECT t FROM Tramite t LEFT JOIN FETCH t.predio p " +
                " LEFT JOIN FETCH p.departamento d LEFT JOIN FETCH p.municipio m " +
                " JOIN FETCH t.solicitud" +
                " LEFT JOIN FETCH t.tramiteDocumentacions td" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH td.documentoSoporte tdd" +
                " LEFT JOIN FETCH tdd.tipoDocumento" +
                " WHERE t.id= :tramiteId");
        query.setParameter("tramiteId", tramiteId);

        Tramite tramite = null;
        try {

            tramite = (Tramite) query.getSingleResult();

            if (tramite.getTramitePredioEnglobes() != null) {
                Hibernate.initialize(tramite.getTramitePredioEnglobes());
                for (TramitePredioEnglobe tpe : tramite
                    .getTramitePredioEnglobes()) {

                    if (tpe.getPredio() != null) {
                        Hibernate.initialize(tpe.getPredio());
                        Hibernate.initialize(tpe.getPredio().getDepartamento());
                        Hibernate.initialize(tpe.getPredio().getMunicipio());
                        if (tpe.getPredio().getPredioDireccions() != null &&
                            !tpe.getPredio().getPredioDireccions().isEmpty()) {
                            Hibernate.initialize(tpe.getPredio().getPredioDireccions());
                            for (PredioDireccion pd : tpe.getPredio().getPredioDireccions()) {
                                Hibernate.initialize(pd);
                            }
                        }
                    }
                }
            }

            if (tramite.getComisionTramites() != null) {
                Hibernate.initialize(tramite.getComisionTramites());
                for (ComisionTramite ct : tramite.getComisionTramites()) {
                    if (ct.getComision() != null) {
                        Hibernate.initialize(ct.getComision());
                    }
                }
            }

            if (tramite.getTramites() != null) {
                Hibernate.initialize(tramite.getTramites());
            }
            Hibernate.initialize(tramite.getDepartamento());
            Hibernate.initialize(tramite.getMunicipio());

            if (tramite.getTramitePruebas() != null &&
                !tramite.getTramitePruebas().getTramitePruebaEntidads().isEmpty() &&
                tramite.getTramitePruebas().getTramitePruebaEntidads() != null) {

                Hibernate.initialize(tramite.getTramitePruebas());
                Hibernate.initialize(tramite.getTramitePruebas().getTramitePruebaEntidads());

                for (TramitePruebaEntidad tpe : tramite.getTramitePruebas().
                    getTramitePruebaEntidads()) {
                    if (tpe.getDireccionDepartamento() != null) {
                        Hibernate.initialize(tpe.getDireccionDepartamento());
                    }

                    if (tpe.getEstructuraOrganizacional() != null) {
                        Hibernate.initialize(tpe.getEstructuraOrganizacional());
                    }

                    if (tpe.getSolicitante() != null) {
                        Hibernate.initialize(tpe.getSolicitante());
                    }

                    if (tpe.getRazonSocial() != null) {
                        Hibernate.initialize(tpe.getRazonSocial());
                    }

                    if (tpe.getNombreCompleto() != null) {
                        Hibernate.initialize(tpe.getNombreCompleto());
                    }
                }
                if (tramite.getTramitePruebas().getAutoPruebasDocumento() != null) {
                    Hibernate.initialize(tramite.getTramitePruebas().getAutoPruebasDocumento());
                }
            }

            if (tramite.getSolicitud().getSolicitanteSolicituds() != null) {
                Hibernate.initialize(tramite.getSolicitud().getSolicitanteSolicituds());

                for (SolicitanteSolicitud solSol : tramite.getSolicitud().getSolicitanteSolicituds()) {
                    if (solSol.getDireccionPais() != null) {
                        Hibernate.initialize(solSol.getDireccionPais());
                    }
                    if (solSol.getDireccionDepartamento() != null) {
                        Hibernate.initialize(solSol.getDireccionDepartamento());
                    }
                    if (solSol.getDireccionMunicipio() != null) {
                        Hibernate.initialize(solSol.getDireccionMunicipio());
                    }
                    if (solSol.getSolicitante() != null) {
                        Hibernate.initialize(solSol.getSolicitante());
                    }
                    if (solSol.getSolicitud() != null) {
                        Hibernate.initialize(solSol.getSolicitud());
                    }

                }
            }

            if (tramite.getSolicitanteTramites() != null) {
                Hibernate.initialize(tramite.getSolicitanteTramites());
                for (SolicitanteTramite solTram : tramite.getSolicitanteTramites()) {
                    if (solTram.getDireccionPais() != null) {
                        Hibernate.initialize(solTram.getDireccionPais());
                    }
                    if (solTram.getDireccionDepartamento() != null) {
                        Hibernate.initialize(solTram.getDireccionDepartamento());
                    }
                    if (solTram.getDireccionMunicipio() != null) {
                        Hibernate.initialize(solTram.getDireccionMunicipio());
                    }
                }
            }

            if (tramite.getTramiteEstados() != null) {
                Hibernate.initialize(tramite.getTramiteEstados());
            }

            if (tramite.getTramiteDocumentos() != null) {
                Hibernate.initialize(tramite.getTramiteDocumentos());
                for (TramiteDocumento td : tramite.getTramiteDocumentos()) {
                    if (td.getDocumento() != null && td.getDocumento().getTipoDocumento() != null) {
                        Hibernate.initialize(td.getDocumento());
                        Hibernate.initialize(td.getDocumento().getTipoDocumento());
                    }
                }
            }

            if (tramite.getPredio() != null) {

                Hibernate.initialize(tramite.getPredio());
                if (tramite.getPredio().getPredioDireccions() != null &&
                    !tramite.getPredio().getPredioDireccions().isEmpty()) {

                    Hibernate.initialize(tramite.getPredio().getPredioDireccions());

                    for (PredioDireccion pd : tramite.getPredio().getPredioDireccions()) {
                        if (pd != null && pd.getPredio() != null) {
                            Hibernate.initialize(pd.getPredio());
                        }
                    }
                }
            }

            if (tramite.getTramiteDepuracions() != null) {
                Hibernate.initialize(tramite.getTramiteDepuracions());
                for (TramiteDepuracion td : tramite.getTramiteDepuracions()) {
                    Hibernate.initialize(td.getRazonesRechazo());
                    if (td.getTramiteDepuracionObservacions() != null) {
                        Hibernate.initialize(td.getTramiteDepuracionObservacions());
                    }
                }
            }

            if (tramite.getTramiteRectificacions() != null) {
                Hibernate.initialize(tramite.getTramiteRectificacions());
            }

            if (tramite.getTramiteInconsistencias() != null) {
                Hibernate.initialize(tramite.getTramiteInconsistencias());
                for (TramiteInconsistencia ti : tramite.getTramiteInconsistencias()) {
                    Hibernate.initialize(ti);
                }
            }

            // TODO fredy.wilches::24/04/2012::En los tramites de mutacion de
            // quinta, no hay predio, hay que revisar todo el proceso para
            // cuando este objeto, dpto y municipio no existen. Sino hay
            // problema sobraria dpto y municipio?::fredy.wilches
        } catch (Exception e) {
            LOGGER.error("TramiteDAOBean#findTramitePruebasByTramiteId error: " +
                e.getMessage(), e);
        }
        return tramite;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findTramitePrediosSimplesByTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified by leidy.gonzalez
     */
    @Override
    public Tramite findTramitePrediosSimplesByTramiteId(Long tramiteId) {

        Query query;

        Tramite tramite = null;
        try {
            query = this.entityManager
                .createQuery("SELECT t FROM Tramite t LEFT JOIN FETCH t.predio p " +
                    " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
                    " WHERE t.id= :tramiteId");
            query.setParameter("tramiteId", tramiteId);
            tramite = (Tramite) query.getSingleResult();
            if (tramite.getResultadoDocumento() != null) {
                Hibernate.initialize(tramite.getResultadoDocumento());

                if (tramite.getResultadoDocumento().getTipoDocumento() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento().getTipoDocumento());
                }
                if (tramite.getResultadoDocumento().getTipoDocumento().getId() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento().getTipoDocumento().getId());
                }
                if (tramite.getResultadoDocumento().getTipoDocumento().getNombre() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento().getTipoDocumento().
                        getNombre());
                }
            }
            for (Predio predio : tramite.getPredios()) {
                Hibernate.initialize(predio.getPredioDireccions());
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramitePruebasByTramiteId");
        }
        return tramite;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     */
    /*
     * @modified pedro.garcia manejo de excepciones nov-2013 CC-NP-CO-034 solo se traen las de
     * conservación. Se tienen en cuenta las canceladas porque el movimiento del trámite, cuando la
     * comisión se ha cancelado, se hace en esta actividad
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<Tramite> getByIdsFetchComisionParaAlistarInfo(long idsTramites[]) {

        List<Tramite> result = null;
        String queryString;
        Query query;
        List<Long> idsTramitesParam;
        Long[] idsTramitesAsLong;
        List<String> estadosExcluidos;

        queryString = "" +
            "SELECT t" +
            " FROM Tramite t" + " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH t.comisionTramites ct" +
            " LEFT JOIN FETCH ct.comision comision" +
            " WHERE t.id in (:tramiteIds_p)" +
            " AND comision.estado NOT IN (:listaEstadosExcluidos_p)";

        //D: para esta la actividad en donde se usa este método solo se muestran las comisiones de conservación  
        queryString += " AND comision.tipo = '" + EComisionTipo.CONSERVACION.getCodigo() + "'";

        //N: truquini para convertir en un ArrayList el long[] que llega, para poder usarlo directamente como parámetro
        idsTramitesAsLong = ArrayUtils.toObject(idsTramites);
        idsTramitesParam = Arrays.asList(idsTramitesAsLong);

        estadosExcluidos = new ArrayList<String>();
        estadosExcluidos.add(EComisionEstado.FINALIZADA.getCodigo());

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIds_p", idsTramitesParam);
            query.setParameter("listaEstadosExcluidos_p", estadosExcluidos);

            result = (List<Tramite>) query.getResultList();
            for (Tramite t : result) {
                t.getTramitePredioEnglobes().size();
                for (TramitePredioEnglobe tpe : t.getTramitePredioEnglobes()) {
                    tpe.getPredio().getNumeroPredial();
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#findByIdsFetchComision");
        }

        return result;

    }
// --------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#buscarTramitesDeEjecutorAsignado(String, String, int...) OJO: no cambiar
     * nunca el join fetch t.comisionTramites porque no funcionarían otras cosas
     */
    @Override
    public List<Tramite> buscarTramitesAsignadosAEjecutor(String idTerritorial,
        String ejecutor, final int... rowStartIdxAndCount) {

        LOGGER.debug("Entra en TramiteDAOBean#buscarTramitesAsignadosAEjecutor");

        String sql = "SELECT t FROM Tramite t, Jurisdiccion j" +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE (j.estructuraOrganizacional" +
            " IN (SELECT eo FROM EstructuraOrganizacional eo" +
            " WHERE eo.estructuraOrganizacionalCod = :idTerritorial)" +
            " OR j.estructuraOrganizacional.codigo = :idTerritorial)" +
            " AND j.municipio = t.municipio" +
            " AND t.funcionarioEjecutor = :ejecutor";
        Query q = entityManager.createQuery(sql);
        q.setParameter("ejecutor", ejecutor);
        q.setParameter("idTerritorial", idTerritorial);

        List<Tramite> tramitesEjecutor = (super.findInRangeUsingQuery(q,
            rowStartIdxAndCount));

        return tramitesEjecutor;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#findTramitesParaAsignacionDeEjecutor(long[], int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> findTramitesParaAsignacionDeEjecutor(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        List<Tramite> answer = null;

        try {
            answer = (List<Tramite>) this.findTramitesAsignacionDeEjecutor(
                idsTramites, false, false, sortField, sortOrder, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findTramitesConEjecutorAsignado(long[], int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> findTramitesConEjecutorAsignado(long[] idsTramites,
        int... rowStartIdxAndCount) {

        List<Tramite> answer;

        try {
            answer = (List<Tramite>) this.findTramitesAsignacionDeEjecutor(
                idsTramites, true, false, null, null, null, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#countTramitesParaAsignacionDeEjecutor(long[], int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int countTramitesParaAsignacionDeEjecutor(long[] idsTramites) {

        int answer = 0;
        BigInteger temp;

        try {
            temp = (BigInteger) this.findTramitesAsignacionDeEjecutor(idsTramites,
                false, true, null, null, null);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        answer = temp.intValue();

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#countTramitesEjecutorAsignado(long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int countTramitesEjecutorAsignado(long[] idsTramites) {

        int answer = 0;
        BigInteger temp;

        try {
            temp = (BigInteger) this.findTramitesAsignacionDeEjecutor(idsTramites,
                true, true, null, null, null);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        answer = temp.intValue();

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace el query de los trámites que cuyos ids estén en el arreglo Según el flag isCount
     * devuelve solo el conteo de registros
     *
     * OJO: si no hay una codición adicional se puede usar WHERE 1=1 y luego concatenar el IN
     *
     * OJO: se está suponiendo que el arreglo de ids de trámites no tiene más de 1000. Si los
     * tuviera habría que usar la forma de consulta que se usa en findTramitesByIds
     *
     * @see ITramiteDAO#findTramitesByIds(java.util.List, java.lang.String, java.lang.String, int[])
     *
     * @author pedro.garcia
     *
     * @param idsTramites arreglo con los id de trámite
     * @param comisionado diferencia el query para traer los trámites que están o no comisionados
     * @param isCount dice si lo que se quiere es contar las filas resultados
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     * @param sortOrder nombre del ordenamiento (UNSORTED, DESCENDING, DESCENDING)
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount
     * @return
     */
    private Object findTramitesComision(long[] idsTramites, boolean comisionado, boolean isCount,
        String sortField, String sortOrder, Map<String, String> filters,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteDAOBean#findTramitesComision");

        List<Tramite> answer1;
        BigInteger answer2;
        Object answer = null;
        StringBuilder queryString, idTramiteIn;
        Query query;
        String comisionadoSiNo, clasificacionTramite;

        queryString = new StringBuilder();
        idTramiteIn = new StringBuilder();

        idTramiteIn.append(" AND t.id IN (");
        for (long idTramite : idsTramites) {
            idTramiteIn.append(idTramite).append(",");
        }
        idTramiteIn.deleteCharAt(idTramiteIn.lastIndexOf(","));
        idTramiteIn.append(")");

        if (!isCount) {
            queryString.append("SELECT t FROM Tramite t LEFT JOIN FETCH t.predio p " +
                " LEFT JOIN FETCH p.municipio LEFT JOIN FETCH p.departamento " +
                " WHERE 1 = 1 ");
        } else {
            // N: esta, para dejarla más liviana se hace de forma diferente
            queryString.append("SELECT count(t) " +
                " FROM Tramite t WHERE 1 = 1 ");

        }

        //D: revisar filtros de búsqueda.
        //N: el operador sql que se use depende del modo de emparejamiento definido en el xhtml (ej: si filterMatchMode="exact" => se usa '=')
        if (filters.containsKey("nombreFuncionarioEjecutor")) {
            queryString.append(" AND t.nombreFuncionarioEjecutor LIKE :nombreFuncionarioEjecutor ");
        }
        if (filters.containsKey("numeroRadicacion")) {
            queryString.append(" AND t.numeroRadicacion LIKE :numeroRadicacion_p ");
        }
        if (filters.containsKey("clasificacion")) {
            queryString.append(" AND UPPER(t.clasificacion) LIKE :clasificacion_p ");
        }

        queryString.append(idTramiteIn);

        clasificacionTramite = ETramiteClasificacion.TERRENO.toString();
        if (comisionado) {
            comisionadoSiNo = ESiNo.SI.toString();
            queryString.append(" AND t.comisionado = :comisionadoP ");
        } else {
            comisionadoSiNo = ESiNo.NO.toString();
            queryString.append(" AND (t.comisionado = :comisionadoP OR t.comisionado IS NULL)");
        }

        //D: se agrega el posible ordenamiento al query
        //OJO: se asume que si va a contar no se necesita ordenamiento
        if (!isCount) {
            if (sortField != null && !sortField.equals("") && !sortOrder.equals("UNSORTED")) {
                queryString.append(" ORDER BY t.").append(sortField).
                    append(sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("comisionadoP", comisionadoSiNo);

            if (filters.containsKey("nombreFuncionarioEjecutor")) {
                query.setParameter("nombreFuncionarioEjecutor", "%" +
                    filters.get("nombreFuncionarioEjecutor") + "%");
            }
            if (filters.containsKey("numeroRadicacion")) {
                query.
                    setParameter("numeroRadicacion_p", "%" + filters.get("numeroRadicacion") + "%");
            }

            if (filters.containsKey("clasificacion")) {
                query.setParameter("clasificacion_p", "%" + filters.get("clasificacion") + "%");
            }

            if (!isCount) {
                answer1 = (List<Tramite>) super.findInRangeUsingQuery(query, rowStartIdxAndCount);
                for (Tramite tramite : answer1) {
                    tramite.getTramitePredioEnglobes().size();
                }

                answer = answer1;
            } else {
                answer2 = new BigInteger(query.getSingleResult().toString());
                answer = answer2;
            }
        } catch (Exception e) {
            LOGGER.error("error on TramiteDAOBean#findTramitesComision: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramitesComision");
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#findTramitesPorComisionar(long[], String, String, int[])
     * @author pedro.garcia
     * @version 2.0
     */
    @Implement
    @Override
    public List<Tramite> findTramitesPorComisionar(long[] idsTramites, String sortField,
        String sortOrder, Map<String, String> filters, int... rowStartIdxAndCount) {

        List<Tramite> answer;

        try {
            answer = (List<Tramite>) this.findTramitesComision(idsTramites, false, false, sortField,
                sortOrder, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#countTramitesPorComisionar(long[], java.lang.String, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int countTramitesPorComisionar(long[] idsTramites, String tipoComision) {
        int answer = 0;
        BigInteger temp;

        HashMap<String, String> filters = new HashMap<String, String>();
        if (tipoComision.equals(EComisionTipo.CONSERVACION.getCodigo())) {
            filters.put("clasificacion", ETramiteClasificacion.TERRENO.toString());
        }

        try {
            temp = (BigInteger) this.findTramitesComision(idsTramites, false, true, "",
                "UNSORTED", filters);
            answer = temp.intValue();
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findTramitesByIds(List<Long>)
     * @author fredy.wilches
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findTramitesByIds(List<Long> tramiteIds) {

        String sql;
        Query q;
        if (tramiteIds.size() >= 1000) {
            Query q2 = entityManager.createNativeQuery("SELECT SESION_ID_SEQ.NEXTVAL FROM DUAL");
            Object o = q2.getSingleResult();
            Query q3 = entityManager.createNativeQuery(
                "INSERT INTO tmp_tramite_session (sesion_id, tramite_id) VALUES (?,?)");
            q3.setParameter(1, (BigDecimal) o);

            for (Long id : tramiteIds) {
                q3.setParameter(2, id);
                q3.executeUpdate();
            }
            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.resultadoDocumento rd " +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH t.tramitePruebas" +
                " WHERE t.id IN (SELECT tts.id.tramiteId FROM TmpTramiteSession tts where tts.id.sesionId=:sesionId)";
            q = entityManager.createQuery(sql);
            q.setParameter("sesionId", (BigDecimal) o);

        } else {

            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.resultadoDocumento rd " +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH t.tramitePruebas" +
                " WHERE t.id IN (:tramiteIds)";

            q = this.entityManager.createQuery(sql);
            if (tramiteIds == null || tramiteIds.isEmpty()) {
                ArrayList<Long> vacio = new ArrayList<Long>();
                vacio.add(0L);
                q.setParameter("tramiteIds", vacio);
            } else {
                q.setParameter("tramiteIds", tramiteIds);
            }
        }
        try {
            List<Tramite> tramites = q.getResultList();
            for (Tramite tramite : tramites) {
                if (tramite.getTramite() != null) {
                    tramite.getTramite().getId();
                }
                tramite.getSolicitud().getSolicitanteSolicituds().size();
                tramite.getTramitePredioEnglobes().size();
                tramite.getComisionTramites().size();
                tramite.getTramiteEstados().size();
                tramite.getTramiteDocumentacions().size();
                tramite.getTramiteDocumentos().size();
                for (ComisionTramite ct : tramite.getComisionTramites()) {
                    ct.getComision().getNumero();
                }
            }

            return tramites;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámites por múltiples ids", e);
        }
    }
    //---------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#findTramitesByIdsParaAprobar(java.util.List)
     */
    @Override
    public List<Tramite> findTramitesByIdsParaAprobar(List<Long> tramiteIds) {

        String sql;
        Query q;
        if (tramiteIds.size() >= 1000) {
            Query q2 = entityManager.createNativeQuery("SELECT SESION_ID_SEQ.NEXTVAL FROM DUAL");
            Object o = q2.getSingleResult();
            Query q3 = entityManager.createNativeQuery(
                "INSERT INTO tmp_tramite_session (sesion_id, tramite_id) VALUES (?,?)");
            q3.setParameter(1, (BigDecimal) o);

            for (Long id : tramiteIds) {
                q3.setParameter(2, id);
                q3.executeUpdate();

            }
            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.tramite " +
                " LEFT JOIN FETCH t.tramiteEstados te" +
                " WHERE t.id IN (SELECT tts.id.tramiteId FROM TmpTramiteSession tts where tts.id.sesionId=:sesionId)";
            q = entityManager.createQuery(sql);
            q.setParameter("sesionId", (BigDecimal) o);

        } else {

            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.tramite" +
                " LEFT JOIN FETCH t.tramiteEstados te" +
                " WHERE t.id IN (:tramiteIds)";

            q = entityManager.createQuery(sql);
            q.setParameter("tramiteIds", tramiteIds);
        }
        try {
            List<Tramite> tramites = q.getResultList();
            for (Tramite tramite : tramites) {
                if (tramite.getTramite() != null) {
                    tramite.getTramite().getId();
                }
                tramite.getTramiteDocumentacions().size();
                if (tramite.getTramiteDocumentos() != null) {
                    tramite.getTramiteDocumentos().size();
                    for (TramiteDocumento td : tramite.getTramiteDocumentos()) {
                        td.getNumeroDocumento();
                    }
                }
                for (TramiteEstado te : tramite.getTramiteEstados()) {
                    LOGGER.debug("" + te.getId());
                }
            }

            return tramites;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámites: findTramitesByIdsParaAprobar", e);
        }
    }

    /**
     *
     * @param tramiteIds
     * @param sortField
     * @param sortOrder
     * @param contadoresDesdeYCuantos
     * @return
     */
//TODO :: fredy.wilches :: 04-05-2012 :: documentar lo que hace el método. Hacerlo en la interfaz :: pedro.garcia
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findTramitesByIds(List<Long> tramiteIds, String sortField, String sortOrder,
        Map<String, String> filters, int... contadoresDesdeYCuantos) {

        String sql;
        String sqlBasico;
        Query q;
        String where = "";

        Map<String, Object> valoresFiltros = new HashMap<String, Object>();
        if (filters != null) {
            for (String campo : filters.keySet()) {
                if (campo.equals("numeroRadicacion") || campo.equals("solicitud.numero")) {
                    if (campo.equals("solicitud.numero")) {
                        where += " AND t." + campo + " like :solicitudNumero";
                        valoresFiltros.put("solicitudNumero", "%" + filters.get(campo) + "%");

                    } else {
                        where += " AND t." + campo + " like :" + campo;
                        valoresFiltros.put(campo, "%" + filters.get(campo) + "%");
                    }
                }
                LOGGER.debug("Campo filtro " + campo);
                LOGGER.debug("Tamaño " + filters.get(campo).length());
                if (campo.equals("fechaRadicacion") && filters.get(campo) != null && filters.get(
                    campo).length() == 10) {
                    LOGGER.debug("Debe filtrar por fechaRadicacion");
                    try {
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                        LOGGER.debug(filters.get(campo));
                        LOGGER.debug("" + df.parse(filters.get(campo)));

                        valoresFiltros.put(campo, df.parse(filters.get(campo)));

                        Date fecha2 = df.parse(filters.get(campo));
                        LOGGER.debug("" + fecha2);
                        Calendar c = Calendar.getInstance();
                        c.setTime(fecha2);
                        c.add(Calendar.DATE, 1);
                        LOGGER.debug("" + c);
                        valoresFiltros.put("fechaRadicacion1", c.getTime());

                        where += " AND t." + campo + " between :" + campo + " and :fechaRadicacion1";
                        LOGGER.debug(where);

                    } catch (ParseException e) {
                        LOGGER.error(e.getMessage());
                    }

                }
            }
        }

        List<Tramite> tramites;
        if (tramiteIds.size() >= 1000) {
            Query q2 = entityManager.createNativeQuery("SELECT SESION_ID_SEQ.NEXTVAL FROM DUAL");
            Object o = q2.getSingleResult();
            for (Long id : tramiteIds) {
                TmpTramiteSession tts = new TmpTramiteSession();
                TmpTramiteSessionId ttsId = new TmpTramiteSessionId();
                ttsId.setSesionId((BigDecimal) o);
                ttsId.setTramiteId(new BigDecimal(id));
                tts.setId(ttsId);
                entityManager.persist(tts);
            }
            sqlBasico = "SELECT DISTINCT t.id FROM Tramite t " +
                " WHERE t.id IN (SELECT tts.id.tramiteId FROM TmpTramiteSession tts where tts.id.sesionId=:sesionId)";

            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.departamento" +
                " LEFT JOIN FETCH t.municipio" +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH t.tramitePruebas" +
                " WHERE t.id IN (:tramiteIds)";

            if (valoresFiltros.size() > 0) {
                sqlBasico += where;
                sql += where;
            }

            if (sortField != null && !sortOrder.equals("UNSORTED")) {

                if (sortField.equals("tipoTramite")) {
                    sortField =
                        "tipoTramite" + (sortOrder.equals("DESCENDING") ? " DESC" : " ASC") +
                        ", t.claseMutacion" + (sortOrder.equals("DESCENDING") ? " DESC" : " ASC") +
                        ", t.subtipo" + (sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
                }

                sql += " ORDER BY t." + sortField + (sortOrder.equals("DESCENDING") ? " DESC" :
                    " ASC");
                sqlBasico += " ORDER BY t." + sortField +
                    (sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

            q = entityManager.createQuery(sqlBasico);
            q.setParameter("sesionId", (BigDecimal) o);

            if (valoresFiltros.size() > 0) {
                for (String campo : valoresFiltros.keySet()) {
                    q.setParameter(campo, valoresFiltros.get(campo));
                    LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
                }
            }

        } else {
            sqlBasico = "SELECT DISTINCT t.id FROM Tramite t" +
                " WHERE t.id IN (:tramiteIds)";

            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.departamento" +
                " LEFT JOIN FETCH t.municipio" +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH t.tramitePruebas" +
                " WHERE t.id IN (:tramiteIds)";

            if (valoresFiltros.size() > 0) {
                sqlBasico += where;
                sql += where;
            }

            if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
                if (sortField.equals("tipoTramite")) {
                    sortField =
                        "tipoTramite" + (sortOrder.equals("DESCENDING") ? " DESC" : " ASC") +
                        ", t.claseMutacion" + (sortOrder.equals("DESCENDING") ? " DESC" : " ASC") +
                        ", t.subtipo" + (sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
                }
                sql += " ORDER BY t." + sortField + (sortOrder.equals("DESCENDING") ? " DESC" :
                    " ASC");
                sqlBasico += " ORDER BY t." + sortField +
                    (sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

            LOGGER.debug("********************* SQL Basico *************");
            LOGGER.debug(sql);

            q = entityManager.createQuery(sqlBasico);
            q.setParameter("tramiteIds", tramiteIds);
            if (valoresFiltros.size() > 0) {
                for (String campo : valoresFiltros.keySet()) {
                    q.setParameter(campo, valoresFiltros.get(campo));
                    LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
                }
            }

        }
        try {
            if (contadoresDesdeYCuantos != null && contadoresDesdeYCuantos.length > 0) {
                int rowStartIdx = Math.max(0, contadoresDesdeYCuantos[0]);
                if (rowStartIdx > 0) {
                    q.setFirstResult(rowStartIdx);
                }

                if (contadoresDesdeYCuantos.length > 1) {
                    int rowCount = Math.max(0, contadoresDesdeYCuantos[1]);
                    if (rowCount > 0) {
                        q.setMaxResults(rowCount);
                    }
                }
            }

            List<Long> tramitesIdsPagina = (List<Long>) q.getResultList();
            if (tramitesIdsPagina == null || tramitesIdsPagina.isEmpty()) {
                return new ArrayList<Tramite>();
            }

            LOGGER.debug("********************* SQL *************");
            LOGGER.debug(sql);

            q = entityManager.createQuery(sql);
            if (tramiteIds == null || tramiteIds.size() == 0) {
                ArrayList<Long> vacio = new ArrayList<Long>();
                vacio.add(0L);
                q.setParameter("tramiteIds", vacio);
            } else {
                q.setParameter("tramiteIds", tramitesIdsPagina);
            }

            if (valoresFiltros.size() > 0) {
                for (String campo : valoresFiltros.keySet()) {
                    q.setParameter(campo, valoresFiltros.get(campo));
                    LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
                }
            }

            tramites = (List<Tramite>) q.getResultList();

            for (Tramite tramite : tramites) {
                tramite.getTramitePredioEnglobes().size();
                tramite.getComisionTramites().size();
                tramite.getTramiteEstados().size();
                tramite.getTramiteDocumentacions().size();
                for (ComisionTramite ct : tramite.getComisionTramites()) {
                    ct.getComision().getNumero();
                }
            }
        } catch (IndexOutOfBoundsException ioe) {
            LOGGER.error(ioe.getMessage());
            return new ArrayList<Tramite>();

        } catch (Exception e) {
            System.out.println(e);
            System.out.println(e.getMessage());
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámites por múltiples ids", e);
        }
        return tramites;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#findTramiteTramiteDocumentacionDocumentoById(Long)
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    @Override
    public Tramite findTramiteTramiteDocumentacionDocumentoById(Long tramiteId) {
        LOGGER.debug("en TramiteDAOBean#findTramiteTramiteDocumentacionDocumentoById");

        Query q = this.entityManager.createQuery("SELECT  t" + " FROM Tramite t" +
            " JOIN FETCH t.departamento" + " JOIN FETCH t.municipio" +
            " JOIN FETCH t.solicitud" + " LEFT JOIN FETCH t.predio" +
            " LEFT JOIN FETCH t.tramiteDocumentacions td" +
            " LEFT JOIN FETCH td.tipoDocumento" +
            " LEFT JOIN FETCH td.documentoSoporte tdd" +
            " LEFT JOIN FETCH tdd.tipoDocumento" +
            " LEFT JOIN FETCH td.departamento" +
            " LEFT JOIN FETCH td.municipio" + " WHERE t.id= :tramiteId");

        try {
            q.setParameter("tramiteId", tramiteId);
            Tramite tramiteAnswer = (Tramite) q.getSingleResult();
            return tramiteAnswer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramiteTramiteDocumentacionDocumentoById");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#findTramiteByIdForAsignacion(java.lang.Long)
     * @author pedro.garcia
     */
    /*
     * @modified by juanfelipe.garcia :: 18-10-2013 :: se adiciona validación para
     * TramiteDepuracions, inicialización de los tramites asociados a la solicitud y sus respectivos
     * documentos
     */
    @Implement
    @Override
    public Tramite findTramiteByIdForAsignacion(Long tramiteId) {

        LOGGER.debug("finding trámite on TramiteDAOBean#findTramiteByIdForAsignacion ...");

        Tramite answer = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " JOIN FETCH t.solicitud s " +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH t.tramiteDocumentacions td " +
            " LEFT JOIN FETCH td.tipoDocumento " +
            " LEFT JOIN FETCH td.departamento" +
            " LEFT JOIN FETCH td.documentoSoporte" +
            " LEFT JOIN FETCH td.municipio " +
            " LEFT JOIN FETCH t.departamento " +
            " LEFT JOIN FETCH t.municipio " +
            " WHERE t.id = :tramIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramIdP", tramiteId);
            answer = (Tramite) query.getSingleResult();

            if (answer.getTramiteRectificacions() != null) {
                answer.getTramiteRectificacions().size();
                for (TramiteRectificacion tRect : answer.getTramiteRectificacions()) {
                    Hibernate.initialize(tRect.getDatoRectificar());
                }
            }

            if (answer.getComisionTramites() != null) {
                answer.getComisionTramites().size();
            }

            if (answer.getTramites() != null) {
                answer.getTramites().size();
            }
            if (answer.getTramitePredioEnglobes() != null) {
                answer.getTramitePredioEnglobes().size();
            }

            if (answer.getSolicitud().getSolicitanteSolicituds() != null) {
                answer.getSolicitud().getSolicitanteSolicituds().size();
            }

            Hibernate.initialize(answer.getSolicitud().getTramites());

            if (answer.getSolicitud().getTramites() != null) {
                try {
                    for (Tramite t : answer.getSolicitud().getTramites()) {
                        t.getTramitePredioEnglobes().size();
                        t.getPredio();
                        for (TramitePredioEnglobe tpe : t.getTramitePredioEnglobes()) {
                            tpe.getPredio().getNumeroPredial();
                        }
                        Hibernate.initialize(t.getTramiteDocumentacions());
                    }
                } catch (Exception e) {
                    LOGGER.error(
                        "error buscando los TramiteDetallePredio de los tramites asociados a la solicitud " +
                        answer.getSolicitud().getId() +
                        e.getMessage());
                    throw SncBusinessServiceExceptions.EXCEPCION_HIBERNATE_INITIALIZE.getExcepcion(
                        LOGGER, e, "TramiteDAOBean#findTramiteByIdForAsignacion", "Tramite");
                }
            }

            for (SolicitanteSolicitud ss : answer.getSolicitud().getSolicitanteSolicituds()) {
                Hibernate.initialize(ss.getDireccionPais());
                Hibernate.initialize(ss.getDireccionDepartamento());
                Hibernate.initialize(ss.getDireccionMunicipio());
            }

            for (TramitePredioEnglobe tpe : answer.getTramitePredioEnglobes()) {
                if (tpe.getPredio().getPredioDireccions() != null) {
                    tpe.getPredio().getPredioDireccions().size();
                }
                if (tpe.getPredio().getReferenciaCartograficas() != null) {
                    tpe.getPredio().getReferenciaCartograficas().size();
                }
            }

            if (answer.getPredio() != null) {
                if (answer.getPredio().getPredioDireccions() != null) {
                    answer.getPredio().getPredioDireccions().size();
                }
                if (answer.getPredio().getReferenciaCartograficas() != null) {
                    answer.getPredio().getReferenciaCartograficas().size();
                }
            }

            if (answer.getTramiteDepuracions() != null) {
                answer.getTramiteDepuracions().size();
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramiteByIdForAsignacion");
        }

        return answer;

    }

// ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarTramitesPorEstado(String)
     * @author david.cifuentes
     */
    @SuppressWarnings({"unchecked", "deprecation"})
    @Override
    public List<Tramite> buscarTramitesPorEstado(String estado) {

        LOGGER.debug("TramiteDAOBean#buscarTramitesPorEstado...");

        List<Tramite> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.solicitud s" +
            " LEFT JOIN FETCH t.predio p " +
            " LEFT JOIN FETCH t.solicitanteTramites s" +
            " WHERE t.estado = :estado";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("estado", estado);
            answer = query.getResultList();

            for (Tramite tramite : answer) {
                for (TramitePredioEnglobe tpe : tramite
                    .getTramitePredioEnglobes()) {
                    tpe.getPredio().getFormattedNumeroPredialCompleto();
                }
                tramite.getSolicitanteTramites().size();
                for (SolicitanteSolicitud ss : tramite.getSolicitud()
                    .getSolicitanteSolicituds()) {
                    ss.getNombreCompleto();
                }

            }
        } catch (Exception e) {
            LOGGER.error("error buscando los trámites en el estado " + estado +
                e.getMessage());
        }
        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#findTramitesParaCargueInfo(idsTramites, rowStartIdxAndCount)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> findTramitesParaCargueInfo(long[] idsTramites, String tipoComision,
        int... rowStartIdxAndCount) {

        LOGGER.debug("on ITramiteDAO#findTramitesParaCargueInfo ...");

        List<Tramite> answer = null;
        StringBuilder queryString;
        Query query;
        List<Long> idsTramitesParam;
        Long[] idsTramitesAsLong;
        ArrayList<String> estadosComision;

        queryString = new StringBuilder();

        //N: truquini para convertir en un ArrayList el long[] que llega, para poder usarlo directamente como parámetro
        idsTramitesAsLong = ArrayUtils.toObject(idsTramites);
        idsTramitesParam = Arrays.asList(idsTramitesAsLong);

        queryString
            .append("SELECT t FROM Tramite t JOIN FETCH t.solicitud s " +
                " LEFT JOIN FETCH t.predio p LEFT JOIN FETCH t.departamento d " +
                " LEFT JOIN FETCH t.municipio m  LEFT JOIN FETCH t.comisionTramites ct " +
                " LEFT JOIN FETCH ct.comision comision");

        queryString.append(" WHERE t.id IN (:idsTramites_p)");

        queryString.append(" AND comision.estado IN (:estadosComision_p)");

        //D: Se consultan solo las comisiones relacionadas a la actividad correspondiente
        queryString.append(" AND comision.tipo = '").append(tipoComision).
            append("'");

        estadosComision = new ArrayList<String>();

        estadosComision.add(EComisionEstado.EN_EJECUCION.getCodigo());
        //estadosComision.add(EComisionEstado.POR_EJECUTAR.getCodigo());
        estadosComision.add(EComisionEstado.CANCELADA.getCodigo());

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idsTramites_p", idsTramitesParam);
            query.setParameter("estadosComision_p", estadosComision);

            answer = (List<Tramite>) super.findInRangeUsingQuery(query, rowStartIdxAndCount);
            LOGGER.debug("ITramiteDAO#findTramitesParaCargueInfo finished");
        } catch (Exception e) {
            LOGGER.error("Error on TramiteDAOBean#findTramitesParaCargueInfo: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramitesParaCargueInfo");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#buscarPrediosPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 22-08-2012 Manejo correcto de excepciones
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosPorTramiteId(Long tramiteId) {
        LOGGER.debug("TramiteDAOBean#buscarPrediosPorTramiteId ...");

        List<Predio> answer = new ArrayList<Predio>();
        Tramite tramiteResultado = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
            " LEFT JOIN FETCH tpe.predio tpep" +
            " LEFT JOIN FETCH tpep.departamento" +
            " LEFT JOIN FETCH tpep.municipio" +
            " WHERE t.id = :tramiteId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);
            tramiteResultado = (Tramite) query.getSingleResult();

            if (tramiteResultado.getTramitePredioEnglobes() != null &&
                !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {

                for (TramitePredioEnglobe tpe : tramiteResultado.getTramitePredioEnglobes()) {
                    answer.add(tpe.getPredio());
                }
            } else if (tramiteResultado.getPredio() != null) {
                answer.add(tramiteResultado.getPredio());
            }

            for (Predio predio : answer) {
                Hibernate.initialize(predio.getUnidadConstruccions());
                Hibernate.initialize(predio.getFichaMatrizs());
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#buscarPrediosPorTramiteId");
        }
        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarPrediosPorListaDeIdsTramite(List<Long>)
     * @author david.cifuentes
     */
    @SuppressWarnings({"unchecked", "deprecation"})
    @Implement
    @Override
    public List<Predio> buscarPrediosPorListaDeIdsTramite(List<Long> idsTramites) {
        LOGGER.debug("TramiteDAOBean#buscarPrediosPorListaDeIdsTramite");

//REVIEW :: david.cifuentes :: 12-09-2013 :: ¿debería estar inicializado en null? :: pedro.garcia     
        List<Predio> answer = new ArrayList<Predio>();

        ArrayList<Tramite> listaTramites;

        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
            " LEFT JOIN FETCH tpe.predio tpep" +
            " LEFT JOIN FETCH tpep.departamento" +
            " LEFT JOIN FETCH tpep.municipio" +
            " WHERE t.id IN ( :idsTramites )";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idsTramites", idsTramites);
            listaTramites = (ArrayList<Tramite>) query.getResultList();

            for (Tramite tramiteResultado : listaTramites) {
                if (tramiteResultado.getTramitePredioEnglobes() != null &&
                    !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {

                    for (TramitePredioEnglobe tpe : tramiteResultado
                        .getTramitePredioEnglobes()) {
                        answer.add(tpe.getPredio());
                    }
                } else if (tramiteResultado.getPredio() != null) {
                    answer.add(tramiteResultado.getPredio());
                }
            }
            //Revisar con Felipe.Cadena
            if (answer != null && !answer.isEmpty()) {
                for (Predio predio : answer) {
                    if (predio != null && predio.getFichaMatrizs() != null &&
                        !predio.getFichaMatrizs().isEmpty()) {
                        Hibernate.initialize(predio.getFichaMatrizs());
                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e,
                    "TramiteDAOBean#buscarPrediosPorListaDeIdsTramite");
        }
        return answer;
    }
//---------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#buscarPrediosPanelAvaluoPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 15-08-2012 Orden. Manejo correcto de excepciones. Se traen las
     * unidades de construcción completas
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosPanelAvaluoPorTramiteId(Long tramiteId) {

        LOGGER.debug("TramiteDAOBean#buscarPrediosPorTramiteId ...");

        List<Predio> answer = new ArrayList<Predio>();
        Tramite tramiteResultado = null;
        String queryString;
        Query query;
        ArrayList<UnidadConstruccion> unidadesConstruccion;

        queryString = "SELECT t FROM Tramite t " +
            " JOIN FETCH t.tramitePredioEnglobes tpe" +
            " JOIN FETCH tpe.predio p" +
            " JOIN p.predioAvaluoCatastrals pac" +
            " WHERE t.id = :tramiteId" + " AND pac.id = (" +
            " SELECT MAX(id) FROM pac" + "	WHERE pac.predio = p)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);
            tramiteResultado = (Tramite) query.getSingleResult();
            if (tramiteResultado.getTramitePredioEnglobes() != null &&
                !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {

                for (TramitePredioEnglobe tpe : tramiteResultado.getTramitePredioEnglobes()) {
                    if (tpe.getPredio().getPredioAvaluoCatastrals() != null &&
                        !tpe.getPredio().getPredioAvaluoCatastrals().isEmpty()) {
                        tpe.getPredio().getPredioAvaluoCatastrals().size();
                    }
                    if (tpe.getPredio().getUnidadConstruccions() != null &&
                        !tpe.getPredio().getUnidadConstruccions().isEmpty()) {
                        tpe.getPredio().getUnidadConstruccions().size();

                        unidadesConstruccion = (ArrayList<UnidadConstruccion>) this.predioDAO.
                            getUnidadesConstruccion(tpe.getPredio().getId());
                        if (null != unidadesConstruccion) {
                            tpe.getPredio().setUnidadConstruccions(unidadesConstruccion);
                        }

                    }
                    if (tpe.getPredio().getPredioZonas() != null &&
                        !tpe.getPredio().getPredioZonas().isEmpty()) {
                        tpe.getPredio().getPredioZonas().size();
                    }

                    answer.add(tpe.getPredio());
                }
            }

        } catch (NoResultException ne) {

            queryString = "SELECT t FROM Tramite t " + " JOIN FETCH t.predio p" +
                " JOIN FETCH p.predioAvaluoCatastrals pac" +
                " WHERE t.id = :tramiteId" + " AND pac.id = (" +
                " SELECT MAX(id) FROM pac" + " WHERE pac.predio = p)";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("tramiteId", tramiteId);
                tramiteResultado = (Tramite) query.getSingleResult();

                if (tramiteResultado.getPredio() != null) {
                    if (tramiteResultado.getPredio().getUnidadConstruccions() != null &&
                        !tramiteResultado.getPredio().getUnidadConstruccions().isEmpty()) {
                        tramiteResultado.getPredio().getUnidadConstruccions().size();

                        unidadesConstruccion = (ArrayList<UnidadConstruccion>) this.predioDAO.
                            getUnidadesConstruccion(tramiteResultado.getPredio().getId());
                        if (null != unidadesConstruccion) {
                            tramiteResultado.getPredio().
                                setUnidadConstruccions(unidadesConstruccion);
                        }

                    }
                    if (tramiteResultado.getPredio().getPredioZonas() != null &&
                        !tramiteResultado.getPredio().getPredioZonas().isEmpty()) {
                        tramiteResultado.getPredio().getPredioZonas().size();
                    }

                    answer.add(tramiteResultado.getPredio());
                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                    "TramiteDAOBean#buscarPrediosPorTramiteId");
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#buscarPrediosPorTramiteId");
        }
        return answer;
    }
// ---------------------------------------------------------------------

    /**
     * Método que obtiene una lista de {@link Tramite} filtrada por el numero de solicitud.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    public List<Tramite> buscarTramitesPorNumeroDeSolicitud(String numSolicitud) {

        Query query = this.entityManager.createQuery("SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH t.solicitud s" +
            " LEFT JOIN FETCH t.solicitanteTramites s" +
            " WHERE s.numero = :numSolicitud");

        query.setParameter("numSolicitud", numSolicitud);
        List<Tramite> tramites = query.getResultList();

        return tramites;
    }

// ---------------------------------------------------------------------
    /**
     * @see ITramiteDAO#recuperarTramitesConDocumentosFaltantes(List<Actividad>)
     * @author david.cifuentes
     */
    @Override
    public List<Tramite> recuperarTramitesConDocumentosFaltantes(
        List<Actividad> actividades) {
        List<Tramite> tramitesConDocumentosFaltantes = new ArrayList<Tramite>();
        List<Tramite> tramitesActividad;

        for (Actividad actividad : actividades) {

            // Id de solicitud
            String numSolicitud = actividad.getNumeroSolicitud();
            tramitesActividad = buscarTramitesPorNumeroDeSolicitud(numSolicitud);
            tramitesConDocumentosFaltantes.addAll(tramitesActividad);

        }
        return tramitesConDocumentosFaltantes;
    }

// ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarPrediosPanelJPropiedadPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 22-08-2012 Manejo correcto de excepciones
     */
    @Override
    public List<Predio> buscarPrediosPanelJPropiedadPorTramiteId(Long tramiteId) {
        LOGGER.debug("TramiteDAOBean#buscarPrediosPanelJPropiedadPorTramiteId ...");

        List<Predio> answer = new ArrayList<Predio>();
        Tramite tramiteResultado;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " JOIN FETCH t.tramitePredioEnglobes tpe" +
            " JOIN FETCH tpe.predio p" + " WHERE t.id = :tramiteId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);
            tramiteResultado = (Tramite) query.getSingleResult();
            if (tramiteResultado.getTramitePredioEnglobes() != null &&
                !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {
                for (TramitePredioEnglobe tpe : tramiteResultado
                    .getTramitePredioEnglobes()) {
                    if (tpe.getPredio().getPersonaPredios() != null &&
                        !tpe.getPredio().getPersonaPredios().isEmpty()) {
                        tpe.getPredio().getPersonaPredios().size();
                        for (PersonaPredio pp : tpe.getPredio()
                            .getPersonaPredios()) {
                            pp.getPersonaPredioPropiedads().size();
                            if (pp.getPersonaPredioPropiedads() != null &&
                                !pp.getPersonaPredioPropiedads()
                                    .isEmpty()) {
                                for (PersonaPredioPropiedad ppp : pp
                                    .getPersonaPredioPropiedads()) {
                                    if (ppp.getDepartamento() != null) {
                                        ppp.getDepartamento().getNombre();
                                    }
                                    if (ppp.getMunicipio() != null) {
                                        ppp.getMunicipio().getNombre();
                                    }
                                }
                            }
                        }
                    }
                    answer.add(tpe.getPredio());
                }
            }

        } catch (NoResultException ne) {

            queryString = "SELECT t FROM Tramite t " + " JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH p.personaPredios pp" +
                " WHERE t.id = :tramiteId";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("tramiteId", tramiteId);
                tramiteResultado = (Tramite) query.getSingleResult();

                if (tramiteResultado.getPredio() != null) {
                    if (tramiteResultado.getPredio().getPersonaPredios() != null &&
                        !tramiteResultado.getPredio()
                            .getPersonaPredios().isEmpty()) {
                        for (PersonaPredio pp : tramiteResultado.getPredio()
                            .getPersonaPredios()) {
                            pp.getPersonaPredioPropiedads().size();

                            if (pp.getPersonaPredioPropiedads() != null &&
                                !pp.getPersonaPredioPropiedads()
                                    .isEmpty()) {
                                for (PersonaPredioPropiedad ppp : pp
                                    .getPersonaPredioPropiedads()) {
                                    if (ppp.getDepartamento() != null) {
                                        ppp.getDepartamento().getNombre();
                                    }
                                    if (ppp.getMunicipio() != null) {
                                        ppp.getMunicipio().getNombre();
                                    }
                                }
                            }
                        }
                    }
                    answer.add(tramiteResultado.getPredio());
                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                    "TramiteDAOBean#buscarPrediosPanelJPropiedadPorTramiteId");
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#buscarPrediosPanelJPropiedadPorTramiteId");
        }
        return answer;
    }

// ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarPrediosPanelPropietariosPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 22-08-2012 Manejo correcto de excepciones
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosPanelPropietariosPorTramiteId(Long tramiteId) {
        LOGGER.debug("TramiteDAOBean#buscarPrediosPanelPropietariosPorTramiteId ...");

        List<Predio> answer = new ArrayList<Predio>();
        Tramite tramiteResultado;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " JOIN FETCH t.tramitePredioEnglobes tpe" +
            " JOIN FETCH tpe.predio p" + " WHERE t.id = :tramiteId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);
            tramiteResultado = (Tramite) query.getSingleResult();
            if (tramiteResultado.getTramitePredioEnglobes() != null &&
                !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {
                for (TramitePredioEnglobe tpe : tramiteResultado
                    .getTramitePredioEnglobes()) {
                    if (tpe.getPredio().getPersonaPredios() != null &&
                        !tpe.getPredio().getPersonaPredios().isEmpty()) {
                        tpe.getPredio().getPersonaPredios().size();
                        for (PersonaPredio pp : tpe.getPredio()
                            .getPersonaPredios()) {
                            pp.getPersona().getNombre();
                        }
                    }
                    answer.add(tpe.getPredio());
                }
            }

        } catch (NoResultException ne) {

            queryString = "SELECT t FROM Tramite t " + " JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH p.personaPredios pp" +
                " WHERE t.id = :tramiteId";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("tramiteId", tramiteId);
                tramiteResultado = (Tramite) query.getSingleResult();

                if (tramiteResultado.getPredio() != null) {
                    if (tramiteResultado.getPredio().getPersonaPredios() != null &&
                        !tramiteResultado.getPredio()
                            .getPersonaPredios().isEmpty()) {
                        for (PersonaPredio pp : tramiteResultado.getPredio()
                            .getPersonaPredios()) {
                            pp.getPersona().getNombre();
                        }
                    }
                    answer.add(tramiteResultado.getPredio());
                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                    "TramiteDAOBean#buscarPrediosPanelPropietariosPorTramiteId");
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#buscarPrediosPanelPropietariosPorTramiteId");
        }
        return answer;
    }

// ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarPrediosPanelFichaMatrizPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public List<Predio> buscarPrediosPanelFichaMatrizPorTramiteId(Long tramiteId) {
        LOGGER.debug("TramiteDAOBean#buscarPrediosPanelPanelFichaMatrizPorTramiteId ...");

        List<Predio> answer = new ArrayList<Predio>();
        Tramite tramiteResultado = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t" +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
            " LEFT JOIN FETCH tpe.predio ptpe" +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE t.id = :tramiteId";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("tramiteId", tramiteId);

        try {
            tramiteResultado = (Tramite) query.getSingleResult();

            if (tramiteResultado.getPredio() != null) {

                if (tramiteResultado.getPredio().getFichaMatrizs() != null &&
                    !tramiteResultado.getPredio().getFichaMatrizs()
                        .isEmpty()) {

                    Hibernate.initialize(tramiteResultado.getPredio().getFichaMatrizs());

                    for (FichaMatriz fm : tramiteResultado.getPredio()
                        .getFichaMatrizs()) {

                        if (fm.getFichaMatrizPredios() != null &&
                            !fm.getFichaMatrizPredios().isEmpty()) {

                            fm.getFichaMatrizPredios().size();

                            for (FichaMatrizPredio fmp : fm
                                .getFichaMatrizPredios()) {

                                fmp.setValorAvaluo(this.predioAvaluoCatastralDAO
                                    .findValorAvaluoByNumeroPredial(fmp
                                        .getNumeroPredial()));
                            }
                        }

                        if (fm.getFichaMatrizTorres() != null &&
                            !fm.getFichaMatrizTorres().isEmpty()) {

                            fm.getFichaMatrizTorres().size();
                        }
                    }
                }

                if (tramiteResultado.getPredio().getPredioZonas() != null &&
                    !tramiteResultado.getPredio().getPredioZonas()
                        .isEmpty()) {

                    tramiteResultado.getPredio().getPredioZonas().size();
                }

                if (tramiteResultado.getPredio().getPredioDireccions() != null &&
                    !tramiteResultado.getPredio().getPredioDireccions()
                        .isEmpty()) {

                    tramiteResultado.getPredio().getPredioDireccions().size();
                }

                if (tramiteResultado.getPredio().getUnidadConstruccions() != null &&
                    !tramiteResultado.getPredio()
                        .getUnidadConstruccions().isEmpty()) {

                    tramiteResultado.getPredio().getUnidadConstruccions()
                        .size();

                    for (UnidadConstruccion uc : tramiteResultado.getPredio()
                        .getUnidadConstruccions()) {

                        if (uc.getUsoConstruccion() != null) {
                            uc.getUsoConstruccion().getNombre();
                        }

                        if (uc.getUnidadConstruccionComps() != null &&
                            !uc.getUnidadConstruccionComps().isEmpty()) {

                            uc.getUnidadConstruccionComps().size();

                            for (UnidadConstruccionComp ucc : uc
                                .getUnidadConstruccionComps()) {

                                ucc.getComponente();

                                if (ucc.getUnidadConstruccion() != null) {
                                    ucc.getUnidadConstruccion().getId();
                                }
                            }
                        }
                    }

                }

                answer.add(tramiteResultado.getPredio());
            }

            if (tramiteResultado.getTramitePredioEnglobes() != null &&
                !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {

                for (TramitePredioEnglobe tpe : tramiteResultado
                    .getTramitePredioEnglobes()) {

                    tpe.getPredio().getCirculoRegistral();

                    if (tpe.getPredio().getFichaMatrizs() != null &&
                        !tramiteResultado.getPredio().getFichaMatrizs()
                            .isEmpty()) {

                        Hibernate.initialize(tpe.getPredio().getFichaMatrizs());

                        for (FichaMatriz fm : tramiteResultado.getPredio()
                            .getFichaMatrizs()) {

                            if (fm.getFichaMatrizPredios() != null &&
                                !fm.getFichaMatrizPredios().isEmpty()) {
                                fm.getFichaMatrizPredios().size();

                                for (FichaMatrizPredio fmp : fm
                                    .getFichaMatrizPredios()) {

                                    fmp.setValorAvaluo(this.predioAvaluoCatastralDAO
                                        .findValorAvaluoByNumeroPredial(fmp
                                            .getNumeroPredial()));
                                }
                            }

                            if (fm.getFichaMatrizTorres() != null &&
                                !fm.getFichaMatrizTorres().isEmpty()) {

                                fm.getFichaMatrizTorres().size();
                            }
                        }
                    }

                    if (tpe.getPredio().getPredioZonas() != null &&
                        !tramiteResultado.getPredio().getPredioZonas()
                            .isEmpty()) {
                        tramiteResultado.getPredio().getPredioZonas().size();
                    }

                    if (tpe.getPredio().getPredioDireccions() != null &&
                        !tramiteResultado.getPredio()
                            .getPredioDireccions().isEmpty()) {
                        tramiteResultado.getPredio().getPredioDireccions()
                            .size();
                    }

                    if (tpe.getPredio().getUnidadConstruccions() != null &&
                        !tramiteResultado.getPredio()
                            .getUnidadConstruccions().isEmpty()) {

                        tpe.getPredio().getUnidadConstruccions().size();

                        for (UnidadConstruccion uc : tramiteResultado
                            .getPredio().getUnidadConstruccions()) {

                            if (uc.getUsoConstruccion() != null) {
                                uc.getUsoConstruccion().getNombre();
                            }

                            if (uc.getUnidadConstruccionComps() != null &&
                                !uc.getUnidadConstruccionComps()
                                    .isEmpty()) {

                                uc.getUnidadConstruccionComps().size();

                                for (UnidadConstruccionComp ucc : uc
                                    .getUnidadConstruccionComps()) {

                                    ucc.getComponente();

                                    if (ucc.getUnidadConstruccion() != null) {
                                        ucc.getUnidadConstruccion().getId();
                                    }
                                }
                            }
                        }
                    }

                    answer.add(tpe.getPredio());
                }
            }

        } catch (NoResultException ne) {

            return new ArrayList<Predio>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

// ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarPrediosPanelUbicacionPredioPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 16-08-2012 Se hace fetch del CirculoRegistral del predio
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId) {
        LOGGER.debug("TramiteDAOBean#buscarPrediosPorTramiteId ...");

        List<Predio> answer = new ArrayList<Predio>();
        Tramite tramiteResultado = null;
        String queryString;
        Query query;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.predio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
            " LEFT JOIN FETCH tpe.predio tpep" +
            " LEFT JOIN FETCH tpep.departamento" +
            " LEFT JOIN FETCH tpep.municipio" +
            " WHERE t.id = :tramiteId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);
            tramiteResultado = (Tramite) query.getSingleResult();

            if (tramiteResultado.getTramitePredioEnglobes() != null &&
                !tramiteResultado.getTramitePredioEnglobes().isEmpty()) {

                for (TramitePredioEnglobe tpe : tramiteResultado
                    .getTramitePredioEnglobes()) {
                    if (tpe.getPredio().getPredioDireccions() != null) {
                        tpe.getPredio().getPredioDireccions().size();
                    }
                    if (tpe.getPredio().getReferenciaCartograficas() != null &&
                        !tpe.getPredio().getReferenciaCartograficas()
                            .isEmpty()) {
                        tpe.getPredio().getReferenciaCartograficas().size();
                    }
                    if (tpe.getPredio().getPredioServidumbres() != null &&
                        !tpe.getPredio().getPredioServidumbres()
                            .isEmpty()) {
                        tpe.getPredio().getPredioServidumbres().size();
                    }
                    if (tpe.getPredio().getPredioZonas() != null &&
                        !tpe.getPredio().getPredioZonas().isEmpty()) {
                        tpe.getPredio().getPredioZonas().size();
                    }
                    answer.add(tpe.getPredio());

                }
            } else if (tramiteResultado.getPredio() != null) {
                if (tramiteResultado.getPredio().getPredioDireccions() != null) {
                    tramiteResultado.getPredio().getPredioDireccions().size();
                }
                if (tramiteResultado.getPredio().getReferenciaCartograficas() != null &&
                    !tramiteResultado.getPredio()
                        .getReferenciaCartograficas().isEmpty()) {
                    tramiteResultado.getPredio().getReferenciaCartograficas()
                        .size();
                }
                if (tramiteResultado.getPredio().getPredioServidumbres() != null &&
                    !tramiteResultado.getPredio()
                        .getPredioServidumbres().isEmpty()) {
                    tramiteResultado.getPredio().getPredioServidumbres().size();
                }
                if (tramiteResultado.getPredio().getPredioZonas() != null &&
                    !tramiteResultado.getPredio().getPredioZonas()
                        .isEmpty()) {
                    tramiteResultado.getPredio().getPredioZonas().size();
                }
                answer.add(tramiteResultado.getPredio());
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ExcepcionSNC("mi codigo de excepcion",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), e);
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @modified juanfelipe.garcia se adiciono Hibernate.initialize para el tramite
     * @see ITramite#buscarTramiteTraerDocumentoResolucionPorIdTramite(Long)
     * @modified leidy.gonzalez se adiciono Hibernate.initialize para los campos getComision y
     * getComision().getEstado() Se organiza la validación para la inicialización de los campos de
     * Hibrnate
     * @modified leidy.gonzalez 22/07/2014 Se agregan validaciones para que al inicializar por
     * Hibernate no generen no vayan campos nulos y se agrega validación para campo Predios de
     * hibernate
     */
    /*
     * @modified by leidy.gonzalez::#16093:: Se inicializaron los campos Tramites de la solicitud y
     * Nombre de la Direccion del Pais del Solicitante de la Solicitud
     */
 /*
     * @modified by leidy.gonzalez:: #16597:: 05/04/2016 SE agrega initialize para los tramites de
     * la solicitud y los predios asociados a los tramites
     */
    @Override
    public Tramite buscarTramiteTraerDocumentoResolucionPorIdTramite(
        Long currentTramiteId) {
        String query;
        Query q;
        Tramite answer = null;

        query = "SELECT t " + "FROM Tramite t " +
            "LEFT JOIN FETCH t.departamento de " +
            "LEFT JOIN FETCH t.municipio m " +
            "LEFT JOIN FETCH t.predio p " +
            "LEFT JOIN FETCH t.resultadoDocumento d " +
            "LEFT JOIN FETCH t.solicitud s " + "WHERE t.id = :tramiteId";
        q = entityManager.createQuery(query);
        q.setParameter("tramiteId", currentTramiteId);
        try {
            answer = (Tramite) q.getSingleResult();
            if (answer.getTramite() != null) {
                Hibernate.initialize(answer.getTramite());
                if (answer.getTramite().getTramites() != null) {
                    Hibernate.initialize(answer.getTramite().getTramites());
                }
                if (answer.getTramite().getTramiteDocumentacions() != null) {
                    Hibernate.initialize(answer.getTramite().getTramiteDocumentacions());
                }
            }
            if (answer.getTramites() != null) {
                Hibernate.initialize(answer.getTramites());
            }
            if (answer.getTramiteRectificacions() != null) {
                Hibernate.initialize(answer.getTramiteRectificacions());
            }
            if (answer.getSolicitanteTramites() != null) {
                Hibernate.initialize(answer.getSolicitanteTramites());
            }
            if (answer.getSolicitud() != null) {
                Hibernate.initialize(answer.getSolicitud());
                if (answer.getSolicitud().getSolicitanteSolicituds() != null) {
                    Hibernate.initialize(answer.getSolicitud()
                        .getSolicitanteSolicituds());

                    if (answer.getSolicitud().getSolicitanteSolicituds() != null) {

                        for (SolicitanteSolicitud solicitanteSolicitudTemp : answer.getSolicitud().
                            getSolicitanteSolicituds()) {

                            if (solicitanteSolicitudTemp.getDireccionPais() != null &&
                                solicitanteSolicitudTemp.getDireccionPais().getNombre() != null) {

                                Hibernate.initialize(solicitanteSolicitudTemp.getDireccionPais().
                                    getNombre());
                            }
                        }

                    }
                }

                if (answer.getSolicitud().getTramites() != null) {
                    Hibernate.initialize(answer.getSolicitud().getTramites());

                    for (Tramite tramiteTemp : answer.getSolicitud().getTramites()) {

                        if (tramiteTemp.getTramiteDocumentacions() != null) {
                            Hibernate.initialize(tramiteTemp.getTramiteDocumentacions());
                        }
                        if (tramiteTemp.getPredios() != null) {
                            Hibernate.initialize(tramiteTemp.getPredios());
                        }
                        if (tramiteTemp.getPredio() != null) {
                            Hibernate.initialize(tramiteTemp.getPredio());
                        }
                        if (tramiteTemp.getEstadoDocumentos() != null) {
                            Hibernate.initialize(tramiteTemp.getEstadoDocumentos());
                        }
                    }

                }
            }
            if (answer.getTramitePredioEnglobes() != null) {
                Hibernate.initialize(answer.getTramitePredioEnglobes());
            }
            if (answer.getPredios() != null) {
                Hibernate.initialize(answer.getPredios());
            }
            if (answer.getTramiteDocumentos() != null) {
                Hibernate.initialize(answer.getTramiteDocumentos());
            }
            if (answer.getComisionTramites() != null) {
                Hibernate.initialize(answer.getComisionTramites());
            }
            if (answer.getTramiteInconsistencias() != null) {
                Hibernate.initialize(answer.getTramiteInconsistencias());
            }

            if (answer.getPredio() != null) {
                Hibernate.initialize(answer.getPredio());
                if (answer.getPredio().getPredioDireccions() != null) {
                    Hibernate.initialize(answer.getPredio()
                        .getPredioDireccions());
                }
                if (answer.getPredio().getDireccionPrincipal() != null) {
                    Hibernate.initialize(answer.getPredio()
                        .getDireccionPrincipal());
                }
                if (answer.getPredio().getReferenciaCartograficas() != null) {
                    Hibernate.initialize(answer.getPredio()
                        .getReferenciaCartograficas());
                }
            }

            if (answer.getComisionTramite() != null) {
                if (answer.getComisionTramite().getComisionTramiteDatos() != null) {
                    Hibernate.initialize(answer.getComisionTramite()
                        .getComisionTramiteDatos());
                }
                if (answer.getComisionTramite().getComision() != null) {
                    Hibernate.initialize(answer.getComisionTramite()
                        .getComision());
                }
            }
            if (answer.getComisionTramite() != null &&
                answer.getComisionTramite().getComision() != null) {
                Hibernate.initialize(answer.getComisionTramite().getComision()
                    .getEstado());
            }
            if (answer.getTramitePredioEnglobes() != null) {
                for (TramitePredioEnglobe tpe : answer
                    .getTramitePredioEnglobes()) {
                    if (tpe.getPredio() != null) {
                        Hibernate.initialize(tpe.getPredio());
                    }
                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getPredioDireccions() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getPredioDireccions());
                    }
                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getDireccionPrincipal() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getDireccionPrincipal());
                    }

                    if (tpe.getPredio() != null &&
                        tpe.getPredio().getReferenciaCartograficas() != null) {
                        Hibernate.initialize(tpe.getPredio()
                            .getReferenciaCartograficas());
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author lorena.salamanca (non-Javadoc)
     * @see co.gov.igac.snc.dao.tramite.ITramiteDAO#getNombreReplicaGDBdeTramite(java.lang.Long)
     */
    @Override
    public String getIdRepositorioDocumentosDeReplicaGDBdeTramite(Long tramiteId,
        Long idTipoDocumento) {

        LOGGER.debug("TramiteDAOBean#getIdRepositorioDocumentosDeReplicaGDBdeTramite");
        String rutaGDB = null;
        try {
            rutaGDB = this.documentoDAOService.obtenerIdRepositorioDocumentosDeReplicaOriginal(
                tramiteId, idTipoDocumento);

        } catch (ExcepcionSNC ex) {
            return null;
        }

        return rutaGDB;
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 06-09-2013 Para arreglar la lista de estados que debe tener en cuenta
     * para excluir trámites
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findTramitesCancelacion(
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize) {

        List<Tramite> answer = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT DISTINCT t FROM Tramite t");
            sql.append(" LEFT JOIN FETCH t.departamento ");
            sql.append(" LEFT JOIN FETCH t.municipio ");
            sql.append(" LEFT JOIN t.tramitePredioEnglobes tpe");

            sql = setupWhereClauseFindTramitesCancelacion(filtro, sql);

            LOGGER.debug("JPQL: " + sql.toString());

            Query query = this.entityManager.createQuery(sql.toString());
            query.setFirstResult(first);
            query.setMaxResults(pageSize);

            query = this.setupParametersFindTramitesCancelacion(filtro, query);

            answer = (List<Tramite>) query.getResultList();

            return answer;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());

        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#buscarTramiteConResolucionSimple(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Tramite buscarTramiteConResolucionSimple(Long tramiteId) {

        Tramite answer = null;
        String queryString;
        Query query;

        queryString =
            "SELECT t FROM Tramite t " +
            "LEFT JOIN FETCH t.resultadoDocumento rd " +
            "LEFT JOIN FETCH rd.tipoDocumento rdTipo " +
            "LEFT JOIN FETCH t.predio p " +
            "LEFT JOIN FETCH p.departamento " +
            "LEFT JOIN FETCH p.municipio " +
            "WHERE t.id = :tidParam";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("tidParam", tramiteId);

        try {
            answer = (Tramite) query.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, e, "Tramite", tramiteId);
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia basado en el método buscarTramiteSolicitudPorId
     * @see ITramite#buscarTramiteConResolucionFull(Long)
     */
    @Implement
    @Override
    public Tramite buscarTramiteConResolucionFull(Long tramiteId) {
        LOGGER.debug("on TramiteDAOBean#buscarTramiteConResolucionFull ...");

        Tramite answer = null;
        List<Tramite> tempAnswer;
        long[] idsTramites;

        idsTramites = new long[]{tramiteId};
        tempAnswer = this.findTramitesConResolucionFullP(idsTramites);

        if (tempAnswer != null) {
            answer = tempAnswer.get(0);
        }

        LOGGER.debug("... finished TramiteDAOBean#buscarTramiteConResolucionFull.");

        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia basado en el método buscarTramiteSolicitudPorId
     * @see ITramite#buscarTramiteConResolucionFull(Long)
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesConResolucionFull(long[] tramitesIds) {

        LOGGER.debug("on TramiteDAOBean#buscarTramitesConResolucionFull ...");

        List<Tramite> answer = null;
        answer = this.findTramitesConResolucionFullP(tramitesIds);

        LOGGER.debug("... finished TramiteDAOBean#buscarTramitesConResolucionFull.");

        return answer;
    }

// --------------------------------------------------------------------------------------------------
    /**
     * método en donde se factoriza lo que hacen los métodos buscarTramitesConResolucionFull(long[]
     * tramitesIds) y buscarTramiteConResolucionSimple(Long tramiteId)
     *
     * @param tramitesIds
     */
    private List<Tramite> findTramitesConResolucionFullP(long[] idsTramites) {
        List<Tramite> answer = null;
        Tramite resultTemp;

        StringBuilder queryString, idTramiteIn;
        Query query;

        queryString = new StringBuilder();
        idTramiteIn = new StringBuilder();

        queryString.append("SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.predio pre" +
            " LEFT JOIN FETCH t.resultadoDocumento rd " +
            " LEFT JOIN FETCH rd.tipoDocumento rdTipo " +
            " LEFT JOIN FETCH t.tramite tt" +
            " JOIN FETCH t.departamento" + " JOIN FETCH t.municipio " +
            " JOIN FETCH t.solicitud s" +
            " LEFT JOIN FETCH t.solicitanteTramites ss" +
            " LEFT JOIN FETCH ss.solicitante sss ");

        idTramiteIn.append(" WHERE t.id IN (");
        for (long idTramite : idsTramites) {
            idTramiteIn.append(idTramite).append(",");
        }

        idTramiteIn.deleteCharAt(idTramiteIn.lastIndexOf(","));
        idTramiteIn.append(")");

        queryString.append(idTramiteIn);

        query = this.entityManager.createQuery(queryString.toString());

        try {
            if (idsTramites.length == 1) {
                answer = new ArrayList<Tramite>();
                resultTemp = (Tramite) query.getSingleResult();
                answer.add(resultTemp);
            } else {
                answer = (ArrayList<Tramite>) query.getResultList();
            }

            for (Tramite tramite : answer) {

                if (tramite.getTramite() != null) {
                    fetchSubTramites(tramite);
                }
                tramite.getTramiteEstados().size();
                tramite.getTramiteDocumentacions().size();
                tramite.getTramitePredioEnglobes().size();

                if (tramite.getTramiteDocumentacions() != null &&
                    !tramite.getTramiteDocumentacions().isEmpty()) {
                    for (int i = 0; i < tramite.getTramiteDocumentacions()
                        .size(); i++) {

                        if (tramite.getTramiteDocumentacions().get(i)
                            .getDocumentoSoporte() != null) {
                            tramite.getTramiteDocumentacions().get(i)
                                .getDocumentoSoporte().getArchivo();

                            tramite.getTramiteDocumentacions().get(i)
                                .getDocumentoSoporte().getTipoDocumento()
                                .getClase();
                        }

                        if (tramite.getTramiteDocumentacions().get(i)
                            .getDepartamento() != null) {
                            tramite.getTramiteDocumentacions().get(i)
                                .getDepartamento().getNombre();
                        }
                        if (tramite.getTramiteDocumentacions().get(i)
                            .getMunicipio() != null) {
                            tramite.getTramiteDocumentacions().get(i)
                                .getMunicipio().getNombre();
                        }
                        //felipe.cadena:: #12226 :: Se carga la lista de comisiones asociadas al tramite
                        if (tramite.getComisionTramites() != null && !tramite.getComisionTramites().
                            isEmpty()) {
                            tramite.getComisionTramites().size();
                            for (ComisionTramite ct : tramite.getComisionTramites()) {
                                ct.getId();
                                ct.getComision().getTipo();
                            }
                        }

                    }
                }

                if (tramite.getTramitePredioEnglobes() != null &&
                    !tramite.getTramitePredioEnglobes().isEmpty()) {
                    for (int j = 0; j < tramite.getTramitePredioEnglobes()
                        .size(); j++) {
                        tramite.getTramitePredioEnglobes().get(j).getPredio()
                            .getNumeroPredial();
                    }
                }

                if (tramite.getFichaMatrizPredioTerrenos() != null &&
                    !tramite.getFichaMatrizPredioTerrenos().isEmpty()) {
                    for (PFichaMatrizPredioTerreno pfmt : tramite.getFichaMatrizPredioTerrenos()) {
                        Hibernate.initialize(pfmt.getFichaMatriz());
                    }
                }

                if (tramite.getTramites() != null && !tramite.getTramites().isEmpty()) {
                    tramite.getTramites().size();
                    for (Tramite t : tramite.getTramites()) {
                        LOGGER.debug("" + t.getId());
                    }
                }
                if (tramite.getTramiteRectificacions() != null && !tramite.
                    getTramiteRectificacions().isEmpty()) {
                    tramite.getTramiteRectificacions().size();
                    for (TramiteRectificacion tr : tramite.getTramiteRectificacions()) {
                        tr.getId();
                    }
                }

            }

        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, e, "Tramite", idTramiteIn.toString());
        }

        return answer;
    }

    public void asociarInfoCampoAdicional(EInfoAdicionalCampo campo, String valor) {

        Long id = new Long(660519l);

        Tramite t = this.findById(id);

        try {
            if (t != null) {
                t.asociarInfoCampoAdicional(campo, valor);
            }

        } catch (NoResultException nre) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nre,
                "TramiteDAOBean#asociarInfoCampoAdicional", "Tramite: ", t.getId().toString());
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#asociarInfoCampoAdicional");
        }

    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 06-09-2013 Para arreglar la lista de estados que debe tener en cuenta
     * para excluir trámites
     */
    @Override
    public int countTramitesCancelacion(
        FiltroDatosConsultaCancelarTramites filtro) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(DISTINCT t) FROM Tramite t");
            sql.append(" LEFT JOIN t.tramitePredioEnglobes tpe");

            sql = setupWhereClauseFindTramitesCancelacion(filtro, sql);

            Query query = this.entityManager.createQuery(sql.toString());

            query = this.setupParametersFindTramitesCancelacion(filtro, query);

            return ((Long) query.getSingleResult()).intValue();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ExcepcionSNC("AlgúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error en en conteo de trámites para cancelar.", e);

        }
    }

    /**
     * Méotodo encargado de ensamblar la cláusula WHERE del query que busca los trámites de
     * cancelación según el filtro
     *
     * @param filtro
     * @param sql
     * @return El StringBuilder de entrada con la cláusula WHERE concatenada
     */
    private StringBuilder setupWhereClauseFindTramitesCancelacion(
        FiltroDatosConsultaCancelarTramites filtro, StringBuilder sql) {

        if (filtro.getNumeroIdentificacion() != null &&
            !filtro.getNumeroIdentificacion().isEmpty()) {
            sql.append(" LEFT JOIN t.solicitud s ");
            sql.append(" LEFT JOIN s.solicitanteSolicituds ss ");
        }

        //Se filtra la estructura organizacional de los tramites
        // Asegura que esté dentro del tiempo en que se puede hacer la
        // cancelación
        sql.append(" WHERE t.resultadoDocumento is null");
        sql.append(" AND t.estado != :archivado");
        sql.append(" AND t.estado != :finalizado");
        sql.append(" AND t.estado != :cancelado");
        sql.append(" AND t.estado != :rechazado");
        sql.append(" AND t.estado != :finalizado_aprobado");
        sql.append(
            " AND t.id not in (select d.tramiteId from Documento d where d.tipoDocumento.id IN " +
            " (" +
            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                getId() + ", " +
            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.getId() +
            " ) and d.tramiteId is not null) ");
        if (filtro.isEsVisibleMnpios()) {
            sql.append(" AND t.municipio.codigo IN (:municipioCods)");
        }

        if (filtro.getNumeroSolicitud() != null &&
            !filtro.getNumeroSolicitud().isEmpty()) {
            sql.append(" AND ");
            sql.append("t.solicitud.numero = :numeroSolicitud");
        }
        if (filtro.getNumeroRadicacion() != null &&
            !filtro.getNumeroRadicacion().isEmpty()) {
            sql.append(" AND ");
            sql.append("t.numeroRadicacion like :numeroRadicacion");
        }
        if (filtro.getNumeroIdentificacion() != null &&
            !filtro.getNumeroIdentificacion().isEmpty()) {
            sql.append(" AND ");
            sql.append("ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtro.getTipoTramite() != null &&
            !filtro.getTipoTramite().isEmpty()) {
            sql.append(" AND ");
            sql.append("t.tipoTramite = :tipoTramite");
        }
        if (filtro.getClaseMutacion() != null &&
            !filtro.getClaseMutacion().isEmpty()) {
            sql.append(" AND ");
            sql.append("t.claseMutacion = :claseMutacion");
        }
        if (filtro.getSubtipo() != null && !filtro.getSubtipo().isEmpty()) {
            sql.append(" AND ");
            sql.append("t.subtipo = :subtipo");
        }
        if (filtro.getDepartamentoNumPredial() != null &&
            !filtro.getDepartamentoNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.departamento.codigo = :departamento");
            } else {
                sql.append("t.predio.departamento.codigo = :departamento");
            }

        }
        if (filtro.getMunicipioNumPredial() != null &&
            !filtro.getMunicipioNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.municipio.codigo LIKE :municipio)");
            } else {
                sql.append("t.predio.municipio.codigo LIKE :municipio");
            }
        }
        if (filtro.getZonaNumPredial() != null &&
            !filtro.getZonaNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.tipoAvaluo = :zona)");
            } else {
                sql.append("t.predio.tipoAvaluo = :zona");
            }
        }
        if (filtro.getSectorNumPredial() != null &&
            !filtro.getSectorNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.sectorCodigo = :sector)");
            } else {
                sql.append("t.predio.sectorCodigo = :sector");
            }
        }
        if (filtro.getComunaNumPredial() != null &&
            !filtro.getComunaNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.barrioCodigo LIKE :comuna)");
            } else {
                sql.append("t.predio.barrioCodigo LIKE :comuna");
            }
        }
        if (filtro.getBarrioNumPredial() != null &&
            !filtro.getBarrioNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.barrioCodigo LIKE :barrio)");
            } else {
                sql.append("t.predio.barrioCodigo LIKE :barrio");
            }
        }
        if (filtro.getManzanaNumPredial() != null &&
            !filtro.getManzanaNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.manzanaCodigo = :manzana)");
            } else {
                sql.append("t.predio.manzanaCodigo = :manzana");
            }
        }
        if (filtro.getCondicionPropiedadNumPredial() != null &&
            !filtro.getCondicionPropiedadNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.condicionPropiedad = :condicionPropiedad)");
            } else {
                sql.append("t.predio.condicionPropiedad = :condicionPropiedad");
            }
        }
        if (filtro.getTerrenoNumPredial() != null &&
            !filtro.getTerrenoNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.predio = :terreno)");
            } else {
                sql.append("t.predio.predio = :terreno");
            }
        }
        if (filtro.getEdificioNumPredial() != null &&
            !filtro.getEdificioNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.edificio = :edificio)");
            } else {
                sql.append("t.predio.edificio = :edificio");
            }
        }
        if (filtro.getPisoNumPredial() != null &&
            !filtro.getPisoNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.piso = :piso)");
            } else {
                sql.append("t.predio.piso = :piso");
            }
        }
        if (filtro.getUnidadNumPredial() != null &&
            !filtro.getUnidadNumPredial().isEmpty()) {
            sql.append(" AND ");
            if (filtro.getTipoTramite() != null &&
                filtro.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) &&
                filtro.getClaseMutacion() != null &&
                filtro.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                filtro.getSubtipo() != null &&
                filtro.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                sql.append("tpe.predio.unidad = :unidad)");
            } else {
                sql.append("t.predio.unidad = :unidad");
            }
        }
        LOGGER.debug("Query JPQL: " + sql.toString());
        return sql;
    }

    /**
     * Méotodo encargado de ubicar los parámetros requeridos para el query de búsqueda de trámites
     * para cancelar según el filtro de búsqueda.
     *
     * @param filtro
     * @param query
     * @return
     */
    private Query setupParametersFindTramitesCancelacion(
        FiltroDatosConsultaCancelarTramites filtro, Query query) {

        String archivado = ETramiteEstado.ARCHIVADO.getCodigo();
        String finalizado = ETramiteEstado.FINALIZADO.getCodigo();
        String cancelado = ETramiteEstado.CANCELADO.getCodigo();
        String rechazado = ETramiteEstado.RECHAZADO.getCodigo();
        String finalizado_aprobado = ETramiteEstado.FINALIZADO_APROBADO.getCodigo();

        query.setParameter("archivado", archivado);
        query.setParameter("finalizado", finalizado);
        query.setParameter("cancelado", cancelado);
        query.setParameter("rechazado", rechazado);
        query.setParameter("finalizado_aprobado", finalizado_aprobado);
        if (filtro.isEsVisibleMnpios()) {
            query.setParameter("municipioCods", filtro.getMunicipiosJurisdiccion());
        }

        if (filtro.getNumeroSolicitud() != null &&
            !filtro.getNumeroSolicitud().isEmpty()) {
            query.setParameter("numeroSolicitud", filtro.getNumeroSolicitud());
        }
        if (filtro.getNumeroRadicacion() != null &&
            !filtro.getNumeroRadicacion().isEmpty()) {
            query.setParameter("numeroRadicacion",
                "%" + filtro.getNumeroRadicacion() + "%");
        }
        if (filtro.getNumeroIdentificacion() != null &&
            !filtro.getNumeroIdentificacion().isEmpty()) {
            query.setParameter("numeroIdentificacion",
                filtro.getNumeroIdentificacion());
        }
        if (filtro.getTipoTramite() != null &&
            !filtro.getTipoTramite().isEmpty()) {
            query.setParameter("tipoTramite", filtro.getTipoTramite());
        }
        if (filtro.getClaseMutacion() != null &&
            !filtro.getClaseMutacion().isEmpty()) {
            query.setParameter("claseMutacion", filtro.getClaseMutacion());
        }
        if (filtro.getSubtipo() != null && !filtro.getSubtipo().isEmpty()) {
            query.setParameter("subtipo", filtro.getSubtipo());
        }
        if (filtro.getDepartamentoNumPredial() != null &&
            !filtro.getDepartamentoNumPredial().isEmpty()) {
            query.setParameter("departamento",
                filtro.getDepartamentoNumPredial());
        }
        if (filtro.getMunicipioNumPredial() != null &&
            !filtro.getMunicipioNumPredial().isEmpty()) {
            query.setParameter("municipio",
                "__" + filtro.getMunicipioNumPredial());
        }
        if (filtro.getZonaNumPredial() != null &&
            !filtro.getZonaNumPredial().isEmpty()) {
            query.setParameter("zona", filtro.getZonaNumPredial());
        }
        if (filtro.getSectorNumPredial() != null &&
            !filtro.getSectorNumPredial().isEmpty()) {
            query.setParameter("sector", filtro.getSectorNumPredial());
        }
        if (filtro.getComunaNumPredial() != null &&
            !filtro.getComunaNumPredial().isEmpty()) {
            query.setParameter("comuna", filtro.getComunaNumPredial() + "__");
        }
        if (filtro.getBarrioNumPredial() != null &&
            !filtro.getBarrioNumPredial().isEmpty()) {
            query.setParameter("barrio", "__" + filtro.getBarrioNumPredial());
        }
        if (filtro.getManzanaNumPredial() != null &&
            !filtro.getManzanaNumPredial().isEmpty()) {
            query.setParameter("manzana", filtro.getManzanaNumPredial());
        }
        if (filtro.getCondicionPropiedadNumPredial() != null &&
            !filtro.getCondicionPropiedadNumPredial().isEmpty()) {
            query.setParameter("condicionPropiedad",
                filtro.getCondicionPropiedadNumPredial());
        }
        if (filtro.getTerrenoNumPredial() != null &&
            !filtro.getTerrenoNumPredial().isEmpty()) {
            query.setParameter("terreno", filtro.getTerrenoNumPredial());
        }
        if (filtro.getEdificioNumPredial() != null &&
            !filtro.getEdificioNumPredial().isEmpty()) {
            query.setParameter("edificio", filtro.getEdificioNumPredial());
        }
        if (filtro.getPisoNumPredial() != null &&
            !filtro.getPisoNumPredial().isEmpty()) {
            query.setParameter("piso", filtro.getPisoNumPredial());
        }
        if (filtro.getUnidadNumPredial() != null &&
            !filtro.getUnidadNumPredial().isEmpty()) {
            query.setParameter("unidad", filtro.getUnidadNumPredial());
        }
        return query;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#lookForCurrentOnPredio(Long)
     * @author pedro.garcia
     *
     * OJO: la tabla que antes se llamaba Tramite_Predio_Englobe la renombraron
     * Tramite_Detalle_Predio, pero por pereza no cambiaron el entity correspondiente
     */
    @Implement
    @Override
    public List<Tramite> lookForCurrentOnPredio(Long predioId) {
        LOGGER.debug("on TramiteDAOBean#lookForCurrentOnPredio ...");

        String queryString;
        Query query;
        ArrayList<Tramite> answer = null;

        queryString = "SELECT t FROM Tramite t " +
            " WHERE (t.predio.id = :predioIdP OR t.id IN " +
            " (SELECT tramiteDetallePredio.tramite.id FROM TramitePredioEnglobe tramiteDetallePredio " +
            "  WHERE tramiteDetallePredio.predio.id = :predioIdP))" +
            " AND (t.estado NOT IN ('RECHAZADO', 'ARCHIVADO', 'CANCELADO', 'FINALIZADO_APROBADO'))" +
            " AND t.numeroRadicacion IS NOT null ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("predioIdP", predioId);
            answer = (ArrayList<Tramite>) query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "TramiteDAOBean#lookForCurrentOnPredio");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#lookForFinishedOnPredio(Long)
     * @author pedro.garcia
     */
    /*
     * Se agrega inicializacion tipo lazy del predio y predioAvaluoCatastral para los tramites
     * consultados @modified by leidy.gonzalez :: #12528::26/05/2015::CU_187
     */
    @Implement
    @Override
    public List<Tramite> lookForFinishedOnPredio(Long predioId) {
        LOGGER.debug("on TramiteDAOBean#lookForFinishedOnPredio ...");

        String queryString;
        Query query;
        ArrayList<Tramite> answer = null;

        queryString = "SELECT t FROM Tramite t " +
            " LEFT JOIN FETCH t.resultadoDocumento " +
            " WHERE (t.predio.id = :predioIdP OR t.id IN " +
            " (SELECT tramiteDetallePredio.tramite.id FROM TramitePredioEnglobe tramiteDetallePredio " +
            "  WHERE tramiteDetallePredio.predio.id = :predioIdP))" +
            "  AND t.resultadoDocumento IS NOT NULL " +
            "  AND t.estado = :tEstadoP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("predioIdP", predioId);
            query.setParameter("tEstadoP", ETramiteEstado.FINALIZADO_APROBADO.getCodigo());
            answer = (ArrayList<Tramite>) query.getResultList();

            if (answer != null) {
                for (Tramite tramite : answer) {
                    Hibernate.initialize(tramite.getPredio());
                    if (tramite.getPredio() != null) {
                        Hibernate.initialize(tramite.getPredio().getPredioAvaluoCatastrals());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "TramiteDAOBean#lookForFinishedOnPredio");
        }

        return answer;

    }

    // -------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramiteDAO#findTramitePredioSolicitudByTramiteId(Long)
     */
    @Override
    public Tramite findTramitePredioSolicitudByTramiteId(Long tramiteId) {

        Tramite answer = null;
        Query query = this.entityManager.createQuery("SELECT t " +
            " FROM Tramite t " +
            " LEFT JOIN FETCH t.predio p " +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio " +
            " JOIN FETCH t.solicitud " +
            " WHERE t.id= :tramiteId ");
        query.setParameter("tramiteId", tramiteId);

        try {
            answer = (Tramite) query.getSingleResult();

            if (answer.getTramiteRelacionTramitesPadres() != null &&
                !answer.getTramiteRelacionTramitesPadres().isEmpty()) {
                answer.getTramiteRelacionTramitesPadres().size();
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#findTramiteSolicitudByFiltroTramite(FiltroDatosConsultaTramite)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<Tramite> findTramiteSolicitudByFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        List<Tramite> answer;

        query.append("SELECT DISTINCT t" + " FROM Tramite t" +
            " JOIN FETCH t.solicitud s" + " LEFT JOIN t.predio predio" +
            " LEFT JOIN FETCH t.departamento tramDepto" +
            " LEFT JOIN FETCH t.municipio tramMuni" +
            " LEFT JOIN predio.departamento departamento" +
            " LEFT JOIN predio.municipio municipio" +
            " LEFT JOIN FETCH t.resultadoDocumento resolucion" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.tramiteEstados te" + " WHERE 1 = 1");

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero = :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion = :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            query.append(" AND s.fecha BETWEEN :fechaSolicitud1 AND :fechaSolicitud2");
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            query.append(" AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2");
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite = :tipoTramite");
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion = :claseMutacion");
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo = :subtipo");
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            query.append(" AND t.clasificacion = :clasificacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            query.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor");
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            query.append(" AND resolucion.numeroDocumento = :numeroResolucion");
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            query.append(
                " AND resolucion.fechaDocumento BETWEEN :fechaResolucion1 AND :fechaResolucion1");
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            query.append(" AND ss.tipoIdentificacion = :tipoIdentificacion");
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            query.append(" AND ss.digitoVerificacion = :digitoVerificacion");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                filtroBusquedaTramite.getNumeroSolicitud());
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                filtroBusquedaTramite.getNumeroRadicacion());
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            q.setParameter("fechaSolicitud1",
                filtroBusquedaTramite.getFechaSolicitud());
            Date hasta = filtroBusquedaTramite.getFechaSolicitud();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaSolicitud2", c.getTime());
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            q.setParameter("fechaRadicacion1",
                filtroBusquedaTramite.getFechaRadicacion());
            Date hasta = filtroBusquedaTramite.getFechaRadicacion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaRadicacion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            q.setParameter("tipoSolicitud",
                filtroBusquedaTramite.getTipoSolicitud());
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite",
                filtroBusquedaTramite.getTipoTramite());
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion",
                filtroBusquedaTramite.getClaseMutacion());
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            q.setParameter("funcionarioEjecutor", filtroBusquedaTramite.getFuncionarioEjecutor());
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            q.setParameter("numeroResolucion",
                filtroBusquedaTramite.getNumeroResolucion());
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            q.setParameter("fechaResolucion1",
                filtroBusquedaTramite.getFechaResolucion());
            Date hasta = filtroBusquedaTramite.getFechaResolucion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaResolucion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtroBusquedaTramite.getTipoIdentificacion());
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                filtroBusquedaTramite.getNumeroIdentificacion());
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                filtroBusquedaTramite.getDigitoVerificacion());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }

        try {
            answer = (List<Tramite>) super.findInRangeUsingQuery(q,
                rowStartIdxAndCount);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#countTramiteSolicitudByFiltroTramite(FiltroDatosConsultaTramite)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Integer countTramiteSolicitudByFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        Long answer;

        query.append("SELECT COUNT (DISTINCT t)" +
            " FROM Tramite t" +
            " JOIN t.solicitud s" +
            " LEFT JOIN t.predio predio" +
            " LEFT JOIN t.departamento tramDepto" +
            " LEFT JOIN t.municipio tramMuni" +
            " LEFT JOIN predio.departamento departamento" +
            " LEFT JOIN predio.municipio municipio" +
            " LEFT JOIN t.resultadoDocumento resolucion" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.tramiteEstados te" +
            " WHERE 1=1");

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero = :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion = :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            query.append(" AND s.fecha BETWEEN :fechaSolicitud1 AND :fechaSolicitud2");
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            query.append(" AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2");
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite = :tipoTramite");
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion = :claseMutacion");
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo = :subtipo");
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            query.append(" AND t.clasificacion = :clasificacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            query.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor");
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            query.append(" AND resolucion.numeroDocumento = :numeroResolucion");
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            query.append(
                " AND resolucion.fechaDocumento BETWEEN :fechaResolucion1 AND :fechaResolucion2");
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            query.append(" AND ss.tipoIdentificacion = :tipoIdentificacion");
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            query.append(" AND ss.digitoVerificacion = :digitoVerificacion");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo like :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo like :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        Query q = this.entityManager.createQuery(query.toString());

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                filtroBusquedaTramite.getNumeroSolicitud());
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                filtroBusquedaTramite.getNumeroRadicacion());
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            q.setParameter("fechaSolicitud1",
                filtroBusquedaTramite.getFechaSolicitud());
            Date hasta = filtroBusquedaTramite.getFechaSolicitud();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaSolicitud2", c.getTime());

        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            q.setParameter("fechaRadicacion1",
                filtroBusquedaTramite.getFechaRadicacion());
            Date hasta = filtroBusquedaTramite.getFechaRadicacion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaRadicacion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            q.setParameter("tipoSolicitud",
                filtroBusquedaTramite.getTipoSolicitud());
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite",
                filtroBusquedaTramite.getTipoTramite());
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion",
                filtroBusquedaTramite.getClaseMutacion());
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            q.setParameter("funcionarioEjecutor", filtroBusquedaTramite.getFuncionarioEjecutor());
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            q.setParameter("numeroResolucion",
                filtroBusquedaTramite.getNumeroResolucion());
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            q.setParameter("fechaResolucion1",
                filtroBusquedaTramite.getFechaResolucion());
            Date hasta = filtroBusquedaTramite.getFechaResolucion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaResolucion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtroBusquedaTramite.getTipoIdentificacion());
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                filtroBusquedaTramite.getNumeroIdentificacion());
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                filtroBusquedaTramite.getDigitoVerificacion());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }

        try {
            answer = (Long) q.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer.intValue();
    }

    //---------------------------------------------------------------------------------------------
    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#getTramitesSaldosDeConservacion(java.util.List)
     */
    @Override
    public List<Tramite> getTramitesSaldosDeConservacion(List<Long> tramiteIds) {
        LOGGER.debug("Ejecutando ITramiteDAO#getTramitesSaldosDeConservacion");

        String query = "SELECT t FROM Tramite t " +
            "JOIN FETCH t.predio p " +
            "WHERE t.id in (:tramiteIds) " +
            "AND t.funcionarioEjecutor IS NULL " +
            "AND t.clasificacion = :clasificacion";
        List<Tramite> answer;
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteIds", tramiteIds);
            q.setParameter("clasificacion", ETramiteClasificacion.TERRENO.toString());
            answer = (List<Tramite>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#getSolicitantesParaNotificarByTramiteId(long)
     * @author juan.agudelo
     * @version 2.0
     */
    /*
     * @documented pedro.garcia @modified pedro.garcia 17-07-2012 eliminación de condición estúpida
     * que preguntaba si el id del trámite consultado es igual al parámetro 25-07-2012 corrección de
     * condición para determinar si el solicitante ya fue notificado @modified leidy.gonzalez Se
     * cambia condicion de break solo cuando encuentre el campo NotificacionDocumentoId diferente de
     * nulo y permita visualizar al solicitante notificado
     */
    @Implement
    @Override
    public List<Solicitante> getSolicitantesParaNotificarByTramiteId(long tramiteId) {

        LOGGER.debug("executing TramiteDAOBean#getSolicitantesParaNotificarByTramiteId");

        StringBuilder query = new StringBuilder();
        List<Solicitante> answer = null;

        query.append("SELECT t " + " FROM Tramite t" + " JOIN t.solicitud s" +
            " LEFT JOIN FETCH t.tramiteDocumentos" +
            " WHERE t.id = :tramiteId");

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("tramiteId", tramiteId);

        try {
            Tramite tramite = (Tramite) q.getSingleResult();

            List<SolicitanteSolicitud> solicitantesSolicitud =
                tramite.getSolicitud().getSolicitanteSolicituds();

            List<SolicitanteTramite> solicitantesTramite = null;

            solicitantesTramite = tramite.getSolicitanteTramites();

            answer = new ArrayList<Solicitante>();

            //D: reglas para obtener los solicitantes (Validado con clara)
            //D: si el trámite es de avisos se traen los solicitantes del trámite
            if (tramite.getSolicitud().isSolicitudAvisos()) {

                if (solicitantesTramite != null && !solicitantesTramite.isEmpty()) {
                    for (SolicitanteTramite stTmp : solicitantesTramite) {
                        answer.add(stTmp.getSolicitante());
                    }
                }
            } //D: si el trámite es de solicitud de autoavalúo se traen los solicitantes del trámite, 
            //   si los hay; si no, se toman los de la solicitud
            else if (tramite.getSolicitud().isSolicitudAutoavaluo()) {

                if (solicitantesTramite != null && !solicitantesTramite.isEmpty()) {
                    for (SolicitanteTramite stTmp : solicitantesTramite) {
                        answer.add(stTmp.getSolicitante());
                    }
                }
                if (solicitantesSolicitud != null && !solicitantesSolicitud.isEmpty()) {
                        for (SolicitanteSolicitud ssTmp : solicitantesSolicitud) {
                            answer.add(ssTmp.getSolicitante());
                        }
                 }

            } //D: si es otro tipo de trámite se traen los de la solicitud y los del trámite
            else {
                if (solicitantesSolicitud != null && !solicitantesSolicitud.isEmpty()) {
                    for (SolicitanteSolicitud ssTmp : solicitantesSolicitud) {
                        answer.add(ssTmp.getSolicitante());
                    }
                }

                if (solicitantesTramite != null && !solicitantesTramite.isEmpty()) {
                    for (SolicitanteTramite stTmp : solicitantesTramite) {
                        answer.add(stTmp.getSolicitante());
                    }
                }

            }

            //D: se revisan condiciones para decir si el solicitante ya fue notificado
            //D: si el trámite tiene documentos se revisan las condiciones
            if (tramite.getTramiteDocumentos() != null &&
                !tramite.getTramiteDocumentos().isEmpty()) {

                for (Solicitante sTemp : answer) {
                    for (TramiteDocumento tdTemp : tramite.getTramiteDocumentos()) {

                        //D: un solicitante se da por notificado cuando ya se ha registrado la 
                        //   notificación, y por lo tanto existe un Documento de notificación asociado
                        if (tdTemp.getIdPersonaNotificacion() != null &&
                            tdTemp.getIdPersonaNotificacion().equals(
                                sTemp.getNumeroIdentificacion())) {

                            if (tdTemp.getNotificacionDocumentoId() != null) {
                                sTemp.setSolicitanteNotificado(ESiNo.SI.getCodigo());
                                break;
                            } else {
                                sTemp.setSolicitanteNotificado(ESiNo.NO.getCodigo());
                            }
                        }
                    }
                }
            } //D ... si no, se asume que no ha sido notificado
            else {
                for (Solicitante sTemp : answer) {
                    sTemp.setSolicitanteNotificado(ESiNo.NO.getCodigo());
                }
            }

        } catch (NoResultException nr) {
            answer = new ArrayList<Solicitante>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        if (answer != null && !answer.isEmpty()) {
            for (Solicitante s : answer) {
                s.getId();

                if (s.getDireccionDepartamento() != null) {
                    s.getDireccionDepartamento().getNombre();
                }

                if (s.getDireccionMunicipio() != null) {
                    s.getDireccionMunicipio().getNombre();
                }
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#buscarTramitesPorNumeroDeResolucion (String resolucion)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> buscarTramitesPorNumeroDeResolucion(String resolucion) {

        LOGGER.debug("Entra a TramiteDAOBean#buscarTramitesPorNumeroDeResolucion");
        List<Tramite> answer = null;
        String claseTipoDocumento = ETipoDocumentoClase.RESOLUCION.toString();
        String queryString;
        Query query;

        queryString = " SELECT DISTINCT t FROM Tramite t" +
            " LEFT JOIN FETCH t.tramitePredioEnglobes" +
            " LEFT JOIN FETCH t.predio" +
            " LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td" +
            " JOIN FETCH t.departamento" +
            " JOIN FETCH t.municipio" +
            " WHERE d.numeroDocumento = :resolucion AND td.clase = :claseTipoDocumento ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("resolucion", resolucion);
            query.setParameter("claseTipoDocumento", claseTipoDocumento);

            answer = (List<Tramite>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#getPredioDptoMuniByTramiteId(Long)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Predio getPredioDptoMuniByTramiteId(Long tramiteId) {

        LOGGER.debug("executing TramiteDAOBean#getPredioDptoMuniByTramiteId");

        Predio answer = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT p FROM Tramite t");
        query.append(" JOIN t.predio p");
        query.append(" LEFT JOIN FETCH p.departamento");
        query.append(" LEFT JOIN FETCH p.municipio");
        query.append(" WHERE t.id = :tramiteId");

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("tramiteId", tramiteId);

        try {
            answer = (Predio) q.getSingleResult();

        } catch (NoResultException nr) {
            return new Predio();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#getPredioDptoMuniByTramiteId");
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see ITramiteDAO#getTramiteDocumentosByTramiteId(Long )
     * @author juan.agudelo
     * @version 2.0
     *
     * @modified by franz.gamba:: este metodo trae los docyuemntos de un tramite desde el tramite y
     * deberia traerlos desde el DAO que esta definido para eso.
     */
    @Deprecated
    @Override
    public List<TramiteDocumento> getTramiteDocumentosByTramiteId(Long tramiteId) {
        LOGGER.debug("executing TramiteDAOBean#getTramiteDocumentosByTramiteId");

        List<TramiteDocumento> answer = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT td FROM Tramite t");
        query.append(" JOIN FETCH t.tramiteDocumentos td");
        query.append(" JOIN FETCH td.documento d");
        query.append(" JOIN FETCH d.tipoDocumento ");
        query.append(" WHERE t.id = :tramiteId");

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("tramiteId", tramiteId);

        try {
            answer = (List<TramiteDocumento>) q.getResultList();

        } catch (IndexOutOfBoundsException ioe) {
            return new ArrayList<TramiteDocumento>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#getTramiteDocumentosByTramiteId");
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see ITramiteDAO#getTramiteDocumentacionsByTramiteId(Long )
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<TramiteDocumentacion> getTramiteDocumentacionsByTramiteId(
        Long tramiteId) {
        LOGGER.debug("executing TramiteDAOBean#getTramiteDocumentacionsByTramiteId");

        List<TramiteDocumentacion> answer = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT td FROM Tramite t");
        query.append(" JOIN t.tramiteDocumentacions td");
        query.append(" WHERE t.id = :tramiteId");

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("tramiteId", tramiteId);

        try {
            answer = (List<TramiteDocumentacion>) q.getResultList();

        } catch (IndexOutOfBoundsException ioe) {
            return new ArrayList<TramiteDocumentacion>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#getTramiteDocumentacionsByTramiteId");
        }

        return answer;
    }

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#getEjecutorTramiteClasificacion(java.util.List)
     */
    @Override
    public SEjecutoresTramite getEjecutorTramiteClasificacion(List<Long> tramiteIds) {

        SEjecutoresTramite ejecutor = new SEjecutoresTramite();

        String query = "SELECT COUNT(t) FROM Tramite t" +
            " WHERE t.clasificacion = :clasificacion" +
            " AND t.id IN(:tramiteIds)";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteIds", tramiteIds);

            q.setParameter("clasificacion", ETramiteClasificacion.OFICINA.toString());

            Long conteo = (Long) q.getSingleResult();
            ejecutor.setOficina(conteo);

            q = this.entityManager.createQuery(query);
            q.setParameter("tramiteIds", tramiteIds);
            q.setParameter("clasificacion", ETramiteClasificacion.TERRENO.toString());

            conteo = (Long) q.getSingleResult();
            ejecutor.setTerreno(conteo);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#getEjecutorTramiteClasificacion");
        }

        return ejecutor;
    }

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#getTramitesDeActGeograficas(java.util.List)
     */
    @Override
    public List<Tramite> getTramitesDeActGeograficas(List<Long> tramiteIds) {

        List<Tramite> answer = new LinkedList<Tramite>();

        String query =
            "SELECT DISTINCT t FROM Tramite t" +
            //				"SELECT t FROM Tramite t" +
            " JOIN FETCH t.departamento " +
            " JOIN FETCH t.municipio " +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
            " LEFT JOIN FETCH tpe.predio pe" +
            " WHERE t.id IN (:tramiteIds) ";
        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("tramiteIds", tramiteIds);

            answer = (List<Tramite>) q.getResultList();

            if (answer != null) {
                for (Tramite t : answer) {
                    if (t.getTramitePredioEnglobes() != null) {
                        for (TramitePredioEnglobe tpe : t.getTramitePredioEnglobes()) {
                            tpe.getPredio().getPredioDireccions().size();
                        }
                    }
                    if (t.getPredio() != null &&
                        t.getPredio().getUnidadConstruccions() != null) {
                        for (UnidadConstruccion uc : t.getPredio().getUnidadConstruccions()) {
                            uc.getAreaConstruida();
                        }
                    }
                    if (t.getPredio() != null &&
                        t.getPredio().getPredioDireccions() != null) {
                        for (PredioDireccion pd : t.getPredio().getPredioDireccions()) {
                            pd.getDireccion();
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#getTramitesDeActGeograficas");
        }

        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see co.gov.igac.snc.dao.tramite.ITramiteDAO#findTramiteIdsDeTramitesParaNotificar()
     */
    /**
     * @modified by leidy.gonzalez::08-08-2018::#70681::Reclamar actividad por parte del usuario autenticado
     */ 
    @Override
    public List<Long> findTramiteIdsDeTramitesParaNotificar(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String nombreActividad, List<Long> iDsTramitesProc,final int... rowStartIdxAndCount) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        List<Tramite> answer;

        query.append("SELECT DISTINCT t" + " FROM Tramite t" +
            " JOIN  t.solicitud s" + " LEFT JOIN t.predio predio" +
            " LEFT JOIN t.departamento tramDepto" +
            " LEFT JOIN t.municipio tramMuni" +
            " LEFT JOIN predio.departamento departamento" +
            " LEFT JOIN predio.municipio municipio" +
            " LEFT JOIN t.resultadoDocumento resolucion" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.tramiteEstados te" + " WHERE 1 = 1" +
            " AND  t.id IN (:iDsTramitesProc)");

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            query.append(" AND s.fecha BETWEEN :fechaSolicitud1 AND :fechaSolicitud2");
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            query.append(" AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2");
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite = :tipoTramite");
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion = :claseMutacion");
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo = :subtipo");
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            query.append(" AND t.clasificacion = :clasificacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            query.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor");
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            query.append(" AND resolucion.numeroDocumento = :numeroResolucion");
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            query.append(
                " AND resolucion.fechaDocumento BETWEEN :fechaResolucion1 AND :fechaResolucion2");
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            query.append(" AND ss.tipoIdentificacion = :tipoIdentificacion");
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            query.append(" AND ss.digitoVerificacion = :digitoVerificacion");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        Query q = entityManager.createQuery(query.toString());
        
        q.setParameter("iDsTramitesProc",iDsTramitesProc);

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud", "%" +
                filtroBusquedaTramite.getNumeroSolicitud() + "%");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion", "%" +
                filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            q.setParameter("fechaSolicitud1",
                filtroBusquedaTramite.getFechaSolicitud());
            Date hasta = filtroBusquedaTramite.getFechaSolicitud();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaSolicitud2", c.getTime());
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            q.setParameter("fechaRadicacion1",
                filtroBusquedaTramite.getFechaRadicacion());
            Date hasta = filtroBusquedaTramite.getFechaRadicacion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaRadicacion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            q.setParameter("tipoSolicitud",
                filtroBusquedaTramite.getTipoSolicitud());
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite",
                filtroBusquedaTramite.getTipoTramite());
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion",
                filtroBusquedaTramite.getClaseMutacion());
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            q.setParameter("funcionarioEjecutor", filtroBusquedaTramite.getFuncionarioEjecutor());
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            q.setParameter("numeroResolucion",
                filtroBusquedaTramite.getNumeroResolucion());
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            q.setParameter("fechaResolucion1",
                filtroBusquedaTramite.getFechaResolucion());
            Date hasta = filtroBusquedaTramite.getFechaResolucion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaResolucion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtroBusquedaTramite.getTipoIdentificacion());
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                filtroBusquedaTramite.getNumeroIdentificacion());
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                filtroBusquedaTramite.getDigitoVerificacion());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }
        List<Long> tramiteIds = null;
        try {
            answer = (List<Tramite>) super.findInRangeUsingQuery(q,
                rowStartIdxAndCount);
            if (answer != null) {
                tramiteIds = new LinkedList<Long>();
                if (answer.size() > 1000) {
                    tramiteIds.add(-1L);
                    return tramiteIds;
                }
                for (Tramite t : answer) {
                    //leidy.gonzalez::08-08-2018::#70681::Reclamar actividad por parte del usuario autenticado 
                    tramiteIds.add(t.getId());
//                    List<Actividad> actividadesTramite = null;
//
//                    if (t.getProcesoInstanciaId() != null && !t.getProcesoInstanciaId().isEmpty()) {
//
//                        actividadesTramite = this.remoteProcesosService
//                            .obtenerActividadesProceso(t
//                                .getProcesoInstanciaId());
//
//                        if (actividadesTramite != null &&
//                            !actividadesTramite.isEmpty()) {
//
//                            for (Actividad aTmp : actividadesTramite) {
//                                if (ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION.
//                                    equals(nombreActividad)) {
//                                    if (aTmp.getNombre() != null && !aTmp.getNombre().isEmpty() &&
//                                        (aTmp.getNombre().equals(
//                                            ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
//                                        aTmp.getNombre().equals(
//                                            ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO))) {
//
//                                        tramiteIds.add(t.getId());
//                                        break;
//                                    }
//                                } else {
//                                    if (aTmp.getNombre() != null &&
//                                        !aTmp.getNombre().isEmpty() &&
//                                        (aTmp.getNombre().equals(nombreActividad))) {
//
//                                        tramiteIds.add(t.getId());
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return tramiteIds;
    }
//---------------------------------------------------------------------------------------------

    private StringBuilder setupQueryForTramitesPorNotificarSearch(StringBuilder query,
        FiltroDatosConsultaTramite filtroBusquedaTramite) {
        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            query.append(" AND s.fecha BETWEEN :fechaSolicitud1 AND :fechaSolicitud2");
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            query.append(" AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2");
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite = :tipoTramite");
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion = :claseMutacion");
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo = :subtipo");
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            query.append(" AND t.clasificacion = :clasificacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            query.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor");
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            query.append(" AND resolucion.numeroDocumento = :numeroResolucion");
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            query.append(
                " AND resolucion.fechaDocumento BETWEEN :fechaResolucion1 AND :fechaResolucion2");
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            query.append(" AND ss.tipoIdentificacion = :tipoIdentificacion");
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            query.append(" AND ss.digitoVerificacion = :digitoVerificacion");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        return query;
    }

    //---------------------------------------------------------------------------------------------
    private Query setupParametersQueryTramitesNotif(Query q,
        FiltroDatosConsultaTramite filtroBusquedaTramite) {

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud", "%" +
                filtroBusquedaTramite.getNumeroSolicitud() + "%");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion", "%" +
                filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            q.setParameter("fechaSolicitud1",
                filtroBusquedaTramite.getFechaSolicitud());
            Date hasta = filtroBusquedaTramite.getFechaSolicitud();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaSolicitud2", c.getTime());
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            q.setParameter("fechaRadicacion1",
                filtroBusquedaTramite.getFechaRadicacion());
            Date hasta = filtroBusquedaTramite.getFechaRadicacion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaRadicacion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            q.setParameter("tipoSolicitud",
                filtroBusquedaTramite.getTipoSolicitud());
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite",
                filtroBusquedaTramite.getTipoTramite());
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion",
                filtroBusquedaTramite.getClaseMutacion());
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            q.setParameter("funcionarioEjecutor", filtroBusquedaTramite.getFuncionarioEjecutor());
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            q.setParameter("numeroResolucion",
                filtroBusquedaTramite.getNumeroResolucion());
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            q.setParameter("fechaResolucion1",
                filtroBusquedaTramite.getFechaResolucion());
            Date hasta = filtroBusquedaTramite.getFechaResolucion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaResolucion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtroBusquedaTramite.getTipoIdentificacion());
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                filtroBusquedaTramite.getNumeroIdentificacion());
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                filtroBusquedaTramite.getDigitoVerificacion());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }
        return q;
    }
    //---------------------------------------------------------------------------------------------

    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findTramitesParaNotificarByIds(List<Long> tramiteIds,
        String sortField, FiltroDatosConsultaTramite filtroBusquedaTramite,
        String sortOrder, Map<String, String> filters, int... contadoresDesdeYCuantos) {

        StringBuilder sql = new StringBuilder();
        String sqlBasico;
        Query q;
        String where = "";

        Map<String, Object> valoresFiltros = new HashMap<String, Object>();
        if (filters != null) {
            for (String campo : filters.keySet()) {
                if (campo.equals("numeroRadicacion") || campo.equals("solicitud.numero")) {
                    if (campo.equals("solicitud.numero")) {
                        where += " AND t." + campo + " like :solicitudNumero";
                        valoresFiltros.put("solicitudNumero", "%" + filters.get(campo) + "%");

                    } else {
                        where += " AND t." + campo + " like :" + campo;
                        valoresFiltros.put(campo, "%" + filters.get(campo) + "%");
                    }
                }
                if (campo.equals("fechaRadicacion") && filters.get(campo) != null && filters.get(
                    campo).length() == 10) {
                    try {
                        DateFormat df = DateFormat.getDateInstance();
                        valoresFiltros.put(campo, df.parse(filters.get(campo)));

                        where += " AND t." + campo + " = :" + campo;
                    } catch (ParseException e) {

                    }

                }
            }
        }

        List<Tramite> tramites;
        if (tramiteIds.size() >= 1000) {
            Query q2 = entityManager.createNativeQuery("SELECT SESION_ID_SEQ.NEXTVAL FROM DUAL");
            Object o = q2.getSingleResult();
            for (Long id : tramiteIds) {
                TmpTramiteSession tts = new TmpTramiteSession();
                TmpTramiteSessionId ttsId = new TmpTramiteSessionId();
                ttsId.setSesionId((BigDecimal) o);
                ttsId.setTramiteId(new BigDecimal(id));
                tts.setId(ttsId);
                entityManager.persist(tts);
            }
            sqlBasico = "SELECT DISTINCT t.id FROM Tramite t " //+ " JOIN t.solicitud "
                +

                
                " WHERE t.id IN (SELECT tts.id.tramiteId FROM TmpTramiteSession tts where tts.id.sesionId=:sesionId)";

            sql.append("SELECT DISTINCT t" + " FROM Tramite t" +
                " JOIN  t.solicitud s" + " LEFT JOIN t.predio predio" +
                " LEFT JOIN FETCH t.departamento tramDepto" +
                " LEFT JOIN FETCH t.municipio tramMuni" +
                " LEFT JOIN predio.departamento departamento" +
                " LEFT JOIN predio.municipio municipio" +
                " LEFT JOIN t.resultadoDocumento resolucion" +
                " LEFT JOIN s.solicitanteSolicituds ss" +
                " LEFT JOIN t.tramiteEstados te" +
                " WHERE t.id IN (:tramiteIds)");

            if (valoresFiltros.size() > 0) {
                sqlBasico += where;
                sql.append(where);
            }

            sql = this.setupQueryForTramitesPorNotificarSearch(sql, filtroBusquedaTramite);

            if (sortField != null && !sortOrder.equals("UNSORTED")) {
                sql.append(" ORDER BY t.").append(sortField).append(sortOrder.equals("DESCENDING") ?
                    " DESC" : " ASC");
                sqlBasico += " ORDER BY t." + sortField +
                    (sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

            //setupQueryForTraanitesPorNotificarSearch
            q = entityManager.createQuery(sqlBasico);
            q.setParameter("sesionId", (BigDecimal) o);

            if (valoresFiltros.size() > 0) {
                for (String campo : valoresFiltros.keySet()) {
                    q.setParameter(campo, valoresFiltros.get(campo));
                    LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
                }
            }

        } else {
            sqlBasico = "SELECT DISTINCT t.id FROM Tramite t" //+ " JOIN t.solicitud "
                +
                 " WHERE t.id IN (:tramiteIds)";

            sql.append("SELECT DISTINCT t" + " FROM Tramite t" +
                " JOIN  t.solicitud s" + " LEFT JOIN t.predio predio" +
                " LEFT JOIN FETCH t.departamento tramDepto" +
                " LEFT JOIN FETCH t.municipio tramMuni" +
                " LEFT JOIN predio.departamento departamento" +
                " LEFT JOIN predio.municipio municipio" +
                " LEFT JOIN t.resultadoDocumento resolucion" +
                " LEFT JOIN s.solicitanteSolicituds ss" +
                " LEFT JOIN t.tramiteEstados te" +
                " WHERE t.id IN (:tramiteIds)");

            if (valoresFiltros.size() > 0) {
                sqlBasico += where;
                sql.append(where);
            }

            if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
                sql.append(" ORDER BY t.").append(sortField).append(sortOrder.equals("DESCENDING") ?
                    " DESC" : " ASC");
                sqlBasico += " ORDER BY t." + sortField +
                    (sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

            LOGGER.debug("********************* SQL Basico *************");
            LOGGER.debug(sql.toString());

            q = entityManager.createQuery(sqlBasico);
            if (tramiteIds == null || tramiteIds.size() == 0) {
                ArrayList<Long> vacio = new ArrayList<Long>();
                vacio.add(0L);
                q.setParameter("tramiteIds", vacio);
            } else {
                q.setParameter("tramiteIds", tramiteIds);
            }
            if (valoresFiltros.size() > 0) {
                for (String campo : valoresFiltros.keySet()) {
                    q.setParameter(campo, valoresFiltros.get(campo));
                    LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
                }
            }

        }
        try {
            if (contadoresDesdeYCuantos != null && contadoresDesdeYCuantos.length > 0) {
                int rowStartIdx = Math.max(0, contadoresDesdeYCuantos[0]);
                if (rowStartIdx > 0) {
                    q.setFirstResult(rowStartIdx);
                }

                if (contadoresDesdeYCuantos.length > 1) {
                    int rowCount = Math.max(0, contadoresDesdeYCuantos[1]);
                    if (rowCount > 0) {
                        q.setMaxResults(rowCount);
                    }
                }
            }

            List<Long> tramitesIdsPagina = (List<Long>) q.getResultList();
            if (tramitesIdsPagina == null || tramitesIdsPagina.isEmpty()) {
                return new ArrayList<Tramite>();
            }

            LOGGER.debug("********************* SQL *************");
            LOGGER.debug(sql.toString());

            sql = this.setupQueryForTramitesPorNotificarSearch(sql, filtroBusquedaTramite);
            q = entityManager.createQuery(sql.toString());
            q.setParameter("tramiteIds", tramitesIdsPagina);
            q = this.setupParametersQueryTramitesNotif(q, filtroBusquedaTramite);
            if (valoresFiltros.size() > 0) {
                for (String campo : valoresFiltros.keySet()) {
                    q.setParameter(campo, valoresFiltros.get(campo));
                    LOGGER.debug("Setting : " + campo + " " + valoresFiltros.get(campo));
                }
            }

            tramites = (List<Tramite>) q.getResultList();

            for (Tramite tramite : tramites) {
                tramite.getTramitePredioEnglobes().size();
                tramite.getComisionTramites().size();
                tramite.getTramiteEstados().size();
                tramite.getTramiteDocumentacions().size();
                tramite.getResultadoDocumento().getNumeroDocumento();
                for (ComisionTramite ct : tramite.getComisionTramites()) {
                    ct.getComision().getNumero();
                }
            }
        } catch (IndexOutOfBoundsException ioe) {
            LOGGER.error(ioe.getMessage());
            return new ArrayList<Tramite>();

        } catch (Exception e) {
            System.out.println(e);
            System.out.println(e.getMessage());
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("ERROR:", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámites por múltiples ids", e);
        }
        return tramites;
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#getIdsDeTramitesAsociadosByTramiteId(java.lang.Long)
     */
    @Override
    public List<Long> getIdsDeTramitesAsociadosByTramiteId(Long tramiteId) {

        List<Long> tramiteIds = null;

        String query = "SELECT t.id FROM Tramite t " +
            " JOIN t.tramite" +
            " WHERE t.tramite.id =:tramiteId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            tramiteIds = (List<Long>) q.getResultList();

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámite por id", e);
        }

        return tramiteIds;
    }

    /**
     *
     * @author lorena.salamanca
     * @param tramiteId
     * @return
     */
    @Override
    public List<Tramite> getTramiteTramitesbyTramiteId(Long tramiteId) {

        List<Tramite> tramites = null;

        String query = "SELECT DISTINCT (t) FROM Tramite t " +
            " LEFT JOIN FETCH t.departamento " +
            " LEFT JOIN FETCH t.municipio " +
            " LEFT JOIN FETCH t.tramiteDocumentacions" +
            " WHERE t.tramite.id =:tramiteId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            tramites = (List<Tramite>) q.getResultList();

            for (Tramite tramite : tramites) {
                tramite.getTramiteDocumentacions().size();
                tramite.getTramites();
                if (tramite.getTramites() != null && !tramite.getTramites().isEmpty()) {
                    for (Tramite tt : tramite.getTramites()) {
                        tt.getTramites();
                    }
                }
                if (tramite.getTramiteDocumentacions() != null &&
                    !tramite.getTramiteDocumentacions().isEmpty()) {
                    tramite.setTramiteDocumentacions(this.remoteTramiteDocumentacionService.
                        findByTramiteId(tramite.getId()));

                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámite por id", e);
        }

        return tramites;
    }
    //---------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.tramite.ITramiteDAO#tramiteExists(java.lang.Long)
     */
    @Override
    public boolean tramiteExists(Long tramiteId) {

        String query = "SELECT COUNT (t) FROM Tramite t " +
            "WHERE t.id =:tramiteId";
        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("tramiteId", tramiteId);

            Integer conteo = Integer.parseInt(q.getSingleResult().toString());

            if (conteo > 0) {
                return true;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#tramiteExists");
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see
     * ITramiteDAO#obtenerTramitesByNumeroPredialPredio(java.lang.String,java.lang.String,java.lang.String)
     * @author javier.barajas
     */
    @Override
    public List<Tramite> obtenerTramitesByNumeroPredialPredio(String numPredial) {
        LOGGER.debug("executing TramiteDAOBean#obtenerTramitesByNumeroPredialPredio");

        List<Tramite> answer = null;
        Query query = null;
        String queryToExecute;

        // OJO: con JPQL se usan los nombres de las clases y los atributos de
        // esas clases, que corresponden al mapeo de las tablas!!!!!
        // Al usar "select new ..." se debe definir -en la clase entity
        // respectiva- un constructor
        // acorde a los campos que se traen en la consulta
        query = this.entityManager.createQuery(
            "SELECT t " +
            " FROM Tramite t " +
            " LEFT JOIN t.predio predio" +
            " WHERE t.numeroRadicacion =:numPredial");

        query.setParameter("numPredial", numPredial);

        try {
            answer = query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("TramiteDAOBean#obtenerTramitesByNumeroPredialPredio: " +
                "La consulta de trámites de un predio no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
     * @see ITramiteDAO#obtenerTramitesTiposTramitesByNumeroPredial(java.lang.String)
     * @author dumar.penuela
     * @param numPredial
     */
    @Override
    public List<Tramite> obtenerTramitesTiposTramitesByNumeroPredial(String numPredial) {
        LOGGER.debug("executing TramiteDAOBean#obtenerTramitesTiposTramitesByNumeroPredial");

        List<Tramite> answer = null;
        Query query;
        String queryToExecute;

        queryToExecute = "SELECT DISTINCT Tr " +
            " FROM Tramite Tr " +
            " JOIN FETCH Tr.predio p " +
            " JOIN FETCH p.municipio " +
            " JOIN FETCH p.departamento " +
            " WHERE Tr.predio.id IN" +
            " (SELECT Pr.id FROM Predio Pr WHERE Pr.numeroPredial = :numPredial) " +
            " OR Tr.id IN ( " +
            " SELECT TPE.tramite.id FROM TramitePredioEnglobe TPE " +
            " WHERE TPE.predio.id = (SELECT Pr.id FROM Predio Pr WHERE Pr.numeroPredial = :numPredial))";

        query = this.entityManager.createQuery(queryToExecute);
        query.setParameter("numPredial", numPredial);

        try {
            answer = query.getResultList();

            if (answer != null && !answer.isEmpty()) {
                for (Tramite tramite : answer) {

                    if (tramite.getResultadoDocumento() != null) {
                        Hibernate.initialize(tramite.getResultadoDocumento());
                    }
                    if (tramite.getSolicitud() != null) {
                        Hibernate.initialize(tramite.getSolicitud());
                    }

                    if (tramite.getSolicitud().getSolicitanteSolicituds() != null) {
                        Hibernate.initialize(tramite.getSolicitud().getSolicitanteSolicituds());
                    }

                    if (tramite.getPredio() != null) {
                        Hibernate.initialize(tramite.getPredio());
                    }

                }
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("TramiteDAOBean#obtenerTramitesByNumeroRadicado: " +
                "La consulta de trámites de un predio no devolvió resultados", nrEx);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
     * @see ITramiteDAO#obtenerTramitesTiposTramitesByNumeroIdentificacion(java.lang.String)
     * @author dumar.penuela
     * @param numIdentificacion
     * @param tipoIdentificacion
     */
    @Override
    public List<Tramite> obtenerTramitesTiposTramitesByNumeroIdentificacion(String numIdentificacion,
        String tipoIdentificacion) {
        LOGGER.debug("executing TramiteDAOBean#obtenerTramitesTiposTramitesByNumeroIdentificacion");

        List<Tramite> answer = null;
        Query query = null;
        String queryToExecute;

        queryToExecute =
            " SELECT DISTINCT t FROM Tramite t JOIN FETCH t.predio p JOIN p.personaPredios pp JOIN pp.persona pers " +
            " JOIN FETCH p.municipio " +
            " JOIN FETCH p.departamento " +
            " WHERE pers.numeroIdentificacion = :numeroIdentificacion " +
            " AND pers.tipoIdentificacion = :tipoIdentificacion ";

        try {

            query = this.entityManager.createQuery(queryToExecute);

            query.setParameter("numeroIdentificacion", numIdentificacion);
            query.setParameter("tipoIdentificacion", tipoIdentificacion);

            answer = query.getResultList();

            if (answer != null && !answer.isEmpty()) {
                for (Tramite tramite : answer) {

                    if (tramite.getResultadoDocumento() != null) {
                        Hibernate.initialize(tramite.getResultadoDocumento());
                    }
                    if (tramite.getSolicitud() != null) {
                        Hibernate.initialize(tramite.getSolicitud());
                    }

                    if (tramite.getSolicitud().getSolicitanteSolicituds() != null) {
                        Hibernate.initialize(tramite.getSolicitud().getSolicitanteSolicituds());
                    }

                }
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("TramiteDAOBean#obtenerTramitesByNumeroRadicado: " +
                "La consulta de trámites de un predio no devolvió resultados", nrEx);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see
     * ITramiteDAO#findTramiteByFiltroTramiteAdministracionAsignaciones(FiltroDatosConsultaTramite)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public List<Tramite> findTramiteByFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        List<Tramite> answer;

        query.append("SELECT DISTINCT t" + " FROM Tramite t" +
            " JOIN FETCH t.solicitud s" + " LEFT JOIN FETCH t.predio predio" +
            " LEFT JOIN FETCH t.departamento tramDepto" +
            " LEFT JOIN FETCH t.municipio tramMuni" +
            " LEFT JOIN predio.departamento departamento" +
            " LEFT JOIN predio.municipio municipio" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.comisionTramites cts" +
            " LEFT JOIN cts.comision c" +
            " WHERE t.funcionarioEjecutor IS NOT NULL" +
            " AND t.resultadoDocumento IS NULL" +
            " AND s.tipo != :viaGubernativa" +
            " AND s.tipo != :revocatoriaDirecta" +
            " AND (c IS NULL OR (c.estado != :porEjecutar " +
            " AND c.estado != :enEjecucion))");

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero = :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            query.append(" AND s.fecha BETWEEN :fechaSolicitud1 AND :fechaSolicitud2");
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            query.append(" AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2");
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite = :tipoTramite");
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion = :claseMutacion");
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo = :subtipo");
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            query.append(" AND t.clasificacion = :clasificacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            query.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor");
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            query.append(" AND resolucion.numeroDocumento = :numeroResolucion");
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            query.append(
                " AND resolucion.fechaDocumento BETWEEN :fechaResolucion1 AND :fechaResolucion1");
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            query.append(" AND ss.tipoIdentificacion = :tipoIdentificacion");
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            query.append(" AND ss.digitoVerificacion = :digitoVerificacion");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        if (filtroBusquedaTramite.getEstadoTramite() != null &&
            !filtroBusquedaTramite.getEstadoTramite()
                .isEmpty()) {
            query.append(" AND t.estado <> :estadoTramite");
        }

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            query.append(" ORDER BY t.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("porEjecutar", EComisionEstado.POR_EJECUTAR.getCodigo());
        q.setParameter("enEjecucion", EComisionEstado.EN_EJECUCION.getCodigo());
        q.setParameter("viaGubernativa", ESolicitudTipo.VIA_GUBERNATIVA.getCodigo());
        q.setParameter("revocatoriaDirecta", ESolicitudTipo.REVOCATORIA_DIRECTA.getCodigo());

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                filtroBusquedaTramite.getNumeroSolicitud());
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                "%" + filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            q.setParameter("fechaSolicitud1",
                filtroBusquedaTramite.getFechaSolicitud());
            Date hasta = filtroBusquedaTramite.getFechaSolicitud();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaSolicitud2", c.getTime());
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            q.setParameter("fechaRadicacion1",
                filtroBusquedaTramite.getFechaRadicacion());
            Calendar c = Calendar.getInstance();
            if (filtroBusquedaTramite.getFechaRadicacionFinal() != null) {
                c.setTime(filtroBusquedaTramite.getFechaRadicacionFinal());
            } else {
                Date hasta = filtroBusquedaTramite.getFechaRadicacion();
                c.setTime(hasta);
            }
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaRadicacion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            q.setParameter("tipoSolicitud",
                filtroBusquedaTramite.getTipoSolicitud());
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite",
                filtroBusquedaTramite.getTipoTramite());
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion",
                filtroBusquedaTramite.getClaseMutacion());
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            q.setParameter("funcionarioEjecutor", filtroBusquedaTramite.getFuncionarioEjecutor());
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            q.setParameter("numeroResolucion",
                filtroBusquedaTramite.getNumeroResolucion());
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            q.setParameter("fechaResolucion1",
                filtroBusquedaTramite.getFechaResolucion());
            Date hasta = filtroBusquedaTramite.getFechaResolucion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaResolucion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtroBusquedaTramite.getTipoIdentificacion());
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                filtroBusquedaTramite.getNumeroIdentificacion());
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                filtroBusquedaTramite.getDigitoVerificacion());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }

        if (filtroBusquedaTramite.getEstadoTramite() != null &&
            !filtroBusquedaTramite.getEstadoTramite()
                .isEmpty()) {
            q.setParameter("estadoTramite", filtroBusquedaTramite.getEstadoTramite());
        }

        try {
            answer = (List<Tramite>) super.findInRangeUsingQuery(q,
                rowStartIdxAndCount);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see
     * ITramiteDAO#findTramiteByFiltroTramiteAdministracionAsignaciones(FiltroDatosConsultaTramite)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public Integer countTramiteByFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        Long answer;

        query.append("SELECT COUNT(DISTINCT t) " + " FROM Tramite t" +
            " JOIN t.solicitud s" + " LEFT JOIN t.predio predio" +
            " LEFT JOIN t.departamento tramDepto" +
            " LEFT JOIN t.municipio tramMuni" +
            " LEFT JOIN predio.departamento departamento" +
            " LEFT JOIN predio.municipio municipio" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.comisionTramites cts" +
            " LEFT JOIN cts.comision c" +
            " WHERE t.funcionarioEjecutor IS NOT NULL" +
            " AND t.resultadoDocumento IS NULL" +
            " AND s.tipo != :viaGubernativa" +
            " AND s.tipo != :revocatoriaDirecta" +
            " AND (c IS NULL OR (c.estado != :porEjecutar " +
            " AND c.estado != :enEjecucion))");

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero = :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            query.append(" AND s.fecha BETWEEN :fechaSolicitud1 AND :fechaSolicitud2");
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            query.append(" AND t.fechaRadicacion BETWEEN :fechaRadicacion1 AND :fechaRadicacion2");
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite = :tipoTramite");
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion = :claseMutacion");
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo = :subtipo");
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            query.append(" AND t.clasificacion = :clasificacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            query.append(" AND t.funcionarioEjecutor = :funcionarioEjecutor");
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            query.append(" AND resolucion.numeroDocumento = :numeroResolucion");
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            query.append(
                " AND resolucion.fechaDocumento BETWEEN :fechaResolucion1 AND :fechaResolucion1");
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            query.append(" AND ss.tipoIdentificacion = :tipoIdentificacion");
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND ss.numeroIdentificacion = :numeroIdentificacion");
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            query.append(" AND ss.digitoVerificacion = :digitoVerificacion");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        if (filtroBusquedaTramite.getEstadoTramite() != null &&
            !filtroBusquedaTramite.getEstadoTramite()
                .isEmpty()) {
            query.append(" AND t.estado <> :estadoTramite");
        }

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("porEjecutar", EComisionEstado.POR_EJECUTAR.getCodigo());
        q.setParameter("enEjecucion", EComisionEstado.EN_EJECUCION.getCodigo());
        q.setParameter("viaGubernativa", ESolicitudTipo.VIA_GUBERNATIVA.getCodigo());
        q.setParameter("revocatoriaDirecta", ESolicitudTipo.REVOCATORIA_DIRECTA.getCodigo());

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                filtroBusquedaTramite.getNumeroSolicitud());
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                "%" + filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }
        if (filtroBusquedaTramite.getFechaSolicitud() != null) {
            q.setParameter("fechaSolicitud1",
                filtroBusquedaTramite.getFechaSolicitud());
            Date hasta = filtroBusquedaTramite.getFechaSolicitud();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaSolicitud2", c.getTime());
        }
        if (filtroBusquedaTramite.getFechaRadicacion() != null) {
            q.setParameter("fechaRadicacion1",
                filtroBusquedaTramite.getFechaRadicacion());
            Calendar c = Calendar.getInstance();
            if (filtroBusquedaTramite.getFechaRadicacionFinal() != null) {
                c.setTime(filtroBusquedaTramite.getFechaRadicacionFinal());
            } else {
                Date hasta = filtroBusquedaTramite.getFechaRadicacion();
                c.setTime(hasta);
            }
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaRadicacion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoSolicitud() != null &&
            !filtroBusquedaTramite.getTipoSolicitud().isEmpty()) {
            q.setParameter("tipoSolicitud",
                filtroBusquedaTramite.getTipoSolicitud());
        }
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite",
                filtroBusquedaTramite.getTipoTramite());
        }
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion",
                filtroBusquedaTramite.getClaseMutacion());
        }
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion()
                .isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor()
                .isEmpty()) {
            q.setParameter("funcionarioEjecutor", filtroBusquedaTramite.getFuncionarioEjecutor());
        }
        if (filtroBusquedaTramite.getNumeroResolucion() != null &&
            !filtroBusquedaTramite.getNumeroResolucion().isEmpty()) {
            q.setParameter("numeroResolucion",
                filtroBusquedaTramite.getNumeroResolucion());
        }
        if (filtroBusquedaTramite.getFechaResolucion() != null) {
            q.setParameter("fechaResolucion1",
                filtroBusquedaTramite.getFechaResolucion());
            Date hasta = filtroBusquedaTramite.getFechaResolucion();
            Calendar c = Calendar.getInstance();
            c.setTime(hasta);
            c.add(Calendar.DATE, 1);
            q.setParameter("fechaResolucion2", c.getTime());
        }
        if (filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !filtroBusquedaTramite.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtroBusquedaTramite.getTipoIdentificacion());
        }
        if (filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !filtroBusquedaTramite.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                filtroBusquedaTramite.getNumeroIdentificacion());
        }
        if (filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !filtroBusquedaTramite.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                filtroBusquedaTramite.getDigitoVerificacion());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }

        if (filtroBusquedaTramite.getEstadoTramite() != null &&
            !filtroBusquedaTramite.getEstadoTramite()
                .isEmpty()) {
            q.setParameter("estadoTramite", filtroBusquedaTramite.getEstadoTramite());
        }

        try {
            answer = (Long) q.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer.intValue();
    }
    // end of class

    /**
     * @see ITramiteDAO#findByNumeroMotivo(String numeroMotivo)
     * @author david.cifuentes
     * @modified by juanfelipe.garcia se modifico el tipo de dato de respuesta para mejorar las
     * comparaciones en las otras capas
     */
    @Override
    public HashMap<Long, Tramite> buscarTramitesPorListaDeIds(List<Long> tramiteIds) {
        LOGGER.debug("TramiteDAOBean#buscarTramitesPorListaDeIds...");
        HashMap<Long, Tramite> resultadoMap = new HashMap<Long, Tramite>();
        List<Tramite> answer = null;
        String queryString;
        Query query;

        try {

            if (tramiteIds.size() >= 1000) {
                Query q2 = entityManager.createNativeQuery("SELECT SESION_ID_SEQ.NEXTVAL FROM DUAL");
                Object o = q2.getSingleResult();
                Query q3 = entityManager.createNativeQuery(
                    "INSERT INTO tmp_tramite_session (sesion_id, tramite_id) VALUES (?,?)");
                q3.setParameter(1, (BigDecimal) o);

                for (Long id : tramiteIds) {
                    q3.setParameter(2, id);
                    q3.executeUpdate();

                }

                queryString = "SELECT DISTINCT t FROM Tramite t" +
                    " LEFT JOIN FETCH t.municipio m" +
                    " LEFT JOIN FETCH t.departamento d" +
                    " LEFT JOIN FETCH t.predio p " +
                    " LEFT JOIN FETCH t.tramitePredioEnglobes tpe " +
                    " LEFT JOIN FETCH tpe.predio ptpe " +
                    " LEFT JOIN FETCH t.solicitud s " +
                    " LEFT JOIN FETCH t.resultadoDocumento trd " +
                    " LEFT JOIN s.solicitanteSolicituds ss " +
                    " LEFT JOIN t.solicitanteTramites st " +
                    " WHERE t.id IN (SELECT tts.id.tramiteId FROM TmpTramiteSession tts where tts.id.sesionId=:sesionId)";

                query = entityManager.createQuery(queryString);
                query.setParameter("sesionId", (BigDecimal) o);

            } else {

                queryString = "SELECT DISTINCT t FROM Tramite t" +
                    " LEFT JOIN FETCH t.municipio m" +
                    " LEFT JOIN FETCH t.departamento d" +
                    " LEFT JOIN FETCH t.predio p " +
                    " LEFT JOIN FETCH t.tramitePredioEnglobes tpe " +
                    " LEFT JOIN FETCH tpe.predio ptpe " +
                    " LEFT JOIN FETCH t.solicitud s " +
                    " LEFT JOIN FETCH t.resultadoDocumento trd " +
                    " LEFT JOIN s.solicitanteSolicituds ss " +
                    " LEFT JOIN t.solicitanteTramites st " +
                    " WHERE t.id IN (:tramiteIds)";

                query = entityManager.createQuery(queryString);
                query.setParameter("tramiteIds", tramiteIds);
            }

            answer = query.getResultList();
            for (Tramite t : answer) {
                resultadoMap.put(t.getId(), t);
            }

        } catch (Exception e) {
            LOGGER.error("ERROR :: TramiteDAOBean#buscarTramitesPorListaDeIds :: " +
                e.getMessage());
        }
        return resultadoMap;
    }

    // -------------------------------------------------------------------------
    /**
     * @see
     * ITramiteDAO#countTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(FiltroDatosConsultaTramite,
     * FiltroDatosConsultaSolicitante)
     * @author javier.aponte
     * @version 1.0
     */
    @Override
    public Integer countTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante) {

        LOGGER.debug(
            "executing TramiteDAOBean#countTramiteByFiltroTramiteRadicarDerechoPeticionOTutela");

        StringBuilder query = new StringBuilder();
        Long answer;

        query.append("SELECT COUNT(DISTINCT t) " + " FROM Tramite t" +
            " JOIN t.solicitud s" + " LEFT JOIN t.predio predio" +
            " LEFT JOIN t.departamento tramDepto" +
            " LEFT JOIN t.municipio tramMuni" +
            " LEFT JOIN t.municipio.jurisdiccions ju" +
            " LEFT JOIN predio.departamento departamento" +
            " LEFT JOIN predio.municipio municipio" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.solicitanteTramites st " +
            " WHERE t.numeroRadicacion IS NOT NULL" +
            " AND t.resultadoDocumento IS NULL");

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            query.append(
                " AND (ju.estructuraOrganizacional.codigo = :tramiteTerritorial OR ju.estructuraOrganizacional.estructuraOrganizacionalCod = :tramiteTerritorial)");
        }
        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }

        // Filtro por tipo de identificación
        if (filtrosSolicitante.getTipoIdentificacion() != null &&
            !filtrosSolicitante.getTipoIdentificacion().trim().isEmpty()) {
            query.append(" AND ( st.tipoIdentificacion = :tipoIdentificacion " +
                " OR ss.tipoIdentificacion = :tipoIdentificacion ) ");
        }

        // Filtro por número de identificación
        if (filtrosSolicitante.getNumeroIdentificacion() != null &&
            !filtrosSolicitante.getNumeroIdentificacion().trim().isEmpty()) {

            query.append(" AND ( st.numeroIdentificacion = :numeroIdentificacion " +
                " OR ss.numeroIdentificacion = :numeroIdentificacion ) ");
        }

        // Filtro por el dígito de verificación
        if (filtrosSolicitante.getDigitoVerificacion() != null &&
            !filtrosSolicitante.getDigitoVerificacion().trim()
                .isEmpty()) {

            query.append(" AND ( st.digitoVerificacion = :digitoVerificacion " +
                " OR ss.digitoVerificacion = :digitoVerificacion ) ");
        }

        // Filtro por primer nombre
        if (filtrosSolicitante.getPrimerNombre() != null &&
            !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerNombre) = :primerNombre " +
                " OR UPPER(ss.primerNombre) = :primerNombre ) ");
        }

        // Filtro por segundo nombre
        if (filtrosSolicitante.getSegundoNombre() != null &&
            !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoNombre) = :segundoNombre " +
                " OR UPPER(ss.segundoNombre) = :segundoNombre ) ");
        }

        // Filtro por primer apellido
        if (filtrosSolicitante.getPrimerApellido() != null &&
            !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerApellido) = :primerApellido " +
                " OR UPPER(ss.primerApellido) = :primerApellido ) ");
        }

        // Filtro por segundo apellido
        if (filtrosSolicitante.getSegundoApellido() != null &&
            !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoApellido) = :segundoApellido " +
                " OR UPPER(ss.segundoApellido) = :segundoApellido ) ");
        }
        // Filtro por razón social
        if (filtrosSolicitante.getRazonSocial() != null &&
            !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.razonSocial) = :razonSocial " +
                " OR UPPER(ss.razonSocial) = :razonSocial ) ");
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            q.setParameter("tramiteTerritorial",
                filtroBusquedaTramite.getTerritorialId());
        }

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                "%" + filtroBusquedaTramite.getNumeroSolicitud() + "%");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                "%" + filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }

        if (filtrosSolicitante != null) {

            if (filtrosSolicitante.getTipoIdentificacion() != null &&
                !filtrosSolicitante.getTipoIdentificacion().trim()
                    .isEmpty()) {
                q.setParameter("tipoIdentificacion", filtrosSolicitante.getTipoIdentificacion());
            }
            if (filtrosSolicitante.getNumeroIdentificacion() != null &&
                !filtrosSolicitante.getNumeroIdentificacion()
                    .trim().isEmpty()) {
                q.setParameter("numeroIdentificacion", filtrosSolicitante.getNumeroIdentificacion());
            }
            if (filtrosSolicitante.getDigitoVerificacion() != null &&
                !filtrosSolicitante.getDigitoVerificacion().trim().isEmpty()) {
                q.setParameter("digitoVerificacion", filtrosSolicitante.getDigitoVerificacion());
            }
            if (filtrosSolicitante.getPrimerNombre() != null &&
                !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {
                q.setParameter("primerNombre", filtrosSolicitante.getPrimerNombre().toUpperCase());
            }
            if (filtrosSolicitante.getSegundoNombre() != null &&
                !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {
                q.setParameter("segundoNombre", filtrosSolicitante.getSegundoNombre().toUpperCase());
            }
            if (filtrosSolicitante.getPrimerApellido() != null &&
                !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {
                q.setParameter("primerApellido", filtrosSolicitante.getPrimerApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getSegundoApellido() != null &&
                !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {
                q.setParameter("segundoApellido", filtrosSolicitante.getSegundoApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getRazonSocial() != null &&
                !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {
                q.setParameter("razonSocial", filtrosSolicitante.getRazonSocial().toUpperCase());
            }
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }

        try {
            answer = (Long) q.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer.intValue();
    }

    // -------------------------------------------------------------------------
    /**
     * @see
     * ITramiteDAO#findTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(FiltroDatosConsultaTramite,
     * FiltroDatosConsultaSolicitante)
     * @author javier.aponte
     * @version 1.0
     */
    @Override
    public List<Tramite> findTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        List<Tramite> answer;

        query.append("SELECT t FROM Tramite t" +
            " JOIN t.solicitud s" + " LEFT JOIN FETCH t.predio predio" +
            " LEFT JOIN FETCH t.departamento tramDepto" +
            " LEFT JOIN FETCH t.municipio tramMuni" +
            " LEFT JOIN t.municipio.jurisdiccions ju" +
            " LEFT JOIN FETCH predio.departamento departamento" +
            " LEFT JOIN FETCH predio.municipio municipio" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.solicitanteTramites st " +
            " WHERE t.numeroRadicacion IS NOT NULL" +
            " AND t.resultadoDocumento IS NULL");

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            query.append(
                " AND (ju.estructuraOrganizacional.codigo = :tramiteTerritorial OR ju.estructuraOrganizacional.estructuraOrganizacionalCod = :tramiteTerritorial)");
        }
        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }

        // Filtro por tipo de identificación
        if (filtrosSolicitante.getTipoIdentificacion() != null &&
            !filtrosSolicitante.getTipoIdentificacion().trim().isEmpty()) {
            query.append(" AND ( st.tipoIdentificacion = :tipoIdentificacion " +
                " OR ss.tipoIdentificacion = :tipoIdentificacion ) ");
        }

        // Filtro por número de identificación
        if (filtrosSolicitante.getNumeroIdentificacion() != null &&
            !filtrosSolicitante.getNumeroIdentificacion().trim().isEmpty()) {

            query.append(" AND ( st.numeroIdentificacion = :numeroIdentificacion " +
                " OR ss.numeroIdentificacion = :numeroIdentificacion ) ");
        }

        // Filtro por el dígito de verificación
        if (filtrosSolicitante.getDigitoVerificacion() != null &&
            !filtrosSolicitante.getDigitoVerificacion().trim()
                .isEmpty()) {

            query.append(" AND ( st.digitoVerificacion = :digitoVerificacion " +
                " OR ss.digitoVerificacion = :digitoVerificacion ) ");
        }

        // Filtro por primer nombre
        if (filtrosSolicitante.getPrimerNombre() != null &&
            !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerNombre) = :primerNombre " +
                " OR UPPER(ss.primerNombre) = :primerNombre ) ");
        }

        // Filtro por segundo nombre
        if (filtrosSolicitante.getSegundoNombre() != null &&
            !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoNombre) = :segundoNombre " +
                " OR UPPER(ss.segundoNombre) = :segundoNombre ) ");
        }

        // Filtro por primer apellido
        if (filtrosSolicitante.getPrimerApellido() != null &&
            !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerApellido) = :primerApellido " +
                " OR UPPER(ss.primerApellido) = :primerApellido ) ");
        }

        // Filtro por segundo apellido
        if (filtrosSolicitante.getSegundoApellido() != null &&
            !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoApellido) = :segundoApellido " +
                " OR UPPER(ss.segundoApellido) = :segundoApellido ) ");
        }
        // Filtro por razón social
        if (filtrosSolicitante.getRazonSocial() != null &&
            !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.razonSocial) = :razonSocial " +
                " OR UPPER(ss.razonSocial) = :razonSocial ) ");
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            query.append(" ORDER BY t.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            q.setParameter("tramiteTerritorial",
                filtroBusquedaTramite.getTerritorialId());
        }

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                "%" + filtroBusquedaTramite.getNumeroSolicitud() + "%");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                "%" + filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }

        if (filtrosSolicitante != null) {

            if (filtrosSolicitante.getTipoIdentificacion() != null &&
                !filtrosSolicitante.getTipoIdentificacion().trim()
                    .isEmpty()) {
                q.setParameter("tipoIdentificacion", filtrosSolicitante.getTipoIdentificacion());
            }
            if (filtrosSolicitante.getNumeroIdentificacion() != null &&
                !filtrosSolicitante.getNumeroIdentificacion()
                    .trim().isEmpty()) {
                q.setParameter("numeroIdentificacion", filtrosSolicitante.getNumeroIdentificacion());
            }
            if (filtrosSolicitante.getDigitoVerificacion() != null &&
                !filtrosSolicitante.getDigitoVerificacion().trim().isEmpty()) {
                q.setParameter("digitoVerificacion", filtrosSolicitante.getDigitoVerificacion());
            }
            if (filtrosSolicitante.getPrimerNombre() != null &&
                !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {
                q.setParameter("primerNombre", filtrosSolicitante.getPrimerNombre().toUpperCase());
            }
            if (filtrosSolicitante.getSegundoNombre() != null &&
                !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {
                q.setParameter("segundoNombre", filtrosSolicitante.getSegundoNombre().toUpperCase());
            }
            if (filtrosSolicitante.getPrimerApellido() != null &&
                !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {
                q.setParameter("primerApellido", filtrosSolicitante.getPrimerApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getSegundoApellido() != null &&
                !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {
                q.setParameter("segundoApellido", filtrosSolicitante.getSegundoApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getRazonSocial() != null &&
                !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {
                q.setParameter("razonSocial", filtrosSolicitante.getRazonSocial().toUpperCase());
            }
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }

        try {
            answer = (List<Tramite>) super.findInRangeUsingQuery(q,
                rowStartIdxAndCount);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
     * @see ITramiteDAO#getTramiteParaEdicionGeografica(java.lang.Long)
     * @modified andres.eslava ::refs #9272
     */
    @Override
    public Tramite getTramiteParaEdicionGeografica(Long tramiteId) {

        Tramite tramiteParaEdicionGeografica = new Tramite();

        String query =
            "SELECT DISTINCT t FROM Tramite t" +
            " JOIN FETCH t.departamento " +
            " JOIN FETCH t.municipio " +
            " LEFT JOIN FETCH t.predio " +
            " LEFT JOIN FETCH t.tramitePredioEnglobes tpe" +
            " LEFT JOIN FETCH tpe.predio pe" +
            " LEFT JOIN t.tramiteRectificacions tr" +
            " WHERE t.id IN (:tramiteId) ";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);
            tramiteParaEdicionGeografica = (Tramite) q.getSingleResult();
            if (tramiteParaEdicionGeografica.getTramitePredioEnglobes() != null) {
                for (TramitePredioEnglobe tpe : tramiteParaEdicionGeografica.
                    getTramitePredioEnglobes()) {
                    tpe.getPredio().getPredioDireccions().size();
                }
            }
            if (tramiteParaEdicionGeografica.getPredio() != null && tramiteParaEdicionGeografica.
                getPredio().getUnidadConstruccions() != null) {
                for (UnidadConstruccion uc : tramiteParaEdicionGeografica.getPredio().
                    getUnidadConstruccions()) {
                    uc.getAreaConstruida();
                }
            }
            if (tramiteParaEdicionGeografica.getPredio() != null && tramiteParaEdicionGeografica.
                getPredio().getPredioDireccions() != null) {
                for (PredioDireccion pd : tramiteParaEdicionGeografica.getPredio().
                    getPredioDireccions()) {
                    pd.getDireccion();
                }
            }
            if (tramiteParaEdicionGeografica.getTramiteRectificacions() != null) {
                for (TramiteRectificacion tr : tramiteParaEdicionGeografica.
                    getTramiteRectificacions()) {
                    Hibernate.initialize(tr.getDatoRectificar());
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#getTramitesDeActGeograficas");
        }

        return tramiteParaEdicionGeografica;
    }

    /**
     * @see IPredioDAO#obtenerPrediosConTramitePormanzana(java.lang.String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Tramite> obtenerTramitesPorManzanaEnEdicion(String numeroManzana) {
        List<Tramite> resultado = new ArrayList<Tramite>();
        Query query;
        String queryString;

        queryString = "SELECT DISTINCT T.*  FROM TRAMITE T " +
            "           INNER JOIN PREDIO P ON P.ID =T.PREDIO_ID " +
            "           INNER JOIN DOCUMENTO D ON D.TRAMITE_ID = T.ID " +
            "           WHERE TIPO_DOCUMENTO_ID = :tipoDocumento " +
            "           AND P.NUMERO_PREDIAL LIKE :numeroManzana " +
            "           AND T.ESTADO NOT IN :estados";

        List<String> estados = new ArrayList<String>();
        estados.add(ETramiteEstado.ARCHIVADO.getCodigo());
        estados.add(ETramiteEstado.CANCELADO.getCodigo());
        estados.add(ETramiteEstado.FINALIZADO.getCodigo());
        estados.add(ETramiteEstado.FINALIZADO_APROBADO.getCodigo());

        try {
            query = this.entityManager.createNativeQuery(queryString, Tramite.class);
            query.setParameter("numeroManzana", numeroManzana + "%");
            query.setParameter("estados", estados);
            query.setParameter("tipoDocumento", ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());

            resultado = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los tramites de la manzana " +
                numeroManzana + ": " + ex.getMessage());
        }

        return resultado;
    }

    /**
     * @see IPredioDAO#obtenerPrediosConTramitePormanzana(java.lang.String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Tramite> obtenerTramitesPorManzanaDepuracionEnEdicion(String numeroManzana) {
        List<Tramite> resultado = new ArrayList<Tramite>();
        Query query;
        String queryString;

        queryString = "SELECT DISTINCT T.*  FROM TRAMITE T " +
            "           INNER JOIN PREDIO P ON P.ID =T.PREDIO_ID " +
            "           INNER JOIN TRAMITE_DEPURACION TD ON T.ID = TD.TRAMITE_ID " +
            "           INNER JOIN DOCUMENTO D ON D.TRAMITE_ID = T.ID " +
            "           WHERE TIPO_DOCUMENTO_ID = :tipoDocumento " +
            "           AND P.NUMERO_PREDIAL LIKE :numeroManzana " +
            "           AND T.ESTADO NOT IN :estados ";

        List<String> estados = new ArrayList<String>();
        estados.add(ETramiteEstado.ARCHIVADO.getCodigo());
        estados.add(ETramiteEstado.CANCELADO.getCodigo());
        estados.add(ETramiteEstado.FINALIZADO.getCodigo());
        estados.add(ETramiteEstado.FINALIZADO_APROBADO.getCodigo());

        try {
            query = this.entityManager.createNativeQuery(queryString, Tramite.class);
            query.setParameter("numeroManzana", numeroManzana + "%");
            query.setParameter("estados", estados);
            query.setParameter("tipoDocumento", ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());

            resultado = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los tramites de la manzana " +
                numeroManzana + ": " + ex.getMessage());
        }

        return resultado;
    }

    // --------------------------------------------------------- //
    /**
     * @see ITramiteDAO#buscarTramiteConViaGubernativaPorIdsTramites(List<Long>)
     * @author david.cifuentes
     */
    @Override
    public List<Tramite> buscarTramiteConViaGubernativaPorIdsTramites(
        List<Long> tramiteIds) {

        // Lista de los trámites que se encuentran en vía gubernativa de los
        // tramites asociados a los ids enviados como parámetro.
        List<Tramite> answer = null;
        try {
            String queryString;
            Query query;

            queryString = "SELECT tramiteViaGub FROM Tramite tramiteViaGub " +
                " JOIN FETCH tramiteViaGub.solicitud solicitudViaGub " +
                " WHERE solicitudViaGub.tramiteNumeroRadicacionRef " +
                " IN (SELECT tramite.numeroRadicacion FROM Tramite tramite " +
                " WHERE tramite.id IN ( :tramiteIds ))";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIds", tramiteIds);

            answer = (List<Tramite>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#consultarParaTramiteDevuelto(java.lang.Long)
     * @param tramiteId
     * @return
     */
    /*
     * @modified by leidy.gonzalez:: #16599:: 05/04/2016 SE agrega initialize para los tramites de
     * la solicitud y los predios asociados a los tramites
     */
    @Implement
    @Override
    public Tramite consultarParaTramiteDevuelto(Long tramiteId) {

        Tramite answer = null;

        try {
            //D: se usa este porque trae los documentos y la solicitud
            answer = this.getDocsOfTramite(tramiteId);

            Hibernate.initialize(answer.getComisionTramites());
            for (ComisionTramite ct : answer.getComisionTramites()) {
                Hibernate.initialize(ct.getComision());
            }
            if (answer.getSolicitud() != null) {
                Hibernate.initialize(answer.getSolicitud());
                if (answer.getSolicitud().getTramites() != null) {
                    Hibernate.initialize(answer.getSolicitud().getTramites());
                }

                for (Tramite tramiteTemp : answer.getSolicitud().getTramites()) {

                    if (tramiteTemp.getPredios() != null) {
                        Hibernate.initialize(tramiteTemp.getPredios());
                    }
                    if (tramiteTemp.getPredio() != null) {
                        Hibernate.initialize(tramiteTemp.getPredio());
                    }

                    if (tramiteTemp.getEstadoDocumentos() != null) {
                        Hibernate.initialize(tramiteTemp.getEstadoDocumentos());
                    }

                }

            }

        } catch (HibernateException hbEx) {
            throw SncBusinessServiceExceptions.EXCEPCION_HIBERNATE_INITIALIZE.getExcepcion(
                LOGGER, hbEx, "TramiteDAOBean#consultarParaTramiteDevuelto", "Tramite");
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarParaTramiteDevuelto");
        }
        return answer;

    }

    /**
     * @see ITramiteDAO#obtenerTramitesPorEjecutorYClasificacion(java.lang.String, java.lang.String)
     * @return
     */
    @Implement
    @Override
    public Long obtenerTramitesPorEjecutorYClasificacion(String ejecutor, String clasificacion) {

        Long answer = 0l;

        try {
            String queryString;
            Query query;

            queryString = "SELECT count(t) FROM Tramite t " +
                " WHERE t.funcionarioEjecutor = :ejecutor " +
                " AND  t.clasificacion = :clasificacion" +
                " AND t.resultadoDocumento is null" +
                " AND (t.estado not like :cancelado AND t.estado not like :finalizado)";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("ejecutor", ejecutor);
            query.setParameter("clasificacion", clasificacion);
            query.setParameter("cancelado", ETramiteEstado.CANCELADO.getCodigo());
            query.setParameter("finalizado", ETramiteEstado.FINALIZADO_APROBADO.getCodigo());

            answer = (Long) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Hace el query de los trámites que cuyos ids estén en el arreglo Según el flag isCount
     * devuelve solo el conteo de registros
     *
     * OJO: si no hay una codición adicional se puede usar WHERE 1=1 y luego concatenar el IN
     *
     * OJO: se está suponiendo que el arreglo de ids de trámites no tiene más de 1000. Si los
     * tuviera habría que usar la forma de consulta que se usa en findTramitesByIds
     *
     * @see ITramiteDAO#findTramitesByIds(java.util.List, java.lang.String, java.lang.String, int[])
     *
     * @author javier.aponte
     *
     * @param idsTramites arreglo con los id de trámite
     * @param isCount dice si lo que se quiere es contar las filas resultados
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     * @param sortOrder nombre del ordenamiento (UNSORTED, ASCENDING, DESCENDING)
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount
     * @return
     */
    private Object findTramitesDeterminarProcedenciaSolicitud(long[] idsTramites, boolean isCount,
        String sortField, String sortOrder, Map<String, String> filters,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteDAOBean#findTramitesDeterminarProcedenciaSolicitud");

        List<Tramite> answer1;
        BigInteger answer2;

        Object answer = null;
        StringBuilder queryString, idTramiteIn;
        Query query;

        queryString = new StringBuilder();
        idTramiteIn = new StringBuilder();

        idTramiteIn = this.concatenarIdsTramites(idsTramites);

        if (!isCount) {
            queryString.append("SELECT t FROM Tramite t " +
                " LEFT JOIN FETCH t.departamento d " +
                " LEFT JOIN FETCH t.municipio m " +
                " LEFT JOIN FETCH t.predio p " +
                " LEFT JOIN FETCH p.municipio LEFT JOIN FETCH p.departamento " +
                " WHERE 1 = 1 ");
        } else {
            // N: esta, para dejarla más liviana se hace de forma diferente
            queryString.append("SELECT count(t) " +
                " FROM Tramite t WHERE 1 = 1 ");

        }

        //D: revisar filtros de búsqueda.
        //N: el operador sql que se use depende del modo de emparejamiento definido en el xhtml (ej: si filterMatchMode="exact" => se usa '=')
        if (filters.containsKey("solicitud.numero")) {
            queryString.append(" AND t.solicitud.numero LIKE :numeroSolicitud ");
        }
        if (filters.containsKey("numeroRadicacion")) {
            queryString.append(" AND t.numeroRadicacion LIKE :numeroRadicacion ");
        }
        if (filters.containsKey("tipoTramite")) {
            queryString.append(
                " AND t.tipoTramite ||'-'||t.claseMutacion||'-'||t.subtipo like :tipoTramite");
        }
        if (filters.containsKey("predio.numeroPredial")) {
            queryString.append(" AND p.numeroPredial like :numeroPredial");
        }
        if (filters.containsKey("municipio.nombre")) {
            queryString.append(" AND t.municipio.nombre like :municipioNombre");
        }
        if (filters.containsKey("fechaRadicacion")) {
            queryString.
                append(" AND TO_CHAR(t.fechaRadicacion, 'dd/mm/yyyy') like :fechaRadicacion");
        }

        queryString.append(idTramiteIn);

        //D: se agrega el posible ordenamiento al query
        //OJO: se asume que si va a contar no se necesita ordenamiento
        if (!isCount) {
            if (sortField != null && !sortField.equals("") && !sortOrder.equals("UNSORTED")) {
                queryString.append(" ORDER BY t.").append(sortField).
                    append(sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());

            if (filters.containsKey("solicitud.numero")) {
                query.setParameter("numeroSolicitud", "%" + filters.get("solicitud.numero") + "%");
            }
            if (filters.containsKey("numeroRadicacion")) {
                query.setParameter("numeroRadicacion", "%" + filters.get("numeroRadicacion") + "%");
            }
            if (filters.containsKey("tipoTramite")) {
                query.setParameter("tipoTramite", filters.get("tipoTramite"));
            }
            if (filters.containsKey("predio.numeroPredial")) {
                query.setParameter("numeroPredial", "%" + filters.get("predio.numeroPredial").
                    toUpperCase() + "%");
            }
            if (filters.containsKey("municipio.nombre")) {
                query.setParameter("municipioNombre", "%" + filters.get("municipio.nombre").
                    toUpperCase() + "%");
            }
            if (filters.containsKey("fechaRadicacion")) {
                query.setParameter("fechaRadicacion", "%" + filters.get("fechaRadicacion").
                    toUpperCase() + "%");
            }

            if (!isCount) {
                answer1 = (List<Tramite>) super.findInRangeUsingQuery(query, rowStartIdxAndCount);

                for (Tramite tramite : answer1) {

                    if (tramite.getTramiteRectificacions() != null) {
                        Hibernate.initialize(tramite.getTramiteRectificacions());
                        for (TramiteRectificacion tRect : tramite.getTramiteRectificacions()) {
                            Hibernate.initialize(tRect.getDatoRectificar());
                        }
                    }

                }

                answer = answer1;
            } else {
                answer2 = new BigInteger(query.getSingleResult().toString());
                answer = answer2;
            }
        } catch (Exception e) {
            LOGGER.error("error on TramiteDAOBean#findTramitesDeterminarProcedenciaSolicitud: " + e.
                getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramitesDeterminarProcedenciaSolicitud");
        }

        return answer;
    }

    /**
     * Método encargado de concatenar los ids de los trámites, en el caso que vengan mas de 1000
     * agrega un or para que no ocurra el error de oracle que en un IN no pueden haber más de 1000
     * registros
     *
     * @author javier.aponte
     */
    private StringBuilder concatenarIdsTramites(long[] idsTramites) {

        StringBuilder idTramiteIn = new StringBuilder();

        int valorMaximoIds = idsTramites.length;

        int limite = 1000;

        if (valorMaximoIds < limite) {
            idTramiteIn.append(" AND t.id IN (");
            for (long idTramite : idsTramites) {
                idTramiteIn.append(idTramite).append(",");
            }
            idTramiteIn.deleteCharAt(idTramiteIn.lastIndexOf(","));
            idTramiteIn.append(")");
        } else {

            int particiones;

            idTramiteIn.append(" AND ( t.id IN (");

            particiones = (idsTramites.length % limite == 0) ? (idsTramites.length / limite) :
                (idsTramites.length / limite) + 1;

            for (int i = 0; i < particiones; i++) {
                for (int j = limite * i; j < Math.min(limite * (i + 1), idsTramites.length); j++) {
                    idTramiteIn.append(idsTramites[j]).append(",");
                }
                idTramiteIn.deleteCharAt(idTramiteIn.lastIndexOf(","));
                idTramiteIn.append(")");
                idTramiteIn.append(" OR t.id IN (");
            }
            idTramiteIn.delete(idTramiteIn.lastIndexOf("OR"), idTramiteIn.length());
            idTramiteIn.append(")");

        }

        return idTramiteIn;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#findTramitesParaDeterminarProcedenciaSolicitud(long[], String, String,
     * int[])
     * @author javier.aponte
     * @version 1.0
     */
    @Implement
    @Override
    public List<Tramite> findTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites,
        String sortField,
        String sortOrder, Map<String, String> filters, int... rowStartIdxAndCount) {

        List<Tramite> answer;

        try {
            answer = (List<Tramite>) this.findTramitesDeterminarProcedenciaSolicitud(idsTramites,
                false, sortField,
                sortOrder, filters, rowStartIdxAndCount);

        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#countTramitesPorComisionar(long[], java.lang.String, int[])
     * @author javier.aponte
     */
    @Implement
    @Override
    public int countTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites) {
        int answer = 0;
        BigInteger temp;

        HashMap<String, String> filters = new HashMap<String, String>();

        try {
            temp = (BigInteger) this.findTramitesDeterminarProcedenciaSolicitud(idsTramites, true,
                "",
                "UNSORTED", filters);

            answer = temp.intValue();
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }

    /**
     * Método encargado de consultar los ids de trámites con su respectiva clasificación
     *
     * @param idsTramites
     * @return Map con clave el id del trámite y el valor la clasificación del trámite
     * correspondiente
     * @author javier.aponte
     */
    public List<TramiteConEjecutorAsignadoDTO> findIdsClasificacionTramitesConEjecutor(
        long[] idsTramites) {

        LOGGER.debug("en TramiteDAOBean#findIdsClasificacionTramitesConEjecutor");

        List<Object[]> resultado;
        List<TramiteConEjecutorAsignadoDTO> answer = null;

        TramiteConEjecutorAsignadoDTO tramite;

        StringBuilder queryString, idTramiteIn;
        Query query;

        queryString = new StringBuilder();

        idTramiteIn = this.concatenarIdsTramites(idsTramites);

        queryString.append("SELECT t.id, t.clasificacion, t.funcionarioEjecutor FROM Tramite t " +
            " WHERE t.funcionarioEjecutor != null " + idTramiteIn);

        try {
            query = this.entityManager.createQuery(queryString.toString());

            resultado = query.getResultList();

            if (resultado != null && !resultado.isEmpty()) {
                answer = new ArrayList<TramiteConEjecutorAsignadoDTO>();

                for (Object[] result : resultado) {
                    tramite = new TramiteConEjecutorAsignadoDTO();
                    tramite.setId(Long.valueOf(String.valueOf(result[0])));
                    tramite.setClasificacion(String.valueOf(result[1]));
                    tramite.setFuncionarioEjecutor(String.valueOf(result[2]));
                    answer.add(tramite);
                }
            }

        } catch (Exception e) {
            LOGGER.error("error on TramiteDAOBean#findIdsClasificacionTramitesConEjecutor: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findIdsClasificacionTramitesConEjecutor");
        }

        return answer;
    }

    @Override
    public List<Tramite> bucarTramiteByFiltroTramiteRecuperacionTamite(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        List<Tramite> answer;

        query.append("SELECT t FROM Tramite t" +
            " JOIN t.solicitud s" + " LEFT JOIN FETCH t.predio predio" +
            " LEFT JOIN FETCH t.departamento tramDepto" +
            " LEFT JOIN FETCH t.municipio tramMuni" +
            " LEFT JOIN t.municipio.jurisdiccions ju" +
            " LEFT JOIN FETCH predio.departamento departamento" +
            " LEFT JOIN FETCH predio.municipio municipio" +
            " LEFT JOIN FETCH t.resultadoDocumento doc" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.solicitanteTramites st " +
            " WHERE t.estado not like 'CANCELADO' ");

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            query.append(
                " AND (ju.estructuraOrganizacional.codigo = :tramiteTerritorial OR ju.estructuraOrganizacional.estructuraOrganizacionalCod = :tramiteTerritorial)");
        }
        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor().isEmpty()) {
            query.append(" AND t.funcionarioEjecutor LIKE :funcionarioEjecutor");
        }

        // Filtro por tipo de identificación
        if (filtrosSolicitante.getTipoIdentificacion() != null &&
            !filtrosSolicitante.getTipoIdentificacion().trim().isEmpty()) {
            query.append(" AND ( st.tipoIdentificacion = :tipoIdentificacion " +
                " OR ss.tipoIdentificacion = :tipoIdentificacion ) ");
        }

        // Filtro por número de identificación
        if (filtrosSolicitante.getNumeroIdentificacion() != null &&
            !filtrosSolicitante.getNumeroIdentificacion().trim().isEmpty()) {

            query.append(" AND ( st.numeroIdentificacion = :numeroIdentificacion " +
                " OR ss.numeroIdentificacion = :numeroIdentificacion ) ");
        }

        // Filtro por el dígito de verificación
        if (filtrosSolicitante.getDigitoVerificacion() != null &&
            !filtrosSolicitante.getDigitoVerificacion().trim()
                .isEmpty()) {

            query.append(" AND ( st.digitoVerificacion = :digitoVerificacion " +
                " OR ss.digitoVerificacion = :digitoVerificacion ) ");
        }

        // Filtro por primer nombre
        if (filtrosSolicitante.getPrimerNombre() != null &&
            !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerNombre) = :primerNombre " +
                " OR UPPER(ss.primerNombre) = :primerNombre ) ");
        }

        // Filtro por segundo nombre
        if (filtrosSolicitante.getSegundoNombre() != null &&
            !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoNombre) = :segundoNombre " +
                " OR UPPER(ss.segundoNombre) = :segundoNombre ) ");
        }

        // Filtro por primer apellido
        if (filtrosSolicitante.getPrimerApellido() != null &&
            !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerApellido) = :primerApellido " +
                " OR UPPER(ss.primerApellido) = :primerApellido ) ");
        }

        // Filtro por segundo apellido
        if (filtrosSolicitante.getSegundoApellido() != null &&
            !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoApellido) = :segundoApellido " +
                " OR UPPER(ss.segundoApellido) = :segundoApellido ) ");
        }
        // Filtro por razón social
        if (filtrosSolicitante.getRazonSocial() != null &&
            !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.razonSocial) = :razonSocial " +
                " OR UPPER(ss.razonSocial) = :razonSocial ) ");
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }
        //Filtros para recuperacion de tramites
        if (filtroBusquedaTramite.getTarea() != null &&
            !filtroBusquedaTramite.getTarea().isEmpty()) {
            query.append(" AND t.tarea = :tarea");
        }
        if (filtroBusquedaTramite.getPaso() != null &&
            !filtroBusquedaTramite.getPaso().isEmpty()) {
            query.append(" AND t.paso = :paso");
        }
        if (filtroBusquedaTramite.getResultado() != null &&
            !filtroBusquedaTramite.getResultado().isEmpty()) {
            query.append(" AND t.resultado like :resultado");
        }

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            query.append(" ORDER BY t.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            q.setParameter("tramiteTerritorial",
                filtroBusquedaTramite.getTerritorialId());
        }

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                "%" + filtroBusquedaTramite.getNumeroSolicitud() + "%");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                "%" + filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor().isEmpty()) {
            q.setParameter("funcionarioEjecutor",
                "%" + filtroBusquedaTramite.getFuncionarioEjecutor() + "%");
        }

        if (filtrosSolicitante != null) {

            if (filtrosSolicitante.getTipoIdentificacion() != null &&
                !filtrosSolicitante.getTipoIdentificacion().trim()
                    .isEmpty()) {
                q.setParameter("tipoIdentificacion", filtrosSolicitante.getTipoIdentificacion());
            }
            if (filtrosSolicitante.getNumeroIdentificacion() != null &&
                !filtrosSolicitante.getNumeroIdentificacion()
                    .trim().isEmpty()) {
                q.setParameter("numeroIdentificacion", filtrosSolicitante.getNumeroIdentificacion());
            }
            if (filtrosSolicitante.getDigitoVerificacion() != null &&
                !filtrosSolicitante.getDigitoVerificacion().trim().isEmpty()) {
                q.setParameter("digitoVerificacion", filtrosSolicitante.getDigitoVerificacion());
            }
            if (filtrosSolicitante.getPrimerNombre() != null &&
                !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {
                q.setParameter("primerNombre", filtrosSolicitante.getPrimerNombre().toUpperCase());
            }
            if (filtrosSolicitante.getSegundoNombre() != null &&
                !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {
                q.setParameter("segundoNombre", filtrosSolicitante.getSegundoNombre().toUpperCase());
            }
            if (filtrosSolicitante.getPrimerApellido() != null &&
                !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {
                q.setParameter("primerApellido", filtrosSolicitante.getPrimerApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getSegundoApellido() != null &&
                !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {
                q.setParameter("segundoApellido", filtrosSolicitante.getSegundoApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getRazonSocial() != null &&
                !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {
                q.setParameter("razonSocial", filtrosSolicitante.getRazonSocial().toUpperCase());
            }
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }
        //Filtros para recuperacion de tramites
        if (filtroBusquedaTramite.getTarea() != null &&
            !filtroBusquedaTramite.getTarea().isEmpty()) {
            q.setParameter("tarea", filtroBusquedaTramite.getTarea());
        }
        if (filtroBusquedaTramite.getPaso() != null &&
            !filtroBusquedaTramite.getPaso().isEmpty()) {
            q.setParameter("paso", filtroBusquedaTramite.getPaso());
        }
        if (filtroBusquedaTramite.getResultado() != null &&
            !filtroBusquedaTramite.getResultado().isEmpty()) {
            q.setParameter("resultado", filtroBusquedaTramite.getResultado());
        }

        try {
            answer = (List<Tramite>) super.findInRangeUsingQuery(q,
                rowStartIdxAndCount);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
     * @see ITramiteDAO.bucarTramiteByFiltroTramiteAdminAsignaciones
     * @author felipe.cadena
     *
     * */
    @Override
    public List<Tramite> buscarTramiteByFiltroTramiteAdminAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("executing TramiteDAOBean#findTramiteSolicitudByFiltroTramite");

        StringBuilder query = new StringBuilder();
        List<Tramite> answer;

        query.append("SELECT t FROM Tramite t" +
            " JOIN t.solicitud s" + " LEFT JOIN FETCH t.predio predio" +
            " LEFT JOIN FETCH t.departamento tramDepto" +
            " LEFT JOIN FETCH t.municipio tramMuni" +
            " LEFT JOIN t.municipio.jurisdiccions ju" +
            " LEFT JOIN FETCH predio.departamento departamento" +
            " LEFT JOIN FETCH predio.municipio municipio" +
            " LEFT JOIN FETCH t.resultadoDocumento doc" +
            " LEFT JOIN s.solicitanteSolicituds ss" +
            " LEFT JOIN t.solicitanteTramites st " +
            " WHERE t.estado not like 'CANCELADO' ");

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            query.append(
                " AND (ju.estructuraOrganizacional.codigo = :tramiteTerritorial OR ju.estructuraOrganizacional.estructuraOrganizacionalCod = :tramiteTerritorial)");
        }
        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            query.append(" AND tramDepto.codigo = :tramiteDepartamento");
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            query.append(" AND tramMuni.codigo = :tramiteMunicipio");
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroSolicitud");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            query.append(" AND t.numeroRadicacion LIKE :numeroRadicacion");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor().isEmpty()) {
            query.append(" AND t.funcionarioEjecutor LIKE :funcionarioEjecutor");
        }

        // Filtro por tipo de identificación
        if (filtrosSolicitante.getTipoIdentificacion() != null &&
            !filtrosSolicitante.getTipoIdentificacion().trim().isEmpty()) {
            query.append(" AND ( st.tipoIdentificacion = :tipoIdentificacion " +
                " OR ss.tipoIdentificacion = :tipoIdentificacion ) ");
        }

        // Filtro por número de identificación
        if (filtrosSolicitante.getNumeroIdentificacion() != null &&
            !filtrosSolicitante.getNumeroIdentificacion().trim().isEmpty()) {

            query.append(" AND ( st.numeroIdentificacion = :numeroIdentificacion " +
                " OR ss.numeroIdentificacion = :numeroIdentificacion ) ");
        }

        // Filtro por el dígito de verificación
        if (filtrosSolicitante.getDigitoVerificacion() != null &&
            !filtrosSolicitante.getDigitoVerificacion().trim()
                .isEmpty()) {

            query.append(" AND ( st.digitoVerificacion = :digitoVerificacion " +
                " OR ss.digitoVerificacion = :digitoVerificacion ) ");
        }

        // Filtro por primer nombre
        if (filtrosSolicitante.getPrimerNombre() != null &&
            !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerNombre) = :primerNombre " +
                " OR UPPER(ss.primerNombre) = :primerNombre ) ");
        }

        // Filtro por segundo nombre
        if (filtrosSolicitante.getSegundoNombre() != null &&
            !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoNombre) = :segundoNombre " +
                " OR UPPER(ss.segundoNombre) = :segundoNombre ) ");
        }

        // Filtro por primer apellido
        if (filtrosSolicitante.getPrimerApellido() != null &&
            !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.primerApellido) = :primerApellido " +
                " OR UPPER(ss.primerApellido) = :primerApellido ) ");
        }

        // Filtro por segundo apellido
        if (filtrosSolicitante.getSegundoApellido() != null &&
            !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.segundoApellido) = :segundoApellido " +
                " OR UPPER(ss.segundoApellido) = :segundoApellido ) ");
        }
        // Filtro por razón social
        if (filtrosSolicitante.getRazonSocial() != null &&
            !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {

            query.append(" AND ( UPPER(st.razonSocial) = :razonSocial " +
                " OR UPPER(ss.razonSocial) = :razonSocial ) ");
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            query.append(" AND departamento.codigo like :departamentoCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            query.append(" AND municipio.codigo like :municipioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            query.append(" AND predio.tipoAvaluo = :tipoAvaluo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            query.append(" AND predio.sectorCodigo = :sectorCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :comunaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            query.append(" AND predio.barrioCodigo LIKE :barrioCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            query.append(" AND predio.manzanaCodigo = :manzanaCodigo");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            query.append(" AND predio.predio = :predio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            query.append(" AND predio.condicionPropiedad = :condicionPropiedad");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            query.append(" AND predio.edificio = :edificio");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            query.append(" AND predio.piso = :piso");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            query.append(" AND predio.unidad = :unidad");
        }
        //Filtros para recuperacion de tramites
        if (filtroBusquedaTramite.getTarea() != null &&
            !filtroBusquedaTramite.getTarea().isEmpty()) {
            query.append(" AND t.tarea = :tarea");
        }
        if (filtroBusquedaTramite.getPaso() != null &&
            !filtroBusquedaTramite.getPaso().isEmpty()) {
            query.append(" AND t.paso = :paso");
        }
        if (filtroBusquedaTramite.getResultado() != null &&
            !filtroBusquedaTramite.getResultado().isEmpty()) {
            query.append(" AND t.resultado like :resultado");
        }
        //filtro tipo tramite
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            query.append(" AND t.tipoTramite like :tipoTramite");
        }
        //filtro clase mutacion
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            query.append(" AND t.claseMutacion like :claseMutacion");
        }
        //filtro subtipo
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            query.append(" AND t.subtipo like :subtipo");
        }
        //filtro clasificacion
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion().isEmpty()) {
            query.append(" AND t.clasificacion like :clasificacion");
        }

        //Filtro por fecha de radicacion :: felipe.cadena
        if (filtroBusquedaTramite.getFechaRadicacion() != null && filtroBusquedaTramite.
            getFechaRadicacionFinal() != null) {
            LOGGER.debug("Debe filtrar por fechaRadicacion");
            LOGGER.debug(filtroBusquedaTramite.getFechaRadicacion().toString());
            LOGGER.debug(" to " + filtroBusquedaTramite.getFechaRadicacionFinal().toString());

            query.append(" AND t.fechaRadicacion between :frInicial and :frFinal");
        }

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            query.append(" ORDER BY t.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroBusquedaTramite.getTerritorialId() != null &&
            !filtroBusquedaTramite.getTerritorialId().isEmpty()) {
            q.setParameter("tramiteTerritorial",
                filtroBusquedaTramite.getTerritorialId());
        }

        if (filtroBusquedaTramite.getDepartamentoId() != null &&
            !filtroBusquedaTramite.getDepartamentoId().isEmpty()) {
            q.setParameter("tramiteDepartamento",
                filtroBusquedaTramite.getDepartamentoId());
        }
        if (filtroBusquedaTramite.getMunicipioId() != null &&
            !filtroBusquedaTramite.getMunicipioId().isEmpty()) {
            q.setParameter("tramiteMunicipio",
                filtroBusquedaTramite.getMunicipioId());
        }
        if (filtroBusquedaTramite.getNumeroSolicitud() != null &&
            !filtroBusquedaTramite.getNumeroSolicitud().isEmpty()) {
            q.setParameter("numeroSolicitud",
                "%" + filtroBusquedaTramite.getNumeroSolicitud() + "%");
        }
        if (filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !filtroBusquedaTramite.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion",
                "%" + filtroBusquedaTramite.getNumeroRadicacion() + "%");
        }
        if (filtroBusquedaTramite.getFuncionarioEjecutor() != null &&
            !filtroBusquedaTramite.getFuncionarioEjecutor().isEmpty()) {
            q.setParameter("funcionarioEjecutor",
                "%" + filtroBusquedaTramite.getFuncionarioEjecutor() + "%");
        }

        if (filtrosSolicitante != null) {

            if (filtrosSolicitante.getTipoIdentificacion() != null &&
                !filtrosSolicitante.getTipoIdentificacion().trim()
                    .isEmpty()) {
                q.setParameter("tipoIdentificacion", filtrosSolicitante.getTipoIdentificacion());
            }
            if (filtrosSolicitante.getNumeroIdentificacion() != null &&
                !filtrosSolicitante.getNumeroIdentificacion()
                    .trim().isEmpty()) {
                q.setParameter("numeroIdentificacion", filtrosSolicitante.getNumeroIdentificacion());
            }
            if (filtrosSolicitante.getDigitoVerificacion() != null &&
                !filtrosSolicitante.getDigitoVerificacion().trim().isEmpty()) {
                q.setParameter("digitoVerificacion", filtrosSolicitante.getDigitoVerificacion());
            }
            if (filtrosSolicitante.getPrimerNombre() != null &&
                !filtrosSolicitante.getPrimerNombre().trim().isEmpty()) {
                q.setParameter("primerNombre", filtrosSolicitante.getPrimerNombre().toUpperCase());
            }
            if (filtrosSolicitante.getSegundoNombre() != null &&
                !filtrosSolicitante.getSegundoNombre().trim().isEmpty()) {
                q.setParameter("segundoNombre", filtrosSolicitante.getSegundoNombre().toUpperCase());
            }
            if (filtrosSolicitante.getPrimerApellido() != null &&
                !filtrosSolicitante.getPrimerApellido().trim().isEmpty()) {
                q.setParameter("primerApellido", filtrosSolicitante.getPrimerApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getSegundoApellido() != null &&
                !filtrosSolicitante.getSegundoApellido().trim().isEmpty()) {
                q.setParameter("segundoApellido", filtrosSolicitante.getSegundoApellido().
                    toUpperCase());
            }
            if (filtrosSolicitante.getRazonSocial() != null &&
                !filtrosSolicitante.getRazonSocial().trim().isEmpty()) {
                q.setParameter("razonSocial", filtrosSolicitante.getRazonSocial().toUpperCase());
            }
        }

        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getMunicipioCodigo().isEmpty()) {
            q.setParameter("municipioCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getDepartamentoCodigo() +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getMunicipioCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtroBusquedaTramite
                .getNumeroPredialPartes().getTipoAvaluo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getSectorCodigo().isEmpty()) {
            q.setParameter("sectorCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getSectorCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getComunaCodigo().isEmpty()) {
            q.setParameter("comunaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getComunaCodigo() + "%");
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getBarrioCodigo().isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroBusquedaTramite.getNumeroPredialPartes()
                    .getBarrioCodigo());

        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getManzanaVeredaCodigo().isEmpty()) {
            q.setParameter("manzanaCodigo", filtroBusquedaTramite
                .getNumeroPredialPartes().getManzanaVeredaCodigo());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPredio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPredio()
                .isEmpty()) {
            q.setParameter("predio", filtroBusquedaTramite
                .getNumeroPredialPartes().getPredio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedad", filtroBusquedaTramite
                .getNumeroPredialPartes().getCondicionPropiedad());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes()
                .getEdificio().isEmpty()) {
            q.setParameter("edificio", filtroBusquedaTramite
                .getNumeroPredialPartes().getEdificio());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getPiso() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getPiso()
                .isEmpty()) {
            q.setParameter("piso", filtroBusquedaTramite
                .getNumeroPredialPartes().getPiso());
        }
        if (filtroBusquedaTramite.getNumeroPredialPartes() != null &&
            filtroBusquedaTramite.getNumeroPredialPartes().getUnidad() != null &&
            !filtroBusquedaTramite.getNumeroPredialPartes().getUnidad()
                .isEmpty()) {
            q.setParameter("unidad", filtroBusquedaTramite
                .getNumeroPredialPartes().getUnidad());
        }
        //Filtros para recuperacion de tramites
        if (filtroBusquedaTramite.getTarea() != null &&
            !filtroBusquedaTramite.getTarea().isEmpty()) {
            q.setParameter("tarea", filtroBusquedaTramite.getTarea());
        }
        if (filtroBusquedaTramite.getPaso() != null &&
            !filtroBusquedaTramite.getPaso().isEmpty()) {
            q.setParameter("paso", filtroBusquedaTramite.getPaso());
        }
        if (filtroBusquedaTramite.getResultado() != null &&
            !filtroBusquedaTramite.getResultado().isEmpty()) {
            q.setParameter("resultado", filtroBusquedaTramite.getResultado());
        }

        //filtro tipo tramite
        if (filtroBusquedaTramite.getTipoTramite() != null &&
            !filtroBusquedaTramite.getTipoTramite().isEmpty()) {
            q.setParameter("tipoTramite", filtroBusquedaTramite.getTipoTramite());
        }
        //filtro clase mutacion
        if (filtroBusquedaTramite.getClaseMutacion() != null &&
            !filtroBusquedaTramite.getClaseMutacion().isEmpty()) {
            q.setParameter("claseMutacion", filtroBusquedaTramite.getClaseMutacion());
        }
        //filtro subtipo
        if (filtroBusquedaTramite.getSubtipo() != null &&
            !filtroBusquedaTramite.getSubtipo().isEmpty()) {
            q.setParameter("subtipo", filtroBusquedaTramite.getSubtipo());
        }
        //filtro clasificacion
        if (filtroBusquedaTramite.getClasificacion() != null &&
            !filtroBusquedaTramite.getClasificacion().isEmpty()) {
            q.setParameter("clasificacion", filtroBusquedaTramite.getClasificacion());
        }

        //Parametros filtro por fecha de radicacion :: felipe.cadena
        if (filtroBusquedaTramite.getFechaRadicacion() != null && filtroBusquedaTramite.
            getFechaRadicacionFinal() != null) {
            LOGGER.debug("Debe filtrar por fechaRadicacion");
            LOGGER.debug(filtroBusquedaTramite.getFechaRadicacion().toString());
            LOGGER.debug(" to " + filtroBusquedaTramite.getFechaRadicacionFinal().toString());

            q.setParameter("frInicial", filtroBusquedaTramite.getFechaRadicacion());
            q.setParameter("frFinal", filtroBusquedaTramite.getFechaRadicacionFinal());

        }

        try {
            answer = (List<Tramite>) super.findInRangeUsingQuery(q,
                rowStartIdxAndCount);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    /**
     *
     * @author leidy.gonzalez
     * @see ITramite#buscarTramitesActualizacionAgrupadoPorPredio(long[])
     */
    @Override
    public List<Tramite> buscarTramitesActualizacionAgrupadoPorPredio(long[] tramitesIds) {

        if (tramitesIds == null || tramitesIds.length == 0) {
            return null;
        }

        int intervalSize = 500;
        ArrayList<List<Long>> splitIds = null;

        ArrayList<Long> idsTramites = new ArrayList<Long>();

        List<Tramite> resultado = new ArrayList<Tramite>();
        Query query;
        String queryString;
        Long[] idTramitesLong = ArrayUtils.toObject(tramitesIds);
        List<Long> idTramites = Arrays.asList(idTramitesLong);

        queryString = "";

        if (tramitesIds.length < intervalSize) {

            queryString = "WITH TRAMITE_RESOLUCION AS" +
                " (SELECT T.* FROM TRAMITE T WHERE ID IN (:idTramites)" +
                " )" +
                " SELECT TT.* from tramite tt where id in (" +
                "    SELECT DISTINCT MIN(T.ID) OVER (PARTITION BY NUMERO_PREDIAL ORDER BY NUMERO_PREDIAL) FROM TRAMITE_RESOLUCION T" +
                " ) AND" +
                " TT.PROCESO = 'ACTUALIZACION' AND" +
                " TT.ESTADO NOT IN ('FINALIZADO_APROBADO', 'CANCELADO', 'RECHAZADO')";

        } else {

            for (long id : tramitesIds) {
                idsTramites.add(id);
            }

            int idxStart = 0;
            int idxFinal = intervalSize;

            int intervalsNum = idsTramites.size() / intervalSize;

            if ((idsTramites.size() % intervalSize) != 0) {
                intervalsNum++;
            }

            splitIds = new ArrayList<List<Long>>();

            for (int i = 1; i <= intervalsNum; i++) {
                if (idxFinal < idsTramites.size()) {
                    splitIds.add(idsTramites.subList(idxStart, idxFinal));
                } else {
                    splitIds.add(idsTramites.subList(idxStart, idsTramites.size()));
                }
                idxStart = idxFinal;
                idxFinal = idxFinal + intervalSize;

            }
            queryString = "WITH TRAMITE_RESOLUCION AS" +
                " (SELECT T.* FROM TRAMITE T WHERE (";

            for (int i = 0; i < intervalsNum; i++) {
                queryString += " ID IN (:tramite" + i + "s)";

                if (i != (intervalsNum - 1)) {
                    queryString += " OR ";
                } else {
                    queryString += ")";
                }
            }

            queryString += ") SELECT TT.* from tramite tt where id in (" +
                "    SELECT DISTINCT MIN(T.ID) OVER (PARTITION BY NUMERO_PREDIAL ORDER BY NUMERO_PREDIAL) FROM TRAMITE_RESOLUCION T" +
                " ) AND" +
                " TT.PROCESO = 'ACTUALIZACION' AND" +
                " TT.ESTADO NOT IN ('FINALIZADO_APROBADO', 'CANCELADO', 'RECHAZADO')";

        }

        try {
            query = this.entityManager.createNativeQuery(queryString, Tramite.class);

            if (idsTramites.size() < intervalSize) {
                query.setParameter("idTramites", idTramites);
            } else {
                int k = 0;
                for (List<Long> idsInterval : splitIds) {

                    query.setParameter("tramite" + k++ + "s", idsInterval);

                }
            }

            resultado = query.getResultList();

            for (Tramite tramite : resultado) {
                if (tramite.getTramite() != null) {
                    Hibernate.initialize(tramite.getTramite());
                    if (tramite.getTramite().getTramites() != null) {
                        Hibernate.initialize(tramite.getTramite().getTramites());
                    }
                }
                if (tramite.getTramites() != null) {
                    Hibernate.initialize(tramite.getTramites());
                }
                if (tramite.getTramiteRectificacions() != null) {
                    Hibernate.initialize(tramite.getTramiteRectificacions());
                }
                if (tramite.getSolicitanteTramites() != null) {
                    Hibernate.initialize(tramite.getSolicitanteTramites());
                }
                if (tramite.getSolicitud() != null) {
                    Hibernate.initialize(tramite.getSolicitud());
                    if (tramite.getSolicitud().getSolicitanteSolicituds() != null) {
                        Hibernate.initialize(tramite.getSolicitud()
                            .getSolicitanteSolicituds());
                    }
                }
                if (tramite.getTramitePredioEnglobes() != null) {
                    Hibernate.initialize(tramite.getTramitePredioEnglobes());
                }
                if (tramite.getPredios() != null) {
                    Hibernate.initialize(tramite.getPredios());
                }
                if (tramite.getTramiteDocumentos() != null) {
                    Hibernate.initialize(tramite.getTramiteDocumentos());
                }
                if (tramite.getComisionTramites() != null) {
                    Hibernate.initialize(tramite.getComisionTramites());
                }
                if (tramite.getTramiteInconsistencias() != null) {
                    Hibernate.initialize(tramite.getTramiteInconsistencias());
                }

                if (tramite.getPredio() != null) {
                    Hibernate.initialize(tramite.getPredio());
                    if (tramite.getPredio().getPredioDireccions() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getPredioDireccions());
                    }
                    if (tramite.getPredio().getDireccionPrincipal() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getDireccionPrincipal());
                    }
                    if (tramite.getPredio().getReferenciaCartograficas() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getReferenciaCartograficas());
                    }
                    if (tramite.getPredio().getNumeroPredial() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getNumeroPredial());
                    }
                }

                if (tramite.getComisionTramite() != null) {
                    if (tramite.getComisionTramite().getComisionTramiteDatos() != null) {
                        Hibernate.initialize(tramite.getComisionTramite()
                            .getComisionTramiteDatos());
                    }
                    if (tramite.getComisionTramite().getComision() != null) {
                        Hibernate.initialize(tramite.getComisionTramite()
                            .getComision());
                    }
                }
                if (tramite.getComisionTramite() != null &&
                    tramite.getComisionTramite().getComision() != null) {
                    Hibernate.initialize(tramite.getComisionTramite().getComision()
                        .getEstado());
                }
                if (tramite.getTramitePredioEnglobes() != null) {
                    for (TramitePredioEnglobe tpe : tramite
                        .getTramitePredioEnglobes()) {
                        if (tpe.getPredio() != null) {
                            Hibernate.initialize(tpe.getPredio());
                        }
                        if (tpe.getPredio() != null &&
                            tpe.getPredio().getPredioDireccions() != null) {
                            Hibernate.initialize(tpe.getPredio()
                                .getPredioDireccions());
                        }
                        if (tpe.getPredio() != null &&
                            tpe.getPredio().getDireccionPrincipal() != null) {
                            Hibernate.initialize(tpe.getPredio()
                                .getDireccionPrincipal());
                        }

                        if (tpe.getPredio() != null &&
                            tpe.getPredio().getReferenciaCartograficas() != null) {
                            Hibernate.initialize(tpe.getPredio()
                                .getReferenciaCartograficas());
                        }
                    }
                }

                if (tramite.getDepartamento() != null) {
                    Hibernate.initialize(tramite.getDepartamento());

                    if (tramite.getDepartamento().getCodigo() != null) {
                        Hibernate.initialize(tramite.getDepartamento().getCodigo());
                    }

                    if (tramite.getDepartamento().getNombre() != null) {
                        Hibernate.initialize(tramite.getDepartamento().getNombre());
                    }
                }

                if (tramite.getMunicipio() != null) {
                    Hibernate.initialize(tramite.getMunicipio());
                    if (tramite.getMunicipio().getNombre() != null) {
                        Hibernate.initialize(tramite.getMunicipio().getNombre());
                    }
                    if (tramite.getMunicipio().getCodigo() != null) {
                        Hibernate.initialize(tramite.getMunicipio().getCodigo());
                    }
                }

                if (tramite.getTramiteTextoResolucions() != null) {
                    Hibernate.initialize(tramite.getTramiteTextoResolucions());
                }

                if (tramite.getResultadoDocumento() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento());

                    if (tramite.getResultadoDocumento().getNumeroDocumento() != null) {
                        Hibernate.initialize(tramite.getResultadoDocumento().getNumeroDocumento());
                    }

                    if (tramite.getResultadoDocumento().getFechaDocumento() != null) {
                        Hibernate.initialize(tramite.getResultadoDocumento().getFechaDocumento());
                    }
                }

            }
        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios ordenados para los tramites" +
                tramitesIds + ": " + ex.getMessage());
            ex.printStackTrace();
        }

        return resultado;
    }

    /**
     *
     * @author felipe.cadena
     * @see ITramite#buscarTramitesEnActualizacion(long[])
     */
    @Override
    public List<Tramite> buscarTramitesEnActualizacion(List<Long> prediosIds) {

        List<Tramite> resultado = null;

        Query query;
        String queryString;

        queryString = "SELECT T.* FROM TRAMITE T" +
            " WHERE T.PREDIO_ID IN :idsPredios" +
            " AND T.PROCESO = :proceso" +
            " AND T.ESTADO NOT IN :estados ";

        List<String> estados = new ArrayList<String>();
        estados.add(ETramiteEstado.ARCHIVADO.getCodigo());
        estados.add(ETramiteEstado.CANCELADO.getCodigo());
        estados.add(ETramiteEstado.FINALIZADO.getCodigo());
        estados.add(ETramiteEstado.FINALIZADO_APROBADO.getCodigo());

        try {
            query = this.entityManager.createNativeQuery(queryString, Tramite.class);

            query.setParameter("idsPredios", prediosIds);
            query.setParameter("proceso", ETramiteProceso.ACTUALIZACION.getCodigo());
            query.setParameter("estados", estados);

            resultado = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los tramites de actualizacion : " + ex.getMessage());
        }

        return resultado;

    }

    /**
     *
     * @author leidy.gonzalez
     * @see ITramite#buscarTramitesConservacionPorIdTramites(long[])
     */
    @Override
    public List<Tramite> buscarTramitesConservacionPorIdTramites(
        List<Long> tramiteIds) {
        String sql = null;
        Query q = null;
        int intervalSize = 500;
        ArrayList<List<Long>> splitIds = null;

        if (tramiteIds.size() < intervalSize) {
            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.resultadoDocumento rd " +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH t.tramitePruebas" +
                " WHERE t.id IN (:tramiteIds)" +
                " AND (t.proceso <> 'ACTUALIZACION' OR t.proceso IS NULL)" +
                " AND t.estado NOT IN ('FINALIZADO_APROBADO', 'CANCELADO', 'RECHAZADO')";
        } else {

            int idxStart = 0;
            int idxFinal = intervalSize;

            int intervalsNum = tramiteIds.size() / intervalSize;

            if ((tramiteIds.size() % intervalSize) != 0) {
                intervalsNum++;
            }

            splitIds = new ArrayList<List<Long>>();

            for (int i = 1; i <= intervalsNum; i++) {
                if (idxFinal < tramiteIds.size()) {
                    splitIds.add(tramiteIds.subList(idxStart, idxFinal));
                } else {
                    splitIds.add(tramiteIds.subList(idxStart, tramiteIds.size()));
                }
                idxStart = idxFinal;
                idxFinal = idxFinal + intervalSize;

            }

            sql = "SELECT DISTINCT t FROM Tramite t" +
                " LEFT JOIN FETCH t.predio p" +
                " LEFT JOIN FETCH t.resultadoDocumento rd " +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio " +
                " LEFT JOIN FETCH t.tramiteDocumentacions td LEFT JOIN FETCH td.documentoSoporte" +
                " LEFT JOIN FETCH td.tipoDocumento" +
                " LEFT JOIN FETCH t.tramitePruebas" +
                " WHERE t.estado NOT IN ('FINALIZADO_APROBADO', 'CANCELADO', 'RECHAZADO')" +
                " AND (t.proceso <> 'ACTUALIZACION' OR t.proceso IS NULL) " +
                "  AND (";

            for (int i = 0; i < intervalsNum; i++) {
                sql += " t.id IN (:tramite" + i + "s)";

                if (i != (intervalsNum - 1)) {
                    sql += " OR ";
                } else {
                    sql += ")";
                }
            }

        }

        try {

            q = this.entityManager.createQuery(sql);

            if (tramiteIds.size() < intervalSize) {

                if (tramiteIds == null || tramiteIds.isEmpty()) {
                    ArrayList<Long> vacio = new ArrayList<Long>();
                    vacio.add(0L);
                    q.setParameter("tramiteIds", vacio);
                } else {
                    q.setParameter("tramiteIds", tramiteIds);
                }

            } else {
                int k = 0;
                for (List<Long> idsInterval : splitIds) {

                    q.setParameter("tramite" + k++ + "s", idsInterval);

                }
            }

            List<Tramite> tramites = q.getResultList();
            for (Tramite tramite : tramites) {
                if (tramite.getTramite() != null) {
                    Hibernate.initialize(tramite.getTramite());
                    if (tramite.getTramite().getTramites() != null) {
                        Hibernate.initialize(tramite.getTramite().getTramites());
                    }
                }
                if (tramite.getTramites() != null) {
                    Hibernate.initialize(tramite.getTramites());
                }
                if (tramite.getTramiteRectificacions() != null) {
                    Hibernate.initialize(tramite.getTramiteRectificacions());
                }
                if (tramite.getSolicitanteTramites() != null) {
                    Hibernate.initialize(tramite.getSolicitanteTramites());
                }
                if (tramite.getSolicitud() != null) {
                    Hibernate.initialize(tramite.getSolicitud());
                    if (tramite.getSolicitud().getSolicitanteSolicituds() != null) {
                        Hibernate.initialize(tramite.getSolicitud()
                            .getSolicitanteSolicituds());
                    }
                }
                if (tramite.getTramitePredioEnglobes() != null) {
                    Hibernate.initialize(tramite.getTramitePredioEnglobes());
                }
                if (tramite.getPredios() != null) {
                    Hibernate.initialize(tramite.getPredios());
                }
                if (tramite.getTramiteDocumentos() != null) {
                    Hibernate.initialize(tramite.getTramiteDocumentos());
                }
                if (tramite.getComisionTramites() != null) {
                    Hibernate.initialize(tramite.getComisionTramites());
                }
                if (tramite.getTramiteInconsistencias() != null) {
                    Hibernate.initialize(tramite.getTramiteInconsistencias());
                }

                if (tramite.getPredio() != null) {
                    Hibernate.initialize(tramite.getPredio());
                    if (tramite.getPredio().getPredioDireccions() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getPredioDireccions());
                    }
                    if (tramite.getPredio().getDireccionPrincipal() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getDireccionPrincipal());
                    }
                    if (tramite.getPredio().getReferenciaCartograficas() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getReferenciaCartograficas());
                    }
                    if (tramite.getPredio().getNumeroPredial() != null) {
                        Hibernate.initialize(tramite.getPredio()
                            .getNumeroPredial());
                    }
                }

                if (tramite.getComisionTramite() != null) {
                    if (tramite.getComisionTramite().getComisionTramiteDatos() != null) {
                        Hibernate.initialize(tramite.getComisionTramite()
                            .getComisionTramiteDatos());
                    }
                    if (tramite.getComisionTramite().getComision() != null) {
                        Hibernate.initialize(tramite.getComisionTramite()
                            .getComision());
                    }
                }
                if (tramite.getComisionTramite() != null &&
                    tramite.getComisionTramite().getComision() != null) {
                    Hibernate.initialize(tramite.getComisionTramite().getComision()
                        .getEstado());
                }
                if (tramite.getTramitePredioEnglobes() != null) {
                    for (TramitePredioEnglobe tpe : tramite
                        .getTramitePredioEnglobes()) {
                        if (tpe.getPredio() != null) {
                            Hibernate.initialize(tpe.getPredio());
                        }
                        if (tpe.getPredio() != null &&
                            tpe.getPredio().getPredioDireccions() != null) {
                            Hibernate.initialize(tpe.getPredio()
                                .getPredioDireccions());
                        }
                        if (tpe.getPredio() != null &&
                            tpe.getPredio().getDireccionPrincipal() != null) {
                            Hibernate.initialize(tpe.getPredio()
                                .getDireccionPrincipal());
                        }

                        if (tpe.getPredio() != null &&
                            tpe.getPredio().getReferenciaCartograficas() != null) {
                            Hibernate.initialize(tpe.getPredio()
                                .getReferenciaCartograficas());
                        }
                    }
                }

                if (tramite.getDepartamento() != null) {
                    Hibernate.initialize(tramite.getDepartamento());

                    if (tramite.getDepartamento().getCodigo() != null) {
                        Hibernate.initialize(tramite.getDepartamento().getCodigo());
                    }

                    if (tramite.getDepartamento().getNombre() != null) {
                        Hibernate.initialize(tramite.getDepartamento().getNombre());
                    }
                }

                if (tramite.getMunicipio() != null) {
                    Hibernate.initialize(tramite.getMunicipio());
                    if (tramite.getMunicipio().getNombre() != null) {
                        Hibernate.initialize(tramite.getMunicipio().getNombre());
                    }
                    if (tramite.getMunicipio().getCodigo() != null) {
                        Hibernate.initialize(tramite.getMunicipio().getCodigo());
                    }
                }

                if (tramite.getTramiteTextoResolucions() != null) {
                    Hibernate.initialize(tramite.getTramiteTextoResolucions());
                }

                if (tramite.getResultadoDocumento() != null) {
                    Hibernate.initialize(tramite.getResultadoDocumento());

                    if (tramite.getResultadoDocumento().getNumeroDocumento() != null) {
                        Hibernate.initialize(tramite.getResultadoDocumento().getNumeroDocumento());
                    }

                    if (tramite.getResultadoDocumento().getFechaDocumento() != null) {
                        Hibernate.initialize(tramite.getResultadoDocumento().getFechaDocumento());
                    }
                }

            }

            return tramites;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error consultando trámites por múltiples ids", e);
        }

    }

    /**
     * @see ITramiteDAO#obtenerDocumentosGeneradosPorIdTramite(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public List<Tramite> obtenerDocumentosGeneradosPorIdTramite(Long tramiteId) {

        LOGGER.debug("on TramiteDAOBean#obtenerDocumentosGeneradosPorIdTramite ...");
        List<Tramite> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT t FROM Tramite t " +
            " LEFT JOIN FETCH t.tramiteDocumentos documentacion " +
            " LEFT JOIN FETCH documentacion.documento documento" +
            " LEFT JOIN FETCH documento.tipoDocumento " +
            " WHERE t.id = :tramiteIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIdP", tramiteId);
            answer = (List<Tramite>) query.getResultList();

            for (Tramite tramite : answer) {

                Hibernate.initialize(tramite.getSolicitud().getTramites());
                Hibernate.initialize(tramite.getTramites());
                Hibernate.initialize(tramite.getTramiteDocumentos());
                Hibernate.initialize(tramite.getTramitePredioEnglobes());
                Hibernate.initialize(tramite.getPredio());
                if (tramite.getPredio() != null) {
                    Hibernate.initialize(tramite.getPredio().getPredioDireccions());
                }

                for (TramitePredioEnglobe tdp : tramite.getTramitePredioEnglobes()) {
                    Hibernate.initialize(tdp.getPredio());
                    Hibernate.initialize(tdp.getPredio().getPredioDireccions());
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error on TramiteDAOBean#obtenerDocumentosGeneradosPorIdTramite: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#obtenerDocumentosGeneradosPorIdTramite");
        }
        return answer;

    }

    /**
     * @see
     * ITramiteDAO.consultarResolucionesPorNumeroResolucionFinalEInicial(productoCatastralDetalle)
     *
     * @author leidy.gonzalez
     *
     */
    @Override
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;
        String municipio = "";

        if (productoCatastralDetalle.getMunicipioCodigo().length() >= 5 &&
            productoCatastralDetalle.getMunicipioCodigo().substring(2, 5) != null) {
            municipio = productoCatastralDetalle.getMunicipioCodigo().substring(2, 5);
        } else if (productoCatastralDetalle.getMunicipioCodigo() != null) {
            municipio = productoCatastralDetalle.getMunicipioCodigo();
        }

        String numeroResolucionInicial = productoCatastralDetalle.getDepartamentoCodigo() +
            "-" + municipio +
            "-" + productoCatastralDetalle.getNumeroResolucionInicial() +
            "-" + productoCatastralDetalle.getAnio();

        String numeroResolucionFinal = productoCatastralDetalle.getDepartamentoCodigo() +
            "-" + municipio +
            "-" + productoCatastralDetalle.getNumeroResolucionFinal() +
            "-" + productoCatastralDetalle.getAnio();

        LOGGER.debug("on TramiteDAOBean#consultarResolucionesPorNumeroResolucionFinalEInicial ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " WHERE d.numeroDocumento BETWEEN :numeroResolucionInicial and :numeroResolucionFinal " +
            " AND td.id= :tipoDocumento ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroResolucionInicial", numeroResolucionInicial);
            query.setParameter("numeroResolucionFinal", numeroResolucionFinal);
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorNumeroResolucionFinalEInicial");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial, final y
     * fecha en que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = "";

        if (productoCatastralDetalle.getMunicipioCodigo().length() >= 5 &&
            productoCatastralDetalle.getMunicipioCodigo().substring(2, 5) != null) {

            municipio = productoCatastralDetalle.getMunicipioCodigo().substring(2, 5);

        } else if (productoCatastralDetalle.getMunicipioCodigo() != null) {
            municipio = productoCatastralDetalle.getMunicipioCodigo();
        }

        String numeroResolucionInicial = productoCatastralDetalle.getDepartamentoCodigo() +
            "-" + municipio +
            "-" + productoCatastralDetalle.getNumeroResolucionInicial() +
            "-" + productoCatastralDetalle.getAnio();
        String numeroResolucionFinal = productoCatastralDetalle.getDepartamentoCodigo() +
            "-" + municipio +
            "-" + productoCatastralDetalle.getNumeroResolucionFinal() +
            "-" + productoCatastralDetalle.getAnio();

        LOGGER.debug(
            "on TramiteDAOBean#consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " WHERE t.estado= :estado " +
            " AND d.numeroDocumento BETWEEN :numeroResolucionInicial and :numeroResolucionFinal " +
            " AND td.id= :tipoDocumento ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroResolucionInicial", numeroResolucionInicial);
            query.setParameter("numeroResolucionFinal", numeroResolucionFinal);
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
            query.setParameter("estado", ETramiteEstado.FINALIZADO_APROBADO.getCodigo());

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucion(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = "";

        if (productoCatastralDetalle.getMunicipioCodigo().length() >= 5 &&
            productoCatastralDetalle.getMunicipioCodigo().substring(2, 5) != null) {

            municipio = productoCatastralDetalle.getMunicipioCodigo().substring(2, 5);

        } else if (productoCatastralDetalle.getMunicipioCodigo() != null) {
            municipio = productoCatastralDetalle.getMunicipioCodigo();
        }

        String numeroResolucionInicial = productoCatastralDetalle.getDepartamentoCodigo() +
            "-" + municipio +
            "-" + productoCatastralDetalle.getNumeroResolucionInicial() +
            "-" + productoCatastralDetalle.getAnio();

        LOGGER.debug("on TramiteDAOBean#consultarResolucionesPorNumeroResolucion ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " WHERE d.numeroDocumento= :numeroResolucionInicial " +
            " AND td.id= :tipoDocumento ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroResolucionInicial", numeroResolucionInicial);
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorNumeroResolucion");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial que
     * aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = "";

        if (productoCatastralDetalle.getMunicipioCodigo().length() >= 5 &&
            productoCatastralDetalle.getMunicipioCodigo().substring(2, 5) != null) {

            municipio = productoCatastralDetalle.getMunicipioCodigo().substring(2, 5);

        } else if (productoCatastralDetalle.getMunicipioCodigo() != null) {
            municipio = productoCatastralDetalle.getMunicipioCodigo();
        }

        String numeroResolucionInicial = productoCatastralDetalle.getDepartamentoCodigo() +
            "-" + municipio +
            "-" + productoCatastralDetalle.getNumeroResolucionInicial() +
            "-" + productoCatastralDetalle.getAnio();

        LOGGER.debug("on TramiteDAOBean#consultarResolucionesPorNumeroResolucionAplicaCambios ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " WHERE t.estado= :estado " +
            " AND d.numeroDocumento= :numeroResolucionInicial " +
            " AND td.id= :tipoDocumento ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroResolucionInicial", numeroResolucionInicial);
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
            query.setParameter("estado", ETramiteEstado.FINALIZADO_APROBADO.getCodigo());

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorNumeroResolucionAplicaCambios");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     * en que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = productoCatastralDetalle.getMunicipioCodigo();

        LOGGER.debug(
            "on TramiteDAOBean#consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " LEFT JOIN FETCH t.departamento dep" +
            " LEFT JOIN FETCH t.municipio mun" +
            " WHERE t.estado= :estado " +
            " AND d.fechaDocumento BETWEEN :fechaResolucionInicial and :fechaResolucionFinal " +
            " AND td.id= :tipoDocumento " +
            " AND dep.codigo= :departamentoCodigo " +
            " AND mun.codigo= :municipioCodigo ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("fechaResolucionInicial", productoCatastralDetalle.
                getFechaResolucionInicial());
            query.setParameter("fechaResolucionFinal", productoCatastralDetalle.
                getFechaResolucionFinal());
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
            query.setParameter("estado", ETramiteEstado.FINALIZADO_APROBADO.getCodigo());
            query.setParameter("departamentoCodigo", productoCatastralDetalle.
                getDepartamentoCodigo());
            query.setParameter("municipioCodigo", municipio);

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = productoCatastralDetalle.getMunicipioCodigo();

        LOGGER.debug("on TramiteDAOBean#consultarResolucionesPorFechaResolucionFinalEInicial ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " LEFT JOIN FETCH t.departamento dep" +
            " LEFT JOIN FETCH t.municipio mun" +
            " WHERE d.fechaDocumento BETWEEN :fechaResolucionInicial and :fechaResolucionFinal " +
            " AND td.id= :tipoDocumento " +
            " AND dep.codigo= :departamentoCodigo " +
            " AND mun.codigo= :municipioCodigo ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("fechaResolucionInicial", productoCatastralDetalle.
                getFechaResolucionInicial());
            query.setParameter("fechaResolucionFinal", productoCatastralDetalle.
                getFechaResolucionFinal());
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
            query.setParameter("departamentoCodigo", productoCatastralDetalle.
                getDepartamentoCodigo());
            query.setParameter("municipioCodigo", municipio);

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorFechaResolucionFinalEInicial");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucion(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = productoCatastralDetalle.getMunicipioCodigo();

        LOGGER.debug("on TramiteDAOBean#consultarResolucionesPorFechaResolucion ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " LEFT JOIN FETCH t.departamento dep" +
            " LEFT JOIN FETCH t.municipio mun" +
            " WHERE d.fechaDocumento= :fechaResolucionInicial " +
            " AND td.id= :tipoDocumento " +
            " AND dep.codigo= :departamentoCodigo " +
            " AND mun.codigo= :municipioCodigo ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("fechaResolucionInicial", productoCatastralDetalle.
                getFechaResolucionInicial());
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
            query.setParameter("departamentoCodigo", productoCatastralDetalle.
                getDepartamentoCodigo());
            query.setParameter("municipioCodigo", municipio);

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorFechaResolucion");
        }
        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion en
     * que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {
        List<Tramite> answer = null;
        String queryString;
        Query query;

        String municipio = productoCatastralDetalle.getMunicipioCodigo();

        LOGGER.debug("on TramiteDAOBean#consultarResolucionesPorFechaResolucionAplicaCambios ...");

        queryString = "SELECT t FROM Tramite t LEFT JOIN FETCH t.resultadoDocumento d " +
            " JOIN FETCH d.tipoDocumento td " +
            " LEFT JOIN FETCH t.departamento dep" +
            " LEFT JOIN FETCH t.municipio mun" +
            " WHERE t.estado= :estado " +
            " AND d.fechaDocumento= :fechaResolucionInicial" +
            " AND td.id= :tipoDocumento " +
            " AND dep.codigo= :departamentoCodigo " +
            " AND mun.codigo= :municipioCodigo ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("fechaResolucionInicial", productoCatastralDetalle.
                getFechaResolucionInicial());
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
            query.setParameter("estado", ETramiteEstado.FINALIZADO_APROBADO.getCodigo());
            query.setParameter("departamentoCodigo", productoCatastralDetalle.
                getDepartamentoCodigo());
            query.setParameter("municipioCodigo", municipio);

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorFechaResolucionAplicaCambios");
        }
        return answer;
    }

    /**
     * @see ITramiteDAO#buscarPredioEnglobesPorTramite(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public List<TramiteDetallePredio> buscarPredioEnglobesPorPredio(Long predioId) {

        LOGGER.debug("on TramiteDAOBean#buscarPredioEnglobesPorPredio");

        List<TramiteDetallePredio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tpe " +
            " FROM TramiteDetallePredio tpe" +
            " JOIN FETCH tpe.predio" +
            " WHERE tpe.predio.id = :predioId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("predioId", predioId);
            answer = (List<TramiteDetallePredio>) query.getResultList();

            for (TramiteDetallePredio tpe : answer) {
                tpe.getPredio().getDepartamento().getNombre();
                tpe.getPredio().getMunicipio().getNombre();
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#buscarPredioEnglobesPorPredio");
        }

        return answer;

    }

    /*
     * @modified pedro.garcia 06-09-2013 Para arreglar la lista de estados que debe tener en cuenta
     * para excluir trÃ¡mites
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Tramite> findTramitesCancelacionOtraTerritorial(
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize) {

        List<Tramite> answer = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT DISTINCT t FROM Tramite t");

            sql = setupWhereClauseFindTramitesCancelacion(filtro, sql);

            LOGGER.debug("JPQL: " + sql.toString());

            Query query = this.entityManager.createQuery(sql.toString());
            query.setFirstResult(first);
            query.setMaxResults(pageSize);

            query = this.setupParametersFindTramitesCancelacion(filtro, query);

            answer = (List<Tramite>) query.getResultList();

            return answer;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());

        }
    }

    /*
     * @modified pedro.garcia 06-09-2013 Para arreglar la lista de estados que debe tener en cuenta
     * para excluir trÃ¡mites
     */
    @Override
    public int countTramitesCancelacionOtraTerritorial(
        FiltroDatosConsultaCancelarTramites filtro) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(DISTINCT t) FROM Tramite t");

            sql = setupWhereClauseFindTramitesCancelacion(filtro, sql);

            Query query = this.entityManager.createQuery(sql.toString());

            query = this.setupParametersFindTramitesCancelacion(filtro, query);

            return ((Long) query.getSingleResult()).intValue();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ExcepcionSNC("AlgÃºnCÃ³digo", ESeveridadExcepcionSNC.ERROR,
                "Error en en conteo de trÃ¡mites para cancelar.", e);

        }
    }

    @Override
    public Tramite findOnlyTramiteByIdTramite(Long tramiteId) {

        Tramite t = null;
        Query query = this.entityManager.createQuery("SELECT t " +
            " FROM Tramite t " +
            " WHERE t.id= :tramiteId");

        try {
            query.setParameter("tramiteId", tramiteId);
            t = (Tramite) query.getSingleResult();

        } catch (NoResultException nre) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nre,
                "TramiteDAOBean#findOnlyTramiteByIdTramite", "Tramite", tramiteId.toString());
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#findOnlyTramiteByIdTramite");
        }
        return t;

    }

    /*
     * @modified juan.agudelo @modified pedro.garcia 19-09-2012 Se eliminó el parámetro
     * tipoInscripcion ya que ese campo no se usa en los trámites.
     */
    @Implement
    @Override
    public RepConsultaPredial getConfiguracionReportePredial(Long tipoInforme) {

        StringBuilder q = new StringBuilder();

        q.append("SELECT t FROM RepConsultaPredial t");
        q.append(" WHERE t.repReporte= :tipoInforme");

        try {
            Query query = this.entityManager.createQuery(q.toString());
            query.setParameter("tipoInforme", tipoInforme);

            return (RepConsultaPredial) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#getConfiguracionReportePredial");
        }
    }

    /**
     * @see ITramiteDAO#getTramiteParaEdicionGeograficaSinProductoCatastralJobTerminado(List<Long>)
     * @author david.cifuentes
     */
    @Override
    public List<Tramite> getTramiteParaEdicionGeograficaSinProductoCatastralJobTerminado(
        List<Long> tramiteIds) {

        LOGGER.debug(
            "TramiteDAOBean#getTramiteParaEdicionGeograficaSinProductoCatastralJobTerminado");

        List<Tramite> answer = null;
        String queryString;
        Query query;

        List<String> estados = new ArrayList<String>();
        estados.add(ProductoCatastralJob.AGS_ERROR);
        estados.add(ProductoCatastralJob.AGS_EN_EJECUCION);
        estados.add(ProductoCatastralJob.SNC_ERROR);
        estados.add(ProductoCatastralJob.AGS_ESPERANDO);

        try {

            if (tramiteIds.size() >= 1000) {

                Query querySesion = entityManager.createNativeQuery(
                    "SELECT SESION_ID_SEQ.NEXTVAL FROM DUAL");
                Object o = querySesion.getSingleResult();
                Query nativeQuery = entityManager.createNativeQuery(
                    "INSERT INTO tmp_tramite_session (sesion_id, tramite_id) VALUES (?,?)");
                nativeQuery.setParameter(1, (BigDecimal) o);

                for (Long id : tramiteIds) {
                    nativeQuery.setParameter(2, id);
                    nativeQuery.executeUpdate();
                }

                queryString = "SELECT DISTINCT T.* FROM TRAMITE T " +
                    " LEFT JOIN DEPARTAMENTO D ON D.CODIGO = T.DEPARTAMENTO_CODIGO " +
                    " LEFT JOIN MUNICIPIO M ON M.CODIGO = T.MUNICIPIO_CODIGO " +
                    " LEFT JOIN PREDIO P ON P.ID = T.PREDIO_ID " +
                    " LEFT JOIN TRAMITE_DETALLE_PREDIO TDP ON TDP.TRAMITE_ID = T.ID " +
                    " LEFT JOIN PREDIO P2 ON TDP.PREDIO_ID = P2.ID " +
                    " LEFT JOIN TRAMITE_RECTIFICACION TR ON TR.TRAMITE_ID = T.ID " +
                    " WHERE T.ID IN ( SELECT tramite_id" +
                    " FROM tmp_tramite_session tts WHERE sesion_id = :sesionId ) " +
                    " AND NOT EXISTS (SELECT 1 FROM PRODUCTO_CATASTRAL_JOB P " +
                    " WHERE ESTADO IN :estados" +
                    " AND TIPO_PRODUCTO = :tipoProducto AND TRAMITE_ID = T.ID )";

                query = entityManager.createQuery(queryString);
                query.setParameter("sesionId", (BigDecimal) o);
                query.setParameter("estados", estados);
                query.setParameter("tipoProducto", ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS);

            } else {

                queryString = "SELECT DISTINCT T.* FROM TRAMITE T " +
                    " LEFT JOIN DEPARTAMENTO D ON D.CODIGO = T.DEPARTAMENTO_CODIGO " +
                    " LEFT JOIN MUNICIPIO M ON M.CODIGO = T.MUNICIPIO_CODIGO " +
                    " LEFT JOIN PREDIO P ON P.ID = T.PREDIO_ID " +
                    " LEFT JOIN TRAMITE_DETALLE_PREDIO TDP ON TDP.TRAMITE_ID = T.ID " +
                    " LEFT JOIN PREDIO P2 ON TDP.PREDIO_ID = P2.ID " +
                    " LEFT JOIN TRAMITE_RECTIFICACION TR ON TR.TRAMITE_ID = T.ID " +
                    " WHERE T.ID IN :tramiteIds " +
                    " AND NOT EXISTS (SELECT 1 FROM PRODUCTO_CATASTRAL_JOB P " +
                    " WHERE ESTADO IN :estados" +
                    " AND TIPO_PRODUCTO = :tipoProducto AND TRAMITE_ID = T.ID )";

                query = this.entityManager.createNativeQuery(queryString, Tramite.class);
                query.setParameter("tramiteIds", tramiteIds);
                query.setParameter("estados", estados);
                query.setParameter("tipoProducto", ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS);
            }

            answer = query.getResultList();

            for (Tramite t : answer) {
                Hibernate.initialize(t.getDepartamento());
                Hibernate.initialize(t.getMunicipio());
                Hibernate.initialize(t.getPredio());
                Hibernate.initialize(t.getTramitePredioEnglobes());
                if (t.getTramitePredioEnglobes() != null) {
                    for (TramitePredioEnglobe tpe : t.getTramitePredioEnglobes()) {
                        Hibernate.initialize(tpe.getPredio());
                    }
                }
                Hibernate.initialize(t.getTramiteRectificacions());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return answer;
    }

    /**
     * @see ITramiteDAO#getTipoTramiteCompuestoCodigo()
     * @author felipe.cadena
     */
    @Override
    public List<String[]> getTipoTramiteCompuestoCodigo() {

        List<String> resultQuery = null;
        List<String[]> result = new ArrayList<String[]>();
        String strQuery =
            "select distinct(t.tipoTramite ||'- '||t.claseMutacion||'- '||t.subtipo) FROM Tramite t";

        try {
            Query query = this.entityManager.createQuery(strQuery);

            resultQuery = (List<String>) query.getResultList();

            for (String str : resultQuery) {

                String[] selectItem = new String[2];
                selectItem[0] = str.replaceAll("\\s", "");
                selectItem[1] = Utilidades.getTipoTramiteCadenaCompleto(str);

                result.add(selectItem);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDAOBean#getTipoTramiteCompuestoCodigo");
        }

        return result;
    }

    //end of class    
}
