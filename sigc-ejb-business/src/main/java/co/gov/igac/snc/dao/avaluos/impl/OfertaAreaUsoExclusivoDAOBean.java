package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOfertaAreaUsoExclusivoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaAreaUsoExclusivo;

@Stateless
public class OfertaAreaUsoExclusivoDAOBean extends GenericDAOWithJPA<OfertaAreaUsoExclusivo, Long>
    implements IOfertaAreaUsoExclusivoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OfertaAreaUsoExclusivoDAOBean.class);

}
