package co.gov.igac.snc.dao.conservacion;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.FiltroCargueDTO;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

@Local
public interface IPPredioDAO extends IGenericJpaDAO<PPredio, Long> {

    /**
     * @author fabio.navarrete Método encargado de buscar un PPredio y traer los datos asociados de
     * la entidad PPredioAvaluoCatastral
     * @param idPredio Id del predio a buscar
     * @return
     */
    public PPredio findPPredioByIdFetchAvaluos(Long idPredio);

    /**
     * @author fredy.wilches Metodo que consulta pPredio pero lo trae con todas sus colecciones
     * dependientes
     * @throws Exception
     */
    public PPredio findPPredioCompletoById(Long idPredio) throws Exception;

    /**
     * @author fredy.wilches Metodo que consulta pPredio pero lo trae con todas sus colecciones
     * dependientes
     * @throws Exception
     */
    public PPredio findPPredioCompletoByIdyTramite(Long idPredio, Long idTramite) throws Exception;

    /**
     * @author fredy.wilches Metodo que consulta pPredio, buscando por el id del tramite pero lo
     * trae con todas sus colecciones dependientes. A usarse unicamente en quinta nuevo
     * @throws Exception
     */
    public PPredio findPPredioCompletoByIdTramite(Long idTramite) throws Exception;

    /**
     * Metodo encargado de buscar un PPredio y traer las PPersonaPredio asociados
     *
     * @author juan.agudelo
     * @param id
     * @return
     */
    public PPredio getPPredioFetchPPersonaPredioPorId(Long id);

    /**
     * Método que consulta un {@link PPredio} haciendo fetch a sus {@link PPredioDireccion}
     * asociados.
     *
     * @param idPredio
     * @return
     */
    public PPredio findPPredioByIdFetchDirecciones(Long idPredio);

    /**
     * Método encargado de buscar los predios proyectados asociados a un trámite específico
     *
     * @param tramiteId Id del trámite al que se desean buscar los predios proyectados asociados.
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> findPPrediosByTramiteId(Long tramiteId);

    /**
     * Método encargado de buscar los predios proyectados asociados a un trámite específico, pero
     * solo los que se mantengan o se inscriban
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<PPredio> findPPrediosResultantesByTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de ppredios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle del avalúo
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPPrediosPanelAvaluoPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle ficha matriz unicamente si el tramite es una mutación de
     * segunda y desenglobe
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPPrediosPanelFichaMatrizPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de ppredios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle justificación de propiedad
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPPrediosPanelJPropiedadPorTramiteId(
        Long tramiteId);

    /**
     * Método que recupera la lista de ppredios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle propietarios y / o poseedores
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPPrediosPanelPropietariosPorTramiteId(
        Long tramiteId);

    /**
     * Método que recupera la lista de predios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle ubicación predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId);

    /**
     * Método que recupera los datos para la tabla de resumen en revisión de proyección
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPPrediosResumenTramitePorTramiteId(Long tramiteId);

    /**
     * Retorna una lista de predios con los campos requeridos para la proyección a partir de una
     * lista de Ids ingresada
     *
     * @param predioIds
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> findByIds(List<Long> predioIds);

    /**
     * Metodo que consulta pPredios NO CANCELADOS asociados a un trámite, los trae con todas sus
     * colecciones dependientes
     *
     * @param idTramite
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> findPPrediosCompletosByTramiteId(Long idTramite);

    /**
     * Retorna una lista de predios con los campos requeridos para la proyección a partir de una
     * lista de Ids ingresada
     *
     * @param predioIds
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> findByIdsPHCondominio(List<Long> predioIds);

    /**
     * Metodo que consulta pPredios NO CANCELADOS de PH o Condominio asociados a un trámite, los
     * trae con todas sus colecciones dependientes
     *
     * @param idTramite
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> findPPrediosCompletosByTramiteIdPHCondominio(
        Long idTramite);

    /**
     * Método que recupera PPredios sencillos sin ningun objeto asociado por trámite ID
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    List<PPredio> findExistingPPrediosByTramiteId(Long tramiteId);

    /**
     * metodo que actualiza el campo de areas en los PPredios
     *
     * @author franz.gamba
     * @param predios
     * @param areas
     * @return
     */
    public boolean actualizacionDeAreasEnPredios(List<String> predios, List<Double> areas);

    /**
     * Método que realiza la suma de los avalúos de los predios asociados a la ficha matriz.
     *
     * @author david.cifuentes
     *
     * @param idTramite
     * @return
     */
    public Double calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long idTramite);

    /**
     * Busca los PPredio relacionados con un trámite (posiblemente filtrados por el valor del campo
     * 'cancela_inscribe) No hace fetch de nada.
     *
     * @author pedro.garcia
     *
     * @param idTramite Si es null, no se tiene en cuenta para la búsqueda
     * @param cmiValuesToUse Lista de valores de 'cancela_inscribe' para filtrar la búsqueda
     * @return
     */
    public List<PPredio> buscarPorTramiteId(Long idTramite, List<String> cmiValuesToUse);

    /**
     * Obtiene el PPredio con los datos correspondientes al módulo 'detalle ubicación predio'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerConDatosUbicacion(Long ppredioId);

    /**
     * Obtiene el PPredio con los datos correspondientes al módulo 'justificación de propiedad'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerConDatosJustificacionPropiedad(Long ppredioId);

    /**
     * Obtiene el PPredio con los datos correspondientes al módulo 'avalúo'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerConDatosAvaluo(Long ppredioId);

    /**
     * Obtiene el PPredio con los datos correspondientes al módulo 'propietarios o poseedores'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerConDatosPropietarios(Long ppredioId);

    /**
     * Obtiene el PPredio con los datos correspondientes al módulo 'ficha matriz'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerConDatosFichaMatriz(Long ppredioId);

    /**
     * Obtiene el PPredio correspondiente al tramite id haciendole fetch al predio Nota: Se utiliza
     * en el servicio web que actualiza los valores de la edicion geografica
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    @Deprecated
    public List<PPredio> getPPrediosByTramiteIdActualizarAvaluo(Long tramiteId);

    /**
     * Método que asigna a los predios inscritos las pPredioZonas del predio original o el que se
     * mantiene
     *
     * @author franz.gamba
     * @param tramiteId
     * @param numPredPrin
     * @param usuario
     * @param esQuinta
     * @return
     */
    public boolean updatePPredioZonasPrediosInscritos(
        Long tramiteId, String numPredPrin, UsuarioDTO usuario, boolean esQuinta);

    /**
     * Método que retorna un PPredio (sin Fetchs) segun su tramite id (Servicios WEB)
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public PPredio getPPredioByTramiteId(Long tramiteId);

    public boolean updatePPredioZonasPrediosInscritos(
        Long tramiteId, String numPredPrin, UsuarioDTO usuario, boolean esQuinta,
        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos, List<PPredio> predTemp);

    /**
     * Retorna un ppredio por el número predial especificado
     *
     * @author javier.aponte
     * @param numPredial número predial del ppredio a obtener
     * @return
     */
    public PPredio getPPredioByNumeroPredial(String numPredial);

    /**
     * Obtiene el PPredio correspondiente al tramite id haciendole fetch al predio Nota: Se utiliza
     * en el servicio web que actualiza los valores de la edicion considera el cancela inscribe null
     * geografica
     *
     * @author andres.eslava
     * @param tramiteId
     * @return lista de predios proyectados asociados al tramite.
     */
    public List<PPredio> getPPrediosByTramiteId(Long tramiteId);

    /**
     * obtiene los predios proyectados a partir de una lista de predios si el tamaño es superior a
     * 1000 lo subdivide en grupos mas pequeños
     *
     * @param listaNumerosPrediales numeros prediales de los predios proyectados que se quieren
     * obtener.
     * @return lista de predios proyectados.
     *
     * @author andres.eslava
     *
     */
    public List<PPredio> obtenerPrediosProyectadosConFichaMatriz(List<String> listaNumerosPrediales);

    /**
     * Obtiene todos lo predios asociados a una ficha matriz
     *
     * @author felipe.cadena
     *
     * @param fichaMatrizId
     * @return
     */
    public List<PPredio> obtenerPrediosProyectadosAsociadosAFicha(Long fichaMatrizId);

    /**
     * Consulta los predios proyectados asociados a un numero de resgitro y circulo registral
     * determinado
     *
     * @author felipe.cadena
     * @param circuloRegistralCodigo
     * @param numeroRegistro
     * @return
     */
    public List<PPredio> buscarPorMatricula(String circuloRegistralCodigo, String numeroRegistro);

    /**
     *
     * Sobre escribe el metodo para obtener informacion de predios proyectados de un tramite con
     * direcciones y construcciones
     *
     * @param tramiteId identificador del predio
     * @param valoresCancelaInscribre cancelaInscribes a consultar
     * @author andres.eslava
     * @TODO::juan.garcia::se debe remplemplazar el metodo deprecated::22/05/2015
     */
    public List<PPredio> getPPrediosByTramiteIdActualizarAvaluo(Long tramiteId,
        List<EProyeccionCancelaInscribe> valoresCancelaInscribre);

    /**
     * Metodo que determina cuales de los predios asociados a un tramite han sido modificados con
     * respecto a una tabla en particular.
     * <BR/>
     * <BR/>
     * La tabla asociada debe estar relacionada directamente al predio con un campo predio_id. Si no
     * existen registros en la tabla para un predio no habra entrada de ese predio en particular. Si
     * la tabla no esta asociada el metodo retorna null
     * <BR/>
     * <BR/>
     * Retorna un mapa con los ids de los predios como llave y un boolean que indica si el predio
     * fue modificado o no.
     *
     * @author felipe.cadena
     * @param idTramite
     * @param nombreTablaAsociada
     * @return
     */
    public TreeMap<Long, Boolean> obtenerPredioModificadoPorTamite(Long idTramite,
        String nombreTablaAsociada);

    /**
     * Metodo para eliminar la proyeccion de un predio, se realiza mediante un query nativo ya que
     * en la DB el borrado esta en cascada se deber tener cuidado de actualizar las entidad cuando
     * se ejecute este metodo.
     *
     * @author felipe.cadena
     * @param idPredio
     */
    public void eliminarProyeccionPredio(Long idPredio);

    /**
     *
     * Metodo que obtiene los PPredios ACTIVOS completos
     *
     * @param idTramite
     * @return
     */
    public List<PPredio> findPPrediosCompletosActivosByTramiteId(Long idTramite);

    /**
     * Metodo que determina cuales de los predios asociados a un tramite se le has modificado el
     * número predial
     * <BR/>
     * Retorna un mapa con los números predial como llave y un boolean que indica si el predio fue
     * modificado o no.
     *
     * @author javier.aponte
     * @param idTramite
     * @return
     */
    public TreeMap<String, Boolean> obtenerPredioConNumeroPredialModificadoPorTamite(Long idTramite);

    /**
     * Obtiene un PPredio por numero predial
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public PPredio obtenerPorNumeroPredialConTramite(String numeroPredial);

    /**
     * Método que realiza la búsqueda de un predio por su id, pero no realiza fetch sobre ninguna
     * tabla, en vez de ello realiza la inicialización de los campos a través de un
     * Hibernate.inicialize
     *
     * @param idPredio
     * @return
     * @throws Exception
     */
    public PPredio buscarPredioCompletoPorIdSinFetch(Long idPredio)
        throws ExcepcionSNC;

    /**
     * Método que realiza la búsqueda de una lista de predios por sus id's
     *
     * @param List idPredio
     * @return
     */
    public List<PPredio> buscarPPredioPorIdPredios(List<Long> predioIds);

    /**
     * Determina la existencia de los predios poyectados asociados a los numeros prediales dados
     *
     *
     * @author felipe.cadena
     * @param nPrediales
     * @param tipoTramite Filtra por tipo tramite dependientdo el parametro equal
     * @param equal Si es true busca coincidencias por tipo de tramite de los contrario busca tipos diferentes al dado
     * @return
     */
    public List<PPredio> consultaPorNumeroPredial(List<String> nPrediales, String tipoTramite,  Boolean equal);

    /**
     * Obtiene una lista de PPredio de consdicion 9 que se encuentren activos consultandolos por id
     * de tramite
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredio> buscarPprediosCond9PorIdTramite(Long idTramite);

    /**
     * Obtiene una lista de PPredio que fueron modificados consultandolos por id de tramite
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredio> buscarPPrediosModPorTramiteId(Long tramiteId, Long fichaMatrizId);

    /**
     * Obtiene una lista de PPredio diferentes a la(s) Ficha Matriz(ces), a condicion de propiedad 0
     * o 5 y que no se encuentren cancelados consultandolos por id de tramite
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredio> buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(
        Long idTramite);

    /**
     * Busca por tipo, municipio y departamento
     *
     * @author felipe.cadena
     *
     * @param munCodigo
     * @param deptoCodigo
     * @return
     */
    public List<PPredio> buscarPorMunDeptoTipo(String munCodigo, String deptoCodigo, String tipoTramite);


}
