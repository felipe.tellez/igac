package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;

@Local
public interface IOfertaInmobiliariaDAO extends IGenericJpaDAO<OfertaInmobiliaria, Long> {

    /**
     * Método que permite buscar una oferta inmobiliaria con las ofertas inmobiliarias asociadas por
     * el id de la oferta inmobiliaria
     *
     * @author juan.agudelo
     * @param id
     * @return
     */
    public OfertaInmobiliaria buscarOfertaInmobiliariaOfertasByOfertaId(
        Long ofertaInmobiliariaId);

    /**
     * Método que permite buscar ofertas inmobiliarias dependiendo de algun parámtero de consulta
     * contenido en FiltroDatosConsultaOfertasInmob
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaOfertasInmob
     * @param contadoresDesdeYCuantos
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     *
     * @param sortOrder nombre del ordenamiento (UNSORTED, DESCENDING, DESCENDING)
     *
     * @return
     */
    public List<OfertaInmobiliaria> buscarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob, String sortField,
        String sortOrder, int... contadoresDesdeYCuantos);

    /**
     * Método que permite contar ofertas inmobiliarias dependiendo de algun parámtero de consulta
     * contenido en FiltroDatosConsultaOfertasInmob
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaOfertasInmob
     * @return
     */
    public Long contarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob);

    /**
     * Método que permite buscar una oferta inmobiliaria con las oferta inmobiliaria padre y las
     * listas inicializadas para visualización de detalles por el id de la oferta inmobiliaria
     *
     * @author juan.agudelo
     * @param id
     * @return
     */
    public OfertaInmobiliaria buscarOfertaInmobiliariaOfertaPadreByOfertaId(
        Long ofertaInmobiliariaId);

    /**
     *
     * Método que consulta la lista de ofertas inmobiliarias de un recolector con determinado estado
     *
     *
     * @author rodrigo.hernandez
     *
     * @param recolectorId - Id del recolector
     * @param regionId - Id de la región
     * @param estado - Estado en el que se encuentran las ofertas
     *
     * @return - Lista de ofertas inmobiliarias de un recolector
     */
    public List<OfertaInmobiliaria> buscarListaOfertasInmobiliariasPorRecolector(String recolectorId,
        Long regionId, String estado);
}
