/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EOrdenPracticaEstado;

/**
 * Interfaz para los métodos de BD del entity OrdePractica
 *
 * @author pedro.garcia
 */
@Local
public interface IOrdenPracticaDAO extends IGenericJpaDAO<OrdenPractica, Long> {

    /**
     * Método para obtener la orden de practica a la cual esta relacionado un avalúo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public OrdenPractica consultarPorAvaluoId(Long idAvaluo);

    /**
     * Método para obtener las ordenes de practica asociadas a un {@link ProfesionalAvaluo} y
     * creadas en un rango de fechas
     *
     * @author christian.rodriguez
     * @param profesionalId
     * @param fechaMinima Fecha mínima para la creación de la orden de práctica
     * @param fechaMaxima Fecha máxima para la creación de la orden de práctica
     * @return Lista con las {@link OrdenPractica} encontradas, o null si no se encontraron
     */
    public List<OrdenPractica> consultarPorProfesionalAvaluosIdYRangoFechas(
        Long profesionalId, Date fechaMinima, Date fechaMaxima);

    /**
     * método para consultar las {@link OrdenPractica} que tengan un estado
     * {@link EOrdenPracticaEstado} especifico
     *
     * @author christian.rodriguez
     * @param estadoOP estado asociadoa la {@link OrdenPractica}, pertenece a un codigo de
     * {@link EOrdenPracticaEstado}
     * @return Lista con las {@link OrdenPractica} encontradas, o null si no se encontraron
     */
    public List<OrdenPractica> consultarPorEstado(String estadoOP);

}
