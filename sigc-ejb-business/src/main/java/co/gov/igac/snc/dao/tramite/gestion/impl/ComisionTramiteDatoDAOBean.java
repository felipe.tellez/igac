/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.impl;

import java.util.ArrayList;
import java.util.List;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDatoDAO;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class ComisionTramiteDatoDAOBean extends GenericDAOWithJPA<ComisionTramiteDato, Long>
    implements IComisionTramiteDatoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionTramiteDatoDAOBean.class);

    public List<ComisionTramiteDato> buscarComisionTramiteDatoPorTramiteId(
        Long tramiteId) {
        List<ComisionTramiteDato> answer = null;

        Query q = null;
        String query;

        query =
            "SELECT ctd from ComisionTramiteDato ctd WHERE ctd.comisionTramite.tramite.id = :tramiteId";

        q = this.entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);

        try {
            answer = (ArrayList<ComisionTramiteDato>) q.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("ComisionTramiteDAOBean#buscarComisionTramiteDatoPorTramiteId: " +
                "La consulta de comisiones trámites datos por id del trámite no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }
//end of class
}
