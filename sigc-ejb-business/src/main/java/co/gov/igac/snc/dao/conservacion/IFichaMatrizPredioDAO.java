package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;

/**
 * @author juan.agudelo
 */
@Local
public interface IFichaMatrizPredioDAO extends
    IGenericJpaDAO<FichaMatrizPredio, Long> {

    /**
     * Método que recupera el coeficiente de copropiedad de un predio dependiendo de su número
     * predial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public Double getCoeficienteCopropiedadByNumeroPredial(String numeroPredial);

    /**
     * Método que recupera la ficha matriz predio por número predial
     *
     * @author javier.aponte
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public FichaMatrizPredio obtenerFichaMatrizPredioPorNumeroPredial(String numeroPredial);
}
