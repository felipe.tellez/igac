/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.actualizacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IMunicipioActualizacionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import co.gov.igac.snc.persistence.util.EMunicipioActualizacionEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementación de los servicios de persistencia del objeto Convenio.
 *
 * @author felipe.cadena
 */
@Stateless
public class MunicipioActualizacionDAOBean extends GenericDAOWithJPA<MunicipioActualizacion, Long>
    implements IMunicipioActualizacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDAOBean.class);

    /**
     * @author felipe.cadena
     * @see IMunicipioActualizacionDAO#obtenerPorMunicipioYEstado(String, String, Boolean, Integer)
     */
    @Override
    public List<MunicipioActualizacion> obtenerPorMunicipioYEstado(String municpioCodigo,
        String estado, Boolean activo, Integer vigencia) {

        List<MunicipioActualizacion> result = new ArrayList<MunicipioActualizacion>();

        String query = "SELECT ma FROM MunicipioActualizacion ma " +
            " JOIN FETCH ma.departamento d " +
            " JOIN FETCH ma.municipio m " +
            " LEFT JOIN FETCH ma.solicitud s " +
            " WHERE ma.municipio.codigo = :municipioCodigo ";

        if (estado != null) {
                query += " AND ma.estado = :estado";
        }

        if (vigencia != null) {
            query += " AND ma.vigencia = :vigencia";
        }

        if (activo != null) {
            query += " AND ma.activo = :activo";
        }

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("municipioCodigo", municpioCodigo);

            if (estado != null) {
                q.setParameter("estado", estado);
            }

            if (vigencia != null) {
                q.setParameter("vigencia", vigencia);
            }

            if (activo != null) {
                if (activo) {
                    q.setParameter("activo", ESiNo.SI.getCodigo());
                } else {
                    q.setParameter("activo", ESiNo.NO.getCodigo());
                }
            }
            LOGGER.info("QUERYY " + query + " vigencia " + vigencia + " activo " +ESiNo.SI.getCodigo() );
            result = (List<MunicipioActualizacion>) (List<MunicipioActualizacion>) q.getResultList();

            for (MunicipioActualizacion ma:result) {
                Hibernate.initialize(ma.getDepartamento());
                Hibernate.initialize(ma.getMunicipio());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "MunicipioActualizacionDAOBean#obtenerPorMunicipioYEstado");
        }

        return result;

    }
    
    /**
     * @author hector.arias
     * @see IMunicipioActualizacionDAO#obtenerPorMunicipioYEstadoUltimoGeografico(String, String, Boolean, Integer)
     */
    @Override
    public List <MunicipioActualizacion> obtenerPorMunicipioYEstadoGeografico(String municpioCodigo,
        String estado, Boolean activo, Integer vigencia) {

        List <MunicipioActualizacion> result = new ArrayList<MunicipioActualizacion>();

        String query = "SELECT ma FROM MunicipioActualizacion ma " +
            " JOIN FETCH ma.departamento d " +
            " JOIN FETCH ma.municipio m " +
            " LEFT JOIN FETCH ma.solicitud s " +
            " WHERE ma.municipio.codigo = :municipioCodigo and ma.archivoGeografico is not null";

        if (estado != null) {
                query += " AND ma.estado = :estado";
        }

        if (vigencia != null) {
            query += " AND ma.vigencia = :vigencia";
        }

        if (activo != null) {
            query += " AND ma.activo = :activo";
        }
        
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("municipioCodigo", municpioCodigo);

            if (estado != null) {
                q.setParameter("estado", estado);
            }

            if (vigencia != null) {
                q.setParameter("vigencia", vigencia);
            }

            if (activo != null) {
                if (activo) {
                    q.setParameter("activo", ESiNo.SI.getCodigo());
                } else {
                    q.setParameter("activo", ESiNo.NO.getCodigo());
                }
            }
            LOGGER.info("QUERYY " + query + " vigencia " + vigencia + " activo " +ESiNo.SI.getCodigo() );
            result = (List<MunicipioActualizacion>) (List<MunicipioActualizacion>) q.getResultList();


        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "MunicipioActualizacionDAOBean#obtenerPorMunicipioYEstadoUltimoGeografico");
        }

        return result;

    }

    /**
     * @author leidy.gonzalez
     * @see IMunicipioActualizacionDAO#obtenerEstadoTramitePorDepartamentoMunicipio(String)
     */
    @Override
    public List<MunicipioActualizacion> obtenerEstadoTramitePorDepartamentoMunicipio(
        String municpioCodigo, String departamentoCodigo) {

        List<MunicipioActualizacion> result = new ArrayList<MunicipioActualizacion>();

        String query = "SELECT ma FROM MunicipioActualizacion ma " +
            " JOIN FETCH ma.departamento d " +
            " JOIN FETCH ma.municipio m " +
            " WHERE m.codigo = :municipioCodigo " +
            " AND d.codigo = :departamentoCodigo " +
            " AND ma.estado = :estado";

        String estado = EMunicipioActualizacionEstado.RADICACION_EXITOSA.getCodigo();

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("municipioCodigo", municpioCodigo);
            q.setParameter("departamentoCodigo", departamentoCodigo);

            q.setParameter("estado", estado);

            result = (List<MunicipioActualizacion>) q.getResultList();

            for (MunicipioActualizacion municipioActualizacion : result) {
                if (municipioActualizacion.getDepartamento() != null) {
                    Hibernate.initialize(municipioActualizacion.getDepartamento());
                    if (municipioActualizacion.getDepartamento().getCodigo() != null) {
                        Hibernate.initialize(municipioActualizacion.getDepartamento().getCodigo());
                    }
                }
                if (municipioActualizacion.getMunicipio() != null) {
                    Hibernate.initialize(municipioActualizacion.getMunicipio());
                    if (municipioActualizacion.getMunicipio().getCodigo() != null) {
                        Hibernate.initialize(municipioActualizacion.getMunicipio().getCodigo());
                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "MunicipioActualizacionDAOBean#obtenerEstadoTramitePorDepartamentoMunicipio");
        }

        return result;
    }

    /**
     * @author felipe.cadena
     * @see IMunicipioActualizacionDAO#obtenerPorIdsTramites(String)
     */
    @Override
    public List<MunicipioActualizacion> obtenerPorIdsTramites(List<Long> idsTramites) {

        int intervalSize = 500;
        List<MunicipioActualizacion> result = new ArrayList<MunicipioActualizacion>();
        String query = "";

        ArrayList<List<Long>> splitIds = null;

        if (idsTramites.size() < intervalSize) {

            query = " SELECT DISTINCT ma FROM MunicipioActualizacion ma, Tramite t " +
                " WHERE t.solicitud.id = ma.solicitud.id" +
                " AND t.id IN :tramites";
        } else {

            int idxStart = 0;
            int idxFinal = intervalSize;

            int intervalsNum = idsTramites.size() / intervalSize;

            if ((idsTramites.size() % intervalSize) != 0) {
                intervalsNum++;
            }

            splitIds = new ArrayList<List<Long>>();

            for (int i = 1; i <= intervalsNum; i++) {
                if (idxFinal < idsTramites.size()) {
                    splitIds.add(idsTramites.subList(idxStart, idxFinal));
                } else {
                    splitIds.add(idsTramites.subList(idxStart, idsTramites.size()));
                }
                idxStart = idxFinal;
                idxFinal = idxFinal + intervalSize;

            }

            query = " SELECT DISTINCT ma FROM MunicipioActualizacion ma, Tramite t " +
                " WHERE t.solicitud.id = ma.solicitud.id" +
                " AND (";

            for (int i = 0; i < intervalsNum; i++) {
                query += " t.id IN (:tramite" + i + "s)";

                if (i != (intervalsNum - 1)) {
                    query += " OR ";
                } else {
                    query += ")";
                }
            }

        }

        try {
            Query q = this.entityManager.createQuery(query);
            if (idsTramites.size() < intervalSize) {
                q.setParameter("tramites", idsTramites);
            } else {
                int k = 0;
                for (List<Long> idsInterval : splitIds) {

                    q.setParameter("tramite" + k++ + "s", idsInterval);

                }
            }
            result = (List<MunicipioActualizacion>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "MunicipioActualizacionDAOBean#obtenerPorIdsTramites");
        }

        return result;
    }

}
