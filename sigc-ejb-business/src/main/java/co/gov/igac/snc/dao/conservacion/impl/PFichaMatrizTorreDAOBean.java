package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizTorreDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import java.util.ArrayList;
import org.hibernate.Hibernate;

@Stateless
public class PFichaMatrizTorreDAOBean extends GenericDAOWithJPA<PFichaMatrizTorre, Long>
    implements IPFichaMatrizTorreDAO {

    /**
     * @see IPFichaMatrizTorreDAO#findByFichaMatrizId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PFichaMatrizTorre> findByFichaMatrizId(Long fichaMatrizId) {
        String sql = "SELECT pfmt FROM PFichaMatrizTorre pfmt" +
            " WHERE pfmt.PFichaMatriz.id = :fichaMatrizId";
        Query query = this.entityManager.createQuery(sql);
        query.setParameter("fichaMatrizId", fichaMatrizId);
        return query.getResultList();
    }

    /**
     * @see IPFichaMatrizTorreDAO#buscarPFichaMatrizTorreporFichaMatrizId(Long)
     */
    @Override
    public List<PFichaMatrizTorre> buscarPFichaMatrizTorreporFichaMatrizId(Long fichaMatrizId) {
        List<PFichaMatrizTorre> resultado = new ArrayList<PFichaMatrizTorre>();

        String sql = "SELECT pfmt FROM PFichaMatrizTorre pfmt" +
            " WHERE pfmt.PFichaMatriz.id = :fichaMatrizId";
        Query query = this.entityManager.createQuery(sql);
        query.setParameter("fichaMatrizId", fichaMatrizId);

        resultado = query.getResultList();

        for (PFichaMatrizTorre pFichaMatrizTorre : resultado) {
            if (pFichaMatrizTorre.getProvieneTorre() != null &&
                pFichaMatrizTorre.getProvieneTorre().getTorre() != null) {
                Hibernate.initialize(pFichaMatrizTorre.getProvieneTorre().getTorre());
            }
        }

        return resultado;
    }

}
