package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 *
 */
@Local
public interface IManzanaVeredaDAO extends IGenericJpaDAO<ManzanaVereda, String> {

    public List<ManzanaVereda> findByBarrio(String barrioId);

    public ManzanaVereda getManzanaVeredaByCodigo(String codigo);

    /**
     * Obtener manzanas de un municipio
     *
     * @param codigoDepartamenteMunicipio
     * @return
     * @throws Exception
     * @author andres.eslava
     */
    public List<ManzanaVereda> obtenerManzanasPorMunicipio(String codigoDepartamentoMunicipio)
        throws Exception;

    /**
     * Obtener manzanas por rango
     *
     * @param manzanaInicial cadena con la manzana inicial del rango a buscar
     * @param manzanaFinal cadena con la manzana final del rango a buscar
     * @return
     * @author andres.eslava
     */
    public List<ManzanaVereda> obtenerManzanasPorRango(String manzanaInicial, String manzanaFinal);

    /**
     * Método para buscar Manzanas de un municipio para asignar coordinador por codigo municipio
     *
     * @author javier.barajas
     * @param codigo municipio
     * @param parametros de paginacion
     * @return Lista<String>
     * @version 1.0
     */
    public List<ManzanaVereda> obtenerManazanasporMunicipioAsignarCoordinadorPaginacion(
        String municipio, final int... rowStartIdxAndCount);

}
