package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IHorarioAtencionTerritorialDAO;

/**
 *
 * @author lorena.salamanca
 *
 */
@Stateless
public class HorarioAtencionTerritorialDAOBean extends GenericDAOWithJPA<HorarioAtencionTerritorial, String>
    implements IHorarioAtencionTerritorialDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        HorarioAtencionTerritorialDAOBean.class);

    /**
     * @see
     * IHorarioAtencionTerritorialDAO#buscarHorarioAtencionByEstructuraOrganizacional(java.lang.String)
     * @author lorena.salamanca
     * @param estructuraOrganizacionalCodigo
     * @return
     */
    @Override
    public HorarioAtencionTerritorial buscarHorarioAtencionByEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo) {
        LOGGER.debug(
            "HorarioAtencionTerritorialDAOBean#buscarHorarioAtencionByEstructuraOrganizacional");
        try {
            String query = "SELECT h FROM HorarioAtencionTerritorial h " +
                " WHERE h.estructuraOrganizacionalCod =  :estructuraOrganizacionalCodigo ";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("estructuraOrganizacionalCodigo", estructuraOrganizacionalCodigo);

            HorarioAtencionTerritorial h = (HorarioAtencionTerritorial) q.getSingleResult();
            return h;
        } catch (Exception e) {
            LOGGER.error("No existe horario para la territorial o UOC: " +
                estructuraOrganizacionalCodigo + e.getMessage());
            return null;
        }
    }

//end of class    
}
