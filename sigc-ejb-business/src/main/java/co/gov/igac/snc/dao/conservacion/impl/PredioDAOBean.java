/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.DetalleVisorVO;
import co.gov.igac.snc.vo.ListDetallesVisorVO;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class PredioDAOBean extends GenericDAOWithJPA<Predio, Long> implements IPredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioDAOBean.class);

//--------------------------------------------------------------------------------------------------    
    /**
     * @author fabio.navarrete
     * @see IPredioDAO#getPredioByNumeroPredial(String)
     */
    @Override
    public Predio getPredioByNumeroPredial(String numPredial) {
        LOGGER.debug("getPredioByNumeroPredial");
        Query q = this.entityManager.createNamedQuery("findPredioByNumeroPredial");
        q.setParameter("numeroPredial", numPredial);
        try {
            Predio p = (Predio) q.getSingleResult();
            return p;
        } catch (Exception e) {
            LOGGER.error("Error en obtener el predio: " + e.getMessage());
            return null;
        }
    }

    /**
     * @author felipe.cadena
     * @see IPredioDAO#obtenerPorRaizNumeroPredial(String)
     */
    @Override
    public List<Predio> obtenerPorRaizNumeroPredial(String raizNumPredial) {
        LOGGER.debug("On obtenerPorRaizNumeroPredial");

        List<Predio> resulPredios;
        String queryString = "SELECT p FROM Predio p JOIN FETCH p.departamento " +
            " JOIN FETCH p.municipio " +
            " WHERE p.numeroPredial LIKE :numeroPredial";

        Query q = this.entityManager.createQuery(queryString);
        q.setParameter("numeroPredial", raizNumPredial + "%");
        try {
            resulPredios = q.getResultList();

            for (Predio resulPredio : resulPredios) {
                Hibernate.initialize(resulPredio.getPredioDireccions());
            }
            return resulPredios;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#getPredioFetchPersonasById");
        }
    }
//-------------------------------------------------------------------------------------------------

    /**
     * Retorna la lista de Predios que cumplen con los criterios de búsqueda que vienen dados en el
     * parámetro
     *
     * @author pedro.garcia
     * @param isCount indica si la consulta se usa solo para contar el número de registros
     * @param datosConsultaPredio Objeto cuyos valores son los que se van a usar en la búsqueda de
     * Predios
     * @param from índice del primer resultado (de la lista de resultados) que se va a devolver.
     * @param howMany máximo número de registros que se van a devolver. Si = -1 => se devuelven
     * todos
     * @return Lista de objetos. Cada uno es el nombre del departamento, del municipio, y el predio
     * con todos sus campos ([2] = id del predio ...)
     *
     * @deprecated ahora se está usando el procedimiento almacenado. Si se quiere volver a usar esto
     * hay que arreglar lo que está comentado
     */
    @SuppressWarnings({"unchecked", "unused"})
    @Deprecated
    private List<Object[]> searchForPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, boolean isCount,
        int from, int howMany) {

        LOGGER.debug("getPrediosBySearchCriteria");

        Query query;
        String constraintData, queryData, wildcard;
        String numeroPredial, direccionPredio, nombrePropietario, identificacionPropietario;
        String numeroRegistro, chip, usoConstruccion, destinoPredio, razonSocialPropietario = null;
        StringBuilder queryToExecute;
        double areaTerrenoMin, areaTerrenoMax, areaConstruidaMin, areaConstruidaMax;
        BigDecimal consecutivoCatastral;

        List<Object[]> answer = null;
        boolean useNumeroPredial, useNombrePropietario, useDireccionPredio, useIdentifPropietario;
        boolean useNumeroRegistro, useConsecutivoCatastral, useChip, useUsoConstruccion, useDestinoPredio;
        boolean useAreaTerrenoMin, useAreaTerrenoMax, useAreaConstruidaMin, useAreaConstruidaMax;
        boolean useRazonSocialPropietario;

        wildcard = "%";
        queryToExecute = new StringBuilder();
        useNumeroPredial = false;
        useNombrePropietario = false;
        useRazonSocialPropietario = false;
        useDireccionPredio = false;
        useIdentifPropietario = false;
        useNumeroRegistro = false;
        useChip = false;
        useUsoConstruccion = false;
        useDestinoPredio = false;
        useAreaTerrenoMin = false;
        useAreaTerrenoMax = false;
        useAreaConstruidaMin = false;
        useAreaConstruidaMax = false;

        armado_query:
            {
                // OJO: los campos que pueden ser nulos en la bd deben ser
                // comparados con null en la consulta
                // OJO: el query se confundía si no se usan alias para D.nombre y
                // M.nombre, y traía la misma columna dos veces
                // D: si solo se quiere contar el número de filas que retorna la
                // consulta (para la paginación), la primera
                // parte del query es diferente
                if (isCount) {
                    queryToExecute.append("select count(1) ");
                } else {
                    queryToExecute
                        .append("select D.Nombre depto, M.Nombre muni, P.* ");
                }
                queryToExecute
                    .append(" From Predio P, Departamento D, Municipio M " +
                        " where P.municipio_codigo = M.codigo and P.Departamento_codigo = D.codigo " +
                        // + " and P.territorial_codigo like :tid " +
                        " and p.MUNICIPIO_CODIGO in (" +
                        " select j.MUNICIPIO_CODIGO from jurisdiccion j " +
                        " where j.ESTRUCTURA_ORGANIZACIONAL_COD like :tid UNION " +
                        " select j.MUNICIPIO_CODIGO from estructura_organizacional eo, jurisdiccion j " +

                        " where j.ESTRUCTURA_ORGANIZACIONAL_COD=eo.codigo and eo.ESTRUCTURA_ORGANIZACIONAL_COD like :tid ) " +
                        " and P.departamento_codigo like :did " +
                        " and P.municipio_codigo like :mid");

                numeroPredial = datosConsultaPredio.getNumeroPredial();
                if (numeroPredial != null && numeroPredial.compareTo("") != 0) {
                    queryToExecute.append(" and P.NUMERO_PREDIAL LIKE :numPredial");
                    useNumeroPredial = true;
                }

                direccionPredio = datosConsultaPredio.getDireccionPredio();
                if (direccionPredio != null && direccionPredio.compareTo("") != 0) {
                    queryToExecute
                        .append(" and P.ID IN (select distinct(PREDIO_ID) " +
                            " from PREDIO_DIRECCION where DIRECCION_ESTANDAR like :dirPredio)");
                    useDireccionPredio = true;
                }

                nombrePropietario = datosConsultaPredio.getNombrePropietario();
                if (nombrePropietario != null &&
                    nombrePropietario.compareTo("") != 0) {
                    queryToExecute
                        .append(" AND P.ID IN (select distinct(PerPre.PREDIO_ID) " +
                            " FROM PERSONA_PREDIO PerPre " +
                            " WHERE PerPre.PERSONA_ID = (select Per.ID FROM PERSONA Per" +
                            " WHERE Per.Primer_Nombre || ' ' || Per.Segundo_Nombre || ' ' || " +
                            " Per.Primer_Apellido || ' ' || Per.Segundo_Apellido" +
                            " like :propName))");
                    useNombrePropietario = true;
                }

                if (datosConsultaPredio.getRazonSocialPropietario() != null &&
                    datosConsultaPredio.getRazonSocialPropietario().compareTo("") != 0) {
                    razonSocialPropietario = datosConsultaPredio.getRazonSocialPropietario();
                    useRazonSocialPropietario = true;
                    queryToExecute.append(" AND AND P.ID IN (select distinct(PerPre.PREDIO_ID) " +
                        " FROM PERSONA_PREDIO PerPre " +
                        " WHERE PerPre.PERSONA_ID = (select Per.ID FROM PERSONA Per" +
                        " WHERE Per.razon_social LIKE :propRazonSocial))");
                }

                identificacionPropietario = datosConsultaPredio
                    .getIdentificacionPropietario();
                if (identificacionPropietario != null &&
                    identificacionPropietario.compareTo("") != 0) {
                    queryToExecute
                        .append(
                            " and P.ID in (select distinct(PerPre.PREDIO_ID) from PERSONA_PREDIO PerPre " +
                            " where PerPre.PERSONA_ID = (select Per.ID from PERSONA Per" +
                            " where Per.NUMERO_IDENTIFICACION like :propIdentif))");
                    useIdentifPropietario = true;
                }

                // D: lo de la búsqueda avanzada
                numeroRegistro = datosConsultaPredio.getNumeroRegistro();
                consecutivoCatastral = datosConsultaPredio.getConsecutivoCatastral();
                chip = datosConsultaPredio.getNip();

//OJO: estos ahora son arreglos de Strings
//			destinoPredio = datosConsultaPredio.getDestinosPredioIds();
//			usoConstruccion = datosConsultaPredio.getUsosConstruccionIds();
                areaTerrenoMin = datosConsultaPredio.getAreaTerrenoMin();
                areaTerrenoMax = datosConsultaPredio.getAreaTerrenoMax();
                areaConstruidaMin = datosConsultaPredio.getAreaConstruidaMin();
                areaConstruidaMax = datosConsultaPredio.getAreaConstruidaMax();

                if (numeroRegistro != null && numeroRegistro.compareTo("") != 0) {
                    queryToExecute.append(" and P.NUMERO_REGISTRO like :numReg ");
                    useNumeroRegistro = true;
                }
//			if (consecutivoCatastral != null
//					&& consecutivoCatastral.compareTo(BigDecimal.ZERO) != 0) {
//				queryToExecute
//						.append(" and P.CONSECUTIVO_CATASTRAL like :consecutCat ");
//				useConsecutivoCatastral = true;
//			}
                if (chip != null && chip.compareTo("") != 0) {
                    queryToExecute.append(" and P.CHIP like :chip");
                    useChip = true;
                }
//			if (destinoPredio != null && destinoPredio.compareTo("") != 0) {
//				queryToExecute.append(" and P.DESTINO = :destinoP");
//				useDestinoPredio = true;
//			}
//			if (usoConstruccion != null && usoConstruccion.compareTo("") != 0) {
//				queryToExecute
//						.append(" and P.ID in (select UC.PREDIO_ID from UNIDAD_CONSTRUCCION UC where"
//								+ " UC.USO_ID = :usoConstr)");
//				useUsoConstruccion = true;
//			}
                // TODO verificar qué valores llegan aquí cuando no se ha escogido
                // nada, y refinar if
                if (areaTerrenoMin != 0) {
                    queryToExecute.append(" and P.AREA_TERRENO >= :areaTerrenoMin");
                    useAreaTerrenoMin = true;
                }
                if (areaTerrenoMax != 0) {
                    queryToExecute.append(" and P.AREA_TERRENO <= :areaTerrenoMax");
                    useAreaTerrenoMax = true;
                }
                if (areaConstruidaMin != 0) {
                    queryToExecute
                        .append(" and P.Id In (select predioId from( " +
                            " Select Sum(Uc.Area_Construida) As Suma, Uc.Predio_Id as predioId" +
                            " From Unidad_Construccion Uc " +
                            " Group By Uc.Predio_Id Having Sum(Uc.Area_Construida) >= :areaConstrMin))");
                    useAreaConstruidaMin = true;
                }
                if (areaConstruidaMax != 0) {
                    queryToExecute
                        .append(" and P.Id In (select predioId from( " +
                            " Select Sum(Uc.Area_Construida) As Suma, Uc.Predio_Id as predioId" +
                            " From Unidad_Construccion Uc " +
                            " Group By Uc.Predio_Id Having Sum(Uc.Area_Construida) <= :areaConstrMax))");
                    useAreaConstruidaMax = true;
                }

            }// end armado_query

        query = entityManager.createNativeQuery(queryToExecute.toString());

        set_parameters:
            {
                constraintData = datosConsultaPredio.getTerritorialId();
                queryData = (constraintData != null && constraintData.compareTo("") != 0) ?
                    constraintData : wildcard;
                query.setParameter("tid", queryData);

                constraintData = datosConsultaPredio.getDepartamentoId();
                queryData = (constraintData != null && constraintData.compareTo("") != 0) ?
                    constraintData : wildcard;
                query.setParameter("did", queryData);

                constraintData = datosConsultaPredio.getMunicipioId();
                queryData = (constraintData != null && constraintData.compareTo("") != 0) ?
                    constraintData : wildcard;
                query.setParameter("mid", queryData);

                if (useNumeroPredial) {
                    query.setParameter("numPredial", numeroPredial + "%");
                }
                if (useDireccionPredio) {
                    String direccionNormalizada;
                    direccionNormalizada = QueryNativoDAO.normalizarDireccion(
                        this.getEntityManager(), direccionPredio);
                    // D: OJO: cuando se usa el comparador like, para que funcione
                    // se debe usar el valor entre %valor% y debe ser concatendo en el momento de
                    // definir el valor del parámetro, no en la definición del query
                    query.setParameter("dirPredio", "%" + direccionNormalizada + "%");
                }
                if (useNombrePropietario) {
                    query.setParameter("propName", "%" + nombrePropietario + "%");
                }
                if (useRazonSocialPropietario) {
                    query.setParameter("propRazonSocial", "%" + razonSocialPropietario + "%");
                }
                if (useIdentifPropietario) {
                    query.setParameter("propIdentif", identificacionPropietario);
                }

                if (useNumeroRegistro) {
                    query.setParameter("numReg", numeroRegistro);
                }
//			if (useConsecutivoCatastral) {
//				query.setParameter("consecutCat", consecutivoCatastral);
//			}
                if (useChip) {
                    query.setParameter("chip", chip);
                }
//			if (useDestinoPredio) {
//				query.setParameter("destinoP", destinoPredio);
//			}
//			if (useUsoConstruccion) {
//				query.setParameter("usoConstr", usoConstruccion);
//			}
                if (useAreaTerrenoMin) {
                    query.setParameter("areaTerrenoMin", areaTerrenoMin);
                }
                if (useAreaTerrenoMax) {
                    query.setParameter("areaTerrenoMax", areaTerrenoMax);
                }
                if (useAreaConstruidaMin) {
                    query.setParameter("areaConstrMin", areaConstruidaMin);
                }
                if (useAreaConstruidaMax) {
                    query.setParameter("areaConstrMax", areaConstruidaMax);
                }

            }// end set_parameters

        try {
            query.setFirstResult(from);
            if (howMany > 0) {
                query.setMaxResults(howMany);
            }

            if (isCount) {
                Object conteo = query.getSingleResult();
                answer = new ArrayList<Object[]>();
                Object[] conteoArray = new Object[]{conteo};
                answer.add(conteoArray);
            } else {
                answer = (List<Object[]>) query.getResultList();
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("PredioDAOBean#searchPredios: " +
                "La consulta de predios no devolvió resultados");
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de predios");
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     * @see IPredioDAO#getPredioFetchPersonasByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchPersonasByNumeroPredial(String numPredial) {
        LOGGER.debug("getPredioFetchPersonasByNumeroPredial");
        String query = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.personaPredios" +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numPredial);
        try {
            Predio p = (Predio) q.getSingleResult();

            // Inicializacion de los bloqueos asociados a la persona
            for (int i = 0; i < p.getPersonaPredios().size(); i++) {
                p.getPersonaPredios().get(i).getPersona().getPersonaBloqueos()
                    .size();
                p.getPersonaPredios().get(i).getPersonaPredioPropiedads()
                    .size();
            }
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IPredioDAO#getPredioFetchPersonasById(Long)
     */
    @Override
    // TODO :: juan.agudelo :: 21-03-2012 :: parece que a este método le metió
    // más fetch de cosas que no se necesitan solo para poder usarlo donde no
    // estaba pensao usarlo :: pedro.garcia
    public Predio getPredioFetchPersonasById(Long predioId) {

        LOGGER.debug("PredioDAOBean#getPredioFetchPersonasById");

        Predio p = null;
        String query = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.personaPredios perPre" +
            " JOIN FETCH p.departamento" + " JOIN FETCH p.municipio" +
            " WHERE p.id = :predioId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("predioId", predioId);

            p = (Predio) q.getSingleResult();

            // Inicializacion de los bloqueos asociados a la persona
            for (int i = 0; i < p.getPersonaPredios().size(); i++) {
                p.getPersonaPredios().get(i).getPersona().getPersonaBloqueos()
                    .size();
                p.getPersonaPredios().get(i).getPersonaPredioPropiedads()
                    .size();

                for (PersonaPredioPropiedad ppp : p.getPersonaPredios().get(i).
                    getPersonaPredioPropiedads()) {
                    Hibernate.initialize(ppp.getDocumentoSoporte());
                    Hibernate.initialize(ppp.getDepartamento());
                    Hibernate.initialize(ppp.getMunicipio());
                }
            }

            for (int j = 0; j < p.getPredioDireccions().size(); j++) {
                p.getPredioDireccions().get(j).getId();
            }
            for (int k = 0; k < p.getPredioServidumbres().size(); k++) {
                p.getPredioServidumbres().get(k).getId();
            }
            for (int h = 0; h < p.getReferenciaCartograficas().size(); h++) {
                p.getReferenciaCartograficas().get(h).getId();
            }

            return p;
        } catch (NoResultException nre) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nre, "PredioDAOBean#getPredioFetchPersonasById", "Predio", predioId.toString());
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#getPredioFetchPersonasById");
        } finally {
            return p;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @see IPredioDAO#getPredioFetchAvaluosByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchAvaluosByNumeroPredial(String numPredial) {
        String query = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.unidadConstruccions uc" +
            " LEFT JOIN FETCH uc.usoConstruccion" +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numPredial);
        try {
            Predio p = (Predio) q.getSingleResult();
            p.getPredioZonas().size();
            p.getPredioAvaluoCatastrals().size();
            Hibernate.initialize(p.getUnidadConstruccions());
            if (p.getUnidadConstruccions() != null) {
                for (UnidadConstruccion uc : p.getUnidadConstruccions()) {
                    Hibernate.initialize(uc.getUnidadConstruccionComps());
                }
            }
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage(), e);
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @see IPredioDAO#getPredioFetchDerechosPropiedadByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchDerechosPropiedadByNumeroPredial(
        String numPredial) {
        String query = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.personaPredios" +
            " JOIN FETCH p.departamento JOIN FETCH p.municipio" +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numPredial);

        try {
            Predio p = (Predio) q.getSingleResult();

            for (PersonaPredio perPre : p.getPersonaPredios()) {
                perPre.getPersonaPredioPropiedads().size();
            }
            return p;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#getPredioFetchDerechosPropiedadByNumeroPredial");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * TODO:Documentar Método -> Recordar adjuntar autor
     */
    @Implement
    @Override
    public Predio getPredioById(Long id) {
        LOGGER.debug("getPredioByIdPredio");
        Predio p = this.entityManager.find(Predio.class, id);
        return p;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @see IPredioDAO#getPredioFetchDatosUbicacionByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchDatosUbicacionByNumeroPredial(String numPredial) {
        String query = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.predioDireccions" +
            " JOIN FETCH p.departamento JOIN FETCH p.municipio" +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numPredial);

        try {
            Predio p = (Predio) q.getSingleResult();
            p.getPredioServidumbres().size();
            p.getPredioZonas().size();
            p.getReferenciaCartograficas().size();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#getPredioFetchDatosUbicacionById(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Predio getPredioFetchDatosUbicacionById(Long predioId) {

        LOGGER.debug("on PredioDAOBean#getPredioFetchDatosUbicacionById ...");

        Predio answer = null;
        String query = "SELECT p FROM Predio p " +
            " JOIN FETCH p.departamento JOIN FETCH p.municipio " +
            " LEFT JOIN FETCH p.circuloRegistral cr " +
            " WHERE p.id = :predioIdP";
        Query q = this.entityManager.createQuery(query);
        q.setParameter("predioIdP", predioId);

        try {
            answer = (Predio) q.getSingleResult();

            //D: forzar a que traiga los objetos Direccion
            for (int j = 0; j < answer.getPredioDireccions().size(); j++) {
                answer.getPredioDireccions().get(j).getId();
            }
            //D: las direcciones deben estar ordenadas por la principal (aparece primero)
            Collections.sort(answer.getPredioDireccions(),
                PredioDireccion.getComparatorDirPrincipal(false));

            for (int k = 0; k < answer.getPredioServidumbres().size(); k++) {
                answer.getPredioServidumbres().get(k).getId();
            }
            for (int h = 0; h < answer.getReferenciaCartograficas().size(); h++) {
                answer.getReferenciaCartograficas().get(h).getId();
            }
            for (int l = 0; l < answer.getPredioBloqueos().size(); l++) {
                answer.getPredioBloqueos().get(l).getId();
            }
            if (answer.getCirculoRegistral() != null) {
                answer.getCirculoRegistral().getNombre();
            }
            // Adición de if al para no traer los predios
            // bloqueados en lazy. Se realizó debido a que en la entidad Predio
            // el método isEstaBloqueado() necesita que el predio tenga el fetch
            // de los predios bloqueados, y éste es el método que consulta el 
            // predio en la pantalla de detalle del predio.
            if (answer.getPredioBloqueos() != null) {
                answer.getPredioBloqueos().size();
            }

        } catch (NoResultException ex) {
            LOGGER.error("No se encuentran resultados para la consulta: ");
        } catch (Exception ex) {
            LOGGER.error("Excepción buscando datos de ubicación de predio: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, ex, "PredioDAOBean#getPredioFetchDatosUbicacionById");
        }

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPredioDAO#getPredioDatosFetchPersonasByNumeroPredial(String)
     */
    @Override
    public Predio getPredioDatosFetchPersonasByNumeroPredial(String numPredial) {
        LOGGER.debug("getPredioFetchPersonasByNumeroPredial");

        Predio p = null;

        try {
            Query q = this.entityManager.createQuery("SELECT pd" + " FROM Predio pd" +
                " JOIN FETCH pd.departamento" + " JOIN FETCH pd.municipio" +
                " WHERE pd.numeroPredial = :numeroPredial");
            q.setParameter("numeroPredial", numPredial);
            p = (Predio) q.getSingleResult();
            p.getPersonaPredios().size();
        } catch (NoResultException nre) {
            LOGGER.warn("No se encontró un predio con número predial " + numPredial);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#getPredioDatosFetchPersonasByNumeroPredial");
        } finally {
            return p;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#searchForPrediosCount(co.gov.igac.snc.util.FiltroDatosConsultaPredio)
     * @author pedro.garcia El costo de hacer la consulta completa comparado el costo de hacer solo
     * el count es casi el mismo
     */
    @Implement
    @Override
    public int searchForPrediosCount(
        FiltroDatosConsultaPredio datosConsultaPredio) {
        int answer = 0;
        List<Object[]> predios;
        BigDecimal rowCount;
        BigInteger rowCountI;

        predios = searchForPredios(datosConsultaPredio, true, 0, -1);

        // D: de la consulta llega un big decimal como respuesta al count
        rowCount = (BigDecimal) predios.get(0)[0];
        rowCountI = rowCount.toBigInteger();
        answer = rowCountI.intValue();

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#searchForPredios(co.gov.igac.snc.util.FiltroDatosConsultaPredio)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Object[]> searchForPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, int from, int howMany) {
        return this.searchForPredios(datosConsultaPredio, false, from, howMany);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#getPredioFetchAvaluosByPredioId(String)
     * @author fabio.navarrete
     */
    /*
     * @modified juan.agudelo @modified
     */
    @Override
    public Predio getPredioFetchAvaluosByPredioId(Long predioId) {

        String query = "SELECT p" + " FROM Predio p" +
            " LEFT JOIN FETCH p.unidadConstruccions uc" +
            " LEFT JOIN FETCH uc.usoConstruccion" +
            " WHERE p.id = :predioId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("predioId", predioId);

            Predio p = (Predio) q.getSingleResult();
            p.getPredioZonas().size();
            p.getPredioAvaluoCatastrals().size();

            if (p.getUnidadConstruccions() != null &&
                !p.getUnidadConstruccions().isEmpty()) {

                for (UnidadConstruccion ucTmp : p.getUnidadConstruccions()) {

                    if (ucTmp.getUnidadConstruccionComps() != null &&
                        !ucTmp.getUnidadConstruccionComps().isEmpty()) {
                        ucTmp.getUnidadConstruccionComps().size();
                    }
                }
            }

            return p;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, "PredioDAOBean#getPredioFetchAvaluosByPredioId");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#findPredioFetchPersonasByPredioId(java.lang.Long)
     * @modified pedro.garcia documentación
     */
    @Override
    public Predio findPredioFetchPersonasByPredioId(Long predioId) {
        LOGGER.debug("getPredioFetchPersonasByNumeroPredial");

        String query = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.personaPredios" +
            " WHERE p.id = :predioId";
        Query q = this.entityManager.createQuery(query);
        q.setParameter("predioId", predioId);
        try {
            Predio p = (Predio) q.getSingleResult();

            // Inicializacion de los bloqueos asociados a la persona
            for (int i = 0; i < p.getPersonaPredios().size(); i++) {
                p.getPersonaPredios().get(i).getPersona().getPersonaBloqueos()
                    .size();
                p.getPersonaPredios().get(i).getPersonaPredioPropiedads()
                    .size();
            }
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPredioDAO#actualizarPrediosDesbloqueo(UsuarioDTO, List)
     */
    public List<Predio> actualizarPrediosDesbloqueo(UsuarioDTO usuario,
        List<Predio> prediosBloqueados) {

        List<Predio> prediosDesbloqueados = new ArrayList<Predio>();

        try {
            for (int p = 0; p < prediosBloqueados.size(); p++) {
                Predio predioTemp = prediosBloqueados.get(p);

                // TODO q se debe hacer con el usuario???
                update(predioTemp);
                prediosDesbloqueados.add(predioTemp);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return prediosDesbloqueados;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#findByNumeroPredial(java.lang.String)
     */
    @Override
    public Predio findByNumeroPredial(String numeroPredial) {
        LOGGER.debug("on PredioDAOBean#findByNumeroPredial...");

        Predio answer = null;
        List<Predio> predios;
        String queryString;
        Query query;

        queryString = "SELECT p FROM Predio p" + " JOIN FETCH p.departamento" +
            " JOIN FETCH p.municipio " +
            " WHERE p.numeroPredial = :numPredial";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("numPredial", numeroPredial);

        try {
            predios = query.getResultList();
            if (predios.size() > 0) {
                answer = predios.get(0);
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
        }

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IPredioDAO#getPredioUnidadConstruccion(String)
     */
    public Predio getPredioUnidadConstruccion(String numPredial) {
        String query = "SELECT p" +
            " FROM Predio p JOIN FETCH p.unidadConstruccion uc" +
            " JOIN FETCH p.departamento JOIN FETCH p.municipio" +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numPredial);

        try {
            Predio p = (Predio) q.getSingleResult();

            for (PersonaPredio perPre : p.getPersonaPredios()) {
                perPre.getPersonaPredioPropiedads().size();
            }
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#getPredioPorNumeroPredial(String)
     */
    @Override
    public Predio getPredioPorNumeroPredial(String numeroPredial) {
        LOGGER.debug("getPredioPorNumeroPredial");

        String query = "SELECT p" + " FROM Predio p " +
            " WHERE p.numeroPredial = :numeroPredial";

        Query q = this.entityManager.createQuery(query);

        try {
            q.setParameter("numeroPredial", numeroPredial);
            Predio p = (Predio) q.getSingleResult();
            return p;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see co.gov.igac.snc.dao.conservacion.IPredioDAO#existePredioPorNumeroPredial
     * (java.lang.String)
     */
    @Override
    public boolean existePredioPorNumeroPredial(String numeroPredial) {
        // LOGGER.debug("existePredioPorNumeroPredial");
        boolean existe = false;
        String query = "SELECT count(p.id)  FROM Predio p " +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numeroPredial);
        try {
            Long conteo = (Long) q.getSingleResult();
            if (conteo > 0) {
                existe = true;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return existe;
    }

    @Override
    public List<Predio> getPrediosPorCodigo(String codigo) {
        LOGGER.debug("getPrediosPorCodigo");

        String query = "SELECT p" + " FROM Predio p " +
            " JOIN FETCH p.departamento " + " JOIN FETCH p.municipio " +
            " LEFT JOIN FETCH p.unidadConstruccions u " +
            " LEFT JOIN FETCH u.usoConstruccion " +
            " WHERE p.numeroPredial LIKE :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", codigo + "%");
        try {
            List<Predio> predios = (List<Predio>) q.getResultList();
            for (Predio p : predios) {
                p.getPredioDireccions().size();
                p.getPredioZonas().size();
            }
            return predios;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    @Override
    public List<Predio> getPrediosPorMunicipioYMatricula(String municipioCod,
        String matriculaInm) {
        LOGGER.debug("getPrediosPorMunicipioYMatricula");

        String query = "SELECT p" + " FROM Predio p " +
            " JOIN FETCH p.departamento " + " JOIN FETCH p.municipio m " +
            " LEFT JOIN FETCH p.unidadConstruccions u " +
            " LEFT JOIN FETCH u.usoConstruccion " +
            " WHERE m.codigo =:codigo " +
            " AND p.numeroRegistro =:numeroRegistro";
        Query q = entityManager.createQuery(query);
        q.setParameter("codigo", municipioCod);
        q.setParameter("numeroRegistro", matriculaInm);
        try {
            List<Predio> predios = (List<Predio>) q.getResultList();
            for (Predio p : predios) {
                p.getPredioDireccions().size();
                p.getPredioZonas().size();
            }
            return predios;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }

    }

    @Override
    public List<Predio> getPrediosPorMunicipioYDireccion(String municipioCod,
        String direccion) {
        LOGGER.debug("getPrediosPorMunicipioYDireccion");
        String direccionNormalizada = QueryNativoDAO.normalizarDireccion(
            this.getEntityManager(), direccion);

        String query = "SELECT p" + " FROM Predio p " +
            " JOIN FETCH p.departamento " + " JOIN FETCH p.municipio m " +
            " LEFT JOIN FETCH p.unidadConstruccions u " +
            " LEFT JOIN FETCH u.usoConstruccion " +
            " WHERE m.codigo =:codigo " + " AND p.id IN " +
            " (SELECT DISTINCT pd.predio.id FROM PredioDireccion pd " +
            " WHERE pd.direccionEstandar = :direccion)";
        Query q = entityManager.createQuery(query);
        q.setParameter("codigo", municipioCod);
        q.setParameter("direccion", direccionNormalizada);
        try {
            List<Predio> predios = (List<Predio>) q.getResultList();
            for (Predio p : predios) {
                p.getPredioDireccions().size();
                p.getPredioZonas().size();
            }
            return predios;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    @Override
    public Set<Predio> findAllReturnSet() {
        Set<Predio> predios = new HashSet<Predio>();
        String sql = "SELECT p FROM Predio p";
        Query q = this.entityManager.createQuery(sql);
        q.setMaxResults(200);
        LOGGER.debug("////////////Pre-execute\\\\\\\\\\\\");
        List<Predio> listaPredios = q.getResultList();
        LOGGER.debug("////////////Post-execute\\\\\\\\\\\\");
        predios = new HashSet<Predio>(listaPredios);
        return predios;
    }
//--------------------------------------------------------------------------------------------------
//TODO :: franz.gamba :: es ineficiente usar primero un método para determinar si el predi existe y
//  luego consultarlo. :: pedro.garcia

    @Override
    public List<Predio> getPrediosPorNumerosPrediales(String numPrediales) {
        // LOGGER.debug("getPrediosPorNumerosPrediales");
        String[] np = numPrediales.split(",");

        List<Predio> predios = new ArrayList<Predio>();
        try {
            for (String codigoPredio : np) {
                if (existePredioPorNumeroPredial(codigoPredio)) {
                    String query = "SELECT p" + " FROM Predio p " +
                        " JOIN FETCH p.departamento " +
                        " JOIN FETCH p.municipio " +
                        " LEFT JOIN FETCH p.unidadConstruccions u " +
                        " LEFT JOIN FETCH u.usoConstruccion " +
                        " WHERE p.numeroPredial =:numeroPredial";
                    Query q = entityManager.createQuery(query);
                    q.setParameter("numeroPredial", codigoPredio);
                    Predio predio = (Predio) q.getSingleResult();
                    if (predio != null) {
                        predio.getPredioDireccions().size();
                        predio.getPredioZonas().size();
                        predios.add(predio);
                    }

                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predios;
    }

//--------------------------------------------------------------------------------------------------
    @Override
    public ListDetallesVisorVO getPrediosPorNumerosPredialesVisor(String numPrediales) {

        LOGGER.debug("getPrediosPorNumerosPredialesVisor");
        String[] np = numPrediales.split(",");

        List<Predio> predios = new ArrayList<Predio>();
        ListDetallesVisorVO listaDetalles;
        try {
            listaDetalles = new ListDetallesVisorVO();
            List<DetalleVisorVO> lista = new LinkedList<DetalleVisorVO>();
            List<String> prediosNulos = new LinkedList<String>();

            for (String codigoPredio : np) {
                if (existePredioPorNumeroPredial(codigoPredio)) {
                    String query = "SELECT p" + " FROM Predio p " +
                        " JOIN FETCH p.departamento " +
                        " JOIN FETCH p.municipio " +
                        " LEFT JOIN FETCH p.unidadConstruccions u " +
                        " LEFT JOIN FETCH u.usoConstruccion " +
                        " WHERE p.numeroPredial =:numeroPredial";
                    Query q = this.entityManager.createQuery(query);
                    q.setParameter("numeroPredial", codigoPredio);
                    Predio predio = (Predio) q.getSingleResult();
                    if (predio != null) {
                        predio.getPredioDireccions().size();
                        predio.getPredioZonas().size();
                        predios.add(predio);

                        DetalleVisorVO detalle = new DetalleVisorVO(predio);
                        lista.add(detalle);
                    }

                } else {
                    prediosNulos.add(codigoPredio);
                }
            }
            listaDetalles.setListaDetalles(lista);
            listaDetalles.setPrediosNulos(prediosNulos);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return listaDetalles;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo que recibe una direccion y la retorna normalizada.
     *
     * @author david.cifuentes
     * @see IPredioDAO#normalizarDireccion(String)
     */
    @Override
    public String normalizarDireccion(String direccion) {

        // La normalización de la dirección fue implementada por juan.mendez en
        // QueryNativoDAO.
        String direccionNormalizada;
        try {
            direccionNormalizada = QueryNativoDAO.normalizarDireccion(
                this.getEntityManager(), direccion);
            return direccionNormalizada;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return "";
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#getAreasCatastralesPorNumeroPredial
     * @author javier.aponte
     */
    @Override
    public double[] getAreasCatastralesPorNumeroPredial(String numeroPredial) {

        double[] resultado = new double[2];
        String query = "SELECT p " + " FROM Predio p " +
            " WHERE p.numeroPredial = :numeroPredial ";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numeroPredial);
        try {
            Predio p = (Predio) q.getSingleResult();

            resultado[0] = 0.0;
            for (PredioZona predZona : p.getPredioZonas()) {
                resultado[0] += predZona.getArea();
            }

            resultado[1] = 0.0;

            for (UnidadConstruccion unidad : p
                .getUnidadesConstruccionConvencional()) {
                resultado[1] += unidad.getAreaConstruida();
            }

            for (UnidadConstruccion unidad : p
                .getUnidadesConstruccionNoConvencional()) {
                resultado[1] += unidad.getAreaConstruida();
            }

            return resultado;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see
     * IPredioDAO#buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(FiltroDatosConsultaPrediosBloqueo)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Predio> buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo) {

        LOGGER.debug("executing PredioDAOBean#buscarPredioBloqueoValido");

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT pbp FROM Predio pbp" +
            " LEFT JOIN FETCH pbp.predioBloqueos pb" +
            " JOIN pbp.departamento" +
            " JOIN pbp.municipio" +
            " WHERE 1 = 1");

        if (filtroDatosConsultaPredioBloqueo.getDepartamento() != null) {
            query.append(" AND pbp.departamento= :departamento");
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipio() != null) {
            query.append(" AND pbp.municipio= :municipio");
        }
        if (filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo()
                .isEmpty()) {
            query.append(" AND pbp.departamento.codigo= :departamentoCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getMunicipioCodigo()
                .isEmpty()) {
            query.append(" AND pbp.municipio.codigo= :municipioCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getTipoAvaluo() != null &&
            !filtroDatosConsultaPredioBloqueo.getTipoAvaluo().isEmpty()) {
            query.append(" AND pbp.tipoAvaluo= :tipoAvaluo");
        }
        if (filtroDatosConsultaPredioBloqueo.getSectorCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getSectorCodigo()
                .isEmpty()) {
            query.append(" AND pbp.sectorCodigo= :sectorCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getComunaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getComunaCodigo()
                .isEmpty()) {
            query.append(" AND pbp.barrioCodigo like :comunaCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getBarrioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getBarrioCodigo()
                .isEmpty()) {
            query.append(" AND pbp.barrioCodigo like :barrioCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo()
                .isEmpty()) {
            query.append(" AND pbp.manzanaCodigo= :manzanaCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getPredio() != null &&
            !filtroDatosConsultaPredioBloqueo.getPredio().isEmpty()) {
            query.append(" AND pbp.predio= :predio");
        }
        if (filtroDatosConsultaPredioBloqueo.getCondicionPropiedad() != null &&
            !filtroDatosConsultaPredioBloqueo.getCondicionPropiedad()
                .isEmpty()) {
            query.append(" AND pbp.condicionPropiedad= :condicionPropiedad");
        }
        if (filtroDatosConsultaPredioBloqueo.getEdificio() != null &&
            !filtroDatosConsultaPredioBloqueo.getEdificio().isEmpty()) {
            query.append(" AND pbp.edificio= :edificio");
        }
        if (filtroDatosConsultaPredioBloqueo.getPiso() != null &&
            !filtroDatosConsultaPredioBloqueo.getPiso().isEmpty()) {
            query.append(" AND pbp.piso= :piso");
        }
        if (filtroDatosConsultaPredioBloqueo.getUnidad() != null &&
            !filtroDatosConsultaPredioBloqueo.getUnidad().isEmpty()) {
            query.append(" AND pbp.unidad= :unidad");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroDatosConsultaPredioBloqueo.getDepartamento() != null) {
            q.setParameter("departamento",
                filtroDatosConsultaPredioBloqueo.getDepartamento());
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipio() != null) {
            q.setParameter("municipio",
                filtroDatosConsultaPredioBloqueo.getMunicipio());
        }
        if (filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo()
                .isEmpty()) {
            q.setParameter("departamentoCodigo",
                filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getMunicipioCodigo()
                .isEmpty()) {

            // Si llega municipioCodigo deben enviar departamentopCodigo
            q.setParameter("municipioCodigo", filtroDatosConsultaPredioBloqueo.
                getDepartamentoCodigo() +
                filtroDatosConsultaPredioBloqueo.getMunicipioCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getTipoAvaluo() != null &&
            !filtroDatosConsultaPredioBloqueo.getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo",
                filtroDatosConsultaPredioBloqueo.getTipoAvaluo());
        }
        if (filtroDatosConsultaPredioBloqueo.getSectorCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getSectorCodigo()
                .isEmpty()) {
            q.setParameter("sectorCodigo",
                filtroDatosConsultaPredioBloqueo.getSectorCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getComunaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getComunaCodigo()
                .isEmpty()) {
            q.setParameter("comunaCodigo",
                filtroDatosConsultaPredioBloqueo.getComunaCodigo() + "__");
        }
        if (filtroDatosConsultaPredioBloqueo.getBarrioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getBarrioCodigo()
                .isEmpty()) {
            q.setParameter("barrioCodigo", "__" +
                filtroDatosConsultaPredioBloqueo.getBarrioCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo()
                .isEmpty()) {
            q.setParameter("manzanaCodigo",
                filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getPredio() != null &&
            !filtroDatosConsultaPredioBloqueo.getPredio().isEmpty()) {
            q.setParameter("predio",
                filtroDatosConsultaPredioBloqueo.getPredio());
        }
        if (filtroDatosConsultaPredioBloqueo.getCondicionPropiedad() != null &&
            !filtroDatosConsultaPredioBloqueo.getCondicionPropiedad()
                .isEmpty()) {
            q.setParameter("condicionPropiedad",
                filtroDatosConsultaPredioBloqueo.getCondicionPropiedad());
        }
        if (filtroDatosConsultaPredioBloqueo.getEdificio() != null &&
            !filtroDatosConsultaPredioBloqueo.getEdificio().isEmpty()) {
            q.setParameter("edificio",
                filtroDatosConsultaPredioBloqueo.getEdificio());
        }
        if (filtroDatosConsultaPredioBloqueo.getPiso() != null &&
            !filtroDatosConsultaPredioBloqueo.getPiso().isEmpty()) {
            q.setParameter("piso", filtroDatosConsultaPredioBloqueo.getPiso());
        }
        if (filtroDatosConsultaPredioBloqueo.getUnidad() != null &&
            !filtroDatosConsultaPredioBloqueo.getUnidad().isEmpty()) {
            q.setParameter("unidad",
                filtroDatosConsultaPredioBloqueo.getUnidad());
        }

        try {
            return q.getResultList();
        } catch (IndexOutOfBoundsException ie) {
            return new ArrayList<Predio>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#findPisosEdificio(String)
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> findPisosEdificio(String numeroPredial) {

        List<String> pisosEdificio = new ArrayList<String>();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT p.numeroPredial");
        sql.append(" FROM Predio p");
        sql.append(" WHERE p.numeroPredial LIKE :numeroPredialEdificio");

        try {
            Query query = this.entityManager.createQuery(sql.toString());

            query.setParameter("numeroPredialEdificio",
                numeroPredial.substring(0, 24) + "%");

            for (String str : (List<String>) query.getResultList()) {
                if (!pisosEdificio.contains(str.substring(24, 26))) {
                    pisosEdificio.add(str.substring(24, 26));
                }
            }

            return pisosEdificio;
        } catch (Exception e) {
            LOGGER.error("Error al realizar la búsqueda de pisos del edificio");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#findPisosEdificio");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#findTerrenosManzana(String)
     * @author fabio.navarrete
     */
    @Override
    public List<String> findTerrenosManzana(String numeroPredial) {
        try {
            List<String> terrenosManzana = new ArrayList<String>();
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT p.numeroPredial");
            sql.append(" FROM Predio p");
            sql.append(" WHERE p.numeroPredial LIKE :numeroPredialManzana");

            Query query = entityManager.createQuery(sql.toString());

            query.setParameter("numeroPredialManzana",
                numeroPredial.substring(0, 18) + "%");

            for (String str : (List<String>) query.getResultList()) {
                if (!terrenosManzana.contains(str.substring(17, 21))) {
                    terrenosManzana.add(str.substring(17, 21));
                }
            }

            return terrenosManzana;
        } catch (Exception e) {
            throw new ExcepcionSNC("AlgúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al realizar la búsqueda de terrenos de la manzana",
                e);
        }
    }

    /**
     * @see IPredioDAO#findTerrenosManzana(String)
     * @author fabio.navarrete
     */
    @Override
    public List<String> findEdificiosTerreno(String numeroPredial) {
        try {
            List<String> edificiosTerreno = new ArrayList<String>();
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT p.numeroPredial");
            sql.append(" FROM Predio p");
            sql.append(" WHERE p.numeroPredial LIKE :numeroPredialTerreno");

            Query query = entityManager.createQuery(sql.toString());

            // El 9 hace referencia a predio en PH
            query.setParameter("numeroPredialTerreno",
                numeroPredial.substring(0, 22) + "9%");

            for (String str : (List<String>) query.getResultList()) {
                if (!edificiosTerreno.contains(str.substring(22, 24))) {
                    edificiosTerreno.add(str.substring(22, 24));
                }
            }

            return edificiosTerreno;
        } catch (Exception e) {
            throw new ExcepcionSNC("AlgúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al realizar la búsqueda de terrenos de la manzana",
                e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see
     * IPredioDAO#buscarRangoPrediosValidsoPorFiltrosDatosConsultaPrediosBloqueo(FiltroDatosConsultaPrediosBloqueo,
     * FiltroDatosConsultaPrediosBloqueo)
     * @author juan.agudelo
     */
    @Override
    public List<Predio> buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB,
        FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB) {

        LOGGER.debug(
            "executing PredioDAOBean#buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo");

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT pbp FROM Predio pbp" +
            " JOIN pbp.departamento" + " JOIN pbp.municipio" +
            " WHERE 1 = 1");

        if (filtroCPredioB.getDepartamento() != null) {
            query.append(" AND pbp.departamento= :departamento");
        }
        if (filtroCPredioB.getMunicipio() != null) {
            query.append(" AND pbp.municipio= :municipio");
        }
        query.append(" AND pbp.departamento.codigo = :departamentoCodigoInicial");
        query.append(" AND pbp.municipio.codigo LIKE :municipioCodigoInicial");
        query.append(" AND pbp.tipoAvaluo BETWEEN :tipoAvaluoInicial");
        query.append(" AND :tipoAvaluoFinal");
        query.append(" AND pbp.sectorCodigo BETWEEN :sectorCodigoInicial");
        query.append(" AND :sectorCodigoFinal");
        query.append(" AND pbp.barrioCodigo BETWEEN :barrioCodigoInicial");
        query.append(" AND :barrioCodigoFinal");
        // TODO debe separarse barrio y comuna
        /*
         * query.append(" AND pbp.barrioCodigo BETWEEN :comunaCodigoInicial"); query.append(" AND
         * :comunaCodigoFinal");
         */
        query.append(" AND pbp.manzanaCodigo BETWEEN :manzanaCodigoInicial");
        query.append(" AND :manzanaCodigoFinal");

        if (filtroCPredioB.getPredio() != null &&
            !filtroCPredioB.getPredio().isEmpty() &&
            filtroFinalCPredioB.getPredio() != null &&
            !filtroFinalCPredioB.getPredio().isEmpty()) {
            query.append(" AND pbp.predio BETWEEN :predioInicial");
            query.append(" AND :predioFinal");
        }
        if (filtroCPredioB.getCondicionPropiedad() != null &&
            !filtroCPredioB.getCondicionPropiedad().isEmpty() &&
            filtroFinalCPredioB.getCondicionPropiedad() != null &&
            !filtroFinalCPredioB.getCondicionPropiedad().isEmpty()) {
            query.append(" AND pbp.condicionPropiedad BETWEEN :condicionPropiedadInicial");
            query.append(" AND :condicionPropiedadFinal");
        }
        if (filtroCPredioB.getEdificio() != null &&
            !filtroCPredioB.getEdificio().isEmpty() &&
            filtroFinalCPredioB.getEdificio() != null &&
            !filtroFinalCPredioB.getEdificio().isEmpty()) {
            query.append(" AND pbp.edificio BETWEEN :edificioInicial");
            query.append(" AND :edificioFinal");
        }
        if (filtroCPredioB.getPiso() != null &&
            !filtroCPredioB.getPiso().isEmpty() &&
            filtroFinalCPredioB.getPiso() != null &&
            !filtroFinalCPredioB.getPiso().isEmpty()) {
            query.append(" AND pbp.piso BETWEEN :pisoInicial");
            query.append(" AND :pisoFinal");
        }
        if (filtroCPredioB.getUnidad() != null &&
            !filtroCPredioB.getUnidad().isEmpty() &&
            filtroFinalCPredioB.getUnidad() != null &&
            !filtroFinalCPredioB.getUnidad().isEmpty()) {
            query.append(" AND pbp.unidad BETWEEN :unidadInicial");
            query.append(" AND :unidadFinal");
        }

        Query q = entityManager.createQuery(query.toString());

        if (filtroCPredioB.getDepartamento() != null) {
            q.setParameter("departamento", filtroCPredioB.getDepartamento());
        }
        if (filtroCPredioB.getMunicipio() != null) {
            q.setParameter("municipio", filtroCPredioB.getMunicipio());
        }

        q.setParameter("departamentoCodigoInicial",
            filtroCPredioB.getDepartamentoCodigo());
        q.setParameter("municipioCodigoInicial",
            "__" + filtroCPredioB.getMunicipioCodigo());
        q.setParameter("tipoAvaluoInicial", filtroCPredioB.getTipoAvaluo());
        q.setParameter("tipoAvaluoFinal", filtroFinalCPredioB.getTipoAvaluo());
        q.setParameter("sectorCodigoInicial", filtroCPredioB.getSectorCodigo());
        q.setParameter("sectorCodigoFinal",
            filtroFinalCPredioB.getSectorCodigo());
        q.setParameter("barrioCodigoInicial", filtroCPredioB.getComunaCodigo() +
            filtroCPredioB.getBarrioCodigo());
        q.setParameter(
            "barrioCodigoFinal",
            filtroFinalCPredioB.getComunaCodigo() +
            filtroFinalCPredioB.getBarrioCodigo());
        // TODO debe separarse barrio y comuna, por el momento se integran los
        // dos parametros en barrio
        q.setParameter("manzanaCodigoInicial",
            filtroCPredioB.getManzanaVeredaCodigo());
        q.setParameter("manzanaCodigoFinal",
            filtroFinalCPredioB.getManzanaVeredaCodigo());

        if (filtroCPredioB.getPredio() != null &&
            !filtroCPredioB.getPredio().isEmpty() &&
            filtroFinalCPredioB.getPredio() != null &&
            !filtroFinalCPredioB.getPredio().isEmpty()) {
            q.setParameter("predioInicial", filtroCPredioB.getPredio());
            q.setParameter("predioFinal", filtroFinalCPredioB.getPredio());
        }
        if (filtroCPredioB.getCondicionPropiedad() != null &&
            !filtroCPredioB.getCondicionPropiedad().isEmpty() &&
            filtroFinalCPredioB.getCondicionPropiedad() != null &&
            !filtroFinalCPredioB.getCondicionPropiedad().isEmpty()) {
            q.setParameter("condicionPropiedadInicial",
                filtroCPredioB.getCondicionPropiedad());
            q.setParameter("condicionPropiedadFinal",
                filtroFinalCPredioB.getCondicionPropiedad());
        }
        if (filtroCPredioB.getEdificio() != null &&
            !filtroCPredioB.getEdificio().isEmpty() &&
            filtroFinalCPredioB.getEdificio() != null &&
            !filtroFinalCPredioB.getEdificio().isEmpty()) {
            q.setParameter("edificioInicial", filtroCPredioB.getEdificio());
            q.setParameter("edificioFinal", filtroFinalCPredioB.getEdificio());
        }
        if (filtroCPredioB.getPiso() != null &&
            !filtroCPredioB.getPiso().isEmpty() &&
            filtroFinalCPredioB.getPiso() != null &&
            !filtroFinalCPredioB.getPiso().isEmpty()) {
            q.setParameter("pisoInicial", filtroCPredioB.getPiso());
            q.setParameter("pisoFinal", filtroFinalCPredioB.getPiso());
        }
        if (filtroCPredioB.getUnidad() != null &&
            !filtroCPredioB.getUnidad().isEmpty() &&
            filtroFinalCPredioB.getUnidad() != null &&
            !filtroFinalCPredioB.getUnidad().isEmpty()) {
            q.setParameter("unidadInicial", filtroCPredioB.getUnidad());
            q.setParameter("unidadFinal", filtroFinalCPredioB.getUnidad());
        }

        try {
            return q.getResultList();
        } catch (IndexOutOfBoundsException ie) {
            LOGGER.debug("Error: " + ie.getMessage());
            return new ArrayList<Predio>();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#contarPrediosNacionales()
     * @author david.cifuentes
     */
    @Override
    public Long contarPrediosNacionales() {
        Long answer = null;
        Query query;
        StringBuilder q = new StringBuilder();

        q.append("SELECT COUNT(p) FROM Predio p WHERE 1 = 1");

        query = entityManager.createQuery(q.toString());
        try {
            answer = (Long) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * Obtiene un predio aleatorio que no tiene asociado un trámite (Aleatorio entre los primeros
     * 200 registros según el código del municipio)
     */
    @Override
    public Predio buscarPredioLibreTramite(Integer codigoEstructuraOrganizacional,
        String codigoMunicipio) {
        LOGGER.info("buscarPredioLibreTramite");
        Random r = new Random();
        int low = 1;
        int high = 200;
        int randomPage = r.nextInt(high - low) + low;

        LOGGER.info("randomPage:" + randomPage);

        String consulta =
            "SELECT p.* FROM Predio p LEFT OUTER JOIN Tramite t ON p.id=t.predio_Id " +
            "INNER JOIN Municipio m ON p.municipio_codigo=m.codigo " +
            "INNER JOIN Jurisdiccion j ON m.codigo=j.municipio_codigo " +
            "WHERE  p.municipio_codigo = '" + codigoMunicipio + "' and t.id IS NULL   " +
            "AND j.estructura_Organizacional_cod=" + codigoEstructuraOrganizacional;
        Query query = entityManager.createNativeQuery(consulta, Predio.class);
        query.setMaxResults(1);
        query.setFirstResult(randomPage);
        return (Predio) query.getSingleResult();
    }

    // -------------------------------------------------------------------------
    /**
     * @see IPredioDAO#getPrediosByNumerosPrediales(List)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Predio> getPrediosByNumerosPrediales(
        List<String> numerosPredialesColindantes) {

        List<Predio> answer = null;
        Query query;
        String q;

        q = "SELECT DISTINCT p FROM Predio p" +
            " JOIN FETCH p.departamento" +
            " JOIN FETCH p.municipio" +
            " WHERE p.numeroPredial IN :numerosPrediales";

        query = entityManager.createQuery(q);
        query.setParameter("numerosPrediales", numerosPredialesColindantes);
        try {
            answer = (List<Predio>) query.getResultList();

        } catch (IndexOutOfBoundsException ne) {
            return new ArrayList<Predio>();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see IPredioDAO#isPredioCanceladoByPredioIdOrNumeroPredial(Predio)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public boolean isPredioCanceladoByPredioIdOrNumeroPredial(Long predioId,
        String numeroPredial) {

        LOGGER.debug("executing PredioDAOBean#isPredioCanceladoByPredioIdOrNumeroPredial");

        boolean answer = false;
        String estadoPredio = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT p.estado FROM Predio p");

        if (predioId != null) {
            query.append(" WHERE p.id = :predioId");
        } else if (numeroPredial != null && !numeroPredial.isEmpty()) {
            query.append(" WHERE p.numeroPredial = :numeroPredial");
        }

        Query q = entityManager.createQuery(query.toString());

        if (predioId != null) {
            q.setParameter("predioId", predioId);
        } else if (numeroPredial != null && !numeroPredial.isEmpty()) {
            q.setParameter("numeroPredial", numeroPredial);
        }

        try {
            estadoPredio = (String) q.getSingleResult();

            if (estadoPredio != null && !estadoPredio.isEmpty() &&
                estadoPredio.equals(EPredioEstado.CANCELADO.getCodigo())) {
                answer = true;
            }

        } catch (NoResultException nr) {
            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PredioDAOBean#isPredioCanceladoByPredioIdOrNumeroPredial");
        }

        return answer;
    }

    /**
     * @see IPredioDAO#buscarPrediosPorRegionCapturaOferta(Long, String)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Predio> buscarPrediosPorRegionCapturaOferta(Long idRegionCaptura,
        String idRecolector) {

        LOGGER.debug("executing PredioDAOBean#buscarPrediosPorRegionCapturaOferta");

        List<Predio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT pr " +
            " FROM Predio pr, DetalleCapturaOferta dco, RegionCapturaOferta rco " +
            " JOIN FETCH pr.departamento " +
            " JOIN FETCH pr.municipio " +
            " WHERE SUBSTRING(pr.numeroPredial,1,17) = dco.manzanaVeredaCodigo " +
            " AND dco.regionCapturaOferta.id = :idRegionCaptura " +
            " AND rco.asignadoRecolectorId = :idRecolector";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRegionCaptura", idRegionCaptura);
            query.setParameter("idRecolector", idRecolector);

            answer = (List<Predio>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#buscarPrediosPorRegionCapturaOferta");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#getTerrenoNumbersFromManzanaAndPropertyCondition(java.lang.String,
     * java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<String> getTerrenoNumbersFromManzanaAndPropertyCondition(
        String manzana, List<String> condicionesPropiedad) {

        LOGGER.debug("on PredioDAOBean#getTerrenoNumbersFromManzanaAndPropertyCondition ...");

        List<String> answer;
        List<Predio> queryAnswer;
        String queryString;
        Query query;

        answer = null;
        queryString = "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :manzanaP AND " +
            "p.condicionPropiedad IN (:condPropP)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("manzanaP", manzana + "%");
            query.setParameter("condPropP", condicionesPropiedad);
            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                answer = new ArrayList<String>();
                for (Predio p : queryAnswer) {
                    if (!answer.contains(p.getTerreno())) {
                        answer.add(p.getTerreno());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#getTerrenoNumbersFromManzanaAndPropertyCondition");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#getEdificioTorreNumbersFromTerreno(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<String> getEdificioTorreNumbersFromTerreno(String terreno) {

        LOGGER.debug("on PredioDAOBean#getEdificioTorreNumbersFromTerreno ...");

        List<String> answer;
        List<Predio> queryAnswer;
        String queryString;
        Query query;

        answer = null;
        queryString = "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :terrenoP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("terrenoP", terreno + "%");
            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                answer = new ArrayList<String>();
                for (Predio p : queryAnswer) {
                    if (!answer.contains(p.getEdificio())) {
                        answer.add(p.getEdificio());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#getEdificioTorreNumbersFromTerreno");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#getPisoNumbersFromEdificioTorre(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<String> getPisoNumbersFromEdificioTorre(String edificioTorre) {

        LOGGER.debug("on PredioDAOBean#getPisoNumbersFromEdificioTorre ...");

        List<String> answer;
        List<Predio> queryAnswer;
        String queryString;
        Query query;

        answer = null;
        queryString = "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :edificioTorreP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("edificioTorreP", edificioTorre + "%");
            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                answer = new ArrayList<String>();
                for (Predio p : queryAnswer) {
                    if (!answer.contains(p.getPiso())) {
                        answer.add(p.getPiso());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#getPisoNumbersFromEdificioTorre");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#existsPredio(java.lang.String, java.lang.String[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean existsPredio(String numeroPredial, String... arrayEstados) {

        LOGGER.debug("on PredioDAOBean#existsPredio");

        boolean useEstados, answer;
        StringBuilder queryString;
        Query query;
        ArrayList<String> estadosList = null;

        useEstados = false;
        answer = false;
        queryString = new StringBuilder();
        queryString.append("SELECT count(p.id)  FROM Predio p ").
            append(" WHERE p.numeroPredial = :numeroPredialP ");

        if (arrayEstados != null && arrayEstados.length > 0) {
            estadosList = new ArrayList<String>();

            //N: ESTO NO FUNCIONA. Parece ser que como no está definido explícitamente como String[]
            //   no lo toma como tal y tratar de usar Arrays.asList con esto genera error
            //estadosList = (ArrayList<String>) Arrays.asList(listaEstados);
            //estadosList.addAll((ArrayList<String>) Arrays.asList(listaEstados));
            for (String estado : arrayEstados) {
                estadosList.add(estado);
            }

            queryString.append(" AND p.estado IN(:estadosP)");
            useEstados = true;
        }
        query = this.entityManager.createQuery(queryString.toString());

        try {
            query.setParameter("numeroPredialP", numeroPredial);
            if (useEstados) {
                query.setParameter("estadosP", estadosList);
            }

            Long conteo = (Long) query.getSingleResult();
            if (conteo > 0) {
                answer = true;
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioDAOBean#existsPredio");
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#lookForPrediosByNumPredialYEstadosNF(String, listaEstados)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Predio> lookForPrediosByNumPredialYEstadosNF(
        String numeroPredial, String... arrayEstados) {

        LOGGER.debug("on PredioDAOBean#lookForPrediosByNumPredialYEstadosNF");

        boolean useEstados;
        StringBuilder queryString;
        Query query;
        ArrayList<String> estadosList = null;
        List<Predio> answer = null;

        useEstados = false;
        queryString = new StringBuilder();
        queryString.append("SELECT p FROM Predio p ")
            .append(" JOIN FETCH p.departamento")
            .append(" JOIN FETCH p.municipio")
            .append(" WHERE p.numeroPredial = :numeroPredialP ");

        if (arrayEstados != null && arrayEstados.length > 0) {
            estadosList = new ArrayList<String>();

            //OJO: NO CAMBIAR POR Arrays.asList
            for (String estado : arrayEstados) {
                estadosList.add(estado);
            }

            queryString.append(" AND p.estado IN(:estadosP)");
            useEstados = true;
        }
        query = this.entityManager.createQuery(queryString.toString());

        try {
            query.setParameter("numeroPredialP", numeroPredial);
            if (useEstados) {
                query.setParameter("estadosP", estadosList);
            }

            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioDAOBean#lookForPrediosByNumPredialYEstadosNF");
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     *
     */
    @Override
    public Predio loadRandomObject(String codeStartsWith) {
        LOGGER.debug("loadRandomObject:" + codeStartsWith);
        Predio row = null;
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("select count(p.id) from Predio p where p.MUNICIPIO_CODIGO = '").
                append(codeStartsWith).append("'");
            Query query = this.entityManager.createNativeQuery(queryString.toString());
            BigDecimal total = (BigDecimal) query.getSingleResult();
            Random random = new Random();
            int from = random.nextInt(total.intValue());
            LOGGER.debug("total:" + total);
            LOGGER.debug("from:" + from);

            queryString = new StringBuilder();
            queryString.append("select id from Predio p where p.MUNICIPIO_CODIGO = '").append(
                codeStartsWith).append("'");
            query = this.entityManager.createNativeQuery(queryString.toString());
            query.setFirstResult(from);
            query.setMaxResults(0);
            BigDecimal id = (BigDecimal) query.getResultList().get(0);
            LOGGER.debug("id:" + id);
            row = this.findById(id.longValue());
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return row;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPredioDAO#getUnidadesConstruccion(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ArrayList<UnidadConstruccion> getUnidadesConstruccion(Long predioId) {

        ArrayList<UnidadConstruccion> answer = null;
        Predio tempAnswer;
        Query query;
        String queryString;

        queryString = "SELECT p " +
            " FROM Predio p " +
            " LEFT JOIN FETCH p.unidadConstruccions uc" +
            " LEFT JOIN FETCH uc.usoConstruccion" +
            " WHERE p.id = :predioId_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("predioId_p", predioId);
            tempAnswer = (Predio) query.getSingleResult();
            if (null != tempAnswer.getUnidadConstruccions() &&
                !tempAnswer.getUnidadConstruccions().isEmpty()) {

                //N: esto genera error al tratar de hacer cast entre PersistentBag y ArrayList,
                //  por eso hay que hacerlo a mano
                //answer = (ArrayList<UnidadConstruccion>) tempAnswer.getUnidadConstruccions();
                answer = new ArrayList<UnidadConstruccion>();
                for (UnidadConstruccion uc : tempAnswer.getUnidadConstruccions()) {
                    answer.add(uc);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error obteniendo las unidades de construcción del Predio con id " +
                predioId + ": " + ex.getMessage());
        }

        return answer;
    }

    /**
     * @see IPredioDAO#getPredioFetchDatosUbicacionParaMovilesById(java.lang.Long)
     * @author lorena.salamanca
     */
    @Implement
    @Override
    public Predio getPredioFetchDatosUbicacionParaMovilesById(Long predioId) {

        LOGGER.debug("on PredioDAOBean#getPredioFetchDatosUbicacionParaMovilesById ...");

        Predio answer = null;
        String query = "SELECT p FROM Predio p " +
            " LEFT JOIN FETCH p.circuloRegistral " +
            " LEFT JOIN FETCH p.unidadConstruccions u " +
            " LEFT JOIN FETCH u.usoConstruccion uS " +
            " WHERE p.id = :predioId";
        Query q = this.entityManager.createQuery(query);
        q.setParameter("predioId", predioId);

        try {
            answer = (Predio) q.getSingleResult();
            answer.getPredioInconsistencias().size();
            answer.getPredioZonas().size();
            answer.getPersonaPredios().size();
            //D: forzar a que traiga los objetos Direccion
            for (int j = 0; j < answer.getPredioDireccions().size(); j++) {
                answer.getPredioDireccions().get(j).getId();
            }
            //D: las direcciones deben estar ordenadas por la principal (aparece primero)
            Collections.sort(answer.getPredioDireccions(),
                PredioDireccion.getComparatorDirPrincipal(false));

            for (int k = 0; k < answer.getPredioServidumbres().size(); k++) {
                answer.getPredioServidumbres().get(k).getId();
            }
            for (int m = 0; m < answer.getReferenciaCartograficas().size(); m++) {
                answer.getReferenciaCartograficas().get(m).getId();
            }
            for (int i = 0; i < answer.getFotos().size(); i++) {
                answer.getUnidadConstruccions().get(i).getId();
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción buscando datos de predio: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, ex, "PredioDAOBean#getPredioFetchDatosUbicacionParaMovilesById");
        }

        return answer;
    }

    /**
     * @see IPredioDAO#obtenerPrediosPorNumeroManzana(java.lang.String)
     * @author javier.barajas
     */
    @Implement
    @Override
    public List<Predio> obtenerPrediosPorNumeroManzana(String numeroManzana) {
        List<Predio> answer = new ArrayList<Predio>();
        Predio tempAnswer;
        Query query;
        String queryString;

        queryString = "SELECT p " +
            " FROM Predio p " +
            " WHERE p.numeroPredial like :numeroManzana";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroManzana", numeroManzana + "%");
            answer = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios de la manzana " +
                numeroManzana + ": " + ex.getMessage());
        }

        return answer;
    }

    /**
     * Metodo para buscar por numero predial anterior o actual usado en IPER
     *
     * @author fredy.wilches
     */
    @Implement
    @Override
    public Predio getPredioByNumeroPredialAnteriorOActual(String numeroPredial) {

        LOGGER.debug("on PredioDAOBean#getPredioByNumeroPredialAnteriorOActual ...");

        Predio answer = null;
        String query = "SELECT DISTINCT p FROM Predio p " +
            " JOIN FETCH p.departamento JOIN FETCH p.municipio " +
            " LEFT JOIN FETCH p.personaPredios pp " +
            " LEFT JOIN FETCH pp.persona pe " +
            " WHERE p.numeroPredialAnterior = :numeroPredialAnterior or p.numeroPredial = :numeroPredial";
        Query q = this.entityManager.createQuery(query);
        q.setParameter("numeroPredialAnterior", numeroPredial);
        q.setParameter("numeroPredial", numeroPredial);

        try {
            List<Predio> lista = q.getResultList();
            if (lista != null && lista.size() > 0) {
                answer = lista.get(0);

                for (int k = 0; k < answer.getPredioBloqueos().size(); k++) {
                    answer.getPredioBloqueos().get(k).getId();
                }
                for (int k = 0; k < answer.getPersonaPredios().size(); k++) {
                    answer.getPersonaPredios().get(k).getPersona().getPersonaBloqueos().size();

                }
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción buscando predio: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, ex, "PredioDAOBean#getPredioByNumeroPredialAnteriorOActual");
        }
        return answer;
    }

    /**
     * Metodo para buscar por los datos Fisicos segun una lista de numeros prediales
     *
     * @author fredy.wilches
     */
    @Override
    public List<Object[]> getDatosFisicosByNumeroPredial(
        List<String> numeroPredial, int first, int pageSize) {

        List<Object[]> answer = null;
        List<Object[]> listaDatosFisicos = new ArrayList<Object[]>();

        Query query;
        String queryString;

        queryString = "SELECT p  FROM Predio p  " +
            " WHERE p.numeroPredial =:numeroPredial AND ( p.condicionPropiedad = :condicion6" +
            " OR p.condicionPropiedad =:condicion9 OR p.condicionPropiedad = :condicion8)";
        try {
            for (String resultPredio : numeroPredial) {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("numeroPredial", resultPredio);
                query.setParameter("condicion6", "6");
                query.setParameter("condicion9", "9");
                query.setParameter("condicion8", "8");
                //answer =  query.getSingleResult();
                //TODO: Convirtiendolo a un Object[]

                Predio p = (Predio) query.getSingleResult();

                p.getFichaMatrizs().size();

                p.getPredioDireccions().size();
                //p.getPredioZonas().size();
                Object[] adicionResultado = {p.getNumeroPredial(), p.getTipo(), p.getDestino(), p.
                    getNumeroRegistro(),
                    p.getPredioDireccions().get(0).getDireccion(),
                    p.getFichaMatrizs().get(0).getFichaMatrizPredios().get(0).getCoeficiente(),
                    p.getFichaMatrizs().get(0).getAreaTotalTerrenoComun(),
                    p.getFichaMatrizs().get(0).getAreaTotalTerrenoPrivada()};

//				answer = new ArrayList<Object[]>();
//				Object[] conteoArray = new Object[] { conteo };
                listaDatosFisicos.add(adicionResultado);
            }

            int dataSize = listaDatosFisicos.size();
            if (dataSize > pageSize) {
                try {
                    return listaDatosFisicos.subList(first, first + pageSize);
                } catch (IndexOutOfBoundsException e) {
                    return listaDatosFisicos.subList(first, first + (dataSize % pageSize));
                }
            }

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios  " +
                numeroPredial.size() + ": " + ex.getMessage());
        }

        return listaDatosFisicos;

    }

    /**
     * Metodo para buscar datos de ficha matriz
     *
     * @see IPredioDAO#getReporteFichaMatriz(java.lang.String)
     * @author javier.barajas
     */
    @Override
    public List<Object[]> getReporteFichaMatriz(List<String> numeroPredial,
        int first, int pageSize) {
        List<Object[]> answer = null;
        List<Object[]> listaFichaMatriz = new ArrayList<Object[]>();

        Query query;
        String queryString;

        queryString = "SELECT p  FROM Predio p  " +
            " WHERE p.numeroPredial =:numeroPredial AND ( p.condicionPropiedad = :condicion6" +
            " OR p.condicionPropiedad =:condicion9 OR p.condicionPropiedad = :condicion8)";
        try {
            for (String resultPredio : numeroPredial) {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("numeroPredial", resultPredio);
                query.setParameter("condicion6", "6");
                query.setParameter("condicion9", "9");
                query.setParameter("condicion8", "8");
                //answer =  query.getSingleResult();
                //TODO: Convirtiendolo a un Object[]

                Predio p = (Predio) query.getSingleResult();

                p.getFichaMatrizs().size();
                p.getFichaMatrizs().get(0).getFichaMatrizTorres().size();
                p.getPredioDireccions().size();
                //p.getPredioZonas().size();
                Object[] adicionResultado = {p.getNumeroPredial(), p.getNumeroRegistro(),
                    p.getPredioDireccions().get(0).getDireccion(),
                    p.getNombre(),
                    p.getFichaMatrizs().get(0).getAreaTotalTerrenoComun(),
                    p.getFichaMatrizs().get(0).getAreaTotalTerrenoPrivada(),
                    p.getFichaMatrizs().get(0).getAreaTotalConstruidaPrivada(),
                    p.getFichaMatrizs().get(0).getAreaTotalConstruidaComun(),
                    p.getFichaMatrizs().get(0).getAreaTotalTerrenoComun(),
                    p.getFichaMatrizs().get(0).getTotalUnidadesPrivadas(),
                    p.getFichaMatrizs().get(0).getAreaTotalTerrenoComun(),
                    p.getFichaMatrizs().get(0).getFichaMatrizPredios().get(0).getNumeroPredial()
                };

//				answer = new ArrayList<Object[]>();
//				Object[] conteoArray = new Object[] { conteo };
                listaFichaMatriz.add(adicionResultado);
            }

            int dataSize = listaFichaMatriz.size();
            if (dataSize > pageSize) {
                try {
                    return listaFichaMatriz.subList(first, first + pageSize);
                } catch (IndexOutOfBoundsException e) {
                    return listaFichaMatriz.subList(first, first + (dataSize % pageSize));
                }
            }

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios  " +
                numeroPredial.size() + ": " + ex.getMessage());
        }

        return listaFichaMatriz;

    }

    /**
     * @see IPredioDAO#getPrediosLibreActualizacionReconocimientoYSaldoConservacion()
     * @author andres.eslava
     */
    @Override
    public List<Predio> getPrediosLibreActualizacionReconocimientoYSaldoConservacion(
        String codigoManzana) {

        LOGGER.debug(
            "PredioDAOBean#getPrediosLibreActualizacionReconocimientoYSaldoConservacion...INICIA");
        try {
            String stringQuery = "SELECT fpr " +
                "FROM Predio fpr " +
                "WHERE fpr.id not in" +
                "(SELECT pr.id " +
                "FROM Predio pr ,ReconocimientoPredio rp " +
                "WHERE pr.numeroPredial=rp.numeroPredial and rp.numeroPredial like :codigoManzana) " +
                "and fpr.numeroPredial like :codigoManzana";

            Query query = this.getEntityManager().createQuery(stringQuery);

            query.setParameter("codigoManzana", codigoManzana + "%");

            @SuppressWarnings("unchecked")
            List<Predio> predios = (List<Predio>) query.getResultList();
            return predios;
        } catch (Exception e) {
            LOGGER.debug(
                "Error obteniendo los predis en: PredioDAOBean#getPrediosLibreActualizacionReconocimientoYSaldoConservacion");
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        } finally {
            LOGGER.debug(
                "PredioDAOBean#getPrediosLibreActualizacionReconocimientoYSaldoConservacion...FINALIZA");

        }
    }

    /**
     * Metodo para buscar datos por una cadena de busqueda
     *
     * @see IPredioDAO#getBuscaPredioGenereacionFormularioSBC(java.lang.String)
     * @author javier.barajas
     */
    @Override
    public List<Predio> getBuscaPredioGenereacionFormularioSBC(
        String cadenaBusqueda) {

        List<Predio> answer = new ArrayList<Predio>();
        Query query;
        String queryString;

        queryString = "SELECT p " +
            " FROM Predio p " +
            " WHERE p.numeroPredial like :cadenaBusqueda";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("cadenaBusqueda", cadenaBusqueda + "%");
            answer = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios de la busqueda " +
                cadenaBusqueda + ": " + ex.getMessage());
        }

        return answer;

    }

    /**
     * @author felipe.cadena
     * @see IPredioDAO#obtenerPredioValidacionesRectificacionById(Long)
     */
    @Override
    public Predio obtenerParaValidacionesRectificacionById(Long predioId) {
        LOGGER.debug("PredioDAOBean#obtenerPredioValidacionesRectificacionById");

        try {
            Predio p = this.findById(predioId);

            // Inicializacion de los atributos necesarios para la validacion
            Hibernate.initialize(p.getPersonaPredios());
            for (PersonaPredio pp : p.getPersonaPredios()) {
                Hibernate.initialize(pp.getPersona());
                Hibernate.initialize(pp.getPersonaPredioPropiedads());
            }
            Hibernate.initialize(p.getUnidadConstruccions());
            for (UnidadConstruccion uc : p.getUnidadConstruccions()) {
                Hibernate.initialize(uc.getUnidadConstruccionComps());
            }
            Hibernate.initialize(p.getPredioDireccions());
            Hibernate.initialize(p.getPredioZonas());
            Hibernate.initialize(p.getFichaMatrizs());

            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see IPredioDAO#obtenerPrediosPH(java.lang.String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Predio> obtenerPrediosPH(String terreno) {

        LOGGER.debug("on PredioDAOBean#obtenerPrediosPHoCondominio ...");

        List<String> answer;
        List<Predio> queryAnswer;
        String queryString;
        Query query;

        answer = null;
        queryString = "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :terrenoP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("terrenoP", terreno + "%");
            queryAnswer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#obtenerPrediosPHoCondominio");
        }

        return queryAnswer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteDAO#obtenerPrediosCompletosEnglobePorTramiteId
     */
    @Override
    public List<Predio> obtenerPrediosCompletosEnglobePorTramiteId(Long tramiteId) {
        LOGGER.debug("TramiteDAOBean#obtenerPrediosCompletosEnglobePorTramiteId...INICIA");

        List<Predio> prediosEnglobe = new ArrayList<Predio>();

        String query = "SELECT p FROM Predio p WHERE p.id in (" +
            "SELECT tpe.predio.id FROM TramiteDetallePredio tpe WHERE tpe.tramite.id = :tramiteId)";

        Query q = this.entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);

        try {
            prediosEnglobe = (List<Predio>) q.getResultList();

            if (!(null == prediosEnglobe) || prediosEnglobe.isEmpty()) {
                for (Predio p : prediosEnglobe) {
                    Hibernate.initialize(p.getPredioDireccions());
                    Hibernate.initialize(p.getUnidadConstruccions());
                }
            }

            return prediosEnglobe;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "TramiteDAOBean#obtenerPrediosCompletosEnglobePorTramiteId");
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IPredioDAO#obtenerPredioCompletoPorNumeroPredial
     */
    @Override
    public Predio obtenerPredioCompletoPorNumeroPredial(String numeroPredial) {

        LOGGER.debug("getPredioByNumeroPredial");
        Query q = this.entityManager.createNamedQuery("findPredioByNumeroPredial");
        q.setParameter("numeroPredial", numeroPredial);
        try {
            Predio p = (Predio) q.getSingleResult();
            Hibernate.initialize(p.getPredioDireccions());
            Hibernate.initialize(p.getUnidadConstruccions());
            return p;
        } catch (Exception e) {
            LOGGER.error("Error en obtener el predio: " + e.getMessage());
            return null;
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see
     * IPredioDAO#obtenerPredioFiltroConsultaCatastralWebService(co.gov.igac.snc.util.FiltroConsultaCatastralWebService)
     */
    @Override
    public List<Predio> obtenerPredioFiltroConsultaCatastralWebService(
        FiltroConsultaCatastralWebService filtro) {

        LOGGER.debug("Inicia ...PredioDAOBean#obtenerPredioFiltroConsultaCatastralWebService");
        List<Predio> informacionCatastral;
        // TODO andres.eslava::09/09/2013::implementar la consulta de acuerdo al filtro.
        String query = "select * from Predio";
        Query q = this.entityManager.createQuery(query);

        try {
            informacionCatastral = q.getResultList();
            return informacionCatastral;
        } catch (Exception e) {

            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PredioDAOBean#obtenerPredioFiltroConsultaCatastralWebService");
        } finally {
            LOGGER.debug("Finaliza...PredioDAOBean#obtenerPredioFiltroConsultaCatastralWebService");
        }

    }

    /**
     * @see IPredioDAO#obtenerPrimerPredioPorNumeroManzana(java.lang.String)
     * @author javier.aponte
     */
    @Implement
    @Override
    public Predio obtenerPrimerPredioPorNumeroManzana(String numeroManzana) {
        Predio answer = new Predio();
        Query query;
        String queryString;

        queryString = "SELECT p " +
            " FROM Predio p " +
            " WHERE p.numeroPredial like :numeroManzana";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroManzana", numeroManzana + "%");
            query.setMaxResults(1);
            answer = (Predio) query.getSingleResult();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios de la manzana " +
                numeroManzana + ": " + ex.getMessage());
        }

        return answer;
    }

    /**
     * @see IPredioDAO#obtenerPrediosPorNumerosPrediales(java.util.List)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Predio> obtenerPrediosPorNumerosPrediales(List<String> numeroTerreno) {
        List<Predio> predios = new ArrayList<Predio>();

        Query query;
        String queryString;

        queryString = "SELECT p " +
            " FROM Predio p " +
            " WHERE p.numeroPredial in :numeroTerreno";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroTerreno", numeroTerreno);
            predios = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios por numero prediales : " + ex.getMessage());
        }

        return predios;

    }

    //-----------------------------------------------------//
    /**
     * @see IPredioDAO#buscarPredioCompletoPorPredioId(Long)
     * @author david.cifuentes
     */
    @Implement
    @Override
    public Predio buscarPredioCompletoPorPredioId(Long idPredio) {

        Predio p = new Predio();
        Query query;

        String queryString = "SELECT p" +
            " FROM Predio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.personaPredios pp" +
            " LEFT JOIN FETCH pp.persona p2" +
            " LEFT JOIN FETCH p2.direccionPais dp" +
            " LEFT JOIN FETCH p2.direccionDepartamento dd" +
            " LEFT JOIN FETCH p2.direccionMunicipio dm" +
            " WHERE p.id = :idPredio";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPredio", idPredio);
            p = (Predio) query.getSingleResult();

            if (p != null) {

                p.getFichaMatrizs().size();
                p.getPredioZonas().size();
                p.getPredioDireccions().size();
                p.getFotos().size();
                p.getUnidadConstruccions().size();
                p.getPredioAvaluoCatastrals().size();
                p.getPredioServidumbres().size();
                p.getReferenciaCartograficas().size();

                if (p.getUnidadConstruccions() != null) {
                    Hibernate.initialize(p.getUnidadConstruccions());
                    for (UnidadConstruccion u : p.getUnidadConstruccions()) {
                        if (u.getUnidadConstruccionComps() != null) {
                            Hibernate.initialize(u
                                .getUnidadConstruccionComps());
                        }
                    }
                }
            }

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los predios : " + ex.getMessage());
        }
        return p;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IPredioDAO#tieneMejorasPredio(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean tieneMejorasPredio(String numeroPredioEnNumeroPredial) {

        boolean answer = false;
        String queryString;
        Query query;
        List<Predio> prediosMismoNumPredio;

        queryString = "" +
            "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :numPredio_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numPredio_p", numeroPredioEnNumeroPredial + "%");

            prediosMismoNumPredio = query.getResultList();

            if (prediosMismoNumPredio != null) {
                for (Predio predioT : prediosMismoNumPredio) {
                    if (predioT.isMejora() && !predioT.getEstado().equals(ETramiteEstado.CANCELADO.
                        getCodigo())) {
                        answer = true;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#tieneMejorasPredio");
        }

        return answer;

    }

    /**
     * @see IPredioDAO#validarExistenciaYActivos(java.lang.List)
     * @author felipe.cadena
     */
    @Override
    public boolean validarExistenciaYActivos(List<String> nPrediales) {

        boolean activos = true;

        //retirar numeros repetidos
        Map<String, Boolean> predios = new HashMap<String, Boolean>();

        for (String numero : nPrediales) {
            predios.put(numero, true);
        }

        String queryString = "SELECT p FROM Predio p where p.numeroPredial in :numeroPredial";
        try {
            Query q = this.entityManager.createQuery(queryString);
            q.setParameter("numeroPredial", nPrediales);

            List<Predio> prediosRes = q.getResultList();

            if (prediosRes.size() != predios.size()) {
                return false;
            } else {
                for (Predio predio : prediosRes) {
                    if (EPredioEstado.CANCELADO.getCodigo().equals(predio.getEstado())) {
                        activos = false;
                        return false;
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#validarExistenciaYActivos");
        }

        return activos;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioDAO#getDetalleAvaluosById(Long)
     * @author pedro.garcia modifiedBy fredy.wilches
     */
    @Implement
    @Override
    public Predio getDetalleAvaluosById(Long predioId) {

        String queryString;
        Query query;
        Predio answer;

        answer = null;
        queryString = "SELECT p" + " FROM Predio p" +
            " LEFT JOIN FETCH p.unidadConstruccions uc" +
            " LEFT JOIN FETCH uc.usoConstruccion" +
            " WHERE p.id = :predioId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("predioId", predioId);

            answer = (Predio) query.getResultList().get(0);

            Hibernate.initialize(answer.getPredioZonas());

            //D: solo se deben mostrar las zonas de la vigencia actual, por eso se eliminn las demás de la respuesta
            if (answer.getPredioZonas() != null && !answer.getPredioZonas().isEmpty() &&
                answer.getPredioZonas().size() > 1) {

                Date vigenciaTemp, vigenciaMasActual;
                //PredioZona laZona;

                //laZona = answer.getPredioZonas().get(0);
                vigenciaMasActual = answer.getPredioZonas().get(0).getVigencia();
                for (PredioZona pz : answer.getPredioZonas()) {
                    vigenciaTemp = pz.getVigencia();

                    //OJO: el remove NO SIRVE
                    if (vigenciaTemp.after(vigenciaMasActual)) {
                        vigenciaMasActual = vigenciaTemp;
                        //laZona = pz;
                    }

                }
                List<PredioZona> predioZonas = new ArrayList<PredioZona>();
                for (PredioZona pz : answer.getPredioZonas()) {
                    if (pz.getVigencia().equals(vigenciaMasActual)) {
                        predioZonas.add(pz);
                    }
                }
                answer.setPredioZonas(predioZonas);
            }

            answer.getPredioAvaluoCatastrals().size();

            if (answer.getUnidadConstruccions() != null &&
                !answer.getUnidadConstruccions().isEmpty()) {

                for (UnidadConstruccion ucTmp : answer.getUnidadConstruccions()) {

                    if (ucTmp.getUnidadConstruccionComps() != null &&
                        !ucTmp.getUnidadConstruccionComps().isEmpty()) {
                        ucTmp.getUnidadConstruccionComps().size();
                    }
                }
            }
        } catch (NoResultException nrEx) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nrEx, "PredioDAOBean#getDetalleAvaluosById", "Predio", predioId.toString());
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "PredioDAOBean#getDetalleAvaluosById");
        } finally {
            return answer;
        }
    }

    /**
     * @see IPredioDAO#getNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal(String,
     * String)
     * @author javier.aponte
     */
    /*
     * @modified felipe.cadena:: 22-12-2016:: permite considerar solo los predios activos que no
     * sean fichas
     */
    @Implement
    @Override
    public List<String> getNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal(
        String numeroPredialInicial,
        String numeroPredialFinal, boolean completo) {

        String queryString;
        Query query;
        List<String> answer;

        answer = null;
        queryString = "SELECT p.numeroPredial FROM Predio p " +
            " WHERE p.numeroPredial BETWEEN :numeroPredialInicial AND " +
            " :numeroPredialFinal ";

        if (!completo) {
            queryString += " AND p.estado = 'ACTIVO'" +
                " AND p.numeroPredial NOT LIKE '%80000000'" +
                " AND p.numeroPredial NOT LIKE '%90000000'";
        }

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroPredialInicial", numeroPredialInicial);
            query.setParameter("numeroPredialFinal", numeroPredialFinal);

            answer = query.getResultList();

        } catch (NoResultException nrEx) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nrEx, "PredioDAOBean#getDetalleAvaluosById", "Predio");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "PredioDAOBean#getDetalleAvaluosById");
        }
        return answer;

    }

    /**
     * @see IPredioDAO#calcularSumaAvaluosTotalesDePrediosFichaMatriz(java.lang.Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public double[] calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long idFicha) {

        double[] answer = new double[3];
        String queryString;
        Query query;
        Object sumaAvaluos;

        //calculo de avaluo predios.
        queryString = "SELECT sum(valor_total_terreno) as v FROM PREDIO_AVALUO_CATASTRAL PAC " +
            " inner join predio p on p.id = pac.predio_id " +
            " INNER JOIN P_FICHA_MATRIZ_PREDIO PFMP ON PFMP.NUMERO_PREDIAL = P.NUMERO_PREDIAL " +
            " WHERE PFMP.FICHA_MATRIZ_ID = " + idFicha +
            " AND PAC.VIGENCIA =  (SELECT MAX(PAC2.VIGENCIA) FROM PREDIO_AVALUO_CATASTRAL PAC2 WHERE PAC2.PREDIO_ID = PAC.PREDIO_ID )";

        try {
            query = this.entityManager.createNativeQuery(queryString);

            sumaAvaluos = query.getSingleResult();
            if (sumaAvaluos == null) {
                answer[0] = 0.0;
            } else {
                answer[0] = ((BigDecimal) sumaAvaluos).doubleValue();
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "PredioDAOBean#calcularSumaAvaluosTotalesDePrediosFichaMatriz");
        }

        //calculo de avaluo construcciones Convencionales.
        queryString =
            "SELECT SUM(uc.avaluo) FROM Predio p,PFichaMatrizPredio pfmp, UnidadConstruccion uc " +
            " WHERE pfmp.numeroPredial = p.numeroPredial " +
            " AND p.id = uc.predio.id" +
            " AND pfmp.PFichaMatriz.id = :idFicha" +
            " AND uc.tipoConstruccion = :tipoCons";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idFicha", idFicha);
            query.setParameter("tipoCons", EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());

            sumaAvaluos = query.getSingleResult();
            if (sumaAvaluos == null) {
                answer[1] = 0.0;
            } else {
                answer[1] = (Double) sumaAvaluos;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "PredioDAOBean#calcularSumaAvaluosTotalesDePrediosFichaMatriz");
        }

        //calculo de avaluo construcciones No Convencionales.      
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idFicha", idFicha);
            query.setParameter("tipoCons", EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo());

            sumaAvaluos = query.getSingleResult();
            if (sumaAvaluos == null) {
                answer[2] = 0.0;
            } else {
                answer[2] = (Double) sumaAvaluos;
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "PredioDAOBean#calcularSumaAvaluosTotalesDePrediosFichaMatriz");
        }
        return answer;
    }

    /**
     * @see IPredioDAO#buscarPorMatricula(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> buscarPorMatricula(String circuloRegistralCodigo, String numeroRegistro) {

        List<Predio> resultado = new ArrayList<Predio>();

        try {
            String query = "SELECT p FROM Predio p " +
                " WHERE pp.circuloRegistral.codigo =  :circuloRegistralCodigo " +
                " AND pp.numeroRegistro = :numeroRegistro";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("circuloRegistralCodigo", circuloRegistralCodigo);
            q.setParameter("numeroRegistro", numeroRegistro);

            resultado = q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#buscarPorMatricula");
        }

        return resultado;
    }

    @Override
    public List obtenerPrediosEditadosGeograficamenteSNC(String codigoManzana) throws Exception {
        LOGGER.debug("PredioDAOBean#obtenerPrediosEditadosGeograficamenteSNC...INICIA");
        List<String> listaPredios = new ArrayList<String>();
        try {
            String sql = "Select P.Numero_Predial" +
                " From Predio P" +
                " Where P.Numero_Predial In (" +
                " Select distinct(h.Numero_Predial)" +
                " From H_Predio H" +
                " WHERE h.tramite_id in (" +
                " With Datorectificaciontramite As (" +
                " Select Ct.Tramite_Id,Ctd.Dato" +
                " From Comision_Tramite Ct" +
                " Join Comision_Tramite_Dato Ctd On Ct.Id=Ctd.Comision_Tramite_Id" +
                " Where Ctd.Dato In('Área construida','Área de terreno','Puntaje de construcción','Usos de la construcción'))" +
                " Select distinct(t.id)as idTramites" +
                " From Tramite T" +
                " Left Join Tramite_Rectificacion Tr   On T.Id= Tr.Tramite_Id" +
                " Left Join datoRectificacionTramite ctd  on ctd.tramite_id=t.id" +
                " Where T.Estado = 'FINALIZADO_APROBADO'" +
                " And    (" +
                " (T.Tipo_Tramite='1' AND T.Clase_Mutacion In('2','3','5'))" +
                " Or" +
                " (T.Tipo_Tramite='2' AND Tr.Dato_Rectificar_Id In('14','12','83'))" +
                " Or" +
                " (T.Tipo_Tramite ='6') And Ctd.Dato In('Área construida','Área de terreno','Puntaje de construcción','Usos de la construcción')" +
                ")" +
                ")" +
                " And H.Numero_Predial Like '" + codigoManzana + "%'" +
                ")";

            Query query = this.entityManager.createNativeQuery(sql);
            listaPredios = (ArrayList<String>) query.getResultList();
            return listaPredios;
        } catch (NoResultException e) {
            return listaPredios;
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerPrediosEditadosGeograficamenteSNC");
        }
    }

    /**
     * @see IPredioDAO#obtenerPrediosCanceladosQuintaCondominioPH(java.lang.String)
     * @author lorena.salamanca
     * @return
     */
    @Override
    public List<Predio> obtenerPrediosCanceladosQuintaCondominioPH(String numeroPredialFichaMatriz) {
        List<Predio> resultado = new ArrayList<Predio>();

        try {
            String query = "SELECT p FROM Predio p " +
                " WHERE p.numeroPredial LIKE :numeroPredialFichaMatriz " +
                " AND p.estado = :estado";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("numeroPredialFichaMatriz", numeroPredialFichaMatriz.substring(0, 22) +
                "%");
            q.setParameter("estado", EPredioEstado.CANCELADO.getCodigo());

            resultado = q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#obtenerPrediosCanceladosQuintaCondominioPH");
        }

        return resultado;
    }

    /**
     * @see IPredioDAO#obtenerMejorasPredio(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> obtenerMejorasPredio(String numeroPredial) {

        List<Predio> resultado = null;
        String queryString;
        Query query;

        queryString = "" +
            "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :numPredio_p" +
            " AND p.estado = 'ACTIVO'";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numPredio_p", numeroPredial.substring(0, 21) +
                EPredioCondicionPropiedad.CP_5.getCodigo() + "%");

            resultado = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#tieneMejorasPredio");
        }

        return resultado;

    }

    /**
     * @see IPredioDAO#validarExistenciaMatriculasPorMunicipio(java.util.List, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> validarExistenciaMatriculasPorMunicipio(List<String> matriculas,
        String municipioCodigo) {

        List<Predio> resultado = new ArrayList<Predio>();
        int partSize = 900;
        int len = matriculas.size();
        if (len > partSize) {
            List<Predio> resultadoTemp = new ArrayList<Predio>();
            int res = len % partSize;
            int parts = (len / partSize) + Math.min(res, 1);
            for (int i = 0; i < parts; i++) {
                List<String> matriculasAux = matriculas.subList(i * partSize, Math.min((i + 1) *
                    partSize, (len)));
                resultadoTemp.addAll(this.validarExistenciaMatriculasPorMunicipio(matriculasAux,
                    municipioCodigo));
            }
            resultado = resultadoTemp;
        } else {

            String queryString;
            Query query;

            queryString = "SELECT p FROM Predio p WHERE p.municipio.codigo = :codigoMunicipio" +
                " AND p.numeroRegistro IN :matriculas";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("codigoMunicipio", municipioCodigo);
                query.setParameter("matriculas", matriculas);

                resultado = query.getResultList();

            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "PredioDAOBean#validarExistenciaMatriculasPorMunicipio");
            }
        }

        return resultado;

    }

    /**
     * @see IPredioDAO#consultaPorNumeroPredial(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> consultaPorNumeroPredial(List<String> nPrediales) {

        List<Predio> resultado = new ArrayList<Predio>();
        int partSize = 900;
        int len = nPrediales.size();
        if (len > partSize) {
            List<Predio> resultadoTemp = new ArrayList<Predio>();
            int res = len % partSize;
            int parts = (len / partSize) + Math.min(res, 1);
            for (int i = 0; i < parts; i++) {
                List<String> matriculasAux = nPrediales.subList(i * partSize, Math.min((i + 1) *
                    partSize, (len)));
                resultadoTemp.addAll(this.consultaPorNumeroPredial(matriculasAux));
            }
            resultado = resultadoTemp;
        } else {

            String queryString;
            Query query;

            queryString = "SELECT p FROM Predio p WHERE p.numeroPredial IN :prediales";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("prediales", nPrediales);

                resultado = query.getResultList();

            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "PredioDAOBean#consultaPorNumeroPredial");
            }
        }

        return resultado;

    }

    /**
     * @see IPredioDAO#consultaPorNumeroPredial(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<Predio> consultaPorPredioIds(List<Long> predioIds) {

        List<Predio> resultado = new ArrayList<Predio>();
        int partSize = 900;
        int len = predioIds.size();
        if (len > partSize) {
            List<Predio> resultadoTemp = new ArrayList<Predio>();
            int res = len % partSize;
            int parts = (len / partSize) + Math.min(res, 1);
            for (int i = 0; i < parts; i++) {
                List<Long> matriculasAux = predioIds.subList(i * partSize, Math.min((i + 1) *
                    partSize, (len)));
                resultadoTemp.addAll(this.consultaPorPredioIds(matriculasAux));
            }
            resultado = resultadoTemp;
        } else {

            String queryString;
            Query query;

            queryString = "SELECT p FROM Predio p WHERE p.id IN :prediales";

            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("prediales", predioIds);

                resultado = query.getResultList();

            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "PredioDAOBean#consultaPorPredioIds");
            }
        }

        return resultado;

    }

    /**
     *
     * @author leidy.gonzalez
     * @see IPredioDAO#contarPrediosPorRangoNumeroPredial
     */
    @Implement
    @Override
    public Integer contarPrediosPorRangoNumeroPredial(String numeroPredial,
        String numeroPredialFinal) {

        Integer conteoPredio = 0;

        Query q = entityManager
            .createQuery("SELECT COUNT (p.numeroPredial)" +
                " FROM Predio p" +
                " WHERE p.numeroPredial BETWEEN :numeroPredial AND :numeroPredialFinal");

        q.setParameter("numeroPredial", numeroPredial);
        q.setParameter("numeroPredialFinal", numeroPredialFinal);

        try {
            conteoPredio = (Integer.parseInt(q.getSingleResult()
                .toString()));
            return conteoPredio;

        } catch (Exception ex) {
            LOGGER.error("Error on IPredioDAO#contarPrediosPorRangoNumeroPredial: " +
                ex.getMessage());

            return null;
        }

    }

    /**
     * @see IPredioDAO#obtenerNumerosTerrenoPorPredioHastaManzana(java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public List<String> obtenerNumerosTerrenoPorPredioHastaManzana(String predioAManzana) {

        LOGGER.debug("on PredioDAOBean#obtenerNumerosTerrenoPorPredioHastaManzana ...");

        List<String> answer;
        List<Predio> queryAnswer;
        String queryString;
        Query query;
        List<String> condicionesPropiedad = new ArrayList<String>();
        condicionesPropiedad.add(EPredioCondicionPropiedad.CP_9.getCodigo());
        condicionesPropiedad.add(EPredioCondicionPropiedad.CP_8.getCodigo());
        condicionesPropiedad.add(EPredioCondicionPropiedad.CP_1.getCodigo());
        

        answer = null;
        queryString = "SELECT p FROM Predio p WHERE p.numeroPredial LIKE :manzanaP "
            + "AND p.condicionPropiedad NOT IN (:condPropP)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("manzanaP", predioAManzana + "%");
            query.setParameter("condPropP", condicionesPropiedad);
            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                answer = new ArrayList<String>();
                for(Predio p : queryAnswer) {
                    if(!answer.contains(p.getTerreno())) {
                        answer.add(p.getTerreno());
                    }
                }
            }
        }
        catch(Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PredioDAOBean#obtenerNumerosTerrenoPorPredioHastaManzana");
        }


        return answer;
    }

}
