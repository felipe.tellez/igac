/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.ITramiteDepuracionDAO;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase para las operaciones en DB de la entidad TramiteDepuracion
 *
 * @author felipe.cadena
 */
@Stateless
public class TramiteDepuracionDAOBean extends GenericDAOWithJPA<TramiteDepuracion, Long>
    implements ITramiteDepuracionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDepuracionDAOBean.class);

    /**
     * @see ITramiteDepuracionDAO#buscarPorIdTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDepuracion> buscarPorIdTramite(Long idTramite) {

        List<TramiteDepuracion> resultado = null;
        String queryString;
        Query query;

        queryString = "SELECT td FROM TramiteDepuracion td " +
            " JOIN td.tramite t WHERE t.id = :idTramite" +
            " ORDER BY td.id DESC";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idTramite", idTramite);

            resultado = query.getResultList();

            for (TramiteDepuracion td : resultado) {
                Hibernate.initialize(td.getTramiteDepuracionObservacions());
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex, "TramiteDepuracion");
        }

        return resultado;
    }

    /**
     * @see ITramiteDepuracionDAO#buscarPorIds
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDepuracion> buscarPorIds(long[] idsTramites, String sortField,
        String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        List<TramiteDepuracion> resultado = (List<TramiteDepuracion>) this.buscarPorIdsCount(
            idsTramites, false, sortField, sortOrder, filters, rowStartIdxAndCount);

        return resultado;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Metodo para consultar las entidades de tramiteDepuracion y usarlo para los filtros de una
     * tabla de PrimeFaces.
     *
     * @author felipe.cadena
     * @modified leidy.gonzalez
     * @return
     */
    private Object buscarPorIdsCount(long[] idsTramites, boolean isCount, String sortField,
        String sortOrder,
        Map<String, String> filters, final int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteDepuracionDAOBean#buscarPorIdsCount");

        List<TramiteDepuracion> resultado = null;
        List<Long> idsList = new ArrayList<Long>();
        BigInteger answer2;
        StringBuilder queryString, idTramiteIn;
        Query query;

        queryString = new StringBuilder();
        idTramiteIn = new StringBuilder();

        for (long id : idsTramites) {
            idsList.add(id);
        }

        idTramiteIn.append(" AND t.id IN :idsTramite");

        if (!isCount) {
            queryString
                .append(
                    "SELECT distinct td FROM TramiteDepuracion td LEFT JOIN FETCH td.tramite t" +
                    " LEFT JOIN FETCH t.predio p " +
                    " LEFT JOIN FETCH t.departamento LEFT JOIN FETCH t.municipio " +
                    " LEFT JOIN FETCH p.municipio m LEFT JOIN FETCH p.departamento d JOIN FETCH t.solicitud solicitud " +
                    " LEFT JOIN FETCH t.tramitePredioEnglobes tpe ");
        } else {
            // N: esta, para dejarla más liviana se hace de forma diferente
            queryString.append("SELECT count(td) " +
                " FROM TramiteDepuracion td  LEFT JOIN FETCH td.tramite t");
        }

        queryString.append(" WHERE td.viable like 'SI'" +
            " AND td.id=(SELECT max(td2.id) FROM TramiteDepuracion td2 WHERE td.tramite.id=td2.tramite.id)");

        if (filters != null) {
            for (String campo : filters.keySet()) {
                if (campo.equals("tramite.numeroRadicacion")) {
                    queryString.append(" AND t.numeroRadicacion like :numeroRadicacion");
                }
                if (campo.equals("tramite.predio.municipio.nombre")) {
                    queryString.append(" AND UPPER(p.municipio.nombre) like :nombreMunicipio");
                }
                if (campo.equals("tramite.predio.numeroPredial")) {
                    queryString.append(" AND p.numeroPredial like :numeroPredial");
                }
                LOGGER.debug("Campo filtro " + campo);
                LOGGER.debug("Tamaño " + filters.get(campo).length());
            }
        }

        queryString.append(idTramiteIn);
        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {

            if (sortField.contains(".municipio.")) {
                queryString.append(" ORDER BY m.nombre").append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            } else if (sortField.contains(".departamento.")) {
                queryString.append(" ORDER BY d.nombre").append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            } else if (sortField.contains(".predio.")) {
                queryString.append(" ORDER BY p.numeroPredial").append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            } else {
                sortField = sortField.substring(8);
                queryString.append(" ORDER BY t.").append(sortField).append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

        }
        try {
            query = this.entityManager.createQuery(queryString.toString());

            if (filters != null) {
                for (String campo : filters.keySet()) {
                    if (campo.equals("tramite.numeroRadicacion")) {
                        query.setParameter("numeroRadicacion", "%" + filters.get(campo).
                            toUpperCase() + "%");
                    }
                    if (campo.equals("tramite.predio.municipio.nombre")) {
                        query.setParameter("nombreMunicipio",
                            "%" + filters.get(campo).toUpperCase() + "%");
                    }
                    if (campo.equals("tramite.predio.numeroPredial")) {
                        query.setParameter("numeroPredial", "%" + filters.get(campo).toUpperCase() +
                            "%");
                    }

                }
            }

            query.setParameter("idsTramite", idsList);

            if (!isCount) {
                resultado = (List<TramiteDepuracion>) super.findInRangeUsingQuery(query,
                    rowStartIdxAndCount);

                for (TramiteDepuracion td : resultado) {
                    Hibernate.initialize(td.getTramite());
                    Hibernate.initialize(td.getTramite().getPredio());
                    if (td.getTramite().getPredio() != null) {
                        Hibernate.initialize(td.getTramite().getPredio().getDepartamento());
                        Hibernate.initialize(td.getTramite().getPredio().getMunicipio());
                    }
                    Hibernate.initialize(td.getTramiteDepuracionObservacions());
                }

                return resultado;
            } else {
                answer2 = new BigInteger(query.getSingleResult().toString());
                return answer2;
            }
        } catch (Exception e) {
            LOGGER.error("error on TramiteDAOBean#findTramitesAsignacionDeEjecutor: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "TramiteDAOBean#findTramitesAsignacionDeEjecutor");
        }
    }

    // ---------------------------------------------------------------- //
    @SuppressWarnings("unchecked")
    /**
     * @see ITramite#buscarTramiteDepuracionPorDigitalizador
     * @author david.cifuentes
     */
    @Implement
    @Override
    public List<TramiteDepuracion> buscarTramiteDepuracionPorDigitalizador(
        String digitalizador) {

        List<TramiteDepuracion> resultado = null;
        String queryString;
        Query query;

        queryString = "SELECT td FROM TramiteDepuracion td " +
            " JOIN td.tramite t WHERE td.nombreDigitalizador = :digitalizador";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("digitalizador", digitalizador);

            resultado = query.getResultList();

            for (TramiteDepuracion td : resultado) {
                Hibernate.initialize(td.getTramiteDepuracionObservacions());
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex, "TramiteDepuracion");
        }

        return resultado;
    }

    // -------------------------------------------------- //
    /**
     * @see IDocumentoDAO#verificarTramitesProvenientesDepuracion(List<Long>)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    public List<Long> verificarTramitesConRegistroTramiteDepuracion(
        List<Long> tramiteIds) {

        List<TramiteDepuracion> tramitesDepuracion = null;
        List<Long> result = new ArrayList<Long>();
        Query query;
        String queryToExecute = "SELECT td FROM TramiteDepuracion td " +
            " JOIN FETCH td.tramite t " +
            " WHERE t.id IN (:tramiteIds)";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("tramiteIds", tramiteIds);

            tramitesDepuracion = (List<TramiteDepuracion>) query.getResultList();

            if (tramitesDepuracion != null && !tramitesDepuracion.isEmpty()) {
                for (TramiteDepuracion td : tramitesDepuracion) {
                    result.add(td.getTramite().getId());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#verificarTramitesProvenientesDepuracion");
        }
        return result;
    }

}
