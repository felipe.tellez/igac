/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_ROL_ACTIVIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmRolActividad.findAll", query = "SELECT b FROM BpmRolActividad b"),
    @NamedQuery(name = "BpmRolActividad.findById", query = "SELECT b FROM BpmRolActividad b WHERE b.id = :id"),
    @NamedQuery(name = "BpmRolActividad.findByIdRol", query = "SELECT b FROM BpmRolActividad b WHERE b.idRol = :idRol"),
    @NamedQuery(name = "BpmRolActividad.findByListIdRol", query = "SELECT b FROM BpmRolActividad b WHERE b.idRol in (:idRol)"),
    @NamedQuery(name = "BpmRolActividad.findByIdActividad", query = "SELECT b FROM BpmRolActividad b WHERE b.idActividad = :idActividad"),
    @NamedQuery(name = "BpmRolActividad.findByIdActividadList", query = "SELECT b FROM BpmRolActividad b WHERE b.idActividad = :idActividad AND b.idRol in (:idRol)")})

public class BpmRolActividad implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "ID_ROL")
    private long idRol;
    @Basic(optional = false)
    @Column(name = "ID_ACTIVIDAD")
    private long idActividad;

    public BpmRolActividad() {
    }

    public BpmRolActividad(Long id) {
        this.id = id;
    }

    public BpmRolActividad(Long id, long idRol, long idActividad) {
        this.id = id;
        this.idRol = idRol;
        this.idActividad = idActividad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdRol() {
        return idRol;
    }

    public void setIdRol(long idRol) {
        this.idRol = idRol;
    }

    public long getIdActividad() {
        return idActividad;
    }

    public void setIdActividad(long idActividad) {
        this.idActividad = idActividad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmRolActividad)) {
            return false;
        }
        BpmRolActividad other = (BpmRolActividad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmRolActividad[ id=" + id + " ]";
    }
    
}
