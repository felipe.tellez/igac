package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.actualizacion.GeneracionFormularioSbc;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;

/**
 * Implementación de los servicios de persistencia del objeto Actualización.
 *
 * @author jamir.avila.
 */
@Stateless
public class ActualizacionDAOBean extends GenericDAOWithJPA<Actualizacion, Long> implements
    IActualizacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDAOBean.class);

    /**
     * {@link #crear(Actualizacion)}
     */
    @Override
    public Actualizacion crear(Actualizacion actualizacion) throws ExcepcionSNC {
        LOGGER.debug("inicio de creación");
        try {
            entityManager.persist(actualizacion);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("JAAM::Actualización", ESeveridadExcepcionSNC.ERROR, e.
                getMessage(), null);
        }
        LOGGER.debug("fin de creación");
        return actualizacion;
    }

    /**
     * {@link #actualizar(Actualizacion)}
     */
    @Override
    public void actualizar(Actualizacion actualizacion) throws ExcepcionSNC {
        LOGGER.debug("inicio de actualizar actualización");
        try {
            entityManager.merge(actualizacion);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("JAAM::Actualización", ESeveridadExcepcionSNC.ERROR, e.
                getMessage(), null);
        }
        LOGGER.debug("fin de actualizar actualización");
    }

    /**
     * {@link #recuperar(Long)}
     */
    @Override
    public Actualizacion recuperar(Long idActualizacion) throws ExcepcionSNC {
        LOGGER.debug("inicio de recuperación de actualización con id: " + idActualizacion);
        Actualizacion actualizacion = null;

        try {
            Query query = entityManager.createNamedQuery(
                "recuperarDepartamentosMunicipiosPorIdActualizacion");
            query.setParameter("id", idActualizacion);
            try {
                actualizacion = (Actualizacion) query.getSingleResult();
                actualizacion.getActualizacionEventos().size();
                actualizacion.getConvenios().size();
                actualizacion.getActualizacionContratos().size();
                actualizacion.getActualizacionResponsables().size();
                if (actualizacion.getActualizacionResponsables() != null &&
                    actualizacion.getActualizacionResponsables().size() > 0) {
                    actualizacion.getActualizacionResponsables().get(0).getRecursoHumanos().size();
                }
                actualizacion.getActualizacionSedeComisions().size();
            } catch (NoResultException e) {
                LOGGER.info("No encontró la actualización de id: " + idActualizacion);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("JAAM::Actualización", ESeveridadExcepcionSNC.ERROR, e.
                getMessage(), null);
        }
        LOGGER.debug("fin de recuperación de actualización con id: " + idActualizacion);

        return actualizacion;
    }

    //------------------------------------------------ //
    /**
     * @see IActualizacionDAO#buscarActualizacionPorIdConConveniosYEntidades(Convenio convenio)
     * @author david.cifuentes
     */
    @Override
    public Actualizacion buscarActualizacionPorIdConConveniosYEntidades(Long idActualizacion) throws
        ExcepcionSNC {
        LOGGER.debug("inicio de recuperación de actualización con id: " + idActualizacion);
        Actualizacion actualizacion = null;

        try {
            Query query = entityManager.createNamedQuery(
                "buscarActualizacionPorIdConConveniosYEntidades");
            query.setParameter("id", idActualizacion);
            try {
                actualizacion = (Actualizacion) query.getSingleResult();
                actualizacion.getDepartamento().getNombre();
                actualizacion.getMunicipio().getNombre();
                if (actualizacion != null && actualizacion.getConvenios() != null) {
                    actualizacion.getConvenios().size();
                    for (Convenio c : actualizacion.getConvenios()) {
                        if (c.getConvenioEntidads() != null) {
                            c.getConvenioEntidads().size();
                        }
                    }
                }

            } catch (NoResultException e) {
                LOGGER.info("No encontrón la actualización de id: " + idActualizacion);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Actualización", ESeveridadExcepcionSNC.ERROR, e.getMessage(),
                null);
        }
        LOGGER.debug("fin de recuperación de actualización con id: " + idActualizacion);

        return actualizacion;
    }

    /**
     * @author franz.gamba
     * @see IActualizacionDAO#getActualizacionConResponsablesByActualizacionId(Long)
     */
    @Override
    public Actualizacion getActualizacionConResponsablesByActualizacionId(
        Long actualizacionId) {
        Actualizacion actualizacion = null;

        String query = "SELECT a FROM Actualizacion a " +
            "JOIN FETCH a.municipio " +
            "WHERE a.id = :actualizacionId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("actualizacionId", actualizacionId);

            actualizacion = (Actualizacion) q.getSingleResult();
            actualizacion.getActualizacionResponsables().size();
            for (ActualizacionResponsable ar : actualizacion.getActualizacionResponsables()) {
                ar.getRecursoHumanos().size();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Actualización", ESeveridadExcepcionSNC.ERROR, e.getMessage(),
                null);
        }
        return actualizacion;
    }

    /**
     * @author javier.aponte
     * @see IActualizacionDAO#obtenerUltimaActualizacionPorMunicipioYZona(String, String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Actualizacion obtenerUltimaActualizacionPorMunicipioYZona(
        String codigoMunicipio, String zona) {

        Actualizacion answer = null;
        List<Actualizacion> actualizaciones = null;

        String query = "SELECT a FROM Actualizacion a " +
            " LEFT JOIN a.convenios c " +
            " WHERE a.municipio.codigo = :codigoMunicipio " +
            " AND c.zona = :zona" +
            " ORDER BY a.fecha DESC";
        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("codigoMunicipio", codigoMunicipio);
            q.setParameter("zona", zona);
            q.setMaxResults(1);

            actualizaciones = q.getResultList();

            if (actualizaciones != null && !actualizaciones.isEmpty()) {

                answer = actualizaciones.get(0);

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Error en el método obtenerUltimaActualizacionPorMunicipioYZona ",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
        return answer;

    }

    /**
     * @author javier.barajas
     * @see IActualizacionDAO#agregarGenerarFormularioSbc(GeneracionFormularioSbc)
     */
    @Override
    public ActualizacionEvento agregarActualizacionEvento(
        ActualizacionEvento actualizacionEvento) {
        LOGGER.info("inicio de agregarActualizacionEvento");

        if (actualizacionEvento.getActualizacion() == null) {
            throw new ExcepcionSNC("Actualización", ESeveridadExcepcionSNC.ERROR,
                "Imposible encontrar la actualización correspondiente a la actualizacioEvento de id: " +
                actualizacionEvento.getId(), null);
        }

        Long actualizacionId = actualizacionEvento.getActualizacion().getId();
        Actualizacion actualizacion = entityManager.find(Actualizacion.class, actualizacionId);
        List<ActualizacionEvento> actualizacionEventos = actualizacion.getActualizacionEventos();

        actualizacionEventos.add(actualizacionEvento);
        LOGGER.info("inicio de agregarActualizacionEvento");

        actualizacionEvento = entityManager.merge(actualizacionEvento);

        LOGGER.info("Paso el merge");
        return actualizacionEvento;
    }

    /**
     * @author javier.barajas
     * @see IActualizacionDAO#agregarGenerarFormularioSbc(GeneracionFormularioSbc)
     */
    @Override
    public GeneracionFormularioSbc agregarGenerarFormularioSbc(
        GeneracionFormularioSbc generarFormularioSbc) {

        LOGGER.info("inicio de agregarGenerarFormularioSbc(");

        if (generarFormularioSbc.getActualizacion() == null) {
            throw new ExcepcionSNC("Actualización", ESeveridadExcepcionSNC.ERROR,
                "Imposible encontrar la actualización correspondiente a la GeneracionFormularioSbc de id: " +
                generarFormularioSbc.getId(), null);
        }

        Long actualizacionId = generarFormularioSbc.getActualizacion().getId();
        Actualizacion actualizacion = entityManager.find(Actualizacion.class, actualizacionId);
        List<GeneracionFormularioSbc> generarFormularios = actualizacion.
            getGeneracionFormularioSbcs();

        generarFormularios.add(generarFormularioSbc);
        LOGGER.info("inicio de agregarGeneracionFormularioSbc");

        generarFormularioSbc = entityManager.merge(generarFormularioSbc);

        LOGGER.info("Paso el merge");
        return generarFormularioSbc;
    }

    /**
     * @see IActualizacionDAO#obtenerActualizacionLevantamiento(List<ActualizacionLevantamiento>)
     * @author javier.barajas
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ActualizacionLevantamiento> obtenerActualizacionLevantamiento(
        Long idActualizacionSeleccionada, Long levantamientoId) {

        List<ActualizacionLevantamiento> answer = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT al FROM ActualizacionLevantamiento al " +
            " LEFT JOIN FETCH al.levantamientoAsignacion la " +
            " WHERE al.actualizacion.id = :idActualizacionSeleccionada ");

        if (levantamientoId != null) {

            query.append(" AND la.id = :levantamientoId ");

        }

        try {
            Query q = this.entityManager.createQuery(query.toString());

            q.setParameter("idActualizacionSeleccionada", idActualizacionSeleccionada);
            if (levantamientoId != null) {
                q.setParameter("levantamientoId", levantamientoId);
            }
            answer = q.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Error en el método obtenerActualizacionLevantamiento",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
        return answer;
    }

    /**
     * @see IActualizacionDAO#obtenerTopografosActualizacion(List<ActualizacionLevantamiento>)
     * @author javier.barajas
     */
    @Override
    public List<LevantamientoAsignacion> obtenerTopografosActualizacion(
        List<ActualizacionLevantamiento> listaLevantamiento) {

        List<LevantamientoAsignacion> answer = new ArrayList<LevantamientoAsignacion>();
        LevantamientoAsignacion topografoTemp = new LevantamientoAsignacion();

        String query = "SELECT la FROM LevantamientoAsignacion la " +
            " WHERE la.id = :topografoId ";
        try {

            for (ActualizacionLevantamiento actRow : listaLevantamiento) {
                Query q = this.entityManager.createQuery(query);
                q.setParameter("topografoId", actRow.getLevantamientoAsignacion().getId());
                topografoTemp = (LevantamientoAsignacion) q.getSingleResult();
                //LOGGER.debug("Topografo temp:"+topografoTemp.getId());
                //LOGGER.debug("Topografo Ant si hay:"+answer.contains(topografoTemp));

                if (!answer.contains(topografoTemp)) {
                    topografoTemp.getRecursoHumano();
                    answer.add(topografoTemp);
                }

            }
            LOGGER.debug("no Topografo:" + answer.size());

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Error en el método obtenerLevantamientoAsignacion",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
        return answer;

    }

    /**
     * @see IActualizacionDAO#obtenerActualizacionConMunicipio(Long)
     */
    @Override
    public Actualizacion obtenerActualizacionConMunicipio(Long actualizacionId) {

        LOGGER.debug("ActualizacionDAOBean#obtenerActualizacionConMunicipio...INICIA");
        String sqlQuery = "SELECT model FROM Actualizacion model WHERE model.id=:actualizacionId ";

        Query query = this.getEntityManager().createQuery(sqlQuery);
        query.setParameter("actualizacionId", actualizacionId);

        try {
            Actualizacion actualizacion = (Actualizacion) query.getSingleResult();
            Hibernate.initialize(actualizacion.getMunicipio());
            Hibernate.initialize(actualizacion.getMunicipio().getDepartamento());
            Hibernate.initialize(actualizacion.getDepartamento());
            return actualizacion;
        } catch (Exception e) {
            throw new ExcepcionSNC("Error en el método obtenerActualizacionConMunicipio",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        } finally {
            LOGGER.debug("ActualizacionDAOBean#obtenerActualizacionConMunicipio...INICIA");

        }

    }

    /**
     * @see IActualizacionDAO#obtenerManazanasporTopografosActualizacion(Long)
     * @author javier.barajas
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ActualizacionLevantamiento> obtenerManazanasporTopografosActualizacion(
        Long actualizacionId, Long levantamientoAsignacionId) {

        List<ActualizacionLevantamiento> answer = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT al FROM ActualizacionLevantamiento al " +
            " LEFT JOIN FETCH al.levantamientoAsignacion la " +
            " WHERE al.actualizacion.id = :idActualizacionSeleccionada " +
            " AND la.id= :levantamientoAsignacionId ");

        try {
            Query q = this.entityManager.createQuery(query.toString());

            q.setParameter("idActualizacionSeleccionada", actualizacionId);
            q.setParameter("levantamientoAsignacionId", levantamientoAsignacionId);

            answer = q.getResultList();

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Error en el método obtenerActualizacionLevantamiento",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
        return answer;

    }

}
