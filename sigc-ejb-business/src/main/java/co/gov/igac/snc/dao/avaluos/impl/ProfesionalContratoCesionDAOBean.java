package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IProfesionalContratoCesionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoCesion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IProfesionalContratoCesionDAO
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class ProfesionalContratoCesionDAOBean extends
    GenericDAOWithJPA<ProfesionalContratoCesion, Long> implements IProfesionalContratoCesionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProfesionalContratoCesionDAOBean.class);

    /**
     * @see IProfesionalContratoCesionDAO#obtenerCesionesDeContratoActivo(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<ProfesionalContratoCesion> obtenerCesionesDeContratoActivo(
        Long idContrato) {

        LOGGER.debug("Inicio ProfesionalContratoCesionDAOBean#obtenerCesionesDeContratoActivo");

        List<ProfesionalContratoCesion> result = null;

        String queryString;
        Query query;

        queryString = "SELECT pcc" +
            " FROM ProfesionalContratoCesion pcc" +
            " LEFT JOIN FETCH pcc.cedeProfesional" +
            " LEFT JOIN FETCH pcc.recibeProfesional" +
            " WHERE pcc.profesionalAvaluosContrato.id = :idContrato";

        try {
            query = this.entityManager.createQuery(queryString).setFirstResult(0).setMaxResults(1);
            query.setParameter("idContrato", idContrato);

            result = query.getResultList();

            for (ProfesionalContratoCesion pcc : result) {
                pcc.getCedeProfesional();
                pcc.getRecibeProfesional();
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER, e,
                    "ProfesionalContratoAdicionDAOBean#obtenerFechaFinalPAContratoAdiciones");

        }

        LOGGER.debug("Fin ProfesionalContratoCesionDAOBean#obtenerCesionesDeContratoActivo");

        return result;

    }

}
