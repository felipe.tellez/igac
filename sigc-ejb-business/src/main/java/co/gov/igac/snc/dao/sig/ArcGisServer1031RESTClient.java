package co.gov.igac.snc.dao.sig;

import java.io.IOException;
import java.io.StringWriter;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.glassfish.jersey.uri.UriComponent;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.snc.dao.sig.vo.ArcgisServerResponse;
import co.gov.igac.snc.util.exceptions.RemoteServiceException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * Cliente para comunicarse con ArcGis Server a través de REST
 * 
 * Documentación ESRI :
 * http://services.arcgisonline.com/ArcGIS/SDK/REST/index.html?overview.html
 * 
 * El cliente java rest utiliza la librería com.sun.jersey.*
 * 
 * El cliente Java Rest invoca las siguientes operaciones Envía el Job al
 * servidor: http://services.arcgisonline.com/ArcGIS/SDK/REST/gpsubmit.html
 * Consulta el estado de ejecución del servidor:
 * http://services.arcgisonline.com/ArcGIS/SDK/REST/gpjob.html Obtiene los
 * resultados del job cuando termina la ejecución:
 * http://services.arcgisonline.com/ArcGIS/SDK/REST/gpresult.html (Requiere una
 * 
 * consulta adicional al servidor por parámetro
 * 
 * Otros http://172.17.2.221:6080/arcgis/sdk/rest/index.html#/Generate_Token/02
 * ss00000059000000/
 * 
 * @author juan.mendez
 */
public class ArcGisServer1031RESTClient extends AbstractArcGisServerClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArcGisServer1031RESTClient.class);

	// prefijo de la carpeta donde se encuentran los servicios de
	// geoprocesamiento (Carpeta con seguridad de arcgis)
	private static final String URL_SECURE_PREFIX = "rest/services/snc-private/";

	/**
	 * 
	 * @param remoteUrl
	 *            Url del geoservicio publicado en arcgis server
	 * @param polling
	 *            cantidad de milisegundos para hacer "polling" al servidor AGS
	 */
	public ArcGisServer1031RESTClient(String remoteServerBaseUrl, String environment, int polling,
			ArcgisServerTokenDTO token) {
		this.url = remoteServerBaseUrl;
		this.environment = environment;
		this.pollingTime = polling;
		this.token = token;
	}

	
	@SuppressWarnings("unchecked")
	public Document obtenerImagenSimplePredio(String codigosPrediales) throws RemoteServiceException {
		LOGGER.debug("obtenerImagenSimplePredio");
		String responseXml = null;
		try {
			String serviceUrl = url + URL_SECURE_PREFIX + "ToolImagenSimplePredio/GPServer/ToolImagenSimplePredio/";
			LOGGER.debug("serviceUrl:" + serviceUrl);
			Client client = createClient();
			WebResource resource = client.resource(serviceUrl);
			resource.accept(MediaType.APPLICATION_JSON_TYPE);

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("environment", environment);
			jsonObj.put("codigos_prediales", codigosPrediales);

			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("token", token.getToken());
			params.add("parameters_as_json", jsonToStringParam(jsonObj));
			params.add("f", "pjson");
			String response = resource.path("submitJob").queryParams(params).get(String.class);
			
			// LOGGER.debug(response);
			ObjectMapper mapper = new ObjectMapper();
			ArcgisServerResponse serverResponse = mapper.readValue(response, ArcgisServerResponse.class);
			serverResponse.validateServerError();
			ArcgisServerResponse serverResult = checkJobStatus(serverResponse, serviceUrl);
			responseXml = getJobResult(serverResult, serviceUrl);
		} catch (Exception e) {
			throw new RemoteServiceException(e.getMessage(), e);
		}
		return SigDAOUtil.parseXmlResponse(responseXml);
	}
	
	
	@SuppressWarnings("unchecked")
	public Document obtenerImagenAvanzadaPredio(String codigosPrediales) throws RemoteServiceException {
		LOGGER.debug("obtenerImagenAvanzadaPredio");
		String responseXml = null;
		try {
			String serviceUrl = url + URL_SECURE_PREFIX + "ToolImagenAvanzadaPredio/GPServer/ToolImagenAvanzadaPredio/";
			LOGGER.debug("serviceUrl:" + serviceUrl);
			Client client = createClient();
			WebResource resource = client.resource(serviceUrl);
			resource.accept(MediaType.APPLICATION_JSON_TYPE);

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("environment", environment);
			jsonObj.put("codigos_prediales", codigosPrediales);

			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("token", token.getToken());
			params.add("parameters_as_json", jsonToStringParam(jsonObj));
			params.add("f", "pjson");
			String response = resource.path("submitJob").queryParams(params).get(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
            ArcgisServerResponse serverResult = checkJobStatus(serverResponse, serviceUrl);
            responseXml = getJobResult(serverResult, serviceUrl);
        } catch (Exception e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return SigDAOUtil.parseXmlResponse(responseXml);
    }

    /**
     * Envía Job para Realizar la eliminación de la versión de SDE asociada a un trámite
     *
     * @param idJob identificador del job a ejecutar.
     * @throws RemoteServiceException
     */
    @SuppressWarnings("unchecked")
    public void enviarJobEliminarVersionTramite(Long idJob) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolVersionesEliminarPorJob/GPServer/ToolVersionesEliminarPorJob/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl);
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", idJob.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).get(String.class);

            ObjectMapper mapper = new ObjectMapper();
            //fire and forget... (async)
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (Exception e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }

    /**
     * Obtiene las zonas homogeneas para un conjunto de predios en edición
     *
     * @param numerosPrediales
     * @param nombreZipReplica
     * @return
     * @throws RemoteServiceException
     */
    @SuppressWarnings("unchecked")
    public Document obtenerZonasHomogeneas(String numerosPrediales, String nombreZipReplica) throws
        RemoteServiceException {
        String xmlResponse = null;
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolObtenerZonas/GPServer/ToolObtenerZonas/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("numeros_prediales", numerosPrediales);
            jsonObj.put("id_archivo_remoto", nombreZipReplica);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("f", "pjson");
            params.add("parameters_as_json", jsonToStringParam(jsonObj));

            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
            // espera hasta que el servicio remoto termine su ejecución y obtiene la respuesta
            ArcgisServerResponse serverResult = checkJobStatus(serverResponse, serviceUrl);
            // Mensaje XML de respuesta
            xmlResponse = getJobResult(serverResult, serviceUrl);
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return SigDAOUtil.parseXmlResponse(xmlResponse);
    }

    /**
     *
     * @param numerosPrediales
     * @param nombreZipReplica
     * @return
     * @throws RemoteServiceException
     */
    @SuppressWarnings("unchecked")
    public void obtenerZonasHomogeneasAsync(Long identificador_job) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolObtenerZonasAsync/GPServer/ToolObtenerZonasAsync/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public void generarCCUAsync(Long identificador_job) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX + "ToolImagenCCU/GPServer/ToolImagenCCU/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).get(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public void generarFPDAsync(Long identificador_job) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX + "ToolImagenFPD/GPServer/ToolImagenFPD/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).get(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }
        
        @SuppressWarnings("unchecked")
	public void generarImagenPredioAsync(Long identificador_job) throws RemoteServiceException {
		try {
			String serviceUrl = url + URL_SECURE_PREFIX+"ToolImagenConsultaPredio/GPServer/ToolImagenConsultaPredio/";
			LOGGER.debug("serviceUrl:"+serviceUrl);
			Client client = createClient();
			WebResource resource = client.resource(serviceUrl+"/submitJob");
			resource.accept(MediaType.APPLICATION_JSON_TYPE);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("environment", environment);
			jsonObj.put("identificador_job", identificador_job.toString());
						
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("token", token.getToken());
			params.add("parameters_as_json", jsonToStringParam(jsonObj));
			params.add("f", "pjson");
			String response = resource.path("submitJob").queryParams(params).get(String.class);
			            
			ObjectMapper mapper = new ObjectMapper();
			ArcgisServerResponse serverResponse = mapper.readValue(response,ArcgisServerResponse.class);
			serverResponse.validateServerError();
		}  catch (IOException e) {
			throw new RemoteServiceException(e.getMessage(), e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void generarCPPAsync(Long identificador_job) throws RemoteServiceException {
		try {
			String serviceUrl = url + URL_SECURE_PREFIX+"ToolImagenCPP/GPServer/ToolImagenCPP/";
			LOGGER.debug("serviceUrl:"+serviceUrl);
			Client client = createClient();
			WebResource resource = client.resource(serviceUrl+"/submitJob");
			resource.accept(MediaType.APPLICATION_JSON_TYPE);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("environment", environment);
			jsonObj.put("identificador_job", identificador_job.toString());
						
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("token", token.getToken());
			params.add("parameters_as_json", jsonToStringParam(jsonObj));
			params.add("f", "pjson");
			String response = resource.path("submitJob").queryParams(params).get(String.class);
			            
			ObjectMapper mapper = new ObjectMapper();
			ArcgisServerResponse serverResponse = mapper.readValue(response,ArcgisServerResponse.class);
			serverResponse.validateServerError();
		}  catch (IOException e) {
			throw new RemoteServiceException(e.getMessage(), e);
		}
	}
	
	
	/**
	 * Obtener colindantes 
	 * 
	 * @param numerosPrediales
	 * @param tipoColindancia
	 * @return
	 * @throws RemoteServiceException
	 */
	@SuppressWarnings("unchecked")
    public Document obtenerColindantes(String numerosPrediales, String tipoColindancia, String JobID) throws RemoteServiceException {
		String xmlResponse = null;
		try {
			String serviceUrl = url + URL_SECURE_PREFIX+"ToolColindancia/GPServer/ToolColindancia/";
			LOGGER.debug("serviceUrl:"+serviceUrl);
			Client client = createClient();
			WebResource resource = client.resource(serviceUrl+"/submitJob");
			resource.accept(MediaType.APPLICATION_JSON_TYPE);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("environment", environment);
			jsonObj.put("codigos_prediales", numerosPrediales);
			jsonObj.put("tipo_colindancia", tipoColindancia);
            jsonObj.put("identificador_job", JobID);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("f", "pjson");
            params.add("parameters_as_json", jsonToStringParam(jsonObj));

            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
            // espera hasta que el servicio remoto termine su ejecución y obtiene la respuesta
            ArcgisServerResponse serverResult = checkJobStatus(serverResponse, serviceUrl);
            // Mensaje XML de respuesta
            xmlResponse = getJobResult(serverResult, serviceUrl);
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return SigDAOUtil.parseXmlResponse(xmlResponse);
    }

    /**
     * Codifica el objeto json para poder ser enviado como un par�metro por http rest de jersey
     *
     * https://java.net/jira/browse/JERSEY-2260
     * https://github.com/jersey/jersey/blob/541b64a44ca0668b386d83dd1e588047b5f5ebac/tests/e2e/src/test/java/org/glassfish/jersey/tests/e2e/common/UriComponentTest.java#L106
     *
     * @return
     * @throws IOException
     */
    private String jsonToStringParam(JSONObject obj) throws IOException {
        StringWriter out = new StringWriter();
        obj.writeJSONString(out);
        String parameters_as_json = out.toString();
        LOGGER.debug("json as string:" + parameters_as_json);
        String encodedJson = UriComponent.encode(parameters_as_json,
            UriComponent.Type.QUERY_PARAM_SPACE_ENCODED);
        LOGGER.debug("encoded json:" + encodedJson);
        return encodedJson;
    }

    /**
     * Obtiene las inconsistencias sobre Geometria predio,geometria manzana y zonas homogeneas con
     * respuesta sincrona
     *
     * @param numerosPrediales
     * @return
     * @throws RemoteServiceException
     *
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    public Document validarInconsistencias(String numerosPrediales) throws RemoteServiceException {
        String xmlResponse = null;
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolValidarInconsistencias/GPServer/ToolValidarInconsistencias/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificadoresPredios", numerosPrediales);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("f", "pjson");
            params.add("parameters_as_json", jsonToStringParam(jsonObj));

            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
            // espera hasta que el servicio remoto termine su ejecución y obtiene la respuesta
            ArcgisServerResponse serverResult = checkJobStatus(serverResponse, serviceUrl);
            // Mensaje XML de respuesta

            xmlResponse = getJobResult(serverResult, serviceUrl);
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return SigDAOUtil.parseXmlResponse(xmlResponse);
    }

    /**
     * Obtiene las inconsistencias sobre Geometria predio,geometria manzana y zonas homogeneas
     *
     * @param numerosPrediales
	 * @param nombreZipReplica
	 * @return
	 * @throws RemoteServiceException
     * 
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    public void validarInconsistenciasAsync(Long identificador_job) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolValidarInconsistenciasAsync/GPServer/ToolValidarInconsistenciasAsync/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }
    
    /**
     * Obtiene las inconsistencias sobre Geometria predio,geometria manzana y zonas homogeneas
     *
     * @param numerosPrediales
	 * @param nombreZipReplica
	 * @return
	 * @throws RemoteServiceException
     * 
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    public void validarInconsistenciasActualizacion(Long identificador_job, String enviroment,String ubicacionArchivo, String validacionZonas, String validacionPredios) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolValidarInconActu/GPServer/ToolValidarInconActu";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());
            jsonObj.put("ubicacionFileGdb", ubicacionArchivo);
            jsonObj.put("validacionZonas", validacionZonas);
            jsonObj.put("validacionPredios", validacionPredios);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }

    /**
	 * Validad si una construccion se encuentra geograficamente 
	 * 
	 * @param numerosPrediales	 
	 * @return
	 * @throws RemoteServiceException 
     * 
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    public Document validarGeometriaUnidadConstruccion(String numerosPrediales) throws
        RemoteServiceException {
        String xmlResponse = null;
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolValidarGeometriaUnidadesConstruccion/GPServer/ToolValidarGeometriaUnidadesConstruccion/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificadoresPredios", numerosPrediales);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("f", "pjson");
            params.add("parameters_as_json", jsonToStringParam(jsonObj));

            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
            // espera hasta que el servicio remoto termine su ejecución y obtiene la respuesta
            ArcgisServerResponse serverResult = checkJobStatus(serverResponse, serviceUrl);
            // Mensaje XML de respuesta
            xmlResponse = getJobResult(serverResult, serviceUrl);
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return SigDAOUtil.parseXmlResponse(xmlResponse);
    }

    /**
     *
     * @param identificador_job
     * @throws RemoteServiceException
     */
    @SuppressWarnings("unchecked")
    public void exportarDatosAsync(Long identificador_job) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolExportarDatos/GPServer/ToolExportarDatos/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param identificador_job
     * @throws RemoteServiceException
     */
    @SuppressWarnings("unchecked")
    public void aplicarCambiosAsync(Long identificador_job) throws RemoteServiceException {
        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolAplicarCambios/GPServer/ToolAplicarCambios/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("identificador_job", identificador_job.toString());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("parameters_as_json", jsonToStringParam(jsonObj));
            params.add("f", "pjson");
            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,
                ArcgisServerResponse.class);
            serverResponse.validateServerError();
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }

    /**
     *
     * Método que realiza el envío del job correspondiente a la actualizacion de colindantes.
     *
     * @author jonathan.chacon
     * @param numerosPrediales
     * @param tipoColindancia
     * @param JobID
     * @throws RemoteServiceException
     */
    @SuppressWarnings("unchecked")
    public void enviarJobActualizarColindancia(String numerosPrediales, String tipoColindancia, String JobID) throws RemoteServiceException {

        try {
            String serviceUrl = url + URL_SECURE_PREFIX +
                "ToolColindancia/GPServer/ToolColindancia/";
            LOGGER.debug("serviceUrl:" + serviceUrl);
            Client client = createClient();
            WebResource resource = client.resource(serviceUrl + "/submitJob");
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("environment", environment);
            jsonObj.put("codigos_prediales", numerosPrediales);
            jsonObj.put("tipo_colindancia", tipoColindancia);
            jsonObj.put("identificador_job", JobID);            

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("f", "pjson");
            params.add("parameters_as_json", jsonToStringParam(jsonObj));

            String response = resource.path("submitJob").queryParams(params).post(String.class);

            ObjectMapper mapper = new ObjectMapper();
            ArcgisServerResponse serverResponse = mapper.readValue(response,ArcgisServerResponse.class);
            serverResponse.validateServerError();

        }  catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
    }   

}
