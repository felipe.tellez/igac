package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ISolicitudDocumentoDAO;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class SolicitudDocumentoDAOBean extends
    GenericDAOWithJPA<SolicitudDocumento, Long> implements
    ISolicitudDocumentoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitudDocumentoDAOBean.class);

    @Override
    public SolicitudDocumento obtenerSolicitudDocumentoPorSolicitudId(
        Long solicitudId) {

        SolicitudDocumento result = null;
        Query query;
        String queryString;

        /*
         * Query para extraer el ultimo SolicitudDocumento asociado a la solicitud
         */
        queryString = " SELECT solDoc" + " FROM" + " SolicitudDocumento solDoc" +
            " WHERE" + " solDoc.solicitud.id = :idSolicitud" +
            " ORDER BY solDoc.id DESC";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", solicitudId);

            result = ((List<SolicitudDocumento>) query.getResultList()).get(0);

            /* Se hace fetch de la lista de documentos faltantes */
            Hibernate.initialize(result.getDetalleDocumentoFaltantes());
            Hibernate.initialize(result.getSoporteDocumento());

            for (DetalleDocumentoFaltante ddf : result
                .getDetalleDocumentoFaltantes()) {
                /* Se hace fetch del tipo de documento para cada detalle */
                Hibernate.initialize(ddf.getTipoDocumento());
            }

        } catch (NoResultException nrEx) {
            LOGGER.info("DocumentoDAOBean#buscarDocumentoPorPredioIdAndTipoDocumento: " +
                "La consulta del nombre del documento por predio id y tipo de documento no retorno datos");
            return result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e,
                    "SolicitudDocumentoDAOBean#obtenerDetallesDocumentosFaltantesPorIdSolicitud");
        }

        return result;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentoPorSolicitudIdAndTipoDocumentoId(Long, Long)
     * @author javier.aponte
     */
    @Override
    @Implement
    public SolicitudDocumento buscarDocumentoPorSolicitudIdAndTipoDocumentoId(
        Long idSolicitud, Long idTipoDocumento) {

        SolicitudDocumento result = null;

        String strQuery = "";
        Query query;

        strQuery = "SELECT sd" +
            " FROM SolicitudDocumento sd" +
            " JOIN FETCH sd.soporteDocumento d" +
            " WHERE sd.solicitud.id = :idSolicitud" +
            " AND d.tipoDocumento.id = :tipoDocumentoId ";

        try {
            query = this.entityManager.createQuery(strQuery);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("tipoDocumentoId", idTipoDocumento);

            result = (SolicitudDocumento) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.info(
                "SolicitudDocumentoDAOBean#buscarDocumentoPorSolicitudIdAndTipoDocumentoId: " +
                "La consulta de la solicitud documento por solicitud id y tipo de documento no retornó datos");
            return result;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e,
                "SolicitudDocumentoDAOBean#buscarDocumentoPorSolicitudIdAndTipoDocumentoId");
        }

        return result;
    }

}
