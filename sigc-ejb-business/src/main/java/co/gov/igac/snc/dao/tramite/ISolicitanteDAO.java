package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;

@Local
public interface ISolicitanteDAO extends IGenericJpaDAO<Solicitante, Long> {
    //TODO: Documentar este método

    /**
     *
     * @param numero de documento y tipo de documento
     * @return solicitante
     */
    public Solicitante buscarSolicitantePorTipoYNumeroDeDocumento(String numeroDocumento,
        String tipoDocumento);

    /**
     *
     * @param tipoDocumento
     * @param numeroDocumento
     * @return
     */
    public Solicitante findSolicitanteByTipoIdentAndNumDocFetchRelacions(
        String tipoDocumento, String numeroDocumento);

    /**
     * Método que busca una lista de {@link Solicitante} dependiendo de un filtro de tipo
     * {@link FiltroDatosConsultaSolicitante}
     *
     * @param datosFiltroSolicitante
     * @param cargarDeptoYMuni true si se quiere cargar el departamento y el municipio
     * @param filtroAdicionalJPQL filtro adicional JPQL, se puede enviar como null
     * @return Solicitante
     *
     * @author david.cifuentes
     *
     * @modified fabio.navarrete
     * @modified christian.rodriguez se cambia el nombre, se agregan los parametros
     * cargarPaisDeptoYMuni, filtroAdicionalJPQL
     */
    public List<Solicitante> buscarSolicitantesPorFiltro(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante, boolean cargarPaisDeptoYMuni,
        String filtroAdicionalJPQL);

    /**
     * Método que busca los solicitante por filtro que estén asociados a un contrato
     * interadministrativo. Esté método llama el método
     * {@link ISolicitanteDAO#buscarSolicitantesPorFiltro(FiltroDatosConsultaSolicitante, boolean, String)}
     *
     * @param datosFiltroSolicitante
     * @return
     */
    public List<Solicitante> buscarSolicitantesPorFiltroConContratoInteradministrativo(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante);

    /**
     * Método para obtener solitantes propietarios de los predios, inicialmente si no existen los
     * crea y los retorna, de lo contrario retorna los ya existentes
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public List<Solicitante> obtenerSolicitantesPropietariosTramiteMasivo(Long tramiteId);

    /**
     * Método que realiza la búsqueda de solicitantes regresando una lista de tipo
     * {@link Solicitante} en caso de que haya más de uno
     *
     * @param tipoDocumento
     * @param numeroDocumento
     * @return
     */
    public List<Solicitante> findSolicitanteByTipoIdentAndNumDocFetchRelacionsList(
        String tipoDocumento, String numeroDocumento);

    /**
     * Método para obtener solitantes propietarios de los predios condicion 0 y 5, inicialmente si
     * no existen los crea y los retorna, de lo contrario retorna los ya existentes
     *
     * @author leidy.gonzalez
     * @param tramiteId
     * @return
     */
    public List<Solicitante> obtenerSolicitantesPropietariosEV(Long tramiteId);
}
