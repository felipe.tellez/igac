/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IProfesionDAO;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class ProfesionDAOBean extends GenericDAOWithJPA<Profesion, Long> implements IProfesionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfesionDAOBean.class);

}
