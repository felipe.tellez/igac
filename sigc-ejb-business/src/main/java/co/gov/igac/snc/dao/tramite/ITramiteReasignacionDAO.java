/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion;
import javax.ejb.Local;

/**
 * Interface para las operaciones DB de la entidad TramiteReasignacion
 *
 * @author felipe.cadena
 */
@Local
public interface ITramiteReasignacionDAO extends IGenericJpaDAO<TramiteReasignacion, Long> {

}
