package co.gov.igac.snc.util.exceptions;

import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;

/**
 * Listado de Excepciones Manejadas por el servicio de negocio SNC
 *
 * @author juan.mendez
 * @version 2.0
 */
public final class SncBusinessServiceExceptions {

    /**
     *
     */
    private SncBusinessServiceExceptions() {

    }

    // D: CN = capa de negocio
    // 01 = módulo integración
    // 02 = módulo de acceso a base de datos
    // 03 = Reglas de Negocio
    // 04 = módulo gestor documental
    // 05 = queues manejados por servidor de aplicaciones
    // TODO:fredy.wilches::02-08-2011::definir los demás módulos para las  excepciones::pedro.garcia
    /**
     * Prefijo para los códigos de error que retornan los geoservicios de ArcGis Server
     */
    public static final String ERROR_SNC_GIS_PREFIX = "ERROR_SNC_GIS_";

    //////////////////////////////
    // CN01 Módulo de Integración / Servicios de Soporte
    //////////////////////////////	
    public static final ExcepcionSNC EXCEPCION_0001 = new ExcepcionSNC(
        "CN010001", ESeveridadExcepcionSNC.ERROR,
        "Error durante la ejecución del servicio de procesos: {0}");

    public static final ExcepcionSNC EXCEPCION_100001 = new ExcepcionSNC(
        "CN010001", ESeveridadExcepcionSNC.ERROR,
        "Error de Acceso a Datos: {0}");

    // D: {0} = Error en el Service locator
    public static final ExcepcionSNC EXCEPCION_010002 = new ExcepcionSNC(
        "CN010002", ESeveridadExcepcionSNC.ERROR,
        "Error al crear el Service Locator: {0}");

    // D: {0} = Error LDAP
    public static final ExcepcionSNC EXCEPCION_100003 = new ExcepcionSNC(
        "CN010003", ESeveridadExcepcionSNC.ERROR,
        "Error del servidor de autenticación: {0}");

    public static final ExcepcionSNC EXCEPCION_100004 = new ExcepcionSNC(
        "CN010004", ESeveridadExcepcionSNC.ERROR,
        "Error durante el acceso al Directorio Activo: {0} ");

    public static final ExcepcionSNC EXCEPCION_100005 = new ExcepcionSNC(
        "CN010005",
        ESeveridadExcepcionSNC.ERROR,
        "El procedimiento almacenado esperaba  {0} parámetros pero solo se pasaron {1} ");

    public static final ExcepcionSNC EXCEPCION_100006 = new ExcepcionSNC(
        "CN010006", ESeveridadExcepcionSNC.ERROR,
        "Error de Configuración. {0}  ");

    public static final ExcepcionSNC EXCEPCION_100007 = new ExcepcionSNC(
        "CN010007", ESeveridadExcepcionSNC.ERROR,
        "Error de Arcgis Server. {0}  ");

    public static final ExcepcionSNC EXCEPCION_100008 = new ExcepcionSNC(
        "CN010008", ESeveridadExcepcionSNC.ERROR,
        "Error de ejecución del Job: {0}  ");

    public static final ExcepcionSNC EXCEPCION_100009 = new ExcepcionSNC(
        "CN010009", ESeveridadExcepcionSNC.ERROR,
        "El parámetro \"{0}\" es obligatorio   ");

    public static final ExcepcionSNC EXCEPCION_100010 = new ExcepcionSNC(
        "CN010010", ESeveridadExcepcionSNC.ERROR,
        "Error durante la ejecución sql");

    public static final ExcepcionSNC EXCEPCION_100011 = new ExcepcionSNC(
        "CN010011", ESeveridadExcepcionSNC.ERROR,
        "Error durante la ejecución del proceso asincrónico : {0}");

    public static final ExcepcionSNC EXCEPCION_100012 = new ExcepcionSNC(
        "CN010012", ESeveridadExcepcionSNC.ERROR,
        "Error en el servicio Documental");

    public static final ExcepcionSNC EXCEPCION_100013 = new ExcepcionSNC(
        "CN010013", ESeveridadExcepcionSNC.ERROR,
        "Error en el servicio de Correo Electrónico: {0}  ");

    public static final ExcepcionSNC EXCEPCION_100014 = new ExcepcionSNC(
        "CN010014", ESeveridadExcepcionSNC.ERROR,
        "Error de validación en el parámetro  {0} : {1} [ {2} ]  ");

    public static final ExcepcionSNC EXCEPCION_100015 = new ExcepcionSNC(
        "CN010015", ESeveridadExcepcionSNC.ERROR,
        "Error de Arcgis Server. Validación de Reglas de Negocio : {0}  ");

    public static final ExcepcionSNC EXCEPCION_100016 = new ExcepcionSNC(
        "CN010016", ESeveridadExcepcionSNC.ERROR,
        "Error en el servidor de productos: {0}  ");

    public static final ExcepcionSNC EXCEPCION_100017 = new ExcepcionSNC(
        "CN010017", ESeveridadExcepcionSNC.ERROR,
        "Error al aplicar cambios geográficos: {0}  ");

    public static final ExcepcionSNC EXCEPCION_100018 = new ExcepcionSNC(
        "CN010018", ESeveridadExcepcionSNC.ERROR,
        "Error al generar productos geográficos: {0}  ");

    public static final ExcepcionSNC EXCEPCION_100019 = new ExcepcionSNC(
        "CN010019", ESeveridadExcepcionSNC.ERROR,
        "Error al crear el archivo zip: {0}  ");

    //////////////////////////////
    // CN02 Excepciones Acceso a datos
    //////////////////////////////
    // D: {0} = tipo de objeto (entity)
    // {1} = id del objeto
    public static final ExcepcionSNC EXCEPCION_0002 = new ExcepcionSNC(
        "CN020002", ESeveridadExcepcionSNC.ERROR,
        "No se encontró un objeto por su id: [{0},{1}]");

    /**
     * Error en query que busca objeto por su id {0} = entidad {1} = id
     */
    public static final ExcepcionSNC EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID = new ExcepcionSNC(
        "CN020003", ESeveridadExcepcionSNC.ERROR,
        "Error en query que busca objeto por su id: [{0},{1}]. Revise query");

    /**
     * Error generado al intentar persistir una entidad. <br />
     * {0} = nombre de la clase de la entidad que se iba a insertar
     */
    public static final ExcepcionSNC EXCEPCION_SQL_INSERCION = new ExcepcionSNC(
        "CN020004", ESeveridadExcepcionSNC.ERROR,
        "Error insertando datos en la tabla correspondienta a la entidad: {0} ");

    /**
     * Excepción general de ejecución de sentencia sql en el módulo de acceso a datos. <br/>
     * {0} identificador del método donde ocurre la excepción Clase#metodo
     */
    public static final ExcepcionSNC EXCEPCION_0005 = new ExcepcionSNC(
        "CN020005", ESeveridadExcepcionSNC.ERROR, "Error durante la ejecución sql. Método: {0}");

    /**
     * Excepción en ejecución en el módulo de acceso a datos de sentencia sql que actualiza un
     * objeto.
     * <br/>
     * {0} clase del objeto que se actualiza <br/>
     * {1} identificador del objeto
     */
    public static final ExcepcionSNC EXCEPCION_0006 = new ExcepcionSNC(
        "CN020005", ESeveridadExcepcionSNC.ERROR,
        "Error ejecutando el sql de actualización del objeto de tipo {0} con id {1}");

    /**
     * Excepción general de ejecución de sentencia sql en el módulo de acceso a datos. <br />
     *
     * {0} = clase#metodo donde ocurre
     */
    public static final ExcepcionSNC EXCEPCION_SQL_GENERAL = new ExcepcionSNC(
        "CN020006", ESeveridadExcepcionSNC.ERROR, "Error durante la ejecución sql en {0}");

    //////////////////////////////
    // CN03 - Excepciones Reglas de Negocio
    //////////////////////////////
    public static final ExcepcionSNC EXCEPCION_NEGOCIO_0001 = new ExcepcionSNC(
        "CN030001", ESeveridadExcepcionSNC.ERROR, "La Solicitud debe tener al menos un solicitante");

    /**
     * Excepción general ejecutando procedimiento almacenado. <br/>
     * {0} = nombre procedimiento almacenado que se ejecuta
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO =
        new ExcepcionSNC("CN0200006", ESeveridadExcepcionSNC.ERROR,
            "Excepción general ejecutando procedimiento almacenado {0}");

    /**
     * Excepción general en el módulo del gestor documental {0} = clase#metodo donde ocurre el error
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_GESTORDOCUMENTAL = new ExcepcionSNC(
        "CN0401", ESeveridadExcepcionSNC.ERROR,
        "Error en el servicio de Alfresco en {0}");

    /**
     * Excepción general para Procesos en la capa de negocio. <br />
     * {0} = clase#metodo donde ocurre el error <br />
     * {1] = método de la interfaz de procesos que se invocó
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_PROCESOS = new ExcepcionSNC(
        "CN0501", ESeveridadExcepcionSNC.ERROR,
        "Error en el servicio de Procesos. Ocurrió en {0} al invocar {1}");

    /**
     * Excepción generada al hacer la inicialización de objetos hijos de un entity por medio de
     * Hibernate. <br />
     *
     * {0} = clase#metodo donde ocurre <br />
     * {1} = Entity padre <br />
     */
    public static final ExcepcionSNC EXCEPCION_HIBERNATE_INITIALIZE = new ExcepcionSNC(
        "CN020007", ESeveridadExcepcionSNC.ERROR,
        "Error en {0} tratando de inicializar un entity hijo de {1}");

    /**
     * Excepción general de ejecución de sentencia sql de actualización o eliminación en el módulo
     * de acceso a datos. <br />
     * {0} = clase#metodo donde ocurre <br />
     * {1} = clase del objeto que se intentó actualizar o eliminar <br />
     * {2} = id del objeto que se intentó actualizar o eliminar <br />
     */
    public static final ExcepcionSNC EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION = new ExcepcionSNC(
        "CN020008", ESeveridadExcepcionSNC.ERROR, "Error durante la ejecución sql en {0}, " +
        "actualizando o eliminando el objeto {1} con id {2}");

    /**
     * Excepción en la generacion de la lista de tareas geograficas para el del xml de servicio Rest
     * del SNC
     *
     *
     */
    public static final ExcepcionSNC EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA =
        new ExcepcionSNC(
            "CN020008", ESeveridadExcepcionSNC.ERROR,
            "Error en la generacion de la lista de tareas geograficas en respuesta" +
            " a la solicitud por metido del servicio web");
    /**
     * Excepción en la generacion de la lista de tareas geograficas para el del xml de servicio Rest
     * del SNC
     *
     *
     */
    public static final ExcepcionSNC EXCEPCION_RECUPERANDO_INFORMACION_TRAMITE_PARA_EDICION_GEOGRAFICA =
        new ExcepcionSNC(
            "CN020008", ESeveridadExcepcionSNC.ERROR,
            "Error en la generacion de la lista de tareas geograficas en respuesta" +
            " a la solicitud por metido del servicio web");

    /**
     * Excepción de ejecución de métodos de la interfaz de integración {0} = clase#metodo donde
     * ocurre {1} = método del servicio de integración invocado
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_INTERFAZ_INTEGRACION = new ExcepcionSNC(
        "CN010019", ESeveridadExcepcionSNC.ERROR,
        "Error durante la ejecución del método de la interfaz de integración {1}, " +
        "invocado desde {0}");

    /**
     * Excepción de ejecución de métodos de la interfaz de integración {0} = clase#metodo donde
     * ocurre
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_QUEUES = new ExcepcionSNC(
        "CN050001", ESeveridadExcepcionSNC.ERROR, "Error relacionado con el manejo de queues. " +
        "Ocurrió en {0}");

    /**
     * Excepción de ejecución de métodos del SigDAO (Dao para acceso a los servicios SIG publicados
     * en el entorno de Arcgis Server a través de servicios REST). <br />
     *
     * {0} = clase#metodo donde ocurre <br />
     * {1} = método del SigDAO que se invocó
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_SERVICIOS_SIG = new ExcepcionSNC(
        "CN050002", ESeveridadExcepcionSNC.ERROR, "Error relacionado con servicios SIG. " +
        "Ocurrió en {0} cuando se invocó el método {1}");

    /**
     * Excepción de ejecución de servicios rest publicados en el servidor ArcGis. <br />
     *
     * {0} = clase#metodo donde ocurre <br />
     * {1} = servicio del cliente rest que se invocó
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_CLIENTE_REST_ARCGIS_SERVER =
        new ExcepcionSNC(
            "CN050002", ESeveridadExcepcionSNC.ERROR,
            "Error relacionado el llamado a un servicio REST del ArcGis server. " +
            "Ocurrió en {0} cuando se invocó el servicio {1}");

    /**
     * Excepción general de ejecución de sentencia sql definida en un named query en el módulo de
     * acceso a datos. <br />
     * {0} = nombre del namedquery Entity.namedQuery <br />
     * {1} = clase#metodo donde ocurre
     */
    public static final ExcepcionSNC EXCEPCION_SQL_GENERAL_NAMED_QUERY = new ExcepcionSNC(
        "CN020009", ESeveridadExcepcionSNC.ERROR,
        "Error durante la ejecución sql del named query {0} en {1}");

    /**
     * Excepción cuando no se encuentra un registro por su id. <br />
     * Esta excepción se debe usar cuando se captura una de tipo NoResultException ya que se espera
     * que sí exista el dato cuando se busca por su id. <br />
     *
     * {0} = clase#metodo donde ocurre <br />
     * {1] = clase del objeto buscado <br />
     * {2} = id del objeto buscado <br />
     */
    public static final ExcepcionSNC EXCEPCION_SQL_BUSQUEDA_POR_ID = new ExcepcionSNC(
        "CN020010", ESeveridadExcepcionSNC.ERROR,
        "En {0} no se encontró el objeto de tipo {1} con id {2}");

    public static final ExcepcionSNC EXCEPCION_40001 = new ExcepcionSNC("CN040001",
        ESeveridadExcepcionSNC.ERROR, "Error de acceso al repositorio documental");

    /**
     * Excepción de ejecución de servicios rest para reportes async. <br />
     *
     * {0} = clase#metodo donde ocurre <br />
     * {1} = id de la table rep_reporte_ejecucion
     */
    public static final ExcepcionSNC EXCEPCION_GENERAL_CLIENTE_REST_REPORT_ASYNC = new ExcepcionSNC(
        "CN055001", ESeveridadExcepcionSNC.ERROR,
        "Error relacionado el llamado a un servicio REST de reportes asincronicos. " +
        "Ocurrió en {0} cuando se invocó el servicio para el reporte {1}");

//end of class    
}
