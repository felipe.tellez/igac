package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IReconocimientoPredioDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ReconocimientoPredio;

/**
 * Implementación de los servicios de persistencia del objeto ControlCalidadCriterio.
 *
 * @author javier.barajas
 */
@Stateless
public class ReconocimientoPredioDAOBean extends GenericDAOWithJPA<ReconocimientoPredio, Long>
    implements IReconocimientoPredioDAO {

    /**
     * Servicio de bitácora.
     */
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReconocimientoPredioDAOBean.class);

    @Override
    public List<ReconocimientoPredio> obtenerReconocimietosPrediosPorManzana(
        String codigoManzana) {

        LOGGER.debug("ReconocimientoPredioDAOBean#obtenerReconocimietosPrediosPorManzana...INICIA");

        String stringQuery =
            "SELECT rp FROM ReconocimientoPredio rp WHERE rp.numeroPredial LIKE :codigoManzana";
        Query query = this.getEntityManager().createQuery(stringQuery);
        query.setParameter("codigoManzana", codigoManzana + "%");

        @SuppressWarnings("unchecked")
        List<ReconocimientoPredio> reconocimientoPredioDeManzana =
            (List<ReconocimientoPredio>) query
                .getResultList();

        for (ReconocimientoPredio recPredios : reconocimientoPredioDeManzana) {
            Hibernate.initialize(recPredios.getRecursoHumano());
        }

        LOGGER.
            debug("ReconocimientoPredioDAOBean#obtenerReconocimietosPrediosPorManzana...FINALIZA");
        return reconocimientoPredioDeManzana;
    }

}
