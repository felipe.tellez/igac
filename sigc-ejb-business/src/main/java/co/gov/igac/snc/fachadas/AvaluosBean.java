package co.gov.igac.snc.fachadas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.DocumentoOfertaDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.avaluos.IAreaCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.IAsignacionDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoAreaRegistradaDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoAsignacionProfesionalDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoCapituloDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoEvaluacionDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoLiquidacionCostoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoMovimientoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoObservacionDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPermisoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioAnexoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioConstruccionDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioCultivoDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioMaquinariaDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioZonaDAO;
import co.gov.igac.snc.dao.avaluos.IAvaluoRequisitoDocumentacionDAO;
import co.gov.igac.snc.dao.avaluos.IComisionOfertaDAO;
import co.gov.igac.snc.dao.avaluos.IComiteAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IContratoInteradminisAdicionDAO;
import co.gov.igac.snc.dao.avaluos.IContratoInteradministrativoDAO;
import co.gov.igac.snc.dao.avaluos.IContratoPagoAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadAvaluoRevDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadOfertaDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadOfertaObDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadRecolectorDAO;
import co.gov.igac.snc.dao.avaluos.IDetalleCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.IFotografiaDAO;
import co.gov.igac.snc.dao.avaluos.IGttOfertaInmobiliariaDAO;
import co.gov.igac.snc.dao.avaluos.IOfertaAreaUsoExclusivoDAO;
import co.gov.igac.snc.dao.avaluos.IOfertaInmobiliariaDAO;
import co.gov.igac.snc.dao.avaluos.IOfertaPredioDAO;
import co.gov.igac.snc.dao.avaluos.IOfertaRecursoHidricoDAO;
import co.gov.igac.snc.dao.avaluos.IOfertaServicioComunalDAO;
import co.gov.igac.snc.dao.avaluos.IOfertaServicioPublicoDAO;
import co.gov.igac.snc.dao.avaluos.IOrdenPracticaDAO;
import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluosContratoDAO;
import co.gov.igac.snc.dao.avaluos.IProfesionalContratoAdicionDAO;
import co.gov.igac.snc.dao.avaluos.IProfesionalContratoCesionDAO;
import co.gov.igac.snc.dao.avaluos.IProfesionalContratoPagoDAO;
import co.gov.igac.snc.dao.avaluos.IRegionCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.ISolicitudDocumentacionDAO;
import co.gov.igac.snc.dao.avaluos.IVProfesionalAvaluosCargaDAO;
import co.gov.igac.snc.dao.avaluos.IVProfesionalAvaluosContratoDAO;
import co.gov.igac.snc.dao.avaluos.IVariableEvaluacionAvaluoDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IProfesionDAO;
import co.gov.igac.snc.dao.tramite.IDetalleDocumentoFaltanteDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.dao.tramite.ISolicitudDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ISolicitudPredioDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentacionDAO;
import co.gov.igac.snc.dao.tramite.ITramiteEstadoDAO;
import co.gov.igac.snc.dao.tramite.ITramitePredioAvaluoDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAreaRegistrada;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacionDetalle;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoObservacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPermiso;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioAnexo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioCultivo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioZona;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoRequisitoDocumentacion;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoParticipante;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoPonente;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradminisAdicion;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoPagoAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidad;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadEspecificacion;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOfertaOb;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadRecolector;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;
import co.gov.igac.snc.persistence.entity.avaluos.GttOfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaAreaUsoExclusivo;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaRecursoHidrico;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioComunal;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioPublico;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoAdicion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoCesion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.VariableEvaluacionAvaluo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosCarga;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EAvaluoDetalle;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EMovimientoAvaluoTipo;
import co.gov.igac.snc.persistence.util.EOfertaAreaCapturaOfertaEstado;
import co.gov.igac.snc.persistence.util.EOfertaComisionEstado;
import co.gov.igac.snc.persistence.util.EOfertaTipoInmueble;
import co.gov.igac.snc.persistence.util.EOrdenPracticaEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EProfesionalContratoPagoEstado;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.enumsOfertasInmob.EOfertaFotografiaTipoFoto;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EDatosBaseCorreoElectronico;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.FiltroDatosConsultaRadicacionesCorrespondencia;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.FuncionarioRecolector;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
//@Interceptors(TimeInterceptor.class)
public class AvaluosBean implements IAvaluos, IAvaluosLocal {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluosBean.class);

    @EJB
    private IOfertaAreaUsoExclusivoDAO ofertaAreaUsoExclusivoService;

    @EJB
    private IOfertaServicioComunalDAO ofertaServicioComunalService;

    @EJB
    private IOfertaInmobiliariaDAO ofertaInmobiliariaService;

    @EJB
    private IOfertaRecursoHidricoDAO ofertaRecursoHidricoService;
    @EJB
    private IFotografiaDAO fotografiaService;

    @EJB
    private IOfertaPredioDAO ofertaPredioService;

    @EJB
    private IOfertaServicioPublicoDAO ofertaServiciosPublicosService;

    @EJB
    private ISNCProcedimientoDAO procedimientoService;

    @EJB
    private IGttOfertaInmobiliariaDAO gttOfertaInmobiliariaService;

    @EJB
    private IAreaCapturaOfertaDAO areaCapturaService;

    @EJB
    private IAreaCapturaOfertaDAO areaCapturaOfertaService;

    @EJB
    private IComisionOfertaDAO comisionOfertaService;

    @EJB
    private IControlCalidadOfertaDAO controlCalidadOfertaService;

    @EJB
    private IControlCalidadDAO controlCalidadService;

    @EJB
    private IControlCalidadRecolectorDAO controlCalidadRecolectorService;

    @EJB
    private IRegionCapturaOfertaDAO regionCapturaOfertaService;

    @EJB
    private IDetalleCapturaOfertaDAO detalleCapturaOfertaService;

    @EJB
    private IControlCalidadOfertaObDAO controlCalidadOfertaObService;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;

    @EJB
    private ITramitePredioAvaluoDAO tramitePredioAvaluoService;

    @EJB
    private IPredioDAO predioService;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private IAvaluoDAO avaluoService;

    @EJB
    private IAvaluoPredioDAO avaluoPredioService;

    @EJB
    private ISolicitudPredioDAO solicitudPredioService;

    @EJB
    private ISolicitudDocumentacionDAO solicitudDocumentacionService;

    @EJB
    private ITramiteDAO tramiteService;

    @EJB
    private ISolicitudDAO solicitudService;

    @EJB
    private ITramiteEstadoDAO tramiteEstadoService;

    @EJB
    private ISolicitanteSolicitudDAO solicitanteSolicitudService;

    @EJB
    private IContratoInteradministrativoDAO contratoInterAdminService;

    @EJB
    private IContratoInteradminisAdicionDAO contratoInteradminisAdicionService;

    @EJB
    private IVProfesionalAvaluosContratoDAO vProfesionalAvaluosContratoService;

    @EJB
    private IProfesionalAvaluosContratoDAO profesionalAvaluosContratoService;

    @EJB
    private IProfesionalContratoAdicionDAO profesionalContratoAdicionService;

    @EJB
    private IProfesionalContratoPagoDAO profesionalContratoPagoService;

    @EJB
    private IProfesionalContratoCesionDAO profesionalContratoCesionService;

    @EJB
    private IProfesionalAvaluoDAO profesionalAvaluoService;

    @EJB
    private IAvaluoMovimientoDAO avaluoMovimientoService;

    @EJB
    private IAvaluoPermisoDAO avaluoPermisoService;

    @EJB
    private IAvaluoLiquidacionCostoDAO avaluoLiquidacionCostoService;

    @EJB
    private IContratoPagoAvaluoDAO contratoPagoAvaluoService;

    @EJB
    private IVProfesionalAvaluosCargaDAO vProfesionalAvaluosCargaService;

    @EJB
    private IAvaluoAsignacionProfesionalDAO avaluoAsignacionProfesionalService;

    @EJB
    private IProfesionDAO profesionService;

    @EJB
    private IControlCalidadAvaluoDAO controlCalidadAvaluoDAOService;

    @EJB
    private IControlCalidadAvaluoRevDAO controlCalidadAvaluoRevDAOService;

    @EJB
    private IGeneralesLocal generalesService;

    @EJB
    private IDetalleDocumentoFaltanteDAO detalleDocumentoFaltanteService;

    @EJB
    private IDocumentoDAO documentoService;

    @EJB
    private IOrdenPracticaDAO ordenPracticaService;

    @EJB
    private ITramiteDocumentacionDAO tramiteDocumentacionService;

    @EJB
    private ISolicitudDocumentoDAO solicitudDocumentoService;

    @EJB
    private IAvaluoEvaluacionDAO avaluoEvaluacionService;

    @EJB
    private IVariableEvaluacionAvaluoDAO variableEvaluacionAvaluoService;

    @EJB
    private IAsignacionDAO asignacionService;

    @EJB
    private IComiteAvaluoDAO comiteAvaluoService;

    @EJB
    private IAvaluoPredioZonaDAO avaluoPredioZonaService;

    @EJB
    private IAvaluoPredioCultivoDAO avaluoPredioCultivoService;

    @EJB
    private IAvaluoPredioConstruccionDAO avaluoPredioConstruccionService;

    @EJB
    private IAvaluoPredioAnexoDAO avaluoPredioAnexoService;

    @EJB
    private IAvaluoPredioMaquinariaDAO avaluoPredioMaquinariaService;

    @EJB
    private IAvaluoObservacionDAO avaluoObservacionService;

    @EJB
    private IAvaluoAreaRegistradaDAO avaluoAreaRegistradaService;

    @EJB
    private IAvaluoCapituloDAO avaluoCapituloService;

    @EJB
    private IAvaluoRequisitoDocumentacionDAO avaluoRequisitoDocumentacionService;

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IAvaluos#guardarOfertasAreasUsoExclusivo(List)
     */
    @Override
    public void guardarOfertasAreasUsoExclusivo(
        List<OfertaAreaUsoExclusivo> ofertaAreaUsoExclusivo) {
        this.ofertaAreaUsoExclusivoService
            .persistMultiple(ofertaAreaUsoExclusivo);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IAvaluos#guardarOfertasServiciosComunales(List)
     */
    @Override
    public void guardarOfertasServiciosComunales(
        List<OfertaServicioComunal> ofertaServicioComunal) {
        this.ofertaServicioComunalService
            .persistMultiple(ofertaServicioComunal);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IAvaluos#guardarActualizarOfertaInmobiliaria(OfertaInmobiliaria, List, UsuarioDTO)
     */
    @Override
    public OfertaInmobiliaria guardarActualizarOfertaInmobiliaria(
        OfertaInmobiliaria oferta,
        List<OfertaInmobiliaria> listaOfertasEliminadas, UsuarioDTO usuario) {

        Calendar fecha = Calendar.getInstance();
        List<Object> paramtrosCodigo = new ArrayList<Object>();

        paramtrosCodigo.add(new BigDecimal(
            ENumeraciones.NUMERACION_OFERTAS_INMOBILIARIAS.getId()));
        paramtrosCodigo.add(usuario.getCodigoTerritorial());
        paramtrosCodigo.add(oferta.getDepartamento().getCodigo());
        paramtrosCodigo.add(oferta.getMunicipio().getCodigo());
        paramtrosCodigo.add(new BigDecimal(fecha.get(Calendar.YEAR)));

        String generarCodigo = (String) procedimientoService
            .generarNumeracion(paramtrosCodigo)[0];

        try {

            if (oferta.getId() == null) {
                oferta.setCodigoOferta(generarCodigo);
            } else {
                OfertaInmobiliaria ofertaDB = this.ofertaInmobiliariaService
                    .buscarOfertaInmobiliariaOfertasByOfertaId(oferta.getId());
                this.actualizarOfertaPredios(oferta.getOfertaPredios(), ofertaDB.getOfertaPredios());
                this.actualizarOfertaServicioComunal(oferta.getOfertaServicioComunals(),
                    ofertaDB.getOfertaServicioComunals());
                this.actualizarOfertaServiciosPublicos(oferta.getOfertaServicioPublicos(),
                    ofertaDB.getOfertaServicioPublicos());
                this.actualizarOfertaAreaUsoExclusivo(oferta.getOfertaAreaUsoExclusivos(),
                    ofertaDB.getOfertaAreaUsoExclusivos());
                this.actualizarFotografias(oferta.getFotografias(), ofertaDB.getFotografias());
                if (oferta.getTipoInmueble() != null &&
                    oferta.getTipoInmueble().equals(EOfertaTipoInmueble.FINCA.getCodigo())) {
                    this.actualizarOfertaRecursosHidricos(oferta.getOfertaRecursoHidricos(),
                        ofertaDB.getOfertaRecursoHidricos());
                }
            }

            oferta.setUsuarioLog(usuario.getLogin());
            oferta.setFechaLog(new Date());

            this.actualizarOfertaOfertas(oferta, usuario, paramtrosCodigo);
            this.eliminarOfertasAsociadas(listaOfertasEliminadas);

            oferta = this.ofertaInmobiliariaService.update(oferta);

            oferta = this.ofertaInmobiliariaService
                .buscarOfertaInmobiliariaOfertasByOfertaId(oferta.getId());

            this.actualizarFotosOfertaAlfresco(oferta);
            this.actualizarFotosOfertaOfertas(oferta);

        } catch (ExcepcionSNC e) {
            throw new ExcepcionSNC("mi codigo de excepcion",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), e);
        }

        return oferta;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarListaOfertaInmobiliaria(List)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public void guardarActualizarListaOfertaInmobiliaria(
        List<OfertaInmobiliaria> ofertasInmobiliaria) {

        try {
            this.ofertaInmobiliariaService.updateMultiple(ofertasInmobiliaria);
        } catch (ExcepcionSNC e) {
            throw new ExcepcionSNC("No se pudo actualizar el conjunto de ofertas",
                SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getMensaje(),
                e.getMessage(), e);
        }
    }

    /**
     * @see IAvaluosLocal#guardarActualizarListaControlCalidadOferta(List)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public void guardarActualizarListaControlCalidadOferta(
        List<ControlCalidadOferta> controlCalidadOferta) {

        try {
            this.controlCalidadOfertaService.updateMultiple(controlCalidadOferta);
        } catch (ExcepcionSNC e) {
            throw new ExcepcionSNC(
                "No se pudo actualizar el conjunto de control calidad ofertasofertas",
                SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getMensaje(),
                e.getMessage(), e);
        }
    }

    /**
     * Realiza la actualizaciÃ³n de las ofertas asociadas a la foerta principal
     *
     * @author christian.rodriguez
     * @param ofertaInmobiliaria oferta principal que contiene las oferta hijas
     * @param usuario usuario responsable de la informaciÃ³n
     * @param paramtrosCodigo parametros para generar el cÃ³digo de la oferta
     *
     */
    private void actualizarOfertaOfertas(OfertaInmobiliaria ofertaInmobiliaria, UsuarioDTO usuario,
        List<Object> paramtrosCodigo) {
        String generarCodigo;
        if (ofertaInmobiliaria.getOfertaInmobiliarias() != null &&
            !ofertaInmobiliaria.getOfertaInmobiliarias().isEmpty()) {
            for (OfertaInmobiliaria oi : ofertaInmobiliaria
                .getOfertaInmobiliarias()) {

                if (oi.getId() != null) {
                    OfertaInmobiliaria ofertaDB = this.ofertaInmobiliariaService
                        .buscarOfertaInmobiliariaOfertasByOfertaId(oi.getId());

                    this.actualizarFotografias(oi.getFotografias(), ofertaDB.getFotografias());
                }

                if (oi.getId() == null) {
                    generarCodigo = (String) procedimientoService
                        .generarNumeracion(paramtrosCodigo)[0];

                    oi.setCodigoOferta(generarCodigo);
                }

                oi.setDepartamento(ofertaInmobiliaria.getDepartamento());
                oi.setMunicipio(ofertaInmobiliaria.getMunicipio());
                oi.setOfertaInmobiliaria(ofertaInmobiliaria);
                oi.setUsuarioLog(usuario.getLogin());
                oi.setFechaLog(new Date());
            }
            ofertaInmobiliaria.setTipoInmueble(null);
        }
    }

    /**
     * Elimina las ofertas indicadas
     *
     * @author christian.rodriguez
     * @param listaOfertasEliminadas lista de las ofertas eliminadas
     */
    private void eliminarOfertasAsociadas(List<OfertaInmobiliaria> listaOfertasEliminadas) {
        if (listaOfertasEliminadas != null &&
            !listaOfertasEliminadas.isEmpty()) {
            for (OfertaInmobiliaria oe : listaOfertasEliminadas) {

                if (oe.getFotografias() != null &&
                    !oe.getFotografias().isEmpty()) {
                    for (Fotografia foto : oe.getFotografias()) {
                        if (foto.getId() != null) {
                            this.fotografiaService.delete(foto);
                        }
                    }
                }

                if (oe.getId() != null) {
                    this.ofertaInmobiliariaService.delete(oe);
                }
            }
        }
    }

    /**
     * Guardar o actualiza las fotos de las ofertas asociadas a la oferta principal
     *
     * @author christian.rodriguez
     * @param ofertaInmobiliaria oferta inmobiliaria que tiene las ofertas hijas
     */
    private void actualizarFotosOfertaOfertas(OfertaInmobiliaria ofertaInmobiliaria) {
        if (ofertaInmobiliaria.getOfertaInmobiliarias() != null &&
            !ofertaInmobiliaria.getOfertaInmobiliarias().isEmpty()) {

            for (OfertaInmobiliaria oi : ofertaInmobiliaria.getOfertaInmobiliarias()) {
                this.actualizarFotosOfertaAlfresco(oi);
            }
        }
    }

    /**
     * Elimina de la base las fotografias de la oferta
     *
     * @param listaNueva lista con las fotografias actualizadss de la oferta
     * @param listaVieja lista con las fotografias actuales (en DB) de la oferta
     * @author christian.rodriguez
     */
    private void actualizarFotografias(List<Fotografia> listaNueva,
        List<Fotografia> listaVieja) {
        if (listaVieja != null && listaNueva != null) {
            for (Fotografia fotografia : listaVieja) {
                if (listaNueva.indexOf(fotografia) == -1) {
                    this.fotografiaService.delete(fotografia);
                }
            }
        }
    }

    /**
     * Elimina de la base los Predios de la oferta que han
     *
     * @param listaNueva lista con los predios actualizados de la oferta
     * @param listaVieja lista con los predios actuales (en DB) de la oferta
     * @author christian.rodriguez
     */
    private void actualizarOfertaPredios(List<OfertaPredio> listaNueva,
        List<OfertaPredio> listaVieja) {
        if (listaVieja != null && listaNueva != null) {
            for (OfertaPredio ofertaPredio : listaVieja) {
                if (listaNueva.indexOf(ofertaPredio) == -1) {
                    this.ofertaPredioService.delete(ofertaPredio);
                }
            }
        }
    }

    /**
     * Elimina de la base los servicios publicos de la oferta
     *
     * @param listaNueva lista con los servicios publicos actualizados de la oferta
     * @param listaVieja lista con los servicios publicos actuales (en DB) de la oferta
     * @author christian.rodriguez
     */
    private void actualizarOfertaServiciosPublicos(
        List<OfertaServicioPublico> listaNueva,
        List<OfertaServicioPublico> listaVieja) {
        if (listaVieja != null && listaNueva != null) {
            for (OfertaServicioPublico ofertaServicioPublico : listaVieja) {
                if (listaNueva.indexOf(ofertaServicioPublico) == -1) {
                    this.ofertaServiciosPublicosService
                        .delete(ofertaServicioPublico);
                }
            }
        }
    }

    /**
     * Elimina de la base los recursos hidricos de la oferta
     *
     * @param listaNueva lista con los recursos hidricos actualizados de la oferta
     * @param listaVieja lista con los recursos hidricos actuales (en DB) de la oferta
     * @author christian.rodriguez
     */
    private void actualizarOfertaRecursosHidricos(
        List<OfertaRecursoHidrico> listaNueva,
        List<OfertaRecursoHidrico> listaVieja) {
        if (listaVieja != null && listaNueva != null) {
            for (OfertaRecursoHidrico ofertaRecursoHidrico : listaVieja) {
                if (listaNueva.indexOf(ofertaRecursoHidrico) == -1) {
                    this.ofertaRecursoHidricoService
                        .delete(ofertaRecursoHidrico);
                }
            }
        }
    }

    /**
     * Elimina de la base los servicios comunales de la oferta
     *
     * @param listaNueva lista con los servicios comunales actualizados de la oferta
     * @param listaVieja lista con los servicios comunales actuales (en DB) de la oferta
     * @author christian.rodriguez
     */
    private void actualizarOfertaServicioComunal(
        List<OfertaServicioComunal> listaNueva,
        List<OfertaServicioComunal> listaVieja) {
        if (listaVieja != null && listaNueva != null) {
            for (OfertaServicioComunal ofertaServicioComunal : listaVieja) {
                if (listaNueva.indexOf(ofertaServicioComunal) == -1) {
                    this.ofertaServicioComunalService
                        .delete(ofertaServicioComunal);
                }
            }
        }
    }

    /**
     * Elimina de la base las areas de uso exclusivo de la oferta
     *
     * @param listaNueva lista con las areas de uso exclusivo actualizados de la oferta
     * @param listaVieja lista con las areas de uso exclusivo actuales (en DB) de la oferta
     * @author christian.rodriguez
     */
    private void actualizarOfertaAreaUsoExclusivo(
        List<OfertaAreaUsoExclusivo> listaNueva,
        List<OfertaAreaUsoExclusivo> listaVieja) {
        if (listaVieja != null && listaNueva != null) {
            for (OfertaAreaUsoExclusivo ofertaAreaUsoExclusivo : listaVieja) {
                if (listaNueva.indexOf(ofertaAreaUsoExclusivo) == -1) {
                    this.ofertaAreaUsoExclusivoService
                        .delete(ofertaAreaUsoExclusivo);
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * MÃ©todo que permite el guardar o actualizar fotografÃ­as asociadas a una oferta inmobiliaria
     *
     * @author juan.agudelo
     * @param ofertainmobiliaria
     * @param usuario
     * @return
     */
    private void actualizarFotosOfertaAlfresco(OfertaInmobiliaria ofertaInmobiliaria) {

        IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
        String fileSeparator = System.getProperty("file.separator");

        try {
            if (ofertaInmobiliaria.getFotografias() != null &&
                !ofertaInmobiliaria.getFotografias().isEmpty()) {
                for (Fotografia fTemp : ofertaInmobiliaria.getFotografias()) {

                    if (fTemp.getIdRepositorioDocumentos() == null ||
                        fTemp.getIdRepositorioDocumentos().isEmpty()) {
                        String tipoFoto = buscarTipoFotoPorCodigo(fTemp.getTipoFoto());

                        DocumentoOfertaDTO documentoOfertaDTO = new DocumentoOfertaDTO(
                            FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                            fTemp.getArchivoFotografia(), tipoFoto, fTemp.getFecha(),
                            ofertaInmobiliaria.getDepartamento().getNombre(),
                            ofertaInmobiliaria.getMunicipio().getNombre(),
                            ofertaInmobiliaria.getCodigoOferta());

                        try {
                            String repositorioDId = servicioDocumental
                                .cargarEnOfertaInmobiliaria(documentoOfertaDTO);
                            fTemp.setIdRepositorioDocumentos(repositorioDId);
                            fTemp.setOfertaInmobiliaria(ofertaInmobiliaria);
                            this.fotografiaService.update(fTemp);
                        } catch (ExcepcionSNC e) {
                            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                                LOGGER, e, e.getMessage());
                        }
                    }
                }
            }

        } catch (ExcepcionSNC e) {
            throw new ExcepcionSNC("mi codigo de excepcion", ESeveridadExcepcionSNC.ERROR,
                e.getMessage(), e);
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * MÃ©todo que recupera el tipo de foto por el cÃ³digo para almacenarlo en alfresco
     *
     * @author juan.agudelo
     */
    private String buscarTipoFotoPorCodigo(String tipoFoto) {

        String answer = null;

        for (EOfertaFotografiaTipoFoto e : EOfertaFotografiaTipoFoto.values()) {

            if (tipoFoto.equals(e.getCodigo())) {
                answer = e.toString();
            }
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IAvaluos#buscarOfertasInmobiliariasPorFiltro(FiltroDatosConsultaOfertasInmob, String,
     * String, int[])
     * @author juan.agudelo
     * @modified pedro.garcia 14-05-2012 orden en las cosas. Try y catch. AdiciÃ³n de parÃ¡metros
     * para ordenamiento de columnas
     */
    @Override
    public List<OfertaInmobiliaria> buscarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob,
        String sortField, String sortOrder, int... contadoresDesdeYCuantos) {

        List<OfertaInmobiliaria> answer = null;
        try {
            answer = this.ofertaInmobiliariaService
                .buscarOfertasInmobiliariasPorFiltro(
                    filtroDatosConsultaOfertasInmob, sortField,
                    sortOrder, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("ExcepciÃ³n consultando ofertas inmobiliarias: " +
                ex.getMensaje());
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IAvaluos#contarOfertasInmobiliariasPorFiltro(FiltroDatosConsultaOfertasInmob)
     * @author juan.agudelo
     */
    @Override
    public Long contarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob) {
        return this.ofertaInmobiliariaService
            .contarOfertasInmobiliariasPorFiltro(filtroDatosConsultaOfertasInmob);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @see IAvaluos#buscarOfertaInmobiliariaPorId(java.lang.Long)
     */
    @Implement
    @Override
    public OfertaInmobiliaria buscarOfertaInmobiliariaPorId(
        Long ofertaInmobiliariaId) {

        LOGGER.debug("on AvaluosBean#buscarOfertaInmobiliariaPorId ...");
        OfertaInmobiliaria answer = null;
        answer = this.ofertaInmobiliariaService
            .buscarOfertaInmobiliariaOfertasByOfertaId(ofertaInmobiliariaId);

        LOGGER.debug("... finished AvaluosBean#buscarOfertaInmobiliariaPorId");
        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IOfertaInmobiliariaDAO#buscarOfertaInmobiliariaOfertaPadreByOfertaId(Long)
     * @author juan.agudelo
     */
    @Override
    public OfertaInmobiliaria buscarOfertaInmobiliariaOfertaPadreByOfertaId(
        Long ofertaInmobiliariaId) {

        return this.ofertaInmobiliariaService
            .buscarOfertaInmobiliariaOfertaPadreByOfertaId(ofertaInmobiliariaId);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IAvaluos#generarSecuenciaGttOfertaInmobiliaria()
     */
    @Override
    public Long generarSecuenciaGttOfertaInmobiliaria() {

        return this.gttOfertaInmobiliariaService
            .generarSecuenciaGttOfertaInmobiliaria();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IAvaluos#guardarIdsOfertasInmobiliarias(List)
     */
    @Override
    public List<GttOfertaInmobiliaria> guardarActualizarIdsOfertasInmobiliarias(
        List<GttOfertaInmobiliaria> gttsOfertasInmobiliarias) {
        return this.gttOfertaInmobiliariaService
            .updateMultipleReturn(gttsOfertasInmobiliarias);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IAvaluos#borrarIdsOfertasInmobiliarias(List)
     */
    @Override
    public void borrarIdsOfertasInmobiliarias(
        List<GttOfertaInmobiliaria> gttsOfertasInmobiliarias) {
        this.gttOfertaInmobiliariaService
            .deleteMultiple(gttsOfertasInmobiliarias);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * IAvaluosLocal#guardarAreaCapturaOferta(co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta,
     * co.gov.igac.generales.dto.UsuarioDTO)
     * @author pedro.garcia
     * @modifiedby christian.rodriguez
     */
    @Implement
    @Override
    public AreaCapturaOferta guardarAreaCapturaOferta(
        AreaCapturaOferta nuevaAreaCapturaOferta, UsuarioDTO usuario) {
        LOGGER.debug("on AvaluosBean#guardarAreaCapturaOferta ...");

        AreaCapturaOferta answer = null;

        try {

            if (nuevaAreaCapturaOferta.getId() == null) {
                nuevaAreaCapturaOferta.setUsuarioLog(usuario.getLogin());
            }

            answer = this.areaCapturaOfertaService.update(nuevaAreaCapturaOferta);

        } catch (ExcepcionSNC e) {
            throw new ExcepcionSNC(
                "Error al guardar/actualizar un Ã¡rea captura de ofertas",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), e);
        }

        LOGGER.debug("... finished AvaluosBean#guardarAreaCapturaOferta");

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#borrarAreaCapturaOferta(java.lang.Long)
     * @author christian.rodriguez
     *
     * @modified pedro.garcia 05-06-2012 + manejo de excepciones + cambio de parÃ¡metro
     */
    @Implement
    @Override
    public boolean borrarAreaCapturaOferta(Long idAreaCapturaOferta) {

        AreaCapturaOferta areaAEliminar;

        try {
            areaAEliminar = this.areaCapturaOfertaService
                .getByIdConAsociaciones(idAreaCapturaOferta);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#borrarAreaCapturaOferta: " +
                ex.getMensaje());
            return false;
        }
        try {

            // D: se verifica que no haya alguna RegionCapturaOferta asociada a
            // la regiÃ³n; si la hay no se puede borrar
            if (areaAEliminar.getRegionCapturaOfertas() != null &&
                !areaAEliminar.getRegionCapturaOfertas().isEmpty()) {
                for (RegionCapturaOferta rco : areaAEliminar.getRegionCapturaOfertas()) {
                    if (rco.getAsignadoRecolectorId() != null ||
                        rco.getAsignadoRecolectorNombre() != null) {
                        return false;
                    }
                }

                // D: borrar los AreaCapturaDetalle relacionados con el Ã¡rea que
                // se
                // va a eliminar, y borrar cada uno
                for (DetalleCapturaOferta dco : areaAEliminar.getDetalleCapturaOfertas()) {
                    this.detalleCapturaOfertaService.delete(dco);
                }
            }

            this.areaCapturaOfertaService.delete(areaAEliminar);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "AvaluosBean#borrarAreaCapturaOferta");
        }

        return true;
    }

    /**
     * @see IAvaluosLocal#buscarAreasCapturaOfertaSinComisionar(String,String)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<AreaCapturaOferta> buscarAreasCapturaOfertaSinComisionar(
        String departamentoCodigo, String municipioCodigo) {

        LOGGER.debug("on AvaluosBean#buscarAreasCapturaOfertaSinComisionar ...");

        List<AreaCapturaOferta> answer = null;
        answer = this.areaCapturaService.getAreasSinComisionarConRecolector(
            departamentoCodigo, municipioCodigo);

        LOGGER.debug("... finished AvaluosBean#buscarAreasCapturaOfertaSinComisionar");

        return answer;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarComisionesOfertas(ArrayList)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public void guardarActualizarComisionesOfertas(ArrayList<ComisionOferta> listaComisiones) {

        LOGGER.debug("on AvaluosBean#guardarActualizarComisionesOfertas ...");

        try {

            this.comisionOfertaService.updateMultiple(listaComisiones);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluosBean#guardarActualizarComisionesOfertas");
        }

        LOGGER.debug("... finished AvaluosBean#guardarActualizarComisionesOfertas ...");
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#guardarActualizarComisionOferta(ComisionOferta, UsuarioDTO)
     * @author christian.rodriguez
     * @modified pedro.garcia 07-06-2012 cambios por cambios en tablas de la bd
     */
    @Implement
    @Override
    public ComisionOferta guardarActualizarComisionOferta(
        ComisionOferta comisionOferta, UsuarioDTO usuario,
        List<RegionCapturaOferta> regionesAEliminar) {

        LOGGER.debug("on AvaluosBean#guardarActualizarComisionOferta ...");

        ComisionOferta answer = null;

        try {

            if (comisionOferta.getId() == null) {
                // Nueva ComisiÃ³n
                comisionOferta.setUsuarioLog(usuario.getLogin());
                comisionOferta = this.comisionOfertaService.update(comisionOferta);
            }
            this.actualizarRegionesCapturaOfertaDeComision(comisionOferta, regionesAEliminar);

            if (comisionOferta.getRegionCapturaOfertas().size() == regionesAEliminar.size()) {
                comisionOferta.setEstado(EOfertaComisionEstado.CANCELADA.toString());
            }

            answer = this.comisionOfertaService.update(comisionOferta);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error al guardar/actualizar una comisiÃ³n de ofertas: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#buscarComisionesOfertaPorAprobar");

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Actualiza las regiones asociadas a la comision. En caso de haber removido alguna regiÃ³n de
     * la comisiÃ³n esta se desvincula de la comision
     *
     * @author christian.rodriguez
     * @param comisionOferta
     * @param regionesAEliminar
     */
    private void actualizarRegionesCapturaOfertaDeComision(
        ComisionOferta comisionOferta,
        List<RegionCapturaOferta> regionesAEliminar) {

        for (RegionCapturaOferta region : comisionOferta.getRegionCapturaOfertas()) {
            try {
                if (regionesAEliminar.isEmpty() || !regionesAEliminar.contains(region)) {
                    region.setComisionOferta(comisionOferta);
                    region.setEstado(EEstadoRegionCapturaOferta.EN_COMISION.getEstado());
                } else {
                    region.setEstado(EEstadoRegionCapturaOferta.ASIGNADA.getEstado());
                    region.setComisionOferta(null);
                }
                this.regionCapturaOfertaService.update(region);
            } catch (ExcepcionSNC ex) {

                LOGGER.error("Error al guardar/actualizar una RegionCapturaOferta: " +
                    ex.getMensaje());
            }
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#buscarAreaCapturaOfertaPorId(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public AreaCapturaOferta buscarAreaCapturaOfertaPorId(Long areaId) {
        LOGGER.debug("on AvaluosBean#buscarAreaCapturaOfertPorId ...");

        AreaCapturaOferta answer = null;

        try {
            answer = this.areaCapturaService.getAreaCapturaOfertaById(areaId);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al traer buscar el Ã¡rea de captura de ofertas: " +
                e.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#buscarAreaCapturaOfertPorId");
        return answer;
    }

    /**
     * @see IAvaluosLocal#buscarAreasCapturaVigentesPorEstrucOrg(String)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<AreaCapturaOferta> buscarAreasCapturaVigentesPorEstrucOrg(
        String estrucOrgCod) {

        LOGGER.debug("on AvaluosBean#buscarAreasCapturaVigentesPorEstrucOrg ...");

        List<AreaCapturaOferta> answer = null;

        try {
            answer = this.areaCapturaOfertaService
                .getAreaCapturaOfertaPorEstadoYEstrucOrg(estrucOrgCod,
                    EOfertaAreaCapturaOfertaEstado.VIGENTE.toString());
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al traer buscar las Ã¡reas de captura vigente para ofertas: " +
                e.getMensaje());
        }
        LOGGER.debug("... finished AvaluosBean#buscarAreasCapturaVigentesPorEstrucOrg");

        return answer;
    }

    /**
     * @see IAvaluosLocal#getInformacionRecolectores(List, Long)
     * @author rodrigo.hernandez
     */
    @Override
    public List<FuncionarioRecolector> getInformacionRecolectores(
        List<String> recolectoresData, Long idAreaRecoleccion) {

        LOGGER.debug("on AvaluosBean#getInformacionRecolectores ...");

        List<FuncionarioRecolector> answer = null;

        answer = this.regionCapturaOfertaService
            .getInformacionRecolectores(recolectoresData, idAreaRecoleccion);

        LOGGER.debug("... finished AvaluosBean#getInformacionRecolectores");

        return answer;
    }

    /**
     * @see IAvaluosLocal#cargarDetallesAreasDeRecolector(String)
     * @author ariel.ortiz
     */
    @Override
    public List<String> buscarDetallesAreasPorRecolector(String idRecolector, Long idRegion) {

        List<String> listaDetalles = null;

        try {
            listaDetalles = this.detalleCapturaOfertaService
                .cargarDetallesAreasPorRecolector(idRecolector, idRegion);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la carga de areas por recolector " + e);
        }

        return listaDetalles;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#buscarRegionCapturaOfertaPorIdAreaYRecolector(Long,String)
     * @author ariel.ortiz
     */
    @Override
    public List<RegionCapturaOferta> buscarRegionCapturaOfertaPorIdAreaYRecolector(
        Long idAreaAsignada, String idRecolector) {
        List<RegionCapturaOferta> listaRegiones = null;
        try {
            listaRegiones = this.regionCapturaOfertaService
                .cargarRegionCapturaOfertaPorIdAreaYRecolector(idAreaAsignada, idRecolector);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error cargando las regiones asociadas a un recolector y Ã¡rea asignada" +
                e);

        }
        return listaRegiones;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerCodigosPlanchas(List<String>, String, boolean )
     * @author ariel.ortiz
     */
    @Override
    public List<String> obtenerCodigosPlanchas(List<String> codigos,
        String escala, boolean esRural, UsuarioDTO usuario) {
        /*
         * List<String> resultado = null; try { resultado =
         * SigDAO.getInstance(logMensajeDAO).obtenerCodigosPlanchas(codigos, escala, esRural,
         * usuario.getSigToken(), usuario);
         *
         * } catch (ExcepcionSNC e) { LOGGER.error("Error en la obtenciÃ³n de los cÃ³digos de las
         * planchas "+ e); throw e; } return resultado;
        * */
        throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
            "Servicio No Implementado. La invocación debe realizarse de forma asincrónica");
    }

    /**
     * @see IAvaluosLocal#buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(String,
     * Long, Long)
     *
     * @author rodrigo.hernandez
     * @modifiedBy christian.rodriguez se le adiciona el parametro de la regiÃ³n de captura de las
     * ofertas
     */
    @Implement
    @Override
    public List<ControlCalidadOferta> buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId) {

        List<ControlCalidadOferta> listaOfertas = null;

        try {
            listaOfertas = this.controlCalidadOfertaService
                .buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
                    recolectorId, controlCalidadId, regionId);
        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "Error en AvaluosBean#buscarOfertasSeleccionadasPorIdRecolectorEIdControlCalidad: " +
                e.getMensaje());
        }
        return listaOfertas;
    }
//----------------------------------------------------------

    /**
     * @see IAvaluosLocal#buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(String,
     * Long, Long)
     *
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<OfertaInmobiliaria> buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId) {

        List<OfertaInmobiliaria> listaOfertas = null;

        try {
            listaOfertas = this.controlCalidadOfertaService
                .buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(recolectorId,
                    controlCalidadId, regionId);
        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "Error en AvaluosBean#buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion: " +
                e.getMensaje());
        }
        return listaOfertas;
    }
//----------------------------------------------------------

    /**
     * @see IAvaluosLocal#buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(String, Long)
     *
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<ControlCalidadOferta> buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(
        String recolectorId, Long controlCalidadId) {

        List<ControlCalidadOferta> listaOfertasControl = null;

        try {
            listaOfertasControl = this.controlCalidadOfertaService
                .buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(recolectorId,
                    controlCalidadId);
        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "Error en AvaluosBean#, buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad: " +
                e.getMensaje());
        }
        return listaOfertasControl;
    }
//----------------------------------------------------------

    /**
     * @see IAvaluosLocal#buscarControlCalidadRecolectorPorIdControl(Long idControl)
     * @author ariel.ortiz
     * @param idControl - id del control de calidad
     *
     */
    @Override
    public List<ControlCalidadRecolector> buscarControlCalidadRecolectorPorIdControl(
        Long idControl) {
        List<ControlCalidadRecolector> controles = null;

        try {
            controles = this.controlCalidadRecolectorService
                .buscarControlCalidadRecolectorPorIdControl(idControl);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la carga de Controles de calidad por el id del control " + e);
        }

        return controles;
    }

    /**
     * @see IAvaluosLocal#buscarObservacionesPorOfertaId(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<ControlCalidadOfertaOb> buscarObservacionesPorOfertaId(Long idOferta) {

        LOGGER.debug("on AvaluosBean#buscarObservacionesPorOfertaId");
        List<ControlCalidadOfertaOb> answer = null;

        try {
            answer = this.controlCalidadOfertaObService.cargarObservacionesPorOfertaId(idOferta);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#buscarObservacionesPorOfertaId: " + ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#buscarObservacionesPorOfertaId");

        return answer;
    }

    //-------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarOfertasMuestraControl()
     * @author ariel.ortiz
     */
    @Override
    public void guardarOfertasMuestraControl(Long controlId) {
        try {
            Object[] errors;
            errors = this.sncProcedimientoDao.guardarCalculoMuestras(controlId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("ExcepciÃ³n en erl guardado de muestras para el control calidad: " +
                ex.getMensaje());
        }
    }
//-----------------------------------------------------------

    /**
     * @see IAvaluosLocal#calcularMuestras(String municipioSeleccionadoCodigo, int valorPedidoD, int
     * areaTerrenoD, int areaConstruidaD, int valorCalculadoD, String login)
     *
     * @author ariel.ortiz
     */
    @Override
    public Object[] calcularMuestras(String municipioSeleccionadoCodigo,
        int valorPedidoD, int areaTerrenoD, int areaConstruidaD,
        int valorCalculadoD, String login) {

        Object[] controlAnswer = null;

        try {
            controlAnswer = this.sncProcedimientoDao.calcularMuestrasControl(
                municipioSeleccionadoCodigo, new BigDecimal(valorPedidoD),
                new BigDecimal(areaTerrenoD), new BigDecimal(
                    areaConstruidaD), new BigDecimal(valorCalculadoD),
                login);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("ExcepciÃ³n en Calculo de Muestras Control Calidad: " +
                ex.getMensaje());
        }
        return controlAnswer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#buscarRegionesCapturaOfertaPorEstadosYIds(List,List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<RegionCapturaOferta> buscarRegionesCapturaOfertaPorEstadosYIds(
        List<Long> regionesCapturaIds, List<String> estados) {

        LOGGER.debug("on AvaluosBean#buscarRegionesCapturaOfertaPorComisionar ...");
        List<RegionCapturaOferta> answer = null;

        try {
            answer = this.regionCapturaOfertaService.buscarRegionesPorIdsYEstados(
                regionesCapturaIds, estados);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarRegionesCapturaOfertaPorComisionar: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerRegionesCapturaOfertaPorComision(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaPorComision(
        Long comisionId) {

        LOGGER.debug("on AvaluosBean#obtenerRegionesCapturaOfertaPorComision ...");
        List<RegionCapturaOferta> answer = null;

        try {
            answer = this.regionCapturaOfertaService
                .buscarPorIdComision(comisionId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#obtenerRegionesCapturaOfertaPorComision: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IAvaluosLocal#buscarObservacionesPorControlCalidadOfertaId(Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<ControlCalidadOfertaOb> buscarObservacionesPorControlCalidadOfertaId(
        Long controlCalidadOfertaId) {

        List<ControlCalidadOfertaOb> listaObservaciones = null;

        try {
            listaObservaciones = this.controlCalidadOfertaObService
                .cargarObservacionesPorControlCalidadOfertaId(controlCalidadOfertaId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#cargarObservacionesPorControlCalidadOfertaId: " +
                ex.getMensaje());
        }

        return listaObservaciones;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarControlCalidadOferta(ControlCalidadOfertaOb,
     * ControlCalidadOferta)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public void guardarActualizarControlCalidadOferta(
        ControlCalidadOfertaOb controlCalidadOfertaOb,
        ControlCalidadOferta controlCalidadOferta) {

        try {
            if (controlCalidadOfertaOb != null) {
                this.controlCalidadOfertaObService
                    .persist(controlCalidadOfertaOb);
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#guardarControlCalidadOferta: " +
                ex.getMensaje());
        }

        this.controlCalidadOfertaService.update(controlCalidadOferta);
    }

    /**
     * @see IAvaluosLocal#guardarActualizarControlCalidad(ControlCalidad)
     *
     * @author christian.rodriguez
     */
    @Override
    public void guardarActualizarControlCalidad(ControlCalidad controlCalidad) {

        try {
            this.controlCalidadService.update(controlCalidad);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#guardarActualizarControlCalidad: " + ex.getMensaje());
        }
    }

//---------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#borrarCalculoMuestras(Long controlCalidadId)
     * @author ariel.ortiz
     */
    @Override
    public void borrarCalculoMuestras(Long controlCalidadId) {
        try {
            Object[] errors;
            errors = this.sncProcedimientoDao
                .borrarCalculoMuestras(controlCalidadId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("ExcepciÃ³n en borrar muestras para el control calidad: " +
                ex.getMensaje());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#buscarComisionesOfertaPorEstadoYACO(java.lang.String, java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<ComisionOferta> buscarComisionesOfertaPorEstadoYRCO(String estadoComision,
        List<Long> regionesCapturaOfertaIds) {

        LOGGER.debug("on AvaluosBean#buscarComisionesOfertaPorEOYEstado");
        List<ComisionOferta> answer = null;

        try {
            answer = this.comisionOfertaService.getComisionesOfertaPorEstadoAndRCO(estadoComision,
                regionesCapturaOfertaIds);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#buscarComisionesOfertaPorEOYEstado: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#buscarComisionesOfertaPorEOYEstado");

        return answer;
    }

    /**
     * @see IAvaluosLocal#asociarManzanasVeredasARegionCreada(AreaCapturaOferta, Long, String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public void asociarManzanasVeredasARegionCreada(
        AreaCapturaOferta areaCapturaOferta, RegionCapturaOferta region,
        String cadenaManzanasVeredas) {
        StringTokenizer tokenizer = null;
        List<String> listaDetalles = new ArrayList<String>();
        tokenizer = new StringTokenizer(cadenaManzanasVeredas, ",");

        if (region.getDetalleCapturaOfertas() == null) {
            region.setDetalleCapturaOfertas(new ArrayList<DetalleCapturaOferta>());
        }

        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            LOGGER.debug(token);
            if (!token.isEmpty()) {
                listaDetalles.add(token);
            }
        }

        // Se recorre la cadena con los codigos de las manzanas/veredas
        // y se compara con los codigos de las manzanas/veredas contenidas
        // en el area de oferta
        for (DetalleCapturaOferta detail : areaCapturaOferta
            .getDetalleCapturaOfertas()) {
            for (String detailManzanaVeredaCod : listaDetalles) {
                if (detailManzanaVeredaCod.equals(detail
                    .getManzanaVeredaCodigo())) {
                    // Se asocia la manzana/vereda con la region
                    region.getDetalleCapturaOfertas().add(detail);
                    detail.setRegionCapturaOferta(region);
                }
            }
        }

        // Se actualizan los datos de la regiÃ³n
        this.regionCapturaOfertaService.update(region);

        // Se actualizan los detalles de la regiÃ³n
        this.detalleCapturaOfertaService.updateMultiple(areaCapturaOferta
            .getDetalleCapturaOfertas());

    }

    /**
     * @see IAvaluosLocal#buscarOfertasPorIdRecolectorEIdControlCalidad(String, Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<ControlCalidadOferta> buscarOfertasPorIdRecolectorEIdControlCalidad(
        String recolectorId, Long controlCalidadId) {

        List<ControlCalidadOferta> listaOfertasControlCalidad = this.controlCalidadOfertaService
            .buscarOfertasPorIdRecolectorEIdControlCalidad(recolectorId,
                controlCalidadId);

        return listaOfertasControlCalidad;
    }

    /**
     * @see IAvaluosLocal#crearRegionCapturaOferta(RegionCapturaOferta)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public RegionCapturaOferta crearRegionCapturaOferta(
        RegionCapturaOferta regionCapturaOferta) {
        RegionCapturaOferta aux = null;
        try {
            aux = this.regionCapturaOfertaService.getEntityManager().merge(
                regionCapturaOferta);
            LOGGER.debug(String.valueOf(aux.getId()));
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#crearRegionCapturaOferta: " +
                ex.getMensaje());
        }

        return aux;
    }

    /**
     * @see IAvaluosLocal#desasignarRegionAreaDescentralizadaRecolector(RegionCapturaOferta[])
     *
     * @author rodrigo.hernandez
     */
    @Override
    public void desasignarRegionAreaDescentralizadaRecolector(
        RegionCapturaOferta[] listaRegionesSeleccionadas) {
        // Elimina las regiones
        for (RegionCapturaOferta rco : listaRegionesSeleccionadas) {
            this.regionCapturaOfertaService.delete(rco);
        }
    }

    /**
     * @see IAvaluosLocal#desasignarRegionRecolector(RegionCapturaOferta[], Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public void desasignarRegionRecolector(
        RegionCapturaOferta[] listaRegionesSeleccionadas) {

        List<DetalleCapturaOferta> listaDetalles = null;

        // Actualiza el estado de los detalles que estaban asociados a la region
        for (RegionCapturaOferta rco : listaRegionesSeleccionadas) {
            listaDetalles = new ArrayList<DetalleCapturaOferta>();

            if (rco != null) {
                for (DetalleCapturaOferta dco : rco.getDetalleCapturaOfertas()) {
                    dco.setRegionCapturaOferta(null);
                    listaDetalles.add(dco);
                }

                this.detalleCapturaOfertaService.updateMultiple(listaDetalles);
            }

        }

        // Elimina las regiones
        for (RegionCapturaOferta rco : listaRegionesSeleccionadas) {
            if (rco != null) {
                this.regionCapturaOfertaService.delete(rco);
            }
        }

    }

    /**
     * @see IAvaluosLocal#moverProcesoControlCalidadOferta(List, String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public void moverProcesoControlCalidadOferta(
        List<ControlCalidadOferta> listaOfertasControlCalidad,
        String estadoOfertas) {

        LOGGER.debug("En AvaluosBean#doDatabaseUpdate");

        //Actualiza el estado de las ofertas inmobiliarias
        for (ControlCalidadOferta cco : listaOfertasControlCalidad) {
            cco.getOfertaInmobiliariaId().setEstado(estadoOfertas);
            this.ofertaInmobiliariaService.update(cco.getOfertaInmobiliariaId());
        }

    }

    /**
     * @see IAvaluosLocal#getIdUltimaRegionRegistrada()
     *
     * @author rodrigo.hernandez
     */
    @Override
    public Long consultarIdUltimaRegionRegistrada() {
        Long id = this.regionCapturaOfertaService.getIdUltimaRegionRegistrada();
        return id;
    }

    /**
     * @see IAvaluosLocal#obtenerAreaCapturaOfertaConDatosPorId(Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public AreaCapturaOferta obtenerAreaCapturaOfertaConDatosPorId(
        Long areaCapturaOfertaId) {
        AreaCapturaOferta aco = buscarAreaCapturaOfertaPorId(areaCapturaOfertaId);
        aco.getMunicipio().getCodigo();
        aco.getDepartamento().getCodigo();
        if (aco.getZona() != null) {
            aco.getZona().getCodigo();
        }

        aco.getDetalleCapturaOfertas().size();
        for (RegionCapturaOferta rco : aco.getRegionCapturaOfertas()) {
            rco.getDetalleCapturaOfertas().size();
        }
        return aco;
    }

    /**
     * @see IAvaluosLocal#buscarListaOfertasInmobiliariasPorRecolector(String, Long, String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<OfertaInmobiliaria> buscarListaOfertasInmobiliariasPorRecolector(
        String recolectorId, Long regionId, String estado) {

        List<OfertaInmobiliaria> listaOfertas = null;

        try {
            listaOfertas = this.ofertaInmobiliariaService
                .buscarListaOfertasInmobiliariasPorRecolector(recolectorId, regionId, estado);

            for (OfertaInmobiliaria oi : listaOfertas) {
                oi.getOfertaPredios().size();
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#crearRegionCapturaOferta: " +
                ex.getMensaje());
        }

        return listaOfertas;
    }

    /**
     * MÃ©todo para cargar buscar una RegionCapturaOferta a partir de un id
     *
     * @author rodrigo.hernandez
     *
     * @param regionId - Id de la regiÃ³n a buscar
     *
     * @return Region de Captura de Ofertas
     */
    @Override
    public RegionCapturaOferta buscarRegionCapturaOfertaPorId(Long regionId) {
        RegionCapturaOferta region = null;

        try {
            region = this.regionCapturaOfertaService.getRegionCapturaOfertaPorId(regionId);

            AreaCapturaOferta aco = region.getAreaCapturaOferta();

            if (aco.getDepartamento() != null) {
                aco.getDepartamento().getCodigo();
            }

            if (aco.getMunicipio() != null) {
                aco.getMunicipio().getCodigo();
            }

            if (aco.getZona() != null) {
                aco.getZona().getCodigo();
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#crearRegionCapturaOferta: " + ex.getMensaje());
        }

        return region;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarRegionCapturaOferta(RegionCapturaOferta)
     */
    @Override
    public RegionCapturaOferta guardarActualizarRegionCapturaOferta(
        RegionCapturaOferta regionCapturaOferta) {
        try {
            regionCapturaOferta = this.regionCapturaOfertaService.update(regionCapturaOferta);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#crearRegionCapturaOferta: " + ex.getMensaje());
        }
        return regionCapturaOferta;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IAvaluosLocal#consultarPrediosPorRegionCapturaOferta(RegionCapturaOferta,String)
     */
    @Override
    public List<Predio> consultarPrediosPorRegionCapturaOferta(
        RegionCapturaOferta regionCapturaOferta, String idRecolector) {

        LOGGER.debug("on AvaluosBean#buscarPrediosPorRegionCapturaOferta");

        List<Predio> answer = null;

        try {

            Long idRegionCaptura = regionCapturaOferta.getId();

            answer = this.predioService.buscarPrediosPorRegionCapturaOferta(idRegionCaptura,
                idRecolector);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#buscarPrediosPorRegionCapturaOferta: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * MÃƒÂ©todo para registrar datos en la tabla DETALLE_CAPTURA_OFERTA
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<DetalleCapturaOferta> crearListaDetallesCapturaOferta(
        List<DetalleCapturaOferta> listaDetallesCapturaOferta) {

        LOGGER.debug("Creando registros para DetalleCapturaOferta");

        try {

            listaDetallesCapturaOferta = this.detalleCapturaOfertaService.updateMultipleReturn(
                listaDetallesCapturaOferta);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#crearListaDetallesCapturaOferta: " +
                ex.getMensaje());
        }
        return listaDetallesCapturaOferta;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerRegionesCapturaOfertaDeComisionNF(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ArrayList<RegionCapturaOferta> obtenerRegionesCapturaOfertaDeComisionNF(Long idComision) {

        ArrayList<RegionCapturaOferta> answer = null;

        try {
            answer = (ArrayList<RegionCapturaOferta>) this.regionCapturaOfertaService.
                getByComisionIdNF(idComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarRegionesCapturaOfertaDeComision: " +
                ex.getMensaje());
        }
        return answer;
    }

    @Override
    public List<TramitePredioAvaluo> buscarPrediosPorTramiteId(Long tramiteId) {
        List<TramitePredioAvaluo> listaPredios = new ArrayList<TramitePredioAvaluo>();
        try {
            listaPredios = tramitePredioAvaluoService
                .buscarPrediosPorTramiteId(tramiteId);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en busqueda de predios por id tramite" +
                e.getMensaje());
        }

        return listaPredios;
    }

    /**
     * @see IAvaluosLocal#obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector(Long, String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector(
        Long idAreaCapturaOferta, String recolectorLogin) {
        LOGGER.debug(
            "Iniciando AvaluosBean#obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector");

        List<RegionCapturaOferta> listaRegionesCapturaOferta = null;

        try {
            listaRegionesCapturaOferta = this.regionCapturaOfertaService
                .getRegionesCapturaOfertaPorAreaCapturaOfertaYRecolector(
                    idAreaCapturaOferta, recolectorLogin);

            for (RegionCapturaOferta rco : listaRegionesCapturaOferta) {
                if (rco.getDetalleCapturaOfertas() != null) {
                    rco.getDetalleCapturaOfertas().size();
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en AvaluosBean#obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector: " +
                ex.getMensaje());
        }
        return listaRegionesCapturaOferta;
    }

    /**
     * @see IAvaluosLocal#obtenerRegionesCapturaOfertaPorAreaCapturaOferta(Long)
     *
     * @author christian.rodriguez
     */
    @Override
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaPorAreaCapturaOferta(
        Long idAreaCapturaOferta) {
        LOGGER.debug(
            "Iniciando AvaluosBean#obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector");

        List<RegionCapturaOferta> listaRegionesCapturaOferta = null;

        try {
            listaRegionesCapturaOferta = this.regionCapturaOfertaService
                .cargarRegionCapturaOfertaPorIdArea(idAreaCapturaOferta);

            for (RegionCapturaOferta rco : listaRegionesCapturaOferta) {
                if (rco.getDetalleCapturaOfertas() != null) {
                    rco.getDetalleCapturaOfertas().size();
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en AvaluosBean#obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector: " +
                ex.getMensaje());
        }
        return listaRegionesCapturaOferta;
    }
//-----------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerListaDetallesCapturaOfertaPorArea(Long)
     *
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<DetalleCapturaOferta> obtenerListaDetallesCapturaOfertaPorArea(
        Long idArea) {

        LOGGER.debug("Iniciando AvaluosBean#obtenerListaDetallesCapturaOfertaPorArea");

        List<DetalleCapturaOferta> listaDetalleCapturaOferta = null;

        try {
            listaDetalleCapturaOferta = this.detalleCapturaOfertaService.
                cargarDetallesCapturaOfertaPorArea(idArea);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerListaDetallesCapturaOfertaPorArea: " +
                ex.getMensaje());
        }

        return listaDetalleCapturaOferta;
    }

    @Override
    public void adicionarPrediosTramite(TramitePredioAvaluo prediosActualizar) {

        LOGGER.debug("Iniciando AvaluosBean#adicionarPrediosTramite");
        try {
            this.tramitePredioAvaluoService.adicionarPredioAAvaluo(prediosActualizar);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#adicionarPrediosTramite: " +
                ex.getMensaje());
        }
    }

    @Override
    public void eliminarPrediosTramite(Long tramitePredioId) {
        LOGGER.debug("Iniciando AvaluosBean#eliminarPrediosTramite");
        try {
            this.tramitePredioAvaluoService.removerPredioDeAvaluo(tramitePredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#eliminarPrediosTramite: " +
                ex.getMensaje());
        }
    }

    @Override
    public void modificarPrediosTramite(TramitePredioAvaluo predioActualizar) {
        LOGGER.debug("Iniciando AvaluosBean#modificarPrediosTramite");
        try {
            this.tramitePredioAvaluoService.actualizarPredioDeAvaluo(predioActualizar);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#modificarPrediosTramite: " +
                ex.getMensaje());
        }
    }

    /**
     * @see IAvaluosLocal#obtenerIdsRegionesAsociadasAControlCalidad(Long)
     */
    @Override
    public List<Long> obtenerIdsRegionesAsociadasAControlCalidad(Long controlCalidadId) {
        LOGGER.debug("Iniciando AvaluosBean#obtenerIdsRegionesAsociadasAControlCalidad");

        List<Long> idRegiones = null;

        try {
            idRegiones = this.controlCalidadOfertaService
                .buscarIdsRegionesAsociadasAControlCalidad(controlCalidadId);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerIdsRegionesAsociadasAControlCalidad: " +
                ex.getMensaje());
        }

        return idRegiones;
    }

    /**
     * @see IAvaluosLocal#obtenerControlCalidadPorId(Long)
     */
    @Implement
    @Override
    public ControlCalidad obtenerControlCalidadPorId(Long idControlCalidad) {
        LOGGER.debug("Iniciando AvaluosBean#buscarControlCalidadPorId ...");

        ControlCalidad answer = null;

        try {
            answer = this.controlCalidadService.findById(idControlCalidad);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en busqueda del control de calidad por id " + e.getMensaje());
        }

        return answer;
    }

    /**
     * @author rodrigo.hernandez
     */
    @Override
    public List<String> consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(
        Long idSolicitante, boolean banderaSolicitudRevision) {

        LOGGER.debug(
            "Iniciando AvaluosBean#consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion ...");

        List<String> answer = new ArrayList<String>();

        try {
            answer = this.avaluoService
                .consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(
                    idSolicitante, banderaSolicitudRevision);
        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "Error buscando Sec. Radicados de solicitud de Avaluo para Revisar o Impugnar " +
                e.getMensaje());
        }

        return answer;
    }

    /**
     * @author rodrigo.hernandez
     */
    @Override
    public List<String> consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(
        Long idSolicitante) {
        LOGGER.debug(
            "Iniciando AvaluosBean#consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo ...");

        List<String> answer = new ArrayList<String>();

        try {
            answer = this.avaluoService
                .consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(
                    idSolicitante);
        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "Error buscando Sec. Radicados de solicitud de Avaluo para Revisar o Impugnar " +
                e.getMensaje());
        }

        return answer;
    }

    /**
     * @author christian.rodriguez
     * @see IAvaluosLocal#guardarActualizarSolicitudPredio(SolicitudPredio)
     */
    @Implement
    @Override
    public SolicitudPredio guardarActualizarSolicitudPredio(SolicitudPredio solicitudPredio) {

        LOGGER.debug("Iniciando AvaluosBean#guardarActualizarSolicitudPredio ...");

        SolicitudPredio answer = new SolicitudPredio();

        try {
            answer = this.solicitudPredioService.update(solicitudPredio);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error guardando SolicitudPredio. " + e.getMensaje());
        }

        return answer;
    }

    /**
     * @author christian.rodriguez
     * @see IAvaluosLocal#guardarActualizarSolicitudesPredio(List)
     */
    @Implement
    @Override
    public List<SolicitudPredio> guardarActualizarSolicitudesPredio(
        List<SolicitudPredio> solicitudesPredio) {

        LOGGER.debug("Iniciando AvaluosBean#guardarActualizarSolicitudesPredio ...");

        List<SolicitudPredio> answer = new ArrayList<SolicitudPredio>();

        try {
            answer = this.solicitudPredioService.updateMultipleReturn(solicitudesPredio);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error guardando SolicitudesPredio. " + e.getMensaje());
        }

        return answer;
    }

    /**
     * @author christian.rodriguez
     * @see IAvaluosLocal#borrarSolicitudPredioPorId(Long)
     */
    @Implement
    @Override
    public boolean borrarSolicitudPredioPorId(SolicitudPredio solicitudPredio) {

        try {

            if (solicitudPredio != null) {
                this.solicitudPredioService.delete(solicitudPredio);
                return true;
            }
            return false;

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#borrarSolicitudPredioPorId: " + ex.getMensaje());
        }
        return false;
    }

    /**
     * @author christian.rodriguez
     * @see IAvaluosLocal#borrarSolicitudesPredio(List)
     */
    @Implement
    @Override
    public boolean borrarSolicitudesPredio(List<SolicitudPredio> solicitudesPredio) {

        try {
            if (solicitudesPredio != null) {
                this.solicitudPredioService.deleteMultiple(solicitudesPredio);
                return true;
            }
            return false;

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#borrarSolicitudesPredioPorId: " + ex.getMensaje());
        }
        return false;
    }

    /**
     * @author felipe.cadena
     * @see IAvaluosLocal#obtenerSolicitudDocumentacionPorIdSolicitud(java.math.Long, Boolean)
     */
    @Override
    public List<SolicitudDocumentacion> obtenerSolicitudDocumentacionPorIdSolicitud(Long idSolicitud,
        Boolean aportado) {

        List<SolicitudDocumentacion> consulta = null;
        try {
            consulta = this.solicitudDocumentacionService.
                obternerSolicitudesDocumentacionPorSolicitud(idSolicitud, aportado);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerSolicitudDocumentacionPorIdSolicitud: " +
                ex.getMensaje());
        }
        return consulta;
    }

    /**
     * @author rodrigo.hernandez
     *
     * @modified felipe.cadena -- Se agrega el parametro usuario
     * @see IAvaluosLocal#buscarRadicacionesNuevasAvaluosComerciales(Date, Date, String, String,
     * String, String, String, String, String)
     */
    @Implement
    @Override
    public List<RadicacionNuevaAvaluoComercialDTO> buscarRadicacionesNuevasAvaluosComerciales(
        FiltroDatosConsultaRadicacionesCorrespondencia filtroDatos, UsuarioDTO usuario) {

        List<RadicacionNuevaAvaluoComercialDTO> radicacionesNuevas =
            new ArrayList<RadicacionNuevaAvaluoComercialDTO>();

        try {
            radicacionesNuevas = this.sncProcedimientoDao
                .buscarRadicacionesNuevasAvaluosComerciales(filtroDatos, usuario);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarRadicacionesNuevasAvaluosComerciales: " +
                ex.getMensaje());
        }
        return radicacionesNuevas;
    }

    /**
     * @modified felipe.cadena -- Se agrega el parametro usuario
     * @see IAvaluosLocal#reclasificarTramiteDocumentoODocumento(java.lang.String, java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public boolean reclasificarTramiteDocumentoODocumento(String numeroRadicacion,
        String codigoTipoTramite, UsuarioDTO usuario) {
        boolean result = true;

        try {
            this.sncProcedimientoDao
                .reclasificarTramiteODocumento(numeroRadicacion, codigoTipoTramite, usuario);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarRadicacionesNuevasAvaluosComerciales: " +
                ex.getMensaje());

            result = false;
        }

        return result;
    }

    /**
     * @author felipe.cadena
     * @see IAvaluosLocal#obtenerSolicitudDocumentacionPorIdSolicitudAgrupadas(java.math.BigDecimal)
     */
    @Override
    public List<SolicitudDocumentacion> obtenerSolicitudDocumentacionPorIdSolicitudAgrupadas(
        Long idSolicitud) {

        List<SolicitudDocumentacion> solicitudes = this.solicitudDocumentacionService
            .obternerSolicitudesDocumentacionPorSolicitud(idSolicitud, true);

        List<SolicitudDocumentacion> solicitudesAgrupadas = new ArrayList<SolicitudDocumentacion>();

        if (solicitudes != null && !solicitudes.isEmpty()) {
            solicitudesAgrupadas.add(solicitudes.get(0));
            for (int i = 1; i < solicitudes.size(); i++) {
                SolicitudDocumentacion sd = solicitudes.get(i);
                if (sd.getTipoDocumento().getId().equals(solicitudes.get(i - 1).getTipoDocumento().
                    getId())) {
                    continue;
                } else {
                    solicitudesAgrupadas.add(sd);
                }

            }

            for (SolicitudDocumentacion sda : solicitudesAgrupadas) {
                sda.setDocumentosSolicitud(new ArrayList<Documento>());
                for (SolicitudDocumentacion sd : solicitudes) {
                    if (sda.getTipoDocumento().getId().equals(sd.getTipoDocumento().getId())) {
                        sda.getDocumentosSolicitud().add(
                            sd.getSoporteDocumentoId());
                    }
                }
            }
        }

        return solicitudesAgrupadas;
    }

    /**
     * @see IAvaluosLocal#buscarRadicacion(String)
     *
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public RadicacionNuevaAvaluoComercialDTO buscarRadicacion(String numeroRadicacion,
        UsuarioDTO usuario) {
        RadicacionNuevaAvaluoComercialDTO radicacion = new RadicacionNuevaAvaluoComercialDTO();

        try {
            radicacion = this.sncProcedimientoDao
                .buscarRadicacion(numeroRadicacion, usuario);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarRadicacion: " +
                ex.getMensaje());
        }
        return radicacion;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluosPorSolicitudId(Long)
     *
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosPorSolicitudId(Long idSolicitud) {
        LOGGER.debug("Iniciando AvaluosBean#buscarAvaluosPorSolicitudId ...");

        List<Avaluo> answer = null;

        try {
            answer = this.avaluoService.buscarAvaluosPorSolicitudId(idSolicitud);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en busqueda de  los avalÃºos asociados al solicitud. " +
                e.getMensaje());
        }

        return answer;
    }

    // ----------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#crearAvaluoRadicacion(Avaluo, Solicitud, SolicitanteSolicitud, List,
     * UsuarioDTO)
     *
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public Avaluo crearAvaluoRadicacion(Avaluo avaluo, Solicitud solicitud,
        SolicitanteSolicitud solicitante, List<SolicitudPredio> prediosAAsociar,
        UsuarioDTO usuario) {

        LOGGER.debug("Iniciando AvaluosBean#crearAvaluoRadicacion ...");

        try {

            if (avaluo != null && solicitud != null && prediosAAsociar != null && usuario != null) {

                String siguienteSecRadicado = this.avaluoService
                    .obtenerSiguienteSecRadicadoPorSolicitud(solicitud, false);

                if (siguienteSecRadicado == null) {
                    return null;
                }

                // Creación del trámite asociado al avalúo
                Tramite tramite = new Tramite();

                tramite.setSolicitud(solicitud);
                tramite.setTipoTramite(solicitud.getTipo());
                tramite.setEstado(ETramiteEstado.EN_PROCESO.getCodigo());
                tramite.setUsuarioLog(usuario.getLogin());
                tramite.setFechaLog(new Date());
                tramite.setNumeroRadicacion(siguienteSecRadicado);

                tramite = this.tramiteService.update(tramite);

                List<SolicitanteSolicitud> solicitantesSolicitud = solicitud
                    .getSolicitanteSolicituds();
                List<SolicitanteTramite> solicitantesTramite = new ArrayList<SolicitanteTramite>();

                for (SolicitanteSolicitud solicitanteSolicitud : solicitantesSolicitud) {
                    SolicitanteTramite solicitanteTramite = new SolicitanteTramite(
                        solicitanteSolicitud);
                    solicitanteTramite.setTramite(tramite);
                    solicitantesTramite.add(solicitanteTramite);
                }

                tramite.setSolicitanteTramites(solicitantesTramite);

                tramite = this.tramiteService.update(tramite);

                if (tramite == null) {
                    return null;
                }

                // CreaciÃ³n del avalÃºo
                avaluo.setTramite(tramite);
                avaluo.setUsuarioLog(usuario.getLogin());
                avaluo.setFechaLog(new Date());
                avaluo.setPlazoEjecucion(0L);
                avaluo.setCalificacion(0L);
                avaluo.setValorCotizacion(0D);

                String estructuraOrganizacionalCod = usuario
                    .getCodigoEstructuraOrganizacional();
                if (estructuraOrganizacionalCod == null) {
                    avaluo.setEstructuraOrganizacionalCod(
                        Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
                } else {
                    avaluo.setEstructuraOrganizacionalCod(estructuraOrganizacionalCod);
                }

                avaluo.setSecRadicado(siguienteSecRadicado);

                avaluo = this.avaluoService.update(avaluo);

                if (avaluo == null) {
                    return null;
                }

                // AsociaciÃ³n de los predios al avalÃºo
                this.asociarPrediosAvaluo(avaluo, prediosAAsociar, usuario);

            } else {
                return null;
            }

        } catch (Exception ex) {
            LOGGER.debug("Ocurrio un error al crear el avaluo:" + ex.getMessage());
        }

        return avaluo;

    }

    // ----------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#actualizarAvaluoRadicacion(Avaluo, Solicitud, List, List, UsuarioDTO)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public Avaluo actualizarAvaluoRadicacion(Avaluo avaluo, Solicitud solicitud,
        List<SolicitudPredio> prediosAAsociar, List<AvaluoPredio> prediosADesasociar,
        UsuarioDTO usuario) {

        if (avaluo != null && avaluo.getId() != null) {

            Long idSolicitud = solicitud.getId();

            this.asociarPrediosAvaluo(avaluo, prediosAAsociar, usuario);
            this.desasociarPrediosAvaluo(avaluo, prediosADesasociar, idSolicitud);

            avaluo.getAvaluoPredios().removeAll(prediosADesasociar);
            try {
                avaluo = this.avaluoService.update(avaluo);
            } catch (ExcepcionSNC e) {
                LOGGER.error("Error actualizando el avalÃºo. " +
                    e.getMensaje());
            }

        }

        avaluo = this.avaluoService.buscarAvaluoPorIdConAvaluoPredio(avaluo.getId());

        return avaluo;
    }

    // ----------------------------------------------------------------------
    /**
     * Asocia predios a un avaluo. Modifica la tabla AvaluoPredio creando los predios nuevos.
     * Modifica la tabal SolicitudPredio asociando los predios al avaluo predio creado
     *
     * @author christian.rodriguez
     * @param avaluo Avaluo al que se le van a crear los predios
     * @param prediosAAsociar lista de predios que se van a asociar al avaluo
     * @param usuario usuario que asocia los predios
     * @return avaluo con los predios modificados
     */
    private void asociarPrediosAvaluo(Avaluo avaluo, List<SolicitudPredio> prediosAAsociar,
        UsuarioDTO usuario) {

        if (prediosAAsociar != null && !prediosAAsociar.isEmpty()) {
            for (SolicitudPredio predio : prediosAAsociar) {
                AvaluoPredio avaluoPredio = new AvaluoPredio(predio);

                avaluoPredio.setAvaluo(avaluo);
                avaluoPredio.setContacto(predio.getContacto());
                avaluoPredio.setUsuarioLog(usuario.getLogin());
                avaluoPredio.setFechaLog(new Date());

//TODO :: christian.rodriguez :: hacer manejo de excepciones :: pedro.garcia
                avaluoPredio = this.avaluoPredioService.update(avaluoPredio);

                predio.setAvaluoPredioId(avaluoPredio.getId());

            }
            try {
                this.solicitudPredioService.updateMultiple(prediosAAsociar);
            } catch (ExcepcionSNC e) {
                LOGGER.error("Error actualizando los predios avalÃºos. " + e.getMensaje());
            }
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Desasociar los predios de un avaluo. Modifica la tabla de AvaluoPredio eliminando los
     * predios. modifica la tabla SolicitudPredio desasociando los predios del avaluo
     *
     * @author christian.rodriguez
     * @param avaluo Avaluo a modificar
     * @param prediosADesasociar predios a desasociar del avaluo
     * @param idSolicitud solicitud asociada al avaluo
     */
    private void desasociarPrediosAvaluo(Avaluo avaluo, List<AvaluoPredio> prediosADesasociar,
        Long idSolicitud) {
        List<SolicitudPredio> prediosADesasociarSolicitud = new ArrayList<SolicitudPredio>();

        Avaluo avaluoBD = this.avaluoService.buscarAvaluoPorIdConAvaluoPredio(avaluo.getId());

        if (avaluoBD != null) {

            for (AvaluoPredio predioAvaluo : avaluoBD.getAvaluoPredios()) {
                if (prediosADesasociar.indexOf(predioAvaluo) != -1) {

                    SolicitudPredio predioADesasociarAvaluo = this.solicitudPredioService
                        .buscarSolicitudPredioPorAvaluoIdSolicitudIdPredioId(
                            predioAvaluo.getPredioId(), idSolicitud, predioAvaluo.getId());

                    predioADesasociarAvaluo.setAvaluoPredioId(null);
                    prediosADesasociarSolicitud.add(predioADesasociarAvaluo);

                }
            }
            try {
                if (!prediosADesasociarSolicitud.isEmpty()) {
//TODO :: ?? :: 23-04-2013 :: validar que se haya hecho la actualizaciÃ³n de todos; si no, dar aviso :: pedro.garcia                    
                    this.solicitudPredioService
                        .updateMultiple(prediosADesasociarSolicitud);
                }

                if (!prediosADesasociar.isEmpty()) {
                    this.avaluoPredioService
                        .deleteMultiple(prediosADesasociar);
                }
            } catch (ExcepcionSNC e) {
                LOGGER.error("Error al desasociar los predios. " + e.getMensaje());
            }
        }
    }

    /**
     * @see IAvaluosLocal#buscarAvaluoPorSecRadicado(java.lang.String)
     *
     * @author felipe.cadena
     */
    @Override
    public Avaluo buscarAvaluoPorSecRadicado(String secRadicado) {
        Avaluo avaluo = null;
        try {
            avaluo = this.avaluoService.buscarAvaluoPorSecRadicado(secRadicado);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarAvaluoPorSecRadicado: " +
                ex.getMensaje());
        }

        return avaluo;
    }

    /**
     * @author felipe.cadena
     * @see IAvaluosLocal#obtenerSolicitudDocumentacionPorSecRadicadoAgrupadas(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public List<SolicitudDocumentacion> obtenerSolicitudDocumentacionPorSecRadicadoAgrupadas(
        Long idSolicitud, Long idTramite) {

        List<SolicitudDocumentacion> solicitudesSecRadicado =
            new ArrayList<SolicitudDocumentacion>();

        List<SolicitudDocumentacion> solicitudes = this.solicitudDocumentacionService
            .obternerSolicitudesDocumentacionPorSolicitud(idSolicitud, true);

        for (SolicitudDocumentacion solicitudDocumentacion : solicitudes) {

            LOGGER.debug("tramite doc " + solicitudDocumentacion.getSoporteDocumentoId().
                getTramiteId());
            LOGGER.debug("tramite consulta " + idTramite);
            if (solicitudDocumentacion.getSoporteDocumentoId().getTramiteId() == idTramite) {
                solicitudesSecRadicado.add(solicitudDocumentacion);
            }
        }

        List<SolicitudDocumentacion> solicitudesAgrupadas = new ArrayList<SolicitudDocumentacion>();

        if (solicitudesSecRadicado != null && !solicitudesSecRadicado.isEmpty()) {
            solicitudesAgrupadas.add(solicitudesSecRadicado.get(0));
            for (int i = 1; i < solicitudesSecRadicado.size(); i++) {
                SolicitudDocumentacion sd = solicitudesSecRadicado.get(i);
                if (sd.getDetalle().equals(solicitudesSecRadicado.get(i - 1).getDetalle())) {
                    continue;
                } else {
                    solicitudesAgrupadas.add(sd);
                }

            }

            for (SolicitudDocumentacion sda : solicitudesAgrupadas) {
                sda.setDocumentosSolicitud(new ArrayList<Documento>());
                for (SolicitudDocumentacion sd : solicitudes) {
                    if (sda.getDetalle().equals(sd.getDetalle())) {
                        sda.getDocumentosSolicitud().add(
                            sd.getSoporteDocumentoId());
                    }
                }
            }
        }

        return solicitudesAgrupadas;
    }

    /**
     * @author felipe.cadena
     * @see IAvaluosLocal#buscarAvaluoPredioPorIdAvaluo(java.lang.Long, java.lang.Long)
     */
    @Override
    public List<AvaluoPredio> buscarAvaluoPredioPorIdAvaluo(Long idAvaluo) {

        List<AvaluoPredio> aps = null;
        try {
            aps = avaluoPredioService.buscarAvaluoPredioPorIdAvaluo(idAvaluo);
        } catch (ExcepcionSNC e) {
            LOGGER.error("AvaluosBean#buscarAvaluoPredioPorIdAvaluo: " + idAvaluo +
                e.getMensaje());
        }

        return aps;

    }

    /**
     * @see IAvaluosLocal#obtenerAvaluoPredioPorId(Long)
     * @author christian.rodriguez
     */
    @Override
    public AvaluoPredio obtenerAvaluoPredioPorId(Long idAvaluoPredio) {
        LOGGER.debug("Iniciando AvaluosBean#buscarAvaluoPredioPorId ...");

        AvaluoPredio answer = null;

        try {
            answer = this.avaluoPredioService.findById(idAvaluoPredio);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en busqueda del avalÃºo predio con id " + idAvaluoPredio +
                e.getMensaje());
        }

        return answer;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarAvaluoPredio(AvaluoPredio)
     * @author christian.rodriguez
     */
    @Override
    public AvaluoPredio guardarActualizarAvaluoPredio(AvaluoPredio avaluoPredio) {
        LOGGER.debug("Iniciando AvaluosBean#guardarActualizarAvaluoPredio ...");

        AvaluoPredio answer = null;

        try {
            answer = this.avaluoPredioService.update(avaluoPredio);
        } //TODO :: christian.rodriguez :: 'update' no lanza ese tipo de excepciÃ³n!! :: pedro.garcia
        catch (ExcepcionSNC e) {
            LOGGER.error("Error al actualizar/guardar el avalÃºo predio: " + e.getMensaje());
        }

        return answer;
    }

    // --------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#eliminarAvaluoRadicaciÃ³n(Avaluo, Long)
     * @author christian.rodriguez
     */
    @Override
    public boolean eliminarAvaluoRadicacion(Avaluo avaluoAEliminar, Long solicitudId) {

        if (avaluoAEliminar != null) {

            this.desasociarPrediosAvaluo(avaluoAEliminar, avaluoAEliminar.getAvaluoPredios(),
                solicitudId);
            try {

                this.avaluoService.delete(avaluoAEliminar);
                this.tramiteService.delete(avaluoAEliminar.getTramite());

                this.reAsignarSecRadicadoAvaluosSolicitud(solicitudId);

                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.error(
                    "Error al eliminar el avalÃºo con id " + avaluoAEliminar.getId() + " :" +
                    e.getMensaje());
            }
        }

        return false;
    }

    /**
     * MÃ©todo que reasigna los sec radicado para los avalÃºos que no han sido movidos en el process
     * (sin id de process)
     *
     * @author christian.rodriguez
     * @param solicitudId id de la solicitud de los avaluos que se quieren reasignar
     */
    private void reAsignarSecRadicadoAvaluosSolicitud(Long solicitudId) {

        Solicitud solicitud = this.solicitudService.findById(solicitudId);

        if (solicitud != null) {

            List<Avaluo> avaluosSolicitud = this.avaluoService
                .buscarAvaluosPorSolicitudId(solicitudId);

            Long digitoSecRadicado = this.avaluoService
                .obtenerSiguienteDigitoSecRadicadoPorSolicitud(solicitud, true);

            List<Avaluo> avaluosModificados = new ArrayList<Avaluo>();
            for (Avaluo avaluo : avaluosSolicitud) {

                if (avaluo.getTramite().getProcesoInstanciaId() != null) {
                    continue;
                }

                String siguienteSecRadicado = solicitud.getNumero().concat("-")
                    .concat(String.valueOf(digitoSecRadicado));

                avaluo.setSecRadicado(siguienteSecRadicado);

                avaluosModificados.add(avaluo);

                digitoSecRadicado++;
            }

            this.avaluoService.updateMultiple(avaluosModificados);

        }

    }

    /**
     * @see IAvaluosLocal#avanzarAvaluosRadicacion(List, Long)
     * @author christian.rodriguez
     */
    @Override
    public boolean avanzarAvaluosRadicacion(List<Avaluo> avaluosAAvanzar, Long solicitudId) {

        Solicitud solicitud = this.solicitudService.findById(solicitudId);

        if (solicitud != null && avaluosAAvanzar != null && !avaluosAAvanzar.isEmpty()) {

            Tramite tramiteAvaluo = null;

            for (Avaluo avaluo : avaluosAAvanzar) {

                if (avaluo.getTramite().getProcesoInstanciaId() != null) {
                    continue;
                }

                String serRadicadoSiguiente = this.avaluoService
                    .obtenerSiguienteSecRadicadoPorSolicitud(solicitud, true);

                avaluo.setSecRadicado(serRadicadoSiguiente);

                /**
                 * TODO CÃ³digo para crear proceso
                 *
                 */
                String idProceso = "PRUEBA";

                tramiteAvaluo = avaluo.getTramite();
                tramiteAvaluo.setProcesoInstanciaId(idProceso);

                this.tramiteService.update(tramiteAvaluo);
                this.avaluoService.update(avaluo);

            }

            this.reAsignarSecRadicadoAvaluosSolicitud(solicitudId);

            return true;
        }

        return false;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * @see IAvaluosLocal#buscarSolicitudPorNumero(String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public Solicitud buscarSolicitudPorNumero(String numero) {
        LOGGER.debug("Inicio AvaluosBean#buscarSolicitudPorNumero ...");

        Solicitud solicitud = null;

        FiltroDatosSolicitud filtroDatosSolicitud = new FiltroDatosSolicitud();

        filtroDatosSolicitud.setNumero(numero);

        List<Solicitud> solicitudes = null;

        try {

            solicitudes = this.solicitudService.buscarSolicitudes(
                filtroDatosSolicitud, 0, 0);

            if (solicitudes != null && !solicitudes.isEmpty()) {
                solicitud = solicitudes.get(0);
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarSolicitudPorNumero: " + ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#buscarSolicitudPorNumero ...");
        return solicitud;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * MÃ©todo para actualizar los SolicitudSolicitante asociados a una solicitud
     *
     * @author rodrigo.hernandez
     *
     * @param listaNueva - Lista de solicitudSolicitante modificada en la pagina
     * @param listaBD - Lista de solicitudSolicitante almacenada en BD
     */
    private void actualizarSolicitudSolicitante(
        List<SolicitanteSolicitud> listaNueva,
        List<SolicitanteSolicitud> listaBD) {

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarActualizarSolicitudAvaluoComercial(Solicitud)
     *
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public Solicitud guardarActualizarSolicitudAvaluoComercial(Solicitud solicitud) {

        LOGGER.debug("Inicio AvaluosBean#guardarActualizarSolicitudAvaluoComercial");

        try {

            solicitud = this.solicitudService.update(solicitud);

        } catch (Exception ex) {
            LOGGER.debug("Ocurrio un error al guardar la solicitud:" +
                ex.getMessage());
        }

        LOGGER.debug("Fin AvaluosBean#guardarActualizarSolicitudAvaluoComercial");

        return solicitud;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarActualizarSolicitanteSolicituds(List, Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<SolicitanteSolicitud> guardarActualizarSolicitanteSolicituds(
        List<SolicitanteSolicitud> listaSolicitanteSolicituds,
        Long solicitudId) {

        /*
         * Se consulta la lista de solicitanteSolicitantes asociados a la solicitud
         */
        List<SolicitanteSolicitud> listaBD = this.solicitanteSolicitudService
            .getSolicitantesBySolicitud(solicitudId);

        try {
            /*
             * Se valida que lista de solicitanteSolicituds no este vacia ni nula
             */
            if (!listaBD.isEmpty() && listaBD != null) {
                if (listaSolicitanteSolicituds != null) {
                    for (SolicitanteSolicitud solicitanteSolicitud : listaBD) {
                        if (!solicitanteSolicitud.getRelacion().equals(
                            ESolicitanteSolicitudRelac.CONTACTO.toString())) {
                            /*
                             * Se eliminan los elementos que no estan en la nueva lista pero estan
                             * en la lista que se cargo desde la BD
                             */
                            if (!listaSolicitanteSolicituds
                                .contains(solicitanteSolicitud)) {
                                this.solicitanteSolicitudService
                                    .delete(solicitanteSolicitud);
                            }
                        }
                    }

                    /*
                     * Se actualizan los SolicitanteSolicitud en la BD
                     */
                    for (SolicitanteSolicitud solSol : listaSolicitanteSolicituds) {
                        this.solicitanteSolicitudService.update(solSol);
                    }

                }

            } /*
             * Se asume que si la lista esta vacÃ­a, se debe a que la solicitud es nueva y no tiene
             * solicitanteSolicituds asociados
             */ else {
                if (listaSolicitanteSolicituds != null) {
                    this.solicitanteSolicitudService
                        .persistMultiple(listaSolicitanteSolicituds);
                }
            }

            /*
             * Carga la lista actualizada de solicitanteSolicituds haciendo fetch a los solicitantes
             */
            listaBD = this.solicitanteSolicitudService
                .getSolicitantesBySolicitud2(solicitudId);

        } catch (Exception ex) {
            LOGGER.debug("Ocurrio un error al actualizar lista de solicitanteSolicituds:" +
                ex.getMessage());
        }

        return listaBD;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#buscarDocsFaltantesSolicitudAvaluo(Long, boolean)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<SolicitudDocumentacion> buscarDocsFaltantesSolicitudAvaluo(
        Long idSolicitud, boolean banderaComGenerado) {
        LOGGER.debug("Inicio AvaluosBean#buscarDocsFaltantesSolicitudAvaluo");

        List<SolicitudDocumentacion> listaDocumentosFaltantes =
            new ArrayList<SolicitudDocumentacion>();

        try {
            /* Se busca si hay documentos que ya fueron solicitados para la solicitud de avaluo */
            listaDocumentosFaltantes = this.solicitudDocumentacionService
                .obternerSolicitudesDocumentacionPorSolicitud(idSolicitud,
                    false);

            /* Si la solicitud de avaluo no tiene documentos solicitados, se carga la lista de
             * documentos basicos
             */
            if (listaDocumentosFaltantes.isEmpty()) {
                listaDocumentosFaltantes = this.solicitudDocumentacionService
                    .obtenerListaDocsBasicosDeSolicitudAvaluo();
            }/* Si la solicitud de avaluo tiene documentos solicitados, se carga la
             * lista de documentos basicos y los documentos que ya fueron solicitados
             */ else {
                List<SolicitudDocumentacion> listaDocumentosBasicosFaltantes =
                    this.solicitudDocumentacionService
                        .obtenerListaDocsBasicosDeSolicitudAvaluo();

                for (SolicitudDocumentacion solDocBas : listaDocumentosBasicosFaltantes) {
                    if (!listaDocumentosFaltantes.contains(solDocBas)) {
                        listaDocumentosFaltantes.add(solDocBas);
                    }
                }
            }

        } catch (Exception ex) {
            LOGGER.debug("Ocurrio un error al consultar lista de documentos faltantes:" +
                ex.getMessage());
        }

        LOGGER.debug("Fin AvaluosBean#buscarDocsFaltantesSolicitudAvaluo");
        return listaDocumentosFaltantes;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#buscarDocsFaltantesSolicitudAvaluoComGenerado(Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<SolicitudDocumentacion> buscarDocsFaltantesSolicitudAvaluoComGenerado(
        Long idSolicitud) {
        LOGGER.debug("Inicio AvaluosBean#buscarDocsFaltantesSolicitudAvaluoComGenerado");
        LOGGER.debug("Fin AvaluosBean#buscarDocsFaltantesSolicitudAvaluoComGenerado");
        return null;

    }

//--------------------------------------------------------------------------------------------------	
    /**
     * @see IAvaluosLocal#buscarDocsFaltantesSolicitudAvaluoSinComGenerado(Long)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<SolicitudDocumentacion> buscarDocsFaltantesSolicitudAvaluoSinComGenerado(
        Long idSolicitud) {
        LOGGER.debug("Inicio AvaluosBean#buscarDocsFaltantesSolicitudAvaluoSinComGenerado");
        LOGGER.debug("Fin AvaluosBean#buscarDocsFaltantesSolicitudAvaluoSinComGenerado");
        return null;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#buscarDepartamentosMunicipiosAvaluo(String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<Municipio> buscarDepartamentosMunicipiosAvaluo(Long idAvaluo) {
        LOGGER.debug("Inicio AvaluosBean#buscarDepartamentosMunicipiosAvaluo");

        List<Municipio> listaMunicipios = null;

        try {
            listaMunicipios = this.avaluoPredioService
                .buscarDepartamentosMunicipiosAvaluo(idAvaluo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarDepartamentosMunicipiosAvaluo: " + ex.
                getMensaje());
        }
        LOGGER.debug("Fin AvaluosBean#buscarDepartamentosMunicipiosAvaluo");

        return listaMunicipios;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerInfoEncabezadoAvaluoPorId(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Avaluo obtenerInfoEncabezadoAvaluoPorId(Long avaluoId) {

        Avaluo answer = null;

        try {
            answer = this.avaluoService.getInfoHeaderAvaluoById(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerAvaluoPorId: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerPrediosDeAvaluo(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Predio> obtenerPrediosDeAvaluo(Long avaluoId) {

        List<Predio> answer = null;
        ArrayList<AvaluoPredio> avaluoPredios;
        Predio predioTemp;

        try {
            avaluoPredios = (ArrayList<AvaluoPredio>) this.avaluoPredioService.
                buscarAvaluoPredioPorIdAvaluo(avaluoId);
            if (avaluoPredios != null) {
                answer = new ArrayList<Predio>();
                for (AvaluoPredio ap : avaluoPredios) {
                    predioTemp = this.predioService.getPredioFetchDatosUbicacionById(ap.
                        getPredioId());
                    if (predioTemp != null) {
                        answer.add(predioTemp);
                    }
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerPrediosDeAvaluo: " + ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IAvaluosLocal#buscarAvaluosPorFiltro(co.gov.igac.snc.util.FiltroDatosConsultaAvaluo)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> buscarAvaluosPorFiltro(FiltroDatosConsultaAvaluo filtro) {

        List<Avaluo> resultado = null;

        try {
            resultado = this.avaluoService.buscarAvaluosPorFiltro(filtro);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarAvaluosPorFiltro: " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#buscarContratosPorFiltroSolicitante(
     * co.gov.igac.snc.util.FiltroDatosConsultaSolicitante, java.lang.String, java.lang.String, int)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<ContratoInteradministrativo> buscarContratosInteradministrativosPorFiltros(
        FiltroDatosConsultaSolicitante filtroSolicitante,
        FiltroDatosConsultaContratoInteradministrativo filtroContrato) {

        List<ContratoInteradministrativo> resultado = null;

        try {
            resultado = this.contratoInterAdminService
                .buscarContratosPorFiltro(filtroSolicitante, filtroContrato);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarContratosPorFiltroSolicitante: " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarContratoInteradminsitrativo(ContratoInteradministrativo,
     * Solicitante, UsuarioDTO)
     * @author christian.rodriguez
     */
    @Override
    public ContratoInteradministrativo guardarActualizarContratoInteradminsitrativo(
        ContratoInteradministrativo contrato, Solicitante entidadSolicitante, UsuarioDTO usuario) {

        if (contrato != null) {

            if (contrato.getId() == null) {

                ProfesionalAvaluo interventor = this.profesionalAvaluoService
                    .consultarPorUsuarioSistemaYNumeroIdentificacion(
                        usuario.getLogin(), usuario.getIdentificacion());

                if (interventor != null) {
                    contrato.setUsuarioLog(usuario.getLogin());
                    contrato.setFechaLog(new Date());
                    contrato.setEntidadSolicitante(entidadSolicitante);
                    contrato.setInterventor(interventor);
                    contrato.setFechaTerminacion(contrato.getFechaFin());
                }
            }

            try {

                if (contrato.getInterventor() != null &&
                    contrato.getEntidadSolicitante() != null) {
                    contrato = this.contratoInterAdminService.update(contrato);
                }

            } catch (ExcepcionSNC ex) {
                LOGGER.error("Error en AvaluosBean#guardarActualizarContratoInteradminsitrativo: " +
                    ex.getMensaje());
            }
        }

        return contrato;
    }

    // ------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerContratoInteradministrativoPorIdConEntidad(Long)
     * @author christian.rodriguez
     */
    @Override
    public ContratoInteradministrativo obtenerContratoInteradministrativoPorIdConEntidad(
        Long idContrato) {

        ContratoInteradministrativo contrato = null;

        try {
            contrato = this.contratoInterAdminService
                .buscarPorIdConEntidadSolicitante(idContrato);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarContratoInteradministrativoPorId: " +
                ex.getMensaje());
        }

        return contrato;
    }

    // ------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerContratoInteradministrativoPorIdConEntidad(Long)
     * @author christian.rodriguez
     */
    @Override
    public ContratoInteradministrativo obtenerContratoInteradministrativoPorIdConConsignaciones(
        Long idContrato) {

        ContratoInteradministrativo contrato = null;

        try {
            contrato = this.contratoInterAdminService.buscarPorIdConConsignaciones(idContrato);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarContratoInteradministrativoPorId: " +
                ex.getMensaje());
        }

        return contrato;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarAvaluosDeContrato(Long, FiltroDatosConsultaAvaluo)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosDeContrato(Long idContrato,
        FiltroDatosConsultaAvaluo filtroConsulta) {

        List<Avaluo> answer = null;
        List<Solicitud> solicitudesDeContrato;
        List<Long> idsSolicitudes;

        try {
            solicitudesDeContrato = this.consultarSolicitudesDeContrato(idContrato);
            if (solicitudesDeContrato != null && !solicitudesDeContrato.isEmpty()) {
                idsSolicitudes = new ArrayList<Long>();
                for (Solicitud solicitud : solicitudesDeContrato) {
                    idsSolicitudes.add(solicitud.getId());
                }
                answer = this.avaluoService.buscarPorSolicitudes(
                    idsSolicitudes, filtroConsulta, true);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluosDeContrato: " + ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarSolicitudesDeContrato(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Solicitud> consultarSolicitudesDeContrato(Long idContrato) {

        List<Solicitud> answer = null;

        try {
            answer = this.solicitudService.buscarPorContrato(idContrato);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarSolicitudesDeContrato: " + ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarContratoAdicion(ContratoInteradminisAdicion)
     * @author felipe.cadena
     */
    @Override
    public ContratoInteradminisAdicion guardarActualizarContratoAdicion(
        ContratoInteradminisAdicion adicion) {

        ContratoInteradminisAdicion adicionNueva = null;

        try {

            adicionNueva = this.contratoInteradminisAdicionService.update(adicion);

            // Se agrega las siguientes lineas para actualizar la fecha de
            // terminaciÃ³n del contrato en caso de que la fecha de adiciÃ³n sea
            // mayor a la fecha actual de terminaciÃ³n del contrato
            ContratoInteradministrativo contrato = this.contratoInterAdminService.findById(adicion
                .getContratoInteradminsitrativoId());

            List<ContratoInteradminisAdicion> adiciones = this.contratoInteradminisAdicionService
                .buscarAdicionesPorIdContrato(adicion.getContratoInteradminsitrativoId());

            Date fechaTerminacion = contrato.getFechaFin();

            for (ContratoInteradminisAdicion adicionTemp : adiciones) {
                if (adicionTemp.getFechaFin().after(fechaTerminacion)) {
                    fechaTerminacion = adicionTemp.getFechaFin();
                }
            }

            if (!fechaTerminacion.equals(contrato.getFechaTerminacion())) {
                contrato.setFechaTerminacion(fechaTerminacion);
                this.contratoInterAdminService.update(contrato);
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#guardarActualizarContratoAdicion: " +
                ex.getMensaje());
        }
        return adicionNueva;

    }

    /**
     * @see IAvaluosLocal#buscarAdicionesPorIdContrato(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<ContratoInteradminisAdicion> buscarAdicionesPorIdContrato(Long idContrato) {

        List<ContratoInteradminisAdicion> resultado = null;
        try {
            resultado =
                this.contratoInteradminisAdicionService.buscarAdicionesPorIdContrato(idContrato);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarAdicionesPorIdContrato: " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#buscarAvaluadoresPorFiltro(FiltroDatosConsultaProfesionalAvaluos)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<VProfesionalAvaluosContrato> buscarAvaluadoresPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtroDatos) {

        LOGGER.debug("Inicio AvaluosBean#buscarAvaluadoresPorFiltro");

        List<VProfesionalAvaluosContrato> avaluadores = new ArrayList<VProfesionalAvaluosContrato>();

        try {
            avaluadores = this.vProfesionalAvaluosContratoService
                .buscarAvaluadoresPorFiltro(filtroDatos);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarAvaluadoresPorFiltro: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#buscarAvaluadoresPorFiltro");

        return avaluadores;

    }

    /**
     * @see IAvaluosLocal#calcularTotalAdicionesPorIdContratoInteradministrativo(Long)
     * @author christian.rodriguez
     */
    @Override
    public Double calcularTotalAdicionesPorIdContratoInteradministrativo(Long idContrato) {
        LOGGER.debug("Inicio AvaluosBean#obtenerTotalAdicionesPorIdContrato");

        Double totalAdiciones = null;

        try {
            totalAdiciones = this.contratoInterAdminService
                .obtenerTotalAdicionesPorIdContrato(idContrato);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerTotalAdicionesPorIdContrato: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#obtenerTotalAdicionesPorIdContrato");

        return totalAdiciones;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#calcularValorComprometidoEjecucionPorIdContratoInteradministrativo(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public Double calcularValorComprometidoEjecucionPorIdContratoInteradministrativo(Long idContrato) {
        LOGGER.debug(
            "Inicio AvaluosBean#obtenerValorComprometidoEjecucionPorIdContratoInteradministrativo");

        Double valorComprometido = 0.0;
        Double valorCotizadoTemp, valorCotizadoConIVATemp;
        FiltroDatosConsultaAvaluo filtro;

        filtro = new FiltroDatosConsultaAvaluo();
        filtro.setEstadoTramiteAvaluo(ETramiteEstado.EN_PROCESO.getCodigo());

        try {

            List<Avaluo> avaluos = this.consultarAvaluosDeContrato(idContrato, filtro);

            if (null == avaluos) {
                return valorComprometido;
            }

            for (Avaluo avaluo : avaluos) {
                valorCotizadoTemp = avaluo.getValorCotizacion();

                valorCotizadoConIVATemp = (avaluo.getValorIva() != null) ? valorCotizadoTemp +
                    (valorCotizadoTemp * avaluo.getValorIva().doubleValue()) :
                    valorCotizadoTemp;

                valorComprometido += valorCotizadoConIVATemp;
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en AvaluosBean#obtenerValorComprometidoEjecucionPorIdContratoInteradministrativo: " +
                ex.getMensaje());
        }

        LOGGER.debug(
            "Fin AvaluosBean#obtenerValorComprometidoEjecucionPorIdContratoInteradministrativo");

        return valorComprometido;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#actualizarAvaluo(co.gov.igac.snc.persistence.entity.avaluos.Avaluo)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void actualizarAvaluo(Avaluo avaluo) {

        try {
            this.avaluoService.update(avaluo);
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#actualizarAvaluo: " + ex.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerContratoAvaluadorPorId(Long)
     * @author rodrigo.hernandez
     */
    @Override
    public ProfesionalAvaluosContrato obtenerContratoAvaluadorPorId(Long contratoId) {

        LOGGER.debug("Inicio AvaluosBean#obtenerContratoAvaluadorPorId");

        ProfesionalAvaluosContrato contrato = null;

        try {
            contrato = this.profesionalAvaluosContratoService
                .findById(contratoId);
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#obtenerContratoAvaluadorPorId: " +
                ex.getMessage());
        }

        LOGGER.debug("Fin AvaluosBean#obtenerContratoAvaluadorPorId");

        return contrato;

    }

    /**
     * @see IAvaluosLocal#buscarAdicionesPorIdProfesionalContrato(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<ProfesionalContratoAdicion> buscarAdicionesPorIdProfesionalContrato(Long idContrato) {

        List<ProfesionalContratoAdicion> resultado = null;
        try {
            resultado = this.profesionalContratoAdicionService.buscarAdicionesPorIdContrato(
                idContrato);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#buscarAdicionesPorIdProfesionalContrato: " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#obtenerTotalAdicionesPorIdProfesionalContrato(Long)
     * @author felipe.cadena
     *
     * @modified rodrigo.hernandez - adiciÃ³n de comentario y control de valor a retornar </br>
     *
     * <b>OJO:</b> Este mÃ©todo se usa cuando no es necesario hacer fetch de las adiciones de un
     * contrato.
     */
    @Override
    public Double obtenerTotalAdicionesPorIdProfesionalContrato(Long idContrato) {

        Double totalAdiciones = 0D;

        try {
            totalAdiciones = this.profesionalContratoAdicionService
                .obtenerTotalAdicionesPorIdContrato(idContrato);

            /*
             * Se controla que si no encuentra el valor dentro de la consulta, retorne cero
             */
            if (totalAdiciones == null) {
                totalAdiciones = 0D;
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerTotalAdicionesPorIdProfesionalContrato: " +
                ex.getMensaje());
        }

        return totalAdiciones;
    }

    /**
     * @see IAvaluosLocal#obtenerFechaFinalPAContratoAdiciones(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public Date calcularFechaFinalPAContratoAdiciones(Long idContrato) {

        LOGGER.debug("Inicio AvaluosBean#calcularFechaFinalPAContratoAdiciones");

        Date fechaFinal = null;

        try {
            fechaFinal = this.profesionalContratoAdicionService
                .obtenerFechaFinalPAContratoAdiciones(idContrato);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#calcularFechaFinalPAContratoAdiciones: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#obtenerFechaFinalPAContratoAdiciones");

        return fechaFinal;
    }

    /**
     * @see IAvaluosLocal#calcularValorEjecutadoPAContrato(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public Double calcularValorEjecutadoPAContrato(Long idContrato) {
        LOGGER.debug("Inicio AvaluosBean#calcularValorEjecutadoPAContrato");

        Double valor = 0D;

        try {
            valor = this.avaluoLiquidacionCostoService
                .calcularValorEjecutadoAvaluosAprobadosPorContratoId(idContrato);

            if (valor == null) {
                valor = 0D;
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerValorEjecutadoPAContrato: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#calcularValorEjecutadoPAContrato");

        return valor;

    }

    /**
     * @see IAvaluosLocal#obtenerCesionesDeContratoActivo(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<ProfesionalContratoCesion> obtenerCesionesDeContratoActivo(
        Long idContrato) {
        LOGGER.debug("Inicio AvaluosBean#obtenerCesionesDeContratoActivo");

        List<ProfesionalContratoCesion> result = null;

        try {

            result = this.profesionalContratoCesionService
                .obtenerCesionesDeContratoActivo(idContrato);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerCesionesDeContratoActivo: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#obtenerCesionesDeContratoActivo");

        return result;

    }

    /**
     * @see IAvaluosLocal#consultarAvaluosEnProcesoAvaluador(Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosEnProcesoAvaluador(Long idAvaluador) {
        List<Avaluo> resultado = null;

        try {
            resultado = this.avaluoService
                .consultarAvaluosEnProcesoAvaluador(idAvaluador);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarAvaluosEnProcesoAvaluador: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#consultarAvaluosEnProcesoAvaluador");

        return resultado;
    }

    // ------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarPagosParafiscalesPorProfesionalAvaluoContratoId(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<ProfesionalContratoPago> consultarPagosParafiscalesPorProfesionalAvaluoContratoId(
        Long idProfesionalAvaluoContrato) {

        LOGGER.debug("en AvaluosBean#consultarPagosParafiscalesPorProfesionalAvaluoContratoId");
        List<ProfesionalContratoPago> answer = null;

        try {
            answer = this.profesionalContratoPagoService
                .consultarDePensionSaludPorIdContrato(idProfesionalAvaluoContrato);
        } catch (Exception ex) {
            LOGGER.error(
                "error en AvaluosBean#consultarPagosParafiscalesPorProfesionalAvaluoContratoId: " +
                ex.getMessage());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerProfesionalAvaluoPorId(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ProfesionalAvaluo obtenerProfesionalAvaluoPorId(Long profesionalAvaluoId) {

        LOGGER.debug("en AvaluosBean#obtenerProfesionalAvaluoPorId");
        ProfesionalAvaluo answer = null;

        try {
            answer = this.profesionalAvaluoService.obtenerByIdConAtributos(profesionalAvaluoId);
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#obtenerProfesionalAvaluoPorId: " + ex.getMessage());
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerProfesionalAvaluoPorIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public ProfesionalAvaluo obtenerProfesionalAvaluoPorIdConAtributos(Long profesionalAvaluoId) {

        LOGGER.debug("en AvaluosBean#obtenerProfesionalAvaluoPorIdConAtributos");
        ProfesionalAvaluo answer = null;

        try {
            answer = this.profesionalAvaluoService.obtenerByIdConAtributos(profesionalAvaluoId);
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#obtenerProfesionalAvaluoPorIdConAtributos: " +
                ex.getMessage());
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerProfesionalAvaluoContratoPorIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public ProfesionalAvaluosContrato obtenerProfesionalAvaluoContratoPorIdConAtributos(
        Long contratoId) {

        LOGGER.debug("en AvaluosBean#obtenerProfesionalAvaluoContratoPorIdConAtributos");
        ProfesionalAvaluosContrato contrato = null;

        try {
            contrato = this.profesionalAvaluosContratoService.obtenerPorIdConAtributos(contratoId);
        } catch (Exception ex) {
            LOGGER.error(
                "error en AvaluosBean#obtenerProfesionalAvaluoContratoPorIdConAtributos: " +
                ex.getMessage());
        }

        return contrato;
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @see IProfesionalAvaluoDAO#obtenerByIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<ProfesionalAvaluosContrato> obtenerProfesionalAvaluosContratoAnterioresPorIdProfesional(
        Long idProfesional) {
        LOGGER.debug("en AvaluosBean#obtenerProfesionalAvaluosContratoAnterioresPorIdProfesional");

        List<ProfesionalAvaluosContrato> answer = null;

        try {
            answer = this.profesionalAvaluosContratoService
                .consultarAnterioresPorIdProfesional(idProfesional);
        } catch (Exception ex) {
            LOGGER.error(
                "error en AvaluosBean#obtenerProfesionalAvaluosContratoAnterioresPorIdProfesional: " +
                ex.getMessage());
        }

        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see
     * IAvaluosLocal#actualizarProfesionalAvaluos(co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void actualizarProfesionalAvaluos(ProfesionalAvaluo avaluador) {

        LOGGER.debug("en AvaluosBean#actualizarProfesionalAvaluos ");

        String direccionNormalizada, direccionAvaluador;

        direccionAvaluador = avaluador.getDireccion();
        try {

            //D: primero se normaliza la direcciÃ³n
            direccionNormalizada = QueryNativoDAO.normalizarDireccion(
                this.profesionalAvaluoService.getEntityManager(), direccionAvaluador);
            if (direccionNormalizada != null && !direccionNormalizada.isEmpty()) {
                avaluador.setDireccion(direccionNormalizada);
            }

            this.profesionalAvaluoService.update(avaluador);
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#actualizarProfesionalAvaluos: " + ex.getMessage());
        }

    }

    //---------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#actualizarProfesionalAvaluos(ProfesionalAvaluo, String, UsuarioDTO)
     * @author christian.rodriguez
     */
    @Override
    public ProfesionalAvaluo actualizarProfesionalAvaluos(ProfesionalAvaluo avaluador,
        String idProfesion, UsuarioDTO usuario) {

        if (idProfesion != null && avaluador != null) {

            Profesion profesion = this.profesionService.findById(Long
                .valueOf(idProfesion));

            if (profesion != null) {
                avaluador.setProfesion(profesion);

                String direccionNormalizada, direccionAvaluador;

                direccionAvaluador = avaluador.getDireccion();
                try {

                    // se normaliza la direcciÃ³n
                    direccionNormalizada = QueryNativoDAO.normalizarDireccion(
                        this.profesionalAvaluoService.getEntityManager(),
                        direccionAvaluador);

                    if (direccionNormalizada != null &&
                        !direccionNormalizada.isEmpty()) {
                        avaluador.setDireccion(direccionNormalizada);
                    }

                    if (avaluador.getId() == null) {
                        avaluador.setFechaLog(new Date());
                        avaluador.setUsuarioLog(usuario.getLogin());
                    }

                    avaluador = this.profesionalAvaluoService.update(avaluador);

                } catch (Exception ex) {
                    LOGGER.error("error en AvaluosBean#actualizarProfesionalAvaluos: " +
                        ex.getMessage());
                }

            }
        }
        return avaluador;
    }

    /**
     * @see IAvaluosLocal#cancelarAvaluo
     * @author felipe.cadena
     */
    @Override
    public Avaluo cancelarAvaluo(Avaluo avaluo, String justificacion, String otraJustificacion,
        UsuarioDTO usuario) {

        LOGGER.debug("en AvaluosBean#cancelarAvaluo");

        AvaluoMovimiento registroCancelacion = new AvaluoMovimiento();
        registroCancelacion.setJustificacion(justificacion);
        registroCancelacion.setAvaluo(avaluo);
        registroCancelacion.setFecha(new Date());
        registroCancelacion.setDiasAmpliacion(0l);
        registroCancelacion.setUsuarioMovimientoId(usuario.getLogin());
        registroCancelacion.setTipoMovimiento(EMovimientoAvaluoTipo.CANCELACION.getCodigo());
        registroCancelacion.setFechaLog(new Date());
        registroCancelacion.setUsuarioLog(usuario.getLogin());
        if (otraJustificacion != null) {
            registroCancelacion.setOtraJustificacion(otraJustificacion);
        }

        TramiteEstado te = new TramiteEstado(ETramiteEstado.CANCELADO.getCodigo(), avaluo.
            getTramite(), usuario.getLogin());
        te = this.tramiteEstadoService.update(te);
        //avaluo.getTramite().setEstado(ETramiteEstado.CANCELADO.getCodigo());

        try {
            avaluo.getTramite().setTramiteEstado(te);
            this.avaluoMovimientoService.update(registroCancelacion);
            avaluo.setTramite(this.tramiteService.update(avaluo.getTramite()));
        } catch (Exception ex) {
            LOGGER.error("Error en AvaluosBean#cancelarAvaluo: " +
                ex.getCause());
        }

        return avaluo;
    }

    /**
     * @see IAvaluosLocal#consultarProfesionalAvaluoContratoPagosCausadosPorIdContrato(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<ProfesionalContratoPago> consultarProfesionalAvaluoContratoPagosCausadosPorIdContrato(
        Long idContrato) {

        LOGGER.debug("en AvaluosBean#consultarProfesionalAvaluoContratoPagosCausadosPorIdContrato");

        List<ProfesionalContratoPago> pagosCausados = null;

        try {
            pagosCausados = this.profesionalContratoPagoService
                .consultarCausadosPorIdContrato(idContrato);
        } catch (Exception ex) {
            LOGGER.error(
                "error en AvaluosBean#consultarProfesionalAvaluoContratoPagosCausadosPorIdContrato: " +
                ex.getMessage());
        }

        return pagosCausados;
    }

    /**
     * @see IAvaluosLocal#consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<ProfesionalContratoPago> consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato(
        Long idContrato) {

        LOGGER.debug(
            "en AvaluosBean#consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato");

        List<ProfesionalContratoPago> pagosProyectados = null;

        try {
            pagosProyectados = this.profesionalContratoPagoService
                .consultarProyectadosPorIdContrato(idContrato);
        } catch (Exception ex) {
            LOGGER.error(
                "error en AvaluosBean#consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato: " +
                ex.getMessage());
        }

        return pagosProyectados;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluadoresPorSecRadicado(String)
     * @author christian.rodriguez
     */
    @Override
    public List<ProfesionalAvaluo> consultarAvaluadoresPorSecRadicado(String secRadicado) {
        LOGGER.debug(
            "en AvaluosBean#consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato");

        List<ProfesionalAvaluo> avaluadores = null;

        try {
            avaluadores = this.profesionalAvaluoService.consultarPorSecRadicadoYActividad(
                secRadicado, EAvaluoAsignacionProfActi.AVALUAR.getCodigo());
        } catch (Exception ex) {
            LOGGER.error(
                "error en AvaluosBean#consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato: " +
                ex.getMessage());
        }

        return avaluadores;
    }

    /**
     * @see IAvaluosLocal#buscarProfesionalesAvaluosPorNombre(String)
     * @author felipe.cadena
     */
    @Override
    public List<VProfesionalAvaluosCarga> buscarProfesionalesAvaluosPorNombre(
        String nombreProfesional,
        String codigoTerritorial) {
        LOGGER.debug("en AvaluosBean#buscarProfesionalesAvaluosPorNombre");

        List<VProfesionalAvaluosCarga> resultado = null;

        try {
            resultado = this.vProfesionalAvaluosCargaService.
                buscarCargaAvaluadoresPorNombreYterritorial(nombreProfesional, codigoTerritorial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#buscarProfesionalesAvaluosPorNombre: " +
                ex.getMessage());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluosRealizadosContratoPA(Long, String, String)
     * @author felipe.cadena
     */
    @Override
    public List<Avaluo> consultarAvaluosRealizadosContratoPA(Long idContrato, String tipoActividad,
        String estadoTramite) {
        LOGGER.debug("en AvaluosBean#consultarAvaluosRealizadosContratoPA");

        List<Avaluo> resultado = null;

        try {
            resultado = this.avaluoService.
                consultarAvaluosRealizadosContratoPA(idContrato, tipoActividad, estadoTramite);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluosRealizadosContratoPA: " +
                ex.getMessage());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluosProfesionalPorAnio(Long, String, String, Integer)
     * @author felipe.cadena
     */
    @Override
    public List<Avaluo> consultarAvaluosProfesionalPorAnio(Long idAvaluador, String tipoActividad,
        String estadoTramite, Integer anio) {
        LOGGER.debug("en AvaluosBean#consultarAvaluosProfesionalPorAnio");

        List<Avaluo> resultado = null;

        try {
            resultado = this.avaluoService.
                consultarAvaluosProfesionalPorAnio(idAvaluador, tipoActividad, estadoTramite, anio);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluosProfesionalPorAnio: " +
                ex.getMessage());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#consultarHistoricoAvaluosProfesional(Long)
     * @author felipe.cadena
     */
    @Override
    public List<Object[]> consultarHistoricoAvaluosProfesional(Long idAvaluador) {
        LOGGER.debug("en AvaluosBean#consultarHistoricoAvaluosProfesional");

        List<Object[]> resultado = null;

        try {
            resultado = this.avaluoService.
                consultarHistoricoAvaluos(idAvaluador);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarHistoricoAvaluosProfesional " +
                ex.getMessage());
        }

        return resultado;
    }

    // ----------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#buscarAreasCapturaOfertaSinComisionar(String,String)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosSinPagoPorContratoYAvaluador(
        Long idContrato, Long idAvaluador) {

        LOGGER.debug("on AvaluosBean#consultarAvaluosSinPagoPorContratoYAvaluador ...");

        List<Avaluo> answer = null;
        answer = this.avaluoService.consultarSinPagoPorAvaluadorYContrato(
            idContrato, idAvaluador);

        LOGGER.debug("... finished AvaluosBean#consultarAvaluosSinPagoPorContratoYAvaluador");

        return answer;
    }

    // ----------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarActualizarProfesionalContratoPago(ProfesionalContratoPago,
     * ProfesionalAvaluosContrato, List, UsuarioDTO)
     * @author christian.rodriguez
     */
    @Override
    public ProfesionalContratoPago guardarActualizarProfesionalContratoPago(
        ProfesionalContratoPago pago, ProfesionalAvaluosContrato contrato,
        List<Avaluo> avaluos, UsuarioDTO usuario) {
        LOGGER.debug("on AvaluosBean#guardarActualizarProfesionalContratoPago ...");

        try {

            if (pago.getId() == null) {
                Long consecutivoPago = this.profesionalContratoPagoService
                    .calcularSiguienteSecPago(contrato.getId());

                pago.setConsecutivoPago(consecutivoPago);
                pago.setUsuarioLog(usuario.getLogin());
                pago.setFechaLog(new Date());
                pago.setEstado(EProfesionalContratoPagoEstado.PROYECTADO
                    .getCodigo());
                pago.setProfesionalAvaluosContrato(contrato);

                pago = this.profesionalContratoPagoService.update(pago);

            } else {

                pago = this.profesionalContratoPagoService
                    .obtenerContratoPagoAvaluosPorId(pago.getId());

                this.desasociarAvaluosDeProfesionalContratoPago(pago, avaluos);

            }

            this.asociarAvaluosAProfesionalContratoPago(pago, avaluos, usuario);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error al guardar/actualizar el profesional contrato pago: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#guardarActualizarProfesionalContratoPago");

        return pago;
    }

    // -----------------------------------------------------------------------------------
    /**
     * MÃ©todo para asociar avaluos ({@link Avaluo}) a un pago de profesional de avaluos
     * ({@link ProfesionalAvaluosContrato})
     *
     * @author christian.rodriguez
     * @param pago pago al que se le asociaran los avaluos
     * @param avaluos lista actual de los avlauos del pago con los que se van a asociar
     * @param usuario usuario que asocia los avaluos
     */
    private void asociarAvaluosAProfesionalContratoPago(
        ProfesionalContratoPago pago, List<Avaluo> avaluos,
        UsuarioDTO usuario) {

        if (pago != null && avaluos != null && !avaluos.isEmpty()) {

            List<ContratoPagoAvaluo> listaVieja = pago.getContratoPagoAvaluos();

            List<Avaluo> listaViejaAvaluos = new ArrayList<Avaluo>();

            for (ContratoPagoAvaluo contratoPagoAvaluo : listaVieja) {
                listaViejaAvaluos.add(contratoPagoAvaluo.getAvaluo());
            }

            for (Avaluo avaluo : avaluos) {
                if (listaViejaAvaluos.indexOf(avaluo) == -1) {

                    ContratoPagoAvaluo cpa = new ContratoPagoAvaluo();
                    cpa.setAvaluo(avaluo);
                    cpa.setFechaLog(new Date());
                    cpa.setProfesionalContratoPago(pago);
                    cpa.setUsuarioLog(usuario.getLogin());

                    this.contratoPagoAvaluoService.update(cpa);
                }
            }
        }
    }

    // -----------------------------------------------------------------------------------
    /**
     * MÃ©todo para desasociar avaluos ({@link Avaluo}) de un pago de profesional de avaluos
     * ({@link ProfesionalAvaluosContrato})
     *
     * @author christian.rodriguez
     * @param pago pago al que se le desasociaran los avaluos
     * @param avaluos lista actual de los avlauos del pago con los que se van a desasociar
     */
    private void desasociarAvaluosDeProfesionalContratoPago(
        ProfesionalContratoPago pago, List<Avaluo> avaluos) {

        if (pago != null && avaluos != null && !avaluos.isEmpty()) {

            List<ContratoPagoAvaluo> listaVieja = pago.getContratoPagoAvaluos();

            for (ContratoPagoAvaluo contratoPagoAvaluo : listaVieja) {
                if (avaluos.indexOf(contratoPagoAvaluo.getAvaluo()) == -1) {
                    this.contratoPagoAvaluoService.delete(contratoPagoAvaluo);
                }
            }
        }
    }

    // ----------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarAvaluosPorProfesionalContratoPagoId(Long)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosPorProfesionalContratoPagoId(
        Long profesionalContratoPagoId) {

        LOGGER.debug("on AvaluosBean#consultarAvaluosPorProfesionalContratoPagoId ...");

        List<Avaluo> answer = null;
        try {
            answer = this.avaluoService
                .consultarPorProfesionalContratoPagoId(profesionalContratoPagoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error on AvaluosBean#consultarAvaluosPorProfesionalContratoPagoId: " +
                ex.getMensaje());
        }
        LOGGER.debug("... finished AvaluosBean#consultarAvaluosPorProfesionalContratoPagoId");

        return answer;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluosAsignacion(Long, String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosAsignacion(Long idProfesional,
        String secRadicado) {

        LOGGER.debug("on AvaluosBean#consultarAvaluosAsignacion ...");

        List<Avaluo> resultado = null;
        try {
            resultado = this.avaluoService
                .consultarAvaluosAsignacion(idProfesional, secRadicado);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluosAsignacion " +
                ex.getMessage());
        }

        return resultado;

    }

    /**
     * @see IAvaluosLocal#desasignarAvaluador(Avaluo, Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public AvaluoAsignacionProfesional desasignarAvaluador(Avaluo avaluo, Long idAvaluador) {

        LOGGER.debug("on AvaluosBean#desasignarAvaluador ...");

        for (AvaluoAsignacionProfesional aap : avaluo.getAvaluoAsignacionProfesionals()) {
            if (aap.getProfesionalAvaluo().getId() == idAvaluador) {
                try {
                    this.avaluoAsignacionProfesionalService.delete(aap);
                } catch (ExcepcionSNC ex) {
                    LOGGER.error("Error en AvaluosBean#desasignarAvaluador: " +
                        ex.getMensaje());
                }

                return aap;
            }
        }
        return null;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluosAsigancionProfesional(Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<ProfesionalAvaluo> buscarAvaluadoresPorTerritorial(String codigoTerritorial) {

        LOGGER.debug("on AvaluosBean#buscarAvaluadoresPorTerritorial ...");

        List<ProfesionalAvaluo> resultado = null;
        try {
            resultado = this.profesionalAvaluoService.
                buscarProfesionalesPorNombreYterritorial(null, codigoTerritorial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#buscarAvaluadoresPorTerritorial " +
                ex.getMessage());
        }
        return resultado;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerAvaluosPorIdsTramite(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> obtenerAvaluosPorIdsTramite(List<Long> idsTramites) {

        LOGGER.debug("on AvaluosBean#obtenerAvaluosConAvaluadoresYPrediosPorIdsTramite");
        List<Avaluo> answer = null;

        try {
            answer = this.avaluoService.obtenerPorIdsTramite(idsTramites);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error on AvaluosBean#obtenerAvaluosConAvaluadoresYPrediosPorIdsTramite: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#obtenerAvaluosConAvaluadoresYPrediosPorIdsTramite");
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluos#consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica(String)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public List<AvaluoAsignacionProfesional> consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica(
        String radicado) {

        LOGGER.debug(
            "on AvaluosBean#consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica");
        List<AvaluoAsignacionProfesional> asignaciones = null;

        try {
            asignaciones = this.avaluoAsignacionProfesionalService
                .consultarPorSolicitudConOrdenPractica(radicado);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error on AvaluosBean#consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica: " +
                ex.getMensaje());
        }

        LOGGER.debug(
            "... finished AvaluosBean#consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica");
        return asignaciones;
    }

    /**
     * @see IAvaluosLocal#buscarSolicitudesPorSolicitante(FiltroDatosSolicitud)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<Solicitud> buscarSolicitudesPorSolicitante(
        FiltroDatosSolicitud filtroDatosSolicitud) {
        List<Solicitud> result = new ArrayList<Solicitud>();

        try {
            result = this.solicitudService
                .buscarSolicitudesPorSolicitante(filtroDatosSolicitud);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error on AvaluosBean#buscarSolicitudesPorSolicitante: " +
                ex.getMensaje());
        }

        return result;
    }

    // --------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#buscarProfesionalesAvaluosPorFiltro(FiltroDatosConsultaProfesionalAvaluos)
     * @author christian.rodriguez
     */
    @Override
    public List<ProfesionalAvaluo> buscarProfesionalesAvaluosPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtro) {

        List<ProfesionalAvaluo> profesionales = new ArrayList<ProfesionalAvaluo>();

        try {
            profesionales = this.profesionalAvaluoService
                .buscarPorFiltro(filtro);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error on AvaluosBean#buscarProfesionalesAvaluosPorFiltro: " +
                ex.getMensaje());
        }

        return profesionales;
    }

    // ------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#eliminarProfesionalAvaluos(ProfesionalAvaluo)
     * @author christian.rodriguez
     */
    @Override
    public boolean eliminarProfesionalAvaluos(ProfesionalAvaluo profesional) {
        LOGGER.debug("Iniciando AvaluosBean#eliminarPrediosTramite");
        try {

            List<AvaluoAsignacionProfesional> asignaciones = this.avaluoAsignacionProfesionalService
                .consultarPorProfesionalId(profesional.getId());

            if (asignaciones == null || asignaciones.isEmpty()) {
                this.profesionalAvaluoService.delete(profesional);
                return true;
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#eliminarPrediosTramite: " +
                ex.getMensaje());
        }
        return false;
    }

    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#crearActualizarControlCalidadAvaluo(ControlCalidadAvaluo, UsuarioDTO)
     * @author felipe.cadena
     */
    @Override
    public ControlCalidadAvaluo crearActualizarControlCalidadAvaluo(
        ControlCalidadAvaluo ccAvaluo, UsuarioDTO usuario) {

        Boolean banderaEdicion = true;
        LOGGER.debug("Iniciando AvaluosBean#crearActualizarControlCalidadAvaluo");
        try {

            if (ccAvaluo != null) {
                if (ccAvaluo.getId() == null) {
                    ccAvaluo.setUsuarioLog(usuario.getLogin());
                    ccAvaluo.setFechaLog(new Date());
                }
                for (ControlCalidadAvaluoRev ccar : ccAvaluo.getControlCalidadAvaluoRevs()) {
                    if (ccar.getId() == null) {
                        ccar.setUsuarioLog(usuario.getLogin());
                        ccar.setFechaLog(new Date());
                    }
                    for (ControlCalidadEspecificacion cce : ccar.getControlCalidadEspecificacions()) {
                        if (cce.getId() == null) {
                            cce.setUsuarioLog(usuario.getLogin());
                            cce.setFechaLog(new Date());
                            banderaEdicion = false;
                        }
                    }
                }
            }

            ccAvaluo = this.controlCalidadAvaluoDAOService.update(ccAvaluo);
            LOGGER.debug("Control guardado");
            //Se crea una asignacion para el control de calidad del avaluo por parte del coodinador del GIT
            if (ccAvaluo.getActividad().
                equals(EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo()) &&
                ccAvaluo.getControlCalidadAvaluoRevs().size() > 2 && !banderaEdicion) {

                if (ccAvaluo.getAprobo().equals(ESiNo.SI.getCodigo())) {
                    Avaluo ava = new Avaluo();
                    ava.setId(ccAvaluo.getAvaluoId());

                    //TODO::felipe.cadena::definir datos de la asignacion que corresponde al Coordinador GIT
                    ProfesionalAvaluo pa = new ProfesionalAvaluo();
                    pa.setId(6L);

                    AvaluoAsignacionProfesional aap = new AvaluoAsignacionProfesional();
                    aap.setActividad(EAvaluoAsignacionProfActi.CC_GIT.getCodigo());
                    aap.setAvaluo(ava);
                    aap.setFechaDesde(new Date());
                    Asignacion asig = new Asignacion();
                    asig.setId(1L);
                    aap.setAsignacion(asig);
                    aap.setFechaHasta(new Date());
                    aap.setAceptada(ESiNo.SI.getCodigo());

                    aap.setProfesionalAvaluo(pa);
                    aap.setUsuarioLog(usuario.getLogin());
                    aap.setFechaLog(new Date());
                    LOGGER.debug("Creando asigancion CGIT...");
                    this.avaluoAsignacionProfesionalService.update(aap);
                } else {
                    //TODO::felipe.cadena::se debe mover el avalÃºo a la tarea de reasignar avaluador
                    //cuando no se aprobo en tercera revisiÃ³n de sede central
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#crearActualizarControlCalidadAvaluo: " +
                ex.getMensaje());
        }
        return ccAvaluo;
    }

    /**
     * @see IAvaluosLocal#obtenerControlCalidadAvaluoRevPorId(Long)
     * @author christian.rodriguez
     */
    @Override
    public ControlCalidadAvaluoRev obtenerControlCalidadAvaluoRevPorId(
        Long idControlCalidadAvaluoRev) {

        LOGGER.debug("Iniciando AvaluosBean#obtenerControlCalidadAvaluoRevPorId");

        ControlCalidadAvaluoRev revision = null;

        try {
            revision = this.controlCalidadAvaluoRevDAOService
                .obtenerPorIdConAtributos(idControlCalidadAvaluoRev);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerControlCalidadAvaluoRevPorId: " +
                ex.getMensaje());
        }
        return revision;
    }

    /**
     * @see IAvaluosLocal#obtenerAvaluoPorIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo obtenerAvaluoPorIdConAtributos(Long idAvaluo) {
        LOGGER.debug("Iniciando AvaluosBean#obtenerAvaluoPorIdConAtributos");

        Avaluo avaluo = null;

        try {
            avaluo = this.avaluoService.obtenerPorIdConAtributos(idAvaluo);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerAvaluoPorIdConAtributos: " +
                ex.getMensaje());
        }
        return avaluo;
    }

    /**
     * @see IAvaluosLocal#consultarAvaluoPorSecRadicadoConAtributos(String)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo consultarAvaluoPorSecRadicadoConAtributos(String secRadicado) {
        LOGGER.debug("Iniciando AvaluosBean#consultarAvaluoPorSecRadicadoConAtributos");

        Avaluo avaluo = null;

        try {
            avaluo = this.avaluoService
                .consultarPorSecRadicadoConAtributos(secRadicado);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarAvaluoPorSecRadicadoConAtributos: " +
                ex.getMensaje());
        }
        return avaluo;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerAvaluosPorIdsParaOrdenPractica(long[], String, String, Map, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> obtenerAvaluosPorIdsParaOrdenPractica(long[] idsTramites, String sortField,
        String sortOrder, Map<String, String> filters, int... contadoresDesdeYCuantos) {

        LOGGER.debug("en AvaluosBean#obtenerAvaluosPorIdsParaOrdenPractica");

        List<Avaluo> answer = null;

        try {
            answer = this.avaluoService.obtenerParaOrdenPractica(
                idsTramites, sortField, sortOrder, filters, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#obtenerAvaluosPorIdsParaOrdenPractica: " +
                ex.getMensaje());
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerProfesionaAvaluosPorIdentificacion(Long)
     * @author christian.rodriguez
     */
    @Override
    public ProfesionalAvaluo obtenerProfesionaAvaluosPorIdentificacion(String identificacion) {
        LOGGER.debug("Iniciando AvaluosBean#obtenerProfesionaAvaluosPorIdentificacion");

        ProfesionalAvaluo pa = null;

        try {
            pa = this.profesionalAvaluoService.obtenerPorIdentificacion(identificacion);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerProfesionaAvaluosPorIdentificacion: " +
                ex.getMensaje());
        }
        return pa;
    }

    /**
     * @see IAvaluosLocal#obtenerControlCalidadAvaluoPorId(Long)
     * @author felipe.cadena
     */
    @Override
    public ControlCalidadAvaluo obtenerControlCalidadAvaluoPorId(Long IdControlCalidad) {
        LOGGER.debug("Iniciando AvaluosBean#obtenerControlCalidadAvaluoPorId");

        ControlCalidadAvaluo cca = null;

        try {
            cca = this.controlCalidadAvaluoDAOService.obtenerPorIdConAtributos(IdControlCalidad);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerControlCalidadAvaluoPorId: " +
                ex.getMensaje());
        }
        return cca;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerContratoInteradministrativoPorAvaluo(java.lang.Long)
     * @author pedro.garcia
     */
    @Override
    public ContratoInteradministrativo obtenerContratoInteradministrativoPorAvaluo(Long idAvaluo) {

        LOGGER.debug("en AvaluosBean#obtenerContratoInteradministrativoPorAvaluo ");

        ContratoInteradministrativo answer = null;
        Avaluo avaluo;
        Long contratoId;

        try {
            avaluo = this.avaluoService.findById(idAvaluo);
            contratoId = avaluo.getTramite().getSolicitud().getContratoInteradministrativId();

            //D: puede que la solicitud no estÃ© atada a un contrato
            if (contratoId != null) {
                answer = this.contratoInterAdminService.obtenerPorIdConFetching(contratoId);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerContratoInteradministrativoPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#guardarActualizarAvaluoMovimiento(AvaluoMovimiento, UsuarioDTO))
     * @author christian.rodriguez
     */
    @Override
    public AvaluoMovimiento guardarActualizarAvaluoMovimiento(
        AvaluoMovimiento movimiento, UsuarioDTO usuario) {
        LOGGER.debug("on AvaluosBean#guardarActualizarAvaluoMovimiento ...");

        try {

            if (movimiento.getId() == null) {
                movimiento.setUsuarioLog(usuario.getLogin());
                movimiento.setFechaLog(new Date());
            }

            movimiento = this.avaluoMovimientoService.update(movimiento);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error al guardar/actualizar el movimiento: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#guardarActualizarAvaluoMovimiento");

        return movimiento;
    }

    /**
     * @see IAvaluosLocal#enviarCorreoElectronicoSolicitudMovimientoAvaluo(AvaluoMovimiento,
     * UsuarioDTO)
     * @author christian.rodriguez
     */
    @Override
    public boolean enviarCorreoElectronicoSolicitudMovimientoAvaluo(UsuarioDTO destinatario,
        AvaluoMovimiento movimiento, UsuarioDTO usuarioEjecutor) {

        Object[] datosCorreo = new Object[8];
        boolean envioExitoso = false;

        Avaluo avaluo = this.avaluoService.obtenerPorIdConAtributos(movimiento
            .getAvaluo().getId());

        datosCorreo[0] = usuarioEjecutor.getNombreCompleto();
        datosCorreo[1] = avaluo.getSecRadicado();
        datosCorreo[2] = movimiento.getJustificacion();
        datosCorreo[3] = movimiento.getOtraJustificacion();

        EPlantilla plantillaCorreo = null;

        // Mensaje de correo cuando es solicitud de suspenciÃ³n
        if (movimiento.getTipoMovimiento().equalsIgnoreCase(
            EMovimientoAvaluoTipo.SUSPENCION.getCodigo())) {

            if (movimiento.getFechaDesde() == null) {
                datosCorreo[4] = movimiento.getFecha();
            } else {
                datosCorreo[4] = movimiento.getFechaDesde();
            }

            if (movimiento.getFechaHasta() == null) {
                datosCorreo[5] = "Indefinido";
            } else {
                datosCorreo[5] = movimiento.getFechaHasta();
            }

            plantillaCorreo = EPlantilla.MENSAJE_COMUNICACION_SOLICITUD_SUSPENCION_AVALUO;

            // Mensaje de correo cuando es solicitud de cancelaciÃ³n
        } else if (movimiento.getTipoMovimiento().equalsIgnoreCase(
            EMovimientoAvaluoTipo.CANCELACION.getCodigo())) {

            plantillaCorreo = EPlantilla.MENSAJE_COMUNICACION_SOLICITUD_CANCELACION_AVALUO;

            // Mensaje de correo cuando es solicitud de ampliaciÃ³n de tiempos
        } else if (movimiento.getTipoMovimiento().equalsIgnoreCase(
            EMovimientoAvaluoTipo.AMPLIACION.getCodigo())) {

            datosCorreo[4] = movimiento.getDiasAmpliacion();
            plantillaCorreo = EPlantilla.MENSAJE_COMUNICACION_SOLICITUD_AMPLIACION_AVALUO;
        }

        // Destinatarios normales
        List<String> destinatarios = new ArrayList<String>();
        destinatarios.add(destinatario.getLogin().concat(
            EDatosBaseCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC
                .getDato()));

        // Destinatarios CC
        List<String> destinatariosCC = new ArrayList<String>();
        ProfesionalAvaluo ejecutorTramite = this
            .consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion(
                usuarioEjecutor.getLogin(),
                usuarioEjecutor.getIdentificacion());
        destinatariosCC.add(ejecutorTramite.getCorreoElectronico());

        // Envio notificaciÃ³n
        envioExitoso = this.generalesService.enviarCorreoElectronicoConCuerpo(
            plantillaCorreo, destinatarios, destinatariosCC, null, datosCorreo, null,
            null);

        return envioExitoso;
    }

    /**
     * @see IAvaluosLocal#obtenerDetallesDocumentosFaltantesPorIdSolicitud(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<DetalleDocumentoFaltante> obtenerDetallesDocumentosFaltantesPorIdSolicitud(
        Long idSolicitud) {

        LOGGER.debug("Iniciando AvaluosBean#obtenerDetallesDocumentosFaltantesPorIdSolicitud");

        List<DetalleDocumentoFaltante> listaDetalles = new ArrayList<DetalleDocumentoFaltante>();

        try {

            listaDetalles = this.detalleDocumentoFaltanteService
                .obtenerDetallesDocumentosFaltantesPorIdSolicitud(idSolicitud);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerDetallesDocumentosFaltantesPorIdSolicitud: " +
                ex.getMensaje());
        }

        LOGGER.debug("Finalizando AvaluosBean#obtenerDetallesDocumentosFaltantesPorIdSolicitud");

        return listaDetalles;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarDetalleDocumentoFaltante(DetalleDocumentoFaltante)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public DetalleDocumentoFaltante guardarActualizarDetalleDocumentoFaltante(
        DetalleDocumentoFaltante detalleDocumentoFaltante) {

        LOGGER.debug("Iniciando AvaluosBean#guardarActualizarDetalleDocumentoFaltante");

        DetalleDocumentoFaltante result = null;

        try {

            result = this.detalleDocumentoFaltanteService
                .update(detalleDocumentoFaltante);

//TODO :: rodrigo.hernandez :: OJO porque update no lanza una ExcepcionSNC :: pedro.garcia
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#guardarActualizarDetalleDocumentoFaltante: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#guardarActualizarDetalleDocumentoFaltante");

        return result;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerNumeroOrdenPractica()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String obtenerNumeroOrdenPractica() {

        LOGGER.debug("en AvaluosBean#obtenerNumeroOrdenPractica");

        String answer = null;
        Object[] answerSP;
        List<Object> paramsNumeracion = new ArrayList<Object>();

        paramsNumeracion.add(new BigDecimal(ENumeraciones.NUMERACION_ORDENES_PRACTICA.getId()));
        //N: hay que decir explÃ­citamente que los otros parÃ¡metros son null
        paramsNumeracion.add(null);
        paramsNumeracion.add(null);
        paramsNumeracion.add(null);
        paramsNumeracion.add(null);

        try {
            //D: en la posiciÃ³n 0 devuelve la numeraciÃ³n sin formato, en la 1 con formato
            answerSP = this.procedimientoService.generarNumeracion(paramsNumeracion);
            answer = (String) answerSP[1];
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerNumeroOrdenPractica: " + ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerAvaluoPorIdConTramiteYSolicitud(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Avaluo obtenerAvaluoPorIdConTramiteYSolicitud(Long idAvaluo) {

        LOGGER.debug("en AvaluosBean#obtenerAvaluoPorIdConTramiteYSolicitud");

        Avaluo answer = null;

        try {
            answer = this.avaluoService.obtenerPorIdConTramiteYSolicitud(idAvaluo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#obtenerAvaluoPorIdConTramiteYSolicitud: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerListaSolDocsPorSolicitudYTipoDocumento(Long, Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<SolicitudDocumentacion> obtenerListaSolDocsPorSolicitudYTipoDocumento(
        Long solicitudId, Long tipoDocumentoId) {

        LOGGER.debug("Iniciando AvaluosBean#obtenerListaSolDocsPorSolicitudYTipoDocumento");

        List<SolicitudDocumentacion> result = new ArrayList<SolicitudDocumentacion>();

        try {

            result = this.solicitudDocumentacionService
                .obtenerListaSolDocsPorSolicitudYTipoDocumento(solicitudId,
                    tipoDocumentoId);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerListaSolDocsPorSolicitudYTipoDocumento: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#obtenerListaSolDocsPorSolicitudYTipoDocumento");

        return result;
    }

    /**
     * @see IAvaluosLocal#obtenerDocumentoParaRadicacionDefinitiva(Long)
     * @author felipe.cadena
     */
    @Override
    @Implement
    public Documento obtenerDocumentoParaRadicacionDefinitiva(Long idSolicitud) {

        Documento resultado = null;

        try {

            resultado = this.documentoService.obtenerUltimoDocSolicitud(idSolicitud);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerDocumentoParaRadicacionDefinitiva: " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#borrarDetalleDocumentoFaltante(DetalleDocumentoFaltante)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public void borrarDetalleDocumentoFaltante(
        DetalleDocumentoFaltante detalleDocumentoFaltante) {
        LOGGER.debug("Iniciando AvaluosBean#borrarDetalleDocumentoFaltante");

        try {

            this.detalleDocumentoFaltanteService.delete(detalleDocumentoFaltante);

//TODO :: rodrigo.hernandez :: OJO porque delete no lanza una ExcepcionSNC :: pedro.garcia
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#borrarDetalleDocumentoFaltante: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#borrarDetalleDocumentoFaltante");

    }

    /**
     * @see IAvaluosLocal#guardarActualizarDetalleDocumentoFaltantes(List)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public void guardarActualizarDetalleDocumentoFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentoFaltantes) {
        LOGGER.debug("Iniciando AvaluosBean#borrarDetalleDocumentoFaltante");

        try {

            this.detalleDocumentoFaltanteService.updateMultiple(listaDetalleDocumentoFaltantes);

//TODO :: rodrigo.hernandez :: OJO porque updateMultiple no lanza una ExcepcionSNC :: pedro.garcia
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#borrarDetalleDocumentoFaltante: " +
                ex.getMensaje());
        }

        LOGGER.debug("Fin AvaluosBean#borrarDetalleDocumentoFaltante");

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see
     * IAvaluosLocal#guardarActualizarOrdenPractica(co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Long guardarActualizarOrdenPractica(OrdenPractica ordenPractica) {

        LOGGER.debug("En AvaluosBean#guardarActualizarOrdenPractica ");

        Long answer = null;
        OrdenPractica insertadaOActualizada;

        try {
            insertadaOActualizada = this.ordenPracticaService.update(ordenPractica);
            if (insertadaOActualizada.getId() != null) {
                answer = insertadaOActualizada.getId();
            }
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#guardarActualizarOrdenPractica: " + ex.getMessage());
        }

        return answer;

    }

    // -----------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarOficioDeNoAprobacionAvaluoPorIdAvaluo(Long)
     * @author christian.rodriguez
     */
    @Override
    public Documento consultarOficioDeNoAprobacionAvaluoPorIdAvaluo(
        Long idAvaluo) {

        Documento oficioNoAprobacion = null;

        try {

            oficioNoAprobacion = this.avaluoService
                .consultarDocumentoPorIdAvaluoYTipoDocumento(idAvaluo,
                    ETipoDocumento.AVALUOS_OFICIO_NO_APROBACION.getId());

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarOficioDeNoAprobacionAvaluoPorIdAvaluo: " +
                ex.getMensaje());
        }

        return oficioNoAprobacion;
    }

    /**
     * @see IAvaluosLocal#consultarControlCalidadPorAvaluoId(Long)
     * @author felipe.cadena
     */
    @Override
    public List<ControlCalidadAvaluo> consultarControlCalidadPorAvaluoId(Long idAvaluo) {

        List<ControlCalidadAvaluo> resultado = null;

        try {
            Asignacion asignacionAvaluo = this.asignacionService
                .consultarAsignacionActualAvaluo(idAvaluo,
                    EAvaluoAsignacionProfActi.CC_TERRITORIAL
                        .getCodigo());
            resultado = this.controlCalidadAvaluoDAOService.
                consultarTodosPorAvaluoId(idAvaluo, asignacionAvaluo.getId());

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarControlCalidadPorAvaluoId: " +
                ex.getMensaje());
        }

        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#actualizarOrdenPracticaEnAvaluoAsignacionProfesional(java.lang.String,
     * java.util.HashMap)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean actualizarOrdenPracticaEnAvaluoAsignacionProfesional(
        String numeroOrdenPractica, List<Long> avaluosOrdenPractica) {

        boolean answer = false;

        try {
            this.avaluoAsignacionProfesionalService.
                actualizarOrdenPractica(numeroOrdenPractica, avaluosOrdenPractica);
            answer = true;
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluosBean#actualizarOrdenPracticaEnAvaluoAsignacionProfesional" +
                ": " + ex.getMensaje());

        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarOrdenPracticaPorAvaluoId(Long)
     * @author felipe.cadena
     */
    @Override
    public OrdenPractica consultarOrdenPracticaPorAvaluoId(Long idAvaluo) {

        OrdenPractica resultado = null;

        try {

            resultado = this.ordenPracticaService
                .consultarPorAvaluoId(idAvaluo);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarOrdenPracticaPorAvaluoId: " +
                ex.getMensaje());
        }

        return resultado;
    }

    /**
     * @see IAvaluosLocal#consultarDocumentosTramitePorIdsTramites(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<TramiteDocumentacion> consultarDocumentosTramitePorIdsTramites(
        List<Long> listaIdsTramites) {

        List<TramiteDocumentacion> result = new ArrayList<TramiteDocumentacion>();

        try {

            result = this.tramiteDocumentacionService
                .buscarTramiteDocumentacionsPorTramitesIds(listaIdsTramites);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarOficioDeNoAprobacionAvaluoPorIdAvaluo: " +
                ex.getMensaje());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#buscarDocumentosDeTramiteDocumentacionsPorSolicitudId(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<Documento> buscarDocsDeTramiteDocumentacionsPorSolicitudId(
        Long idSolicitud) {

        List<Documento> result = new ArrayList<Documento>();

        try {

            result = this.documentoService
                .buscarDocsDeTramiteDocumentacionsPorSolicitudId(idSolicitud);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#consultarOficioDeNoAprobacionAvaluoPorIdAvaluo: " +
                ex.getMensaje());
        }

        return result;
    }

    // ---------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarActualizarAvaluoMovimiento(AvaluoMovimiento, UsuarioDTO))
     * @author christian.rodriguez
     */
    @Override
    public AvaluoPermiso guardarActualizarAvaluoPermiso(AvaluoPermiso permiso,
        UsuarioDTO usuario) {

        LOGGER.debug("on AvaluosBean#guardarActualizarAvaluoPermiso ...");

        try {

            if (permiso.getId() == null) {
                permiso.setUsuarioLog(usuario.getLogin());
                permiso.setFechaLog(new Date());
            }

            permiso = this.avaluoPermisoService.update(permiso);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error al guardar/actualizar el avaluo permiso: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#guardarActualizarAvaluoPermiso");

        return permiso;
    }

    // ---------------------------------------------------------------------------
    /**
     * @author christian.rodriguez
     * @see IAvaluosLocal#borrarSolicitudPredioPorId(Long)
     */
    @Implement
    @Override
    public boolean borrarAvaluoPermisoPorId(Long avaluoPermisoId) {

        LOGGER.debug("on AvaluosBean#borrarAvaluoPermisoPorId ...");

        try {

            if (avaluoPermisoId != null) {

                AvaluoPermiso permisoABorrar = this.avaluoPermisoService
                    .findById(avaluoPermisoId);
                if (permisoABorrar != null) {
                    this.avaluoPermisoService.delete(permisoABorrar);
                    return true;
                }
            }
            return false;

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#borrarAvaluoPermisoPorId: " +
                ex.getMensaje());
        }
        return false;
    }

//TODO :: rodrigo.hernandez :: documentar segÃºn estÃ¡ndar :: pedro.garcia
    @Override
    public SolicitudDocumento obtenerSolicitudDocumentoPorSolicitudId(
        Long solicitudId) {

        SolicitudDocumento result = null;

        try {

            result = this.solicitudDocumentoService
                .obtenerSolicitudDocumentoPorSolicitudId(solicitudId);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerSolicitudDocumentoPorSolicitudId: " +
                ex.getMensaje());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion(String,
     * String)
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public ProfesionalAvaluo consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion(
        String usuarioSistema, String numeroIdentificacion) {

        LOGGER.debug(
            "on AvaluosBean#consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion");

        ProfesionalAvaluo profesionalEncontrado = null;

        try {

            profesionalEncontrado = this.profesionalAvaluoService
                .consultarPorUsuarioSistemaYNumeroIdentificacion(
                    usuarioSistema, numeroIdentificacion);

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluosBean#consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion: " +
                ex.getMensaje());
        }

        return profesionalEncontrado;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarControlCalidadAValuoRev(ControlCalidadAvaluoRev,
     * UsuarioDTO)
     * @author christian.rodriguez
     *
     */
    @Override
    public ControlCalidadAvaluoRev guardarActualizarControlCalidadAValuoRev(
        ControlCalidadAvaluoRev revision, UsuarioDTO usuario) {

        LOGGER.debug("on AvaluosBean#guardarActualizarControlCalidadAValuoRev ...");

        try {

            if (revision.getId() == null) {
                revision.setUsuarioLog(usuario.getLogin());
                revision.setFechaLog(new Date());
            }

            revision = this.controlCalidadAvaluoRevDAOService.update(revision);

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error al guardar/actualizar la revisiÃ³n de control calidad de avalÃºos: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#guardarActualizarControlCalidadAValuoRev");

        return revision;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarAvaluoEvaluacion(AvaluoEvaluacion, UsuarioDTO)
     * @author felipe.cadena
     *
     */
    @Override
    public AvaluoEvaluacion guardarActualizarAvaluoEvaluacion(
        AvaluoEvaluacion avaluoEvaluacion, UsuarioDTO usuario) {

        LOGGER.debug("on AvaluosBean#guardarActualizarAvaluoEvaluacion ...");

        try {

            if (avaluoEvaluacion.getId() == null) {
                avaluoEvaluacion.setUsuarioLog(usuario.getLogin());
                avaluoEvaluacion.setFechaLog(new Date());
                for (AvaluoEvaluacionDetalle aed : avaluoEvaluacion.getAvaluoEvaluacionDetalles()) {
                    if (aed.getId() == null) {
                        aed.setUsuarioLog(usuario.getLogin());
                        aed.setFechaLog(new Date());
                    }
                }
            }

            avaluoEvaluacion = this.avaluoEvaluacionService.update(avaluoEvaluacion);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error al guardar/actualizar la evaluaciÃ³n del avalÃºo: " +
                ex.getMensaje());
        }

        LOGGER.debug("... finished AvaluosBean#guardarActualizarAvaluoEvaluacion");

        return avaluoEvaluacion;
    }

    /**
     * @see IAvaluosLocal#consultarOrdenesPracticaPorAvaluadorIdYTipoVinculacion(Long, String)
     * @author christian.rodriguez
     */
    @Override
    public List<OrdenPractica> consultarOrdenesPracticaPorAvaluadorIdYTipoVinculacion(
        Long profesionalId, String tipoVinculacionProfesional) {

        LOGGER.debug("on AvaluosBean#consultarOrdenesPracticaPorAvaluadorIdYTipoVinculacion ...");

        List<OrdenPractica> ordenes = null;
        Date fechaMinima = null;
        Date fechaMaxima = null;

        if (tipoVinculacionProfesional
            .equalsIgnoreCase(EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA
                .getCodigo())) {

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());

            cal.set(Calendar.MONTH, 0);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            fechaMinima = cal.getTime();

            cal.set(Calendar.MONTH, 12);
            cal.set(Calendar.DAY_OF_MONTH, 31);
            fechaMaxima = cal.getTime();

        } else if (tipoVinculacionProfesional
            .equalsIgnoreCase(EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS
                .getCodigo())) {

            ProfesionalAvaluo profesional = this.profesionalAvaluoService
                .obtenerByIdConAtributos(profesionalId);

            fechaMinima = profesional.getContratoActivo().getFechaDesde();
            fechaMaxima = profesional.getContratoActivo().getFechaHasta();
        }

        ordenes = this.ordenPracticaService
            .consultarPorProfesionalAvaluosIdYRangoFechas(profesionalId,
                fechaMinima, fechaMaxima);

        LOGGER.debug(
            "... finished AvaluosBean#consultarOrdenesPracticaPorAvaluadorIdYTipoVinculacion");

        return ordenes;
    }

    // ---------------------------------------------------------------------------
    /**
     * @author christian.rodriguez
     * @see IAvaluosLocal#borrarOrdenPracticaPorId(Long)
     */
    @Implement
    @Override
    public boolean borrarOrdenPracticaPorId(Long ordenPracticaId) {

        LOGGER.debug("on AvaluosBean#borrarOrdenPractiaPorId ...");

        try {

            if (ordenPracticaId != null) {

                OrdenPractica ordenABorrar = this.ordenPracticaService
                    .findById(ordenPracticaId);
                if (ordenABorrar != null) {

                    Long documentoId = ordenABorrar.getDocumentoId();

                    // Desasociar orden
                    List<AvaluoAsignacionProfesional> asignacionesADesasociar =
                        this.avaluoAsignacionProfesionalService
                            .consultarPorOrdenPracticaId(ordenPracticaId);
                    for (AvaluoAsignacionProfesional asignacion : asignacionesADesasociar) {
                        asignacion.setOrdenPractica(null);
                    }
                    this.avaluoAsignacionProfesionalService
                        .updateMultiple(asignacionesADesasociar);
                    ordenABorrar.setDocumentoId(null);

                    // EliminaciÃ³n orden
                    this.ordenPracticaService.delete(ordenABorrar);

                    // EliminaciÃ³n documento DB
                    Documento documentoAsociadoABorrar = this.documentoService
                        .findById(documentoId);
                    this.generalesService
                        .borrarDocumentoEnBDyAlfresco(documentoAsociadoABorrar);

                    return true;
                }
            }
            return false;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#borrarOrdenPractiaPorId: " +
                ex.getMensaje());
        }
        return false;
    }

    // ---------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarAvaluosPorOrdenPracticaIdYProfesionalId(Long, Long)
     * @author christian.rodriguez
     */
    @Override
    public List<Avaluo> consultarAvaluosPorOrdenPracticaIdYProfesionalId(
        Long profesionalId, Long ordenPracticaId) {

        LOGGER.debug("on AvaluosBean#consultarAvaluosPorOrdenPracticaIdYProfesionalId");

        List<Avaluo> avaluos = null;

        try {

            avaluos = this.avaluoService
                .consultarPorOrdenPracticaIdYProfesionalId(profesionalId,
                    ordenPracticaId);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluosPorOrdenPracticaIdYProfesionalId: " +
                ex.getMensaje());
        }

        return avaluos;
    }

    // ---------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarAvaluosPorOrdenPracticaId(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<Avaluo> consultarAvaluosPorOrdenPracticaId(Long ordenPracticaId) {

        LOGGER.debug("on AvaluosBean#consultarAvaluosPorOrdenPractica");

        List<Avaluo> avaluos = null;

        try {

            avaluos = this.avaluoService
                .consultarPorOrdenPracticaId(ordenPracticaId);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluosPorOrdenPractica: " +
                ex.getMensaje());
        }

        return avaluos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerAvaluosPorIdsParaAsignacion(long[], java.lang.String, boolean
     * java.lang.String, java.util.Map, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> obtenerAvaluosPorIdsParaAsignacion(long[] idsTramites,
        String actividadCodigo, boolean asignados, String sortField, String sortOrder,
        Map<String, String> filters, int... contadoresDesdeYCuantos) {

        LOGGER.debug("en AvaluosBean#obtenerAvaluosPorIdsParaAsignacion");

        List<Avaluo> answer = null;

        try {
            answer = this.avaluoService.obtenerParaAsignacion(idsTramites, actividadCodigo,
                asignados, sortField, sortOrder, filters, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#obtenerAvaluosPorIdsParaAsignacion: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarEvaluacionesPorAvaluoAsignacionConAtributos(AvaluoEvaluacion,
     * UsuarioDTO)
     * @author felipe.cadena
     *
     */
    @Override
    public AvaluoEvaluacion consultarEvaluacionesPorAvaluoAsignacionConAtributos(
        Long idAvaluo, Long idAsignacion) {

        LOGGER.debug("on AvaluosBean#consultarEvaluacionesPorAvaluoAsignacionConAtributos ...");

        AvaluoEvaluacion resultado = null;

        try {
            resultado = this.avaluoEvaluacionService.consultarPorAvaluoAsignacionConAtributos(
                idAvaluo, idAsignacion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluosBean#consultarEvaluacionesPorAvaluoAsignacionConAtributos: " +
                ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IAvaluosLocal#consultarTodasVariableEvaluacionAvaluo()
     * @author felipe.cadena
     *
     */
    @Override
    public List<VariableEvaluacionAvaluo> consultarTodasVariableEvaluacionAvaluo() {

        LOGGER.debug("on AvaluosBean#consultarTodasVariableEvaluacionAvaluo ...");

        List<VariableEvaluacionAvaluo> resultado = null;

        try {
            resultado = this.variableEvaluacionAvaluoService.findAll();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarTodasVariableEvaluacionAvaluo: " +
                ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IAvaluosLocal#consultarAsignacionActualAvaluo(Long, String)
     * @author felipe.cadena
     *
     */
    @Override
    public Asignacion consultarAsignacionActualAvaluo(Long idAvaluo, String actividad) {

        LOGGER.debug("on AvaluosBean#consultarAsignacionActualAvaluo ...");

        Asignacion resultado = null;

        try {
            resultado = this.asignacionService.consultarAsignacionActualAvaluo(
                idAvaluo, actividad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAsignacionActualAvaluo: " +
                ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IAvaluosLocal#calcularDevolucionesAvaluo(Long)
     * @author felipe.cadena
     *
     */
    @Override
    public Long calcularDevolucionesAvaluo(Long idAvaluo, Long idAsignacion, String actividad) {

        LOGGER.debug("on AvaluosBean#calcularDevolucionesAvaluo ...");

        Long resultado = null;

        try {
            resultado = this.controlCalidadAvaluoRevDAOService.
                calcularNumeroDevoluciones(idAvaluo, idAsignacion, actividad);

            if (resultado == 2) {
                ControlCalidadAvaluo ccActual = this.controlCalidadAvaluoDAOService.
                    consultarPorAvaluoAsignacionActividad(idAvaluo, idAsignacion, actividad);
                if (ccActual.getAprobo().equals(ESiNo.NO.getCodigo())) {
                    resultado = 3L;
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#calcularDevolucionesAvaluo: " +
                ex.getMensaje());
        }
        return resultado;
    }

    /**
     * @see IAvaluosLocal#calcularTiempoEjecucionDias(Long)
     * @author felipe.cadena
     *
     */
    @Override
    public Integer calcularTiempoEjecucionDias(Long idAvaluo) {

        LOGGER.debug("on AvaluosBean#calcularTiempoEjecucionDias ...");

        OrdenPractica op = this.consultarOrdenPracticaPorAvaluoId(idAvaluo);
        Calendar fechaInicio = Calendar.getInstance();
        fechaInicio.setTime(op.getFechaEnvio());
        //TODO::felipe.cadena::determinar la fecha de entrega del informe de avalÃºo CU-XXX
        Calendar fechaFin = Calendar.getInstance();
        fechaFin.setTime(new Date());

        Integer diasBase = QueryNativoDAO.calcularDiasHabilesEntreDosFechas(
            this.documentoService.getEntityManager(),
            fechaInicio.getTime(), fechaFin.getTime());

        //Se restan los dias que estuvo suspendido el avaluo
        diasBase -= this.avaluoMovimientoService.calcularDiasSuspensionAvaluo(idAvaluo);

        return diasBase;
    }

    /**
     * @see IAvaluosLocal#calcularTiempoRetrasoDias(Long)
     * @author felipe.cadena
     *
     */
    @Override
    public Integer calcularTiempoRetrasoDias(Long idAvaluo) {

        LOGGER.debug("on AvaluosBean#calcularTiempoRetrasoDias ...");

        Avaluo ava = this.avaluoService.obtenerPorIdConAtributos(idAvaluo);

        Integer diasAmpliacion = this.avaluoMovimientoService.calcularDiasAmpliacionAvaluo(idAvaluo);
        Integer diasEjecucion = this.calcularTiempoEjecucionDias(idAvaluo);
        Integer diasPlazo = ava.getPlazoEjecucion().intValue();
        if (diasEjecucion - (diasPlazo + diasAmpliacion) < 0) {
            return 0;
        } else {
            return diasEjecucion - (diasPlazo + diasAmpliacion);
        }

    }

    /**
     * @see IAvaluosLocal#desasociarTramitesDeOrdenDePractica(Long, Avaluo[])
     * @author christian.rodriguez
     */
    @Override
    public boolean desasociarTramitesDeOrdenDePractica(
        Long ordenAModificarId, Avaluo[] tramitesADesasociar) {

        LOGGER.debug("on AvaluosBean#desasociarTramitesDeOrdenDePractica ...");

        boolean desasignacionExitosa = false;

        if (ordenAModificarId != null && tramitesADesasociar.length > 0) {

            List<AvaluoAsignacionProfesional> asignaciones = this.avaluoAsignacionProfesionalService
                .consultarPorOrdenPracticaId(ordenAModificarId);

            List<Avaluo> avaluosAActualizar = new ArrayList<Avaluo>();
            List<AvaluoAsignacionProfesional> asignacionesAActualizar =
                new ArrayList<AvaluoAsignacionProfesional>();

            for (Avaluo avaluoADesasociar : tramitesADesasociar) {

                for (AvaluoAsignacionProfesional asignacionDB : asignaciones) {

                    if (avaluoADesasociar.getId().intValue() == asignacionDB
                        .getAvaluo().getId().intValue()) {

                        Avaluo avaluoDB = asignacionDB.getAvaluo();
                        avaluoDB.setPlazoEjecucion(0L);

                        avaluosAActualizar.add(avaluoDB);

                        asignacionDB.setOrdenPractica(null);
                        asignacionesAActualizar.add(asignacionDB);

                    }
                }
            }

            try {

                if (!avaluosAActualizar.isEmpty()) {
                    this.avaluoService.updateMultiple(avaluosAActualizar);
                }

                if (!asignacionesAActualizar.isEmpty()) {
                    this.avaluoAsignacionProfesionalService
                        .updateMultiple(asignacionesAActualizar);
                }

                desasignacionExitosa = true;

            } catch (ExcepcionSNC ex) {
                LOGGER.error("error en AvaluosBean#desasociarTramitesDeOrdenDePractica: " +
                    ex.getMensaje());
            }
        }

        return desasignacionExitosa;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#contarAvaluosPorIdsParaAsignacion(long[], java.lang.String, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarAvaluosPorIdsParaAsignacion(long[] idsTramites, String actividadCodigo,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("en AvaluosBean#contarAvaluosPorIdsParaAsignacion");

        int answer = 0;

        try {
            answer = this.avaluoService.contarParaAsignacion(idsTramites, actividadCodigo, false,
                contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#contarAvaluosPorIdsParaAsignacion: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#contarAvaluosPorIdsConAsignacionSinMover(long[], java.lang.String, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarAvaluosPorIdsConAsignacionSinMover(long[] idsTramites, String actividadCodigo,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("en AvaluosBean#contarAvaluosPorIdsConAsignacionSinMover");

        int answer = 0;

        try {
            answer = this.avaluoService.contarParaAsignacion(idsTramites, actividadCodigo, true,
                contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#contarAvaluosPorIdsConAsignacionSinMover: " +
                ex.getMensaje());
        }

        return answer;

    }

    // -----------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#aprobarAvaluos(List)
     * @author christian.rodriguez
     */
    @Override
    public boolean aprobarAvaluos(List<Avaluo> avaluosAAprobar) {

        LOGGER.debug("en AvaluosBean#aprobarAvaluos");

        boolean aprobacionExitosa = false;

        if (avaluosAAprobar != null && !avaluosAAprobar.isEmpty()) {

            Calendar fechaActual = Calendar.getInstance();
            fechaActual.setTime(new Date());
            long anioAprobacion = fechaActual.get(Calendar.YEAR);

            for (Avaluo avaluo : avaluosAAprobar) {

                avaluo.setAprobado(ESiNo.SI.getCodigo());
                avaluo.setFechaAprobacion(new Date());
                avaluo.setAnioAprobacion(anioAprobacion);
            }

            try {
                this.avaluoService.updateMultiple(avaluosAAprobar);
                aprobacionExitosa = true;
            } catch (ExcepcionSNC ex) {
                LOGGER.error("error en AvaluosBean#aprobarAvaluos: " +
                    ex.getMensaje());
            }
        }

        return aprobacionExitosa;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarProfesionalesAvaluosCargaPorTerritorial(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<VProfesionalAvaluosCarga> consultarProfesionalesAvaluosCargaPorTerritorial(
        String codigoTerritorial) {

        LOGGER.debug("on AvaluosBean#consultarProfesionalesAvaluosCargaPorTerritorial");

        List<VProfesionalAvaluosCarga> answer = null;

        try {
            answer = this.vProfesionalAvaluosCargaService.consultarPorTerritorial(codigoTerritorial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarProfesionalesAvaluosCargaPorTerritorial: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#validarAsignadoControlCalidadNoEsAvaluador(long, long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean validarAsignadoControlCalidadNoEsAvaluador(long profesionalAvaluosId,
        long[] idsAvaluos) {

        LOGGER.debug("en AvaluosBean#validarAsignadoControlCalidadNoEsAvaluador");

        boolean answer = false;

        try {
            answer = this.avaluoAsignacionProfesionalService.
                existeAsignacionDeProfesional(profesionalAvaluosId,
                    EAvaluoAsignacionProfActi.AVALUAR.getCodigo(), idsAvaluos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#validarAsignadoControlCalidadNoEsAvaluador: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#obtenerAvaluoPorIdParaImpugnacionRevision(Long)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo obtenerAvaluoPorIdParaImpugnacionRevision(Long avaluoId) {
        LOGGER.debug("en AvaluosBean#obtenerAvaluoPorIdParaImpugnacionRevision");

        Avaluo avaluoAImpugnarRevisar = null;

        if (avaluoId != null) {

            try {
                avaluoAImpugnarRevisar = this.avaluoService
                    .obtenerPorIdParaImpugnacionRevision(avaluoId);
            } catch (ExcepcionSNC ex) {
                LOGGER.error("error en AvaluosBean#obtenerAvaluoPorIdParaImpugnacionRevision: " +
                    ex.getMensaje());
            }
        }

        return avaluoAImpugnarRevisar;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#validarAsignadoControlCalidadNoEsAvaluador(long, long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean validarAsignadosRevisionSonAvaluadores(long[] idsProfesionalesAvaluos,
        Long idAvaluo) {

        LOGGER.debug("en AvaluosBean#validarAsignadosRevisionSonAvaluadores");
        boolean answer = false;
        List<AvaluoAsignacionProfesional> asignacionesActividadAvaluar;
        long[] idsAvaluadoresActividadAvaluar;
        int counter;

        try {
            asignacionesActividadAvaluar = this.avaluoAsignacionProfesionalService.
                obtenerDeUltimaAsignacion(EAvaluoAsignacionProfActi.AVALUAR.getCodigo(), idAvaluo);

            if (asignacionesActividadAvaluar == null || asignacionesActividadAvaluar.isEmpty()) {
                return false;
            }

            idsAvaluadoresActividadAvaluar = new long[asignacionesActividadAvaluar.size()];
            counter = 0;
            for (AvaluoAsignacionProfesional aap : asignacionesActividadAvaluar) {
                idsAvaluadoresActividadAvaluar[counter] =
                    aap.getProfesionalAvaluo().getId().longValue();
                counter++;
            }

            Arrays.sort(idsProfesionalesAvaluos);
            Arrays.sort(idsAvaluadoresActividadAvaluar);
            answer = Arrays.equals(idsProfesionalesAvaluos, idsAvaluadoresActividadAvaluar);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#validarAsignadosRevisionSonAvaluadores: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * IAvaluosLocal#guardarActualizarAsignacion(co.gov.igac.snc.persistence.entity.avaluos.Asignacion)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Asignacion guardarActualizarAsignacion(Asignacion asignacion) {

        LOGGER.debug("en AvaluosBean#guardarActualizarAsignacion ");

        Asignacion answer = null;

        try {
            answer = this.asignacionService.update(asignacion);
        } catch (Exception ex) {
            LOGGER.error("Error al actualizar/guardar una Asignacion: " + ex.getMessage());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#guardarAvaluosAsignacionProfesional(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean guardarAvaluosAsignacionProfesional(
        List<AvaluoAsignacionProfesional> newAsignaciones) {

        LOGGER.debug("en AvaluosBean#guardarAvaluosAsignacionProfesional ");
        boolean answer = false;

        try {
            this.avaluoAsignacionProfesionalService.persistMultiple(newAsignaciones);
            answer = true;
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#guardarAvaluosAsignacionProfesional: " +
                ex.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#borrarAsignacionesVigentesDeAvaluo(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean borrarAsignacionesVigentesDeAvaluo(Long idAvaluo, String actividad) {

        LOGGER.debug("en AvaluosBean#borrarAsignacionesVigentesDeAvaluo ");

        boolean answer = false;

        try {
            answer = this.avaluoAsignacionProfesionalService.borrarDeAvaluo(idAvaluo, actividad,
                true);
//TODO :: snc.avaluos :: aquí se podrá revisar si queda alguna Asignacion sin AvaluoAsignacionProfesional relacionadas para borrarla
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#borrarAsignacionesVigentesDeAvaluo: " +
                ex.getMensaje());
        }

        return answer;
    }

    // ------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarComiteAvaluoPorIdAvaluo(Long)
     * @author christian.rodriguez
     */
    @Override
    public ComiteAvaluo consultarComiteAvaluoPorIdAvaluo(Long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarComiteAvaluoPorIdAvaluo");

        ComiteAvaluo comite = null;
        if (avaluoId != null) {

            try {
                Asignacion asignacionActual = this
                    .consultarAsignacionActualAvaluo(avaluoId,
                        EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL
                            .getCodigo());

                ControlCalidadAvaluo cca = this.controlCalidadAvaluoDAOService
                    .consultarPorAvaluoAsignacionActividad(avaluoId,
                        asignacionActual.getId(),
                        EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL
                            .getCodigo());

                comite = this.comiteAvaluoService
                    .consultarPorIdControlCalidadAvaluo(cca.getId());
            } catch (ExcepcionSNC ex) {
                LOGGER.error("error en AvaluosBean#consultarComiteAvaluoPorIdAvaluo: " +
                    ex.getMensaje());
            }
        }

        return comite;
    }

    /**
     * @see IAvaluoDAO#guardarActualizarAvaluoPredioZona(AvaluoPredioZona)
     * @author felipe.cadena
     */
    @Override
    public AvaluoPredioZona guardarActualizarAvaluoPredioZona(AvaluoPredioZona detalle,
        UsuarioDTO usuario) {

        try {
            if (detalle.getId() == null) {
                detalle.setUsuarioLog(usuario.getLogin());
                detalle.setFechaLog(new Date());
            }
            detalle = this.avaluoPredioZonaService.update(detalle);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, "AvaluoPredioZona");
        }

        return detalle;
    }

    /**
     * @see IAvaluoDAO#guardarActualizarAvaluoPredioCultivo(AvaluoPredioCultivo)
     * @author felipe.cadena
     */
    @Override
    public AvaluoPredioCultivo guardarActualizarAvaluoPredioCultivo(AvaluoPredioCultivo detalle,
        UsuarioDTO usuario) {

        try {
            if (detalle.getId() == null) {
                detalle.setUsuarioLog(usuario.getLogin());
                detalle.setFechaLog(new Date());
            }
            detalle = this.avaluoPredioCultivoService.update(detalle);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, "AvaluoPredioCultivo");
        }

        return detalle;
    }

    /**
     * @see IAvaluoDAO#guardarActualizarAvaluoPredioConstruccion(AvaluoPredioConstruccion)
     * @author felipe.cadena
     */
    @Override
    public AvaluoPredioConstruccion guardarActualizarAvaluoPredioConstruccion(
        AvaluoPredioConstruccion detalle, UsuarioDTO usuario) {

        try {
            if (detalle.getId() == null) {
                detalle.setUsuarioLog(usuario.getLogin());
                detalle.setFechaLog(new Date());
            }
            detalle = this.avaluoPredioConstruccionService.update(detalle);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, "AvaluoPredioConstruccion");
        }

        return detalle;
    }

    /**
     * @see IAvaluoDAO#guardarActualizarAvaluoPredioMaquinaria(AvaluoPredioMaquinaria)
     * @author felipe.cadena
     */
    @Override
    public AvaluoPredioMaquinaria guardarActualizarAvaluoPredioMaquinaria(
        AvaluoPredioMaquinaria detalle, UsuarioDTO usuario) {

        try {
            if (detalle.getId() == null) {
                detalle.setUsuarioLog(usuario.getLogin());
                detalle.setFechaLog(new Date());
            }
            detalle = this.avaluoPredioMaquinariaService.update(detalle);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, "AvaluoPredioMaquinaria");
        }

        return detalle;
    }

    /**
     * @see IAvaluoDAO#guardarActualizarAvaluoPredioAnexo(AvaluoPredioAnexo)
     * @author felipe.cadena
     */
    @Override
    public AvaluoPredioAnexo guardarActualizarAvaluoPredioAnexo(AvaluoPredioAnexo detalle,
        UsuarioDTO usuario) {

        try {
            if (detalle.getId() == null) {
                detalle.setUsuarioLog(usuario.getLogin());
                detalle.setFechaLog(new Date());
            }
            detalle = this.avaluoPredioAnexoService.update(detalle);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, "AvaluoZonaPredio");
        }

        return detalle;
    }

    // ------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarActualizarComiteAvaluo(ComiteAvaluo, Long, UsuarioDTO)
     * @author christian.rodriguez
     */
    @Override
    public ComiteAvaluo guardarActualizarComiteAvaluo(ComiteAvaluo comiteAvaluo,
        Long idAvaluo, UsuarioDTO usuario) {

        LOGGER.debug("on AvaluosBean#guardarActualizarComiteAvaluo ...");

        try {
            if (comiteAvaluo.getId() == null) {

                Asignacion asignacionActual = this
                    .consultarAsignacionActualAvaluo(idAvaluo,
                        EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL
                            .getCodigo());

                ControlCalidadAvaluo cca = this.controlCalidadAvaluoDAOService
                    .consultarPorAvaluoAsignacionActividad(idAvaluo,
                        asignacionActual.getId(),
                        EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL
                            .getCodigo());

                if (cca != null) {

                    comiteAvaluo.setUsuarioLog(usuario.getLogin());
                    comiteAvaluo.setFechaLog(new Date());
                    comiteAvaluo.setControlCalidadAvaluoId(cca.getId());

                    for (ComiteAvaluoParticipante participante : comiteAvaluo
                        .getComiteAvaluoParticipantes()) {

                        participante.setFechaLog(new Date());
                        participante.setUsuarioLog(usuario.getLogin());
                        participante.setComiteAvaluo(comiteAvaluo);
                    }

                    for (ComiteAvaluoPonente ponente : comiteAvaluo
                        .getComiteAvaluoPonentes()) {

                        ponente.setFechaLog(new Date());
                        ponente.setUsuarioLog(usuario.getLogin());
                        ponente.setComiteAvaluo(comiteAvaluo);
                    }

                }
            }

            comiteAvaluo = this.comiteAvaluoService.update(comiteAvaluo);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "AvaluosBean#guardarActualizarComiteAvaluo");
        }

        return comiteAvaluo;

    }

    // ------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#obtenerNumeroActaComiteAvaluos()
     * @author christian.rodriguez
     */
    @Implement
    @Override
    public String obtenerNumeroActaComiteAvaluos() {

        LOGGER.debug("en AvaluosBean#obtenerSiguienteNumeroActaComiteAvaluos");

        String numero = null;
        Object[] numeroSP;

        List<Object> paramsNumeracion = new ArrayList<Object>();

        paramsNumeracion.add(new BigDecimal(
            ENumeraciones.NUMERACION_ACTA_COMITE_AVALUOS.getId()));
        // N: hay que decir explÃ­citamente que los otros parÃ¡metros son null
        paramsNumeracion.add(null);
        paramsNumeracion.add(null);
        paramsNumeracion.add(null);
        paramsNumeracion.add(null);

        try {
            // D: en la posiciÃ³n 0 devuelve la numeraciÃ³n sin formato, en la 1
            // con formato
            numeroSP = this.procedimientoService
                .generarNumeracion(paramsNumeracion);
            numero = (String) numeroSP[1];
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerSiguienteNumeroActaComiteAvaluos: " +
                ex.getMensaje());
        }

        return numero;
    }

    /**
     * @see IAvaluosLocal#obtenerAvaluadoresPorInterventorId(Long, boolean)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<ProfesionalAvaluo> obtenerAvaluadoresPorInterventorId(
        Long idInterventor, boolean banderaEsAvaluadorExterno) {

        List<ProfesionalAvaluo> result = null;

        try {
            result = this.profesionalAvaluoService
                .obtenerAvaluadoresPorInterventorId(idInterventor,
                    banderaEsAvaluadorExterno);

        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#guardarAvaluosAsignacionProfesional: " +
                ex.getMessage());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#obtenerAvaluosConMovimientosPorFiltro(FiltroDatosConsultaAvaluo)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<Avaluo> obtenerAvaluosConMovimientosPorFiltro(
        FiltroDatosConsultaAvaluo filtro) {
        List<Avaluo> result = null;

        try {
            result = this.avaluoService
                .obtenerAvaluosConMovimientosPorFiltro(filtro);

        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#guardarAvaluosAsignacionProfesional: " +
                ex.getMessage());
        }

        return result;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#guardarModificarDetalleAvaluo(int, java.lang.Object)
     * @author pedro.garcia
     *
     * 1 = AvaluoPredioAnexo 2 = AvaluoPredioConstruccion 3 = AvaluoPredioCultivo 4 =
     * AvaluoPredioMaquinaria 5 = AvaluoPredioZona
     */
    @Implement
    @Override
    public boolean guardarModificarDetalleAvaluo(int tipoDetalleAvaluo,
        Object detalleAvaluoNuevoModificado) {

        boolean answer = true;

        try {
            switch (tipoDetalleAvaluo) {
                case (1): {
                    this.avaluoPredioAnexoService.update(
                        (AvaluoPredioAnexo) detalleAvaluoNuevoModificado);
                    break;
                }
                case (2): {
                    this.avaluoPredioConstruccionService.update(
                        (AvaluoPredioConstruccion) detalleAvaluoNuevoModificado);
                    break;
                }
                case (3): {
                    this.avaluoPredioCultivoService.update(
                        (AvaluoPredioCultivo) detalleAvaluoNuevoModificado);
                    break;
                }
                case (4): {
                    this.avaluoPredioMaquinariaService.update(
                        (AvaluoPredioMaquinaria) detalleAvaluoNuevoModificado);
                    break;
                }
                case (5): {
                    this.avaluoPredioZonaService.update(
                        (AvaluoPredioZona) detalleAvaluoNuevoModificado);
                    break;
                }
                case (6): {
                    this.avaluoPredioService.update(
                        (AvaluoPredio) detalleAvaluoNuevoModificado);
                    break;
                }

                default:
                    break;
            }
        } catch (Exception ex) {
            answer = false;
            LOGGER.error("error en AvaluosBean#guardarModificarDetalleAvaluo: " + ex.getMessage());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<AvaluoPredioAnexo> consultarAvaluoPredioAnexoPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarAvaluoPredioAnexoPorAvaluo");

        List<AvaluoPredioAnexo> answer = null;

        try {
            answer = this.avaluoPredioAnexoService.consultarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluoPredioAnexoPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<AvaluoPredioConstruccion> consultarAvaluoPredioConstruccionPorAvaluo(long avaluoId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarAvaluoPredioCultivoPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioCultivo> consultarAvaluoPredioCultivoPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarAvaluoPredioCultivoPorAvaluo");

        List<AvaluoPredioCultivo> answer = null;

        try {
            answer = this.avaluoPredioCultivoService.consultarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluoPredioCultivoPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarAvaluoPredioMaquinariaPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioMaquinaria> consultarAvaluoPredioMaquinariaPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarAvaluoPredioMaquinariaPorAvaluo");
        List<AvaluoPredioMaquinaria> answer = null;

        try {
            answer = this.avaluoPredioMaquinariaService.consultarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluoPredioMaquinariaPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarAvaluoPredioZonaPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioZona> consultarAvaluoPredioZonaPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarAvaluoPredioZonaPorAvaluo");
        List<AvaluoPredioZona> answer = null;

        try {
            answer = this.avaluoPredioZonaService.consultarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluoPredioZonaPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#eliminarDetalleAvaluo(int, long)
     * @author pedro.garcia
     *
     * 1 = AvaluoPredioAnexo 2 = AvaluoPredioConstruccion 3 = AvaluoPredioCultivo 4 =
     * AvaluoPredioMaquinaria 5 = AvaluoPredioZona 6 = AvaluoPredio
     */
    @Implement
    @Override
    public boolean eliminarDetalleAvaluo(int tipoDetalleAvaluo, long idDetalleAvaluo) {

        boolean answer = true;

        try {
            switch (tipoDetalleAvaluo) {
                case (1): {
                    AvaluoPredioAnexo toDelete = new AvaluoPredioAnexo();
                    toDelete.setId(idDetalleAvaluo);
                    this.avaluoPredioAnexoService.delete(toDelete);
                    break;
                }
                case (2): {
                    AvaluoPredioConstruccion toDelete = new AvaluoPredioConstruccion();
                    toDelete.setId(idDetalleAvaluo);
                    this.avaluoPredioConstruccionService.delete(toDelete);
                    break;
                }
                case (3): {
                    AvaluoPredioCultivo toDelete = new AvaluoPredioCultivo();
                    toDelete.setId(idDetalleAvaluo);
                    this.avaluoPredioCultivoService.delete(toDelete);
                    break;
                }
                case (4): {
                    AvaluoPredioMaquinaria toDelete = new AvaluoPredioMaquinaria();
                    toDelete.setId(idDetalleAvaluo);
                    this.avaluoPredioMaquinariaService.delete(toDelete);
                    break;
                }
                case (5): {
                    AvaluoPredioZona toDelete = new AvaluoPredioZona();
                    toDelete.setId(idDetalleAvaluo);
                    this.avaluoPredioZonaService.delete(toDelete);
                    break;
                }
                case (6): {
                    AvaluoPredio toDelete = new AvaluoPredio();
                    toDelete.setId(idDetalleAvaluo);
                    this.avaluoPredioService.delete(toDelete);
                    break;
                }

                default:
                    break;
            }
        } catch (Exception ex) {
            answer = false;
            LOGGER.error("error en AvaluosBean#eliminarDetalleAvaluo: " + ex.getMessage());
        }

        return answer;
    }

    /**
     * @see IAvaluosLocal#calcularZonasPreviasAvaluo(Long, int)
     * @author felipe.cadena
     */
    @Override
    public Boolean calcularDetallesPreviosAvaluo(Long idAvaluo, int tipoDetalle) {

        Boolean resultado = null;

        try {
            if (tipoDetalle == EAvaluoDetalle.ANEXOS.getCodigo()) {
                resultado = this.avaluoPredioAnexoService.calcularAnexosPreviosAvaluo(idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.ZONAS_ECONOMICAS.getCodigo()) {
                resultado = this.avaluoPredioZonaService.calcularZonasPreviasAvaluo(idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.CONSTRUCCIONES.getCodigo()) {
                resultado = this.avaluoPredioConstruccionService.
                    calcularConstruccionesPreviasAvaluo(idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.MAQUINARIA.getCodigo()) {
                resultado = this.avaluoPredioMaquinariaService.calcularMaquinariasPreviasAvaluo(
                    idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.CULTIVOS.getCodigo()) {
                resultado = this.avaluoPredioCultivoService.calcularCultivosPreviosAvaluo(idAvaluo);
            }
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#calcularDetallesPreviosAvaluo: " +
                ex.getMessage());
        }
        return resultado;
    }

    /**
     * @see IAvaluosLocal#eliminarDetallesPreviosAvaluo(Long, int)
     * @author felipe.cadena
     */
    @Override
    public Boolean eliminarDetallesPreviosAvaluo(Long idAvaluo, int tipoDetalle) {

        Boolean resultado = null;

        try {
            if (tipoDetalle == EAvaluoDetalle.ANEXOS.getCodigo()) {
                resultado = this.avaluoPredioAnexoService.eliminarAnexosPreviosAvaluo(idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.ZONAS_ECONOMICAS.getCodigo()) {
                resultado = this.avaluoPredioZonaService.eliminarZonasPreviasAvaluo(idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.CONSTRUCCIONES.getCodigo()) {
                resultado = this.avaluoPredioConstruccionService.
                    eliminarConstruccionesPreviasAvaluo(idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.MAQUINARIA.getCodigo()) {
                resultado = this.avaluoPredioMaquinariaService.eliminarMaquinariaPreviasAvaluo(
                    idAvaluo);
            }
            if (tipoDetalle == EAvaluoDetalle.CULTIVOS.getCodigo()) {
                resultado = this.avaluoPredioCultivoService.eliminarCultivosPreviosAvaluo(idAvaluo);
            }
        } catch (Exception ex) {
            LOGGER.error("error en AvaluosBean#eliminarDetallesPreviosAvaluo: " +
                ex.getMessage());
        }
        return resultado;
    }

    /**
     * @see IAvaluosLocal#obtenerCapitulosPorAvaluoId(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<AvaluoCapitulo> obtenerCapitulosPorAvaluoId(Long idAvaluo) {

        List<AvaluoCapitulo> result = new ArrayList<AvaluoCapitulo>();

        try {
            result = this.avaluoCapituloService.obtenerCapitulosPorAvaluoId(idAvaluo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#obtenerCapitulosPorAvaluoId: " +
                ex.getMensaje());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#guardarActualizarDocsRequisitoProyeccionCotizacion(List)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public boolean guardarActualizarDocsRequisitoProyeccionCotizacion(
        List<AvaluoRequisitoDocumentacion> listaDocsRequisito) {

        /*
         * Bandera que indica si la actualizacion/almacenamiento fue exitoso
         */
        boolean result = true;

        /*
         * Se carga la lista que actualmente esta en BD
         */
        List<AvaluoRequisitoDocumentacion> listaBD = this
            .obtenerDocsRequisitoProyeccionCotizacionPorAvaluoId(listaDocsRequisito
                .get(0).getAvaluoId());

        /*
         * Se valida que existan docs requisito asociados a la proyeccion de cotizacion
         */
        if (listaBD != null && !listaBD.isEmpty()) {
            /*
             * Se crea una lista con los tipos de documento de los docs requisito a
             * guardar/actualizar
             */
            List<Long> listaTipoDocumentoDocsAActualizar = new ArrayList<Long>();

            for (AvaluoRequisitoDocumentacion doc : listaDocsRequisito) {
                listaTipoDocumentoDocsAActualizar.add(doc.getTipoDocumentoId());
            }

            /*
             * Se revisa si hay capitulos que estaban en la BD y en la lista a actualizar ya no
             * estÃ¡n
             */
            for (AvaluoRequisitoDocumentacion doc : listaBD) {
                /*
                 * Se elimina el registro de BD que ya no se encuentra en la lista a actualizar
                 */
                if (!listaTipoDocumentoDocsAActualizar.contains(doc.getTipoDocumentoId())) {
                    this.avaluoRequisitoDocumentacionService.delete(doc);
                }
            }

        }

        try {
            for (AvaluoRequisitoDocumentacion doc : listaDocsRequisito) {
                this.avaluoRequisitoDocumentacionService.update(doc);
            }
            result = true;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#obtenerCapitulosPorAvaluoId: " +
                ex.getMensaje());
        }

        return result;
    }

    // -------------------------------------------------------------------------------
    /**
     * @see IAvaluosLocal#consultarOrdenesPracticaPorEnviar()
     * @author christian.rodriguez
     */
    @Override
    public List<OrdenPractica> consultarOrdenesPracticaPorEnviar() {
        LOGGER.debug("en AvaluosBean#consultarOrdenesPracticaPorEnviar");
        List<OrdenPractica> ordenesEncontradas = null;

        try {
            ordenesEncontradas = this.ordenPracticaService
                .consultarPorEstado(EOrdenPracticaEstado.GENERADA
                    .getCodigo());
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarOrdenesPracticaPorEnviar: " +
                ex.getMensaje());
        }

        return ordenesEncontradas;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluosLocal#consultarAvaluoPredioPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredio> consultarAvaluoPredioPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarAvaluoPredioPorAvaluo");
        List<AvaluoPredio> answer = null;

        try {
            answer = this.avaluoPredioService.consultarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluoPredioPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }

    /**
     * @see IAvaluosLocal#consultarObservacionesPorAvaluo(long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<AvaluoObservacion> consultarObservacionesPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarObservacionesPorAvaluo");
        List<AvaluoObservacion> answer = null;

        try {
            answer = this.avaluoObservacionService.buscarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarObservacionesPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }

    /**
     * @see IAvaluosLocal#guardarActualizarObservacionesAvaluo(long, UsuarioDTO)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public AvaluoObservacion guardarActualizarObservacionesAvaluo(
        AvaluoObservacion observacion, UsuarioDTO usuario) {

        LOGGER.debug("en AvaluosBean#consultarObservacionesPorAvaluo");
        AvaluoObservacion answer = null;

        try {
            if (observacion.getId() == null) {
                observacion.setFechaLog(new Date());
                observacion.setUsuarioLog(usuario.getLogin());
            }
            answer = this.avaluoObservacionService.update(observacion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#guardarActualizarObservacionesAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }

    /**
     * @see IAvaluosLocal#guardarActualizarCapitulosProyeccionCotizacion(List)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public boolean guardarActualizarCapitulosProyeccionCotizacion(
        List<AvaluoCapitulo> listaCapitulos) {

        /*
         * Bandera que indica si la actualizacion/almacenamiento fue exitoso
         */
        boolean result = true;

        /*
         * Se carga la lista que actualmente esta en BD
         */
        List<AvaluoCapitulo> listaBD = this
            .obtenerCapitulosPorAvaluoId(listaCapitulos.get(0)
                .getAvaluoId());

        /*
         * Se valida que existan capitulos asociados a la proyeccion de cotizacion
         */
        if (listaBD != null && !listaBD.isEmpty()) {
            /*
             * Se crea una lista con los nombres de los capitulos a guardar/actualizar
             */
            List<String> listaNombresCapsAActualizar = new ArrayList<String>();

            for (AvaluoCapitulo cap : listaCapitulos) {
                listaNombresCapsAActualizar.add(cap.getTitulo());
            }

            /*
             * Se revisa si hay capitulos que estaban en la BD y en la lista a actualizar ya no
             * estÃ¡n
             */
            for (AvaluoCapitulo cap : listaBD) {
                /*
                 * Se elimina el registro de BD que ya no se encuentra en la lista a actualizar
                 */
                if (!listaNombresCapsAActualizar.contains(cap.getTitulo())) {
                    this.avaluoCapituloService.delete(cap);
                }
            }

        }

        try {
            for (AvaluoCapitulo cap : listaCapitulos) {
                this.avaluoCapituloService.update(cap);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#guardarActualizarCapitulosProyeccionCotizacion: " +
                ex.getMensaje());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#obtenerDocsRequisitoProyeccionCotizacionPorAvaluoId(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<AvaluoRequisitoDocumentacion> obtenerDocsRequisitoProyeccionCotizacionPorAvaluoId(
        Long avaluoId) {
        List<AvaluoRequisitoDocumentacion> result = new ArrayList<AvaluoRequisitoDocumentacion>();

        try {
            result = this.avaluoRequisitoDocumentacionService
                .obtenerDocsRequisitoPorAvaluoId(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluosBean#obtenerDocsRequisitoProyeccionCotizacionPorAvaluoId: " +
                ex.getMensaje());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#eliminarCapituloProyeccionCotizacion(AvaluoCapitulo)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public boolean eliminarCapituloProyeccionCotizacion(AvaluoCapitulo capitulo) {
        boolean result = true;

        try {
            this.avaluoCapituloService.delete(capitulo);
        } catch (ExcepcionSNC ex) {

            result = false;

            LOGGER.error("error en AvaluosBean#eliminarCapituloProyeccionCotizacion: " +
                ex.getMensaje());
        }

        return result;
    }

    /**
     * @see IAvaluosLocal#eliminarDocRequisitoProyeccionCotizacion(AvaluoRequisitoDocumentacion)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public boolean eliminarDocRequisitoProyeccionCotizacion(
        AvaluoRequisitoDocumentacion docRequisito) {

        boolean result = true;

        try {
            this.avaluoRequisitoDocumentacionService.delete(docRequisito);
        } catch (ExcepcionSNC ex) {

            result = false;

            LOGGER.error("error en AvaluosBean#eliminarDocRequisitoProyeccionCotizacion: " +
                ex.getMensaje());
        }

        return result;

    }

    /**
     * @see IAvaluosLocal#consultarAreasRegistradasPorAvaluo(long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<AvaluoAreaRegistrada> consultarAreasRegistradasPorAvaluo(long avaluoId) {

        LOGGER.debug("en AvaluosBean#consultarAreasRegistradasPorAvaluo");
        List<AvaluoAreaRegistrada> answer = null;

        try {
            answer = this.avaluoAreaRegistradaService.buscarPorAvaluo(avaluoId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAreasRegistradasPorAvaluo: " +
                ex.getMensaje());
        }

        return answer;

    }

    /**
     * @see IAvaluosLocal#consultarAreasRegistradasPorIdConAtributo(long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public AvaluoAreaRegistrada consultarAreasRegistradasPorIdConAtributo(long idArea) {

        LOGGER.debug("en AvaluosBean#consultarAreasRegistradasPorIdConAtributo");
        AvaluoAreaRegistrada answer = null;

        try {
            answer = this.avaluoAreaRegistradaService.buscarPorIdConAtributos(idArea);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAreasRegistradasPorIdConAtributo: " +
                ex.getMensaje());
        }

        return answer;

    }

    /**
     * @see IAvaluosLocal#guardarActualizarAvaluoAreaRegistradaAvaluo(AvaluoAreaRegistrada,
     * UsuarioDTO)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public AvaluoAreaRegistrada guardarActualizarAvaluoAreaRegistradaAvaluo(
        AvaluoAreaRegistrada area, UsuarioDTO usuario) {

        LOGGER.debug("en AvaluosBean#guardarActualizarAvaluoAreaRegistradaAvaluo");
        AvaluoAreaRegistrada answer = null;

        try {
            if (area.getId() == null) {
                area.setFechaLog(new Date());
                area.setUsuarioLog(usuario.getLogin());
            }
            answer = this.avaluoAreaRegistradaService.update(area);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#guardarActualizarAvaluoAreaRegistradaAvaluo: " +
                ex.getMensaje());
        }
        return answer;

    }

    /**
     * @see IAvaluosLocal#eliminarAvaluoAreaRegistradaAvaluo(AvaluoAreaRegistrada)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public AvaluoAreaRegistrada eliminarAvaluoAreaRegistradaAvaluo(AvaluoAreaRegistrada area) {

        LOGGER.debug("en AvaluosBean#eliminarAvaluoAreaRegistradaAvaluo");
        AvaluoAreaRegistrada answer = null;

        try {
            this.avaluoAreaRegistradaService.delete(area);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#eliminarAvaluoAreaRegistradaAvaluo: " +
                ex.getMensaje());
        }
        return answer;

    }

    /**
     * @see IAvaluosLocal#consultarAvaluoPredioConstruccionPorId(Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public AvaluoPredioConstruccion consultarAvaluoPredioConstruccionPorId(Long idConstruccion) {

        LOGGER.debug("en AvaluosBean#consultarAvaluoPredioConstruccionPorId");
        AvaluoPredioConstruccion answer = null;

        try {
            answer = this.avaluoPredioConstruccionService.findById(idConstruccion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en AvaluosBean#consultarAvaluoPredioConstruccionPorId: " +
                ex.getMensaje());
        }
        return answer;

    }
//end of class
}
