package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;
import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionCompDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fabio.navarrete
 *
 */
@Stateless
public class PUnidadConstruccionCompDAOBean
    extends GenericDAOWithJPA<PUnidadConstruccionComp, Long>
    implements IPUnidadConstruccionCompDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        PUnidadConstruccionCompDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPUnidadConstruccionCompDAO#updateByUnidadConstruccionId(java.util.List)
     */
    @Override
    public void updateByUnidadConstruccionId(
        List<PUnidadConstruccionComp> unidadesConstruccionComp) {

        LOGGER.debug("PUnidadConstruccionCompDAOBean#updatePUnidadConstruccionCompRegistros " +
            "borrando e insertando registros ...");
        List<PUnidadConstruccionComp> registersToDelete;
        List<String[]> criteriaToDelete;
        String[] criterion;

        criteriaToDelete = new ArrayList<String[]>();
        criterion = new String[3];

        criterion[0] = "PUnidadConstruccion.id";
        criterion[1] = "number";
        criterion[2] = unidadesConstruccionComp.get(0).getPUnidadConstruccion().getId().toString();
        criteriaToDelete.add(criterion);

        try {
            registersToDelete = this.findByExactCriteria(criteriaToDelete);
            this.removeMultiple(registersToDelete);
            //N: aquí no hay problema porque todos los de la lista son nuevos, no tienen id
            this.persistMultiple(unidadesConstruccionComp);
            LOGGER.debug("...OK");

        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPUnidadConstruccionCompDAO#deleteByUnidadConstruccionId(java.lang.Long)
     */
    @Override
    public void deleteByUnidadConstruccionId(Long unidadConstruccionId) {

        LOGGER.debug("PUnidadConstruccionCompDAOBean#deleteByUnidadConstruccionId " +
            "borrando registros ...");

        List<String[]> criteriaToDelete;
        String[] criterion;

        criteriaToDelete = new ArrayList<String[]>();
        criterion = new String[3];

        criterion[0] = "PUnidadConstruccion.id";
        criterion[1] = "number";
        criterion[2] = unidadConstruccionId.toString();
        criteriaToDelete.add(criterion);

        try {
            this.deleteByExactCriteria(criteriaToDelete);
            LOGGER.debug("...OK");

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                getExcepcion(LOGGER, e,
                    "PUnidadConstruccionCompDAOBean#deleteByUnidadConstruccionId",
                    "PUnidadConstruccionComp", "?");
        }
    }

    /**
     * @author leidy.gonzalez
     * @see co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionCompDAO
     * #buscarUnidadDeConstruccionPorUnidadConstruccionId(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccionComp> buscarUnidadDeConstruccionPorUnidadConstruccionId(
        Long unidadConstruccionId) {

        List<PUnidadConstruccionComp> answer = null;

        String sql = "SELECT DISTINCT pucc FROM PUnidadConstruccionComp pucc" +
            " WHERE pucc.PUnidadConstruccion.id = :unidadConstruccionId";

        Query query = this.entityManager.createQuery(sql);
        query.setParameter("unidadConstruccionId", unidadConstruccionId);
        try {
            answer = (List<PUnidadConstruccionComp>) query.getResultList();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;

    }

//end of class
}
