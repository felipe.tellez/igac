package co.gov.igac.snc.dao.generales.impl;

import javax.persistence.Query;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.INumeracionDAO;
import co.gov.igac.snc.persistence.entity.generales.Numeracion;

import javax.ejb.Stateless;

@Stateless
public class NumeracionDAOBean extends GenericDAOWithJPA<Numeracion, String>
    implements INumeracionDAO {

    // --------------------------------------------------------------------------------------------------
    /**
     * @see INumeracionDAO#conseguirNumeracionPorId(Integer)
     */
    public Numeracion conseguirNumeracionPorId(Long numeracionId) {

        Query q = entityManager.createQuery("select n from" + " Numeracion n" +
            " where id= :numeracionId");
        q.setParameter("numeracionId", numeracionId);

        Numeracion numeracion = (Numeracion) q.getSingleResult();

        return numeracion;
    }

}
