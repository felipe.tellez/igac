/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales;

import java.util.Date;

import co.gov.igac.persistence.entity.generales.CodigoHomologado;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.ESistema;

import javax.ejb.Local;

/**
 * interface para el DAO de CodigoHomologado
 *
 * @author fredy.wilches
 */
@Local
public interface ICodigoHomologadoDAO extends IGenericJpaDAO<CodigoHomologado, Long> {

    public String obtenerCodigoHomologado(ECodigoHomologado codigo, ESistema origen,
        ESistema destino, String valor, Date fecha);

    public String obtenerCodigo(ECodigoHomologado codigo, ESistema origen, ESistema destino,
        String valor, Date fecha);

}
