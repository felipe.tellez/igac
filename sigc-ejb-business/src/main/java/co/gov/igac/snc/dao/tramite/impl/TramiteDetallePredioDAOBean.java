/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteDetallePredioDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class TramiteDetallePredioDAOBean extends GenericDAOWithJPA<TramiteDetallePredio, Long>
    implements ITramiteDetallePredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDetallePredioDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDetallePredioDAO#getByTramiteId(java.lang.Long)
     */
    @Implement
    @Override
    public List<TramiteDetallePredio> getByTramiteId(Long idTramite) {

        LOGGER.debug("on TramiteDetallePredioDAOBean#getByTramiteId ...");

        List<TramiteDetallePredio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tpe FROM TramiteDetallePredio tpe JOIN FETCH tpe.predio p " +
            " WHERE tpe.tramite.id = :idTramiteP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramiteP", idTramite);
            answer = query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;

    }

    /**
     * @see ITramiteDetallePredioDAO#getByTramiteIdYPredioId(java.lang.Long, java.lang.Long)
     */
    @Implement
    @Override
    public TramiteDetallePredio getByTramiteIdYPredioId(Long idTramite, Long idPredio) {

        LOGGER.debug("on TramiteDetallePredioDAOBean#getByTramiteId ...");

        TramiteDetallePredio answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tpe FROM TramiteDetallePredio tpe JOIN FETCH tpe.predio p " +
            " WHERE tpe.tramite.id = :idTramiteP" +
            " AND p.id =:idPredioP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramiteP", idTramite);
            query.setParameter("idPredioP", idPredio);
            answer = (TramiteDetallePredio) query.getSingleResult();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "TramiteDetallePredioDAOBean#getByTramiteIdYPredioId");
        }

        return answer;

    }

    @Override
    public List<String> getNumerosPredialesByTramiteId(List<Long> tramiteIds) {
        LOGGER.debug("on TramiteDetallePredioDAOBean#getNumerosPredialesByTramiteId ...");
        List<String> numerosPrediales = new ArrayList<String>();
        String queryString = "SELECT tpe.predio.numeroPredial FROM TramiteDetallePredio tpe " +
            " LEFT JOIN tpe.predio " + " LEFT JOIN tpe.tramite " +
            " WHERE tpe.tramite.id in (:tramiteIds)";

        try {
            Query query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIds", tramiteIds);
            numerosPrediales = (List<String>) query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return numerosPrediales;
    }

    /**
     * @see ITramiteDetallePredioDAO#countByTramiteId(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public Integer countByTramiteId(Long idTramite) {

        LOGGER.debug("on TramiteDetallePredioDAOBean#countByTramiteId ...");

        Long answer = null;
        String queryString;
        Query query;

        queryString = "SELECT count(tdp) FROM TramiteDetallePredio tdp " +
            " WHERE tdp.tramite.id = :idTramiteP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramiteP", idTramite);
            answer = (Long) query.getSingleResult();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        if (answer != null) {
            return answer.intValue();
        } else {
            return null;
        }

    }

}
