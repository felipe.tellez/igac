package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoFormato;

/**
 * Servicios de persistencia para el objeto ProductoFormato
 *
 * @author javier.barajas
 */
@Local
public interface IProductoFormatoDAO extends IGenericJpaDAO<ProductoFormato, Long> {

    /**
     * Método que retorna la lista de formato de productos por el campo formato
     *
     * @param producto Referirse a el Producto para encontrar los valores
     * @author leidy.gonzalez
     */
    public List<ProductoFormato> findByIdProducto(Long productoId);

}
