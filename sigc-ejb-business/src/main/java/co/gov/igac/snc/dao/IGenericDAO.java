package co.gov.igac.snc.dao;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IGenericDAO {

    /**
     * Retorna un entity a partir de su id
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public Object findById(Class clazz, Long id);

    /**
     * Elimina un entity en la base de datos
     *
     * @param o
     * @author fabio.navarrete
     */
    public void delete(Object o);

    /**
     * Realiza una operación merge al objeto ingresado
     *
     * @param o
     * @return
     * @author fabio.navarrete
     */
    public Object update(Object o);

    /**
     * Retorna todos los objetos de una tabla
     *
     * @param clazz
     * @param rowStartIdxAndCount
     * @return
     * @author fabio.navarrete
     */
    public List findAll(Class clazz, final int... rowStartIdxAndCount);

    /**
     * Setter para el entityManager del dao
     *
     * @param em
     * @author fabio.navarrete
     */
    public void setEntityManager(EntityManager em);

    /**
     * Getter para el entityManager del dao
     *
     * @return
     * @author fabio.navarrete
     */
    public EntityManager getEntityManager();

    /**
     * Realiza una operación merge multiple a la lista de objetos ingresados
     *
     * @param list o
     * @author javier.aponte
     */
    public void updateMultiple(List<Object> o);
}
