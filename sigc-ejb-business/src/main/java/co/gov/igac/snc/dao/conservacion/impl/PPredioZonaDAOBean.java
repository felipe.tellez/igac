package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sjl.io.process.ProcessInput.Prepared;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.Date;
import java.util.logging.Level;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
public class PPredioZonaDAOBean extends GenericDAOWithJPA<PPredioZona, Long>
    implements IPPredioZonaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioZonaDAOBean.class);

    // ---------------------------------//
    /**
     * Método que busca las zonas homogéneas de un PPredio por su id.
     *
     * @see IPPredioZonaDAO#buscarZonasHomogeneasPorPPredioId(String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Implement
    public List<PPredioZona> buscarZonasHomogeneasPorPPredioId(Long idPPredio) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        // Se deben mostrar las zonas de la última vigencia
        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio " +
            "AND (ppz.cancelaInscribe != 'C') " +
            " and ppz.area>0 AND ppz.vigencia  " +
            "IN( SELECT MAX(ppz.vigencia) FROM ppz WHERE ppz.PPredio.id = :idPPredio)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarZonasHomogeneasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see IPPredioZonaDAO#updateAreaPorPredioZonaByPredioId(List, List)
     */
    //* @modified by javier.aponte se guarda el valor del área truncado a 2 cifras decimales refs#12022
    @Override
    public boolean updateAreaPorPredioZonaByPredioId(String numeroPredial,
        Double area) {

        String query = "SELECT ppz FROM PPredioZona ppz " +
            " JOIN ppz.PPredio pp " +
            " WHERE pp.numeroPredial =:numeroPredial " +
            " AND (ppz.cancelaInscribe !=:cancelado OR ppz.cancelaInscribe IS NULL) ";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("numeroPredial", numeroPredial);
            q.setParameter("cancelado", EProyeccionCancelaInscribe.CANCELA.getCodigo());

            List<PPredioZona> prediozonas = (List<PPredioZona>) q.getResultList();

            if (prediozonas != null && !prediozonas.isEmpty() && prediozonas.size() == 1) {

                //Se redondea el número a dos cifras decimales
                prediozonas.get(0).setArea(Utilidades.redondearNumeroADosCifrasDecimales(area.
                    doubleValue()));
                this.update(prediozonas.get(0));
                return true;
            } else if (prediozonas != null && !prediozonas.isEmpty() && prediozonas.size() > 1) {
                return true;
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioZonaDAOBean#updateAreaPorPredioZonaByPredioId");
        }
        return false;

    }

    /**
     *
     * @author felipe.cadena
     * @see IPPredioZonaDAO#buscarZonasPorPPredioId
     */
    @Implement
    @Override
    public List<PPredioZona> buscarZonasPorPPredioId(Long idPPredio) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        // Se deben mostrar las zonas de la última vigencia
        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio " +
            "AND ppz.vigencia " +
            "IN( SELECT MAX(ppz.vigencia) FROM ppz WHERE ppz.PPredio.id = :idPPredio)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarZonasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author felipe.cadena
     * @see IPPredioZonaDAO#buscarZonasPorPPredioIdYEstadoP
     */
    @Implement
    @Override
    public List<PPredioZona> buscarZonasPorPPredioIdYEstadoP(Long idPPredio, String cancelaInscribe) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        // Se deben mostrar las zonas de la última vigencia
        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio ";
        if (cancelaInscribe != null) {
            queryString += "AND (ppz.cancelaInscribe = :cancelaInscribe) ";
        }
        queryString += "AND ppz.vigencia " +
            "IN( SELECT MAX(ppz.vigencia) FROM ppz WHERE ppz.PPredio.id = :idPPredio)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            if (cancelaInscribe != null) {
                query.setParameter("cancelaInscribe", cancelaInscribe);
            }
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarZonasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author felipe.cadena
     * @see IPPredioZonaDAO#buscarZonasPorPPredioIdYEstadoP
     */
    @Implement
    @Override
    public List<PPredioZona> buscarTodasZonasPorPPredioIdYEstadoP(Long idPPredio,
        String cancelaInscribe) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        // Se deben mostrar las zonas de la última vigencia
        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio " +
            "AND (ppz.cancelaInscribe = :cancelaInscribe) ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            query.setParameter("cancelaInscribe", cancelaInscribe);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarZonasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author felipe.cadena
     * @see IPPredioZonaDAO#buscarTodasZonasPorPPredioId
     */
    @Implement
    @Override
    public List<PPredioZona> buscarTodasZonasPorPPredioId(Long idPPredio) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio order by ppz.vigencia desc,ppz.area desc  ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarTodasZonasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public void delete(PPredioZona entity) {
        LOGGER.debug("Eliminando " + this.getClass().getSimpleName(), Level.INFO,
            null);
        try {
            entity = this.entityManager.merge(entity);

            this.entityManager.remove(entity);
            LOGGER.debug("Eliminación exitosa de entidad de tipo " +
                this.getClass().getSimpleName());
            this.entityManager.flush();
            LOGGER.debug("Transaccion committed ");
        } catch (RuntimeException re) {
            LOGGER.error("Eliminación fallida", Level.SEVERE, re);
            throw re;
        }

    }

    // ---------------------------------//
    /**
     * Método que busca las zonas homogéneas de un PPredio por su número predial.
     *
     * @see IPPredioZonaDAO#buscarZonasHomogeneasPorNumeroPredialPPredio(String)
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Implement
    public List<PPredioZona> buscarZonasHomogeneasPorNumeroPredialPPredio(String numeroPredial) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        // Se deben mostrar las zonas de la última vigencia
        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.numeroPredial = :numeroPredial " +
            "AND (ppz.cancelaInscribe != 'C') " +
            "AND ppz.vigencia " +
            "IN( SELECT MAX(ppz.vigencia) FROM ppz WHERE ppz.PPredio.numeroPredial = :numeroPredial)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("numeroPredial", numeroPredial);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error(
                "Error on PPredioZonaDAOBean#buscarZonasHomogeneasPorNumeroPredialPPredio: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioZonaDAO#buscarZonasPorPPredioIdYVigencia
     */
    @Override
    public List<PPredioZona> buscarZonasPorPPredioIdYVigencia(Long idPPredio, Date vigencia) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio " +
            "AND ppz.vigencia = :vigencia ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            query.setParameter("vigencia", vigencia);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarTodasZonasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioZonaDAO#buscarZonasNoCanceladaNiFechaInsMayorPorPPredioId
     */
    @Implement
    @Override
    public List<PPredioZona> buscarZonasNoCanceladaPorPPredioId(Long idPPredio) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "WHERE pp.id = :idPPredio " +
            "AND (ppz.cancelaInscribe != :cancelado or ppz.cancelaInscribe is null)  order by ppz.vigencia desc,ppz.area desc ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPPredio", idPPredio);
            query.setParameter("cancelado", EProyeccionCancelaInscribe.CANCELA.getCodigo());
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarTodasZonasPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioZonaDAO#buscarZonasTempPorIdTramite
     */
    @Override
    public List<PPredioZona> buscarZonasTempPorIdTramite(Long idTram) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ppz FROM PPredioZona ppz " +
            "LEFT JOIN FETCH ppz.PPredio pp " +
            "LEFT JOIN FETCH pp.tramite t " +
            "WHERE t.id = :idTram " +
            "AND ppz.cancelaInscribe = :temporal ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTram", idTram);
            query.setParameter("temporal", EProyeccionCancelaInscribe.TEMP.getCodigo());
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioZonaDAOBean#buscarZonasTempPorIdTramite: " +
                ex.getMessage());
        }
        return answer;
    }
    
    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioZonaDAO#obtenerPZonasPorIds
     */
    @Override
    public List<PPredioZona> obtenerPZonasPorIds(List<Long> id) {

        List<PPredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT pz FROM PPredioZona pz " +
            "WHERE pz.id IN (:id) ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("id", id);
            answer = (List<PPredioZona>)query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PredioZonaDAOBean#obtenerPZonasPorIds: " +
                ex.getMessage());
        }
        return answer;
    }

}
