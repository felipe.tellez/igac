package co.gov.igac.snc.dao.generales.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.io.FileUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.DocumentoBloqueoDTO;
import co.gov.igac.generales.dto.DocumentoMemorandoDTO;
import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.DocumentosServiceExceptions;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.comun.colecciones.Pair;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IGeneralesLocal;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EErrorProcedimientoSQL;
import co.gov.igac.snc.persistence.util.EExcepcionSNC;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoCorrespondencia;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.math.BigDecimal;
import java.util.HashMap;
import javax.persistence.TypedQuery;

@Stateless
public class DocumentoDAOBean extends GenericDAOWithJPA<Documento, Long>
    implements IDocumentoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DominioDAOBean.class);

    private static final String FILESEPARATOR = System.getProperty("file.separator");

    @EJB
    private ITipoDocumentoDAO remoteTipoDocumentoService;

    @EJB
    private ITramiteDAO tramiteDAO;

    @EJB
    private IPredioDAO remotePredioDAO;

    @EJB
    private IPPredioDAO remotePPredioDAO;

    /**
     * Método utilizado en las pruebas unitarias
     *
     * @param tramiteDAO
     */
    public void setTramiteDAO(ITramiteDAO tramiteDAO) {
        this.tramiteDAO = tramiteDAO;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see co.gov.igac.snc.dao.generales.IDocumentoDAO#guardarDocumentoGestorDocumental(UsuarioDTO,
     * DocumentoTramiteDTO)
     */
    @Implement
    @Override
    public Documento guardarDocumentoGestorDocumental(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO,
        Documento documento) {
        LOGGER.debug("DocumentoDAOBean#guardarDocumentoAlfresco");
        Documento respuesta = null;

        String rutaAlfresco = null;
        IDocumentosService servicioDocumental = null;
        try {
            servicioDocumental = DocumentalServiceFactory.getService();
            this.borrarDocumentoGestorDocumental(documento);
        } catch (ExcepcionSNC e) {
            //no debe hacer nada si al intentar borrar se presenta fallo
        }

        try {
            rutaAlfresco = servicioDocumental.cargarDocumentoTramite(documentoTramiteDTO, usuario);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_40001.getExcepcion(LOGGER, e,
                SncBusinessServiceExceptions.EXCEPCION_40001.getMessage(),
                "DocumentoDAOBean#guardarDocumentoAlfresco");
        }

        try {
            if (rutaAlfresco != null && !rutaAlfresco.isEmpty()) {
                // felipe.cadena::se borra el archivo anterior de alfresco
                // this.borrarDocumentoGestorDocumental(documento);

                documento.setIdRepositorioDocumentos(rutaAlfresco);

                if (documento.getArchivo() != null) {
                    documento.setArchivo(documento.getArchivo()
                        .replace(FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
                }

                respuesta = update(documento);
                respuesta.getTipoDocumento().getNombre();
            }

        } catch (ExcepcionSNC e) {
            try {
                respuesta = update(documento);
            } catch (ExcepcionSNC ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, e, e.
                    getMessage(),
                    "DocumentoDAOBean#guardarDocumentoAlfresco");
            }

        }
        return respuesta;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see
     * co.gov.igac.snc.dao.generales.IDocumentoDAO#guardarFotoPredioGestorDocumental(UsuarioDTO,
     * DocumentoTramiteDTO)
     */
    @Override
    public Documento guardarFotoPredioGestorDocumental(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) {
        LOGGER.debug("DocumentoDAOBean#guardarFotoPredioAlfresco");
        Documento respuesta = null;

        try {

            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();
            String rutaFtp = servicioDocumental.cargarDocumentoPredioFoto(
                documentoTramiteDTO, usuario);
            if (rutaFtp != null && !rutaFtp.isEmpty()) {
                documento.setIdRepositorioDocumentos(rutaFtp);

                if (documento.getArchivo() != null) {
                    documento.setArchivo(documento.getArchivo().replace(
                        FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
                }

                respuesta = update(documento);
            }

        } catch (ExcepcionSNC e) {
            if (e.getCodigoDeExcepcion().equals(
                EExcepcionSNC.ERROR_DURANTE_LA_EJECUCION.getCodigo())) {
                respuesta = update(documento);
            } else {
                throw SncBusinessServiceExceptions.EXCEPCION_100010
                    .getExcepcion(LOGGER, e, e.getMessage(),
                        "DocumentoDAOBean#guardarFotoPredioAlfresco");
            }
        }
        return respuesta;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#nombreFinalDocumento(String, String, String, String)
     * @author juan.agudelo
     */
    @Override
    public String nombreFinalDocumento(String rutaDocumento,
        String idObjetoAsociado, String tipoDocId, String documentoId) {
        LOGGER.debug("DocumentoDAOBean#nombreFinalDocumento");

        String nombreF[] = rutaDocumento
            .split(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);

        StringBuilder nombreTemporalDocumento = new StringBuilder();

        // Compenente inical de la ruta del documento
        nombreTemporalDocumento.append(nombreF[0]);
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // Identificador definitivo del obejto asociado puede ser trámite o
        // predio
        String idOA = org.apache.commons.lang.StringUtils.leftPad(
            idObjetoAsociado, 10, "0");
        nombreTemporalDocumento.append(idOA);
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // Tipo de documento ID
        String idTD = org.apache.commons.lang.StringUtils.leftPad(tipoDocId, 5,
            "0");
        nombreTemporalDocumento.append(idTD);
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // Código N solo aplica para Fótografias y corresponde al identificador
        // de la unidad de construcción
        nombreTemporalDocumento.append(nombreF[3]);
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // ID del documento en la base de datos, como no existe el documento el
        // la DB sera una cadena de ceros
        String idDoc = org.apache.commons.lang.StringUtils.leftPad(documentoId,
            10, "0");
        nombreTemporalDocumento.append(idDoc);
        // punto de separación del nombre y la extensión del archivo
        nombreTemporalDocumento.append(".");
        // extension del documento
        String extension[] = nombreF[nombreF.length - 1].split("\\.");
        nombreTemporalDocumento.append(extension[extension.length - 1]);

        LOGGER.debug("Nombre: " + nombreTemporalDocumento.toString());
        return nombreTemporalDocumento.toString();
    }

//---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#actualizarDocumentoYGestorDocumental(UsuarioDTO, DocumentoTramiteDTO,
     * Documento)
     * @author juan.agudelo
     */
    @Override
    public boolean actualizarDocumentoYGestorDocumental(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) {
        try {

            if (documento.getArchivo() != null) {
                documento.setArchivo(documento.getArchivo().replace(
                    FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
            }

            guardarDocumentoGestorDocumental(usuario, documentoTramiteDTO, documento);
            update(documento);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#guardarDocumento(UsuarioDTO, DocumentoTramiteDTO, Documento)
     * @author juan.agudelo
     */
    @Override
    public Documento guardarDocumento(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) {
        Documento doc = null;
        try {

            doc = guardarDocumentoGestorDocumental(usuario, documentoTramiteDTO,
                documento);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#guardarDocumento");
        }
        return doc;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see IDocumentoDAO#buscarDocumentoAlfresco(String)
     */
//    @Override
//	public DocumentoNombreMimeDTO buscarDocumentoAlfresco(
//			String idRepositorioDocumentos) {
//		LOGGER.debug("DocumentoDAOBean#buscarDocumentoAlfresco");
//		DocumentoNombreMimeDTO doc = null;
//
//		try {
//			IDocumentosService servicioDocumental = DocumentalServiceFactory
//					.getService();
//			doc = servicioDocumental.obtenerDocumento(idRepositorioDocumentos);
//		} catch (ExcepcionSNC e) {
//			LOGGER.error(e.getMessage(), e);
//			throw e;
//		}
//		return doc;
//	}
//--------------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#actualizarDocumentoYSubirArchivoAlfresco(UsuarioDTO, DocumentoTramiteDTO,
     * Documento)
     * @author david.cifuentes
     */
    @Override
    public Documento actualizarDocumentoYSubirArchivoAlfresco(
        UsuarioDTO usuario, DocumentoTramiteDTO documentoTramiteDTO,
        Documento documento) {
        Documento doc = null;
        try {

            if (documento.getArchivo() != null) {
                documento.setArchivo(documento.getArchivo().replace(
                    FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
            }

            doc = update(documento);
            doc = guardarDocumentoGestorDocumental(usuario, documentoTramiteDTO, doc);
            if (doc != null) {
                return doc;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return doc;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#parseDocumento(Object[])
     * @author juan.agudelo
     */
    /*
     * @modified felipe.cadena -Se hace manejo del error cuando no se encuentra un documento
     * asociado al número de radicación
     */
    @SuppressWarnings("unchecked")
    @Override
    public Documento parseDocumento(Object[] radicadoCorrespondenciaSP) {

        // MANEJO DE ERRORES
        if (((ArrayList<Object>) radicadoCorrespondenciaSP[1]).size() > 0) {
            Object[] erroresRadicado =
                ((Object[]) ((ArrayList<Object>) radicadoCorrespondenciaSP[1])
                    .get(0));
            //TODO::felipe.cadena::queda pendiente hacer la validación de los demas errores que puedan venir en el cursor.
            if (erroresRadicado[0].equals(EErrorProcedimientoSQL.COD_100.getCodigo())) {
                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                    getExcepcion(LOGGER);
            } else {
                return null;
            }
        }

        // MANEJO DE RESULTADOS
        if (((ArrayList<Object>) radicadoCorrespondenciaSP[0]).size() > 0) {
            Object[] radicadoCorrespondencia =
                ((Object[]) ((ArrayList<Object>) radicadoCorrespondenciaSP[0])
                    .get(0));
            Documento documento = new Documento();
            documento.setNumeroRadicacion((String) radicadoCorrespondencia[30]);
            documento.setFechaRadicacion((Date) radicadoCorrespondencia[31]);
            documento.setEstructuraOrganizacionalCod((String) radicadoCorrespondencia[0]);

            documento.setTipoDocumento(this.remoteTipoDocumentoService
                .findById(ETipoDocumento.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId()));

            // Descripcion temporalmente tendra la entidad generadora del
            // documento
            if (ETipoCorrespondencia.INTERNA_RECIBIDA.getCodigo().equals(
                radicadoCorrespondencia[2])) {
                documento
                    .setEstructuraOrganizacionalCod((String) radicadoCorrespondencia[0] +
                        " " + (String) radicadoCorrespondencia[8]);
            } else if (ETipoCorrespondencia.EXTERNA_RECIBIDA.getCodigo()
                .equals(radicadoCorrespondencia[2])) {
                documento
                    .setNombreEntidadExterna((String) radicadoCorrespondencia[27]);

            }

            return documento;
        }
        return null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#descargarOficioDeGestorDocumentalATempLocalMaquina(String idAlfresco)
     * @author david.cifuentes
     */
    /*
     * @modified juan.agudelo @modified pedro.garcia lanzar excepción de las definidas
     */
 /*
     * @modified by juanfelipe.garcia :: 25-11-2013 :: Cambio en los métodos del gestor documental
     * versión 1.3
     */
    @Implement
    @Override
    public String descargarOficioDeGestorDocumentalATempLocalMaquina(String idAlfresco) {

        String url = "";
        String destino = FileUtils.getTempDirectory().getAbsolutePath();
        IDocumentosService servicioDocumental;

        try {
            servicioDocumental = DocumentalServiceFactory.getService();
            //version 1.3
            url = servicioDocumental.descargarDocumento(idAlfresco);
            //url = servicioDocumental.descargarDocumento(idAlfresco, destino);
        } catch (Exception e) {
            throw DocumentosServiceExceptions.EXCEPCION_0003.getExcepcion(LOGGER, e, e.getMessage());
        }
        return url;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que busca las resoluciones en los tramites, buscados por numero predial
     *
     * @see IDocumentoDAO#buscarResolucionesByNumeroPredialPredio(String numeroPredial)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Documento> buscarResolucionesByNumeroPredialPredio(
        String numeroPredial) {
        LOGGER.debug("executing DocumentoDAOBean#buscarResolucionesByNumeroPredialPredio");

        List<Documento> answer = new ArrayList<Documento>();
        Query query;
        String claseTipoDocumento = ETipoDocumentoClase.RESOLUCION.toString();
        String queryToExecute;

        queryToExecute = "SELECT DISTINCT doc FROM VTramitePredio vtp " +
            " JOIN vtp.tramite t " +
            " JOIN t.resultadoDocumento doc " +
            " JOIN vtp.predio p " +
            " WHERE p.numeroPredial = :numeroPredial " +
            " AND doc.tipoDocumento.clase = :claseTipoDocumento ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("numeroPredial", numeroPredial);
        query.setParameter("claseTipoDocumento", claseTipoDocumento);

        try {
            answer = (ArrayList<Documento>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarResolucionesContraProcedeByNumeroPredialPredio: " +
                "La consulta de trámites de un predio no devolvió resultados");
            return answer;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de trámites de predio: " +
                ex.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#buscarDocumentoResultadoPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public String buscarDocumentoResultadoPorTramiteId(Long tramiteId) {

        String documentoResultado = "";
        Query query;
        String q;

        q = "SELECT d FROM Documento d" + " WHERE d.tramiteId = :tramiteId" +
            " AND  d.id =" + " (SELECT r.id FROM Tramite t" +
            " JOIN t.resultadoDocumento r" + " WHERE t.id = :tramiteId)";

        query = this.entityManager.createQuery(q);
        query.setParameter("tramiteId", tramiteId);
        try {
            Documento d = (Documento) query.getSingleResult();
            if (d.getIdRepositorioDocumentos() != null &&
                !d.getIdRepositorioDocumentos().isEmpty()) {
                documentoResultado = descargarOficioDeGestorDocumentalATempLocalMaquina(d
                    .getIdRepositorioDocumentos());
            }

        } catch (NoResultException ne) {
            return null;
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#buscarDocumentoResultadoPorTramiteId");
        }

        return documentoResultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#recuperarRutaGestorDocumentalImagenInicialYfinalDelTramite(Long)
     * @author juan.agudelo
     */
    @Implement
    @Override
    public String[] recuperarRutaGestorDocumentalImagenInicialYfinalDelTramite(Long tramiteId) {

        String[] rutasAlfresco = null;
        Query query1;
        String q;
        Query query2;
        Documento documentoInicial, documentoFinal;

        q = "SELECT d FROM Documento d" + " JOIN d.tipoDocumento td" +
            " WHERE d.tramiteId = :tramiteId" +
            " AND td.id = :tipoDocumentoId";

        query1 = this.entityManager.createQuery(q);
        query1.setParameter("tramiteId", tramiteId);
        query1.setParameter("tipoDocumentoId",
            ETipoDocumentoId.IMA_GEO_PREDIO_ANTES_TRAMITE.getId());
        query2 = entityManager.createQuery(q);
        query2.setParameter("tramiteId", tramiteId);
        query2.setParameter("tipoDocumentoId",
            ETipoDocumentoId.IMA_GEO_PREDIO_DESPUES_TRAMITE.getId());
        try {
            documentoInicial = (Documento) query1.getSingleResult();
            documentoFinal = (Documento) query2.getSingleResult();

            if (documentoInicial.getIdRepositorioDocumentos() != null &&
                !documentoInicial.getIdRepositorioDocumentos().isEmpty() &&
                documentoFinal.getIdRepositorioDocumentos() != null &&
                !documentoFinal.getIdRepositorioDocumentos().isEmpty()) {
                rutasAlfresco = new String[2];

                rutasAlfresco[0] = descargarOficioDeGestorDocumentalATempLocalMaquina(
                    documentoInicial
                        .getIdRepositorioDocumentos());

                rutasAlfresco[1] = descargarOficioDeGestorDocumentalATempLocalMaquina(documentoFinal
                    .getIdRepositorioDocumentos());
            }

        } catch (NoResultException nr) {
            return rutasAlfresco = new String[2];
        } catch (IndexOutOfBoundsException iobe) {
            return rutasAlfresco = new String[2];
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010
                .getExcepcion(LOGGER, e, e.getMessage(),
                    "DocumentoDAOBean#recuperarRutaAlfrescoImagenInicialYfinalDelTramite");
        }

        return rutasAlfresco;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see
     * co.gov.igac.snc.dao.generales.IDocumentoDAO#guardarMemorandoConservacionGestorDocumental(DocumentoMemorandoDTO,
     * Documento)
     */
    @Override
    public Documento guardarMemorandoConservacionGestorDocumental(
        DocumentoMemorandoDTO documentoMemorandoDTO, Documento documento) {
        LOGGER.debug("En DocumentoDAOBean#guardarMemorandoConservacionAlfresco");
        Documento answer = null;

        try {

            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();
            String rutaAlfresco = servicioDocumental
                .cargarMemorandoConservacion(documentoMemorandoDTO);
            if (rutaAlfresco != null && !rutaAlfresco.isEmpty()) {
                documento.setIdRepositorioDocumentos(rutaAlfresco);

                if (documento.getArchivo() != null) {
                    //N: lo que tenía en el campo 'archivo' era la ruta completa, pero luego queda 
                    //  como solo el nombre del archivo (que es el que se muestra en web)
                    documento.setArchivo(documento.getArchivo().replace(
                        FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error guardando el memorando en alfresco ");
            throw ex;
        }

        try {
            answer = update(documento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error actualizando en bd el memorando que se acaba de guardar en alfreso");
            throw ex;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#almacenarDocumentoAlfresco(UsuarioDTO, Documento)
     * @author fredy.wilches
     */
    @Override
    public Documento clasificarAlmacenamientoDocumentoGestorDocumental(UsuarioDTO usuario,
        Documento documento) {

        Documento doc = new Documento();
        IDocumentosService service = DocumentalServiceFactory.getService();
        DocumentoTramiteDTO documentoTramiteDTO;

        File archivo = new File(documento.getArchivo());

        if (documento.getTramiteId() == null && documento.getPredioId() == null) {
            return null;
        } else {
            if ((documento.getTramiteId() != null) ||
                (documento.getTramiteId() != null && documento
                .getPredioId() == null)) {
                String nombreDepartamento = "";
                String nombreMunicipio = "";
                String numeroPredial = "";
                String tipoTramite = "";
                TipoDocumento tipoDocumento = null;
                Date fechaCreacionTramite = null;
                Long tramiteId = null;

                Tramite tTemp = this.tramiteDAO
                    .findTramitePrediosSimplesByTramiteId(documento
                        .getTramiteId());
                if (tTemp != null) {
                    tramiteId = tTemp.getId();
                    fechaCreacionTramite = tTemp.getFechaRadicacion();
                    tipoTramite = tTemp.getTipoTramite();
                    nombreDepartamento = tTemp.getDepartamento().getNombre();
                    nombreMunicipio = tTemp.getMunicipio().getNombre();
                    if (tTemp.getPredio() != null) {
                        numeroPredial = tTemp.getPredio().getNumeroPredial();
                    } else if (tTemp.getTramitePredioEnglobes() != null &&
                        !tTemp.getTramitePredioEnglobes().isEmpty()) {
                        numeroPredial = tTemp.getTramitePredioEnglobes().get(0)
                            .getPredio().getNumeroPredial();
                    }
                    if (documento.getTipoDocumento().getId() != null &&
                        documento.getTipoDocumento().getNombre() == null) {
                        tipoDocumento = this.remoteTipoDocumentoService
                            .findById(documento.getTipoDocumento().getId());
                    } else {
                        tipoDocumento = documento.getTipoDocumento();
                    }

                    if (archivo.exists()) {
                        documentoTramiteDTO = new DocumentoTramiteDTO(
                            archivo.getAbsolutePath(),
                            tipoDocumento.getNombre(), fechaCreacionTramite,
                            nombreDepartamento, nombreMunicipio, numeroPredial,
                            tipoTramite, tramiteId);
                    } else {
                        documentoTramiteDTO = new DocumentoTramiteDTO(new File(
                            FileUtils.getTempDirectory().getAbsolutePath(),
                            documento.getArchivo()).getAbsolutePath(),
                            tipoDocumento.getNombre(), fechaCreacionTramite,
                            nombreDepartamento, nombreMunicipio, numeroPredial,
                            tipoTramite, tramiteId);
                    }

                    // Validación adicional del número predial.
                    // Si es un trámite de mutación quinta o una solicitud de
                    // vía gubernativa.
                    boolean numeroPredialAsociado = true;
                    if (tTemp.isQuinta() ||
                        (tTemp.getSolicitud() != null && tTemp
                        .getSolicitud().isViaGubernativa())) {

                        // Tiene un número predial asociado.
                        if (numeroPredial != null &&
                            !numeroPredial.trim().isEmpty()) {
                            numeroPredialAsociado = true;
                        } else {
                            numeroPredialAsociado = false;
                        }
                    }

                    if (numeroPredialAsociado) {
                        // Tiene un número predial asociado
                        doc = guardarDocumento(usuario, documentoTramiteDTO,
                            documento);
                    } else {
                        // No se tiene un número predial asociado
                        doc = guardarDocumentoGestorDocumentalSinNumeroPredial(usuario,
                            documentoTramiteDTO, documento);
                    }
                }
            } else if (documento.getTramiteId() == null &&
                documento.getPredioId() != null) {
                Predio pTemp = this.remotePredioDAO.getPredioById(documento
                    .getPredioId());
                if (pTemp != null) {

                    String rutaArchivo;

                    if (archivo.exists()) {
                        rutaArchivo = documento.getArchivo();
                    } else {
                        rutaArchivo = FileUtils.getTempDirectory().getAbsolutePath() +
                            FILESEPARATOR + documento.getArchivo();
                    }
                    Date fechaCreacionTramite = new Date();

                    documentoTramiteDTO = DocumentoTramiteDTO
                        .crearArgumentoCargarDocumentoPredioFoto(
                            rutaArchivo, fechaCreacionTramite,
                            pTemp.getNumeroPredial());
                    service.cargarDocumentoPredioFoto(documentoTramiteDTO,
                        usuario);

                    doc = guardarFotoPredioGestorDocumental(usuario,
                        documentoTramiteDTO, documento);
                }
            }
            return doc;
        }
    }

//---------------------------------------------------------------------------------------------
    /**
     * @see
     * IDocumentoDAO#almacenarDocumentoAlfresco(co.gov.igac.snc.persistence.entity.generales.Documento,
     * co.gov.igac.generales.dto.UsuarioDTO, java.lang.String)
     * @author juan.agudelo
     */
    /*
     * @modified by fredy.wilches @modified by david.cifuentes
     */
    @Override
    public Documento almacenarDocumentoFotoEnGestorDocumental(Documento documento,
        UsuarioDTO usuario,
        String rutaLocal) {

        String idDocumentoEnRepositorio;

        PPredio pTemp = this.remotePPredioDAO.findById(documento.getPredioId());

        Tramite t = this.tramiteDAO.findById(documento.getTramiteId());

        Date fechaRadicacion;
        if (t == null) {
            fechaRadicacion = new Date();
        } else {
            fechaRadicacion = t.getFechaRadicacion();
        }
        //felipe.cadena:: se utiliza fecha default para cuando la foto no esta relacionada a un tramite:: #7799
        DocumentoTramiteDTO documentoTramiteDTO = DocumentoTramiteDTO
            .crearArgumentoCargarDocumentoPredioFoto(
                rutaLocal, fechaRadicacion,
                pTemp.getNumeroPredial());
        try {

            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            idDocumentoEnRepositorio = servicioDocumental.cargarDocumentoPredioFoto(
                documentoTramiteDTO, usuario);
            if (idDocumentoEnRepositorio != null && !idDocumentoEnRepositorio.isEmpty()) {
                documento.setIdRepositorioDocumentos(idDocumentoEnRepositorio);
            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.getExcepcion(
                LOGGER, e, "DocumentoDAOBean#almacenarDocumentoAlfresco");
        }
        return documento;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#almacenarDocumentosBloqueoGestorDocumental(UsuarioDTO, Documento, List,
     * List)
     * @author juan.agudelo
     */
    @Override
    public Map<String, String> almacenarDocumentosBloqueoGestorDocumental(
        UsuarioDTO usuario, Documento documento, List<Persona> personas,
        List<Predio> predios) {

        @SuppressWarnings("unused")
        IDocumentosService service;
        IDocumentosService servicioDocumental;
        String fileSeparator1 = System.getProperty("file.separator");

        String rutaArchivo;
        DocumentoBloqueoDTO documentoBloqueoDTO;
        List<String> numerosPrediales;
        List<Pair<String, String>> numerosIdentificacion;
        String tipoDocumento = null;
        Map<String, String> rutasAlfresco = null;
        Pair<String, String> numeroIdentificacion;

        if (documento.getTipoDocumento().getId() == null) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(
                LOGGER, null, "tipoDocumento");
        } else if (documento.getArchivo() == null ||
            (documento.getArchivo() != null && documento.getArchivo()
            .isEmpty())) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(
                LOGGER, null, "archivo");
        } else {

            try {
                service = DocumentalServiceFactory.getService();
                servicioDocumental = DocumentalServiceFactory.getService();
                numerosPrediales = new ArrayList<String>();

                if (predios != null && !predios.isEmpty()) {
                    rutaArchivo = FileUtils.getTempDirectory().getAbsolutePath() +
                        fileSeparator1 + documento.getArchivo();

                    if (documento
                        .getTipoDocumento()
                        .getId()
                        .equals(ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PREDIO
                            .getId())) {
                        tipoDocumento = ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PREDIO
                            .toString();
                    } else if (documento
                        .getTipoDocumento()
                        .getId()
                        .equals(ETipoDocumentoId.DOC_SOPORTE_DESBLOQUEO_PREDIO
                            .getId())) {
                        tipoDocumento = ETipoDocumentoId.DOC_SOPORTE_DESBLOQUEO_PREDIO
                            .toString();
                    }

                    documentoBloqueoDTO = new DocumentoBloqueoDTO(rutaArchivo,
                        tipoDocumento, new Date());

                    documento.setFechaLog(new Date());
                    documento.setUsuarioLog(usuario.getLogin());

                    for (Predio p : predios) {
                        numerosPrediales.add(p.getNumeroPredial());
                    }

                    rutasAlfresco = servicioDocumental
                        .cargarDocumentoBloqueoPredios(documentoBloqueoDTO,
                            numerosPrediales, usuario);

                } else if (personas != null && !personas.isEmpty()) {
                    rutaArchivo = FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator1 +
                        documento.getArchivo();
                    numerosIdentificacion = new ArrayList<Pair<String, String>>();

                    if (documento
                        .getTipoDocumento()
                        .getId()
                        .equals(ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PERSONA
                            .getId())) {
                        tipoDocumento = ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PERSONA
                            .toString();
                    } else if (documento
                        .getTipoDocumento()
                        .getId()
                        .equals(ETipoDocumentoId.DOC_SOPORTE_DESBLOQUEO_PERSONA
                            .getId())) {
                        tipoDocumento = ETipoDocumentoId.DOC_SOPORTE_DESBLOQUEO_PERSONA
                            .toString();
                    }

                    documentoBloqueoDTO = new DocumentoBloqueoDTO(rutaArchivo,
                        tipoDocumento, new Date());

                    documento.setFechaLog(new Date());
                    documento.setUsuarioLog(usuario.getLogin());
                    int indice = 0;
                    for (Persona persona : personas) {
                        if (persona != null) {
                            numeroIdentificacion = new Pair<String, String>();
                            numeroIdentificacion.setFirst(persona
                                .getNumeroIdentificacion());
                            numeroIdentificacion.setSecond("_" + (++indice) + "_" + persona.
                                getNombre());
                            // Se puede dar que las personas sean las mismas 
                            if (!numerosIdentificacion.contains(numeroIdentificacion)) {
                                numerosIdentificacion.add(numeroIdentificacion);
                            }
                        }
                    }

                    rutasAlfresco = servicioDocumental
                        .cargarDocumentoBloqueoPersonas(
                            documentoBloqueoDTO, numerosIdentificacion,
                            usuario);
                }

            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010
                    .getExcepcion(LOGGER, e, e.getMessage(),
                        "DocumentoDAOBean#almacenarDocumentosBloqueoAlfresco");
            }
        }
        return rutasAlfresco;
    }

    // --------------------------------------- //
    /**
     * @author david.cifuentes
     * @see
     * co.gov.igac.snc.dao.generales.IDocumentoDAO#guardarDocumentoAlfrescoSinNumeroPredial(UsuarioDTO,
     * DocumentoTramiteDTO)
     */
    @Override
    public Documento guardarDocumentoGestorDocumentalSinNumeroPredial(
        UsuarioDTO usuario, DocumentoTramiteDTO documentoTramiteDTO,
        Documento documento) {
        LOGGER.debug("DocumentoDAOBean#guardarDocumentoAlfrescoSinNumeroPredial");
        Documento respuesta = null;

        try {

            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();
            String rutaFtp = servicioDocumental
                .cargarDocumentoTramiteSinPredio(documentoTramiteDTO,
                    usuario);
            if (rutaFtp != null && !rutaFtp.isEmpty()) {
                documento.setIdRepositorioDocumentos(rutaFtp);

                if (documento.getArchivo() != null) {
                    documento.setArchivo(documento.getArchivo().replace(
                        FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
                }
                respuesta = update(documento);
            }

        } catch (ExcepcionSNC e) {
            if (e.getCodigoDeExcepcion().equals(
                EExcepcionSNC.ERROR_DURANTE_LA_EJECUCION.getCodigo())) {
                respuesta = update(documento);
            } else {
                LOGGER.error(e.getMessage(), e);
                throw e;
            }
        }
        return respuesta;
    }

    // ------------------------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see IDocumentoDAO#aactualizarDocumentoYAlfrescoConValidacionNumeroPredial(UsuarioDTO,
     * DocumentoTramiteDTO, Documento, Tramite)
     */
    @Override
    public boolean actualizarDocumentoYGestorDocumentalConValidacionNumeroPredial(
        UsuarioDTO usuario, DocumentoTramiteDTO documentoTramiteDTO,
        Documento documento, Tramite tramite) {

        try {

            if (documento.getArchivo() != null) {
                documento.setArchivo(documento.getArchivo().replace(
                    FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
            }

            update(documento);
            if (tramite != null && !tramite.isQuinta() &&
                tramite.getSolicitud() != null &&
                !tramite.getSolicitud().isViaGubernativa()) {
                guardarDocumentoGestorDocumental(usuario, documentoTramiteDTO,
                    documento);
            } else {
                guardarDocumentoGestorDocumentalSinNumeroPredial(usuario,
                    documentoTramiteDTO, documento);
            }
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#almacenarDocumentoAlfrescoSinPredio(UsuarioDTO, Documento)
     * @author david.cifuentes
     */
    @Override
    public Documento almacenarDocumentoGestorDocumentalSinPredio(UsuarioDTO usuario,
        Documento documento) {

        Documento doc = new Documento();

        try {
            DocumentoTramiteDTO documentoTramiteDTO = null;

            if (documento.getTramiteId() != null) {

                String nombreDepartamento;
                String nombreMunicipio;
                String tipoTramite;
                TipoDocumento tipoDocumento = null;
                Date fechaCreacionTramite = null;
                Long tramiteId = null;

                Tramite tTemp = this.tramiteDAO
                    .findTramiteByIdForAsignacion(documento.getTramiteId());

                if (tTemp != null) {
                    tramiteId = tTemp.getId();
                    fechaCreacionTramite = tTemp.getFechaRadicacion();
                    tipoTramite = tTemp.getTipoTramite();
                    nombreDepartamento = tTemp.getDepartamento().getNombre();
                    nombreMunicipio = tTemp.getMunicipio().getNombre();

                    if (documento.getTipoDocumento().getId() != null &&
                        documento.getTipoDocumento().getNombre() == null) {
                        tipoDocumento = this.remoteTipoDocumentoService
                            .findById(documento.getTipoDocumento().getId());
                    } else {
                        tipoDocumento = documento.getTipoDocumento();
                    }

                    documentoTramiteDTO = new DocumentoTramiteDTO(
                        documento.getArchivo(),
                        tipoDocumento.getNombre(), fechaCreacionTramite,
                        nombreDepartamento, nombreMunicipio, "",
                        tipoTramite, tramiteId);

                    doc = guardarDocumentoGestorDocumentalSinNumeroPredial(usuario,
                        documentoTramiteDTO, documento);
                }
            }
            return doc;
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            throw (e);
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Metodo que retorna las imagenes antes y despues del tramite
     *
     * @author fredy.wilches
     * @param t
     * @return
     */
    @Override
    public List<Documento> buscarImagenesTramite(Tramite t) {
        LOGGER.debug("executing DocumentoDAOBean#buscarImagenesTramite");

        List<Documento> answer = new ArrayList<Documento>();
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.tramiteId = :tramiteId " +
            " AND d.tipoDocumento.id IN (:tipoImagenAntes, :tipoImagenDespues) ";

        try {
            query = entityManager.createQuery(queryToExecute);
            query.setParameter("tramiteId", t.getId());
            query.setParameter("tipoImagenAntes",
                ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE.getId());
            query.setParameter("tipoImagenDespues",
                ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId());

            answer = (ArrayList<Documento>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarImagenesTramite: " +
                "La consulta de los documentos de las imagenes antes y despues del tramite no retorno datos");
            return answer;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de imagenes del tramite: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     * @author felipe.cadena
     * @see co.gov.igac.snc.dao.generales.IDocumentoDAO#guardarDocumentosSolicitudAvaluo(UsuarioDTO,
     * DocumentoSolicitudDTO, Documento)
     */
    @Implement
    @Override
    public Documento guardarDocumentosSolicitudAvaluoAlfresco(UsuarioDTO usuario,
        DocumentoSolicitudDTO documentoSolicitudDTO, Documento documento) {

        LOGGER.debug("DocumentoDAOBean#guardarDocumentosSolicitudAvaluo");

        Documento respuesta = null;
        String idArchivoAlfresco;

        try {
            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();

            idArchivoAlfresco = servicioDocumental.cargarDocumentoSolicitudAvaluoComercial(
                documentoSolicitudDTO, usuario);
            LOGGER.debug("Id alfresco + " + idArchivoAlfresco);
            if (idArchivoAlfresco != null && !idArchivoAlfresco.isEmpty()) {
                documento.setIdRepositorioDocumentos(idArchivoAlfresco);
                if (documento.getArchivo() != null) {
                    documento.setArchivo(documento.getArchivo().replace(
                        FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
                }
                respuesta = update(documento);
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#guardarDocumentosSolicitudAvaluo");
        }
        return respuesta;
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see IDocumentoDAO#getImagenAfterEdicionByTramiteId(Long)
     */
    @Override
    public Documento getImagenAfterEdicionByTramiteId(Long tramiteId) {
        LOGGER.debug("DocumentoDAOBean#getImagenAfterEdicionByTramiteId");

        Documento answer = null;
        String query = "SELECT COUNT (d) FROM Documento d " +
            " JOIN d.tipoDocumento t " +
            " WHERE t.id =:tipoDocumentoId " +
            " AND d.tramiteId =:tramiteId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tipoDocumentoId",
                ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId());
            q.setParameter("tramiteId", tramiteId);

            Long answerCount = (Long) q.getSingleResult();
            if (answerCount != null && answerCount.longValue() > 0) {
                query = "SELECT d FROM Documento d " +
                    " JOIN FETCH d.tipoDocumento t " +
                    " WHERE t.id =:tipoDocumentoId " +
                    " AND d.tramiteId =:tramiteId";
                q = this.entityManager.createQuery(query);
                q.setParameter("tipoDocumentoId",
                    ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId());
                q.setParameter("tramiteId", tramiteId);

                answer = (Documento) q.getSingleResult();
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#getImagenAfterEdicionByTramiteId");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @author lorena.salamanca
     * @see IDocumentoDAO#obtenerIdRepositorioDocumentosDeReplicaOriginal(java.lang.Long,
     * java.lang.Long)
     */
    /*
     * @modified pedro.garcia 09-09-2013 se devuelve el campo 'idRepositorioDocumentos', no
     * 'archivo', porque es en este donde se debe guardar el id del documento en el gestor
     * documental
     */
    @Override
    public String obtenerIdRepositorioDocumentosDeReplicaOriginal(
        Long tramiteId, Long idTipoDocumento) {

        LOGGER.debug("executing DocumentoDAOBean#obtenerIdRepositorioDocumentosDeReplicaOriginal");

        String answer = "";
        Query query = null;
        boolean error = false;

        LOGGER.debug("tramiteId: " + tramiteId);
        LOGGER.debug("idTipoDocumento: " + idTipoDocumento);
        String queryToExecute;

//OJO :: el id en el repositorio de documentos NO está 'archivo' sino en el campo idRepositorioDocumentos
        queryToExecute = "SELECT d.idRepositorioDocumentos FROM Documento d " +
            " WHERE d.tramiteId = :tramiteId " +
            " AND d.tipoDocumento.id = :tipoReplica";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("tramiteId", tramiteId);
            query.setParameter("tipoReplica", idTipoDocumento);

            answer = (String) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#obtenerNombreDeReplica: " +
                "La consulta del nombre de la replica tipo " + idTipoDocumento +
                " del tramite no retorno datos");
            error = true;
            return answer;
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta de la replica tipo " + idTipoDocumento +
                " del tramite: " + tramiteId +
                ex.getMessage());
            error = true;
        } finally {
            if (error) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, null,
                    "DocumentoDAOBean#obtenerIdRepositorioDocumentosDeReplicaOriginal");
            }
        }
        return answer;
    }
    //----------------------------------------------------------------------------------------------

    /**
     * @author lorena.salamanca
     * @see IDocumentoDAO#buscarDocumentoPorTramiteIdyTipoDocumento(Long, Long)
     */
    @Override
    public Documento buscarDocumentoPorTramiteIdyTipoDocumento(Long tramiteId, Long idTipoDocumento) {
        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentoPorTramiteIdyTipoDocumento");

        List<Documento> answer = null;
        Documento documento = null;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.tramiteId = :tramiteId " +
            " AND d.tipoDocumento.id = :idTipoDocumento ";

        try {
            query = entityManager.createQuery(queryToExecute);
            query.setParameter("tramiteId", tramiteId);
            query.setParameter("idTipoDocumento", idTipoDocumento);

            answer = (ArrayList<Documento>) query.getResultList();
            if (answer != null && !answer.isEmpty()) {
                documento = answer.get(0);
                Hibernate.initialize(documento.getTipoDocumento());
            }

        } catch (NoResultException nrEx) {
            LOGGER.debug("DocumentoDAOBean#buscarDocumentoPorTramiteIdyTipoDocumento: " +
                "La consulta del nombre del documento del tramite no retorno datos");
            return documento;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "DocumentoDAOBean#buscarDocumentoPorTramiteIdyTipoDocumento");
        }
        return documento;
    }
    //---------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.generales.IDocumentoDAO#existsImagenTramiteInicialByTramiteId(java.lang.Long)
     */
    @Override
    public boolean existsImagenTramiteInicialByTramiteId(Long tramiteId) {
        LOGGER.debug("DocumentoDAOBean#getImagenAfterEdicionByTramiteId");

        boolean answer = false;
        String query = "SELECT COUNT (d) FROM Documento d " +
            " JOIN d.tipoDocumento t " + " WHERE t.id =:tipoDocumentoId " +
            " AND d.tramiteId =:tramiteId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter(
                "tipoDocumentoId",
                ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE
                    .getId());
            q.setParameter("tramiteId", tramiteId);

            Long answerCount = (Long) q.getSingleResult();
            if (answerCount != null && answerCount.longValue() > 0) {
                return true;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#getImagenAfterEdicionByTramiteId");
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#almacenarFichaPredialDigitalAlfresco(Documento, UsuarioDTO)
     * @author javier.aponte
     */
    @Override
    public Documento almacenarFichaPredialDigitalGestorDocumental(Documento documento,
        UsuarioDTO usuario, String rutaLocal) {

        Documento answer = null;
        try {
            Predio pTemp = this.remotePredioDAO.findById(documento.getPredioId());

            DocumentoTramiteDTO documentoTramiteDTO = DocumentoTramiteDTO
                .crearArgumentoCargarDocumentoPredioFicha(rutaLocal,
                    pTemp.getNumeroPredial(), ETipoDocumento.FICHA_PREDIAL_DIGITAL.toString());

            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            String rutaAlfresco = servicioDocumental.cargarDocumentoPredioFichaGenerada(
                documentoTramiteDTO, usuario);
            if (rutaAlfresco != null && !rutaAlfresco.isEmpty()) {
                documento.setIdRepositorioDocumentos(rutaAlfresco);
            }

            if (documento.getArchivo() != null) {
                documento.setArchivo(documento.getArchivo().replace(
                    FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
            }
            answer = update(documento);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#almacenarFichaPredialDigitalAlfresco");
        }

        return answer;

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#almacenarCertificadoPlanoPredialCatastral(Documento, UsuarioDTO)
     * @author javier.aponte
     */
    @Override
    public Documento almacenarCertificadoPlanoPredialCatastral(Documento documento,
        UsuarioDTO usuario, String rutaLocal) {

        Documento answer = null;
        try {
            Predio pTemp = this.remotePredioDAO.findById(documento.getPredioId());

            DocumentoTramiteDTO documentoTramiteDTO = DocumentoTramiteDTO
                .crearArgumentoCargarDocumentoPredioFicha(rutaLocal,
                    pTemp.getNumeroPredial(), ETipoDocumento.CERTIFICADO_PLANO_PREDIAL_CATASTRAL.
                    toString());

            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            String rutaAlfresco = servicioDocumental.cargarDocumentoPredioFichaGenerada(
                documentoTramiteDTO, usuario);
            if (rutaAlfresco != null && !rutaAlfresco.isEmpty()) {
                documento.setIdRepositorioDocumentos(rutaAlfresco);
            }

            if (documento.getArchivo() != null) {
                documento.setArchivo(documento.getArchivo().replace(
                    FileUtils.getTempDirectory().getAbsolutePath() + FILESEPARATOR, ""));
            }
            answer = update(documento);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#almacenarFichaPredialDigitalAlfresco");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @author franz.gamba
     * @see IDocumentoDAO#deleteDocumentoAlsoGestorDocumental(Documento)
     */
    @Override
    public boolean deleteDocumentoAlsoGestorDocumental(Documento documento) {
        boolean answer = false;

        IDocumentosService documentosService = DocumentalServiceFactory.getService();
        try {
            if (documento.getIdRepositorioDocumentos() != null) {
                documentosService.borrarDocumento(documento
                    .getIdRepositorioDocumentos());
            }
            this.delete(documento);
            answer = true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#deleteDocumentoAlsoAlfresco");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @author felipe.cadena
     * @see IDocumentoDAO#borrarDocumentoGestorDocumental(Documento)
     */
    @Override
    public boolean borrarDocumentoGestorDocumental(Documento documento) {
        boolean answer = false;

        IDocumentosService documentosService = DocumentalServiceFactory.getService();
        try {
            if (documento.getIdRepositorioDocumentos() != null) {
                documentosService.borrarDocumento(documento
                    .getIdRepositorioDocumentos());
                answer = true;
            } else {
                return false;
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#borrarDocumentoAlfresco");
        }

        return answer;
    }

    /**
     * @author franz.gamba
     * @see IDocumentoDAO#buscarFormato(String)
     */
//	@Override
//	public String buscarFormato(String nombre) {
//		
//		String path =  null;
//		IDocumentosService documentosService = DocumentalServiceFactory.getService();
//		try{
//			
//			Map<String,String> mapaFormato = documentosService.buscarFormato(nombre);
////REVIEW :: ?? :: se supone que como llaves en el Map están los nombres de los archivos, ¿pero es que acaso son ÚNICOS? :: pedro.garcia
//			String idRepositorio = mapaFormato.get(nombre);
//			if(idRepositorio != null){
//				path = descargarOficioDeGestorDocumentalATempLocalMaquina(idRepositorio);
//			}			
//			
//		}catch (Exception e) {
//			throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
//					LOGGER, e, e.getMessage(),
//					"DocumentoDAOBean#buscarFormatoYSubirAlFTP");
//		}
//		return path;
//	}
//--------------------------------------------------------------------------------------------------    
    /**
     * @author felipe.cadena
     * @see IDocumentoDAO#obtenerUltimoDocSolicitud(Long)
     */
    @Override
    public Documento obtenerUltimoDocSolicitud(Long idSolicitud) {

        Documento documento = new Documento();
        Date maxFecha;

        String queryDate = "SELECT MAX(d.fechaRadicacion) FROM SolicitudDocumentacion sd" +
            " JOIN sd.soporteDocumentoId d" +
            " WHERE sd.solicitud.id =:solicitud ";
        try {
            Query q = this.entityManager.createQuery(queryDate);
            q.setParameter("solicitud", idSolicitud);

            maxFecha = (Date) q.getSingleResult();
            LOGGER.debug("Fecha Maxima : " + maxFecha);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#obtenerUltimoDocSolicitud");
        }

        String queryDoc = "SELECT d FROM SolicitudDocumentacion sd" +
            " JOIN sd.soporteDocumentoId d" +
            " WHERE sd.solicitud.id =:solicitud " +
            " AND d.fechaRadicacion = :fechaMax" +
            " ORDER BY d.id DESC";
        try {
            Query q = this.entityManager.createQuery(queryDoc);
            q.setParameter("solicitud", idSolicitud);
            q.setParameter("fechaMax", maxFecha);

            documento = (Documento) q.getResultList().get(0);
            LOGGER.debug("Documento : " + documento.getArchivo());
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#obtenerUltimoDocSolicitud");
        }

        return documento;

    }

    /**
     * @see IDocumentoDAO#buscarDocumentosDeTramiteDocumentacionsPorSolicitudId(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<Documento> buscarDocsDeTramiteDocumentacionsPorSolicitudId(
        Long idSolicitud) {

        List<Documento> result = new ArrayList<Documento>();

        String strQuery = "";
        Query query;

        strQuery = "SELECT DISTINCT doc" +
            " FROM TramiteDocumentacion traDoc," +
            " Tramite tra," +
            " Documento doc" +
            " WHERE " +
            " traDoc.tramite.id = tra.id" +
            " AND traDoc.documentoSoporte.id = doc.id" +
            " AND tra.solicitud.id = :idSolicitud";

        try {
            query = this.entityManager.createQuery(strQuery);
            query.setParameter("idSolicitud", idSolicitud);

            result = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#obtenerUltimoDocSolicitud");
        }

        return result;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @author javier.aponte
     * @see IDocumentoDAO#buscarDocumentoPorPredioIdAndTipoDocumento(Long, Long)
     */
    @Override
    public Documento buscarDocumentoPorPredioIdAndTipoDocumento(Long predioId, Long idTipoDocumento) {
        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentoPorPredioIdAndTipoDocumento");

        Documento answer = null;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.predioId = :predioId " +
            " AND d.tipoDocumento.id = :tipoDocumentoId ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("predioId", predioId);
        query.setParameter("tipoDocumentoId", idTipoDocumento);

        try {
            answer = (Documento) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.info("DocumentoDAOBean#buscarDocumentoPorPredioIdAndTipoDocumento: " +
                "La consulta del nombre del documento por predio id y tipo de documento no retorno datos");
            return answer;
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta del documento por predio id y tipo de documento " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "DocumentoDAOBean#buscarDocumentoPorPredioIdAndTipoDocumento");

        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IDocumentoDAO#almacenarDocumentoMemoriaTecnicaActualizacion(UsuarioDTO,
     * Documento,Actualizacion)
     * @author javier.aponte
     */
    @Override
    public Documento almacenarDocumentoMemoriaTecnicaActualizacion(Documento documento,
        Actualizacion actualizacion, UsuarioDTO usuario) {

        Documento answer = null;
//		IDocumentosService service = DocumentalServiceFactory.getService();
//		DocumentoMemoriaTecnicaActualizacionDTO documentoMemoriaTecnicaActualizacionDTO = null;
//TODO :: javier.aponte :: hablar con fredy de esto :: 26-11-2011
//		File archivo = new File(documento.getArchivo());
//
//		if (actualizacion == null) {
//			return null;
//		} else {
//			try{
//				String nombreDepartamento = "";
//				String nombreMunicipio = "";
//				TipoDocumento tipoDocumento = null;
//				Long actualizacionId = null;
//				int vigencia;
//	
//				actualizacionId = actualizacion.getId();
//				nombreDepartamento = actualizacion.getDepartamento().getNombre();
//				nombreMunicipio = actualizacion.getMunicipio().getNombre();
//				if(actualizacion.getAnio() != null){
//					vigencia = actualizacion.getAnio().intValue();
//				}
//				else{
//					Calendar fechaActual = Calendar.getInstance();
//					vigencia = fechaActual.get(Calendar.YEAR);
//				}
//					
//				if (documento.getTipoDocumento().getId() != null
//						&& documento.getTipoDocumento().getNombre() == null) {
//					tipoDocumento = this.remoteTipoDocumentoService
//							.findById(documento.getTipoDocumento().getId());
//				} else {
//					tipoDocumento = documento.getTipoDocumento();
//				}
//					
//TODO: javier.aponte :: hablar con fredy de esto :: 26-11-2011
//TODO: javier.aponte :: esto está quemado por ahora, arreglar cuando se definan esos campos
//				if(archivo.exists()){
//					documentoMemoriaTecnicaActualizacionDTO = new DocumentoMemoriaTecnicaActualizacionDTO(
//						nombreDepartamento, nombreMunicipio,
//						vigencia, EActualizacionDocumentacionZona.RURAL.getDescripcion(),
//						tipoDocumento.getNombre(), 1,
//						archivo.getAbsolutePath());
//				}
//				
//				else{
//					documentoMemoriaTecnicaActualizacionDTO = new DocumentoMemoriaTecnicaActualizacionDTO(
//							nombreDepartamento, nombreMunicipio,
//							vigencia, EActualizacionDocumentacionZona.RURAL.getDescripcion(),
//							tipoDocumento.getNombre(), 1,
//							new File(FileUtils.getTempDirectory().getAbsolutePath(),
//								documento.getArchivo()).getAbsolutePath());
//	
//				}
//				
//				String rutaAlfresco = service.
//					cargarDocumentoMemoriaTecnicaActualizacion(documentoMemoriaTecnicaActualizacionDTO, usuario);
//								
//				if (rutaAlfresco != null && !rutaAlfresco.isEmpty()) {
//					documento.setIdRepositorioDocumentos(rutaAlfresco);
//
//					if (documento.getArchivo() != null) {
//						documento.setArchivo(documento.getArchivo().replace(
//								FileUtils.getTempDirectory().getAbsolutePath()+FILESEPARATOR, ""));
//					}
//					
//				 	answer = update(documento);
//					answer.getTipoDocumento().getNombre();
//					
//				}
//				
//			}catch (Exception e) {
//				throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.getExcepcion(
//							LOGGER, e, e.getMessage(),
//							"DocumentoDAOBean#almacenarDocumentoMemoriaTecnicaActualizacion");
//			}
//		}
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDocumentoDAO#buscarDocumentoPorNumeroDocumento(String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Documento buscarDocumentoPorNumeroDocumento(String numResolucion) {
        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentoPorNumeroDocumento");

        List<Documento> answer = null;
        Documento doc = null;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.numeroDocumento = :numeroDocumento ";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("numeroDocumento", numResolucion);

            answer = (ArrayList<Documento>) query.getResultList();
            if (answer != null && !answer.isEmpty()) {
                doc = answer.get(0);
                doc.getTipoDocumento();
                if (doc.getTipoDocumento() != null) {
                    Hibernate.initialize(doc.getTipoDocumento());
                }
            }
        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarDocumentoPorNumeroDocumento: " +
                "La consulta del documento por numero de documento no retorno datos");
            return doc;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return doc;
    }

    /**
     * @see IDocumentoDAO#buscarPorNumeroDocumentoYTipo(String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Documento> buscarPorNumeroDocumentoYTipo(String numero, Long tipoDocumentoId) {
        LOGGER.debug("executing DocumentoDAOBean#buscarPorNumeroDocumentoYTipo");

        List<Documento> answer = null;
        Documento doc = null;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.numeroDocumento = :numeroDocumento " +
            " AND d.tipoDocumento.id = :tipoDocumento " +
            " ORDER BY d.id DESC";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("numeroDocumento", numero);
            query.setParameter("tipoDocumento", tipoDocumentoId);

            answer = (ArrayList<Documento>) query.getResultList();

            for (Documento docTemp : answer) {
                if (docTemp.getTipoDocumento() != null) {
                    Hibernate.initialize(docTemp.getTipoDocumento());
                }
            }

        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return answer;
    }
    
    /**
     * @see IDocumentoDAO#buscarDocumentoPorTipoActualizacion(String, Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Documento> buscarDocumentoPorTipoActualizacion(String numeroDocumento,Long tipoDocumentoId) {
        LOGGER.debug("executing DocumentoDAOBean#buscarPorNumeroDocumentoYTipo");

        List<Documento> answer = null;
        Documento doc = null;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.numeroDocumento LIKE :numeroDocumento " +
            " AND d.tipoDocumento.id = :tipoDocumento ";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("numeroDocumento", "%" +numeroDocumento);
            query.setParameter("tipoDocumento", tipoDocumentoId);

            answer = (ArrayList<Documento>) query.getResultList();

            for (Documento docTemp : answer) {
                if (docTemp.getTipoDocumento() != null) {
                    Hibernate.initialize(docTemp.getTipoDocumento());
                }
            }

        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return answer;
    }
    
    /**
     * @see IDocumentoDAO#buscarPorNumeroUltimoDocumentoYTipo(String, Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Documento buscarPorNumeroUltimoDocumentoYTipo(String numero, Long tipoDocumentoId) {
        LOGGER.debug("executing DocumentoDAOBean#buscarPorNumeroDocumentoYTipo");

        Documento answer = null;
        Documento doc = null;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.numeroDocumento = :numeroDocumento " +
            " AND d.tipoDocumento.id = :tipoDocumento ";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("numeroDocumento", numero);
            query.setParameter("tipoDocumento", tipoDocumentoId);

            answer = (Documento) query.getSingleResult();

        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#actualizarDocumentoSinAlfresco(Documento)
     * @author juanfelipe.garcia
     */
    @Override
    public boolean actualizarDocumentoSinGestorDocumental(Documento documento) {
        try {
            update(documento);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IDocumentoDAO#buscarDocumentoCompletoPorId(Documento)
     * @author juanfelipe.garcia
     */
    @Override
    public Documento buscarDocumentoCompletoPorId(Long documentoId) {

        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentoPorTramiteId");

        Documento answer = new Documento();
        Query query = null;
        String queryToExecute = "SELECT d FROM Documento d " +
            " JOIN FETCH d.tipoDocumento t " +
            " WHERE d.id = :documentoId ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("documentoId", documentoId);

        try {
            answer = (Documento) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarDocumentoPorTramiteId: " +
                "La consulta del nombre del documento del tramite no retorno datos");
            return answer;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------	
    /**
     * @see IDocumentoDAO#buscarDocumentoPorNumeroPredialAndTipoDocumento(String, Long)
     * @author javier.aponte
     */
    @Override
    public Documento buscarDocumentoPorNumeroPredialAndTipoDocumento(String numeroPredial,
        Long idTipoDocumento) {
        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentoPorNumeroPredialAndTipoDocumento");

        Documento answer = null;
        List<Documento> documentos;
        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.numeroPredial LIKE :numeroPredial " +
            " AND d.tipoDocumento.id = :tipoDocumentoId ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("numeroPredial", numeroPredial + "%");
        query.setParameter("tipoDocumentoId", idTipoDocumento);

        try {
            documentos = (List<Documento>) query.getResultList();
            if (documentos != null && !documentos.isEmpty()) {
                answer = documentos.get(0);
            }

        } catch (NoResultException nrEx) {
            LOGGER.info("DocumentoDAOBean#buscarDocumentoPorNumeroPredialAndTipoDocumento: " +
                "La consulta del documento por número predial y tipo documento id no retorno datos");
            return answer;
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta del documento por número predial y tipo documento id: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "DocumentoDAOBean#buscarDocumentoPorNumeroPredialAndTipoDocumento");
        }
        return answer;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentosPorSolicitudId(Long)
     * @author javier.aponte
     */
    @Override
    @Implement
    public List<Documento> buscarDocumentosPorSolicitudId(Long idSolicitud) {

        List<Documento> result = null;

        String strQuery = "";
        Query query;

        strQuery = "SELECT DISTINCT d" +
            " FROM SolicitudDocumento sd" +
            " JOIN  sd.soporteDocumento d" +
            " JOIN FETCH d.tipoDocumento td" +
            " WHERE sd.solicitud.id = :idSolicitud";

        try {
            query = this.entityManager.createQuery(strQuery);
            query.setParameter("idSolicitud", idSolicitud);

            result = (List<Documento>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#buscarDocumentosPorSolicitudId");
        }

        return result;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentacionPorSolicitudId(Long)
     * @author javier.aponte
     */
    @Override
    @Implement
    public List<Documento> buscarDocumentacionPorSolicitudId(Long idSolicitud) {

        List<Documento> result = null;

        String strQuery = "";
        Query query;

        strQuery = "SELECT DISTINCT d" +
            " FROM SolicitudDocumentacion sd" +
            " JOIN  sd.soporteDocumentoId d" +
            " JOIN FETCH d.tipoDocumento td" +
            " WHERE sd.solicitud.id = :idSolicitud";

        try {
            query = this.entityManager.createQuery(strQuery);
            query.setParameter("idSolicitud", idSolicitud);

            result = (List<Documento>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#buscarDocumentacionPorSolicitudId");
        }

        return result;
    }

    // -------------------------------------------------- //
    /**
     * @see IDocumentoDAO#verificarTramitesCopiaDBDepuracion(List<Long>)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    public List<Long> verificarTramitesCopiaDBDepuracion(
        List<Long> tramiteIds) {

        List<Long> result = null;
        Query query;
        String queryToExecute = "SELECT d.id FROM Documento d " +
            " JOIN FETCH d.tipoDocumento t " +
            " WHERE ( t.id = :tipoDocCopiaGeoFinal or t.id = tipoDocCopiaGeoOriginal ) " +
            " AND d.tramiteId NOT IN (:tramiteIds)";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("tipoDocCopiaGeoFinal", ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.
                getId());
            query.setParameter("tipoDocCopiaGeoOriginal", ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.
                getId());
            query.setParameter("tramiteIds", tramiteIds);

            result = (List<Long>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#verificarTramitesProvenientesDepuracion");
        }

        return result;
    }

    /**
     * @see IDocumentoDAO#guardarDocumentoProductosCatastrales(usuario,productoCatastralDetalle)
     * @author leidy.gonzalez
     */
    @Override
    @Implement
    public Documento guardarDocumentoProductosCatastrales(UsuarioDTO usuario,
        ProductoCatastralDetalle productoCatastralDetalle, Object[] numeracionCertificado) {
        Documento documento = new Documento();

        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento
            .setId(ETipoDocumento.CERTIFICADO_PLANO_PREDIAL_CATASTRAL
                .getId());

        documento.setTipoDocumento(tipoDocumento);

        documento.setArchivo(productoCatastralDetalle.getRutaProductoEjecutado());

        documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        documento.setBloqueado(ESiNo.NO.getCodigo());
        documento.setUsuarioCreador(usuario.getLogin());
        documento.setUsuarioLog(usuario.getLogin());
        documento.setFechaLog(new Date(System.currentTimeMillis()));
        documento.setEstructuraOrganizacionalCod(usuario.getCodigoEstructuraOrganizacional());
        documento.setIdRepositorioDocumentos(productoCatastralDetalle.getRutaProductoEjecutado());
        documento.setNumeroPredial(productoCatastralDetalle.getNumeroPredial());
        documento.setFechaDocumento(new Date(System.currentTimeMillis()));

        documento.setNumeroDocumento(numeracionCertificado[0].toString());

        update(documento);

        return documento;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentosFTP
     * @author leidy.gonzalez
     */
    @Override
    public List<Documento> buscarDocumentosFTP() {

        List<Documento> result = null;

        String strQuery = "";
        Query query;

        strQuery = "SELECT d FROM Documento d " +
            " WHERE d.numeroDocumento IS NOT NULL " +
            " AND d.idRepositorioDocumentos IS NULL " +
            " AND d.tipoDocumento.id =:tipoDocumento ";

        try {
            query = this.entityManager.createQuery(strQuery);
            query.setParameter("tipoDocumento",
                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());

            result = (List<Documento>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#buscarDocumentosPorSolicitudId");
        }

        return result;
    }

    /**
     * @see IDocumentoDAO#validarExistenciaGestorDocumental
     *
     * @author felipe.cadena
     */
    @Override
    public boolean validarExistenciaGestorDocumental(Documento documento) {

        boolean result;
        String ruta;

        try {
            ruta = documento.getIdRepositorioDocumentos();
            if (ruta == null || ruta.isEmpty()) {
                return false;
            }

            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();
            result = servicioDocumental.validarExistencia(ruta);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#validarExistenciaGestorDocumental");
        }
        return result;
    }

    /**
     * @author leidy.gonzalez
     * @see IDocumentoDAO#buscarFirmaUsuario()
     */
    @Override
    public Documento buscarFirmaUsuario(Long idDocumento) {
        LOGGER.debug("executing DocumentoDAOBean#buscarFirmaUsuario");

        Documento answer = null;
        List<Documento> documentos;

        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.id = :idDocumento ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("idDocumento", idDocumento);

        try {
            documentos = (List<Documento>) query.getResultList();
            if (documentos != null && !documentos.isEmpty()) {
                answer = documentos.get(0);
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarFirmaUsuario: " +
                "La consulta del id del documento del tramite no retorno datos");
            return answer;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentoPorNumeroDocumentoAndTipoDocumento
     *
     * @author leidy.gonzalez :: #12528::26/05/2015::CU_187
     */
    @Override
    public Documento buscarDocumentoPorNumeroDocumentoAndTipoDocumento(String numeroDocumento) {
        Documento documento = null;

        Query query = null;
        String queryToExecute = "SELECT d FROM Documento d " +
            " JOIN FETCH d.tipoDocumento t " +
            " WHERE d.numeroDocumento = :numeroDocumento " +
            " AND t.id = :tipoDocumento ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("numeroDocumento", numeroDocumento);
        query.setParameter("tipoDocumento",
            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                getId());

        try {
            documento = (Documento) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarDocumentoPorNumeroDocumentoAndTipoDocumento: " +
                "La consulta del nombre del documento del tramite no retorno datos");
            return documento;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }

        return documento;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentosPorSolicitudIdSinRepositorio(Long,double)
     * @author leidy.gonzalez
     */
    @Override
    @Implement
    public List<Documento> buscarDocumentosPorSolicitudIdSinRepositorio(Long idSolicitud,
        int cantidadDocumentos) {

        List<Documento> result = null;

        Query query;
        String queryString;

        queryString = "SELECT DISTINCT D.*  FROM DOCUMENTO D " +
            "           INNER JOIN TRAMITE TR ON D.TRAMITE_ID = TR.ID " +
            "           WHERE TR.SOLICITUD_ID = :idSolicitud " +
            "           AND D.TIPO_DOCUMENTO_ID <> :tipoDocumento " +
            "           AND D.ID_REPOSITORIO_DOCUMENTOS IS NULL " +
            "           AND ROWNUM < '" + cantidadDocumentos + "'";

        try {
            query = this.entityManager.createNativeQuery(queryString, Documento.class);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("tipoDocumento", ETipoDocumento.FOTOGRAFIA.getId());
            result = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los documentos" + ex.getMessage());
        }

        return result;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentosPorSolicitudIdSinRepositorio(Long)
     * @author felipe.cadena
     */
    @Override
    public List<Documento> obtenerMUnidadesConstruccionDocumento(List<Documento> documentos) {

        List<Object[]> queryResult = null;

        Query query;
        String queryString;

        List<Long> idsDocumentos = new ArrayList<Long>();

        for (Documento d : documentos) {
            idsDocumentos.add(d.getId());
        }
//TODO:: cambiar consulta para trabajar con tablas M
        queryString = " SELECT d.id,muc.id unidadId, muc.unidad FROM DOCUMENTO D" +
            " INNER JOIN M_FOTO MF ON MF.FOTO_DOCUMENTO_ID = D.ID" +
            " INNER JOIN M_UNIDAD_CONSTRUCCION MUC ON MUC.ID = MF.UNIDAD_CONSTRUCCION_ID" +
            " WHERE D.ID IN ( :idsDocumento )";

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.setParameter("idsDocumento", idsDocumentos);
            queryResult = query.getResultList();

            Map<Long, PUnidadConstruccion> pUnidades = new HashMap<Long, PUnidadConstruccion>();
            for (Object[] obj : queryResult) {
                Long idDoc = ((BigDecimal) obj[0]).longValue();
                Long idUnidad = ((BigDecimal) obj[1]).longValue();
                String unidad = (String) obj[2];

                PUnidadConstruccion puc = new PUnidadConstruccion();
                puc.setId(idUnidad);
                puc.setUnidad(unidad);

                pUnidades.put(idDoc, puc);
            }

            for (Documento doc : documentos) {
                doc.setpUnidadConstruccion(pUnidades.get(doc.getId()));
            }

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los documentos" + ex.getMessage());
        }

        return documentos;
    }

    /**
     * @see IDocumentoDAO#buscarDocumentosPorSolicitudIdSinRepositorioFotos(Long,double)
     * @author leidy.gonzalez
     */
    @Override
    @Implement
    public List<Documento> buscarDocumentosPorSolicitudIdSinRepositorioFotos(Long idSolicitud,
        int cantidadDocumentos) {

        List<Documento> result = null;

        Query query;
        String queryString;

        queryString = "SELECT DISTINCT D.*  FROM DOCUMENTO D " +
            "           INNER JOIN TRAMITE TR ON D.TRAMITE_ID = TR.ID " +
            "           WHERE TR.SOLICITUD_ID = :idSolicitud " +
            "           AND D.TIPO_DOCUMENTO_ID = :tipoDocumento " +
            "           AND D.ID_REPOSITORIO_DOCUMENTOS IS NULL " +
            "           AND ROWNUM < '" + cantidadDocumentos + "'";

        try {
            query = this.entityManager.createNativeQuery(queryString, Documento.class);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("tipoDocumento", ETipoDocumento.FOTOGRAFIA.getId());
            result = query.getResultList();

        } catch (Exception ex) {
            LOGGER.error("error obteniendo los documentos" + ex.getMessage());
        }

        return result;
    }

    /**
     * @see IDocumentoDAO#contarDocumentosPorSolicitudIdSinRepositorio(Long,double)
     * @author leidy.gonzalez
     */
    @Override
    @Implement
    public Integer contarDocumentosPorSolicitudIdSinRepositorio(Long idSolicitud) {

        Query query;
        String queryString;

        queryString = "SELECT COUNT (D.id)  FROM DOCUMENTO D " +
            "           INNER JOIN TRAMITE TR ON D.TRAMITE_ID = TR.ID " +
            "           WHERE TR.SOLICITUD_ID = :idSolicitud " +
            "           AND D.TIPO_DOCUMENTO_ID <> :tipoDocumento " +
            "           AND D.ID_REPOSITORIO_DOCUMENTOS IS NULL ";

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("tipoDocumento", ETipoDocumento.FOTOGRAFIA.getId());

            Integer conteoDocumentosCargados = (Integer.parseInt(query.getSingleResult()
                .toString()));

            return conteoDocumentosCargados;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }

    }

    /**
     * @see IDocumentoDAO#contarDocumentosPorSolicitudIdSinRepositorioFotos(Long,double)
     * @author leidy.gonzalez
     */
    @Override
    @Implement
    public Integer contarDocumentosPorSolicitudIdSinRepositorioFotos(Long idSolicitud) {

        //Long conteoDocumentosCargadosL = null;
        Query query;
        String queryString;

        queryString = "SELECT COUNT (D.id)  FROM DOCUMENTO D " +
            "           INNER JOIN TRAMITE TR ON D.TRAMITE_ID = TR.ID " +
            "           WHERE TR.SOLICITUD_ID = :idSolicitud " +
            "           AND D.TIPO_DOCUMENTO_ID = :tipoDocumento " +
            "           AND D.ID_REPOSITORIO_DOCUMENTOS IS NULL ";

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("tipoDocumento", ETipoDocumento.FOTOGRAFIA.getId());

            Integer conteoDocumentosCargados = (Integer.parseInt(query.getSingleResult()
                .toString()));

            return conteoDocumentosCargados;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }

    }

    /**
     * @see IDocumentoDAO#obtenerNumerosPredialesSinFichaPredialDigital(String)
     * @author javier.aponte
     */
    @Override
    @Implement
    public String obtenerNumerosPredialesSinFichaPredialDigital(String numerosPrediales) {

        List<String> numerosPredialesPrediosSinFicha;

        StringBuilder resultado = new StringBuilder();

        String qString = "SELECT p.numero_predial" +
            " FROM predio p" +
            " WHERE p.numero_predial IN ( " + numerosPrediales + " )" +
            " MINUS" +
            " SELECT p.numero_predial" +
            " FROM predio p" +
            " INNER JOIN documento d" +
            " ON p.id                 = d.predio_id" +
            " WHERE p.numero_predial IN ( " + numerosPrediales + " )" +
            " AND d.tipo_documento_id = :idTipoDocumento";
        try {
            Query q = this.entityManager.createNativeQuery(qString);
            q.setParameter("idTipoDocumento", ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());

            numerosPredialesPrediosSinFicha = (List<String>) q.getResultList();

            if (numerosPredialesPrediosSinFicha != null && !numerosPredialesPrediosSinFicha.
                isEmpty()) {

                for (String numerosPredial : numerosPredialesPrediosSinFicha) {

                    resultado.append(numerosPredial + Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);

                }

                if (resultado.toString().endsWith(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD)) {
                    resultado.deleteCharAt(resultado.length() - 1);
                }

            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "DocumentoDAOBean#obtenerNumerosPredialesSinFichaPredialDigital");
        }

        return resultado.toString();
    }

    /**
     * @see IDocumentoDAO#guardarDocumentosActualizacionX
     * @author felipe.cadena
     */
    @Override
    public String guardarDocumentosActualizacionX(String rutaZip, String municipioActId,
        UsuarioDTO usuarioDTO, String municipio) {

        String directorioBase = "";
        try {
            IDocumentosService servicioDocumental = DocumentalServiceFactory
                .getService();

            directorioBase = servicioDocumental.
                cargarArchivosActualizacionX(rutaZip, municipioActId, usuarioDTO, municipio);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "DocumentoDAOBean#guardarDocumentosActualizacionX");
        }

        return directorioBase;
    }

    /**
     * @author leidy.gonzalez
     * @see IDocumentoDAO#buscarDocumentoPorTipoDocumentoYUsuario()
     */
    @Override
    public Documento buscarDocumentoPorTipoDocumentoYUsuario(String usuario, Long tipoDocumentoId) {
        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentoPorTipoDocumentoYUsuario");

        Documento answer = null;

        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.usuarioCreador = :usuario " +
            " AND d.tipoDocumento.id = :tipoDocumentoId ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("tipoDocumentoId", tipoDocumentoId);
        query.setParameter("usuario", usuario);

        try {
            answer = (Documento) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.error("DocumentoDAOBean#buscarDocumentoPorTipoDocumentoYUsuario: ", nrEx);
            return answer;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage(), ex);
        }
        return answer;
    }

    /**
     * @see IDocumentoDAO#obtenerDocumentosComisionPorTramiteId
     * @author wilmanjose.vega
     */
    @Override
    public List<Documento> obtenerDocumentosComisionPorTramiteId(final Long idTramite) {
        StringBuilder sbQuery = new StringBuilder(64);
        List<Documento> listaDocumentosComision = new ArrayList<Documento>();
        sbQuery.append(" SELECT d FROM Documento d, Comision c, ComisionTramite ct " +
            " WHERE d.id = c.memorandoDocumentoId AND ct.comision.id = c.id " +
            " AND ct.tramite.id = :idTramite ");
        TypedQuery<Documento> query;

        try {
            query = this.entityManager.createQuery(sbQuery.toString(), Documento.class);
            query.setParameter("idTramite", idTramite);
            listaDocumentosComision = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, e, e.
                getMessage(), "DocumentoDAOBean#obtenerDocumentosComisionPorTramiteId");
        }

        return listaDocumentosComision;
    }

    /**
     * @author leisy.gonzalez
     * @see IDocumentoDAO#buscarDocumentosPorTramiteIdyTiposDocumento(Long, List<Long>)
     */
    @Override
    public List<Documento> buscarDocumentosPorTramiteIdyTiposDocumento(Long tramiteId,
        List<Long> idTipoDocumentos) {
        LOGGER.debug("executing DocumentoDAOBean#buscarDocumentosPorTramiteIdyTiposDocumento");

        List<Documento> answer = null;

        Query query;
        String queryToExecute = "SELECT d FROM Documento d " +
            " WHERE d.tramiteId = :tramiteId " +
            " AND d.tipoDocumento.id IN (:idTipoDocumento) ";

        try {
            query = entityManager.createQuery(queryToExecute);
            query.setParameter("tramiteId", tramiteId);
            query.setParameter("idTipoDocumento", idTipoDocumentos);

            answer = (ArrayList<Documento>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.debug("DocumentoDAOBean#buscarDocumentosPorTramiteIdyTiposDocumento: " +
                "La consulta del nombre del documento del tramite no retorno datos");
            return answer;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "DocumentoDAOBean#buscarDocumentosPorTramiteIdyTiposDocumento");
        }
        return answer;
    }
}
