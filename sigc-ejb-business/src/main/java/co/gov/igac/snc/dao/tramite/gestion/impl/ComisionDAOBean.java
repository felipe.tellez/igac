/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.impl;

import java.util.Date;
import java.util.List;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.gestion.IComisionDAO;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class ComisionDAOBean extends GenericDAOWithJPA<Comision, Long>
    implements IComisionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IComisionDAO#updateAsVComision(co.gov.igac.generales.dto.UsuarioDTO,
     * co.gov.igac.snc.persistence.entity.vistas.VComision)
     */
    @Implement
    @Override
    public void updateAsVComision(UsuarioDTO loggedInUser, VComision selectedComision) {

        LOGGER.debug("on ComisionDAOBean#updateAsVComision ...");
        Comision comisionToUpdate;

        /**
         * En el entity de VComision quedó como BigDecimal, pero es Long
         */
        Long comisionId;

        comisionId = new Long(selectedComision.getId().longValue());
        comisionToUpdate = this.findById(comisionId);

        //D: campos que no deben ser nulos
        comisionToUpdate.setFechaInicio(selectedComision.getFechaInicio());
        comisionToUpdate.setFechaFin(selectedComision.getFechaFin());
        comisionToUpdate.setDuracion(selectedComision.getDuracion());
        comisionToUpdate.setObjeto(selectedComision.getObjeto());
        comisionToUpdate.setDuracionTotal(selectedComision.getDuracionTotal());
        comisionToUpdate.setMemorandoDocumentoId(selectedComision.getMemorandoDocumentoId());

        //D: campos que pueden ser nulos
        if (selectedComision.getConductor() != null) {
            comisionToUpdate.setConductor(selectedComision.getConductor());
        }
        if (selectedComision.getPlacaVehiculo() != null) {
            comisionToUpdate.setPlacaVehiculo(selectedComision.getPlacaVehiculo());
        }
        if (selectedComision.getDuracionAjuste() != null) {
            comisionToUpdate.setDuracionAjuste(selectedComision.getDuracionAjuste());
        }
        if (selectedComision.getRazonAjuste() != null) {
            comisionToUpdate.setRazonAjuste(selectedComision.getRazonAjuste());
        }
        if (selectedComision.getEstado() != null) {
            comisionToUpdate.setEstado(selectedComision.getEstado());
        }
        if (selectedComision.getObservaciones() != null) {
            comisionToUpdate.setObservaciones(selectedComision.getObservaciones());
        }

        //D: datos de log
        comisionToUpdate.setFechaLog(new Date());
        comisionToUpdate.setUsuarioLog(loggedInUser.getLogin());

        try {
            this.update(comisionToUpdate);
            LOGGER.debug("... termina ok");
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "ComisionDAOBean#updateAsVComision");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionDAO#updateComision(co.gov.igac.generales.dto.UsuarioDTO,
     * co.gov.igac.snc.persistence.entity.tramite.Comision)
     */
    /*
     * @modified pedro.garcia try y catch
     */
    @Implement
    @Override
    public Comision updateComision(UsuarioDTO loggedInUser, Comision comision) {

        Comision answer = new Comision();
        comision.setFechaLog(new Date(System.currentTimeMillis()));
        comision.setUsuarioLog(loggedInUser.getLogin());

        try {
            answer = this.update(comision);
        } catch (Exception e) {
            LOGGER.error("ComisionDAOBean#updateComision error: " + e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @see IComisionDAO#findComisionesPorIdTerritorialAndEstadoComision(String, String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Comision> findComisionesPorIdTerritorialAndEstadoComision(
        String territorialId, String estadoComision) {

        List<Comision> answer = null;
        String query = "SELECT DISTINCT NEW co.gov.igac.snc.persistence.entity.tramite.Comision(" +
            "c.id, c.numero, c.fechaInicio, c.fechaFin, c.estado, " +
            "c.duracion, c.usuarioLog, c.fechaLog) " +
            "FROM Comision c JOIN c.comisionTramites ct JOIN ct.tramite t " +
            "JOIN t.municipio m JOIN m.jurisdiccions j JOIN j.estructuraOrganizacional eo " +
            "WHERE ct.comision.id = c.id AND ct.tramite.id = t.id AND t.municipio.codigo = j.municipio.codigo " +
            "AND j.estructuraOrganizacional.codigo = eo.codigo " +
            "AND c.estado = :estadoComision AND c.numero IS NOT NULL " +
            "AND ((j.estructuraOrganizacional.codigo = :idTerritorial) OR (eo.estructuraOrganizacionalCod = :idTerritorial))";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("idTerritorial", territorialId);
            q.setParameter("estadoComision", estadoComision);

            answer = (List<Comision>) q.getResultList();
        } catch (NoResultException nrEx) {
            LOGGER.error("ComisionDAOBean#findComisionesPorIdTerritorialAndEstadoComision: " +
                "La consulta de comisiones por id territorial y estado comision no devolvió resultados");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, nrEx,
                "ComisionDAOBean#findComisionesPorIdTerritorialAndEstadoComision");
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta de comisiones por id territorial y estado comision: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionDAOBean#findComisionesPorIdTerritorialAndEstadoComision");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @see IComisionDAO#findFuncionariosEjecutoresByComision(Long) String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> findFuncionariosEjecutoresByComision(Long idComision,
        String territorialId, String estadoComision) {
        List<String> answer = null;
        String query = "SELECT DISTINCT t.nombreFuncionarioEjecutor " +
            "FROM Comision c JOIN c.comisionTramites ct JOIN ct.tramite t " +
            "JOIN t.municipio m JOIN m.jurisdiccions j JOIN j.estructuraOrganizacional eo " +
            "WHERE ct.comision.id = c.id AND ct.tramite.id = t.id AND t.municipio.codigo = j.municipio.codigo " +
            "AND j.estructuraOrganizacional.codigo = eo.codigo " +
            "AND c.estado = :estadoComision AND c.numero IS NOT NULL " +
            "AND ((j.estructuraOrganizacional.codigo = :idTerritorial) OR (eo.estructuraOrganizacionalCod = :idTerritorial))" +
            "AND c.id = :idComision";

        Query q = entityManager.createQuery(query);
        q.setParameter("idTerritorial", territorialId);
        q.setParameter("estadoComision", estadoComision);
        q.setParameter("idComision", idComision);

        try {
            answer = (List<String>) q.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("ComisionDAOBean#findComisionesPorIdTerritorialAndEstadoComision: " +
                "La consulta de comisiones por id territorial y estado comision no devolvió resultados");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, nrEx,
                "ComisionDAOBean#findFuncionariosEjecutoresByComision");

        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta de comisiones por id territorial y estado comision: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionDAOBean#findFuncionariosEjecutoresByComision");

        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionDAO#getComisionParaTests
     * @author fredy.wilches
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Comision getComisionParaTests() {
        String consulta = "SELECT c FROM Comision c";
        Query q = this.entityManager.createQuery(consulta);
        q.setMaxResults(1);
        return (Comision) q.getSingleResult();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionDAO#findComisionesPorTerritorialYTipoYEstado(java.lang.String,
     * java.lang.String, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Comision> findComisionesPorTerritorialYTipoYEstado(
        String territorialId, String tipoComision, String estadoComision) {

        List<Comision> answer = null;
        String queryString;
        Query query;

        queryString = "" +
            "SELECT DISTINCT NEW co.gov.igac.snc.persistence.entity.tramite.Comision(" +
            "c.id, c.numero, c.fechaInicio, c.fechaFin, c.estado, " +
            "c.duracion, c.usuarioLog, c.fechaLog) " +
            "FROM Comision c JOIN c.comisionTramites ct JOIN ct.tramite t " +
            "JOIN t.municipio m JOIN m.jurisdiccions j JOIN j.estructuraOrganizacional eo " +
            "WHERE ct.comision.id = c.id AND ct.tramite.id = t.id AND t.municipio.codigo = j.municipio.codigo " +
            "AND j.estructuraOrganizacional.codigo = eo.codigo " +
            "AND c.estado = :estadoComision_p  AND c.tipo = :tipoComision_p " +
            "AND c.numero IS NOT NULL " +
            "AND ((j.estructuraOrganizacional.codigo = :idTerritorial_p) OR (eo.estructuraOrganizacionalCod = :idTerritorial_p))";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTerritorial_p", territorialId);
            query.setParameter("tipoComision_p", tipoComision);
            query.setParameter("estadoComision_p", estadoComision);

            answer = (List<Comision>) query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionDAOBean#findComisionesPorTerritorialYEstadoYTipo");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionDAO#updateComision2(co.gov.igac.generales.dto.UsuarioDTO,
     * co.gov.igac.snc.persistence.entity.tramite.Comision)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean updateComision2(UsuarioDTO usuarioActual, Comision comision) {

        Comision updatedComision;
        boolean answer = false;
        comision.setFechaLog(new Date(System.currentTimeMillis()));
        comision.setUsuarioLog(usuarioActual.getLogin());

        try {
            updatedComision = this.update(comision);
            answer = true;
        } catch (Exception e) {
            LOGGER.error("ComisionDAOBean#updateComision2 error: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                getExcepcion(LOGGER, e, "ComisionDAOBean#updateComision2", "Comision",
                    comision.getId().toString());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionDAO#consultarActivasPorTramite(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Comision> consultarActivasPorTramite(Long idTramite, String tipoComision) {

        List<Comision> answer = null;
        String queryString;
        Query query;
        ArrayList<String> estadosComision;

        queryString = "" +
            " SELECT DISTINCT NEW co.gov.igac.snc.persistence.entity.tramite.Comision(" +
            "c.id, c.numero, c.fechaInicio, c.fechaFin, c.estado, " +
            "c.duracion, c.usuarioLog, c.fechaLog) " +
            "FROM Comision c JOIN c.comisionTramites ct JOIN ct.tramite t " +
            "WHERE c.id = ct.comision.id AND t.id = :idTramite_p " +
            "AND c.estado IN(:estadosComision_p)";

        if (tipoComision != null && !tipoComision.isEmpty()) {
            queryString +=
                " AND c.tipo = :tipoComision_p";
        }

        estadosComision = new ArrayList<String>();
        estadosComision.add(EComisionEstado.EN_EJECUCION.getCodigo());
        estadosComision.add(EComisionEstado.POR_EJECUTAR.getCodigo());
        estadosComision.add(EComisionEstado.SUSPENDIDA.getCodigo());
        estadosComision.add(EComisionEstado.APLAZADA.getCodigo());

        //D: se usan también estos estados transitorios porque puede pasar que se haya hecho la primera 
        //  parte de la administración de la comisión, pero aún no haya sido aprobada la acción por el director territorial
        estadosComision.add(EComisionEstado.POR_APROBAR_AMPLIACION.getCodigo());
        estadosComision.add(EComisionEstado.POR_APROBAR_APLAZAMIENTO.getCodigo());
        estadosComision.add(EComisionEstado.POR_APROBAR_CANCELACION.getCodigo());
        estadosComision.add(EComisionEstado.POR_APROBAR_REACTIVACION.getCodigo());
        estadosComision.add(EComisionEstado.POR_APROBAR_SUSPENSION.getCodigo());

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramite_p", idTramite.longValue());
            query.setParameter("estadosComision_p", estadosComision);

            if (tipoComision != null && !tipoComision.isEmpty()) {
                query.setParameter("tipoComision_p", tipoComision);
            }

            answer = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionDAOBean#consultarActivasPorTramite");
        }

        return answer;
    }

//end of class    
}
