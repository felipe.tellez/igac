package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioColindanteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioColindante;

/**
 *
 * @author juan.mendez
 *
 */
@Stateless
public class PredioColindanteDAOBean extends GenericDAOWithJPA<PredioColindante, Long> implements
    IPredioColindanteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioColindanteDAOBean.class);

    /**
     * @author juan.mendez (non-Javadoc)
     * @see
     * co.gov.igac.snc.dao.conservacion.IPredioColindanteDAO#getPredioColindanteByNumeroPredial(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PredioColindante> getPredioColindanteByNumeroPredial(String numeroPredial) {
        Query q = entityManager.createNamedQuery("findPredioColindanteByNumeroPredial");
        q.setParameter("numeroPredial", numeroPredial);
        return q.getResultList();
    }

    /**
     * @author juan.mendez (non-Javadoc)
     * @see
     * co.gov.igac.snc.dao.conservacion.IPredioColindanteDAO#getPredioColindanteByNumeroPredialYTipo(java.lang.String,
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PredioColindante> getPredioColindanteByNumeroPredialYTipo(String numeroPredial,
        String tipo) {
        Query q = entityManager.createNamedQuery("findPredioColindanteByNumeroPredialYTipo");
        q.setParameter("numeroPredial", numeroPredial);
        q.setParameter("tipo", tipo);
        return q.getResultList();
    }

}
