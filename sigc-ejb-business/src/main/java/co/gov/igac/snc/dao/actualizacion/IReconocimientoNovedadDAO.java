package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ReconocimientoNovedad;

/**
 * Servicios de persistencia para el objeto ReconocimientoNovedad
 *
 * @author javier.barajas
 */
@Local
public interface IReconocimientoNovedadDAO extends IGenericJpaDAO<ReconocimientoNovedad, Long> {

}
