package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IContratoPagoAvaluoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoPagoAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IContratoPagoAvaluoDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ContratoPagoAvaluoDAOBean extends
    GenericDAOWithJPA<ContratoPagoAvaluo, Long> implements
    IContratoPagoAvaluoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ContratoPagoAvaluo.class);

    /**
     * @see IContratoPagoAvaluoDAO#consultarPorProfesionalContratoPagoId(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ContratoPagoAvaluo> consultarPorProfesionalContratoPagoId(
        Long profesionalContratoPagoId) {

        List<ContratoPagoAvaluo> pagos = new ArrayList<ContratoPagoAvaluo>();

        String queryString;
        Query query;

        queryString = "SELECT cpa " +
            " FROM ContratoPagoAvaluo cpa " +
            " WHERE cpa.profesionalContratoPago.id = :profesionalContratoPagoId";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("profesionalContratoPagoId",
                profesionalContratoPagoId);

            pagos = (List<ContratoPagoAvaluo>) query.getResultList();

            for (ContratoPagoAvaluo contratoPagoAvaluo : pagos) {
                Hibernate.initialize(contratoPagoAvaluo.getAvaluo());
                Hibernate.initialize(contratoPagoAvaluo.getProfesionalContratoPago());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalAvaluosContratoDAOBean#consultarPorProfesionalContratoPagoId");
        return pagos;
    }

}
