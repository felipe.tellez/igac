/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoPermisoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPermiso;

/**
 * @see IAvaluoPermisoDAO
 * @author christian.rodriguez
 */
@Stateless
public class AvaluoPermisoDAOBean extends
    GenericDAOWithJPA<AvaluoPermiso, Long> implements IAvaluoPermisoDAO {

}
