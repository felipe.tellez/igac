package co.gov.igac.snc.dao.avaluos.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IProfesionalContratoAdicionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoAdicion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IProfesionalContratoAdicionDAO
 * @author felipe.cadena
 */
@Stateless
public class ProfesionalContratoAdicionDAOBean extends
    GenericDAOWithJPA<ProfesionalContratoAdicion, Long> implements IProfesionalContratoAdicionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ProfesionalContratoAdicionDAOBean.class);

    /**
     * @see IProfesionalContratoAdicionDAO#buscarAdicionesPorIdContrato(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<ProfesionalContratoAdicion> buscarAdicionesPorIdContrato(Long idContrato) {

        LOGGER.debug("Iniciando ProfesionalContratoAdicionDAOBean#buscarAdicionesPorIdContrato");

        List<ProfesionalContratoAdicion> resultado = null;

        String queryString;
        Query query;

        queryString = "SELECT a FROM ProfesionalContratoAdicion a " +
            "WHERE a.profesionalAvaluosContrato.id =:idContrato";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER, e,
                    "ProfesionalContratoAdicionDAOBean#buscarAdicionesPorIdContrato");

        }
        return resultado;
    }

    /**
     * @see IProfesionalContratoAdicionDAO#obtenerTotalAdicionesPorIdContrato(Long)
     * @author felipe.cadena
     */
    @Override
    public Double obtenerTotalAdicionesPorIdContrato(Long idContrato) {
        LOGGER.debug(
            "Iniciando ProfesionalContratoAdicionDAOBean#obtenerTotalAdicionesPorIdContrato");

        Double totalAdiciones = null;

        String queryString;
        Query query;

        queryString = "SELECT SUM(a.valor) " +
            " FROM ProfesionalContratoAdicion a " +
            " WHERE a.profesionalAvaluosContrato.id = :idContrato ";
        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            totalAdiciones = (Double) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ProfesionalContratoAdicionDAOBean#obtenerTotalAdicionesPorIdContrato");

        }
        return totalAdiciones;
    }

    /**
     * @see IProfesionalContratoAdicionDAO#obtenerFechaFinalPAContratoAdiciones(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public Date obtenerFechaFinalPAContratoAdiciones(Long idContrato) {
        LOGGER.
            debug("Inicio ProfesionalContratoAdicionDAOBean#obtenerFechaFinalPAContratoAdiciones");

        Date fechaFinal = null;

        String queryString;
        Query query;

        queryString = "SELECT MAX(pca.fechaHasta)" +
            " FROM ProfesionalContratoAdicion pca" +
            " WHERE pca.profesionalAvaluosContrato.id =:idContrato";

        try {
            query = this.entityManager.createQuery(queryString).setFirstResult(0).setMaxResults(1);
            query.setParameter("idContrato", idContrato);

            fechaFinal = (Date) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER, e,
                    "ProfesionalContratoAdicionDAOBean#obtenerFechaFinalPAContratoAdiciones");

        }

        LOGGER.debug("Fin ProfesionalContratoAdicionDAOBean#obtenerFechaFinalPAContratoAdiciones");

        return fechaFinal;
    }

}
