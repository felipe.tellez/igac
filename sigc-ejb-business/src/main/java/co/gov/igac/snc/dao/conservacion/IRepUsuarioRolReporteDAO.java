package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte control reporte
 *
 * @author javier.aponte
 */
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.util.FiltroDatosConsultaParametrizacionReporte;

@Local
public interface IRepUsuarioRolReporteDAO extends IGenericJpaDAO<RepUsuarioRolReporte, Long> {

    /**
     * Método que realiza la búsqueda de la parametrización de un usuario para la generación de
     * reportes
     *
     * @author david.cifuentes
     * @param usuario
     * @param categoria
     * @return
     */
    public List<RepUsuarioRolReporte> buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
        UsuarioDTO usuario, String categoria);

    /**
     * Método para búscar las parametrizaciones de reportes de un usuario dados los parámetros de
     * búsqueda ingresados en el {@link FiltroDatosConsultaParametrizacionReporte}
     *
     * @param datosConsultaParametrizacion
     * @return
     */
    public List<RepUsuarioRolReporte> buscarParametrizacionReporteUsuario(
        FiltroDatosConsultaParametrizacionReporte datosConsultaParametrizacion);

    /**
     * Método que realiza la eliminación de un {@link RepUsuarioRolReporte} por su id.
     *
     * @author david.cifuentes
     * @param idRepUsuarioRolReporte
     * @return
     */
    public boolean eliminarRepUsuarioRolReportePorId(Long idRepUsuarioRolReporte);

    /**
     * Método que realiza la búsqueda de un {@link RepUsuarioRolReporte} por su id trayendo sus
     * relaciones en fetch.
     *
     * @param id
     * @return
     */
    public RepUsuarioRolReporte buscarParametrizacionReporteUsuarioCompletaPorId(
        Long id);

    /**
     * Determina si un usuario tiene permisos nacionales para un reporte determinado
     *
     * @author felipe.cadena
     * @param usuario
     * @param reporteId
     * @return
     */
    public boolean determinarNacional(String usuario, Long reporteId);

}
