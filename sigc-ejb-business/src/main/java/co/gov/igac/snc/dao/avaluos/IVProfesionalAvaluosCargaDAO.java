package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosCarga;

/**
 * Interfaz para el dao de VProfesionalAvaluosCarga que corresponde a la vista
 * v_profesional_avaluos_carga
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IVProfesionalAvaluosCargaDAO extends
    IGenericJpaDAO<VProfesionalAvaluosCarga, Long> {

    /**
     * Método para buscar información de los avaluadores relacionados a un patron de busqueda nombre
     * y una territorial.
     *
     * @param nombreProfesional
     * @param codigoTerritorial
     * @return
     */
    public List<VProfesionalAvaluosCarga> buscarCargaAvaluadoresPorNombreYterritorial(
        String nombreProfesional, String codigoTerritorial);

    /**
     * Hace la consulta de los que pertenezcan a la territorial dada
     *
     * @author pedro.garcia
     * @param codigoTerritorial
     * @return
     */
    public List<VProfesionalAvaluosCarga> consultarPorTerritorial(String codigoTerritorial);

}
