package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoPredio
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IAvaluoPredioDAO extends IGenericJpaDAO<AvaluoPredio, Long> {

    /**
     *
     * Métdo para consultar avaluos predio asociado a un avaluo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public List<AvaluoPredio> buscarAvaluoPredioPorIdAvaluo(Long idAvaluo);

    /**
     * Método para consultar los departamentos y municipios relacionados con avaluo. La consulta se
     * hace a partir del sec radicado del avaluo.
     *
     * @author rodrigo.hernandez
     *
     * @param idAvaluo - id del avalúo
     * @return
     */
    public List<Municipio> buscarDepartamentosMunicipiosAvaluo(
        Long idAvaluo);

    /**
     * Hace la consulta por el id del Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredio> consultarPorAvaluo(long avaluoId);

}
