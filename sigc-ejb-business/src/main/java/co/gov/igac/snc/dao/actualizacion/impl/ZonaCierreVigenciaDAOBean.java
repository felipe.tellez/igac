package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IZonaCierreVigenciaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ZonaCierreVigencia;

/**
 * Implementación de los servicios de persistencia del objeto {@link ZonaCierreVigencia} .
 *
 * @author david.cifuentes
 */
@Stateless(mappedName = "ZonaCierreVigenciaDAO")
public class ZonaCierreVigenciaDAOBean extends
    GenericDAOWithJPA<ZonaCierreVigencia, Long> implements
    IZonaCierreVigenciaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ZonaCierreVigenciaDAOBean.class);

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IZonaCierreVigencia#eliminarZonasDelMunicipioCierreVigencia( List<ZonaCierreVigencia>
     * listaZonaCierreVigencia)
     */
    @Override
    public boolean eliminarZonasDelMunicipioCierreVigencia(
        List<ZonaCierreVigencia> listaZonaCierreVigencia) {

        LOGGER.debug("ZonaCierreVigenciaDAOBean#eliminarZonasDelMunicipioCierreVigencia");
        try {
            this.deleteMultiple(listaZonaCierreVigencia);
            LOGGER.debug("OK");
            return true;

        } catch (Exception e) {
            LOGGER.error(
                "ERROR: ZonaCierreVigenciaDAOBean#eliminarZonasDelMunicipioCierreVigencia: " +
                e.getMessage());
            return false;
        }
    }

}
