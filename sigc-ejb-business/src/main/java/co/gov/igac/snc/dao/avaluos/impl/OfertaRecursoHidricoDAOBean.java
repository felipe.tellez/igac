package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOfertaRecursoHidricoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaRecursoHidrico;

@Stateless
public class OfertaRecursoHidricoDAOBean extends GenericDAOWithJPA<OfertaRecursoHidrico, Long>
    implements IOfertaRecursoHidricoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OfertaRecursoHidricoDAOBean.class);

}
