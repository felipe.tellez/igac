package co.gov.igac.snc.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;

/**
 * Procesa los mensajes de error de las acciones Asincrónicas
 *
 * @author juan.mendez
 *
 */
public class ErrorUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorUtil.class);

    /**
     * Notifica por correo electrónico la ocurrencia de un error durante el procesamiento
     * asincrónico
     *
     * @param userLogin
     * @param idTramite
     */
    public static void notificarErrorPorCorreoElectronico(ExcepcionSNC e, String userLogin,
        Long idTramite, String causa) {

        if (e != null) {
            LOGGER.error(e.getMensaje(), e);
        }
        String userEmail = null;
        if (userLogin != null) {
            userEmail = userLogin +
                ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC;
        }
        //e.printStackTrace();

        String objeto = causa + " \nTrámite:" + (idTramite != null ? idTramite : " ");
        String detalle = objeto + "\n Favor comunicarse con el administrador del sistema";

        LOGGER.debug("Enviando notificación del error a través de correo electrónico...");
        LOGGER.error(detalle);
        if (userEmail != null) {
            Mailer.getInstance().sendEmail(userEmail, objeto, detalle, null, null);
        } else {
            //Cuando no se especifica destinatario, se Notifica al administrador del sistema
            Mailer.getInstance().sendEmailToSysAdmin(objeto, detalle);
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * @param mensajes
     * @return
     */
//TODO :: juan.mendez :: documentar el método. ¿porqué supone que los arreglos tienen 4 objetos? :: pedro.garcia    
    public static String getError(Object mensajes[]) {
        String mensaje = null;

        if (mensajes != null) {
            int numeroErrores = 0;
            if (mensajes.length > 0) {
                for (Object o : mensajes) {
                    List l = (List) o;
                    if (l.size() > 0) {

                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                                mensaje += (((Object[]) obje)[0] != null) ? ((Object[]) obje)[0].
                                    toString() : "";
                                mensaje += (((Object[]) obje)[1] != null) ? ((Object[]) obje)[1].
                                    toString() : "";
                                mensaje += (((Object[]) obje)[2] != null) ? ((Object[]) obje)[2].
                                    toString() : "";
                                mensaje += (((Object[]) obje)[3] != null) ? ((Object[]) obje)[3].
                                    toString() : "";
                            }
                        }
                        if (numeroErrores > 0) {
                            return mensaje;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
                return null;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
