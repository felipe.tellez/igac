package co.gov.igac.snc.fachadas;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import co.gov.igac.snc.dao.actualizacion.impl.IVCargueCicaDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.*;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.util.FiltroCargueDTO;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.business.conservacion.IAplicarCambios;
import co.gov.igac.snc.dao.actualizacion.IActualizacionComisionDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionContratoDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDocumentacionDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDocumentoDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionEventoDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionLevantamientoDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionNomenclaturaViaDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionOrdenAjusteDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionPerimetroUrbanoDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionReconocimientoDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionResponsableDAO;
import co.gov.igac.snc.dao.actualizacion.IAsignacionControlCalidadDAO;
import co.gov.igac.snc.dao.actualizacion.IComisionIntegranteDAO;
import co.gov.igac.snc.dao.actualizacion.IControlCalidadCriterioDAO;
import co.gov.igac.snc.dao.actualizacion.IControlCalidadMuestraDAO;
import co.gov.igac.snc.dao.actualizacion.IConvenioDAO;
import co.gov.igac.snc.dao.actualizacion.IConvenioEntidadDAO;
import co.gov.igac.snc.dao.actualizacion.ICriterioControlCalidadDAO;
import co.gov.igac.snc.dao.actualizacion.IEventoAsistenteDAO;
import co.gov.igac.snc.dao.actualizacion.IGeneracionFormularioSbcDAO;
import co.gov.igac.snc.dao.actualizacion.IInformacionBasicaAgroDAO;
import co.gov.igac.snc.dao.actualizacion.IInformacionBasicaCartoDAO;
import co.gov.igac.snc.dao.actualizacion.ILevantamientoAsignacionDAO;
import co.gov.igac.snc.dao.actualizacion.ILevantamientoAsignacionInfDAO;
import co.gov.igac.snc.dao.actualizacion.IMunicipioActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.IMunicipioCierreVigenciaDAO;
import co.gov.igac.snc.dao.actualizacion.IPerimetroUrbanoNormaDAO;
import co.gov.igac.snc.dao.actualizacion.IPredioActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.IPredioFormularioSbcDAO;
import co.gov.igac.snc.dao.actualizacion.IReconocimientoPredioDAO;
import co.gov.igac.snc.dao.actualizacion.IRecursoHumanoDAO;
import co.gov.igac.snc.dao.actualizacion.IReporteActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.ISaldoConservacionDAO;
import co.gov.igac.snc.dao.actualizacion.IValoresActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.IZonaCierreVigenciaDAO;
import co.gov.igac.snc.dao.conservacion.IAxMunicipioDAO;
import co.gov.igac.snc.dao.conservacion.IMPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IManzanaVeredaDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.dao.vistas.IVJurisdiccionProductoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.persistence.entity.conservacion.MPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.vistas.VJurisdiccionProducto;
import co.gov.igac.snc.persistence.util.EActualizacionTipoProceso;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EDatoRectificarTipo;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EProductoCatastralEstado;
import co.gov.igac.snc.persistence.util.EReporteActualizacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EValoresActualizacionEstado;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;
import java.util.Calendar;

@Stateless
//@Interceptors(TimeInterceptor.class)
public class ActualizacionBean implements IActualizacion, IActualizacionLocal {

    /**
     *
     */
    @SuppressWarnings("unused")
    private static final long serialVersionUID = 1L;
    /**
     * Servicio de bitácora
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionBean.class);
    /**
     * Objeto de servicio para la gestión de procesos
     *
     * @todo Reemplazar por this.getProcesoService
     */
    @EJB
    protected IProcesos procesoService;
    @EJB
    private IVJurisdiccionProductoDAO jurisdiccionProductoDAOService;
    @EJB
    private IActualizacionDAO actualizacionDAOService;
    @EJB
    private IActualizacionDocumentoDAO actualizacionDocumentoDAOService;
    @EJB
    private IActualizacionDocumentacionDAO actualizacionDocumentacionDAOService;
    @EJB
    private IActualizacionContratoDAO actualizacionContratoDAOService;
    @EJB
    private IDepartamentoDAO departamentoDAOService;
    @EJB
    private IMunicipioDAO municipioDAOService;
    @EJB
    private IConvenioDAO convenioDAOService;
    @EJB
    private IInformacionBasicaCartoDAO informacionBasicaCartograficaDAOService;
    @EJB
    private IInformacionBasicaAgroDAO informacionBasicaAgrologicaDAOService;
    @EJB
    private IConvenioEntidadDAO convenioEntidadDAOService;
    @EJB
    private IActualizacionEventoDAO actualizacionEventoDAOService;
    @EJB
    private IEventoAsistenteDAO eventoAsistenteDAOService;
    @EJB
    private IActualizacionResponsableDAO actualizacionResponsableDAOService;
    @EJB
    private IRecursoHumanoDAO recursoHumanoDAOService;
    @EJB
    private ISaldoConservacionDAO saldoConservacionDAOService;
    @EJB
    private IActualizacionComisionDAO actualizacionComisionDAOService;
    @EJB
    private IComisionIntegranteDAO comisionIntegranteDAOService;
    @EJB
    private ILevantamientoAsignacionDAO levantamientoAsignacionDAO;
    @EJB
    private IActualizacionLevantamientoDAO actualizacionLevantamientoDAO;
    @EJB
    private IGenerales generalesService;
    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;
    @EJB
    private IGeneracionFormularioSbcDAO generacionFormularioDAOservice;
    @EJB
    private IPredioFormularioSbcDAO predioFormularioSbcDAOservice;
    @EJB
    private IPredioDAO predioDao;
    @EJB
    private ILogMensajeDAO logMensajeDAO;
    @EJB
    private IActualizacionNomenclaturaViaDAO actualizacionNomenclaturaViaDao;
    @EJB
    private IControlCalidadCriterioDAO controlCalidadCriterioDao;
    @EJB
    private IControlCalidadMuestraDAO controlCalidadMuestraDao;
    @EJB
    private IManzanaVeredaDAO manzanaVeredaDAO;
    @EJB
    private IActualizacionPerimetroUrbanoDAO actualizacionPerimetroDAO;
    @EJB
    private IPerimetroUrbanoNormaDAO perimetroUrbanoNormaDAO;
    @EJB
    private IActualizacionOrdenAjusteDAO actualizacionOrdenAjusteDAO;
    @EJB
    private IReconocimientoPredioDAO reconocimientoPredioDAO;
    @EJB
    private IAsignacionControlCalidadDAO asignacionControlCalidadDao;
    @EJB
    private ICriterioControlCalidadDAO criterioControlCalidadDao;
    @EJB
    private IActualizacionReconocimientoDAO actualizacionReconocimientoDao;
    @EJB
    private IDocumentoDAO documentoDao;
    @EJB
    private ILevantamientoAsignacionInfDAO levantamientoAsignacionInfDao;
    @EJB
    private IMunicipioCierreVigenciaDAO municipioCierreVigenciaDao;
    @EJB
    private ITransaccionalLocal transaccionalService;
    @EJB
    private IZonaCierreVigenciaDAO zonaCierreVigenciaDao;
    @EJB
    private IMunicipioActualizacionDAO municipioActualizacionDao;
    @EJB
    private IPredioActualizacionDAO predioActualizacionDao;
    @EJB
    private IProductoCatastralJobDAO productoCatastralJobDAO;
    @EJB
    private IProcesos procesosService;
    @EJB
    private IMPredioDAO mPredioDao;
    @EJB
    private IValoresActualizacionDAO valoresActualizacionDAO;
    @EJB
    private IReporteActualizacionDAO reporteActualizacionDAO;
    @EJB
    private IAxMunicipioDAO axMunicipioDAO;
    @EJB
    private IPPredioDAO pPredioDao;
    @EJB
    private IVCargueCicaDAO vCargueCicaDAO;

    /**
     * Interfaces locales de servicios
     */
    @EJB
    private ITramite tramiteService;

    @EJB
    private IConservacion conservacionService;

    @EJB
    private IAplicarCambios aplicarCambios;

    /**
     * @javila @see IActualizacionLocal#ordenarRegistroDiagnosticoMunicipio(java.lang.String,
     * java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public Long ordenarRegistroDiagnosticoMunicipio(String departamentoCodigo, String zona,
        String municipioCodigo, Date vigencia,
        String responsableDiagnosticoNombre,
        String responsableDiagnosticoId, UsuarioDTO usuarioDTO)
        throws ExcepcionSNC {
        LOGGER.debug("inicio");

        // Inicio de la validación de las precondiciones
        if (null == departamentoCodigo || null == municipioCodigo ||
            null == vigencia || null == responsableDiagnosticoNombre ||
            null == responsableDiagnosticoId || null == usuarioDTO) {
            LOGGER.error(
                "Algunos de los parámetros de invocación al método ActualizacionBean:ordenarRegistroDiagnosticoMunicipio(...) es nulo");
            throw new ExcepcionSNC("JAAM::Actualización",
                ESeveridadExcepcionSNC.ERROR,
                "Alguno de los parámetros es nulo", null);
        }

        Departamento departamento = departamentoDAOService
            .getDepartamentoByCodigo(departamentoCodigo);
        if (null == departamento) {
            LOGGER.error("Departamento de código " + departamentoCodigo +
                " no encontrado.");
            throw new ExcepcionSNC("JAAM::Actualización",
                ESeveridadExcepcionSNC.ERROR, "Departamento de código " +
                departamentoCodigo + " no encontrado.", null);
        }
        Municipio municipio = municipioDAOService
            .getMunicipioByCodigo(municipioCodigo);
        if (null == municipio) {
            LOGGER.error("Municipio de código " + municipioCodigo +
                " no encontrado.");
            throw new ExcepcionSNC("JAAM::Actualización",
                ESeveridadExcepcionSNC.ERROR, "Municipio de código " +
                municipioCodigo + " no encontrado.", null);
        }
        // Fin de la validación de las precondiciones

        Actualizacion actualizacion = new Actualizacion(departamento,
            municipio, EActualizacionTipoProceso.ACTUALIZACION.toString(),
            usuarioDTO.getLogin());
        actualizacion.setFecha(vigencia);
        actualizacion.setResponsableDiagnostico(responsableDiagnosticoNombre);
        actualizacion.setResponsableDiagnosticoId(responsableDiagnosticoId);
        actualizacion.setFechaLog(new java.util.Date());
        actualizacion.setZona(zona);

        actualizacion = actualizacionDAOService.crear(actualizacion);
        // TODO actualizar a uso de ActividadesUsuarios
        //
        // Creación de la instancia del proceso de actualización.
        //
        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());

        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());
        List<UsuarioDTO> usuarios = new LinkedList<UsuarioDTO>();
        UsuarioDTO usuarioProceso = new UsuarioDTO();
        usuarioProceso.setLogin(responsableDiagnosticoId);
        usuarioProceso.setDescripcionTerritorial(usuarioDTO
            .getDescripcionEstructuraOrganizacional());
        usuarios.add(usuarioProceso);

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_INFO_DIAGNOSTICO,
                usuarios));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(usuarioDTO
            .getDescripcionEstructuraOrganizacional());
        String procesoInstanciaId = procesoService
            .crearProceso(actualizacionCatastral);

        actualizacion.setProcesoInstanciaId(procesoInstanciaId);
        actualizacionDAOService.actualizar(actualizacion);

        LOGGER.debug("fin");
        return actualizacion.getId();
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarGestionarEstudioDeCostos(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public void ordenarGestionarEstudioDeCostos(String idActividad,
        Actualizacion actualizacion, UsuarioDTO usuario) {

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());

        List<UsuarioDTO> usuarios = new LinkedList<UsuarioDTO>();
        usuarios.add(usuario);

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_ESTUDIO_COSTOS,
                usuarios));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(usuario
            .getDescripcionEstructuraOrganizacional());

        //TODO:franz.gamba :: manejar la excepcion :: 29-11-2012 :: javier.aponte
        this.procesoService.avanzarActividad(idActividad,
            actualizacionCatastral);

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarVerificarEstudioDeCostos(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion, java.util.List)
     */
    @Override
    public void ordenarVerificarEstudioDeCostos(String idActividad,
        Actualizacion actualizacion, List<UsuarioDTO> usuario)
        throws ExcepcionSNC {
        if (actualizacion == null || usuario == null) {
            throw new ExcepcionSNC(
                "FSGP::Actualización",
                ESeveridadExcepcionSNC.ERROR,
                "El objeto actualización o el usuario director territorial es nulo",
                null);
        }

        this.actualizacionDAOService.actualizar(actualizacion);

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_ESTUDIO_COSTOS_PROG,
                usuario));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(usuario.get(0)
            .getDescripcionEstructuraOrganizacional());

        //TODO:franz.gamba :: manejar la excepcion, qué debería pasar, cómo le avisa al usuario
        // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte		
        this.procesoService.avanzarActividad(idActividad,
            actualizacionCatastral);

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarRevisionEstudioDeCostos(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion, java.util.List)
     */
    @Override
    public void ordenarRevisionEstudioDeCostos(String idActividad,
        Actualizacion actualizacion, List<UsuarioDTO> usuario) {

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_ESTUDIO_COSTOS,
                usuario));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(usuario.get(0)
            .getDescripcionEstructuraOrganizacional());

        //TODO:franz.gamba :: manejar la excepcion, qué debería pasar, cómo le avisa al usuario
        // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte
        this.procesoService.avanzarActividad(idActividad,
            actualizacionCatastral);

    }

    /**
     * @author franz.gamba
     * @see co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarIngresarConvenios(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion, java.util.List)
     */
    @Override
    public void ordenarIngresarConvenios(String idActividad,
        Actualizacion actualizacion, List<UsuarioDTO> usuario) {

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios.add(new ActividadUsuarios(
            ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_CONVENIO,
            usuario));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(usuario.get(0)
            .getDescripcionEstructuraOrganizacional());

        //TODO:franz.gamba :: manejar la excepcion, qué debería pasar, cómo le avisa al usuario
        // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte
        this.procesoService.avanzarActividad(idActividad,
            actualizacionCatastral);

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarAvanzarASiguienteActividad(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion,
     * co.gov.igac.snc.persistence.util.ERol, java.lang.String, java.lang.String)
     */
    @Override
    public boolean ordenarAvanzarASiguienteActividad(String idActividad,
        Actualizacion actualizacion, ERol rol, String transicion,
        String territorial) {

        List<UsuarioDTO> usuario = LdapDAO.getUsuariosEstructuraRol2(
            territorial, rol);

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios.add(new ActividadUsuarios(transicion, usuario));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(territorial);
        try {
            this.procesoService.avanzarActividad(idActividad,
                actualizacionCatastral);

            return true;
        } catch (Exception e) {
            //TODO:franz.gamba :: Aquí esta la excepción pero sólo la captura
            //se debería poner el error en el log,
            //manejar la excepcion, qué debería pasar, cómo le avisa al usuario
            // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte
            return false;
        }
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarAvanzarASiguienteActividad(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion,
     * co.gov.igac.generales.dto.UsuarioDTO, java.lang.String, java.lang.String)
     */
    @Override
    public boolean ordenarAvanzarASiguienteActividad(String idActividad,
        Actualizacion actualizacion, UsuarioDTO usuario, String transicion,
        String territorial) {

        List<UsuarioDTO> usuarios = new LinkedList<UsuarioDTO>();
        usuarios.add(usuario);

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios.add(new ActividadUsuarios(transicion, usuarios));
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);

        actualizacionCatastral.setTerritorial(territorial);
        try {
            this.procesoService.avanzarActividad(idActividad,
                actualizacionCatastral);

            return true;
        } catch (Exception e) {
            //TODO:franz.gamba :: Aquí esta la excepción pero sólo la captura
            //se debería poner el error en el log,
            //manejar la excepcion, qué debería pasar, cómo le avisa al usuario
            // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte
            return false;
        }
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarActualizacionAutorizada(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion, java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public boolean ordenarActualizacionAutorizada(String idActividad,
        Actualizacion actualizacion, String territorial, UsuarioDTO usuario) {

        List<UsuarioDTO> directorTerritorial = LdapDAO
            .getUsuariosEstructuraRol2(territorial,
                ERol.DIRECTOR_TERRITORIAL);
        // List<UsuarioDTO> responsableActualizacion =
        // LdapDAO.getUsuariosEstructuraRol2(
        // territorial, ERol.RESPONSABLE_ACTUALIZACION);

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_DOC_TRAMITE_MUNICIPAL,
                directorTerritorial));

        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_ASIGNAR_RESPONSABLE_PROC_ACTUALIZACION,
                directorTerritorial));

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);
        actualizacionCatastral.setTerritorial(territorial);

        try {
            this.procesoService.avanzarActividad(idActividad,
                actualizacionCatastral);

            return true;
        } catch (Exception e) {
            //TODO:franz.gamba :: Aquí esta la excepción pero sólo la captura
            //se debería poner el error en el log,
            //manejar la excepcion, qué debería pasar, cómo le avisa al usuario
            // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte
            return false;
        }
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#ordenarInduccionAlPersonalYRegistroAreaDeTrabajo(java.lang.String,
     * co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion, java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public void ordenarInduccionAlPersonalYRegistroAreaDeTrabajo(
        String idActividad, Actualizacion actualizacion,
        String territorial, UsuarioDTO responsable) {

        List<UsuarioDTO> responsableActualizacion = new LinkedList<UsuarioDTO>();
        responsableActualizacion.add(responsable);

        List<ActividadUsuarios> actividadesUsuarios = new LinkedList<ActividadUsuarios>();
        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REALIZAR_INDUCCION_PERSONAL,
                responsableActualizacion));

        actividadesUsuarios
            .add(new ActividadUsuarios(
                ProcesoDeActualizacion.ACT_PLANIFICACION_PLAN_REGISTRAR_AREA_TRABAJO,
                responsableActualizacion));

        ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
        actualizacionCatastral.setIdentificador(actualizacion.getId());
        actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio()
            .getCodigo());
        actualizacionCatastral.setIdDepartamento(actualizacion
            .getDepartamento().getCodigo());
        actualizacionCatastral.setActividadesUsuarios(actividadesUsuarios);
        actualizacionCatastral.setTerritorial(territorial);

        //TODO:franz.gamba :: Aquí esta la excepción pero sólo la captura
        //se debería poner el error en el log,
        //manejar la excepcion, qué debería pasar, cómo le avisa al usuario
        // que no pudo avanzar el proceso :: 29-11-2012 :: javier.aponte
        this.procesoService.avanzarActividad(idActividad,
            actualizacionCatastral);
    }

    /**
     * @jamir.avila
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#recuperarJurisdiccionProductoPorCodigoMunicipio(java.lang.String)
     */
    @Override
    public VJurisdiccionProducto recuperarJurisdiccionProductoPorCodigoMunicipio(
        String codigoMunicipio) throws ExcepcionSNC {
        LOGGER.debug("codigoMunicipio: " + codigoMunicipio);

        VJurisdiccionProducto vJurisdiccionProducto = jurisdiccionProductoDAOService
            .recuperarJurisdiccionProductoPorCodigoMunicipio(codigoMunicipio);

        LOGGER.debug("vJurisdiccionProducto: " + vJurisdiccionProducto);

        return vJurisdiccionProducto;
    }

    /**
     * @jamir.avila
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#recuperarResponsablesDeActualizacion(java.lang.String)
     */
    @Override
    public List<UsuarioDTO> recuperarResponsablesDeActualizacion(
        String codigoTerritorial) throws ExcepcionSNC {
        String estructuraOrganizacional = codigoTerritorial;
        ERol rol = ERol.RESPONSABLE_ACTUALIZACION;

        List<UsuarioDTO> responsables = LdapDAO.getUsuariosEstructuraRol2(
            estructuraOrganizacional, rol);
        LOGGER.debug((responsables != null) ? responsables.size() +
            " encontrados." :
            "No se encontraron responsables de actualización para la territorial de código: " +
            codigoTerritorial);

        return responsables;
    }

    /**
     * @jamir.avila
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#recuperarDirectorTerritorial(java.lang.String)
     */
    @Override
    public UsuarioDTO recuperarDirectorTerritorial(String codigoTerritorial)
        throws ExcepcionSNC {
        String estructuraOrganizacional = codigoTerritorial;
        ERol rol = ERol.DIRECTOR_TERRITORIAL;

        List<UsuarioDTO> directores = LdapDAO.getUsuariosRolTerritorial(
            estructuraOrganizacional, rol);
        LOGGER.debug((directores != null) ? directores.size() + " encontrados." :
            "No se encontraron directores territoriales para la territorial de código: " +
            codigoTerritorial);

        UsuarioDTO director = null;
        if (directores != null && directores.size() > 0) {
            director = directores.get(0);
        }

        return director;
    }

    /**
     * @jamir.avila
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#registrarInformacionDiagnosticoMunicipio(co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion,
     * java.util.List, java.util.List, co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public void registrarInformacionDiagnosticoMunicipio(
        Actualizacion actualizacion,
        List<InformacionBasicaCarto> informacionesCartograficas,
        List<InformacionBasicaAgro> informacionesAgrologicas,
        UsuarioDTO usuarioDTO) throws ExcepcionSNC {
        LOGGER.debug("comenzando la operación de registro...");

        actualizacion.setInformacionBasicaCartos(informacionesCartograficas);
        for (InformacionBasicaCarto informacion : informacionesCartograficas) {
            informacionBasicaCartograficaDAOService.crear(informacion);
        }

        actualizacion.setInformacionBasicaAgros(informacionesAgrologicas);
        for (InformacionBasicaAgro informacion : informacionesAgrologicas) {
            informacionBasicaAgrologicaDAOService.crear(informacion);
        }
        actualizacionDAOService.actualizar(actualizacion);

        LOGGER.debug("fin de la operación de registro...");
    }

    /**
     * @jamir.avila
     * @see co.gov.igac.snc.fachadas.IActualizacionLocal#recuperarActualizacionPorId(java.lang.Long)
     */
    @Override
    public Actualizacion recuperarActualizacionPorId(Long idActualizacion)
        throws ExcepcionSNC {
        Actualizacion actualizacion = null;

        actualizacion = actualizacionDAOService.recuperar(idActualizacion);

        return actualizacion;
    }

    /**
     * @jamir.avila
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#recuperarConveniosPorIdActualizacion(java.lang.Long)
     */
    public List<Convenio> recuperarConveniosPorIdActualizacion(
        Long idActualizacion) throws ExcepcionSNC {
        LOGGER.debug("invocando a convenioDAOService...");
        List<Convenio> convenios = convenioDAOService
            .recuperarConveniosPorIdActualizacion(idActualizacion);
        return convenios;
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IActualizacion#eliminarEntidad(ConvenioEntidad entidad)
     */
    public void eliminarEntidad(ConvenioEntidad entidad) {
        LOGGER.debug("ActualizacionBean - Eliminando entidad");
        this.convenioEntidadDAOService.delete(entidad);
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IActualizacion#actualizarConvenio(Convenio convenio)
     */
    public Convenio actualizarConvenio(Convenio convenio) {
        LOGGER.debug("ActualizacionBean - Actualizando convenio");
        return this.convenioDAOService.update(convenio);
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IActualizacion#buscarActualizacionPorIdConConveniosYEntidades(Convenio convenio)
     */
    public Actualizacion buscarActualizacionPorIdConConveniosYEntidades(
        Long idActualizacion) throws ExcepcionSNC {
        Actualizacion answer = null;
        answer = this.actualizacionDAOService
            .buscarActualizacionPorIdConConveniosYEntidades(idActualizacion);
        if (answer != null) {
            if (answer.getConvenios() != null) {
                answer.getConvenios().size();
            }
        }
        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IActualizacion#guardarConvenioEntidad(ConvenioEntidad convenioEntidad)
     */
    public ConvenioEntidad guardarConvenioEntidad(
        ConvenioEntidad convenioEntidad) {
        LOGGER.debug("ActualizacionBean - Actualizando convenioEntidad");
        return this.convenioEntidadDAOService.update(convenioEntidad);
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IActualizacion#guardarListaEntidadConvenio(List<ConvenioEntidad>
     * entidades)
     */
    public List<ConvenioEntidad> guardarListaEntidadConvenio(
        List<ConvenioEntidad> entidades) {
        LOGGER.debug("ActualizacionBean - Actualizando lista de convenioEntidad");
        return this.convenioEntidadDAOService.updateMultipleReturn(entidades);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#crearActualizacionDocumento(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento)
     */
    @Override
    public void crearActualizacionDocumento(
        ActualizacionDocumento actualizacionDocumento) {
        this.actualizacionDocumentoDAOService.persist(actualizacionDocumento);

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#updateActualizacionDocumento(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento)
     */
    @Override
    public void updateActualizacionDocumento(
        ActualizacionDocumento actualizacionDocumento) {
        this.actualizacionDocumentoDAOService.update(actualizacionDocumento);

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#getContratosPorActualizacion(java.lang.Long)
     */
    @Override
    public List<ActualizacionContrato> getContratosPorActualizacion(
        Long actualizacionId) {
        List<ActualizacionContrato> contratos = this.actualizacionContratoDAOService
            .obtenerContratosProActualizacion(actualizacionId);
        return contratos;
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYActualizarActualizacion(co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion)
     */
    @Override
    public Actualizacion guardarYActualizarActualizacion(
        Actualizacion actualizacion) {

        return this.actualizacionDAOService.update(actualizacion);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYActualizarActualizacionDocumento(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento)
     */
    @Override
    public ActualizacionDocumento guardarYActualizarActualizacionDocumento(
        ActualizacionDocumento actualizacionDocumento) {

        return this.actualizacionDocumentoDAOService
            .update(actualizacionDocumento);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYActualizarActualizacionDocumentacion(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumentacion)
     */
    @Override
    public ActualizacionDocumentacion guardarYActualizarActualizacionDocumentacion(
        ActualizacionDocumentacion actualizacionDocumentacion) {

        return this.actualizacionDocumentacionDAOService
            .update(actualizacionDocumentacion);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYactualizarActualizacionEvento(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento)
     */
    @Override
    public ActualizacionEvento guardarYactualizarActualizacionEvento(
        ActualizacionEvento evento) {
        return this.actualizacionEventoDAOService.update(evento);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#eliminarActualizacionEvento(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento)
     */
    @Override
    public void eliminarActualizacionEvento(ActualizacionEvento evento) {

        this.actualizacionEventoDAOService.delete(evento);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYactualizarEventoAsistentes(java.util.List)
     */
    @Override
    public List<EventoAsistente> guardarYactualizarEventoAsistentes(
        List<EventoAsistente> asistentes) {

        List<EventoAsistente> asistentesEvento = new ArrayList<EventoAsistente>();

        for (EventoAsistente ea : asistentes) {
            asistentesEvento.add(this.eventoAsistenteDAOService.update(ea));
        }

        return asistentesEvento;
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYactualizarActualizacionResponsable(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable)
     */
    @Override
    public ActualizacionResponsable guardarYactualizarActualizacionResponsable(
        ActualizacionResponsable actualizacionResponsable) {

        return this.actualizacionResponsableDAOService
            .update(actualizacionResponsable);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYactualizarRecursoHumanoList(java.util.List)
     */
    @Override
    public List<RecursoHumano> guardarYactualizarRecursoHumanoList(
        List<RecursoHumano> recursosHumanos) {
        List<RecursoHumano> recursosHumanosUp = new ArrayList<RecursoHumano>();

        for (RecursoHumano rrhh : recursosHumanos) {
            recursosHumanosUp.add(this.recursoHumanoDAOService.update(rrhh));
        }
        return recursosHumanosUp;
    }

    /**
     * @author david.cifuentes
     * @see IActualizacion#guardarConvenio(Convenio convenio)
     */
    public Convenio guardarConvenio(Convenio convenio) {
        LOGGER.debug("ActualizacionBean - Actualizando convenio");
        return this.convenioDAOService.update(convenio);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#getRecursoHumanoByResponsableActId(java.lang.Long)
     */
    @Override
    public List<RecursoHumano> getRecursoHumanoByResponsableActId(
        Long responsableActId) {

        return this.recursoHumanoDAOService
            .obtenerRecursoPorResponsableActualizacion(responsableActId);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#getActualizacionConResponsables(java.lang.Long)
     */
    @Override
    public Actualizacion getActualizacionConResponsables(Long actualizacionId) {

        return this.actualizacionDAOService
            .getActualizacionConResponsablesByActualizacionId(actualizacionId);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYActualizarSaldosDeConservacion(java.util.List)
     */
    @Override
    public List<SaldoConservacion> guardarYActualizarSaldosDeConservacion(
        List<SaldoConservacion> saldos) {

        List<SaldoConservacion> answer = new LinkedList<SaldoConservacion>();

        for (SaldoConservacion s : saldos) {
            answer.add(this.saldoConservacionDAOService.update(s));
        }

        return answer;
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#obtenerSaldosConservacionPorActualizacionId(java.lang.Long)
     */
    @Override
    public List<SaldoConservacion> obtenerSaldosConservacionPorActualizacionId(
        Long actualizacionId) {

        return this.saldoConservacionDAOService
            .getSaldosConservacionByActualizacionId(actualizacionId);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#actualizarActualizacionComisiones(java.util.List)
     */
    @Override
    public List<ActualizacionComision> actualizarActualizacionComisiones(
        List<ActualizacionComision> actualizacionComisiones) {

        List<ActualizacionComision> answer = new LinkedList<ActualizacionComision>();

        for (ActualizacionComision ac : actualizacionComisiones) {
            answer.add(this.actualizacionComisionDAOService.update(ac));
        }

        return answer;
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#actualizarComisionIntegrantes(java.util.List)
     */
    @Override
    public List<ComisionIntegrante> actualizarComisionIntegrantes(
        List<ComisionIntegrante> comisionIntegrantes) {

        List<ComisionIntegrante> answer = new LinkedList<ComisionIntegrante>();

        for (ComisionIntegrante ci : comisionIntegrantes) {
            answer.add(this.comisionIntegranteDAOService.update(ci));
        }

        return answer;
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#actualizarActualizacionComision(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionComision)
     */
    @Override
    public ActualizacionComision actualizarActualizacionComision(
        ActualizacionComision actualizacionComision) {

        return this.actualizacionComisionDAOService
            .update(actualizacionComision);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYActualizarActualizacionContrato(co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato)
     */
    @Override
    public ActualizacionContrato guardarYActualizarActualizacionContrato(
        ActualizacionContrato actualizacionContrato) {

        return this.actualizacionContratoDAOService
            .update(actualizacionContrato);
    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarYActualizarListActualizacionContrato(java.util.List)
     */
    @Override
    public List<ActualizacionContrato> guardarYActualizarListActualizacionContrato(
        List<ActualizacionContrato> actualizacionContrato) {

        List<ActualizacionContrato> answer = new LinkedList<ActualizacionContrato>();

        for (ActualizacionContrato ac : actualizacionContrato) {
            answer.add(this.actualizacionContratoDAOService.update(ac));
        }

        return answer;
    }

    /**
     * TODO:franz.gamba :: documentar, revisar si se utiliza porque sólo vi que lo usan en mover
     * proceso ... :: 29-11-2012 :: javier.aponte
     */
    @Override
    public ActualizacionContrato getActualizacionContratoById(Long idContrato) {

        return this.actualizacionContratoDAOService.findById(idContrato);
    }

    /**
     * @author javier.aponte
     * @see IActualizacionLocal#getRecursoHumanoPorActualizacionId(actualizacionId)
     */
    @Override
    public List<RecursoHumano> getRecursoHumanoPorActualizacionId(
        Long actualizacionId) {
        return this.recursoHumanoDAOService
            .obtenerRecursoPorActualizacionId(actualizacionId);
    }

    /**
     * @author javier.aponte
     * @see IActualizacionLocal#guardarYactualizarEventoAsistente(EventoAsistente)
     */
    @Override
    public EventoAsistente guardarYactualizarEventoAsistente(
        EventoAsistente eventoAsistente) {

        EventoAsistente answer = null;

        answer = this.eventoAsistenteDAOService.update(eventoAsistente);

        return answer;
    }

    /**
     * @author javier.aponte
     * @see IActualizacionLocal#guardarYactualizarEventoAsistente(EventoAsistente)
     */
    @Override
    public Actualizacion obtenerUltimaActualizacionPorMunicipioYZona(
        String codigoMunicipio, String zona) {

        return this.actualizacionDAOService
            .obtenerUltimaActualizacionPorMunicipioYZona(codigoMunicipio,
                zona);
    }

    /**
     * @see
     * co.gov.igac.snc.fachadas.IActualizacionLocal#guardarActualizarEventoAsistente(co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente)
     * @author javier.barajas
     */
    @Override
    public EventoAsistente guardarActualizarEventoAsistente(
        EventoAsistente asistenteNuevo) {

        return this.eventoAsistenteDAOService.update(asistenteNuevo);
    }

    /**
     * @see IActualizacionLocal#cargarActaEnComisionDeCampo(long, ActualizacionEvento, Documento)}
     * @author javier.barajas
     */
    @Override
    public ActualizacionEvento cargarActaEnComisionDeCampo(Long actualizacionId,
        ActualizacionEvento actualizacionEvento, Documento documento) {

        //TODO: Comentado para guardar el documento
//    	Long tipoDocumentoId = documento.getTipoDocumento().getId();
//        TipoDocumento tipoDocumento = generalesService
//                .buscarTipoDocumentoPorId(tipoDocumentoId);
//        documento.setTipoDocumento(tipoDocumento);
//        documento = generalesService.guardarDocumento(documento);
        List<ActualizacionDocumento> actualizacionDocumentosLocal =
            this.actualizacionDocumentoDAOService
                .findByActualizacionId(actualizacionId);
        for (ActualizacionDocumento actualizacionDocumento : actualizacionDocumentosLocal) {
            Long actualizacionDocumentoId = actualizacionDocumento.getId();
            ActualizacionDocumento actualizacionDocumentoLocal =
                this.actualizacionDocumentoDAOService
                    .findById(actualizacionDocumentoId);
            actualizacionDocumentoLocal.setDocumento(documento);
            this.actualizacionDocumentoDAOService
                .update(actualizacionDocumentoLocal);
        }

        Actualizacion actualizacionLocal = this.actualizacionDAOService
            .findById(actualizacionId);
        actualizacionEvento.setActualizacion(actualizacionLocal);
        this.actualizacionEventoDAOService.persist(actualizacionEvento);

        actualizacionEvento = this.actualizacionDAOService
            .agregarActualizacionEvento(actualizacionEvento);
        return actualizacionEvento;

    }

    /**
     * @see IActualizacionLocal#obtenerPrediosdeActualizacionPorZonas(ActualizacionId)
     * @author javier.barajas
     */
    @SuppressWarnings("unused")
    @Override
    public List<Object[]> obtenerPrediosdeActualizacionPorZonas(
        Long ActualizacionId, String sortField, String sortOrder,
        boolean iscount,
        final int... rowStartIdxAndCount) {

        FiltroGenerarReportes datosConsultaPredio = new FiltroGenerarReportes();
        List<Convenio> listaConvenios = new ArrayList<Convenio>();
        String zona = "";
        List<Object[]> answer = null;

        Actualizacion actulzacionSelecionada = this.actualizacionDAOService.
            findById(ActualizacionId);
        datosConsultaPredio.setDepartamentoId(actulzacionSelecionada.getDepartamento().getCodigo());
        datosConsultaPredio.setMunicipioId(actulzacionSelecionada.getMunicipio().getCodigo());

//TODO :: javier.aponte :: hacer cambios a esto por cambios en el filtro :: 13/09/2013
        /*
         * datosConsultaPredio.setNumeroPredialS1(actulzacionSelecionada.getDepartamento().getCodigo());
         * datosConsultaPredio.setNumeroPredialS1f(actulzacionSelecionada.getDepartamento().getCodigo());
         * datosConsultaPredio.setNumeroPredialS2(actulzacionSelecionada.getMunicipio().getCodigo().
         * replace(actulzacionSelecionada.getDepartamento().getCodigo(), ""));
         * datosConsultaPredio.setNumeroPredialS2f(actulzacionSelecionada.getMunicipio().getCodigo().
         * replace(actulzacionSelecionada.getDepartamento().getCodigo(), ""));
         */
        try {
            //TODO:Comentado para obtener la zona rural desde el objeto de la actualizacion
//        	listaConvenios = this.recuperarConveniosPorIdActualizacion(ActualizacionId);

            if (actulzacionSelecionada.getZona().equals("RURAL")) {
                zona = "00";
            }
            if (actulzacionSelecionada.getZona().equals("URBANA")) {
                zona = "01";
            }

//TODO :: javier.aponte :: hacer cambios a esto por cambios en el filtro :: 13/09/2013
            /*
             * datosConsultaPredio.setNumeroPredialS3(zona);
             * datosConsultaPredio.setNumeroPredialS3f(zona);
             * datosConsultaPredio.assembleNumeroPredialFromSegments();
             */
            try {
                answer = this.sncProcedimientoDao.buscarPrediosPorCriteriosRangoNumeroPredial(
                    datosConsultaPredio, iscount, 1, rowStartIdxAndCount);

            } catch (ExcepcionSNC ex) {
                LOGGER.error("Excepción en buscarPredios: " + ex.getMensaje());

            }

        } catch (Exception e) {
            LOGGER.error("Excepción en buscarConvenios: " + e.getMessage());
        }
        return answer;
    }

    /**
     * @see IActualizacionLocal#guardarGeneracionFormulariosSbc( Long actualizacionId, Documento
     * documento, GeneracionFormularioSbc generarFormularios)
     * @author javier.barajas
     */
    @Override
    public GeneracionFormularioSbc guardarGeneracionFormulariosSbc(
        Long actualizacionId, Documento documento,
        GeneracionFormularioSbc generarFormularios) {

        Long tipoDocumentoId = documento.getTipoDocumento().getId();
        //TODO : javier.barajas :: quitar este código si no se usa:: 29-11-2012 :: javier.aponte
        //TODO : javier.barajas :: quitar este código si no se usa:: 29-11-2012 :: javier.aponte

        TipoDocumento tipoDocumento = generalesService
            .buscarTipoDocumentoPorId(tipoDocumentoId);
        //TODO : javier.barajas :: quitar este código si no se usa:: 29-11-2012 :: javier.aponte
        //TODO : javier.barajas :: quitar este código si no se usa:: 29-11-2012 :: javier.aponte

        documento.setTipoDocumento(tipoDocumento);
        documento = generalesService.guardarDocumento(documento);

        Actualizacion actualizacionLocal = this.actualizacionDAOService
            .findById(actualizacionId);
        generarFormularios.setActualizacion(actualizacionLocal);
        generarFormularios.setDocumento(documento);
        this.generacionFormularioDAOservice.persist(generarFormularios);
        generarFormularios = this.actualizacionDAOService
            .agregarGenerarFormularioSbc(generarFormularios);
        LOGGER.debug("actualizacionDAOService.agregarGenerarFormularioSbc(generarFormularios): " +
            generarFormularios.getId());

        return generarFormularios;
    }

    /**
     * @see IActualizacionLocal#guardarYactualizarPredioFormularioSbc(List prediosFormularios)
     * @author javier.barajas
     */
    @Override
    public List<PredioFormularioSbc> guardarYactualizarPredioFormularioSbc(
        List<PredioFormularioSbc> prediosFormularios) {

        List<PredioFormularioSbc> prediosActualizar = new ArrayList<PredioFormularioSbc>();

        for (PredioFormularioSbc ea : prediosFormularios) {
            prediosActualizar.add(this.predioFormularioSbcDAOservice
                .update(ea));
        }

        return prediosActualizar;
    }

    /**
     * @see IActualizacionLocal#obtenerManzanasPorZonaActualizacion(Long actualizacionId)
     * @author javier.barajas
     */
    @Override
    public List<ManzanaVereda> obtenerManzanasPorZonaActualizacion(String barrio) {

        List<ManzanaVereda> manzanaZona = new ArrayList<ManzanaVereda>();
        try {
            LOGGER.debug("Codigo Barrio: " + barrio);
            manzanaZona = this.generalesService.getManzanaVeredaFromBarrio(barrio);
            LOGGER.debug("obtenerManzanasPorZonaActualizacion " + manzanaZona.size());
        } catch (Exception e) {
            LOGGER.error("getManzanaVeredaFromBarrio: " + e.getMessage());
        }
        return manzanaZona;
    }

    /**
     * @see IActualizacionLocal#obtenerManzanasExportar(Long actualizacionId)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionLevantamiento> obtenerManzanasExportar(
        Long idActulizacion) {

        List<ActualizacionLevantamiento> manzanasExportar =
            new ArrayList<ActualizacionLevantamiento>();
        try {
            manzanasExportar = this.actualizacionDAOService.obtenerActualizacionLevantamiento(
                idActulizacion, null);

        } catch (Exception e) {
            LOGGER.error("getManzanaVeredaFromBarrio: " + e.getMessage());
        }
        return manzanasExportar;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IActualizacionLocal#obtenerActComisionPoractualizacionIdYObjeto(Long, String)
     */
    @Override
    public ActualizacionComision obtenerActComisionPoractualizacionIdYObjeto(
        Long actualizacionId, String objeto) {
        ActualizacionComision comision = null;

        try {
            comision = this.actualizacionComisionDAOService.
                getActualizacionComisionByActualizacionId(actualizacionId, objeto);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return comision;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IActualizacionLocal#obtenerComisionIntegrantesByActualizacionIdYObjeto(Long, String)
     */
    @Override
    public List<ComisionIntegrante> obtenerComisionIntegrantesByActualizacionIdYObjeto(
        Long actualizacionId, String objeto) {
        List<ComisionIntegrante> integrantes = null;

        try {
            integrantes = this.comisionIntegranteDAOService.
                getcomisionIntegrantesByActualizacionIdAndObjeto(actualizacionId, objeto);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return integrantes;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IActualizacionLocal#actualizarComisionIntegrante(ComisionIntegrante)
     */
    @Override
    public ComisionIntegrante actualizarComisionIntegrante(
        ComisionIntegrante integrante) {
        ComisionIntegrante answer = null;

        try {
            answer = this.comisionIntegranteDAOService.update(integrante);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @see IActualizacionLocal#buscarPrediosTramitePorNumeroManzana(String[] manzanasBuscar)
     * @author javier.barajas
     */
    @Override
    public List<Predio> buscarPrediosTramitePorNumeroManzana(
        String[] manzanasBuscar) {
        List<Predio> prediosManzana = new ArrayList<Predio>();
        for (String m : manzanasBuscar) {
            prediosManzana.addAll(this.predioDao.obtenerPrediosPorNumeroManzana(m));

        }
        return prediosManzana;
    }

    /**
     * @see IActualizacionLocal#buscarActualizacionLevantamientoPorActualizacionId(Long
     * actualizacionId, Long levantamientoId)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionLevantamiento> buscarActualizacionLevantamientoPorActualizacionId(
        Long actualizacionId, Long levantamientoId) {

        return this.actualizacionDAOService.obtenerActualizacionLevantamiento(actualizacionId,
            levantamientoId);
    }

    /**
     * @see
     * IActualizacionLocal#buscarTopografosLevantamientoActualizacion(List<ActualizacionLevantamiento>
     * listaLevantamiento)
     * @author javier.barajas
     */
    @Override
    public List<LevantamientoAsignacion> buscarTopografosLevantamientoActualizacion(
        List<ActualizacionLevantamiento> listaLevantamiento) {

        return this.actualizacionDAOService.obtenerTopografosActualizacion(listaLevantamiento);
    }

    /**
     *
     * @see IActualizacionLocal#exportarManzanasaTopografos(String codManzanas, String formato,
     * ArcgisServerTokenDTO token, co.gov.igac.snc.fachadas.UsuarioDTO usuario)
     * @author javier.barajas
     */
    @Override
    public String exportarManzanasaTopografos(String codManzanas,
        String formato, UsuarioDTO usuario) {
        /*
         * ArcgisServerTokenDTO token = SigDAO.getInstance(logMensajeDAO).obtenerToken(usuario);
         * String rutaArchivo = SigDAO.getInstance(logMensajeDAO).exportarCadShape(codManzanas,
         * formato, token, usuario); return rutaArchivo;
         */
        throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
            "Servicio No Implementado. La invocación debe realizarse de forma asincrónica");
    }

    /**
     * @see
     * IActualizacionLocal#guardarYActualizarActualizacionNomenclaturaVia(ActualizacionNomenclaturaVia)
     * @author david.cifuentes
     */
    @Override
    public ActualizacionNomenclaturaVia guardarYActualizarActualizacionNomenclaturaVia(
        ActualizacionNomenclaturaVia actualizacionNomenclaturaVia) {

        try {
            actualizacionNomenclaturaVia = this.actualizacionNomenclaturaViaDao
                .update(actualizacionNomenclaturaVia);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(
                LOGGER, e, "ActualizacionNomenclaturaVia");
        }
        return actualizacionNomenclaturaVia;
    }

    /**
     * @see IActualizacionLocal#obtenerRecursoHumanoPorActividad(String)
     * @author javier.aponte
     */
    @Override
    public List<RecursoHumano> obtenerRecursoHumanoPorActividad(String actividad) {

        return this.recursoHumanoDAOService.obtenerRecursoHumanoPorActividad(actividad);

    }

    /**
     * @see
     * IActualizacionLocal#asignarTopografoAManzanasYAreas(ActualizacionLevantamiento[],RecursoHumano,
     * UsuarioDTO)
     * @author javier.aponte
     */
    @Override
    public LevantamientoAsignacion asignarTopografoAManzanasYAreas(
        ActualizacionLevantamiento[] actualizacionLevantamientos,
        RecursoHumano topografo, UsuarioDTO usuario) {

        LevantamientoAsignacion levantamientoAsignacion = this.levantamientoAsignacionDAO.
            buscarLevantamientoAsignacionPorTopografoIdentificacion(topografo.
                getDocumentoIdentificacion());

        if (levantamientoAsignacion == null) {
            levantamientoAsignacion = new LevantamientoAsignacion();
            levantamientoAsignacion.setFecha(new Date(System.currentTimeMillis()));
// TODO :: javier.aponte :: Comentado para compilar, ajustar con los nuevos cambios de la entidad LevantamientoAsignacion :: david.cifuentes :: 06/11/2012
            // levantamientoAsignacion.setTopografoNombre(topografo.getNombre());
            //levantamientoAsignacion.setTopografoIdentificacion(topografo.getDocumentoIdentificacion());
            levantamientoAsignacion.setUsuarioLog(usuario.getLogin());
            levantamientoAsignacion.setFechaLog(new Date(System.currentTimeMillis()));
            levantamientoAsignacion = this.levantamientoAsignacionDAO.
                update(levantamientoAsignacion);
        }

        for (ActualizacionLevantamiento temp : actualizacionLevantamientos) {
            temp.setLevantamientoAsignacion(levantamientoAsignacion);
        }

        this.actualizacionLevantamientoDAO.
            updateMultiple(Arrays.asList(actualizacionLevantamientos));

        return levantamientoAsignacion;

    }

    /**
     * @see IManzanaVeredaDAO#obtenerManzanasPorMunicipio(int)
     * @author javier.barajas
     */
    @Override
    public List<ManzanaVereda> obtenerManzanasMunicipio(String departamentoMunicipio) {
        LOGGER.debug("ActualizacionBean#obtenerManzanasMunicipio...INICIA");
        try {
            List<ManzanaVereda> manzanasMunicipio = manzanaVeredaDAO.obtenerManzanasPorMunicipio(
                departamentoMunicipio);
            return manzanasMunicipio;

        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#obtenerManzanasMunicipio...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#agregarActualizacionReconocimiento(ActualizacionReconocimiento)
     * @author javier.barajas
     */
    @Override
    public ActualizacionReconocimiento agregarActualizacionReconocimiento(
        ActualizacionReconocimiento actReconocimiento) {
        try {
            return this.actualizacionReconocimientoDao.update(actReconocimiento);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_0001.
                getExcepcion(LOGGER, e, e.getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#agregarActualizacionReconocimiento...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscarrecursoHumanoporActividadYActualizacionId(Long,String)
     * @author javier.barajas
     */
    @Override
    public List<RecursoHumano> buscarrecursoHumanoporActividadYActualizacionId(
        Long actualizacionId, String actividad) {
        try {
            return this.recursoHumanoDAOService.obtenerRecuroHumanoporActualizacionId(
                actualizacionId, actividad);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionBean#buscarrecursoHumanoporActividadYActualizacionId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#getManazanasporMunicipioAsignarCoordinadorPaginacion(String,int...)
     * @author javier.barajas
     */
    @Override
    public List<ManzanaVereda> getManazanasporMunicipioAsignarCoordinadorPaginacion(
        String municipio, final int... rowStartIdxAndCount) {
        try {
            return this.manzanaVeredaDAO.obtenerManazanasporMunicipioAsignarCoordinadorPaginacion(
                municipio, rowStartIdxAndCount);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionBean#getManazanasporMunicipioAsignarCoordinadorPaginacion...finaliza");
        }
    }

    /**
     * @see
     * IActualizacionLocal#getManazanasporMunicipioAsignarCoordinadorPaginacionActualizacionReconocimiento(String,int...)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionReconocimiento> getManazanasporMunicipioAsignarCoordinadorPaginacionActualizacionReconocimiento(
        Long actualizacionId, final int... rowStartIdxAndCount) {
        try {
            return this.actualizacionReconocimientoDao.
                getActualizacionReconocimientoByActualizacionId(actualizacionId, rowStartIdxAndCount);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionBean#getManazanasporMunicipioAsignarCoordinadorPaginacionActualizacionReconocimiento...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#guardarDocumento(documento, actualizacion, usuario)
     * @author javier.aponte
     */
    @Override
    public Documento guardarDocumentoDeActualizacionEnAlfresco(Documento documento,
        Actualizacion actualizacion,
        UsuarioDTO usuario) {
        Documento answer = null;
        try {
            answer = this.documentoDao.
                almacenarDocumentoMemoriaTecnicaActualizacion(documento, actualizacion, usuario);
        } catch (Exception e) {
            LOGGER.
                error("Ocurrió un error en el método de guardarDocumentoDeActualizacionEnAlfreco");
        }
        return answer;
    }

    /**
     * @see IActualizacionLocal#guardarActualizacionPerimetroUrbano(ActualizacionPerimetroUrbano)
     */
    @Override
    public ActualizacionPerimetroUrbano guardarActualizacionPerimetroUrbano(
        ActualizacionPerimetroUrbano actualizacionPerimetroUrbano, UsuarioDTO usuario) {

        LOGGER.debug("ActualizacionBean#guardarActualizacionPerimetroUrbano... INICIA");
        try {

            actualizacionPerimetroUrbano.setFechaLog(new Date(System.currentTimeMillis()));
            actualizacionPerimetroUrbano.setUsuarioLog(usuario.getLogin());
            ActualizacionPerimetroUrbano actualizacionPerimetro = actualizacionPerimetroDAO.update(
                actualizacionPerimetroUrbano);
            return actualizacionPerimetro;

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
        } finally {
            LOGGER.debug("ActualizacionBean#guardarActualizacionPerimetroUrbano... FINALIZA");

        }

    }

    /**
     * @see
     * IActualizacionLocal#guardarPerimetroUrbanoNorma(co.gov.igac.snc.persistence.entity.actualizacion.PerimetroUrbanoNorma)
     */
    @Override
    public PerimetroUrbanoNorma guardarPerimetroUrbanoNorma(
        PerimetroUrbanoNorma perimetroUrbanoNorma, UsuarioDTO usuario) {
        LOGGER.debug("ActualizacionBean#guardarPerimetroUrbanoNorma... INICIA");
        try {
            perimetroUrbanoNorma.setUsuarioLog(usuario.getLogin());
            perimetroUrbanoNorma.setFechaLog(new Date(System.currentTimeMillis()));
            PerimetroUrbanoNorma perimetroUrbNorma = perimetroUrbanoNormaDAO.update(
                perimetroUrbanoNorma);
            return perimetroUrbNorma;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
        } finally {
            LOGGER.debug("ActualizacionBean#guardarPerimetroUrbanoNorma... FINALIZA");
        }
    }

    /**
     * @see IActualizacionLocal#obtenerActualizacionConMunicipio(Long)
     */
    @Override
    public Actualizacion obtenerActualizacionConMunicipio(Long actualizacionId) {
        LOGGER.debug("ActualizacionBean#obtenerActualizacionConMunicipio... INICIA");

        try {
            Actualizacion unaActualizacion = actualizacionDAOService.
                obtenerActualizacionConMunicipio(actualizacionId);
            return unaActualizacion;
        } catch (Exception e) {
            LOGGER.debug("Error en obtenerActualizacionConMunicipio(");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
        } finally {
            LOGGER.debug("ActualizacionBean#obtenerActualizacionConMunicipio... FINALIZA");
        }

    }

    /**
     * @see IActualizacionLocal#obtenerManzanasPorRango(String, String)
     */
    @Override
    public List<ManzanaVereda> obtenerManzanasPorRango(String rangoInicial, String rangoFinal) {
        LOGGER.debug("ActualizacionBean#obtenerManzanasPorRang...INICIA");
        try {
            List<ManzanaVereda> manzanasEnRango = manzanaVeredaDAO.obtenerManzanasPorRango(
                rangoInicial, rangoFinal);
            return manzanasEnRango;
        } catch (Exception e) {
            LOGGER.debug(
                "Error en la recuperacion de un rango de manzanas en ActualizacionBean#obtenerManzanasPorRango");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
        } finally {
            LOGGER.debug("ActualizacionBean#obtenerManzanasPorRang...FINALIZA");
        }

    }

    /**
     * @see IActualizacionLocal#guardarActualizacionOrdenAjuste(ActualizacionOrdenAjuste, List)
     */
    @Override
    public ActualizacionOrdenAjuste guardarActualizacionOrdenAjuste(
        ActualizacionOrdenAjuste actualizacionOrdenAjuste) {
        LOGGER.debug("ActualizacionBean#guardarActualizacionOrdenAjuste...INICIA");
        ActualizacionOrdenAjuste ordenAjuste = null;
        try {
            ordenAjuste = actualizacionOrdenAjusteDAO.update(actualizacionOrdenAjuste);
        } catch (Exception e) {
            LOGGER.debug("Error guardando orden de ajuste");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
        }

        return ordenAjuste;
    }

    /**
     * Guarda la asignacion de predios a un reconocedor en el procesode de reconocimiento en
     * actualizacion.
     *
     * @param reconocimientoPredio asignaciones de predios al reconocedor.
     * @return
     * @author andres.eslava
     */
    @Override
    public ReconocimientoPredio guardarReconocimientoPredio(
        ReconocimientoPredio reconocimientoPredio) {

        LOGGER.debug("ActualizacionBean#guardarReconocimientoPredio...INICIA");
        try {
            ReconocimientoPredio unReconocimientoPredio = reconocimientoPredioDAO.update(
                reconocimientoPredio);
            return unReconocimientoPredio;
        } catch (Exception e) {
            LOGGER.debug("Error guardando el reconocimiento del predio");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
        } finally {
            LOGGER.debug("ActualizacionBean#guardarReconocimientoPredio...FINALIZA");
        }
    }

    /**
     * @see IActualizacionLocal#buscarActualizacionReconocimientoporId(Long)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionReconocimiento> buscarActualizacionReconocimientoporId(
        Long actualizacionId) {
        try {

            return this.actualizacionReconocimientoDao.
                getActualizacionReconocimientoByActualizacionId(actualizacionId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscarActualizacionReconocimientoporId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscarRecursoHumanoporId(Long)
     * @author javier.barajas
     */
    @Override
    public RecursoHumano buscarRecursoHumanoporId(Long recursoHumanoId) {
        try {
            return this.recursoHumanoDAOService.findById(recursoHumanoId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscarRecursoHumanoporId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscarLevantamientoAsignacionId(Long)
     * @author javier.barajas
     */
    @Override
    public LevantamientoAsignacion buscarLevantamientoAsignacionId(
        Long levantamientoAsignacionId) {
        try {
            return this.levantamientoAsignacionDAO.findById(levantamientoAsignacionId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscarLevantamientoAsignacionId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscarCriterioControlCalidad(Long)
     * @author javier.barajas
     */
    @Override
    public CriterioControlCalidad buscarCriterioControlCalidad(
        Long criterioControlCalidadId) {
        try {
            return this.criterioControlCalidadDao.consultarCriterioControlCalidad(
                criterioControlCalidadId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscarCriterioControlCalidad...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#guardarYactualizarAsignacionControlCalidad(Long)
     * @author javier.barajas
     */
    @Override
    public AsignacionControlCalidad guardarYactualizarAsignacionControlCalidad(
        AsignacionControlCalidad asignacionControlCalidad) {
        try {
            this.asignacionControlCalidadDao.persist(asignacionControlCalidad);
            return asignacionControlCalidad;
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#guardarYactualizarAsignacionControlCalidad...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#guardarYactualizarControlCalidadMuestra(Long)
     * @author javier.barajas
     */
    @Override
    public ControlCalidadMuestra guardarYactualizarControlCalidadMuestra(
        ControlCalidadMuestra muestra) {
        try {
            return this.controlCalidadMuestraDao.update(muestra);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#guardarYactualizarControlCalidadMuestra...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#guardarYactualizarControlCalidad(List<ControlCalidadCriterio>)
     * @author javier.barajas
     */
    @Override
    public List<ControlCalidadCriterio> guardarYactualizarControlCalidad(
        List<ControlCalidadCriterio> listaCotrolCalidad) {

        List<ControlCalidadCriterio> listaControlCalidadCriterio =
            new ArrayList<ControlCalidadCriterio>();
        try {
            for (ControlCalidadCriterio ccc : listaCotrolCalidad) {
                listaControlCalidadCriterio.add(this.controlCalidadCriterioDao.update(ccc));
            }

            return listaControlCalidadCriterio;
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#guardarYactualizarControlCalidad...finaliza");
        }

    }

    /**
     * @see IActualizacionLocal#buscarManzanasporTopografosActualizacionId(Long, Long)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionLevantamiento> buscarManzanasporTopografosActualizacionId(
        Long actualizacionId, Long recursoHumanoId) {
        LevantamientoAsignacion levantamientoId = this.levantamientoAsignacionDAO.
            buscarLevantamientoAsignacionPorRecursoHumanoId(recursoHumanoId);
        try {
            return this.actualizacionDAOService.obtenerManazanasporTopografosActualizacion(
                actualizacionId, levantamientoId.getId());
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscarManzanasporTopografosActualizacionId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscarTopografosActualizacionId(Long)
     * @author javier.barajas
     */
    @Override
    public List<RecursoHumano> buscarTopografosActualizacionId(
        Long actualizacionId) {
        try {
            return this.recursoHumanoDAOService.obtenerTopografosporActualizacionId(actualizacionId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscarTopografosActualizacionId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscaLevantamientoTopograficoInforme(Long)
     * @author javier.barajas
     */
    @Override
    public LevantamientoAsignacionInf buscaLevantamientoTopograficoInforme(
        Long levantamientoAsignacionId) {
        try {
            return this.levantamientoAsignacionInfDao.
                obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId(
                    levantamientoAsignacionId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#buscaLevantamientoTopograficoInforme...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscaLevantamientoAsignacionconRecursoHumanoId(Long)
     * @author javier.barajas
     */
    @Override
    public LevantamientoAsignacion buscaLevantamientoAsignacionconRecursoHumanoId(
        Long recursoHumanoId) {
        try {
            return this.levantamientoAsignacionDAO.buscarLevantamientoAsignacionPorRecursoHumanoId(
                recursoHumanoId);
        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionBean#buscaLevantamientoAsignacionconRecursoHumanoId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#buscaActualizacionReconocimientoPorRecursoHumanoId(Long)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionReconocimiento> buscaActualizacionReconocimientoPorRecursoHumanoId(
        Long recursoHumanoId) {
        try {
            return this.actualizacionReconocimientoDao.
                getActualizacionReconocimientoporRecursoHumanoId(recursoHumanoId);

        } catch (Exception e) {
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionBean#buscaLevantamientoAsignacionconRecursoHumanoId...finaliza");
        }
    }

    /**
     * @see IActualizacionLocal#obtenerOrdenesAjustePorResponsableSig(RecursoHumano)
     * @author andres.eslava
     */
    @Override
    public List<ActualizacionOrdenAjuste> obtenerOrdenesAjustePorResponsableSig(
        RecursoHumano responsableSig) {
        LOGGER.debug("ActualizacionBean#obtenerOrdenesAjustePorResponsableSig...INICIA");
        List<ActualizacionOrdenAjuste> listaDeAjustes = null;
        try {
            listaDeAjustes = actualizacionOrdenAjusteDAO.getOrdenesAjustePorResponsableSIG(
                responsableSig);
            return listaDeAjustes;

        } catch (Exception e) {
            LOGGER.debug("Error en la busqueda de ordenes de ajuste por Responsable SIG");
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("ActualizacionBean#obtenerOrdenesAjustePorResponsableSig...FINALIZA");
        }
    }

    /**
     * @see IActualizacionLocal#obtenerManzanasAsignadasPorReconocedor(RecursoHumano)
     * @author andres.eslava
     */
    public List<ActualizacionReconocimiento> obtenerManzanasAsignadasPorReconocedor(
        RecursoHumano cordinador) {
        LOGGER.debug("ActualizacionBean#obtenerManzanasAsignadasPorReconocedor...INICIA");

        List<ActualizacionReconocimiento> listaActualizacionReconocimiento;
        listaActualizacionReconocimiento = actualizacionReconocimientoDao.
            getActualizacionReconocimientoByRecursoHumanoId(cordinador.getId());

        LOGGER.debug("ActualizacionBean#obtenerManzanasAsignadasPorReconocedor...FINALIZA");

        return listaActualizacionReconocimiento;
    }

    /**
     * @see IActualizacionLocal#obtenerPrediosPorManzana(String)
     * @author andres.eslava
     */
    public List<Predio> obtenerPrediosPorManzana(String codigoManzana) {
        LOGGER.debug("ActualizacionBean#obtenerManzanasAsignadasPorReconocedor...INICIA");

        List<Predio> prediosManzana = predioDao.obtenerPrediosPorNumeroManzana(codigoManzana);

        LOGGER.debug("ActualizacionBean#obtenerManzanasAsignadasPorReconocedor...FINALIZA");

        return prediosManzana;
    }

    /**
     * @see IActualizacionLocal#obtenerRecursoHumanoPorIdentificacionYActualizacionId(String, Long)
     * @author andres.eslava
     */
    @Override
    public RecursoHumano obtenerRecursoHumanoPorIdentificacionYActualizacionId(
        String identificacionRecursoHumano, Long actualizacionId) {

        LOGGER.debug(
            "ActualizacionBean#obtenerRecursoHumanoPorIdentificacionYActualizacionId...INICIA");

        RecursoHumano unRecursoHumano;
        unRecursoHumano = recursoHumanoDAOService
            .obtenerRecursoHumanoByIdentificacionYActualizacionId(
                identificacionRecursoHumano, actualizacionId);

        LOGGER.debug(
            "ActualizacionBean#obtenerRecursoHumanoPorIdentificacionYActualizacionId...FINALIZA");

        return unRecursoHumano;
    }

    /**
     * @see IActualizacionLocal#obtenerReconocimientosPredioPorManzana(String)
     */
    @Override
    public List<ReconocimientoPredio> obtenerReconocimientosPredioPorManzana(String codigoManzana) {

        LOGGER.debug("ActualizacionBean#obtenerReconocimientosPredio...INICIA");
        List<ReconocimientoPredio> reconocimientos = reconocimientoPredioDAO.
            obtenerReconocimietosPrediosPorManzana(codigoManzana);
        LOGGER.debug("ActualizacionBean#obtenerReconocimientosPredio...FINALIZA");
        return reconocimientos;

    }

    /**
     * @see IActualizacionLocal#getSaldosConservacionByNumeroPredial(String)
     * @author andres.eslava
     */
    @Override
    public List<SaldoConservacion> getSaldosConservacionByNumeroPredial(
        String numeroPredial) {
        LOGGER.debug("ActualizacionBean#getSaldosConservacionBynNumeroPredial...INICIA");
        List<SaldoConservacion> saldos = saldoConservacionDAOService.
            getSaldosConservacionByNumeroPredial(numeroPredial);
        LOGGER.debug("ActualizacionBean#getSaldosConservacionBynNumeroPredial...FINALIZA");
        return saldos;

    }

    /**
     * @see IActualizacionLocal#getPrediosLibreActualizacionReconocimientoYSaldoConservacion(String)
     * @author andres.eslava
     */
    @Override
    public List<Predio> getPrediosLibreActualizacionReconocimientoYSaldoConservacion(
        String codigoManzana) {

        LOGGER.debug(
            "ActualizacionBean#getPrediosLibreActualizacionReconocimientoYSaldoConservacion...INICIA");

        List<Predio> listaDePrediosSinProcesosReconocimiento = null;
        listaDePrediosSinProcesosReconocimiento = predioDao.
            getPrediosLibreActualizacionReconocimientoYSaldoConservacion(codigoManzana);

        LOGGER.debug(
            "ActualizacionBean#getPrediosLibreActualizacionReconocimientoYSaldoConservacion...FINALIZA");

        return listaDePrediosSinProcesosReconocimiento;

    }

    /**
     * @see IActualizacionLocal#getActualizacionReconocimientoById(Long)
     * @author andres.eslava
     */
    public ActualizacionReconocimiento getActualizacionReconocimientoById(
        Long idActualizacionReconocimiento) {

        LOGGER.debug("ActualizacionBean#getActualizacionReconocimiento...INICIA");

        ActualizacionReconocimiento actualizacionReconocimiento = null;
        actualizacionReconocimiento = actualizacionReconocimientoDao.findById(
            idActualizacionReconocimiento);

        LOGGER.debug("ActualizacionBean#getActualizacionReconocimiento...FINALIZA");

        return actualizacionReconocimiento;
    }

    /**
     * @see IActualizacionLocal#guardaActualizacionLevantamiento(ActualizacionLevantamiento)
     * @author andres.eslava
     */
    @Override
    public ActualizacionLevantamiento guardaActualizacionLevantamiento(
        ActualizacionLevantamiento unaActualizacionLevantamiento) {
        ActualizacionLevantamiento actualizacionLevantamiento = null;
        LOGGER.debug("ActualizacionBean#guardaActualizacionLevantamiento...INICIA");
        actualizacionLevantamiento = this.actualizacionLevantamientoDAO.update(
            unaActualizacionLevantamiento);
        LOGGER.debug("ActualizacionBean#guardaActualizacionLevantamiento...FINALIZA");
        return actualizacionLevantamiento;
    }

    // ------------------------------------------------ //
    /**
     * @see IActualizacion# consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios
     * (DecretoAvaluoAnual decretoAvaluoAnual, List<String>
     * codigosMunicipios)
     *
     * @author david.cifuentes
     *
     */
    public List<MunicipioCierreVigencia> consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
        DecretoAvaluoAnual decretoAvaluoAnual,
        List<String> codigosMunicipios) throws ExcepcionSNC {
        LOGGER.debug(
            "ActualizacionBean#consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios");

        List<MunicipioCierreVigencia> answer = null;
        try {
            answer = this.municipioCierreVigenciaDao
                .consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
                    decretoAvaluoAnual, codigosMunicipios);
        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "ERROR: ActualizacionBean#consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios");
            throw (e);
        }
        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @see IActualizacion#validarExistenciaMunicipiosPorDecreto (DecretoAvaluoAnual
     * decretoAvaluoAnual, UsuarioDTO usuario)
     *
     * @author david.cifuentes
     *
     */
    @Override
    public void validarExistenciaMunicipiosPorDecreto(
        DecretoAvaluoAnual decretoAvaluoAnual, UsuarioDTO usuario) throws ExcepcionSNC {
        try {

            this.transaccionalService
                .validarExistenciaMunicipiosPorDecretoNuevaTransaccion(decretoAvaluoAnual, usuario);

        } catch (ExcepcionSNC e) {
            LOGGER.error("ERROR: ActualizacionBean#validarExistenciaMunicipiosPorDecreto");
            throw (e);
        }
    }

    // ------------------------------------------------ //
    /**
     * @see IActualizacion#co.gov.igac.snc.fachadas.ActualizacionBean.
     * guardarListaMunicipioCierreVigencias(List<MunicipioCierreVigencia>)
     *
     * @author david.cifuentes
     *
     */
    @Override
    public boolean guardarListaMunicipioCierreVigencias(
        List<MunicipioCierreVigencia> listaMunicipioCierreVigencia) {
        return this.municipioCierreVigenciaDao
            .guardarListaMunicipioCierreVigencias(listaMunicipioCierreVigencia);
    }

    // ------------------------------------------------ //
    /**
     * @see IActualizacion#co.gov.igac.snc.fachadas.ActualizacionBean.
     * eliminarZonasDelMunicipioCierreVigencia(List<ZonaCierreVigencia>)
     *
     * @author david.cifuentes
     *
     */
    @Override
    public boolean eliminarZonasDelMunicipioCierreVigencia(
        List<ZonaCierreVigencia> listaZonas) {
        return this.zonaCierreVigenciaDao
            .eliminarZonasDelMunicipioCierreVigencia(listaZonas);
    }

    // ------------------------------------------------ //
    /**
     * @see IActualizacion#co.gov.igac.snc.fachadas.ActualizacionBean.
     * ejecutarProcedimientoCierreAnual(EProcedimientoAlmacenadoFuncion, String,
     * MunicipioCierreVigencia[], Long, String)
     *
     * @author david.cifuentes
     */
    @Override
    public Object[] ejecutarProcedimientoCierreAnual(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        String codDepartamento, MunicipioCierreVigencia[] municipios,
        Long vigencia, String usuario) {
        Object[] respuestaObject = null;
        try {
            /*
             * CONSIDERACIONES:
             *
             * - Sí sólo hay un municipio como parámetro, la ejecución del procedimiento se
             * realizará sólo para dicho municipio.
             *
             * - Sí se envia el departamento seleccionado y no se envian municipios, la ejecución
             * del procedimiento se realiza para la totalidad del Departamento.
             *
             * - Sí se reciben más de un municipio, la ejecución del procedimiento se realiza
             * municipio a municipio, de manera que se realizan n llamados al procedimiento.
             */
            if (municipios != null && (municipios.length > 0)) {
                for (MunicipioCierreVigencia mcv : municipios) {
                    respuestaObject = sncProcedimientoDao
                        .ejecutarProcedimientoCierreAnual(
                            nombreProcedimiento, codDepartamento, mcv, vigencia, usuario);
                }
            } else {
                respuestaObject = sncProcedimientoDao
                    .ejecutarProcedimientoCierreAnual(nombreProcedimiento,
                        codDepartamento, null, vigencia, usuario);
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al efectuar la ejecución del procedimiento " +
                nombreProcedimiento +
                "ActualizacionBean#ejecutarProcedimientoCierreAnual.");
        }
        return respuestaObject;
    }

    /**
     * @see IActualizacion#obtenerMunicipioActualizacionPorMunicipioYEstado(String, String, Boolean, Integer)
     * @author felipe.cadena
     */
    @Override
    public List<MunicipioActualizacion> obtenerMunicipioActualizacionPorMunicipioYEstado(
        String municpioCodigo, String estado, Boolean activo, Integer vigencia) {

        List<MunicipioActualizacion> result = null;
        try {

            result = this.municipioActualizacionDao.obtenerPorMunicipioYEstado(municpioCodigo,
                estado, activo, vigencia);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerMunicipioActualizacionPorMunicipioYEstado.", e);
        }
        return result;
    }
    
    /**
     * @see IActualizacion#obtenerPorMunicipioYEstadoGeografico(String, String, Boolean, Integer)
     * @author hector.arias
     */
    @Override
    public List <MunicipioActualizacion> obtenerPorMunicipioYEstadoGeografico(String municpioCodigo,
        String estado, Boolean activo, Integer vigencia) {

        List<MunicipioActualizacion> result = null;
        try {

            result = this.municipioActualizacionDao.obtenerPorMunicipioYEstadoGeografico(municpioCodigo,
                estado, activo, vigencia);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerPorMunicipioYEstadoGeografico.", e);
        }
        return result;
    }

    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IActualizacionLocal#crearJobGenerarResolucionesActualizacion
     * @author leidy.gonzalez
     */
    @Override
    public ProductoCatastralJob crearJobGenerarResolucionesActualizacion(Tramite tramite,
        Map<String, String> parametros, Actividad actividad, UsuarioDTO usuarioLogeado) {
        LOGGER.debug("crearJobGenerarResolucionesActualizacion");

        ProductoCatastralJob pcjTemp = new ProductoCatastralJob();

        try {

        pcjTemp.setCodigo(String.valueOf(actividad) +
            Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO + String.valueOf(tramite.getId()));
        pcjTemp.setEstado(ProductoCatastralJob.JPS_ESPERANDO);
        pcjTemp.setFechaLog(new Date());
        pcjTemp.setTipoProducto(ProductoCatastralJob.TIPO_RESOLUCIONES_ACTUALIZACION);
        pcjTemp.setUsuarioLog(usuarioLogeado.getLogin());
        pcjTemp.setTramiteId(tramite.getId());

        StringBuilder resultado = new StringBuilder();
        resultado.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        resultado.append("<resultado>");
        resultado.append("<parametro ");
        resultado.append("tramiteId=\"" + parametros.get("TRAMITE_ID") + "\" ");
        resultado.append(
            "nombreResponsable=\"" + parametros.get("NOMBRE_RESPONSABLE_ACTUALIZACION") + "\" ");

        if (parametros.get("FUNCIONARIO_ACTUALIZACION") != null) {
            resultado.append("funcionarioActualizacion=\"" + parametros.get(
                "FUNCIONARIO_ACTUALIZACION") + "\" ");
        }

        resultado.append("numeroResolucion=\"" + parametros.get("NUMERO_RESOLUCION") + "\" ");
        resultado.append("fechaResolucion=\"" + parametros.get("FECHA_RESOLUCION") + "\" ");
        resultado.append("fechaResolucionConFormato=\"" + parametros.get(
            "FECHA_RESOLUCION_CON_FORMATO") + "\" ");
        resultado.append("copiaUsoRestringido=\"" + parametros.get("COPIA_USO_RESTRINGIDO") + "\">");
        resultado.append("</parametro>");
        resultado.append("</resultado>");

        pcjTemp.setResultado(resultado.toString());

        pcjTemp = this.productoCatastralJobDAO.update(pcjTemp);

        }catch (ExcepcionSNC e) {
            LOGGER.error("Error al crear job de resoluciones de actualizacion" +
                    "ActualizacionBean#crearJobGenerarResolucionesActualizacion.", e);
        }
        return pcjTemp;
    }

    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IActualizacionLocal#procesarResolucionesActualizacionEnJasperServer
     * @author leidy.gonzalez
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarResolucionesActualizacionEnJasperServer(ProductoCatastralJob job,
        int contador) {

        LOGGER.debug("procesarResolucionesConservacionEnJasperServer");
        boolean resultadoOk = true;
        Tramite tramite = new Tramite();
        EReporteServiceSNC enumeracionReporte = null;
        Parametro maxEnvioJob = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());
        String nombreResponsableActualizacion;

        try {
            contador++;
            UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService
                .buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            Long tramiteId = job.getTramiteId();

            tramite = tramiteService.buscarTramitePorId(tramiteId);

            if (tramite != null && tramite.getTipoTramite() != null) {
                if (tramite.getSubtipo() == null) {
                    tramite.setSubtipo("");
                }
                if (tramite.getRadicacionEspecial() == null) {
                    tramite.setRadicacionEspecial("");
                }
                enumeracionReporte = tramiteService.
                    obtenerUrlReporteResolucionesActualizacion(tramite.getTipoTramite(),
                        tramite.getClaseMutacion(),
                        tramite.getSubtipo(), tramite.getRadicacionEspecial(),
                        tramite.isRectificacionCancelacionDobleInscripcion());

                String urlReporte = enumeracionReporte.getUrlReporte();

                Reporte reporte = new Reporte(usuario, estructuraOrganizacional);

                reporte.setEstructuraOrganizacional(estructuraOrganizacional);

                reporte.setUrl(urlReporte);

                int indiceSeparador = job.getCodigo().indexOf(
                    Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO);
                Actividad actividad = new Actividad();
                String actividadId = null;
                if (indiceSeparador > 0) {

                    actividadId = job.getCodigo().substring(0, indiceSeparador);
                    actividad.setId(actividadId);
                }

                List<String> resultados = this.
                    obtenerParametrosDeProductoCatastralJobResolucionesActualizacion(job);

                String numeroDocumento = resultados.get(3);

                reporte.addParameter("TRAMITE_ID", (resultados.get(0) != null) ? resultados.get(0) :
                    "");
                nombreResponsableActualizacion = (resultados.get(1) != null) ? resultados.get(1) :
                    "";
                reporte.addParameter("NOMBRE_RESPONSABLE_CONSERVACION",
                    nombreResponsableActualizacion);
                reporte.addParameter("FUNCIONARIO_ACTUALIZACION", (resultados.get(2) != null) ?
                    resultados.get(2) : "");
                reporte.addParameter("NUMERO_RESOLUCION", (resultados.get(3) != null) ? resultados.
                    get(3) : "");
                reporte.addParameter("FECHA_RESOLUCION", (resultados.get(4) != null) ? resultados.
                    get(4) : "");
                reporte.addParameter("FECHA_RESOLUCION_CON_FORMATO", (resultados.get(5) != null) ?
                    resultados.get(5) : "");

                if (resultados.get(6) != null && !resultados.get(6).isEmpty()) {
                    reporte.addParameter("FIRMA_USUARIO", resultados.get(6));
                }

                /*
                 * En este punto se realiza la generación de la firma escaneada @modified by
                 * javier.aponte 12/09/2016 #19708
                 */
                if (nombreResponsableActualizacion != null &&
                    !nombreResponsableActualizacion.trim().isEmpty()) {

                    FirmaUsuario firma;
                    String documentoFirma;

                    firma = this.generalesService.buscarDocumentoFirmaPorUsuario(
                        nombreResponsableActualizacion);

                    if (firma != null && nombreResponsableActualizacion.equals(firma.
                        getNombreFuncionarioFirma())) {

                        documentoFirma = this.generalesService.
                            descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                                getIdRepositorioDocumentos());

                        reporte.addParameter("FIRMA_USUARIO", documentoFirma);
                    }
                }

                reporte.addParameter("COPIA_USO_RESTRINGIDO", (resultados.get(7) != null) ?
                    resultados.get(7) : "");

                tramite = this.generarArchivoResolucionesActualizacion(usuario, reporte,
                    numeroDocumento, tramite, job, contador);

                if (actividad != null && actividad.getId() != null &&
                    job != null && job.getEstado().equals(ProductoCatastralJob.JPS_TERMINADO)) {

                    this.avanzarTramiteDeGenerarResolucionProcess(tramite, usuario, actividad);
                    resultadoOk = true;

                } else {

                    LOGGER.error("No se encontro actividad para el tramite_id del job:" + job);
                    String message =
                        "Ocurrió un error durante la generación del producto catastral job para resoluciones de actualizacion";
                    throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                        message);
                }

            } else {
                LOGGER.error("No se encontro tramite para el job:" + job);
                String message =
                    "Ocurrió un error durante la generación del producto catastral job para resoluciones de actualizacion";
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    message);
            }

        } catch (ExcepcionSNC e) {
            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + e.getMensaje(), e);
            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                this.procesarResolucionesActualizacionEnJasperServer(job, contador);
            } else {
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                resultadoOk = false;
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
            }
        } catch (Exception ex) {
            LOGGER.debug("procesarResolucionesConservacionEnJasperServer:" + ex.getMessage(), ex);
            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                this.procesarResolucionesActualizacionEnJasperServer(job, contador);
            } else {
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                resultadoOk = false;
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
            }
        }
        return resultadoOk;
    }

    /**
     * Método para obtener una lista de parametros a partir de los resultados de un job para las
     * resoluciones de actualizacion
     *
     * @author leidy.gonzalez
     */
    public List<String> obtenerParametrosDeProductoCatastralJobResolucionesActualizacion(
        ProductoCatastralJob productoGenerado) {
        List<String> resultados = new ArrayList<String>();
        try {
            if (productoGenerado.getResultado().contains("<?xml")) {
                LOGGER.debug(productoGenerado.getResultado());
                org.dom4j.Document document = (org.dom4j.Document) DocumentHelper.parseText(
                    productoGenerado.getResultado());
                List parametro = ((Node) document).selectNodes("//parametro");
                for (Iterator iter = parametro.iterator(); iter.hasNext();) {
                    Element productoXml = (Element) iter.next();
                    resultados.add(productoXml.attributeValue("tramiteId"));
                    resultados.add(productoXml.attributeValue("nombreResponsable"));
                    resultados.add(productoXml.attributeValue("funcionarioActualizacion"));
                    resultados.add(productoXml.attributeValue("numeroResolucion"));
                    resultados.add(productoXml.attributeValue("fechaResolucion"));
                    resultados.add(productoXml.attributeValue("fechaResolucionConFormato"));
                    resultados.add(productoXml.attributeValue("firmaUsuario"));
                    resultados.add(productoXml.attributeValue("copiaUsoRestringido"));
                }

            } else {
                LOGGER.error(productoGenerado.getResultado());
                String message =
                    "Ocurrió un error durante la generación del producto catastral job para resoluciones de actualizacion";
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    message);
            }
        } catch (DocumentException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultados;
    }

    /**
     * Método para generar el archivo de las resolciones de actualizacion
     *
     * @author leidy.gonzalez
     */
    private Tramite generarArchivoResolucionesActualizacion(UsuarioDTO usuario, Reporte reporte,
        String numeroDocumento, Tramite tramite, ProductoCatastralJob job, int contador) {

        Documento documento = null;
        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        File archivoResolucionesConservacion = null;
        IReporteService reportesService = ReporteServiceFactory.getService();

        List<Documento> documentos = this.documentoDao.
            buscarPorNumeroDocumentoYTipo(numeroDocumento,
                ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                    getId());
        if (documentos != null && !documentos.isEmpty()) {
            documento = documentos.get(0);
        }

        if (documento == null) {
            LOGGER.error(
                "generarArchivoResolucionesActualizacion#Error al consultar la resolución: " +
                numeroDocumento);
            return null;
        }

        tramiteDocumento.setTramite(tramite);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(usuario.getLogin());

        archivoResolucionesConservacion = reportesService.getReportAsFile(
            reporte, new LogMensajesReportesUtil(this.generalesService,
                usuario));

        if (archivoResolucionesConservacion != null && documento != null) {
            LOGGER.debug(
                "TareaGenerarArchivoResolucionesConservacion: Reporte Resoluciones Actualizacion generado exitosamente" +
                archivoResolucionesConservacion.getAbsolutePath());

            tramite = this.aplicarCambios.guardaDocumentoTramiteResoluciones(
                tramite, tramiteDocumento, documento, usuario,
                archivoResolucionesConservacion, job, contador);

        } else {
            LOGGER.error("No se pudo generar el archivo de resoluciones para el tramite_id:" +
                tramite.getId());
            String message =
                "Ocurrió un error durante la generación del producto catastral job para resoluciones de actualizacion";
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(
                LOGGER, null, message);
        }

        return tramite;
    }

    /**
     * @see IActualizacion#obtenerEstadoTramitePorDepartamentoMunicipio(java.lang.String,
     * java.lang.String)
     *
     * @author leidy.gonzalez
     */
    @Override
    public List<MunicipioActualizacion> obtenerEstadoTramitePorDepartamentoMunicipio(
        String municpioCodigo, String departamentoCodigo) {

        List<MunicipioActualizacion> result = null;
        try {

            result = this.municipioActualizacionDao.obtenerEstadoTramitePorDepartamentoMunicipio(
                municpioCodigo, departamentoCodigo);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerMunicipioActualizacionPorMunicipioYEstado.");
        }
        return result;
    }

    /**
     * Método para avanzar en process el tramite A Comunicar Notificacion o Aplicar Cambios
     *
     * @author leidy.gonzalez
     */
    @Override
    public void avanzarTramiteDeGenerarResolucionProcess(Tramite tramite, UsuarioDTO usuario,
        Actividad actividad) {

        // Parametro Tramites Actualizacion basado en las siguientes
        // condiciones: Tipo Tramite, Clase Mutacion, Subtipo, Tipo Radicacion
        // Especial y Tramite de Actualizacion
        //Se actualiza tramite de generacion de la resolucion.
        Parametro notificaTramitesActualizacionParam = this.generalesService.
            getCacheParametroPorNombre(
                EParametro.TRAMITES_ACTUALIZACION_NOTIFICAN.toString());
        String[] listaParametrosActualizacionNotifican = null;
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        String notificaActualizacion;

        if (notificaTramitesActualizacionParam.getValorCaracter() != null) {
            listaParametrosActualizacionNotifican = notificaTramitesActualizacionParam
                .getValorCaracter().split(",");
        }

        String tramitesComunicanNotificacionActualizacion = "";

        if (tramite.getTipoTramite() != null) {

            int contadorTramiteNotificanRectificacion = 0;

            tramitesComunicanNotificacionActualizacion = tramite.getTipoTramite();

            if (tramite.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()) &&
                tramite.getTramiteRectificacions() != null) {

                for (TramiteRectificacion tr : tramite.getTramiteRectificacions()) {

                    if (tr.getDatoRectificar() != null && EDatoRectificarTipo.PREDIO.toString().
                        equalsIgnoreCase(tr.getDatoRectificar().getTipo())) {

                        if (tramite.isRectificacionMatriz() &&
                            EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().
                                equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

                            tramitesComunicanNotificacionActualizacion =
                                tramitesComunicanNotificacionActualizacion + "-" + tr.
                                    getDatoRectificar().getNombre() + "-" +
                                tramite.getRadicacionEspecial();
                        }

                        if (EDatoRectificarNombre.AREA_TERRENO.getCodigo().equalsIgnoreCase(tr.
                            getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.AREA_CONSTRUIDA.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.COEFICIENTE.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.DIMENSION.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.ZONA_FISICA.getCodigo().equalsIgnoreCase(tr.
                                getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.ZONA_GEOECONOMICA.getCodigo().equalsIgnoreCase(
                                tr.getDatoRectificar().getNombre()) ||
                            EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().
                                equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

                            //@modified by leidy.gonzalez:: #16317::08/03/2016 Modifica tramitesComunicanNotificacion para cuando son rectificacion,
                            // debido a que puede existir mas de un tipo de rectificacion a notificar
                            if (contadorTramiteNotificanRectificacion >= 1) {
                                tramitesComunicanNotificacionActualizacion =
                                    tramitesComunicanNotificacionActualizacion + "," + tramite.
                                        getTipoTramite() + "-" + tr.getDatoRectificar().getNombre();

                            } else {

                                tramitesComunicanNotificacionActualizacion =
                                    tramitesComunicanNotificacionActualizacion + "-" + tr.
                                        getDatoRectificar().getNombre();
                                contadorTramiteNotificanRectificacion++;
                            }

                        }

                    }
                    continue;
                }

            }

            if (tramite.getClaseMutacion() != null) {

                tramitesComunicanNotificacionActualizacion =
                    tramitesComunicanNotificacionActualizacion +
                    "-" + tramite.getClaseMutacion();

                if ((tramite.getSubtipo() != null && !tramite.getSubtipo().equals("")) &&
                    (tramite.getRadicacionEspecial() != null && !tramite.getRadicacionEspecial().
                    equals(""))) {

                    tramitesComunicanNotificacionActualizacion =
                        tramitesComunicanNotificacionActualizacion + "-" +
                        tramite.getSubtipo() + "-" +
                        tramite.getRadicacionEspecial();

                }

            }

            //@modified by leidy.gonzalez:: #16317::08/03/2016
            //Tramites de Rectificacion van a Comunicar Notificacion
            if (tramite.getTipoTramite().equals("2")) {
                String[] rectificacionSplit = tramitesComunicanNotificacionActualizacion.split(",");
                String rectificanSeparado;

                for (int i = 0; i < listaParametrosActualizacionNotifican.length; i++) {
                    notificaActualizacion = listaParametrosActualizacionNotifican[i];

                    for (int j = 0; j < rectificacionSplit.length; j++) {
                        rectificanSeparado = rectificacionSplit[j];

                        if (rectificanSeparado.equals(notificaActualizacion)) {

                            // Avance a comunicar Notificacion Process
                            solicitudCatastral.setUsuarios(this.tramiteService
                                .buscarFuncionariosPorRolYTerritorial(
                                    usuario.getDescripcionEstructuraOrganizacional(),
                                    ERol.SECRETARIA_CONSERVACION));
                            solicitudCatastral
                                .setTransicion(
                                    ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION);

                            procesosService.avanzarActividad(usuario, actividad.getId(),
                                solicitudCatastral);

                            break;

                        }
                    }
                }

            } else {

                for (int i = 0; i < listaParametrosActualizacionNotifican.length; i++) {
                    notificaActualizacion = listaParametrosActualizacionNotifican[i];

                    if (tramitesComunicanNotificacionActualizacion.equals(notificaActualizacion)) {

                        // Avance a comunicar Notificacion Process
                        solicitudCatastral.setUsuarios(this.tramiteService
                            .buscarFuncionariosPorRolYTerritorial(
                                usuario.getDescripcionEstructuraOrganizacional(),
                                ERol.SECRETARIA_CONSERVACION));
                        solicitudCatastral
                            .setTransicion(
                                ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION);

                        procesosService.avanzarActividad(usuario, actividad.getId(),
                            solicitudCatastral);

                        break;

                    }
                }

            }

            if (solicitudCatastral.getTransicion() == null) {

                // Si no se encuentran dentro de las
                // validaciones anteriores los tramites pasan a
                // Avanzar e aplicar Cambios
                // Si no se encuentran dentro de las
                // validaciones anteriores los tramites pasan a
                // Aplicar Cambios
                //inicia la aplicacion de cambios
                tramite.setActividadActualTramite(actividad);
                LOGGER.debug("on AplicacionCambiosMB#aplicarCambiosInicio ya tiene Actividad");

                //se marca el tramite como en aplicar cambios
                tramite.setEstadoGdb(Constantes.ACT_APLICAR_CAMBIOS);

                if (!tramite.getEstadoGdb().equals(Constantes.ACT_APLICAR_CAMBIOS)) {
                    LOGGER.error(
                        "No se pudo Actualizar el tramite");
                } else {

                    boolean validaAplicaCambios = false;

                    validaAplicaCambios = conservacionService.aplicarCambiosActualizacion(tramite,
                        usuario, 1);

                    if (validaAplicaCambios == true) {
                        tramite.setEstado(ETramiteEstado.FINALIZADO_APROBADO.getCodigo());
                        tramite = tramiteService.guardarActualizarTramite(tramite);
                    }
                    return;
                }

            }

        }

    }

    /**
     * @see IActualizacion#obtenerPredioActualizacionPorSolicitudId(java.lang.String,
     * java.lang.String)
     *
     * @author felipe.cadena
     */
    @Override
    public List<PredioActualizacion> obtenerPredioActualizacionPorSolicitudId(Long solicitudId) {

        List<PredioActualizacion> result = null;
        try {

            result = this.predioActualizacionDao.obtenerPorSolicitudId(solicitudId);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerPredioActualizacionPorSolicitudId.");
        }
        return result;
    }

    /**
     * @see IActualizacion#guardarArchivosActualizacion
     *
     * @author felipe.cadena
     */
    @Override
    public MunicipioActualizacion guardarArchivosActualizacion(
        MunicipioActualizacion municipioActualizacion, UsuarioDTO usuario, String rutaZip) {

        String directorioBase;
        try {

            municipioActualizacion = this.municipioActualizacionDao.update(municipioActualizacion);
            directorioBase = this.documentoDao.guardarDocumentosActualizacionX(rutaZip,
                municipioActualizacion.getId().toString(), usuario, municipioActualizacion.
                getMunicipio().getCodigo());

            directorioBase = directorioBase.replace("\\", "/");
            municipioActualizacion.setArchivoAlfanumerico(directorioBase);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al guardar documentos de carga actualizacion express " +
                "ActualizacionBean#guardarArchivosActualizacion.", e);
        }
        return municipioActualizacion;
    }

    /**
     * @see IActualizacion#guardarActualizarMunicipioActualizacion(MunicipioActualizacion
     * municipioActualizacion)
     *
     * @author felipe.cadena
     */
    @Override
    public MunicipioActualizacion guardarActualizarMunicipioActualizacion(
        MunicipioActualizacion municipioActualizacion) {

        MunicipioActualizacion result = null;
        try {

            result = this.municipioActualizacionDao.update(municipioActualizacion);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al actualizar procesos de actualizacion" +
                "ActualizacionBean#guardarActualizarMunicipioActualizacion.", e);
        }
        return result;
    }

    /**
     * @see IActualizacion#obtenerMunicipioActualizacionPorIdsTramites(List<Long> isTramtites)
     *
     * @author felipe.cadena
     */
    @Override
    public List<MunicipioActualizacion> obtenerMunicipioActualizacionPorIdsTramites(
        List<Long> isTramtites) {

        List<MunicipioActualizacion> result = null;
        try {

            result = this.municipioActualizacionDao.obtenerPorIdsTramites(isTramtites);

            for (MunicipioActualizacion municipioActualizacion : result) {
                List<Tramite> tramitesAsociados = this.tramiteService.
                    consultarTramitesSolicitud(municipioActualizacion.getSolicitud().getId());
                municipioActualizacion.setTramitesAsociados(tramitesAsociados);
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerMunicipioActualizacionPorIdsTramites.", e);
        }
        return result;
    }

    /**
     * @see IActualizacion#obtenerMPredioPorIdTramite(Long idTramite)
     *
     * @author leidy.gonzalez
     */
    @Override
    public MPredio obtenerMPredioPorIdTramite(Long idTramite) {

        MPredio result = null;

        try {
            result = this.mPredioDao.findPPredioCompletoByIdTramite(idTramite);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerMunicipioActualizacionPorIdsTramites.");
        } catch (Exception e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                "ActualizacionBean#obtenerMunicipioActualizacionPorIdsTramites.");
            e.printStackTrace();
        }
        return result;

    }

    /**
     *
     * @author felipe.cadena
     *
     * @see IActualizacion#obtenerValoresActualizacionPorMunicipioDepartamento
     */
    @Override
    public List<ValoresActualizacion> obtenerValoresActualizacionPorMunicipioDepartamento(
        String municipioCodigo, String deptoCodigo, String vigencia) {

        List<ValoresActualizacion> resultado = null;

        try {
            resultado = this.valoresActualizacionDAO.obtenerPorMunicipioYDepartamento(
                municipioCodigo, deptoCodigo, vigencia);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar los valores actualizacion de un municipio" +
                "ActualizacionBean#obtenerValoresActualizacionPorMunicipioDepartamento.");
            e.printStackTrace();
        }
        return resultado;

    }

    /**
     *
     * @author felipe.cadena
     *
     * @see IActualizacion#obtenerMunicipiosEnActualizacionPorDepto(java.lang.String)
     *
     */
    @Override
    public List<Municipio> obtenerMunicipiosEnActualizacionPorDepto(String deptoCodigo) {

        List<Municipio> resultado = null;

        try {
            resultado = this.municipioDAOService.obtenerMunicipiosActualizacionPorDepto(deptoCodigo);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar los municipios en actualizacion" +
                "ActualizacionBean#obtenerMunicicpiosEnActualizacionPorDepto.");
            e.printStackTrace();
        }
        return resultado;

    }

    /**
     * @author @author felipe.cadena
     * @see IActualizacion#eliminarValoresActualizacion
     */
    @Override
    public Object[] eliminarValoresActualizacion(Long valorActualizacionId, String municipioCodigo) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.eliminarValoresActualizacion(valorActualizacionId,
                municipioCodigo);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#eliminarValoresActualizacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @author @author felipe.cadena
     * @see IActualizacion#cargarTablasLiquidacion
     */
    @Override
    public Object[] cargarTablasLiquidacion(String municipioCodigo, Long valorActualizacionId,
        String usuario) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.cargarTablasLiquidacion(municipioCodigo,
                valorActualizacionId, usuario);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#cargarTablasLiquidacion: " +
                e.getMensaje());
        }
        return resultado;
    }
    
    /**
     * 
     * @param resultado 
     * @author hector.arias
     */
    @Override
    public void guardarRegistroArchivoInconsistenciasActualizacionGeo(List <String> resultado) {
        
        UsuarioDTO usuario = this.generalesService.getCacheUsuario(resultado.get(5));
        
        boolean validacionZonas = false;
        boolean validacionPredios = false;
        
        //resultado posición 0 validacion zonas
        //resultado posición 1 validacion predios
        //resultado posición 2 ubicacion archivo inconsistencias
        //resultado posición 3 codigo municipio
        //resultado posición 4 codigo departamento
        //resultado posición 5 usuario
        //resultado posición 6 valido zonas
        //resultado posición 7 valido predios
        
        //Se verifica si se validaron zonas y si la validación fue exitosa
        if (resultado.get(6).equals("NO")) {
            validacionZonas = true;
        } else if (resultado.get(6).equals("NO")) {
            if (resultado.get(0).equals("True")) {
                validacionZonas = true;
            } else {
                validacionZonas = false;
            }
        }
        //Se verifica si se validaron predios y si la validación fue exitosa
        if (resultado.get(7).equals("NO")) {
                validacionPredios = true;
        } else if (resultado.get(7).equals("SI")) {
            if (resultado.get(1).equals("True")) {
                validacionPredios = true;
            } else {
                validacionPredios = false;
            }
        }

        //Elimina los registros en la tabla documento almacenado anteriormente 
        //de reporte de inconsistencias
        List<Documento> docGDB = new ArrayList<Documento>(); 
        
        docGDB = this.generalesService.buscarDocumentoPorTipoActualizacion(
                                resultado.get(3),
                        ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getId());
        
        if (!docGDB.isEmpty()) {
            for (Documento documento : docGDB) {
                this.generalesService.eliminarDocumentoFirmaMecanica(documento);
            }
        }
        
        DocumentoTramiteDTO dtDto = new DocumentoTramiteDTO(
                resultado.get(2),
                ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getNombre(),
                new Date(), null, 0);

        dtDto.setNumeroPredial(resultado.get(3));
        dtDto.setNombreDepartamento(resultado.get(4));
        dtDto.setNombreMunicipio(resultado.get(3));

        Documento d = new Documento();
        TipoDocumento td = new TipoDocumento();
        td.setId(ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getId());
        d.setTipoDocumento(td);
        d.setNumeroDocumento("ACT" + "-"
                + resultado.get(3));
        d.setArchivo("resultado_validacion.log");
        d.setUsuarioCreador(resultado.get(5));
        d.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        d.setBloqueado(ESiNo.NO.getCodigo());
        d.setUsuarioLog(resultado.get(5));
        d.setIdRepositorioDocumentos(resultado.get(2));
        d.setFechaLog(new Date());
        
        if (validacionZonas && validacionPredios) {
            d.setObservaciones("VALIDACION EXITOSA");
        } else {
            d.setObservaciones("VALIDACION FALLIDA");
        }
        
        d = this.generalesService.guardarDocumento(d);
        
        LOGGER.info("d");
        
    }

    /**
     * @author @author felipe.cadena
     * @see IActualizacion#calcularAvaluosTablas
     */
    @Override
    public Object[] calcularAvaluosTablas(String municipioCodigo) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.calcularAvaluosTablas(municipioCodigo);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#calcularAvaluosTablas: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @author @author felipe.cadena
     * @see IActualizacion#aplicarTablasLiquidacion
     */
    @Override
    public Object[] aplicarTablasLiquidacion(String municipioCodigo) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.aplicarTablasLiquidacion(municipioCodigo);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#aplicarTablasLiquidacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     *
     * @author felipe.cadena
     *
     * @see
     * IActualizacion#guardarActualizarValoresActualizacion(guardarActualizarValoresActualizacion)
     *
     */
    @Override
    public ValoresActualizacion guardarActualizarValoresActualizacion(ValoresActualizacion va) {

        ValoresActualizacion resultado = null;

        try {
            resultado = this.valoresActualizacionDAO.update(va);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al guardar los valores actualizacion" +
                "ActualizacionBean#guardarActualizarValoresActualizacion.");
            e.printStackTrace();
        }
        return resultado;

    }

    /**
     *
     * @author javier.aponte
     *
     * @see IActualizacion#buscarReportesActualizacionPorCodigoMunicipio(String)
     *
     */
    @Override
    public List<Documento> buscarReportesActualizacionPorCodigoMunicipio(
        String codigoMunicipio, String vigencia) {

        List<Documento> respuesta = null;

        try {

            respuesta = this.reporteActualizacionDAO.buscarReportesActualizacionPorCodigoMunicipio(
                codigoMunicipio, vigencia);

        } catch (Exception ex) {
            LOGGER.error("Error al buscar reportes actualizacion por municipio actualizacion id" +
                "ActualizacionBean#buscarReportesActualizacionPorCodigoMunicipio.");
            ex.printStackTrace();
        }

        return respuesta;

    }

    /**
     *
     * @author javier.aponte
     *
     * @see IActualizacion#guardarYActualizarReportesActualizacion(List)
     *
     */
    @Override
    public List<ReporteActualizacion> guardarYActualizarReportesActualizacion(
        List<ReporteActualizacion> reportesActualizacion) {

        List<ReporteActualizacion> respuesta = new ArrayList<ReporteActualizacion>();

        try {
            for (ReporteActualizacion raTemp : reportesActualizacion) {
                raTemp = this.reporteActualizacionDAO.update(raTemp);
                respuesta.add(raTemp);
            }

        } catch (Exception ex) {
            LOGGER.error(
                "Error al guardar y actualizar reportes de actualizacion por municipio actualizacion id" +
                "ActualizacionBean#guardarYActualizarReportesActualizacion.");
            ex.printStackTrace();
        }

        return respuesta;

    }

    /**
     *
     * @author javier.aponte
     *
     * @see IActualizacion#generarYActualizarReportesActualizacion(String)
     *
     */
    @Override
    public void borrarYActualizarReportesActualizacion(String codigoMunicipio, String usuarioLogin,
        boolean generarReportes) {

        List<Documento> reportesActualizacion;

        ProductoCatastralJob productoCatastralJob;

        UsuarioDTO usuario = this.generalesService.getCacheUsuario(usuarioLogin);

        try {

            reportesActualizacion = this.buscarReportesActualizacionPorCodigoMunicipio(
                codigoMunicipio, "");

            if (generarReportes &&
                (reportesActualizacion == null || reportesActualizacion.isEmpty())) {
                this.crearRegistrosReportesActualizacion(codigoMunicipio, usuario);
            } else {

                IDocumentosService documentosService = DocumentalServiceFactory.getService();

                for (Documento raTemp : reportesActualizacion) {

                    //productoCatastralJob = raTemp.getProductoCatastralJob();

                    //1. Eliminar el archivo que se había generado del ftp 
                    if (raTemp.getIdRepositorioDocumentos() != null) {
                        documentosService.borrarDocumento(raTemp.getIdRepositorioDocumentos());
                    }

                    raTemp.setIdRepositorioDocumentos(null);
                   // raTemp.setFechaGeneracion(null);

                    //2. Actualizar tabla Reporte Actualización
                    //this.reporteActualizacionDAO.update(raTemp);

                    //3. Actualizar el estado del job a JPS_ESPERANDO para que se vuelva a generar el reporte;
                    //sólo cuando el parámetro de generarReportes esté en true
                    /*if (generarReportes) {
                        productoCatastralJob.setEstado(ProductoCatastralJob.JPS_ESPERANDO);
                        this.productoCatastralJobDAO.update(productoCatastralJob);
                    }*/

                }
            }

        } catch (Exception ex) {
            LOGGER.error("Error al buscar reportes actualizacion por código de municipio " +
                "ActualizacionBean#generarYActualizarReportesActualizacion.");
            ex.printStackTrace();
        }

    }

    /**
     * Método encargado de crear los registros en la tabla reporte actualización para el municipio
     * asociado.
     *
     * @author javier.aponte
     */
    private void crearRegistrosReportesActualizacion(String codigoMunicipio, UsuarioDTO usuario) {

        ReporteActualizacion reporteActualizacion;
        Municipio municipio = municipioDAOService.getMunicipioByCodigo(codigoMunicipio);
        List<ReporteActualizacion> reportesActualizacion = new ArrayList<ReporteActualizacion>();

        ProductoCatastralJob productoCatastralJob;

        StringBuilder codigo;

        for (EReporteActualizacion eraTemp : EReporteActualizacion.values()) {

            reporteActualizacion = new ReporteActualizacion();
            reporteActualizacion.setCodigoReporte(eraTemp.getCodigo());
            reporteActualizacion.setNombreReporte(eraTemp.getNombre());
            reporteActualizacion.setMunicipio(municipio);
            reporteActualizacion.setFechaLog(new Date());
            reporteActualizacion.setUsuarioLog(usuario.getLogin());

            reporteActualizacion = this.reporteActualizacionDAO.update(reporteActualizacion);

            //Crea el registro de producto catastral job
            productoCatastralJob = new ProductoCatastralJob();

            productoCatastralJob.setFechaLog(new Date());
            productoCatastralJob.setUsuarioLog(usuario.getLogin());
            productoCatastralJob.setEstado(ProductoCatastralJob.JPS_ESPERANDO);
            productoCatastralJob.setTipoProducto(ProductoCatastralJob.TIPO_REPORTES_ACTUALIZACION);

            codigo = new StringBuilder();

            codigo.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            codigo.append("<codigo>");
            codigo.append("<parametro ");
            codigo.append("codigoMunicipio=\"" + codigoMunicipio + "\" ");
            codigo.append("tipoReporte=\"" + eraTemp.getTipoReporte() + "\" ");
            codigo.append("idReporteActualizacion=\"" + reporteActualizacion.getId() + "\">");
            codigo.append("</parametro>");
            codigo.append("</codigo>");

            productoCatastralJob.setCodigo(codigo.toString());

            productoCatastralJob = this.productoCatastralJobDAO.update(productoCatastralJob);

            reporteActualizacion.setProductoCatastralJob(productoCatastralJob);

            reportesActualizacion.add(reporteActualizacion);

        }

        reportesActualizacion = this.guardarYActualizarReportesActualizacion(reportesActualizacion);

    }

    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IActualizacionLocal#procesarReportesActualizacionEnJasperServer(ProductoCatastralJob,
     * int)
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarReportesActualizacionEnJasperServer(ProductoCatastralJob job,
        int contador) {

        LOGGER.debug("procesarReportesActualizacionEnJasperServer");
        boolean resultadoOk = true;
        EReporteServiceSNC enumeracionReporte = null;

        //Se deja la misma cantidad máxima de envío de job de resoluciones
        Parametro maxEnvioJob = this.generalesService.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());

        try {
            contador++;
            UsuarioDTO usuario = this.generalesService.getCacheUsuario(job.getUsuarioLog());

            EstructuraOrganizacional estructuraOrganizacional = this.generalesService.
                buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            List<String> resultados = this.
                obtenerParametrosDeProductoCatastralJobReportesActualizacion(job);

            enumeracionReporte = this.obtenerUrlListadoReportesActualizacion(resultados.get(1));

            String urlReporte = enumeracionReporte.getUrlReporte();

            Reporte reporte = new Reporte(usuario, estructuraOrganizacional);

            reporte.setEstructuraOrganizacional(estructuraOrganizacional);

            reporte.setFormato(Reporte.FORMAT_EXCEL);

            reporte.setUrl(urlReporte);

            reporte.addParameter("CODIGO_MUNICIPIO",
                (resultados.get(0) != null) ? resultados.get(0) : "");

            this.generarReporteActualizacion(usuario, reporte, job, resultados.get(2));

        } catch (ExcepcionSNC e) {
            LOGGER.debug("procesarReportesActualizacionEnJasperServer:" + e.getMensaje(), e);
            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                this.procesarReportesActualizacionEnJasperServer(job, contador);
            } else {
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                resultadoOk = false;
            }
        } catch (Exception ex) {
            LOGGER.debug("procesarReportesActualizacionEnJasperServer:" + ex.getMessage(), ex);
            //reenvia job
            if (contador <= maxEnvioJob.getValorNumero()) {
                this.procesarReportesActualizacionEnJasperServer(job, contador);
            } else {
                LOGGER.error("revisar el job:" + job + "debido a que no se pudo reenviar");
                resultadoOk = false;
            }
        }
        return resultadoOk;
    }

    /**
     * Método para generar el reporte de actualización
     *
     * @param usuario
     * @param reporte
     * @param job
     * @author javier.aponte
     */
    private void generarReporteActualizacion(UsuarioDTO usuario,
        Reporte reporte, ProductoCatastralJob job, String idReporteActualizacion) {

        File archivoReporteActualizacion = null;
        IReporteService reportesService = ReporteServiceFactory.getService();
        String rutaFtp;

        archivoReporteActualizacion = reportesService.getReportAsFile(reporte,
            new LogMensajesReportesUtil(this.generalesService, usuario));

        if (archivoReporteActualizacion != null) {
            LOGGER.debug(
                "generarReporteActualizacion: Reporte Actualizacion generado exitosamente" +
                archivoReporteActualizacion.getAbsolutePath());

            try {
                IDocumentosService service = DocumentalServiceFactory.getService();
                rutaFtp = service.cargarDocumentoWorkspaceProducto(archivoReporteActualizacion.
                    getPath(),
                    usuario.getCodigoEstructuraOrganizacional());

                //El archivo se cargó con éxito al ftp
                if (rutaFtp != null) {

                    ReporteActualizacion reporteActualizacion = this.reporteActualizacionDAO.
                        findById(Long.valueOf(idReporteActualizacion));
                    if (reporteActualizacion != null) {
                        reporteActualizacion.setIdRepositorioDocumentos(rutaFtp);
                        reporteActualizacion.setFechaGeneracion(new Date());
                        this.reporteActualizacionDAO.update(reporteActualizacion);
                    } else {
                        LOGGER.error(
                            "error en ActualizacionBean#generarReporteActualizacion: al buscar reporte " +
                            "actualización por id: " + idReporteActualizacion);
                    }
                }

            } catch (ExcepcionSNC ex) {
                LOGGER.error("error en ActualizacionBean#generarReporteActualizacion: " + ex.
                    getMensaje());
            }

        } else {
            LOGGER.error("No se pudo generar el archivo de reporte de resoluciones:");
            String message =
                "Ocurrió un error durante la generación del reporte para el producto catastral job " +
                "para reportes de actualizacion";
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null, message);
        }

    }

    /**
     * Método para obtener una lista de parametros a partir de los resultados de un job para las
     * resoluciones de actualizacion
     *
     * @author javier.aponte
     */
    public List<String> obtenerParametrosDeProductoCatastralJobReportesActualizacion(
        ProductoCatastralJob productoGenerado) {
        List<String> resultados = new ArrayList<String>();
        try {
            if (productoGenerado.getCodigo().contains("<?xml")) {
                LOGGER.debug(productoGenerado.getCodigo());
                org.dom4j.Document document = (org.dom4j.Document) DocumentHelper.parseText(
                    productoGenerado.getCodigo());
                List parametro = ((Node) document).selectNodes("//parametro");
                for (Iterator iter = parametro.iterator(); iter.hasNext();) {
                    Element productoXml = (Element) iter.next();
                    resultados.add(productoXml.attributeValue("codigoMunicipio"));
                    resultados.add(productoXml.attributeValue("tipoReporte"));
                    resultados.add(productoXml.attributeValue("idReporteActualizacion"));
                }

            } else {
                LOGGER.error(productoGenerado.getResultado());
                String message =
                    "Ocurrió un error durante la generación del producto catastral job para reportes de actualizacion";
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    message);
            }
        } catch (DocumentException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultados;
    }

    /**
     * Método para obtener el nombre de la enumeración del reporte a generar a partir del tipo de
     * reporte
     *
     * @author javier.aponte
     */
    public EReporteServiceSNC obtenerUrlListadoReportesActualizacion(String tipoReporte) {

        EReporteServiceSNC answer = null;

        if (EReporteActualizacion.LISTADO_DIFERENCIA_DE_AREA_PREDIOS.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_PREDIO_DIFERENCIA_AREA;
        } else if (EReporteActualizacion.LISTADO_PREDIOS_NO_LIQUIDADOS.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_PREDIO_NO_LIQUIDADO;
        } else if (EReporteActualizacion.LISTADO_PREDIOS_POR_USO_CONSTRUCCION.getTipoReporte().
            equals(tipoReporte)) {
            answer = EReporteServiceSNC.AX_PREDIO_POR_USO_CONSTRUCCION;
        } else if (EReporteActualizacion.INCREMENTO_DE_AVALUO_POR_SECTOR.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_INCREMENTO_AVALUO_POR_SECTOR;
        } else if (EReporteActualizacion.INCREMENTO_DE_AREA_CONSTRUIDA.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_INCREMENTO_AREA_CONSTRUIDA;
        } else if (EReporteActualizacion.LISTADO_AVALUO_POR_PREDIO.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_AVALUO_POR_PREDIO;
        } else if (EReporteActualizacion.ESTADISTICA_POR_DESTINO.getTipoReporte().
            equals(tipoReporte)) {
            answer = EReporteServiceSNC.AX_ESTADISTICA_POR_DESTINO;
        } else if (EReporteActualizacion.ESTADISTICA_POR_ZONA_FISICA.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_ESTADISTICA_POR_ZONA_FISICA;
        } else if (EReporteActualizacion.ESTADISTICA_POR_ZONA_GEOECONOMICA.getTipoReporte().equals(
            tipoReporte)) {
            answer = EReporteServiceSNC.AX_ESTADISTICA_POR_ZONA_GEOECONOMICA;
        }

        return answer;
    }

    /**
     * @see IActualizacion#buscarPPrediosCargueCica
     *
     * @author felipe.cadena
     */
    @Override
    public List<VCargueCica> buscarPPrediosCargueCica(FiltroCargueDTO filtroCargueDTO){

        List<VCargueCica> resultado = null;
        try {

            resultado = this.vCargueCicaDAO.buscarProyeccionCargueCica(filtroCargueDTO);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar predios de cargue cica " +
                    "ActualizacionBean#buscarPPrediosCargueCica.", e);
        }
        return resultado;
    }

    /**
     * @see IActualizacion#buscarPPrediosCargueCica
     *
     * @author felipe.cadena
     */
    @Override
    public int countPPrediosCargueCica(FiltroCargueDTO filtroCargueDTO){

        Integer resultado = null;
        try {

            resultado = this.vCargueCicaDAO.countProyeccionCargueCica(filtroCargueDTO);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar predios de cargue cica " +
                    "ActualizacionBean#buscarPPrediosCargueCica.", e);
        }
        return resultado;
    }

    /**
     * @see IActualizacion#eliminarMunicipioActualizacion
     * @author felipe.cadena
     */
    @Override
    public void eliminarMunicipioActualizacion(MunicipioActualizacion municipioActualizacion) {

        List<MunicipioActualizacion> result = null;
        try {

            this.municipioActualizacionDao.delete(municipioActualizacion);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar procesos de actualizacion" +
                    "ActualizacionBean#obtenerMunicipioActualizacionPorMunicipioYEstado.", e);
        }
    }

    /**
     * @see IActualizacion#iniciarCargueCica(Long, String)
     * @author felipe.cadena
     */
    @Override
    public Object[] iniciarCargueCica(Long municipioActualizacionId, String usuario) {
        try {
            return this.sncProcedimientoDao.iniciarCargueCica(municipioActualizacionId,
                    usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#eliminarProyeccionCica(List)
     * @author felipe.cadena
     */
    @Override
    public Object[] eliminarProyeccionCica(List<Long> idsPredios) {
        try {
            return this.sncProcedimientoDao.eliminarProyeccionCica(idsPredios);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#preliquidarCargueAct(String , Integer)
     * @author felipe.cadena
     */
    @Override
    public Object[] preliquidarCargueAct(String municipioCodigo, Integer vigencia) {
        try {
            return this.sncProcedimientoDao.preliquidarCargueAct(municipioCodigo, vigencia);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#aplicarCambiosAct(String , Integer)
     * @author felipe.cadena
     */
    @Override
    public Object[] aplicarCambiosAct(String municipioCodigo, Integer vigencia) {
        try {
            return this.sncProcedimientoDao.aplicarCambiosAct(municipioCodigo, vigencia);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#buscarPrediosProyectadosCica(List<String>)
     * @author felipe.cadena
     */
    @Override
    public List<VCargueCica> buscarPrediosProyectadosCica(List<String> predios) {
        try {
            return this.vCargueCicaDAO.buscarPrediosProyectados(predios);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#buscarResumenPredios(FiltroCargueDTO)
     * @author felipe.cadena
     */
    @Override
    public  List<Object[]>  buscarResumenPredios(FiltroCargueDTO filtroCargueDTO) {
        try {
            return this.vCargueCicaDAO.buscarResumenPredios(filtroCargueDTO);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#buscarRangosFiltros(FiltroCargueDTO)
     * @author felipe.cadena
     */
    @Override
    public  Object[]  buscarRangosFiltros(FiltroCargueDTO filtroCargueDTO) {
        try {
            return this.vCargueCicaDAO.buscarRangosFiltros(filtroCargueDTO);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see IActualizacion#buscarPrediosAsociadosCargue(List)
     * @author felipe.cadena
     */
    @Override
    public  List<VCargueCica>  buscarPrediosAsociadosCargue(List<VCargueCica> predios) {
        try {
            return this.vCargueCicaDAO.buscarPrediosAsociadosCargue(predios);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }


}
