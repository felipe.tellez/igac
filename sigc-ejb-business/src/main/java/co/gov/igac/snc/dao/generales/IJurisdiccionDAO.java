package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Jurisdiccion;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 * @author juan.agudelo
 */
@Local
public interface IJurisdiccionDAO extends IGenericJpaDAO<Jurisdiccion, String> {

    /**
     * Retorna los municipios por código de estructura organizacional
     *
     * @param codigoEO
     * @return
     * @author juan.agudelo
     */
    public List<Municipio> findMunicipiosbyCodigoEstructuraOrganizacional(
        String codigoEO);

    /**
     * Retorna los municipios para una territorial, incluyendo los de las UOC's, por un código de
     * estructura organizacional
     *
     * @author juan.agudelo
     * @param departamentoCodigo
     * @param codigosEstructuraOrganizacionalList
     * @return
     */
    public List<Municipio> findMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional(
        String departamentoCodigo,
        List<String> codigosEstructuraOrganizacionalList);

    /**
     * Método encargado de obtener el código de departamento a partir del código de estructura
     * organizacional del usuarioDTO
     *
     * @author juan.agudelo
     * @param codigo Código de la estructura organizacional
     * @return Departamento
     */
    public List<String> getCodigoDepartamentoByCodigoEstructuraOrganizacional(
        List<String> codigo);

    /**
     * Metodo que retorna el identificador de la estructura organizacional que tiene jurisdiccion
     * sobre el municipio
     *
     * @author franz.gamba
     * @param municipioCod
     * @return
     */
    public String getEstructuraOrganizacionalIdByMunicipioCod(String municipioCod);

    /**
     * Metodo que retorna la estructura organizacional que tiene jurisdiccion sobre el municipio
     *
     * @author christian.rodriguez
     * @param municipioCod Identificador del Municipio del cual se desea saber la territorial a la
     * que pertenece
     * @return EstructuraOrganizacional que tiene la jurisdiccion sobre el municipio
     */
    public EstructuraOrganizacional getEstructuraOrganizacionalByMunicipioCod(
        String municipioCod);

    /**
     * Metodo que retorna los codigos de los municipios bajo la jusrisdiccion de una estructura
     * organizacional
     *
     * @author franz.gamba
     * @param estructuraOrgcod
     * @return
     */
    public List<String> getMunicipioCodsByEstructuraOrganizacionalCod(String estructuraOrgcod);

    /**
     * Obtiene el código de estructura organizacional de un municipio
     *
     * @author pedro.garcia
     * @param codigoMunicipio
     * @return
     */
    public String obtenerCodigoEstructuraOrganizacionalPorMunicipio(String codigoMunicipio);

//end opf interface    
}
