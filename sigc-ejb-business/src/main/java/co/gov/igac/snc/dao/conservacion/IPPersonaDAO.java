package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;

@Local
public interface IPPersonaDAO extends IGenericJpaDAO<PPersona, Long> {

}
