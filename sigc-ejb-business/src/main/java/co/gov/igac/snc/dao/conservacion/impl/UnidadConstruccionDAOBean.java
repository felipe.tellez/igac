package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IUnidadConstruccionDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;

@Stateless
public class UnidadConstruccionDAOBean extends GenericDAOWithJPA<UnidadConstruccion, Long>
    implements IUnidadConstruccionDAO {

    private static final Logger log = LoggerFactory.getLogger(UnidadConstruccionDAOBean.class);

    public UnidadConstruccion findUnidadConstruccionByPredioANDUnidad(Predio predio, String unidad) {
        log.debug("findUnidadConstruccionByPredioANDUnidad");
        Query q = entityManager.createNamedQuery("findUnidadConstruccionByPredioANDUnidad");
        q.setParameter("predio", predio);
        q.setParameter("uni", unidad);
        return (UnidadConstruccion) q.getSingleResult();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.conservacion.IUnidadConstruccionDAO#getUnidadsConstruccionByPredioId(java.lang.Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<UnidadConstruccion> getUnidadsConstruccionByPredioId(
        Long predioId) {

        List<UnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT uc FROM UnidadConstruccion uc" +
            " LEFT JOIN FETCH uc.usoConstruccion " +
            " LEFT JOIN FETCH uc.predio p" +
            " WHERE p.id = :predioId";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("predioId", predioId);

            answer = (List<UnidadConstruccion>) q.getResultList();
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     * @see IUnidadConstruccionDAO#getUnidadsConstruccionByPredioIdNoCanceladas(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<UnidadConstruccion> getUnidadsConstruccionByPredioIdNoCanceladas(
        Long predioId) {

        List<UnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT uc FROM UnidadConstruccion uc" +
            " LEFT JOIN FETCH uc.usoConstruccion " +
            " LEFT JOIN FETCH uc.predio p" +
            " LEFT JOIN FETCH uc.unidadConstruccionComps ucc" +
            " WHERE uc.predio.id = :predioId " +
            " AND uc.anioCancelacion is null";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("predioId", predioId);

            answer = (List<UnidadConstruccion>) q.getResultList();
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @author franz.gamba
     * @see IUnidadConstruccionDAO#getUnidadsConstruccionByNumeroPredial(String)
     */
    @Override
    public List<UnidadConstruccion> getUnidadsConstruccionByNumeroPredial(
        String numPred) {

        List<UnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT uc FROM UnidadConstruccion uc" +
            " LEFT JOIN FETCH uc.usoConstruccion " +
            " LEFT JOIN FETCH uc.predio p" +
            " WHERE p.numeroPredial = :numPred";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("numPred", numPred);

            answer = (List<UnidadConstruccion>) q.getResultList();
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see
     * co.gov.igac.snc.dao.conservacion.IUnidadConstruccionDAO#obtenerUnidadesConstruccionPorListaId(java.lang.Long)
     */
    @Override
    public List<UnidadConstruccion> obtenerUnidadesConstruccionPorListaId(
        List<Long> idPUnidadMod) {

        List<UnidadConstruccion> answer = null;

        String query = "SELECT uc FROM UnidadConstruccion uc" +
            " WHERE uc.id IN (:idPUnidadMod)";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("idPUnidadMod", idPUnidadMod);

            answer = (List<UnidadConstruccion>) q.getResultList();
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        return answer;
    }

}
