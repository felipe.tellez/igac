package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import co.gov.igac.snc.util.FiltroCargueDTO;
import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioAvaluoCatastralDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.ITipificacionUnidadConstDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteTipoInscripcion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

import javax.persistence.NoResultException;

@Stateless
public class PPredioDAOBean extends GenericDAOWithJPA<PPredio, Long>
    implements IPPredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PPredioDAOBean.class);

    @EJB
    private IPPredioAvaluoCatastralDAO ppredioAvaluoCatastralDAO;

    @EJB
    private ITipificacionUnidadConstDAO tipificacionUnidadConstDAO;

    @EJB
    private IPPredioZonaDAO pPredioZonaDAO;

    @EJB
    private IDocumentoDAO documentoDAO;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDAO;

    @EJB
    private IPFichaMatrizDAO pFichaMatrizDAO;

    //-------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see IPPredioDAO#getPPredioById(Long)
     */
    public PPredio getPPredioById(Long id) {
        LOGGER.debug("getPPredioByIdPPredio");
        PPredio p = this.findById(id);
        return p;
    }

    /**
     * @author fabio.navarrete
     * @see IPPredioDAO#findPPredioByIdFetchAvaluos(Long)
     */
    @Override
    public PPredio findPPredioByIdFetchAvaluos(Long idPredio) {
        String query = "SELECT p" +
            " FROM PPredio p LEFT JOIN FETCH p.PUnidadConstruccions uc" +
            " LEFT JOIN FETCH uc.usoConstruccion" +
            " WHERE p.id = :idPredio";
        Query q = entityManager.createQuery(query);
        q.setParameter("idPredio", idPredio);
        try {
            PPredio p = (PPredio) q.getSingleResult();
            p.getPPredioZonas().size();
            p.getPPredioAvaluoCatastrals().size();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * Metodo que consulta pPredio pero lo trae con todas sus colecciones dependientes
     *
     * @throws Exception
     * @author fredy.wilches
     */
    @Override
    public PPredio findPPredioCompletoById(Long idPredio) throws Exception {
        LOGGER.info("findPPredioCompletoById ====== VALUE =================== " + idPredio);
        String query = "SELECT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.tramite t " +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " LEFT JOIN FETCH pp.PPersona p2" +
            " LEFT JOIN FETCH p2.direccionPais dp" +
            " LEFT JOIN FETCH p2.direccionDepartamento dd" +
            " LEFT JOIN FETCH p2.direccionMunicipio dm" +
            " WHERE p.id = :idPredio";
        Query q = entityManager.createQuery(query);
        q.setParameter("idPredio", idPredio);
        try {
            PPredio p = (PPredio) q.getSingleResult();
            Hibernate.initialize(p.getPFichaMatrizs());
            Hibernate.initialize(p.getPPredioZonas().size());
            Hibernate.initialize(p.getPPredioDireccions().size());
            Hibernate.initialize(p.getPFotos().size());
            Hibernate.initialize(p.getPUnidadConstruccions().size());
            Hibernate.initialize(p.getPPredioAvaluoCatastrals().size());
            Hibernate.initialize(p.getPPredioServidumbre().size());
            Hibernate.initialize(p.getPReferenciaCartograficas().size());
            Hibernate.initialize(p.getTramite());

            /* if (p.getPPersonaPredios()!=null && !p.getPPersonaPredios().isEmpty()){ for
             * (PPersonaPredio pp:p.getPPersonaPredios()){ pp.getPPersonaPredioPropiedads().size();
             * } } */
            if (p.getPUnidadConstruccions().size() > 0) {
                for (IModeloUnidadConstruccion puc : p.getPUnidadConstruccions()) {
                    if (puc.getUsoConstruccion() != null) {
                        puc.getUsoConstruccion().getNombre();
                    }

                    String tipificacion = puc.getTipificacion();
                    if (tipificacion != null && !tipificacion.equals("")) {

                        if (tipificacion.equals("NA")) {
                            puc.setTipificacionValor("NA");
                        } else {
                            String criterio[] = new String[]{"tipo", "=", tipificacion};
                            List<String[]> criterios = new ArrayList<String[]>();
                            criterios.add(criterio);

                            List<TipificacionUnidadConst> tucs = tipificacionUnidadConstDAO.
                                findByCriteria(criterios);
                            if (tucs != null && !tucs.isEmpty()) {
                                Hibernate.initialize(tucs);
                                puc.setTipificacionValor(tucs.get(0).getTipificacion());
                            }
                        }
                    }
                }
            }

            /* if (p.getPUnidadConstruccions().size() > 0) { for (PUnidadConstruccion puc :
             * p.getPUnidadConstruccions()) { if (puc.getProvienePredio() != null) {
             * puc.getProvienePredio().getNumeroPredial(); } } } */
            if (p.getPPersonaPredios() != null) {
                Hibernate.initialize(p.getPPersonaPredios());
                p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                    .getPPersonaPredios()));
            }
            /*
             * p.getPPersonaPredios().size();
             *
             * if (p.getPPersonaPredios()!=null){ for (PPersonaPredio pp:p.getPPersonaPredios()){
             * PPersona persona=pp.getPPersona(); Hibernate.initialize(persona)
             * persona.getDireccionPais(); persona.getDireccionDepartamento();
             * persona.getDireccionMunicipio(); }
             *
             * }
             */

            return p;
        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el predio o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage());
        }
        throw new Exception("No se encontró el predio");
    }

    /**
     * Metodo que consulta pPredio pero lo trae con todas sus colecciones dependientes
     *
     * @throws Exception
     * @author fredy.wilches
     */
    @Override
    public PPredio findPPredioCompletoByIdyTramite(Long idPredio, Long idTramite) throws Exception {
        String query = "SELECT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " LEFT JOIN FETCH pp.PPersona p2" +
            " LEFT JOIN FETCH p2.direccionPais dp" +
            " LEFT JOIN FETCH p2.direccionDepartamento dd" +
            " LEFT JOIN FETCH p2.direccionMunicipio dm" +
            " WHERE p.id = :idPredio and p.tramite.id = :idTramite";
        Query q = entityManager.createQuery(query);
        q.setParameter("idPredio", idPredio);
        q.setParameter("idTramite", idTramite);
        try {
            PPredio p = (PPredio) q.getSingleResult();
            p.getPFichaMatrizs().size();
            p.getPPredioZonas().size();
            p.getPPredioDireccions().size();
            p.getPFotos().size();
            p.getPUnidadConstruccions().size();
            p.getPPredioAvaluoCatastrals().size();
            p.getPPredioServidumbre().size();
            p.getPReferenciaCartograficas().size();

            if (p.getPUnidadConstruccions().size() > 0) {
                for (IModeloUnidadConstruccion puc : p.getPUnidadConstruccions()) {
                    if (puc.getUsoConstruccion() != null) {
                        puc.getUsoConstruccion().getNombre();
                    }

                    String tipificacion = puc.getTipificacion();
                    if (tipificacion != null && !tipificacion.equals("")) {

                        if (tipificacion.equals("NA")) {
                            puc.setTipificacionValor("NA");
                        } else {
                            String criterio[] = new String[]{"tipo", "=", tipificacion};
                            List<String[]> criterios = new ArrayList<String[]>();
                            criterios.add(criterio);

                            List<TipificacionUnidadConst> tucs = tipificacionUnidadConstDAO.
                                findByCriteria(criterios);
                            if (tucs != null && !tucs.isEmpty()) {
                                puc.setTipificacionValor(tucs.get(0).getTipificacion());
                            }
                        }
                    }

                }
            }

            if (p.getPUnidadConstruccions().size() > 0) {
                for (PUnidadConstruccion puc : p.getPUnidadConstruccions()) {
                    if (puc.getProvienePredio() != null) {
                        puc.getProvienePredio().getNumeroPredial();
                    }
                }
            }

            if (p.getPPersonaPredios() != null) {
                p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                    .getPPersonaPredios()));
            }
            return p;
        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el predio asociado a dicho trámite o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage());
        }
        throw new Exception("No se encontró el predio asociado a dicho trámite");
    }

    /**
     * Metodo que consulta pPredio pero lo trae con todas sus colecciones dependientes, excluye los
     * cancelados
     *
     * @throws Exception
     * @author fredy.wilches
     */
    @Override
    public PPredio findPPredioCompletoByIdTramite(Long idTramite) throws Exception {
        String query = "SELECT p" +
            " FROM PPredio p LEFT " +
            " JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " LEFT JOIN FETCH pp.PPersona p2" +
            " LEFT JOIN FETCH p2.direccionPais dp" +
            " LEFT JOIN FETCH p2.direccionDepartamento dd" +
            " LEFT JOIN FETCH p2.direccionMunicipio dm" +
            " WHERE p.tramite.id = :idTramite AND (p.cancelaInscribe IS NULL OR p.cancelaInscribe != 'C')";
        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", idTramite);
        try {
            List<PPredio> predios = q.getResultList();
            if (predios != null && predios.size() > 0) {
                PPredio p = predios.get(0);
                p.getPFichaMatrizs().size();
                p.getPPredioZonas().size();
                p.getPPredioDireccions().size();
                p.getPFotos().size();
                p.getPUnidadConstruccions().size();
                p.getPPredioAvaluoCatastrals().size();
                p.getPPredioServidumbre().size();
                p.getPReferenciaCartograficas().size();

                /* if (p.getPUnidadConstruccions().size() > 0) { for (IModeloUnidadConstruccion puc
                 * : p.getPUnidadConstruccions()) { if (puc.getUsoConstruccion() != null) {
                 * puc.getUsoConstruccion().getNombre(); }
                 *
                 * String tipificacion = puc.getTipificacion(); if (tipificacion != null &&
                 * !tipificacion.equals("")) {
                 *
                 * if (tipificacion.equals("NA")){ puc.setTipificacionValor("NA"); }else{ String
                 * criterio[]=new String[]{"tipo","=",tipificacion}; List<String[]> criterios=new
                 * ArrayList<String[]>(); criterios.add(criterio);
                 *
                 * List<TipificacionUnidadConst>
                 * tucs=tipificacionUnidadConstDAO.findByCriteria(criterios); if (tucs!=null &&
                 * !tucs.isEmpty()){ puc.setTipificacionValor(tucs.get(0).getTipificacion()); } } }
                 *
                 * }
                 * } */
                if (p.getPPersonaPredios() != null) {
                    Hibernate.initialize(p.getPPersonaPredios());
                    p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                        .getPPersonaPredios()));
                }

                /* if (p.getPUnidadConstruccions().size() > 0) { for (PUnidadConstruccion puc :
                 * p.getPUnidadConstruccions()) { if (puc.getProvienePredio() != null) {
                 * puc.getProvienePredio().getNumeroPredial();
                 * puc.getProvienePredio().getDerechosPropiedad().size();
                 * LOGGER.debug(puc.getProvienePredio().getNumeroPredial());
                 * LOGGER.debug(""+puc.getProvienePredio().getDerechosPropiedad().size()); } } } */
                return p;
            }

        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el predio asociado a dicho trámite o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage());
        }
        throw new Exception("No se encontró el predio asociado a dicho trámite");
    }

    /**
     * @see IPPredioDAO#findPPrediosCompletosByTramiteId(Long)
     *
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> findPPrediosCompletosByTramiteId(Long idTramite) {
        String query = "SELECT DISTINCT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " LEFT JOIN FETCH pp.PPersona p2" +
            " LEFT JOIN FETCH p2.direccionPais" +
            " LEFT JOIN FETCH p2.direccionDepartamento" +
            " LEFT JOIN FETCH p2.direccionMunicipio" +
            " WHERE p.tramite.id = :idTramite" +
            " AND (p.cancelaInscribe != 'C' OR p.cancelaInscribe IS NULL)";
        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", idTramite);
        try {
            @SuppressWarnings("unchecked")
            List<PPredio> pPredios = q.getResultList();
            for (PPredio p : pPredios) {
                p.getAreaConstruccion();
                p.getAreaTerreno();
                p.getPFichaMatrizs().size();
                p.getPPredioZonas().size();
                p.getPPredioDireccions().size();
                p.getPFotos().size();
                p.getPUnidadConstruccions().size();
                p.getPPredioAvaluoCatastrals().size();
                p.getPPredioServidumbre().size();

                if (p.getPUnidadConstruccions().size() > 0) {
                    for (IModeloUnidadConstruccion puc : p
                        .getPUnidadConstruccions()) {
                        if (puc.getUsoConstruccion() != null) {
                            puc.getUsoConstruccion().getNombre();
                        }
                    }
                }

                if (p.getPPersonaPredios() != null) {
                    Hibernate.initialize(p.getPPersonaPredios());
                    p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                        .getPPersonaPredios()));
                }
                //@modified by leidy.gonzalez
                if (p.getPReferenciaCartograficas() != null &&
                    !p.getPReferenciaCartograficas().isEmpty()) {
                    Hibernate.initialize(p.getPReferenciaCartograficas());
                }
                /*
                 * p.getPPersonaPredios().size();
                 *
                 * if (p.getPPersonaPredios()!=null){ for (PPersonaPredio
                 * pp:p.getPPersonaPredios()){ PPersona persona=pp.getPPersona();
                 * Hibernate.initialize(persona) persona.getDireccionPais();
                 * persona.getDireccionDepartamento(); persona.getDireccionMunicipio(); }
                 *
                 * }
                 */
            }
            return pPredios;
        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el pPredio o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage(), e);
            throw new ExcepcionSNC("AlgúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "No se encontró el predio", e);
        }

    }

    /**
     * Método que inicializa los datos requeridos (Fetch) de la lista
     *
     * @param pPersonaPredios
     */
    private List<PPersonaPredio> initializePPersonaPrediosData(
        List<PPersonaPredio> pPersonaPredios) {
        for (PPersonaPredio pp : pPersonaPredios) {
            if (pp.getPPersonaPredioPropiedads() != null) {
                pp.getPPersonaPredioPropiedads().size();
                for (PPersonaPredioPropiedad pppp : pp
                    .getPPersonaPredioPropiedads()) {
                    if (pppp.getDepartamento() != null) {
                        pppp.getDepartamento().getNombre();
                    }

                    if (pppp.getMunicipio() != null) {
                        pppp.getMunicipio().getNombre();
                    }

                    if (pppp.getDocumentoSoporte() != null &&
                        pppp.getDocumentoSoporte().getTipoDocumento() != null) {
                        pppp.getDocumentoSoporte().getTipoDocumento()
                            .getNombre();
                    }
                }
            }

        }
        return pPersonaPredios;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPPredioDAO#getPPredioFetchPPersonaPredioPorID(Long)
     */
    public PPredio getPPredioFetchPPersonaPredioPorId(Long id) {
        LOGGER.debug("getPPredioFetchPPersonaPredioPorID");
        String query = "SELECT p2" +
            " FROM PPredio p2 LEFT JOIN FETCH p2.PPersonaPredios pp" +
            " JOIN FETCH pp.PPersona" +
            " LEFT JOIN FETCH p2.departamento" +
            " LEFT JOIN FETCH p2.municipio" + " WHERE p2.id = :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("id", id);
        try {
            PPredio p = (PPredio) q.getSingleResult();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage(), e);
            return null;
        }
    }

    /**
     * Metodo que consulta pPredio y lo trae con las direcciones y las servidumbres
     *
     * @see IPPredioDAO#findPPredioByIdFetchDirecciones(Long)
     * @author javier.aponte
     */
    @Override
    public PPredio findPPredioByIdFetchDirecciones(Long idPredio) {
        String query = "SELECT p" // + " FROM PPredio p LEFT JOIN FETCH p.PPredioDireccions ppd"
            +
             " FROM PPredio p JOIN FETCH p.PPredioDireccions ppd" +
            " WHERE p.id = :idPredio";
        Query q = entityManager.createQuery(query);
        q.setParameter("idPredio", idPredio);
        try {
            PPredio p = (PPredio) q.getSingleResult();
            p.getPPredioServidumbre().size();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }

    }

    /**
     * @see IPPredioDAO#findPPrediosByTramiteId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredio> findPPrediosByTramiteId(Long tramiteId) {
        String query = "SELECT p" + " FROM PPredio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPredioDireccions ppd" +
            " WHERE p.tramite.id = :tramiteId";
        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        try {
            return q.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDAO#findPPrediosResultantesByTramiteId(java.lang.Long)
     */
    @Override
    public List<PPredio> findPPrediosResultantesByTramiteId(Long tramiteId) {
        String query = "SELECT DISTINCT p" + " FROM PPredio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPredioDireccions ppd" +
            " WHERE p.tramite.id = :tramiteId" +
            " AND p.cancelaInscribe != :cancelado";
        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        q.setParameter("cancelado", EProyeccionCancelaInscribe.CANCELA.getCodigo());
        try {
            List<PPredio> predios = (List<PPredio>) q.getResultList();
            for (PPredio pp : predios) {
                pp.getPPersonaPredios().size();
            }
            return predios;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @author juan.agudelo
     * @see IPPredioDAO#findExistingPPrediosByTramiteId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredio> findExistingPPrediosByTramiteId(Long tramiteId) {
        String query = "SELECT p" + " FROM PPredio p" +
            " WHERE p.tramite.id = :tramiteId";
        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        try {
            return q.getResultList();
        } catch (IndexOutOfBoundsException e) {
            return new ArrayList<PPredio>();
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPPrediosPanelAvaluoPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo correcto de excepciones. Manejo de Logger @modified
     * pedro.garcia 15-08-2012 definición de las unidades de construcción mediante llamado a método
     * que las consulta haciendo fetch de UsoConstruccion
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredio> buscarPPrediosPanelAvaluoPorTramiteId(Long tramiteId) {

        LOGGER.debug("on PPredioDAOBean#buscarPPrediosPanelAvaluoPorTramiteId");

        List<PPredio> answer;
        ArrayList<PUnidadConstruccion> pUnidadesConstruccion;

        String query = "SELECT p FROM PPredio p" +
            " LEFT JOIN FETCH p.PPredioAvaluoCatastrals pac " +
            " WHERE p.tramite.id = :tramiteId " +
            "   AND pac.id = (" +
            " SELECT MAX(id) FROM pac	WHERE pac.PPredio = p)";

        Query q = this.entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        try {
            answer = q.getResultList();

            if (answer != null) {
                for (PPredio pp : answer) {
                    if (pp.getPPredioAvaluoCatastrals() != null &&
                        !pp.getPPredioAvaluoCatastrals().isEmpty()) {
                        pp.getPPredioAvaluoCatastrals().size();
                    }

                    pUnidadesConstruccion = this.getPUnidadesConstruccion(pp.getId());
                    pp.setPUnidadConstruccions(pUnidadesConstruccion);

                    if (pp.getPPredioZonas() != null &&
                        !pp.getPPredioZonas().isEmpty()) {
                        pp.getPPredioZonas().size();
                    }
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosPanelAvaluoPorTramiteId");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPPrediosPanelJPropiedadPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo correcto de excepciones. Eliminación anotación
     * suppresWarnins(unchecked). Manejo de Logger
     */
    @Override
    public List<PPredio> buscarPPrediosPanelJPropiedadPorTramiteId(Long tramiteId) {

        LOGGER.debug("on PPredioDAOBean#buscarPPrediosPanelJPropiedadPorTramiteId");

        List<PPredio> answer;
        String query = "SELECT p FROM PPredio p" +
            " LEFT JOIN FETCH p.PPersonaPredios pp " +
            " WHERE p.tramite.id = :tramiteId" +
            " AND pp.cancelaInscribe != :cancelado";

        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        q.setParameter("cancelado", EProyeccionCancelaInscribe.CANCELA.getCodigo());

        try {
            answer = q.getResultList();

            if (answer != null) {
                for (PPredio p : answer) {
                    if (p.getPPersonaPredios() != null &&
                        !p.getPPersonaPredios().isEmpty()) {
                        for (PPersonaPredio pp : p.getPPersonaPredios()) {
                            pp.getPPersonaPredioPropiedads().size();
                            if (pp.getPPersonaPredioPropiedads() != null &&
                                !pp.getPPersonaPredioPropiedads().isEmpty()) {
                                for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {

                                    //N: no se sabe para qué hace que traiga depto y muni :: pedro.garcia
                                    if (ppp.getDepartamento() != null) {
                                        ppp.getDepartamento().getNombre();
                                    }
                                    if (ppp.getMunicipio() != null) {
                                        ppp.getMunicipio().getNombre();
                                    }
                                    //D: obliga a traer el objeto Documento
                                    if (ppp.getDocumentoSoporte() != null) {
                                        ppp.getDocumentoSoporte().getIdRepositorioDocumentos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosPanelJPropiedadPorTramiteId");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPPrediosPanelPropietariosPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo correcto de excepciones. Manejo de Logger
     */
    @Override
    public List<PPredio> buscarPPrediosPanelPropietariosPorTramiteId(Long tramiteId) {

        LOGGER.debug("on PPredioDAOBean#buscarPPrediosPanelPropietariosPorTramiteId");

        List<PPredio> answer;
        String query = "SELECT p FROM PPredio p" +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " WHERE p.tramite.id = :tramiteId" +
            " AND pp.cancelaInscribe != :cancelado";

        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        q.setParameter("cancelado", EProyeccionCancelaInscribe.CANCELA.getCodigo());
        try {
            answer = q.getResultList();

            if (answer != null) {
                for (PPredio p : answer) {
                    if (p.getPPersonaPredios() != null &&
                        !p.getPPersonaPredios().isEmpty()) {
                        for (PPersonaPredio pp : p.getPPersonaPredios()) {
                            pp.getPPersona().getNombre();
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosPanelPropietariosPorTramiteId");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPPrediosPanelUbicacionPredioPorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo correcto de excepciones. Manejo de Logger @modified
     * pedro.garcia 16-08-2012 Se hace fetch del CirculoRegistral del predio
     */
    @Implement
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredio> buscarPPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId) {

        LOGGER.debug("on PPredioDAOBean#buscarPPrediosPanelUbicacionPredioPorTramiteId");

        List<PPredio> ppAnswer;
        String queryString;
        Query query;

        queryString = "SELECT p" + " FROM PPredio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPredioServidumbre" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " WHERE p.tramite.id = :tramiteId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);

            ppAnswer = query.getResultList();

            if (ppAnswer != null) {
                for (PPredio pp : ppAnswer) {
                    if (pp.getPPredioDireccions() != null &&
                        !pp.getPPredioDireccions().isEmpty()) {
                        pp.getPPredioDireccions().size();
                    }
                    if (pp.getPPredioZonas() != null &&
                        !pp.getPPredioZonas().isEmpty()) {
                        pp.getPPredioZonas().size();
                    }
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosPanelUbicacionPredioPorTramiteId");
        }

        return ppAnswer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPPrediosResumenTramitePorTramiteId(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 30-07-2012 manejo correcto de excepciones. Manejo de Logger
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredio> buscarPPrediosResumenTramitePorTramiteId(Long tramiteId) {

        LOGGER.debug("on PPredioDAOBean#buscarPPrediosResumenTramitePorTramiteId");

        List<PPredio> answer;
        String query = "SELECT p FROM PPredio p" +
            " LEFT JOIN FETCH p.PPredioAvaluoCatastrals pac" +
            " WHERE p.tramite.id = :tramiteId" + " AND pac.id = (" +
            " SELECT MAX(id) FROM pac" + "	WHERE pac.PPredio = p)";

        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        try {
            answer = q.getResultList();

            if (answer != null) {
                for (PPredio pp : answer) {
                    if (pp.getPPredioAvaluoCatastrals() != null &&
                        !pp.getPPredioAvaluoCatastrals().isEmpty()) {
                        pp.getPPredioAvaluoCatastrals().size();
                    }
                    if (pp.getPUnidadConstruccions() != null &&
                        !pp.getPUnidadConstruccions().isEmpty()) {
                        pp.getPUnidadConstruccions().size();
                    }
                    if (pp.getPPredioZonas() != null &&
                        !pp.getPPredioZonas().isEmpty()) {
                        pp.getPPredioZonas().size();
                    }
                    if (pp.getPPersonaPredios() != null &&
                        !pp.getPPersonaPredios().isEmpty()) {
                        pp.getPPersonaPredios().size();
                    }
                    if (pp.getPFichaMatrizs() != null &&
                        !pp.getPFichaMatrizs().isEmpty()) {
                        pp.getPFichaMatrizs().size();
                    }
                }

            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosResumenTramitePorTramiteId");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#findByIds(List)
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> findByIds(List<Long> predioIds) {
        String query = "SELECT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE p.id IN (:predioIds) AND p.cancelaInscribe != 'C'";
        Query q = entityManager.createQuery(query);
        q.setParameter("predioIds", predioIds);
        try {
            @SuppressWarnings("unchecked")
            List<PPredio> pPredios = q.getResultList();

            //se inicializan los datos requeridos (A la fuerza)
            pPredios = this.initializePPredioData(pPredios);

            return pPredios;
        } catch (Exception e) {
            LOGGER.debug("Error en consulta de proyecciones de predios");
            LOGGER.debug("Error: " + e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error en consulta de proyecciones de predios", e);
        }
    }

    /**
     * @see IPPredioDAO#findByIdsPHCondominio(List)
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> findByIdsPHCondominio(List<Long> predioIds) {
        String query = "SELECT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE p.id IN (:predioIds)" +
            " AND (p.cancelaInscribe != :cancela" +
            " OR p.condicionPropiedad = :condicionCondominio" +
            " OR p.condicionPropiedad = :condicionPH)";
        Query q = entityManager.createQuery(query);

        q.setParameter("cancela", EProyeccionCancelaInscribe.CANCELA.getCodigo());
        q.setParameter("condicionCondominio", ETramiteTipoInscripcion.CONDOMINIO.getCodigo());
        q.setParameter("condicionPH", ETramiteTipoInscripcion.PH.getCodigo());
        q.setParameter("predioIds", predioIds);
        try {
            @SuppressWarnings("unchecked")
            List<PPredio> pPredios = q.getResultList();

            //se inicializan los datos requeridos (A la fuerza)
            pPredios = this.initializePPredioData(pPredios);

            return pPredios;
        } catch (Exception e) {
            LOGGER.debug("Error en consulta de proyecciones de predios");
            LOGGER.debug("Error: " + e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error en consulta de proyecciones de predios", e);
        }
    }

    private List<PPredio> initializePPredioData(List<PPredio> pPredios) {
        for (PPredio pp : pPredios) {

            //Se obtiene el tramite asociado a la proyeccion
            Hibernate.initialize(pp.getTramite());

            pp.getPFichaMatrizs().size();
            pp.getPPredioZonas().size();
            pp.getPPredioDireccions().size();
            pp.getPFotos().size();
            pp.getPUnidadConstruccions().size();
            pp.getPPredioAvaluoCatastrals().size();
            pp.getPPredioServidumbre().size();
            pp.getPPersonaPredios().size();

            for (PPersonaPredio ppp : pp.getPPersonaPredios()) {
                if (ppp.getPPersona().getDireccionPais() != null) {
                    ppp.getPPersona().getDireccionPais().getNombre();
                }
                if (ppp.getPPersona().getDireccionDepartamento() != null) {
                    ppp.getPPersona().getDireccionDepartamento().getNombre();
                }
                if (ppp.getPPersona().getDireccionMunicipio() != null) {
                    ppp.getPPersona().getDireccionMunicipio().getNombre();
                }
                ppp.getPPersonaPredioPropiedads().size();
                for (PPersonaPredioPropiedad pppp : ppp
                    .getPPersonaPredioPropiedads()) {
                    if (pppp.getDepartamento() != null) {
                        pppp.getDepartamento().getNombre();
                    }

                    if (pppp.getMunicipio() != null) {
                        pppp.getMunicipio().getNombre();
                    }

                    if (pppp.getDocumentoSoporte() != null &&
                        pppp.getDocumentoSoporte().getTipoDocumento() != null) {
                        pppp.getDocumentoSoporte().getTipoDocumento()
                            .getNombre();
                    }
                }
            }

            if (pp.getPUnidadConstruccions().size() > 0) {
                for (IModeloUnidadConstruccion puc : pp
                    .getPUnidadConstruccions()) {
                    puc.getUsoConstruccion().getNombre();
                }
            }

            if (pp.getPPersonaPredios() != null) {
                for (PPersonaPredio ppp : pp.getPPersonaPredios()) {
                    if (ppp.getPPersonaPredioPropiedads() != null) {
                        ppp.getPPersonaPredioPropiedads().size();
                        for (PPersonaPredioPropiedad pppp : ppp
                            .getPPersonaPredioPropiedads()) {
                            if (pppp.getDocumentoSoporte() != null &&
                                pppp.getDocumentoSoporte()
                                    .getTipoDocumento() != null) {
                                pppp.getDocumentoSoporte().getTipoDocumento()
                                    .getNombre();
                            }
                        }
                    }
                }
            }
        }
        return pPredios;
    }

    /**
     * @see IPPredioDAO#findPPrediosCompletosByTramiteIdPHCondominio(Long)
     *
     * @author fabio.navarrete
     */
    @Override
    public List<PPredio> findPPrediosCompletosByTramiteIdPHCondominio(
        Long idTramite) {
        String query = "SELECT DISTINCT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " LEFT JOIN FETCH pp.PPersona p2" +
            " LEFT JOIN FETCH p2.direccionPais" +
            " LEFT JOIN FETCH p2.direccionDepartamento" +
            " LEFT JOIN FETCH p2.direccionMunicipio" +
            " WHERE p.tramite.id = :idTramite" +
            " AND (p.cancelaInscribe != 'C'" +
            " OR p.cancelaInscribe IS NULL" +
            " OR p.condicionPropiedad = :condicionCondominio" +
            " OR p.condicionPropiedad = :condicionPH)";
        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", idTramite);
        q.setParameter("condicionCondominio", ETramiteTipoInscripcion.CONDOMINIO.getCodigo());
        q.setParameter("condicionPH", ETramiteTipoInscripcion.PH.getCodigo());

        try {
            @SuppressWarnings("unchecked")
            List<PPredio> pPredios = q.getResultList();
            for (PPredio p : pPredios) {
                p.getPFichaMatrizs().size();
                p.getPPredioZonas().size();
                p.getPPredioDireccions().size();
                p.getPFotos().size();
                p.getPUnidadConstruccions().size();
                p.getPPredioAvaluoCatastrals().size();
                p.getPPredioServidumbre().size();

                if (p.getPUnidadConstruccions().size() > 0) {
                    for (IModeloUnidadConstruccion puc : p
                        .getPUnidadConstruccions()) {
                        if (puc.getUsoConstruccion() != null) {
                            puc.getUsoConstruccion().getNombre();
                        }
                        if (puc.getPUnidadConstruccionComps() != null) {
                            puc.getPUnidadConstruccionComps().size();
                        }
                    }
                }

                if (p.getPPersonaPredios() != null) {
                    p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                        .getPPersonaPredios()));
                }

                if (p.getPPredioDireccions() != null) {
                    Hibernate.initialize(p.getPPredioDireccions());
                }
            }
            return pPredios;
        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el pPredio o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage(), e);
            throw new ExcepcionSNC("AlgúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "No se encontró el predio", e);
        }
    }
//---------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPPrediosPanelFichaMatrizPorTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public List<PPredio> buscarPPrediosPanelFichaMatrizPorTramiteId(
        Long tramiteId) {
        LOGGER.debug("PPredioDAOBean#buscarPrediosPanelPanelFichaMatrizPorTramiteId ...");

        List<PPredio> answer = new ArrayList<PPredio>();
        String queryString;
        Query query;

        queryString = "SELECT p FROM PPredio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " WHERE p.tramite.id = :tramiteId";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);

            answer = (List<PPredio>) query.getResultList();

            if (answer != null && !answer.isEmpty()) {

                for (PPredio pp : answer) {

                    if (pp.getPFichaMatrizs() != null &&
                        !pp.getPFichaMatrizs().isEmpty()) {

                        for (PFichaMatriz fm : pp.getPFichaMatrizs()) {

                            if (fm.getPFichaMatrizPredios() != null &&
                                !fm.getPFichaMatrizPredios().isEmpty()) {
                                fm.getPFichaMatrizPredios().size();

                                for (PFichaMatrizPredio fmp : fm
                                    .getPFichaMatrizPredios()) {

                                    fmp.setValorAvaluo(this.ppredioAvaluoCatastralDAO
                                        .findValorAvaluoByNumeroPredial(fmp
                                            .getNumeroPredial()));
                                }
                            }

                            if (fm.getPFichaMatrizTorres() != null &&
                                !fm.getPFichaMatrizTorres().isEmpty()) {

                                fm.getPFichaMatrizTorres().size();
                            }
                        }
                    }

                    if (pp.getPPredioZonas() != null &&
                        !pp.getPPredioZonas().isEmpty()) {

                        pp.getPPredioZonas().size();
                    }

                    if (pp.getPPredioDireccions() != null &&
                        !pp.getPPredioDireccions().isEmpty()) {

                        pp.getPPredioDireccions().size();
                    }

                    if (pp.getPUnidadConstruccions() != null &&
                        !pp.getPUnidadConstruccions().isEmpty()) {

                        pp.getPUnidadConstruccions().size();

                        for (PUnidadConstruccion uc : pp
                            .getPUnidadConstruccions()) {

                            if (uc.getUsoConstruccion() != null) {
                                uc.getUsoConstruccion().getNombre();
                            }

                            if (uc.getPUnidadConstruccionComps() != null &&
                                !uc.getPUnidadConstruccionComps()
                                    .isEmpty()) {

                                uc.getPUnidadConstruccionComps().size();

                                for (PUnidadConstruccionComp ucc : uc
                                    .getPUnidadConstruccionComps()) {

                                    ucc.getComponente();

                                    if (ucc.getPUnidadConstruccion() != null) {
                                        ucc.getPUnidadConstruccion().getId();
                                    }
                                }
                            }
                        }

                    }
                }
            }

        } catch (IndexOutOfBoundsException ne) {

            return new ArrayList<PPredio>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
    //---------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDAO#actualizacionDeAreasEnPredios(java.util.List,
     * java.util.List)
     *
     * @modified andres.eslava::05/NOV/2013::el area se guarda sin cifras decimales requerimiento
     * #5638 @modified andres.eslava¨::07/ENE/2013::error cuando se trabaja con una lista superior a
     * 1000 en la sentencia IN de SQL; se agrega metodo que valida y hace conjuntos mas pequeños de
     * predios. @modified by javier.aponte se guarda el valor del área truncado a 2 cifras decimales
     * refs#12022
     */
    @Override
    public boolean actualizacionDeAreasEnPredios(List<String> predios,
        List<Double> areas) {
        boolean flag = false;
        List<PPredio> prediosProy = obtenerPrediosProyectadosConFichaMatriz(predios);

        try {
            if (!prediosProy.isEmpty()) {

                for (int i = 0; i < predios.size(); i++) {
                    EqualPredicate nameEqlPredicate = new EqualPredicate(predios.get(i));
                    BeanPredicate beanPredicate = new BeanPredicate("numeroPredial",
                        nameEqlPredicate);
                    PPredio ppredio = (PPredio) CollectionUtils.find(prediosProy, beanPredicate);
                    // Se redondea el área a dos cifras decimales refs#12022

                    if (ppredio != null) {
                        ppredio.setAreaTerreno(Utilidades.redondearNumeroADosCifrasDecimales(areas.
                            get(i)));
                        ppredio = this.update(ppredio);

                        if (ppredio.isEsPredioFichaMatriz()) {
                            PFichaMatriz pfm = this.pFichaMatrizDAO.findByPredioId(ppredio.getId());
                            //Se redondea el área a dos cifras decimales refs#12022
                            pfm.setAreaTotalTerrenoComun(Utilidades.
                                redondearNumeroADosCifrasDecimales(areas.get(i)));
                            pfm = this.pFichaMatrizDAO.update(pfm);
                        }
                    }
                    flag = true;
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return flag;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Obtiene las unidades de construccion relacionadas con un PPredio. Hace fetch de los
     * UsoConstruccion
     *
     * @author pedro.garcia
     * @version 2.0
     * @param ppredioId
     * @return
     */
    public ArrayList<PUnidadConstruccion> getPUnidadesConstruccion(Long ppredioId) {

        ArrayList<PUnidadConstruccion> answer = null;
        PPredio tempAnswer;
        Query query;
        String queryString;

        queryString = "SELECT p " +
            " FROM PPredio p " +
            " LEFT JOIN FETCH p.PUnidadConstruccions uc" +
            " LEFT JOIN FETCH uc.usoConstruccion" +
            " WHERE p.id = :pPredioId_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("pPredioId_p", ppredioId);
            tempAnswer = (PPredio) query.getSingleResult();
            if (null != tempAnswer.getPUnidadConstruccions() &&
                !tempAnswer.getPUnidadConstruccions().isEmpty()) {

                //N: esto genera error al tratar de hacer cast entre PersistentBag y ArrayList,
                //  por eso hay que hacerlo a mano
                //answer = (ArrayList<PUnidadConstruccion>) tempAnswer.getPUnidadConstruccions();
                answer = new ArrayList<PUnidadConstruccion>();
                for (PUnidadConstruccion uc : tempAnswer.getPUnidadConstruccions()) {
                    answer.add(uc);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error obteniendo las unidades de construcción del PPredio con id " +
                ppredioId + ": " + ex.getMessage());
        }

        return answer;
    }

    //------------------------------------------------------//
    /**
     * Método que realiza la suma de los avalúos de los predios asociados a la ficha matriz.
     *
     * @author david.cifuentes
     *
     * @param idTramite
     * @return
     */
    public Double calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long idTramite) {

        Double answer = null;
        String queryString;
        Query query;
        Object sumaAvaluos;

        queryString = "SELECT SUM(p.avaluoCatastral) FROM PPredio p" +
            " WHERE p.tramite.id = :idTramite";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramite", idTramite);

            sumaAvaluos = query.getSingleResult();
            answer = (Double) sumaAvaluos;
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#buscarPorTramiteId(java.lang.Long, java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<PPredio> buscarPorTramiteId(Long idTramite, List<String> cmiValuesToUse) {

        LOGGER.debug("on PPredioDAOBean#buscarPorTramiteId ");

        List<PPredio> answer = null;
        StringBuilder queryString;
        boolean useCMI;
        Query query;

        queryString = new StringBuilder();
        useCMI = false;

        queryString.append("SELECT p FROM PPredio p WHERE p.tramite.id = :idTramiteP");

        if (null != cmiValuesToUse && !cmiValuesToUse.isEmpty()) {
            queryString.append(" AND p.cancelaInscribe IN (:cmiP)");
            useCMI = true;
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idTramiteP", idTramite);
            if (useCMI) {
                query.setParameter("cmiP", cmiValuesToUse);
            }

            answer = query.getResultList();

            if (answer != null) {
                for (PPredio pp : answer) {
                    if (pp.getPUnidadConstruccions() != null) {
                        Hibernate.initialize(pp.getPUnidadConstruccions());
                        for (PUnidadConstruccion pu : pp
                            .getPUnidadConstruccions()) {
                            if (pu.getPUnidadConstruccionComps() != null) {
                                Hibernate.initialize(pu
                                    .getPUnidadConstruccionComps());
                            }
                        }
                    }

                    if (pp.getPPredioDireccions() != null) {
                        Hibernate.initialize(pp.getPPredioDireccions());
                    }

                    if (pp.getDepartamento() != null) {

                        Hibernate.initialize(pp.getDepartamento());

                        if (pp.getDepartamento().getNombre() != null) {

                            Hibernate.initialize(pp.getDepartamento().getNombre());
                        }
                    }

                    if (pp.getMunicipio() != null) {

                        Hibernate.initialize(pp.getMunicipio());

                        if (pp.getMunicipio().getNombre() != null) {

                            Hibernate.initialize(pp.getMunicipio().getNombre());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PPredioDAOBean#buscarPorTramiteId");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#obtenerConDatosPanelUbicacion(Long.MIN_VALUE)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerConDatosUbicacion(Long ppredioId) {

        LOGGER.debug("on PPredioDAOBean#obtenerConDatosPanelUbicacion ");

        PPredio answer = null;
        String queryString;
        Query query;

        queryString = "SELECT p FROM PPredio p " +
            " LEFT JOIN FETCH p.departamento " +
            " LEFT JOIN FETCH p.municipio " +
            " LEFT JOIN FETCH p.PPredioServidumbre " +
            " LEFT JOIN FETCH p.circuloRegistral " +
            " WHERE p.id = :ppredioIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("ppredioIdP", ppredioId);

            answer = (PPredio) query.getSingleResult();

            if (answer != null) {
                if (answer.getPPredioDireccions() != null &&
                    !answer.getPPredioDireccions().isEmpty()) {
                    answer.getPPredioDireccions().size();
                }
                if (answer.getPPredioZonas() != null && !answer.getPPredioZonas().isEmpty()) {
                    answer.getPPredioZonas().size();
                }

            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerConDatosPanelUbicacion");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#obtenerConDatosJustificacionPropiedad(Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerConDatosJustificacionPropiedad(Long ppredioId) {

        LOGGER.debug("on PPredioDAOBean#obtenerConDatosJustificacionPropiedad");

        PPredio answer;
        String queryString;
        Query query;

        queryString = "SELECT p FROM PPredio p " +
            " LEFT JOIN FETCH p.PPersonaPredios pp " +
            " WHERE p.id = :ppredioIdP ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("ppredioIdP", ppredioId);
            answer = (PPredio) query.getSingleResult();

            if (answer != null) {
                if (answer.getPPersonaPredios() != null && !answer.getPPersonaPredios().isEmpty()) {
                    for (PPersonaPredio pp : answer.getPPersonaPredios()) {
                        pp.getPPersonaPredioPropiedads().size();
                        if (pp.getPPersonaPredioPropiedads() != null &&
                            !pp.getPPersonaPredioPropiedads().isEmpty()) {
                            for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {

                                if (ppp.getDepartamento() != null) {
                                    ppp.getDepartamento().getNombre();
                                }
                                if (ppp.getMunicipio() != null) {
                                    ppp.getMunicipio().getNombre();
                                }
                                //D: obliga a traer el objeto Documento
                                if (ppp.getDocumentoSoporte() != null) {
                                    ppp.getDocumentoSoporte().getIdRepositorioDocumentos();
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerConDatosJustificacionPropiedad");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#obtenerConDatosAvaluo(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerConDatosAvaluo(Long ppredioId) {

        LOGGER.debug("on PPredioDAOBean#obtenerConDatosAvaluo");

        PPredio answer;
        ArrayList<PUnidadConstruccion> pUnidadesConstruccion;
        String queryString;
        Query query;

        queryString = "SELECT p FROM PPredio p" +
            " LEFT JOIN FETCH p.PPredioAvaluoCatastrals pac " +
            " WHERE p.id = :ppredioIdP " +
            "   AND pac.id = (" +
            " SELECT MAX(id) FROM pac	WHERE pac.PPredio = p)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("ppredioIdP", ppredioId);

            answer = (PPredio) query.getSingleResult();

            if (answer != null) {
                if (answer.getPPredioAvaluoCatastrals() != null &&
                    !answer.getPPredioAvaluoCatastrals().isEmpty()) {
                    answer.getPPredioAvaluoCatastrals().size();
                }

                pUnidadesConstruccion = this.getPUnidadesConstruccion(answer.getId());
                answer.setPUnidadConstruccions(pUnidadesConstruccion);

                if (answer.getPPredioZonas() != null && !answer.getPPredioZonas().isEmpty()) {
                    answer.getPPredioZonas().size();
                }

                if (answer.getPUnidadConstruccions() != null && !answer.getPUnidadConstruccions().
                    isEmpty()) {
                    answer.getPUnidadConstruccions().size();
                    for (PUnidadConstruccion puc : answer.getPUnidadConstruccions()) {
                        puc.getPUnidadConstruccionComps().size();
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PPredioDAOBean#obtenerConDatosAvaluo");
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#obtenerConDatosPropietarios(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerConDatosPropietarios(Long ppredioId) {

        LOGGER.debug("on PPredioDAOBean#obtenerConDatosPropietarios ");

        PPredio answer;
        String queryString;
        Query query;

        queryString = "SELECT p FROM PPredio p " +
            " LEFT JOIN FETCH p.PPersonaPredios pp " +
            " WHERE p.id = :ppredioIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("ppredioIdP", ppredioId);

            answer = (PPredio) query.getSingleResult();

            if (answer != null) {
                if (answer.getPPersonaPredios() != null && !answer.getPPersonaPredios().isEmpty()) {
                    for (PPersonaPredio pp : answer.getPPersonaPredios()) {
                        pp.getPPersona().getNombre();
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "PPredioDAOBean#obtenerConDatosPropietarios");
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPPredioDAO#obtenerConDatosFichaMatriz(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public PPredio obtenerConDatosFichaMatriz(Long ppredioId) {

        LOGGER.debug("PPredioDAOBean#obtenerConDatosFichaMatriz ");

        PPredio answer;
        String queryString;
        Query query;

        queryString = "SELECT p FROM PPredio p" +
             " LEFT JOIN FETCH p.departamento" +
             " LEFT JOIN FETCH p.municipio" +
             " LEFT JOIN FETCH p.circuloRegistral" +
             " LEFT JOIN FETCH p.PUnidadConstruccions pU" +
             " LEFT JOIN FETCH pU.usoConstruccion uso" +
             " WHERE p.id = :ppredioIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("ppredioIdP", ppredioId);

            answer = (PPredio) query.getSingleResult();

            if (answer != null) {

                if (answer.getPFichaMatrizs() != null && !answer.getPFichaMatrizs().isEmpty()) {

                    for (PFichaMatriz fm : answer.getPFichaMatrizs()) {

                        if (fm.getPFichaMatrizPredios() != null &&
                             !fm.getPFichaMatrizPredios().isEmpty()) {

                            fm.getPFichaMatrizPredios().size();

                            for (PFichaMatrizPredio fmp : fm.getPFichaMatrizPredios()) {

                                fmp.setValorAvaluo(this.ppredioAvaluoCatastralDAO
                                    .findValorAvaluoByNumeroPredial(fmp.getNumeroPredial()));
                            }
                        }

                        if (fm.getPFichaMatrizTorres() != null &&
                             !fm.getPFichaMatrizTorres().isEmpty()) {

                            fm.getPFichaMatrizTorres().size();
                        }
                    }
                }

                if (answer.getPPredioZonas() != null && !answer.getPPredioZonas().isEmpty()) {
                    answer.getPPredioZonas().size();
                }

                if (answer.getPPredioDireccions() != null &&
                     !answer.getPPredioDireccions().isEmpty()) {

                    answer.getPPredioDireccions().size();
                }

                if (answer.getDepartamento() != null) {

                    Hibernate.initialize(answer.getDepartamento());

                    if (answer.getDepartamento().getNombre() != null) {

                        Hibernate.initialize(answer.getDepartamento().getNombre());
                    }
                }

                if (answer.getMunicipio() != null) {

                    Hibernate.initialize(answer.getMunicipio());

                    if (answer.getMunicipio().getNombre() != null) {

                        Hibernate.initialize(answer.getMunicipio().getNombre());
                    }
                }

                if (answer.getPUnidadConstruccions() != null &&
                     !answer.getPUnidadConstruccions().isEmpty()) {

                    answer.getPUnidadConstruccions().size();
                    Hibernate.initialize(answer.getPUnidadConstruccions());

                    for (PUnidadConstruccion uc : answer.getPUnidadConstruccions()) {

                        if (uc.getUsoConstruccion() != null) {
                            Hibernate.initialize(uc.getUsoConstruccion());
                            uc.getUsoConstruccion().getNombre();
                            Hibernate.initialize(uc.getUsoConstruccion().getNombre());
                        }

                        if (uc.getPUnidadConstruccionComps() != null &&
                             !uc.getPUnidadConstruccionComps().isEmpty()) {

                            uc.getPUnidadConstruccionComps().size();
                            Hibernate.initialize(uc.getPUnidadConstruccionComps());

                            for (PUnidadConstruccionComp ucc : uc.getPUnidadConstruccionComps()) {

                                ucc.getComponente();
                                if (ucc.getPUnidadConstruccion() != null) {
                                    ucc.getPUnidadConstruccion().getId();
                                }
                            }
                        }
                    }
                }

                if (answer.getPUnidadesConstruccionConvencional() != null &&
                     !answer.getPUnidadesConstruccionConvencional().isEmpty()) {

                    answer.getPUnidadesConstruccionConvencional().size();
                    Hibernate.initialize(answer.getPUnidadesConstruccionConvencional());

                    for (PUnidadConstruccion ucConv : answer.getPUnidadesConstruccionConvencional()) {

                        if (ucConv.getUsoConstruccion() != null) {
                            Hibernate.initialize(ucConv.getUsoConstruccion());
                            ucConv.getUsoConstruccion().getNombre();
                            Hibernate.initialize(ucConv.getUsoConstruccion().getNombre());
                        }

                    }
                }

                if (answer.getPUnidadesConstruccionNoConvencional() != null &&
                     !answer.getPUnidadesConstruccionNoConvencional().isEmpty()) {

                    answer.getPUnidadesConstruccionNoConvencional().size();
                    Hibernate.initialize(answer.getPUnidadesConstruccionNoConvencional());

                    for (PUnidadConstruccion ucNConv : answer.
                        getPUnidadesConstruccionNoConvencional()) {

                        if (ucNConv.getUsoConstruccion() != null) {
                            Hibernate.initialize(ucNConv.getUsoConstruccion());
                            ucNConv.getUsoConstruccion().getNombre();
                            Hibernate.initialize(ucNConv.getUsoConstruccion().getNombre());
                        }

                    }
                }
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerConDatosFichaMatriz");
        }
        return answer;

    }

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDAO#getPPrediosByTramiteIdActualizarAvaluo(java.lang.Long)
     */
    @Override
    public List<PPredio> getPPrediosByTramiteIdActualizarAvaluo(Long tramiteId) {

        LOGGER.debug("PPredioDAOBean#getPPrediosByTramiteIdActualizarAvaluo ");
        List<PPredio> answer = null;
        String query = "SELECT pp FROM PPredio pp" +
            " JOIN pp.tramite t " +
            " WHERE t.id =:tramiteId and" +
            " pp.cancelaInscribe NOT LIKE 'C'" +
            " ORDER BY pp.numeroPredial";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            answer = (List<PPredio>) q.getResultList();

            if (answer != null) {
                for (PPredio unPredioProyectado : answer) {
                    Hibernate.initialize(unPredioProyectado.getPPredioDireccions());
                    Hibernate.initialize(unPredioProyectado.getPUnidadConstruccions());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#getPPrediosByTramiteIdActualizarAvaluo");
        }

        return answer;
    }

    /**
     * @modified juan.mendez 2014/04/03 Se modificó la firma del método de SigDao. Ya no requiere el
     * parámetro token.
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public boolean updatePPredioZonasPrediosInscritos(
        Long tramiteId, String numPredPrin, UsuarioDTO usuario, boolean esQuinta) {
        String query = "SELECT DISTINCT pp FROM PPredio pp " +
            " LEFT JOIN FETCH pp.PPredioZonas ppz" +
            " WHERE pp.tramite.id =:tramiteId";

        Query q = this.entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);

        List<PPredio> predTemp = (List<PPredio>) q.getResultList();

        String replica = this.documentoDAO.
            obtenerIdRepositorioDocumentosDeReplicaOriginal(tramiteId,
                ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());

        if (predTemp != null && !predTemp.isEmpty()) {
            List<PPredio> auxTemp = new ArrayList<PPredio>();
            for (PPredio pp : predTemp) {
                if ((!pp.getNumeroPredial().equals(numPredPrin) ||
                    pp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    pp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) &&
                    !pp.isMejora() &&
                    !pp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo()) &&
                    !pp.isUnidadEnPH()) {

                    auxTemp.add(pp);
                }
            }
            predTemp = auxTemp;
        }

        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos = SigDAOv2.getInstance().
            obtenerZonasHomogeneas(predTemp, replica, usuario);
        return this.updatePPredioZonasPrediosInscritos(tramiteId, numPredPrin, usuario, esQuinta,
            datos, predTemp);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see IPPredioDAO#updatePPredioZonasPrediosInscritos(Long,String,UsuarioDTO)
     */
    // @modified by javier.aponte se guarda el valor del área truncado a 3 cifras decimales refs#12022
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean updatePPredioZonasPrediosInscritos(
        Long tramiteId, String numPredPrin, UsuarioDTO usuario, boolean esQuinta,
        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos, List<PPredio> predTemp) {

        try {

            if (predTemp != null && !predTemp.isEmpty()) {
                List<PPredioZona> predioZonas = null;
                List<PPredioZona> predioZonasCancelar = null;

                for (PPredio pp : predTemp) {
                    predioZonas = new ArrayList<PPredioZona>();
                    predioZonasCancelar = new ArrayList<PPredioZona>();

//					if ((!pp.getNumeroPredial().equals(numPredPrin)
//							|| pp.getCancelaInscribe().equals(
//									EProyeccionCancelaInscribe.MODIFICA.getCodigo()))
//									&& !pp.isMejora()
//									&& !pp.getCancelaInscribe().equals(
//											EProyeccionCancelaInscribe.CANCELA.getCodigo())
//									&& !pp.isUnidadEnPH()) {
                    List<ZonasFisicasGeoeconomicasVO> resultado = datos.get(pp.getNumeroPredial());

                    if (resultado != null && !resultado.isEmpty()) {
                        for (ZonasFisicasGeoeconomicasVO zona : resultado) {
                            boolean existe = false;
                            PPredioZona zonaExistente = null;

                            //Ciclo para validar la existencia de la zona
                            if (pp.getPPredioZonas() != null &&
                                !pp.getPPredioZonas().isEmpty()) {
                                for (PPredioZona ppz : pp.getPPredioZonas()) {
                                    if (ppz.getZonaFisica().equals(zona.getCodigoFisica()) &&
                                        ppz.getZonaGeoeconomica().equals(zona.
                                            getCodigoGeoeconomica()) &&
                                        (ppz.getCancelaInscribe() == null || !ppz.
                                        getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.CANCELA.getCodigo()))) {
                                        existe = true;
                                        zonaExistente = ppz;
                                    }
                                }
                            }
                            if (!existe) {
                                PPredioZona predioZona = new PPredioZona();

                                predioZona.setPPredio(pp);
                                predioZona.setZonaFisica(zona.getCodigoFisica());
                                predioZona.setZonaGeoeconomica(zona.getCodigoGeoeconomica());
                                //se redondea el valor del área a 2 cifras decimales refs#12022
                                predioZona.setArea(Utilidades.redondearNumeroADosCifrasDecimales(
                                    Double.valueOf(zona.getArea())));
                                predioZona.setUsuarioLog(usuario.getLogin());
                                predioZona.setFechaLog(new Date());
                                predioZona.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
                                    getCodigo());
                                predioZona.setFechaInscripcionCatastral(new Date());

                                Calendar cal = Calendar.getInstance();
                                //cal.set(Calendar.YEAR, new Date().getYear());
                                cal.set(Calendar.MONTH, 0);
                                cal.set(Calendar.DAY_OF_MONTH, 1);
                                predioZona.setVigencia(cal.getTime());

                                Double valorM2Terreno = this.sncProcedimientoDAO
                                    .obtenerValorUnidadTerreno(pp
                                        .getNumeroPredial()
                                        .substring(0, 7), pp
                                        .getDestino(), zona
                                            .getCodigoGeoeconomica(),
                                        pp.getId());
                                if (valorM2Terreno != null) {
                                    predioZona.setValorM2Terreno(valorM2Terreno);
                                } else {
                                    predioZona.setValorM2Terreno(new Double(-1));
                                }
                                // Se calcula el avalúo
                                predioZona.setAvaluo(predioZona.getArea() *
                                    predioZona.getValorM2Terreno());

                                this.pPredioZonaDAO.update(predioZona);

                                predioZonas.add(predioZona);
                            } else {
                                zonaExistente.setArea(Utilidades.redondearNumeroADosCifrasDecimales(
                                    Double.valueOf(zona.getArea())));
                                zonaExistente.setUsuarioLog(usuario.getLogin());
                                zonaExistente.setFechaLog(new Date());
                                Calendar cal = Calendar.getInstance();
                                //cal.set(Calendar.YEAR, new Date().getYear());
                                cal.set(Calendar.MONTH, 0);
                                cal.set(Calendar.DAY_OF_MONTH, 1);
                                zonaExistente.setVigencia(cal
                                    .getTime());
                                LOGGER.debug("####### ZONA = " + zonaExistente.getArea());
                                Double valorM2Terreno = this.sncProcedimientoDAO
                                    .obtenerValorUnidadTerreno(pp
                                        .getNumeroPredial()
                                        .substring(0, 7), pp
                                        .getDestino(), zona
                                            .getCodigoGeoeconomica(),
                                        pp.getId());
                                if (valorM2Terreno != null && valorM2Terreno != -1) {
                                    zonaExistente.setValorM2Terreno(valorM2Terreno);
                                } else {
                                    zonaExistente.setValorM2Terreno(new Double(-1));
                                }
                                // Se calcula el avalúo
                                zonaExistente.setAvaluo(zonaExistente.getArea() *
                                    zonaExistente.getValorM2Terreno());

                                this.pPredioZonaDAO.update(zonaExistente);

                                predioZonas.add(zonaExistente);
                            }
                        }
                        if (pp.getPPredioZonas() != null && !pp.getPPredioZonas().isEmpty()) {
                            for (PPredioZona ppz : pp.getPPredioZonas()) {
                                boolean existe = false;
                                for (PPredioZona zona : predioZonas) {
                                    if (zona.getZonaFisica().equals(ppz.getZonaFisica()) &&
                                        zona.getZonaGeoeconomica().equals(ppz.getZonaGeoeconomica())) {
                                        existe = true;
                                        break;
                                    }
                                }
                                if (!existe) {
                                    predioZonasCancelar.add(ppz);
                                }
                            }
                            if (predioZonasCancelar != null && !predioZonasCancelar.isEmpty()) {
                                for (PPredioZona zn : predioZonasCancelar) {
                                    zn.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
                                        getCodigo());
                                    this.pPredioZonaDAO.update(zn);
                                }
                            }
                        }
                        pp.setPPredioZonas(predioZonas);
                    }
                    //}
                }

                return true;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#updatePPredioZonasPrediosInscritos");
        }
        return false;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IPPredioDAO#getPPredioByTramiteId(Long)
     */
    @Override
    public PPredio getPPredioByTramiteId(Long tramiteId) {
        PPredio answer = null;

        String query = "SELECT pp FROM PPredio pp " +
            " JOIN pp.tramite t " +
            " WHERE t.id =:tramiteId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            answer = (PPredio) q.getSingleResult();

        } catch (NoResultException nr) {
            LOGGER.debug("No existen resultados para la consulta", nr);
            answer = null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#getPPredioByTramiteId");
        }

        return answer;
    }

    /**
     * @author javier.aponte
     * @see IPPredioDAO#getPPredioByNumeroPredial(String)
     */
    @Override
    public PPredio getPPredioByNumeroPredial(String numeroPredial) {
        LOGGER.debug("getPPredioByNumeroPredial");
        PPredio p = null;
        String query = "SELECT pp FROM PPredio pp " +
            "LEFT join fetch pp.departamento" +
            " LEFT join fetch pp.municipio" +
            " where pp.numeroPredial = :numeroPredial";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("numeroPredial", numeroPredial);
            p = (PPredio) q.getSingleResult();

            return p;
        } catch (Exception e) {
            LOGGER.debug("Error en obtener el predio: " + e.getMessage());
            return null;
        }
    }

    /**
     * @author andres.eslava
     * @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDAO#getPPrediosByTramiteIdActualizarAvaluo(java.lang.Long)
     */
    @Override
    public List<PPredio> getPPrediosByTramiteId(Long tramiteId) {

        LOGGER.debug("PPredioDAOBean#getPPrediosByTramiteIdActualizarAvaluo ");
        List<PPredio> answer = null;
        String query = "SELECT pp FROM PPredio pp" +
            " JOIN pp.tramite t " +
            " WHERE t.id =:tramiteId and" +
            " (pp.cancelaInscribe NOT LIKE 'C' or pp.cancelaInscribe IS NULL)";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            answer = (List<PPredio>) q.getResultList();

            if (answer != null) {
                for (PPredio unPredioProyectado : answer) {
                    Hibernate.initialize(unPredioProyectado.getPPredioDireccions());
                    Hibernate.initialize(unPredioProyectado.getPUnidadConstruccions());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#getPPrediosByTramiteIdActualizarAvaluo");
        }

        return answer;
    }

    /**
     * @see IPPredioDAO#obtenerPrediosProyectadosConFichaMatriz(java.util.List)
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredio> obtenerPrediosProyectadosConFichaMatriz(List<String> listaNumerosPrediales) {

        try {
            String query = "SELECT pp FROM PPredio pp " +
                " LEFT JOIN FETCH pp.PFichaMatrizs " +
                " WHERE pp.numeroPredial IN (:predios)";

            Query q = this.entityManager.createQuery(query);

            List<PPredio> prediosProyectados = new LinkedList<PPredio>();

            // Verifica que la cantidad de predios enviados es inferior a 1000, de lo contrario hace la consulta
            // por conjuntos parciales inferiores, 500.
            if (!listaNumerosPrediales.isEmpty() && listaNumerosPrediales.size() > 999) {

                int limiteInferior = 0;
                int limiteSuperior = 500;

                while (limiteSuperior <= listaNumerosPrediales.size()) {
                    List<String> predios = listaNumerosPrediales.subList(limiteInferior,
                        limiteSuperior);
                    q.setParameter("predios", predios);
                    List<PPredio> resultado = (List<PPredio>) q.getResultList();
                    if (!resultado.isEmpty()) {
                        for (PPredio pp : resultado) {
                            if (pp.getPFichaMatrizs() != null) {
                                for (PFichaMatriz pfm : pp.getPFichaMatrizs()) {
                                    Hibernate.initialize(pfm.getPPredio());
                                }
                            }
                        }
                        prediosProyectados.addAll(resultado);
                    }
                    limiteInferior = limiteSuperior;
                    if (limiteSuperior == listaNumerosPrediales.size()) {
                        break;
                    }
                    limiteSuperior = Math.min(limiteSuperior + 500, listaNumerosPrediales.size());
                }

                return prediosProyectados;
            } //Si el tamaño de la lista es inferior a 1000 hace el query por el total de prediales enviado.
            else {
                List<String> predios = listaNumerosPrediales;
                q.setParameter("predios", predios);
                return q.getResultList();
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IPPredioDAO#obtenerPrediosProyectadosConFichaMatriz(java.util.List)
     * @author andres.eslava
     */
    @Override
    public List<PPredio> obtenerPrediosProyectadosAsociadosAFicha(Long fichaMatrizId) {

        List<PPredio> resultado = new ArrayList<PPredio>();

        try {
            String query = "SELECT pp FROM PPredio pp, " +
                " PFichaMatrizPredio pfmp" +
                " WHERE pfmp.numeroPredial =  pp.numeroPredial" +
                " AND pfmp.PFichaMatriz.id = :fmId";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("fmId", fichaMatrizId);

            resultado = q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerPrediosProyectadosAsociadosAFicha");
        }

        return resultado;
    }

    /**
     * @see IPPredioDAO#buscarPorMatricula(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<PPredio> buscarPorMatricula(String circuloRegistralCodigo, String numeroRegistro) {

        List<PPredio> resultado = new ArrayList<PPredio>();

        try {
            String query = "SELECT pp FROM PPredio pp " +
                " WHERE pp.circuloRegistral.codigo =  :circuloRegistralCodigo " +
                " AND pp.numeroRegistro = :numeroRegistro";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("circuloRegistralCodigo", circuloRegistralCodigo);
            q.setParameter("numeroRegistro", numeroRegistro);

            resultado = q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPorMatricula");
        }

        return resultado;
    }

    /**
     * @see IPPredioDAO#getPPrediosByTramiteIdActualizarAvaluo(java.lang.Long, java.util.List)
     */
    public List<PPredio> getPPrediosByTramiteIdActualizarAvaluo(Long tramiteId,
        List<EProyeccionCancelaInscribe> valoresCancelaInscribre) {

        LOGGER.debug("PPredioDAOBean#getPPrediosByTramiteIdActualizarAvaluo ");
        List<PPredio> answer = null;
        String codigosCancelaInscribe = "";

        for (EProyeccionCancelaInscribe cancelaInscribe : valoresCancelaInscribre) {
            codigosCancelaInscribe += "'" + cancelaInscribe.getCodigo() + "',";
        }
        codigosCancelaInscribe = codigosCancelaInscribe.substring(0,
            codigosCancelaInscribe.length() - 1);
        String query = "SELECT pp FROM PPredio pp" +
            " JOIN pp.tramite t " +
            " WHERE t.id =:tramiteId and" +
            " pp.cancelaInscribe in (" + codigosCancelaInscribe +
            " ) ORDER BY pp.id";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);

            answer = (List<PPredio>) q.getResultList();

            if (answer != null) {
                for (PPredio unPredioProyectado : answer) {
                    Hibernate.initialize(unPredioProyectado.getPPredioDireccions());
                    Hibernate.initialize(unPredioProyectado.getPUnidadConstruccions());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#getPPrediosByTramiteIdActualizarAvaluo");
        }

        return answer;
    }

    /**
     * @see IPPredioDAO#obtenerPredioModificadoPorTamite(java.lang.Long, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public TreeMap<Long, Boolean> obtenerPredioModificadoPorTamite(Long idTramite,
        String nombreTablaAsociada) {

        TreeMap<Long, Boolean> resultado = new TreeMap<Long, Boolean>();

        String qString = "SELECT PREDIO_ID, SUM(MDF) TMDF FROM( " +
            "    SELECT PPD.PREDIO_ID, CASE WHEN PPD.CANCELA_INSCRIBE IS NULL THEN 0 " +
            "                               ELSE 1 " +
            "                          END  MDF " +
            "    FROM " + nombreTablaAsociada + " PPD " +
            "    WHERE PPD.PREDIO_ID IN ( " +
            "    SELECT PP.ID FROM P_PREDIO PP WHERE PP.TRAMITE_ID = :idTramite " +
            "    ) " +
            ")GROUP BY PREDIO_ID ";

        try {
            Query q = this.entityManager.createNativeQuery(qString);
            q.setParameter("idTramite", idTramite);

            List<Object> answer = (List<Object>) q.getResultList();

            LOGGER.debug(answer.toString());

            for (Object object : answer) {
                Object[] item = (Object[]) object;
                Long idPredio = ((BigDecimal) item[0]).longValue();
                int modificado = ((BigDecimal) item[1]).intValue();
                if (modificado > 0) {
                    resultado.put(idPredio, Boolean.TRUE);
                } else {
                    resultado.put(idPredio, Boolean.FALSE);
                }

            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerPredioModificadoPorTamite");
        }

        return resultado;
    }

    /**
     * @see IPPredioDAO#eliminarProyeccionPredio(Long)
     * @author felipe.cadena
     */
    @Override
    public void eliminarProyeccionPredio(Long idPredio) {

        LOGGER.debug("PPredioDAOBean#eliminarProyeccionPredio");

        String query = "DELETE FROM P_PREDIO WHERE ID = " + idPredio.toString();
        try {
            Query q = this.entityManager.createNativeQuery(query);
            q.executeUpdate();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#eliminarProyeccionPredio");
        }
    }

    /**
     * @param idTramite
     * @return
     * @see IPPredioDAO#findPPrediosCompletosActivosByTramiteId(Long)
     *
     * @author lorena.salamanca
     */
    @Override
    public List<PPredio> findPPrediosCompletosActivosByTramiteId(Long idTramite) {
        String query = "SELECT DISTINCT p" +
            " FROM PPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " LEFT JOIN FETCH p.PPersonaPredios pp" +
            " LEFT JOIN FETCH pp.PPersona p2" +
            " LEFT JOIN FETCH p2.direccionPais" +
            " LEFT JOIN FETCH p2.direccionDepartamento" +
            " LEFT JOIN FETCH p2.direccionMunicipio" +
            " WHERE p.tramite.id = :idTramite" +
            " AND p.estado <> 'CANCELADO'" +
            " AND (p.cancelaInscribe != 'C' OR p.cancelaInscribe IS NULL)";
        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", idTramite);
        try {
            @SuppressWarnings("unchecked")
            List<PPredio> pPredios = q.getResultList();
            for (PPredio p : pPredios) {
                p.getPFichaMatrizs().size();
                p.getPPredioZonas().size();
                p.getPPredioDireccions().size();
                p.getPFotos().size();
                p.getPUnidadConstruccions().size();
                p.getPPredioAvaluoCatastrals().size();
                p.getPPredioServidumbre().size();

                if (p.getPUnidadConstruccions().size() > 0) {
                    for (IModeloUnidadConstruccion puc : p
                        .getPUnidadConstruccions()) {
                        if (puc.getUsoConstruccion() != null) {
                            puc.getUsoConstruccion().getNombre();
                        }
                    }
                }

                if (p.getPPersonaPredios() != null) {
                    Hibernate.initialize(p.getPPersonaPredios());
                    p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                        .getPPersonaPredios()));
                }
            }
            return pPredios;
        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el pPredio o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage(), e);
            throw new ExcepcionSNC("AlgúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "No se encontró el predio", e);
        }

    }

    /**
     * @see IPPredioDAO#obtenerPredioConNumeroPredialModificadoPorTamite(Long)
     * @author javier.aponte
     */
    @Override
    public TreeMap<String, Boolean> obtenerPredioConNumeroPredialModificadoPorTamite(Long idTramite) {

        TreeMap<String, Boolean> resultado = new TreeMap<String, Boolean>();

        String qString = "SELECT NUMERO_PREDIAL," +
            "  		 SUM(MDF) TMDF " +
            "FROM				   " +
            "	  (SELECT PFMP.NUMERO_PREDIAL,								   " +
            "	    CASE                                                       " +
            "	      WHEN PFMP.NUMERO_PREDIAL != PFMP.NUMERO_PREDIAL_ORIGINAL " +
            "	      THEN 1                                                   " +
            "	      ELSE 0                                                   " +
            "	    END MDF                                                    " +
            "	  FROM P_FICHA_MATRIZ_PREDIO PFMP                              " +
            "	  INNER JOIN P_PREDIO PP                                       " +
            "	  ON PP.NUMERO_PREDIAL = PFMP.NUMERO_PREDIAL                   " +
            "	  WHERE PP.TRAMITE_ID  = :idTramite                            " +
            "	  )                                                            " +
            "GROUP BY NUMERO_PREDIAL";

        try {
            Query q = this.entityManager.createNativeQuery(qString);
            q.setParameter("idTramite", idTramite);

            List<Object> answer = (List<Object>) q.getResultList();

            LOGGER.debug(answer.toString());

            for (Object object : answer) {
                Object[] item = (Object[]) object;
                String numeroPredial = item[0].toString();
                int modificado = ((BigDecimal) item[1]).intValue();
                if (modificado > 0) {
                    resultado.put(numeroPredial, Boolean.TRUE);
                } else {
                    resultado.put(numeroPredial, Boolean.FALSE);
                }

            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerPredioConNumeroPredialModificadoPorTamite");
        }
        return resultado;
    }

    /**
     * Método que realiza la búsqueda de un predio por su id, pero no realiza fetch sobre ninguna
     * tabla, en vez de ello realiza la inicialización de los campos a través de un
     * Hibernate.inicialize
     *
     * @param idPredio
     * @return
     * @throws Exception
     */
    @Override
    public PPredio buscarPredioCompletoPorIdSinFetch(Long idPredio)
        throws ExcepcionSNC {
        String query = "SELECT p" + " FROM PPredio p" +
            " WHERE p.id = :idPredio";
        Query q = entityManager.createQuery(query);
        q.setParameter("idPredio", idPredio);

        try {
            PPredio p = (PPredio) q.getSingleResult();

            // Inicialización
            Hibernate.initialize(p.getDepartamento());
            Hibernate.initialize(p.getCirculoRegistral());
            Hibernate.initialize(p.getMunicipio());
            Hibernate.initialize(p.getPPersonaPredios());
            Hibernate.initialize(p.getPFichaMatrizs());
            Hibernate.initialize(p.getPPredioZonas());
            Hibernate.initialize(p.getPPredioDireccions());
            Hibernate.initialize(p.getPFotos());
            Hibernate.initialize(p.getPUnidadConstruccions());
            Hibernate.initialize(p.getPPredioAvaluoCatastrals());
            Hibernate.initialize(p.getPPredioServidumbre());
            Hibernate.initialize(p.getPReferenciaCartograficas());
            Hibernate.initialize(p.getTramite());

            if (p.getPUnidadConstruccions() != null && !p.getPUnidadConstruccions().isEmpty()) {
                for (IModeloUnidadConstruccion puc : p
                    .getPUnidadConstruccions()) {
                    Hibernate.initialize(puc.getUsoConstruccion());

                    String tipificacion = puc.getTipificacion();
                    if (tipificacion != null && !"".equals(tipificacion)) {

                        if ("NA".equals(tipificacion)) {
                            puc.setTipificacionValor("NA");
                        } else {
                            String criterio[] = new String[]{"tipo", "=",
                                tipificacion};
                            List<String[]> criterios = new ArrayList<String[]>();
                            criterios.add(criterio);

                            List<TipificacionUnidadConst> tucs = tipificacionUnidadConstDAO
                                .findByCriteria(criterios);
                            if (tucs != null && !tucs.isEmpty()) {
                                Hibernate.initialize(tucs);
                                puc.setTipificacionValor(tucs.get(0)
                                    .getTipificacion());
                            }
                        }
                    }
                }
            }

            if (p.getPPersonaPredios() != null) {
                Hibernate.initialize(p.getPPersonaPredios());

                for (PPersonaPredio ppp : p.getPPersonaPredios()) {
                    Hibernate.initialize(ppp.getPPersona());
                    if (ppp.getPPersona() != null) {
                        Hibernate.initialize(ppp.getPPersona()
                            .getDireccionDepartamento());
                        Hibernate.initialize(ppp.getPPersona()
                            .getDireccionMunicipio());
                        Hibernate.initialize(ppp.getPPersona()
                            .getDireccionPais());
                    }
                }
                p.setPPersonaPredios(this.initializePPersonaPrediosData(p
                    .getPPersonaPredios()));
            }

            return p;

        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            throw new ExcepcionSNC("Error:", ESeveridadExcepcionSNC.ERROR,
                "No se encontró el predio", e);
        }
    }

    /**
     * @see IPPredioDAO#obtenerPorNumeroPredialConTramite(String)
     * @author felipe.cadena
     */
    @Override
    public PPredio obtenerPorNumeroPredialConTramite(String numeroPredial) {

        PPredio resultado = null;

        try {
            String query = "SELECT pp FROM PPredio pp JOIN FETCH pp.tramite t " +
                " WHERE pp.numeroPredial=:numeroPredial";
            Query q = this.entityManager.createQuery(query);
            q.setParameter("numeroPredial", numeroPredial);

            resultado = (PPredio) q.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#obtenerPorNumeroPredialConTramite");
        }

        return resultado;
    }

    /**
     * @author leidy.gonzalez
     * @see IPPredioDAO#buscarPPredioPorIdPredios(Long)
     */
    @Override
    public List<PPredio> buscarPPredioPorIdPredios(List<Long> predioIds) {

        List<PPredio> pPredios = new ArrayList<PPredio>();
        int partSize = 900;
        int len = predioIds.size();
        if (len > partSize) {
            List<PPredio> resultadoTemp = new ArrayList<PPredio>();
            int res = len % partSize;
            int parts = (len / partSize) + Math.min(res, 1);
            for (int i = 0; i < parts; i++) {
                List<Long> idPrediosAux = predioIds.subList(i * partSize, Math.min((i + 1) *
                    partSize, (len)));
                resultadoTemp.addAll(this.buscarPPredioPorIdPredios(idPrediosAux));
            }
            pPredios = resultadoTemp;

        } else {

            String query = "SELECT DISTINCT p" +
                " FROM PPredio p LEFT JOIN FETCH p.PUnidadConstruccions uc" +
                " LEFT JOIN FETCH uc.usoConstruccion" +
                " LEFT JOIN FETCH p.departamento" +
                " LEFT JOIN FETCH p.municipio" +
                " WHERE p.id IN (:predioIds)";

            Query q = entityManager.createQuery(query);
            q.setParameter("predioIds", predioIds);

            try {
                pPredios = q.getResultList();

                for (PPredio pPredio : pPredios) {

                    if (pPredio.getPPredioZonas() != null &&
                        !pPredio.getPPredioZonas().isEmpty()) {
                        Hibernate.initialize(pPredio.getPPredioZonas());
                    }
                    if (pPredio.getPPredioAvaluoCatastrals() != null &&
                        !pPredio.getPPredioAvaluoCatastrals().isEmpty()) {
                        Hibernate.initialize(pPredio.getPPredioAvaluoCatastrals());
                    }

                    if (pPredio.getPFichaMatrizs() != null &&
                        !pPredio.getPFichaMatrizs().isEmpty()) {

                        Hibernate.initialize(pPredio.getPFichaMatrizs());
                    }

                    if (pPredio.getPUnidadConstruccions() != null &&
                        !pPredio.getPUnidadConstruccions().isEmpty()) {

                        for (PUnidadConstruccion pUnidad : pPredio.getPUnidadConstruccions()) {

                            if (pUnidad.getProvienePredio() != null) {
                                Hibernate.initialize(pUnidad.getProvienePredio());
                            }

                            if (pUnidad.getProvieneUnidad() != null) {
                                Hibernate.initialize(pUnidad.getProvieneUnidad());
                            }

                        }
                    }

                    if (pPredio.getPUnidadesConstruccionConvencionalNuevas() != null &&
                        !pPredio.getPUnidadesConstruccionConvencionalNuevas().isEmpty()) {

                        Hibernate.initialize(pPredio.getPUnidadesConstruccionConvencionalNuevas());

                        for (PUnidadConstruccion pUnidad : pPredio.
                            getPUnidadesConstruccionConvencionalNuevas()) {

                            if (pUnidad.getProvienePredio() != null) {
                                Hibernate.initialize(pUnidad.getProvienePredio());
                            }

                            if (pUnidad.getProvieneUnidad() != null) {
                                Hibernate.initialize(pUnidad.getProvieneUnidad());
                            }

                        }
                    }

                    if (pPredio.getPUnidadesConstruccionNoConvencionalNuevas() != null &&
                        !pPredio.getPUnidadesConstruccionNoConvencionalNuevas().isEmpty()) {

                        Hibernate.initialize(pPredio.getPUnidadesConstruccionNoConvencionalNuevas());

                        for (PUnidadConstruccion pUnidad : pPredio.
                            getPUnidadesConstruccionNoConvencionalNuevas()) {

                            if (pUnidad.getProvienePredio() != null) {
                                Hibernate.initialize(pUnidad.getProvienePredio());
                            }

                            if (pUnidad.getProvieneUnidad() != null) {
                                Hibernate.initialize(pUnidad.getProvieneUnidad());
                            }

                        }
                    }
                }

                return pPredios;
            } catch (Exception e) {
                LOGGER.debug("Error: " + e.getMessage());
                return null;
            }

        }

        return pPredios;
    }

    /**
     * @see IPPredioDAO#consultaPorNumeroPredial(List, String, Boolean)
     * @author felipe.cadena
     */
    @Override
    public List<PPredio> consultaPorNumeroPredial(List<String> nPrediales, String tipoTramite, Boolean equal) {

        List<PPredio> resultado = new ArrayList<PPredio>();
        int partSize = 900;
        int len = nPrediales.size();
        if (len > partSize) {
            List<PPredio> resultadoTemp = new ArrayList<PPredio>();
            int res = len % partSize;
            int parts = (len / partSize) + Math.min(res, 1);
            for (int i = 0; i < parts; i++) {
                List<String> matriculasAux = nPrediales.subList(i * partSize, Math.min((i + 1) *
                    partSize, (len)));
                resultadoTemp.addAll(this.consultaPorNumeroPredial(matriculasAux, tipoTramite, equal));
            }
            resultado = resultadoTemp;
        } else {

            String queryString;
            Query query;

            if(tipoTramite!=null){
                queryString = "SELECT p FROM PPredio p" +
                        " INNER JOIN p.tramite t  WHERE p.numeroPredial IN :prediales AND t.tipoTramite";
                if(equal){
                    queryString += " = :tipoTramite";
                } else{
                    queryString += " <> :tipoTramite";
                }
            } else {
                queryString = "SELECT p FROM PPredio p WHERE p.numeroPredial IN :prediales";
            }
            try {
                query = this.entityManager.createQuery(queryString);
                query.setParameter("prediales", nPrediales);

                if(tipoTramite!=null){
                    query.setParameter("tipoTramite", tipoTramite);
                }

                resultado = query.getResultList();

            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "PredioDAOBean#consultaPorNumeroPredial");
            }
        }

        return resultado;

    }

    /**
     * @see IPredioDAO#buscarPprediosCond9PorIdTramite(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPprediosCond9PorIdTramite(Long idTramite) {

        List<PPredio> predios = new ArrayList<PPredio>();
        String query = "SELECT p" +
            " FROM PPredio p " +
            " WHERE p.tramite.id = :idTramite " +
            " AND p.estado = :estado " +
            " AND p.condicionPropiedad = :condicion ";

        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", idTramite);
        q.setParameter("estado", EPredioEstado.ACTIVO.getCodigo());
        q.setParameter("condicion", EPredioCondicionPropiedad.CP_9.getCodigo());

        try {

            predios = q.getResultList();

        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el predio asociado a dicho trámite o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage());
        }
        return predios;
    }

    public List<Documento> buscarDocsDeTramiteDocumentacionsPorSolicitudId(
        Long idSolicitud) {

        List<Documento> result = new ArrayList<Documento>();

        String strQuery = "";
        Query query;

        strQuery = "SELECT DISTINCT doc" +
            " FROM TramiteDocumentacion traDoc," +
            " Tramite tra," +
            " Documento doc" +
            " WHERE " +
            " traDoc.tramite.id = tra.id" +
            " AND traDoc.documentoSoporte.id = doc.id" +
            " AND tra.solicitud.id = :idSolicitud";

        try {
            query = this.entityManager.createQuery(strQuery);
            query.setParameter("idSolicitud", idSolicitud);

            result = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#obtenerUltimoDocSolicitud");
        }

        return result;
    }

    /**
     * @see IPPredioDAO#buscarPPrediosModPorTramiteId(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPPrediosModPorTramiteId(Long tramiteId, Long fichaMatrizId) {

        List<PPredio> pPredios = new ArrayList<PPredio>();

        String query = "SELECT DISTINCT p" +
            " FROM PPredio p," +
            " PFichaMatriz pfm," +
            " PFichaMatrizPredio pfmp" +
            " WHERE p.tramite.id = :tramiteId" +
            " AND pfm.id = :fmId" +
            " and pfm.id = pfmp.PFichaMatriz.id" +
            " AND p.id  != pfm.PPredio.id" +
            " AND p.cancelaInscribe IS NOT NULL" +
            " AND EXISTS  (" +
            " SELECT 1 " +
            " FROM PPredioZona ppz " +
            " WHERE p.id = ppz.PPredio.id " +
            " AND ppz.cancelaInscribe IS NOT NULL )" +
            " AND EXISTS  (" +
            " SELECT 1 " +
            " FROM PPredioAvaluoCatastral pac " +
            " WHERE p.id = pac.PPredio.id " +
            " AND pac.cancelaInscribe IS NOT NULL )";

        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        q.setParameter("fmId", fichaMatrizId);
        try {

            pPredios = q.getResultList();

        } catch (Exception e) {

            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosModPorTramiteId");
        }

        return pPredios;
    }

    /**
     * @see co.gov.igac.snc.dao.conservacion.IPPredioDAO
     * #buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<PPredio> buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(
        Long idTramite) {

        LOGGER.debug("PPredioDAOBean#buscarPPrediosDifFichaMatrizCon0Y5NoCancelados");

        List<PPredio> answer = new ArrayList<PPredio>();
        List<PPredio> predSinFicha = new ArrayList<PPredio>();

        try {
            String sql = "SELECT p FROM PPredio p" +
                " WHERE p.tramite.id = :idTramite " +
                " AND p.condicionPropiedad != :condicion " +
                " AND p.estado != :estado ";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idTramite", idTramite);
            query.setParameter("estado", EPredioEstado.CANCELADO.getCodigo());
            query.setParameter("condicion", EPredioCondicionPropiedad.CP_0.getCodigo());
            query.setParameter("condicion", EPredioCondicionPropiedad.CP_5.getCodigo());

            answer = (List<PPredio>) query.getResultList();

            for (PPredio pPredio : answer) {

                if (!pPredio.isEsPredioFichaMatriz()) {
                    predSinFicha.add(pPredio);
                }

            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDAOBean#buscarPPrediosDifFichaMatrizCon0Y5NoCancelados");
        }
        return predSinFicha;
    }

    /**
     * @author fabio.navarrete
     * @see IPPredioDAO#findPPredioByIdFetchAvaluos(Long)
     */
    @Override
    public List<PPredio> buscarPorMunDeptoTipo(String munCodigo, String deptoCodigo, String tipoTramite) {
        String query = "SELECT p" +
                " FROM PPredio p INNER JOIN p.tramite t" +
                " INNER JOIN  t.departamento d" +
                " INNER JOIN  t.municipio m" +
                " WHERE d.codigo = :deptoCodigo" +
                " AND m.codigo = :munCodigo";
        Query q = entityManager.createQuery(query);
        q.setParameter("deptoCodigo", deptoCodigo);
        q.setParameter("munCodigo", munCodigo);
        try {
            List<PPredio> p = q.getResultList();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

//end of class
}
