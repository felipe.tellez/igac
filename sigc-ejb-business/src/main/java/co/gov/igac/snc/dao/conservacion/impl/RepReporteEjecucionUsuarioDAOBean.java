/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionUsuarioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementacion metodos de acceso de datos de la entidad RepReporteEjecucionUsuario
 *
 * @author felipe.cadena
 */
@Stateless
public class RepReporteEjecucionUsuarioDAOBean extends GenericDAOWithJPA<RepReporteEjecucionUsuario, Long>
    implements IRepReporteEjecucionUsuarioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RepReporteEjecucionUsuarioDAOBean.class);

    @Override
    public List<RepReporteEjecucionUsuario> obtenerPorUsuario(String usuario) {
        List<RepReporteEjecucionUsuario> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT rreu FROM RepReporteEjecucionUsuario rreu " +
            "where rreu.usuarioGenera = :usuario ";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("usuario", usuario);

            answer = query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("RepReporteEjecucionUsuarioDAOBean#obtenerPorUsuario: " +
                "La consulta de subscripciones de ejecucion no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;

    }

    @Override
    public List<RepReporteEjecucionUsuario> obtenerPorUsuarioIdReporte(String usuario,
        Long idReporte) {
        List<RepReporteEjecucionUsuario> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT rreu FROM RepReporteEjecucionUsuario rreu " +
            "where rreu.usuarioGenera = :usuario " +
            "and rreu.reporteEjecucionId = :idReporte ";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("usuario", usuario);
            query.setParameter("idReporte", idReporte);

            answer = query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("RepReporteEjecucionUsuarioDAOBean#obtenerPorUsuarioIdReporte: " +
                "La consulta de subscripciones de ejecucion no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;

    }

}
