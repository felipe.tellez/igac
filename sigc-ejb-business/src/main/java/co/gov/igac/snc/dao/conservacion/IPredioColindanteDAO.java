package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioColindante;

/**
 *
 *
 * @author juan.mendez
 *
 */
@Local
public interface IPredioColindanteDAO extends IGenericJpaDAO<PredioColindante, Long> {

    /**
     * Obtiene los colindantes de un predio a partir de su número predial
     *
     * @author juan.mendez
     * @param numeroPredial
     * @return
     */
    public List<PredioColindante> getPredioColindanteByNumeroPredial(String numeroPredial);

    /**
     * Obtiene los colindantes de un predio a partir de su número predial y el tipo de colindancia
     *
     * @author juan.mendez
     * @param numeroPredial
     * @param tipo
     * @return
     */
    public List<PredioColindante> getPredioColindanteByNumeroPredialYTipo(String numeroPredial,
        String tipo);
}
