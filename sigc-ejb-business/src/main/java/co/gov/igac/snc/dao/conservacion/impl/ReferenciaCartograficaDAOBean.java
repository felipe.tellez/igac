package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IReferenciaCartograficaDAO;
import co.gov.igac.snc.dao.generales.impl.DominioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.ReferenciaCartografica;

@Stateless
public class ReferenciaCartograficaDAOBean extends
    GenericDAOWithJPA<ReferenciaCartografica, Long> implements
    IReferenciaCartograficaDAO {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(ReferenciaCartograficaDAOBean.class);

    /**
     * @see IReferenciaCartograficaDAO#getReferenciaCartograficaByPredio(Predio)
     */
    @SuppressWarnings("unchecked")
    public List<ReferenciaCartografica> getReferenciaCartograficaByPredio(
        Predio predio) {
        LOGGER.debug("getReferenciaCartograficaByPredio");
        Query q = entityManager.createNamedQuery("findReferenciaCartograficaByPredio");
        q.setParameter("predio", predio);
        return q.getResultList();
    }

}
