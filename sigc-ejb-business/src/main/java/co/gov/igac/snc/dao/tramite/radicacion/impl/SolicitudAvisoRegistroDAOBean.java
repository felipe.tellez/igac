/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudAvisoRegistroDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudAvisoRegistro;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class SolicitudAvisoRegistroDAOBean extends GenericDAOWithJPA<SolicitudAvisoRegistro, Long>
    implements ISolicitudAvisoRegistroDAO {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(SolicitudAvisoRegistroDAOBean.class);

}
