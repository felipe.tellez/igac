package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ZonaCierreVigencia;

/**
 * Servicios de persistencia del objeto {@link ZonaCierreVigencia}.
 *
 * @author david.cifuentes
 */
@Local
public interface IZonaCierreVigenciaDAO extends
    IGenericJpaDAO<ZonaCierreVigencia, Long> {

    // ------------------------------------------------ //
    /**
     * Método que elimina una lista de {@link ZonaCierreVigencia}. En caso de que realizar la
     * eliminación adecuadamente retorna true, de lo contrario retorna false.
     *
     * @author david.cifuentes
     *
     * @param Lista de {@link ZonaCierreVigencia} a eliminar
     *
     * @eturn true si eliminaron correctamente, false si hubo alguna inconsistencia
     */
    public boolean eliminarZonasDelMunicipioCierreVigencia(
        List<ZonaCierreVigencia> listaZonas);

}
