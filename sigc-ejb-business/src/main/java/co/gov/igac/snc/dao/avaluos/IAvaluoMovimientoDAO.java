/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoMovimiento
 *
 * @author felipe.cadena
 */
@Local
public interface IAvaluoMovimientoDAO extends IGenericJpaDAO<AvaluoMovimiento, Long> {

    /**
     * Método para calcular los dias que ha estado en suspension un avaluo.
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public Integer calcularDiasSuspensionAvaluo(Long idAvaluo);

    /**
     * Método para calcular el número de dias de ampliación de tiene un avaluo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public Integer calcularDiasAmpliacionAvaluo(Long idAvaluo);

}
