package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte control reporte
 *
 * @author javier.aponte
 */
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepControlReporte;

@Local
public interface IReporteControlReporteDAO extends IGenericJpaDAO<RepControlReporte, Long> {

    /**
     * Método encargado de buscar objetos de reporte control reporte por usuario y por estado de
     * reporte
     *
     * @param usuarioLogin login del usuario
     * @return Retorna la Lista de reportes control reporte asociados a un usuario
     * @author javier.aponte
     */
    public List<RepControlReporte> buscarReporteControlReportePorUsuario(String usuarioLogin);
}
