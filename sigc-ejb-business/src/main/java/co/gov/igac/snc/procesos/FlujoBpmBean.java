/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.contenedores.RutaActividad;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionAvanzarActividad;
import co.gov.igac.snc.apiprocesos.protocolo.rest.RespuestaConteoActividad;
import co.gov.igac.snc.apiprocesos.servicio.excepciones.ManejadorExcepcionesProcesos;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.colecciones.Par;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import static co.gov.igac.snc.fachadas.ProcesosBean.LOGGER;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.procesos.Entidades.BpmActividad;
import co.gov.igac.snc.procesos.Entidades.BpmFlujoTramite;
import co.gov.igac.snc.procesos.Entidades.BpmProcesoInstancia;
import co.gov.igac.snc.procesos.Entidades.BpmRol;
import co.gov.igac.snc.procesos.Entidades.BpmRolActividad;
import co.gov.igac.snc.procesos.Entidades.BpmTipoTramite;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author michael.pena
 */
@Stateless
public class FlujoBpmBean {

    @EJB
    private FlujoBpmDAOBean flujoBpmDaoBean;
    
    public static final Logger LOGGER = LoggerFactory.getLogger(FlujoBpmBean.class);
    
    //==============================PRODUCTOS CATASTRALES ===================================
    //-------------------------------------------------------------------------------------------------
    //METODOS DE PERSISTENCIA
    public SolicitudProductoCatastral crearProcesoProductosCatastrales(SolicitudProductoCatastral solicitudProductoCatastral) {
        
        List<ActividadUsuarios> actividadUsuarios = null;
        SolicitudProductoCatastral resultado = null;
        List<Object[]> bpmProcesoInstancia = new ArrayList<Object[]>();
        List<BpmProcesoInstancia> idProcesoDAO = null;
        Long idTramite = null;
        List<String> listaRoles = null;
        String actividad = null;
        String fechaRadiacion = null;
        String observaciones = null;
        String estadoActividad = null;
        String estadoProceso = null;
        String usuarioPropietario = null;
        String idProceso = null;

        try {
            resultado = new SolicitudProductoCatastral();
          //CONSULTA SI EL TRAMITEID EXISTE EN LOS PROCESOS CREADOS
            bpmProcesoInstancia = flujoBpmDaoBean.bpmProcesoInstanciaSolicitud(String.valueOf(solicitudProductoCatastral.getIdentificador()));

            if (bpmProcesoInstancia.get(0)[1]!= null && !bpmProcesoInstancia.get(0)[1].toString().isEmpty()) {
                LOGGER.info("TRAMITE BMP EXISTENTE CON TRAMITEID " + solicitudProductoCatastral.getIdentificador() + "-- PID =" + bpmProcesoInstancia.get(0)[1].toString());
                resultado.setResultado(bpmProcesoInstancia.get(0)[1].toString());
            } else {            
                
                idTramite = solicitudProductoCatastral.getIdentificador();
                actividadUsuarios = solicitudProductoCatastral.getActividadesUsuarios();
                actividad = actividadUsuarios.get(0).getActividad();
                Calendar cal = solicitudProductoCatastral.getFechaSolicitud();
                fechaRadiacion = fechaFinal(cal);
                observaciones = validaNull(solicitudProductoCatastral.getObservaciones()); 
                
                String codigoTerritorial = null;
                String codigoUOC = null;
                
              if (actividadUsuarios.get(0).getUsuarios().size() > 1) {
                    estadoActividad = "STATE_READY";
                    codigoTerritorial= validaNull(actividadUsuarios.get(0).getUsuarios().get(0).getCodigoTerritorial());
                    codigoUOC = validaNull(actividadUsuarios.get(0).getUsuarios().get(0).getCodigoUOC());
                    usuarioPropietario = null;
                } else {
                    listaRoles = rolesCadenaLista(actividadUsuarios.get(0).getUsuarios().get(0).getRolesCadena());
                    //usuario = (UsuarioDTO) solicitudCatastral.getActividadesUsuarios();            
                    estadoActividad = "STATE_CLAIMED";
                    usuarioPropietario = actividadUsuarios.get(0).getUsuarios().get(0).getLogin();
                    codigoTerritorial= validaNull(actividadUsuarios.get(0).getUsuarios().get(0).getCodigoTerritorial());
                    codigoUOC = validaNull(actividadUsuarios.get(0).getUsuarios().get(0).getCodigoUOC());
                }

                estadoProceso = "STATE_RUNNING";
                LOGGER.info("DATOS A VALIDAR ---- idTramite [" + idTramite + "] actividad [" + actividad + "] fechaRadiacion [" + fechaRadiacion + "]");
                LOGGER.info("DATOS A VALIDAR ---- estadoActividad [" + estadoActividad + "] estadoProceso [" + estadoProceso + "] "
                        + "usuarioPropietario [" + usuarioPropietario + "] observaciones [" + observaciones + "]");
                flujoBpmDaoBean.crearProceoConservacionDAO(String.valueOf(idTramite), actividad, fechaRadiacion, estadoActividad, estadoProceso, usuarioPropietario,
                        "P",codigoTerritorial, codigoUOC, observaciones);
                idProcesoDAO = flujoBpmDaoBean.bpmProcesoInstanciafindByTramite(solicitudProductoCatastral.getIdentificador());

                long max = 0;
                for (int i = 0; i < idProcesoDAO.size(); i++) {

                    if (idProcesoDAO.get(i).getId() > max) {
                        max = idProcesoDAO.get(i).getId();
                    }

                }

                idProceso = String.valueOf(max);
            }
            
            resultado.setResultado(idProceso);       

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM crearProcesoProductosCatastrales " + e);
        }

        return resultado;

    }
    
    
    public SolicitudProductoCatastral avanzarActividadProductos(SolicitudProductoCatastral solicitudCatastral, PeticionAvanzarActividad avanceActividad) {
        SolicitudProductoCatastral resultado = null;
        String idObjeto = null;
        List<BpmFlujoTramite> actividadFlujoTramite = null;
        List<ActividadUsuarios> actividadUsuarios = null;
        String usuarioResponsable = null;
        String usuarioAvanzar = null;
        Date FechaFinal = null;
        String actividad = null;
        String codTerritorial = null;
        String codUoc = null;
        String territorial = null;
        String uoc = null;
        int idEstado = 0;

        try {

            idObjeto = avanceActividad.getIdObjeto();
            actividadFlujoTramite = flujoBpmDaoBean.bpmFlujoTramiteFindById(Long.valueOf(idObjeto));
            usuarioResponsable = actividadFlujoTramite.get(0).getUsuarioResponsable();
            actividad = solicitudCatastral.getActividadesUsuarios().get(0).getActividad();
            String estadoOld = actividadFlujoTramite.get(0).getIdEstado().toString();
            
            if (actividadFlujoTramite.get(0).getIdEstado()!= 5) {

            if (usuarioResponsable != null && !usuarioResponsable.isEmpty()) {
                LOGGER.info("TRAMITE ID " + idObjeto + " TIENE USUARIO " + usuarioResponsable);

            } else {
                LOGGER.info("TRAMITE ID " + idObjeto + " NO TIENE USUARIO RESPONSABLE " + usuarioResponsable);
                String usuarioPotencial = avanceActividad.getUsuario();
                String atributo = "login";
                String login = usuarioJson(usuarioPotencial, atributo);
                boolean reaclamar = flujoBpmDaoBean.reclamarActividadConservacionDAO(Long.valueOf(idObjeto), login);
            }
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
            FechaFinal = new Date();
            String fechaFinal = dateFormat.format(FechaFinal);
            int idEstadoOld = 5;
            flujoBpmDaoBean.updateActividadOldFlujoTramite(fechaFinal, Long.valueOf(idEstadoOld), Long.valueOf(idObjeto));

            if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().size() > 1) {
                idEstado = 2;
                codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                territorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionTerritorial();
                uoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionUOC();
            } else if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().isEmpty()) {
                LOGGER.info("=======================OBJETO RETORNA LISTA DE USUARIOS VACIA ========================================");
                idEstado = 2;
                String usuarioPotencial = avanceActividad.getUsuario();
                String codigoTerritorial = "codigoTerritorial";
                String codigoUOC = "codigoUOC";
                String descripcionTerritorial = "descripcionTerritorial";
                String descripcionUOC = "descripcionUOC";
                codTerritorial = usuarioJson(usuarioPotencial, codigoTerritorial);
                codUoc = usuarioJson(usuarioPotencial, codigoUOC);
                territorial = usuarioJson(usuarioPotencial, descripcionTerritorial);
                uoc = usuarioJson(usuarioPotencial, descripcionUOC);
                
            } else {
                idEstado = 8;
                usuarioAvanzar = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getLogin();
                codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                territorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionTerritorial();
                uoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionUOC();
            }

            if (actividad.equals("TERMINAR")) {
                idEstado = 5;
                int idEstadoProceso = 1003;
                String idIntanciaSnc = actividadFlujoTramite.get(0).getProcesoInstanciaId().toString();
                flujoBpmDaoBean.updateBpmProcesoInstancia(fechaFinal, Long.valueOf(idEstadoProceso), idIntanciaSnc);
            }

            LOGGER.info("CONUSLTA ACTIVIDAD == " + actividad);
            List<BpmActividad> bpmActividadDAO = flujoBpmDaoBean.findByNombreactividad(actividad);
            long valor1 = 13;

            LOGGER.info("CREANDO ACTIVIDAD --------------------- IDFLUJOTRAMUITE === " + actividad + " ID ==== " + bpmActividadDAO.get(0).getId().toString());
            
            if(codTerritorial==null){
                codTerritorial = actividadFlujoTramite.get(0).getCodTerritorial()+"";
            }
            
            if(codUoc==null){
                codUoc = actividadFlujoTramite.get(0).getUoc();
            }
            
            
            flujoBpmDaoBean.crearActividadConservacion(fechaFinal, actividadFlujoTramite.get(0).getProcesoInstanciaId(), Long.valueOf(bpmActividadDAO.get(0).getId().toString()),
                    actividadFlujoTramite.get(0).getTipoTramiteId(), idEstado, usuarioAvanzar, territorial, uoc, codTerritorial, codUoc);

            List<BpmFlujoTramite> idBpmFlujoTramite = flujoBpmDaoBean.maxIdBpmFlujoTramite(actividadFlujoTramite.get(0).getProcesoInstanciaId());

            long max = 0;

            for (int i = 0; i < idBpmFlujoTramite.size(); i++) {

                if (idBpmFlujoTramite.get(i).getId() > max) {
                    max = idBpmFlujoTramite.get(i).getId();
                }

            }

            long id0 = 0;

            LOGGER.info("VALOR MAXIMO ID = " + max);
            resultado = new SolicitudProductoCatastral();
            actividadUsuarios = solicitudCatastral.getActividadesUsuarios();
            resultado.setIdentificador(id0);
            resultado.setIdCorrelacion(id0);
            resultado.setResultado(String.valueOf(max));
            resultado.setOperacion(bpmActividadDAO.get(0).getProceso());
            resultado.setActividadesUsuarios(actividadUsuarios);
            resultado.setTransicion(bpmActividadDAO.get(0).getNombreactividad());
            
            }else{
                
            LOGGER.info("LA ACTIVIDAD ID == " + idObjeto + " YA SE ENCUENTRA AVANZADA");
            resultado = new SolicitudProductoCatastral();          
            
          }   

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM avanzarActividadConservacion : " + e);

        }

        return resultado;
    }
    
    

    //============================== CONSERVACION ===================================
    //-------------------------------------------------------------------------------------------------
    //METODOS DE PERSISTENCIA
    public SolicitudCatastral crearProcesoConservacion(SolicitudCatastral solicitudCatastral) {

        SolicitudCatastral resultado = new SolicitudCatastral();
        List<ActividadUsuarios> actividadUsuarios = null;
        List<BpmProcesoInstancia> bpmProcesoInstancia = null;
        List<BpmProcesoInstancia> idProcesoDAO = null;
        UsuarioDTO usuario = null;
        Long idTramite = null;
        List<String> listaRoles = null;
        String actividad = null;
        String fechaRadiacion = null;
        String observaciones = null;
        String estadoActividad = null;
        String estadoProceso = null;
        String usuarioPropietario = null;
        Long idProceso = null;

        try {
            //CONSULTA SI EL TRAMITEID EXISTE EN LOS PROCESOS CREADOS
            bpmProcesoInstancia = flujoBpmDaoBean.bpmProcesoInstanciafindByTramite(solicitudCatastral.getIdentificador());
            //idTramite = bpmProcesoInstancia.getTramite();            
            LOGGER.info("DATO TRAMITEID = " + bpmProcesoInstancia.size());

            if (bpmProcesoInstancia.size() > 0) {
                LOGGER.info("TRAMITE BMP EXISTENTE CON TRAMITEID " + solicitudCatastral.getIdentificador() + "-- PID =" + bpmProcesoInstancia.get(0).getPiid());
                resultado.setResultado(bpmProcesoInstancia.get(0).getPiid());
            } else {

                idTramite = solicitudCatastral.getIdentificador();
                actividadUsuarios = solicitudCatastral.getActividadesUsuarios();
                actividad = actividadUsuarios.get(0).getActividad();
                Calendar cal = solicitudCatastral.getFechaRadicacion();
                fechaRadiacion = fechaFinal(cal);
                observaciones = validaNull(solicitudCatastral.getObservaciones());

                if (actividadUsuarios.get(0).getUsuarios().size() > 1) {
                    LOGGER.info("CONTIENE MAS DE UN USUARIO POTENCIAL " + actividadUsuarios.get(0).getUsuarios().size());
                    estadoActividad = "STATE_READY";
                    usuarioPropietario = null;
                } else {
                    LOGGER.info("CONTIENE UN SOLO USUARIO " + actividadUsuarios.get(0).getUsuarios().get(0).getLogin());
                    listaRoles = rolesCadenaLista(actividadUsuarios.get(0).getUsuarios().get(0).getRolesCadena());
                    //usuario = (UsuarioDTO) solicitudCatastral.getActividadesUsuarios();            
                    estadoActividad = "STATE_CLAIMED";
                    usuarioPropietario = actividadUsuarios.get(0).getUsuarios().get(0).getLogin();
                }

                estadoProceso = "STATE_RUNNING";
                LOGGER.info("DATOS A VALIDAR ---- idTramite [" + idTramite + "] actividad [" + actividad + "] fechaRadiacion [" + fechaRadiacion + "]");
                LOGGER.info("DATOS A VALIDAR ---- estadoActividad [" + estadoActividad + "] estadoProceso [" + estadoProceso + "] "
                        + "usuarioPropietario [" + usuarioPropietario + "] observaciones [" + observaciones + "]");
                flujoBpmDaoBean.crearProceoConservacionDAO(String.valueOf(idTramite), actividad, fechaRadiacion, estadoActividad, estadoProceso, usuarioPropietario, 
                        "T",null,null,observaciones);
                idProcesoDAO = flujoBpmDaoBean.bpmProcesoInstanciafindByTramite(solicitudCatastral.getIdentificador());
                idProceso = idProcesoDAO.get(0).getId();
            }
            resultado.setResultado(String.valueOf(idProceso));

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM crearProcesoConservacion" + e);
        }

        return resultado;

    }

    public SolicitudCatastral reclamarActividadConservacion(String idActividad, UsuarioDTO usuario) {

        SolicitudCatastral resultado = new SolicitudCatastral();
        List<String> roles = null;
        List<BpmFlujoTramite> actividadFlujoTramite = null;

        try {
            actividadFlujoTramite = flujoBpmDaoBean.bpmFlujoTramiteFindById(Long.valueOf(idActividad));
            if(actividadFlujoTramite.get(0).getIdEstado() == 8){
                
              LOGGER.info("ACTIVIDAD RECLAMADA POR EL USUARIO = " + actividadFlujoTramite.get(0).getUsuarioResponsable() + " ESTADO = " +actividadFlujoTramite.get(0).getIdEstado() );
              resultado.setResultado("EL USUARIO NO TIENE PERMISOS PARA RECLAMAR LA ACTIVIDAD, ACTIVIDAD RECLAMADA");  
                
            } else {
            long idAct = Long.valueOf(idActividad);
            if (usuario.getLogin().equals("snc_tiempos_muertos")) {
                flujoBpmDaoBean.reclamarActividadConservacionDAO(idAct, usuario.getLogin());
            } else {

                roles = rolesCadenaLista(usuario.getRolesCadena());
                boolean validaRol = validaRolActividad(idActividad, roles);

                if (validaRol == false) {

                    LOGGER.info("EL USUARIO " + usuario.getLogin() + "NO TIENE PERMISOS PARA RECLAMAR LA ACTIVIDAD ----- " + validaRol);
                    resultado.setResultado("EL USUARIO NO TIENE PERMISOS PARA RECLAMAR LA ACTIVIDAD, ROL NO VALIDO");

                } else {
                    LOGGER.info("RECLAMANDO ACTIVIDAD CON USUARIO " + usuario.getLogin() + " ----- " + validaRol);
                    flujoBpmDaoBean.reclamarActividadConservacionDAO(idAct, usuario.getLogin());
                }

            }
          }

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM reclamarActividad : " + e);

        }

        return resultado;

    }

    public  SolicitudCatastral avanzarActividadConservacion (SolicitudCatastral solicitudCatastral, PeticionAvanzarActividad avanceActividad) {
        return AvanzarActividad.getInstance().avanzarActividadConservacion(flujoBpmDaoBean, solicitudCatastral, avanceActividad);
    }
    
    public synchronized SolicitudCatastral avanzarActividadConservacion2(SolicitudCatastral solicitudCatastral, PeticionAvanzarActividad avanceActividad) {
        SolicitudCatastral resultado = null;
        String idObjeto = null;
        List<BpmFlujoTramite> actividadFlujoTramite = null;
        List<ActividadUsuarios> actividadUsuarios = null;
        String usuarioResponsable = null;
        String usuarioAvanzar = null;
        Date FechaFinal = null;
        String actividad = null;
        String codTerritorial = null;
        String codUoc = null;
        String territorial = null;
        String uoc = null;
        Long tipoTramite = null;
        int idEstado = 0;

        try {
            idObjeto = avanceActividad.getIdObjeto();
            actividadFlujoTramite = flujoBpmDaoBean.bpmFlujoTramiteFindById(Long.valueOf(idObjeto));            
            
            LOGGER.info("ACTUALIZANDO TAREA OLD --------------------- IDFLUJOTRAMUITE = " + idObjeto);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
            FechaFinal = new Date();
            String fechaFinal = dateFormat.format(FechaFinal);
            int idEstadoOld = 5;
            flujoBpmDaoBean.updateActividadOldFlujoTramite(fechaFinal, Long.valueOf(idEstadoOld), Long.valueOf(idObjeto));          
            
            usuarioResponsable = actividadFlujoTramite.get(0).getUsuarioResponsable();
            actividad = solicitudCatastral.getActividadesUsuarios().get(0).getActividad();
            String estadoOld = actividadFlujoTramite.get(0).getIdEstado().toString();
            
            if (actividadFlujoTramite.get(0).getIdEstado()!= 5) {
                
                LOGGER.info("ESTADO ACTIVIDAD A AVANZAR ============================ " + estadoOld);

            if (usuarioResponsable != null && !usuarioResponsable.isEmpty()) {
                LOGGER.info("TRAMITE ID " + idObjeto + " TIENE USUARIO " + usuarioResponsable);

            } else {
                LOGGER.info("TRAMITE ID " + idObjeto + " NO TIENE USUARIO RESPONSABLE " + usuarioResponsable);
                String usuarioPotencial = avanceActividad.getUsuario();
                String atributo = "login";
                String login = usuarioJson(usuarioPotencial, atributo);
                LOGGER.info("ACTIVIDAD A RECLAMAR CON USUARIO " + login);
                boolean reaclamar = flujoBpmDaoBean.reclamarActividadConservacionDAO(Long.valueOf(idObjeto), login);
                //flujoBpmDaoBean.testUpdate();
                LOGGER.info("ESTADO ACTIVIDAD RECLAMADA " + reaclamar);
                //usuarioAvanzar = login;

            }
            
//            LOGGER.info("ACTUALIZANDO TAREA OLD --------------------- IDFLUJOTRAMUITE = " + idObjeto);
//            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
//            FechaFinal = new Date();
//            String fechaFinal = dateFormat.format(FechaFinal);
//            int idEstadoOld = 5;
//            flujoBpmDaoBean.updateActividadOldFlujoTramite(fechaFinal, Long.valueOf(idEstadoOld), Long.valueOf(idObjeto));

            if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().size() > 1) {
                LOGGER.info("CONTIENE MAS DE UN USUARIO POTENCIAL === " + solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().size());
                idEstado = 2;                
                //codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial() != null 
                        && !solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial().isEmpty()) {
                    codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                    codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                }else {
                    codTerritorial = String.valueOf(actividadFlujoTramite.get(0).getCodTerritorial());
                    codUoc = String.valueOf(actividadFlujoTramite.get(0).getCodUoc());
                }              
                //codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                territorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionTerritorial();
                uoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionUOC();
            } else if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().isEmpty()) {
                LOGGER.info("=======================OBJETO RETORNA LISTA DE USUARIOS VACIA ========================================");
                idEstado = 2;
                String usuarioPotencial = avanceActividad.getUsuario();
                String codigoTerritorial = "codigoTerritorial";
                String codigoUOC = "codigoUOC";
                String descripcionTerritorial = "descripcionTerritorial";
                String descripcionUOC = "descripcionUOC";
                codTerritorial = usuarioJson(usuarioPotencial, codigoTerritorial);
                codUoc = usuarioJson(usuarioPotencial, codigoUOC);
                territorial = usuarioJson(usuarioPotencial, descripcionTerritorial);
                uoc = usuarioJson(usuarioPotencial, descripcionUOC);
            } else {
                idEstado = 8;
                usuarioAvanzar = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getLogin();
                //codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial() != null 
                        && !solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial().isEmpty()) {
                    codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                    codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                }else {
                    codTerritorial = String.valueOf(actividadFlujoTramite.get(0).getCodTerritorial());
                    codUoc = String.valueOf(actividadFlujoTramite.get(0).getCodUoc());
                } 
                //codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                territorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionTerritorial();
                uoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionUOC();
            }

            if (actividad.equals("TERMINAR")) {
                idEstado = 5;
                int idEstadoProceso = 1003;
                String idIntanciaSnc = actividadFlujoTramite.get(0).getProcesoInstanciaId().toString();
                flujoBpmDaoBean.updateBpmProcesoInstancia(fechaFinal, Long.valueOf(idEstadoProceso), idIntanciaSnc);
            }
            
            if (solicitudCatastral.getTipoTramite()!= null && !solicitudCatastral.getTipoTramite().isEmpty()){
                List<BpmTipoTramite> bpmTipoTramite = flujoBpmDaoBean.bpmTipoTramitefindByTramite(solicitudCatastral.getTipoTramite());
                tipoTramite = bpmTipoTramite.get(0).getId();
                
            }else{                
                tipoTramite = actividadFlujoTramite.get(0).getTipoTramiteId();
            }

            LOGGER.info("CONSULTA ACTIVIDAD == " + actividad);
            List<BpmActividad> bpmActividadDAO = flujoBpmDaoBean.findByNombreactividad(actividad);
            long valor1 = 13;

            LOGGER.info("CREANDO ACTIVIDAD --------------------- IDFLUJOTRAMUITE === " + actividad + " ID ==== " + bpmActividadDAO.get(0).getId().toString());

            flujoBpmDaoBean.crearActividadConservacion(fechaFinal, actividadFlujoTramite.get(0).getProcesoInstanciaId(), Long.valueOf(bpmActividadDAO.get(0).getId().toString()),
                    tipoTramite, idEstado, usuarioAvanzar, territorial, uoc, codTerritorial, codUoc);

            List<BpmFlujoTramite> idBpmFlujoTramite = flujoBpmDaoBean.maxIdBpmFlujoTramite(actividadFlujoTramite.get(0).getProcesoInstanciaId());

            long max = 0;

            for (int i = 0; i < idBpmFlujoTramite.size(); i++) {

                if (idBpmFlujoTramite.get(i).getId() > max) {
                    max = idBpmFlujoTramite.get(i).getId();
                }

            }

            long id0 = 0;

            LOGGER.info("VALOR MAXIMO ID = " + max);
            resultado = new SolicitudCatastral();
            actividadUsuarios = solicitudCatastral.getActividadesUsuarios();
            resultado.setIdentificador(id0);
            resultado.setIdCorrelacion(id0);
            resultado.setResultado(String.valueOf(max));
            resultado.setOperacion(bpmActividadDAO.get(0).getProceso());
            resultado.setActividadesUsuarios(actividadUsuarios);
            resultado.setTransicion(bpmActividadDAO.get(0).getNombreactividad());
            
         }else{
                
            LOGGER.info("LA ACTIVIDAD ID == " + idObjeto + " YA SE ENCUENTRA AVANZADA");
            LOGGER.info("ESTADO ACTIVIDAD A AVANZAR (5) ============================ " + estadoOld);
            resultado = new SolicitudCatastral();           
            
          }            

        } catch (Exception e) {
            
            LOGGER.error("ERROR METODO BPM avanzarActividadConservacion : " + e);

        }

        return resultado;

    }
    
    public boolean transferirActividadConservacion(List<String> idsActividades, UsuarioDTO usuario) {
        List<Long> idLong = null;
        try {
            idLong = new ArrayList<Long>();
            String login = usuario.getLogin();
            
            for (String e : idsActividades) {
                idLong.add(Long.valueOf(e));
            }
            
            for (int i = 0; i < idLong.size(); i++){
                
                if (flujoBpmDaoBean.bpmFlujoTramiteFindById(idLong.get(i)).get(0).getIdEstado() != 5){
                    flujoBpmDaoBean.transferirActividadConservacionDao(idLong, login);
                }
                
            }
            
        } catch (Exception e) {
            LOGGER.error("ERROR METODO BPM transferirActividadConservacion " + e);
            return false;
        }
        return true;
    }

    public boolean cancelarProcesoPorIdProceso(String idProceso, String motivo) {

        List<BpmProcesoInstancia> bpmProceso = null;
        Date fechaFin = null;

        try {
            
            fechaFin = new Date();
            java.sql.Date fechaFinal = new java.sql.Date(fechaFin.getTime());
            
            bpmProceso = flujoBpmDaoBean.findByIdInstanciaSnc(idProceso);            
            //List<BpmFlujoTramite> idBpmFlujoTramite = flujoBpmDaoBean.maxIdBpmFlujoTramite(bpmProceso.get(0).getId());          
            Long idProcesoInstancia = bpmProceso.get(0).getId();
            int idEstado = 1006;
            flujoBpmDaoBean.cancelarProcesoPorIdProcesoDao(idProceso, motivo, Long.valueOf(idEstado), idProcesoInstancia, fechaFinal);
            flujoBpmDaoBean.cancelarTramiteId(bpmProceso.get(0).getTramite().toString());

        } catch (Exception e) {
            LOGGER.error("ERROR METODO BPM cancelarProcesoPorIdProceso " + e);
            return false;
        }

        return true;
    }
    
    //-------------------------------------------------------------------------------------------------
    //METODOS DE FILTRADO
    
     public List<Actividad> obtenerActividadesProceso(String idProceso) {

        List<Object[]> listadoActividades = null;
        List<Actividad> resultado = null;

        try {
            
            listadoActividades = flujoBpmDaoBean.obtenerActividadesProcesoDAO(idProceso);
            resultado = construyeActividad(listadoActividades);

            //CONSTRUIR LOGICA DE NEGOCIO 
        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM obtenerActividadesProceso " + e);
        }

        return resultado;

    }
    
    public List<RespuestaConteoActividad> obtenerConteoActividadesUsuario(UsuarioDTO usuario) {

        List<RespuestaConteoActividad> resultado = null;
        List<BpmRolActividad> bpmRolActividad = null;
        List<Long> idRol = null;
        List<Long> idActividad = null;

        try {

            LOGGER.info("ROLES CADENA = " + usuario.getRolesCadena());
            List<String> rolesCadena = rolesCadenaLista(usuario.getRolesCadena());

            List<BpmRol> bpmRol = flujoBpmDaoBean.bpmRolDao(rolesCadena);
            idRol = new ArrayList<Long>();
            for (BpmRol e : bpmRol) {
                idRol.add(e.getId());
            }
            bpmRolActividad = flujoBpmDaoBean.findByListIdRol(idRol);
            idActividad = new ArrayList<Long>();
            for (BpmRolActividad e : bpmRolActividad) {
                LOGGER.info("ID ACTIVIDADES  = " + e.getIdActividad());
                idActividad.add(e.getIdActividad());
            }

            List<Object[]> conteoDao = flujoBpmDaoBean.obtenerConteoActividadesUsuarioDAO(usuario.getLogin(), usuario.getCodigoTerritorial(), usuario.getCodigoUOC(), idActividad);
            resultado = construyeConteoActividad(conteoDao);

        } catch (Exception e) {
            LOGGER.error("ERROR METODO BPM obtenerConteoActividadesUsuario" + e);
        }

        return resultado;

    }

    public List<Actividad> obtenerTareasPorUsuario(UsuarioDTO usuario, String actividad) {

        List<Actividad> resultado = null;
        List<Object[]> listadoActividades = null;
        List<Object[]> rolTarea = null;

        try {

            listadoActividades = flujoBpmDaoBean.obtenerTareasPorUsuarioDAO(usuario.getLogin(), usuario.getCodigoTerritorial(), usuario.getCodigoUOC(), actividad);
            resultado = construyeActividadFront(listadoActividades);

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM obtenerTareasPorUsuario " + e);

        }

        return resultado;

    }

    public List<Actividad> obtenerListaActividadesConFiltro(List<Par> filtros) {

        List<Actividad> actividades = null;
        List<Object[]> listadoActividades = null;
        try {
            FiltroGenerador filtrGenerador = new FiltroGenerador();
            String queryFiltros = filtrGenerador.QueryFiltroTareas(filtros);
            listadoActividades = flujoBpmDaoBean.obtenerListaActividadesConFiltroDAO(queryFiltros);

            actividades = construyeActividad(listadoActividades);

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM obtenerListaActividadesConFiltro " + e);
        }

        return actividades;
    }

    public SolicitudCatastral obtenerObjetoNegocioConservacion(String idProceso, String nombreActividad) {

        SolicitudCatastral resultado = null;
        List<Object[]> listaObjetoNegocio = null;
        List<ActividadUsuarios> actividadesUsuario = new ArrayList();
        List<UsuarioDTO> usuario = new ArrayList();

        ActividadUsuarios actividad = null;
        UsuarioDTO usuarioDTO = null;

        try {
            LOGGER.info("IDPROCESO = " + idProceso);
            LOGGER.info("nombreActividad = " + nombreActividad);

            listaObjetoNegocio = flujoBpmDaoBean.obtenerObjetoNegocioConservacionDAO(idProceso, nombreActividad);

            usuarioDTO = new UsuarioDTO();
            usuario.add(usuarioDTO);
            resultado = new SolicitudCatastral();
            actividad = new ActividadUsuarios(nombreActividad, usuario);
            actividadesUsuario.add(actividad);
            resultado.setIdentificador(Long.valueOf(listaObjetoNegocio.get(0)[14].toString()));
            resultado.setIdCorrelacion(0);
            resultado.setActividadesUsuarios(actividadesUsuario);
            resultado.setOperacion(listaObjetoNegocio.get(0)[6].toString());
            resultado.setTransicion(nombreActividad);
            resultado.setFechaRadicacion(fechaFormato(listaObjetoNegocio.get(0)[21].toString()));
            resultado.setNumeroPredial(listaObjetoNegocio.get(0)[17].toString());
            resultado.setNumeroRadicacion(listaObjetoNegocio.get(0)[16].toString());
            resultado.setNumeroSolicitud(listaObjetoNegocio.get(0)[15].toString());
            resultado.setTipoTramite(listaObjetoNegocio.get(0)[19].toString());

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM obtenerObjetoNegocioConservacion " + e);
        }

        return resultado;

    }

    public List<RespuestaConteoActividad> construyeConteoActividad(List<Object[]> conteoActividades) {
        List<RespuestaConteoActividad> actividades = new ArrayList();
        RespuestaConteoActividad conteo = null;
        int result = 0;
        try {
            for (int i = 0; i < conteoActividades.size(); i++) {

                conteo = new RespuestaConteoActividad();
                conteo.setConteo(result = Integer.parseInt(conteoActividades.get(i)[0].toString()));
                conteo.setRutaActividad(conteoActividades.get(i)[1].toString());
                conteo.setTipoActividad(conteoActividades.get(i)[4].toString());
                conteo.setTipoProcesamiento(conteoActividades.get(i)[3].toString());
                conteo.setTransiciones(construyeArrayList(conteoActividades.get(i)[5].toString()));
                conteo.setUrlCasoDeUso(conteoActividades.get(i)[2].toString());
                actividades.add(conteo);
            }

        } catch (Exception er) {

            LOGGER.error("ERROR CONSTRUYENDO CONTEO ACTIVIDADES: " + er);

        }

        return actividades;
    }
    //-------------------------------------------------------------------------------------------------
    //METODOS DE CONSTRUCCIÓN & LOGICA DE NEGOCIO
    public List<Actividad> construyeActividad(List<Object[]> ListaActividades) {
        List<Actividad> actividades = new ArrayList();
        Actividad actividad = null;
        List<Object[]> rolTarea = null;
        List<String> rolesFinal = null;
        String ultimaActividadDepuracion = null;
        LOGGER.info("NUMERO CONSULTA DAO ACTIVIDADES = " + ListaActividades.size());
        try {
            
            if (ListaActividades.isEmpty() || ListaActividades.equals(null)) {
                LOGGER.info("CONSULTA DAO VACIA PARA FILTRO DE ACTIVIDADES POR USUARIO");
                actividad = new Actividad();
                actividad.setAlerta(0);
                actividad.setNumeroCorrelaciones(0);
                actividades.add(actividad);
            } else {
                for (int i = 0; i < ListaActividades.size(); i++) {
                    String NombreActividad = ListaActividades.get(i)[2].toString();

                    if (NombreActividad.contains("Depuraci")) {
                        String tramite = ListaActividades.get(i)[14].toString();
                        List<Object[]> filtroDepuracion = flujoBpmDaoBean.filtroDepuracion(tramite);
                        ultimaActividadDepuracion = filtroDepuracion.get(0)[9].toString();
                    }
                    String IdObjeto = ListaActividades.get(i)[14].toString();
                    long idObjetoFinal = Long.parseLong(IdObjeto);
                    actividad = new Actividad();
                    actividad.setNumeroCorrelaciones(0);
                    if (ListaActividades.get(i)[0].toString().equals("STATE_CLAIMED")) {
                        actividad.setEstado("Reclamada");
                        actividad.setEstaReclamada(true);
                        actividad.setReclamada(true);
                    } else if (ListaActividades.get(i)[0].toString().equals("STATE_READY")) {
                        actividad.setEstado("Por Reclamar");
                        actividad.setEstaReclamada(false);
                        actividad.setReclamada(false);
                    }else{
                      actividad.setEstado("Finalizada");
                      actividad.setEstaReclamada(true);
                      actividad.setReclamada(true);                       
                    }

                    actividad.setURLCasoUso(ListaActividades.get(i)[1].toString());

                    actividad.setNombre(ListaActividades.get(i)[2].toString());

                    actividad.setPrioridad(5);

                    actividad.setRutaActividad(construyeRutaActividad(ListaActividades , i));

                    actividad.setNumeroSolicitud(ListaActividades.get(i)[15].toString());

                    actividad.setUsuarioOriginador("admin");
                    if (ListaActividades.get(i)[18] == null || ListaActividades.get(i)[18].toString().isEmpty()) {

                        actividad.setUsuarioEjecutor(null);

                    } else {

                        actividad.setUsuarioEjecutor(ListaActividades.get(i)[18].toString());

                    }
                    actividad.setId(ListaActividades.get(i)[11].toString());
                    actividad.setNombreDeDespliegue(ListaActividades.get(i)[8].toString());
                    actividad.setAlerta(0);
                    actividad.setIdCorrelacion(0);
                    actividad.setIdObjetoNegocio(idObjetoFinal);
                    actividad.setTipoProcesamiento(ListaActividades.get(i)[5].toString());
                    actividad.setTransiciones(construyeArrayList(ListaActividades.get(i)[9].toString()));
                    actividad.setTipoTramite(ListaActividades.get(i)[19].toString());
                    actividad.setTipoActividad(ListaActividades.get(i)[4].toString());
                    if (ListaActividades.get(i)[16] == null || ListaActividades.get(i)[16].toString().isEmpty()) {
                        actividad.setNumeroRadicacion(null);
                    } else {
                        actividad.setNumeroRadicacion(ListaActividades.get(i)[16].toString());
                    }
                    if (ListaActividades.get(i)[17] == null || ListaActividades.get(i)[17].toString().isEmpty()) {
                        actividad.setNumeroPredial(null);
                    } else {
                        actividad.setNumeroPredial(validaNull(ListaActividades.get(i)[17].toString()));
                    }
                    rolTarea = flujoBpmDaoBean.rolTarea(ListaActividades.get(i)[20].toString());
                    rolesFinal = construyeListaROL(rolTarea);
                    actividad.setRoles(rolesFinal);
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = format.parse(ListaActividades.get(i)[12].toString());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    actividad.setFechaExpiracion(cal);
                    actividad.setFechaAsignacion(cal);
                    actividad.setObservacion(ultimaActividadDepuracion);
                    actividades.add(actividad);

                }

                LOGGER.info("ACTIVIDAD CONSTRUIDA = " + Arrays.toString(actividades.toArray()));

            }

        } catch (Exception er) {

            LOGGER.info("ERROR CONSTRUYENDO ACTIVIDADES: " + er);

        }

        return actividades;
    }
     //Metodo creado por lentitud de tareas en tramites de depuración.
     public List<Actividad> construyeActividadFront(List<Object[]> ListaActividades) {
        List<Actividad> actividades = new ArrayList();
        Actividad actividad = null;
        List<Object[]> rolTarea = null;
        List<String> rolesFinal = null;
        String ultimaActividadDepuracion = null;
        LOGGER.info("NUMERO CONSULTA DAO ACTIVIDADES = " + ListaActividades.size());
        try {
            
            if (ListaActividades.isEmpty() || ListaActividades.equals(null)) {
                LOGGER.info("CONSULTA DAO VACIA PARA FILTRO DE ACTIVIDADES POR USUARIO");
                actividad = new Actividad();
                actividad.setAlerta(0);
                actividad.setNumeroCorrelaciones(0);
                actividades.add(actividad);
            } else {
                
                rolTarea = flujoBpmDaoBean.rolTarea(ListaActividades.get(0)[20].toString());
                rolesFinal = construyeListaROL(rolTarea);
                                                
                for (int i = 0; i < ListaActividades.size(); i++) {
                    String NombreActividad = ListaActividades.get(i)[2].toString();

                    if (NombreActividad.contains("Depuraci")) {
                        String tramite = ListaActividades.get(i)[14].toString();
                        List<Object[]> filtroDepuracion = flujoBpmDaoBean.filtroDepuracion(tramite);
                        ultimaActividadDepuracion = filtroDepuracion.get(0)[9].toString();
                        LOGGER.info("ULTIMA ACTIVIDAD DEPURACION =  " + ultimaActividadDepuracion);
                    }
                    String IdObjeto = ListaActividades.get(i)[14].toString();
                    long idObjetoFinal = Long.parseLong(IdObjeto);
                    actividad = new Actividad();
                    actividad.setNumeroCorrelaciones(0);
                    if (ListaActividades.get(i)[0].toString().equals("STATE_CLAIMED")) {
                        actividad.setEstado("Reclamada");
                        actividad.setEstaReclamada(true);
                        actividad.setReclamada(true);
                    } else if (ListaActividades.get(i)[0].toString().equals("STATE_READY")) {
                        actividad.setEstado("Por Reclamar");
                        actividad.setEstaReclamada(false);
                        actividad.setReclamada(false);
                    }else{
                      actividad.setEstado("Finalizada");
                      actividad.setEstaReclamada(true);
                      actividad.setReclamada(true);                       
                    }
                    actividad.setURLCasoUso(ListaActividades.get(i)[1].toString());

                    actividad.setNombre(ListaActividades.get(i)[2].toString());

                    actividad.setPrioridad(5);

                    actividad.setRutaActividad(construyeRutaActividad(ListaActividades , i));

                    actividad.setNumeroSolicitud(ListaActividades.get(i)[15].toString());

                    actividad.setUsuarioOriginador("admin");
                    if (ListaActividades.get(i)[18] == null || ListaActividades.get(i)[18].toString().isEmpty()) {

                        actividad.setUsuarioEjecutor(null);

                    } else {

                        actividad.setUsuarioEjecutor(ListaActividades.get(i)[18].toString());

                    }
                    actividad.setId(ListaActividades.get(i)[11].toString());
                    actividad.setNombreDeDespliegue(ListaActividades.get(i)[8].toString());
                    actividad.setAlerta(0);
                    actividad.setIdCorrelacion(0);
                    actividad.setIdObjetoNegocio(idObjetoFinal);
                    actividad.setTipoProcesamiento(ListaActividades.get(i)[5].toString());
                    actividad.setTransiciones(construyeArrayList(ListaActividades.get(i)[9].toString()));
                    actividad.setTipoTramite(ListaActividades.get(i)[19].toString());
                    actividad.setTipoActividad(ListaActividades.get(i)[4].toString());
                    if (ListaActividades.get(i)[16] == null || ListaActividades.get(i)[16].toString().isEmpty()) {
                        actividad.setNumeroRadicacion(null);
                    } else {
                        actividad.setNumeroRadicacion(ListaActividades.get(i)[16].toString());
                    }
                    if (ListaActividades.get(i)[17] == null || ListaActividades.get(i)[17].toString().isEmpty()) {
                        actividad.setNumeroPredial(null);
                    } else {
                        actividad.setNumeroPredial(validaNull(ListaActividades.get(i)[17].toString()));
                    }                    
                    actividad.setRoles(rolesFinal);
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = format.parse(ListaActividades.get(i)[12].toString());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    actividad.setFechaExpiracion(cal);
                    actividad.setFechaAsignacion(cal);
                    actividad.setObservacion(ultimaActividadDepuracion);
                    actividades.add(actividad);

                }

                LOGGER.info("ACTIVIDAD CONSTRUIDA = " + Arrays.toString(actividades.toArray()));

            }

        } catch (Exception er) {

            LOGGER.info("ERROR CONSTRUYENDO ACTIVIDADES construyeActividadFront: " + er);

        }

        return actividades;
    }

    public List<String> construyeListaROL(List<Object[]> roles) {
        ArrayList<String> transiciones = new ArrayList<String>();
        String rol = null;
        String[] listRol = null;
        String rolEnd = null;

        for (int i = 0; i < roles.size(); i++) {

            rol = roles.get(i)[1].toString();

            if (rol.contains("_")) {
                listRol = rol.split("_");
                rolEnd = listRol[0].substring(0, 1) + listRol[0].substring(1).toLowerCase() + "_" + listRol[1].substring(0, 1) + listRol[1].substring(1).toLowerCase();
            } else {
                rolEnd = rol.substring(0, 1) + rol.substring(1).toLowerCase();

            }
            transiciones.add(rolEnd);

        }
        LOGGER.info("ROLES OBTENIDOS = " + Arrays.toString(transiciones.toArray()));

        return transiciones;
    }

    public RutaActividad construyeRutaActividad(List<Object[]> ListaActividades , int i) {
        
        RutaActividad rutaActividad = new RutaActividad();
        rutaActividad.setActividad(ListaActividades.get(i)[8].toString());
        rutaActividad.setMacroproceso(ListaActividades.get(i)[3].toString());
        rutaActividad.setProceso(ListaActividades.get(i)[6].toString());
        rutaActividad.setSeparador(".");
        rutaActividad.setSubproceso(ListaActividades.get(i)[7].toString());
        return rutaActividad;

    }

    public ArrayList<String> construyeArrayList(String cadenaTransicione) {
        ArrayList<String> transiciones = new ArrayList<String>(Arrays.asList(cadenaTransicione.trim().split(";")));
        return transiciones;
    }

    public Calendar fechaFormato(String fecha) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        try {

            Date date = format.parse(fecha);
            cal.setTime(date);

        } catch (Exception e) {

            LOGGER.info("ERROR CONSTRUYENDO FECHA = " + e);

        }
        LOGGER.info("VALORES DATE = " + cal);
        return cal;

    }

    public String fechaFinal(Calendar cal) {

        String Respuesta = null;
        try {

            SimpleDateFormat fecha = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
            Respuesta = fecha.format(cal.getTime());

        } catch (Exception e) {
            LOGGER.error("ERROR METODO BPM fechaFinal : " + e);

        }

        return Respuesta;

    }

    public String validaNull(String valor) {

        String resultado = null;

        try {
            if (valor != null && !valor.isEmpty()) {
                resultado = valor;

            }

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM validaNull " + e);
        }

        return resultado;

    }

    //METODO PARA CONVERTIR A ARRAY STRING ROLES CADENA
    public List<String> rolesCadenaLista(String rolesCadena) {

        List<String> resultado = null;
        String[] roles = null;

        try {

            roles = rolesCadena.split(", ");
            resultado = new ArrayList<String>();
            Collections.addAll(resultado, roles);

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM rolesCadenaLista " + e);
        }

        return resultado;

    }

    public boolean validaRolActividad(String actividad, List<String> listaRoles) {

        boolean resultado = false;
        List<BpmRol> bpmRol = null;
        List<Long> idRol = null;
        List<BpmFlujoTramite> bpmActividadDAO = null;
        List<BpmRolActividad> bpmRolActividad = null;
        Long idActividad = null;
        try {
            bpmActividadDAO = flujoBpmDaoBean.bpmFlujoTramiteFindById(Long.valueOf(actividad));
            idActividad = bpmActividadDAO.get(0).getActividadId();
            bpmRol = flujoBpmDaoBean.bpmRolDao(listaRoles);

            idRol = new ArrayList<Long>();
            for (BpmRol e : bpmRol) {
                idRol.add(e.getId());
            }
            bpmRolActividad = flujoBpmDaoBean.rolActividadDAO(idActividad, idRol);

            if (bpmRolActividad.size() > 0) {
                resultado = true;
            }

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM validaRolActividad " + e);
        }

        return resultado;

    }

    public static String usuarioJson(String usuarioPotencial, String atributo) throws ParseException {

        String Respuesta = null;
        try {

            JSONParser parser = new JSONParser();
            JSONObject jsonUsuario = (JSONObject) parser.parse(usuarioPotencial);

            if (jsonUsuario.get(atributo) == null) {

                Respuesta = null;

            } else {
                Respuesta = jsonUsuario.get(atributo).toString();
            }

        } catch (Exception e) {

            LOGGER.error("ERROR METODO BPM usuarioJson  " + e);

        }

        return Respuesta;

    }
}
