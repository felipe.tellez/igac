/**
 *
 */
package co.gov.igac.snc.dao.sig.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author juan.mendez
 *
 */
public class ArcGisServerResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9107381974573879320L;
    /**
     *
     */

    private ArcGisServerResultParam resultado;

    public ArcGisServerResultParam getResultado() {
        return resultado;
    }

    public void setResultado(ArcGisServerResultParam resultado) {
        this.resultado = resultado;
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
