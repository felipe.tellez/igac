package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;

public interface IPPersonaPredioDAO extends IGenericJpaDAO<PPersonaPredio, Long> {

    /**
     * Método encargado de ubicar un objeto de tipo PPersonaPredio asociado a un Predio identificado
     * mediante su número predial.
     *
     * @author juan.agudelo
     * @param id
     * @return
     */
    public PPersonaPredio buscarPPersonaPredioPorIdFetchPredio(Long id);

    /**
     * Retorna una lista de PPersonaPredio asociada al id del PPredio ingresado
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public List<PPersonaPredio> findByPPredioId(Long idPPredio);

    /**
     * Método encargado de reemplazar los PPersonaPredio asociados a un PPredio
     *
     * @param pPredio
     * @param pPersonaPredios
     * @return
     * @author fabio.navarrete
     */
    public PPredio replacePropietarios(PPredio pPredio,
        List<PPersonaPredio> pPersonaPredios);

    /**
     * Método encargado de consultar los PPersonaPredio asociados a una lista de predios
     *
     * @param idPPredio
     * @return
     * @author leidy.gonzalez
     */
    public List<PPersonaPredio> findByIdPPredios(List<Long> idPPredio);

    /**
     * Método que retorna los PersonaPredios asociados a una lista de predios
     *
     * @author leidy.gonzalez
     * @param predioIds
     * @return
     */
    public List<PPersonaPredio> buscarPPersonasPredioContandoPredios(List<Long> predioIds);

    /**
     * Método que retorna el conteo de cuantos persona predios pueden haber por persona
     *
     * @param personaId
     * @return
     */
    public Long countPersonaPrediosByPersonaId(Long personaId);

    /**
     * Método que retorna los PersonaPredios asociados a un predio con cancela/Inscribe en Inscribe
     *
     * @author leidy.gonzalez
     * @param idPPredio
     * @return
     */
    public List<PPersonaPredio> findByPPredioIdCancelaInscritos(Long idPPredio);
}
