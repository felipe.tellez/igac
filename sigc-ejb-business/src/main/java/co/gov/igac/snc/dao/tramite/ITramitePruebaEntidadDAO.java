package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;

/**
 *
 * @author fabio.navarrete
 *
 */
@Local
public interface ITramitePruebaEntidadDAO extends
    IGenericJpaDAO<TramitePruebaEntidad, Long> {

    /**
     * Metodo que retorna la entidades asignadas a pruebas de un tramite segun el id del tramite
     *
     * @param tramiteId
     * @return
     */
    public List<TramitePruebaEntidad> findTramitePruebaEntidadByTramiteId(Long tramiteId);
}
