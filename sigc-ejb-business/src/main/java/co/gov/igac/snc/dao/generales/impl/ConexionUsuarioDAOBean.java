/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IConexionUsuarioDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.util.EEstadoSesion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementaciôn del DAO para ConexionUsuario
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class ConexionUsuarioDAOBean extends GenericDAOWithJPA<ConexionUsuario, Long> implements
    IConexionUsuarioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConexionUsuarioDAOBean.class);

    /**
     * @see IConexionUsuarioDAO#obtenerNumeroConexionesPorUsuario(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public Long obtenerNumeroConexionesPorUsuario(String usuario) {

        Long resultado = null;
        String queryString = "SELECT COUNT(cu) FROM ConexionUsuario cu WHERE cu.usuario = :usuario";

        try {
            Query query = entityManager.createQuery(queryString);
            query.setParameter("usuario", usuario);

            resultado = (Long) query.getSingleResult();
        } catch (NoResultException e) {
            //N: esto no se debe tratar como error
            LOGGER.info(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("ConexionUsuarioDAOBean#obtenerNumeroConexionesPorUsuario",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }

        return resultado;

    }

    /**
     * @see IConexionUsuarioDAO#obtenerNumeroConexionesPorUsuario(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public ConexionUsuario obtenerPorIdSesion(String idSesion) {

        ConexionUsuario resultado = null;
        String queryString = "SELECT cu FROM ConexionUsuario cu WHERE cu.sesion = :idSesion";

        try {
            Query query = entityManager.createQuery(queryString);
            query.setParameter("idSesion", idSesion);

            resultado = (ConexionUsuario) query.getSingleResult();
        } catch (NoResultException e) {
            //N: esto no se debe tratar como error
            LOGGER.info(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("ConexionUsuarioDAOBean#obtenerPorIdSesion",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }

        return resultado;

    }
}
