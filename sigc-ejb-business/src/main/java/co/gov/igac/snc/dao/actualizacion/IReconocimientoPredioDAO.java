package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ReconocimientoPredio;

/**
 * Servicios de persistencia para el objeto ReconocimientoPredio
 *
 * @author javier.barajas
 */
@Local
public interface IReconocimientoPredioDAO extends IGenericJpaDAO<ReconocimientoPredio, Long> {

    /**
     * Busca las tareas de reconocimiento sobre un predio que pertenecen a una manzana.
     *
     * @param codigoManzana
     * @return
     * @author andres.eslava
     */
    public List<ReconocimientoPredio> obtenerReconocimietosPrediosPorManzana(String codigoManzana);

}
