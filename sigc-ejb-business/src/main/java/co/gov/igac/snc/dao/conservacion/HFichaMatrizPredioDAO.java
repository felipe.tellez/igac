package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatrizPredio;

/**
 * Servicios de persistencia del objeto {@link HFichaMatrizPredio}.
 *
 * @author david.cifuentes
 */
@Local
public interface HFichaMatrizPredioDAO extends IGenericJpaDAO<HFichaMatrizPredio, Long> {

    /**
     * Método para consultar los {@link HFichaMatrizPredio} por su estado y el id de la
     * {@link FichaMatriz}
     *
     * @author david.cifuentes
     * @param estado
     * @param fichaMatrizId
     * @return
     */
    public List<HFichaMatrizPredio> buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(
        String estado, Long fichaMatrizId);

}
