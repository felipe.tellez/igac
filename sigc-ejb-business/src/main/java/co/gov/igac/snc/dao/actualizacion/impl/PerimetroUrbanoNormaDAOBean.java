package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IPerimetroUrbanoNormaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.PerimetroUrbanoNorma;

@Stateless
public class PerimetroUrbanoNormaDAOBean extends GenericDAOWithJPA<PerimetroUrbanoNorma, Long>
    implements IPerimetroUrbanoNormaDAO {

}
