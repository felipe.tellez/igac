package co.gov.igac.snc.dao.avaluos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoAsignacionProfesional
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IAvaluoAsignacionProfesionalDAO extends
    IGenericJpaDAO<AvaluoAsignacionProfesional, Long> {

    /**
     * consulta las {@link AvaluoAsignacionProfesional} que tengan orden de practica y que estén
     * asociadas a un número de radicado de una solicitud. Carga los objetos asociados al avalúo: - {@link ProfesionalAvaluo}
     * - {@link Avaluo} Con sus {@link AvaluoPredio}
     * - {@link OrdenPractica}
     *
     * @author christian.rodriguez
     * @param radicado String con el número de radicado de una solicitud
     * @return Lista con las {@link AvaluoAsignacionProfesional} si se encontraron coincidencias,
     * null en otro caso
     */
    public List<AvaluoAsignacionProfesional> consultarPorSolicitudConOrdenPractica(
        String radicado);

    /**
     * Consulta las {@link AvaluoAsignacionProfesional} que esten asociadas a un
     * {@link ProfesionalAvaluo}
     *
     * @author christian.rodriguez
     * @param idProfesional id del {@link ProfesionalAvaluo}
     * @return lista con las asignaciones del profesional
     */
    public List<AvaluoAsignacionProfesional> consultarPorProfesionalId(
        Long idProfesional);

    /**
     * Actualiza los registros de la tabla cambiando el valor del id de la orden de práctica.
     *
     * @author pedro.garcia
     *
     * @param numeroOrdenPractica número de la orden de práctica
     * @param avaluosOP lista de los id de los Avaluo que hacen parte de la orden de práctica
     */
    public void actualizarOrdenPractica(String numeroOrdenPractica, List<Long> avaluosOP);

    /**
     * Consulta las {@link AvaluoAsignacionProfesional} asociadas a una {@link OrdenPractica}
     * especificada
     *
     * @author christian.rodriguez
     * @param ordenPracticaId identificador de la {@link OrdenPractica}
     * @return Lista con las {@link AvaluoAsignacionProfesional} encontradas, o null si no se
     * obtuvieron resultados
     */
    public List<AvaluoAsignacionProfesional> consultarPorOrdenPracticaId(Long ordenPracticaId);

    /**
     * retorna true si existe alguna asignación del profesional X, para la actividad Y, para alguno
     * de los avalúos.
     *
     * @author pedro.garcia
     *
     * @param profesionalAvaluosId
     * @param codActividad
     * @param idsAvaluos
     * @return
     */
    public boolean existeAsignacionDeProfesional(long profesionalAvaluosId,
        String codActividad, long[] idsAvaluos);

    /**
     * Busca las asignaciones del profesional X, para la actividad Y, para alguno de los avalúos.
     *
     * @author pedro.garcia
     *
     * @param profesionalAvaluosId
     * @param codActividad
     * @param idsAvaluos
     * @return
     */
    public List<AvaluoAsignacionProfesional> buscarPorProfesionalYActividadYAvaluos(
        long profesionalAvaluosId, String codActividad, long[] idsAvaluos);

    /**
     * Obtiene los registros que correspondan a la última asignación que se haya hecho para la
     * actividad X del avaluo Y.
     *
     * Se hace fetch del ProfesionalAvaluos
     *
     * @author pedro.garcia
     *
     * @param codActividad
     * @param idAvaluo
     * @return
     */
    public List<AvaluoAsignacionProfesional> obtenerDeUltimaAsignacion(
        String codActividad, Long idAvaluo);

    /**
     * borra las entradas de la tabla que correspondan al avalúo para la actividad y que estén
     * vigentes no, dependiendo del parámetro 'vigentes'. Si 'codActividad' es nulo o vacío, no se
     * incluye ese criterio en la selección.
     *
     * @author pedro.garcia
     * @param idAvaluo
     * @param codActividad
     * @param vigentes
     * @return
     */
    public boolean borrarDeAvaluo(Long idAvaluo, String codActividad, boolean vigentes);

}
