package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionOrdenAjuste;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;

@Local
public interface IActualizacionOrdenAjusteDAO extends IGenericJpaDAO<ActualizacionOrdenAjuste, Long> {

    /**
     * Obtiene las ordenes de ajuste de acuerdo al responsable SIG Asignado.
     *
     * @param responsableSig responsable Sig de interes
     * @return
     * @author andres.eslava
     */
    public List<ActualizacionOrdenAjuste> getOrdenesAjustePorResponsableSIG(
        RecursoHumano responsableSig);

}
