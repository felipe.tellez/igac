package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPersonaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;

@Stateless
public class PPersonaDAOBean extends GenericDAOWithJPA<PPersona, Long>
    implements IPPersonaDAO {

}
