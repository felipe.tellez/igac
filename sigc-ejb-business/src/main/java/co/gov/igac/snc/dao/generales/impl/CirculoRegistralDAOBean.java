/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ICirculoRegistralDAO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class CirculoRegistralDAOBean extends GenericDAOWithJPA<CirculoRegistral, String>
    implements ICirculoRegistralDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(CirculoRegistralDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see ICirculoRegistralDAO#findByMunicipio(java.lang.String)
     *
     * N: recordar que jpql requiere que el tipo de dato que se pasa al setParameter sea el
     * correspondiente al del entity (ej: id de CirculoRegistral Long, id Municipio String)
     */
    @Implement
    @Override
    public List<CirculoRegistral> findByMunicipio(String idMunicipio) {

        LOGGER.debug("on CirculoRegistralDAOBean#findByMunicipio...");
        List<CirculoRegistral> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT cr from CirculoRegistral cr JOIN cr.circuloRegistralMunicipios crm " +
            " WHERE crm.municipio.id = :idMuni";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("idMuni", idMunicipio);

        try {
            answer = query.getResultList();
            LOGGER.debug("... ok.");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "CirculoRegistralDAOBean#findByMunicipio");
        }

        return answer;
    }

    /**
     * @see ICirculoRegistralDAO#findByCodigo(String)
     * @author christian.rodriguez
     */
    public CirculoRegistral findByCodigo(String codigo) {

        LOGGER.debug("on CirculoRegistralDAOBean#findByCodigo...");
        CirculoRegistral answer = null;
        String queryString;
        Query query;

        queryString = " SELECT cr from CirculoRegistral cr " + " WHERE cr.codigo = :codigo";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("codigo", codigo);

        try {
            answer = (CirculoRegistral) query.getSingleResult();
            LOGGER.debug("... ok.");
        } catch (Exception e) {
            LOGGER.error("... error. " + e.getMessage());
        }

        return answer;
    }

    /**
     * @see ICirculoRegistralDAO#findByDepartamento(java.lang.String)
     *
     */
    public List<CirculoRegistral> findByDepartamento(String idDepartamento) {

        LOGGER.debug("on CirculoRegistralDAOBean#findByDepartamento...");
        List<CirculoRegistral> answer = null;
        String queryString;
        Query query;

        queryString =
            "SELECT DISTINCT cr from CirculoRegistral cr JOIN cr.circuloRegistralMunicipios crm " +
            " WHERE crm.municipio.departamento.id = :idDepto";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("idDepto", idDepartamento);

        try {
            answer = query.getResultList();
            LOGGER.debug("... ok.");
        } catch (Exception e) {
            LOGGER.error("... error. " + e.getMessage());
        }
        return answer;
    }

}
