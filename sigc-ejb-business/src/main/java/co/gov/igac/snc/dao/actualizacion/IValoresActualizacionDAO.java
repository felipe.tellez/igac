/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.dao.actualizacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ValoresActualizacion;
import java.util.List;

/**
 *
 * @author felipe.cadena
 */
public interface IValoresActualizacionDAO extends IGenericJpaDAO<ValoresActualizacion, Long> {

    /**
     * Obtiene los registros asociados al municipio y al departamento del parametro
     *
     * @author felipe.cadena
     *
     * @param municipioCodigo
     * @param deptoCodigo
     * @return
     */
    public List<ValoresActualizacion> obtenerPorMunicipioYDepartamento(String municipioCodigo,
        String deptoCodigo, String vigencia);

}
