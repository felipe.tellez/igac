package co.gov.igac.snc.dao.util;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.Calendar;
import java.util.Date;

/**
 * Extrae los datos del usuario desde un LDAP
 *
 * @author juan.mendez
 *
 */
public class LdapSecurityUtil {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(LdapSecurityUtil.class);

    /**
     * Obtiene los detalles del usuario
     *
     * @param authentication
     * @return
     */
    /*
     * public static UsuarioDTO getUserDetails(Authentication authentication) { SNCUserDetails
     * userDetails = (SNCUserDetails) authentication.getPrincipal(); UsuarioDTO usuarioDto = new
     * UsuarioDTO(); usuarioDto.setLogin(authentication.getName());
     *
     * String codigos[] = getTerritorial(userDetails.getDn());
     * usuarioDto.setDescripcionTerritorial(codigos[0]); usuarioDto.setDescripcionUOC(codigos[1]);
     *
     * usuarioDto.setPrimerNombre(getPrimerNombre(userDetails.getDn()));
     *
     * String[] rolesArr = new String[LdapSecurityUtil.getRoles(authentication).length]; int i = 0;
     * for (String rolStr : LdapSecurityUtil.getRoles(authentication)) { rolesArr[i] =
     * rolStr.toUpperCase(); i++; }
     *
     * usuarioDto.setRoles(rolesArr); usuarioDto.setContratista(userDetails.isContratista()); return
     * usuarioDto; }
     */
    /**
     *
     * @param attrs
     * @return
     */
    public static UsuarioDTO getUserDetails(Attributes attrs) {
        //LOGGER.debug("getUserDetails" );
        UsuarioDTO usuarioDto = null;
        try {
            String dn = attrs.get("distinguishedName").get(0).toString();
            String lastKnownParent = dn.substring(dn.indexOf(",OU=") + 1);

            String codigos[] = getTerritorial(lastKnownParent);

            //LOGGER.debug("lastKnownParent: " + lastKnownParent);
            //LOGGER.debug("territorial: " + territorial);
            usuarioDto = new UsuarioDTO();
            usuarioDto.setLogin(attrs.get("sAMAccountName").get(0).toString());

            usuarioDto.setDescripcionTerritorial(codigos[0]);
            usuarioDto.setDescripcionUOC(codigos[1]);
            if (attrs.get("mail") != null) {
                usuarioDto.setEmail(attrs.get("mail").get(0).toString());
            }

            String nombres;
            if (attrs.get("givenName") != null && attrs.get("givenName").get(0) != null) {
                nombres = attrs.get("givenName").get(0).toString();
            } else {
                nombres = "nombres_no_definidos";
            }
            LOGGER.debug("nombres: " + nombres);

            String primerNombre = null;
            String segundoNombre = null;
            if (nombres.indexOf(" ") > 0) {
                primerNombre = nombres.substring(0, nombres.indexOf(" ")).trim();
                segundoNombre = nombres.substring(nombres.indexOf(" ")).trim();
            } else {
                primerNombre = nombres.trim();
            }
            usuarioDto.setPrimerNombre(primerNombre);
            usuarioDto.setSegundoNombre(segundoNombre);

            String apellidos;
            if (attrs.get("sn") != null && attrs.get("sn").get(0) != null) {
                apellidos = attrs.get("sn").get(0).toString();
            } else {
                apellidos = "apellidos_no_definidos";
            }
            LOGGER.debug("apellidos: " + apellidos);

            String primerApellido = null;
            String segundoApellido = null;

            if (apellidos.indexOf(" ") > 0) {
                primerApellido = apellidos.substring(0, apellidos.indexOf(" ")).trim();
                segundoApellido = apellidos.substring(apellidos.indexOf(" ")).trim();
            } else {
                primerApellido = apellidos;
            }
            usuarioDto.setPrimerApellido(primerApellido);
            usuarioDto.setSegundoApellido(segundoApellido);

            if (attrs.get("employeeId") != null && attrs.get("employeeId").get(0) != null) {
                usuarioDto.setIdentificacion(attrs.get("employeeId").get(0).toString());
            }

            if (attrs.get("employeeType") != null && attrs.get("employeeType").get(0) != null) {
                usuarioDto.setTipoIdentificacion(attrs.get("employeeType").get(0).toString());
            }

            //felipe.cadena::#15864::29-02-2016::Se incluyen atributos para 
            //determinar si los usuarios son validado para asignarles tramites
            if (attrs.get("accountExpires") != null && attrs.get("accountExpires").get(0) != null) {

                Long zeroDate = new Long(0l);
                Calendar actualLdapDate = Calendar.getInstance();

                if (zeroDate.equals(Long.valueOf(attrs.get("accountExpires").get(0).toString()))) {
                    //la fecha de expiracion en cero implica que el usuario no expira
                    actualLdapDate.add(Calendar.YEAR, 2000);
                } else {
                    Long actualLdapTime = Long.
                        valueOf(attrs.get("accountExpires").get(0).toString()) / 10000;
                    actualLdapDate.setTimeInMillis(actualLdapTime);
                    actualLdapDate.add(Calendar.YEAR, -369); // La base del sistema de ldap tiene un desfase de 369 años con la base de java
                }
                usuarioDto.setFechaExpiracion(actualLdapDate.getTime());

            }
            if (attrs.get("userAccountControl") != null && attrs.get("userAccountControl").get(0) !=
                null) {
                usuarioDto.setEstado(attrs.get("userAccountControl").get(0).toString());
            }

            ///////////////////////////////////////////////////////////////////////////////
            Attribute memberOf = attrs.get("memberOf");
//			if(memberOf == null){
//				String message = "El usuario "+usuarioDto.getLogin()+" no tiene roles asociados en el sistema";
//				throw SncBusinessServiceExceptions.EXCEPCION_100004.getExcepcion(LOGGER, null, message);
//			}else{
//				@SuppressWarnings("rawtypes")
//				NamingEnumeration  rol  = memberOf.getAll();
//				
//				List<String> rolesList = new ArrayList<String>();
//				while (rol.hasMoreElements()) {
//					String rolStr = LdapSecurityUtil.extractRol(rol.next().toString());
//					rolesList.add(rolStr);
//				}
//				String[] rolesArr = new String[rolesList.size()];
//				int i = 0;
//				for (String rolStr : rolesList) {
//					rolesArr[i] = rolStr.toUpperCase();
//					i++;
//				}
//				usuarioDto.setRoles(rolesArr);
//			}
            if (memberOf != null) {
                @SuppressWarnings("rawtypes")
                NamingEnumeration rol = memberOf.getAll();

                List<String> rolesList = new ArrayList<String>();
                while (rol.hasMoreElements()) {
                    String rolStr = LdapSecurityUtil.extractRol(rol.next().toString());
                    rolesList.add(rolStr);
                }
                String[] rolesArr = new String[rolesList.size()];
                int i = 0;
                for (String rolStr : rolesList) {
                    rolesArr[i] = rolStr.toUpperCase();
                    i++;
                }
                usuarioDto.setRoles(rolesArr);
            }

            ///////////////////////////////////////////////////////////////////////////////
            usuarioDto.setContratista(LdapUtil.extraerContratista(attrs));
        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100004.getExcepcion(LOGGER, e, e.
                getMessage());
        }

        return usuarioDto;
    }

    /**
     * Obtiene los roles del usuario en el sistema
     *
     * @param authentication
     * @return
     */
    /*
     * private static String[] getRoles(Authentication authentication) { String roles[] = new
     * String[authentication.getAuthorities().size()]; int rol = 0; if (roles.length > 0) {
     * Collection<? extends GrantedAuthority> c = authentication.getAuthorities(); for
     * (GrantedAuthority ga : c) { String authority = ga.getAuthority(); String rolStr =
     * extractRol(authority); roles[rol++] = rolStr.toUpperCase(); } } return roles; }
     */
    /**
     *
     * @param authority
     * @return
     */
    private static String extractRol(String authority) {
        return authority.substring(authority.indexOf('=') + 1, authority.indexOf(','));
    }

    /**
     *
     * @modified felipe.cadena::06-12-2016::Se identifica si el usuario pertenece a una entidad
     * delegada
     * @param authentication
     * @return
     */
    private static String[] getTerritorial(String dn) {
        String resultado[] = new String[2];
        // Primer indice territorial
        // segundo indice uoc, si aplica
        if (dn != null) {

            //verificar si el usuario es delegado
            if (dn.contains(Constantes.DN_LDAP_DELEGADOS)) {
                int extIndex = dn.indexOf(Constantes.DN_LDAP_DELEGADOS);
                int primerOU = dn.indexOf("OU=");
                resultado[0] = dn.substring(primerOU + 3, extIndex - 1);
                return resultado;
            }

            dn = dn.toUpperCase();
            int posTerritoriales = dn.indexOf("OU=TERRITORIALES");
            if (posTerritoriales == -1) {
                resultado[0] = "SEDE_CENTRAL";
            } else {
                int primerOU = dn.indexOf("OU=");
                int segundoOU = dn.indexOf("OU=", primerOU + 1);
                if (posTerritoriales == segundoOU) {
                    //Es una territorial
                    resultado[0] = dn.substring(primerOU + 3, segundoOU - 1);
                } else {
                    // Es una UOC
                    resultado[1] = dn.substring(primerOU + 3, segundoOU - 1);
                    resultado[0] = dn.substring(segundoOU + 3, posTerritoriales - 1);
                }
            }
        }
        return resultado;
    }

//end of class
}
