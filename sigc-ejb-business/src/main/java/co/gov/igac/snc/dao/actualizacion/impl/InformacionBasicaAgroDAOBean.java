package co.gov.igac.snc.dao.actualizacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IInformacionBasicaAgroDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.InformacionBasicaAgro;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session Bean implementation class InformacionBasicaAgroDAOBean
 */
@Stateless
public class InformacionBasicaAgroDAOBean extends GenericDAOWithJPA<InformacionBasicaAgro, Long>
    implements IInformacionBasicaAgroDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(InformacionBasicaAgroDAOBean.class);

    /**
     * Default constructor.
     */
    public InformacionBasicaAgroDAOBean() {
    }

    /**
     * {@link #crear(InformacionBasicaAgro)}
     */
    @Override
    public InformacionBasicaAgro crear(
        InformacionBasicaAgro informacionBasicaAgrologica)
        throws ExcepcionSNC {

        LOGGER.debug("inicio de creación");
        try {
            entityManager.persist(informacionBasicaAgrologica);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("JAAM::Actualización", ESeveridadExcepcionSNC.ERROR, e.
                getMessage(), null);
        }
        LOGGER.debug("fin de creación");
        return informacionBasicaAgrologica;
    }
}
