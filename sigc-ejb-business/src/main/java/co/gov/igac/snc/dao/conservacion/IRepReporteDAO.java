package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte control reporte
 *
 * @author javier.aponte
 */
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;

@Local
public interface IRepReporteDAO extends IGenericJpaDAO<RepReporte, Long> {

    /**
     * Método encargado de buscar en la base de datos los tipos de reporte asociados a la categoria
     *
     * @param categoria del reporte
     * @return lista de tipos de reportes asociados a la categoria
     * @author leidy.gonzalez
     */
    public List<RepReporte> obtenerListaTipoReportePorCategoria(String categoria);

    public RepReporte buscarReportePorId(Long idReporteConsultar);

    /**
     * Método encargado de buscar en la base de datos los nombres de los tipos de reporte prediales
     *
     * @param categoria del reporte
     * @return lista de tipos de reportes prediales
     * @author leidy.gonzalez
     */
    public List<String> obtenerListaNombreTipoReporte();

    /**
     * Método encargado de buscar en la base de datos los tipos de reporte prediales
     *
     * @param categoria del reporte
     * @return lista de tipos de reportes prediales
     * @author leidy.gonzalez
     */
    public List<RepReporte> consultarListaTipoReportePorNombre(List<String> nombresTiposReportes);

    /**
     * Método encargado de buscar en la base de datos los tipos de reporte asociados a la
     * tipoReporte
     *
     * @param categoria del reporte
     * @return lista de tipos de reportes asociados a la tipoReporte
     * @author leidy.gonzalez
     */
    public List<RepReporte> obtenerListaTipoReportePorTipoReporte(String tipoReporte);
}
