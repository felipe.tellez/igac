package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPersonaPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class PersonaPredioDAOBean extends
    GenericDAOWithJPA<PersonaPredio, Long> implements IPersonaPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PersonaPredioDAOBean.class);

    /**
     * @see IPersonaPredioDAO#getPersonaPredioById(Long)
     * @author fabio.navarrete
     */
    public PersonaPredio getPersonaPredioById(Long idPersonaPredio) {
        try {
            Query query = this.entityManager
                .createQuery("SELECT pp FROM PersonaPredio pp WHERE pp.id = :idPersonaPredio");
            query.setParameter("idPersonaPredio", idPersonaPredio);
            PersonaPredio result = (PersonaPredio) query.getSingleResult();
            return result;
        } catch (NoResultException nrEx) {
            LOGGER.error("La consulta no devolvio PersonaPredio");
            return null;
        }
    }

    /**
     * @see IPersonaPredioDAO#getPersonaPredioFetchPropiedadsById(Long)
     * @author fabio.navarrete
     */
    public PersonaPredio getPersonaPredioFetchPropiedadsById(
        Long idPersonaPredio) {
        try {
            Query query = this.entityManager
                .createQuery("SELECT pp FROM PersonaPredio pp WHERE pp.id = :idPersonaPredio");
            query.setParameter("idPersonaPredio", idPersonaPredio);
            PersonaPredio result = (PersonaPredio) query.getSingleResult();
            result.getPersonaPredioPropiedads().size();
            return result;
        } catch (NoResultException nrEx) {
            LOGGER.error("La consulta no devolvio PersonaPredio");
            return null;
        }
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IPersonaPredioDAO#getPersonaPrediosByPredioId(Long)
     */
    @Override
    public List<PersonaPredio> getPersonaPrediosByPredioId(Long predioId) {

        List<PersonaPredio> answer = null;

        String query = "SELECT pp FROM PersonaPredio pp " +
            " JOIN pp.predio p " +
            " WHERE p.id = :predioId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("predioId", predioId);

            answer = (List<PersonaPredio>) q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PersonaPredioDAOBean#getPersonaPrediosByPredioId");
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author.franz.gamba
     * @see IPersonaPredioDAO#countPersonaPrediosByPersonaId(Long)
     */
    @Override
    public Long countPersonaPrediosByPersonaId(Long personaId) {
        Long answer = null;

        String query = "SELECT COUNT (pp) FROM PersonaPredio pp " +
            " WHERE pp.persona.id = :personaId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("personaId", personaId);

            answer = (Long) q.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PersonaPredioDAOBean#countPersonaPrediosByPersonaId");
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IPersonaPredioDAO#getPersonaPrediosByPersonaId(Long)
     */
    @Override
    public List<PersonaPredio> getPersonaPrediosByPersonaId(Long personaId) {

        List<PersonaPredio> answer = null;

        String query = "SELECT pp FROM PersonaPredio pp " +
            " JOIN pp.predio p " +
            " JOIN pp.persona per " +
            " WHERE per.id = :personaId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("personaId", personaId);

            answer = (List<PersonaPredio>) q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PersonaPredioDAOBean#getPersonaPrediosByPersonaId");
        }
        return answer;
    }

}
