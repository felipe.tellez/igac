package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoRequisitoDocumentacionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoRequisitoDocumentacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class AvaluoRequisitoDocumentacionDAOBean extends
    GenericDAOWithJPA<AvaluoRequisitoDocumentacion, Long> implements
    IAvaluoRequisitoDocumentacionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoRequisitoDocumentacionDAOBean.class);

    /**
     * @see IAvaluoRequisitoDocumentacionDAO#obtenerDocsRequisitoPorAvaluoId(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<AvaluoRequisitoDocumentacion> obtenerDocsRequisitoPorAvaluoId(
        Long avaluoId) {

        List<AvaluoRequisitoDocumentacion> result = new ArrayList<AvaluoRequisitoDocumentacion>();
        String queryString;
        Query query;

        queryString = "SELECT ard" +
            " FROM AvaluoRequisitoDocumentacion ard" +
            " WHERE ard.avaluoId = :avaluoId";

        /*
         * Se ejecuta el query para buscar los docs requisito de la proyeccion de cotizacion del
         * avaluo
         */
        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("avaluoId", avaluoId);

            result = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex,
                "AvaluoCapituloDAOBean#obtenerCapitulosPorAvaluoId");
        }

        return result;
    }
}
