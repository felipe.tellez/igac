package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDAO;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Producto;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.util.EProductoCatastralEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;

/**
 * Implementación de los servicios de persistencia del objeto ProductoCatastral.
 *
 * @author javier.barajas
 */
@Stateless
public class ProductoCatastralDAOBean extends GenericDAOWithJPA<ProductoCatastral, Long> implements
    IProductoCatastralDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProductoCatastralDAOBean.class);

    /**
     * @see IProductoCatastralDAO#guardarYactualizarProductosCatastrales(List<ProductoCatastral>)
     * @author javier.barajas
     */
    @Override
    public List<ProductoCatastral> guardarYactualizarProductosCatastrales(
        List<ProductoCatastral> listaProductos) {
        List<ProductoCatastral> result = new ArrayList<ProductoCatastral>();

        try {

            for (ProductoCatastral pc : listaProductos) {
                ProductoCatastral prodcatas = new ProductoCatastral();
                prodcatas = update(pc);
                result.add(prodcatas);
            }

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
        }
        return result;
    }

    /**
     * @see IProductoCatastralDAO#consultarProductosCatastralesporId(Long)
     * @author javier.barajas
     */
    @Override
    public ProductoCatastral consultarProductosCatastralesporId(
        Long productoCatastralId) {
        ProductoCatastral result = new ProductoCatastral();
        Query query = null;

        String queryToExecute = "SELECT pc FROM ProductoCatastral pc " +
            " WHERE pc.id = :productoCatastralId ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("productoCatastralId", productoCatastralId);

        try {
            result = (ProductoCatastral) query.getSingleResult();

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
        }

        return result;
    }

    /**
     * @see IProductoCatastralDAO#guardarYactualizarProductoCatastral(ProductoCatastral)
     * @author javier.barajas
     */
    @Override
    public ProductoCatastral guardarYactualizarProductoCatastral(
        ProductoCatastral producto) {
        ProductoCatastral result = new ProductoCatastral();

        try {
            result = update(producto);

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
        }
        return result;
    }

    /**
     * @see ProductoCatastralDAOBean#buscarProductosCatastralesPorSolicitudId(Long)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastral> buscarProductosCatastralesPorSolicitudId(Long solicitudId) {

        LOGGER.debug("Entra en ProductoCatastralDAOBean#buscarProductosCatastralesPorSolicitudId");

        List<ProductoCatastral> answer = null;

        Query q = entityManager.createQuery("SELECT pc FROM ProductoCatastral pc " +
            " JOIN FETCH pc.producto p " +
            " JOIN FETCH p.grupoProducto gp " +
            " WHERE pc.solicitud.id = :solicitudId");
        q.setParameter("solicitudId", solicitudId);

        try {
            answer = (List<ProductoCatastral>) q.getResultList();

        } catch (NoResultException e) {
            LOGGER.error("ProductoDAOBean#buscarProductosCatastralesPorSolicitudId:" +
                "Error al consultar la lista de productos del SNC");
        }
        return answer;

    }

    /**
     *
     * @see co.gov.igac.snc.dao.generales.IProductoCatastralDAO#obtenerJobsPendientesEnSNC(Long)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastral> obtenerJobsPendientesEnSNC(Long cantidad) {
        LOGGER.debug("obtenerJobsPendientesEnSNC");

        List<ProductoCatastral> resultados = null;
        String queryString;

        queryString = "SELECT pc FROM ProductoCatastral pc " +
            "WHERE pc.estado = :estado and pc.formato = :formato ORDER BY pc.id ASC";
        Query query = this.entityManager.createQuery(queryString);

        try {
            query.setParameter("estado", EProductoCatastralEstado.DISPONIBLE_DESCARGA.getCodigo());
            query.setParameter("formato", "PDF");
            if (cantidad > 0) {
                query.setMaxResults(cantidad.intValue());
            }
            query.setFirstResult(0);
            resultados = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ProductoCatastralDAOBean#obtenerJobsPendientesEnSNC");
        } finally {
            return resultados;
        }
    }

    /**
     * @see co.gov.igac.snc.dao.generales.IProductoCatastralDAO#actualizarEstado(ProductoCatastral,
     * String)
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @Override
    public void actualizarEstado(ProductoCatastral job, String nuevoEstado) {
        LOGGER.debug("actualizarEstado job: " + job + "  ,\n nuevoEstado:" + nuevoEstado);
        LOGGER.debug("Estado Actual:" + job.getEstado());
        job.setEstado(nuevoEstado);
        job = this.entityManager.merge(job);
        LOGGER.debug("Nuevo Estado:" + job.getEstado());
    }

}
