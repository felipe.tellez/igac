package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoPagoAvaluo;

/**
 * Interface que contiene metodos de consulta sobre entity {@link ContratoPagoAvaluo}
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IContratoPagoAvaluoDAO extends
    IGenericJpaDAO<ContratoPagoAvaluo, Long> {

    /**
     * consulta las relaciones entre el contrato y los avaluos de un pago
     *
     * @author christian.rodriguez
     * @param profesionalContratoPagoId identificador del pago
     * @return Lista con los {@link ContratoPagoAvaluo} asociados al pago
     */
    public List<ContratoPagoAvaluo> consultarPorProfesionalContratoPagoId(
        Long profesionalContratoPagoId);
}
