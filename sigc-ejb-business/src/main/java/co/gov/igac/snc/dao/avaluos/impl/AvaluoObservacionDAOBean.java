package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoObservacionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoObservacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoObservacionDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoObservacionDAOBean extends GenericDAOWithJPA<AvaluoObservacion, Long>
    implements IAvaluoObservacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoObservacionDAOBean.class);

    /**
     * @see IAvaluoObservacionDAO#buscarPorAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public List<AvaluoObservacion> buscarPorAvaluo(Long idAvaluo) {

        List<AvaluoObservacion> resultado = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT obs " +
                " FROM AvaluoObservacion obs " +
                " WHERE obs.avaluo.id = :avaluo ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluo", idAvaluo);
            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

}
