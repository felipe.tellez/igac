package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFmConstruccionComponenteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFmConstruccionComponente;

/**
 * Implementación de los servicios de persistencia del objeto {@link PFmConstruccionComponente}.
 *
 * @author david.cifuentes
 */
@Stateless
public class PFmConstruccionComponenteDAOBean extends
    GenericDAOWithJPA<PFmConstruccionComponente, Long> implements
    IPFmConstruccionComponenteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PFmConstruccionComponenteDAOBean.class);

    // ------------------------------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see IPFmConstruccionComponenteDAO# null null     actualizarPFmConstruccionComponenteRegistros(List<
	 *      PFmConstruccionComponente> )
     */
    @Override
    public void actualizarPFmConstruccionComponenteRegistros(
        List<PFmConstruccionComponente> newPFmConstruccionComponenteList) {

        LOGGER.debug(
            "PFmConstruccionComponenteDAOBean#actualizarPFmConstruccionComponenteRegistros " +
            "borrando e insertando registros ...");
        List<PFmConstruccionComponente> registersToDelete;
        List<String[]> criteriaToDelete;
        String[] criterion;

        criteriaToDelete = new ArrayList<String[]>();
        criterion = new String[3];

        criterion[0] = "PFmModeloConstruccion.id";
        criterion[1] = "number";
        criterion[2] = newPFmConstruccionComponenteList.get(0)
            .getPFmModeloConstruccion().getId().toString();
        criteriaToDelete.add(criterion);

        try {
            registersToDelete = this.findByExactCriteria(criteriaToDelete);
            this.removeMultiple(registersToDelete);
            // N: aquí no hay problema porque todos los de la lista son nuevos,
            // no tienen id
            this.persistMultiple(newPFmConstruccionComponenteList);
            LOGGER.debug("...OK");

        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
        }

    }

    // ------------------------------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see IPFmConstruccionComponenteDAO#borrarPFmConstruccionComponenteRegistros(Long)
     */
    @Override
    public void borrarPFmConstruccionComponenteRegistros(
        Long pFmModeloConstruccionId) {
        LOGGER.debug("PFmConstruccionComponenteDAOBean#borrarPFmConstruccionComponenteRegistros " +
            "borrando registros ...");

        List<String[]> criteriaToDelete;
        String[] criterion;

        criteriaToDelete = new ArrayList<String[]>();
        criterion = new String[3];

        criterion[0] = "PFmModeloConstruccion.id";
        criterion[1] = "number";
        criterion[2] = pFmModeloConstruccionId.toString();
        criteriaToDelete.add(criterion);

        try {
            this.deleteByExactCriteria(criteriaToDelete);
            LOGGER.debug("...OK");

        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
        }
    }

}
