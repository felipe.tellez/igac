package co.gov.igac.snc.dao.tramite.radicacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.FiltroDatosSolicitud;

@Local
public interface ISolicitudDAO extends IGenericJpaDAO<Solicitud, Long> {

    /**
     * Regresa las solicitudes que cumplen con los criterios de búsqueda dados.
     *
     * @param filtroDatosSolicitud
     * @param desde indica la posición a partir de la cual se deben recuperar los registros.
     * @param cantidad representa la cantidad de registros a retornar, un valor igual o menor a cero
     * indica que se deben regresar todos los registros cuya posición sea igual o superior al
     * parámetro desde.
     * @return una lista de objetos Solicitud.
     */
    public List<Solicitud> buscarSolicitudes(FiltroDatosSolicitud filtroDatosSolicitud, int desde,
        int cantidad);

    /**
     * Método encargado de contar la cantidad de trámites que satisfacen los criterios de busqueda
     * para asociar derecho de petición o tutela
     *
     * @author javier.aponte
     */
    public Integer contarSolicitudesByFiltroSolicitud(FiltroDatosSolicitud filtroDatosSolicitud);

    /**
     * Crea una solicitud y un trámite de avalúo correspondiente.
     *
     * @param usuario datos del usuario que realiza la operación.
     * @param solicitud datos de la solicitud, incluyendo el trámite, a crear.
     * @return la solicitud creada con el identificador (llave primaria) asignada.
     * @author jamir.avila
     */
    public Solicitud crearSolicitudTramite(UsuarioDTO usuario, Solicitud solicitud);

    /**
     * Retorna una solicitud a partir de su número de radicación, adicionalmente trae sus datos de
     * solicitanteSolicituds y trámites.
     *
     * @param numeroSolicitud
     * @return
     * @author fabio.navarrete
     */
    public Solicitud findSolicitudByNumSolicitudFetchGestionarSolicitudData(
        String numeroSolicitud);

    /**
     * Retorna la solicitud por numero con los datos basicos.
     *
     * @author felipe.cadena
     * @param numeroSolicitud
     * @return
     */
    public Solicitud findSolicitudByNumSolicitud(String numeroSolicitud);

    /**
     * Método que retorna una solicitud con todos sus datos asociados requeridos en la pantalla de
     * gestionarSolicitudes
     *
     * @param numeroSolicitud
     * @return
     * @author fabio.navarrete
     */
    public Solicitud findSolicitudByNumSolicitudFetchGestionarSolicitudesData(
        String numeroSolicitud);

    /**
     * Método que recupera una solicitud con trámites y trámite documentación
     *
     * @author juan.agudelo
     * @param solicitudId
     * @return
     */
    public Solicitud buscarSolicitudFetchTramitesBySolicitudId(Long solicitudId);

    /**
     * Método que busca una {@link Solicitud} con sus {@link SolicitanteSolicitud} asociados.
     *
     * @author david.cifuentes
     * @param solicitudId
     * @param fetchTramites
     * @return
     */
    /*
     * @modified felipe.cadena :: 06/10/2015 :: Se establece como opcional el fetch de los tramites
     * asociados a los solicitantes
     */
    public Solicitud buscarSolicitudConSolicitantesSolicitudPorId(Long solicitudId,
        boolean fetchTramites);

    /**
     * Busca las solicitudes que estén asociadas al contrato interadministrativo dado. Hace fetch de
     * nada.
     *
     * @author pedro.garcia
     * @param idContrato
     * @return
     */
    public List<Solicitud> buscarPorContrato(Long idContrato);

    /**
     *
     * Método para buscar solicitudes a partir de los datos de un solicitante
     *
     * @author rodrigo.hernandez
     *
     * @param filtroDatosSolicitud - Filtros de datos para búsqueda de solicitud
     * @return
     */
    public List<Solicitud> buscarSolicitudesPorSolicitante(FiltroDatosSolicitud filtroDatosSolicitud);

    /**
     * Método que devuelve la solicitud con los productos asociados y lista de documentos soporte
     *
     * @cu: CU-TV-PR-0001,CU-TV-PR-0002,CU-TV-PR-0003,CU-TV-PR-0004
     * @param SolicitudId
     * @return List<Producto>
     * @version:1.0
     * @author javier.barajas
     */
    public Solicitud obtenerSolicitudPorIdconProductosYDocsSoporte(Long solicitudId);

}
