package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDetalleDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;

/**
 * Implementación de los servicios de persistencia del objeto ProductoCatastralDetalle.
 *
 * @author javier.barajas
 */
@Stateless
public class ProductoCatastralDetalleDAOBean extends GenericDAOWithJPA<ProductoCatastralDetalle, Long>
    implements IProductoCatastralDetalleDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProductoCatastralDetalleDAOBean.class);

    //--------------------------------------------------------------------------------------------------------
    /**
     * @see ProductoCatastralDAOBean#buscarProductosCatastralesDetallePorProductoCatastralId(Long)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastralDetalle> buscarProductosCatastralesDetallePorProductoCatastralId(
        Long productoCatastralId) {

        LOGGER.debug(
            "Entra en ProductoCatastralDAOBean#buscarProductosCatastralesDetallePorProductoCatastralId");

        List<ProductoCatastralDetalle> answer = null;

        Query q = entityManager.createQuery("SELECT pcd FROM ProductoCatastralDetalle pcd " +
            " JOIN FETCH pcd.productoCatastral pc " +
            " JOIN FETCH pc.producto p " +
            " JOIN FETCH p.grupoProducto gp " +
            " WHERE pcd.productoCatastral.id = :productoCatastralId");
        q.setParameter("productoCatastralId", productoCatastralId);

        try {
            answer = (List<ProductoCatastralDetalle>) q.getResultList();

        } catch (NoResultException e) {
            LOGGER.error(
                "ProductoDAOBean#buscarProductosCatastralesDetallePorProductoCatastralId:" +
                "Error al consultar la lista de productos del SNC");
        }
        return answer;

    }

}
