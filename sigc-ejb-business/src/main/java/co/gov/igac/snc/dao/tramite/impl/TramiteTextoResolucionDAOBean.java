package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteTextoResolucionDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;

/**
 *
 * @author javier.aponte
 *
 */
@Stateless
public class TramiteTextoResolucionDAOBean extends
    GenericDAOWithJPA<TramiteTextoResolucion, Long> implements
    ITramiteTextoResolucionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteTextoResolucionDAOBean.class);

    @Override
    public TramiteTextoResolucion findTramiteTextoResolucionPorIdTramite(
        Long tramiteId) {
        LOGGER.debug("TramiteTextoResolucionDAOBean#findTramiteTextoResolucionPorIdTramite");
        String query = "SELECT ttr FROM TramiteTextoResolucion ttr " +
            "WHERE ttr.tramite.id = :idTramite ";

        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", tramiteId);
        try {
            return (TramiteTextoResolucion) q.getSingleResult();
        } catch (Exception e) {
            LOGGER.debug("Error al consultar los tramites textos resolucion: " + e.getMessage());
            return null;
        }
    }

}
