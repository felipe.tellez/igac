package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.InformacionBasicaCarto;

/**
 * Servicios de persistencia para los objetos InformacionBasicaCatografica. Estos objetos
 * representan aquella información básica cartográfica que está disponible para el proceso de
 * actualización.
 *
 * @author jamir.avila
 *
 */
@Local
public interface IInformacionBasicaCartoDAO extends IGenericJpaDAO<InformacionBasicaCarto, Long> {

    /**
     * Crea una nueva información básica cartográfica asociada con un proceso de actualización.
     *
     * @param informacionBasicaAgrologica objeto de transporte con la información básica
     * cartográfica asociar.
     * @return el objeto de transporte creado en el sistema de persistencia incluyendo su
     * identificador asignado.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal en la operación
     * del método.
     * @author jamir.avila.
     */
    public InformacionBasicaCarto crear(InformacionBasicaCarto informacionBasicaCartografica) throws
        ExcepcionSNC;
}
