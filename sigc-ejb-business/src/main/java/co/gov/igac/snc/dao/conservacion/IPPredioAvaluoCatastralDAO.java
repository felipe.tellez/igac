package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;

@Local
public interface IPPredioAvaluoCatastralDAO extends
    IGenericJpaDAO<PPredioAvaluoCatastral, Long> {

    /**
     * Busca el valor de avalúo por número predial, especificamente utilizado en FichaMatrizPredio
     * en el atributo transient valorAvaluo
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public Double findValorAvaluoByNumeroPredial(String numeroPredial);

    /**
     * Método que obtiene los {@link PPredioAvaluoCatastral} del {@link PPredio}.
     *
     * @author david.cifuentes
     * @param pPredioId
     * @return
     */
    public List<PPredioAvaluoCatastral> obtenerListaPPredioAvaluoCatastralPorIdPPredio(
        Long pPredioId);

}
