package co.gov.igac.snc.dao.vistas;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VTramitePredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import java.util.HashMap;

/**
 * Interfaz para los servicios de persistencia del objeto {@link VTramitePredio}.
 *
 * @author david.cifuentes
 */
@Local
public interface IVTramitePredioDAO extends
    IGenericJpaDAO<VTramitePredio, Long> {

    /**
     * Obtiene una lista de {@link VTramitePredio} filtrada por ciertos parametros de búsqueda.
     *
     * @param estadoTramite
     * @param numeroSolicitudBusqueda
     * @param numeroRadicacionBusqueda
     * @param datosConsultaPredio
     * @param solicitanteBusqueda
     * @return
     * @author david.cifuentes
     */
    public List<VTramitePredio> buscarTramitesPorFiltros(String estadoTramite,
        String numeroSolicitudBusqueda, String numeroRadicacionBusqueda,
        FiltroDatosConsultaPredio datosConsultaPredio,
        FiltroDatosConsultaSolicitante solicitanteBusqueda);

    // ------------------------------------------------------------------ //
    /**
     * Método que obtiene una lista de trámites filtrada por parametros de trámite, predio y
     * solicitante.
     *
     * @author david.cifuentes
     *
     * @param filtrosTramite
     * @param filtrosPredio
     * @param filtrosSolicitante
     * @param codigoMunicipioTerritorialSeleccionada
     * @return
     */
    public HashMap<Long, Tramite> consultaDeTramitesPorFiltros(
        FiltroDatosConsultaTramite filtrosTramite,
        FiltroDatosConsultaPredio filtrosPredio,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String codigoMunicipioTerritorialSeleccionada,
        int maxResults);
}
