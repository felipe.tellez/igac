package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.InformacionBasicaAgro;

/**
 * Servicios de persistencia para los objetos InformacionBasicaAgrologica. Estos objetos representan
 * aquella información básica agrológica que está disponible para el proceso de actualización.
 *
 * @author jamir.avila
 *
 */
@Local
public interface IInformacionBasicaAgroDAO extends IGenericJpaDAO<InformacionBasicaAgro, Long> {

    /**
     * Crea una nueva información básica agrológica asociada con un proceso de actualización.
     *
     * @param informacionBasicaAgrologica objeto de transporte con la información básica agrológica
     * a asociar.
     * @return el objeto de transporte creado en el sistema de persistencia incluyendo su
     * identificador asignado.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal en la operación
     * del método.
     * @author jamir.avila.
     */
    public InformacionBasicaAgro crear(InformacionBasicaAgro informacionBasicaAgrologica) throws
        ExcepcionSNC;
}
