package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;

/**
 *
 * Interface para los métodos de bd de la DETALLE_DOCUMENTO_FALTANTE
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IDetalleDocumentoFaltanteDAO extends
    IGenericJpaDAO<DetalleDocumentoFaltante, Long> {

    /**
     * Método para cargar los detalles de los documentos faltantes de una solicitud a partir del id
     * de la solicitud
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitud
     * @return
     */
    public List<DetalleDocumentoFaltante> obtenerDetallesDocumentosFaltantesPorIdSolicitud(
        Long idSolicitud);

}
