/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface IPredioBloqueoDAO extends IGenericJpaDAO<PredioBloqueo, Long> {

    /**
     * Retorna un objeto PredioBloqueo si hay uno que esté vigente para el predio con el número
     * predial dado
     *
     * Regla: (la misma que se definió en las funciones de la bd) bloqueado si fecha actual GE fecha
     * inicio bloqueo y ((fecha actual LE fecha desbloqueo o (fecha desbloqueo is null y fecha
     * actual LE fecha termina bloqueo) o (fecha desbloqueo is null o fecha termina bloqueo is
     * null))
     *
     * @author pedro.garcia
     * @param numeroPredial
     * @return
     */
    public PredioBloqueo findCurrentPredioBloqueo(String numeroPredial);

    /**
     * Obtiene el contedo de la lista de predios bloqueados de acuerdo a los parametros de la
     * busqueda
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPredioBloqueo
     * @return
     */
    public Integer contarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo);

    /**
     * Método que verifica si una entidad se encuentra o no asociada al bloqueo de un
     * {@link Predio}.
     *
     * @param entidad
     * @return
     */
    public boolean existeAsociacionEntidadBloqueo(Entidad entidad);

    /**
     * Método que retorna un predio bloqueado según el tipo de bloqueo
     *
     * @param numeroPredial
     * @param tipoBloqueo
     * @return
     */
    public PredioBloqueo busquedaPredioBloqueoByTipoBloqueo(String numeroPredial, String tipoBloqueo);

    /**
     * Método que retorna predio bloqueado más reciente
     *
     * @param numeroPredial
     * @return
     */
    public PredioBloqueo busquedaPredioBloqueoReciente(String numeroPredial);

    /**
     * Método que retorna la lista de predios desbloqueados
     *
     * @param numeroPredial
     * @return
     */
    public List<PredioBloqueo> getPredioDesBloqueosByNumeroPredial(String numeroPredial);

    /**
     * Método que retorna predio desbloqueado más reciente
     *
     * @param numeroPredial
     * @return
     */
    public PredioBloqueo busquedaPredioDesBloqueoReciente(String numeroPredial);

    /**
     * Obtiene los bloqueos qu tiene asociado un predio en particular, tambien filtra por el estado
     * del bloqueo, si el stado es nulo los retorna todos.
     *
     * @author felipe.cadena
     * @param predioId
     * @param estadoBloqueo
     * @return
     */
    public List<PredioBloqueo> obtenerBloqueosPredio(Long predioId, String estadoBloqueo);

}
