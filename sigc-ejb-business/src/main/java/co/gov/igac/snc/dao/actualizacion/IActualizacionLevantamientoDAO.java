package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.GeneracionFormularioSbc;

/**
 * Servicios de persistencia para el objeto ActualizacionLevantamiento
 *
 * @author javier.barajas
 */
@Local
public interface IActualizacionLevantamientoDAO extends
    IGenericJpaDAO<ActualizacionLevantamiento, Long> {

}
