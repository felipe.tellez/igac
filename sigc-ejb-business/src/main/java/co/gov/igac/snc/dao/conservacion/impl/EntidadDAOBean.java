/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IEntidadDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @author david.cifuentes
 */
@Stateless
public class EntidadDAOBean extends GenericDAOWithJPA<Entidad, Long> implements
    IEntidadDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EntidadDAOBean.class);

    // ---------------------------------------------------------------------- //
    /**
     * @see IEntidadDao#buscarEntidadesPorNombreYEstado(String, String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Entidad> buscarEntidadesPorNombreYEstado(String nombreEntidad,
        String estadoEntidad) {

        LOGGER.debug("Executing EntidadDAOBean#buscarEntidadesPorNombreYEstado");

        Query query;
        List<Entidad> answer = null;

        try {
            StringBuilder q = new StringBuilder();
            q.append("SELECT e FROM Entidad e WHERE 1 = 1 ");

            if (nombreEntidad != null && !nombreEntidad.trim().isEmpty()) {
                q.append("AND UPPER(e.nombre) like :nombreEntidad ");
            }
            if (estadoEntidad != null && !estadoEntidad.trim().isEmpty()) {
                q.append("AND e.estado = :estadoEntidad ");
            }

            query = this.entityManager.createQuery(q.toString());

            if (nombreEntidad != null && !nombreEntidad.trim().isEmpty()) {
                query.setParameter("nombreEntidad", nombreEntidad.toUpperCase() + "%");
            }
            if (estadoEntidad != null && !estadoEntidad.trim().isEmpty()) {
                query.setParameter("estadoEntidad", estadoEntidad);
            }

            answer = query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("EntidadDAOBean#buscarEntidadesPorNombreYEstado: " +
                Constantes.NO_HAY_RESULTADOS_ENTIDADES + nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "EntidadDAOBean#buscarEntidadesPorNombreYEstado");
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IEntidadDao#buscarEntidades(String, String, String, String)
     * @author dumar.penuela
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Entidad> buscarEntidades(String nombreEntidad,
        String estadoEntidad, String departamento, String municipio) {

        LOGGER.debug("Executing EntidadDAOBean#buscarEntidades");

        Query query;
        List<Entidad> answer = null;

        try {
            StringBuilder q = new StringBuilder();
            q.append("SELECT e FROM Entidad e WHERE 1 = 1 ");

            if (nombreEntidad != null && !nombreEntidad.trim().isEmpty()) {
                q.append("AND UPPER(e.nombre) like :nombreEntidad ");
            }
            if (estadoEntidad != null && !estadoEntidad.trim().isEmpty()) {
                q.append("AND e.estado = :estadoEntidad ");
            }

            if (departamento != null && !departamento.trim().isEmpty()) {
                q.append("AND e.departamento.codigo = :departamento ");
            }

            if (municipio != null && !municipio.trim().isEmpty()) {
                q.append("AND e.municipio.codigo = :municipio ");
            }

            q.append("ORDER BY e.departamento.codigo, e.municipio.codigo, e.nombre");

            query = this.entityManager.createQuery(q.toString());

            if (nombreEntidad != null && !nombreEntidad.trim().isEmpty()) {
                query.setParameter("nombreEntidad", nombreEntidad.toUpperCase() + "%");
            }
            if (estadoEntidad != null && !estadoEntidad.trim().isEmpty()) {
                query.setParameter("estadoEntidad", estadoEntidad);
            }

            if (departamento != null && !departamento.trim().isEmpty()) {
                query.setParameter("departamento", departamento);
            }

            if (municipio != null && !municipio.trim().isEmpty()) {
                query.setParameter("municipio", municipio);
            }

            answer = query.getResultList();

            for (Entidad entidad : answer) {

                if (entidad.getDepartamento() != null &&
                    entidad.getDepartamento().getNombre() != null &&
                    entidad.getMunicipio() != null &&
                    entidad.getMunicipio().getNombre() != null) {

                    entidad.getDepartamento().getNombre();
                    entidad.getMunicipio().getNombre();
                }

                if (entidad.getOrganizacion() != null &&
                    entidad.getOrganizacion().getNombre() != null &&
                    entidad.getOrganizacion().getTipo() != null &&
                    entidad.getOrganizacion().getCodigo() != null) {

                    entidad.getOrganizacion().getNombre();
                    entidad.getOrganizacion().getTipo();
                    entidad.getOrganizacion().getCodigo();
                }

            }
        } catch (NoResultException nrEx) {
            LOGGER.error("EntidadDAOBean#buscarEntidades: " +
                Constantes.NO_HAY_RESULTADOS_ENTIDADES + nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "EntidadDAOBean#buscarEntidades");
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IEntidadDao#buscarEntidadesPorEstado(String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Entidad> buscarEntidadesPorEstado(String estado) {

        LOGGER.debug("Executing EntidadDAOBean#buscarEntidadesPorEstado");

        Query query;
        List<Entidad> answer = null;

        try {
            query = this.entityManager.createQuery("SELECT e FROM Entidad e" +
                " WHERE e.estado = :estado");

            query.setParameter("estado", estado);

            answer = query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("EntidadDAOBean#buscarEntidadesPorNombreYEstado: " +
                Constantes.NO_HAY_RESULTADOS_ENTIDADES + nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "EntidadDAOBean#buscarEntidadesPorEstado");
        }

        return answer;
    }
    // end of class

    // ---------------------------------------------------------------------- //
    /**
     * @see IEntidadDao#buscarEntidad(String,String,String,String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public Entidad buscarEntidad(String nombre, String codDepartamento, String codMunicipio,
        String correo) {
        LOGGER.debug("Executing EntidadDAOBean#buscarEntidad");

        Query query;
        Entidad answer = null;
        StringBuilder sql = new StringBuilder();
        List<Entidad> entidades = null;

        try {

            sql.append("SELECT e FROM Entidad e  WHERE 1 = 1 ");

            if (nombre != null && !nombre.trim().isEmpty()) {
                sql.append("AND UPPER(e.nombre)= :nombre ");
            }

            if (codDepartamento != null && !codDepartamento.trim().isEmpty()) {
                sql.append("AND e.departamento.codigo= :codDepartamento ");
            }

            if (codMunicipio != null && !codMunicipio.trim().isEmpty()) {
                sql.append("AND e.municipio.codigo= :codMunicipio ");
            }

            if (correo != null && !correo.trim().isEmpty()) {
                sql.append("AND UPPER(e.correo)= :correo ");
            }

            query = this.entityManager.createQuery(sql.toString());

            if (nombre != null && !nombre.trim().isEmpty()) {
                query.setParameter("nombre", nombre.toUpperCase());
            }

            if (codDepartamento != null && !codDepartamento.trim().isEmpty()) {
                query.setParameter("codDepartamento", codDepartamento);
            }

            if (codMunicipio != null && !codMunicipio.trim().isEmpty()) {
                query.setParameter("codMunicipio", codMunicipio);
            }

            if (correo != null && !correo.trim().isEmpty()) {
                query.setParameter("correo", correo);
            }

            entidades = (List<Entidad>) query.getResultList();

            if (entidades != null && !entidades.isEmpty()) {
                answer = entidades.get(0);
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("EntidadDAOBean#buscarEntidad: " +
                Constantes.NO_HAY_RESULTADOS_ENTIDADES + nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "EntidadDAOBean#buscarEntidad");
        }
        return answer;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IEntidadDao#validaExistenciaEntidad(String, String, String, String)
     * @author dumar.penuela
     */
    @Override
    public boolean validaExistenciaEntidad(String nombreEntidad, String estadoEntidad,
        String departamento, String municipio) {
        LOGGER.debug("Executing EntidadDAOBean#validaExistenciaEntidad");

        Query query;
        boolean answer = false;
        Object cantidadEntidades = null;

        try {
            StringBuilder q = new StringBuilder();
            q.append("SELECT Count(e) FROM Entidad e WHERE 1 = 1 ");

            if (nombreEntidad != null && !nombreEntidad.trim().isEmpty()) {
                q.append("AND UPPER(e.nombre) like :nombreEntidad ");
            }
            if (estadoEntidad != null && !estadoEntidad.trim().isEmpty()) {
                q.append("AND e.estado = :estadoEntidad ");
            }

            if (departamento != null && !departamento.trim().isEmpty()) {
                q.append("AND e.departamento.codigo = :departamento ");
            }

            if (municipio != null && !municipio.trim().isEmpty()) {
                q.append("AND e.municipio.codigo = :municipio ");
            }

            query = this.entityManager.createQuery(q.toString());

            if (nombreEntidad != null && !nombreEntidad.trim().isEmpty()) {
                query.setParameter("nombreEntidad", nombreEntidad.toUpperCase() + "%");
            }
            if (estadoEntidad != null && !estadoEntidad.trim().isEmpty()) {
                query.setParameter("estadoEntidad", estadoEntidad);
            }

            if (departamento != null && !departamento.trim().isEmpty()) {
                query.setParameter("departamento", departamento);
            }

            if (municipio != null && !municipio.trim().isEmpty()) {
                query.setParameter("municipio", municipio);
            }

            cantidadEntidades = query.getSingleResult();

            if (cantidadEntidades != null && Integer.parseInt(cantidadEntidades.toString()) != 0) {
                answer = true;
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("EntidadDAOBean#validaExistenciaEntidad: " +
                Constantes.NO_HAY_RESULTADOS_ENTIDADES + nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "EntidadDAOBean#validaExistenciaEntidad");
        }
        return answer;
    }

}
