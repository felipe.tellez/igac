package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 *
 */
@Local
public interface IMunicipioDAO extends IGenericJpaDAO<Municipio, String> {

    public List<Municipio> findByDepartamento(String departamentoId);

    /**
     * Retorna la lista de Municipios de un Departamento dado. Hubo que crear una nueva para que
     * funcionara con native query
     *
     * @param departamentoId Código del departamento
     * @return
     */
    public List<Municipio> findByDepartamentoNQ(String departamentoId);

    /**
     * Método encargado de obtener un municipio a partir de su código.
     *
     * @param codigo Código del municipio
     * @return Municipio con el código que se recibe como parámetro.
     */
    public Municipio getMunicipioByCodigo(String codigo);

    /**
     * Método que consulta los municipios asociados a un departamento exceptuando los municipios con
     * catastro centralizado (Bogotá, Cali y los municipios de Antioquia)
     *
     * @param codigoDepartamento
     * @return
     * @author david.cifuentes
     */
    public List<Municipio> buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
        String departamentoCodigo);

    /**
     * Metodo para obtener losdepratamentos asociados a una oficina
     *
     * @author felipe.cadena
     * @param entidadTerritorialCode
     * @return
     */
    public List<Municipio> findByEntidadTerritorial(String entidadTerritorialCode);

    /**
     * Metodo para obtener todos los municipios asociados a entidades delegadas
     *
     * @author felipe.cadena
     * @return
     */
    public List<Municipio> obtenerDelegados();
    
    public List<Municipio> obtenerHabilitados();

    /**
     * Retorna todos los municipios que se encuentran en actualizacion actualmente en el municipio
     *
     * @author felipe.cadena
     *
     * @param deptoCodigo
     * @return
     */
    public List<Municipio> obtenerMunicipiosActualizacionPorDepto(String deptoCodigo);

    /**
     * @see IMunicipioDAO#validaMunicipioDepartamento(String,String)
     * @ dumar.penuela verifica la exitencia de un municipio en un departamento
     * @return
     */
    public Boolean validaMunicipioDepartamento(String departamentoId, String municipioId);

    /**
     * Retorna todos los municipios con informacion en el SNC y no delegados
     *
     * @return
     * @author felipe.cadena
     */
    public List<Municipio> buscarMunicipiosSNCNoDelegada();

}
