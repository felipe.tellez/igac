package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPModeloConstruccionFotoDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.persistence.entity.conservacion.PModeloConstruccionFoto;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;

/**
 * Servicios de persistencia del objeto {@link PModeloConstruccionFoto}.
 *
 * @author david.cifuentes
 */
@Stateless
public class PModeloConstruccionFotoDAOBean extends
    GenericDAOWithJPA<PModeloConstruccionFoto, Long> implements
    IPModeloConstruccionFotoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PModeloConstruccionFotoDAOBean.class);

    @EJB
    private ITipoDocumentoDAO tipoDocumentoDaoService;

    // ---------------------------------------- //
    /**
     * @see IPModeloConstruccionFotoDAO#findByUnidadDelModeloIdFetchDocumento(java.lang.Long)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    public List<PModeloConstruccionFoto> findByUnidadDelModeloIdFetchDocumento(
        Long unidadDelModeloId) {

        LOGGER.info("PModeloConstruccionFotoDAOBean#findByUnidadDelModeloIdFetchDocumento");
        List<PModeloConstruccionFoto> answer = null;
        Query query;

        query = this.entityManager
            .createNamedQuery("findByUnidadDelModeloIdFetchDocumento");
        query.setParameter("pFmModeloConstruccionIdParam", unidadDelModeloId);

        try {
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error");
        }
        return answer;
    }

    // ---------------------------------------- //
    /**
     * @see IPModeloConstruccionFotoDAO#guardarPModeloConstruccionFotos(List<
     *      PModeloConstruccionFoto>)
     * @author david.cifuentes
     */
    @Override
    public void guardarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> listPFotosUnidadDelModelo) {
        TipoDocumento tipoDocumento;

        try {
            tipoDocumento = this.tipoDocumentoDaoService
                .findById(ETipoDocumento.FOTOGRAFIA.getId());

            for (int i = 0; i < listPFotosUnidadDelModelo.size(); i++) {
                listPFotosUnidadDelModelo.get(i).getFotoDocumento()
                    .setTipoDocumento(tipoDocumento);

                this.update(listPFotosUnidadDelModelo.get(i));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ---------------------------------------- //
    /**
     * @see IPModeloConstruccionFotoDAO#actualizarPModeloConstruccionFotos(List<
     *      PModeloConstruccionFoto>)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public void actualizarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> pModeloConstruccionFotoList) {

        LOGGER.info("PModeloConstruccionFotoDAO#actualizarPModeloConstruccionFotos");

        List<PModeloConstruccionFoto> toDelete;
        Query query;

        try {
            query = this.entityManager
                .createNamedQuery("findByUnidadDelModeloConstruccionId");
            query.setParameter("pFmModeloConstruccionIdParam",
                pModeloConstruccionFotoList.get(0)
                    .getPFmModeloConstruccion().getId());

            toDelete = query.getResultList();

            // D: como se definió que el atributo 'documento' de PFoto tuviera
            // todas las operaciones en cascada, se borran también esos
            // registros
            removeMultiple(toDelete);

            // D: hay que forzar a que los id de las PModeloConstruccionFoto y
            // sus Documento sean null para que, una vez borrados, se puedan
            // insertar sin problema. Si los id no eran null se lanza una
            // excepción
            for (PModeloConstruccionFoto tempPModeloConstruccionFoto : pModeloConstruccionFotoList) {
                tempPModeloConstruccionFoto.setId(null);
                tempPModeloConstruccionFoto.getFotoDocumento().setId(null);
            }
            // D: ... y luego se insertan los nuevos en ambas tablas
            persistPModeloConstruccionFotos(pModeloConstruccionFotoList);
        } catch (Exception ex) {
            LOGGER.error("Error en PModeloConstruccionFotoDAO#actualizarPModeloConstruccionFotos:" +
                ex);
        }

    }

    // ---------------------------------------- //
    /**
     * @see IPModeloConstruccionFotoDAO#persistPModeloConstruccionFotos(List<
     *      PModeloConstruccionFoto>)
     * @author david.cifuentes
     */
    @Override
    public void persistPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> pModeloConstruccionFotoList) {

        TipoDocumento tipoDocumento;

        try {
            tipoDocumento = this.tipoDocumentoDaoService
                .findById(ETipoDocumento.FOTOGRAFIA.getId());
            for (int i = 0; i < pModeloConstruccionFotoList.size(); i++) {
                pModeloConstruccionFotoList.get(i).getFotoDocumento()
                    .setTipoDocumento(tipoDocumento);
                this.persist(pModeloConstruccionFotoList.get(i));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ---------------------------------------- //
    /**
     * @author david.cifuentes
     * @see IConservacion#guardarPModeloConstruccionFoto(PModeloConstruccionFoto
     * pModeloConstruccionFoto)
     */
    @Override
    public PModeloConstruccionFoto guardarPModeloConstruccionFoto(
        PModeloConstruccionFoto pModeloConstruccionFoto) {

        PModeloConstruccionFoto answer = null;

        Documento doc = pModeloConstruccionFoto.getFotoDocumento();
        doc.setTipoDocumento(this.tipoDocumentoDaoService
            .findById(ETipoDocumento.FOTOGRAFIA.getId()));
        doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioCreador(pModeloConstruccionFoto.getUsuarioLog());
        doc.setUsuarioLog(pModeloConstruccionFoto.getUsuarioLog());
        doc.setFechaLog(pModeloConstruccionFoto.getFechaLog());
        doc.setDescripcion(pModeloConstruccionFoto.getDescripcion());
        pModeloConstruccionFoto.setFotoDocumento(doc);
        answer = this.update(pModeloConstruccionFoto);

        return answer;
    }

}
