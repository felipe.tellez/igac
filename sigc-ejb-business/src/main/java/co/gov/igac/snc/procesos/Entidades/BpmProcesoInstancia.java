/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_PROCESO_INSTANCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmProcesoInstancia.findAll", query = "SELECT b FROM BpmProcesoInstancia b"),
    @NamedQuery(name = "BpmProcesoInstancia.findById", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.id = :id"),
    @NamedQuery(name = "BpmProcesoInstancia.findByFechaInicio", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "BpmProcesoInstancia.findByFechaFin", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.fechaFin = :fechaFin"),
    @NamedQuery(name = "BpmProcesoInstancia.findByIdInstanciaSnc", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.idInstanciaSnc = :idInstanciaSnc"),
    @NamedQuery(name = "BpmProcesoInstancia.findByIdEstado", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.idEstado = :idEstado"),
    @NamedQuery(name = "BpmProcesoInstancia.findByTramite", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.tramite = :tramite"),
    @NamedQuery(name = "BpmProcesoInstancia.findByPiid", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.piid = :piid"),
    @NamedQuery(name = "BpmProcesoInstancia.findByObservaciones", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.observaciones = :observaciones"),
    @NamedQuery(name = "BpmProcesoInstancia.findByNumeroPredial", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.numeroPredial = :numeroPredial"),
    @NamedQuery(name = "BpmProcesoInstancia.findByProcesoPadre", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.procesoPadre = :procesoPadre"),
    @NamedQuery(name = "BpmProcesoInstancia.findByTipoTramite", query = "SELECT b FROM BpmProcesoInstancia b WHERE b.tipoTramite = :tipoTramite")})
public class BpmProcesoInstancia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Column(name = "ID_INSTANCIA_SNC")
    private String idInstanciaSnc;
    @Column(name = "ID_ESTADO")
    private Long idEstado;
    @Column(name = "TRAMITE")
    private Long tramite;
    @Column(name = "PIID")
    private String piid;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "NUMERO_PREDIAL")
    private String numeroPredial;
    @Column(name = "PROCESO_PADRE")
    private Long procesoPadre;
    @Column(name = "TIPO_TRAMITE")
    private String tipoTramite;

    public BpmProcesoInstancia() {
    }

    public BpmProcesoInstancia(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIdInstanciaSnc() {
        return idInstanciaSnc;
    }

    public void setIdInstanciaSnc(String idInstanciaSnc) {
        this.idInstanciaSnc = idInstanciaSnc;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public Long getTramite() {
        return tramite;
    }

    public void setTramite(Long tramite) {
        this.tramite = tramite;
    }

    public String getPiid() {
        return piid;
    }

    public void setPiid(String piid) {
        this.piid = piid;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public Long getProcesoPadre() {
        return procesoPadre;
    }

    public void setProcesoPadre(Long procesoPadre) {
        this.procesoPadre = procesoPadre;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmProcesoInstancia)) {
            return false;
        }
        BpmProcesoInstancia other = (BpmProcesoInstancia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmProcesoInstancia[ id=" + id + " ]";
    }
    
}
