package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte control reporte
 *
 * @author javier.aponte
 */
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;

@Local
public interface IRepTerritorialReporteDAO extends IGenericJpaDAO<RepTerritorialReporte, Long> {

    /**
     * Método encargado de buscar en la base de datos los tipos de reporte asociados a la categoria
     *
     * @param categoria del reporte
     * @return lista de tipos de reportes asociados a la categoria
     * @author leidy.gonzalez
     */
    public List<Departamento> obtenerRepTerritorialReporte(String codigoTerritorial,
        String codigoUoc);

}
