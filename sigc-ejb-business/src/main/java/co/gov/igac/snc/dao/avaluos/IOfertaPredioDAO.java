package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;

@Local
public interface IOfertaPredioDAO extends IGenericJpaDAO<OfertaPredio, Long> {

}
