package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * Interfaz para los métodos de bd de la tabla SolicitudDocumentacion
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface ISolicitudDocumentacionDAO extends IGenericJpaDAO<SolicitudDocumentacion, Long> {

    /**
     * Metodo para recuperar las solicitudes de documentación relacionadas a una solicitud
     *
     * @author felipe.cadena
     *
     * @param idSolicitud id de la solicitud
     * @return Lista de solicitudDocumentacion con los documentos asociados
     */
    public List<SolicitudDocumentacion> obternerSolicitudesDocumentacionPorSolicitud(
        Long idSolicitud, Boolean aportado);

    /**
     * Método que busca los documentos basicos que se deben asociar a una solicitud de avaluo
     * comercial
     *
     * @author rodrigo.hernandez
     *
     * @return Lista de documentos obligatorios para solicitud de avaluo comercial
     *
     */
    public List<SolicitudDocumentacion> obtenerListaDocsBasicosDeSolicitudAvaluo();

    /**
     *
     * Método para obtener la lista de documentos faltantes asociados a una solicitud
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<SolicitudDocumentacion> obtenerListaDocsFaltantesDeSolicitudAvaluo();

    /**
     * Método que busca la lista de anexos a partir del id de una solicitud y un tipo de documento
     *
     * @return
     */
    public List<SolicitudDocumentacion> obtenerListaSolDocsPorSolicitudYTipoDocumento(
        Long solicitudId, Long tipoDocumentoId);

}
