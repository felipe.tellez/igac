/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPersonaDAO;
import co.gov.igac.snc.dao.conservacion.IPersonasBloqueoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IErrorProcesoMasivoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;
import co.gov.igac.snc.persistence.util.EErrorProcesoMasivoUsuario;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.util.EProgramaGeneraError;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author juan.agudelo
 *
 * @deprecated esta clase no cumple con el estándar de codificación del proyecto
 */
// TODO :: juan.agudelo :: 21-05-2012 :: usar la clase que corresponde ::
// pedro.garcia
@Stateless
@Deprecated
public class PersonasBloqueoDAOBean extends
    GenericDAOWithJPA<PersonaBloqueo, Long> implements IPersonasBloqueoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonasBloqueoDAOBean.class);

    @EJB
    private IPersonaDAO personaService;

    @EJB
    private IDocumentoDAO documentoService;

    @EJB
    private IErrorProcesoMasivoDAO errorProcesoMasivoService;

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPersonasBloqueoDAO#actualizarPersonasBloqueo(UsuarioDTO, List)
     */
    @Override
    public List<PersonaBloqueo> actualizarPersonasDesbloqueo(
        UsuarioDTO usuario, List<PersonaBloqueo> personasBloqueadas) {

        List<PersonaBloqueo> pBloqueadas = null;
        Map<String, String> rutasAlfrescoDocumentosSoporte = null;
        List<Persona> personas;
        Documento dr;

        if (personasBloqueadas != null && !personasBloqueadas.isEmpty()) {
            try {
                pBloqueadas = new ArrayList<PersonaBloqueo>();
                personas = new ArrayList<Persona>();

                for (PersonaBloqueo p : personasBloqueadas) {
                    personas.add(p.getPersona());
                }

                if (personasBloqueadas.get(0).getDocumentoSoporteDesbloqueo() != null) {
                    rutasAlfrescoDocumentosSoporte = this.documentoService
                        .almacenarDocumentosBloqueoGestorDocumental(usuario,
                            personasBloqueadas.get(0)
                                .getDocumentoSoporteDesbloqueo(),
                            personas, null);
                }

                for (PersonaBloqueo personaB : personasBloqueadas) {
                    personaB.setUsuarioLogDesbloqueo(usuario.getLogin());
                    personaB.setFechaLogDesbloqueo(new Date(System.currentTimeMillis()));

                    if (rutasAlfrescoDocumentosSoporte != null &&
                        !rutasAlfrescoDocumentosSoporte.isEmpty()) {

                        for (String url : rutasAlfrescoDocumentosSoporte
                            .keySet()) {
                            if (url.equals(personaB.getPersona()
                                .getNumeroIdentificacion())) {
                                personaB.getDocumentoSoporteDesbloqueo()
                                    .setIdRepositorioDocumentos(
                                        rutasAlfrescoDocumentosSoporte
                                            .get(url));
                            }
                        }
                    }

                    personaB.getDocumentoSoporteDesbloqueo().setUsuarioLog(
                        usuario.getLogin());
                    personaB.getDocumentoSoporteDesbloqueo().setUsuarioCreador(
                        usuario.getLogin());
                    dr = this.documentoService.update(personaB
                        .getDocumentoSoporteDesbloqueo());
                    personaB.setDocumentoSoporteDesbloqueo(dr);

                    update(personaB);
                    pBloqueadas.add(personaB);

                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010
                    .getExcepcion(LOGGER, e, e.getMessage());
            }
        }
        return pBloqueadas;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPersonasBloqueoDAO#actualizarPersonaBloqueoM(UsuarioDTO, ArrayList)
     */
    @Override
    public List<PersonaBloqueo> actualizarPersonaBloqueoM(UsuarioDTO usuario,
        ArrayList<PersonaBloqueo> bloqueoMasivo) {

        List<PersonaBloqueo> perBloqueadas = new ArrayList<PersonaBloqueo>();
        Map<String, String> rutasAlfrescoDocumentosSoporte;
        List<Persona> personas;
        Persona personaTemp = null;
        Documento dr;

        if (bloqueoMasivo != null && !bloqueoMasivo.isEmpty()) {

            Documento tempDoc = bloqueoMasivo.get(0)
                .getDocumentoSoporteBloqueo();
            tempDoc.setUsuarioLog(usuario.getLogin());
            tempDoc.setUsuarioCreador(usuario.getLogin());
            tempDoc.setFechaLog(new Date(System.currentTimeMillis()));
            personas = new ArrayList<Persona>();

            try {
                for (PersonaBloqueo p : bloqueoMasivo) {
                    personaTemp = this.personaService
                        .buscarPersonaByNombreDocumentoDV(p.getPersona());
                    personas.add(personaTemp);
                }

                if (personas != null && !personas.isEmpty()) {

                    rutasAlfrescoDocumentosSoporte = this.documentoService
                        .almacenarDocumentosBloqueoGestorDocumental(usuario,
                            bloqueoMasivo.get(0)
                                .getDocumentoSoporteBloqueo(),
                            personas, null);

                    for (int i = 0; i < bloqueoMasivo.size(); i++) {
                        PersonaBloqueo personaB = bloqueoMasivo.get(i);
                        personaB.setUsuarioLog(usuario.getLogin());
                        personaB.setFechaLog(new Date(System
                            .currentTimeMillis()));

                        try {
                            if (rutasAlfrescoDocumentosSoporte != null &&
                                !rutasAlfrescoDocumentosSoporte
                                    .isEmpty()) {

                                for (String url : rutasAlfrescoDocumentosSoporte
                                    .keySet()) {
                                    if (url.equals(personaB.getPersona()
                                        .getNumeroIdentificacion())) {
                                        personaB.getDocumentoSoporteBloqueo()
                                            .setIdRepositorioDocumentos(
                                                rutasAlfrescoDocumentosSoporte
                                                    .get(url));
                                    }
                                }

                                personaB.getDocumentoSoporteBloqueo()
                                    .setUsuarioLog(usuario.getLogin());
                                personaB.getDocumentoSoporteBloqueo()
                                    .setUsuarioCreador(usuario.getLogin());
                                dr = this.documentoService.update(personaB
                                    .getDocumentoSoporteBloqueo());
                                personaB.setDocumentoSoporteBloqueo(dr);

                                personaB.setPersona(personaTemp);
                                personaB.setDocumentoSoporteBloqueo(dr);
                                update(personaB);
                                perBloqueadas.add(personaB);
                            }

                        } catch (Exception ex) {
                            throw SncBusinessServiceExceptions.EXCEPCION_100010
                                .getExcepcion(LOGGER, ex, ex.getMessage());
                        }
                    }
                }
            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010
                    .getExcepcion(LOGGER, ex, ex.getMessage());
            }
        }

        return perBloqueadas;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPersonasBloqueoDAO#bloquearMasivamentePersonas(UsuarioDTO, ArrayList)
     */
    @Override
    public ResultadoBloqueoMasivo bloquearMasivamentePersonas(
        UsuarioDTO usuario, ArrayList<PersonaBloqueo> bloqueoMasivo) {

        List<PersonaBloqueo> perBloqueadas = new ArrayList<PersonaBloqueo>();
        List<PersonaBloqueo> personasBloqueadas;
        List<PersonaBloqueo> personasTemp = null;
        List<ErrorProcesoMasivo> erroresPersonaBloqueo = null;
        List<ErrorProcesoMasivo> erroresResultado = null;
        ErrorProcesoMasivo errorPM;
        ErrorProcesoMasivo errorTemp;
        Map<String, String> rutasAlfrescoDocumentosSoporte = null;
        Map<String, String> rutasAlfrescoDocumentoBloqueoMasivo = null;
        List<Persona> personas;
        List<Persona> personasHomonimas;
        Documento dr;
        ResultadoBloqueoMasivo answer = null;

        try {
            if (bloqueoMasivo != null && !bloqueoMasivo.isEmpty()) {

                Documento tempDoc = bloqueoMasivo.get(0)
                    .getDocumentoSoporteBloqueo();
                tempDoc.setUsuarioLog(usuario.getLogin());
                tempDoc.setUsuarioCreador(usuario.getLogin());
                tempDoc.setFechaLog(new Date(System.currentTimeMillis()));
                personas = new ArrayList<Persona>();

                for (PersonaBloqueo p : bloqueoMasivo) {

                    p.setUsuarioLog(usuario.getLogin());
                    p.setFechaLog(new Date(System.currentTimeMillis()));

                    personasHomonimas = this.personaService.
                        buscarPersonaPorNombrePartesTipoDocNumDocTemp(p.getPersona());

                    if (personasHomonimas != null && !personasHomonimas.isEmpty()) {
                        personas.addAll(personasHomonimas);
                        for (Persona perHomonima : personasHomonimas) {
                            PersonaBloqueo pClone = (PersonaBloqueo) p.clone();
                            pClone.setPersona(perHomonima);
                            perBloqueadas.add(pClone);
                        }
                    } else {
                        errorPM = new ErrorProcesoMasivo();
                        errorPM.setErrorUsuario(EErrorProcesoMasivoUsuario.PERSONA_NO_ENCONTRADA
                            .getCodigo());
                        errorPM.setPrograma(EProgramaGeneraError.PERSONA_BLOQUEO
                            .getCodigo());
                        errorPM.setFecha(new Date());
                        if (p.getPersona()
                            .getTipoIdentificacion()
                            .equals(EPersonaTipoIdentificacion.NIT
                                .getCodigo())) {
                            p.getPersona().setTipoPersona(
                                EPersonaTipoPersona.JURIDICA.getCodigo());
                        } else {
                            p.getPersona().setTipoPersona(
                                EPersonaTipoPersona.NATURAL.getCodigo());
                        }
                        errorPM.setTextoFuente(p.getPersona()
                            .getTipoIdentificacion() +
                            ";" +
                            p.getPersona().getNumeroIdentificacion() +
                            ";" + p.getPersona().getNombreCompleto());

                        if (erroresPersonaBloqueo == null) {
                            erroresPersonaBloqueo = new ArrayList<ErrorProcesoMasivo>();
                        }
                        erroresPersonaBloqueo.add(errorPM);
                    }
                }

                if (!personas.isEmpty()) {

                    if (bloqueoMasivo.get(0) != null &&
                        bloqueoMasivo.get(0).getDocumentoSoporteBloqueo() != null) {
                        rutasAlfrescoDocumentosSoporte = this.documentoService
                            .almacenarDocumentosBloqueoGestorDocumental(usuario,
                                bloqueoMasivo.get(0)
                                    .getDocumentoSoporteBloqueo(),
                                personas, null);

                        if (bloqueoMasivo.get(0).getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos() != null &&
                            bloqueoMasivo.get(0)
                                .getDocumentoSoporteBloqueo()
                                .getDocumentoArchivoAnexos().get(0) != null &&
                            bloqueoMasivo.get(0)
                                .getDocumentoSoporteBloqueo()
                                .getDocumentoArchivoAnexos().get(0)
                                .getDocumento() != null) {

                            rutasAlfrescoDocumentoBloqueoMasivo = this.documentoService
                                .almacenarDocumentosBloqueoGestorDocumental(
                                    usuario,
                                    bloqueoMasivo
                                        .get(0)
                                        .getDocumentoSoporteBloqueo()
                                        .getDocumentoArchivoAnexos()
                                        .get(0).getDocumento(),
                                    personas, null);
                        }
                    }
                }

                try {

                    personasTemp = new ArrayList<PersonaBloqueo>();
                    personasTemp.addAll(perBloqueadas);
                    personasBloqueadas = new ArrayList<PersonaBloqueo>();
                    DocumentoArchivoAnexo documentoSoporteAnexoTmp = null;

                    for (PersonaBloqueo personaB : personasTemp) {

                        if (rutasAlfrescoDocumentosSoporte != null &&
                            !rutasAlfrescoDocumentosSoporte.isEmpty()) {

                            for (String url : rutasAlfrescoDocumentosSoporte
                                .keySet()) {
                                if (url.equals(personaB.getPersona()
                                    .getNumeroIdentificacion())) {

                                    personaB.getDocumentoSoporteBloqueo()
                                        .setIdRepositorioDocumentos(
                                            rutasAlfrescoDocumentosSoporte
                                                .get(url));
                                }
                            }

                            if (rutasAlfrescoDocumentoBloqueoMasivo != null &&
                                !rutasAlfrescoDocumentoBloqueoMasivo
                                    .isEmpty()) {

                                for (String url : rutasAlfrescoDocumentoBloqueoMasivo
                                    .keySet()) {
                                    if (url.equals(personaB.getPersona()
                                        .getNumeroIdentificacion())) {

                                        for (DocumentoArchivoAnexo daa : personaB
                                            .getDocumentoSoporteBloqueo()
                                            .getDocumentoArchivoAnexos()) {

                                            daa.setIdRepositorioDocumentos(
                                                rutasAlfrescoDocumentoBloqueoMasivo
                                                    .get(url));
                                        }
                                    }
                                }
                            }

                            personaB.getDocumentoSoporteBloqueo()
                                .setUsuarioLog(usuario.getLogin());
                            personaB.getDocumentoSoporteBloqueo()
                                .setUsuarioCreador(usuario.getLogin());

                            dr = this.documentoService.update(personaB
                                .getDocumentoSoporteBloqueo());

                            if (dr != null &&
                                dr.getDocumentoArchivoAnexos() != null &&
                                !dr.getDocumentoArchivoAnexos()
                                    .isEmpty()) {
                                documentoSoporteAnexoTmp = dr
                                    .getDocumentoArchivoAnexos().get(0);
                            }

                            personaB.setDocumentoSoporteBloqueo(dr);

                            personaB.setDocumentoSoporteBloqueo(dr);

                            personaB = update(personaB);
                            personasBloqueadas.add(personaB);

                        }
                    }

                    if (erroresPersonaBloqueo != null &&
                        !erroresPersonaBloqueo.isEmpty()) {

                        erroresResultado = new ArrayList<ErrorProcesoMasivo>();

                        for (ErrorProcesoMasivo error : erroresPersonaBloqueo) {

                            if (documentoSoporteAnexoTmp != null &&
                                documentoSoporteAnexoTmp.getDocumento() != null) {
                                error.setSoporteDocumento(documentoSoporteAnexoTmp
                                    .getDocumento());
                            }

                            errorTemp = this.errorProcesoMasivoService
                                .update(error);
                            erroresResultado.add(errorTemp);
                        }
                    }

                    answer = new ResultadoBloqueoMasivo(null,
                        personasBloqueadas, erroresResultado);
                } catch (Exception ex) {

                    throw SncBusinessServiceExceptions.EXCEPCION_100010
                        .getExcepcion(LOGGER, ex, ex.getMessage());
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, ex.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
}
