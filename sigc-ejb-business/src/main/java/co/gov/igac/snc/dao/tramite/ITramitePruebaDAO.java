package co.gov.igac.snc.dao.tramite;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;

/**
 *
 * @author juan.agudelo
 *
 */
@Local
public interface ITramitePruebaDAO extends IGenericJpaDAO<TramitePrueba, Long> {

    /**
     * Método que guarda o actualiza una solicitud de prueba
     *
     * @author juan.agudelo
     * @param tp
     * @param usuario
     * @param estadoFuturoTramite
     * @return
     */
    TramitePrueba guardarActualizarTramitePruebas(TramitePrueba tp,
        UsuarioDTO usuario, String estadoFuturoTramite);

}
