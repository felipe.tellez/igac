package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IProfesionalContratoPagoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;
import co.gov.igac.snc.persistence.util.EProfesionalContratoPagoEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IProfesionalContratoPagoDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ProfesionalContratoPagoDAOBean extends
    GenericDAOWithJPA<ProfesionalContratoPago, Long> implements IProfesionalContratoPagoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfesionalContratoPago.class);

    /**
     * @see IProfesionalContratoPagoDAO#consultarPorIdContrato(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalContratoPago> consultarPorIdContrato(Long idContrato) {
        LOGGER.debug("Inicio ProfesionalContratoPagoDAOBean#consultarPorIdContrato");

        List<ProfesionalContratoPago> result = new ArrayList<ProfesionalContratoPago>();

        String queryString;
        Query query;

        queryString = "SELECT pcp " +
            " FROM ProfesionalContratoPago pcp " +
            " WHERE pcp.profesionalAvaluosContrato.id = :idContrato  ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);

            result = (List<ProfesionalContratoPago>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalContratoPagoDAOBean#consultarPorIdContrato");
        return result;

    }

    /**
     * @see IProfesionalContratoPagoDAO#consultarDePensionSaludPorIdContrato(Long idContrato);
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalContratoPago> consultarDePensionSaludPorIdContrato(Long idContrato) {
        LOGGER.debug("Inicio ProfesionalContratoPagoDAOBean#consultarDePensionSaludPorIdContrato");

        List<ProfesionalContratoPago> result = new ArrayList<ProfesionalContratoPago>();

        String queryString;
        Query query;

        queryString = "SELECT pcp " +
            " FROM ProfesionalContratoPago pcp " +
            " WHERE pcp.profesionalAvaluosContrato.id = :idContrato  " +
            " AND (pcp.valorPagarPension IS NOT NULL OR pcp.valorPagarSalud IS NOT NULL)  ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);

            result = (List<ProfesionalContratoPago>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalContratoPagoDAOBean#consultarDePensionSaludPorIdContrato");
        return result;

    }

    /**
     * @author christian.rodriguez
     * @see IProfesionalContratoPagoDAO#consultarProyectadosPorIdContrato(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalContratoPago> consultarProyectadosPorIdContrato(Long idContrato) {
        List<ProfesionalContratoPago> pagosProyectados = new ArrayList<ProfesionalContratoPago>();

        String queryString;
        Query query;

        queryString = "SELECT pcp " +
            " FROM ProfesionalContratoPago pcp " +
            " WHERE pcp.profesionalAvaluosContrato.id = :idContrato " +
            " AND pcp.estado = :estado";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            query.setParameter("estado",
                EProfesionalContratoPagoEstado.PROYECTADO.getCodigo());

            pagosProyectados = (List<ProfesionalContratoPago>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalContratoPagoDAOBean#consultarProyectadosPorIdContrato");
        return pagosProyectados;
    }

    /**
     * @author christian.rodriguez
     * @see IProfesionalContratoPagoDAO#consultarCausadosPorIdContrato(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalContratoPago> consultarCausadosPorIdContrato(Long idContrato) {
        List<ProfesionalContratoPago> pagosProyectados = new ArrayList<ProfesionalContratoPago>();

        String queryString;
        Query query;

        queryString = "SELECT pcp " +
            " FROM ProfesionalContratoPago pcp " +
            " WHERE pcp.profesionalAvaluosContrato.id = :idContrato " +
            " AND pcp.estado = :estado";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            query.setParameter("estado",
                EProfesionalContratoPagoEstado.CAUSADO.getCodigo());

            pagosProyectados = (List<ProfesionalContratoPago>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalContratoPagoDAOBean#consultarCausadosPorIdContrato");
        return pagosProyectados;
    }

    /**
     * @author christian.rodriguez
     * @see IProfesionalContratoPagoDAO#calcularSiguienteSecPago(Long)
     */
    @Override
    public Long calcularSiguienteSecPago(Long idContrato) {

        Long answer = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT COUNT(pcp) " +
                " FROM ProfesionalContratoPago pcp " +
                " WHERE pcp.profesionalAvaluosContrato.id = :idContrato ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);

            answer = (Long) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return ++answer;
    }

    /**
     * @author christian.rodriguez
     * @see IProfesionalContratoPagoDAO#obtenerContratoPagoAvaluosPorId(Long)
     */
    @Override
    public ProfesionalContratoPago obtenerContratoPagoAvaluosPorId(
        Long pagoId) {

        ProfesionalContratoPago pago = null;

        try {
            pago = this.findById(pagoId);

            if (pago != null) {
                Hibernate.initialize(pago.getContratoPagoAvaluos());
                Hibernate.initialize(pago.getProfesionalAvaluosContrato());
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return pago;
    }

}
