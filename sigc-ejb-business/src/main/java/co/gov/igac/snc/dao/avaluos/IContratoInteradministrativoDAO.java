package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Consignacion;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * Interfaz para los métodos de bd de la tabla ContratoInteradministrativo
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IContratoInteradministrativoDAO extends
    IGenericJpaDAO<ContratoInteradministrativo, Long> {

    /**
     * Retorna una lista de contratos relacionados con los parametros del filtro de solicitante y
     * los paramertos realcionados directamente al contrato, si alguna de los parametros es nulo no
     * se toma en cuenta en la consulta.
     *
     * @author felipe.cadena
     * @modifiedby christian.rodriguez 07/11/2012 Se agrego el uso del
     * {@link FiltroDatosConsultaContratoInteradministrativo}
     *
     * @param filtroSolicitante
     * @param filtroContrato
     * @return
     */
    public List<ContratoInteradministrativo> buscarContratosPorFiltro(
        FiltroDatosConsultaSolicitante filtroSolicitante,
        FiltroDatosConsultaContratoInteradministrativo filtroContrato);

    /**
     * Retorna el {@link ContratoInteradministrativo} que tiene el id indicado.
     *
     * @author christian.rodriguez
     * @param id Identificador del contrato
     * @return Contrato encontrado o null si no se encontraroin resultados
     */
    public ContratoInteradministrativo buscarPorIdConEntidadSolicitante(Long idContrato);

    /**
     * Retorna el {@link ContratoInteradministrativo} que tiene el id indicado. Tambien carga las
     * {@link Consignacion} asociadas
     *
     * @author christian.rodriguez
     * @param id Identificador del contrato
     * @return Contrato encontrado o null si no se encontraroin resultados
     */
    public ContratoInteradministrativo buscarPorIdConConsignaciones(Long idContrato);

    /**
     * Obtiene el resultado de la sumatoria de los valores de las adiciones echas en un contrato
     *
     * @author christian.rodriguez
     * @param idContrato id del contrato del cual se quieren obtener las adiciones
     * @return valor double con el total de la sumatoria de las adiciones realizadas a un contrato
     */
    public Double obtenerTotalAdicionesPorIdContrato(Long idContrato);

    /**
     * Obtiene el ContratoInteradministrativo dado su id. No hace fetch de nada
     *
     * @author pedro.garcia
     * @param idContrato
     * @return
     */
    public ContratoInteradministrativo obtenerPorId(Long idContrato);

    /**
     * Obtiene el ContratoInteradministrativo dado su id, haciendo fetch de: interventor
     *
     * @author pedro.garcia
     * @param idContrato
     * @return
     */
    public ContratoInteradministrativo obtenerPorIdConFetching(Long idContrato);

}
