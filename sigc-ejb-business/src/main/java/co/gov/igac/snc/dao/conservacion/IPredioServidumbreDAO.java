package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioServidumbre;

@Local
public interface IPredioServidumbreDAO extends IGenericJpaDAO<PredioServidumbre, Long> {

    /**
     * Metodo encargado de buscar las servidumbres asociadas a un predio.
     *
     * @param predio Predio sobre el cual se realiza la búsqueda de servidumbres.
     * @return Retorna las servidumbres asociadas al predio.
     */
    public List<PredioServidumbre> getPredioServidumbreByPredio(Predio predio);

}
