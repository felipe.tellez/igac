/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;

@Local
public interface IUnidadConstruccionDAO extends IGenericJpaDAO<UnidadConstruccion, Long> {

    public UnidadConstruccion findUnidadConstruccionByPredioANDUnidad(Predio predio, String unidad);

    /**
     * Método que retorna las unidades de construccion asociadas a un predio
     *
     * @author franz.gamba
     * @param predioId
     * @return
     */
    public List<UnidadConstruccion> getUnidadsConstruccionByPredioId(Long predioId);

    /**
     * Método que retorna las unidades de construccion asociadas a un predio por su numero predial
     *
     * @author franz.gamba
     * @param numPred
     * @return
     */
    public List<UnidadConstruccion> getUnidadsConstruccionByNumeroPredial(String numPred);

    /**
     * Retorna las construcciones asociadas a un idde predio que no esten canceladas
     *
     * @author felipe.cadena
     * @param predioId
     * @return
     */
    public List<UnidadConstruccion> getUnidadsConstruccionByPredioIdNoCanceladas(Long predioId);
    
    /**
     * Retorna las construcciones asociadas a una lista de id predios
     * 
     * @author leidy.gonzalez
     * @param idPUnidadMod
     * @return 
     */
    public List<UnidadConstruccion> obtenerUnidadesConstruccionPorListaId(
			List<Long> idPUnidadMod);
}
