package co.gov.igac.snc.dao.tramite;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.RepConsultaPredial;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.util.DetallesDTO;
import co.gov.igac.snc.util.FiltroDatosConsultaCancelarTramites;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.TramiteConEjecutorAsignadoDTO;

import java.util.HashMap;

//TODO::pedro.garcia::eliminar todos mis métodos deprecated cuando ya no se usen definitivamente
@Local
public interface ITramiteDAO extends IGenericJpaDAO<Tramite, Long> {

    /**
     *
     * @param numeroRadicacion
     * @return
     */
    public Tramite findTramiteByNoRadicacion(String numeroRadicacion);

    /**
     * Método que muestra los detalles de un predio correspondiente a un Tramite, mediante su numero
     * de redicacion
     *
     * @param numeroRadicacion
     *
     * @return
     */
    public DetallesDTO detallesPredioDeTramite(Long tramiteId);

    /**
     * Retorna la lista de trámites asociados a un predio
     *
     * @param numeroPredial número predial del predio
     * @return
     */
    public List<Tramite> getTramitesByNumeroPredialPredio(String numeroPredial);

    /**
     * @author fredy.wilches Retorna el listado de trámites a partir del id de la solicitud
     * @param solicitudId
     * @return
     */
    public List<Tramite> findBySolicitud(Long solicitudId);

    /**
     * @author fredy.wilches Retorna el listado de trámites a partir del numero de la solicitud
     * @param numeroSolicitud
     */
    public List<Tramite> findBySolicitud(String numeroSolicitud);

    /**
     * Retorna la lista de predios en desenglobe de un tramite.
     *
     * Consulta sobre la tabla TramiteDetallePredio, pero en conservación esa tabla guarda las
     * asociaciones para englobes.
     *
     * @author pedro.garcia
     * @param tramiteId id del tramite
     * @return
     */
    public List<TramiteDetallePredio> buscarPredioEnglobesPorTramite(Long tramiteId);

    /**
     * Busca los trámites con id contenido en idsTramites. No se usa un método genérico para esta
     * consulta porque dependiendo del subproceso se necesita traer más o menos información del
     * trámite. No se usa la territorial como parámetro porque los ids de trámites me los da el bpm.
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Tramite> findTramitesParaAsignacionDeEjecutor(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        final int... rowStartIdxAndCount);

    /**
     * Cuenta los trámites que estén en la lista de ids que no tengan ejecutor asignado
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @return
     */
    public int countTramitesParaAsignacionDeEjecutor(long[] idsTramites);

    /**
     * Cuenta los trámites que estén en la lista de ids que no tengan ejecutor asignado
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @return
     */
    public int countTramitesEjecutorAsignado(long[] idsTramites);

    /**
     * Busca los trámites con id contenido en idsTramites. No se usa un método genérico para esta
     * consulta porque dependiendo del subproceso se necesita traer más o menos información del
     * trámite. No se usa la territorial como parámetro porque los ids de trámites me los da el bpm.
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param rowStartIdxAndCount opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Tramite> findTramitesConEjecutorAsignado(
        long[] idsTramites, final int... rowStartIdxAndCount);

    /**
     * Busca los trámites cuyos ids estén en el arreglo que se pasa como parámetro y que tengan el
     * campo 'comisionado' en NO. Estos trámites son los que el jefe de conservación ve en la
     * pantalla "Manejo de comisiones para trámites"
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     * @param sortOrder nombre del ordenamiento (UNSORTED, DESCENDING, DESCENDING)
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    public List<Tramite> findTramitesPorComisionar(long[] idsTramites, String sortField,
        String sortOrder, Map<String, String> filters, int... rowStartIdxAndCount);

    /**
     * Cuenta los trámites que estén en el arreglo idsTramites que tengan el campo 'comisionado' en
     * false. Estos trámites son los que el jefe de conservación ve en la pantalla "Manejo de
     * comisiones para trámites"
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     */
    public int countTramitesPorComisionar(long[] idsTramites, String tipoComision);

    /**
     * Recupera un tramite con los tramites inicializados
     *
     * @author juan.agudelo
     *
     */
    public Tramite buscarTramiteTramitesPorTramiteId(Long tramiteId);

    /**
     * Recupera un tramite con los tramites inicializados para proyeccion
     *
     * @param tramiteId
     * @author franz.gamba
     */
    public Tramite buscarTramitePorTramiteIdProyeccion(Long tramiteId);

    /**
     * Crea un trámite correspondiente a una solicitud de cotización de avalúo comercial.
     *
     * @author jamir.avila
     * @param tramite objeto con los datos para crear el trámite.
     * @return el objeto con los datos con los que se creó el trámite.
     */
    public Tramite crearSolicitudCotizacionAvaluo(Tramite tramite);

    /**
     * Cuenta los trámites que estén asignados a un ejecutor
     *
     * @author juan.agudelo
     * @param ejecutor es el nombre del ejecutor que ingresa al sistema
     * @return
     */
    @Deprecated
    public Integer contarTramitesDeEjecutorAsignado(String ejecutor);

    /**
     * Busca los trámites que estén asignados a un ejecutor
     *
     * @author juan.agudelo
     * @param idTerritorial
     * @param ejecutor
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    @Deprecated
    public List<Tramite> buscarTramitesDeEjecutorAsignado(String idTerritorial, String ejecutor,
        final int... rowStartIdxAndCount);

    /**
     *
     *
     * @param idTerritorial
     * @param ejecutor
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> buscarTramitesAsignadosAEjecutor(String idTerritorial, String ejecutor,
        final int... rowStartIdxAndCount);

    /**
     * Obtiene el tramite relacionado a un id determinado solo con los datos de municipio y
     * departamento asociados
     *
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public Tramite obtenerTramitePorIdReasignacion(Long tramiteId);

    /**
     * Busca los trámites asociados a una comision. Usa native query para solo traer lo necesario
     *
     * @author pedro.garcia
     * @param comisionId id de la comisión
     * @return
     */
    public List<Tramite> findByComisionId(Long comisionId);

    /**
     * Método que busca los trámites asociados a los id's de comisión enviados como parámetro.
     *
     * @author david.cifuentes
     * @param comisionesId Lista de id's de las comisiones.
     * @return
     */
    public List<Tramite> buscarTramitesDeComisiones(List<Long> comisionesId);

    /**
     * Devuelve un trámite a partir del id, para ser ejecutado. Se diferencia del método
     * sobreescrito en que se trae el predio, departemento, municipio y los trámites hijos
     * relacionados
     *
     * @author fredy.wilches
     * @param tramiteId
     * @return
     */
    @Override
    public Tramite findById(Long tramiteId);

    /**
     * Metodo para consultar la configuracion de las secciones para la ejecucion de un tramite
     *
     * @author fredy.wilches
     * @modified juan.agudelo
     */
    public TramiteSeccionDatos getConfiguracionTramite(String tipoTramite, String claseMutacion,
        String subtipo, String radicacionEspecial);

    /**
     * Método que cuenta los tramites que tiene un funcionario ejecutor
     *
     * @author javier.aponte
     */
    public Integer contarTramitesDeFuncionarioEjecutor(String ejecutor);

    /**
     * @author david.cifuentes Metodo que consulta un texto motivo teniendo el numero de motivo
     */
    public String findByNumeroMotivo(String numeroMotivo);

    /**
     * Método que permite archivar un trámite
     *
     * @author juan.agudelo
     * @param usuario
     * @param tramiteParaArchivar
     * @return
     */
    public boolean archivarTramite(UsuarioDTO usuario,
        Tramite tramiteParaArchivar);

    /**
     * Método que busca un tramite con Solicitud,SolicitanteTramite, TramiteDocumentacions, Predio y
     * TramiteEstado
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Tramite buscarTramiteSolicitudPorId(Long tramiteId);

    /**
     * busca los trámites que tienen asignado el funcionario ejecutor dado y queestán asignados a la
     * comisión dada
     *
     * @author pedro.garcia
     * @param idFuncionarioEjecutor
     * @param idComision
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     *
     * @return
     */
    public List<Tramite> findByEjecutorAndComision(String idFuncionarioEjecutor, Long idComision)
        throws ExcepcionSNC;

    /**
     * retorna la lista de predios asociados a un trámite
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public List<Predio> getPrediosOfTramite(Long tramiteId);

    /**
     * retorna la lista de predios asociados a un trámite, para predios de quinta nuevo finalizado
     * busca en hpredio, para no finalizado busca en ppredio
     *
     * @author carlos.ferro
     * @param tramiteId
     * @return
     */
    public List<Predio> getPrediosOfTramiteQuintaNuevo(Long tramiteId);

    /**
     * Consulta la documentación de un trámite. Trae el trámite habiéndole hecho fetch con los
     * TramiteDocumentacion y con la solicitud (para obtener datos de esta)
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite getDocsOfTramite(Long tramiteId);

    /**
     * Consulta la documentación completa de un trámite.
     *
     * @author juanfelipe.garcia
     * @param tramiteId
     * @return
     */
    public Tramite getDocumentacionCompletaTramite(Long tramiteId);

    /**
     * consulta los TramiteRectificaciones de este trámite
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<TramiteRectificacion> getTramiteRectificaciones(Long idTramite);

    /**
     *
     * Método que busca los Tramites a los que se les haya aplicado cierta resolución
     *
     * @author david.cifuentes
     *
     * @param resolucion
     * @param documentoEstado
     * @param documentoEstadoAdicional
     * @return
     */
    public List<Tramite> getTramitesByResolucion(String resolucion, String documentoEstado,
        String documentoEstadoAdicional);

    /**
     * Método que busca los trámites por el parametro resolucion y el numero predial
     *
     * @author david.cifuentes
     * @param resolucion , numeroPredial
     * @return
     */
    public List<Tramite> buscarTramitesConResolucionYNumeroPredialPredio(String resolucion,
        String numeroPredial);

    /**
     * Método que busca un Tramite por su id, trayendo sus Predios, Solicitantes y Documentación
     * asociada
     *
     * @author david.cifuentes
     * @param tramiteId
     * @return Tramite
     */
    public Tramite buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(Long tramiteId);

    /**
     * Método que obtiene una lista de tipo SEjecutoresTramites con los datos de los funcionarios
     * ejecutores que se envian como parametro, para formar la lista cuenta el número de trámites de
     * terreno y de oficina que tenga asignado cada funcionario ejecutor, tambien asigna el nombre
     * del funcionario y el login que tenga asociado
     *
     * @author javier.aponte
     * @param atributosEjecutores
     * @return
     */
    public List<SEjecutoresTramite> obtenerFuncionariosEjecutoresConNumeroDeTramitesDeTerrenoYOficinaPorFuncionarioEjecutor(
        List<UsuarioDTO> atributosEjecutores);

    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite para el
     * CU cargar pruebas
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public Tramite findTramiteCargarPruebasByTramiteId(Long tramiteId);

    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite
     *
     * @author juan.agudelo
     * @param solicitudId
     * @return
     */
    public Tramite findTramitePruebasByTramiteId(Long tramiteId);

    /**
     * Retorna el trámite y las entidades asignadas a pruebas solicitadas si existen a partir del id
     * del trámite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public Tramite findTramiteConTramitePruebaEntidadByTramiteId(Long tramiteId);

    /**
     * Método que retorna los trámites cuyo id es alguno de los que vienen en el parámetro, con sus
     * comisiones. </br>
     *
     * No se tienen en cuenta las comisiones cuyo estado sea 'finalizada', y se tienen en cuenta las
     * de tipo conservación
     *
     * @param idTramite ids de los trámites que se van a buscar
     * @return
     */
    /*
     * @modified pedro.garcia 30-05-2013 se excluyen las comisiones canceladas y finalizadas porque
     * los registros de la tabla Comision_Tramite no se borran al finalizar una comisión (las
     * canceladas no se miran por si acaso no se borraron esos registros; lo que sí debería hacerse)
     * nov-2013 sí se tienen en cuenta las canceladas por CC-NP-CO-034
     */
    public List<Tramite> getByIdsFetchComisionParaAlistarInfo(long idTramite[]);

    /**
     * Retorna un listado de trámites para los ids que entran como parámetro
     *
     * @param tramiteIds
     * @return
     */
    public List<Tramite> findTramitesByIds(List<Long> tramiteIds);

    /**
     * Retorna un listado de trámites para los ids que entran como parámetro (CU Aprobar trámites)
     *
     * @author franz.gamba
     * @param tramiteIds
     * @return
     */
    public List<Tramite> findTramitesByIdsParaAprobar(List<Long> tramiteIds);

    /**
     * Retorna un listado de trámites para los ids que entran como parámetro (Paginado)
     *
     * @param tramiteIds
     * @param sortField
     * @param sortOrder
     * @param contadoresDesdeYCuantos
     * @return
     */
    public List<Tramite> findTramitesByIds(List<Long> tramiteIds, String sortField, String sortOrder,
        Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Metodo que retorna los tramites asociados a un tramite mediante el id del Tramite padre
     *
     * @author franz.gamba
     * @param tramiteId
     *
     */
    public List<Tramite> getTramitesAsociados(Long tramiteId);

    /**
     * Método que retorna un trámite junto con la documentación asociada y la información de los
     * documentos
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Tramite findTramiteTramiteDocumentacionDocumentoById(Long tramiteId);

    /**
     * Busca el trámite dado su id, haciendo fetch de los datos que se encesitan para las pantallas
     * de asignación
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite findTramiteByIdForAsignacion(Long tramiteId);

    /**
     * Busca los trámites cuyo id sea alguno de los que viena en el arreglo, haciendo fetch de los
     * datos que se necesitan mostrar para el CU cargue de información al sistema.
     *
     * OJO: como lo que se busca con esta consulta es lrelacionar los trámites con las comisiones en
     * las que posiblemente esté, para dar el informe, se aplica el filtro de que tales comisiones
     * deben estar en estado 'EN_EJECUCION'. Si no se hiciera esto, mostraría datos de comisiones
     * que ya fueron finalizadas o canceladas.
     *
     * @author pedro.garcia
     * @modified felipe.cadena - 18/12/2013 - se agrega parametro tipo de comision para filtrar los
     * tramites de depuracion o consevacion
     *
     * @param idsTramites
     * @param tipoComision
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> findTramitesParaCargueInfo(long[] idsTramites, String tipoComision,
        final int... rowStartIdxAndCount);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPorTramiteId(Long tramiteId);

    /**
     * Método que recupera una lista de predios basados en una lista de id's de trámites.
     *
     * @author david.cifuentes
     * @param idsTramites
     * @return
     */
    public List<Predio> buscarPrediosPorListaDeIdsTramite(List<Long> idsTramites);

    /**
     * Método que busca un trámite por id recuperando adicionalmente la información base del predio
     * o de los predios segun corresponda, no recupera información de objetos asociados a los
     * predios
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Tramite findTramitePrediosSimplesByTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle del avalúo
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelAvaluoPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de Trámites de una lista de actividades que están en la
     * actividad ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA
     *
     * @author david.cifuentes
     * @param actividades
     * @return
     */
    public List<Tramite> recuperarTramitesConDocumentosFaltantes(List<Actividad> actividades);

    /**
     * Método que busca trámites por el campo estado
     *
     * @author david.cifuentes
     * @param estado
     * @return
     */
    public List<Tramite> buscarTramitesPorEstado(String estado);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle justificación de propiedad
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelJPropiedadPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle propietarios y / o poseedores
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelPropietariosPorTramiteId(
        Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle ficha matriz unicamente si el tramite es una mutación de
     * segunda y desenglobe
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelFichaMatrizPorTramiteId(Long tramiteId);

    /**
     * * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle ubicación predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId);

    /**
     * Método que permite recuperar el trámite y trae el documento resolución asociado
     *
     * @author javier.aponte
     * @param currentTramiteId
     * @return Tramite
     */
    public Tramite buscarTramiteTraerDocumentoResolucionPorIdTramite(Long currentTramiteId);

    /**
     * Retorna el id en el repositorio de documentos del documento -replica GDB- de tipo
     * idTipoDocumento para el trámite con id tramiteId. <br />
     * Por el nombre dado al método, se usa para obtener documentos que sean réplicas de un trámite.
     *
     * @author lorena.salamanca
     * @param tramiteId
     * @param idTipoDocumento
     */
    /*
     * @documented pedro.garcia
     */
    public String getIdRepositorioDocumentosDeReplicaGDBdeTramite(Long tramiteId,
        Long idTipoDocumento);

    /**
     * Busca un trámite por su id haciendo fetch del Documento resultado que es donde se guarda la
     * resolución
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite buscarTramiteConResolucionSimple(Long tramiteId);

    /**
     * Busca un trámite por su id haciendo fetch del Documento resultado que es donde se guarda la
     * resolución y de todos los demás datos que necesita para ver detalles del trámite
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite buscarTramiteConResolucionFull(Long tramiteId);

    /**
     * Busca los trámite por su id haciendo fetch del Documento resultado que es donde se guarda la
     * resolución y de todos los demás datos que necesita para ver detalles del trámite
     *
     * @author pedro.garcia
     * @param tramitesIds
     * @return
     */
    public List<Tramite> buscarTramitesConResolucionFull(long[] tramitesIds);

    /**
     * Método que consulta una lista de trámites susceptibles de cancelación a partir de los datos
     * ingresados en el filtro
     *
     * <b>OJO: solo usar para la búsqueda de trámites para cancelación porque además de los datos en
     * el filtro, filtra también por ciertos estados de los trámites</b>
     *
     * @param filtro
     * @return
     * @author fabio.navarrete
     */
    /*
     * @documented pedro.garcia
     */
    public List<Tramite> findTramitesCancelacion(
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize);

    /**
     * Método que retorna el contéo de la búsqueda de trámites para cancelación.
     *
     * <b>OJO: solo usar para la búsqueda de trámites para cancelación porque además de los datos en
     * el filtro, filtra también por ciertos estados de los trámites</b>
     *
     * @param filtro
     * @return
     * @author fabio.navarrete
     */
    public int countTramitesCancelacion(FiltroDatosConsultaCancelarTramites filtro);

    /**
     * Método que retorna el contéo de la búsqueda de trámites para cancelación.
     *
     * <b>OJO: solo usar para la búsqueda de trámites para cancelación porque además de los datos en
     * el filtro, filtra también por ciertos estados de los trámites</b>
     *
     * @param filtro
     * @return
     * @author fabio.navarrete
     */
    public int countTramitesCancelacionOtraTerritorial(FiltroDatosConsultaCancelarTramites filtro);

    /**
     * Busca los trámites que estén en ejecución y relacionados con un predio
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId
     * @return
     */
    public List<Tramite> lookForCurrentOnPredio(Long predioId);

    /**
     * Busca los trámites que ya hayan sido finalizados relacionados con un predio. La condición
     * para determinar si el trámite fue finalizado en que el campo 'resultadoDocumento' no sea
     * null, y que el estado del trámite sea "finalizado".
     *
     * En este se hace fetch del documento de resolución.
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId
     * @return
     */
    public List<Tramite> lookForFinishedOnPredio(Long predioId);

    // -------------------------------------------------------------------------
    /**
     * Método encargado de buscar un trámite con el predio asociado, la solicitud
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    Tramite findTramitePredioSolicitudByTramiteId(Long tramiteId);

    // -------------------------------------------------------------------------
    /**
     * Busqueda de trámites por filtro
     *
     * @author juan.agudelo
     * @version 2.0
     * @cu CU-NP-CO-166
     * @param filtroBusquedaTramite
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> findTramiteSolicitudByFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        final int... rowStartIdxAndCount);

    // -------------------------------------------------------------------------
    /**
     * Contar trámites por filtro
     *
     * @author juan.agudelo
     * @version 2.0
     * @cu CU-NP-CO-166
     * @param filtroBusquedaTramite
     * @return
     */
    public Integer countTramiteSolicitudByFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite);

    /**
     * Metodo que retorna los tramites sin ejectutor asignado para envio como saldos de conservacion
     * para la actualizacion
     *
     * @param tramiteIds
     * @return
     */
    public List<Tramite> getTramitesSaldosDeConservacion(List<Long> tramiteIds);

    /**
     * obtiene la lista completa de solicitantes y afectados de un trámite por trámite id
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tramiteId
     * @return
     */
    public List<Solicitante> getSolicitantesParaNotificarByTramiteId(long tramiteId);

    /**
     *
     * Método que busca el trámite asociado al número de resolución enviado como parametro.
     *
     * @author david.cifuentes
     *
     * @param resolucion
     * @return
     */
    public List<Tramite> buscarTramitesPorNumeroDeResolucion(String resolucion);

    /**
     * Método que busca los trámite asociados a una solicitud
     *
     * @author carlos.ferro
     *
     * @param numSolicitud
     * @return
     */
    public List<Tramite> buscarTramitesPorNumeroDeSolicitud(String numSolicitud);

    /**
     * Método que recupera un predio inicializando los objetos departamento y municipio por
     * tramiteId
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tramiteId
     * @return
     */
    public Predio getPredioDptoMuniByTramiteId(Long tramiteId);

    /**
     * Método que recupera los documentos correspondientes a tramiteDocumentos asociados a un
     * trámite por trámite id
     *
     * @author juan.agudelo
     * @version 2.0
     * @return
     */
    public List<TramiteDocumento> getTramiteDocumentosByTramiteId(Long tramiteId);

    /**
     * Método que recupera los documentos correspondientes a tramiteDocumentacions asociados a un
     * trámite por trámite id
     *
     * @author juan.agudelo
     * @version 2.0
     * @return
     */
    public List<TramiteDocumentacion> getTramiteDocumentacionsByTramiteId(
        Long tramiteId);

    /**
     * Método que retorna un ejecutor con el numero de tramites que tiene según su clasificación
     *
     * @author franz.gamba
     * @param tramiteIds
     * @return
     */
    public SEjecutoresTramite getEjecutorTramiteClasificacion(List<Long> tramiteIds);

    /**
     * Método que retorna los trámites asociados a un ejecutor con los datos que debe mostrar para
     * el servicio WEB
     *
     * @author franz.gamba
     * @param tramiteIds
     * @return
     */
    public List<Tramite> getTramitesDeActGeograficas(List<Long> tramiteIds);

    /**
     * Método que retorna los ids de los trámites que se encuentran en la actividad
     * registrarNotificacion
     *
     * @author franz.gamba
     * @return
     */
    /**
     * @modified by leidy.gonzalez::08-08-2018::#70681::Reclamar actividad por parte del usuario autenticado
     */ 
    public List<Long> findTramiteIdsDeTramitesParaNotificar(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String nombreActividad,List<Long> iDsTramitesProc,
        final int... rowStartIdxAndCount);

    /**
     * Método que retorna los tramites para notificar mediante los Ids de los trámites
     *
     * @author franz.gamba
     * @param tramiteIds
     * @param sortField
     * @param sortOrder
     * @param filters
     * @param contadoresDesdeYCuantos
     * @return
     */
    public List<Tramite> findTramitesParaNotificarByIds(List<Long> tramiteIds,
        String sortField, FiltroDatosConsultaTramite filtroBusquedaTramite, String sortOrder,
        Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Método que retorna los ids de los tramites asociados a un trámite mediante el id del trámite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<Long> getIdsDeTramitesAsociadosByTramiteId(Long tramiteId);

    /**
     * Método que retorna el objeto completo de tramite de los tramites asociados a un tramite
     *
     * @author lorena.salamanca
     * @param tramiteId
     * @return
     */
    public List<Tramite> getTramiteTramitesbyTramiteId(Long tramiteId);

    /**
     * Método que valida que el tramite exista en BD segun su id
     *
     * @param tramiteId
     * @return
     */
    public boolean tramiteExists(Long tramiteId);

    // -------------------------------------------------------------------------
    /**
     * Busqueda de trámites por filtro para el caso de uso de administración de asignaciones
     *
     * @version 2.0
     * @cu CU-NP-CO-171 Administración de asignaciones
     * @param filtroBusquedaTramite
     * @param rowStartIdxAndCount
     * @param sortField
     * @param sortOrder
     * @return
     * @author javier.aponte
     */
    public List<Tramite> findTramiteByFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount);

    // -------------------------------------------------------------------------
    /**
     * Cuenta el número de trámites por filtro para el caso de uso de administración de asignaciones
     *
     * @version 1.0
     * @cu CU-NP-CO-171 Administración de asignaciones
     * @param filtroBusquedaTramite
     * @return
     * @author javier.aponte
     */
    public Integer countTramiteByFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite);

    /**
     * Retorna la lista de trámites asociados a un predio
     *
     * @param departamentoCod número predial del predio
     * @param municipioCod número predial del predio
     * @param zona número predial del predio
     * @author javier.barajas
     * @return
     */
    public List<Tramite> obtenerTramitesByNumeroPredialPredio(String numPredial);

    /**
     *
     * Método que busca una lista de trámites para los ids de la lista pasada como parametro,
     * haciendo fetch del predio, la solicitud y los solicitantes.
     *
     * @author david.cifuentes
     *
     * @param tramiteIds
     * @return
     */
    public HashMap<Long, Tramite> buscarTramitesPorListaDeIds(List<Long> tramiteIds);

    /**
     * Método encargado de contar la cantidad de trámites que satisfacen los criterios de busqueda
     * para asociar derecho de petición o tutela
     *
     * @author javier.aponte
     */
    public Integer countTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante);

    /**
     * Método encargado de devolver los trámites que satisfacen los criterios de busqueda para
     * asociar derecho de petición o tutela
     *
     * @author javier.aponte
     */
    public List<Tramite> findTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante, String sortField, String sortOrder,
        final int... rowStartIdxAndCount);

    /**
     * Método que obtiene un trámite con la información necesaria para la edición geográfica.
     *
     * @param tramiteId identificador del tramite
     * @return Trámite con info de depto, municipio, tramiteDetallePredio y Predio asociado al
     * trámite
     *
     * @author andres.eslava
     */
    public Tramite getTramiteParaEdicionGeografica(Long tramiteId);

    /**
     * Mètodo para obtener los tramites vigentes en una manzana
     *
     * @authore felipe.cadena
     * @param numeroManzana
     * @return
     */
    public List<Tramite> obtenerTramitesPorManzanaEnEdicion(String numeroManzana);

    /**
     * Mètodo para obtener los tramites vigentes de depuración en una manzana que esten en edicion
     * geografica, es decir que tengan copia de edicion codigo 97
     *
     * @authore felipe.cadena
     *
     * @param numeroManzana
     * @return
     */
    public List<Tramite> obtenerTramitesPorManzanaDepuracionEnEdicion(String numeroManzana);

    /**
     *
     * Método que verifica de una lista de ids cuales tienen interpuesta una via gubernativa.
     *
     * Para esto se debe tener en cuenta si la solicitud es de via gubernativa, o si la solicitud
     * tiene referenciado el número de radicación del trámite en el campo tramiteNumeroRadicacionRef
     *
     * @author david.cifuentes
     *
     * @param tramiteIds
     * @return
     */
    public List<Tramite> buscarTramiteConViaGubernativaPorIdsTramites(
        List<Long> tramiteIds);

    /**
     * Consulta un trámite para la actividad 'revisar trámite devuelto'. </br>
     * Trae la documentación del trámite, la solicitud y la comisión de conservación a la que
     * pertenece, si es de oficina.
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite consultarParaTramiteDevuelto(Long tramiteId);

    /**
     * Busca tramites para recuperar
     *
     * @author felipe.cadena
     *
     * @param filtroBusquedaTramite
     * @param filtrosSolicitante
     * @param sortField
     * @param sortOrder
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> bucarTramiteByFiltroTramiteRecuperacionTamite(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount);

    /**
     * Metodo que retorna el numero de tramites asignados a un ejecutor de una clasificacion
     * determinada Solo se toman en cuanta los tramites que estan activos en el sistema y que aun no
     * se le ha generado resolucion
     *
     * @author felipe.cadena
     *
     * @param ejecutor
     * @param clasificacion
     * @return
     */
    public Long obtenerTramitesPorEjecutorYClasificacion(String ejecutor, String clasificacion);

    /**
     * Busca los trámites con id contenido en idsTramites.
     *
     * @author javier.aponte
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Tramite> findTramitesParaDeterminarProcedenciaSolicitud(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        final int... rowStartIdxAndCount);

    /**
     * Cuenta los trámites que estén en la lista de ids para determinar procedencia de la solicitud
     *
     * @author javier.aponte
     * @param idsTramites arreglo con los id de trámite
     * @return
     */
    public int countTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites);

    /**
     * Método encargado de consultar los trámites que tienen los ids como parámetros que se envian
     *
     * @param idsTramites
     * @return Lista de los trámites con ejecutor asignado
     * @author javier.aponte
     */
    public List<TramiteConEjecutorAsignadoDTO> findIdsClasificacionTramitesConEjecutor(
        long[] idsTramites);

    /**
     * Metodo para obtener los tramites por filtro necesarios para la administracion de asignaciones
     *
     * @author felipe.cadena
     *
     * @param filtroBusquedaTramite
     * @param filtrosSolicitante
     * @param sortField
     * @param sortOrder
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> buscarTramiteByFiltroTramiteAdminAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount);

    /**
     * Método encargado de consultar los trámites que tienen los ids como parámetros que se envian
     *
     * @param tramitesIds
     * @return Lista de los trámites de actualizacion
     * @author leidy.gonzalez
     */
    public List<Tramite> buscarTramitesActualizacionAgrupadoPorPredio(long[] tramitesIds);

    /**
     * Metodo para obtener los tramites del proceso de actualizacon relacionados a los predios del
     * parametro
     *
     * @author felipe.cadena
     *
     * @param tramitesIds
     * @return
     */
    public List<Tramite> buscarTramitesEnActualizacion(List<Long> prediosIds);

    /**
     * Retorna los tramites de conservacion de una lista de tramites
     *
     * @author leidy.gonzalez
     *
     * @param tramitesIds
     * @return
     */
    public List<Tramite> buscarTramitesConservacionPorIdTramites(List<Long> tramiteIds);

    /**
     * Consulta la documentación de un trámite que genero el sistema. Trae el trámite habiéndole
     * hecho fetch con los TramiteDocumento (para obtener datos de esta)
     *
     * @author leidy.gonzalez
     * @param tramiteId
     * @return
     */
    public List<Tramite> obtenerDocumentosGeneradosPorIdTramite(Long tramiteId);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial y final
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial, final y
     * fecha en que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucion(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial que
     * aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Obtiene el tramite por el id sin ningÃºn dato adicional, este mÃ©todo se creo para que sea
     * mÃ¡s liviano y se use sÃ³lo cuando se necesiten datos bÃ¡sicos del trÃ¡mite
     *
     * @author javier.aponte
     * @param tramiteId
     * @return
     */
    public Tramite findOnlyTramiteByIdTramite(Long tramiteId);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     * en que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucion(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion en
     * que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Retorna la lista de predios en desenglobe de un predio.
     *
     * Consulta sobre la tabla TramiteDetallePredio, pero en conservación esa tabla guarda las
     * asociaciones para englobes.
     *
     * @author leidy.gonzalez
     * @param predioId del tramite
     * @return
     */
    public List<TramiteDetallePredio> buscarPredioEnglobesPorPredio(Long predioId);

    public List<Tramite> obtenerTramitesTiposTramitesByNumeroIdentificacion(String numIdentificacion,
        String tipoIdentificacion);

    /**
     * Retorna la lista de trámites asociados a un predio pero de todos los estados en los que se
     * encuentre.
     *
     * @param numPredial
     * @author dumar.penuela
     * @return
     */
    public List<Tramite> obtenerTramitesTiposTramitesByNumeroPredial(String numPredial);

    /*
     * @documented cesar.vega
     */
    public List<Tramite> findTramitesCancelacionOtraTerritorial(
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize);

    /**
     * Metodo para consultar la configuracion del tipo de informe para visualizar en la consulta
     * avanzada
     *
     * @author leidy.gonzalez
     *
     * @param tipoInforme
     * @return
     */
    public RepConsultaPredial getConfiguracionReportePredial(Long tipoInforme);

    /**
     * Método que realiza la consulta de tramites para edición geográfica basado en una lista de
     * ids, y que estos tengan un producto catatral job sin terminar geográficamente.
     *
     * @author david.cifuentes
     * @param idsTramite
     * @return
     */
    public List<Tramite> getTramiteParaEdicionGeograficaSinProductoCatastralJobTerminado(
        List<Long> idsTramite);

    /**
     *
     * MÃ©todo que busca los trÃ¡mite asociados a una solicitud
     *
     * @author carlos.ferro
     *
     * @param numSolicitud
     * @return
     */
    public void asociarInfoCampoAdicional(EInfoAdicionalCampo campo, String valor);

    /**
     * Retorna lista con los tipos de tramites disponibles en la BD
     *
     * @return
     *
     * @author felipe.cadena
     */
    public List<String[]> getTipoTramiteCompuestoCodigo();

//end of interface        
}
