package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;

/**
 * Interface que contiene metodos de consulta sobre entity ProfesionalAvaluo
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IProfesionalAvaluoDAO extends IGenericJpaDAO<ProfesionalAvaluo, Long> {

    /**
     * Obtiene el avaluador {@link ProfesionalAvaluo} con el id especificado. Carga el departamento,
     * municipio, territorial y profesion
     *
     * @author christian.rodriguez
     * @param idAvaluador id del avaluador
     * @return avaluador con el is asociado, null si no lo encuentra
     */
    /*
     * @modified pedro.garcia 25-09-2012 Hace fetch de la Profesion
     */
    public ProfesionalAvaluo obtenerByIdConAtributos(Long idAvaluador);

    /**
     * Obtiene los profesionales asociados al avaluo con el sec radicado especificado y que se
     * encuentren realizando al actividad especificada Carga los {@link ProfesionalAvaluosContrato}
     * asociados
     *
     * @author christian.rodriguez
     * @param secRadicado Cadena de texto con el secradicado del avaluo
     * @param actividad Actividad en la que se encuentra el profesional, corresponde a un valor del
     * dominio {@link EAvaluoAsignacionProfActi}
     * @return Lista con los profesionales asociados al avaluo
     */
    public List<ProfesionalAvaluo> consultarPorSecRadicadoYActividad(String secRadicado,
        String actividad);

    /**
     * Método para buscar profesionales por un patron de busqueda relacionado al nombre y a una
     * territorial, si los valores de los parametros son nulos no se consideran en la consulta.
     *
     * @author felipe.cadena
     *
     * @param nombreProfesional - Nombre del avaluador que se esta buscando
     * @param codigoTerritorial - Territorial para filtrar la consulta
     * @return - Lista de profesionales de avalúos, lista vacia si no hay coincidencias
     */
    public List<ProfesionalAvaluo> buscarProfesionalesPorNombreYterritorial(String nombreProfesional,
        String codigoTerritorial);

    /**
     * Método que busca los {@link ProfesionalAvaluo} según un
     * {@link FiltroDatosConsultaProfesionalAvaluos}. Carga el
     * {@link Departamento}, {@link Municipio} y {@link EstructuraOrganizacional}
     *
     * @author christian.rodriguez
     * @param filtro {@link FiltroDatosConsultaProfesionalAvaluos} que contiene los datos de
     * busqueda
     * @return Lista con los {@link ProfesionalAvaluo} encontrados o null si no encuentra nada
     */
    public List<ProfesionalAvaluo> buscarPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtro);

    /**
     * Método para buscar profesionales por el número de identificación
     *
     * @author felipe.cadena
     *
     * @param identificacion
     * @return
     */
    public ProfesionalAvaluo obtenerPorIdentificacion(String identificacion);

    /**
     * Método para consultar un profesional por su usuario de sistema (LDAP) y su número de
     * identificación de LDAP
     *
     * @author christian.rodriguez
     * @param usuarioSistema cadena de texto con el usuario ldap
     * @param numeroIdentificacion cadena de texto con el numero de identificacion que tiene el
     * usuario ldap
     * @return {@link ProfesionalAvaluo} profesional encontrado o null si no se encontró
     */
    public ProfesionalAvaluo consultarPorUsuarioSistemaYNumeroIdentificacion(
        String usuarioSistema, String numeroIdentificacion);

    /**
     * Método para obtener los avaluadores asociados a un interventor
     *
     * @author rodrigo.hernandez
     *
     * @param idInterventor - Id del interventor
     * @param banderaEsAvaluadorExterno - bandera que indica si se estan buscando avaluadores
     * externos o por nomina
     */
    public List<ProfesionalAvaluo> obtenerAvaluadoresPorInterventorId(
        Long idInterventor, boolean banderaEsAvaluadorExterno);

}
