package co.gov.igac.snc.dao.tramite;

import javax.ejb.Local;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 *
 * @author juan.mendez
 *
 */
@Local
public interface ITramiteMovilesDAO extends IGenericJpaDAO<Tramite, Long> {

    /**
     * Obtiene la ficha predial asociada a un trámite específico Incluye la ficha digital y la ficha
     * escaneada
     *
     * @param idTramite
     */
    public String obtenerFichaPredialCompleta(Long idTramite);

    /**
     * Genera un archivo xml con la información del trámite
     *
     * @param idTramite
     * @return ruta física del archivo generado
     */
    public String getXmlTramiteByTramiteId(Long idTramite);

    /**
     * Genera un archivo xml con la información del predio
     *
     * @param idPredio
     * @return ruta física del archivo generado
     */
    public String getXmlPredioByPredioId(Long idPredio);

    /**
     * Genera un archivo de imágen con el mapa (ortofoto) asociada al tramite
     *
     * @param idTramite
     * @return ruta física del archivo generado
     */
    public String generarOrtofotoDeTramite(UsuarioDTO usuario, ArcgisServerTokenDTO token,
        String identificadoresPredios);

}
