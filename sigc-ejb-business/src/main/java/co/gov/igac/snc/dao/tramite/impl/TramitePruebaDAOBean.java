package co.gov.igac.snc.dao.tramite.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteEstadoDAO;
import co.gov.igac.snc.dao.tramite.ITramitePruebaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class TramitePruebaDAOBean extends
    GenericDAOWithJPA<TramitePrueba, Long> implements ITramitePruebaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramitePruebaDAOBean.class);

    @EJB
    private ITramiteEstadoDAO remoteTramiteEstado;

    /**
     *
     */
    @Override
    public TramitePrueba guardarActualizarTramitePruebas(TramitePrueba tp,
        UsuarioDTO usuario, String estadoFuturoTramite) {

        LOGGER.debug("guardarActualizarTramitePruebas Inicio...");
        TramitePrueba tpResultado = null;
        try {
            tp.setUsuarioLog(usuario.getLogin());
            tp.setFechaLog(new Date());
            tpResultado = update(tp);

            if (estadoFuturoTramite != null && !estadoFuturoTramite.isEmpty()) {
                TramiteEstado te = new TramiteEstado();
                te.setEstado(estadoFuturoTramite);
                te.setTramite(tp.getTramite());
                te.setFechaInicio(new Date());
                te.setUsuarioLog(usuario.getLogin());
                te.setFechaLog(new Date());

                this.remoteTramiteEstado.crearActualizarTramiteEstado(te,
                    usuario);
            }
            LOGGER.debug("guardarActualizarTramitePruebas Fin...");

        } catch (Exception e) {
            LOGGER.error("guardarActualizarTramitePruebas error...");
            LOGGER.error(
                "error en TramitePruebaDAOBean#TramitePrueba " +
                e.getMessage(), e);
        }

        return tpResultado;
    }
}
