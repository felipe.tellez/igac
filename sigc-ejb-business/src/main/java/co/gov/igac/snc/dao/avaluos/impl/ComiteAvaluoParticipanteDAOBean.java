package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IComiteAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IComiteAvaluoParticipanteDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoParticipante;

/**
 * @see IComiteAvaluoDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ComiteAvaluoParticipanteDAOBean extends
    GenericDAOWithJPA<ComiteAvaluoParticipante, Long> implements
    IComiteAvaluoParticipanteDAO {
}
