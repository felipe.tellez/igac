package co.gov.igac.snc.dao.generales;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

/**
 * Acceso a datos para la entidad ProductoCatastralJob
 *
 * @author juan.mendez
 *
 */
@Local
public interface IProductoCatastralJobDAO extends IGenericJpaDAO<ProductoCatastralJob, Long> {

    /**
     * Crea un Job para programar la tarea geográfica encargada de Generar la copia de datos
     * geográficos a ser editados durante el proceso en SNC
     *
     * @param tramite
     * @param identificadoresPredios
     * @param tipoMutacion
     * @param usuario
     * @return
     */
    public ProductoCatastralJob crearJobGenerarDatosEdicion(Tramite tramite,
        String identificadoresPredios,
        String tipoMutacion, UsuarioDTO usuario);

    /**
     * Crea un Job para programar la tarea geográfica encargada de exportar la copia de datos
     * geográficos asociados a un trámite en formato shapefile o filegeodatabase
     *
     *
     * @param tramite
     * @param identificadoresPredios
     * @param tipoMutacion
     * @param formato
     * @param usuario
     * @return
     */
    public ProductoCatastralJob crearJobExportarDatosEdicion(Tramite tramite,
        String identificadoresPredios,
        String tipoMutacion, String formato, UsuarioDTO usuario);

    /**
     *
     * @param identificadorProducto
     * @param formato
     * @param codigosEntidadesGeograficas
     * @param usuario
     * @return
     */
    //public ProductoCatastralJob crearJobGenerarProductosDatos(String identificadorProducto, String formato, String codigosEntidadesGeograficas, UsuarioDTO usuario);
    /**
     * Crea un Job para programar la tarea geográfica encargada de Cargar los cambios a los datos
     * geográficos resultantes durante la ejecución del proceso catastral.
     *
     * @param tramite
     * @param idArchivoSistemaDocumental
     * @param usuario
     * @return
     */
    public ProductoCatastralJob crearJobFinalizarEdicionTramite(Tramite tramite,
        String idArchivoSistemaDocumental, UsuarioDTO usuario);

    /**
     * Crea un Job para programar la tarea geográfica encargada de eliminar la versión de SDE
     * asociada a un trámite
     *
     * @param nombreVersion nombre de la versión a eliminar
     * @param usuario
     * @param tramite
     * @return
     */
    public ProductoCatastralJob crearJobEliminarVersionSDE(String nombreVersion, UsuarioDTO usuario,
        Tramite tramite);

    /**
     * Crea un Job para exportar imagen a partir de los datos geograficos
     *
     * @param codigo codigo a exportar
     * @param tipoImagen tipo de imagen
     * @param usuario
     * @return
     */
    public ProductoCatastralJob crearJobExportarImagen(String codigo, String tipoImagen,
        UsuarioDTO usuario);

    /**
     * Obtiene la lista de jobs que completaron su ejecución en el SIG y que tienen pendiente ser
     * completados en Java
     *
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsPendientesEnSNC();

    public List<ProductoCatastralJob> obtenerJobsSIGConErrores();

    public List<ProductoCatastralJob> obtenerJobsSNCConErrores();

    public void reprogramarEjecucionJob(ProductoCatastralJob job);

    /**
     * Obtiene una lista de jobs pendientes por procesar en SNC (Los jobs están organizados por
     * orden de creación)
     *
     * @param cantidad de registros a obtener
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsPendientesEnSNC(Long cantidad);

    /**
     * Actualiza el estado de un job ( Maneja su propia transacción )
     *
     * @param job
     * @param nuevoEstado
     * @deprecated replaced by
     * {@link #actualizar( ProductoCatastralJob job, String nuevoEstado, String nuevoResultado)}
     */
    @Deprecated
    public void actualizarEstado(ProductoCatastralJob job, String nuevoEstado);

    /**
     * Actualiza el estado de un job ( Maneja su propia transacción )
     *
     * @param job
     * @param nuevoEstado
     * @deprecated replaced by
     * {@link #actualizar( ProductoCatastralJob job, String nuevoEstado, String nuevoResultado)}
     */
    @Deprecated
    public void actualizarResultado(ProductoCatastralJob job, String nuevoResultado);

    /**
     *
     * @param job
     * @param nuevoEstado
     * @param nuevoResultado
     * @return
     */
    public ProductoCatastralJob actualizar(ProductoCatastralJob job, String nuevoEstado,
        String nuevoResultado);

    /**
     * Obtiene la lista de los jobs de arcgis server que están esperando hace más de n horas y una
     * cantidad de jobs especifica.
     *
     * @param horas cantidad de horas que lleva en el estado de esperando
     * @param cantidadJobs cantidad maxima de jobs a reenviar
     * @return lista de jobs resultado del query
     * @author andres.eslava
     */
    public List<ProductoCatastralJob> obtenerJobsGeograficosEsperando(int horas, int cantidadJobs,
        int numeroReintentos);

    /**
     * Obtiene la lista de los jobs de arcgis server que están en error hace una cantidad de horas y
     * una cantidad de jobs especifica.
     *
     * @param horas cantidad de horas que lleva en el estado de error
     * @param cantidadJobs cantidad maxima de jobs a reenviar
     * @return lista de jobs resultado del query
     * @author andres.eslava
     */
    public List<ProductoCatastralJob> obtenerJobsGeograficosError(int horas, int cantidadJobs,
        int numeroReintentos);

    /**
	 * Obtiene la lista de los jobs de arcgis server que generaron la imagen predial.
	 * @param horas cantidad de horas que lleva en el estado de esperando
         * @param cantidadJobs cantidad maxima de jobs a reenviar 
	 * @return lista de jobs resultado del query
         * @author carlos.ferro
	 */
	 
	public List<ProductoCatastralJob>  obtenerJobsGeograficosImagenPredialTerminado(int horas, int cantidadJobs, int numeroReintentos);
    
    /**
     * Obtiene la lista de los jobs asociados a los parametros
     *
     *
     * @author felipe.cadena
     * @param estadoJob
     * @param tipoJob
     * @param tramiteId
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsGeograficosPorTramiteTipoEstado(Long tramiteId,
        String tipoJob, String estadoJob);
    
    /**
     * 
     * @param tipoProducto
     * @param usuarioLog
     * @return 
     */
    public ProductoCatastralJob obtenerJobsCargueActualizacion(String tipoProducto, String usuarioLog);

    /**
     * Metodo para obtener los jobs asociados a un tramite y con unos estado determinados
     *
     * @author felipe.cadena
     * @param estados
     * @param tramiteId
     * @param tipoJob
     * @return
     */
    public List<ProductoCatastralJob> obtenerPorEstadoTramiteIdTipo(List<String> estados,
        Long tramiteId, String tipoJob);

    /**
     * Obtiene el ultimo job de un tipo en particular para un tramite
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param tipo
     * @return
     */
    public ProductoCatastralJob obtenerUltimoJobPorTramite(Long tramiteId, String tipo);

    /**
     * Obtener los jobs de aplicar Cambios en estado AGS_ESPERANDO para ser enviados por primera vez
     *
     * author andres.eslava
     */
    public List<ProductoCatastralJob> obtenerJobsEsperandoUnoPorManzana(long Cantidad) throws
        Exception;

    /**
     * Crear job para el proceso asincronico de validacion de geometrias
     *
     * @param identificadoresPredios cadena con los numeros prediales para validar las
     * inconsistencias
     * @param tramite identificador del predio
     * @param usuario usuario que esta solicitando la validacion de inconsistencias
     * @param tipoMutacion enumeracion del tipo de mutacion
     * @author andres.eslava
     */
    public ProductoCatastralJob crearJobValidacionInconsistencias(String identificadoresPredios,
        Tramite tramite, UsuarioDTO usuario, String tipoMutacion, String radicacionEspecial);
    
    /**
     * Crear job para el proceso asincronico de validacion de geometrias
     *
     * @param identificadoresPredios cadena con los numeros prediales para validar las
     * inconsistencias
     * @param tramite identificador del predio
     * @param usuario usuario que esta solicitando la validacion de inconsistencias
     * @param tipoMutacion enumeracion del tipo de mutacion
     * @author andres.eslava
     */
    public ProductoCatastralJob crearJobObtenerZonasHomogeneas(String identificadoresPredios,
        Tramite tramite, UsuarioDTO usuario, String tipoMutacion, String URLReplica);
    
    /**
     * Crear job para el proceso asincronico de validacion de geometrias
     *
     * @param ubicacionArchivo ruta del archivo bd actualización geográfica en FTP
     * @param validacionPredios identificaor de validación de predios
     * @param validacionZonas identificador de validación de zonas
     * @param usuario usuario que carga el archivo
     * @author hector.arias
     */
    public ProductoCatastralJob crearJobValidacionInconsistenciasActualizacion(String enviroment, String ubicacionArchivo,
        String validacionPredios, String validacionZonas, UsuarioDTO usuario, String departamentoSeleccionado, String municipioSeleccionado);

    /**
     * Obtiene un diccionario con las zonas a partir de la columna resultado del producto catastral
     * job.
     *
     * @param idJob identificador del job
     * @return mapa que representa las zonas de los predios enviadas al servicios.
     * @author andres.eslava
     */
    public HashMap<String, List<ZonasFisicasGeoeconomicasVO>> obtenerXMLResultadoObtenerZonasHomegeneas(
        Long idJob);

    /**
     * Obtiene el ultimo job de obtener zonas para un identificador de tramite en especifico
     *
     * @pàrams idTramite identificador del tramite
     * @return ulitmo job de obtener zonas homogeneas
     * @author andres.eslava
     */
    public ProductoCatastralJob obtenerUltimoJobObtenerZonasSinTerminarGeograficamente(
        Long idTramite) throws Exception;

    /**
     * Obtiene la lista de los jobs de jasper server que están esperando, una cantidad de jobs
     * especifica.
     *
     * @param cantidadJobs cantidad maxima de jobs a reenviar
     * @return lista de jobs resultado del query
     * @author javier.aponte
     */
    public List<ProductoCatastralJob> obtenerJobsJasperServerEsperando(int cantidadJobs);

    /**
     * Obtiene el ultimo job de un tipo en particular para un tramite
     *
     * @author leidy.gonzalez
     * @param idTramites
     * @param tipo
     * @return
     */
    public List<ProductoCatastralJob> obtenerUltimoJobPorIdTramites(List<Long> idTramites,
        String tipo);

    /**
     * Crea un ProductoCatastralJob para la generación de Cartas catastrales urbanas de conservación
     * (Versión con Arcgis Server 10.3.1)
     *
     * @param codigoManzana
     * @param generarEnPDF
     * @param tramiteId
     * @param usuario
     * @return
     * @author juan.mendez
     */
    public ProductoCatastralJob crearJobGenerarCartaCatastralUrbana(String codigoManzana,
        boolean generarEnPDF,
        Tramite tramite, ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);

    /**
     * Crea un ProductoCatastralJob para la generación de Fichas prediales digitales (Versión con
     * Arcgis Server 10.3.1)
     *
     * @param codigoPredio
     * @param usuario
     * @return
     */
    public ProductoCatastralJob crearJobGenerarFichaPredialDigital(String codigoPredio,
        Tramite tramite,
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);

    /**
      * Crea un ProductoCatastralJob para la generación de la imagen predial
      * 
      * @param codigoPredio
      * @param usuario
      * @return
      */
     public ProductoCatastralJob crearJobGenerarImagenPredial( String codigoPredio,Tramite tramite,
    		 ProductoCatastralDetalle productoCatastralDetalle,  UsuarioDTO usuario);
     /**
      * Crea un ProductoCatastralJob para la generación de Certificado Plano Predial
      * (Versión con Arcgis Server 10.3.1)
     *
     * @param codigoPredio
     * @param usuario
     * @return
     */
    public ProductoCatastralJob crearJobGenerarCertificadoPlanoPredial(String codigoPredio,
        Tramite tramite,
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);

    /**
     * Consulta el ultimo {@link ProductoCatastralJob} asociado a un
     * {@link ProductoCatastralDetalle}
     *
     * @author david.cifuentes
     * @param idProductoCatastralDetalle
     * @return
     */
    public ProductoCatastralJob buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(
        Long idProductoCatastralDetalle);

    /**
     * Crea un job para realizar la tarea geogrÃ¡fica de actualizar la tabla de colindantes
     *
     * @param predios
     * @param usuario
     * @param tramite
     * @return
     */
    public ProductoCatastralJob crearJobActualizarColindancia(String predios, UsuarioDTO usuario,
        Tramite tramite);

}
