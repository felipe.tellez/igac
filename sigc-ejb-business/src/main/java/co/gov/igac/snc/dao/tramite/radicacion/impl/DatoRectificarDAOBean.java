/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.IDatoRectificarDAO;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class DatoRectificarDAOBean extends GenericDAOWithJPA<DatoRectificar, Long>
    implements IDatoRectificarDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatoRectificarDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IDatoRectificarDAO#findByTipo(java.lang.String)
     */
    @Implement
    @Override
    public List<DatoRectificar> findByTipo(String tipoDatoRectifCompl) {

        LOGGER.debug("on DatoRectificarDAOBean#findByTipo ...");
        List<DatoRectificar> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT dr FROM DatoRectificar dr WHERE dr.tipo = :drTipoP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("drTipoP", tipoDatoRectifCompl);

            answer = query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IDatoRectificarDAO#findByTipoAndNaturaleza(String, String)
     * @author juan.agudelo
     * @version 2.0
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<DatoRectificar> findByTipoAndNaturaleza(String tipoDatoRectifCompl,
        String naturaleza) {

        LOGGER.debug("on DatoRectificarDAOBean#findByTipoAndNaturaleza ...");

        List<DatoRectificar> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT dr FROM DatoRectificar dr" +
            " WHERE dr.tipo = :drTipoP" +
            " AND dr.naturaleza = :naturaleza" +
            " ORDER BY dr.nombre ASC";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("drTipoP", tipoDatoRectifCompl);
            query.setParameter("naturaleza", naturaleza);

            answer = query.getResultList();

        } catch (Exception e) {
            LOGGER.
                error("Error en DatoRectificarDAOBean#findByTipoAndNaturaleza: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "DatoRectificarDAOBean#findByTipoAndNaturaleza");
        }

        return answer;
    }

//end of class
}
