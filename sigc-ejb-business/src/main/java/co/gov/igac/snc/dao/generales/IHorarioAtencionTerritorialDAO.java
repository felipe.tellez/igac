package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 * @author lorena.salamanca
 */
@Local
public interface IHorarioAtencionTerritorialDAO extends
    IGenericJpaDAO<HorarioAtencionTerritorial, String> {

    /**
     * Consulta el horario para una territorial o UOC
     *
     * @author lorena.salamanca
     * @param estructuraOrganizacionalCodigo
     * @return
     */
    HorarioAtencionTerritorial buscarHorarioAtencionByEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo);

//end of class   
}
