package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.ConfiguracionReporte;

@Local
public interface IConfiguracionReporteDAO extends IGenericJpaDAO<ConfiguracionReporte, Long> {

    /**
     * Método que consulta un objeto de tipo configuración reporte por el id de éste objeto
     *
     * @param configuracionReporteId
     * @author javier.aponte
     *
     * @return
     */
    public ConfiguracionReporte obtenerConfiguracionReportePorId(Long configuracionReporteId);

}
