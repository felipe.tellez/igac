package co.gov.igac.snc.dao.avaluos.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.ListUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoDAO;
import co.gov.igac.snc.dao.generales.IParametroDAO;
import co.gov.igac.snc.dao.generales.impl.ParametroDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPermiso;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EMovimientoAvaluoTipo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Clase que contiene los queries sobre la tabla AVALUO en SNC_AVALUOS
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class AvaluoDAOBean extends GenericDAOWithJPA<Avaluo, Long> implements IAvaluoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoDAOBean.class);

    /**
     * @see
     * IAvaluoDAO#consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(Long,
     * boolean)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<String> consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(
        Long idSolicitante, boolean banderaRevision) {

        LOGGER.debug(
            "Iniciando AvaluoDAOBean#consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevision");

        DateFormat formatter = new SimpleDateFormat("dd:MM:yyyy");
        List<String> answer = new ArrayList<String>();
        String queryString;
        Query query;

        /* Variables para manejar datos del tipo de tramite */
        // Segun el dato banderaRevision, si la solicitud es de revision, se
        // usa este campo para validar que el avaluo no tenga una
        // solicitud de revision asociada.
        // Si la solicitud es de impugnacion, se
        // usa este campo para validar que el avaluo no tenga una
        // solicitud de impugnacion asociada.
        String avaluoTipoRadicacion = "";

        /* Variables para manejar datos del tipo de tramite */
        String idTipoTramite = ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo();

        /* Variables para manejar rango de fechas para la consulta */
        Calendar cal = Calendar.getInstance();

        Date fechaActual = null;
        Date fechaActualMenosParametro = null;

        /*
         * Variables para cargar parametro asociado al numero de meses dentro de los cuales se deben
         * buscar los Sec. Radicado
         */
        Parametro parametro = null;
        String nombreParametro = "";

//TODO :: snc.avaluos :: revisar si esto debería usarse con inyección de ejb :: pedro.garcia        
        IParametroDAO parametroDAOBean = new ParametroDAOBean();
        parametroDAOBean.setEntityManager(this.entityManager);

        /*
         * Se carga el parametro asociado al numero de meses dentro de los cuales se deben buscar
         * los Sec. Radicado
         */
        nombreParametro = banderaRevision ? "AV_MESES_RADICACIONES_SOLICITUD_REVISION" :
            "AV_MESES_RADICACIONES_SOLICITUD_IMPUGNACION";

        avaluoTipoRadicacion = banderaRevision ? "ava.numeroRadicacionRevision" :
            "ava.numeroRadicacionImpugnacion";

        parametro = parametroDAOBean.getParametroPorNombre(nombreParametro);

        fechaActual = Calendar.getInstance().getTime();
        cal.setTime(fechaActual);
        cal.add(Calendar.MONTH, -parametro.getValorNumero().intValue());
        fechaActualMenosParametro = cal.getTime();

        LOGGER.debug("Fecha actual: " + formatter.format(fechaActual));
        LOGGER.debug("Fecha actual menos parametro: " + formatter.format(fechaActualMenosParametro));

        queryString = "SELECT " + "  ava.secRadicado " + "FROM " +
            "  Avaluo ava, " + "  Tramite tra, " +
            "  SolicitanteSolicitud sol_sol, " + "  Solicitud sol " +
            "WHERE " + "  ava.tramite.id = tra.id " +
            "AND  tra.solicitud.id         = sol.id " +
            "AND tra.tipoTramite       = '" + idTipoTramite + "' " +
            "AND sol_sol.solicitud.id   = sol.id " +
            "AND sol_sol.solicitante.id = :idSolicitante " +
            "AND tra.fechaRadicacion   < :fechaInicio " +
            "AND tra.fechaRadicacion > :fechaParametro " + "AND " +
            avaluoTipoRadicacion + " is null";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitante", idSolicitante);
            query.setParameter("fechaInicio", fechaActual);
            query.setParameter("fechaParametro", fechaActualMenosParametro);
            answer = (List<String>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e,
                    "AvaluoDAOBean#consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevision");
        }

        LOGGER.debug(
            "Finalizando AvaluoDAOBean#consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevision");

        return answer;
    }

    /**
     * @see IAvaluoDAO#consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(Long)
     *
     * @author rodrigo.hernandez
     *
     */
    @Override
    public List<String> consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(
        Long idSolicitante) {

        LOGGER.debug(
            "Iniciando AvaluoDAOBean#consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo");

        List<String> answer = new ArrayList<String>();
        String queryString;
        Query query;

        /* Variables para manejar datos del tipo de tramite */
        String idTipoTramite = ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.getCodigo();

        /* Variables para manejar rango de fechas para la consulta */
        Calendar cal = Calendar.getInstance();

        Date fechaActual = null;
        Date fechaActualMenosParametro = null;

        /*
         * Variables para cargar parametro asociado al numero de meses dentro de los cuales se deben
         * buscar los Sec. Radicado
         */
        Parametro parametro = null;
        String nombreParametro = "";
        IParametroDAO parametroDAOBean = new ParametroDAOBean();
        parametroDAOBean.setEntityManager(this.entityManager);

        /*
         * Se carga el parametro asociado al numero de meses dentro de los cuales se deben buscar
         * los Sec. Radicado
         */
        nombreParametro = "AV_MESES_RADICACIONES_SOLICITUD_COTIZACION";

        parametro = parametroDAOBean.getParametroPorNombre(nombreParametro);

        fechaActual = Calendar.getInstance().getTime();
        cal.setTime(fechaActual);
        cal.add(Calendar.MONTH, -parametro.getValorNumero().intValue());
        fechaActualMenosParametro = cal.getTime();

        queryString = "SELECT " + "  ava.secRadicado " + "FROM " +
            "  Avaluo ava, " + "  Tramite tra, " +
            "  SolicitanteSolicitud sol_sol, " + "  Solicitud sol " +
            "WHERE " + "  ava.tramite.id = tra.id " +
            "AND  tra.solicitud.id         = sol.id " +
            "AND tra.tipoTramite       = '" + idTipoTramite + "' " +
            "AND sol_sol.solicitud.id   = sol.id " +
            "AND sol_sol.solicitante.id = :idSolicitante " +
            "AND tra.fechaRadicacion   < :fechaInicio " +
            "AND tra.fechaRadicacion > :fechaParametro ";
        // TODO::rodrigo.hernandez::pendiente validar si la cotizacion ya tiene
        // asociada una solicitud de avaluo

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitante", idSolicitante);
            query.setParameter("fechaInicio", fechaActual);
            query.setParameter("fechaParametro", fechaActualMenosParametro);
            answer = (List<String>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER,
                    e,
                    "AvaluoDAOBean#consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo");
        }

        LOGGER.debug(
            "Finalizando AvaluoDAOBean#consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo");
        return answer;
    }

    /**
     * @see IAvaluoDAO#buscarAvaluoPorSecRadicado(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public Avaluo buscarAvaluoPorSecRadicado(String secRadicado) {

        Avaluo resultado = null;
        String queryString;
        Query query;

        queryString =
            "SELECT o FROM Avaluo o JOIN FETCH o.tramite t WHERE o.secRadicado = :secRadicado";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("secRadicado", secRadicado);

            resultado = (Avaluo) query.getSingleResult();
            resultado.getTramite().getSolicitud().getSolicitanteSolicituds().size();
        } catch (NoResultException ne) {
            LOGGER.info("No hay avaluos relacionados");
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#buscarAvaluoPorSecRadicado");

        }

        return resultado;
    }

    /**
     * @see IAvaluoDAO#buscarAvaluosPorSolicitudId(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<Avaluo> buscarAvaluosPorSolicitudId(Long idSolicitud) {

        List<Avaluo> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT ava " +
            " FROM Avaluo ava " +
            " LEFT JOIN FETCH ava.tramite atr " +
            " LEFT JOIN FETCH ava.avaluoPredios avp" +
            " LEFT JOIN FETCH atr.departamento " +
            " LEFT JOIN FETCH atr.municipio " +
            " LEFT JOIN FETCH avp.departamento " +
            " LEFT JOIN FETCH avp.municipio " +
            " LEFT JOIN FETCH avp.contacto " +
            " WHERE ava.tramite.id " +
            " IN (SELECT tr.id FROM Tramite tr WHERE tr.solicitud.id = :idSolicitud) " +
            " ORDER BY ava.id";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);

            answer = (List<Avaluo>) query.getResultList();

            for (Avaluo avaluo : answer) {
                Hibernate.initialize(avaluo.getTramite().getSolicitud().getSolicitanteSolicituds());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;
    }

    /**
     * @see IAvaluoDAO#obtenerSiguienteDigitoSecRadicadoPorSolicitud(Solicitud, boolean)
     * @author christian.rodriguez
     */
    @Override
    public Long obtenerSiguienteDigitoSecRadicadoPorSolicitud(Solicitud solicitud,
        boolean contarConProcesoId) {

        Long answer = null;
        String queryString;
        Query query;

        queryString = "SELECT COUNT(ava) " +
            " FROM Avaluo ava " +
            " WHERE ava.tramite.id " +
            " IN (SELECT tr.id FROM Tramite tr WHERE tr.solicitud.id = :idSolicitud) ";

        if (contarConProcesoId) {
            queryString = queryString.concat(" AND ava.tramite.procesoInstanciaId IS NOT NULL");
        }

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", solicitud.getId());

            answer = (Long) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return ++answer;
    }

    /**
     * @see IAvaluoDAO#obtenerSiguienteSecRadicadoPorSolicitud(Solicitud, boolean)
     * @author christian.rodriguez
     */
    @Override
    public String obtenerSiguienteSecRadicadoPorSolicitud(Solicitud solicitud,
        boolean contarConProcesoId) {

        Long siguienteDigitoSecRadicado = this.obtenerSiguienteDigitoSecRadicadoPorSolicitud(
            solicitud, contarConProcesoId);

        if (siguienteDigitoSecRadicado == null) {
            return null;
        }

        return solicitud.getNumeroRadicacionDefinitiva().concat("-")
            .concat(String.valueOf(siguienteDigitoSecRadicado));
    }

    /**
     * @see IAvaluoDAO#buscarAvaluoPorIdConAvaluoPredio(Long)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo buscarAvaluoPorIdConAvaluoPredio(Long idAvaluo) {

        Avaluo answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ava " +
            " FROM Avaluo ava " +
            " LEFT JOIN FETCH ava.avaluoPredios avp" +
            " LEFT JOIN FETCH avp.departamento " +
            " LEFT JOIN FETCH avp.municipio " +
            " LEFT JOIN FETCH avp.contacto " +
            " WHERE ava.id = :idAvaluo ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            answer = (Avaluo) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoDAO#getInfoHeaderAvaluoById(Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Avaluo getInfoHeaderAvaluoById(Long avaluoId) {

        Avaluo answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ava " +
            " FROM Avaluo ava " +
            " LEFT JOIN FETCH ava.tramite tram " +
            " LEFT JOIN FETCH tram.solicitud sol" +
            " LEFT JOIN FETCH sol.solicitanteSolicituds" +
            " WHERE ava.id = :idAvaluoP";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluoP", avaluoId);

            answer = (Avaluo) query.getSingleResult();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, ex,
                    "Avaluo", avaluoId);
        }

        return answer;

    }

    /**
     * @see IAvaluoDAO#buscarAvaluosPorFiltro(co.gov.igac.snc.util.FiltroDatosConsultaAvaluo)
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<Avaluo> buscarAvaluosPorFiltro(FiltroDatosConsultaAvaluo filtro) {

        List<Avaluo> resultado;

        LOGGER.debug("on IAvaluoDAO#buscarAvaluosPorFiltro");

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT a FROM Avaluo a " +
            " LEFT JOIN a.tramite t " +
            " LEFT JOIN t.solicitud s " +
            " LEFT JOIN s.solicitanteSolicituds soli " +
            " LEFT JOIN a.avaluoPredios ap " +
            " LEFT JOIN a.avaluoMovimientos amov " +
            " LEFT JOIN ap.municipio mun " +
            " LEFT JOIN ap.departamento dep " +
            " LEFT JOIN a.avaluoAsignacionProfesionals res " +
            " LEFT JOIN res.profesionalAvaluo avaluador "
        );

        query.append(" WHERE 1=1 ");

        if (filtro.getNumeroRadicacion() != null &&
            !filtro.getNumeroRadicacion().isEmpty()) {
            query.append(" AND s.numero LIKE :numeroRadicacion");
        }

        if (filtro.getNumeroRadicacionDefinitivo() != null &&
            !filtro.getNumeroRadicacionDefinitivo().isEmpty()) {
            query.append(" AND s.numeroRadicacionDefinitiva LIKE :numeroRadicacionDefinitivo");
        }

        if (filtro.getNumeroSecRadicado() != null &&
            !filtro.getNumeroSecRadicado().isEmpty()) {
            query.append(" AND a.secRadicado LIKE :numeroSec");
        }

        if (filtro.getTipoAvaluo() != null &&
            !filtro.getTipoAvaluo().isEmpty()) {
            query.append(" AND a.tipoAvaluo LIKE :tipoAvaluo");
        }

        if ((filtro.getFechaInicial() != null) && (filtro.getFechaFinal() != null)) {
            query.
                append(" AND t.fechaRadicacion >= :fechaInicio AND t.fechaRadicacion <= :fechaFin");
        }

        if (filtro.getTerritorial() != null &&
            !filtro.getTerritorial().isEmpty()) {
            query.append(" AND a.estructuraOrganizacionalCod = :territorial");
        }

        if (filtro.getTipoTramite() != null &&
            !filtro.getTipoTramite().isEmpty()) {
            query.append(" AND s.tipo = :tipoSolicitud");
        }

        if (filtro.getEstadoTramiteAvaluo() != null &&
            !filtro.getEstadoTramiteAvaluo().isEmpty()) {
            query.append(" AND t.estado = :estadoTramite");
        }

        if (filtro.getDepartamento() != null &&
            !filtro.getDepartamento().isEmpty()) {
            query.append(" AND dep.codigo = :codigoDepartamento");
        }

        if (filtro.getMunicipio() != null && !filtro.getMunicipio().isEmpty()) {
            query.append(" AND mun.codigo = :codigoMunicipio");
        }

        if (filtro.getDireccion() != null && !filtro.getDireccion().isEmpty()) {
            query.append(" AND ap.direccion = :direccion");
        }

        if (filtro.getNumeroPredial() != null &&
            !filtro.getNumeroPredial().isEmpty()) {
            query.append(" AND ap.numeroPredial like :numeroPredial");
        }

        if (filtro.getNombreRazonSocial() != null &&
            !filtro.getNombreRazonSocial().isEmpty()) {
            query.append(" AND LOWER(soli.razonSocial) = LOWER(:razonSocial)");
        }

        if (filtro.getNombreSolicitante() != null &&
            !filtro.getNombreSolicitante().isEmpty()) {
            query.append(
                " AND CONCAT (UPPER(soli.primerNombre), UPPER(soli.segundoNombre), UPPER(soli.primerApellido), UPPER(soli.segundoApellido)) " +
                " LIKE CONCAT('%',:nombreSolicitante,'%')");
        }

        if (filtro.getNombreAvaluador() != null &&
            !filtro.getNombreAvaluador().isEmpty()) {
            query.append(" AND CONCAT (UPPER(avaluador.primerNombre), " +
                "UPPER(avaluador.segundoNombre), UPPER(avaluador.primerApellido), " +
                "UPPER(avaluador.segundoApellido)) " +
                "LIKE CONCAT('%',:nombreAvaluador,'%')");
        }

        if (filtro.getIdProfesional() != null) {
            query.append(" AND avaluador.id = :idProfesionalAvaluos ");
        }

        if (filtro.getFechaCancelacionDesde() != null ||
            filtro.getFechaCancelacionHasta() != null) {

            String restriccion = " AND amov.tipoMovimiento LIKE '".concat(
                EMovimientoAvaluoTipo.CANCELACION.getCodigo()).concat("' ");

            query.append(restriccion);

            if (filtro.getFechaCancelacionDesde() != null) {
                query.append(" AND amov.fecha >= :fechaCancelacionDesde");
            }

            if (filtro.getFechaCancelacionHasta() != null) {
                query.append(" AND amov.fecha <= :fechaCancelacionHasta");
            }
        }

        if (filtro.getTipoEmpresa() != null && !filtro.getTipoEmpresa().isEmpty()) {
            query.append(" AND LOWER(soli.tipoPersona) = LOWER(:tipoPersona)");
        }

        if (filtro.getSigla() != null && !filtro.getSigla().isEmpty()) {
            query.append(" AND LOWER(soli.sigla) = LOWER(:sigla)");
        }

        if (filtro.getPrimerNombre() != null && !filtro.getPrimerNombre().isEmpty()) {
            query.append(" AND LOWER(soli.primerNombre) = LOWER(:primerNombre)");
        }

        if (filtro.getSegundoNombre() != null && !filtro.getSegundoNombre().isEmpty()) {
            query.append(" AND LOWER(soli.segundoNombre) = LOWER(:segundoNombre)");
        }

        if (filtro.getPrimerApellido() != null && !filtro.getPrimerApellido().isEmpty()) {
            query.append(" AND LOWER(soli.primerApellido) = LOWER(:primerApellido)");
        }

        if (filtro.getSegundoApellido() != null && !filtro.getSegundoApellido().isEmpty()) {
            query.append(" AND LOWER(soli.segundoApellido) = LOWER(:segundoApellido)");
        }

        if (filtro.isConActaControlCalidad()) {
            query.append(" AND a.id IN (" +
                " SELECT DISTINCT cc.avaluoId " +
                " FROM ControlCalidadAvaluo cc " +
                " WHERE cc.id IN(" +
                " SELECT ca.controlCalidadAvaluoId " +
                " FROM ComiteAvaluo ca " +
                " WHERE ca.actaFirmadaDocumentoId IS NOT NULL))");
        }

        if (filtro.getReservado() != null && !filtro.getReservado().isEmpty()) {
            query.append(" AND a.reservado = :reservado");
        }

        if (filtro.getAnioAprobacion() != null && !filtro.getAnioAprobacion().isEmpty()) {
            query.append(" AND a.anioAprobacion = :anioAprobacion");
        }

        if (filtro.getTramitesId() != null && !filtro.getTramitesId().isEmpty()) {
            query.append(" AND t.id IN :tramitesId");
        }

        if (filtro.getAprobado() != null && !filtro.getAprobado().isEmpty()) {
            if (filtro.getAprobado().equals(ESiNo.SI.getCodigo())) {
                query.append(" AND a.aprobado = 'SI'");
            } else {
                query.append(" AND (a.aprobado = 'NO' OR a.aprobado IS NULL) ");
            }
        }

        if (filtro.getSolicitanteTipoIdentificacion() != null &&
            !filtro.getSolicitanteTipoIdentificacion().isEmpty()) {
            query.append(" AND soli.tipoIdentificacion = :tipoIdentificacion");
        }

        if (filtro.getSolicitanteNumeroIdentificacion() != null &&
            !filtro.getSolicitanteNumeroIdentificacion().isEmpty()) {
            query.append(" AND soli.numeroIdentificacion = :numeroIdentificacion");
        }

        if (filtro.getValorCotizacionDesde() != null &&
            filtro.getValorCotizacionDesde() >= 0 &&
            filtro.getValorCotizacionHasta() != null &&
            filtro.getValorCotizacionHasta() > 0) {
            query.append(
                " AND a.valorCotizacion >= :valorCotizacionDesde AND a.valorCotizacion <= :valorCotizacionHasta");
        }

        query.append(" AND soli.relacion = :relacionSolicitante");

        LOGGER.debug(query.toString());
        Query q = entityManager.createQuery(query.toString());

        if (filtro.getNumeroRadicacion() != null &&
            !filtro.getNumeroRadicacion().isEmpty()) {
            q.setParameter("numeroRadicacion", filtro.getNumeroRadicacion());
        }

        if (filtro.getNumeroRadicacionDefinitivo() != null &&
            !filtro.getNumeroRadicacionDefinitivo().isEmpty()) {
            q.setParameter("numeroRadicacionDefinitivo", filtro.getNumeroRadicacionDefinitivo());
        }

        if (filtro.getNumeroSecRadicado() != null &&
            !filtro.getNumeroSecRadicado().isEmpty()) {
            q.setParameter("numeroSec", filtro.getNumeroSecRadicado());
        }

        if (filtro.getTipoAvaluo() != null &&
            !filtro.getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo", filtro.getTipoAvaluo());
        }

        if ((filtro.getFechaInicial() != null) && (filtro.getFechaFinal() != null)) {
            q.setParameter("fechaFin", filtro.getFechaFinal());
            q.setParameter("fechaInicio", filtro.getFechaInicial());
        }

        if (filtro.getTerritorial() != null &&
            !filtro.getTerritorial().isEmpty()) {
            q.setParameter("territorial", filtro.getTerritorial());
        }

        if (filtro.getTipoTramite() != null &&
            !filtro.getTipoTramite().isEmpty()) {
            q.setParameter("tipoSolicitud", filtro.getTipoTramite());
        }

        if (filtro.getEstadoTramiteAvaluo() != null &&
            !filtro.getEstadoTramiteAvaluo().isEmpty()) {
            q.setParameter("estadoTramite", filtro.getEstadoTramiteAvaluo());
        }

        if (filtro.getDepartamento() != null &&
            !filtro.getDepartamento().isEmpty()) {
            q.setParameter("codigoDepartamento", filtro.getDepartamento());
        }

        if (filtro.getMunicipio() != null && !filtro.getMunicipio().isEmpty()) {
            q.setParameter("codigoMunicipio", filtro.getMunicipio());
        }

        if (filtro.getDireccion() != null && !filtro.getDireccion().isEmpty()) {
            q.setParameter("direccion", filtro.getDireccion());
        }

        if (filtro.getNumeroPredial() != null &&
            !filtro.getNumeroPredial().isEmpty()) {
            q.setParameter("numeroPredial", filtro.getNumeroPredial());
        }

        if (filtro.getNombreRazonSocial() != null &&
            !filtro.getNombreRazonSocial().isEmpty()) {
            q.setParameter("razonSocial", filtro.getNombreRazonSocial());
        }

        if (filtro.getNombreSolicitante() != null &&
            !filtro.getNombreSolicitante().isEmpty()) {
            String param = filtro.getNombreSolicitante();
            param = param.replace(" ", "%");
            LOGGER.debug(param);
            q.setParameter("nombreSolicitante", param.toUpperCase());
        }

        if (filtro.getNombreAvaluador() != null &&
            !filtro.getNombreAvaluador().isEmpty()) {
            String param = filtro.getNombreAvaluador();
            param = param.replace(" ", "%");
            LOGGER.debug(param.toUpperCase());
            q.setParameter("nombreAvaluador", param.toUpperCase());
        }

        if (filtro.getIdProfesional() != null) {
            q.setParameter("idProfesionalAvaluos", filtro.getIdProfesional());
        }

        if (filtro.getFechaCancelacionDesde() != null) {
            q.setParameter("fechaCancelacionDesde",
                filtro.getFechaCancelacionDesde());
        }

        if (filtro.getFechaCancelacionHasta() != null) {
            q.setParameter("fechaCancelacionHasta",
                filtro.getFechaCancelacionHasta());
        }

        if (filtro.getTipoEmpresa() != null && !filtro.getTipoEmpresa().isEmpty()) {
            q.setParameter("tipoPersona", filtro.getTipoEmpresa());
        }

        if (filtro.getSigla() != null && !filtro.getSigla().isEmpty()) {
            q.setParameter("sigla", filtro.getSigla());
        }

        if (filtro.getPrimerNombre() != null &&
            !filtro.getPrimerNombre().isEmpty()) {
            q.setParameter("primerNombre", filtro.getPrimerNombre());
        }

        if (filtro.getSegundoNombre() != null &&
            !filtro.getSegundoNombre().isEmpty()) {
            q.setParameter("segundoNombre", filtro.getSegundoNombre());
        }

        if (filtro.getPrimerApellido() != null &&
            !filtro.getPrimerApellido().isEmpty()) {
            q.setParameter("primerApellido", filtro.getPrimerApellido());
        }

        if (filtro.getSegundoApellido() != null &&
            !filtro.getSegundoApellido().isEmpty()) {
            q.setParameter("segundoApellido", filtro.getSegundoApellido());
        }

        if (filtro.getReservado() != null &&
            !filtro.getReservado().isEmpty()) {
            q.setParameter("reservado", filtro.getReservado());
        }

        if (filtro.getAnioAprobacion() != null &&
            !filtro.getAnioAprobacion().isEmpty()) {
            q.setParameter("anioAprobacion", Long.valueOf(filtro.getAnioAprobacion()));
        }

        if (filtro.getTramitesId() != null && !filtro.getTramitesId().isEmpty()) {
            q.setParameter("tramitesId", filtro.getTramitesId());
        }

        if (filtro.getSolicitanteTipoIdentificacion() != null &&
            !filtro.getSolicitanteTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                filtro.getSolicitanteTipoIdentificacion());
        }

        if (filtro.getSolicitanteNumeroIdentificacion() != null &&
            !filtro.getSolicitanteNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion", filtro.getSolicitanteNumeroIdentificacion());
        }

        if (filtro.getValorCotizacionDesde() != null &&
            filtro.getValorCotizacionDesde() >= 0 &&
            filtro.getValorCotizacionHasta() != null &&
            filtro.getValorCotizacionHasta() > 0) {

            q.setParameter("valorCotizacionDesde",
                filtro.getValorCotizacionDesde());
            q.setParameter("valorCotizacionHasta",
                filtro.getValorCotizacionHasta());
        }

        q.setParameter("relacionSolicitante",
            ESolicitanteSolicitudRelac.PROPIETARIO.toString());

        try {
            resultado = (List<Avaluo>) q.getResultList();
            for (Avaluo avaluo : resultado) {
                this.inicializarObjetosAvaluo(avaluo);
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                ex.getMessage());
        }
        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoDAO#buscarPorSolicitudes(List, FiltroDatosConsultaAvaluo, boolean)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> buscarPorSolicitudes(List<Long> idsSolicitudes,
        FiltroDatosConsultaAvaluo filtroConsulta, boolean cargarObjetosExtra) {

        LOGGER.debug("on AvaluoDAOBean#buscarPorSolicitudes ");

        List<Avaluo> answer = null;
        StringBuilder queryString;
        Query query;
        boolean useEstadoTramite, useAprobado;

        useEstadoTramite = false;
        useAprobado = false;

        queryString = new StringBuilder();
        queryString.append(
            " SELECT a FROM Avaluo a " +
            " WHERE a.tramite.id IN (" +
            "  SELECT t.id FROM Tramite t " +
            "  WHERE t.solicitud.id IN (:idsSolicitud_p)");

        if (filtroConsulta.getEstadoTramiteAvaluo() != null &&
            !filtroConsulta.getEstadoTramiteAvaluo().isEmpty()) {
            useEstadoTramite = true;
        }

        queryString = (useEstadoTramite) ? queryString.append(" AND t.estado = :estadoTramite_p)") :
            queryString.append(")");

        if (filtroConsulta.getAprobado() != null &&
            !filtroConsulta.getAprobado().isEmpty()) {
            queryString.append(" AND a.aprobado = :aprobado_p");
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idsSolicitud_p", idsSolicitudes);

            if (useEstadoTramite) {
                query.setParameter("estadoTramite_p", filtroConsulta.getEstadoTramiteAvaluo());
            }

            if (filtroConsulta.getAprobado() != null &&
                !filtroConsulta.getAprobado().isEmpty()) {
                query.setParameter("aprobado_p", ESiNo.SI.getCodigo());
            }

            answer = query.getResultList();

            if (answer != null && cargarObjetosExtra) {
                for (Avaluo avaluo : answer) {

                    Hibernate.initialize(avaluo.getAvaluoPredios());
                    for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                        Hibernate.initialize(predio.getDepartamento());
                        Hibernate.initialize(predio.getMunicipio());
                    }

                    Hibernate.initialize(avaluo.getAvaluoLiquidacionCosto());

                    Hibernate.initialize(avaluo
                        .getAvaluoAsignacionProfesionals());
                    for (AvaluoAsignacionProfesional asignacion : avaluo
                        .getAvaluoAsignacionProfesionals()) {
                        Hibernate.initialize(asignacion.getProfesionalAvaluo());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoDAOBean#buscarPorSolicitudes");
        }

        return answer;
    }

    /**
     * @see IAvaluoDAO#consultarAvaluosEnProcesoAvaluador(Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosEnProcesoAvaluador(Long idAvaluador) {

        LOGGER.debug("on AvaluoDAOBean#buscarAvaluosEnProcesoAvaluador ");

        List<Avaluo> resultado = null;
        String queryString;
        Query query;

        queryString = " SELECT DISTINCT a " +
            " FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " LEFT JOIN FETCH a.avaluoPredios avp" +
            " LEFT JOIN FETCH avp.departamento " +
            " LEFT JOIN FETCH avp.municipio " +
            " LEFT JOIN a.avaluoAsignacionProfesionals res " +
            " LEFT JOIN res.profesionalAvaluo pa " +
            " WHERE pa.id = :idAvaluador" +
            " AND t.estado = :estadoTramite" +
            " AND t.tipoTramite = :tipoTramite" +
            " AND a.aprobado= 'NO'";

        LOGGER.debug(queryString);
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluador", idAvaluador);
            query.setParameter("estadoTramite", ETramiteEstado.EN_PROCESO.getCodigo());
            query.setParameter("tipoTramite", ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo());

            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#buscarAvaluosEnProcesoAvaluador");

        }
        return resultado;

    }

    /**
     * @see IAvaluoDAO#consultarAvaluosRealizadosContratoPA(Long, String, String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosRealizadosContratoPA(Long idContrato,
        String tipoActividad, String estadoTramite) {

        LOGGER.debug("on AvaluoDAOBean#consultarAvaluosRealizadosContrato ");

        List<Avaluo> resultado = null;
        StringBuilder queryString;
        Query query;

        queryString = new StringBuilder();
        queryString.append(" SELECT DISTINCT a " +
            " FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " LEFT JOIN FETCH a.avaluoPredios avp" +
            " LEFT JOIN FETCH avp.departamento " +
            " LEFT JOIN FETCH avp.municipio " +
            " LEFT JOIN a.avaluoAsignacionProfesionals res " +
            " WHERE res.profesionalAvaluoContratoId = :idContrato" +
            " AND t.tipoTramite not like :tipoTramite");

        if (tipoActividad != null) {
            queryString.append(" AND res.actividad like :tipoActividad ");
        }
        if (estadoTramite != null) {
            queryString.append(" AND t.estado = :estadoTramite ");
        }
        LOGGER.debug(queryString.toString());
        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idContrato", idContrato);

            query.setParameter("tipoTramite", ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.
                getCodigo());

            if (tipoActividad != null) {
                query.setParameter("tipoActividad", tipoActividad);
            }
            if (tipoActividad != null) {
                query.setParameter("estadoTramite", estadoTramite);
            }

            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#consultarAvaluosRealizadosContrato");

        }
        return resultado;

    }

    /**
     * @see IAvaluoDAO#consultarAvaluosProfesionalPorAnio(Long, String, String, Integer)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosProfesionalPorAnio(Long idAvaluador,
        String tipoActividad, String estadoTramite, Integer anio) {

        LOGGER.debug("on AvaluoDAOBean#consultarAvaluosProfesionalPorAnio ");

        List<Avaluo> resultado = null;
        StringBuilder queryString;
        Query query;
        Calendar vigencia = Calendar.getInstance();
        vigencia.set(anio, 0, 1);
        Date fiVigencia = vigencia.getTime();
        vigencia.set(anio, 11, 31);
        Date ffVigencia = vigencia.getTime();

        queryString = new StringBuilder();
        queryString.append(" SELECT DISTINCT a " +
            " FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " LEFT JOIN FETCH a.avaluoPredios avp" +
            " LEFT JOIN FETCH avp.departamento " +
            " LEFT JOIN FETCH avp.municipio " +
            " LEFT JOIN a.avaluoAsignacionProfesionals res " +
            " LEFT JOIN res.profesionalAvaluo avaluador " +
            " WHERE avaluador.id = :idAvaluador " +
            " AND res.fechaDesde BETWEEN :fInicio AND :fFin " +
            " AND t.tipoTramite not like :tipoTramite ");

        if (tipoActividad != null) {
            queryString.append(" AND res.actividad like :tipoActividad ");
        }
        if (estadoTramite != null) {
            queryString.append(" AND t.estado = :estadoTramite ");
        }
        LOGGER.debug(queryString.toString());
        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idAvaluador", idAvaluador);
            query.setParameter("fInicio", fiVigencia);
            query.setParameter("fFin", ffVigencia);

            query.setParameter("tipoTramite", ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.
                getCodigo());

            if (tipoActividad != null) {
                query.setParameter("tipoActividad", tipoActividad);
            }
            if (tipoActividad != null) {
                query.setParameter("estadoTramite", estadoTramite);
            }

            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#consultarAvaluosProfesionalPorAnio");

        }
        return resultado;

    }

    /**
     * @see IAvaluoDAO#consultarhistoricaoAvaluos(Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Object[]> consultarHistoricoAvaluos(Long idAvaluador) {

        LOGGER.debug("on AvaluoDAOBean#consultarhistoricaoAvaluos ");

        List<Object[]> resultado = null;
        StringBuilder queryString;
        Query query;

        queryString = new StringBuilder();
        queryString.append(" SELECT EXTRACT(year FROM ap.fechaDesde), " +
            " SUM(CASE WHEN (ap.actividad = 'AVALUAR') THEN 1 ELSE 0 END), " +
            " SUM(CASE WHEN (ava.aprobado = 'SI' AND ap.actividad = 'AVALUAR') THEN 1 ELSE 0 END), " +

            " SUM(CASE WHEN (ava.aprobado = 'NO' AND ap.actividad = 'AVALUAR') THEN 1 ELSE 0 END), " +
            " SUM(CASE WHEN (ap.actividad = 'AVALUAR') THEN 0 ELSE 1 END), " +
            " AVG(ava.calificacion), " +
            " AVG(ava.calificacion) " //TODO::felipe.cadena::definir promedio calificacion control de calidad
            +
             " FROM AvaluoAsignacionProfesional ap " +
            " LEFT JOIN ap.avaluo ava " +
            " LEFT JOIN ap.profesionalAvaluo proAva " +
            " LEFT JOIN ava.tramite tr " +
            " WHERE proAva.id = :idAvaluador " +
            " AND tr.estado LIKE 'FINALIZADO' " +
            " AND tr.tipoTramite NOT LIKE '14' " +
            " GROUP BY EXTRACT(year FROM ap.fechaDesde)" +
            " ORDER BY EXTRACT(year FROM ap.fechaDesde)");

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idAvaluador", idAvaluador);

            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#consultarhistoricaoAvaluos");

        }
        return resultado;

    }

    // ---------------------------------------------------------------------------------------
    /**
     * @see IAvaluoDAO#consultarSinPagoPorAvaluadorYContrato(Long, Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Avaluo> consultarSinPagoPorAvaluadorYContrato(Long idContrato,
        Long idAvaluador) {

        List<Avaluo> avaluos = null;
        String queryString;
        Query query;

        queryString = "SELECT ava " +
            " FROM Avaluo ava " +
            " WHERE ava.id IN " +
            " (SELECT aap.avaluo.id " +
            " FROM AvaluoAsignacionProfesional aap " +
            " WHERE aap.profesionalAvaluo.id = :idAvaluador " +
            " AND aap.actividad = :actividad) " +
            " AND ava.id NOT IN " +
            " (SELECT cpa.avaluo.id " +
            " FROM ContratoPagoAvaluo cpa " +
            " WHERE cpa.profesionalContratoPago.profesionalAvaluosContrato.id = :idContrato)";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            query.setParameter("idAvaluador", idAvaluador);
            query.setParameter("actividad",
                EAvaluoAsignacionProfActi.AVALUAR.getCodigo());

            avaluos = (List<Avaluo>) query.getResultList();

            for (Avaluo avaluo : avaluos) {
                Hibernate.initialize(avaluo.getAvaluoPredios());
                for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                    Hibernate.initialize(predio.getDepartamento());
                    Hibernate.initialize(predio.getMunicipio());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return avaluos;
    }

    // ---------------------------------------------------------------------------------------
    /**
     * @see IAvaluoDAO#consultarPorProfesionalContratoPagoId(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Avaluo> consultarPorProfesionalContratoPagoId(
        Long idContratoPago) {

        List<Avaluo> avaluos = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT cpa.avaluo " +
                " FROM ContratoPagoAvaluo cpa " +
                " WHERE cpa.profesionalContratoPago.id = :idContratoPago ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContratoPago", idContratoPago);

            avaluos = (List<Avaluo>) query.getResultList();

            for (Avaluo avaluo : avaluos) {
                Hibernate.initialize(avaluo.getAvaluoPredios());
                for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                    Hibernate.initialize(predio.getDepartamento());
                    Hibernate.initialize(predio.getMunicipio());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return avaluos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoDAO#obtenerPorIdsTramite(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> obtenerPorIdsTramite(List<Long> idsTramites) {

        List<Avaluo> answer = null;
        String queryString;
        Query query;

        queryString =
            " SELECT a FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " WHERE a.tramite.id IN (:idsTramites_p) ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idsTramites_p", idsTramites);

            answer = (List<Avaluo>) query.getResultList();

            for (Avaluo avaluo : answer) {
                this.inicializarObjetosAvaluo(avaluo);
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoDAOBean#obtenerConAvaluadoresYPrediosPorIdsTramite");
        }

        return answer;

    }

    /**
     * @see IAvaluoDAO#consultarAvaluosAsignacion(Long, String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Avaluo> consultarAvaluosAsignacion(Long idProfesional, String secRadicado) {

        List<Avaluo> resultado = null;

        LOGGER.debug("on IAvaluoDAO#consultarAvaluosAsigancion");

        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT DISTINCT a FROM Avaluo a " +
            " LEFT JOIN FETCH a.tramite t " +
            " INNER JOIN FETCH a.avaluoAsignacionProfesionals res " +
            " LEFT JOIN FETCH res.profesionalAvaluo avaluador " +
            " WHERE 1=1 " +
            " AND res.ordenPractica IS NULL ");

        if (idProfesional != null) {
            queryString.append(" AND avaluador.id = :idAvaluador ");
        }

        if (secRadicado != null && !"".equals(secRadicado)) {
            queryString.append(" AND a.secRadicado LIKE :numeroSec ");
        }

        Query q = entityManager.createQuery(queryString.toString());

        if (idProfesional != null) {
            q.setParameter("idAvaluador", idProfesional);
        }
        if (secRadicado != null && !"".equals(secRadicado)) {
            q.setParameter("numeroSec", secRadicado);
        }
        try {
            resultado = q.getResultList();

            for (Avaluo avaluo : resultado) {
                Hibernate.initialize(avaluo.getAvaluoPredios());
                for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                    Hibernate.initialize(predio.getDepartamento());
                    Hibernate.initialize(predio.getMunicipio());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    /**
     * @see IAvaluoDAO#obtenerPorIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo obtenerPorIdConAtributos(Long idAvaluo) {

        LOGGER.debug("on IAvaluoDAO#obtenerPorIdConDatosParaControlCalidad");

        Avaluo avaluo = null;

        if (idAvaluo != null) {

            try {

                avaluo = this.findById(idAvaluo);

                this.inicializarObjetosAvaluo(avaluo);

            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                    .getExcepcion(LOGGER, e, e.getMessage());
            }
        }
        return avaluo;
    }

    /**
     * @see IAvaluoDAO#consultarPorSecRadicadoConAtributos(String)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo consultarPorSecRadicadoConAtributos(String secRadicado) {

        LOGGER.debug("on IAvaluoDAO#obtenerPorIdConDatosParaControlCalidad");

        Avaluo avaluo = null;

        String queryString;
        Query query;

        queryString = "SELECT ava " +
            " FROM Avaluo ava " +
            " WHERE ava.secRadicado = :secRadicado";

        if (secRadicado != null && !secRadicado.isEmpty()) {

            try {

                query = this.entityManager.createQuery(queryString);
                query.setParameter("secRadicado", secRadicado);

                avaluo = (Avaluo) query.getSingleResult();

                this.inicializarObjetosAvaluo(avaluo);

            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                    .getExcepcion(LOGGER, e, e.getMessage());
            }
        }
        return avaluo;
    }

    // ------------------------------------------------------------------------
    /**
     * método que inicializa los objetos relacionados con un avaluo Carga los siguientes objetos
     * asociados
     *
     * <ul>
     * <li>{@link Tramite}</li>
     * <li>{@link Solicitud} del tramite</li>
     * <li>{@link SolicitanteSolicitud} de la solicitud</li>
     * <li>{@link AvaluoPredio}</li>
     * <li>{@link Departamento} de los predios</li>
     * <li>{@link Municipio} de los predios</li>
     * <li>{@link AvaluoAsignacionProfesional}</li>
     * <li>{@link ProfesionalAvaluo} de las asignaciones</li>
     * </ul>
     *
     * @author christian.rodriguez
     * @param avaluo {@link Avaluo}
     */
    private void inicializarObjetosAvaluo(Avaluo avaluo) {
        if (avaluo != null) {

            Hibernate.initialize(avaluo.getTramite());
            Hibernate.initialize(avaluo.getTramite().getSolicitud());
            Hibernate.initialize(avaluo.getTramite().getSolicitud()
                .getSolicitanteSolicituds());

            for (SolicitanteSolicitud ss : avaluo
                .getTramite().getSolicitud()
                .getSolicitanteSolicituds()) {
                Hibernate.initialize(ss.getDireccionDepartamento());
                Hibernate.initialize(ss.getDireccionMunicipio());
            }
            Hibernate.initialize(avaluo.getAvaluoPredios());
            for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                Hibernate.initialize(predio.getDepartamento());
                Hibernate.initialize(predio.getMunicipio());
            }
            Hibernate.initialize(avaluo
                .getAvaluoAsignacionProfesionals());
            for (AvaluoAsignacionProfesional aap : avaluo
                .getAvaluoAsignacionProfesionals()) {
                Hibernate.initialize(aap.getProfesionalAvaluo());
                Hibernate.initialize(aap.getProfesionalAvaluo().getProfesionalAvaluosContratos());
            }
            Hibernate.initialize(avaluo.getAvaluoMovimientos());
            Hibernate.initialize(avaluo.getAvaluoPermisos());
            for (AvaluoPermiso permiso : avaluo.getAvaluoPermisos()) {
                Hibernate.initialize(permiso.getProfesionalAvaluos());
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoDAO#obtenerParaOrdenPractica(long[], java.lang.String, java.lang.String,
     * java.util.Map, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Avaluo> obtenerParaOrdenPractica(long[] idsTramites, String sortField,
        String sortOrder, Map<String, String> filters, int... contadoresDesdeYCuantos) {

        List<Avaluo> answer = null;
        StringBuilder queryString;
        Query query;
        ArrayList<Long> idsTramitesAsArray;
        ArrayList<ProfesionalAvaluosContrato> contratosActivos;
        ArrayList<AvaluoAsignacionProfesional> asignacionesTipoAvaluar;

        queryString = new StringBuilder();
        queryString.append(
            " SELECT a FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " LEFT JOIN FETCH t.solicitanteTramites" +
            " WHERE t.id IN (:idsTramites_p) " +
            " AND a.id NOT IN (" +
            "	SELECT DISTINCT ap.avaluo.id " +
            "	FROM AvaluoAsignacionProfesional ap " +
            "	WHERE ap.ordenPractica IS NOT NULL)");

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            queryString.append(" ORDER BY a.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

// descomentar esto si se necesita que esta consulta admita filtros
//		if (filters != null){
//			for (String campo : filters.keySet()){
//				if (campo.equals("numeroRadicacion")){
//					queryString.append(" AND t.numeroRadicacion like :numeroRadicacion");
//				}
//			}
//		}
        idsTramitesAsArray = new ArrayList<Long>();
        for (long id : idsTramites) {
            idsTramitesAsArray.add(new Long(id));
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idsTramites_p", idsTramitesAsArray);
            answer = (List<Avaluo>) super.findInRangeUsingQuery(query, contadoresDesdeYCuantos);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoDAOBean#obtenerParaOrdenPractica");
        }

        try {
            if (answer != null) {
                for (Avaluo avaluo : answer) {

                    //D: el fetch del Tramite trae la Solicitud (porque en el entity se definió eager)
                    Hibernate.initialize(avaluo.getAvaluoPredios());
                    for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                        Hibernate.initialize(predio.getDepartamento());
                        Hibernate.initialize(predio.getMunicipio());
                    }

                    //D: solo se deben llevar las asignaciones de tipo AVALUAR
                    Hibernate.initialize(avaluo.getAvaluoAsignacionProfesionals());
                    asignacionesTipoAvaluar = new ArrayList<AvaluoAsignacionProfesional>();
                    for (AvaluoAsignacionProfesional asignacion : avaluo.
                        getAvaluoAsignacionProfesionals()) {
                        if (asignacion.getActividad().equalsIgnoreCase(
                            EAvaluoAsignacionProfActi.AVALUAR.getCodigo())) {
                            asignacionesTipoAvaluar.add(asignacion);
                        }
                    }
                    avaluo.setAvaluoAsignacionProfesionals(asignacionesTipoAvaluar);

                    for (AvaluoAsignacionProfesional asignacion : avaluo.
                        getAvaluoAsignacionProfesionals()) {
                        Hibernate.initialize(asignacion.getProfesionalAvaluo());

                        //D: traer la territorial
                        Hibernate.initialize(asignacion.getProfesionalAvaluo().getTerritorial());

                        Hibernate.initialize(asignacion.getProfesionalAvaluo().
                            getProfesionalAvaluosContratos());

                        //D: esto se hace para retornar solamente el contrato activo (que es
                        //  el que se necesita cuando estoy asignando avaluador)
                        for (ProfesionalAvaluosContrato pac : asignacion.getProfesionalAvaluo().
                            getProfesionalAvaluosContratos()) {
                            if (pac.isContratoActivo()) {
                                //D: traer el interventor del contrato activo
                                Hibernate.initialize(pac.getInterventor());
                                contratosActivos = new ArrayList<ProfesionalAvaluosContrato>();
                                contratosActivos.add(pac);
                                asignacion.getProfesionalAvaluo().
                                    setProfesionalAvaluosContratos(contratosActivos);
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_HIBERNATE_INITIALIZE.getExcepcion(LOGGER,
                ex, "AvaluoDAOBean#obtenerParaOrdenPractica", "Avaluo");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoDAO#obtenerPorIdConTramiteYSolicitud(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Avaluo obtenerPorIdConTramiteYSolicitud(Long idAvaluo) {

        Avaluo answer = null;
        String queryString;
        Query query;

        queryString =
            " SELECT a " +
            " FROM Avaluo a JOIN FETCH a.tramite t JOIN FETCH t.solicitud " +
            " WHERE a.id = :idAvaluo_p";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idAvaluo_p", idAvaluo);
            answer = (Avaluo) query.getSingleResult();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, ex,
                    "Avaluo", idAvaluo.toString());
        }

        return answer;
    }

    // ------------------------------------------------------------------------------
    /**
     * @see IAvaluoDAO#consultarDocumentoPorIdAvaluoYTipoDocumento(Long, Long)
     * @author christian.rodriguez
     */
    @Override
    public Documento consultarDocumentoPorIdAvaluoYTipoDocumento(Long idAvaluo,
        Long idTipoDocumento) {
        LOGGER.debug("on IAvaluoDAO#consultarDocumentoPorIdAvaluoYTipoDocumento");

        Documento documento = null;

        String queryString;
        Query query;

        queryString = "SELECT trd.documento " +
            " FROM TramiteDocumento trd " +
            " WHERE trd.tramite.id IN ( " +
            " SELECT ava.tramite.id " +
            " FROM Avaluo ava	" + " WHERE ava.id = :idAvaluo) " +
            " AND trd.documento.tipoDocumento.id = :idTipoDocumento ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);
            query.setParameter("idTipoDocumento", idTipoDocumento);

            documento = (Documento) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return documento;
    }

    /**
     * @see IAvaluoDAO#consultarPorOrdenPracticaIdYProfesionalId(Long, Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Avaluo> consultarPorOrdenPracticaIdYProfesionalId(
        Long profesionalId, Long ordenPracticaId) {

        List<Avaluo> avaluos = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT ap.avaluo " +
            " FROM AvaluoAsignacionProfesional ap " +
            " WHERE ap.ordenPractica.id = :ordenPracticaId " +
            " AND ap.profesionalAvaluo.id = :profesionalId " +
            " AND ap.fechaHasta IS NULL " +
            " AND (ap.avaluo.reservado IS NULL OR ap.avaluo.reservado = :reservado)";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("profesionalId", profesionalId);
            query.setParameter("ordenPracticaId", ordenPracticaId);
            query.setParameter("reservado", ESiNo.NO.getCodigo());

            avaluos = (List<Avaluo>) query.getResultList();

            for (Avaluo avaluo : avaluos) {
                this.inicializarObjetosAvaluo(avaluo);
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex, "Avaluo");
        }

        return avaluos;
    }

    //------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoDAO#consultarPorOrdenPracticaId(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Avaluo> consultarPorOrdenPracticaId(Long ordenPracticaId) {

        List<Avaluo> avaluos = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT ap.avaluo " +
            " FROM AvaluoAsignacionProfesional ap " +
            " WHERE ap.ordenPractica.id = :ordenPracticaId " +
            " AND ap.fechaHasta IS NULL " +
            " AND (ap.avaluo.reservado IS NULL OR ap.avaluo.reservado = :reservado)";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("ordenPracticaId", ordenPracticaId);
            query.setParameter("reservado", ESiNo.NO.getCodigo());

            avaluos = (List<Avaluo>) query.getResultList();

            for (Avaluo avaluo : avaluos) {
                this.inicializarObjetosAvaluo(avaluo);
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex, "Avaluo");
        }

        return avaluos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoDAO#obtenerParaAsignacion(long[], java.lang.String, boolean, java.lang.String,
     * java.lang.String, java.util.Map, int[])
     * @author pedro.garcia
     *
     * Los asignados son los que tienen alguna asignación para la actividad dada y con fecha
     * vigente. Los no asignados son los que no tienen alguna asignación para la actividad dada con
     * fecha vigente.
     */
    @Implement
    @Override
    public List<Avaluo> obtenerParaAsignacion(long[] idsTramites, String actividadCodigo,
        boolean asignados, String sortField, String sortOrder, Map<String, String> filters,
        int[] contadoresDesdeYCuantos) {

        List<Avaluo> answer, listaAvaluosTemp;
        StringBuilder queryString;
        Query query;
        ArrayList<Long> idsTramitesAsArray;
        boolean validAsignacion, isAsignado;
        Avaluo avaluoTemp;
        AvaluoAsignacionProfesional aapTemp;
        List<AvaluoAsignacionProfesional> aapListTemp;

        queryString = new StringBuilder();
        queryString.append(
            " SELECT a FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " LEFT JOIN FETCH t.solicitanteTramites" +
            " WHERE t.id IN (:idsTramites_p)");

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            queryString.append(" ORDER BY a.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

// descomentar esto si se necesita que esta consulta admita filtros
//		if (filters != null){
//			for (String campo : filters.keySet()){
//				if (campo.equals("numeroRadicacion")){
//					queryString.append(" AND t.numeroRadicacion like :numeroRadicacion");
//				}
//			}
//		}
        idsTramitesAsArray = new ArrayList<Long>();
        for (long id : idsTramites) {
            idsTramitesAsArray.add(new Long(id));
        }

        answer = null;
        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idsTramites_p", idsTramitesAsArray);
            answer = (List<Avaluo>) super.findInRangeUsingQuery(query, contadoresDesdeYCuantos);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoDAOBean#obtenerParaAsignacion");
        }

        try {
            if (answer != null) {

                //N: si se va a remover algún elemento de la lista que se recorre, es mejor no usar iteradores
                //for (Avaluo avaluo : answer) {
                listaAvaluosTemp = new ArrayList<Avaluo>(answer);
                for (int noa = 0; noa < answer.size(); noa++) {
                    avaluoTemp = answer.get(noa);

                    isAsignado = false;
                    Hibernate.initialize(avaluoTemp.getAvaluoAsignacionProfesionals());

                    aapListTemp = new ArrayList<AvaluoAsignacionProfesional>(
                        avaluoTemp.getAvaluoAsignacionProfesionals());
                    for (int noaap = 0; noaap < avaluoTemp.getAvaluoAsignacionProfesionals().size();
                        noaap++) {

                        validAsignacion = false;
                        aapTemp = avaluoTemp.getAvaluoAsignacionProfesionals().get(noaap);

                        if (aapTemp.getActividad().equals(actividadCodigo)) {
                            if (aapTemp.getFechaHasta() == null) {
                                isAsignado = true;
                                validAsignacion = true;
                            }
                        }

                        if (!validAsignacion) {
                            aapListTemp.remove(aapTemp);
                        } else {
                            Hibernate.initialize(aapTemp.getProfesionalAvaluo());
                            Hibernate.initialize(aapTemp.getAsignacion());
                        }
                    }

                    if (!isAsignado) {
                        listaAvaluosTemp.remove(avaluoTemp);
                    } else {
                        avaluoTemp.setAvaluoAsignacionProfesionals(aapListTemp);
                    }
                }

                if (asignados) {
                    answer = listaAvaluosTemp;
                } else {
                    answer = ListUtils.subtract(answer, listaAvaluosTemp);
                }

                //D: esto se hace en un ciclo separado para evitar traer datos que se descartan en
                // la respuesta
                for (Avaluo avaluo : answer) {
                    Hibernate.initialize(avaluo.getAvaluoPredios());
                    for (AvaluoPredio predio : avaluo.getAvaluoPredios()) {
                        Hibernate.initialize(predio.getDepartamento());
                        Hibernate.initialize(predio.getMunicipio());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_HIBERNATE_INITIALIZE.getExcepcion(LOGGER,
                ex, "AvaluoDAOBean#obtenerParaOrdenPractica", "Avaluo");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoDAO#contarParaAsignacion(long[], java.lang.String, boolean, int...)
     * @author pedro.garcia
     */
    @Override
    public int contarParaAsignacion(long[] idsTramites, String actividadCodigo, boolean asignados,
        int... contadoresDesdeYCuantos) {

        int answer, asignadosCounter;
        List<Avaluo> avaluos = null;
        StringBuilder queryString;
        Query query;
        ArrayList<Long> idsTramitesAsArray;

        answer = -1;

        queryString = new StringBuilder();
        queryString.append(
            " SELECT a FROM Avaluo a " +
            " JOIN FETCH a.tramite t " +
            " WHERE t.id IN (:idsTramites_p)");

        idsTramitesAsArray = new ArrayList<Long>();
        for (long id : idsTramites) {
            idsTramitesAsArray.add(new Long(id));
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idsTramites_p", idsTramitesAsArray);
            avaluos = (List<Avaluo>) super.findInRangeUsingQuery(query, contadoresDesdeYCuantos);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoDAOBean#contarParaAsignacion");
        }

        try {
            if (avaluos != null) {
                asignadosCounter = 0;
                for (Avaluo avaluo : avaluos) {
                    Hibernate.initialize(avaluo.getAvaluoAsignacionProfesionals());
                    for (AvaluoAsignacionProfesional aap : avaluo.getAvaluoAsignacionProfesionals()) {
                        if (aap.getActividad().equals(actividadCodigo) &&
                            aap.getFechaHasta() == null) {
                            asignadosCounter++;
                            break;
                        }
                    }
                }

                if (asignados) {
                    answer = asignadosCounter;
                } else {
                    answer = avaluos.size() - asignadosCounter;
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_HIBERNATE_INITIALIZE.getExcepcion(LOGGER,
                ex, "AvaluoDAOBean#contarParaAsignacion", "Avaluo");
        }

        return answer;
    }

    /**
     * @see IAvaluoDAO#obtenerPorIdParaImpugnacionRevision(Long)
     * @author christian.rodriguez
     */
    @Override
    public Avaluo obtenerPorIdParaImpugnacionRevision(Long avaluoId) {
        Avaluo avaluoAImpugnarORevisar = null;
        String queryString;
        Query query;

        queryString = "SELECT ava " +
            " FROM Avaluo ava " +
            " WHERE ava.tramite.id = (" +
            " SELECT ava.tramite.solicitud.tramiteNumeroRadicacionRef " +
            " FROM Avaluo ava " +
            " WHERE  ava.id = :avaluoId)";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("avaluoId", avaluoId);

            avaluoAImpugnarORevisar = (Avaluo) query.getSingleResult();

            this.inicializarObjetosAvaluo(avaluoAImpugnarORevisar);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex, "Avaluo");
        }

        return avaluoAImpugnarORevisar;
    }

    /**
     * @see IAvaluoDAO#obtenerAvaluosConMovimientosPorIdAvaluador(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<Avaluo> obtenerAvaluosConMovimientosPorFiltro(
        FiltroDatosConsultaAvaluo filtro) {

        List<Avaluo> listaAvaluos = new ArrayList<Avaluo>();
        List<Long> listaIdsAvaluos = new ArrayList<Long>();
        List<AvaluoMovimiento> listaAvaluoMovimientos = new ArrayList<AvaluoMovimiento>();
        String queryString;
        Query query;

        /*
         * Banderas para determinar si ya fueron encontrados los ultimos movimientos de cada avaluo
         */
        boolean banderaMovimientoAmpliacionEncontrado = false;
        boolean banderaMovimientoSuspensionEncontrado = false;
        boolean banderaMovimientoReactivacionEncontrado = false;

        /*
         * Query para buscar los movimientos de los avaluos
         */
        queryString = "SELECT avaMo" + " FROM AvaluoMovimiento avaMo" +
            " WHERE" + " avaMo.avaluo.id IN :listaAvaluos" +
            " ORDER BY avaMo.id DESC";

        /*
         * Se consultan los avaluos
         */
        listaAvaluos = this.buscarAvaluosPorFiltro(filtro);

        /*
         * Se inicia la lista de ids de los avaluos
         */
        for (Avaluo avaluo : listaAvaluos) {
            listaIdsAvaluos.add(avaluo.getId());
        }

        /*
         * Se inicia la lista de movimientos para cada avaluo
         */
        for (Avaluo avaluo : listaAvaluos) {
            avaluo.setAvaluoMovimientos(new ArrayList<AvaluoMovimiento>());

            /*
             * Se hace fetch de las asignaciones de cada avaluo
             */
            Hibernate.initialize(avaluo.getAvaluoAsignacionProfesionals());
            for (AvaluoAsignacionProfesional aap : avaluo
                .getAvaluoAsignacionProfesionals()) {

                /*
                 * Se hace fetch de las ordenes de practica de cada asignacion
                 */
                Hibernate.initialize(aap.getOrdenPractica());
            }
        }

        /*
         * Se ejecuta el query para buscar los movimientos de los avaluos
         */
        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("listaAvaluos", listaIdsAvaluos);

            listaAvaluoMovimientos = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex,
                "AvaluoBean#obtenerAvaluosConMovimientosPorFiltro");
        }

        /*
         * Se buscan los ultimos movimientos de cada tipo para cada avaluo
         */
        for (Avaluo avaluo : listaAvaluos) {
            for (AvaluoMovimiento avaMo : listaAvaluoMovimientos) {

                /*
                 * Se valida que el movimiento esté asociado al avaluo
                 */
                if (avaMo.getAvaluo().getId().equals(avaluo.getId())) {
                    /*
                     * Se valida si el movimiento es de tipo AMPLIACION
                     */
                    if (avaMo.getTipoMovimiento().equals(
                        EMovimientoAvaluoTipo.AMPLIACION.getCodigo())) {

                        /*
                         * Se valida si ya se encontró un movimiento de tipo AMPLIACION
                         */
                        if (!banderaMovimientoAmpliacionEncontrado) {
                            /*
                             * Si no, se almacena en la lista de movimientos del avaluo asumiendo
                             * que es el ultimo movimiento de tipo AMPLIACION
                             */
                            avaluo.getAvaluoMovimientos().add(avaMo);

                            /*
                             * Se indica que ya se encontró el ultimo movimiento de tipo AMPLIACION
                             */
                            banderaMovimientoAmpliacionEncontrado = true;

                        }
                    }/*
                     * Se valida si el movimiento es de tipo SUSPENSION
                     */ else if (avaMo.getTipoMovimiento().equals(
                        EMovimientoAvaluoTipo.SUSPENCION.getCodigo())) {

                        /*
                         * Se valida si ya se encontró un movimiento de tipo SUSPENSION
                         */
                        if (!banderaMovimientoSuspensionEncontrado) {
                            /*
                             * Si no, se almacena en la lista de movimientos del avaluo asumiendo
                             * que es el ultimo movimiento de tipo SUSPENSION
                             */
                            avaluo.getAvaluoMovimientos().add(avaMo);

                            /*
                             * Se indica que ya se encontró el ultimo movimiento de tipo SUSPENSION
                             */
                            banderaMovimientoSuspensionEncontrado = true;

                        }
                    }/*
                     * Se valida si el movimiento es de tipo REACTIVACION
                     */ else if (avaMo.getTipoMovimiento().equals(
                        EMovimientoAvaluoTipo.REACTIVACION.getCodigo())) {

                        /*
                         * Se valida si ya se encontró un movimiento de tipo REACTIVACION
                         */
                        if (!banderaMovimientoReactivacionEncontrado) {
                            /*
                             * Si no, se almacena en la lista de movimientos del avaluo asumiendo
                             * que es el ultimo movimiento de tipo REACTIVACION
                             */
                            avaluo.getAvaluoMovimientos().add(avaMo);

                            /*
                             * Se indica que ya se encontró el ultimo movimiento de tipo
                             * REACTIVACION
                             */
                            banderaMovimientoReactivacionEncontrado = true;

                        }
                    }
                }
            }

            /*
             * Se resetean los valores de las banderas para el siguiente avaluo
             */
            banderaMovimientoAmpliacionEncontrado = false;
            banderaMovimientoReactivacionEncontrado = false;
            banderaMovimientoSuspensionEncontrado = false;
        }

        return listaAvaluos;
    }

//end of class
}
