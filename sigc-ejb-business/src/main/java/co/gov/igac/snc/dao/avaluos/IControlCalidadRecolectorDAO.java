/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadRecolector;

/**
 * interfaz para los métodos de bd de la tabla CONTROL_CALIDAD_RECOLECTOR
 *
 * @author ariel.ortiz
 * @version 2.0
 */
@Local
public interface IControlCalidadRecolectorDAO extends
    IGenericJpaDAO<ControlCalidadRecolector, Long> {

    /**
     * Metodo para buscar los controles de calidad asociados a un recolector por el id del Control
     * de Calidad
     *
     * @param idControl - id del control de calidad.
     *
     * @return
     */
    List<ControlCalidadRecolector> buscarControlCalidadRecolectorPorIdControl(
        Long idControl);

}
