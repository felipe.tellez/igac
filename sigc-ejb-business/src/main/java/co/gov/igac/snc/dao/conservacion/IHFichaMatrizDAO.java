package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatriz;

/**
 * Servicios de persistencia del objeto {@link HFichaMatriz}.
 *
 * @author leidy.gonzalez
 */
@Local
public interface IHFichaMatrizDAO extends
    IGenericJpaDAO<HFichaMatriz, Long> {

    /**
     * Método para consultar los {@link HFichaMatriz} por su numPredial
     *
     * @author leidy.gonzalez
     * @param numPredial
     * @return
     */
    public List<HFichaMatriz> findByNumeroPredial(String numPredial);

    /**
     * Retorna fichasMatriz encontrados del listado de hprediosIds enviado
     *
     * @author juan.cruz
     * @param prediosId
     * @return
     */
    public List<HFichaMatriz> buscarFichasMatrizporListadoprediosId(List<Long> prediosId);
}
