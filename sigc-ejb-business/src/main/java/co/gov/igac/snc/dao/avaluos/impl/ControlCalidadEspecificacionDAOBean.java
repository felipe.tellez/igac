/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadEspecificacionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadEspecificacion;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IControlCalidadEspecificacionDAO
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class ControlCalidadEspecificacionDAOBean extends GenericDAOWithJPA<ControlCalidadEspecificacion, Long>
    implements
    IControlCalidadEspecificacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControlCalidadEspecificacion.class);

}
