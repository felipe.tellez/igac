package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte datos reporte
 *
 * @author javier.aponte
 */
import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepDatosReporte;

@Local
public interface IReporteDatosReporteDAO extends IGenericJpaDAO<RepDatosReporte, Long> {

    /**
     * Método encargado de contar la cantidad de registros en la tabla reporte datos reporte por id
     * de reporte control reporte
     *
     * @param idReporteControlReporte
     * @return Cantidad de registros en la tabla reporte datos reporte
     * @author javier.aponte
     */
    public Integer contarRegistrosReporteDatosReportePorControlReporteId(
        Long idReporteControlReporte);
}
