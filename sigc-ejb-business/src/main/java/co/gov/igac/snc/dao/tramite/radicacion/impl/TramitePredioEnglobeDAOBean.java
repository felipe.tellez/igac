/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.ITramitePredioEnglobeDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 * @deprecated Porque la tabla se llama TRAMITE_DETALLE_PREDIO. Usar el entity y las clases
 * asociadas TramiteDetallePredio
 */
@Deprecated
@Stateless
public class TramitePredioEnglobeDAOBean extends GenericDAOWithJPA<TramitePredioEnglobe, Long>
    implements ITramitePredioEnglobeDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramitePredioEnglobeDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramitePredioEnglobeDAO#getByTramiteId(java.lang.Long)
     */
    @Implement
    @Override
    public List<TramitePredioEnglobe> getByTramiteId(Long idTramite) {

        LOGGER.debug("on TramitePredioEnglobeDAOBean#getTramitePrediosEnglobe ...");

        List<TramitePredioEnglobe> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tpe FROM TramitePredioEnglobe tpe JOIN FETCH tpe.predio p " +
            " WHERE tpe.tramite.id = :idTramiteP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idTramiteP", idTramite);
            answer = query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;

    }

    @Override
    public List<String> getNumerosPredialesByTramiteId(List<Long> tramiteIds) {
        LOGGER.debug("on TramitePredioEnglobeDAOBean#getNumerosPredialesByTramiteId ...");
        List<String> numerosPrediales = new ArrayList<String>();
        String queryString = "SELECT tpe.predio.numeroPredial FROM TramitePredioEnglobe tpe " +
            " LEFT JOIN tpe.predio " + " LEFT JOIN tpe.tramite " +
            " WHERE tpe.tramite.id in (:tramiteIds)";

        try {
            Query query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteIds", tramiteIds);
            numerosPrediales = (List<String>) query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramitePredioEnglobeDAOBean#getNumerosPredialesByTramiteId");
        }

        return numerosPrediales;
    }

}
