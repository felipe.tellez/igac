/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ITramiteRelacionTramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentacionDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRelacionTramite;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class TramiteRelacionTramiteDAOBean extends
    GenericDAOWithJPA<TramiteRelacionTramite, Long> implements
    ITramiteRelacionTramiteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteRelacionTramiteDAOBean.class);

    @EJB
    private ITramiteRelacionTramiteDAO tramiteRelacionTramiteDAO;

//--------------------------------------------------------------------------------------------------    
    /**
     * @see ITramiteDocumentacionDAO#findTramiteRelacionTramiteByIdTramite(Long)
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TramiteRelacionTramite> findTramiteRelacionTramiteByIdTramite(Long tramiteId) {
        Query query = this.entityManager.createQuery("SELECT trt" +
            " FROM TramiteRelacionTramite trt" +
            " WHERE trt.tramite1.id = :tramiteId");
        query.setParameter("tramiteId", tramiteId);
        try {
            return (List<TramiteRelacionTramite>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteRelacionTramiteDAOBean#findTramiteRelacionTramiteByIdTramite");
        }
    }
}
