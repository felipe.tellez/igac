package co.gov.igac.snc.dao.conservacion.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPersonaPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class PPersonaPredioDAOBean extends GenericDAOWithJPA<PPersonaPredio, Long> implements
    IPPersonaPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPersonaPredioDAOBean.class);

    /**
     * @author juan.agudelo
     * @see IPPersonaPredioDAO#buscarPPersonaPredioPorIdFetchPredio(Long)
     */
    public PPersonaPredio buscarPPersonaPredioPorIdFetchPredio(Long id) {
        String query = "SELECT pp" +
            " FROM PPersonaPredio pp " +
            " JOIN FETCH pp.PPredio" +
            " WHERE pp.id = :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("id", id);
        try {
            PPersonaPredio p = (PPersonaPredio) q.getSingleResult();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see IPPersonaPredioDAO#findByPPredioId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPersonaPredio> findByPPredioId(Long idPPredio) {
        String sql = "SELECT ppp FROM PPersonaPredio ppp" +
            " LEFT JOIN FETCH ppp.PPersona" +
            " LEFT JOIN FETCH ppp.PPredio" +
            " LEFT JOIN FETCH ppp.PPersonaPredioPropiedads" +
            " WHERE ppp.PPredio.id = :idPPredio";
        Query q = this.entityManager.createQuery(sql).setParameter("idPPredio",
            idPPredio);
        return q.getResultList();
    }

    /**
     * @see IPPersonaPredioDAO#replacePropietarios(PPredio, List)
     * @author fabio.navarrete
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PPredio replacePropietarios(PPredio pPredio,
        List<PPersonaPredio> pPersonaPredios) {
        for (PPersonaPredio ppp : pPredio.getPPersonaPredios()) {
            PPersonaPredio pppAux = this.findById(ppp.getId());
            this.delete(pppAux);
        }

        for (PPersonaPredio ppp : pPersonaPredios) {
            ppp.setId(null);
            ppp.setFechaLog(new Date());
            ppp.setPPredio(pPredio);
        }

        pPredio.setPPersonaPredios(pPersonaPredios);
        return this.entityManager.merge(pPredio);
    }

    /**
     * @see IPPersonaPredioDAO#findByIdPPredios(List<Long>)
     */
    @Override
    public List<PPersonaPredio>  findByIdPPredios(List<Long> idPPredio) {

        List<PPersonaPredio> answer = null;

        String sql = "SELECT ppp FROM PPersonaPredio ppp" +
            " LEFT JOIN FETCH ppp.PPersona" +
            " LEFT JOIN FETCH ppp.PPredio" +
            " WHERE ppp.PPredio.id IN (:idPPredio)";

        try {

            Query q = this.entityManager.createQuery(sql).setParameter("idPPredio",
                idPPredio);

            answer = (List<PPersonaPredio>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPersonaPredioDAOBean#findByIdPPredios");
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IPersonaPredioDAO#buscarPPersonasPredioContandoPredios(Long)
     */
    @Override
    public List<PPersonaPredio> buscarPPersonasPredioContandoPredios(List<Long> predioIds) {

        List<PPersonaPredio> answer = null;

        String query = "SELECT DISTINCT pp FROM PPersonaPredio pp " +
            " LEFT JOIN FETCH pp.PPersona" +
            " LEFT JOIN FETCH pp.PPredio p" +
            " WHERE p.id IN (:predioIds)";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("predioIds", predioIds);

            answer = (List<PPersonaPredio>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PersonaPredioDAOBean#buscarPPersonasPredioContandoPredios");
        }
        return answer;
    }

    /**
     * @author.franz.gamba
     * @see IPersonaPredioDAO#countPersonaPrediosByPersonaId(Long)
     */
    @Override
    public Long countPersonaPrediosByPersonaId(Long personaId) {
        Long answer = null;

        String query = "SELECT COUNT (pp) FROM PPersonaPredio pp " +
            " WHERE pp.persona.id = :personaId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("personaId", personaId);

            answer = (Long) q.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PersonaPredioDAOBean#countPersonaPrediosByPersonaId");
        }
        return answer;
    }

    /**
     * @see IPPersonaPredioDAO#findByPPredioIdCancelacionInscritos(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPersonaPredio> findByPPredioIdCancelaInscritos(Long idPPredio) {
        List<PPersonaPredio> answer = null;

        String sql = "SELECT ppp FROM PPersonaPredio ppp" +
            " LEFT JOIN FETCH ppp.PPersona" +
            " LEFT JOIN FETCH ppp.PPredio" +
            " LEFT JOIN FETCH ppp.PPersonaPredioPropiedads" +
            " WHERE ppp.PPredio.id = :idPPredio" +
            " AND ppp.cancelaInscribe = :cancelaInscribe";

        try {

            Query q = this.entityManager.createQuery(sql).setParameter("idPPredio",
                idPPredio);
            q.setParameter("cancelaInscribe", EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

            answer = q.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#consultarResolucionesPorFechaResolucionAplicaCambios");
        }

        return answer;
    }

}
