/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.formacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.formacion.ICalificacionAnexoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class CalificacionAnexoDAOBean extends GenericDAOWithJPA<CalificacionAnexo, Long>
    implements ICalificacionAnexoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalificacionAnexoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    public List<CalificacionAnexo> findByUsoConstruccionId(Long usoConstruccionId) {

        List<CalificacionAnexo> answer = null;
        Query query;

        try {
            query = this.entityManager.createNamedQuery("findByUsoConstruccionId");
            query.setParameter("idUC", usoConstruccionId);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.debug(
                "error en la consulta de calificacion_anexo por id de uso de construcción: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     * @see ICalificacionAnexoDAO#getAllCalificacionAnexo()
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<CalificacionAnexo> getAllCalificacionAnexo() {
        List<CalificacionAnexo> calificacionesAnexo = null;
        Query query;
        String q;

        q = " SELECT ca FROM CalificacionAnexo ca ";

        query = entityManager.createQuery(q);

        try {
            calificacionesAnexo = (List<CalificacionAnexo>) query.getResultList();
        } catch (ExcepcionSNC e) {

            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "CalificacionAnexoDAOBean#getAllCalificacionAnexo");
        }

        return calificacionesAnexo;
    }

}
