/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.DuracionTramite;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla DURACION_TRAMITE
 *
 * @author pedro.garcia
 */
@Local
public interface IDuracionTramiteDAO extends IGenericJpaDAO<DuracionTramite, Long> {

    /**
     * retorna el valor del campo NORMA_DIA de la tabla según los criterios que se pasan como
     * parámetros. Además revisa que la fecha en que se consulta esté entre las fechas de vigencia
     * para esa norma.
     *
     * @auhor pedro.garcia
     * @param tipoAvaluo
     * @param esSede
     * @param area
     * @return
     */
    public Integer getNormaDia(String tipoAvaluo, String esSede, double area);
}
