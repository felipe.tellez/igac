/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.CirculoRegistralMunicipio;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 * interface para el DAO de CirculoRegistralMunicipio
 *
 * @author javier.aponte
 */
@Local
public interface ICirculoRegistralMunicipioDAO extends
    IGenericJpaDAO<CirculoRegistralMunicipio, String> {

    /**
     * Retorna la lista de círculo registral municipio por el código del circulo registral
     *
     * @author javier.aponte
     * @param codigo código del circulo registral
     * @return
     */
    public List<CirculoRegistralMunicipio> findByCodigoCirculoRegistral(
        String codigoCirculoRegistral);

}
