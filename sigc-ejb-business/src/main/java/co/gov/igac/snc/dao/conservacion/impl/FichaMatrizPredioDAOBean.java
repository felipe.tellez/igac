/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author juan.agudelo
 */
@Stateless
public class FichaMatrizPredioDAOBean extends
    GenericDAOWithJPA<FichaMatrizPredio, Long> implements
    IFichaMatrizPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FichaMatrizPredioDAOBean.class);

    // -------------------------------------------------------------------------
    /**
     * @see IFichaMatrizPredioDAO#getCoeficienteCopropiedadByNumeroPredial(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Double getCoeficienteCopropiedadByNumeroPredial(String numeroPredial) {

        LOGGER.debug("executing PredioDAOBean#getCoeficienteCopropiedadByNumeroPredial");

        Double answer = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT fmp.coeficiente FROM FichaMatrizPredio fmp");
        query.append(" WHERE fmp.numeroPredial = :numeroPredial");

        Query q = entityManager.createQuery(query.toString());

        q.setParameter("numeroPredial", numeroPredial);

        try {
            answer = (Double) q.getSingleResult();
        } catch (NoResultException nr) {
            return -0.0;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PredioDAOBean#getCoeficienteCopropiedadByNumeroPredial");
        }

        return answer;

    }

    /**
     *
     * @see IFichaMatrizPredioDAO#obtenerFichaMatrizPredioPorNumeroPredial(java.lang.String)
     * @author javier.aponte
     * @param numeroPredial
     * @return
     */
    @Override
    public FichaMatrizPredio obtenerFichaMatrizPredioPorNumeroPredial(String numeroPredial) {

        LOGGER.debug("executing FichaMatrizPredioDAOBean#obtenerFichaMatrizPredioPorNumeroPredial");

        FichaMatrizPredio resultado = null;

        String q1 = "SELECT fmp FROM FichaMatrizPredio fmp" +
            " JOIN FETCH fmp.fichaMatriz fm " +
            " WHERE fmp.numeroPredial = :numeroPredial";

        try {

            Query q = entityManager.createQuery(q1);

            q.setParameter("numeroPredial", numeroPredial);

            resultado = (FichaMatrizPredio) q.getSingleResult();

            Hibernate.initialize(resultado.getFichaMatriz());

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FichaMatrizPredioDAOBean#obtenerFichaMatrizPredioPorNumeroPredial");
        }

        return resultado;

    }

    // end of class
}
