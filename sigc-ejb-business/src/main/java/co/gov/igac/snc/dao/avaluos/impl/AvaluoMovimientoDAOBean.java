/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoMovimientoDAO;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import co.gov.igac.snc.persistence.util.EMovimientoAvaluoTipo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoMovimientoDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoMovimientoDAOBean extends GenericDAOWithJPA<AvaluoMovimiento, Long>
    implements IAvaluoMovimientoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoMovimientoDAOBean.class);

    /**
     * @see IAvaluoMovimientoDAO#calcularDiasSuspensionAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Integer calcularDiasSuspensionAvaluo(Long idAvaluo) {

        Integer resultado = 0;
        String queryString;
        Query query;

        try {

            queryString = "SELECT am " +
                " FROM AvaluoMovimiento am " +
                " INNER JOIN am.avaluo ava " +
                " WHERE ava.id = :avaluo " +
                " AND am.tipoMovimiento LIKE :tipoMovimiento " +
                " AND am.fechaHasta != NULL ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluo", idAvaluo);
            query.setParameter("tipoMovimiento", EMovimientoAvaluoTipo.SUSPENCION.getCodigo());

            List<AvaluoMovimiento> suspensiones = query.getResultList();
            LOGGER.debug("Movs --- " + suspensiones.size());

            for (AvaluoMovimiento avaluoMovimiento : suspensiones) {

                if (avaluoMovimiento.getFechaDesde() != null && avaluoMovimiento.getFechaHasta() !=
                    null) {
                    resultado += QueryNativoDAO.calcularDiasHabilesEntreDosFechas(
                        this.entityManager,
                        avaluoMovimiento.getFechaDesde(),
                        avaluoMovimiento.getFechaHasta());
                }

                LOGGER.debug("Mov --- " + avaluoMovimiento.getId() + " - Dias >> " + resultado);
            }
            //TODO::felipe.cadena::se debe definir los dias de suspension indefininida

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

    /**
     * @see IAvaluoMovimientoDAO#calcularDiasAmpliacionAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Integer calcularDiasAmpliacionAvaluo(Long idAvaluo) {

        Long resultado = 0L;
        String queryString;
        Query query;

        try {

            queryString = "SELECT sum(am.diasAmpliacion) " +
                " FROM AvaluoMovimiento am " +
                " INNER JOIN am.avaluo ava " +
                " WHERE ava.id = :avaluo " +
                " AND am.tipoMovimiento LIKE :tipoMovimiento " +
                " GROUP BY ava.id ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluo", idAvaluo);
            query.setParameter("tipoMovimiento", EMovimientoAvaluoTipo.AMPLIACION.getCodigo());

            resultado = (Long) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado.intValue();
    }

}
