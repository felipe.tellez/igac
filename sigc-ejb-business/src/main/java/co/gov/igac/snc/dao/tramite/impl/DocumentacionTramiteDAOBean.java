package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IDocumentacionTramiteDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class DocumentacionTramiteDAOBean extends
    GenericDAOWithJPA<TramiteDocumentacion, Long> implements
    IDocumentacionTramiteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DocumentacionTramiteDAOBean.class);

    /**
     * @author juan.agudelo
     */
    @Override
    public List<TramiteDocumentacion> buscarDocumentosTramitePorNumeroDeRadicacion(
        String numeroRadicacion) {
        LOGGER.
            debug("executing DocumentacionTramiteDAO#buscarDocumentosTramitePorumeroDeRadicacion");
        //probar con null para las pruebas de integración
        List<TramiteDocumentacion> tramiteDocumentacions = null;

        Query q = entityManager.createQuery("select td " +
            "from TramiteDocumentacion td JOIN FETCH" +
            " td.departamento JOIN FETCH td.municipio JOIN FETCH td.tipoDocumento" +
            " where td.tramite.numeroRadicacion = :numeroRadicacion");
        q.setParameter("numeroRadicacion", numeroRadicacion);

        try {
            tramiteDocumentacions = q.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de documentacion de tramites");
        }

        return tramiteDocumentacions;
    }

//--------------------------------------------------------------------------------------------------
    /**
     *
     */
    @Override
    public void ingresarDocumentosAdicionalesRequeridos(
        List<TramiteDocumentacion> documentosAdicionales) {

        LOGGER.
            debug("executing DocumentacionTramiteDAOBean#ingresarDocumentosAdicionalesRequeridos");

        for (int td = 0; td < documentosAdicionales.size(); td++) {

            TramiteDocumentacion documentacion = documentosAdicionales.get(td);
            try {
                this.update(documentacion);
            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                    getExcepcion(LOGGER, ex,
                        "DocumentacionTramiteDAOBean#ingresarDocumentosAdicionalesRequeridos",
                        "TramiteDocumentacion", documentacion.getId());
            }
        }

    }

}
