/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteTareaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see ITramiteTareaDAO
 *
 * @author felipe.cadena
 */
@Stateless
public class TramiteTareaDAOBean extends GenericDAOWithJPA<TramiteTarea, Long> implements
    ITramiteTareaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteTareaDAOBean.class);

    /**
     * @see ITramiteTareaDAO#obtenerTareasTramite(java.lang.Long, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<TramiteTarea> obtenerTareasTramite(Long tramiteId, String tarea) {
        List<TramiteTarea> resultado = null;

        StringBuffer queryString;
        Query query;

        queryString = new StringBuffer("SELECT tt " +
            " FROM TramiteTarea tt " +
            " JOIN FETCH tt.tramite t" +
            " WHERE t.id = :tramiteId ");

        if (tarea != null) {
            queryString.append(" AND tt.tarea like :tarea");
        }

        LOGGER.debug(queryString.toString());

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("tramiteId", tramiteId);

            if (tarea != null) {
                query.setParameter("tarea", tarea);
            }

            resultado = (List<TramiteTarea>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return resultado;
    }

    /**
     * @see ITramiteTareaDAO#obtenerTramitesEnJobGeografico()
     * @author felipe.cadena
     */
    @Override
    public List<TramiteTarea> obtenerTramitesEnJobGeografico() {

        List<TramiteTarea> answer = null;
        try {
            String queryString;
            Query query;

            queryString = "SELECT tt FROM TramiteTarea tt " +
                " JOIN FETCH tt.tramite t " +
                " WHERE " +
                " (t.resultado not like '%ERROR') AND " +
                " ((T.tarea = 'APLICAR_CAMBIOS_CONSERVACION' AND t.paso = 1) OR " +
                " (T.tarea = 'APLICAR_CAMBIOS_DEPURACION' AND T.paso = 2) OR " +
                " (T.tarea = 'GENERAR_COPIA_CONSULTA' AND T.paso = 3) OR " +
                " (T.tarea = 'GENERAR_COPIA_EDICION' AND T.paso = 3) )" +
                " and tt.id = " +
                " ( SELECT MAX(tt2.id) FROM TramiteTarea tt2 " +
                "  WHERE tt2.tramite.id = t.id AND tt2.tarea = t.tarea AND tt2.paso = t.paso AND tt2.resultado = t.resultado)";

            query = this.entityManager.createQuery(queryString);

            answer = (List<TramiteTarea>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;

    }

}
