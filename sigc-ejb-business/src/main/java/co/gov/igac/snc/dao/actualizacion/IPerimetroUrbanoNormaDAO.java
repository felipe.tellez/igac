package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.PerimetroUrbanoNorma;

@Local
public interface IPerimetroUrbanoNormaDAO extends IGenericJpaDAO<PerimetroUrbanoNorma, Long> {

}
