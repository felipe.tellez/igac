/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.tramite.radicacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.IDeterminaTramitePHDAO;
import co.gov.igac.snc.persistence.entity.tramite.DeterminaTramitePH;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author felipe.cadena
 */
@Stateless
public class DeterminaTramitePHDAOBean extends GenericDAOWithJPA<DeterminaTramitePH, Long>
    implements IDeterminaTramitePHDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeterminaTramitePH.class);

    /**
     * @see DeterminaTramitePHDAOBean#obtenerPorParametrosTramite(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<DeterminaTramitePH> obtenerPorParametrosTramite(String tipoTramite,
        String claseMutacion, String subtipo, String condicionPropiedad) {

        LOGGER.debug("on DeterminaTramitePHDAOBean#obtenerPorParametrosTramite ");

        List<DeterminaTramitePH> answer = null;
        StringBuilder queryString = new StringBuilder();
        Query query;

        try {

            queryString.append("SELECT dt FROM DeterminaTramitePH dt" +
                " WHERE 1 = 1 ");

            if (tipoTramite != null && !tipoTramite.isEmpty()) {
                queryString.append(" AND dt.tipoTramite = :tipoTramiteP");
            }
            if (claseMutacion != null && !claseMutacion.isEmpty()) {
                queryString.append(" AND dt.claseMutacion = :claseMutacionP");
            }
            if (subtipo != null && !subtipo.isEmpty()) {
                queryString.append(" AND dt.subtipo = :subtipoP");
            }
            if (condicionPropiedad != null && !condicionPropiedad.isEmpty()) {
                queryString.append(" AND dt.condicionPropiedad = :condicionPropiedadP");
            }

            query = this.entityManager.createQuery(queryString.toString());

            if (tipoTramite != null && !tipoTramite.isEmpty()) {
                query.setParameter("tipoTramiteP", tipoTramite);
            }
            if (claseMutacion != null && !claseMutacion.isEmpty()) {
                query.setParameter("claseMutacionP", claseMutacion);
            }
            if (subtipo != null && !subtipo.isEmpty()) {
                query.setParameter("subtipoP", subtipo);
            }
            if (condicionPropiedad != null && !condicionPropiedad.isEmpty()) {
                query.setParameter("condicionPropiedadP", condicionPropiedad);
            }

            answer = query.getResultList();

        } catch (Exception e) {
            LOGGER.error("Error en DeterminaTramitePHDAOBean#obtenerPorParametrosTramite: " + e.
                getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "DeterminaTramitePHDAOBean#obtenerPorParametrosTramite");
        }

        return answer;

    }

}
