package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ConvenioEntidad;

/**
 * Servicios de persistencia del objeto {@link ConvenioEntidad}.
 *
 * @author david.cifuentes
 */
@Local
public interface IConvenioEntidadDAO extends IGenericJpaDAO<ConvenioEntidad, Long> {

}
