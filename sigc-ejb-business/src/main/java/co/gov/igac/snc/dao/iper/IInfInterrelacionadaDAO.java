/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.iper;

import java.util.List;

import javax.ejb.Local;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionada;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;

/**
 * Interfaz para IPER (Interrelación Permanente)
 *
 * @author fredy.wilches
 * @version 2.0
 */
@Local
public interface IInfInterrelacionadaDAO extends IGenericJpaDAO<InfInterrelacionada, Long> {

    /**
     * Retorna los tramites de primera pendientes de procesar
     *
     * @author fredy.wilches
     * @return
     */
    public List<InfInterrelacionada> getTramitesPrimeraPendientes();

}
