package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author fredy.wilches
 *
 */
@Stateless
public class SolicitanteSolicitudDAOBean extends
    GenericDAOWithJPA<SolicitanteSolicitud, Long> implements
    ISolicitanteSolicitudDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitanteSolicitudDAOBean.class);

    @Override
    public List<SolicitanteSolicitud> getSolicitantesBySolicitud(Long solicitudId) {
        LOGGER.debug("SolicitanteSolicitudDAOBean#getSolicitantesBySolicitud");
        String queryString = "SELECT ss FROM SolicitanteSolicitud ss" +
            " WHERE ss.solicitud.id = :idSolicitud";

        Query q = entityManager.createQuery(queryString);
        q.setParameter("idSolicitud", solicitudId);
        try {
            return (List<SolicitanteSolicitud>) q.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Error al consultar los solicitantes de la solicitud: " + e.getMessage());
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @modified pedro.garcia - hacer fetch del Solicitante, SIN left porque el solicitante es
     * obligatorio (si se totea paila) - NO hacer return del resultado de la ejecución de query en
     * una sola línea (porque no me deja ver la variable en el debug!!!) - hacer LEFT JOIN FETCH
     * porque por malos datos podría no obtener resultados cuando sí los hay - se hace left join del
     * país, departamento y municipio del atributo solicitante
     */
    @Override
    public List<SolicitanteSolicitud> findBySolicitudId(Long solicitudId) {
        LOGGER.debug("SolicitanteSolicitudDAOBean#getSolicitantesBySolicitud");

        List<SolicitanteSolicitud> answer;
        String queryString = "SELECT ss FROM SolicitanteSolicitud ss " +
            " JOIN FETCH ss.solicitante s " +
            " LEFT JOIN FETCH s.direccionPais LEFT JOIN FETCH s.direccionDepartamento " +
            " LEFT JOIN FETCH s.direccionMunicipio" +
            " LEFT JOIN FETCH ss.direccionPais " +
            " LEFT JOIN FETCH ss.direccionDepartamento " +
            " LEFT JOIN FETCH ss.direccionMunicipio " +
            " WHERE ss.solicitud.id = :idSolicitud";

        Query q = entityManager.createQuery(queryString);
        q.setParameter("idSolicitud", solicitudId);
        try {
            answer = (List<SolicitanteSolicitud>) q.getResultList();
            return answer;
        } catch (Exception e) {
            LOGGER.debug("Error al consultar los solicitantes de la solicitud: " + e.getMessage());
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISolicitanteSolicitudDAO#getSolicitantesBySolicitud2(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<SolicitanteSolicitud> getSolicitantesBySolicitud2(Long solicitudId) {

        LOGGER.debug("on SolicitanteSolicitudDAOBean#getSolicitantesBySolicitud2 ...");
        List<SolicitanteSolicitud> answer = null;
        Query query;

        String queryString = "SELECT ss FROM SolicitanteSolicitud ss " +
            " LEFT JOIN FETCH ss.solicitante s" +
            " LEFT JOIN FETCH ss.direccionPais " +
            " LEFT JOIN FETCH ss.direccionDepartamento " +
            " LEFT JOIN FETCH ss.direccionMunicipio " +
            " WHERE ss.solicitud.id = :idSolicitud ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", solicitudId);

            answer = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "SolicitanteSolicitudDAOBean#getSolicitantesBySolicitud2");
        } finally {
            return answer;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISolicitanteSolicitudDAO#getSolicitanteSolicitudAleatorio
     * @author fredy.wilches
     */
    @Override
    public List<SolicitanteSolicitud> getSolicitanteSolicitudAleatorio() {
        String consulta = "SELECT ss FROM SolicitanteSolicitud ss";
        Query q = this.entityManager.createQuery(consulta);
        q.setMaxResults(1);
        SolicitanteSolicitud ss = (SolicitanteSolicitud) q.getSingleResult();

        consulta += " WHERE ss.solicitante.id<>:solicitanteId";
        q = this.entityManager.createQuery(consulta);
        q.setParameter("solicitanteId", ss.getSolicitante().getId());
        q.setMaxResults(1);
        SolicitanteSolicitud ss2 = (SolicitanteSolicitud) q.getSingleResult();

        consulta += " AND ss.solicitante.id<>:solicitanteId2";
        q = this.entityManager.createQuery(consulta);
        q.setParameter("solicitanteId", ss.getSolicitante().getId());
        q.setParameter("solicitanteId2", ss2.getSolicitante().getId());
        q.setMaxResults(1);
        SolicitanteSolicitud ss3 = (SolicitanteSolicitud) q.getSingleResult();

        List<SolicitanteSolicitud> solicitantes = new ArrayList<SolicitanteSolicitud>();
        solicitantes.add(ss);
        solicitantes.add(ss2);
        solicitantes.add(ss3);

        return solicitantes;
    }

    /**
     * @see ISolicitanteSolicitudDAO#getSolicitanteSolicitudByIdSolicitudRelacion(Long, String)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<SolicitanteSolicitud> getSolicitanteSolicitudByIdSolicitudRelacion(
        Long idSolicitud, String solicitanteSolicitudRelacion) {

        LOGGER.debug(
            "on SolicitanteSolicitudDAOBean#getSolicitanteSolicitudByIdSolicitudRelacion ...");

        List<SolicitanteSolicitud> answer = null;

        String queryString = "SELECT ss " +
            " FROM SolicitanteSolicitud ss " +
            " LEFT JOIN FETCH ss.direccionDepartamento " +
            " LEFT JOIN FETCH ss.direccionMunicipio " +
            " LEFT JOIN FETCH ss.solicitante " +
            " WHERE ss.solicitud.id = :idSolicitud " +
            " AND ss.relacion = :solicitanteSolicitudEstado";

        Query q = entityManager.createQuery(queryString);
        q.setParameter("idSolicitud", idSolicitud);
        q.setParameter("solicitanteSolicitudEstado", solicitanteSolicitudRelacion);

        try {
            answer = (List<SolicitanteSolicitud>) q.getResultList();
            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.debug("Error al consultar los solicitantes de la solicitud: " + e.getMessage());
            return null;
        }
        return answer;
    }

}
