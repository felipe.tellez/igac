package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.ILevantamientoAsignacionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;

/**
 * Implementación de los servicios de persistencia del objeto LevantamientoAsignacion.
 *
 * @author javier.barajas
 */
@Stateless
public class LevantamientoAsignacionDAOBean extends GenericDAOWithJPA<LevantamientoAsignacion, Long>
    implements ILevantamientoAsignacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(LevantamientoAsignacionDAOBean.class);

    /**
     * @seeILevantamientoAsignacionDAO#buscarLevantamientoAsignacionPorTopografoIdentificacion(String)
     * @author javier.aponte
     */
    @Override
    public LevantamientoAsignacion buscarLevantamientoAsignacionPorTopografoIdentificacion(
        String topografoIdentificacion) {

        LOGGER.debug(
            "Entra en LevantamientoAsignacionDAOBean#buscarLevantamientoAsignacionPorTopografoIdentificacion");

        LevantamientoAsignacion answer = null;

        Query q = entityManager
            .createQuery("SELECT la" +
                " FROM LevantamientoAsignacion la " +
                " WHERE la.topografoIdentificacion = :topografoIdentificacion");
        q.setParameter("topografoIdentificacion", topografoIdentificacion);

        try {
            answer = (LevantamientoAsignacion) q.getSingleResult();

            return answer;
        } catch (NoResultException e) {
            LOGGER.error(
                "LevantamientoAsignacionDAOBean#buscarLevantamientoAsignacionPorTopografoIdentificacion:" +
                "Error al consultar levantamiento asignacion por topografo identificación");
            return null;
        }

    }

    /**
     * @seeILevantamientoAsignacionDAO#buscarLevantamientoAsignacionPorRecursoHumanoId(Long)
     * @author javier.barajas
     */
    @Override
    public LevantamientoAsignacion buscarLevantamientoAsignacionPorRecursoHumanoId(
        Long recursoHumanoId) {
        LOGGER.debug(
            "Entra en LevantamientoAsignacionDAOBean#buscarLevantamientoAsignacionPorRecursoHumanoId");

        LevantamientoAsignacion answer = null;
        //List<LevantamientoAsignacion> answer = null;

        Query q = entityManager
            .createQuery("SELECT la" +
                " FROM LevantamientoAsignacion la " //+ " LEFT JOIN FETCH  la.recursoHumano rh " 
                +
                 " WHERE la.recursoHumano.id = :recursoHumanoId"
            );
        q.setParameter("recursoHumanoId", recursoHumanoId);

        try {
            answer = (LevantamientoAsignacion) q.getSingleResult();
            //answer = (List<LevantamientoAsignacion>) q.getResultList();
            //LOGGER.debug("Tamaño answer:"+answer.size());

            return answer;
        } catch (NoResultException e) {
            LOGGER.error(
                "LevantamientoAsignacionDAOBean#buscarLevantamientoAsignacionPorTopografoIdentificacion:" +
                "Error al consultar levantamiento asignacion por topografo identificación");
            return null;
        }
    }

}
