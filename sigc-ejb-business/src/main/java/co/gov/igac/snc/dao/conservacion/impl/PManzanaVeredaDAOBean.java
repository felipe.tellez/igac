/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPManzanaVeredaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author felipe.cadena
 */
@Stateless
public class PManzanaVeredaDAOBean extends GenericDAOWithJPA<PManzanaVereda, String> implements
    IPManzanaVeredaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PManzanaVeredaDAOBean.class);

    /**
     *
     * @see PManzanaVeredaDAOBean#obtenerPorTramiteId(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<PManzanaVereda> obtenerPorTramiteId(Long tramiteId) {

        List<PManzanaVereda> result = new ArrayList<PManzanaVereda>();
        String sql = "SELECT pm FROM PManzanaVereda pm" +
            " WHERE pm.tramite.id = :idTramite";

        Query query = this.entityManager.createQuery(sql);
        query.setParameter("idTramite", tramiteId);
        try {
            result = query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;

    }

}
