package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPReferenciaCartograficaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;

@Stateless
public class PReferenciaCartograficaDAOBean extends
    GenericDAOWithJPA<PReferenciaCartografica, Long> implements
    IPReferenciaCartograficaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        PReferenciaCartograficaDAOBean.class);

}
