package co.gov.igac.snc.dao.sig;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * Cliente para obtener el token de seguridad de ArcGis Server a través de REST
 *
 * http://help.arcgis.com/en/arcgisserver/10.0/help/arcgis_server_java_help/index.html#/Tokens_and_token_services/0092000000p2000000/
 *
 * @author juan.mendez
 */
public class ArcGisServerTokenRESTClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArcGisServerTokenRESTClient.class);

    /**
     * Máxima cantidad de veces que intenta obtener el token
     */
    private final int MAX_ATTEMPTS = 10;

    private String tokenServiceUrl;
    private String tokenServiceUser;
    private String tokenServicePwd;
    private ClientConfig cc;

    /**
     *
     * @param remoteUrl Url del geoservicio publicado en arcgis server
     * @param polling cantidad de milisegundos para hacer "polling" al servidor AGS
     */
    public ArcGisServerTokenRESTClient(String remoteUrl, String user, String pwd) {
        this.tokenServiceUrl = remoteUrl;
        tokenServiceUser = user;
        tokenServicePwd = pwd;
        cc = new DefaultClientConfig();
        cc.getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
    }

    /**
     * Se obtiene el token de arcgis. Implementa un mecanismo de failover que intenta obtener el
     * token varias veces (MAX_ATTEMPTS) (En el caso que algún servidor atrás del balanceador de
     * carga se encuentre fallando)
     *
     * Los MAX_ATTEMPTS deberían definirse según la cantidad de servidores de procesamiento que se
     * tengan disponibles
     *
     * http://services.arcgisonline.com/ArcGIS/SDK/REST/index.html?overview.html
     *
     * @return true si es válido
     */
    public ArcgisServerTokenDTO obtenerToken() {
        LOGGER.debug("obtenerToken");
        ArcgisServerTokenDTO token = null;
        int attempts = 0;
        while (token == null && attempts <= MAX_ATTEMPTS) {
            attempts++;
            try {
                token = requestToken(attempts);
            } catch (Exception e) {
                LOGGER.warn("Error obteniendo Token : " + e.getMessage(), e);
                try {
                    //esperando n segundos
                    Thread.sleep(attempts * 1000);
                } catch (InterruptedException e1) {
                    LOGGER.warn(e1.getMessage());
                }
            }

        }
        if (token == null) {
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, null,
                "No se pudo obtener conexión a ningún servidor SIG (Arcgis Server)");
        }
        return token;
    }

    /**
     * Se encarga de obtener un token de autenticación remota en el servidor de Arcgis Server
     *
     * @return
     */
    private ArcgisServerTokenDTO requestToken(int attempt) {
        LOGGER.debug("ObtenerToken  -  attempt :" + attempt);
        ArcgisServerTokenDTO token = null;
        final List<Object> cookies = new ArrayList<Object>();

        try {
            String replicaServiceUrl = tokenServiceUrl;
            Client client = Client.create(cc);
            client.addFilter(new ClientFilter() {
                /**
                 * Obtiene una copia de los cookies que retornó el servidor remoto (Para el caso del
                 * balanceador de carga, se busca que la ejecución y consultas al job se realicen
                 * únicamente al servidor que está ejecutando el trabajo. ( Estrategia dados los
                 * problemas de clustering para los toolbox en AGS 10 )
                 */
                @Override
                public ClientResponse handle(ClientRequest request) throws ClientHandlerException {
                    ClientResponse response = getNext().handle(request);
                    MultivaluedMap<String, String> headers = response.getHeaders();
                    // Nota : este fu� necesario para que funcionara tanto en JBoss como en la prueba unitaria
                    LOGGER.debug("Headers:" + response.getHeaders());
                    for (String key : headers.keySet()) {
                        LOGGER.debug("HEADER |  key : " + key + " , value : " + headers.get(key));
                        if (key.equals("Set-Cookie")) {
                            List<String> headerCookies = headers.get(key);
                            for (Iterator iterator = headerCookies.iterator(); iterator.hasNext();) {
                                String cookiesAsString = (String) iterator.next();
                                LOGGER.debug("cookiesAsString : " + cookiesAsString);
                                String[] cookiesArray = cookiesAsString.split(";");
                                LOGGER.debug("cookiesArray : " + cookiesArray.length);
                                for (int i = 0; i < cookiesArray.length; i++) {
                                    LOGGER.debug(cookiesArray[i]);
                                    cookies.add(cookiesArray[i]);
                                    /*
                                     * String [] cookieValue = cookiesArray[i].split("="); String
                                     * cookieName = cookieValue[0]; String cookieVal =
                                     * cookieValue[1]; LOGGER.debug(cookieName);
                                     * LOGGER.debug(cookieVal); NewCookie cookieObject = new
                                     * NewCookie(cookieName,cookieVal); LOGGER.debug("cookieObject :
                                     * "+cookieObject);
                                     */
                                }
                            }
                        }
                    }

                    /*
                     * // Nota : este solo funcionaba en la prueba unitaria LOGGER.debug("Response
                     * Cookies :"+response.getCookies()); List<NewCookie> responseCookies =
                     * response.getCookies() ; if (responseCookies!= null &&
                     * responseCookies.size()>0) { cookies.addAll(response.getCookies());
                     * LOGGER.debug("Cookies :"+cookies); }
                     * //LOGGER.debug("****************************************");
                     */
                    return response;
                }
            });

            WebResource resource = client.resource(replicaServiceUrl);
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("username", this.tokenServiceUser);
            params.add("password", this.tokenServicePwd);
            params.add("f", "json");
            String response = resource.post(String.class, params); //.queryParams(params).post( );
            LOGGER.debug("response:" + response);

            ObjectMapper mapper = new ObjectMapper();
            token = mapper.readValue(response, ArcgisServerTokenDTO.class);
            validateServerError(token);

            //LOGGER.debug("Cookies :"+cookies);
            token.setCookies(cookies);
            LOGGER.debug("Token : " + token);
        } catch (Exception e) {
            LOGGER.error("cookies : " + cookies);
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return token;
    }

    /**
     * Valida los mensajes de error del servidor (Errores de conexión)
     *
     * @param resultMap
     */
    public void validateServerError(ArcgisServerTokenDTO token) {
        if (token.getError() != null) {
            String message = token.getError().getMessage();
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, null, message);
        }
    }

}
