package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepUsuarioRolReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.util.FiltroDatosConsultaParametrizacionReporte;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class RepUsuarioRolReporteDAOBean extends
    GenericDAOWithJPA<RepUsuarioRolReporte, Long> implements
    IRepUsuarioRolReporteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RepUsuarioRolReporteDAOBean.class);

    // ----------------------------------------------------- //
    /**
     * @see IRepUsuarioRolReporteDAO#buscarListaRepUsuarioRolReportePorUsuarioYCategoria(UsuarioDTO,
     * String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<RepUsuarioRolReporte> buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
        UsuarioDTO usuario, String categoria) {

        LOGGER.debug(
            "Entra en RepUsuarioRolReporteDAOBean#buscarListaRepUsuarioRolReportePorUsuario");

        List<RepUsuarioRolReporte> answer = null;

        Query q = entityManager.createQuery("SELECT rurr FROM RepUsuarioRolReporte rurr " +
            " JOIN FETCH rurr.repReporte rr " +
            " WHERE rr.categoria = :categoria " +
            " AND (rurr.usuarioAsignado = :usuarioAsignado" +
            " OR (rurr.usuarioAsignado is null AND rurr.rolAsignado IN (:rolesAsignados))) ");

        q.setParameter("usuarioAsignado", usuario.getLogin());
        q.setParameter("rolesAsignados", usuario.getRolesCadena());
        q.setParameter("categoria", categoria);

        try {
            answer = (List<RepUsuarioRolReporte>) q.getResultList();

            if (answer != null) {
                for (RepUsuarioRolReporte urr : answer) {
                    Hibernate.initialize(urr.getRepTerritorialReportes());
                    if (urr.getRepTerritorialReportes() != null) {
                        for (RepTerritorialReporte rtr : urr.getRepTerritorialReportes()) {
                            Hibernate.initialize(rtr.getDepartamento());
                            Hibernate.initialize(rtr.getMunicipio());
                            Hibernate.initialize(rtr.getTerritorial());
                            Hibernate.initialize(rtr.getUOC());
                        }
                    }
                }
            }
        } catch (NoResultException e) {
            LOGGER.error(
                "RepUsuarioRolReporteDAOBean#buscarListaRepUsuarioRolReportePorUsuarioYCategoria:" +
                "Error al consultar la lista de RepUsuarioRolReporte del SNC");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ----------------------------------------------------- //
    /**
     * @see IRepUsuarioRolReporteDAO#buscarListaRepUsuarioRolReportePorUsuarioYCategoria(UsuarioDTO,
     * String)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<RepUsuarioRolReporte> buscarParametrizacionReporteUsuario(
        FiltroDatosConsultaParametrizacionReporte datosConsultaParametrizacion) {

        LOGGER.debug("Entra en RepUsuarioRolReporteDAOBean#buscarParametrizacionReporteUsuario");

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT rurr" +
            " FROM RepUsuarioRolReporte rurr" +
            " JOIN rurr.repTerritorialReportes rtr" +
            " WHERE 1=1");

        if (datosConsultaParametrizacion.getCategoria() != null &&
            !datosConsultaParametrizacion.getCategoria().isEmpty()) {
            query.append(" AND rurr.repReporte.categoria = :categoria");
        }
        if (datosConsultaParametrizacion.getTipoReporteId() != null &&
            !datosConsultaParametrizacion.getTipoReporteId().equals(0l)) {
            query.append(" AND rurr.repReporte.id = :tipoReporteId");
        }
        if (datosConsultaParametrizacion.getTerritorialCodigo() != null &&
            !datosConsultaParametrizacion.getTerritorialCodigo().isEmpty()) {
            query.append(" AND rtr.territorial.codigo = :territorialCodigo");
        }
        if (datosConsultaParametrizacion.getUOCCodigo() != null &&
            !datosConsultaParametrizacion.getUOCCodigo().isEmpty()) {
            query.append(" AND rtr.UOC.codigo = :UOCCodigo");
        }
        if (datosConsultaParametrizacion.getDepartamentoCodigo() != null &&
            !datosConsultaParametrizacion.getDepartamentoCodigo().isEmpty()) {
            query.append(" AND rtr.departamento.codigo = :departamentoCodigo");
        }
        if (datosConsultaParametrizacion.getMunicipioCodigo() != null &&
            !datosConsultaParametrizacion.getMunicipioCodigo()
                .isEmpty()) {
            query.append(" AND rtr.municipio.codigo = :municipioCodigo");
        }
        if (datosConsultaParametrizacion.getFuncionario() != null &&
            !datosConsultaParametrizacion.getFuncionario().isEmpty()) {
            query.append(" AND rurr.usuarioAsignado = :funcionario");
        }
        if (datosConsultaParametrizacion.getRol() != null &&
            !datosConsultaParametrizacion.getRol().isEmpty()) {
            query.append(" AND rurr.rolAsignado = :rol");
        }
        if (datosConsultaParametrizacion.getEstado() != null &&
            !datosConsultaParametrizacion.getEstado().isEmpty()) {
            query.append(" AND rurr.estado = :estado");
        }
        if (datosConsultaParametrizacion.getEntidadCreacion() != null &&
            !datosConsultaParametrizacion.getEntidadCreacion().isEmpty()) {
            query.append(" AND rurr.entidadCreacion = :entidadCreacion");
        }
        if (datosConsultaParametrizacion.getGenerar() != null &&
            !datosConsultaParametrizacion.getGenerar().isEmpty()) {
            query.append(" AND rurr.generar = :generar");
        }
        if (datosConsultaParametrizacion.getConsultar() != null &&
            !datosConsultaParametrizacion.getConsultar().isEmpty()) {
            query.append(" AND rurr.consultar = :consultar");
        }

        // Query
        Query q = this.entityManager.createQuery(query.toString());

        if (datosConsultaParametrizacion.getCategoria() != null &&
            !datosConsultaParametrizacion.getCategoria().isEmpty()) {
            q.setParameter("categoria", datosConsultaParametrizacion.getCategoria());
        }
        if (datosConsultaParametrizacion.getTipoReporteId() != null &&
            !datosConsultaParametrizacion.getTipoReporteId().equals(0l)) {
            q.setParameter("tipoReporteId", datosConsultaParametrizacion.getTipoReporteId());
        }
        if (datosConsultaParametrizacion.getTerritorialCodigo() != null &&
            !datosConsultaParametrizacion.getTerritorialCodigo().isEmpty()) {
            q.setParameter("territorialCodigo", datosConsultaParametrizacion.getTerritorialCodigo());
        }
        if (datosConsultaParametrizacion.getUOCCodigo() != null &&
            !datosConsultaParametrizacion.getUOCCodigo().isEmpty()) {
            q.setParameter("UOCCodigo", datosConsultaParametrizacion.getUOCCodigo());
        }
        if (datosConsultaParametrizacion.getDepartamentoCodigo() != null &&
            !datosConsultaParametrizacion.getDepartamentoCodigo().isEmpty()) {
            q.setParameter("departamentoCodigo", datosConsultaParametrizacion.
                getDepartamentoCodigo());
        }
        if (datosConsultaParametrizacion.getMunicipioCodigo() != null &&
            !datosConsultaParametrizacion.getMunicipioCodigo()
                .isEmpty()) {
            q.setParameter("municipioCodigo", datosConsultaParametrizacion.getMunicipioCodigo());
        }
        if (datosConsultaParametrizacion.getRol() != null &&
            !datosConsultaParametrizacion.getRol().isEmpty()) {
            q.setParameter("rol", datosConsultaParametrizacion.getRol());
        }
        if (datosConsultaParametrizacion.getFuncionario() != null &&
            !datosConsultaParametrizacion.getFuncionario().isEmpty()) {
            q.setParameter("funcionario", datosConsultaParametrizacion.getFuncionario());
        }
        if (datosConsultaParametrizacion.getEstado() != null &&
            !datosConsultaParametrizacion.getEstado().isEmpty()) {
            q.setParameter("estado", datosConsultaParametrizacion.getEstado());
        }
        if (datosConsultaParametrizacion.getEntidadCreacion() != null &&
            !datosConsultaParametrizacion.getEntidadCreacion().isEmpty()) {
            q.setParameter("entidadCreacion", datosConsultaParametrizacion.getEntidadCreacion());
        }
        if (datosConsultaParametrizacion.getGenerar() != null &&
            !datosConsultaParametrizacion.getGenerar().isEmpty()) {
            q.setParameter("generar", datosConsultaParametrizacion.getGenerar());
        }
        if (datosConsultaParametrizacion.getConsultar() != null &&
            !datosConsultaParametrizacion.getConsultar().isEmpty()) {
            q.setParameter("consultar", datosConsultaParametrizacion.getConsultar());
        }

        try {

            List<RepUsuarioRolReporte> answer = (ArrayList<RepUsuarioRolReporte>) q.getResultList();

            if (answer != null) {
                for (RepUsuarioRolReporte rep : answer) {
                    Hibernate.initialize(rep.getRepReporte());
                    Hibernate.initialize(rep.getRepTerritorialReportes());
                    if (rep.getRepTerritorialReportes() != null &&
                        !rep.getRepTerritorialReportes().isEmpty()) {
                        for (RepTerritorialReporte rtr : rep.getRepTerritorialReportes()) {
                            Hibernate.initialize(rtr.getDepartamento());
                            Hibernate.initialize(rtr.getMunicipio());
                            Hibernate.initialize(rtr.getTerritorial());
                            Hibernate.initialize(rtr.getUOC());
                        }
                    }
                }
            }
            return answer;
        } catch (NoResultException nrEx) {
            LOGGER.warn("RepReporteEjecucionDAOBean#buscarParametrizacionReporteUsuario: " +
                "La consulta de parametrización de reportes de usuario no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * @see IRepUsuarioRolReporteDAO#eliminarRepUsuarioRolReportePorId(Long)
     * @author david.cifuentes
     */
    @Override
    public boolean eliminarRepUsuarioRolReportePorId(Long idRepUsuarioRolReporte) {
        LOGGER.debug("RepUsuarioRolReporteDAOBean#eliminarRepUsuarioRolReportePorId " +
            "borrando registros ...");

        boolean answer = false;
        Query query;
        String queryString = "DELETE FROM RepUsuarioRolReporte rurr WHERE rurr.id = :id ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("id", idRepUsuarioRolReporte);
            query.executeUpdate();
            answer = true;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                getExcepcion(LOGGER, ex,
                    "RepUsuarioRolReporteDAOBean#eliminarRepUsuarioRolReportePorId",
                    "RepUsuarioRolReporte", "Id");
        }

        return answer;
    }

    // ----------------------------------------------------- //
    /**
     * @see IRepUsuarioRolReporteDAO#buscarParametrizacionReporteUsuarioCompletaPorId(Long)
     * @author david.cifuentes
     */
    @Override
    public RepUsuarioRolReporte buscarParametrizacionReporteUsuarioCompletaPorId(Long id) {

        LOGGER.debug(
            "Entra en RepUsuarioRolReporteDAOBean#buscarParametrizacionReporteUsuarioCompletaPorId");

        RepUsuarioRolReporte answer = null;

        Query q = entityManager.createQuery("SELECT rurr FROM RepUsuarioRolReporte rurr " +
            " JOIN FETCH rurr.repReporte rr " +
            " WHERE rurr.id = :id ");
        q.setParameter("id", id);

        try {
            answer = (RepUsuarioRolReporte) q.getSingleResult();

            if (answer != null) {
                Hibernate.initialize(answer.getRepTerritorialReportes());
                if (answer.getRepTerritorialReportes() != null) {
                    for (RepTerritorialReporte rtr : answer.getRepTerritorialReportes()) {
                        Hibernate.initialize(rtr.getDepartamento());
                        Hibernate.initialize(rtr.getMunicipio());
                        Hibernate.initialize(rtr.getTerritorial());
                        Hibernate.initialize(rtr.getUOC());
                    }
                }
            }
        } catch (NoResultException e) {
            LOGGER.error(
                "RepUsuarioRolReporteDAOBean#buscarParametrizacionReporteUsuarioCompletaPorId:" +
                "Error al consultar el RepUsuarioRolReporte del SNC");
        }
        return answer;
    }

    /**
     * @see IRepUsuarioRolReporteDAO#determinarNacional(String, Long)
     * @author felipe.cadena
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean determinarNacional(String usuario, Long reporteId) {
        Query query;
        String queryString;
        Long result = null;

        queryString = "select count(*) from" +
                " (select m.codigo cod from municipio m" +
                "   inner join jurisdiccion j on m.codigo = j.municipio_codigo" +
                "   inner join estructura_organizacional eo on j.estructura_organizacional_cod = eo.codigo" +
                "   inner join municipio_complemento mc on mc.municipio_codigo = m.codigo" +
                "   where " +
                "   tipo <> 'DELEGACION'" +
                "   and mc.con_informacion_gdb = 'SI') msnc" +
                " left join " +
                " (select tr.municipio_codigo cod_rep from rep_usuario_rol_reporte urr" +
                "   inner join rep_territorial_reporte tr on tr.USUARIO_ROL_REPORTE_ID = urr.id" +
                "   where urr.usuario_asignado = :usuario" +
                "   and urr.reporte_id = :reporteId) mreporte " +
                " on mreporte.cod_rep = msnc.cod" +
                " where cod_rep is null";

        try {
            query = this.entityManager.createNativeQuery(queryString, Long.class);
            query.setParameter("usuario", usuario);
            query.setParameter("reporteId", reporteId);

            result = (Long) query.getSingleResult();

            return result.equals(0L);

        } catch (Exception e) {
            LOGGER.error(
                    "RepUsuarioRolReporteDAOBean#determinarNacional:" +
                            "Error al consultar el RepUsuarioRolReporte del SNC");
            return false;

        }

    }
}
