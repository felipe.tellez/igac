/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.gestion.IComisionEstadoDAO;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class ComisionEstadoDAOBean extends GenericDAOWithJPA<ComisionEstado, Long>
    implements IComisionEstadoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionEstadoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IComisionEstadoDAO#getHistoricalRecord(java.lang.Long, int)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<ComisionEstado> getHistoricalRecord(Long idComision, int numRegistros) {

        LOGGER.debug("on ComisionEstadoDAOBean#getHistoricalRecord ");

        List<ComisionEstado> answer = null;
        String columnaOrdenamiento = "fecha";
        String queryString;
        Query query;

        queryString = "SELECT ce FROM ComisionEstado ce LEFT JOIN FETCH ce.memorandoDocumento" +
            " WHERE ce.comision.id = :comisionIdP " +
            " ORDER BY " + columnaOrdenamiento + " DESC";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("comisionIdP", idComision);

            //D: trae todos
            if (numRegistros < 0) {
                answer = this.findInRangeUsingQuery(query, 0, numRegistros);
                //answer = this.findAllOrderedByColumn2(columnaOrdenamiento, true);
            } else {
                answer = this.findInRangeUsingQuery(query, 0, 0);
                //answer = this.findAllOrderedByColumn2(columnaOrdenamiento, false, 0, numRegistros);
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionEstadoDAOBean#getHistoricalRecord");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IComisionEstadoDAO#obtenerUltimoDeComision(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ComisionEstado obtenerUltimoDeComision(Long idComision, String estadoComision) {

        ComisionEstado answer = null;
        boolean useEstado;
        String columnaOrdenamiento = "fecha";
        StringBuilder queryString;
        Query query;

        useEstado = false;
        queryString = new StringBuilder();
        queryString.append(
            "SELECT ce " +
            "FROM ComisionEstado ce " +
            "WHERE ce.comision.id = :comisionId_p ");

        if (estadoComision != null && !estadoComision.isEmpty()) {
            queryString.append(
                "AND ce.estado = :estadoComision_p");
            useEstado = true;
        }

        queryString.append(" ORDER BY ").append(columnaOrdenamiento).append(" DESC");

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("comisionId_p", idComision);

            if (useEstado) {
                query.setParameter("estadoComision_p", estadoComision);
            }
            query.setMaxResults(1);
            answer = (ComisionEstado) query.getSingleResult();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "ComisionEstadoDAOBean#obtenerUltimoDeComision");
        }

        return answer;
    }

//end of class    
}
