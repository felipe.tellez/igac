/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVPersonaBloqueadaHoyDAO;
import co.gov.igac.snc.persistence.entity.vistas.VPersonaBloqueadaHoy;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class VPersonaBloqueadaHoyDAOBean extends
    GenericDAOWithJPA<VPersonaBloqueadaHoy, Long> implements
    IVPersonaBloqueadaHoyDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPersonaBloqueadaHoyDAOBean.class);

    // end of class
}
