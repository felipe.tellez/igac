package co.gov.igac.snc.dao.formacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;

/**
 * Interfaz para los servicios de persistencia del objeto {@link DecretoAvaluoAnual}.
 *
 * @author david.cifuentes
 */
@Local
public interface IDecretoAvaluoAnualDAO extends
    IGenericJpaDAO<DecretoAvaluoAnual, Long> {

    /**
     * Método que retorna el {@link DecretoAvaluoAnual} registrado para el presente año.
     *
     * @author david.cifuentes
     * @return
     */
    public DecretoAvaluoAnual buscarDecretoActual();

    /**
     * Método que retorna el {@link DecretoAvaluoAnual} registrado para la vigencia del año enviada
     * como parametro.
     *
     * @author david.cifuentes
     *
     * @param {@link String} con el formato yyyy del año.
     * @return
     */
    public DecretoAvaluoAnual buscarDecretoPorVigencia(String anioVigencia);

}
