package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;

@Local
public interface IAvaluoCapituloDAO extends
    IGenericJpaDAO<AvaluoCapitulo, Long> {

    /**
     *
     * Metodo para obtener los capitulos de proyeccion de cotizacion asociados a un avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param idAvaluo - Id del avaluo
     * @return
     */
    public List<AvaluoCapitulo> obtenerCapitulosPorAvaluoId(Long idAvaluo);

}
