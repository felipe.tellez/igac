/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;

/**
 *
 * Interfaz remota para el bean de consulta predio
 *
 * @author juan.agudelo
 */
@Local
public interface IConsultaPPredio {

    /**
     * @author juan.agudelo
     * @param id
     * @return
     */
    public PPredio getPPredioFetchPPersonaPredioPorId(Long id);
}
