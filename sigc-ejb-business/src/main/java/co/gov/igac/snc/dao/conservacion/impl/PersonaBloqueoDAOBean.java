/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPersonaBloqueoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import co.gov.igac.snc.util.FuncionPredioPersonaBloqueoHoy;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class PersonaBloqueoDAOBean extends GenericDAOWithJPA<PersonaBloqueo, Long>
    implements IPersonaBloqueoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaBloqueoDAOBean.class);

    @EJB
    private IDocumentoDAO documentoService;

    /**
     * @modified juan.agudelo
     * @see IPersonaBloqueoDAO#findCurrentPersonaBloqueoByPersonaId(Long)
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public PersonaBloqueo findCurrentPersonaBloqueoByPersonaId(Long idPersona) {

        LOGGER.debug("on PersonaBloqueoDAOBean#findCurrentPersonaBloqueoByPersonaId ...");

        Query query;
        String queryToExecute;
        PersonaBloqueo answer = null;
        ArrayList<PersonaBloqueo> personaBloqueos = null;

        queryToExecute = "SELECT pb FROM PersonaBloqueo pb" +
            " JOIN FETCH pb.persona pbp" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " WHERE  pbp.id= :idPersona" +
            " AND FNC_PERSONA_BLOQUEADA (pbp.id, sysdate) = :si)";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("idPersona", idPersona);
        query.setParameter("si", ESiNo.SI.getCodigo());
        query.setMaxResults(1);

        try {
            personaBloqueos = (ArrayList<PersonaBloqueo>) query.getResultList();
            if (personaBloqueos != null && !personaBloqueos.isEmpty()) {
                answer = personaBloqueos.get(0);
            } else {
                throw new NoResultException();
            }
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de bloqueos de persona");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PersonaBloqueoDAOBean#findCurrentPersonaBloqueoByPersonaId");

        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IPersonaBloqueoDAO#buscarPersonaBloqueoByNumeroIdentificacion(String)
     */
    @Override
    public List<PersonaBloqueo> buscarPersonaBloqueoByNumeroIdentificacion(
        String numeroIdentificacion, String tipoIdentificacion) {
        LOGGER.debug("executing PersonaBloqueoDAOBean#buscarPersonaBloqueoByNumeroIdentificacion");

        String query;

        query = "SELECT pb FROM PersonaBloqueo pb" +
            " JOIN FETCH pb.persona pbp" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " JOIN FETCH pb.departamento" +
            " JOIN FETCH pb.municipio" +
            " WHERE  pbp.tipoIdentificacion= :tipoIdentificacion" +
            " AND pbp.numeroIdentificacion= :numeroIdentificacion" +
            " AND ((pb.fechaInicioBloqueo<= :fechaActual " +
            " AND pb.fechaTerminaBloqueo> :fechaActual" +
            " AND pb.fechaDesbloqueo IS null)" +
            " OR (pb.fechaInicioBloqueo<= :fechaActual " +
            " AND pb.fechaTerminaBloqueo IS null" +
            " AND pb.fechaDesbloqueo IS null)" +
            " OR (pb.fechaInicioBloqueo<= :fechaActual " +
            " AND pb.fechaDesbloqueo> :fechaActual))";

        Query q = entityManager.createQuery(query);
        q.setParameter("tipoIdentificacion", tipoIdentificacion);
        q.setParameter("numeroIdentificacion", numeroIdentificacion);
        q.setParameter("fechaActual", new Date(System.currentTimeMillis()));

        try {
            return q.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Error: " + e);
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPersonaBloqueoDAO#getPersonaBloqueosByPersonaId(Long)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<PersonaBloqueo> getPersonaBloqueosByPersonaId(Long idPersona) {
        LOGGER.debug("executing PersonasBloqueoDAOBean#getPersonaBloqueosByPersonaId");

        List<PersonaBloqueo> answer;
        StringBuilder query = new StringBuilder();

        query.append("SELECT pb FROM PersonaBloqueo pb" +
            " JOIN FETCH pb.persona pbp" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " LEFT JOIN FETCH pb.documentoSoporteDesbloqueo" +
            " WHERE  pbp.id= :idPersona");

        query.append(" AND ").append(FuncionPredioPersonaBloqueoHoy
            .getQueryprediopersonabloqueohoy());

        Query q = entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());
        q.setParameter("idPersona", idPersona);

        try {
            answer = q.getResultList();
        } catch (IndexOutOfBoundsException iobe) {
            return new ArrayList<PersonaBloqueo>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PersonasBloqueoDAOBean#getPersonaBloqueosByPersonaId");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IPersonasBloqueoDAO#buscarPersonasBloqueo( FiltroDatosConsultaPersonasBloqueo
     */
    @Override
    public List<PersonaBloqueo> buscarPersonasBloqueo(
        FiltroDatosConsultaPersonasBloqueo filtroDatosConsultaPersonasBloqueo) {
        LOGGER.debug("executing PersonasBloqueoDAOBean#buscarPersonasBloqueo");

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT pb FROM PersonaBloqueo pb" +
            " JOIN FETCH pb.persona pbp" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " WHERE  pbp.tipoIdentificacion= :tipoIdentificacion" +
            " AND pbp.numeroIdentificacion= :numeroIdentificacion");

        query.append(" AND ").append(
            FuncionPredioPersonaBloqueoHoy.getQueryprediopersonabloqueohoy());

        Query q = entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());
        q.setParameter("tipoIdentificacion",
            filtroDatosConsultaPersonasBloqueo.getTipoIdentificacion());
        q.setParameter("numeroIdentificacion",
            filtroDatosConsultaPersonasBloqueo.getNumeroIdentificacion());

        try {
            return q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PersonaBloqueoDAOBean#buscarPersonasBloqueo");

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IPersonasBloqueoDAO#actualizarPersonasBloqueo(UsuarioDTO, List)
     */
    @Implement
    @Override
    public List<PersonaBloqueo> actualizarPersonasBloqueo(UsuarioDTO usuario,
        List<PersonaBloqueo> personasBloqueadas) {

        List<PersonaBloqueo> pBloqueadas = null;
        Map<String, String> rutasAlfrescoDocumentosSoporte = null;
        List<Persona> personas;
        Documento dr;

        if (personasBloqueadas != null && !personasBloqueadas.isEmpty()) {
            try {
                pBloqueadas = new ArrayList<PersonaBloqueo>();
                personas = new ArrayList<Persona>();

                for (PersonaBloqueo p : personasBloqueadas) {
                    personas.add(p.getPersona());
                }

                if (personasBloqueadas.get(0).getDocumentoSoporteBloqueo() != null) {
                    rutasAlfrescoDocumentosSoporte = this.documentoService
                        .almacenarDocumentosBloqueoGestorDocumental(usuario,
                            personasBloqueadas.get(0)
                                .getDocumentoSoporteBloqueo(),
                            personas, null);
                }

                for (PersonaBloqueo personaB : personasBloqueadas) {

                    if (personaB.getPersona() != null &&
                        personaB.getPersona().getNumeroIdentificacion() != null) {

                        personaB.setUsuarioLog(usuario.getLogin());
                        personaB.setFechaLog(new Date(System.currentTimeMillis()));

                        if (rutasAlfrescoDocumentosSoporte != null &&
                            !rutasAlfrescoDocumentosSoporte.isEmpty()) {

                            for (String url : rutasAlfrescoDocumentosSoporte
                                .keySet()) {
                                if (url.equals(personaB.getPersona()
                                    .getNumeroIdentificacion())) {
                                    personaB.getDocumentoSoporteBloqueo()
                                        .setIdRepositorioDocumentos(
                                            rutasAlfrescoDocumentosSoporte
                                                .get(url));
                                }
                            }
                        }

                        personaB.getDocumentoSoporteBloqueo().setUsuarioLog(
                            usuario.getLogin());
                        personaB.getDocumentoSoporteBloqueo().setUsuarioCreador(
                            usuario.getLogin());
                        dr = this.documentoService.update(personaB
                            .getDocumentoSoporteBloqueo());
                        personaB.setDocumentoSoporteBloqueo(dr);

                        update(personaB);
                        pBloqueadas.add(personaB);
                    }
                }
            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010
                    .getExcepcion(LOGGER, e, e.getMessage());
            }
        }
        return pBloqueadas;
    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IPersonaBloqueoDao#existeAsociacionEntidadBloqueo(Entidad)
     * @author david.cifuentes
     */
    @Override
    public boolean existeAsociacionEntidadBloqueo(Entidad entidad) {

        LOGGER.debug("Executing PersonaBloqueoDAOBean#existeAsociacionEntidadBloqueo");

        Query query;
        boolean answer = false;

        try {
            query = this.entityManager.createQuery(
                "SELECT count(pb.id) FROM PersonaBloqueo pb WHERE pb.entidadId = :idEntidad");

            query.setParameter("idEntidad", entidad.getId());

            Long conteo = (Long) query.getSingleResult();
            if (conteo > 0) {
                answer = true;
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("PersonaBloqueoDAOBean#existeAsociacionEntidadBloqueo: " +
                Constantes.NO_HAY_RESULTADOS_ENTIDADES + nrEx);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "PersonaBloqueoDAOBean#existeAsociacionEntidadBloqueo");
        }

        return answer;
    }

}
