package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.tramite.Producto;

/**
 * Servicios de persistencia para el objeto Producto
 *
 * @author javier.barajas
 */
@Local
public interface IProductoDAO extends IGenericJpaDAO<Producto, Long> {

    /**
     * Método que devuelve la lista de productos generados por el id del grupo del producto
     *
     * @cu: CU-TV-PR-0001,CU-TV-PR-0002,CU-TV-PR-0003,CU-TV-PR-0004
     * @param idGrupo
     * @return List<Producto>
     * @version:1.0
     * @author javier.aponte
     */
    public List<Producto> obtenerListaProductosPorGrupoId(Long idGrupo);

    /**
     * Método encargado de obtener un producto por código
     *
     * @param codigo código del producto
     * @author javier.aponte
     * @return
     */
    public Producto obtenerProductoPorCodigo(String codigo);
}
