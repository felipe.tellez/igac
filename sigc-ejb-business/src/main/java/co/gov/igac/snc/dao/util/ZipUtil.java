package co.gov.igac.snc.dao.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * Utilitario para generación de zips (código extraído de TramiteMovilesDAOBean ) *
 */
public class ZipUtil {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ZipUtil.class);

    /**
     * Crea un archivo zip de una carpeta completa
     *
     * @param srcFolder
     * @param destZipFile
     * @throws Exception
     */
    public static void zipFolder(String srcFolder, String destZipFile) throws Exception {
        ZipOutputStream zip = null;
        FileOutputStream fileWriter = null;

        fileWriter = new FileOutputStream(destZipFile);
        zip = new ZipOutputStream(fileWriter);

        addFolderToZip("", srcFolder, zip);
        zip.flush();
        zip.close();
    }

    /**
     * Comprime una lista de archivos en un zip. Reemplaza los nombres de archivos con caracteres
     * con acentos Ejm: áéíóú.txt queda convertido en aeiou.txt Esto fué necesario debido al
     * problema de consistencia de nombres para los zip en java 6.
     *
     * @param listadoRutasArchivos Lista con las rutas absolutas de los archivos a comprimir.
     *
     * @return ruta del archivo zip generado
     * @throws Exception
     */
    public static String zipFiles(List<String> listadoRutasArchivos) {
        LOGGER.debug("zipFiles");
        String zipPath = null;
        try {
            File tempDir = FileUtils.getTempDirectory();
            File destDir = new File(tempDir, UUID.randomUUID().toString());
            destDir.mkdir();
            for (String ruta : listadoRutasArchivos) {
                File srcFile = new File(ruta);
                String flattenFilename = StringUtil.flattenToAscii(srcFile.getName());
                LOGGER.debug("originalFilename:" + srcFile.getName());
                LOGGER.debug("flattenFilename:" + flattenFilename);
                File destFile = new File(destDir, flattenFilename);
                FileUtils.copyFile(srcFile, destFile);
                LOGGER.debug("destFile:" + destFile.getAbsolutePath());
                //FileUtils.copyFileToDirectory(srcFile, destDir);
            }
            File destZip = new File(tempDir, UUID.randomUUID().toString() + ".zip");
            ZipUtil.zipFolder(destDir.getAbsolutePath(), destZip.getAbsolutePath());
            zipPath = destZip.getAbsolutePath();
            destDir.delete();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100019.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return zipPath;
    }

    /**
     * Funcionalidad para la funcion zipFolder
     *
     * @param path
     * @param srcFolder
     * @param zip
     * @throws Exception
     */
    private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws
        Exception {
        File folder = new File(srcFolder);

        for (String filename : folder.list()) {
            if (path.equals("")) {
                addFileToZip(folder.getName(), srcFolder + "/" + filename, zip);
            } else {
                addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + filename, zip);
            }
        }
    }

    /**
     * Funcionalidad para la funcion zipFolder
     *
     * @param path
     * @param srcFile
     * @param zip
     * @throws Exception
     */
    private static void addFileToZip(String path, String srcFile, ZipOutputStream zip) throws
        Exception {

        File folder = new File(srcFile);
        if (folder.isDirectory()) {
            addFolderToZip(path, srcFile, zip);
        } else {
            byte[] buf = new byte[1024];
            int len;
            FileInputStream in = new FileInputStream(srcFile);
            zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
            while ((len = in.read(buf)) > 0) {
                zip.write(buf, 0, len);
            }
        }
    }

}
