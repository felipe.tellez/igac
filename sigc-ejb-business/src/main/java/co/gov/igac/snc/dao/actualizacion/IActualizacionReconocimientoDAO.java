package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionReconocimiento;

/**
 * Servicios de persistencia para el objeto ActualizacionReconocimiento
 *
 * @author javier.barajas
 */
@Local
public interface IActualizacionReconocimientoDAO extends
    IGenericJpaDAO<ActualizacionReconocimiento, Long> {

    /**
     * Método que trae los Actualizacion Reconocimiento de una actualizacion
     *
     * @param actualizacionId
     * @return
     * @author javier.barajas
     */
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoByActualizacionId(
        Long actualizacionId);

    /**
     * Método que trae los Actualizacion Reconocimiento de una actualizacion con parametros de
     * paginacion
     *
     * @param actualizacionId
     * @return
     * @author javier.barajas
     */
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoByActualizacionId(
        Long actualizacionId, int... rowStartIdxAndCount);

    /**
     * Método que trae los Actualizacion Reconocimiento por el coordinadorId
     *
     * @param recursoHumanoId
     * @cu 220
     * @return List<ActualizacionReconocimiento>
     * @version 1.0
     * @author javier.barajas
     */
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoporRecursoHumanoId(
        Long recursoHumanoId);

    /**
     * Obtiene la actualizacion por reconocimiento humano id
     *
     * @param recursoHumanoId identificador del recurso humano en el modelo
     * @return
     * @author andres.eslava
     */
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoByRecursoHumanoId(
        Long recursoHumanoId);

}
