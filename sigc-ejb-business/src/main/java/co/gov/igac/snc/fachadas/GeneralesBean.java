package co.gov.igac.snc.fachadas;

import java.io.File;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import co.gov.igac.snc.dao.conservacion.*;
import co.gov.igac.snc.persistence.util.*;
import co.gov.igac.snc.util.*;
import org.apache.commons.io.FileUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.DocumentoMemorandoDTO;
import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.CirculoRegistralMunicipio;
import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.EReportesRadicacionesDinamicosSNC;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.business.conservacion.IAplicarCambios;
import co.gov.igac.snc.business.conservacion.IConsultaPredio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDocumentoDAO;
import co.gov.igac.snc.dao.conservacion.IColindanteDAO;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioInconsistenciaDAO;
import co.gov.igac.snc.dao.conservacion.IRepReporteDAO;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionDAO;
import co.gov.igac.snc.dao.conservacion.IRepTerritorialReporteDAO;
import co.gov.igac.snc.dao.conservacion.IUsoConstruccionDAO;
import co.gov.igac.snc.dao.generales.IBarrioDAO;
import co.gov.igac.snc.dao.generales.ICirculoRegistralDAO;
import co.gov.igac.snc.dao.generales.ICirculoRegistralMunicipioDAO;
import co.gov.igac.snc.dao.generales.ICodigoHomologadoDAO;
import co.gov.igac.snc.dao.generales.IComponenteDAO;
import co.gov.igac.snc.dao.generales.IComunaDAO;
import co.gov.igac.snc.dao.generales.IConexionUsuarioDAO;
import co.gov.igac.snc.dao.generales.IConfiguracionReporteDAO;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.dao.generales.IEstructuraOrganizacionalDAO;
import co.gov.igac.snc.dao.generales.IFirmaUsuarioDAO;
import co.gov.igac.snc.dao.generales.IGrupoDAO;
import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.snc.dao.generales.ILogAccesoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IManzanaVeredaDAO;
import co.gov.igac.snc.dao.generales.IMunicipioComplementoDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.IPaisDAO;
import co.gov.igac.snc.dao.generales.IParametroDAO;
import co.gov.igac.snc.dao.generales.IPlantillaDAO;
import co.gov.igac.snc.dao.generales.IPlantillaSeccionReporteDAO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.generales.IProfesionDAO;
import co.gov.igac.snc.dao.generales.ISectorDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.generales.IVPermisoDAO;
import co.gov.igac.snc.dao.generales.IZonaDAO;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.sig.vo.ProductoCatastralResultado;
import co.gov.igac.snc.dao.tramite.IGrupoProductoDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDetalleDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralErrorDAO;
import co.gov.igac.snc.dao.tramite.IProductoDAO;
import co.gov.igac.snc.dao.tramite.IProductoFormatoDAO;
import co.gov.igac.snc.dao.tramite.ISolicitudDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteEstadoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteInconsistenciaDAO;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.dao.util.ZipUtil;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.jms.IIntegracionLocal;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Componente;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.persistence.entity.generales.ConfiguracionReporte;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Grupo;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;
import co.gov.igac.snc.persistence.entity.tramite.Producto;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralError;
import co.gov.igac.snc.persistence.entity.tramite.ProductoFormato;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioInconsistenciaTipo;
import co.gov.igac.snc.persistence.util.EProcesosAsincronicos;
import co.gov.igac.snc.persistence.util.ERepReporteEjecucionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESistema;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.DocumentoResultadoPPRedios;
import co.gov.igac.snc.util.EEstadoSesion;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.util.ELogAccionEvento;
import co.gov.igac.snc.util.ErrorUtil;
import co.gov.igac.snc.util.Mailer;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;
import co.gov.igac.snc.vo.ImagenPredioVO;
import co.gov.igac.snc.vo.ParametroEditorVO;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import org.apache.commons.io.FilenameUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;


@Stateless
//@Interceptors(TimeInterceptor.class)
public class GeneralesBean implements IGenerales, IGeneralesLocal {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralesBean.class);

    /**
     * interfaces de servicio
     */
    @EJB
    private IConservacionLocal conservacionService;

    @EJB
    private IIntegracionLocal integracionService;

    @EJB
    private ITramiteLocal tramiteService;

    /**
     * Interfaces de Acceso a Datos
     */
    @EJB
    private IDominioDAO dominioDAO;

    @EJB
    private IPaisDAO paisDAO;

    @EJB
    private IConsultaPredio consultaPredioService;

    @EJB
    private ITransaccionalLocal transaccionalService;

    @EJB
    private IComponenteDAO componenteDAO;

    @EJB
    private IRepTerritorialReporteDAO repTerritorialDao;

    @EJB
    private IParametroDAO parametroDAO;

    @EJB
    private IDepartamentoDAO departamentoDAO;

    @EJB
    private IMunicipioDAO municipioDAO;

    @EJB
    private IEstructuraOrganizacionalDAO estructuraOrganizacionalDAOService;

    @EJB
    private ITipoDocumentoDAO tipoDocumentoDAO;

    @EJB
    private IDocumentoDAO documentoDAO;

    @EJB
    private ISolicitudDocumentoDAO solicitudDocumentoDAO;

    @EJB
    private IVPermisoDAO permisosService;

    @EJB
    private ICirculoRegistralDAO circulosRegistralesService;

    @EJB
    private ICirculoRegistralMunicipioDAO circuloRegistralMunicipioService;

    @EJB
    private IJurisdiccionDAO jurisdicionService;

    @EJB
    private ICodigoHomologadoDAO codigoHomologadoDAO;

    @EJB
    private IPPredioDAO pPredioService;

    @EJB
    private IPredioDAO predioDAO;

    @EJB
    private ITramiteEstadoDAO tramiteEstadoService;

    @EJB
    private IPlantillaDAO plantillaDAOService;

    @EJB
    private ISNCProcedimientoDAO procedimientoService;

    @EJB
    private IZonaDAO zonaDAO;

    @EJB
    private ISectorDAO sectorDAO;

    @EJB
    private IComunaDAO comunaDAO;

    @EJB
    private IBarrioDAO barrioDAO;

    @EJB
    private IManzanaVeredaDAO manzanaVeredaDAO;

    @EJB
    private IActualizacionDocumentoDAO actualizacionDocumentoDAO;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private IColindanteDAO colindanteDAO;

    @EJB
    private IUsoConstruccionDAO usoConstruccionDAO;

    @EJB
    private IProfesionDAO profesionDAO;

    @EJB
    private IConfiguracionReporteDAO configuracionReporteDAO;

    @EJB
    private ITramiteDAO tramiteDAO;

    @EJB
    private ITramiteInconsistenciaDAO tramiteInconsistenciaDAO;

    @EJB
    private IPredioInconsistenciaDAO predioInconsistenciaDAO;

    @EJB
    private IProductoCatastralJobDAO productoCatastralJobDAO;

    @EJB
    private ILogAccesoDAO logAccesoDAO;

    @EJB
    private IConexionUsuarioDAO conexionUsuarioDAO;

    @EJB
    private IGrupoProductoDAO grupoProductoDAO;

    @EJB
    private IProductoDAO productoDAO;

    @EJB
    private IProductoCatastralDAO productoCatastralDAO;

    @EJB
    private IProductoCatastralErrorDAO productoCatastralErrorDAO;

    @EJB
    private IProductoCatastralDetalleDAO productoCatastralDetalleDAO;

    @EJB
    private IMunicipioComplementoDAO municipioComplementoDAO;

    @EJB
    private ITramiteDocumentoDAO tramiteDocumentoDao;

    @EJB
    private IFichaMatrizPredioDAO fichaMatrizPredioDAO;

    @EJB
    private IFichaMatrizDAO fichaMatrizDAO;

    @EJB
    private IProductoFormatoDAO productoFormatoDAO;

    @EJB
    private IAplicarCambios aplicarCambios;

    @EJB
    private IRepReporteDAO repReporteDAO;

    @EJB
    private IRepReporteEjecucionDAO repReporteEjecucionDAO;

    @EJB
    private IFirmaUsuarioDAO firmaUsuarioDAO;

    @EJB
    private IPlantillaSeccionReporteDAO plantillaSeccionReporteDAO;

    @EJB
    private IActualizacionLocal actualizacionService;

    @EJB
    private IGrupoDAO grupoDAO;

    @EJB
    private IRepUsuarioRolReporteDAO repUsuarioRolReporteDAO;

    /**
     * @see IGenerales#getCacheDominioPorNombre(EDominio)
     */
    @Override
    public List<Dominio> getCacheDominioPorNombre(Enum d) {
        return this.dominioDAO.findByNombre(d);
    }

    /**
     * @see IGenerales#getCachePaises()
     */
    @Override
    public List<Pais> getCachePaises() {
        return this.paisDAO.findAll();
    }

    /**
     * @see IGenerales#getCacheParametroPorNombre(String)
     */
    @Override
    public Parametro getCacheParametroPorNombre(String nombre) {
        return parametroDAO.getParametroPorNombre(nombre);
    }

    /**
     * @see IGenerales#getCacheDepartamentosPorPais(String)
     */
    @Override
    public List<Departamento> getCacheDepartamentosPorPais(String codigoPais) {
        return this.departamentoDAO.findByPais(codigoPais);
    }

    /**
     * @see IGenerales#getCacheMunicipiosPorDepartamento(String)
     */
    @Override
    public List<Municipio> getCacheMunicipiosPorDepartamento(
        String codigoDepartamento) {
        return this.municipioDAO.findByDepartamento(codigoDepartamento);
    }

    /**
     * @see IGenerales#getCacheEstructuraOrganizacionalPorTipoYPadre(EEstructuraOrganizacional,
     * String)
     */
    @Override
    public List<EstructuraOrganizacional> getCacheEstructuraOrganizacionalPorTipoYPadre(
        EEstructuraOrganizacional tipo, String codigoPadre) {
        return this.estructuraOrganizacionalDAOService.getDependencias(tipo,
            codigoPadre);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#getCacheTerritoriales()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<EstructuraOrganizacional> getCacheTerritoriales() {

        List<EstructuraOrganizacional> answer = null;

        try {
            answer = this.consultaPredioService.getTerritorialesList();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#getCacheTerritoriales: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#getCacheDepartamentosPorTerritorial(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Departamento> getCacheDepartamentosPorTerritorial(String territorialCode) {

        List<Departamento> answer = null;

        try {
            answer = this.consultaPredioService.getDepartamentosList(territorialCode);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#getCacheDepartamentosPorTerritorial: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#getCacheMunicipiosByDepartamento(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Municipio> getCacheMunicipiosByDepartamento(String departamentoCode) {

        List<Municipio> answer = new LinkedList<Municipio>();

        try {
            answer = this.consultaPredioService.getMunicipiosList(departamentoCode);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#getCacheMunicipiosByDepartamento: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#getCacheDepartamentoByCodigo(String)
     */
    @Override
    public Departamento getCacheDepartamentoByCodigo(String codigo) {
        return departamentoDAO.getDepartamentoByCodigo(codigo);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#getCirculoRegistralByCodigo(String)
     */
    @Override
    public CirculoRegistral getCirculoRegistralByCodigo(String codigo) {
        return circulosRegistralesService.findByCodigo(codigo);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#getCacheMunicipioByCodigo(String)
     */
    @Override
    public Municipio getCacheMunicipioByCodigo(String codigo) {
        return municipioDAO.getMunicipioByCodigo(codigo);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#getCacheDepartamentos()
     */
    @Override
    public List<Departamento> getCacheDepartamentos() {
        return this.departamentoDAO.findAll();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el listado de tipos de documentos que son requeridos para un tipo de tramite, clase
     * de mutacion y subtipo
     *
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @return
     */
    @Override
    public List<TipoDocumento> getCacheTiposDocumentoPorTipoTramite(
        String tipoTramite, String claseMutacion, String subtipo) {
        return this.tipoDocumentoDAO.findByTipoTramite(tipoTramite,
            claseMutacion, subtipo);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#solicitarTipoDocumentoPorClase(String)
     */
    @Override
    public TipoDocumento getCacheTipoDocumentoPorClase(String clase) {
        return this.tipoDocumentoDAO.getTipoDocumentoPorClase(clase);
    }

    /**
     * Ejecuta el SP para generacion de numeraciones y devuelve en dos formatos OJO: los parámetros
     * que no se usen para generar la numeración deben ir como cadenas vacías o como 0s (ej: para
     * generar una basada solo en la territorial se pasan ENumeraciones, idTerritorial, "", "", 0
     *
     * @param numeracion
     * @param estructuraOrganizacional
     * @param departamento
     * @param municipio
     * @param tipoDocumento
     * @author fredy.wilches
     * @return arreglo de objetos. [0] el resultado de la numeración sin '-'. [1] el mismo pero con
     * '-' separando los campos dados para generar el número (ej: secuencia con territorial y id
     * 1100-0000004-2011)
     */
    @Override
    public Object[] generarNumeracion(ENumeraciones numeracion,
        String estructuraOrganizacional, String departamento,
        String municipio, Integer tipoDocumento) {

        Object[] answer;
        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(numeracion.getId()));
        parametros.add(estructuraOrganizacional);
        parametros.add(departamento);
        parametros.add(municipio);

        if (tipoDocumento != null) {
            parametros.add(new BigDecimal(tipoDocumento));
        }

        try {
            answer = this.procedimientoService.generarNumeracion(parametros);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Ejecuta el SP provisional que permite radicar en correspondencia cualquier documento
     *
     * @param parametros
     * @author fredy.wilches
     * @return
     * @modified pedro.garcia para seguir el estándar de llamar procedimientos desde una clase con
     * métodos centralizados
     */
    @Override
    public Object[] radicarEnCorrespondencia(List<Object> parametros) {
        //NO MODIFICAR ESTE MÉTODO POR INTEGRACIONES
        Object[] answer;
        try {
            answer = this.procedimientoService.radicarEnCorrespondencia(parametros);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @modified pedro.garcia para seguir el estándar de llamar procedimientos desde una clase con
     * métodos centralizados
     * @modified felipe.cadena -- se agrega el parametro usuario
     */
    @Override
    public Object[] consultarRadicadoCorrespondencia(String numeroRadicacionCorrespondencia,
        UsuarioDTO usuario) {

        Object[] answer;
        List<Object> parametros = new ArrayList<Object>();

        parametros.add(numeroRadicacionCorrespondencia);
        parametros.add(usuario.getLogin());
        try {
            answer = this.procedimientoService.consultarRadicadoCorrespondencia(parametros);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * Retorna las opciones del arbol del menú
     *
     * @param roles
     * @return
     */
    @Override
    public List<VPermisoGrupo> getCacheMenu(String roles[]) {
        return this.permisosService.getMenu(roles);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#getCacheCirculosRegistralesPorMunicipio(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<CirculoRegistral> getCacheCirculosRegistralesPorMunicipio(String idMunicipio) {

        List<CirculoRegistral> answer = null;
        try {
            answer = this.circulosRegistralesService.findByMunicipio(idMunicipio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#getCacheCirculosRegistralesPorMunicipio: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#getCacheCirculosRegistralesPorDepartamento(java.lang.String)
     */
//TODO :: fredy.wilches :: documentar según estándar. Hacer manejo de excepciones :: pedro.garcia
    @Override
    public List<CirculoRegistral> getCacheCirculosRegistralesPorDepartamento(String idDepartamento) {

        List<CirculoRegistral> answer = null;
        answer = this.circulosRegistralesService.findByDepartamento(idDepartamento);
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Obtiene un código homologado
     *
     * @author fredy.wilches
     * @param codigo
     * @param origen
     * @param destino
     * @param valor
     * @param fecha
     * @return
     */
//TODO :: fredy.wilches :: documentar según estándar. :: pedro.garcia
    @Override
    public String getCacheCodigoHomologado(ECodigoHomologado codigo, ESistema origen,
        ESistema destino, String valor, Date fecha) {
        return codigoHomologadoDAO.obtenerCodigoHomologado(codigo, origen, destino, valor, fecha);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#averiguarSiMunicipioEsSede(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean averiguarSiMunicipioEsSede(String codigoMunicipio, String idTerritorial) {

        boolean answer = false;

        answer = this.estructuraOrganizacionalDAOService.
            findOutIfIsSede(codigoMunicipio, idTerritorial);
        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /*
     *
     */

    @Override
    public UsuarioDTO getCacheUsuario(String login) {
        UsuarioDTO user = LdapDAO.searchUser(login);
        LOGGER.info(user.getDescripcionTerritorial() == null ? 
            "user.getDescripcionTerritorial() ==> NULL":user.getDescripcionTerritorial());
        if (user.getDescripcionTerritorial() != null) {
            user.setCodigoTerritorial(
                getCacheCodigoHomologado(ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
                    ESistema.LDAP, ESistema.SNC, user.getDescripcionTerritorial(), null));
        }
        if (user.getDescripcionUOC() != null) {
            user.setCodigoUOC(
                getCacheCodigoHomologado(ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
                    ESistema.LDAP, ESistema.SNC, user.getDescripcionUOC(), null));
        }
        return user;
    }

    /**
     * @see IGenerales#obtenerMunicipiosDelegados()
     * @author felipe.cadena
     */
    @Override
    public List<Municipio> obtenerMunicipiosDelegados() {
        List<Municipio> result = new ArrayList<Municipio>();

        try {
            result = this.municipioDAO.obtenerDelegados();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#obtenerMunicipiosDelegados: ",
                ex.getMensaje());
        }
        return result;
    }
    
    @Override
    public List<Municipio> obtenerMunicipiosHabilitados(){
        List<Municipio> result = new ArrayList<Municipio>();

        try {
            result = this.municipioDAO.obtenerDelegados();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#obtenerMunicipiosDelegados: ",
                ex.getMensaje());
        }
        return result;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#buscarTiposDeDocumentos(String claseDoc, String generado)
     * @author david.cifuentes
     */
    @Override
    public List<TipoDocumento> buscarTiposDeDocumentos(String claseDoc, String generado) {
        return this.tipoDocumentoDAO.findTipoDeDocumentosAll(claseDoc, generado);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo usado para enviar correos electrónicos desde la capa web
     *
     * @author fredy.wilches
     * @documented pedro.garcia
     *
     * @param destinatario
     * @param objeto tema u objeto del correo
     * @param mensaje cuerpo del correo
     * @param adjuntos url de los archivos que se van a adjuntar
     * @param titulos nombres de los archivos adjuntos
     */
    /*
     * @modified pedro.garcia 03-10-2012 Manejo de excepciones
     */
    @Override
    public boolean enviarCorreo(String destinatario, String objeto, String mensaje,
        String[] adjuntos,
        String[] titulos) {

        try {
            Mailer.getInstance().sendEmail(destinatario, objeto, mensaje, adjuntos, titulos);
            return true;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#enviarCorreo: " + ex.getMensaje());
            return false;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author david.cifuentes Método que inserta un nuevo {@link Documento}
     * @see IGenerales#guardarDocumento(Documento documentoSoporte)
     */
    @Override
    public Documento guardarDocumento(Documento documentoSoporte) {

        Documento answer = null;
        try {
            answer = this.documentoDAO.update(documentoSoporte);
        } catch (Exception ex) {
            LOGGER.error("Error en GeneralesBean#guardarDocumento : " +
                ex.getMessage());
        }

        if (answer != null) {
            answer.setTipoDocumento(documentoSoporte.getTipoDocumento());
        }
        return answer;
    }

    /**
     * @see IGenerales#actualizarDocumentoYGestorDocumental(UsuarioDTO, DocumentoTramiteDTO,
     * Documento)
     * @author juan.agudelo
     */
    @Override
    public boolean actualizarDocumentoYAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) throws ExcepcionSNC {
        return this.documentoDAO.actualizarDocumentoYGestorDocumental(usuario, documentoTramiteDTO,
            documento);
    }

    /**
     * Método que busca un {@link Documento} por su id
     *
     * @see IGenerales#buscarDocumentoPorId(Long documentoId)
     * @author david.cifuentes
     */
    @Override
    public Documento buscarDocumentoPorId(Long documentoId) {
        return this.documentoDAO.findById(documentoId);
    }

    /**
     * @see IGeneralesLocal#buscarDocumentoPorIdConTipoDocumento(Long)
     * @author christian.rodriguez
     */
    @Override
    public Documento buscarDocumentoPorIdConTipoDocumento(Long documentoId) {

        Documento documentoEncontrado = this.documentoDAO.findById(documentoId);
        Hibernate.initialize(documentoEncontrado.getTipoDocumento());
        return documentoEncontrado;
    }

    /**
     * Método que busca un {@link TipoDocumento} por su id
     *
     * @see IGenerales#buscarDocumentoPorId(Long documentoId)
     * @author david.cifuentes
     */
    @Override
    public TipoDocumento buscarTipoDocumentoPorId(Long tipoDocumentoId) {
        return this.tipoDocumentoDAO.findById(tipoDocumentoId);
    }

    /**
     * Método que busca un {@link Documento} guardado en Alfresco por su id en Alfresco
     *
     * @see IGenerales#buscarDocumentoAlfresco(String idRepositorioDocumentos)
     * @author david.cifuentes
     */
//	@Override
//	public DocumentoNombreMimeDTO buscarDocumentoAlfresco( String idRepositorioDocumentos ){
//		return this.documentoDAO.buscarDocumentoAlfresco(idRepositorioDocumentos);
//	}
    /**
     * @see IGenerales#actualizarDocumentoYSubirArchivoAlfresco(UsuarioDTO, DocumentoTramiteDTO,
     * Documento)
     * @author david.cifuentes
     */
    @SuppressWarnings("deprecation")
    @Override
    public Documento actualizarDocumentoYSubirArchivoAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) {
        return this.documentoDAO.actualizarDocumentoYSubirArchivoAlfresco(usuario,
            documentoTramiteDTO, documento);
    }

    /**
     * @see IGenerales#subirArchivoAlfresco(UsuarioDTO, DocumentoTramiteDTO, Documento)
     * @author david.cifuentes
     */
    @SuppressWarnings("deprecation")
    @Override
    public void subirArchivoAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) {
        this.documentoDAO.actualizarDocumentoYSubirArchivoAlfresco(usuario, documentoTramiteDTO,
            documento);
    }

    /**
     * @author david.cifuentes
     * @see IGenerales#buscarResolucionesByNumeroPredialPredio(String)
     */
    @Override
    public List<Documento> buscarResolucionesByNumeroPredialPredio(
        String numeroPredial) {
        return this.documentoDAO.buscarResolucionesByNumeroPredialPredio(numeroPredial);
    }

    /**
     * @see IGenerales#getCacheTerritorialesConDireccionGeneral()
     * @author fabio.navarrete
     */
    @Override
    public List<EstructuraOrganizacional> getCacheTerritorialesConDireccionGeneral() {
        return this.estructuraOrganizacionalDAOService.findTerritorialesAndDireccionGeneral();
    }

    /**
     * @see IGenerales#getCacheSubdireccionesYOficinas(String)
     * @author fabio.navarrete
     */
    @Override
    public List<EstructuraOrganizacional> getCacheSubdireccionesYOficinas(
        String codigoPadre) {
        return this.estructuraOrganizacionalDAOService.findSubdireccionesOficinas(codigoPadre);
    }

    /**
     * @see IGenerales#getCacheGruposTrabajo(String)
     * @author fabio.navarrete
     */
    @Override
    public List<EstructuraOrganizacional> getCacheGruposTrabajo(
        String estructuraOrganizacionalCodigo) {
        return this.estructuraOrganizacionalDAOService
            .findGruposTrabajo(estructuraOrganizacionalCodigo);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#buscarDocumentoResultadoPorTramiteId
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 16-08-2012 Manejo correcto de excepciones
     */
    @Override
    public DocumentoResultadoPPRedios buscarDocumentoResultadoPorTramiteId(Long tramiteId) {

        String documentoResultado;
        List<PPredio> ppredios;
        List<TramiteEstado> historicoObservaciones = null;
        TramiteEstado observacionTemporal;
        DocumentoResultadoPPRedios docP = new DocumentoResultadoPPRedios();

        try {
            documentoResultado = this.documentoDAO
                .buscarDocumentoResultadoPorTramiteId(tramiteId);
            ppredios = this.pPredioService
                .buscarPPrediosResumenTramitePorTramiteId(tramiteId);
            historicoObservaciones = this.tramiteEstadoService.
                buscarHistoricoObservacionesPorTramiteId(tramiteId);
            observacionTemporal = this.tramiteEstadoService.buscarObservacionTemporalPorTramiteId(
                tramiteId);

            docP.setDocumentoResultado(documentoResultado);
            docP.setPprediosTramite(ppredios);
            docP.setHistoricoObservaciones(historicoObservaciones);
            docP.setObservacionTemporal(observacionTemporal);

        } catch (Exception ex) {
            LOGGER.error("Error en GeneralesBean#buscarDocumentoResultadoPorTramiteId : " +
                ex.getMessage());
        }

        return docP;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#buscarTramiteEstadosPorTramiteId()
     * @author franz.gamba
     */
//TODO :: franz.gamba :: 16-08-2012 :: hacer manejo de excepciones :: pedro.garcia
    @Override
    public List<TramiteEstado> buscarTramiteEstadosPorTramiteId(Long tramiteId) {
        return this.tramiteEstadoService.buscarHistoricoObservacionesPorTramiteId(tramiteId);
    }

    /**
     * @see IGenerales#getAllTipoDocumento()
     * @author juan.agudelo
     */
    @Override
    public List<TipoDocumento> getAllTipoDocumento() {
        return this.tipoDocumentoDAO.getAllTipoDocumento();
    }

    /**
     *
     * @see co.gov.igac.snc.fachadas.IGenerales#obtenerDireccionNormalizada(java.lang.String)
     */
    @Override
    public String obtenerDireccionNormalizada(String direccion) {

        String direccionNormalizada = new String();

        try {
            direccionNormalizada = QueryNativoDAO.normalizarDireccion(
                tipoDocumentoDAO.getEntityManager(), direccion);
        } catch (Exception e) {
            LOGGER.debug("Error al obtener la direccion normalizada : " +
                e.getMessage());
        }
        return direccionNormalizada;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#obtenerFechaConAdicionDiasHabiles(java.util.Date, int)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Date obtenerFechaConAdicionDiasHabiles(Date fechaInicial, int diasSumados) {

        Date answer = null;
        LOGGER.debug("on GeneralesBean#obtenerFechaConAdicionDiasHabiles ...");

        //D: se usa cualquiera de los EJB que implementen IGenericJpaDAO para obtener el entity manager
        answer = QueryNativoDAO.calcularFechaConDiasHorasHabiles(
            this.tipoDocumentoDAO.getEntityManager(), fechaInicial, diasSumados);

        LOGGER.debug("... finished GeneralesBean#obtenerFechaConAdicionDiasHabiles.");
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Obtiene la entidad plantilla a partir de su código.
     *
     * @param codigo es el código de la plantilla a recuperar.
     * @return el objeto Plantilla correspondiente o null en caso de no encontrarlo.
     * @author jamir.avila.
     */
    @Override
    public Plantilla recuperarPlantillaPorCodigo(String codigo) {
        LOGGER.debug("codigo: " + codigo);
        Plantilla plantilla = plantillaDAOService.recuperarPorCodigo(codigo);
        return plantilla;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     */
    @Override
    public ActualizacionDocumento guardarActualizacionDocumento(
        ActualizacionDocumento actualizacionDoc) {

        actualizacionDoc = this.actualizacionDocumentoDAO.update(actualizacionDoc);
        return actualizacionDoc;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#descargarArchivoDeAlfrescoATemp(java.lang.String)
     * @author pedro.garcia copy y paste de uno que ya existía de david.cifuentes
     */
    @Implement
    @Override
    public String descargarArchivoDeAlfrescoATemp(String idAlfresco) {

        LOGGER.debug("on GeneralesBean#consultarURLDeArchivoEnAlfresco ");
        String answer = "";
        try {
            answer = this.documentoDAO.
                descargarOficioDeGestorDocumentalATempLocalMaquina(idAlfresco);
        } catch (Exception e) {
            LOGGER.error("Error en método que trae docs de alfresco: " + e.getMessage());
        }

        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#guardarMemorandoConservacion(Documento, UsuarioDTO)
     * @author javier.aponte
     */
    @Override
    public Documento guardarMemorandoConservacion(Documento documento, String numeroRadicado,
        UsuarioDTO usuario) {

        Documento doc = null;
        DocumentoMemorandoDTO documentoMemorandoDTO = null;

        if (documento == null || usuario == null) {
            return null;
        }

        if (usuario.getCodigoEstructuraOrganizacional() != null) {
            EstructuraOrganizacional estructuraOrganizacional =
                this.estructuraOrganizacionalDAOService.findById(usuario.
                    getCodigoEstructuraOrganizacional());

            String nombreDepartamento = "";
            String nombreMunicipio = "";

            if (estructuraOrganizacional == null) {
                nombreDepartamento = "CENTRAL";
                nombreMunicipio = "CENTRAL";
            } else {
                nombreDepartamento = estructuraOrganizacional.getMunicipio()
                    .getDepartamento().getNombre();
                nombreMunicipio = estructuraOrganizacional.getMunicipio().getNombre();
            }

            Date fechaMemorando = new Date(System.currentTimeMillis());
            String numeroMemorando = numeroRadicado;

            documentoMemorandoDTO = new DocumentoMemorandoDTO(
                documento.getArchivo(),
                fechaMemorando, nombreDepartamento,
                nombreMunicipio, numeroMemorando);

            doc = this.documentoDAO.guardarMemorandoConservacionGestorDocumental(
                documentoMemorandoDTO,
                documento);
        }
        return doc;
    }

    /**
     * @see IGenerales#getZonasFromMunicipio(String)
     */
    @Override
    public List<Zona> getZonasFromMunicipio(String municipioCodigo) {
        return this.zonaDAO.findByMunicipio(municipioCodigo);
    }

    /**
     * @see IGenerales#getSectoresFromZona(String)
     */
    @Override
    public List<Sector> getSectoresFromZona(String zonaId) {
        return this.sectorDAO.findByZona(zonaId);
    }

    /**
     * @see IGenerales#getComunasFromSector(String)
     */
    @Override
    public List<Comuna> getComunasFromSector(String sectorId) {
        return this.comunaDAO.findBySector(sectorId);
    }

    /**
     * @see IGenerales#getBarriosFromComuna(String)
     */
    @Override
    public List<Barrio> getBarriosFromComuna(String comunaId) {
        return this.barrioDAO.findByComuna(comunaId);
    }

    /**
     * @see IGenerales#getManzanaVeredaFromBarrio(String)
     */
    @Override
    public List<ManzanaVereda> getManzanaVeredaFromBarrio(String barrioId) {
        return this.manzanaVeredaDAO.findByBarrio(barrioId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGenerales#borrarDepartamento(Departamento)
     */
    @Override
    public void borrarDepartamento(Departamento departamento) {
        this.departamentoDAO.delete(departamento);
    }

    /**
     * @see IGenerales#borrarMunicipio(Municipio)
     */
    @Override
    public void borrarMunicipio(Municipio municipio) {
        this.municipioDAO.delete(municipio);
    }

    /**
     * @see IGenerales#borrarZona(Zona)
     */
    @Override
    public void borrarZona(Zona zona) {
        this.zonaDAO.delete(zona);
    }

    /**
     * @see IGenerales#borrarSector(Sector)
     */
    @Override
    public void borrarSector(Sector sector) {
        this.sectorDAO.delete(sector);
    }

    /**
     * @see IGenerales#borrarComuna(Comuna)
     */
    @Override
    public void borrarComuna(Comuna comuna) {
        this.comunaDAO.delete(comuna);
    }

    /**
     * @see IGenerales#borrarBarrio(Barrio)
     */
    @Override
    public void borrarBarrio(Barrio barrio) {
        this.barrioDAO.delete(barrio);
    }

    /**
     * @see IGenerales#borrarManzanaVereda(ManzanaVereda)
     */
    @Override
    public void borrarManzanaVereda(ManzanaVereda manzanaVereda) {
        this.manzanaVeredaDAO.delete(manzanaVereda);
    }

    /**
     * @see IGenerales#borrarDepartamento(Departamento)
     */
    @Override
    public void agregarDepartamento(Departamento departamento) {
        this.departamentoDAO.persist(departamento);
    }

    /**
     * @see IGenerales#borrarMunicipio(Municipio)
     */
    @Override
    public void agregarMunicipio(Municipio municipio) {
        this.municipioDAO.persist(municipio);
    }

    /**
     * @see IGenerales#borrarZona(Zona)
     */
    @Override
    public void agregarZona(Zona zona) {
        this.zonaDAO.persist(zona);
    }

    /**
     * @see IGenerales#borrarSector(Sector)
     */
    @Override
    public void agregarSector(Sector sector) {
        this.sectorDAO.persist(sector);
    }

    /**
     * @see IGenerales#borrarComuna(Comuna)
     */
    @Override
    public void agregarComuna(Comuna comuna) {
        this.comunaDAO.persist(comuna);
    }

    /**
     * @see IGenerales#borrarBarrio(Barrio)
     */
    @Override
    public void agregarBarrio(Barrio barrio) {
        this.barrioDAO.persist(barrio);
    }

    /**
     * @see IGenerales#borrarManzanaVereda(ManzanaVereda)
     */
    @Override
    public void agregarManzanaVereda(ManzanaVereda manzanaVereda) {
        this.manzanaVeredaDAO.persist(manzanaVereda);
    }

    /**
     * @see IGenerales#modificarDepartamento(Departamento)
     */
    @Override
    public void modificarDepartamento(Departamento departamento) {
        this.departamentoDAO.update(departamento);
    }

    /**
     * @see IGenerales#modificarMunicipio(Municipio)
     */
    @Override
    public void modificarMunicipio(Municipio municipio) {
        this.municipioDAO.update(municipio);
    }

    /**
     * @see IGenerales#modificarZona(Zona)
     */
    @Override
    public void modificarZona(Zona zona) {
        this.zonaDAO.update(zona);
    }

    /**
     * @see IGenerales#modificarSector(Sector)
     */
    @Override
    public void modificarSector(Sector sector) {
        this.sectorDAO.update(sector);
    }

    /**
     * @see IGenerales#modificarComuna(Comuna)
     */
    @Override
    public void modificarComuna(Comuna comuna) {
        this.comunaDAO.update(comuna);
    }

    /**
     * @see IGenerales#modificarBarrio(Barrio)
     */
    @Override
    public void modificarBarrio(Barrio barrio) {
        this.barrioDAO.update(barrio);
    }

    /**
     * @see IGenerales#modificarManzanaVereda(ManzanaVereda)
     */
    @Override
    public void modificarManzanaVereda(ManzanaVereda manzanaVereda) {
        this.manzanaVeredaDAO.update(manzanaVereda);
    }

    /**
     *
     * @see
     * co.gov.igac.snc.fachadas.IGenerales#agregarLogMensaje(co.gov.igac.snc.persistence.entity.generales.LogMensaje)
     * @author juan.mendez
     */
    @Override
    public void agregarLogMensaje(LogMensaje mensaje) {
        this.logMensajeDAO.persist(mensaje);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#getCacheZonaByCodigo(String)
     */
    @Override
    public Zona getCacheZonaByCodigo(String codigo) {
        return zonaDAO.getZonaByCodigo(codigo);
    }

    /**
     * @see IGenerales#getCacheSectorByCodigo(String)
     */
    @Override
    public Sector getCacheSectorByCodigo(String codigo) {
        return sectorDAO.getSectorByCodigo(codigo);
    }

    /**
     * @see IGenerales#getCacheComunaByCodigo(String)
     */
    @Override
    public Comuna getCacheComunaByCodigo(String codigo) {
        return comunaDAO.getComunaByCodigo(codigo);
    }

    /**
     * @see IGenerales#getCacheBarrioByCodigo(String)
     */
    @Override
    public Barrio getCacheBarrioByCodigo(String codigo) {
        return barrioDAO.getBarrioByCodigo(codigo);
    }

    /**
     * @see IGenerales#getCacheManzanaVeredaByCodigo(String)
     */
    @Override
    public ManzanaVereda getCacheManzanaVeredaByCodigo(String codigo) {
        return manzanaVeredaDAO.getManzanaVeredaByCodigo(codigo);
    }

    /**
     * @see IGenerales#getFuncionariosTerritorial(String,String)
     */
    @Override
    public List<UsuarioDTO> getFuncionariosTerritorial(String territorial,
        String uo) {

        return LdapDAO.getFuncionariosDto(territorial, uo);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#esColindante(java.lang.String, java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean esColindante(String numPredialPredio, List<String> numPredialesComparados) {
        boolean answer = false;

        try {
            answer = this.colindanteDAO.isAdjacent(numPredialPredio, numPredialesComparados);
        } catch (Exception ex) {
            LOGGER.error("Excepción en el método GeneralesBean#esColindante: " + ex.getMessage());
        }

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#obtenerFuncionarioTerritorialYRol(String, ERol)
     * @author franz.gamba
     */
    @Override
    public List<UsuarioDTO> obtenerFuncionarioTerritorialYRol(String territorial, ERol rol) {
        LOGGER.info("inicio: " + territorial + ", " + rol);
        String nombreHomologado = null;
        try {
            nombreHomologado = this.codigoHomologadoDAO.obtenerCodigoHomologado(
                ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL, ESistema.SNC, ESistema.LDAP,
                territorial, new Date());
        } catch (ExcepcionSNC e) {
            LOGGER.info(e.getMensaje());//0k
        }
        String nombreTerritorial = (nombreHomologado == null) ? territorial : nombreHomologado;

        if(nombreTerritorial==null){
            return Collections.emptyList();
        }
        
        LOGGER.info("fin: nombreHomologado=" + territorial);

        return LdapDAO.getUsuariosEstructuraRol2(nombreTerritorial, rol);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#getDepartamentoByCodigo(String)
     */
    /*
     * @modified juan.agudelo
     */
    @Override
    public Departamento getDepartamentoByCodigo(String codigo) {
        return this.departamentoDAO.getDepartamentoByCodigo(codigo);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#getCacheMunicipiosPorCodigoEstructuraOrganizacional(String)
     * @author juan.agudelo
     */
    @Override
    public List<Municipio> getCacheMunicipiosPorCodigoEstructuraOrganizacional(String codigoEO) {
        return this.jurisdicionService
            .findMunicipiosbyCodigoEstructuraOrganizacional(codigoEO);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see
     * IGeneralesLocal#getCacheMunicipiosTerritorialPorDepartametoYCodigoEstructuraOrganizacional(String)
     * @author juan.agudelo
     */
    @Override
    public List<Municipio> getCacheMunicipiosTerritorialPorDepartametoYCodigoEstructuraOrganizacional(
        String departamentoCodigo, String codigoEO) {

        List<String> codigosEstructuraOrganizacionalList = this.estructuraOrganizacionalDAOService
            .findMunicipiosTerritorialByCodigoExtructuraOrganizacional(codigoEO);

        return this.jurisdicionService
            .findMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional(
                departamentoCodigo, codigosEstructuraOrganizacionalList);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#getDepartamentoByCodigoEstructuraOrganizacional(String)
     */
    /*
     * @modified juan.agudelo
     */
    @Override
    public List<Departamento> getDepartamentoByCodigoEstructuraOrganizacional(
        String codigo) {

        List<String> codigosEO = this.estructuraOrganizacionalDAOService
            .getCodigosEOAsociadosByCodigoEstructuraOrganizacional(codigo);

        if (codigosEO != null && !codigosEO.isEmpty()) {
            List<String> deptoCodigo = this.jurisdicionService
                .getCodigoDepartamentoByCodigoEstructuraOrganizacional(codigosEO);

            List<Departamento> answer = new ArrayList<Departamento>();
            for (String sTmp : deptoCodigo) {
                answer.add(this.departamentoDAO.getDepartamentoByCodigo(sTmp));
            }
            return answer;
        } else {
            return new ArrayList<Departamento>();
        }
    }

    // --------------------------------------------------------- //
    /**
     * @see IGenerales#buscarDominioPorNombreOrderByNombre(EDominio)
     */
    @Override
    public List<Dominio> buscarDominioPorNombreOrderByNombre(Enum d) {
        return this.dominioDAO.findByNombreOrderByNombre(d);
    }

    // --------------------------------------------------------- //
    /**
     * @see IGenerales#buscarDepartamentosConCatastroCentralizadoPorCodigoPais(String)
     */
    @Override
    public List<Departamento> buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
        String codigoPais) {
        return this.departamentoDAO.buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
            codigoPais);
    }

    // --------------------------------------------------------- //
    /**
     * @see IGenerales#buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(String)
     */
    @Override
    public List<Municipio> buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
        String codigoDepartamento) {
        return this.municipioDAO.buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
            codigoDepartamento);
    }

    // --------------------------------------------------------- //
    /**
     * @see IGenerales#getDescripcionEstructuraOrganizacionalPorMunicipioCod
     */
    @Override
    public String getDescripcionEstructuraOrganizacionalPorMunicipioCod(
        String municipioCod) {

        String codigo = this.jurisdicionService.getEstructuraOrganizacionalIdByMunicipioCod(
            municipioCod);

        String answer = this.getCacheCodigoHomologado(
            ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
            ESistema.SNC, ESistema.LDAP, codigo, null);

        return answer;
    }

    // --------------------------------------------------------- //

    /**
     * @see IGenerales#getEstructuraOrgSuperiorPorMunicipioCod
     */
    @Override
    public String getEstructuraOrgSuperiorPorMunicipioCod(String municipioCod) {

        String estructuraCod = this.estructuraOrganizacionalDAOService.
            findEstructuraOrgSuperiorByMunicipioCod(municipioCod);

        String answer = this.getCacheCodigoHomologado(
            ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
            ESistema.SNC, ESistema.LDAP, estructuraCod, new Date());

        return answer;
    }

    // --------------------------------- //
    /**
     * @see IGenerales#actualizarDocumentoYGestorDocumentalConValidacionNumeroPredial(UsuarioDTO,
     * DocumentoTramiteDTO, Documento, Tramite)
     * @author david.cifuentes
     */
    @Override
    public boolean actualizarDocumentoYAlfrescoConValidacionNumeroPredial(
        UsuarioDTO usuario, DocumentoTramiteDTO documentoTramiteDTO,
        Documento documento, Tramite tramite) {
        return this.documentoDAO.actualizarDocumentoYGestorDocumentalConValidacionNumeroPredial(
            usuario,
            documentoTramiteDTO, documento, tramite);
    }

    @Override
    public UsuarioDTO obtenerSubdirectorCatastro() {

        return LdapDAO.getSubdirectorCatastro();
    }

    /**
     * @see IGenerales#municipioPerteneceACatastroDescentralizado(String)
     * @author christian.rodriguez
     */
    @Override
    public boolean municipioPerteneceACatastroDescentralizado(String codMunicipio) {

        boolean banderaResultado = false;

        List<Dominio> municipiosDescentralizados = this
            .getCacheDominioPorNombre(EDominio.CATASTRO_DESCENTRALIZADO);

        for (Dominio dominio : municipiosDescentralizados) {
            if (dominio.getCodigo().equals(codMunicipio)) {
                banderaResultado = true;
            }
        }

        return banderaResultado;
    }
//--------------------------------------------------------------------------------------------------	

    @Override
    public EstructuraOrganizacional buscarEstructuraOrganizacionalPorCodigo(
        String codigoEstructuraOrganizacional) {
        return this.estructuraOrganizacionalDAOService.buscarEstructuraOrganizacionalPorCodigo(
            codigoEstructuraOrganizacional);
    }

    // --------------------------------- //
    /**
     * @see IGenerales#actualizarListaDeDocumentos(List<Documento>)
     * @author david.cifuentes
     */
    @Override
    public boolean actualizarListaDeDocumentos(
        List<Documento> documentosActualizar) {
        try {
            this.documentoDAO.updateMultiple(documentosActualizar);
            return true;
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje());
            return false;
        }
    }

    /**
     * @see IGenerales#getCacheUsoConstruccionById(Long id)
     * @author fredy.wilches
     */
    @Override
    public UsoConstruccion getCacheUsoConstruccionById(Long id) {
        return usoConstruccionDAO.findById(id);
    }

    /**
     * @see IGenerales#buscarValorDelDominioPorDominioYCodigo(EDominio, String)
     * @author david.cifuentes
     */
    @Override
    public Dominio buscarValorDelDominioPorDominioYCodigo(EDominio nombre, String codigo) {
        return dominioDAO.findByCodigo(nombre, codigo);
    }

    /**
     * @see IGenerales#guardarDocumentosSolicitudAvaluoAlfresco(EDominio, String)
     * @author felipe.cadena
     */
    @Override
    public Documento guardarDocumentosSolicitudAvaluoAlfresco(UsuarioDTO usuario,
        DocumentoSolicitudDTO documentoSolicitudDTO, Documento documento) {

        Documento documentoAlmacenado = null;
        try {
            documentoAlmacenado =
                this.documentoDAO.guardarDocumentosSolicitudAvaluoAlfresco(usuario,
                    documentoSolicitudDTO, documento);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#guardarDocumentosSolicitudAvaluoAlfresco: " +
                ex.getMensaje());
        }

        return documentoAlmacenado;
    }

    /**
     * @see IGenerales#obtenerTerritorialPorCodigo(String)
     * @author felipe.cadena
     */
    @Override
    public EstructuraOrganizacional obtenerTerritorialPorCodigo(String codigo) {
        EstructuraOrganizacional territorial = null;
        try {
            territorial = this.estructuraOrganizacionalDAOService.
                buscarEstructuraOrganizacionalPorCodigo(codigo);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en AvaluosBean#obtenerTerritorialPorCodigo: " +
                ex.getMensaje());
        }
        return territorial;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * Método para validar la existencia de geometría para los predios indicados por el parámetro
     * (Forma asincrónica)
     *
     * @param tramite	tramite para el cual sus predios asociados van a ser validados
     * @param usuario
     * @author fredy.wilches
     */
    /*
     * @modified by javier.aponte ajustes varios para validar la geometría de los predios 15/08/2013
     */
    @Override
    public void validarGeometriaPredios(Tramite tramite, UsuarioDTO usuario) {

        LOGGER.debug("En GeneralesBean#validarGeometriaPredios ...");

        try {
            StringBuilder identificadorPredios = new StringBuilder();

            List<TramiteDetallePredio> tdp =
                this.tramiteService.getTramiteDetallePredioByTramiteId(tramite.getId());

            FichaMatriz fichaMatriz;
            FichaMatrizPredio fichaMatrizPredio;

            if (tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz() || tramite.
                isTerceraMasiva()) {
                //felipe.cadena :: manejo de inconsistencias para tramites de desenglobe por etapas y rectificaciones de ficha matriz
                identificadorPredios.append(tramite.getPredio().getNumeroPredial()).append(",");;

                if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad())) {
                    for (TramiteDetallePredio tdpTemp : tdp) {
                        identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).append(
                            ",");
                    }
                }
                identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);
            } else if (tdp != null && !tdp.isEmpty()) {
                for (TramiteDetallePredio tdpTemp : tdp) {

                    if (!tramite.isSegunda() && EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                        tdpTemp.getPredio().getCondicionPropiedad())) {
                        fichaMatrizPredio = this.fichaMatrizPredioDAO.
                            obtenerFichaMatrizPredioPorNumeroPredial(tdpTemp.getPredio().
                                getNumeroPredial());
                        fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                        if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                            identificadorPredios.append(fichaMatriz.getPredio().getNumeroPredial()).
                                append(",");
                        }
                    } else if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tdpTemp.
                        getPredio().getCondicionPropiedad()) &&
                        !EPredioCondicionPropiedad.CP_6.getCodigo().equals(tdpTemp.getPredio().
                            getCondicionPropiedad())) {
                        identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).append(
                            ",");
                    }
                }
                identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);

            } else if (tramite.getPredio() != null) {

                if (!tramite.isSegunda() && EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                    tramite.getPredio().getCondicionPropiedad())) {
                    fichaMatrizPredio = this.fichaMatrizPredioDAO.
                        obtenerFichaMatrizPredioPorNumeroPredial(tramite.getPredio().
                            getNumeroPredial());
                    fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                    if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                        identificadorPredios.append(fichaMatriz.getPredio().getNumeroPredial());
                    }
                } else if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad()) &&
                    !EPredioCondicionPropiedad.CP_6.getCodigo().equals(tramite.getPredio().
                        getCondicionPropiedad())) {
                    identificadorPredios.append(tramite.getPredio().getNumeroPredial());
                }
            }

            if (identificadorPredios.length() > 0) {
                this.integracionService.validarGeometriaPredios(
                    identificadorPredios.toString(), tramite.getId(), usuario, tramite.
                    getTipoTramite());
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la validación de la geometría de predios: " + e);
            throw e;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#determinarInconsistenciasGeograficas(List<String>, String, boolean )
     * @author felipe.cadena
     */
    /*
     * modified andres.eslava::25-01-2016::Migracion Arcgis10.3 procesamiento nueva respuesta
     * servicio.
     */
    @Override
    public List<TramiteInconsistencia> determinarInconsistenciasGeograficas(String numeroPredial,
        Tramite tramite, UsuarioDTO usuario) throws Exception {

        List<TramiteInconsistencia> resultado = new ArrayList<TramiteInconsistencia>();
        try {
            String condicionProp = numeroPredial.substring(21, 22);
            if (EPredioCondicionPropiedad.CP_5.getCodigo().equals(condicionProp)) {
                resultado = this.procesarInconsistenciasGeograficasPrediosSync(numeroPredial,
                    usuario, tramite, false, true);
            } else {
                resultado = this.procesarInconsistenciasGeograficasPrediosSync(numeroPredial,
                    usuario, tramite, false, false);
            }
        } catch (ExcepcionSNC e) {
            throw new Exception(e);
        }

        return resultado;

    }
//--------------------------------------------------------------------------------------------------        

    /**
     * @see
     * IGeneralesLocal#obtenerRutaDatosGeograficosTramiteAsShape(co.gov.igac.snc.persistence.entity.generales.Documento)
     * @author juanfelipe.garcia
     */
    @Override
    public String obtenerRutaDatosGeograficosTramiteAsShape(String numeroTramite,
        String identificadoresPredios, String tipoMutacion, UsuarioDTO usuario) {
        String ruta = null;
        try {
            //ruta = SigDAO.getInstance(logMensajeDAO).exportarDatosGeograficosTramiteAsShape(numeroTramite, identificadoresPredios, tipoMutacion, token, usuario);
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                "Servicio No Implementado. La invocación debe realizarse de forma asincrónica");
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error obteniendo la ruta del comprimido con los shapes" + e);
        }
        return ruta;
    }

    /**
     * @see
     * IGeneralesLocal#actualizarDocumento(co.gov.igac.snc.persistence.entity.generales.Documento)
     * @author felipe.cadena
     */
    @Override
    public Documento actualizarDocumento(Documento documento) {
        Documento doc = null;

        try {
            doc = this.documentoDAO.update(documento);
            TipoDocumento tipoDoc = this.tipoDocumentoDAO.
                findById(documento.getTipoDocumento().getId());
            doc.setTipoDocumento(tipoDoc);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la actualización del documento :" + e);
        }
        return doc;
    }

    /**
     * @see IGeneralesLocal#buscarDocumentoPorTramiteIdyTipoDocumento(Long, Long)
     * @author lorena.salamanca
     * @modified by javier.aponte
     */
    /*
     * Se agrega manejo de excepciones @author javier.aponte
     */
    @Override
    public Documento buscarDocumentoPorTramiteIdyTipoDocumento(Long tramiteId, Long idTipoDocumento) {

        Documento doc = null;

        try {
            doc = this.documentoDAO.buscarDocumentoPorTramiteIdyTipoDocumento(tramiteId,
                idTipoDocumento);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorTramiteIdyTipoDocumento: " + ex.
                getMessage());
        }
        return doc;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#getCacheProfesiones()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Profesion> getCacheProfesiones() {

        List<Profesion> answer = null;

        try {
            answer = this.profesionDAO.findAll();
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#getCacheProfesiones: " + ex.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * @see IGeneralesLocal#getCapitalDepartamentoByCodigoDepartamento()
     * @author lorena.salamanca
     */
    @Override
    public Municipio getCapitalDepartamentoByCodigoDepartamento(String departamentoId) {
        Municipio municipio = new Municipio();
        try {
            municipio = municipioDAO.getMunicipioByCodigo(departamentoId + "001");
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#getCapitalDepartamentoByCodigoDepartamento: " +
                ex.getMensaje());
        }

        return municipio;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#obtenerConfiguracionReportePorId
     * @author javier.aponte
     */
    @Override
    public ConfiguracionReporte obtenerConfiguracionReportePorId(Long configuracionReporteId) {
        return this.configuracionReporteDAO.obtenerConfiguracionReportePorId(configuracionReporteId);
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#getNumeroManzanasPorMunicipio()
     * @author javier.barajas
     */
    @Override
    public List<Barrio> getNumeroManzanasPorMunicipio(String codigoMunicipio) {

        try {
            return this.barrioDAO.numeroBarriosMunicipio(codigoMunicipio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#getNumeroManzanasPorMunicipio: " +
                ex.getMensaje());
        }
        return null;
    }
//---------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see IGeneralesLocal#borrarDocumentoEnBDyAlfresco(Documento)
     */
    @Override
    public boolean borrarDocumentoEnBDyAlfresco(Documento documento) {
        boolean answer = false;
        try {
            answer = this.documentoDAO.deleteDocumentoAlsoGestorDocumental(documento);
        } catch (Exception e) {
            LOGGER.error("Error en la eliminación del documento : " + e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author christian.rodriguez
     * @see IGeneralesLocal#borrarDocumentoEnAlfresco(String)
     */
    @Override
    public boolean borrarDocumentoEnAlfresco(String idDocumentoAlfresco) {
        boolean borradoExitoso = false;
        try {
            IDocumentosService documentoService = DocumentalServiceFactory.getService();
            documentoService.borrarDocumento(idDocumentoAlfresco);
            borradoExitoso = true;
        } catch (Exception e) {
            LOGGER.error("Error en la eliminación del documento de alfresco: " +
                e.getMessage());
        }
        return borradoExitoso;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IGeneralesLocal#enviarCorreoElectronicoConCuerpo(EPlantilla, List, Object[], String[],
     * String[])
     * @author christian.rodriguez
     */
    @Override
    public boolean enviarCorreoElectronicoConCuerpo(EPlantilla plantillaCorreo,
        List<String> destinatariosTC, List<String> destinatariosCC,
        List<String> destinatariosBCC, Object[] datosCorreo,
        String[] adjuntos, String[] titulos) {

        boolean envioExitoso = false;
        MessageFormat mf;

        String cuerpoCorreo;

        try {
            Plantilla plantilla = recuperarPlantillaPorCodigo(plantillaCorreo
                .getCodigo());

            if (plantilla != null) {
                cuerpoCorreo = plantilla.getHtml();
                if (cuerpoCorreo != null) {

                    Locale colombia = new Locale("ES", "es_CO");
                    mf = new MessageFormat(cuerpoCorreo, colombia);

                    cuerpoCorreo = mf.format(datosCorreo);

                    try {

                        envioExitoso = this.enviarCorreo(destinatariosTC,
                            destinatariosCC, destinatariosBCC,
                            plantillaCorreo.getMensaje(), cuerpoCorreo,
                            adjuntos, titulos);

                    } catch (Exception e) {
                        throw SncBusinessServiceExceptions.EXCEPCION_100013
                            .getExcepcion(LOGGER, e, e.getMessage());
                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return envioExitoso;
    }
//---------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see IGeneralesLocal#buscarFormatoEnAlfresco(String)
     */
//	@Override
//	public String buscarFormatoEnAlfresco(String nombreFormato) {
//		String answer = null;
//		try{
//			answer = this.documentoDAO.buscarFormato(nombreFormato);
//		}
//        catch (Exception e) {
//			LOGGER.error("Error en GeneralesBean#buscarFormatoEnAlfrescoYenElFtp: "
//					+e.getMessage());
//		}
//		return answer;
//	}
//---------------------------------------------------------------------------------------------	
    /**
     * @see IGeneralesLocal#obtenerReporteGenerico(Map, String, UsuarioDTO)
     * @author javier.aponte
     */
    @Override
    public File obtenerReporteDinamico(Map<String, String> parameters, Long configuracionReporteId,
        UsuarioDTO loggedInUser) {

        File resultado = null;

        ConfiguracionReporte configuracionReporte;

        configuracionReporte = this.obtenerConfiguracionReportePorId(configuracionReporteId);

        if (configuracionReporte == null) {
            LOGGER.error("Error en GeneralesBean#obtenerReporteDinamico: No se encontraron datos " +
                "en la tabla configuración reporte, para el id: " + configuracionReporteId);
            return null;
        }

        try {
            /*
             * ReporteGenerico reporte = new ReporteGenerico(loggedInUser,
             * configuracionReporte.getDefinicionXml(), this.dominioDAO.getEntityManager()); *
             * IReporteGenericoService service = ReporteServiceFactory.getGenericReportService();
             *
             * Set<Entry<String, String>> entries = parameters.entrySet(); Iterator<Entry<String,
             * String>> it = entries.iterator(); while (it.hasNext()) { Entry<String, String> entry
             * = (Entry<String, String>) it.next(); reporte.addParameter(entry.getKey(),
             * entry.getValue()); }
             *
             * LogMensajesReportesUtil log = new LogMensajesReportesUtil(this, loggedInUser);
             *
             * resultado = service.getReportAsFile(reporte,log);
             */
        } catch (Exception e) {
            LOGGER.error("Error al obtener el reporte generico: " + e.getMessage());
        }

        return resultado;
    }

    /**
     * @see IGeneralesLocal#publicarArchivoEnWeb(String)
     * @author javier.aponte
     */
    @Override
    public String publicarArchivoEnWeb(String path) {
        String rutaWeb = "";
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            //Original :: String rutaWeb = service.publicarArchivoEnWeb(path);
            //pach del archivo en la carpeta temp local
            /*
             * juanfelipe.garcia :: 15/05/2013 :: cambio de metodo en Documental Se carga el
             * documento de la temporal local, a la carpeta publica de alfresco para poder verlo por
             * http
             */
            DocumentoVO documentoVO = service.cargarDocumentoWorkspacePreview(path);
            rutaWeb = documentoVO.getUrlPublico();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#publicarReporteDinamicoEnWeb: " + ex.getMensaje());
        }
        return rutaWeb;
    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#descargarArchivoAlfrescoYSubirAPreview(String idRepositorioDocumentos)
     * @author juanfelipe.garcia
     */
    @Override
    public String descargarArchivoAlfrescoYSubirAPreview(String idRepositorioDocuentos) {
        String rutaPreview = "";
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            DocumentoVO documentoVO = service.cargarDocumentoPrivadoEnWorkspacePreview(
                idRepositorioDocuentos);
            rutaPreview = documentoVO.getUrlPublico();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#descargarArchivoAlfrescoYSubirAPreview: " + ex.
                getMensaje());
        }
        return rutaPreview;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#enviarCorreo(java.util.List, java.lang.String, java.lang.String,
     * java.lang.String[], java.lang.String[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean enviarCorreo(List<String> destinatarios, String tema, String mensaje,
        String[] adjuntos, String[] titulos) {

        boolean envioCorreosExitoso = true;

        for (String destinatario : destinatarios) {
            envioCorreosExitoso = enviarCorreo(destinatario, tema, mensaje, adjuntos, titulos);
            if (!envioCorreosExitoso) {
                envioCorreosExitoso = false;
            }
        }

        return envioCorreosExitoso;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#enviarCorreo(java.util.List, java.util.List, java.util.List,
     * java.lang.String, java.lang.String, java.lang.String[], java.lang.String[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean enviarCorreo(List<String> destinatariosTO, List<String> destinatariosCC,
        List<String> destinatariosBCC, String asunto, String mensaje, String[] adjuntos,
        String[] titulos) {

        boolean envioCorreosExitoso;

        try {
            Mailer.getInstance().sendEmail(destinatariosTO, destinatariosCC, destinatariosBCC,
                asunto, mensaje, adjuntos, titulos);
            envioCorreosExitoso = true;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#enviarCorreo: " + ex.getMensaje());
            envioCorreosExitoso = false;
        }

        return envioCorreosExitoso;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#obtenerTipoDocumentoAvaluoAdicional(java.lang.String)
     *
     * @author felipe.cadena
     * @param tipoTramite
     * @return
     */
    @Override
    public List<TipoDocumento> obtenerTipoDocumentoAvaluoAdicional(String tipoTramite) {
        List<TipoDocumento> tipoDocumentos = null;

        try {
            tipoDocumentos = this.tipoDocumentoDAO.obtenerTipoDocumentoAvaluoAdicional(tipoTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#obtenerTipoDocumentoAvaluoAdicional: " + ex.
                getMensaje());
        }
        return tipoDocumentos;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGenerales#calcularDiasHabilesEntreDosFechas(java.util.Date, java.util.Date)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public Integer calcularDiasHabilesEntreDosFechas(Date fechaDesde, Date fechaHasta) {

        Integer answer = null;
        LOGGER.debug("on GeneralesBean#calcularDiasHabilesEntreDosFechas ...");

        //D: se usa cualquiera de los EJB que implementen IGenericJpaDAO para obtener el entity manager
        answer = QueryNativoDAO.calcularDiasHabilesEntreDosFechas(
            this.tipoDocumentoDAO.getEntityManager(), fechaDesde, fechaHasta);

        LOGGER.debug("... finished GeneralesBean#calcularDiasHabilesEntreDosFechas.");
        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGenerales#esDiaHabil(java.util.Date)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public boolean esDiaHabil(Date fecha) {

        boolean answer;
        LOGGER.debug("on GeneralesBean#esDiaHabil ...");

        //D: se usa cualquiera de los EJB que implementen IGenericJpaDAO para obtener el entity manager
        answer = QueryNativoDAO.esDiaHabil(
            this.tipoDocumentoDAO.getEntityManager(), fecha);

        LOGGER.debug("... finished GeneralesBean#esDiaHabil.");
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#obtenerNumeroRadicado()
     * @author javier.aponte
     */
    @Override
    public String obtenerNumeroRadicado(List<Object> parametros) {

        String numeroRadicadoResultado = null;
        Object[] resultado = this.radicarEnCorrespondencia(parametros);
        if (resultado != null && resultado.length > 0) {
            numeroRadicadoResultado = resultado[5].toString();
        }
        return numeroRadicadoResultado;

    }

    /**
     * @see IGeneralesLocal#obtenerEstructuraOrganizacionalPorMunicipioCodigo(String)
     * @author christian.rodriguez
     */
    @Override
    public EstructuraOrganizacional obtenerEstructuraOrganizacionalPorMunicipioCodigo(
        String municipioCodigo) {

        LOGGER.debug("en AvaluosBean#obtenerEstructuraOrganizacionalPorMunicipioCodigo");
        EstructuraOrganizacional estructuraOrganizacionalEncontrada = null;

        try {
            estructuraOrganizacionalEncontrada = this.jurisdicionService
                .getEstructuraOrganizacionalByMunicipioCod(municipioCodigo);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluosBean#obtenerEstructuraOrganizacionalPorMunicipioCodigo: " +
                ex.getMensaje());
        }

        return estructuraOrganizacionalEncontrada;
    }

    /**
     * @see IGenerales#obtenerCapitulosAvaluo()
     * @author rodrigo.hernandez
     *
     * @return Lista de capitulos usados para la proyeccion de cotizacion de un avaluo
     *
     * Tener en cuenta que los datos: - id - avaluoId - fechaLog - usuarioLog - otraDescripcion de
     * cada elemento de la lista quedan en null porque se definen en el MB donde se use la lista
     */
    @Implement
    @Override
    public List<AvaluoCapitulo> obtenerCapitulosAvaluo() {

        List<AvaluoCapitulo> listaCapitulos = new ArrayList<AvaluoCapitulo>();
        List<Dominio> listaDominios = new ArrayList<Dominio>();
        AvaluoCapitulo capitulo = null;

        listaDominios = this.getCacheDominioPorNombre(EDominio.AVALUO_CAPITULO_CODIGO);

        for (Dominio dominio : listaDominios) {
            capitulo = new AvaluoCapitulo();
            capitulo.setCodigo(dominio.getCodigo());
            capitulo.setDescripcion(dominio.getDescripcion());
            capitulo.setTitulo(dominio.getValor());
            listaCapitulos.add(capitulo);
        }

        return listaCapitulos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#buscarTipoDocumentoSoporteProductos()
     * @author javier.barajas
     */
    @Override
    public List<TipoDocumento> buscarTipoDocumentoSoporteProductos() {
        List<TipoDocumento> tiposDocumento = null;
        try {
            tiposDocumento = this.tipoDocumentoDAO.obtenerTipoDocumentoSoporteProductos();

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluosBean#obtenerEstructuraOrganizacionalPorMunicipioCodigo: " +
                ex.getMensaje());
        }
        return tiposDocumento;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * IGeneralesLocal#obtenerCodigosMunicipiosPorCodDepartamentoSinCatastroDescentralizado(String)
     * @author david.cifuentes
     */
    @Override
    public List<String> obtenerCodigosMunicipiosPorCodDepartamentoSinCatastroDescentralizado(
        String codigoDepartamento) {

        // Búsqueda de municipios de catastro descentralizado, los cuales se van
        // a excluir en el cargue de los códigos de municipios
        List<String> codMunicipiosCatastroDescentralizado = new ArrayList<String>();

        List<Dominio> municipiosDescentralizados = this
            .getCacheDominioPorNombre(EDominio.CATASTRO_DESCENTRALIZADO);

        for (Dominio dominio : municipiosDescentralizados) {
            if (dominio.getCodigo() != null &&
                !dominio.getCodigo().trim().isEmpty()) {
                codMunicipiosCatastroDescentralizado.add(dominio.getCodigo());
            }
        }

        // Busqueda de los municipios por departamento excluyendo los municipios
        // encontrados con catastro desentralizado.
        List<String> codigosMunicipios = new ArrayList<String>();
        List<Municipio> municipios = getCacheMunicipiosByDepartamento(codigoDepartamento);
        if (municipios != null && !municipios.isEmpty()) {
            for (Municipio m : municipios) {
                if (m.getCodigo() != null &&
                    !m.getCodigo().trim().isEmpty() &&
                    !codMunicipiosCatastroDescentralizado.contains(m
                        .getCodigo())) {
                    codigosMunicipios.add(m.getCodigo());
                }
            }
        }
        return codigosMunicipios;
    }

    /**
     * @see IGeneralesLocal#obteneMunicipiosDepartamentoSinCatastroDescentralizado(String)
     * @author dumar.penuela
     */
    @Override
    public List<Municipio> obteneMunicipiosDepartamentoSinCatastroDescentralizado(
        String codigoDepartamento) {
        Municipio municipio;
        // Búsqueda de municipios de catastro descentralizado, los cuales se van
        // a excluir en el cargue de los códigos de municipios
        List<String> codMunicipiosCatastroDescentralizado = new ArrayList<String>();

        List<Dominio> municipiosDescentralizados = this
            .getCacheDominioPorNombre(EDominio.CATASTRO_DESCENTRALIZADO);

        for (Dominio dominio : municipiosDescentralizados) {
            if (dominio.getCodigo() != null &&
                !dominio.getCodigo().trim().isEmpty()) {
                codMunicipiosCatastroDescentralizado.add(dominio.getCodigo());
            }
        }

        // Busqueda de los municipios por departamento excluyendo los municipios
        // encontrados con catastro desentralizado.
        List<Municipio> municipiosTemp = new ArrayList<Municipio>();
        List<Municipio> municipios = getCacheMunicipiosByDepartamento(codigoDepartamento);
        if (municipios != null && !municipios.isEmpty()) {
            for (Municipio m : municipios) {
                if (m.getCodigo() != null &&
                    !m.getCodigo().trim().isEmpty() &&
                    !codMunicipiosCatastroDescentralizado.contains(m
                        .getCodigo())) {

                    municipio = this.municipioDAO.getMunicipioByCodigo(m.getCodigo());
                    municipiosTemp.add(municipio);

                }
            }
        }
        return municipiosTemp;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IGeneralesLocal#obtenerDocumentoPorId(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Documento obtenerDocumentoPorId(Long idDocumento) {

        Documento answer = null;
        try {
            answer = this.documentoDAO.findById(idDocumento);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#obtenerDocumentoPorId: " + ex.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IGeneralesLocal#buscarDocumentoPorNumeroDocumento(String)
     */
    @Override
    public Documento buscarDocumentoPorNumeroDocumento(String numResolucion) {
        Documento answer = null;
        try {
            answer = this.documentoDAO
                .buscarDocumentoPorNumeroDocumento(numResolucion);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorNumeroDocumento: " +
                ex.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#obtenerDocumentoPorId(java.lang.Long)
     * @author felipe.cadena
     * @return
     */
    @Implement
    @Override
    public Documento guardarDocumentoAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento) {

        Documento answer = null;
        try {
            answer = this.documentoDAO.
                guardarDocumentoGestorDocumental(usuario, documentoTramiteDTO, documento);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#guardarDocumentoAlfresco: " + ex.getMessage());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#validarGeometriaPredios(List<String>, String, String)
     * @author javier.aponte
     */
    /*
     * @modified juanfelipe.garcia @modified pedro.garcia 30-10-2013 uso de enumeraciones y
     * constantes para evitar valores quemados 19-12-2013 uso de método que factoriza el manejo de
     * la excepción generada al validar la geometría de predios
     */
//TODO :: juanfelipe.garcia :: qué y cuándo modificó :: pedro.garcia
    /*
     * @modified andres.eslava::25-01-2016::Se modifica generacion de inconsistencias por migracion
     * arcgis 10.3.1
     */
    @Override
    //REVIEW :: javier.aponte :: debería devolver algo diferente de null porque si ocurre una excepción el resultado es null, pero no necesariamente porque no hay error en las validaciones :: pedro.garcia    
    public String revalidarGeometriaPredios(Tramite tramite, UsuarioDTO usuario) {

        String resultado = "";

        try {
            StringBuilder identificadorPredios = new StringBuilder();

            List<TramiteDetallePredio> tdp = this.tramiteService.getTramiteDetallePredioByTramiteId(
                tramite.getId());

            FichaMatriz fichaMatriz;
            FichaMatrizPredio fichaMatrizPredio;

            if (tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz() || tramite.
                isTerceraMasiva() || tramite.isQuintaMasivo()) {
                //felipe.cadena :: manejo de inconsistencias para tramites de desenglobe por etapas y rectificaciones de ficha matriz
                identificadorPredios.append(tramite.getPredio().getNumeroPredial()).append(",");;

                if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad())) {
                    for (TramiteDetallePredio tdpTemp : tdp) {
                        identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).append(
                            ",");
                    }
                }
                identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);
            } else if (tdp != null && !tdp.isEmpty()) {
                for (TramiteDetallePredio tdpTemp : tdp) {

                    if (!tramite.isSegunda() && EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                        tdpTemp.getPredio().getCondicionPropiedad())) {
                        fichaMatrizPredio = this.fichaMatrizPredioDAO.
                            obtenerFichaMatrizPredioPorNumeroPredial(tdpTemp.getPredio().
                                getNumeroPredial());
                        fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                        if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                            identificadorPredios.append(fichaMatriz.getPredio().getNumeroPredial()).
                                append(",");
                        }
                    } else if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tdpTemp.
                        getPredio().getCondicionPropiedad()) &&
                        !EPredioCondicionPropiedad.CP_6.getCodigo().equals(tdpTemp.getPredio().
                            getCondicionPropiedad())) {
                        identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).append(
                            ",");
                    }
                }
                identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);

            } else if (tramite.getPredio() != null) {
                if (!tramite.isSegunda() && EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                    tramite.getPredio().getCondicionPropiedad())) {
                    fichaMatrizPredio = this.fichaMatrizPredioDAO.
                        obtenerFichaMatrizPredioPorNumeroPredial(tramite.getPredio().
                            getNumeroPredial());
                    fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                    if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                        identificadorPredios.append(fichaMatriz.getPredio().getNumeroPredial());
                    }
                } else if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad()) &&
                    !EPredioCondicionPropiedad.CP_6.getCodigo().equals(tramite.getPredio().
                        getCondicionPropiedad())) {
                    identificadorPredios.append(tramite.getPredio().getNumeroPredial());
                }
            }

//            HashMap<String, String>  erroresIngresados = new HashMap<String, String>();
//            String predios[]=identificadorPredios.toString().split(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
//            boolean errores = false;
            List<TramiteInconsistencia> inconsistencias = this.
                procesarInconsistenciasGeograficasPrediosSync(identificadorPredios.toString(),
                    usuario, tramite, true, false);
//    		for (String predioNumeroPredial : predios){
//	            try{
//	                erroresIngresados = SigDAOv2.getInstance().validarInconsistencias(predioNumeroPredial, usuario);	              
//	    			this.guardarTramiteInconsistencia(tramite.getId(), erroresIngresados, predioNumeroPredial, usuario.getLogin());		
//	            }
//                catch (Exception e) {
//	    			LOGGER.error("Se encontró un error en la validación de geometría de predios...", e.getMessage(), e);                    
//                    //this.analizarExcepcionValidacionGeometriaPredios(e, predioNumeroPredial, usuario, tramite.getId(), false);     
//                    //errores = true;
//	    		}
//    		}

            if (inconsistencias != null && !inconsistencias.isEmpty()) {
                if (inconsistencias.size() == 1 && inconsistencias.get(0).getInconsistencia().
                    equals(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre())) {
                    resultado = null;

                }
                this.tramiteInconsistenciaDAO.updateMultiple(inconsistencias);
            }

//            if (!errores) {
//                resultado=null;
//                erroresIngresados.put(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getCodigoSig(),
//                    EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre());
//                this.guardarTramiteInconsistencia(tramite.getId(), erroresIngresados, predios[0], usuario.getLogin());
//            }
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la validación de la geometría de predios " + e);
            throw e;
        }
        return resultado;
    }

    /**
     * @see IGeneralesLocal#validarGeometriaPrediosAsincronico
     * @author felipe.cadena
     * @modified::andres.eslava::23/09/2015:: modificado para soporte a radicacion especial, se deja
     * TODO para modificacion casos radicaciones especial
     */
    @Override
    public String validarGeometriaPrediosAsincronico(Tramite tramite, UsuarioDTO usuario) {

        String resultado = "";

        try {
            StringBuilder identificadorPredios = new StringBuilder();

            List<TramiteDetallePredio> tdp = this.tramiteService.getTramiteDetallePredioByTramiteId(
                tramite.getId());

            FichaMatriz fichaMatriz;
            FichaMatrizPredio fichaMatrizPredio;

            if (tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz() ||
                tramite.isQuintaMasivo() || tramite.isTerceraMasiva()) {
                //felipe.cadena :: manejo de inconsistencias para tramites de desenglobe por etapas y rectificaciones de ficha matriz
                identificadorPredios.append(tramite.getPredio().getNumeroPredial()).append(",");;

                if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad())) {
                    for (TramiteDetallePredio tdpTemp : tdp) {
                        identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).append(
                            ",");
                    }
                }
                //lorena.salamanca :: 25-11-2015 :: Se agrega esta validacion porque cuando es PH se debe validar el terreno de la ficha matriz
//                else if (EPredioCondicionPropiedad.CP_9.getCodigo().equals(tramite.getPredio().getCondicionPropiedad())){
//                    identificadorPredios.append(tramite.getPredio().getNumeroPredial()).append(",");
//                }
                identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);
            } else if (tdp != null && !tdp.isEmpty()) {
                for (TramiteDetallePredio tdpTemp : tdp) {

                    if (!tramite.isSegunda() && (EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                        tdpTemp.getPredio().getCondicionPropiedad()))) {
                        fichaMatrizPredio = this.fichaMatrizPredioDAO.
                            obtenerFichaMatrizPredioPorNumeroPredial(tdpTemp.getPredio().
                                getNumeroPredial());
                        fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                        if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                            identificadorPredios.append(fichaMatriz.getPredio().getNumeroPredial()).
                                append(",");
                        }
                    } else if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tdpTemp.
                        getPredio().getCondicionPropiedad()) &&
                        !EPredioCondicionPropiedad.CP_6.getCodigo().equals(tdpTemp.getPredio().
                            getCondicionPropiedad())) {
                        identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).append(
                            ",");
                    }
                }
                identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);

            } else if (tramite.getPredio() != null) {
                if (!tramite.isSegunda() && EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                    tramite.getPredio().getCondicionPropiedad())) {
                    fichaMatrizPredio = this.fichaMatrizPredioDAO.
                        obtenerFichaMatrizPredioPorNumeroPredial(tramite.getPredio().
                            getNumeroPredial());
                    fichaMatriz = fichaMatrizPredio.getFichaMatriz();
                    if (fichaMatriz != null && fichaMatriz.getPredio() != null) {
                        identificadorPredios.append(fichaMatriz.getPredio().getNumeroPredial());
                    }
                } else if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tramite.getPredio().
                    getCondicionPropiedad()) &&
                    !EPredioCondicionPropiedad.CP_6.getCodigo().equals(tramite.getPredio().
                        getCondicionPropiedad())) {
                    identificadorPredios.append(tramite.getPredio().getNumeroPredial());
                }
            }

            String predios[] = identificadorPredios.toString().split(
                Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
            String numerosPrediales = "";

            for (String predioNumeroPredial : predios) {
                numerosPrediales += predioNumeroPredial + ",";
            }

            numerosPrediales = numerosPrediales.substring(0, numerosPrediales.lastIndexOf(
                Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD));

            try {
                // lorena.salamanca :: 24-09-2015 :: Se agrega lógica para
                // enviar el parámetro al servicio de validación de
                // inconsistencias cuando el trámite es QUINTA_MASIVO
                if (tramite.isQuintaMasivo()) {
                    SigDAOv2.getInstance().validarInconsistenciasAsync(productoCatastralJobDAO,
                        numerosPrediales, usuario, tramite, tramite.getClaseMutacion(),
                        ETramiteRadicacionEspecial.QUINTA_MASIVO.getCodigo());
                } else {
                    SigDAOv2.getInstance().validarInconsistenciasAsync(productoCatastralJobDAO,
                        numerosPrediales, usuario, tramite, tramite.getClaseMutacion(), null);
                }

            } catch (Exception e) {
                LOGGER.error("Se encontró un error en la validación de geometría de predios...",
                    e.getMessage(), e);
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la validación de la geometría de predios " + e);
            throw e;
        }
        return resultado;
    }
    /**
     * @see IGeneralesLocal#validarInconsistenciasActualizacion
     * @author hector.arias
     * 
     */
    @Override
    public String validarInconsistenciasActualizacion(String departamentoSeleccionado, String municipioSeleccionado, String ubicacionArchivo, String validacionZonas, String validacionPredios, UsuarioDTO usuario) {
        String resultado = "";
        
        SigDAOv2.getInstance().validarInconsistenciasActualizacion(productoCatastralJobDAO, departamentoSeleccionado, municipioSeleccionado, 
                ubicacionArchivo, validacionZonas, validacionPredios, usuario);
        
        return resultado;
    }
    
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de guardar el registro en trámite inconsistencia
     *
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void guardarTramiteInconsistencia(Long idTramite, HashMap<String, String> errores,
        String identificadorPredio, String userLogin) {

        List<TramiteInconsistencia> tramitesInconsistencia = this.tramiteInconsistenciaDAO.
            buscarPorIdTramite(idTramite);

        if ((tramitesInconsistencia == null || tramitesInconsistencia.isEmpty()) &&
            identificadorPredio != null) {

            Iterator it = errores.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry) it.next();
                System.out.println(e.getKey() + " " + e.getValue());
                TramiteInconsistencia ti = new TramiteInconsistencia();
                ti.setFechaLog(new Date());
                ti.setUsuarioLog(userLogin);
                Tramite tramite = this.tramiteDAO.findById(idTramite);

                //Si el identificador de predio es null, es porque el predio no tiene inconsistencias geográficas
                //en ese caso, no se asigna un tipo
                if (identificadorPredio != null) {
                    ti.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
                }
                ti.setTramite(tramite);
                ti.setDepurada(ESiNo.NO.getCodigo());
                ti.setInconsistencia(e.getValue().toString());
                ti.setNumeroPredial(identificadorPredio);
                this.tramiteInconsistenciaDAO.persist(ti);
            }
        }

    }

//--------------------------------------------------------------------------------------------------    
    /**
     * @see IGeneralesLocal#validarGeometriaPrediosTramite(Tramite,List,UsuarioDTO)
     * @author felipe.cadena
     */
    /*
     * @modified andres.eslava::25-01-2016::agrega soporte arcgis10.3.1
     */
    @Override
    public List<TramiteInconsistencia> validarGeometriaPrediosTramite(
        Tramite tramite, List<Predio> predios, UsuarioDTO usuario) {

        StringBuilder identificadorPredios = new StringBuilder();
        List<TramiteInconsistencia> inconsistencias = new ArrayList<TramiteInconsistencia>();

        try {
            if (predios == null) {
                List<TramiteDetallePredio> tdp = this.tramiteService.
                    getTramiteDetallePredioByTramiteId(tramite.getId());

                if (tdp != null && !tdp.isEmpty()) {
                    for (TramiteDetallePredio tdpTemp : tdp) {
                        if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tdpTemp.getPredio().
                            getCondicionPropiedad())) {
                            identificadorPredios.append(tdpTemp.getPredio().getNumeroPredial()).
                                append(",");
                        }
                    }
                    identificadorPredios.deleteCharAt(identificadorPredios.length() - 1);

                } else if (tramite.getPredio() != null) {
                    if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(tramite.getPredio().
                        getCondicionPropiedad())) {
                        identificadorPredios.append(tramite.getPredio().getNumeroPredial());
                    }
                }
            } else {
                for (Predio predio : predios) {
                    if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(predio.
                        getCondicionPropiedad())) {
                        identificadorPredios.append(predio.getNumeroPredial()).append(",");
                    }
                }
            }

            //String prediosStr[] = identificadorPredios.toString().split(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
            inconsistencias = this.procesarInconsistenciasGeograficasPrediosSync(
                identificadorPredios.toString(), usuario, tramite, false, false);

            this.tramiteInconsistenciaDAO.updateMultiple(inconsistencias);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la validación de la geometría de predios " + e);
            throw e;
        }
        return inconsistencias;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#validarGeometriaPrediosTramite(Tramite,List,UsuarioDTO)
     * @author felipe.cadena
     */
    @Override
    public List<TramiteInconsistencia> validarGeometriaMejorasTramite(
        Tramite tramite, List<Predio> predios, UsuarioDTO usuario) {

        StringBuilder identificadorPredios = new StringBuilder();
        List<TramiteInconsistencia> inconsistencias = new ArrayList<TramiteInconsistencia>();
        try {
            for (Predio predio : predios) {
                identificadorPredios.append(predio.getNumeroPredial()).append(",");
            }
            inconsistencias = this.procesarInconsistenciasGeograficasPrediosSync(
                identificadorPredios.toString(), usuario, tramite, true, true);

            this.tramiteInconsistenciaDAO.updateMultiple(inconsistencias);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en la validación de la geometría de mejoras " + e);
            throw e;
        }
        return inconsistencias;
    }

    /**
     * Método para validar de nuevo la geometría de los predios
     *
     * @author javier.aponte
     */
    /*
     * @modified felipe.cadena :: 01-11-2013 :: se agregan validaciones para predios de mejoras
     * @modified pedro.garcia 03-12-2013 se revisa si es trámite es de cancelación de predio o de
     * rectificación por cancelación por doble inscripción para usar otro método para validar de
     * forma sincrónica
     */
    public String validarInconsistencias(Tramite tramite, UsuarioDTO usuario) {

        String resultado = "";
        tramite = this.tramiteDAO.findById(tramite.getId());
        try {
            if (tramite.isTramiteMasivo()) {

                if (tramite.getPredio().getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END) ||
                    !tramite.getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END) ||
                    tramite.isSegundaEnglobe()) {
                    Parametro parametroMasivo = getCacheParametroPorNombre(
                        EParametro.EDITOR_UMBRAL_ASYNC.toString());
                    int numeroPredios = this.conservacionService.contarPrediosAsociadosATramite(
                        tramite.getId());

                    if (numeroPredios >= parametroMasivo.getValorNumero() || tramite.
                        isQuintaMasivo()) {
                        validarGeometriaPrediosAsincronico(tramite, usuario);
                        resultado =
                            "Se esta realizando la validación de la geometría. Esto puede tardar unos minutos";
                        return resultado;
                    }
                }
            }

            if (!tramite.isSegundaEnglobe()) {
                if (!tramite.isQuinta() || tramite.isQuintaMasivo()) {
                    if (tramite.getPredio().isMejora()) {
                        resultado = this.validarGeometriaMejoras(tramite, usuario);
                    } else {

                        //D: CC-NP-CO-195 se valida diferente si el trámites es de cancelación de predio o de
                        //  rectificación por cancelación por doble inscripción
                        if (tramite.isCancelacionPredio() ||
                            (tramite.isEsRectificacion() && tramite.isRectificacionCDI())) {
                            boolean validarCancelPredioS;
                            validarCancelPredioS = validarGeometriaPrediosTramCancelPredioS(tramite.
                                getId(), usuario);
                            if (!validarCancelPredioS) {
                                LOGGER.error(
                                    "Ocurrió un error al validar sincrónicamente la geometría de predios de un trámite de cancelación de predio o de rectificación por cancelación por doble inscripción");
                            }
                        } else {
                            resultado = revalidarGeometriaPredios(tramite, usuario);
                        }
                    }
                }
            } //N: si es de englobe
            else {
                int k = 0;
                int lenPredios = tramite.getPredios().size();
                for (Predio p : tramite.getPredios()) {
                    if (p.isMejora()) {
                        k++;
                    }
                }
                if (k == lenPredios) {
                    resultado = this.validarGeometriaMejoras(tramite, usuario);
                } else if (k > 0) {
                    resultado = this.validarGeometriaMejorasTerreno(tramite, usuario);
                } else {

                    resultado = revalidarGeometriaPredios(tramite, usuario);
                }
            }
            return resultado;
        } catch (Exception e) {
            return "Ocurrió un error al validar la geometría de predios GeneralesBean#validarInconsistencias";
        }
    }

    /**
     * Método para validar cuando los predios del tramite son de tipo mejora.
     *
     * @author felipe.cadena
     * @return
     */
    public String validarGeometriaMejoras(Tramite tramite, UsuarioDTO usuario) {
        try {

            List<Predio> prediosTerreno = new ArrayList<Predio>();
            List<String> numerosTerreno = new ArrayList<String>();

            //obtener numeros de terreno de las mejoras
            for (Predio predio : tramite.getPredios()) {
                String numeroTerreno = predio.getNumeroPredial().substring(0, 21);
                numerosTerreno.add(numeroTerreno + "000000000");
            }

            numerosTerreno = this.eliminarNumerosPredialesDuplicados(numerosTerreno);

            prediosTerreno = this.conservacionService.obtenerPrediosPorNumerosPrediales(
                numerosTerreno);

            if (prediosTerreno.isEmpty()) {
                // this.addMensajeError("Las mejoras no tienen un predio de terreno asociado");
                this.crearInconsistenciaTerreno(tramite, usuario);
                return "";
            }

            List<TramiteInconsistencia> resultadoTerreno = validarGeometriaPrediosTramite(tramite,
                prediosTerreno, usuario);

            List<TramiteInconsistencia> resultadoMejoras = validarGeometriaMejorasTramite(tramite,
                tramite.getPredios(), usuario);

            //Se descarta la inconsistencia de control 
            if (resultadoMejoras.size() == 1 && resultadoMejoras.get(0).getInconsistencia().
                equals(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre())) {
                resultadoMejoras = new ArrayList<TramiteInconsistencia>();
            }

            if (resultadoMejoras.isEmpty() && resultadoTerreno.isEmpty()) {
                return null;
            } else {
                return "";
            }
        } catch (Exception e) {
            return "Ocurrió un error GeneralesBean#validarGeometriaMejoras";
        }
    }

    /**
     * Método para validar cuando los predios del tramite son de tipo mejora y de tipo terreno.
     *
     * @author felipe.cadena
     * @return
     */
    public String validarGeometriaMejorasTerreno(Tramite tramite, UsuarioDTO usuario) {
        try {

            List<Predio> prediosMejora = new ArrayList<Predio>();

            //obtener predios mejora
            for (Predio predio : tramite.getPredios()) {
                if (predio.isMejora()) {
                    prediosMejora.add(predio);
                }
            }

            //las posibles inconsistencias detectadas se almacenan en la BD
            String resultadoTerreno = revalidarGeometriaPredios(tramite, usuario);

            List<TramiteInconsistencia> resultadoMejoras = validarGeometriaMejorasTramite(tramite,
                prediosMejora, usuario);

            if (resultadoMejoras.isEmpty() && resultadoTerreno == null) {
                return null;
            } else {
                return "";
            }
        } catch (Exception e) {
            return "Ocurrió un error GeneralesBean#validarGeometriaMejorasTerreno";
        }
    }

    /**
     * Mètodo para eliminar los números prediales repetidos que puedan existir en una lista
     *
     * @author felipe.cadena
     * @param numeros
     * @return
     */
    public List<String> eliminarNumerosPredialesDuplicados(List<String> numeros) {
        try {

            List<String> resultado = new ArrayList<String>();

            if (numeros.size() < 2) {
                return numeros;
            }
            String numero = numeros.get(0);
            resultado.add(numero);

            for (int i = 1; i < numeros.size(); i++) {
                String numAux = numeros.get(i);
                if (!resultado.contains(numAux)) {
                    resultado.add(numAux);
                }
            }

            return resultado;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Metodo para crear una inconsistencia cuando una mejora no tiene predio de terreno
     * correspondiente
     *
     * @author felipe.cadena
     */
    public TramiteInconsistencia crearInconsistenciaTerreno(Tramite tramite, UsuarioDTO usuario) {

        TramiteInconsistencia tic = null;
        try {
            tic = new TramiteInconsistencia();
            tic.setFechaLog(new Date());
            tic.setUsuarioLog(usuario.getLogin());
            tic.setTramite(tramite);
            tic.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
            tic.setInconsistencia(EInconsistenciasGeograficas.MEJORA_SIN_TERRENO.getNombre());
            tic.setDepurada(ESiNo.NO.getCodigo());
            tic.setNumeroPredial(tramite.getPredio().getNumeroPredial());

            tic = this.tramiteService.guardarActualizarTramiteInconsistencia(tic);
        } catch (Exception e) {
            return null;
        }
        return tic;

    }

    //--------------------------------------------------------------------------------------------------	
    /**
     * @see IGeneralesLocal#crearZipAPartirDeRutaDeDocumentos(List)
     * @author javier.aponte
     */
    @Override
    public String crearZipAPartirDeRutaDeDocumentos(List<String> listadoRutasArchivos) {
        return ZipUtil.zipFiles(listadoRutasArchivos);
    }

    /**
     * @see IGeneralesLocal#calcularDigitoVerificacion(String)
     * @author felipe.cadena
     */
    @Override
    public int calcularDigitoVerificacion(String nit) {

        int[] primos = Constantes.NUMEROS_DIGITO_VERIFICACION;
        int[] digitos = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int total = 0;
        int digitoCalculado = 0;

        StringBuilder nitSB = new StringBuilder(nit);
        nitSB = nitSB.reverse();

        for (int i = 0; i < nitSB.length(); i++) {
            digitos[i] = Integer.valueOf(nitSB.charAt(i) - 48);
        }

        for (int i = 0; i < primos.length; i++) {
            total += primos[i] * digitos[i];
        }

        total = total % 11;

        if (total <= 1) {
            digitoCalculado = total;
        } else {
            digitoCalculado = 11 - total;
        }

        return digitoCalculado;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IGenerales#getCirculoRegistralMunicipioByCodigoCirculoRegistral(String)
     * @author javier.aponte
     */
    @Override
    public List<CirculoRegistralMunicipio> getCirculoRegistralMunicipioByCodigoCirculoRegistral(
        String codigoCirculoRegistral) {
        return circuloRegistralMunicipioService.findByCodigoCirculoRegistral(codigoCirculoRegistral);
    }
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
    /**
     * Metodo que retorna los departamentos de una UOC
     *
     * @param codigoUOC
     * @return
     */
    @Override
    public List<Departamento> getCacheDepartamentosByCodigoUOC(String codigoUoc) {
        return this.departamentoDAO.buscarDepartamentosByUOC(codigoUoc);
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IGeneralesLocal#buscarDocumentoPorSolicitudIdAndTipoDocumentoId(Long, Long)
     * @author javier.aponte
     */
    @Override
    @Implement
    public SolicitudDocumento buscarDocumentoPorSolicitudIdAndTipoDocumentoId(
        Long idSolicitud, Long idTipoDocumento) {

        SolicitudDocumento result = null;

        try {

            result = this.solicitudDocumentoDAO
                .buscarDocumentoPorSolicitudIdAndTipoDocumentoId(idSolicitud, idTipoDocumento);

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Error en ConservacionBean#buscarDocumentoPorSolicitudIdAndTipoDocumentoId: " +
                ex.getMensaje());
        }

        return result;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#obtenerLogAccesoPorUsuario(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    @Implement
    public LogAcceso obtenerLogAccesoPorUsuario(String usuario) {

        LogAcceso result = null;

        try {

            result = this.logAccesoDAO.obtenerPorLogin(usuario);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerLogAccesoPorUsuario: " +
                ex.getMensaje());
        }

        return result;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#registrarAcceso(co.gov.igac.snc.persistence.entity.generales.LogAcceso)
     * @author felipe.cadena
     */
    @Override
    @Implement
    public Long registrarAcceso(LogAcceso logAcceso) {

        Long result = null;
        LogAccion accionLogin = new LogAccion();

        try {

            result = this.procedimientoService.registrarAccesoUsuario(logAcceso);

            accionLogin.setEvento(ELogAccionEvento.LOGIN.toString());
            accionLogin.setLogAccesoId(result);

            this.registrarlogAccion(accionLogin);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#registrarAcceso: " +
                ex.getMensaje());
        }

        return result;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see
     * IGeneralesLocal#registrarlogAccion(co.gov.igac.snc.persistence.entity.generales.LogAccion)
     * @author felipe.cadena
     */
    @Override
    @Implement
    public void registrarlogAccion(LogAccion logAccion) {

        try {

            this.procedimientoService.registrarAccionUsuario(logAccion);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#registrarAcceso: " +
                ex.getMensaje());
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     *
     * @see IGeneralesLocal#registrarLogout(co.gov.igac.snc.persistence.entity.generales.LogAcceso)
     * @author felipe.cadena
     */
    @Override
    @Implement
    public void registrarLogout(String usuario, String motivo) {

        LogAcceso accesoPrevio;
        LogAccion accionLogout = new LogAccion();

        try {
            accesoPrevio = this.obtenerLogAccesoPorUsuario(usuario);
            //Se evita cerrar un acceso previamente cerrado, este caso solo se 
            //presenta cuando se permiten varias sesiones del mismo usuario, ya que solo uno cierra
            //la sesion en la bd, para los demas ya esta cerrada. 
            if (accesoPrevio == null) {
                return;
            }
            accesoPrevio.setEstadoSesion(EEstadoSesion.CANCELADA.toString());
            accesoPrevio.setFechaFinSesion(new Date());

            this.logAccesoDAO.update(accesoPrevio);

            accionLogout.setEvento(ELogAccionEvento.LOGOUT.toString());
            accionLogout.setEventoMensaje(motivo);
            accionLogout.setLogAccesoId(accesoPrevio.getId());

            this.registrarlogAccion(accionLogout);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#actualizarRegistroAcceso: " +
                ex.getMensaje());
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     *
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#verificarEjecucionJobsPendientesEnSNC()
     */
    @Override
    public void verificarEjecucionJobsPendientesEnSNC() {
        LOGGER.debug("verificarEjecucionJobsPendientesEnSNC");
        List<ProductoCatastralJob> jobs = this.productoCatastralJobDAO.obtenerJobsPendientesEnSNC();
        LOGGER.debug("Número de Jobs por sincronizar: " + jobs.size());
        for (ProductoCatastralJob job : jobs) {
            verificarEjecucionJobPendienteEnSNC(job.getId());
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsPendientesEnSNC(java.lang.Long)
     */
    /*
     * @modified pedro.garcia 08-11-2013 manejo de excepciones @modified felipe.cadena 21-03-2017 Se
     * consultan los jobs desde un procedimiento almacenado
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsPendientesEnSNC(Long cantidad) {
        List<ProductoCatastralJob> answer = null;
        try {
            answer = this.procedimientoService.consultarJobsPendientesPorProcesar(
                cantidad.intValue(),
                ProductoCatastralJob.AGS_TERMINADO,
                ProductoCatastralJob.SNC_EN_EJECUCION);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerJobsPendientesEnSNC: " + ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     *
     * @see
     * co.gov.igac.snc.fachadas.IGeneralesLocal#verificarEjecucionJobPendienteEnSNC(java.lang.Long)
     * @autor juan.mendez
     *
     */
    /*
     * @modified andres.eslava::13/Jul/2015::Refs 13127,13126 recuperacion jobs terminados de zonas
     * e inconsistencias::
     */
    @Override
    public void verificarEjecucionJobPendienteEnSNC(Long idJob) {
        LOGGER.debug("verificarEjecucionJobPendienteEnSNC con id " + idJob.toString());
        ProductoCatastralJob job = null;
        //versión 1.1.4
        try {
            ///////////////////////////////////////////////////////////////////////////////////////////////
            job = this.productoCatastralJobDAO.findById(idJob);
            LOGGER.debug("job:" + job);
            boolean resultadoOk = false;
            ///////////////////////////////////////////////////////////////////////////////////////////////
            if (ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION.equals(job.getTipoProducto())) {
                resultadoOk = this.conservacionService.guardarDocumentoReplica(job);
            } else if (ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS.equals(job.getTipoProducto())) {
                //felipe.cadena::#16146::24-02-2014::Ahora existe un solo job para la generacion de replicas,
                //por eso se debe verificar de que proceso asincronico fue enviado
                Map<String, String> paramsCodigoJob = Utilidades.obtenerParametrosXMLJob(job.
                    getCodigo(), "/SigJob/parameters/parameter");

                Long tramiteId = Long.valueOf(paramsCodigoJob.get("numeroTramite"));

                Tramite tramite = this.tramiteDAO.findById(tramiteId);

                if (tramite.getTarea().equals(EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.
                    toString())) {
                    resultadoOk = this.conservacionService.guardarDocumentoReplicaConsulta(job);
                } else {
                    resultadoOk = this.conservacionService.guardarDocumentoReplica(job);
                }
            } else if (ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION.equals(job.getTipoProducto())) {
                resultadoOk = this.conservacionService.aplicarCambiosDeProyeccionAP2(job);
            } else if (ProductoCatastralJob.SIG_JOB_IMAGEN_FICHA_PREDIAL_DIGITAL.equals(job.
                getTipoProducto())) {
                if (job.getProductoCatastralDetalleId() == null) {
                    resultadoOk = this.aplicarCambios.finalizarFichaPredial(job);
                } else {
                    //es producto
                    resultadoOk = this.procesarFichaPredialDigitalProductos(job);
                }
            } else if (ProductoCatastralJob.SIG_JOB_IMAGEN_CARTA_CATASTRAL_URBANA.equals(job.
                getTipoProducto())) {
                if (job.getProductoCatastralDetalleId() == null) {
                    resultadoOk = this.aplicarCambios.finalizarCartaCatastral(job);
                } else {
                    //es producto
                    resultadoOk = this.procesarCartaCatastralUrbanaProductos(job);
                }
            } else if (ProductoCatastralJob.SIG_JOB_ELIMINAR_VERSION_SDE.equals(job.
                getTipoProducto())) {
                LOGGER.debug("En el caso de este job no se hace nada");
                resultadoOk = true;
            } else if (ProductoCatastralJob.SIG_JOB_ACTUALIZAR_COLINDANTES.equals(job.
                getTipoProducto())) {
                UsuarioDTO usuario = LdapDAO.searchUser(job.getUsuarioLog());
                this.aplicarCambios.verificaJobActualizarColindancia(job, usuario);
                resultadoOk = true;
            } else if (ProductoCatastralJob.SIG_JOB_IMAGEN_CERTIFICADO_PLANO_PREDIAL.equals(job.
                getTipoProducto())) {
                resultadoOk = this.procesarCertificadoPlanoPredialCatastralProductos(job);
            } else if (ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS.equals(job.getTipoProducto())) {
                resultadoOk = conservacionService.actualizacionZonasHomogeneasAsync(job);
            } else if (ProductoCatastralJob.SIG_JOB_VALIDAR_INCONSISTENCIAS.equals(job.
                getTipoProducto())) {
                //TODO juanFelipe.Garcia::Implementacion web para validar inconsistencias.    
                resultadoOk = this.guardarInconsistenciasAsincronicas(job);
            } else if (ProductoCatastralJob.SIG_JOB_IMAGEN_PREDIAL.equals(job.
                getTipoProducto())) {
                LOGGER.debug("Begin    ----- ");
                resultadoOk = this.generarDocumentoImagenPredialTerminado(job.getId());
                LOGGER.debug("End     ----- ");
            } else if (ProductoCatastralJob.SIG_JOB_CARGUE_ACTUALIZACION.equals(job.getTipoProducto())) {
                this.obtenerParametrosDeProductoCatastralJobActualizacion(job);
                resultadoOk = true;
            } else {
                LOGGER.warn("No se encuentra implementada la ejecución para el tipo:" + job.
                    getTipoProducto());
                LOGGER.warn("El job se marcará como terminado con error en SNC ");
                resultadoOk = false;
            }
            if (resultadoOk) {
                this.productoCatastralJobDAO.actualizarEstado(job,
                    ProductoCatastralJob.SNC_TERMINADO);
            } else {
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.SNC_ERROR);
            }
        } catch (Exception e) {
            LOGGER.error("Snc error en job id " + idJob + " " + e.getMessage(), e);
            if (job != null) {
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.SNC_ERROR);
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#reenviarJobGeograficoEsperando(java.lang.Long)
     * @modified juan.mendez 2012-04-03 Se cambió la firma de los métodos de SigDao. Ya no requieren
     * como parámetro el token.
     */
    @Override
    public void reenviarJobGeograficoEsperando(Long idJob) {
        LOGGER.debug("reenviarJobGeograficoEsperando con id " + idJob.toString());
        ProductoCatastralJob job = null;
        try {
            ///////////////////////////////////////////////////////////////////////////////////////////////
            job = this.productoCatastralJobDAO.findById(idJob);
            LOGGER.debug("job:" + job);

            //Actualiza el job con la fecha del nuevo intento de ejecución
            job.setFechaActualizadoLog(new Date());
            job.setNumeroEnvio(job.getNumeroEnvio()+1);
            this.productoCatastralJobDAO.update(job);
            UsuarioDTO usuario = LdapDAO.searchUser(job.getUsuarioLog());
            //Reintentar generar datos para edición
            if (ProductoCatastralJob.SIG_JOB_GENERAR_DATOS_EDICION.equals(job.getTipoProducto())) {
                SigDAOv2.getInstance().
                    exportarDatosAsync(this.productoCatastralJobDAO, job, usuario);
            } //Reintentar aplicar cambios geograficos
            else if (ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION.equals(job.getTipoProducto())) {
                SigDAOv2.getInstance().aplicarCambiosAsync(this.productoCatastralJobDAO, job,
                    usuario);
            } //Reintento Generar replica de consulta
            else if (ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS.equals(job.getTipoProducto())) {
                SigDAOv2.getInstance().
                    exportarDatosAsync(this.productoCatastralJobDAO, job, usuario);
            } // Reintento de generar imagen del predio
            else if (ProductoCatastralJob.SIG_JOB_IMAGEN_PREDIAL.equals(job.getTipoProducto())) {
                SigDAOv2.getInstance().reSendGenerarImagenPredioAsync(job, usuario);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            if (job != null) {
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.SNC_ERROR);
            }
        }
    }
//--------------------------------------------------------------------------------------------------	

    @Override
    public Boolean generarDocumentoImagenPredialTerminado(Long idJob) {
        LOGGER.debug("generarDocumentoImagenPredialTerminado con id " + idJob.toString());
        ProductoCatastralJob job = null;
        try {
            job = this.productoCatastralJobDAO.findById(idJob);
            //UsuarioDTO usuario = LdapDAO.searchUser(job.getUsuarioLog());
            //String resultadoJob = job.obtenerResultadosParaProductosCatatrales();

            UsuarioDTO usuario = this.getCacheUsuario(job.getUsuarioLog());

            List<ProductoCatastralResultado> resultadosIFP = job.
                obtenerResultadosParaProductosCatatrales();
            if (resultadosIFP == null || resultadosIFP.isEmpty()) {
                String message = "El servidor de productos no retornó resultados.";
                LOGGER.debug("El servidor de productos no retornó resultados.");
                throw SncBusinessServiceExceptions.EXCEPCION_100018.getExcepcion(LOGGER, null,
                    message);
            }

            List<String> numerosPrediales = job.obtenerParametroNumerosPrediales();
            LOGGER.debug("numerosPrediales: " + numerosPrediales);
            for (String numeroPredial : numerosPrediales) {
                String publicUrlImgPredio = null;

                for (ProductoCatastralResultado resultado : resultadosIFP) {
                    if (resultado.getCodigo().equals(numeroPredial)) {
                        if (resultado.getTipoProducto().equals(ProductoCatastralJob.TIPO_IMAGEN_CIP)) {

                            Documento doc = this.documentoDAO.
                                buscarDocumentoPorNumeroPredialAndTipoDocumento(
                                    numeroPredial, Constantes.TIPO_IMG_PREDIAL);
                            if (doc == null) {
                                doc = new Documento();
                                Predio predio = this.conservacionService.getPredioByNumeroPredial(
                                    numeroPredial);

                                doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
                                doc.setBloqueado(ESiNo.NO.getCodigo());
                                doc.setUsuarioCreador(usuario.getLogin());
                                doc.setPredioId(predio.getId());
                                doc.setNumeroPredial(predio.getNumeroPredial());
                                //doc.setTramiteId(this.tramite.getId());
                                doc.setUsuarioCreador(usuario.getLogin());
                                doc.setFechaRadicacion(new Date(System.currentTimeMillis()));
                                doc.setUsuarioLog(usuario.getLogin());
                                doc.setFechaLog(new Date(System.currentTimeMillis()));
                                TipoDocumento td = new TipoDocumento();
                                td.setId(Constantes.TIPO_IMG_PREDIAL);
                                doc.setTipoDocumento(td);
                                doc.setArchivo(resultado.getIdArchivo());
                            } else {
                                doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
                                doc.setArchivo(resultado.getIdArchivo());
                            }
                            this.documentoDAO.persist(doc);
                        }
                        if (resultado.getTipoProducto().equals(ProductoCatastralJob.TIPO_IMAGEN_CIP_DET)) {
                            Documento docDet = this.documentoDAO.
                                buscarDocumentoPorNumeroPredialAndTipoDocumento(
                                    numeroPredial, Constantes.TIPO_IMG_PREDIAL_DETALLE);
                            if (docDet == null) {
                                docDet = new Documento();
                                Predio predio = this.conservacionService.getPredioByNumeroPredial(
                                    numeroPredial);

                                docDet.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
                                docDet.setBloqueado(ESiNo.NO.getCodigo());
                                docDet.setUsuarioCreador(usuario.getLogin());
                                docDet.setPredioId(predio.getId());
                                docDet.setNumeroPredial(predio.getNumeroPredial());
                                //docDet.setTramiteId(this.tramite.getId());
                                docDet.setUsuarioCreador(usuario.getLogin());
                                docDet.setFechaRadicacion(new Date(System.currentTimeMillis()));
                                docDet.setUsuarioLog(usuario.getLogin());
                                docDet.setFechaLog(new Date(System.currentTimeMillis()));
                                TipoDocumento td = new TipoDocumento();
                                td.setId(Constantes.TIPO_IMG_PREDIAL_DETALLE);
                                docDet.setTipoDocumento(td);
                                docDet.setArchivo(resultado.getIdArchivo());
                            } else {
                                docDet.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
                                docDet.setArchivo(resultado.getIdArchivo());
                            }
                            this.documentoDAO.persist(docDet);
                        }
                    }
                }

            }
            this.productoCatastralJobDAO.actualizar(job, ProductoCatastralJob.SNC_TERMINADO,null);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see
     * IGeneralesLocal#obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(Tramite)
     * @author javier.aponte
     */
    @Override
    public UsuarioDTO obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(
        Tramite tramite, UsuarioDTO usuarioWeb) {

        List<UsuarioDTO> usuarios = null;
        UsuarioDTO usuario = null;

        String estructuraOrg;

        if (tramite.getSolicitud().isViaGubernativa()) {
            if (tramite.getTipoTramite().equals(
                ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo()) ||
                tramite.getTipoTramite()
                    .equals(ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION
                        .getCodigo())) {
                estructuraOrg = this.getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    tramite.getPredio().getMunicipio().getCodigo());
                if (estructuraOrg == null) {
                    LOGGER.warn(
                        "No fué posible determinar la estructura organizacional destino para el municipio : " +
                        tramite.getPredio().getMunicipio().getCodigo());
                } else {
                    usuarios = this.tramiteService.buscarFuncionariosPorRolYTerritorial(
                        estructuraOrg, ERol.RESPONSABLE_CONSERVACION);
                }
            } // ETramiteTipoTramite.RECURSO_DE_QUEJA
            // ETramiteTipoTramite.RECURSO_DE_APELACION
            else {

                // TODO TEST para las OUC
                estructuraOrg = this.getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    tramite.getPredio().getMunicipio().getCodigo());
                if (estructuraOrg == null) {
                    LOGGER.warn(
                        "No fué posible determinar la estructura organizacional destino para el municipio : " +
                        tramite.getPredio().getMunicipio().getCodigo());
                } else {
                    usuarios = this.tramiteService
                        .buscarFuncionariosPorRolYTerritorial(
                            (usuarioWeb.isTerritorial() ? usuarioWeb.getDescripcionTerritorial() :
                            estructuraOrg), ERol.DIRECTOR_TERRITORIAL);
                }

            }

        } else {
            if (!tramite.isQuinta()) {
                estructuraOrg = this.
                    getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                        tramite.getPredio().getMunicipio().getCodigo());
                if (estructuraOrg == null) {
                    LOGGER.warn(
                        "No fué posible determinar la estructura organizacional destino para el municipio : " +
                        tramite.getPredio().getMunicipio().getCodigo());
                } else {
                    usuarios = this.tramiteService.
                        buscarFuncionariosPorRolYTerritorial(
                            estructuraOrg, ERol.RESPONSABLE_CONSERVACION);
                }

            } else {

                estructuraOrg = this.getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    tramite.getMunicipio().getCodigo());
                if (estructuraOrg == null) {
                    LOGGER.warn(
                        "No fué posible determinar la estructura organizacional destino para el municipio : " +
                        tramite.getMunicipio().getCodigo());
                } else {
                    usuarios = this.tramiteService
                        .buscarFuncionariosPorRolYTerritorial(
                            estructuraOrg, ERol.RESPONSABLE_CONSERVACION);
                }
            }

        } // fin de: no es vía gubernativa

        if (usuarios != null && !usuarios.isEmpty()) {
            usuario = usuarios.get(0);
        }

        return usuario;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see IGeneralesLocal#validarGeometriaPrediosTramCancelPredio(Long,
     * co.gov.igac.generales.dto.UsuarioDTO)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean validarGeometriaPrediosTramCancelPredioA(Long idTramite, UsuarioDTO usuario) {

        LOGGER.debug("En GeneralesBean#validarGeometriaPrediosTramCancelPredioA ...");

        boolean answer = false;
        String numeroPredial, numeroPredialPredio;
        Tramite tramite;

        try {
            tramite = this.tramiteDAO.findTramitePrediosSimplesByTramiteId(idTramite);
        } catch (ExcepcionSNC sncEx) {
            LOGGER.error(
                "Error en GeneralesBean#validarGeometriaPrediosTramCancelPredioA consultando" +
                " el predio con id " + idTramite.toString() + ": " + sncEx.getMensaje());
            return answer;
        }

        if (tramite.getPredio() != null) {
            boolean inicioProcesoValidacion;
            numeroPredial = tramite.getPredio().getNumeroPredial();

            //D: cuando la condición de propiedad es 5, se valida la geometría de la unidad de construcción
            // y del predio que resulta de reemplazar los dígitos desde el número de terreno hacia adelante por 0s
            if (tramite.getPredio().getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo())) {
                //D: se toma el número predial hasta la posición del campo predio y se completa con 0s
                numeroPredialPredio = numeroPredial.
                    substring(0, Constantes.NUMPREDIAL_PREDIO_INDICE);
                numeroPredialPredio = numeroPredialPredio.concat("000000000");
                inicioProcesoValidacion = this.integracionService.validarGeometriaPredios(
                    numeroPredialPredio, idTramite, usuario, tramite.getTipoTramite());
                if (inicioProcesoValidacion) {
                    inicioProcesoValidacion = this.integracionService.
                        validarGeometriaUnidadesConstruccionA(numeroPredialPredio, idTramite,
                            usuario);
                    answer = inicioProcesoValidacion ? true : false;
                }
            } else {
                inicioProcesoValidacion = this.integracionService.validarGeometriaPredios(
                    numeroPredial, idTramite, usuario, tramite.getTipoTramite());
                answer = inicioProcesoValidacion ? true : false;
            }
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerTodosGrupoProductos()
     * @author javier.aponte
     *
     */
    /*
     * @modified by leidy.gonzalez:: 22/01/2015 :: #11143 Se modifica el metodo que busca los grupos
     * de producto existentes ordenandolos alfabeticamente por el campo descripcion
     */
    @Override
    public List<GrupoProducto> obtenerTodosGrupoProductos() {
        List<GrupoProducto> answer = null;
        try {
            answer = this.grupoProductoDAO.obtenerTodosGrupoProductos();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerTodosGrupoProductos: " + ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#obtenerListaProductosPorGrupoId()
     * @author javier.aponte
     */
    @Implement
    @Override
    public List<Producto> obtenerListaProductosPorGrupoId(Long idGrupo) {

        List<Producto> answer = null;
        try {
            answer = this.productoDAO.obtenerListaProductosPorGrupoId(idGrupo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#obtenerProductoPorCodigo()
     * @author javier.barajas
     */
    @Implement
    @Override
    public Producto obtenerProductoPorCodigo(String codigo) {

        Producto answer = null;
        try {
            answer = this.productoDAO.obtenerProductoPorCodigo(codigo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see IGenerales#buscarProductosCatastralesPorSolicitudId(Long)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastral> buscarProductosCatastralesPorSolicitudId(Long solicitudId) {

        List<ProductoCatastral> answer = null;
        try {
            answer = this.productoCatastralDAO.buscarProductosCatastralesPorSolicitudId(solicitudId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * IGeneralesLocal#guardarActualizarProductoCatastralAsociadoASolicitud(co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral))
     * @author javier.aponte
     */
    @Override
    public ProductoCatastral guardarActualizarProductoCatastralAsociadoASolicitud(
        ProductoCatastral productoCatastral) {

        ProductoCatastral answer = null;
        try {
            answer = this.productoCatastralDAO.update(productoCatastral);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

//-------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#buscarGrupoProductoPorId(Long documentoId)
     * @author javier.aponte
     */
    @Override
    public GrupoProducto buscarGrupoProductoPorId(Long grupoProductoId) {

        GrupoProducto answer = null;
        try {
            answer = this.grupoProductoDAO.findById(grupoProductoId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * @see IGeneralesLocal#validarGeometriaPrediosTramCancelPredio(Long,
     * co.gov.igac.generales.dto.UsuarioDTO)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean validarGeometriaPrediosTramCancelPredioS(Long idTramite, UsuarioDTO usuario)
        throws Exception {

        LOGGER.debug("En GeneralesBean#validarGeometriaPrediosTramCancelPredioS ...");

        boolean answer = false;
        String numeroPredial, numeroPredialPredio;
        Tramite tramite;

        try {
            tramite = this.tramiteDAO.findTramitePrediosSimplesByTramiteId(idTramite);
        } catch (ExcepcionSNC sncEx) {
            LOGGER.error(
                "Error en GeneralesBean#validarGeometriaPrediosTramCancelPredio consultando" +
                " el predio con id " + idTramite.toString() + ": " + sncEx.getMensaje());
            return answer;
        }

        if (tramite.getPredio() != null) {
            numeroPredial = tramite.getPredio().getNumeroPredial();

            HashMap<String, String> erroresIngresados = new HashMap<String, String>();

            //D: cuando la condición de propiedad es 5, se valida la geometría de la unidad de construcción
            // y del predio que resulta de reemplazar los dígitos desde el número de terreno hacia adelante por 0s
            if (tramite.getPredio().getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo())) {
                //D: se toma el número predial hasta la posición del campo predio y se completa con 0s
                numeroPredialPredio = numeroPredial.
                    substring(0, Constantes.NUMPREDIAL_PREDIO_INDICE);
                numeroPredialPredio = numeroPredialPredio.concat("000000000");

                try {
                    erroresIngresados = SigDAOv2.getInstance().validarInconsistencias(
                        numeroPredialPredio, usuario);
                    erroresIngresados.put(EPredioInconsistenciaTipo.NO_INCONSISTENCIA.getCodigo(),
                        EPredioInconsistenciaTipo.NO_INCONSISTENCIA.getValor());
                    this.guardarTramiteInconsistencia(tramite.getId(), erroresIngresados,
                        numeroPredialPredio, usuario.getLogin());
                } catch (Exception e) {
                    throw new Exception(e);
                }

                try {
                    List<TramiteInconsistencia> inconsistencias = this.
                        procesarInconsistenciasGeograficasPrediosSync(
                            numeroPredial, usuario, tramite, false, true);
                    this.tramiteInconsistenciaDAO.updateMultiple(inconsistencias);

                } catch (Exception e) {
                    throw new Exception(e);

                }
                answer = true;
            } else {

                try {
                    List<TramiteInconsistencia> inconsistencias = this.
                        procesarInconsistenciasGeograficasPrediosSync(numeroPredial, usuario,
                            tramite, true, false);
//                    erroresIngresados.put(EPredioInconsistenciaTipo.NO_INCONSISTENCIA.getCodigo(), EPredioInconsistenciaTipo.NO_INCONSISTENCIA.getValor());
//                    this.guardarTramiteInconsistencia(tramite.getId(), erroresIngresados, numeroPredial, usuario.getLogin());
                    this.tramiteInconsistenciaDAO.updateMultiple(inconsistencias);
                } catch (Exception e) {
                    throw new Exception(e);
                }
                answer = true;
            }

        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsGeograficosEsperando(int,int)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosEsperando(int horas, int cantidadJobs,
        int numeroReintentos) {
        return this.productoCatastralJobDAO.obtenerJobsGeograficosEsperando(horas, cantidadJobs,
            numeroReintentos);
    }

    /**
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsGeograficosError(int,int)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosError(int horas, int cantidadJobs,
        int numeroReintentos) {
        return this.productoCatastralJobDAO.obtenerJobsGeograficosError(horas, cantidadJobs,
            numeroReintentos);
    }

    /**
     * @see
     * co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsGeograficosImagenPredialTerminado(int,int)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsGeograficosImagenPredialTerminado(int horas,
        int cantidadJobs, int numeroReintentos) {
        return this.productoCatastralJobDAO.obtenerJobsGeograficosImagenPredialTerminado(horas,
            cantidadJobs, numeroReintentos);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * IGeneralesLocal#guardarProductoCatastralDetalle(co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle))
     * @author javier.aponte
     */
    @Override
    public ProductoCatastralDetalle guardarProductoCatastralDetalle(
        ProductoCatastralDetalle productoCatastralDetalle) {

        ProductoCatastralDetalle answer = null;
        try {
            answer = this.productoCatastralDetalleDAO.update(productoCatastralDetalle);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    //--------------------------------------------------------------------------------------------------	
    /**
     * @see IGenerales#buscarProductosCatastralesDetallePorProductoCatastralId(Long)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastralDetalle> buscarProductosCatastralesDetallePorProductoCatastralId(
        Long productoCatastralId) {

        List<ProductoCatastralDetalle> answer = null;
        try {
            answer = this.productoCatastralDetalleDAO.
                buscarProductosCatastralesDetallePorProductoCatastralId(productoCatastralId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see ITramiteLocal#verificarTramitesConRegistroTramiteDepuracion(List<Long>)
     * @author felipe.cadena
     */
    @Override
    public MunicipioComplemento obtenerMunicipioComplementoPorCodigo(String codigo) {

        MunicipioComplemento resultado = null;
        try {
            resultado = this.municipioComplementoDAO.findById(codigo);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return resultado;
    }

    /**
     * @see IGeneralesLocal#guardarProductoGeneradoEnGestorDocumental(String, String)
     * @author javier.aponte
     */
    @Override
    public String guardarProductoGeneradoEnGestorDocumental(String path,
        String codigoEstructuraOrganizacional) {
        String rutaWeb = "";
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            rutaWeb = service.cargarDocumentoWorkspaceProducto(path, codigoEstructuraOrganizacional);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#guardarProductoGeneradoEnGestorDocumental: " + ex.
                getMensaje());
        }
        return rutaWeb;
    }

    /**
     * @see
     * IGeneralesLocal#guardarActualizarProductoCatastralError(co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralError))
     * @author javier.aponte
     */
    @Override
    public ProductoCatastralError guardarActualizarProductoCatastralError(
        ProductoCatastralError productoCatastralError) {
        //versión 1.1.4
        ProductoCatastralError answer = null;
        try {
            answer = this.productoCatastralErrorDAO.update(productoCatastralError);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    /**
     * @see IGeneralesLocal#obtenerPorcentajeIncremento(Date, Long)
     * @author javier.aponte
     */
    @Override
    public Double obtenerPorcentajeIncremento(Date vigencia, Long idPredio) {

        Double result = null;
        try {
            result = QueryNativoDAO.obtenerPorcentajeIncremento(this.actualizacionDocumentoDAO.
                getEntityManager(), vigencia, idPredio);
        } catch (Exception e) {
            LOGGER.error("Error procedimiento almacenado");
        }

        return result;
    }

    //versión 1.1.4
    //--------------------------------------------------------------------------------------------------	
    /**
     * Para los niveles de transaccionalidad ver Transaction Annotations
     * http://tomee.apache.org/transaction-annotations.html
     *
     * @see IGeneralesLocal#generarFichaPredialDigitalProductos(String, UsuarioDTO ){
     * @author javier.aponte
     */
    @Override
    public boolean generarFichaPredialDigitalProductos(
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {

        LOGGER.debug("en ConservacionBean#generarFichaPredialDigitalProductos ");
        boolean answer = true;

        try {//v1.1.9
            SigDAOv2.getInstance().generarFPDAsync(this.productoCatastralJobDAO,
                productoCatastralDetalle.getNumeroPredial(), null,
                productoCatastralDetalle, usuario);
        } catch (ExcepcionSNC e) {
            answer = false;
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * Método encargado de generar la imagen predial
     *
     * @param numerosPredial
     * @param usuario
     * @return variable booleana que indica si el llamado a generar la imagene se realizó
     * correctamente
     * @author carlos.ferro
     */
    @Override
    public boolean generarImagenPredial(ProductoCatastralDetalle productoCatastralDetalle,
        UsuarioDTO usuario) {

        LOGGER.debug("en ConservacionBean#generarImagenPredial ");
        boolean answer = true;

        try {
            SigDAOv2.getInstance().generarImagenPredial(this.productoCatastralJobDAO,
                productoCatastralDetalle.getNumeroPredial(), null, productoCatastralDetalle, usuario);
        } catch (ExcepcionSNC e) {
            answer = false;
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------    
    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IGeneralesLocal#procesarFichaPredialDigitalProductos(ProductoCatastralJob)
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarFichaPredialDigitalProductos(ProductoCatastralJob job) {
        LOGGER.debug("procesarFichaPredialDigitalProductos");
        boolean resultadoOk = true;
        try {
            UsuarioDTO usuario = this.getCacheUsuario(job.getUsuarioLog());
            List<Foto> fotosFachadaPredio;
            String urlTemporalFotoFachada, urlWebFotoFachada = null;

            String urlReporte = EReporteServiceSNC.FICHA_PREDIAL_DIGITAL.getUrlReporte();

            File archivoFichaPredialDigitalProductos = null;
            //v1.1.9
            Long productoCatastralDetalleId = job.getProductoCatastralDetalleId();
            IReporteService reportesService = ReporteServiceFactory.getService();
            IDocumentosService documentosService = DocumentalServiceFactory.getService();

            Reporte reporte = new Reporte(usuario);
            reporte.setUrl(urlReporte);

            EstructuraOrganizacional estructuraOrganizacional = this.
                buscarEstructuraOrganizacionalPorCodigo(usuario.getCodigoEstructuraOrganizacional());
            reporte.setEstructuraOrganizacional(estructuraOrganizacional);

            List<ProductoCatastralResultado> resultadosFPD = job.
                obtenerResultadosParaProductosCatatrales();
            if (resultadosFPD == null || resultadosFPD.isEmpty()) {
                String message = "El servidor de productos no retornó resultados.";
                LOGGER.debug("El servidor de productos no retornó resultados.");
                throw SncBusinessServiceExceptions.EXCEPCION_100018.getExcepcion(LOGGER, null,
                    message);
            }

            List<String> numerosPrediales = job.obtenerParametroNumerosPrediales();
            LOGGER.debug("numerosPrediales: " + numerosPrediales);
            for (String numeroPredial : numerosPrediales) {
                String publicUrlFPD = null;
                String publicUrlMPD = null;
                String escalaFichaPredialDigital = null;

                for (ProductoCatastralResultado resultado : resultadosFPD) {
                    if (resultado.getCodigo().equals(numeroPredial)) {
                        if (resultado.getTipoProducto().equals(
                            ProductoCatastralJob.TIPO_IMAGEN_FPD_PRINCIPAL)) {
                            //se mueve el documento de una ruta privada del gestor documental a una publica para poder verlo por http (cambio en la arquitectura documental 24/06/2013)
                            DocumentoVO documentoVO = documentosService.
                                cargarDocumentoPrivadoEnWorkspacePreview(resultado.getIdArchivo());
                            publicUrlFPD = documentoVO.getUrlPublico();
                            escalaFichaPredialDigital = resultado.getEscala();
                            continue;
                        }
                        if (resultado.getTipoProducto().equals(
                            ProductoCatastralJob.TIPO_IMAGEN_FPD_MANZANA)) {
                            //se mueve el documento de una ruta privada del gestor documental a una publica para poder verlo por http (cambio en la arquitectura documental 24/06/2013)
                            DocumentoVO documentoVO = documentosService.
                                cargarDocumentoPrivadoEnWorkspacePreview(resultado.getIdArchivo());
                            publicUrlMPD = documentoVO.getUrlPublico();
                            continue;
                        }
                    }
                }
                LOGGER.debug("publicUrlFPD: " + publicUrlFPD);
                LOGGER.debug("publicUrlMPD: " + publicUrlMPD);

                Predio predio = this.conservacionService.getPredioByNumeroPredial(numeroPredial);

                if (predio != null) {
                    fotosFachadaPredio = this.conservacionService.
                        buscarFotografiaPorIdPredioAndFotoTipo(predio.getId(),
                            EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo());

                    if (fotosFachadaPredio != null && !fotosFachadaPredio.isEmpty() &&
                        fotosFachadaPredio.get(0).getDocumento() != null) {

                        urlTemporalFotoFachada = this.descargarArchivoDeAlfrescoATemp(
                            fotosFachadaPredio.get(0).getDocumento().getIdRepositorioDocumentos());

                        fotosFachadaPredio = this.conservacionService.
                            buscarFotografiaPorIdPredioAndFotoTipo(predio.getId(),
                                EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo());

                        if (fotosFachadaPredio != null && !fotosFachadaPredio.isEmpty() &&
                            fotosFachadaPredio.get(0).getDocumento() != null) {

                            urlTemporalFotoFachada = this.descargarArchivoDeAlfrescoATemp(
                                fotosFachadaPredio.get(0).getDocumento().
                                    getIdRepositorioDocumentos());

                            if (urlTemporalFotoFachada != null && !urlTemporalFotoFachada.isEmpty()) {
                                File archivoReporte = new File(FileUtils.getTempDirectory().
                                    getAbsolutePath(), urlTemporalFotoFachada);

                                if (archivoReporte.exists()) {
                                    urlWebFotoFachada = this.publicarArchivoEnWeb(archivoReporte.
                                        getPath());
                                }
                            }
                        }
                    }

                    reporte.addParameter("ESCALA", escalaFichaPredialDigital);
                    if (urlWebFotoFachada != null) {
                        reporte.addParameter("FOTO_FACHADA", urlWebFotoFachada);
                    }
                    reporte.addParameter("FOTO_LOCALIZACION_GEOGRAFICA", publicUrlFPD);
                    reporte.addParameter("FOTO_LOCALIZACION_MANZANA", publicUrlMPD);
                    reporte.addParameter("NUMERO_PREDIAL", numeroPredial);
                    reporte.addParameter("COPIA_USO_RESTRINGIDO", "false");

                    archivoFichaPredialDigitalProductos = reportesService.getReportAsFile(reporte,
                        new LogMensajesReportesUtil(this, usuario));

                    if (archivoFichaPredialDigitalProductos != null) {
                        LOGGER.debug(
                            "TareaGenerarFichaPredialDigitalAction: Reporte ficha predial digital productos generado exitosamente" +
                            archivoFichaPredialDigitalProductos.getAbsolutePath());

                        String rutaProducto = this.guardarProductoGeneradoEnGestorDocumental(
                            archivoFichaPredialDigitalProductos.getPath(), usuario.
                            getCodigoEstructuraOrganizacional());

                        if (rutaProducto != null) {
                            //v1.1.9
                            ProductoCatastralDetalle productoCatastralDetalle =
                                this.productoCatastralDetalleDAO.findById(Long.valueOf(
                                    productoCatastralDetalleId));
                            if (productoCatastralDetalle != null) {
                                productoCatastralDetalle.setRutaProductoEjecutado(rutaProducto);
                                this.guardarProductoCatastralDetalle(productoCatastralDetalle);
                            }

                        }

                    }
                } else {
                    LOGGER.debug(
                        "ProcesarFichaPredialDigital: Ocurrió un error al consultar el predio: " +
                        numeroPredial + " en la base de datos");
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ProcesarFichaPredialDigitalProductos:" + e.getMensaje(), e);
            resultadoOk = false;
        } catch (Exception ex) {
            LOGGER.debug("ProcesarFichaPredialDigitalProductos:" + ex.getMessage(), ex);
            resultadoOk = false;
        }
        return resultadoOk;
    }

    /**
     * @see
     * IGeneralesLocal#generarCertificadoPlanoPredialCatastralProductos(ProductoCatastralDetalle,
     * UsuarioDTO)
     * @author javier.aponte
     */
    public boolean generarCertificadoPlanoPredialCatastralProductos(
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {
        LOGGER.debug("en GeneralesBean#generarCertificadoPlanoPredialCatastralProductos ");
        boolean answer = true;
        try {
            SigDAOv2.getInstance().generarCPPAsync(this.productoCatastralJobDAO,
                productoCatastralDetalle.getNumeroPredial(),
                null, productoCatastralDetalle, usuario);
        } catch (ExcepcionSNC e) {
            answer = false;
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------    
      /**
       * Nota: Este proceso maneja su propia transacción.
       *        
       * @see IGeneralesLocal#procesarCertificadoPlanoPredialCatastralProductos(ProductoCatastralJob)
       * @author javier.aponte
       * @modified by leidy.gonzalez 11/09/2014 Se almacena en Producto Catastral detalle el documento generado
       * para el certificado plano predial.
       */
      @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
      @Override
       
      public boolean procesarCertificadoPlanoPredialCatastralProductos(ProductoCatastralJob job){
        	LOGGER.debug("procesarCertificadoPlanoPredialCatastralProductos");
        	boolean resultadoOk = true;
        	try {
    			UsuarioDTO usuario = this.getCacheUsuario(job.getUsuarioLog());
                String urlReporte = "";
                
                
                //Se verifica que tipo de certificado predial se debe generar: el de condicion 5,8,9 o el general
                List<String> resultados = this.obtenerParametrosDeProductoCatastralJobCPP(job);
                
                String numeroPredialRes = resultados.get(0);
                
                if(numeroPredialRes != null && !numeroPredialRes.isEmpty()){
                    
                    String condicion = numeroPredialRes.substring(21, 22);
                    
                    if(condicion.equals("5")){
                        
                        urlReporte = EReporteServiceSNC.CERTIFICADO_PLANO_PREDIAL_CATASTRAL_5.getUrlReporte();
                        
                    }else if(condicion.equals("8")){
                        
                        urlReporte = EReporteServiceSNC.CERTIFICADO_PLANO_PREDIAL_CATASTRAL_8.getUrlReporte();
                        
                    }else if(condicion.equals("9")){
                        
                        urlReporte = EReporteServiceSNC.CERTIFICADO_PLANO_PREDIAL_CATASTRAL_9.getUrlReporte();
                        
                    }else {
                       urlReporte = EReporteServiceSNC.CERTIFICADO_PLANO_PREDIAL_CATASTRAL.getUrlReporte(); 
                    }
                
                }
                
    			
    			
    			Long productoCatastralDetalleId = job.getProductoCatastralDetalleId();
    			Documento resultadoDocumento = new Documento();
    			
    			IReporteService reportesService = ReporteServiceFactory.getService();
    			IDocumentosService documentosService = DocumentalServiceFactory.getService();

            Reporte reporte = new Reporte(usuario);
            reporte.setUrl(urlReporte);

            EstructuraOrganizacional estructuraOrganizacional = this.
                buscarEstructuraOrganizacionalPorCodigo(usuario.getCodigoEstructuraOrganizacional());
            reporte.setEstructuraOrganizacional(estructuraOrganizacional);

            List<ProductoCatastralResultado> resultadosCPP = job.
                obtenerResultadosParaProductosCatatrales();
            if (resultadosCPP == null || resultadosCPP.isEmpty()) {
                String message = "El servidor de productos no retornó resultados.";
                LOGGER.debug("El servidor de productos no retornó resultados.");
                throw SncBusinessServiceExceptions.EXCEPCION_100018.getExcepcion(LOGGER, null,
                    message);
            }

            List<String> numerosPrediales = job.obtenerParametroNumerosPrediales();
            LOGGER.debug("numerosPrediales: " + numerosPrediales);
            for (String numeroPredial : numerosPrediales) {
                String publicUrlCPC = null;
                String publicUrlMCPC = null;
                String escala = null;

                for (ProductoCatastralResultado resultado : resultadosCPP) {
                    if (resultado.getCodigo().equals(numeroPredial)) {
                        if (resultado.getTipoProducto().equals(
                            ProductoCatastralJob.TIPO_IMAGEN_CPP_PRINCIPAL)) {
                            //se mueve el documento de una ruta privada del gestor documental a una publica para poder verlo por http (cambio en la arquitectura documental 24/06/2013)
                            DocumentoVO documentoVO = documentosService.
                                cargarDocumentoPrivadoEnWorkspacePreview(resultado.getIdArchivo());
                            publicUrlCPC = documentoVO.getUrlPublico();
                            escala = resultado.getEscala().replace(".0", "");
                            continue;
                        }
                        if (resultado.getTipoProducto().equals(
                            ProductoCatastralJob.TIPO_IMAGEN_CPP_MANZANA)) {
                            //se mueve el documento de una ruta privada del gestor documental a una publica para poder verlo por http (cambio en la arquitectura documental 24/06/2013)
                            DocumentoVO documentoVO = documentosService.
                                cargarDocumentoPrivadoEnWorkspacePreview(resultado.getIdArchivo());
                            publicUrlMCPC = documentoVO.getUrlPublico();
                            continue;
                        }
                    }
                }
                LOGGER.debug("publicUrlCPC: " + publicUrlCPC);
                LOGGER.debug("publicUrlMCPC: " + publicUrlMCPC);
                LOGGER.debug("escala: " + escala);

                Predio predio = this.conservacionService.getPredioByNumeroPredial(numeroPredial);

                if (predio != null) {
                    String departamento = predio.getNumeroPredial().substring(0, 2);
                    String municipio = predio.getNumeroPredial().substring(0, 5);

                    TipoDocumento tipoDocumento = new TipoDocumento();
                    tipoDocumento.setId(ETipoDocumento.CERTIFICADO_PLANO_PREDIAL_CATASTRAL.getId());

                    String numeracionCertificadoS = "";
                    Object[] numeracionCertificado = generarNumeracion(
                        ENumeraciones.NUMERACION_CERTIFICADO_PLANO_PREDIAL_CATASTRAL,
                        "0", departamento, municipio, tipoDocumento.getId().intValue());
                    numeracionCertificadoS = (String) numeracionCertificado[1];

                    reporte.addParameter("NUMERO_PREDIAL", numeroPredial);
                    reporte.addParameter("FOTO_LOCALIZACION_GEOGRAFICA", publicUrlCPC);
                    reporte.addParameter("FOTO_LOCALIZACION_MANZANA", publicUrlMCPC);
                    reporte.addParameter("NUMERO_DOCUMENTO", numeracionCertificadoS);
                    reporte.addParameter("ESCALA", escala);

                    File archivoCertificadoPlanoPredialCatastralProductos = reportesService.
                        getReportAsFile(reporte, new LogMensajesReportesUtil(this, usuario));
                    if (archivoCertificadoPlanoPredialCatastralProductos != null) {
                        LOGGER.debug(
                            "Reporte certificado plano predial catastral de productos generado exitosamente" +
                            archivoCertificadoPlanoPredialCatastralProductos.getAbsolutePath());

                        String rutaProducto = this.guardarProductoGeneradoEnGestorDocumental(
                            archivoCertificadoPlanoPredialCatastralProductos.getPath(),
                            usuario.getCodigoEstructuraOrganizacional());

                        if (rutaProducto != null) {
                            ProductoCatastralDetalle productoCatastralDetalle =
                                this.productoCatastralDetalleDAO.findById(Long.valueOf(
                                    productoCatastralDetalleId));
                            if (productoCatastralDetalle != null) {
                                productoCatastralDetalle.setRutaProductoEjecutado(rutaProducto);
                                resultadoDocumento = this.documentoDAO.
                                    guardarDocumentoProductosCatastrales(usuario,
                                        productoCatastralDetalle, numeracionCertificado);
                                productoCatastralDetalle.setProductoDocumentoId(resultadoDocumento);
                                this.guardarProductoCatastralDetalle(productoCatastralDetalle);
                            }
                        }
                    }
                } else {
                    LOGGER.debug(
                        "ProcesarFichaPredialDigital: Ocurrió un error al consultar el predio: " +
                        numeroPredial + " en la base de datos");
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ProcesarCertificadoPlanoPredialCatastralProductos:" + e.getMensaje(), e);
            resultadoOk = false;
        } catch (Exception ex) {
            LOGGER.debug("ProcesarCertificadoPlanoPredialCatastralProductos:" + ex.getMessage(), ex);
            resultadoOk = false;
        }
        return resultadoOk;
    }

    //versión 1.1.4
    //--------------------------------------------------------------------------------------------------	
    /**
     * Para los niveles de transaccionalidad ver Transaction Annotations
     * http://tomee.apache.org/transaction-annotations.html
     *
     * @see IGeneralesLocal#generarCartaCatastralUrbanaProductos(ProductoCatastralDetalle,
     * UsuarioDTO ){
     * @author javier.aponte
     */
    //@modified by leidy.gonzalez #10054 :: 24/10/2014 ::Se agrega parametro fecha de vigencia al enviarJobGenerarCartaCatastralUrbanaProductos
    //@modified by leidy.gonzalez #10108 :: 06/11/2014 ::Se envia parametro en true para que genere pdf en el metodo enviarJobGenerarCartaCatastralUrbanaProductos
    @Override
    public boolean generarCartaCatastralUrbanaProductos(
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {

        LOGGER.debug("en GeneralesBean#generarCartaCatastralUrbanaProductos ");
        boolean answer = true;

        String numeroManzana;
        EstructuraOrganizacional eo;
        Departamento departamento;
        Municipio municipio;

        String nombreDepartamento;
        String nombreMunicipio;

        try {
            numeroManzana = productoCatastralDetalle.getNumeroPredial().substring(0, 17);

            eo = this.jurisdicionService.getEstructuraOrganizacionalByMunicipioCod(numeroManzana.
                substring(0, 5));
            //String territorial = eo.getNombre().trim();

            departamento = this.departamentoDAO.getDepartamentoByCodigo(numeroManzana.
                substring(0, 2));
            municipio = this.municipioDAO.getMunicipioByCodigo(numeroManzana.substring(0, 5));

            nombreDepartamento = departamento.getNombre().trim().replace("(",
                Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
            nombreMunicipio = municipio.getNombre().trim().replace("(",
                Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);

            nombreDepartamento = nombreDepartamento.split(
                Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)[0];
            nombreMunicipio =
                nombreMunicipio.split(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)[0];

            boolean generarEnPDF = true;
            SigDAOv2.getInstance().generarCCUAsync(this.productoCatastralJobDAO, numeroManzana,
                generarEnPDF, null, productoCatastralDetalle, usuario);

        } catch (ExcepcionSNC e) {
            answer = false;
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------    
    /**
     *
     *
     * @see IConservacionLocal#procesarFichaPredialDigital
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarCartaCatastralUrbanaProductos(ProductoCatastralJob job) {
        LOGGER.debug("procesarCartaCatastralUrbanaProductos");
        boolean resultadoOk = true;
        try {
            UsuarioDTO usuario = this.getCacheUsuario(job.getUsuarioLog());
            try {
                String numManzana = job.obtenerParametroCodigoManzana().get(0);
                LOGGER.debug("numerosManzana: " + numManzana);

                List<ProductoCatastralResultado> resultadoCartaCatastralUrbana = job.
                    obtenerResultadosParaProductosCatatrales();
                if (resultadoCartaCatastralUrbana == null || resultadoCartaCatastralUrbana.isEmpty()) {
                    String message = "El servidor de productos no retornó resultados.";
                    throw SncBusinessServiceExceptions.EXCEPCION_100018.getExcepcion(LOGGER, null,
                        message);
                }

                String rutaCarpetaTemporalImagenCCU = this.descargarArchivoDeAlfrescoATemp(
                    resultadoCartaCatastralUrbana.get(0).getIdArchivo());
                String rutaProducto = this.guardarProductoGeneradoEnGestorDocumental(
                    rutaCarpetaTemporalImagenCCU, usuario.getCodigoEstructuraOrganizacional());

                if (rutaProducto != null) {
                    Long productoCatastralDetalleId = job.getProductoCatastralDetalleId();
                    ProductoCatastralDetalle productoCatastralDetalle =
                        this.productoCatastralDetalleDAO.findById(productoCatastralDetalleId);
                    if (productoCatastralDetalle != null) {
                        productoCatastralDetalle.setRutaProductoEjecutado(rutaProducto);
                        this.guardarProductoCatastralDetalle(productoCatastralDetalle);
                    }
                }
            } catch (ExcepcionSNC e) {
                LOGGER.error(e.getMessage(), e);
                ErrorUtil.notificarErrorPorCorreoElectronico(e, usuario.getLogin(), null, e.
                    getMessage());
                resultadoOk = false;
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje());
            resultadoOk = false;
        }
        return resultadoOk;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author leidy.gonzalez
     * @see IGeneralesLocal#eliminarProductoCatastralDetalle(productoCatastralDetalle)
     */
    @Override
    public void eliminarProductoCatastralDetalle(ProductoCatastralDetalle productoCatastralDetalle) {
        try {
            this.productoCatastralDetalleDAO.delete(productoCatastralDetalle);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
    }

    /**
     * Ejecuta el SP provisional que permite consultar la Cantidad de regidtors prediales a generar
     * en el Producto Catastral
     *
     * @param parametros
     * @author leidy.gonzalez
     * @return
     */
    @Override
    public Object[] contarRegistrosPrediales(List<Object> parametros) {

        Object[] answer;
        try {
            answer = this.procedimientoService.contarRegistrosPrediales(parametros);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGeneralesLocal#borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(Long, Long)
     * @author javier.aponte
     */
    public boolean borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(Long tramiteId,
        Long tipoDocumentoId) {

        boolean answer = true;

        List<TramiteDocumento> tramiteDocumentos;

        Documento documento;

        try {

            tramiteDocumentos = this.tramiteDocumentoDao.findByTramiteAndTipoDocumento(tramiteId,
                tipoDocumentoId);

            if (tramiteDocumentos != null && !tramiteDocumentos.isEmpty()) {
                for (TramiteDocumento tdTemp : tramiteDocumentos) {

                    documento = tdTemp.getDocumento();

                    this.tramiteDocumentoDao.delete(tdTemp);

                    answer = this.documentoDAO.deleteDocumentoAlsoGestorDocumental(documento);
                }
            }

        } catch (Exception e) {
            answer = false;
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;

    }

    /**
     * @see IGenerales#getCacheDominioPorNombre(EDominio)
     */
    public List<ProductoFormato> getCacheFormatoPorProducto(Long productoId) {
        return this.productoFormatoDAO.findByIdProducto(productoId);
    }

    /**
     * @see
     * IGeneralesLocal#guardarNumerosPredialesEntreRangosEnProductoCatastralDetalle(productoCatastralDetalle)
     * @author leidy.gonzalez
     */
    public void guardarNumerosPredialesEntreRangosEnProductoCatastralDetalle(
        ProductoCatastralDetalle productoCatastralDetalle) {

        ProductoCatastralDetalle pcdCopia = new ProductoCatastralDetalle();
        pcdCopia = (ProductoCatastralDetalle) productoCatastralDetalle.clone();
        List<ProductoCatastralDetalle> productosDetalles = new ArrayList<ProductoCatastralDetalle>();
        List<String> rangosPredios = null;

        rangosPredios = this.predioDAO.
            getNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal(
                pcdCopia.getNumeroPredialInicial(),
                pcdCopia.getNumeroPredialFinal(),
                true);

        pcdCopia = new ProductoCatastralDetalle();
        pcdCopia = (ProductoCatastralDetalle) productoCatastralDetalle.clone();
        pcdCopia.setNumeroPredialInicial(pcdCopia.getNumeroPredialInicial());

        productosDetalles.add(pcdCopia);

        int contador = 0;

        for (String predios : rangosPredios) {

            if (contador >= 1) {

                productosDetalles.get(contador - 1).setNumeroPredialFinal(predios);
                pcdCopia = new ProductoCatastralDetalle();
                pcdCopia = (ProductoCatastralDetalle) productoCatastralDetalle.clone();
                pcdCopia.setNumeroPredialInicial(predios);
                pcdCopia.setNumeroPredial(predios);
                productosDetalles.add(pcdCopia);

            }
            contador++;

            if (pcdCopia.getNumeroPredialFinal().equals(productoCatastralDetalle.
                getNumeroPredialFinal())) {
                pcdCopia.setNumeroPredialFinal(null);
            }
        }

        this.productoCatastralDetalleDAO.updateMultiple(productosDetalles);

    }

    /**
     * @see IGeneralesLocal#obtenerNumeroAccesosIniciadosPorUsuario(usuario)
     * @author felipe.cadena
     */
    @Override
    public Long obtenerNumeroAccesosIniciadosPorUsuario(String usuario) {
        Long resultado = null;
        try {
            resultado = this.logAccesoDAO.obtenerNumeroAccesosIniciadosPorUsuario(usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    /**
     * @see IGeneralesLocal#obtenerNumeroConexionesPorUsuario(usuario)
     * @author felipe.cadena
     */
    @Override
    public Long obtenerNumeroConexionesPorUsuario(String usuario) {
        Long resultado = null;
        try {
            resultado = this.conexionUsuarioDAO.obtenerNumeroConexionesPorUsuario(usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    /**
     * @see
     * IGeneralesLocal#guardarActualizarConexionUsuario(co.gov.igac.snc.persistence.entity.generales.ConexionUsuario)
     * @author felipe.cadena
     */
    @Override
    public ConexionUsuario guardarActualizarConexionUsuario(ConexionUsuario cu) {
        ConexionUsuario resultado = null;
        try {
            resultado = this.conexionUsuarioDAO.update(cu);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    /**
     * @see IGeneralesLocal#obtenerNumeroConexionesPorUsuario(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public void eliminarConexionUsuario(ConexionUsuario cu) {

        try {
            this.conexionUsuarioDAO.delete(cu);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IGeneralesLocal#obtenerConexionUsuarioPorId(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public ConexionUsuario obtenerConexionUsuarioPorId(Long id) {
        ConexionUsuario resultado = null;
        try {
            resultado = this.conexionUsuarioDAO.findById(id);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    /**
     * @see IGeneralesLocal#obtenerConexionUsuarioPorSesionId(java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public ConexionUsuario obtenerConexionUsuarioPorSesionId(String idSesion) {
        ConexionUsuario resultado = null;
        try {
            resultado = this.conexionUsuarioDAO.obtenerPorIdSesion(idSesion);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }

    //--------------------------------------------------------------------------------------------------	
    /**
     *
     * @see
     * co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsReportesPredialesPendientesEnSNC(java.lang.Long)
     * @author javier.aponte
     */
    @Override
    public List<ProductoCatastral> obtenerJobsReportesPredialesPendientesEnSNC(Long cantidad) {
        List<ProductoCatastral> answer = null;
        try {
            answer = this.productoCatastralDAO.obtenerJobsPendientesEnSNC(cantidad);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerJobsPendientesEnSNC: " + ex.getMensaje());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------    
    /**
     * Nota: Este proceso maneja su propia transacción.
     *
     * @see IGeneralesLocal#procesarRegistrosPredialesProductosPDF(ProductoCatastral)
     * @author javier.aponte
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public boolean procesarRegistrosPredialesProductosPDF(ProductoCatastral job) {

        LOGGER.debug("procesarRegistrosPredialesProductosPDF");
        boolean resultadoOk = true;

        String rutaProducto;

        File archivoRegistrosPredialesProductos = null;

        String urlReporte, urlReporteExtendidos = null;

        try {

            UsuarioDTO usuario = this.getCacheUsuario(job.getUsuarioLog());

            if (ESiNo.SI.getCodigo().equals(job.getDatosPropietarios())) {
                urlReporte = EReporteServiceSNC.REGISTROS_PREDIALES_DATOS_BASICOS.getUrlReporte();
            } else {
                urlReporte = EReporteServiceSNC.REGISTROS_PREDIALES_DATOS_BASICOS_SIN_PROPIETARIO.
                    getUrlReporte();
            }
            if (ESiNo.SI.getCodigo().equals(job.getDatosExtendidos())) {
                urlReporteExtendidos = EReporteServiceSNC.REGISTROS_PREDIALES_DATOS_EXTENDIDOS.
                    getUrlReporte();
            }

            IReporteService reportesService = ReporteServiceFactory.getService();
            IDocumentosService documentosService = DocumentalServiceFactory.getService();

            Reporte reporte = new Reporte(usuario);
            reporte.setUrl(urlReporte);

            EstructuraOrganizacional estructuraOrganizacional = this
                .buscarEstructuraOrganizacionalPorCodigo(
                    usuario.getCodigoEstructuraOrganizacional());

            reporte.setEstructuraOrganizacional(estructuraOrganizacional);

            reporte.addParameter("PRODUCTO_CATASTRAL_DETALLE_ID", String.valueOf(job.getId()));

            archivoRegistrosPredialesProductos = reportesService.getReportAsFile(reporte,
                new LogMensajesReportesUtil(this, usuario));

            if (archivoRegistrosPredialesProductos != null) {
                LOGGER.debug(
                    "TareaGenerarFichaPredialDigitalAction: Reporte registros prediales generado exitosamente" +
                    archivoRegistrosPredialesProductos.getAbsolutePath());

                rutaProducto = this.guardarProductoGeneradoEnGestorDocumental(
                    archivoRegistrosPredialesProductos.getPath(),
                    usuario.getCodigoEstructuraOrganizacional());

                if (rutaProducto != null) {
                    job.setRutaProductoEjecutado(rutaProducto);
                }
            }

            if (urlReporteExtendidos != null) {
                Reporte reporteExtendidos = new Reporte(usuario);
                reporteExtendidos.setUrl(urlReporteExtendidos);

                reporteExtendidos.setEstructuraOrganizacional(estructuraOrganizacional);

                reporteExtendidos.addParameter("PRODUCTO_CATASTRAL_DETALLE_ID", String.valueOf(job.
                    getId()));

                archivoRegistrosPredialesProductos = reportesService.getReportAsFile(
                    reporteExtendidos,
                    new LogMensajesReportesUtil(this, usuario));

                if (archivoRegistrosPredialesProductos != null) {
                    LOGGER.debug(
                        "TareaGenerarFichaPredialDigitalAction: Reporte registros prediales con datos extendidos generado exitosamente" +
                        archivoRegistrosPredialesProductos.getAbsolutePath());

                    rutaProducto = this.guardarProductoGeneradoEnGestorDocumental(
                        archivoRegistrosPredialesProductos.getPath(),
                        usuario.getCodigoEstructuraOrganizacional());

                    if (rutaProducto != null) {
                        job.setRutaProductoEjecutado(job.getRutaProductoEjecutado() + ";" +
                            rutaProducto);
                    }
                }
            }

            job = this.productoCatastralDAO.update(job);

        } catch (Exception ex) {
            LOGGER.debug("procesarRegistrosPredialesProductosPDF:" + ex.getMessage(), ex);
            resultadoOk = false;
        }

        return resultadoOk;
    }

    //--------------------------------------------------------------------------------------------------	
    /**
     *
     * @see
     * co.gov.igac.snc.fachadas.IGeneralesLocal#verificarEjecucionJobPendienteEnSNC(java.lang.Long)
     * @autor javier.aponte
     *
     */
    @Override
    public void verificarEjecucionJobReportesPredialesPendienteEnSNC(Long idJob) {
        LOGGER.debug("verificarEjecucionJobReportesPredialesPendienteEnSNC con id " + idJob.
            toString());
        ProductoCatastral job = null;
        //versión 1.1.4
        try {
            ///////////////////////////////////////////////////////////////////////////////////////////////
            job = this.productoCatastralDAO.findById(idJob);
            LOGGER.debug("job:" + job);
            boolean resultadoOk = false;
            this.productoCatastralDAO.actualizarEstado(job, ProductoCatastral.SNC_EN_EJECUCION);
            ///////////////////////////////////////////////////////////////////////////////////////////////
            resultadoOk = this.procesarRegistrosPredialesProductosPDF(job);

            if (resultadoOk) {
                this.productoCatastralDAO.actualizarEstado(job, ProductoCatastral.SNC_TERMINADO);
            } else {
                this.productoCatastralDAO.actualizarEstado(job, ProductoCatastral.SNC_ERROR);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            if (job != null) {
                this.productoCatastralDAO.actualizarEstado(job, ProductoCatastral.SNC_ERROR);
            }
        }
    }

    /**
     * @author felipe.cadena
     * @see IGeneralesLocal#obtenerJobsTramite(java.lang.Long)
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsTramite(Long tramiteId) {
        List<ProductoCatastralJob> catastralJobs = new LinkedList<ProductoCatastralJob>();

        try {
            catastralJobs = this.productoCatastralJobDAO.obtenerJobsGeograficosPorTramiteTipoEstado(
                tramiteId, null, null);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar los jobs del tramite" + e.getMensaje());
        }

        return catastralJobs;
    }
    /**
     * 
     * @param tipoProducto
     * @param usuarioLog
     * @return 
     */
    @Override
    public ProductoCatastralJob obtenerJobsCargueActualizacion(String tipoProducto, String usuarioLog){
        ProductoCatastralJob job = new ProductoCatastralJob();
        
        try {
            job = this.productoCatastralJobDAO.obtenerJobsCargueActualizacion(
                tipoProducto, usuarioLog);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar los jobs del tramite" + e.getMensaje());
        }

        return job;
    }

    /**
     * @author felipe.cadena
     * @see IGeneralesLocal#obtenerJobsTramite(java.lang.Long)
     */
    /*
     * @modified by leidy.gonzalez:: 15/10/2015 :: 14043
     */
    @Override
    public ProductoCatastralJob obtenerUltimoJobPorTramite(Long tramiteId, String tipoProducto) {
        ProductoCatastralJob catastralJob = null;

        try {
            catastralJob = this.productoCatastralJobDAO.obtenerUltimoJobPorTramite(tramiteId,
                tipoProducto);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar los jobs del tramite " + e.getMensaje());
        }

        return catastralJob;
    }

    /**
     * Metodo que obtiene una lista de los Nombres a visualizar de los tipos de reportes prediales
     *
     * @author leidy.gonzalez
     */
    public List<RepReporte> obtenerNombreCategoriaReporte() {

        List<RepReporte> answer = null;
        try {
            answer = this.repReporteDAO.findAll();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerTodosGrupoProductos: " + ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#reenviarJobReportesTerminado(java.lang.Long)
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public void reenviarJobReportesTerminado(Long idJob) {
        LOGGER.debug("reenviarJobReportesTerminado con id " + idJob.toString());
        RepReporteEjecucion job = null;
        String documentoLogo;

        Documento documento = null;

        File archivoReportesPrediales = null;
        String repReporteId = null;

        List<String> destinatariosList = new ArrayList<String>();
        IReporteService service = ReporteServiceFactory.getService();
        String rutaProducto = null;
        UsuarioDTO usuario;
        RepReporte repReporte = null;
        Reporte reporte;
        Double tamanoMegaReporte = 1048576D;
        Parametro maxEnvioJob = this.getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_ENVIO_JOB_RESOLUCIONES.toString());

        try {

            job = this.repReporteEjecucionDAO.buscarReportePorId(Long.valueOf(idJob));
            Hibernate.initialize(job.getRepReporte().getId());

            if (job != null && job.getRepReporte() != null &&
                job.getRepReporte().getId() != null) {
                if (job.getNumeroEnvio() != null) {
                    job.setFechaLog(new Date());
                    job.setNumeroEnvio(job.getNumeroEnvio() + 1);
                    job = this.repReporteEjecucionDAO.update(job);
                }

                repReporte = this.repReporteDAO.buscarReportePorId(job.getRepReporte().getId());
            }

            if (repReporte != null && repReporte.getUrlReporteJasper() != null &&
                !repReporte.getUrlReporteJasper().isEmpty()) {

                usuario = this.getCacheUsuario(job.getUsuario());
                reporte = new Reporte(usuario);
                if(Constantes.REPORTE_VACIO.equals(job.getReporteVacio())){

                    Parametro param = this.getCacheParametroPorNombre(EParametro.URL_REPORTE_ESTADISTICO_VACIO.toString());
                    reporte.setUrl(param.getValorCaracter());

                }else {
                    reporte.setUrl(repReporte.getUrlReporteJasper());
                }

                EstructuraOrganizacional estructuraOrganizacional = this
                    .buscarEstructuraOrganizacionalPorCodigo(usuario
                        .getCodigoEstructuraOrganizacional());

                reporte.setEstructuraOrganizacional(estructuraOrganizacional);

                repReporteId = job.getId().toString();

                reporte.addParameter("REPORTE_EJECUCION_ID", repReporteId);

                documento = documentoDAO.buscarDocumentoPorTipoDocumentoYUsuario("SNC",
                    ETipoDocumento.OTRAS_IMAGENES.getId());

                if (documento != null && documento.getIdRepositorioDocumentos() != null) {

                    documentoLogo = this.descargarArchivoAlfrescoYSubirAPreview(documento.
                        getIdRepositorioDocumentos());

                    if (documentoLogo != null) {
                        reporte.addParameter("RUTA_LOGO", documentoLogo);
                    }
                }

                if (job.getFormato() != null) {
                    reporte.setFormato(job.getFormato());
                }

                reporte.addParameter("FORMATO_EXPORTACION", job.getFormato());

                //Se envia el Responsable de Conservacion y la Firma cuando el tipo de reporte de Radicaciones
                //Es: RADICACIONES DE PREDIOS CON MATRÍCULAS REGISTRALES
                if (repReporte.getId().equals(
                    EReportesRadicacionesDinamicosSNC.PREDIOS_CON_MATRICULAS_REGISTRALES.
                        getIdConfiguracionReporte()) &&
                    !estructuraOrganizacional.getCodigo().isEmpty()) {

                    String estructuraOrg = estructuraOrganizacional.getCodigo();

                    List<UsuarioDTO> usuarios = this.tramiteService.
                        buscarFuncionariosPorRolYTerritorial(
                            estructuraOrg, ERol.RESPONSABLE_CONSERVACION);

                    if (usuarios != null && !usuarios.isEmpty()) {
                        usuario = usuarios.get(0);
                    }

                    if (usuario != null &&
                        usuario.getNombreCompleto() != null &&
                        !usuario.getNombreCompleto().isEmpty()) {

                        reporte.addParameter("NOMBRE_RESPONSABLE_CONSERVACION", usuario.
                            getNombreCompleto());

                        FirmaUsuario firma;
                        String documentoFirma;

                        firma = buscarDocumentoFirmaPorUsuario(usuario.getNombreCompleto());

                        if (firma != null && usuario.getNombreCompleto().equals(firma.
                            getNombreFuncionarioFirma())) {

                            documentoFirma = descargarArchivoAlfrescoYSubirAPreview(firma.
                                getDocumentoId().getIdRepositorioDocumentos());

                            reporte.addParameter("FIRMA_USUARIO", documentoFirma);
                        }
                    }

                }

                //felipe.cadena::06-03-2018::Uso de servicio asincronico para reportes nocturnos
                if (job.getEjecucionHorario() != null && job.getEjecucionHorario().equalsIgnoreCase(
                    Constantes.NOCTURNO)) {

                    SNCPropertiesUtil props = SNCPropertiesUtil.getInstance();
                    props.getProperty("reports.server.async.url");
                    //String url = "http://172.17.3.104:3000/api/jasper/";
                    String url = props.getProperty("reports.server.async.url");
                    if (!NetClient.GET(url + job.getId(), null)) {
                        throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_CLIENTE_REST_REPORT_ASYNC.
                            getExcepcion(LOGGER, null, "reenviarJobReportesTerminado", job.getId());
                    } else {
                        return;
                    }
                }

                archivoReportesPrediales = service.getReportAsFile(reporte,
                    new LogMensajesReportesUtil(this, usuario));

                if (archivoReportesPrediales != null) {

                    //Modificacion de extension para archivos CSV
                    String rutaReporte;
                    if (Reporte.FORMAT_CSV.equals(reporte.getFormato())) {
                        rutaReporte = cambiarExtensionReporte(archivoReportesPrediales, reporte.
                            getFormato());
                    } else {
                        rutaReporte = archivoReportesPrediales.getPath();
                    }

                    LOGGER.debug(
                        "TareaGenerarRegistrosPredialesAction: Reporte registros prediales generado exitosamente" +
                        archivoReportesPrediales.getAbsolutePath());

                    if (job.getUOC() != null && !job.getUOC().getCodigo().isEmpty()) {
                        rutaProducto = this.
                            guardarReporteGeneradoEnGestorDocumental(rutaReporte, "", job.
                                getRepReporte().getNombre(),
                                job.getUOC().getCodigo(), job.getDepartamentoCodigo(), job.
                                getMunicipioCodigo(), job.getCodigoReporte(), job.getVigencia());

                    } else if (job.getTerritorial() != null && !job.getTerritorial().getCodigo().
                        isEmpty()) {
                        rutaProducto = this.
                            guardarReporteGeneradoEnGestorDocumental(rutaReporte, "", job.
                                getRepReporte().getNombre(),
                                job.getTerritorial().getCodigo(), job.getDepartamentoCodigo(), job.
                                getMunicipioCodigo(), job.getCodigoReporte(), job.getVigencia());
                    }

                    if (rutaProducto != null) {

                        //Guarda datos en la Tabla Rer_reporte_ejecucion
                        Double tamanoReporte = archivoReportesPrediales.length() / tamanoMegaReporte;
                        BigDecimal bTamanioReporte = new BigDecimal(tamanoReporte);
                        job.setUnidadTamano("MB");
                        job.setValorTamano(bTamanioReporte);
                        job.setRutaReporteGenerado(rutaProducto);
                        job.setEstado(ERepReporteEjecucionEstado.FINALIZADO.getCodigo());
                        job.setFechaGeneracion(new Date(System.currentTimeMillis()));
                        job.setFechaLog(new Date());
                        job.setUsuarioLog(usuario.getLogin());
                        job = this.repReporteEjecucionDAO.update(job);
                    }

                }

                //Envía email al Usuario Informando la finalizacion de la generacion del reporte
                UsuarioDTO usuarioCorreo = usuario;

                if (usuarioCorreo != null && usuarioCorreo.getEmail() != null &&
                    !usuarioCorreo.getEmail().trim().isEmpty()) {
                    destinatariosList.add(usuarioCorreo.getEmail());
                }

                String[] destinatarios = destinatariosList
                    .toArray(new String[destinatariosList.size()]);

                // Asunto y el contenido del correo electronico.
                String asunto;
                if(job.getRepReporte().getCategoria().equals(ERepReporteCategoria.REPORTES_ESTADISTICOS.getCategoria())){
                    asunto =
                            ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_REGISTRO_REPORTES_ESTADISTICOS;
                }else{
                    asunto =
                            ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_REGISTRO__REPORTES_PREDIALES;
                }


                // Plantilla de contenido
                String codigoPlantillaContenido =
                    EPlantilla.MENSAJE_COMUNICACION_REGISTRO_REPORTES_PREDIALES
                        .getCodigo();

                // Parametros de la plantilla de contenido
                String[] parametrosPlantillaContenido = new String[2];
                parametrosPlantillaContenido[0] = job.getCodigoReporte();
                parametrosPlantillaContenido[1] = job.getFechaGeneracion().toString();

                // Cargar parametros en el managed bean del componente.
                String codigoPlantillaEncabezado = EPlantilla.ENCABEZADO_CORREO_ELECTRONICO
                    .getCodigo();

                Plantilla plantillaEncabezado = recuperarPlantillaPorCodigo(
                    codigoPlantillaEncabezado);

                String encabezadoCorreo = plantillaEncabezado.getHtml();

                if (encabezadoCorreo != null) {

                    // Se instancia el messageFormat.
                    Locale colombia = new Locale("ES", "es_CO");
                    MessageFormat messageFormat = new MessageFormat(
                        encabezadoCorreo, colombia);

                    // Se reemplazan los parametros en el contenido de la plantilla.
                    encabezadoCorreo = messageFormat
                        .format(parametrosPlantillaContenido);
                }

                // Consulta de la plantilla del contenido.
                Plantilla plantillaContenido = recuperarPlantillaPorCodigo(codigoPlantillaContenido);

                if (plantillaContenido != null) {

                    // Consulta del contenido del correo.
                    String contenidoCorreo = plantillaContenido.getHtml();

                    if (contenidoCorreo != null) {

                        // Se instancia el messageFormat.
                        Locale colombia = new Locale("ES", "es_CO");
                        MessageFormat messageFormat = new MessageFormat(
                            contenidoCorreo, colombia);

                        // Se reemplazan los parametros en el contenido de la
                        // plantilla.
                        contenidoCorreo = messageFormat
                            .format(parametrosPlantillaContenido);

                    }

                    // Envio de correo electronico
                    for (String correoDestinatario : destinatarios) {
                        if (correoDestinatario != null &&
                            !correoDestinatario.trim().isEmpty()) {
                            enviarCorreo(correoDestinatario, asunto,
                                contenidoCorreo, null, null);
                        }
                    }

                }
            } else {
                LOGGER.error(
                    "Se debe agregar la ruta del reporte en la tabla: REP_REPORTE_EJECUCION, columna: URL_REPORTE_JASPER");
                return;
            }

        } catch (Exception e) {

            if (job != null) {
                String usuarioLog =
                    repReporte == null || repReporte.getUsuarioLog() == null ? null : repReporte.
                    getUsuarioLog();

                //reenvia job
                if (job.getNumeroEnvio() != null &&
                    job.getNumeroEnvio() <= maxEnvioJob.getValorNumero()) {

                    this.reenviarJobReportesTerminado(job.getId());

                } else if (archivoReportesPrediales == null) {

                    job.setEstado(ERepReporteEjecucionEstado.ERROR_GENERANDO.getCodigo());

                    job.setFechaLog(new Date());
                    job = this.repReporteEjecucionDAO.update(job);
                    LOGGER.error("Se ha producido un error en la generación del reporte", e);

                } else if (rutaProducto == null) {
                    job.setEstado(ERepReporteEjecucionEstado.ERROR_ALMACENANDO.getCodigo());

                    job.setFechaLog(new Date());
                    job = this.repReporteEjecucionDAO.update(job);
                    LOGGER.error("Ha ocurrido un error en el almacenamiento del reporte", e);

                }

            } else {
                LOGGER.error("No se encontro el job con id: " + idJob);
                return;
            }

            LOGGER.error(
                "Ha ocurrido un error de conexión con la Base de Datos. Comunicarse con el administrador." +
                e.getMessage(), e);
        }

    }

    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------
    /**
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsControlReporteTerminado(int)
     */
    public List<RepReporteEjecucion> obtenerJobsReporteEjecucionTerminado(Long cantidad) {
        return this.repReporteEjecucionDAO.obtenerJobsReporteEjecucionTerminado(cantidad);
    }

    // ------------------------------------------------//
    /**
     * @author david.cifuentes
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#buscarRepReportePorId(Long)
     */
    @Override
    public RepReporte buscarRepReportePorId(Long idRepReporte) {
        RepReporte answer = null;
        try {
            answer = this.repReporteDAO.findById(idRepReporte);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#buscarRepReportePorId: " +
                ex.getMensaje());
        }
        return answer;
    }

    // ------------------------------------------------//
    /**
     * @author david.cifuentes
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#buscarRepReportesPorCategoria(String)
     */
    @Override
    public List<RepReporte> buscarRepReportesPorCategoria(String categoria) {
        List<RepReporte> answer = null;
        try {
            answer = this.repReporteDAO.obtenerListaTipoReportePorCategoria(categoria);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#buscarRepReportesPorCategoria: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @author felipe.cadena
     * @see
     * co.gov.igac.snc.fachadas.IGeneralesLocal#validarExistenciaDocumentoGestorDocumental(co.gov.igac.snc.persistence.entity.generales.Documento)
     */
    @Override
    public Boolean validarExistenciaDocumentoGestorDocumental(Documento documento) {
        Boolean answer = null;
        try {
            answer = this.documentoDAO.validarExistenciaGestorDocumental(documento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#validarExistenciaDocumentoGestorDocumental: " +
                ex.getMensaje());
        }
        return answer;
    }

    // ------------------------------------------------//
    /**
     * @author felipe.cadena
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#buscarUOCporCodigoTerritorial(String)
     */
    @Override
    public List<EstructuraOrganizacional> buscarUOCporCodigoTerritorial(String codigoTerritorial) {
        List<EstructuraOrganizacional> answer = null;
        try {
            answer = this.estructuraOrganizacionalDAOService.buscarUOCporCodigoTerritorial(
                codigoTerritorial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#buscarUOCporCodigoTerritorial: " +
                ex.getMensaje());
        }
        return answer;
    }

    // ------------------------------------------------//
    /**
     * @author felipe.cadena
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#buscarMunicipioPorIdOficina(String)
     */
    @Override
    public List<Municipio> buscarMunicipioPorIdOficina(String codigoOficina) {
        List<Municipio> answer = null;
        try {
            answer = this.municipioDAO.findByEntidadTerritorial(codigoOficina);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#buscarUOCporCodigoTerritorial: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @author felipe.cadena
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerTiposSolicitudesTramites()
     */
    @Override
    public Map<String, List<Dominio>> obtenerTiposSolicitudesTramites() {

        Map<String, List<Dominio>> resultado = null;

        try {
            resultado = this.dominioDAO.obtenerSolicitudesYTramites();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerTiposSolicitudesTramites: " +
                ex.getMensaje());
        }
        return resultado;

    }

    /**
     * @see IGeneralesLocal#obtenerJobsEsperandoUnoPorManzana(int)
     * @param cantidadJobs
     * @return
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsEsperandoUnoPorManzana(long cantidadJobs) throws
        Exception {

        LOGGER.debug("obtenerJobsEsperandoUnoPorManzana...INICIA");
        try {
            List<ProductoCatastralJob> jobs = this.productoCatastralJobDAO.
                obtenerJobsEsperandoUnoPorManzana(cantidadJobs);
            LOGGER.debug("Número de Jobs por sincronizar: " + jobs.size());
            return jobs;
        } catch (Exception e) {
            throw e;
        } finally {
            LOGGER.debug("obtenerJobsEsperandoUnoPorManzana...Finaliza");

        }
    }

    /**
     * @see IGeneralesLocal#aplicarCambiosGeograficos(java.lang.Long)
     * @author andres.eslava
     */
    public void aplicarCambiosGeograficos(ProductoCatastralJob job) throws Exception {
        LOGGER.debug("aplicarCambiosGeograficos...INICIA");
        try {
            UsuarioDTO usuario = this.getCacheUsuario(job.getUsuarioLog());
            SigDAOv2.getInstance().aplicarCambiosAsync(this.productoCatastralJobDAO, job, usuario);
        } catch (Exception e) {
            throw e;
        } finally {
            LOGGER.debug("aplicarCambiosGeograficos...FINALIZA");
        }
    }

    /**
     * @see IGeneralesLocal#confirmarEjecucionJobErrorGeografico()
     */
    @Override
    public Boolean confirmarEjecucionJobErrorGeografico() throws Exception {
        LOGGER.debug("confirmarEjecucionJobAplicarCambios...INICIA");
        Parametro parametro = null;
        try {
            parametro = parametroDAO.getParametroPorNombre(EParametro.EJECUTA_JOB_ERROR_GEOGRAFICO.
                toString());
            if (parametro.getValorCaracter().equals("SI")) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } catch (Exception e) {
            throw new Exception("Error obteniendo la bandera para la ejecucion del JOB");
        } finally {
            LOGGER.debug("confirmarEjecucionJobAplicarCambios...FINALIZA");
        }
    }

    /**
     * @see IGeneralesLocal#confirmarEjecucionJobAplicarCambios
     */
    @Override
    public Boolean confirmarEjecucionJobAplicarCambios() throws Exception {
        LOGGER.debug("confirmarEjecucionJobAplicarCambios...INICIA");
        Parametro parametro = null;
        try {
            parametro = parametroDAO.getParametroPorNombre(EParametro.EJECUTA_JOB_APLICAR_CAMBIOS.
                toString());
            if (parametro.getValorCaracter().equals("SI")) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } catch (Exception e) {
            throw new Exception("Error obteniendo la bandera para la ejecucion del JOB");
        } finally {
            LOGGER.debug("confirmarEjecucionJobAplicarCambios...FINALIZA");
        }
    }

    /**
     * @see IGeneralesLocal#obtenerJobsZonasPorEstadoYTramiteId
     * @author felipe.cadena
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsPorEstadoTramiteIdTipo(List<String> estados,
        Long tramiteId, String tipo) {
        LOGGER.debug("obtenerJobsZonasPorEstadoYTramiteId");
        List<ProductoCatastralJob> resultado = new ArrayList<ProductoCatastralJob>();
        try {
            resultado = this.productoCatastralJobDAO.obtenerPorEstadoTramiteIdTipo(estados,
                tramiteId, tipo);
        } catch (Exception e) {
            LOGGER.error("Error al consultar jobs del tramite");
        }

        return resultado;
    }

    /**
     * @see IGeneralesLocal#obtenerParametrosEditor();
     */
    @Override
    public List<ParametroEditorVO> obtenerParametrosEditor() {
        LOGGER.debug("GeneralesBean#obtenerParametrosEditor...INICIA");
        List<Parametro> parametrosEditor = parametroDAO.getParametroPorPrefijo("EDITOR");
        ArrayList<ParametroEditorVO> parametrosEditorVO = new ArrayList<ParametroEditorVO>();
        if (!parametrosEditor.isEmpty()) {

            for (Parametro p : parametrosEditor) {
                ParametroEditorVO pe = null;
                if (p.getTipoDato().equals("CARACTER")) {
                    pe = new ParametroEditorVO(p.getNombre(), p.getValorCaracter());
                } else if (p.getTipoDato().equals("NUMERICO")) {
                    pe = new ParametroEditorVO(p.getNombre(), p.getValorNumero().toString());
                }
                parametrosEditorVO.add(pe);
            }
            return parametrosEditorVO;
        } else {
            return parametrosEditorVO;
        }

    }

    /**
     * Procesa los resultados del job de calculo de inconsistencias y guarda los resultados en la BD
     *
     * @author felipe.cadena
     * @return
     */
    @Override
    public Boolean guardarInconsistenciasAsincronicas(ProductoCatastralJob job) {

        Boolean resultado = Boolean.TRUE;

        try {

            Tramite tramite = this.tramiteDAO.buscarTramiteSolicitudPorId(job.getTramiteId());
            resultado = this.tramiteInconsistenciaDAO.guardarInconsistenciasAsincronicas(tramite,
                job.getResultado());

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al procesar las inconsistencias asincronicas");
            e.printStackTrace();
            return Boolean.FALSE;
        }

        return resultado;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#obtenerJobsJasperServerEsperando(int)
     */
    /*
     * @modified felipe.cadena 21-03-2017 Se consultan los jobs desde un procedimiento almacenado
     */
    @Override
    public List<ProductoCatastralJob> obtenerJobsJasperServerEsperando(int cantidadJobs) {
        List<ProductoCatastralJob> answer = null;
        try {
            LOGGER.debug("Inicio metodo: obtenerJobsJasperServerEsperando, envio parametros");
            answer = this.procedimientoService.consultarJobsPendientesPorProcesar(
                cantidadJobs,
                ProductoCatastralJob.JPS_ESPERANDO,
                ProductoCatastralJob.JPS_EN_EJECUCION);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerJobsJasperServerEsperando: " + ex.
                getMensaje());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------	
    /**
     *
     * @see
     * co.gov.igac.snc.fachadas.IGeneralesLocal#verificarEjecucionJobJasperServerPendientesEnSNC(java.lang.Long)
     * @autor javier.aponte
     *
     */
    @Override
    public void verificarEjecucionJobJasperServerPendientesEnSNC(Long idJob) {
        LOGGER.debug("verificarEjecucionJobJasperServerPendientesEnSNC con id " + idJob.toString());
        ProductoCatastralJob job = null;
        //versión 1.1.4
        try {
            ///////////////////////////////////////////////////////////////////////////////////////////////
            job = this.productoCatastralJobDAO.findById(idJob);
            LOGGER.debug("Consultar Job: findById");
            LOGGER.debug("job Id:" + idJob);
            boolean resultadoOk = false;
            ///////////////////////////////////////////////////////////////////////////////////////////////
            if (ProductoCatastralJob.SIG_JOB_IMAGEN_FICHA_PREDIAL_DIGITAL.equals(job.
                getTipoProducto())) {
                resultadoOk = this.conservacionService.
                    procesarFichaPredialDigitalEnJasperServer(job);
            } else if (ProductoCatastralJob.TIPO_RESOLUCIONES_CONSERVACION.equals(job.
                getTipoProducto())) {
                int contador = 0;
                LOGGER.debug("GeneralesBean:procesarResolucionesConservacionEnJasperServer:");
                LOGGER.debug("job con id:" + job.getId());

                if (job.getTramiteId() != null) {

                    Tramite tramite = tramiteService.buscarTramitePorId(job.getTramiteId());

                    if (tramite != null &&
                        !tramite.isCancelacionMasiva()) {

                        resultadoOk = this.conservacionService.
                            procesarResolucionesConservacionEnJasperServer(job, contador);
                    }

                }

            } else if (ProductoCatastralJob.TIPO_RESOLUCIONES_ACTUALIZACION.equals(job.
                getTipoProducto())) {
                int contador = 0;
                resultadoOk = this.actualizacionService.
                    procesarResolucionesActualizacionEnJasperServer(job, contador);
            } else if (ProductoCatastralJob.TIPO_REPORTES_ACTUALIZACION.
                equals(job.getTipoProducto())) {
                int contador = 0;
                resultadoOk = this.actualizacionService.procesarReportesActualizacionEnJasperServer(
                    job, contador);
            }

            if (ProductoCatastralJob.TIPO_REPORTES_ACTUALIZACION.equals(job.getTipoProducto())) {
                if (resultadoOk) {
                    this.productoCatastralJobDAO.actualizarEstado(job,
                        ProductoCatastralJob.JPS_TERMINADO);
                } else {
                    this.productoCatastralJobDAO.actualizarEstado(job,
                        ProductoCatastralJob.JPS_ERROR);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            if (job != null) {
                this.productoCatastralJobDAO.actualizarEstado(job, ProductoCatastralJob.JPS_ERROR);
            }
        }
    }

    /**
     * @see IGeneralesLocal#guardarDocumentoFirmaMecanicaUsuario
     */
    @Override
    public Documento guardarDocumentoFirmaMecanicaUsuario(Documento documento) {
        Documento documentoTemp = null;
        try {
            documentoTemp = this.documentoDAO.update(documento);

        } catch (ExcepcionSNC e) {
            LOGGER.debug("guardarDocumentoFirmaMecanicaUsuario:" + e.getMensaje(), e);
            return null;
        } catch (Exception ex) {
            LOGGER.debug("guardarDocumentoFirmaMecanicaUsuario:" + ex.getMessage(), ex);
            return null;
        }

        return documentoTemp;
    }

    /**
     * @see IGeneralesLocal#guardarFirmaMecanicaUsuarioEnGestorDocumental(String, String)
     * @author leidy.gonzalez
     */
    @Override
    public String guardarFirmaMecanicaUsuarioEnGestorDocumental(String path,
        String codigoEstructuraOrganizacional) {
        String rutaWeb = "";
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            rutaWeb = service.cargarDocumentoFirmaMecanicaUsuario(path,
                codigoEstructuraOrganizacional);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#guardarProductoGeneradoEnGestorDocumental: " + ex.
                getMensaje());
        }
        return rutaWeb;
    }

    /**
     * @author leidy.gonzalez
     * @see IGeneralesLocal#eliminarDocumentoFirmaMecanica(documento)
     */
    @Override
    public void eliminarDocumentoFirmaMecanica(Documento documento) {
        this.documentoDAO.delete(documento);
    }

    /**
     * @see IGeneralesLocal#guardarFirmaUsuario
     */
    @Override
    public FirmaUsuario guardarFirmaUsuario(FirmaUsuario firmaUsuario) {
        FirmaUsuario firmaUsuarioTemp = null;
        try {
            firmaUsuarioTemp = this.firmaUsuarioDAO.update(firmaUsuario);

        } catch (ExcepcionSNC e) {
            LOGGER.debug("guardarDocumentoFirmaMecanicaUsuario:" + e.getMensaje(), e);
            return null;
        } catch (Exception ex) {
            LOGGER.debug("guardarDocumentoFirmaMecanicaUsuario:" + ex.getMessage(), ex);
            return null;
        }

        return firmaUsuarioTemp;
    }

    /**
     * @see IGeneralesLocal#buscarFirmaUsuario
     * @author leidy.gonzalez
     */
    @Override
    public Documento buscarFirmaUsuario(Long idDocumento) {

        Documento answer = null;

        try {
            answer = this.documentoDAO.buscarFirmaUsuario(idDocumento);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarFuncionariosPorRolYTerritorial: " +
                ex.getMessage());
        }

        return answer;
    }

    /**
     * @see IGeneralesLocal#consultarUsuarioFirma
     */
    @Override
    public FirmaUsuario consultarUsuarioFirma(String nombreUsuario) {
        FirmaUsuario firmaUsuarioTemp = null;
        try {
            firmaUsuarioTemp = this.firmaUsuarioDAO.consultarUsuarioFirma(nombreUsuario);

        } catch (ExcepcionSNC e) {
            LOGGER.debug("consultarUsuarioFirma:" + e.getMensaje(), e);
            return null;
        } catch (Exception ex) {
            LOGGER.debug("consultarUsuarioFirma:" + ex.getMessage(), ex);
            return null;
        }

        return firmaUsuarioTemp;
    }

    /**
     * @author leidy.gonzalez
     * @see IGeneralesLocal#eliminarFirmaUsuario(firmaUsuario)
     */
    @Override
    public void eliminarFirmaUsuario(FirmaUsuario firmaUsuario) {
        this.firmaUsuarioDAO.delete(firmaUsuario);
    }

    /**
     * @see IGeneralesLocal#buscarDocumentoFirmaPorUsuario(Long, Long)
     * @author leidy.gonzalez
     */
    @Override
    public FirmaUsuario buscarDocumentoFirmaPorUsuario(String nombre) {

        FirmaUsuario firma = null;

        try {
            firma = this.firmaUsuarioDAO.buscarDocumentoFirmaPorUsuario(nombre);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorTramiteIdyTipoDocumento: " + ex.
                getMessage());
        }
        return firma;
    }

    /**
     * @author leidy.gonzalez:: #12528::26/05/2015 Se modifica por CU_187
     * @see IGeneralesLocal#buscarDocumentoPorNumeroDocumentoAndTipoDocumento(String)
     */
    @Override
    public Documento buscarDocumentoPorNumeroDocumentoAndTipoDocumento(String numDocumento) {
        Documento answer = null;
        try {
            answer = this.documentoDAO
                .buscarDocumentoPorNumeroDocumentoAndTipoDocumento(numDocumento);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorNumeroDocumento: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     * @author lorena.salamanca
     * @see co.gov.igac.snc.fachadas.IGeneralesLocal#buscarUOCporTerritorialSinRolEnUOC(String)
     */
    @Override
    public List<EstructuraOrganizacional> buscarUOCporTerritorialSinRolEnUOC(
        String codigoTerritorial, ERol rol) {
        List<EstructuraOrganizacional> uocs = new ArrayList<EstructuraOrganizacional>();
        try {
            List<EstructuraOrganizacional> uocsTemp = this.estructuraOrganizacionalDAOService.
                buscarUOCporCodigoTerritorial(codigoTerritorial);
            EstructuraOrganizacional territorial = buscarEstructuraOrganizacionalPorCodigo(
                codigoTerritorial);
            List<UsuarioDTO> users = LdapDAO.getUsuariosEstructuraRolYUOC(territorial.getNombre().
                toUpperCase(), rol);
            for (EstructuraOrganizacional uoc : uocsTemp) {
                for (UsuarioDTO user : users) {
                    if (user.getDescripcionUOC() != null) {
                        if (!user.getDescripcionUOC().contains(uoc.getNombre().replace(' ', '_').
                            toUpperCase())) {
                            uocs.add(uoc);
                        }
                    }
                }

            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#buscarUOCporTerritorialSinRolEnUOC: " +
                ex.getMensaje());
        }
        return uocs;
    }

    /**
     * @author felipe.cadena
     * @see IGeneralesLocal#buscarDocumentoPorNumeroDocumentoYTipo(String)
     */
    @Override
    public List<Documento> buscarDocumentoPorNumeroDocumentoYTipo(String numeroDoc, Long idTipoDoc) {
        List<Documento> answer = null;
        try {
            answer = this.documentoDAO
                .buscarPorNumeroDocumentoYTipo(numeroDoc, idTipoDoc);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorNumeroDocumentoYTipo: " +
                ex.getMessage());
        }
        return answer;
    }
    
    /**
     * @author hector.arias
     * @see IGeneralesLocal#buscarDocumentoPorTipoActualizacion(String, Long)
     */
    @Override
    public List<Documento> buscarDocumentoPorTipoActualizacion(String numeroDocumento, Long tipoDocumentoId) {
        List<Documento> answer = null;
        try {
            answer = this.documentoDAO
                .buscarDocumentoPorTipoActualizacion(numeroDocumento, tipoDocumentoId);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorNumeroDocumentoYTipo: " +
                ex.getMessage());
        }
        return answer;
    }
    
    /**
     * @author hector.arias
     * @see IGeneralesLocal#buscarDocumentoPorNumeroDocumentoYTipo(String, Long)
     */
    @Override
    public Documento buscarPorNumeroUltimoDocumentoYTipo(String numeroDoc, Long idTipoDoc) {
        Documento answer = null;
        try {
            answer = this.documentoDAO
                .buscarPorNumeroUltimoDocumentoYTipo(numeroDoc, idTipoDoc);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarPorNumeroUltimoDocumentoYTipo: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see IGeneralesLocal#obtenerUltimoJobPorIdTramites(java.lang.Long,java.lang.String)
     */
    @Override
    public List<ProductoCatastralJob> obtenerUltimoJobPorIdTramites(List<Long> idTramites,
        String tipoProducto) {
        List<ProductoCatastralJob> catastralJob = null;

        try {
            catastralJob = this.productoCatastralJobDAO.obtenerUltimoJobPorIdTramites(idTramites,
                tipoProducto);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar los jobs del tramite " + e.getMensaje());
        }

        return catastralJob;
    }

    /**
     * @see IGeneralesLocal#obtenerImagenesPredios(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    public List<ImagenPredioVO> obtenerImageneSimplePredios(String predios, UsuarioDTO usuario) {
        LOGGER.debug("Inicia...obtenerImagenesPredios");
        List<ImagenPredioVO> imagenesPredios = null;
        try {
            imagenesPredios = SigDAOv2.getInstance().obtenerImagenSimplePredio(predios, usuario);
            return imagenesPredios;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("Finaliza...obtenerImagenesPredios");
        }

    }

    /**
     * @see IGeneralesLocal#obtenerImagenesPredios(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public List<ImagenPredioVO> obtenerImagenSimplePredios(String predios, UsuarioDTO usuario) {
        LOGGER.debug("Inicia...obtenerImagenesPredios");
        List<ImagenPredioVO> imagenesPredios = null;
        try {
            imagenesPredios = SigDAOv2.getInstance().obtenerImagenSimplePredio(predios, usuario);
            return imagenesPredios;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("Finaliza...obtenerImagenesPredios");
        }

    }

    /**
     * @see IGeneralesLocal#obtenerImagenesPredios(java.lang.String,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Override
    public List<ImagenPredioVO> obtenerImageneAvanzadaPredios(String predios, UsuarioDTO usuario) {
        LOGGER.debug("Inicia...obtenerImagenesPredios");
        List<ImagenPredioVO> imagenesPredios = null;
        try {
            imagenesPredios = SigDAOv2.getInstance().obtenerImagenAvanzadaPredio(predios, usuario);
            return imagenesPredios;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug("Finaliza...obtenerImagenesPredios");
        }

    }

    /**
     * @see IGenerales#buscarProductosCatastralesDetallePorProductoCatastralId(Long)
     * @author lorena.salamanca
     */
    @Override
    public ProductoCatastralDetalle buscarProductoCatastralesDetallePorId(
        Long productoCatastralDetalleId) {

        ProductoCatastralDetalle answer = null;
        try {
            answer = this.productoCatastralDetalleDAO.findById(productoCatastralDetalleId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * Metodo para obtener y procesar las inconsistencias asociadas a un predio de manera sincronica
     *
     * @param numerosPrediales
     * @param usuario
     * @param tramite
     * @param inconsistenciaControl - Determina si se construye la inconsistencia de control cuando
     * el predio no presenta onconsistencias
     * @param construcciones - Valida sober la capa de construcciones si es true de la contrario
     * valida sobre la capa de terreno
     * @return
     */
    private List<TramiteInconsistencia> procesarInconsistenciasGeograficasPrediosSync(
        String numerosPrediales, UsuarioDTO usuario,
        Tramite tramite, boolean inconsistenciaControl, boolean construcciones) {

        List<TramiteInconsistencia> result = new ArrayList<TramiteInconsistencia>();
        HashMap<String, String> serviceResult = new HashMap<String, String>();
        boolean noInconsistencias = true;

        if (numerosPrediales.charAt(numerosPrediales.length() - 1) == ',') {
            numerosPrediales = numerosPrediales.substring(0, numerosPrediales.length() - 1);
        }

        if (construcciones) {
            serviceResult = SigDAOv2.getInstance().validarGeometriaUnidadesConstruccion(
                numerosPrediales.toString(), usuario);
        } else {
            serviceResult = SigDAOv2.getInstance().validarInconsistencias(numerosPrediales.
                toString(), usuario);
        }

        for (String key : serviceResult.keySet()) {

            String inconsistencias = serviceResult.get(key);

            if (inconsistencias != null && !inconsistencias.isEmpty()) {

                String[] splitInc = inconsistencias.split(",");

                for (String string : splitInc) {
                    for (EInconsistenciasGeograficas eig : EInconsistenciasGeograficas.values()) {

                        if (eig.getCodigoSig().equals(string)) {
                            noInconsistencias = false;
                            TramiteInconsistencia ti = new TramiteInconsistencia();

                            ti.setFechaLog(new Date());
                            ti.setUsuarioLog(usuario.getLogin());
                            ti.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
                            ti.setTramite(tramite);
                            ti.setDepurada(ESiNo.NO.getCodigo());
                            ti.setInconsistencia(eig.getNombre());
                            ti.setNumeroPredial(key);

                            result.add(ti);
                        }

                    }
                }

            }

        }

        //Si el tramite no presento inconsistencias se ingresa un registro e control para validaciones
        if (noInconsistencias && inconsistenciaControl) {
            TramiteInconsistencia ti = new TramiteInconsistencia();

            ti.setFechaLog(new Date());
            ti.setUsuarioLog(usuario.getLogin());
            ti.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
            ti.setTramite(tramite);
            ti.setDepurada(ESiNo.NO.getCodigo());
            ti.setInconsistencia(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre());
            ti.setNumeroPredial("");

            result.add(ti);
        }
        return result;
    }

    /**
     * @see IGenerales#buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(Long)
     * @author david.cifuentes
     */
    @Override
    public ProductoCatastralJob buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(
        Long idProductoCatastralDetalle) {

        ProductoCatastralJob answer = null;
        try {
            answer = this.productoCatastralJobDAO.
                buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(
                    idProductoCatastralDetalle);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGenerales#buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(String)
     * @author david.cifuentes
     */
    @Override
    public PlantillaSeccionReporte buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo) {

        PlantillaSeccionReporte answer = null;
        try {
            answer = this.plantillaSeccionReporteDAO
                .buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(
                    estructuraOrganizacionalCodigo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGenerales#guardarPlantillaSeccionReporte(PlantillaSeccionReporte)
     * @author david.cifuentes
     */
    @Override
    public PlantillaSeccionReporte guardarPlantillaSeccionReporte(
        PlantillaSeccionReporte plantilla) {

        PlantillaSeccionReporte answer = null;
        try {
            answer = this.plantillaSeccionReporteDAO.update(plantilla);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGenerales#obtenerNumerosPredialesEntreRangoPredial(String, String, boolean)
     * @author felipe.cadena
     */
    @Override
    public List<String> obtenerNumerosPredialesEntreRangoPredial(String nPredialInicial,
        String nPredialFinal, boolean completo) {

        List<String> rangosPredios = null;

        try {
            rangosPredios = this.predioDAO.
                getNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal(
                    nPredialInicial, nPredialFinal, false);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return rangosPredios;
    }

    /**
     * @see IGenerales#buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(String)
     * @author david.cifuentes
     * @throws Exception
     */
    @Override
    public List<TramiteDocumento> consultaTramiteDocumentosPorDocumentoId(
        Long documentoId) {

        List<TramiteDocumento> answer = null;

        try {

            answer = this.tramiteDocumentoDao.consultaTramiteDocumentosPorDocumentoId(documentoId);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGeneralesLocal#buscarEstructuraOrganizacionalByNombre(String,String)
     * @author dumar.penuela
     */
    @Override
    public List<EstructuraOrganizacional> buscarEstructuraOrganizacionalByNombre(String oUC,
        String territorial) {

        LOGGER.debug("en BloqueoEntidades#buscarEstructuraOrganizacionalByNombre");
        List<EstructuraOrganizacional> estructuraOrganizacionales = null;

        try {
            estructuraOrganizacionales = this.estructuraOrganizacionalDAOService.
                buscarEstructuraOrganizacionalByNombre(
                    oUC, territorial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en BloqueoEntidades#buscarEstructuraOrganizacionalByNombre: " +
                ex.getMensaje());
        }
        return estructuraOrganizacionales;
    }

    /**
     * @author dumar.penuela
     * @see IGeneralesLocal#validaMunicipioDepartamento(String,String)
     */
    @Override
    public Boolean validaMunicipioDepartamento(String departamentoId, String municipioId) {
        Boolean answer = false;
        try {
            answer = this.municipioDAO.validaMunicipioDepartamento(departamentoId, municipioId);

        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorNumeroDocumentoYTipo: " +
                ex.getMessage(), ex);
        }
        return answer;
    }

    /**
     * @see IGenerales#buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(String)
     * @author david.cifuentes
     * @throws Exception
     */
    @Override
    public void verificarUrlSeccionesPlantillaReportes() throws Exception {
        try {
            String validarUrlPlantillaReportes = this.parametroDAO.getParametroPorNombre(
                EParametro.VALIDACION_URLS_PLANTILLA_SECCION_REPORTE.toString()).getValorCaracter();
            boolean generarUrls = this.plantillaSeccionReporteDAO.
                verificarUrlSeccionesPlantillaReportes();

            if (generarUrls) {
                List<PlantillaSeccionReporte> plantillas = this.plantillaSeccionReporteDAO.findAll();
                if (plantillas != null && !plantillas.isEmpty()) {
                    this.publicarUrlsPlantillas(plantillas);
                }
            } else if (validarUrlPlantillaReportes != null && ESiNo.SI.getCodigo().equals(
                validarUrlPlantillaReportes)) {
                List<PlantillaSeccionReporte> plantillas = this.plantillaSeccionReporteDAO.findAll();
                if (plantillas != null && !plantillas.isEmpty()) {

                    // Verificación de existencia de las URL's
                    boolean validarUrls = this.validarUrls(plantillas);
                    if (!validarUrls) {
                        this.publicarUrlsPlantillas(plantillas);
                    }
                }

            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * Método que realiza la publicación de las urls para los archivos de las secciones que han sido
     * guardados en el FTP
     *
     * @author david.cifuentes
     * @param plantillas
     * @throws Exception
     */
    private void publicarUrlsPlantillas(List<PlantillaSeccionReporte> plantillas) throws Exception {

        for (PlantillaSeccionReporte plantilla : plantillas) {

            // Cabecera
            if (plantilla.getRutaCabecera() != null && !(plantilla.getRutaCabecera()).isEmpty()) {

                // Descarga del FTP
                String rutaTempLocal = this.descargarArchivoDeAlfrescoATemp(plantilla.
                    getRutaCabecera());

                if (rutaTempLocal == null || rutaTempLocal.isEmpty()) {
                    LOGGER.error("Ocurrió un error al descargar el documento de alfresco: " +
                        "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                    throw (new Exception(
                        "Ocurrió un error al descargar el documento de alfresco: " +
                        "descargarArchivoDeAlfrescoATemp retorno null o vacio"));
                }

                // Publicación en web
                String urlTempWeb = this.publicarArchivoEnWeb(rutaTempLocal);
                plantilla.setUrlCabecera(urlTempWeb);
            } else {
                plantilla.setUrlCabecera(null);
            }

            // Marca de agua
            if (plantilla.getRutaMarcaDeAgua() != null && !(plantilla.getRutaMarcaDeAgua()).
                isEmpty()) {

                // Descarga del FTP
                String rutaTempLocal = this.descargarArchivoDeAlfrescoATemp(plantilla.
                    getRutaMarcaDeAgua());

                if (rutaTempLocal == null ||
                    rutaTempLocal.isEmpty()) {
                    LOGGER.error("Ocurrió un error al descargar el documento de alfresco: " +
                        "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                    throw (new Exception(
                        "Ocurrió un error al descargar el documento de alfresco: " +
                        "descargarArchivoDeAlfrescoATemp retorno null o vacio"));
                }

                // Publicación en web
                String urlTempWeb = this
                    .publicarArchivoEnWeb(rutaTempLocal);
                plantilla.setUrlMarcaDeAgua(urlTempWeb);
            } else {
                plantilla.setUrlMarcaDeAgua(null);
            }

            // Pie de página
            if (plantilla.getRutaPieDePagina() != null && !(plantilla.getRutaPieDePagina()).
                isEmpty()) {

                // Descarga del FTP
                String rutaTempLocal = this.descargarArchivoDeAlfrescoATemp(plantilla.
                    getRutaPieDePagina());

                if (rutaTempLocal == null ||
                    rutaTempLocal.isEmpty()) {
                    LOGGER.error("Ocurrió un error al descargar el documento de alfresco: " +
                        "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                    throw (new Exception(
                        "Ocurrió un error al descargar el documento de alfresco: " +
                        "descargarArchivoDeAlfrescoATemp retorno null o vacio"));
                }

                // Publicación en web
                String urlTempWeb = this
                    .publicarArchivoEnWeb(rutaTempLocal);
                plantilla.setUrlPieDePagina(urlTempWeb);
            } else {
                plantilla.setUrlPieDePagina(null);
            }
        }

        // Actualización de las plantillas
        this.transaccionalService.actualizarPlantillaSeccionReportesNuevaTransaccion(plantillas);
    }

    /**
     * Método que realiza la validación de la existencia de las urls publicadas.
     *
     * @author david.cifuentes
     * @param plantillas
     * @return
     * @throws Exception
     */
    private boolean validarUrls(List<PlantillaSeccionReporte> plantillas) throws Exception {
        try {
            for (PlantillaSeccionReporte plantilla : plantillas) {

                // Validación Cabecera
                if (plantilla.getUrlCabecera() != null) {
                    URL u = new URL(plantilla.getUrlCabecera());
                    HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                    huc.setRequestMethod("GET");
                    huc.connect();

                    if (huc.getResponseCode() == 404) {
                        return false;
                    }
                }

                // Validación Marca de agua
                if (plantilla.getUrlMarcaDeAgua() != null) {
                    URL u = new URL(plantilla.getUrlMarcaDeAgua());
                    HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                    huc.setRequestMethod("GET");
                    huc.connect();

                    if (huc.getResponseCode() == 404) {
                        return false;
                    }
                }

                // Validación Pie de página
                if (plantilla.getUrlPieDePagina() != null) {
                    URL u = new URL(plantilla.getUrlPieDePagina());
                    HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                    huc.setRequestMethod("GET");
                    huc.connect();

                    if (huc.getResponseCode() == 404) {
                        return false;
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("ERROR - Urls de plantillas de reportes no válidas: " + e.getMessage());
            return false;
        }
        return true;
    }

    //---------------------------------------------- // 
    /**
     * @see IGenerales#buscarEstructuraOrgPorMunicipioCod(String)
     * @author david.cifuentes
     */
    @Override
    public String buscarEstructuraOrgPorMunicipioCod(
        String codigo) {
        String answer = null;
        try {
            answer = this.estructuraOrganizacionalDAOService.buscarEstructuraOrgPorMunicipioCod(
                codigo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGenerales#getMunicipioCodsByEstructuraOrganizacionalCod(String)
     * @author david.cifuentes
     */
    @Override
    public List<Municipio> findMunicipiosbyCodigoEstructuraOrganizacional(
        String codigo) {
        List<Municipio> answer = null;
        try {
            answer = this.jurisdicionService.findMunicipiosbyCodigoEstructuraOrganizacional(codigo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------- // 
    /**
     * @see IGenerales#consultarGrupoPorEstadoActivo(String)
     * @author david.cifuentes
     */
    @Override
    public List<Grupo> consultarGrupoPorEstadoActivo(String activo) {
        List<Grupo> answer = null;
        try {
            answer = this.grupoDAO.consultarGrupoPorEstadoActivo(activo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGenerales#buscarGrupos()
     * @author david.cifuentes
     */
    @Override
    public List<Grupo> buscarGrupos() {
        List<Grupo> answer = null;
        try {
            answer = this.grupoDAO.buscarGrupos();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------- // 
    /**
     * @see IGenerales#buscarTodasTerritoriales()
     * @author david.cifuentes
     */
    @Override
    public List<EstructuraOrganizacional> buscarTodasTerritoriales() {
        List<EstructuraOrganizacional> answer = null;
        try {
            answer = this.estructuraOrganizacionalDAOService.findAll();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ---------------------------------------------- //
    /**
     * @see IGenerales#guardarGrupo(Grupo)
     * @author david.cifuentes
     */
    @Override
    public void guardarGrupo(Grupo rolSeleccionado) {
        try {
            this.grupoDAO.update(rolSeleccionado);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IGeneralesLocal#guardarReporteGeneradoEnGestorDocumental(String, String)
     * @author leidy.gonzalez
     */
    @Override
    public String guardarReporteGeneradoEnGestorDocumental(String rutaArchivoLocal, String tipo,
        String tipoReporte,
        String codigoOrganizacional, String departamento, String municipio, String codigoReporte,
        String anio) {
        String rutaWeb = "";
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            rutaWeb = service.cargarDocumentoReportes(rutaArchivoLocal, tipo, tipoReporte,
                codigoOrganizacional, departamento, municipio, codigoReporte, anio);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#guardarReporteGeneradoEnGestorDocumental: " + ex.
                getMensaje(), ex);
        }
        return rutaWeb;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#obtenerCodigoHomologadoNombreTerritorial(String)
     * @author david.cifuentes
     */
    @Override
    public String obtenerCodigoHomologadoNombreTerritorial(String codTerritorial) {
        String nombreHomologado = null;
        try {
            nombreHomologado = this.codigoHomologadoDAO
                .obtenerCodigoHomologado(
                    ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
                    ESistema.SNC, ESistema.LDAP, codTerritorial,
                    new Date());
        } catch (ExcepcionSNC e) {
            LOGGER.info(e.getMensaje());
        }
        return (nombreHomologado == null) ? codTerritorial : nombreHomologado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IGeneralesLocal#obtenerComponentePorNombre(String)
     * @author felipe.cadena
     */
    @Override
    public Componente obtenerComponentePorNombre(String nombre) {
        Componente resultado = null;
        try {
            resultado = this.componenteDAO.obtenerComponentePorNombre(nombre);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("error en GeneralesBean#obtenerComponentePorNombre: " + ex.getMensaje(), ex);
        }
        return resultado;
    }

    /**
     * @see IGenerales#getCacheDominioPorNombreOrdenadoPorId(EDominio)
     */
    @Override
    public List<Dominio> getCacheDominioPorNombreOrdenadoPorId(Enum d) {
        return this.dominioDAO.buscarNombreOrdenadoPorId(d);
    }

    /**
     * Metodo que obtiene una lista de los Nombres a visualizar de los tipos de cisrculos
     * registrales
     *
     * @author leidy.gonzalez
     */
    @Override
    public List<CirculoRegistral> obtenerListaCirculoRegistral() {
        List<CirculoRegistral> answer = null;
        try {
            answer = this.circulosRegistralesService.findAll();
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("Error en GeneralesBean#obtenerTodosGrupoProductos: " + ex.getMensaje(), ex);
        }
        return answer;
    }

    /**
     * @see IGeneralesLocal#obtenerListaTipoReportePorCategoria()
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public List<RepReporte> obtenerListaTipoReportePorTipoReporte(String categoria) {

        List<RepReporte> answer = null;
        try {
            answer = this.repReporteDAO.obtenerListaTipoReportePorTipoReporte(categoria);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    /**
     * Metodo que obtiene una lista de los nombres de los tipos de reportes prediales
     *
     * @author leidy.gonzalez
     */
    @Override
    public List<String> obtenerListaNombreTipoReporte() {
        List<String> answer = null;
        try {
            answer = this.repReporteDAO.obtenerListaNombreTipoReporte();
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en GeneralesBean#obtenerTodosGrupoProductos: " +
                ex.getMensaje(), ex);
        }
        return answer;
    }

    /**
     * Metodo que obtiene una lista de los tipos de reportes prediales
     *
     * @author leidy.gonzalez
     */
    @Override
    public List<RepReporte> consultarListaTipoReportePorNombre(List<String> nombresTiposReportes) {
        List<RepReporte> answer = null;
        try {
            answer = this.repReporteDAO.consultarListaTipoReportePorNombre(nombresTiposReportes);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("Error en GeneralesBean#obtenerTodosGrupoProductos: " + ex.getMensaje(), ex);
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see IGenerales#obtenerRepTerritorialReporte(java.lang.String)
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public List<Departamento> obtenerRepTerritorialReporte(String codigoTerritorial,
        String codigoUoc) {

        List<Departamento> answer = null;

        try {
            answer = this.repTerritorialDao.obtenerRepTerritorialReporte(codigoTerritorial,
                codigoUoc);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en GeneralesBean#obtenerRepTerritorialReporte: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * Ajuste para reportes tipo csv
     *
     * @author felipe.cadena
     * @param archivoOrigen
     * @param formato
     * @return
     */
    private String cambiarExtensionReporte(File archivoOrigen, String formato) {
        String ruta = archivoOrigen.getPath() == null ? "" : archivoOrigen.getPath();
        if (!ruta.isEmpty() && formato != null && !formato.isEmpty()) {
            String ext = ruta.toLowerCase().substring(ruta.lastIndexOf("."));
            if (".null".equals(ext)) {
                ruta = ruta.substring(0, ruta.lastIndexOf(".")) + "." + formato.toLowerCase();
            }
            File file = new File(ruta);
            archivoOrigen.renameTo(file);
        }

        return ruta;
    }

    /**
     * @see IGeneralesLocal#buscarDocumentosPorTramiteIdyTiposDocumento(Long, Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<Documento> buscarDocumentosPorTramiteIdyTiposDocumento(Long tramiteId,
        List<Long> idTipoDocumentos) {

        List<Documento> doc = null;

        try {
            doc = this.documentoDAO.buscarDocumentosPorTramiteIdyTiposDocumento(tramiteId,
                idTipoDocumentos);
        } catch (Exception ex) {
            LOGGER.error("error en GeneralesBean#buscarDocumentoPorTramiteIdyTipoDocumento: " + ex.
                getMessage());
        }
        return doc;
    }

    /**
     * @see IGenerales#findByNombreOrderByValor(EDominio)
     * @author felipe.cadena
     */
    @Override
    public List<Dominio> findByNombreOrderByValor(Enum d) {
        return this.dominioDAO.findByNombreOrderByValor(d);
    }
    /**
     * @see IGeneralesLocal#reenviarJobsProductosCatastrales(ProductoCatastralJob, String, String)
     * @author hector.arias
     */
    @Override
    public void reenviarJobsProductosCatastrales(ProductoCatastralJob job, String nuevoEstado,
        String nuevoResultado) {

        if (ProductoCatastralJob.AGS_ERROR.equals(job.getEstado())) {
            this.productoCatastralJobDAO.actualizar(job, ProductoCatastralJob.AGS_ESPERANDO, job.
                getResultado());
        } else if (ProductoCatastralJob.JPS_ERROR.equals(job.getEstado())) {
            this.productoCatastralJobDAO.actualizar(job, ProductoCatastralJob.JPS_ESPERANDO, job.
                getResultado());
        } else if (ProductoCatastralJob.SNC_ERROR.equals(job.getEstado())) {
            this.productoCatastralJobDAO.actualizar(job, ProductoCatastralJob.AGS_TERMINADO, job.
                getResultado());
        }
    }


    /**
     * @see IGeneralesLocal#buscarMunicipiosSNCNoDelegada()
     * @author felipe.cadena
     */
    @Override
    public List<Municipio> buscarMunicipiosSNCNoDelegada() {
        List<Municipio> resultado = null;
        try {
            resultado = this.municipioDAO.buscarMunicipiosSNCNoDelegada();
        } catch (ExcepcionSNC ex) {
            LOGGER.
                    error("error en GeneralesBean#buscarMunicipiosSNCNoDelegada: " + ex.getMensaje(), ex);
        }
        return resultado;
    }

    /**
     * @see IGeneralesLocal#determinarReporteNacional(String, Long)
     * @author felipe.cadena
     */
    public boolean determinarReporteNacional(String usuario, Long reporteId){

        boolean resultado = false;
        try {
            resultado = this.repUsuarioRolReporteDAO.determinarNacional(usuario, reporteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                    error("error en GeneralesBean#determinarReporteNacional: " + ex.getMensaje(), ex);
        }
        return resultado;

    }

     /**
     * @see IGenerales#obtenerListaNombresGrupo(String)
     * @author juan.cruz
     */
    @Override
    public List<String> obtenerListaNombresGrupo(String activo) {
        List<String> answer = null;
        try {
            answer = this.grupoDAO.obtenerListaNombresGrupo(activo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
    
    
    /**
     * MÃ©todo para obtener una lista de parametros a partir de los resultados de un job para el 
     * Certificado Plano Predial
     * @author leidy.gonzalez
     */
    public List<String> obtenerParametrosDeProductoCatastralJobCPP(ProductoCatastralJob productoGenerado ){
    	List<String> resultados = new  ArrayList<String>();
		try {
			if (productoGenerado.getResultado().contains("<") ){
				LOGGER.debug(productoGenerado.getResultado());
				Document document = DocumentHelper.parseText(productoGenerado.getResultado());
				List productos = document.selectNodes( "//Imagen" );
                
				for ( Iterator iter = productos.iterator(); iter.hasNext(); ) {
			      Element productoXml = (Element) iter.next();
                  
			      resultados.add(productoXml.attributeValue("code"));  
			    }
				
			}else{
				String message = "OcurriÃ³ un error durante el procesamiento del certificado plano.  No se obtuvieron Resultados";
				throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null, message);
			} 
            } catch (DocumentException e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.getMessage());
		}
		return resultados;
	}
    
    /**
     * Método para obtener una lista de parametros a partir de los resultados de un job para la
     * validación geográfica de actualización
     * @author hector.arias
     */
    public List<String> obtenerParametrosDeProductoCatastralJobActualizacion(ProductoCatastralJob productoGenerado ){
    	List<String> resultados = new  ArrayList<String>();
        try {
			if (productoGenerado.getResultado().contains("<") ){
				LOGGER.debug(productoGenerado.getResultado());
				Document document = DocumentHelper.parseText(productoGenerado.getResultado());
				Document documentCodigo = DocumentHelper.parseText(productoGenerado.getCodigo());
				List productZonas = document.selectNodes( "//Response//Results//VALIDACION_DE_ZONAS" );
				List productoPredios = document.selectNodes( "//Response//Results//VALIDACION_DE_PREDIOS" );
				List productosUbicacionFile = document.selectNodes( "//Response//Results//ubicacionArchivoValidacion" );
				List productosMunicipio = documentCodigo.selectNodes( "//SigJob//parameters//parameter//codigoMunicipio" );
				List productosDepartamento = documentCodigo.selectNodes( "//SigJob//parameters//parameter//codigoDepartamento" );
				List productosUsuario = documentCodigo.selectNodes( "//SigJob//parameters//parameter//value" );
				List productosValidoZonas = documentCodigo.selectNodes( "//SigJob//parameters//parameter//validarZonas" );
				List productosValidoPredios = documentCodigo.selectNodes( "//SigJob//parameters//parameter//validarPredios" );
                
				for ( Iterator iter = productZonas.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productoPredios.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productosUbicacionFile.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productosMunicipio.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productosDepartamento.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productosUsuario.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productosValidoZonas.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                for ( Iterator iter = productosValidoPredios.iterator(); iter.hasNext(); ) {
                                    Element productoXml = (Element) iter.next();
                                    resultados.add(productoXml.getText());
                                }
                                
			}else{
				String message = "Ocurrió un error durante el procesamiento de la validación geográfica de actualización.";
				throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null, message);
			} 
                        
                        this.actualizacionService.guardarRegistroArchivoInconsistenciasActualizacionGeo(resultados);
                        
            } catch (DocumentException e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.getMessage());
		}
		return resultados;
                
                
	}
    
    
    /**
     * Elimina las inconsistencias de un tramite
     *
     * @author vsocarras
     * @return
     */
    @Override
    public void eliminarInconsistenciasPorTramite(Long idTramite) {

       

        try {

        	this.tramiteInconsistenciaDAO.eliminarInconsistenciasPorTramite(idTramite);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al eliminar las inconsistencias asincronicas");
            e.printStackTrace();
         
        }

        
    }
    
    
    //end of class
}
