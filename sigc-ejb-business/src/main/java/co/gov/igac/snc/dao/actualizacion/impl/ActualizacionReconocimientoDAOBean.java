package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionReconocimientoDAO;
import co.gov.igac.snc.dao.generales.IManzanaVeredaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionReconocimiento;
import co.gov.igac.snc.persistence.entity.actualizacion.SaldoConservacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionReconocimiento.
 *
 * @author javier.barajas
 */
@Stateless
public class ActualizacionReconocimientoDAOBean extends GenericDAOWithJPA<ActualizacionReconocimiento, Long>
    implements IActualizacionReconocimientoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionReconocimientoDAOBean.class);

    /**
     * @see IActualizacionReconocimientoDAO#getActualizacionReconocimientoByActualizacionId(String,
     * int...)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoByActualizacionId(
        Long actualizacionId) {
        List<ActualizacionReconocimiento> actReconocimientos = null;

        String query = "SELECT ar FROM ActualizacionReconocimiento ar " +
            " WHERE ar.actualizacion.id = :actualizacionId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("actualizacionId", actualizacionId);

            actReconocimientos = (List<ActualizacionReconocimiento>) q.getResultList();

        } catch (Exception e) {
            LOGGER.error("Error en SaldoConservacionDAOBean: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return actReconocimientos;

    }

    /**
     * @see IActualizacionReconocimientoDAO#getActualizacionReconocimientoByActualizacionId(String,
     * int...)
     * @author javier.barajas
     */
    @Override
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoByActualizacionId(
        Long actualizacionId, int... rowStartIdxAndCount) {
        LOGGER.debug(
            "IActualizacionReconocimientoDAO#getActualizacionReconocimientoByActualizacionId...INICIA ");

        List<ActualizacionReconocimiento> manzanasMunicipio;
        String stringQuery = "SELECT ar FROM ActualizacionReconocimiento ar " +
            " WHERE ar.actualizacion.id = :actualizacionId";

        Query query = this.getEntityManager().createQuery(stringQuery);
        query.setParameter("actualizacionId", actualizacionId);

        //manzanasMunicipio = (List<ManzanaVereda>)query.getResultList();		
        LOGGER.debug(
            "ManzanaVeredaDAOBean#obtenerManazanasporMunicipioAsignarCoordinadorPaginacion...FINALIZA ");
        try {
            manzanasMunicipio = (super.findInRangeUsingQuery(
                query, rowStartIdxAndCount));
            for (ActualizacionReconocimiento ar : manzanasMunicipio) {
                ar.getRecursoHumano();
            }
            return manzanasMunicipio;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }

    }

    @Override
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoporRecursoHumanoId(
        Long recursoHumanoId) {
        LOGGER.debug(
            "IActualizacionReconocimientoDAO#getActualizacionReconocimientoporRecursoHumanoId...INICIA ");

        List<ActualizacionReconocimiento> manzanasMunicipio;
        String stringQuery = "SELECT ar FROM ActualizacionReconocimiento ar " +
            " WHERE ar.recursoHumano.id = :recursoHumanoId";
        try {
            Query query = this.getEntityManager().createQuery(stringQuery);
            query.setParameter("recursoHumanoId", recursoHumanoId);

            manzanasMunicipio = (List<ActualizacionReconocimiento>) query.getResultList();

            return manzanasMunicipio;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see IActualizacionReconocimientoDAO#getActualizacionReconocimientoByRecursoHumanoId(Long)
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ActualizacionReconocimiento> getActualizacionReconocimientoByRecursoHumanoId(
        Long recursoHumanoId) {
        LOGGER.debug(
            "ActualizacionReconocimientoDAOBean#getActualizacionReconocimientoByRecursoHumanoId...INICIA");
        try {
            String stringQuery =
                "SELECT ar FROM ActualizacionReconocimiento ar join ar.recursoHumano rh WHERE rh.id=:recursoHumanoID";
            Query query = this.getEntityManager().createQuery(stringQuery);
            query.setParameter("recursoHumanoID", recursoHumanoId);
            List<ActualizacionReconocimiento> listaDeActualizacionReconocimientoDeUnCordinador;
            listaDeActualizacionReconocimientoDeUnCordinador =
                (List<ActualizacionReconocimiento>) query.getResultList();
            return listaDeActualizacionReconocimientoDeUnCordinador;

        } catch (Exception e) {
            LOGGER.debug(
                "Error recuperandola actulizacion reconocimiento por el id del recurso humano", e.
                    getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, e, e.
                getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionReconocimientoDAOBean#getActualizacionReconocimientoByRecursoHumanoId...FINALIZA");
        }

    }

}
