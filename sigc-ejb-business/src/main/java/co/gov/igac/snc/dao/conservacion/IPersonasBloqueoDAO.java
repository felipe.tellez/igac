/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;

/**
 *
 * @author juan.agudelo
 *
 * @deprecated la que se debe usar es IPersonaBloqueoDAO que es la que cumple con el estándar de
 * nombramiento para la tabla PersonaBloqueo
 */
@Local
@Deprecated
public interface IPersonasBloqueoDAO extends
    IGenericJpaDAO<PersonaBloqueo, Long> {

    /**
     * Método que sirve para desbloquear una persona
     *
     * @author juan.agudelo
     * @param usuario
     * @param personasBloqueadas
     * @return
     */
    public List<PersonaBloqueo> actualizarPersonasDesbloqueo(
        UsuarioDTO usuario, List<PersonaBloqueo> personasBloqueadas);

    /**
     * Actualiza una lista de personas bloqueadas solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public List<PersonaBloqueo> actualizarPersonaBloqueoM(UsuarioDTO usuario,
        ArrayList<PersonaBloqueo> bloqueoMasivo);

    /**
     * Actualiza una lista de personas bloqueadas solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public ResultadoBloqueoMasivo bloquearMasivamentePersonas(
        UsuarioDTO usuario, ArrayList<PersonaBloqueo> bloqueoMasivo);

}
