package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.ICriterioControlCalidadDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.CriterioControlCalidad;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto ControlCalidadCriterio.
 *
 * @author javier.barajas
 */
@Stateless
public class CriterioControlCalidadDAOBean extends GenericDAOWithJPA<CriterioControlCalidad, Long>
    implements ICriterioControlCalidadDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(CriterioControlCalidadDAOBean.class);

    /**
     * @see ICriterioControlCalidad#consultarCriterioControlCalidad(Long)
     * @author javier.barajas
     */
    @Override
    public CriterioControlCalidad consultarCriterioControlCalidad(
        Long criterioControlCalidadId) {
        LOGGER.debug("obtenerAsistenteEventoporActualizacionEvento");

        CriterioControlCalidad answer = null;
        try {
            Query q = entityManager
                .createQuery("SELECT ccc" +
                    " FROM CriterioControlCalidad ccc" +
                    " WHERE ccc.id= :criterioControlCalidadId");
            q.setParameter("criterioControlCalidadId", criterioControlCalidadId);

            answer = (CriterioControlCalidad) q.getSingleResult();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

}
