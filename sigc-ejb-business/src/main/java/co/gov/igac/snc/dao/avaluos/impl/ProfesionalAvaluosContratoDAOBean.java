package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluosContratoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IProfesionalAvaluosContratoDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ProfesionalAvaluosContratoDAOBean extends
    GenericDAOWithJPA<ProfesionalAvaluosContrato, Long> implements IProfesionalAvaluosContratoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfesionalAvaluosContrato.class);

    /**
     * @see IProfesionalAvaluoDAO#obtenerByIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalAvaluosContrato> consultarAnterioresPorIdProfesional(Long idProfesional) {
        LOGGER.debug("Inicio ProfesionalAvaluosContratoDAOBean#obtenerAnterioresPorIdProfesional");

        List<ProfesionalAvaluosContrato> result = new ArrayList<ProfesionalAvaluosContrato>();

        String queryString;
        Query query;

        queryString = "SELECT DISTINCT pac " +
            " FROM ProfesionalAvaluosContrato pac " +
            " WHERE pac.profesionalAvaluo.id = :idProfesionalAvaluo " +
            " AND pac.activo = :activo";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idProfesionalAvaluo", idProfesional);
            query.setParameter("activo", ESiNo.NO.getCodigo());

            result = (List<ProfesionalAvaluosContrato>) query.getResultList();

            for (ProfesionalAvaluosContrato profesionalAvaluosContrato : result) {
                this.inicializarAtributos(profesionalAvaluosContrato);
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalAvaluosContratoDAOBean#obtenerAnterioresPorIdProfesional");
        return result;
    }

    /**
     * @see IProfesionalAvaluosContratoDAO#obtenerPorIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Override
    public ProfesionalAvaluosContrato obtenerPorIdConAtributos(Long idContrato) {

        ProfesionalAvaluosContrato contrato = new ProfesionalAvaluosContrato();

        try {
            contrato = this.findById(idContrato);

            if (contrato != null) {
                this.inicializarAtributos(contrato);
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        LOGGER.debug("Fin ProfesionalAvaluosContratoDAOBean#obtenerAnterioresPorIdProfesional");
        return contrato;
    }

    /**
     * Inicializa los atributos relacionados con el objeto {@link ProfesionalAvaluosContrato}
     *
     * @author christian.rodriguez
     * @param contrato
     */
    private void inicializarAtributos(ProfesionalAvaluosContrato contrato) {
        Hibernate.initialize(contrato.getProfesionalAvaluo());
        Hibernate.initialize(contrato.getProfesionalContratoAdicions());
        Hibernate.initialize(contrato.getProfesionalContratoCesions());
        Hibernate.initialize(contrato.getProfesionalContratoPagos());
        Hibernate.initialize(contrato.getInterventor());
    }

}
