/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ReporteActualizacion;
import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 *
 * @author javier.aponte
 */
public interface IReporteActualizacionDAO extends IGenericJpaDAO<ReporteActualizacion, Long> {

    /**
     * Consulta los registros de la tabla AX_REPORTE_ACTUALIZACION por el código del municipio
     *
     * @author javier.aponte
     *
     * @param codigoMunicipio Código del municipio
     * @return Lista con registros de la tabla AX_REPORTE_ACTUALIZACION
     */
    public List<Documento> buscarReportesActualizacionPorCodigoMunicipio(
        String codigoMunicipio, String vigencia);
}
