/**
 *
 */
package co.gov.igac.snc.dao.sig.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author juan.mendez
 *
 */
public class ArcGisServerResultParam implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3662319964152772599L;
    /**
     *
     */

    private String paramUrl;

    public String getParamUrl() {
        return paramUrl;
    }

    public void setParamUrl(String paramUrl) {
        this.paramUrl = paramUrl;
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
