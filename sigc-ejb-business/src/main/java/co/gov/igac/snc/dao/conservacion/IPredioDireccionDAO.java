package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;

@Local
public interface IPredioDireccionDAO extends IGenericJpaDAO<PredioDireccion, Long> {

    /**
     * Método encargado de buscar las direcciones asociadas a un predio.
     *
     * @param predio Predio asociado a las direcciones a obtener.
     * @return Retorna la Lista de direcciones asociadas a un predio
     */
    public List<PredioDireccion> getPredioDireccionByPredio(Predio predio);
}
