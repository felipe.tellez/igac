/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoEvaluacionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacionDetalle;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoEvaluacionDAO
 *
 * @author felipe.cadena
 */
@Stateless
public class AvaluoEvaluacionDAOBean extends
    GenericDAOWithJPA<AvaluoEvaluacion, Long> implements
    IAvaluoEvaluacionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoEvaluacionDAOBean.class);

    /**
     * @see IControlCalidadAvaluoDAO#consultarPorAvaluoAsignacion(Long)
     * @author felipe.cadena
     */
    @Override
    public AvaluoEvaluacion consultarPorAvaluoAsignacionConAtributos(
        Long idAvaluo, Long idAsignacion) {

        AvaluoEvaluacion resultado = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT ae " +
                " FROM AvaluoEvaluacion ae " +
                " WHERE ae.avaluoId = :idAvaluo " +
                " AND ae.asignacionId = :idAsignacion ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);
            query.setParameter("idAsignacion", idAsignacion);

            resultado = (AvaluoEvaluacion) query.getSingleResult();
            if (resultado != null) {
                Hibernate.initialize(resultado.getAvaluoEvaluacionDetalles());
                for (AvaluoEvaluacionDetalle aed : resultado.getAvaluoEvaluacionDetalles()) {
                    Hibernate.initialize(aed.getVariableEvaluacionAvaluo());
                }
                Hibernate.initialize(resultado.getDocumento());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

}
