package co.gov.igac.snc.dao;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class GenericDAOBean implements IGenericDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GenericDAOBean.class);

    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext(unitName = "SigcEntityManager")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * @see IGenericDAO#findById(Long)
     * @author fabio.navarrete
     */
    @Override
    public Object findById(Class clazz, Long id) {
        LOGGER.debug("Buscando " + clazz.getSimpleName() + " con ID=" + id,
            Level.INFO, null);
        try {
            return entityManager.find(clazz, id);
        } catch (RuntimeException re) {
            LOGGER.error(re.getMessage(), Level.SEVERE, re);
            re.printStackTrace();
            LOGGER.error("Búsqueda fallida", Level.SEVERE, re);
            throw re;
        } catch (Exception ex) {
            LOGGER.error("Búsqueda fallida. Error desconocido:", Level.SEVERE,
                ex);
            return null;
        }
    }

    /**
     * @see IGenericDAO#delete(Object)
     * @author fabio.navarrete
     */
    @Override
    public void delete(Object o) {
        LOGGER.debug("Eliminando " + o.getClass().getSimpleName(), Level.INFO,
            null);
        try {
            // N: esto sirve para evitar que salga error cuando se trata de
            // borrar algo y dice que la entidad esta detached.
            Object entity = this.entityManager.merge(o);

            this.entityManager.remove(entity);
            LOGGER.debug("Eliminación exitosa", Level.INFO, null);
        } catch (RuntimeException re) {
            LOGGER.error("Eliminación fallida", Level.SEVERE, re);
            throw re;
        }

    }

    /**
     * @see IGenericDAO#update(Object)
     * @author fabio.navarrete
     */
    @Override
    public Object update(Object o) {
        LOGGER.debug("Actualizando " + o.getClass().getSimpleName(),
            Level.INFO, null);
        try {
            Object entity = entityManager.merge(o);
            LOGGER.debug("Actualización exitosa", Level.INFO, null);
            return entity;
        } catch (RuntimeException re) {
            LOGGER.error("Actualización fallida", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * @see IGenericDAO#updateMultiple(List<Object>)
     * @author javier.aponte
     */
    @Override
    public void updateMultiple(List<Object> o) {
        LOGGER.debug("Actualizando " + o.getClass().getSimpleName(), Level.INFO, null);
        try {
            Object entity = entityManager.merge(o);
            for (Object objectTemp : o) {
                entityManager.merge(o);
            }
            LOGGER.debug("Actualización exitosa", Level.INFO, null);

        } catch (RuntimeException re) {
            LOGGER.error("Actualización fallida", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List findAll(Class clazz, int... rowStartIdxAndCount) {
        LOGGER.debug("Buscando todos los " + clazz.getSimpleName(), Level.INFO,
            null);
        try {
            final String queryString = "SELECT model FROM " +
                clazz.getSimpleName() + " model";
            Query query = entityManager.createQuery(queryString);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            LOGGER.error("Buscando todos falló", Level.SEVERE, re);
            throw re;
        }
    }

}
