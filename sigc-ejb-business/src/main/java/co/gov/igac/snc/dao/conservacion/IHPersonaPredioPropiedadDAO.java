/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.HPersonaPredioPropiedad;

/**
 *
 * @author fabio.navarrete
 *
 */
@Local
public interface IHPersonaPredioPropiedadDAO extends
    IGenericJpaDAO<HPersonaPredioPropiedad, Long> {

    /**
     * Retorna una lista de HPersonaPredioPropiedads relacionados con el Id de predio ingresado
     *
     * @author fabio.navarrete
     * @return
     */
    public List<HPersonaPredioPropiedad> findByPredioId(
        Long predioId);

}
