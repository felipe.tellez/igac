package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;
import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 *
 */
@Local
public interface IComunaDAO extends IGenericJpaDAO<Comuna, String> {

    public List<Comuna> findBySector(String sectorId);

    public Comuna getComunaByCodigo(String codigo);

}
