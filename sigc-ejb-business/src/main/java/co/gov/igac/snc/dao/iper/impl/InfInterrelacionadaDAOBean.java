/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.iper.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.iper.IInfInterrelacionadaDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionada;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionadaRe;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;

/**
 * Implementación de los métodos para consulta de IPER
 *
 * @author fredy.wilches
 * @version 2.0
 */
@Stateless
public class InfInterrelacionadaDAOBean extends GenericDAOWithJPA<InfInterrelacionada, Long>
    implements IInfInterrelacionadaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfInterrelacionadaDAOBean.class);

//--------------------------------------------------------------------------------------------------
    public List<InfInterrelacionada> getTramitesPrimeraPendientes() {
        try {
            String sql = "SELECT i FROM InfInterrelacionada i " +
                " WHERE i.automatica='SI' and i.estado IS NULL and i.estadoInterrelacion='Interrelacionado'";
            Query query = entityManager.createQuery(sql);
            return (List<InfInterrelacionada>) query.getResultList();
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage());
            return null;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al buscar InfInterrelacionada");
        }
    }

//end of class
}
