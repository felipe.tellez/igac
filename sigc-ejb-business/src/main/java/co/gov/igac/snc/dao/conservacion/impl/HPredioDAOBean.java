package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IHPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.HPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.HUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.HFoto;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.HUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.HFmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.util.EHPredioTipoHistoria;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import org.hibernate.Hibernate;

@Stateless
public class HPredioDAOBean extends GenericDAOWithJPA<HPredio, Long> implements IHPredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(HPredioDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<HPredio> findHPredioListByConsecutivoCatastral(
        BigDecimal consecutivoCatastral) {
        LOGGER.debug("findHPredioListByConsecutivoCatastral");
        Query q = entityManager.createNamedQuery("findHPredioByConsecutivoCatastral");
        q.setParameter("consecutivoCatastral", consecutivoCatastral);
        return (List<HPredio>) (q.getResultList());
    }

    /**
     * @see IHPredioDAO#buscarHistoricoPrediosPorIdTramite(Long)
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<HPredio> buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(
        Long idTramite) {

        List<HPredio> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT hp FROM HPredio hp JOIN FETCH hp.predioId p " +
            " WHERE hp.tramiteId = :idTramite and " +
            "(hp.cancelaInscribe = :etiquetaInscribe1  or hp.cancelaInscribe = :etiquetaInscribe2) " +
            "and hp.tipoHistoria = :tipoHistoriaP";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idTramite", idTramite);
            query.setParameter("etiquetaInscribe1", EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            query.setParameter("etiquetaInscribe2", EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            query.setParameter("tipoHistoriaP", EHPredioTipoHistoria.PROYECCION.getCodigo());

            answer = (ArrayList<HPredio>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("HPredioDAOBean#buscarHistorioPrediosPorIdTramite: " +
                "La consulta de historico de predios no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;

    }

    /**
     * @see IHPredioDAO#buscarHPredioPorNumeroPredialYEstado(String, String)
     * @author david.cifuentes
     */
    @Override
    public HPredio buscarHPredioPorNumeroPredialYEstado(String numeroPredial, String estado) {
        try {
            String sql = "SELECT hp FROM HPredio hp" +
                " WHERE hp.numeroPredial = :numeroPredial" +
                " AND hp.estado = :estado" +
                " ORDER BY hp.fechaLog DESC";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("numeroPredial", numeroPredial);
            query.setParameter("estado", estado);
            query.setMaxResults(1);

            HPredio hPredio = (HPredio) query.getSingleResult();
            return hPredio;
        } catch (Exception e) {
            LOGGER.debug("Error en la búsqueda del HFichaMatrizPredio: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see IHPredioDAO#buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(String, String)
     * @author david.cifuentes
     */
    @Override
    public HPredio buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(
        String numeroPredial, String estadoCancelaInscribe) {
        try {
            String sql = "SELECT hp FROM HPredio hp" +
                " WHERE hp.numeroPredial = :numeroPredial" +
                " AND hp.cancelaInscribe = :cancelaInscribe" +
                " ORDER BY hp.fechaLog DESC";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("numeroPredial", numeroPredial);
            query.setParameter("cancelaInscribe", estadoCancelaInscribe);
            query.setMaxResults(1);

            HPredio hPredio = (HPredio) query.getSingleResult();
            return hPredio;
        } catch (Exception e) {
            LOGGER.debug("Error en la búsqueda del HFichaMatrizPredio: " + e.getMessage());
            return null;
        }
    }

    /**
     *
     * @author leidy.gonzalez
     * @see IHPredioDAO#contarPrediosPorRangoNumeroPredial
     */
    @Implement
    @Override
    public Integer contarPrediosPorRangoNumeroPredialHistorico(String numeroPredial,
        String numeroPredialFinal) {

        Integer conteoPredio = 0;

        Query q = entityManager
            .createQuery("SELECT COUNT (p.numeroPredial)" +
                " FROM HPredio p" +
                " WHERE p.numeroPredial BETWEEN :numeroPredial AND :numeroPredialFinal");

        q.setParameter("numeroPredial", numeroPredial);
        q.setParameter("numeroPredialFinal", numeroPredialFinal);

        try {
            conteoPredio = (Integer.parseInt(q.getSingleResult()
                .toString()));
            return conteoPredio;

        } catch (Exception ex) {
            LOGGER.error("Error on IPredioDAO#contarPrediosPorRangoNumeroPredial: " +
                ex.getMessage());

            return null;
        }

    }

    /**
     * @author juan.cruz
     * @see IHPredioDAO#obtenerEstadoOrigenNumeroPredial
     */
    @Override
    public HPredio obtenerEstadoOrigenNumeroPredial(String numeroPredial) {

        try {
            String query = "SELECT hp " +
                "FROM HPredio hp " +
                "WHERE hp.numeroPredial = :numeroPredial " +
                "AND hp.tipoHistoria = :tipoHistoria " +
                "ORDER BY hp.id ASC ";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("numeroPredial", numeroPredial);
            q.setParameter("tipoHistoria", EHPredioTipoHistoria.PROYECCION.getCodigo());
            q.setMaxResults(1);

            HPredio hPredio = (HPredio) q.getSingleResult();

            Hibernate.initialize(hPredio.getHFichaMatrizs());

            return hPredio;

        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioDAOBean#obtenerEstadoOrigenNumeroPredial");
        }
    }

    /**
     * @author juan.cruz
     * @see IPredioDAO#obtenerPrediosOrigenPorTramiteId
     */
    @Override
    public List<HPredio> obtenerPrediosOrigenPorTramiteId(Long tramiteId) {
        try {
            String query = "SELECT hp " +
                "FROM HPredio hp " +
                "LEFT JOIN hp.HFichaMatrizs " +
                "WHERE hp.tramiteId = :tramiteId " +
                "AND hp.tipoHistoria = :tipoHistoria ";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("tramiteId", tramiteId);
            q.setParameter("tipoHistoria", EHPredioTipoHistoria.ORIGINAL.getCodigo());

            List<HPredio> hPredios = q.getResultList();
            List<HPredio> resultado = new ArrayList<HPredio>();
            List<String> numerosPrediales = new ArrayList<String>();

            for (HPredio hPredio : hPredios) {
                if (!numerosPrediales.contains(hPredio.getNumeroPredial())) {
                    Hibernate.initialize(hPredio.getHFichaMatrizs());
                    Hibernate.initialize(hPredio.getPredioId());
                    numerosPrediales.add(hPredio.getNumeroPredial());
                    resultado.add(hPredio);
                }
            }

            return resultado;

        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "HPredioDAOBean#obtenerPrediosOrigenPorTramiteId");
        }
    }

    /**
     * @author juan.cruz
     * @see PredioDAO#obtenerPredioCompletoPorId
     */
    @Override
    public HPredio obtenerHPredioCompletoPorId(Long id) {
        try {
            String query = "SELECT hp " +
                "FROM HPredio hp " +
                "JOIN FETCH hp.departamento " +
                "JOIN FETCH hp.municipio " +
                "JOIN FETCH hp.circuloRegistral " +
                "LEFT JOIN FETCH hp.HUnidadConstruccions huc " +
                "WHERE hp.id = :id";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("id", id);

            HPredio hPredio = (HPredio) q.getSingleResult();

            Hibernate.initialize(hPredio.getHPersonaPredios());
            List<HPersonaPredio> HPersonasPredios = hPredio.getHPersonaPredios();
            if (HPersonasPredios != null) {
                for (HPersonaPredio aHPersonaPredio : HPersonasPredios) {
                    Hibernate.initialize(aHPersonaPredio.gethPersonaPredioPropiedads());
                }
            }

            List<HUnidadConstruccion> HUnidadConstruccions = hPredio.getHUnidadConstruccions();
            if (HUnidadConstruccions != null) {
                for (HUnidadConstruccion aHUnidadConstruccion : HUnidadConstruccions) {
                    Hibernate.initialize(aHUnidadConstruccion.getUsoConstruccion());
                    Hibernate.initialize(aHUnidadConstruccion.getHUnidadConstruccionComps());
                }
            }

            Hibernate.initialize(hPredio.getPredioId());

            Hibernate.initialize(hPredio.getHFotos());

            Hibernate.initialize(hPredio.getHPredioAvaluoCatastrals());
//                List<HFoto> hFotos = hPredio.getHFotos();
//                if(hFotos != null){
//                    for(HFoto aHFoto : hFotos){
//                        
//                    }
//                }
            //juan.cruz::19012:07/12/2017:: Se Suspende la inicialización de Ficha Matriz, debido a que según lo conversado
            //con felipe el día 06/12/2017 un predio origen no mostrará la pestaña ficha matriz

            //<INICIO>#########FICHA MATRIZ############
//                Hibernate.initialize(hPredio.getHFichaMatrizs());
//                
//                List<HFichaMatriz> HFichaMatrizs = hPredio.getHFichaMatrizs();
//                for(HFichaMatriz aHFichaMatriz : HFichaMatrizs){
//                    Hibernate.initialize(aHFichaMatriz.getHPredio().getHPredioZonas());
//                    Hibernate.initialize(aHFichaMatriz.getHPredio().getHPredioDireccions());
//                    
//                    Hibernate.initialize(aHFichaMatriz.getHFichaMatrizPredios());
//                    Hibernate.initialize(aHFichaMatriz.getHFichaMatrizTorres());
//                    Hibernate.initialize(aHFichaMatriz.getHFichaMatrizModelos());
//                    
//                    List<HFichaMatrizModelo> modelos = aHFichaMatriz.getHFichaMatrizModelos();
//                    for(HFichaMatrizModelo aModelo : modelos){
//                        Hibernate.initialize(aModelo.getHFmModeloConstruccions());
//                        List<HFmModeloConstruccion> modelConstruccions = aModelo.getHFmModeloConstruccions();
//                        for(HFmModeloConstruccion aHFmModeloConstruccion : modelConstruccions){
//                            Hibernate.initialize(aHFmModeloConstruccion.getUsoConstruccion());
//                        }
//                    }
//                    
//                    for(HUnidadConstruccion aHUnidadConstruccion : aHFichaMatriz.getHPredio().getHUnidadConstruccions()){
//                        Hibernate.initialize(aHUnidadConstruccion.getUsoConstruccion());
//                        Hibernate.initialize(aHUnidadConstruccion.getHUnidadConstruccionComps());
//                        for(HUnidadConstruccionComp aHUnidadConstruccionComp : aHUnidadConstruccion.getHUnidadConstruccionComps()){
//                            Hibernate.initialize(aHUnidadConstruccionComp.getHUnidadConstruccion());
//                            
//                        }
//                    }
//                    
//                }
            //<FIN>#########FICHA MATRIZ############
            return hPredio;

        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "HPredioDAOBean#obtenerPredioCompletoPorId");
        }
    }
    // end of class
}
