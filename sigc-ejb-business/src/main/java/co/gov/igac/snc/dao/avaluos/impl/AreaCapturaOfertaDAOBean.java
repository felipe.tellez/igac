/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import java.util.List;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAreaCapturaOfertaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Stateless
public class AreaCapturaOfertaDAOBean extends GenericDAOWithJPA<AreaCapturaOferta, Long>
    implements IAreaCapturaOfertaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaCapturaOfertaDAOBean.class);

    /**
     * @see IAreaCapturaOfertaDAO#getAreasSinComisionarConRecolector(String,String)
     * @author christian.rodriguez
     *
     * */
    @SuppressWarnings("unchecked")
    @Override
    public List<AreaCapturaOferta> getAreasSinComisionarConRecolector(String departamentoCodigo,
        String municipioCodigo) {
        LOGGER.debug("on AreaCapturaOfertaDAOBean#getAreasSinAsignar");

        List<AreaCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT aco " +
            "FROM AreaCapturaOferta aco, AreaCapturaDetalle acd " +
            "JOIN FETCH aco.departamento " +
            "JOIN FETCH aco.municipio " +
            "JOIN FETCH aco.zona " +
            "WHERE aco.id = acd.areaCapturaOferta.id " +
            "AND acd.comisionOferta = null " +
            "AND acd.asignadoRecolectorId != null " +
            "AND aco.departamento.codigo = :departamentoCod  " +
            "AND aco.municipio.codigo = :municipioCod ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("departamentoCod", departamentoCodigo);
            query.setParameter("municipioCod", municipioCodigo);
            answer = (List<AreaCapturaOferta>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, "AreaCapturaOfertaDAOBean#getAreasSinAsignar");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAreaCapturaOfertaDAO#getAreaCapturaOfertaById(Long)
     * @author christian.rodriguez
     *
     * */
    @Override
    public AreaCapturaOferta getAreaCapturaOfertaById(Long areaId) {
        LOGGER.debug("on AreaCapturaOfertaDAOBean#getAreaCapturaOfertaById");

        AreaCapturaOferta answer = null;
        String queryString;
        Query query;

        queryString = "SELECT aco " +
            "FROM AreaCapturaOferta aco " +
            "LEFT JOIN FETCH aco.departamento " +
            "LEFT JOIN FETCH aco.municipio " +
            "LEFT JOIN FETCH aco.zona " +
            "LEFT JOIN FETCH aco.detalleCapturaOfertas " +
            "WHERE aco.id = :areaId ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("areaId", areaId);
            answer = (AreaCapturaOferta) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, e,
                    "AreaCapturaOferta", areaId);
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAreaCapturaOfertaDAO#getAreaCapturaOfertaPorEstadoYEstrucOrg(String, String)
     * @author christian.rodriguez
     *
     * @modified pedro.garcia 06-06-2012 ordenamiento por id
	 * */
    @SuppressWarnings("unchecked")
    @Override
    public List<AreaCapturaOferta> getAreaCapturaOfertaPorEstadoYEstrucOrg(String estrucOrgCod,
        String estadoAreaCaptura) {
        LOGGER.debug("on AreaCapturaOfertaDAOBean#getAreaCapturaOfertaPorEstadoYEstrucOrg");

        List<AreaCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT aco " +
            "FROM AreaCapturaOferta aco " +
            "LEFT JOIN FETCH aco.departamento " +
            "LEFT JOIN FETCH aco.municipio " +
            "LEFT JOIN FETCH aco.zona " +
            "WHERE aco.estructuraOrganizacionalCod = :estrucOrgCod " +
            "AND  aco.estado = :estadoAreaCaptura " +
            "ORDER BY aco.id";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);

            if (estrucOrgCod != null) {
                query.setParameter("estrucOrgCod", estrucOrgCod);
            }

            query.setParameter("estadoAreaCaptura", estadoAreaCaptura);

            answer = (List<AreaCapturaOferta>) query.getResultList();

            if (answer != null) {
                for (AreaCapturaOferta areaCapturaOferta : answer) {
                    areaCapturaOferta.getRegionCapturaOfertas().size();
                    areaCapturaOferta.getDetalleCapturaOfertas().size();
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "AreaCapturaOfertaDAOBean#getAreaCapturaOfertaPorEstadoYEstrucOrg");
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAreaCapturaOfertaDAO#getByIdConAsociaciones(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public AreaCapturaOferta getByIdConAsociaciones(Long areaCapturaOfertaId) {

        LOGGER.debug("on AreaCapturaOfertaDAOBean#getByIdConAsociaciones ...");
        AreaCapturaOferta answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT aco " +
            " FROM AreaCapturaOferta aco " +
            " LEFT JOIN FETCH aco.departamento " +
            " LEFT JOIN FETCH aco.municipio " +
            " LEFT JOIN FETCH aco.zona " +
            " WHERE aco.id = :areaCapturaOfertaIdP ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("areaCapturaOfertaIdP", areaCapturaOfertaId);

            answer = (AreaCapturaOferta) query.getSingleResult();
            if (answer != null) {
                if (answer.getRegionCapturaOfertas() != null) {
                    answer.getRegionCapturaOfertas().size();
                }
                if (answer.getDetalleCapturaOfertas() != null) {
                    answer.getDetalleCapturaOfertas().size();
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, ex,
                    "AreaCapturaOferta", areaCapturaOfertaId.toString());
        }

        return answer;
    }

//end of class
}
