package co.gov.igac.snc.dao.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.SNCPropertiesUtil;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import javax.naming.Name;
import javax.naming.ldap.LdapName;

/**
 * Clase utilizada para ejecutar consultas a LDAP
 *
 * @author fredy.wilches
 *
 */
public class LdapDAO {

    /**
     * Variable para uso de logs
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LdapDAO.class);

    private static final String DN_APLICACION_SNC = "OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC;
    private static final String DN_TERRITORIALES = "OU=TERRITORIALES,OU=IGAC," + Constantes.LDAP_DC;
    private static final String DN_DELEGADOS = "OU=EXTERNOS," + Constantes.LDAP_DC;
    private static final String DN_HABILITADOS = "OU=EXTERNOS," + Constantes.LDAP_DC;

    private static String SERVIDOR = null;
    private static String PUERTO = null;
    private static String DOMINIO = null;
    private static String USUARIO = null;
    private static String CONTRASENA = null;
    private static String INITIAL_CONTEXT_FACTORY = null;
    private static String DN = null;

    /**
     *
     */
    static {
        try {
            SERVIDOR = SNCPropertiesUtil.getInstance().getProperty("servidor");
            PUERTO = SNCPropertiesUtil.getInstance().getProperty("puerto");
            DOMINIO = SNCPropertiesUtil.getInstance().getProperty("dominio");
            USUARIO = SNCPropertiesUtil.getInstance().getProperty("usuario");
            CONTRASENA = SNCPropertiesUtil.getInstance().getProperty("contrasena");
            INITIAL_CONTEXT_FACTORY = SNCPropertiesUtil.getInstance().getProperty(
                "initial_context_factory");
            DN = SNCPropertiesUtil.getInstance().getProperty("dn");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100006.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     *
     */
    private static LdapContext getLDAPConnection() {
        LdapContext ctxGC = null;

        String principal = USUARIO + "@" + DOMINIO;
        LOGGER.debug("principal:" + principal);

        Hashtable<String, String> environment = new Hashtable<String, String>();
        environment.put(Context.INITIAL_CONTEXT_FACTORY,
            INITIAL_CONTEXT_FACTORY);
        environment.put(Context.PROVIDER_URL, "ldap://" + SERVIDOR + ":" +
            PUERTO);
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, principal);
        environment.put(Context.SECURITY_CREDENTIALS, CONTRASENA);
        try {
            ctxGC = new InitialLdapContext(environment, null);
        } catch (NamingException e) {
            LOGGER.error("No se pudo obtener conexión con el servidor LDAP : " + e.getMessage(), e);

            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        LOGGER.debug("Se autenticó satisfactoriamente ante el servidor LDAP");
        return ctxGC;
    }

    /**
     * Retorna u usuario para una estructura organizacional(Territorial o Unidad Operativa) y rol
     *
     * @param estructuraOrganizacional
     * @param rol
     */
    public static List<Attributes> getUsuariosEstructuraRol(
        String estructuraOrganizacional, ERol rol) {
        List<Attributes> jefes = null;
        try {

            String searchFilter = "(&(objectClass=user)(memberOf=CN=" +
                rol.getRol() + "," + DN_APLICACION_SNC +
                ")(lastKnownParent=OU=" + estructuraOrganizacional + "," +
                DN_TERRITORIALES + "))";

            String returnedAtts[] = {"name", "displayName", "mail",
                "memberOf", "cn", "distinguishedName", "lastKnownParent",
                "sAMAccountName", "accountExpires", "userAccountControl"};
            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answer = getLDAPConnection()
                .search(DN, searchFilter, searchCtls);
            Attributes attrs = null;

            jefes = new ArrayList<Attributes>();

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                attrs = sr.getAttributes();
                jefes.add(attrs);
            }

        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return jefes;
    }

    /**
     * Retorna todos los usuarios para una estructura organizacional(Territorial o Unidad Operativa)
     *
     * @author felipe.cadena
     * @param territorial
     * @param uoc -- Si es null solo retorna los usuarios de la territorial
     * @return
     */
    public static List<UsuarioDTO> getUsuariosEstructura(
        String territorial, String uoc) {
        List<UsuarioDTO> usuarios = null;

        if (territorial.equals("SEDE_CENTRAL")) {
            territorial = "SUB_CATASTRO";
        }

        territorial = Utilidades.delTildes(territorial.toUpperCase());

        try {
            String searchFilter = "(objectClass=user)";
            String returnedAtts[] = {"name", "displayName", "mail",
                "sn", //nombres
                "givenName",//apellidos
                "memberOf", "cn", "distinguishedName", "lastKnownParent",
                "sAMAccountName", "employeeId", "employeeType", "accountExpires",
                "userAccountControl"};

            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

            String dn;
            //Los dn varian si se trata de una unidad operativa
            if (uoc != null) {
                dn = "OU=" + uoc + ",OU=" + territorial + "," + DN_TERRITORIALES;
            } else if (territorial.contains(Constantes.PREFIJO_DELEGADOS)) {
                dn = "OU=" + territorial + "," + DN_DELEGADOS;
            } else if(territorial.contains(Constantes.PREFIJO_HABILITADOS)){
                dn = "OU=" + territorial + "," + DN_HABILITADOS;
            } else {
                dn = "OU=" + territorial + "," + DN_TERRITORIALES;
            }

            Name name = new LdapName(dn);
            NamingEnumeration<SearchResult> answer = getLDAPConnection().search(name, searchFilter,
                null, searchCtls);
            Attributes attrs = null;

            usuarios = new ArrayList<UsuarioDTO>();

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                attrs = sr.getAttributes();

                //Se descartan los usuarios sin rol en el LDAP
                if (attrs.get("memberOf") == null) {
                    continue;
                }
                String distinguisedName = attrs.get("distinguishedName").toString();

                try {
                    UsuarioDTO usuarioDto = LdapSecurityUtil.getUserDetails(attrs);
                    usuarios.add(usuarioDto);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }

        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return usuarios;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * @param estructuraOrganizacional
     * @param rol
     * @return
     */
    /*
     * @modified by alejandro.sanchez 10/07/2013 Se modificó el código correspondiente a la
     * separación por distinguisedName de los usuarios del rol que puede realizar la actividad.
     *
     */
    public static List<UsuarioDTO> getUsuariosEstructuraRol2(
        String estructuraOrganizacional, ERol rol) {
        List<UsuarioDTO> usuarios = null;

        if (estructuraOrganizacional.equals("SEDE_CENTRAL")) {
            estructuraOrganizacional = "SUB_CATASTRO";
        }

        estructuraOrganizacional = Utilidades.delTildes(estructuraOrganizacional.toUpperCase());

        try {
            String searchFilter = "(&(objectClass=group)(distinguishedName=CN=" + rol.getRol() +
                ",OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC + "))";
            String returnedAtts[] = {"member"};

            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answer = getLDAPConnection().search(DN, searchFilter,
                searchCtls);
            Attributes attrs = null;

            usuarios = new ArrayList<UsuarioDTO>();

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                attrs = sr.getAttributes();
                String member = attrs.get("member").toString().trim().toUpperCase();
                //LOGGER.debug("member:" +member );
                String cadenas[] = member.split("CN=");
                for (String m : cadenas) {
                    if (m.startsWith("MEMBER")) {
                        continue;
                    }
                    String distinguisedName = "";
                    //Si es la última cadena, entonces no se elimina la coma al final. Si se eliminara, se quitaría una parte
                    //por lo que se genera un error al quitar el último usuario.
                    distinguisedName = (m.trim().endsWith(",") ? "CN=".concat(m.substring(0, m.
                        lastIndexOf(','))) : "CN=".concat(m));

                    if (m.indexOf("OU") == -1) {
                        continue;
                    }
                    String codigoEstructura = m.substring(m.indexOf("OU"));
                    //LOGGER.debug("m:" +m );
                    //LOGGER.debug("distinguisedName:" +distinguisedName );

                    //valida el último nivel organizacional al que pertenece el usuario
                    //Ejm: 	Si se solicita estructuraOrganizacional cundinamarca, no se tienen
                    //		en cuenta las unidades operativas
                    if (codigoEstructura.startsWith("OU=" + estructuraOrganizacional)) {
                        //LOGGER.debug("codigoEstructura:" +codigoEstructura );
                        try {
                            UsuarioDTO usuarioDto = searchUserByDistinguishedName(distinguisedName);
                            usuarios.add(usuarioDto);
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                    }

                }
            }
        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return usuarios;
    }

    /**
     * @author fredy.wilches Este metodo hay que revisarlo porque el usuario fue creado sin
     * coincidir en algo con los usuarios anteriores
     * @return
     */
    public static UsuarioDTO getSubdirectorCatastro() {
        return searchUser("subcatastro");
    }

    /**
     *
     * @param login
     * @return
     */
    public static UsuarioDTO searchUser(String login) {
        UsuarioDTO user = null;
        try {
            String searchFilter = "(&(objectClass=user)(sAMAccountName=" + login + ")" + ")";

            String returnedAtts[] = {"name", "displayName", "mail",
                "sn", //nombres
                "givenName",//apellidos
                "memberOf", "cn", "distinguishedName", "lastKnownParent",
                "sAMAccountName", "employeeId", "employeeType", "accountExpires",
                "userAccountControl"};
            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answer = getLDAPConnection()
                .search(DN, searchFilter, searchCtls);

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                user = LdapSecurityUtil.getUserDetails(attrs);
                LOGGER.debug("user:" + user);
                break;
            }

            if (user == null) {
                throw SncBusinessServiceExceptions.EXCEPCION_100004.getExcepcion(LOGGER, null,
                    "No Existe el Usuario " + login);
            }

        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return user;
    }

    /**
     *
     * @param login
     */
    public static List<UsuarioDTO> listAllUsers() {
        List<UsuarioDTO> users = new ArrayList<UsuarioDTO>();
        try {
            String searchFilter = "(&(objectClass=user)(sAMAccountName=*)" + ")";

            String returnedAtts[] = {"name", "displayName", "mail",
                "memberOf", "cn", "distinguishedName", "lastKnownParent",
                "sAMAccountName", "employeeId", "employeeType", "accountExpires",
                "userAccountControl"};
            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            LdapContext ldapConn = getLDAPConnection();
            int pageSize = 200;
            // Activate paged results
            ldapConn.setRequestControls(new Control[]{new PagedResultsControl(pageSize,
                Control.NONCRITICAL)});

            byte[] cookie = null;
            int total;
            do {
                NamingEnumeration<SearchResult> answer = ldapConn.search(DN, searchFilter,
                    searchCtls);
                while (answer.hasMoreElements()) {
                    try {
                        SearchResult sr = (SearchResult) answer.next();
                        Attributes attrs = sr.getAttributes();
                        UsuarioDTO user = LdapSecurityUtil.getUserDetails(attrs);
                        //LOGGER.debug("user:" + user.getLogin());
                        users.add(user);
                    } catch (Exception e) {
                        //LOGGER.error(e.getMessage(), e);
                        //LOGGER.error(e.getMessage());
                    }
                }

                // Examine the paged results control response
                Control[] controls = ldapConn.getResponseControls();
                if (controls != null) {
                    for (int i = 0; i < controls.length; i++) {
                        if (controls[i] instanceof PagedResultsResponseControl) {
                            PagedResultsResponseControl prrc =
                                (PagedResultsResponseControl) controls[i];
                            total = prrc.getResultSize();
                            /*
                             * if (total != 0) { LOGGER.debug("** END-OF-PAGE " +"(total : " + total
                             * +") **"); } else { LOGGER.debug("** END-OF-PAGE " +"(total: unknown)
                             * **\n"); }
                             */
                            cookie = prrc.getCookie();
                        }
                    }
                } else {
                    LOGGER.debug("No controls were sent from the server");
                }
                // Re-activate paged results
                ldapConn.setRequestControls(new Control[]{new PagedResultsControl(pageSize, cookie,
                    Control.CRITICAL)});
            } while (cookie != null);
            ldapConn.close();
        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return users;
    }

    /**
     * Busca un usuario en el LDAP por su distinguisedName
     *
     * @author christian.rodriguez
     * @param distinguishedName distinguisedName del usuario que se desea buscar
     * @return usuario que se encontró en el LDAP
     */
    public static UsuarioDTO searchUserByDistinguishedName(String distinguishedName) {
        //LOGGER.debug("searchUserByDistinguishedName" );
        UsuarioDTO user = null;
        try {
            String searchFilter = "(&(objectClass=user)(distinguishedName=" + distinguishedName +
                ")" + ")";
            LOGGER.debug("searchFilter:" + searchFilter);

            String returnedAtts[] = {"name", "displayName", "mail",
                "sn", //nombres
                "givenName",//apellidos
                "memberOf", "cn", "distinguishedName", "lastKnownParent",
                "sAMAccountName", "employeeId", "employeeType", "accountExpires",
                "userAccountControl"};

            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answer = getLDAPConnection().search(DN, searchFilter,
                searchCtls);

            while (answer.hasMoreElements()) {
                //LOGGER.debug("answer:" );
                SearchResult sr = (SearchResult) answer.next();
                //LOGGER.debug("sr:" + sr);
                Attributes attrs = sr.getAttributes();
                user = LdapSecurityUtil.getUserDetails(attrs);
                //LOGGER.debug("user:" + user);
                break;
            }

            if (user == null) {
                String causaError = "No se encontró el usuario: " + distinguishedName;
                throw SncBusinessServiceExceptions.EXCEPCION_100004.getExcepcion(LOGGER, null,
                    causaError);
            }

        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return user;
    }

    /**
     * Retorna el jefe de conservacion para una estructura organizacional (Territorial o Unidad
     * Operativa)
     *
     * @param estructuraOrganizacional
     */
    /*
     * public static List<Attributes> getUsuariosJefeConservacion( String estructuraOrganizacional)
     * { return getUsuariosEstructuraRol(estructuraOrganizacional, ERol.RESPONSABLE_CONSERVACION); }
     */
    /**
     *
     * @param estructuraOrganizacional
     * @return
     */
    /*
     * public static List<UsuarioDTO> getJefeConservacion( UsuarioDTO usuario) { List<Attributes>
     * jefes = LdapDAO
     * .getUsuariosJefeConservacion(usuario.getDescripcionEstructuraOrganizacional()); return
     * convertLDAPToDTO(usuario, jefes); }
     */
//--------------------------------------------------------------------------------------------------
    /**
     * @author fredy.wilches Retorna los UsuarioDTO de una territorial y rol
     * @param estructuraOrganizacional
     * @param rol
     * @return
     */
    public static List<UsuarioDTO> getUsuariosRolTerritorial(
        String estructuraOrganizacional, ERol rol) {
        return LdapDAO.getUsuariosEstructuraRol2(
            estructuraOrganizacional, rol);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fredy.wilches Convierte usuarios del LDAP en UsuarioDTO
     * @param estructuraOrganizacional
     * @param usuariosLDAP
     * @return
     */
    private static List<UsuarioDTO> convertLDAPToDTO(
        UsuarioDTO usuarioDto, List<Attributes> usuariosLDAP) {
        ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        for (Attributes attrs : usuariosLDAP) {
            if (attrs != null) {
                UsuarioDTO usuario = new UsuarioDTO();
                try {
                    usuario.setLogin(attrs.get("sAMAccountName").get(0)
                        .toString());
                    usuario.setPrimerNombre(attrs.get("name").get(0).toString());
                    usuario.setCodigoUOC(usuarioDto.getCodigoUOC());
                    usuario.setCodigoTerritorial(usuarioDto.getCodigoTerritorial());
                    usuario.setDescripcionUOC(usuarioDto.getDescripcionUOC());
                    usuario.setDescripcionTerritorial(usuarioDto.getDescripcionTerritorial());
                    usuarios.add(usuario);
                } catch (NamingException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }
        return usuarios;
    }

    /**
     * Convierte usuarios del LDAP en UsuarioDTO
     *
     * @author fredy.wilches
     * @param usuariosLDAP
     * @return
     */
    private static List<UsuarioDTO> convertLDAPToDTO(List<Attributes> usuariosLDAP) {
        ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        for (Attributes attrs : usuariosLDAP) {
            if (attrs != null) {
                UsuarioDTO usuario = new UsuarioDTO();
                try {
                    usuario.setLogin(attrs.get("sAMAccountName").get(0)
                        .toString());
                    usuario.setPrimerNombre(attrs.get("name").get(0).toString());
                    usuarios.add(usuario);
                } catch (NamingException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }
        return usuarios;
    }

    /**
     * Retorna los atributos LDAP para una territorial y/o UO
     *
     * @param territorial
     * @param uo
     * @return
     */
    private static List<Attributes> getFuncionarios(
        String territorial, String uo) {
        List<Attributes> funcionarios = null;
        try {

            String searchFilter = "(&(objectClass=user)(!(objectClass=computer)))";
            String returnedAtts[] = {"name", "displayName", "mail",
                "memberOf", "cn", "distinguishedName", "lastKnownParent",
                "sAMAccountName", "accountExpires", "userAccountControl"};
            String base;
            if (uo != null) {
                base = "OU=" + uo + ",OU=" + territorial + ",OU=TERRITORIALES,OU=IGAC," +
                    Constantes.LDAP_DC;
            } else {
                base = "OU=" + territorial + ",OU=TERRITORIALES,OU=IGAC," + Constantes.LDAP_DC;
            }

            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answer = getLDAPConnection()
                .search(base, searchFilter, searchCtls);
            Attributes attrs = null;
            funcionarios = new ArrayList<Attributes>();

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                attrs = sr.getAttributes();
                funcionarios.add(attrs);
            }

        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return funcionarios;
    }

    /**
     *
     * @param territorial
     * @param uo
     * @return
     */
    public static List<UsuarioDTO> getFuncionariosDto(String territorial, String uo) {
        return convertLDAPToDTO(getFuncionarios(territorial, uo));
    }

    /**
     * Este metodo es igual a getUsuariosEstructuraRol2, solo que aca se obtienen los roles de la
     * territorial y de sus UOC.
     *
     * @author lorena.salamanca
     * @param estructuraOrganizacional
     * @param rol
     * @return
     */
    public static List<UsuarioDTO> getUsuariosEstructuraRolYUOC(String estructuraOrganizacional,
        ERol rol) {
        List<UsuarioDTO> usuarios = null;

        if (estructuraOrganizacional.equals("SEDE_CENTRAL")) {
            estructuraOrganizacional = "SUB_CATASTRO";
        }

        estructuraOrganizacional = Utilidades.delTildes(estructuraOrganizacional.toUpperCase());

        try {
            String searchFilter = "(&(objectClass=group)(distinguishedName=CN=" + rol.getRol() +
                ",OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC + "))";
            String returnedAtts[] = {"member"};

            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> answer = getLDAPConnection().search(DN, searchFilter,
                searchCtls);
            Attributes attrs = null;

            usuarios = new ArrayList<UsuarioDTO>();

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                attrs = sr.getAttributes();
                String member = attrs.get("member").toString().trim().toUpperCase();
                String cadenas[] = member.split("CN=");
                for (String m : cadenas) {
                    if (m.startsWith("MEMBER")) {
                        continue;
                    }
                    String distinguisedName;
                    distinguisedName = (m.trim().endsWith(",") ? "CN=".concat(m.substring(0, m.
                        lastIndexOf(','))) : "CN=".concat(m));

                    if (!m.contains("OU")) {
                        continue;
                    }
                    String codigoEstructura = m.substring(m.indexOf("OU"));
                    if (codigoEstructura.contains(estructuraOrganizacional)) {
                        try {
                            UsuarioDTO usuarioDto = searchUserByDistinguishedName(distinguisedName);
                            usuarios.add(usuarioDto);
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                    }

                }
            }
        } catch (NamingException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100003.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return usuarios;
    }
}
