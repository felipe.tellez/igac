package co.gov.igac.snc.dao.tramite.radicacion.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class SolicitudDAOBean extends GenericDAOWithJPA<Solicitud, Long>
    implements ISolicitudDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudDAOBean.class);

    /**
     * @see ISolicitudDAO#buscarSolicitudes(co.gov.igac.snc.util.FiltroDatosSolicitud)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Solicitud> buscarSolicitudes(
        FiltroDatosSolicitud filtroDatosSolicitud, int desde, int cantidad) {
        if (desde < 0) {
            desde = 0;
        }

        StringBuilder sql = new StringBuilder("SELECT DISTINCT s FROM Solicitud s ");

        sql.append(" LEFT JOIN s.solicitanteSolicituds ss ");

        sql.append(" LEFT JOIN FETCH s.solicitudAvisoRegistro WHERE 1 = 1 ");

        if (filtroDatosSolicitud.getNumero() != null &&
            filtroDatosSolicitud.getNumero().trim().length() > 0) {
            sql.append(" AND s.numero = :numero ");
        }
        if (filtroDatosSolicitud.getFechaInicial() != null) {
            if (filtroDatosSolicitud.getFechaFinal() != null) {
                sql.append(" AND s.fecha BETWEEN :fechaInicialSolicitud1 AND :fechaFinalSolicitud2");
            } else {
                sql.append(
                    " AND s.fecha BETWEEN :fechaInicialSolicitud1 AND :fechaInicialSolicitud2");
            }

        }
        if (filtroDatosSolicitud.getSolicitanteId() != null) {
            sql.append("   AND s.solicitante.id = :solicitanteId ");
        }

        // Filtro por tipo de identificación
        if (filtroDatosSolicitud.getTipoIdentificacion() != null &&
            !filtroDatosSolicitud.getTipoIdentificacion().trim().isEmpty()) {
            sql.append(" AND ss.tipoIdentificacion = :tipoIdentificacion ");
        }

        // Filtro por número de identificación
        if (filtroDatosSolicitud.getNumeroIdentificacion() != null &&
            !filtroDatosSolicitud.getNumeroIdentificacion().trim().isEmpty()) {

            sql.append(" AND ss.numeroIdentificacion = :numeroIdentificacion ");
        }

        // Filtro por primer nombre
        if (filtroDatosSolicitud.getPrimerNombre() != null &&
            !filtroDatosSolicitud.getPrimerNombre().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.primerNombre) = :primerNombre ");
        }

        // Filtro por segundo nombre
        if (filtroDatosSolicitud.getSegundoNombre() != null &&
            !filtroDatosSolicitud.getSegundoNombre().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.segundoNombre) = :segundoNombre  ");
        }

        // Filtro por primer apellido
        if (filtroDatosSolicitud.getPrimerApellido() != null &&
            !filtroDatosSolicitud.getPrimerApellido().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.primerApellido) = :primerApellido ");
        }

        // Filtro por segundo apellido
        if (filtroDatosSolicitud.getSegundoApellido() != null &&
            !filtroDatosSolicitud.getSegundoApellido().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.segundoApellido) = :segundoApellido  ");
        }
        // Filtro por razón social
        if (filtroDatosSolicitud.getRazonSocial() != null &&
            !filtroDatosSolicitud.getRazonSocial().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.razonSocial) = :razonSocial ");
        }

        if (filtroDatosSolicitud.getTipo() != null) {
            sql.append("   AND s.tipo = :tipo ");
        }
        if (filtroDatosSolicitud.getDepartamentoCodigo() != null) {
            sql.append("   AND s.direccionDepartamentoCodigo = :departamentoCodigo ");
        }
        if (filtroDatosSolicitud.getMunicipioCodigo() != null) {
            sql.append("   AND s.direccionMunicipioCodigo = :municipioCodigo");
        }

        Query query = entityManager.createQuery(sql.toString());

        if (filtroDatosSolicitud.getNumero() != null &&
            filtroDatosSolicitud.getNumero().trim().length() > 0) {
            query.setParameter("numero", filtroDatosSolicitud.getNumero());
        }
        if (filtroDatosSolicitud.getFechaInicial() != null) {
            query.setParameter("fechaInicialSolicitud1", filtroDatosSolicitud.getFechaInicial());

            if (filtroDatosSolicitud.getFechaFinal() != null) {
                Date hasta = filtroDatosSolicitud.getFechaFinal();
                Calendar c = Calendar.getInstance();
                c.setTime(hasta);
                c.add(Calendar.DATE, 1);
                query.setParameter("fechaFinalSolicitud2", c.getTime());
            } else {
                Date hasta = filtroDatosSolicitud.getFechaInicial();
                Calendar c = Calendar.getInstance();
                c.setTime(hasta);
                c.add(Calendar.DATE, 1);
                query.setParameter("fechaInicialSolicitud2", c.getTime());
            }
        }

        if (filtroDatosSolicitud.getSolicitanteId() != null) {
            query.setParameter("solicitanteId",
                filtroDatosSolicitud.getSolicitanteId());
        }
        if (filtroDatosSolicitud.getTipo() != null) {
            query.setParameter("tipo", filtroDatosSolicitud.getTipo());
        }
        if (filtroDatosSolicitud.getDepartamentoCodigo() != null) {
            query.setParameter("departamentoCodigo",
                filtroDatosSolicitud.getDepartamentoCodigo());
        }
        if (filtroDatosSolicitud.getMunicipioCodigo() != null) {
            query.setParameter("municipioCodigo",
                filtroDatosSolicitud.getMunicipioCodigo());
        }

        if (filtroDatosSolicitud.getTipoIdentificacion() != null &&
            !filtroDatosSolicitud.getTipoIdentificacion().trim()
                .isEmpty()) {
            query.setParameter("tipoIdentificacion", filtroDatosSolicitud.getTipoIdentificacion());
        }
        if (filtroDatosSolicitud.getNumeroIdentificacion() != null &&
            !filtroDatosSolicitud.getNumeroIdentificacion()
                .trim().isEmpty()) {
            query.setParameter("numeroIdentificacion", filtroDatosSolicitud.
                getNumeroIdentificacion());
        }
        if (filtroDatosSolicitud.getPrimerNombre() != null &&
            !filtroDatosSolicitud.getPrimerNombre().trim().isEmpty()) {
            query.setParameter("primerNombre", filtroDatosSolicitud.getPrimerNombre().toUpperCase());
        }
        if (filtroDatosSolicitud.getSegundoNombre() != null &&
            !filtroDatosSolicitud.getSegundoNombre().trim().isEmpty()) {
            query.setParameter("segundoNombre", filtroDatosSolicitud.getSegundoNombre().
                toUpperCase());
        }
        if (filtroDatosSolicitud.getPrimerApellido() != null &&
            !filtroDatosSolicitud.getPrimerApellido().trim().isEmpty()) {
            query.setParameter("primerApellido", filtroDatosSolicitud.getPrimerApellido().
                toUpperCase());
        }
        if (filtroDatosSolicitud.getSegundoApellido() != null &&
            !filtroDatosSolicitud.getSegundoApellido().trim().isEmpty()) {
            query.setParameter("segundoApellido", filtroDatosSolicitud.getSegundoApellido().
                toUpperCase());
        }
        if (filtroDatosSolicitud.getRazonSocial() != null &&
            !filtroDatosSolicitud.getRazonSocial().trim().isEmpty()) {
            query.setParameter("razonSocial", filtroDatosSolicitud.getRazonSocial().toUpperCase());
        }

        query.setFirstResult(desde);
        if (cantidad > 0) {
            query.setMaxResults(cantidad);
        }

        List<Solicitud> resultado = null;
        try {
            resultado = (List<Solicitud>) query.getResultList();
            for (Solicitud solicitud : resultado) {
                Hibernate.initialize(solicitud.getSolicitanteSolicituds());
                Hibernate.initialize(solicitud.getTramites());
                for (SolicitanteSolicitud ss : solicitud.getSolicitanteSolicituds()) {
                    Hibernate.initialize(ss.getSolicitante().getDireccionPais());
                    Hibernate.initialize(ss.getSolicitante().getDireccionDepartamento());
                    Hibernate.initialize(ss.getSolicitante().getDireccionMunicipio());
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return resultado;
    }

    /**
     * @see ISolicitudDAO#buscarSolicitudes(co.gov.igac.snc.util.FiltroDatosSolicitud)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Integer contarSolicitudesByFiltroSolicitud(
        FiltroDatosSolicitud filtroDatosSolicitud) {

        StringBuilder sql = new StringBuilder("SELECT COUNT(DISTINCT s) FROM Solicitud s ");

        sql.append(" LEFT JOIN s.solicitanteSolicituds ss ");

        sql.append(" LEFT JOIN s.solicitudAvisoRegistro WHERE 1 = 1 ");

        if (filtroDatosSolicitud.getNumero() != null &&
            filtroDatosSolicitud.getNumero().trim().length() > 0) {
            sql.append(" AND s.numero = :numero ");
        }
        if (filtroDatosSolicitud.getFechaInicial() != null) {
            sql.append("   AND s.fecha >= :fechaInicial ");
        }
        if (filtroDatosSolicitud.getFechaFinal() != null) {
            sql.append("   AND s.fecha <= :fechaFinal ");
        }
        if (filtroDatosSolicitud.getSolicitanteId() != null) {
            sql.append("   AND s.solicitante.id = :solicitanteId ");
        }

        // Filtro por tipo de identificación
        if (filtroDatosSolicitud.getTipoIdentificacion() != null &&
            !filtroDatosSolicitud.getTipoIdentificacion().trim().isEmpty()) {
            sql.append(" AND ss.tipoIdentificacion = :tipoIdentificacion ");
        }

        // Filtro por número de identificación
        if (filtroDatosSolicitud.getNumeroIdentificacion() != null &&
            !filtroDatosSolicitud.getNumeroIdentificacion().trim().isEmpty()) {

            sql.append(" AND ss.numeroIdentificacion = :numeroIdentificacion ");
        }

        // Filtro por primer nombre
        if (filtroDatosSolicitud.getPrimerNombre() != null &&
            !filtroDatosSolicitud.getPrimerNombre().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.primerNombre) = :primerNombre ");
        }

        // Filtro por segundo nombre
        if (filtroDatosSolicitud.getSegundoNombre() != null &&
            !filtroDatosSolicitud.getSegundoNombre().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.segundoNombre) = :segundoNombre  ");
        }

        // Filtro por primer apellido
        if (filtroDatosSolicitud.getPrimerApellido() != null &&
            !filtroDatosSolicitud.getPrimerApellido().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.primerApellido) = :primerApellido ");
        }

        // Filtro por segundo apellido
        if (filtroDatosSolicitud.getSegundoApellido() != null &&
            !filtroDatosSolicitud.getSegundoApellido().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.segundoApellido) = :segundoApellido  ");
        }
        // Filtro por razón social
        if (filtroDatosSolicitud.getRazonSocial() != null &&
            !filtroDatosSolicitud.getRazonSocial().trim().isEmpty()) {

            sql.append(" AND UPPER(ss.razonSocial) = :razonSocial ");
        }

        if (filtroDatosSolicitud.getTipo() != null) {
            sql.append("   AND s.tipo = :tipo ");
        }
        if (filtroDatosSolicitud.getDepartamentoCodigo() != null) {
            sql.append("   AND s.direccionDepartamentoCodigo = :departamentoCodigo ");
        }
        if (filtroDatosSolicitud.getMunicipioCodigo() != null) {
            sql.append("   AND s.direccionMunicipioCodigo = :municipioCodigo");
        }

        Query query = entityManager.createQuery(sql.toString());

        if (filtroDatosSolicitud.getNumero() != null &&
            filtroDatosSolicitud.getNumero().trim().length() > 0) {
            query.setParameter("numero", filtroDatosSolicitud.getNumero());
        }
        if (filtroDatosSolicitud.getFechaInicial() != null) {
            query.setParameter("fechaInicial",
                filtroDatosSolicitud.getFechaInicial());
        }
        if (filtroDatosSolicitud.getFechaFinal() != null) {
            query.setParameter("fechaFinal",
                filtroDatosSolicitud.getFechaFinal());
        }
        if (filtroDatosSolicitud.getSolicitanteId() != null) {
            query.setParameter("solicitanteId",
                filtroDatosSolicitud.getSolicitanteId());
        }
        if (filtroDatosSolicitud.getTipo() != null) {
            query.setParameter("tipo", filtroDatosSolicitud.getTipo());
        }
        if (filtroDatosSolicitud.getDepartamentoCodigo() != null) {
            query.setParameter("departamentoCodigo",
                filtroDatosSolicitud.getDepartamentoCodigo());
        }
        if (filtroDatosSolicitud.getMunicipioCodigo() != null) {
            query.setParameter("municipioCodigo",
                filtroDatosSolicitud.getMunicipioCodigo());
        }

        if (filtroDatosSolicitud.getTipoIdentificacion() != null &&
            !filtroDatosSolicitud.getTipoIdentificacion().trim()
                .isEmpty()) {
            query.setParameter("tipoIdentificacion", filtroDatosSolicitud.getTipoIdentificacion());
        }
        if (filtroDatosSolicitud.getNumeroIdentificacion() != null &&
            !filtroDatosSolicitud.getNumeroIdentificacion()
                .trim().isEmpty()) {
            query.setParameter("numeroIdentificacion", filtroDatosSolicitud.
                getNumeroIdentificacion());
        }
        if (filtroDatosSolicitud.getPrimerNombre() != null &&
            !filtroDatosSolicitud.getPrimerNombre().trim().isEmpty()) {
            query.setParameter("primerNombre", filtroDatosSolicitud.getPrimerNombre().toUpperCase());
        }
        if (filtroDatosSolicitud.getSegundoNombre() != null &&
            !filtroDatosSolicitud.getSegundoNombre().trim().isEmpty()) {
            query.setParameter("segundoNombre", filtroDatosSolicitud.getSegundoNombre().
                toUpperCase());
        }
        if (filtroDatosSolicitud.getPrimerApellido() != null &&
            !filtroDatosSolicitud.getPrimerApellido().trim().isEmpty()) {
            query.setParameter("primerApellido", filtroDatosSolicitud.getPrimerApellido().
                toUpperCase());
        }
        if (filtroDatosSolicitud.getSegundoApellido() != null &&
            !filtroDatosSolicitud.getSegundoApellido().trim().isEmpty()) {
            query.setParameter("segundoApellido", filtroDatosSolicitud.getSegundoApellido().
                toUpperCase());
        }
        if (filtroDatosSolicitud.getRazonSocial() != null &&
            !filtroDatosSolicitud.getRazonSocial().trim().isEmpty()) {
            query.setParameter("razonSocial", filtroDatosSolicitud.getRazonSocial().toUpperCase());
        }

        Integer answer;
        try {
            answer = (Integer.parseInt(query.getSingleResult().toString()));
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see SolicitudDAOBean#crearSolicitudTramite(UsuarioDTO, Solicitud)
     */
    @Override
    public Solicitud crearSolicitudTramite(UsuarioDTO usuario,
        Solicitud solicitud) {
        solicitud.setUsuarioLog(usuario.getLogin());
        solicitud.setFechaLog(new java.util.Date());

        entityManager.persist(solicitud);

        LOGGER.info("identificador de la solicitud: " + solicitud.getId());
        if (solicitud.getTramites().size() > 0) {
            Tramite tramite = solicitud.getTramites().get(0);
            LOGGER.info("identificador del primer trámite en la solicitud: " +
                tramite.getId());
        }

        return solicitud;
    }

    /**
     * @see ISolicitudDAO#findSolicitudByNumSolicitudFetchGestionarSolicitudData(String)
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public Solicitud findSolicitudByNumSolicitudFetchGestionarSolicitudData(
        String numeroSolicitud) {
        StringBuilder sql = new StringBuilder("SELECT s FROM Solicitud s");
        sql.append(" LEFT JOIN FETCH s.tramites t");
        sql.append(" LEFT JOIN FETCH t.predio p");
        sql.append(" LEFT JOIN FETCH t.departamento");
        sql.append(" LEFT JOIN FETCH t.municipio");
        sql.append(" WHERE s.numero = :numeroSolicitud");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("numeroSolicitud", numeroSolicitud);
        try {
            Solicitud sol = (Solicitud) query.getSingleResult();
            query = this.entityManager.createNamedQuery("solicitanteSolicitud.findBySolicitudId");
            query.setParameter("solicitudId", sol.getId());
            sol.setSolicitanteSolicituds(query.getResultList());

            if (sol.getSolicitudAvisoRegistro() != null) {
                if (sol.getSolicitudAvisoRegistro().getCirculoRegistral() != null) {
                    sol.getSolicitudAvisoRegistro().getCirculoRegistral()
                        .getNombre();
                }
                if (sol.getSolicitudAvisoRegistro().getAvisoRegistroRechazos() != null) {
                    sol.getSolicitudAvisoRegistro().getAvisoRegistroRechazos()
                        .size();
                    for (AvisoRegistroRechazo arr : sol
                        .getSolicitudAvisoRegistro()
                        .getAvisoRegistroRechazos()) {
                        arr.getAvisoRegistroRechazoPredios().size();
                    }
                }
            }

            for (Tramite t : sol.getTramites()) {
                t.getTramiteDocumentacions().size();
                t.getTramitePredioEnglobes().size();
                for (TramitePredioEnglobe tpe : t.getTramitePredioEnglobes()) {
                    LOGGER.debug(tpe.getPredio().getNumeroPredial());
                }
                t.getSolicitanteTramites().size();
                t.getTramites().size();
                if (t.getPredio() != null) {
                    t.getPredio().getNombre();
                }
            }

            for (SolicitanteSolicitud solSol : sol.getSolicitanteSolicituds()) {
                if (solSol.getDireccionPais() != null) {
                    solSol.getDireccionPais().getNombre();
                }

                if (solSol.getDireccionDepartamento() != null) {
                    solSol.getDireccionDepartamento().getNombre();
                }

                if (solSol.getDireccionMunicipio() != null) {
                    solSol.getDireccionMunicipio().getNombre();
                }

                Hibernate.initialize(solSol.getSolicitante());

            }
            return sol;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see ISolicitudDAO#findSolicitudByNumSolicitud(String)
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Override
    public Solicitud findSolicitudByNumSolicitud(
        String numeroSolicitud) {
        StringBuilder sql = new StringBuilder("SELECT s FROM Solicitud s");
        sql.append(" WHERE s.numero = :numeroSolicitud");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("numeroSolicitud", numeroSolicitud);
        try {
            return (Solicitud) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISolicitudDAO#findSolicitudByNumSolicitudFetchGestionarSolicitudesData(String)
     * @author fabio.navarrete
     */
    @Override
    public Solicitud findSolicitudByNumSolicitudFetchGestionarSolicitudesData(
        String numeroSolicitud) {
        StringBuilder sql = new StringBuilder("SELECT s FROM Solicitud s");
        sql.append(" JOIN FETCH s.tramites t");
        sql.append(" LEFT JOIN FETCH t.predio tp");
        sql.append(" LEFT JOIN FETCH tp.departamento");
        sql.append(" LEFT JOIN FETCH tp.municipio");
        sql.append(" JOIN FETCH t.departamento");
        sql.append(" JOIN FETCH t.municipio");
        sql.append(" LEFT JOIN FETCH s.solicitudAvisoRegistro sar");
        sql.append(" LEFT JOIN FETCH sar.circuloRegistral");
        sql.append(" LEFT JOIN FETCH sar.circuloRegistral");
        sql.append(" WHERE s.numero = :numeroSolicitud");

        try {
            Query query = this.entityManager.createQuery(sql.toString());
            query.setParameter("numeroSolicitud", numeroSolicitud);

            Solicitud sol = (Solicitud) query.getSingleResult();
            sol.getSolicitanteSolicituds().size();
            for (SolicitanteSolicitud solSol : sol.getSolicitanteSolicituds()) {
                if (solSol.getDireccionPais() != null) {
                    solSol.getDireccionPais().getNombre();
                }
                if (solSol.getDireccionDepartamento() != null) {
                    solSol.getDireccionDepartamento().getNombre();
                }
                if (solSol.getDireccionMunicipio() != null) {
                    solSol.getDireccionMunicipio().getNombre();
                }
                solSol.getSolicitante().getCodigoPostal();
            }
            for (Tramite tram : sol.getTramites()) {
                tram.getTramitePredioEnglobes().size();
                for (TramitePredioEnglobe tpe : tram.getTramitePredioEnglobes()) {
                    tpe.getPredio().getDepartamento().getNombre();
                    tpe.getPredio().getMunicipio().getNombre();
                }
                tram.getSolicitanteTramites().size();
                for (SolicitanteTramite solTram : tram.getSolicitanteTramites()) {
                    if (solTram.getDireccionDepartamento() != null) {
                        solTram.getDireccionDepartamento().getNombre();
                    }
                    if (solTram.getDireccionMunicipio() != null) {
                        solTram.getDireccionMunicipio().getNombre();
                    }
                }
                tram.getTramiteDocumentacions().size();
                for (TramiteDocumentacion tramDoc : tram
                    .getTramiteDocumentacions()) {
                    if (tramDoc.getTipoDocumento() != null) {
                        tramDoc.getTipoDocumento().getNombre();
                    }
                    if (tramDoc.getDocumentoSoporte() != null) {
                        tramDoc.getDocumentoSoporte().getArchivo();
                    }
                    if (tramDoc.getDepartamento() != null) {
                        tramDoc.getDepartamento().getNombre();
                    }
                    if (tramDoc.getMunicipio() != null) {
                        tramDoc.getMunicipio().getNombre();
                    }
                }
            }
            if (sol.getSolicitudAvisoRegistro() != null &&
                sol.getSolicitudAvisoRegistro()
                    .getAvisoRegistroRechazos() != null) {
                sol.getSolicitudAvisoRegistro().getAvisoRegistroRechazos()
                    .size();
                for (AvisoRegistroRechazo arr : sol.getSolicitudAvisoRegistro()
                    .getAvisoRegistroRechazos()) {
                    arr.getAvisoRegistroRechazoPredios().size();
                    /*
                     * for(AvisoRegistroRechazoPredio arrp: arr.getAvisoRegistroRechazoPredios()){
                     * arrp.getPredio().getDepartamento().getNombre();
                     * arrp.getPredio().getMunicipio().getNombre(); }
                     */
                }
            }
            return sol;
        } catch (NoResultException e) {
            LOGGER.info("No se encontraron Solicitud por el número " + numeroSolicitud);
            return null;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al buscar la solicitud por número");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISolicitudDAO#buscarSolicitudFetchTramitesBySolicitudId(Long)
     * @author juan.agudelo
     */
    @Override
    public Solicitud buscarSolicitudFetchTramitesBySolicitudId(Long solicitudId) {

        LOGGER.debug("on SolicitudDAOBean#buscarSolicitudFetchTramitesBySolicitudId ...");

        Solicitud answer = null;
        Query query;

        StringBuilder sql = new StringBuilder("SELECT s FROM Solicitud s");
        sql.append(" JOIN FETCH s.tramites t");
        sql.append(" LEFT JOIN FETCH t.predio tp");
        sql.append(" LEFT JOIN FETCH tp.departamento");
        sql.append(" LEFT JOIN FETCH tp.municipio");
        sql.append(" JOIN FETCH t.departamento");
        sql.append(" JOIN FETCH t.municipio");
        sql.append(" LEFT JOIN FETCH s.solicitudAvisoRegistro sar");
        sql.append(" LEFT JOIN FETCH sar.circuloRegistral");
        sql.append(" WHERE s.id = :solicitudId");

        try {
            query = this.entityManager.createQuery(sql.toString());
            query.setParameter("solicitudId", solicitudId);

            Solicitud sol = (Solicitud) query.getSingleResult();

            if (sol.getSolicitanteSolicituds() != null &&
                !sol.getSolicitanteSolicituds().isEmpty()) {
                sol.getSolicitanteSolicituds().size();
                for (SolicitanteSolicitud solSol : sol
                    .getSolicitanteSolicituds()) {
                    if (solSol.getDireccionPais() != null) {
                        solSol.getDireccionPais().getNombre();
                    }
                    if (solSol.getDireccionDepartamento() != null) {
                        solSol.getDireccionDepartamento().getNombre();
                    }
                    if (solSol.getDireccionMunicipio() != null) {
                        solSol.getDireccionMunicipio().getNombre();
                    }
                }
            }

            if (sol.getTramites() != null) {
                for (Tramite tram : sol.getTramites()) {
                    tram.getTramites().size();
                    if (tram.getTramitePredioEnglobes() != null &&
                        !tram.getTramitePredioEnglobes().isEmpty()) {

                        tram.getTramitePredioEnglobes().size();

                        for (TramitePredioEnglobe tpe : tram
                            .getTramitePredioEnglobes()) {
                            tpe.getPredio().getDepartamento().getNombre();
                            tpe.getPredio().getMunicipio().getNombre();
                        }
                    }

                    if (tram.getSolicitanteTramites() != null &&
                        !tram.getSolicitanteTramites().isEmpty()) {

                        tram.getSolicitanteTramites().size();

                        for (SolicitanteTramite solTram : tram
                            .getSolicitanteTramites()) {
                            if (solTram.getDireccionPais() != null) {
                                solTram.getDireccionPais().getNombre();
                            }
                            if (solTram.getDireccionDepartamento() != null) {
                                solTram.getDireccionDepartamento().getNombre();
                            }
                            if (solTram.getDireccionMunicipio() != null) {
                                solTram.getDireccionMunicipio().getNombre();
                            }
                        }
                    }

                    if (tram.getTramiteDocumentacions() != null &&
                        !tram.getTramiteDocumentacions().isEmpty()) {

                        tram.getTramiteDocumentacions().size();

                        for (TramiteDocumentacion tramDoc : tram
                            .getTramiteDocumentacions()) {
                            if (tramDoc.getTipoDocumento() != null) {
                                tramDoc.getTipoDocumento().getNombre();
                            }
                            if (tramDoc.getDocumentoSoporte() != null) {
                                tramDoc.getDocumentoSoporte().getArchivo();
                            }
                            if (tramDoc.getDepartamento() != null) {
                                tramDoc.getDepartamento().getNombre();
                            }
                            if (tramDoc.getMunicipio() != null) {
                                tramDoc.getMunicipio().getNombre();
                            }
                        }
                    }
                }
            }

            if (sol.getSolicitudAvisoRegistro() != null &&
                sol.getSolicitudAvisoRegistro()
                    .getAvisoRegistroRechazos() != null) {
                sol.getSolicitudAvisoRegistro().getAvisoRegistroRechazos()
                    .size();
                for (AvisoRegistroRechazo arr : sol.getSolicitudAvisoRegistro()
                    .getAvisoRegistroRechazos()) {
                    arr.getAvisoRegistroRechazoPredios().size();
                }
            }
            LOGGER.debug("... finished");
            return sol;
        } catch (NoResultException ne) {
            LOGGER.error("No se encontró la Solicitud con id " + solicitudId.toString() +
                ". OJO: Esto no es un error si se está creando la Solicitud");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "SolicitudDAOBean#buscarSolicitudFetchTramitesBySolicitudId");
        }
        return answer;
    }

    // ----------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see ISolicitudDAO#buscarSolicitudConSolicitantesSolicitudPorId(Long)
     */
    @Override
    public Solicitud buscarSolicitudConSolicitantesSolicitudPorId(
        Long solicitudId, boolean fetchTramites) {

        LOGGER.debug("Entra TramiteDAOBean#buscarSolicitudConSolicitantesSolicitudPorId");

        Query q = this.entityManager.createQuery("SELECT s FROM Solicitud s " +
            " WHERE s.id = :solicitudId");

        try {
            q.setParameter("solicitudId", solicitudId);
            Solicitud sol = (Solicitud) q.getSingleResult();

            Hibernate.initialize(sol.getSolicitanteSolicituds());
            if (sol.getSolicitanteSolicituds() != null && !sol.getSolicitanteSolicituds().isEmpty()) {
                for (SolicitanteSolicitud ss : sol.getSolicitanteSolicituds()) {
                    Hibernate.initialize(ss.getSolicitante());
                    Hibernate.initialize(ss.getDireccionPais());
                    Hibernate.initialize(ss.getDireccionDepartamento());
                    Hibernate.initialize(ss.getDireccionMunicipio());
                }
            }

            if (fetchTramites) {
                Hibernate.initialize(sol.getTramites());
            }
            return sol;
        } catch (NoResultException e) {
            LOGGER.error("No se encontró la Solicitud con id " + solicitudId.toString());
            return null;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#buscarSolicitudConSolicitantesSolicitudPorId");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISolicitudDAO#buscarPorContrato(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Solicitud> buscarPorContrato(Long idContrato) {

        LOGGER.debug("on SolicitudDAOBean#buscarPorContrato ");

        List<Solicitud> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT s FROM Solicitud s " +
            " WHERE s.contratoInteradministrativId = :contratoIp_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("contratoIp_p", idContrato);
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "SolicitudDAOBean#buscarPorContrato");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ISolicitudDAO#buscarSolicitudesPorSolicitante(FiltroDatosSolicitud) Hace fetch a
     * SolicitanteSolicitud
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<Solicitud> buscarSolicitudesPorSolicitante(
        FiltroDatosSolicitud filtroDatosSolicitud) {

        List<Solicitud> result = new ArrayList<Solicitud>();

        StringBuilder sql = new StringBuilder("SELECT sol ");
        sql.append(" FROM Solicitud sol,");
        sql.append(" SolicitanteSolicitud solSol");
        sql.append(" WHERE sol.id = solSol.solicitud.id");
        sql.append(" AND sol.tipo IN (").append(ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo()).
            append(",").append(ETramiteTipoTramite.SOLICITUD_DE_CORRECCION.getCodigo()).
            append(",").append(ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.getCodigo()).
            append(",").append(ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION.getCodigo()).
            append(",").append(ETramiteTipoTramite.SOLICITUD_DE_REVISION.getCodigo()).append(")");

        sql.append(" AND solSol.relacion = '").append(
            ESolicitanteSolicitudRelac.PROPIETARIO.toString()).append("'");

        if (filtroDatosSolicitud.getNumero() != null &&
            !filtroDatosSolicitud.getNumero().isEmpty()) {
            sql.append(" AND sol.numero = :numero");
        }

        if (filtroDatosSolicitud.getPrimerNombre() != null &&
            !filtroDatosSolicitud.getPrimerNombre().isEmpty()) {
            sql.append(" AND solSol.primerNombre = :primerNombre");
        }

        if (filtroDatosSolicitud.getSegundoNombre() != null &&
            !filtroDatosSolicitud.getSegundoNombre().isEmpty()) {
            sql.append(" AND solSol.segundoNombre = :segundoNombre");
        }

        if (filtroDatosSolicitud.getPrimerApellido() != null &&
            !filtroDatosSolicitud.getPrimerApellido().isEmpty()) {
            sql.append(" AND solSol.primerApellido = :primerApellido");
        }

        if (filtroDatosSolicitud.getSegundoApellido() != null &&
            !filtroDatosSolicitud.getSegundoApellido().isEmpty()) {
            sql.append(" AND solSol.segundoApellido = :segundoApellido");
        }
        if (filtroDatosSolicitud.getSigla() != null &&
            !filtroDatosSolicitud.getSigla().isEmpty()) {
            sql.append(" AND solSol.sigla = :sigla");
        }
        if (filtroDatosSolicitud.getRazonSocial() != null &&
            !filtroDatosSolicitud.getRazonSocial().isEmpty()) {
            sql.append(" AND solSol.razonSocial = :razonSocial");
        }

        Query query = this.entityManager.createQuery(sql.toString());

        if (filtroDatosSolicitud.getNumero() != null &&
            !filtroDatosSolicitud.getNumero().isEmpty()) {
            query.setParameter("numero", filtroDatosSolicitud.getNumero());
        }

        if (filtroDatosSolicitud.getPrimerNombre() != null &&
            !filtroDatosSolicitud.getPrimerNombre().isEmpty()) {
            query.setParameter("primerNombre",
                filtroDatosSolicitud.getPrimerNombre());
        }

        if (filtroDatosSolicitud.getSegundoNombre() != null &&
            !filtroDatosSolicitud.getSegundoNombre().isEmpty()) {
            query.setParameter("segundoNombre",
                filtroDatosSolicitud.getSegundoNombre());
        }

        if (filtroDatosSolicitud.getPrimerApellido() != null &&
            !filtroDatosSolicitud.getPrimerApellido().isEmpty()) {
            query.setParameter("primerApellido",
                filtroDatosSolicitud.getPrimerApellido());
        }

        if (filtroDatosSolicitud.getSegundoApellido() != null &&
            !filtroDatosSolicitud.getSegundoApellido().isEmpty()) {
            query.setParameter("segundoApellido",
                filtroDatosSolicitud.getSegundoApellido());
        }

        if (filtroDatosSolicitud.getRazonSocial() != null &&
            !filtroDatosSolicitud.getRazonSocial().isEmpty()) {
            query.setParameter("razonSocial",
                filtroDatosSolicitud.getRazonSocial());
        }

        if (filtroDatosSolicitud.getSigla() != null &&
            !filtroDatosSolicitud.getSigla().isEmpty()) {
            query.setParameter("sigla", filtroDatosSolicitud.getSigla());
        }

        try {
            result = (List<Solicitud>) query.getResultList();
            for (Solicitud sol : result) {
                Hibernate.initialize(sol);
                Hibernate.initialize(sol.getSolicitanteSolicituds());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ISolicitudDAO#obtenerSolicitudPorIdconProductosYDocsSoporte(Long)
     * @author javier.barajas
     */
    @Override
    public Solicitud obtenerSolicitudPorIdconProductosYDocsSoporte(
        Long solicitudId) {
        LOGGER.debug("on SolicitudDAOBean#obtenerSolicitudPorIdconProductosYDocsSoporte ...");

        Solicitud answer = null;
        Query query;

        StringBuilder sql = new StringBuilder("SELECT s FROM Solicitud s");
        sql.append(" WHERE s.id = :solicitudId");

        try {
            query = entityManager.createQuery(sql.toString());
            query.setParameter("solicitudId", solicitudId);

            Solicitud sol = (Solicitud) query.getSingleResult();

            if (sol.getSolicitudDocumentos() != null &&
                !sol.getSolicitudDocumentos().isEmpty()) {
                sol.getSolicitudDocumentos().size();
                for (SolicitudDocumentacion sd : sol.getSolicitudDocumentos()) {
                    sd.getTipoDocumento();
                }
            }

            if (sol.getProductoCatastrals() != null &&
                !sol.getProductoCatastrals().isEmpty()) {
                sol.getProductoCatastrals().size();
            }
            LOGGER.debug("... finished");
            return sol;
        } catch (NoResultException ne) {
            LOGGER.error("No se encontró la Solicitud con id " + solicitudId.toString());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                ne, "SolicitudDAOBean#obtenerSolicitudPorIdconProductosYDocsSoporte", "Solicitud",
                solicitudId.toString());
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "SolicitudDAOBean#obtenerSolicitudPorIdconProductosYDocsSoporte");
        } finally {
            return answer;
        }
    }

//end of class	
}
