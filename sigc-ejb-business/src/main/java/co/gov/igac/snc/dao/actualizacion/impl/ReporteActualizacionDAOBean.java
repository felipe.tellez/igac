/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IReporteActualizacionDAO;
import co.gov.igac.snc.fachadas.IActualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ReporteActualizacion;
import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * Implementación de los servicios de persistencia del objeto ReporteActualizacion.
 *
 * @author javier.aponte
 */
@Stateless
public class ReporteActualizacionDAOBean extends GenericDAOWithJPA<ReporteActualizacion, Long>
    implements IReporteActualizacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDAOBean.class);

    /**
     *
     * @author javier.aponte
     *
     * @see IActualizacion#buscarReportesActualizacionPorCodigoMunicipio(String)
     *
     */
    @Override
    public List<Documento> buscarReportesActualizacionPorCodigoMunicipio(
        String codigoMunicipio, String vigencia) {

        //List<ReporteActualizacion> respuesta = null;
        
        List<Documento> respuesta = null;
        String variable = vigencia + "-"+ codigoMunicipio;

        /*String query = "SELECT ra FROM ReporteActualizacion ra " +
            " JOIN FETCH ra.productoCatastralJob pcj " +
            " WHERE ra.municipio.codigo = :codigoMunicipio ";*/
        String query = "SELECT do FROM Documento do " +
                " JOIN FETCH do.tipoDocumento tipdoc "+
                " WHERE tipdoc.clase = 'REPORTES_AVALUO' and do.numeroDocumento = :codigoMunicipio ";

        try {

            Query q = this.entityManager.createQuery(query);
            q.setParameter("codigoMunicipio", variable);

            respuesta = (List<Documento>) q.getResultList();

        } catch (Exception ex) {
            LOGGER.error("Error al buscar reportes actualizacion por municipio actualizacion id" +
                "ReporteActualizacionDAOBean#buscarReportesActualizacionPorCodigoMunicipio.");
            ex.printStackTrace();
        }

        return respuesta;

    }

}
