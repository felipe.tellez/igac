/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRangoReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RangoReporte;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementacion de metodos de acceso a datos de la entidad RangoReporte
 *
 * @author felipe.cadena
 */
@Stateless
public class RangoReporteDAOBean extends GenericDAOWithJPA<RangoReporte, Long> implements
    IRangoReporteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(RangoReporteDAOBean.class);

}
