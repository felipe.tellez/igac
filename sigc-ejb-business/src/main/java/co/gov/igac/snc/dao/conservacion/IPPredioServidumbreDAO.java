package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioServidumbre;

@Local
public interface IPPredioServidumbreDAO extends IGenericJpaDAO<PPredioServidumbre, Long> {

    /**
     * Método que elimina un predio servidumbre por su id.
     *
     * @author leidy.gonzalez
     * @param idServidumbre
     * @return
     */
    public void delete1(Long idServidumbre);

}
