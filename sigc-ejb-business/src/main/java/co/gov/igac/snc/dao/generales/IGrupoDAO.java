package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.Grupo;

/**
 * Clase para las consultas de la clase {@link Grupo}
 *
 * @author david.cifuentes
 */
@Local
public interface IGrupoDAO extends IGenericJpaDAO<Grupo, Long> {

    /**
     * Método que realiza la consulta de todos los roles registrados
     *
     * @author david.cifuentes
     * @return
     */
    public List<Grupo> buscarGrupos();

    /**
     * Método que realiza la consulta de todos los roles registrados para el SNC según su estado.
     *
     * @author david.cifuentes
     * @param activo
     * @return
     */
    public List<Grupo> consultarGrupoPorEstadoActivo(String activo);
    
    /**
     * Método que realiza la consulta de todos los nombres de los roles registrados para el SNC según su estado.
     *
     * @author juan.cruz
     * @param activo
     * @return
     */
    public List<String> obtenerListaNombresGrupo(String activo);

}
