package co.gov.igac.snc.dao.actualizacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IInformacionBasicaCartoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.InformacionBasicaCarto;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session Bean implementation class InformacionBasicaCartoDAOBean
 */
@Stateless
public class InformacionBasicaCartoDAOBean extends GenericDAOWithJPA<InformacionBasicaCarto, Long>
    implements IInformacionBasicaCartoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(InformacionBasicaCartoDAOBean.class);

    /**
     * Default constructor.
     */
    public InformacionBasicaCartoDAOBean() {
    }

    /**
     * {@link #crear(InformacionBasicaCarto)}
     */
    @Override
    public InformacionBasicaCarto crear(
        InformacionBasicaCarto informacionBasicaCartografica)
        throws ExcepcionSNC {

        LOGGER.debug("inicio de creación");
        try {
            entityManager.persist(informacionBasicaCartografica);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("JAAM::Actualización", ESeveridadExcepcionSNC.ERROR, e.
                getMessage(), null);
        }
        LOGGER.debug("fin de creación");
        return informacionBasicaCartografica;
    }
}
