/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de un AvisoRegistroRechazoPredio
 *
 * @author pedro.garcia
 */
@Local
public interface IAvisoRegistroRechazoPredioDAO
    extends IGenericJpaDAO<AvisoRegistroRechazoPredio, Long> {

    /**
     * Hace la búsqueda por id de predio
     *
     * @author pedro.garcia
     * @param predioId
     * @return
     */
    public List<AvisoRegistroRechazoPredio> findByPredioId(Long predioId);

//end of interface
}
