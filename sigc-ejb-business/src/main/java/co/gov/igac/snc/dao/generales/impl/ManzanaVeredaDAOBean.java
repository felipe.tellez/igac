package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.snc.dao.generales.IManzanaVeredaDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;

@Stateless
public class ManzanaVeredaDAOBean extends GenericDAOWithJPA<ManzanaVereda, String> implements
    IManzanaVeredaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManzanaVeredaDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<ManzanaVereda> findByBarrio(String barrioId) {
        Query query = this.entityManager.createQuery(
            "Select m from ManzanaVereda m where m.barrio.codigo = :barrioId");
        query.setParameter("barrioId", barrioId);
        return query.getResultList();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IJurisdiccionDAO#getMunicipioByCodigo(String)
     */
    @Override
    public ManzanaVereda getManzanaVeredaByCodigo(String codigo) {
        ManzanaVereda m = null;
        try {
            Query q = entityManager.createNamedQuery("ManzanaVereda.findManzanaVeredaByCodigo");
            q.setParameter("codigo", codigo);
            m = (ManzanaVereda) q.getSingleResult();
        } catch (Exception e) {
            LOGGER.debug("Error en consulta manzana");
        }

        return m;
    }

    /**
     * @see IManzanaVeredaDAO#obtenerManzanasPorMunicipio(int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ManzanaVereda> obtenerManzanasPorMunicipio(String codigoDepartamentoMunicipio)
        throws Exception {

        LOGGER.debug("ManzanaVeredaDAOBean#obtenerManzanasPorMunicipio...INICIA ");

        List<ManzanaVereda> manzanasMunicipio;
        String stringQuery =
            "select model from ManzanaVereda model where model.codigo like :codigoMunicipioDepartamento  ";

        Query query = this.getEntityManager().createQuery(stringQuery);
        query.setParameter("codigoMunicipioDepartamento", codigoDepartamentoMunicipio + "%");

        manzanasMunicipio = (List<ManzanaVereda>) query.getResultList();

        LOGGER.debug("ManzanaVeredaDAOBean#obtenerManzanasPorMunicipio...FINALIZA ");

        return manzanasMunicipio;

    }

    /**
     * @see IManzanaVeredaDAO#obtenerManzanasPorRango(String, String)
     */
    @Override
    public List<ManzanaVereda> obtenerManzanasPorRango(String manzanaInicial, String manzanaFinal) {

        LOGGER.debug("ManzanaVeredaDAOBean#obtenerManzanasPorRango...INICIA ");

        String codigoManzanaInicial = manzanaInicial.substring(13, 17);
        String codigoManzanaFinal = manzanaFinal.substring(13, 17);;
        String baseNumeroPredial = manzanaInicial.substring(0, 13) + "%";

        String stringQuery = "SELECT mv " +
            "FROM ManzanaVereda mv " +
            "WHERE mv.codigo LIKE:baseNumeroPredial " +
            "and SUBSTRING(mv.codigo,14,4) " +
            "BETWEEN :manzanaInicial and :manzanaFinal";

        Query query = this.getEntityManager().createQuery(stringQuery);
        query.setParameter("baseNumeroPredial", baseNumeroPredial);
        query.setParameter("manzanaInicial", codigoManzanaInicial);
        query.setParameter("manzanaFinal", codigoManzanaFinal);

        try {

            @SuppressWarnings("unchecked")
            List<ManzanaVereda> manzanasEnRango = (List<ManzanaVereda>) query.getResultList();
            return manzanasEnRango;

        } catch (Exception e) {
            LOGGER.debug(
                "Error obteniendo las manzanas en el rando en: ManzanaVeredaDAOBean#obtenerManzanasPorRango ");
            throw new ExcepcionSNC("Error en el método obtenerManzanasPorRango ",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        } finally {
            LOGGER.debug("ManzanaVeredaDAOBean#obtenerManzanasPorRango...FINALIZA ");
        }

    }

    /**
     * @see IManzanaVeredaDAO#obtenerManazanasporMunicipioAsignarCoordinadorPaginacion(String,
     * int...)
     * @author javier.barajas
     */
    @Override
    public List<ManzanaVereda> obtenerManazanasporMunicipioAsignarCoordinadorPaginacion(
        String municipio, int... rowStartIdxAndCount) {

        LOGGER.debug(
            "ManzanaVeredaDAOBean#obtenerManazanasporMunicipioAsignarCoordinadorPaginacion...INICIA ");

        List<ManzanaVereda> manzanasMunicipio;
        String stringQuery =
            "select model from ManzanaVereda model where model.codigo like :codigoMunicipioDepartamento  ";

        Query query = this.getEntityManager().createQuery(stringQuery);
        query.setParameter("codigoMunicipioDepartamento", municipio + "%");

        //manzanasMunicipio = (List<ManzanaVereda>)query.getResultList();		
        LOGGER.debug(
            "ManzanaVeredaDAOBean#obtenerManazanasporMunicipioAsignarCoordinadorPaginacion...FINALIZA ");
        try {
            manzanasMunicipio = (super.findInRangeUsingQuery(
                query, rowStartIdxAndCount));
            return manzanasMunicipio;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }

    }

}
