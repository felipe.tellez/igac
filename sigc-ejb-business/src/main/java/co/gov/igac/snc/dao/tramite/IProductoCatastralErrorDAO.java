package co.gov.igac.snc.dao.tramite;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralError;

/**
 * Servicios de persistencia para el objeto ProductoCatastralError
 *
 * @author javier.aponte
 */
@Local
public interface IProductoCatastralErrorDAO extends IGenericJpaDAO<ProductoCatastralError, Long> {

}
