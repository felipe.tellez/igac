package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 *
 */
@Local
public interface IZonaDAO extends IGenericJpaDAO<Zona, String> {

    public List<Zona> findByMunicipio(String municipioCodigo);

    public Zona getZonaByCodigo(String codigo);

}
