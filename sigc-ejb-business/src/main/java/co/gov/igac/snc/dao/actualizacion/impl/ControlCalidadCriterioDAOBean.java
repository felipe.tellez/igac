package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IControlCalidadCriterioDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ControlCalidadCriterio;

/**
 * Implementación de los servicios de persistencia del objeto ControlCalidadCriterio.
 *
 * @author javier.barajas
 */
@Stateless
public class ControlCalidadCriterioDAOBean extends GenericDAOWithJPA<ControlCalidadCriterio, Long>
    implements IControlCalidadCriterioDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadCriterioDAOBean.class);

}
