/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;

/**
 * Servicios de persistencia del objeto {@link VUsoConstruccionZona}.
 *
 * @author fredy.wilches
 */
@Local
public interface IVUsoConstruccionZonaDAO extends
    IGenericJpaDAO<VUsoConstruccionZona, Long> {

    /**
     * Método que realiza la búsqueda de los usos de construcción asociados a una zona.
     *
     * @author fredy.wilches
     * @param zona
     * @return
     */
    public List<VUsoConstruccionZona> buscarVUsosConstruccionPorZona(String zona);
}
