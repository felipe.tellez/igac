package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;

@Local
public interface ITipoDocumentoDAO extends IGenericJpaDAO<TipoDocumento, Long> {

    /**
     * Retorna el listado de tipos de documentos que son requeridos para un tipo de tramite, clase
     * de mutacion y subtipo
     *
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @return
     */
    public List<TipoDocumento> findByTipoTramite(String tipoTramite,
        String claseMutacion, String subtipo);

    /**
     * Retorna el tipo de documento por clase
     *
     * @param String clase
     * @author juan.agudelo
     * @return
     */
    public TipoDocumento getTipoDocumentoPorClase(String clase);

    /**
     * Retorna la lista de todos los {@link TipoDocumento} filtrados por clase o generado.
     *
     * @author david.cifuentes
     * @param String claseDoc
     * @param String generado
     * @return
     */
    public List<TipoDocumento> findTipoDeDocumentosAll(String tipoDoc,
        String generado);

    /**
     * Método que recupera todos los tipos de documentos que no son generados por el sistema
     *
     * @author juan.agudelo
     * @return
     */
    public List<TipoDocumento> getAllTipoDocumento();

    /**
     * Método para obtener los tipos de documentos adicionales relacionados a un tipo de solicitud
     *
     * @param tipoTramite
     *
     * @author felipe.cadena
     * @return
     */
    public List<TipoDocumento> obtenerTipoDocumentoAvaluoAdicional(String tipoTramite);

    /**
     * Metodo para obtener los tipos de documentos soporte para el servidor de productos
     *
     * @return List<TipoDocumento>
     * @cu CU-TV-PR-0001 INGRESAR SOLICITUD PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public List<TipoDocumento> obtenerTipoDocumentoSoporteProductos();
}
