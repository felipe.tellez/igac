/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;

/**
 * Dao para acceder a la entidad PlantillaSeccionReporte
 *
 * @author david.cifuentes
 */
@Local
public interface IPlantillaSeccionReporteDAO extends
    IGenericJpaDAO<PlantillaSeccionReporte, Long> {

    /**
     * Método que realiza el cargue de la plantilla para una unidad operativa o territorial
     * seleccionada
     *
     * @author david.cifuentes
     * @param estructuraOrganizacionalCodigo
     * @return
     */
    public PlantillaSeccionReporte buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo);

    /**
     * Consulta las urls de los {@link PlantillaSeccionReporte} y si no existen debido al borrado
     * diario las crea nuevamente.
     *
     * @author david.cifuentes
     * @return
     */
    public boolean verificarUrlSeccionesPlantillaReportes();

}
