package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IPlantillaDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;

/**
 * Implementación de los servicios de persistencia de las entidades Plantilla.
 *
 * @author jamir.avila.
 *
 */
@Stateless
public class PlantillaDAOBean extends GenericDAOWithJPA<Plantilla, Long> implements IPlantillaDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(PlantillaDAOBean.class);

    @Override
    public Plantilla recuperarPorCodigo(String codigo) throws ExcepcionSNC {
        Query query = entityManager.createNamedQuery("recuperarPlantillaPorCodigo");
        query.setParameter("codigo", codigo);

        Plantilla plantilla = null;
        try {
            plantilla = (Plantilla) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("JAAM::Generales", ESeveridadExcepcionSNC.ERROR, e.getMessage(),
                null);
        }

        return plantilla;
    }
}
