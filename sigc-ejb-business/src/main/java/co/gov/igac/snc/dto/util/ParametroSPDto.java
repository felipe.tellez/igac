package co.gov.igac.snc.dto.util;

import java.sql.DatabaseMetaData;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Title: SNC
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 *
 * <p>
 * Company: IGAC
 * </p>
 *
 * @author Fredy Wilches
 * @version 1.0
 */
public class ParametroSPDto {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ParametroSPDto.class);

    private String columnName;
    private short columnType;
    private int dataType;
    private String typeName;

    /**
     *
     * @param columnName
     * @param columnType
     * @param dataType
     * @param typeName
     */
    public ParametroSPDto(String columnName, short columnType, int dataType,
        String typeName) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.dataType = dataType;
        this.typeName = typeName;
    }

    public String getColumnName() {
        return columnName;
    }

    public short getColumnType() {
        return columnType;
    }

    public int getDataType() {
        return dataType;
    }

    public String getTypeName() {
        return typeName;
    }

    /**
     *
     */
    /*
     * public String toString() { return " Columna nombre " + columnName + " Tipo " + columnType + "
     * Tipo dato " + dataType + " Nombre tipo " + typeName;
     *
     * }
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     *
     * @return
     */
    /*
     * public boolean isOutputParameter() { boolean isInput = (this.columnType ==
     * DatabaseMetaData.procedureColumnOut || this.columnType ==
     * DatabaseMetaData.procedureColumnInOut || this.columnType ==
     * DatabaseMetaData.procedureColumnReturn); LOGGER.debug("isInput:" + isInput); return isInput;
     * }
     */
    /**
     *
     * @return
     */
    public boolean isInputParameter() {
        boolean isInput = (this.columnType != DatabaseMetaData.procedureColumnOut &&
            this.columnType != DatabaseMetaData.procedureColumnReturn);
        LOGGER.debug("isInput:" + isInput);
        return isInput;

    }

}
