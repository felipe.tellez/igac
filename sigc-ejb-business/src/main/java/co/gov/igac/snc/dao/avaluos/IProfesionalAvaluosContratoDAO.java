package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;

/**
 * Interface que contiene metodos de consulta sobre entity ProfesionalAvaluosContrato
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IProfesionalAvaluosContratoDAO extends
    IGenericJpaDAO<ProfesionalAvaluosContrato, Long> {

    /**
     * Obtiene los contratos anteriores para un avaluador {@link ProfesionalAvaluo}
     *
     * @author christian.rodriguez
     * @param idProfesional id del avaluador
     * @return contratos anteriores del avaluador seleccionado
     */
    public List<ProfesionalAvaluosContrato> consultarAnterioresPorIdProfesional(Long idProfesional);

    /**
     * Consulta el contrato del avaluador con el id especificado. CArga la lista de adiciones,
     * cesiones y pagos
     *
     * @author christian.rodriguez
     * @param idContrato id del contrato a consultar
     * @return {@link ProfesionalAvaluosContrato} con el id especificado
     */
    public ProfesionalAvaluosContrato obtenerPorIdConAtributos(Long idContrato);

}
