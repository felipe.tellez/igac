package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PModeloConstruccionFoto;

/**
 * Servicios de persistencia del objeto {@link PModeloConstruccionFoto}.
 *
 * @author david.cifuentes
 */
@Local
public interface IPModeloConstruccionFotoDAO extends
    IGenericJpaDAO<PModeloConstruccionFoto, Long> {

    /**
     * Método que busca los objetos {@link PModeloConstruccionFoto} relacionados con una unidad de
     * un modelo de construcción.
     *
     * @author david.cifuentes
     * @param unidadDelModeloId
     * @return
     */
    public List<PModeloConstruccionFoto> findByUnidadDelModeloIdFetchDocumento(
        Long unidadDelModeloId);

    // -----------------------------------------------------//
    /**
     * Metodo para guardar una lista de {@link PModeloConstruccionFoto} de una unidad de un modelo
     * de construcción.
     *
     * @author david.cifuentes
     * @param listPFotosUnidadDelModelo
     */
    public void guardarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> listPFotosUnidadDelModelo);

    // -----------------------------------------------------//
    /**
     * Actualiza los registros de la tabla {@link PModeloConstruccionFoto} asociados a una unidad de
     * un modelo de construcción. Lo que hace es borrar los registros existentes e insertar los
     * nuevos.
     *
     * Copia del método de pedro.garcia IPFotoDAO#updateDataByUnidadConstruccion ajustado para
     * modelos de construcción.
     *
     * @author david.cifuentes
     * @param pModeloConstruccionFotoList PRE: la lista es no vacía OJO: solo debe usarse en el caso
     * en que se crean PModeloConstruccionFoto para PFmModeloConstruccion
     */
    public void actualizarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> pModeloConstruccionFotoList);

    // -----------------------------------------------------//
    /**
     * Metodo para persistir una lista de {@link PModeloConstruccionFoto} de una unidad de un modelo
     * de construcción.
     *
     * @author david.cifuentes
     * @param pModeloConstruccionFotoList
     */
    public void persistPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> pModeloConstruccionFotoList);

    // -----------------------------------------------------//
    /**
     * Metodo que permite almacenar un {@link PModeloConstruccionFoto} asociados a una unidad de un
     * modelo de construcción.
     *
     * Copia del método guardarPFoto ajustado para modelos de construcción.
     *
     * @author david.cifuentes
     * @param pModeloConstruccionFoto
     * @return
     */
    public PModeloConstruccionFoto guardarPModeloConstruccionFoto(
        PModeloConstruccionFoto pModeloConstruccionFoto);

}
