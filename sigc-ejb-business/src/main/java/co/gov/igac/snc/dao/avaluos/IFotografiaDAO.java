package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;

@Local
public interface IFotografiaDAO extends IGenericJpaDAO<Fotografia, Long> {

}
