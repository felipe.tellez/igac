package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidad;

/**
 * @see IControlCalidadDAO
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class ControlCalidadDAOBean extends
    GenericDAOWithJPA<ControlCalidad, Long> implements IControlCalidadDAO {
}
