/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface para las operaciones DB de la entidad TramiteDigitalizacion
 *
 * @author felipe.cadena
 */
@Local
public interface ITramiteDigitalizacionDAO extends IGenericJpaDAO<TramiteDigitalizacion, Long> {

    /**
     * Metodo para consultar las movidas asociadas un tramite
     *
     * @author felipe.cadena
     *
     * @param idTramite
     * @return
     */
    public List<TramiteDigitalizacion> buscarPorIdTramite(Long idTramite);

    /**
     * Bucar las digitalizaciones activas de un digitalizador en particular
     *
     * @author felipe.cadena
     *
     * @param digitalizador login del ususario Digitalizador.
     *
     *
     */
    public List<TramiteDigitalizacion> buscarPorDigitalizador(String digitalizador);

    /**
     * Método para la digitalización activa de un tramite y un digitalizador
     *
     * @author felipe.cadena
     *
     * @param digitalizador
     * @param idTramite
     * @return
     */
    public TramiteDigitalizacion buscarPorDigitalizadorYTramite(String digitalizador, Long idTramite);

    /**
     * Metodo para recuperar el ultimo regsitro por tramite y ejecutor.
     *
     * @author felipe.cadena
     *
     * @param ejecutor
     * @param idTramite
     * @return
     */
    public TramiteDigitalizacion buscarPorEjecutorYTramite(String ejecutor, Long idTramite);
}
