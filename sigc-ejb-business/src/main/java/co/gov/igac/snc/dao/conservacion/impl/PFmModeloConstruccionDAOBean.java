package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFmModeloConstruccionDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;

/**
 * Implementación de los servicios de persistencia del objeto {@link PFmModeloConstruccion}.
 *
 * @author david.cifuentes
 */
@Stateless
public class PFmModeloConstruccionDAOBean extends
    GenericDAOWithJPA<PFmModeloConstruccion, Long> implements
    IPFmModeloConstruccionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PFmModeloConstruccionDAOBean.class);

    /**
     * @author david.cifuentes
     * @see IPFmModeloConstruccionDAO#buscarUnidadDelModeloDeConstruccionPorSuId(Long)
     */
    @Override
    public PFmModeloConstruccion buscarUnidadDelModeloDeConstruccionPorSuId(
        Long pFmModeloConstruccionId) {

        PFmModeloConstruccion answer = null;

        String sql = "SELECT DISTINCT pfmmc FROM PFmModeloConstruccion pfmmc" +
            " LEFT JOIN FETCH pfmmc.PFmConstruccionComponentes pfmcc" +
            " LEFT JOIN FETCH pfmmc.usoConstruccion " +
            " WHERE pfmmc.id = :pFmModeloConstruccionId";

        Query query = this.entityManager.createQuery(sql);
        query.setParameter("pFmModeloConstruccionId", pFmModeloConstruccionId);
        try {
            answer = (PFmModeloConstruccion) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;
    }

}
