package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IPaisDAO;
import javax.ejb.Stateless;

@Stateless
public class PaisDAOBean extends GenericDAOWithJPA<Pais, String> implements IPaisDAO {

}
