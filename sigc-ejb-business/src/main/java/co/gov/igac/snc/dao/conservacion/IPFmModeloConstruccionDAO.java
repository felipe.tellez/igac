package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;

/**
 * Servicios de persistencia del objeto {@link PFmModeloConstruccion}.
 *
 * @author david.cifuentes
 */
@Local
public interface IPFmModeloConstruccionDAO extends
    IGenericJpaDAO<PFmModeloConstruccion, Long> {

    // ----------------------------------------------------- //
    /**
     * Metodo que busca un {@link PFmModeloConstruccion} por su id, trayendo las
     * PFmConstruccionComponente asociadas.
     *
     * @author david.cifuentes
     * @param idPFmModeloConstruccion
     */
    public PFmModeloConstruccion buscarUnidadDelModeloDeConstruccionPorSuId(
        Long idPFmModeloConstruccion);

}
