package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity ControlCalidadAvaluoRev
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IControlCalidadAvaluoRevDAO extends
    IGenericJpaDAO<ControlCalidadAvaluoRev, Long> {

    /**
     * Método que consulta y devuelve el {@link ControlCalidadAvaluoRev} que tenga el id
     * especificado. Carga el {@link ControlCalidadAvaluo} asociado
     *
     * @author christian.rodriguez
     * @param idControlCalidadAvaluooRev id del {@link ControlCalidadAvaluoRev}
     * @return {@link ControlCalidadAvaluoRev} encontrado o null si no se encontraron resultado
     */
    public ControlCalidadAvaluoRev obtenerPorIdConAtributos(
        Long idControlCalidadAvaluooRev);

    /**
     * Método para calcular el número de devoluciones que tuvo un avalúo durante el control de
     * calidad, ya sea en trerritorial o sede central
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param idAsignacion
     * @param actividadCC tipo de actividad de control de calidad {@link EAvaluoAsignacionProfActi}
     * @return
     */
    public Long calcularNumeroDevoluciones(Long idAvaluo,
        Long idAsignacion, String actividadCC);

}
