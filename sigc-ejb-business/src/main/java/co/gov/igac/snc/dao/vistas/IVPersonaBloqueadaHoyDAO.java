/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VPersonaBloqueadaHoy;

/**
 * Interfaz para el dao de VPersonaBloqueadaHoy
 *
 * @author juan.agudelo
 */
@Local
public interface IVPersonaBloqueadaHoyDAO extends IGenericJpaDAO<VPersonaBloqueadaHoy, Long> {

}
