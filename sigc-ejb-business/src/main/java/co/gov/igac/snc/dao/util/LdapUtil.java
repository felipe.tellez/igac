package co.gov.igac.snc.dao.util;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LdapUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(LdapUtil.class);

    /**
     *
     * @param attrs
     * @return
     */
    public static boolean extraerContratista(Attributes attrs) {
        boolean isContratista = false;
        try {
            Attribute attrDesc = attrs.get("description");
            if (attrDesc != null) {
                String description = (String) attrDesc.get();
                description = description.trim().toUpperCase();
                if (description.indexOf("CONTRATISTA") > -1) {
                    isContratista = true;
                }
            }
        } catch (NamingException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return isContratista;
    }

}
