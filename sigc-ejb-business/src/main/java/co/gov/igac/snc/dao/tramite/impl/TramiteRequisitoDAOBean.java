/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteRequisitoDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;

@Stateless
public class TramiteRequisitoDAOBean extends
    GenericDAOWithJPA<TramiteRequisito, Long> implements
    ITramiteRequisitoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteRequisitoDAOBean.class);

    @Resource
    SessionContext sessionContext;

    /**
     * @see ITramiteRequisitoDAO#conseguirDocumentacionTramiteRequerida(Tramite)
     * @author juan.agudelo
     */
    public List<TramiteRequisito> conseguirDocumentacionTramiteRequerida(
        Tramite tramite) {

        //validar condicion propiedad
        String cp = null;
        if (tramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
            tramite.getClaseMutacion().equals(EMutacionClase.SEGUNDA.getCodigo())) {

            if (tramite.getSubtipo().equals(EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                cp = tramite.getPredio().getCondicionPropiedad();
                if (cp != null) {
                    if (!cp.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
                        cp = EPredioCondicionPropiedad.CP_0.getCodigo();
                    }
                }
            }
            if (tramite.getSubtipo().equals(EMutacion2Subtipo.ENGLOBE.getCodigo())) {

                int nPredios = tramite.getTramitePredioEnglobes().size();
                int nPredios05 = 0;
                for (TramitePredioEnglobe tpe : tramite.getTramitePredioEnglobes()) {
                    String auxCP = tpe.getPredio().getCondicionPropiedad();

                    if (!auxCP.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
                        cp = EPredioCondicionPropiedad.CP_0.getCodigo();
                        break;
                    }
                    nPredios05++;

                }

                if (nPredios == nPredios05) {
                    cp = EPredioCondicionPropiedad.CP_5.getCodigo();
                }
            }
        } else if (tramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
            tramite.getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo())) {
            cp = EPredioCondicionPropiedad.CP_0.getCodigo();
        } else {
            cp = tramite.getPredio().getCondicionPropiedad();
        }

        LOGGER.debug("Entra en TramiteRequisitoDAOBean#conseguirDocumentacionTramiteRequerida");

        StringBuilder sql = new StringBuilder(
            "SELECT DISTINCT tr FROM TramiteRequisito tr");
        sql.append(" JOIN FETCH tr.tramiteRequisitoDocumentos trd");
        sql.append(" JOIN FETCH trd.tipoDocumento td WHERE 1 = 1");
        if (tramite.getTipoTramite() != null) {
            sql.append(" AND tr.tipoTramite= :tipoTramite");
        }
        if (tramite.getClaseMutacion() != null) {
            sql.append(" AND tr.claseMutacion= :claseMutacion");
        }
        if (tramite.getClasificacion() != null) {
            sql.append(" AND tr.clasificacion= :clasificacion");
        }
        if (tramite.getSubtipo() != null) {
            sql.append(" AND tr.subtipo= :subtipo");
        }
        if (tramite.getPredio() != null &&
            tramite.getPredio().getZonaUnidadOrganica() != null) {
            sql.append("  AND trd.zonaUnidadOrganica= :zonaUnidadOrganica");
        }

        sql.append("  AND trd.requerido= :requerido");
        sql.append("  AND trd.condicionPropiedad= :cPropiedad");
        Query q = entityManager.createQuery(sql.toString());
        if (tramite.getTipoTramite() != null) {
            q.setParameter("tipoTramite", tramite.getTipoTramite());
        }
        if (tramite.getClaseMutacion() != null) {
            q.setParameter("claseMutacion", tramite.getClaseMutacion());
        }
        if (tramite.getSubtipo() != null) {
            q.setParameter("subtipo", tramite.getSubtipo());
        }
        if (tramite.getClasificacion() != null) {
            q.setParameter("clasificacion", tramite.getClasificacion());
        }
        if (tramite.getPredio() != null &&
            tramite.getPredio().getZonaUnidadOrganica() != null) {
            q.setParameter("zonaUnidadOrganica", tramite.getPredio()
                .getZonaUnidadOrganica());
        }
        q.setParameter("requerido", ESiNo.SI.getCodigo());
        q.setParameter("cPropiedad", cp);

        try {
            @SuppressWarnings("unchecked")
            List<TramiteRequisito> tramiteDocumentacionRequeridoTemp = q
                .getResultList();
            List<TramiteRequisito> tramiteDocumentacionRequerido = new ArrayList<TramiteRequisito>();
            if (tramiteDocumentacionRequeridoTemp != null &&
                tramiteDocumentacionRequeridoTemp.size() > 0) {
                tramiteDocumentacionRequerido
                    .add(tramiteDocumentacionRequeridoTemp.get(0));
            }
            return tramiteDocumentacionRequerido;
        } catch (IndexOutOfBoundsException ie) {
            LOGGER.error(ie.getMessage(), ie);
            List<TramiteRequisito> tramiteDocumentacionRequerido = new ArrayList<TramiteRequisito>();
            return tramiteDocumentacionRequerido;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

}
