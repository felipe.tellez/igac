package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IControlCalidadMuestraDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ControlCalidadMuestra;

/**
 * Implementación de los servicios de persistencia del objeto LevantamientoAsignacion.
 *
 * @author javier.barajas
 */
@Stateless
public class ControlCalidadMuestraDAOBean extends GenericDAOWithJPA<ControlCalidadMuestra, Long>
    implements IControlCalidadMuestraDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadMuestraDAOBean.class);

}
