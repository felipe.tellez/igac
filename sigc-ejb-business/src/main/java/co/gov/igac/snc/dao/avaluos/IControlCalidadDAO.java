package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidad;

/**
 *
 * Interfaz para los métodos de bd de la tabla CONTROL_CALIDAD
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IControlCalidadDAO extends IGenericJpaDAO<ControlCalidad, Long> {

}
