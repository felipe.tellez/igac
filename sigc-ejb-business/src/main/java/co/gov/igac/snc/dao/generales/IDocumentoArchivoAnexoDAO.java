package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;

/**
 *
 * @author juan.agudelo
 *
 */
@Local
public interface IDocumentoArchivoAnexoDAO extends IGenericJpaDAO<DocumentoArchivoAnexo, Long> {

}
