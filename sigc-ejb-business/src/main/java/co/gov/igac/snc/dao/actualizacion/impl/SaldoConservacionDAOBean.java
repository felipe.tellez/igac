package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.ISaldoConservacionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.SaldoConservacion;
import co.gov.igac.snc.persistence.entity.generales.Numeracion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto SaldoConservacion.
 *
 * @author franz.gamba
 */
@Stateless
public class SaldoConservacionDAOBean extends GenericDAOWithJPA<SaldoConservacion, Long> implements
    ISaldoConservacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(SaldoConservacionDAOBean.class);

    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.dao.actualizacion.ISaldoConservacionDAO#getSaldosConservacionByActualizacionId(java.lang.Long)
     */
    @Override
    public List<SaldoConservacion> getSaldosConservacionByActualizacionId(
        Long actualizacionId) {

        List<SaldoConservacion> saldos = null;

        String query = "SELECT s FROM SaldoConservacion s " +
            " WHERE s.actualizacion.id = :actualizacionId";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("actualizacionId", actualizacionId);

            saldos = (List<SaldoConservacion>) q.getResultList();

        } catch (Exception e) {
            LOGGER.error("Error en SaldoConservacionDAOBean: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return saldos;
    }

    @Override
    public List<SaldoConservacion> getSaldosConservacionByNumeroPredial(
        String numeroPredial) {
        LOGGER.debug("#etSaldosConservacionByNumeroPredial...INICIA");
        try {
            String queryString =
                "SELECT sc FROM SaldoConservacion sc WHERE sc.numeroPredial=:numeroPredial";
            Query query = this.getEntityManager().createQuery(queryString);
            query.setParameter("numeroPredial", numeroPredial);
            @SuppressWarnings("unchecked")
            List<SaldoConservacion> saldos = (List<SaldoConservacion>) query.getResultList();
            return saldos;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        } finally {
            LOGGER.debug("#etSaldosConservacionByNumeroPredial...INICIA");

        }
    }

    /**
     *
     */
}
