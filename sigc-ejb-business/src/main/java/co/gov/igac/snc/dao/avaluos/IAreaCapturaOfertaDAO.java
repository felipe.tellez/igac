/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;

/**
 * interfaz para los métodos de bd de la tabla AREA_CAPTURA_OFERTA
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Local
public interface IAreaCapturaOfertaDAO extends IGenericJpaDAO<AreaCapturaOferta, Long> {

    /**
     * Método para consultar el listado de Areas para Oferta sin comisionar y con recolector
     * asociado
     *
     * @author christian.rodriguez
     * @param departamentoCodigo Código del departamento asociado a las areas
     * @param municipioCodigo Código del departamento asociado a los departamentos
     * @return lista de las áreas que no se han comisionado
     */
    public List<AreaCapturaOferta> getAreasSinComisionarConRecolector(String departamentoCodigo,
        String municipioCodigo);

    /**
     * Busca y retorna el area de captura con el id especificado
     *
     * @param areaId id del área
     * @return area de captura de ofertas con el id especificado
     */
    public AreaCapturaOferta getAreaCapturaOfertaById(Long areaId);

    /**
     * Retorna la lista de áreas captura de ofertas que tengan el estado indicado
     *
     * @author christian.rodriguez
     * @param estrucOrgCod código de la estructura organizacional
     * @param estadoAreaCaptura estado del área que debería corresponder a un estado de la
     * enumeración EOfertaAreaCapturaOfertaEstado
     * @return lista de comisiones de ofertas que tengan el estado indicado y pertenezcan a la
     * estructura organizacional indicada
     */
    public List<AreaCapturaOferta> getAreaCapturaOfertaPorEstadoYEstrucOrg(String estrucOrgCod,
        String estadoAreaCaptura);

    /**
     * obtiene el área con las listas de asociaciones con las tablas REGION_CAPTURA_OFERTA y
     * DETALLE_CAPTURA_OFERTA
     *
     * @author pedro.garcia
     * @param areaCapturaOfertaId
     * @return
     */
    public AreaCapturaOferta getByIdConAsociaciones(Long areaCapturaOfertaId);

//end of interface
}
