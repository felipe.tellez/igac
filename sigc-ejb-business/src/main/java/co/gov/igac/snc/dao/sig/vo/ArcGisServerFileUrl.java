package co.gov.igac.snc.dao.sig.vo;

/**
 *
 * @author juan.mendez
 *
 */
public class ArcGisServerFileUrl {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
