/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVEjecutorComisionDAO;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class VEjecutorComisionDAOBean extends GenericDAOWithJPA<VEjecutorComision, Long>
    implements IVEjecutorComisionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(VEjecutorComisionDAOBean.class);
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVEjecutorComisionDAO#findByNumeroComision(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<VEjecutorComision> findByNumeroComision(String numeroComision) {

        LOGGER.debug("on VEjecutorComisionDAOBean#findByNumeroComision ...");
        List<VEjecutorComision> answer = null;
        Query query;

        try {
            query = this.entityManager.createNamedQuery("findByNumeroComision");
            query.setParameter("numComisionP", numeroComision);
            answer = query.getResultList();

            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "VEjecutorComisionDAOBean#findByNumeroComision");
        }

        return answer;
    }

}
