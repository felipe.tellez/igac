package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoPredioConstruccion
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IAvaluoPredioConstruccionDAO extends IGenericJpaDAO<AvaluoPredioConstruccion, Long> {

    /**
     * Método para determinar si existen contrucciones asociadas a un avalúo o a los predios del
     * avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public Boolean calcularConstruccionesPreviasAvaluo(Long idAvaluo);

    /**
     * Método para eliminar las construcciones asociadas a un avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public boolean eliminarConstruccionesPreviasAvaluo(Long idAvaluo);

}
