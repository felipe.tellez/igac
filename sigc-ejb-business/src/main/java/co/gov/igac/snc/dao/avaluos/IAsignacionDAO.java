package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;

import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity Asignacion
 *
 * @author felipe.cadena
 */
@Local
public interface IAsignacionDAO extends
    IGenericJpaDAO<Asignacion, Long> {

    /**
     * Método para consultar la asignación actual relacionada a un avaluo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param actividad actividad asociada a la asociación, debe ser un codigo de
     * {@link EAvaluoAsignacionProfActi}
     * @return
     */
    public Asignacion consultarAsignacionActualAvaluo(Long idAvaluo, String actividad);

}
