package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionContratoDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;

/**
 * Implementación de los servicios de persistencia del objeto ActualizaciónDocumento.
 *
 * @author franz.gamba
 */
@Stateless
public class ActualizacionContratoDAOBean extends GenericDAOWithJPA<ActualizacionContrato, Long>
    implements IActualizacionContratoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionContratoDAOBean.class);

    @Override
    public List<ActualizacionContrato> obtenerContratosProActualizacion(
        Long actualizacionId) {

        LOGGER.debug("obtenerContratosProActualizacion");
        String query = "SELECT ac " +
            "FROM ActualizacionContrato ac " +
            "WHERE ac.actualizacion.id = :idActualizacion";

        Query q = entityManager.createQuery(query);
        q.setParameter("idActualizacion", actualizacionId);
        try {
            List<ActualizacionContrato> contratos =
                (List<ActualizacionContrato>) q.getResultList();
            return contratos;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }

    }

}
