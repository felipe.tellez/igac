/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPersonaDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class PersonaDAOBean extends GenericDAOWithJPA<Persona, Long> implements
    IPersonaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PersonaDAOBean.class);

    /**
     * @author juan.agudelo
     * @see IPersonasBloqueoDAO#buscarPersonasBloqueo( FiltroDatosConsultaPersonasBloqueo
     */
    @Override
    public Persona buscarPersonaByNombreDocumentoDV(Persona personaDatos) {
        LOGGER.debug("executing PersonaDAOBean#buscarPersonaByNombreDocumentoDV");

        String query;

        if (personaDatos.getRazonSocial() != null &&
            !personaDatos.getRazonSocial().isEmpty()) {
            query = "SELECT p FROM Persona p" +
                " WHERE  tipoIdentificacion= :tipoIdentificacion" +
                " AND numeroIdentificacion= :numeroIdentificacion" +
                " AND razonSocial= :razonSocial" +
                " AND digitoVerificacion= :digitoVerificacion";

        } else {
            if (personaDatos.getSegundoNombre() != null &&
                !personaDatos.getSegundoNombre().isEmpty()) {
                query = "SELECT p FROM Persona p" +
                    " WHERE  tipoIdentificacion= :tipoIdentificacion" +
                    " AND numeroIdentificacion= :numeroIdentificacion" +
                    " AND primerNombre= :primerNombre" +
                    " AND segundoNombre= :segundoNombre" +
                    " AND primerApellido= :primerApellido" +
                    " AND segundoApellido= :segundoApellido";
            } else {
                query = "SELECT p FROM Persona p" +
                    " WHERE  tipoIdentificacion= :tipoIdentificacion" +
                    " AND numeroIdentificacion= :numeroIdentificacion" +
                    " AND primerNombre= :primerNombre" +
                    " AND primerApellido= :primerApellido" +
                    " AND segundoApellido= :segundoApellido";
            }
        }

        Query q = entityManager.createQuery(query);
        q.setParameter("tipoIdentificacion",
            personaDatos.getTipoIdentificacion());
        q.setParameter("numeroIdentificacion",
            personaDatos.getNumeroIdentificacion());

        if (personaDatos.getRazonSocial() != null &&
            !personaDatos.getRazonSocial().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                personaDatos.getTipoIdentificacion());
            q.setParameter("numeroIdentificacion",
                personaDatos.getNumeroIdentificacion());
            q.setParameter("razonSocial", personaDatos.getRazonSocial());
            q.setParameter("digitoVerificacion",
                personaDatos.getDigitoVerificacion());
        } else {
            q.setParameter("primerNombre", personaDatos.getPrimerNombre());
            if (personaDatos.getSegundoNombre() != null &&
                !personaDatos.getSegundoNombre().isEmpty()) {
                q.setParameter("segundoNombre", personaDatos.getSegundoNombre());
            }
            q.setParameter("primerApellido", personaDatos.getPrimerApellido());
            q.setParameter("segundoApellido", personaDatos.getSegundoApellido());
        }

        try {
            return (Persona) q.getSingleResult();
        } catch (NoResultException ne) {
            LOGGER.debug("Error: " + ne.getMessage());
            return null;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @author fabio.navarrete
     * @see IPersonaDAO#findByTipoNumeroIdentificacion(String, String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Persona> findByTipoNumeroIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion) {
        try {
            String sql = "SELECT p FROM Persona p" +
                " WHERE p.tipoIdentificacion = :tipoIdentificacion" +
                " AND p.numeroIdentificacion = :numeroIdentificacion";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("tipoIdentificacion", tipoIdentificacion);
            query.setParameter("numeroIdentificacion", numeroIdentificacion);
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error en el proceso de búsqueda de Personas", e);
        }
    }

    /**
     * @see IPersonaDAO#buscarPersonaPorNumeroYTipoIdentificacion(String, String)
     * @author juan.agudelo
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Persona> buscarPersonaPorNumeroYTipoIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion) {
        LOGGER.debug("executing PersonaDAOBean#buscarPersonaPorNumeroYTipoIdentificacion");

        List<Persona> answer = null;
        Query query;
        String queryToExecute;

        queryToExecute = "SELECT p FROM Persona p" +
            " WHERE p.numeroIdentificacion = :numeroIdentificacion" +
            " AND p.tipoIdentificacion = :tipoIdentificacion";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("numeroIdentificacion", numeroIdentificacion);
        query.setParameter("tipoIdentificacion", tipoIdentificacion);

        try {
            answer = (List<Persona>) query.getResultList();

        } catch (NoResultException e) {
            return answer;
        } catch (IndexOutOfBoundsException ne) {
            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IPersonaDAO#buscarPersonaPorNombrePartesTipoDocNumDoc(PersonaBloqueo)
     * @author juan.agudelo
     */
    @Override
    public Persona buscarPersonaPorNombrePartesTipoDocNumDoc(Persona persona) {
        LOGGER.debug("executing PersonaDAOBean#buscarPersonaPorNombrePartesTipoDocNumDoc");
        Persona answer = null;
        List<Persona> personas;
        Query q;
        StringBuilder query = new StringBuilder();

        query.append("SELECT p from Persona p");
        query.append(" WHERE 1 = 1");
        if (persona.getTipoIdentificacion() != null &&
            !persona.getTipoIdentificacion().isEmpty()) {
            query.append(" AND p.tipoIdentificacion = :tipoIdentificacion");
        }
        if (persona.getNumeroIdentificacion() != null &&
            !persona.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND p.numeroIdentificacion = :numeroIdentificacion");
        }
        if (persona.getPrimerNombre() != null &&
            !persona.getPrimerNombre().isEmpty()) {
            query.append(" AND UPPER p.primerNombre = :primerNombre");
        }
        if (persona.getSegundoNombre() != null &&
            !persona.getSegundoNombre().isEmpty()) {
            query.append(" AND UPPER p.segundoNombre = :segundoNombre");
        }
        if (persona.getPrimerApellido() != null &&
            !persona.getPrimerApellido().isEmpty()) {
            query.append(" AND UPPER p.primerApellido = :primerApellido");
        }
        if (persona.getSegundoApellido() != null &&
            !persona.getSegundoApellido().isEmpty()) {
            query.append(" AND UPPER p.segundoApellido = :segundoApellido");
        }
        if (persona.getRazonSocial() != null &&
            !persona.getRazonSocial().isEmpty()) {
            query.append(" AND UPPER p.razonSocial = :razonSocial");
        }
        if (persona.getDigitoVerificacion() != null &&
            !persona.getDigitoVerificacion().isEmpty()) {
            query.append(" AND p.digitoVerificacion = :digitoVerificacion");
        }

        q = entityManager.createQuery(query.toString());

        if (persona.getTipoIdentificacion() != null &&
            !persona.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                persona.getTipoIdentificacion());
        }
        if (persona.getNumeroIdentificacion() != null &&
            !persona.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                persona.getNumeroIdentificacion());
        }
        if (persona.getPrimerNombre() != null &&
            !persona.getPrimerNombre().isEmpty()) {
            q.setParameter("primerNombre", persona.getPrimerNombre().toUpperCase());
        }
        if (persona.getSegundoNombre() != null &&
            !persona.getSegundoNombre().isEmpty()) {
            q.setParameter("segundoNombre", persona.getSegundoNombre().toUpperCase());
        }
        if (persona.getPrimerApellido() != null &&
            !persona.getPrimerApellido().isEmpty()) {
            q.setParameter("primerApellido", persona.getPrimerApellido().toUpperCase());
        }
        if (persona.getSegundoApellido() != null &&
            !persona.getSegundoApellido().isEmpty()) {
            q.setParameter("segundoApellido", persona.getSegundoApellido().toUpperCase());
        }
        if (persona.getRazonSocial() != null &&
            !persona.getRazonSocial().isEmpty()) {
            q.setParameter("razonSocial", persona.getRazonSocial());
        }
        if (persona.getDigitoVerificacion() != null &&
            !persona.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                persona.getDigitoVerificacion());
        }

        try {
            personas = q.getResultList();
            if (personas != null && personas.size() > 1) {
                answer = personas.get(0);
            }
        } catch (NoResultException e) {
            return answer;
        } catch (NonUniqueResultException ne) {
            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see IPersonaDAO#buscarPersonaPorNombrePartesTipoDocNumDocTemp(PersonaBloqueo)
     * @author juan.agudelo
     */
    @Override
    public List<Persona> buscarPersonaPorNombrePartesTipoDocNumDocTemp(Persona persona) {
        LOGGER.debug("executing PersonaDAOBean#buscarPersonaPorNombrePartesTipoDocNumDoc");
        List<Persona> answer = null;
        Query q;
        StringBuilder query = new StringBuilder();

        query.append("SELECT p from Persona p");
        query.append(" WHERE 1 = 1");
        if (persona.getTipoIdentificacion() != null &&
            !persona.getTipoIdentificacion().isEmpty()) {
            query.append(" AND p.tipoIdentificacion = :tipoIdentificacion");
        }
        if (persona.getNumeroIdentificacion() != null &&
            !persona.getNumeroIdentificacion().isEmpty()) {
            query.append(" AND p.numeroIdentificacion = :numeroIdentificacion");
        }
        if (persona.getPrimerNombre() != null &&
            !persona.getPrimerNombre().isEmpty()) {
            query.append(" AND UPPER (p.primerNombre) like :primerNombre");
        }

        if (persona.getSegundoNombre() != null &&
            !persona.getSegundoNombre().isEmpty()) {
            query.append(" AND UPPER (p.segundoNombre) = :segundoNombre");
        }
        if (persona.getPrimerApellido() != null &&
            !persona.getPrimerApellido().isEmpty()) {
            query.append(" AND UPPER (p.primerApellido) LIKE :primerApellido");
        }
        if (persona.getSegundoApellido() != null &&
            !persona.getSegundoApellido().isEmpty()) {
            query.append(" AND UPPER (p.segundoApellido) = :segundoApellido");
        }
        if (persona.getRazonSocial() != null &&
            !persona.getRazonSocial().isEmpty()) {
            query.append(" AND UPPER (p.razonSocial) = :razonSocial");
        }
        if (persona.getDigitoVerificacion() != null &&
            !persona.getDigitoVerificacion().isEmpty()) {
            query.append(" AND p.digitoVerificacion = :digitoVerificacion");
        }

        q = entityManager.createQuery(query.toString());

        if (persona.getTipoIdentificacion() != null &&
            !persona.getTipoIdentificacion().isEmpty()) {
            q.setParameter("tipoIdentificacion",
                persona.getTipoIdentificacion());
        }
        if (persona.getNumeroIdentificacion() != null &&
            !persona.getNumeroIdentificacion().isEmpty()) {
            q.setParameter("numeroIdentificacion",
                persona.getNumeroIdentificacion());
        }
        if (persona.getPrimerNombre() != null &&
            !persona.getPrimerNombre().isEmpty()) {
            q.setParameter("primerNombre", persona.getPrimerNombre().toUpperCase());
        }
        if (persona.getSegundoNombre() != null &&
            !persona.getSegundoNombre().isEmpty()) {
            q.setParameter("segundoNombre", persona.getSegundoNombre().toUpperCase());
        }
        if (persona.getPrimerApellido() != null &&
            !persona.getPrimerApellido().isEmpty()) {
            q.setParameter("primerApellido", persona.getPrimerApellido().toUpperCase() + "%");
        }
        if (persona.getSegundoApellido() != null &&
            !persona.getSegundoApellido().isEmpty()) {
            q.setParameter("segundoApellido", persona.getSegundoApellido().toUpperCase());
        }
        if (persona.getRazonSocial() != null &&
            !persona.getRazonSocial().isEmpty()) {
            q.setParameter("razonSocial", persona.getRazonSocial().toUpperCase());
        }
        if (persona.getDigitoVerificacion() != null &&
            !persona.getDigitoVerificacion().isEmpty()) {
            q.setParameter("digitoVerificacion",
                persona.getDigitoVerificacion());
        }

        try {
            answer = (List<Persona>) q.getResultList();

        } catch (NoResultException e) {
            return answer;
        } catch (IndexOutOfBoundsException ne) {
            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see
     * IPersonaDAO#getPersonabyTipoIdentificacionNumeroIdentificacion(String)
     */
    @Override
    public Persona getPersonabyTipoIdentificacionNumeroIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion) {
        Persona answer = null;
        Query query;
        String queryToExecute;

        queryToExecute = "SELECT p FROM Persona p" +
            " WHERE p.numeroIdentificacion = :numeroIdentificacion" +
            " AND p.tipoIdentificacion = :tipoIdentificacion";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("numeroIdentificacion", numeroIdentificacion);
        query.setParameter("tipoIdentificacion", tipoIdentificacion);

        try {
            answer = (Persona) query.getSingleResult();

        } catch (NoResultException e) {
            return answer;
        } catch (IndexOutOfBoundsException ne) {
            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

}
