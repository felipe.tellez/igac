package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoPredioMaquinaria
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IAvaluoPredioMaquinariaDAO extends IGenericJpaDAO<AvaluoPredioMaquinaria, Long> {

    /**
     * Hace la consulta por el id del Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioMaquinaria> consultarPorAvaluo(long avaluoId);

//end of interface
    /**
     * Método para determinar si existen maquinarias asociadas a un avalúo o a los predios del
     * avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public Boolean calcularMaquinariasPreviasAvaluo(Long idAvaluo);

    /**
     * Método para eliminar la maquinaria asociada a un avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public boolean eliminarMaquinariaPreviasAvaluo(Long idAvaluo);

}
