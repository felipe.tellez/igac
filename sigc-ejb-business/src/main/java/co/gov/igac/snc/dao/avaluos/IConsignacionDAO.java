package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Consignacion;

/**
 *
 * Interfaz para los métodos de bd de la tabla Consignacion
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IConsignacionDAO extends
    IGenericJpaDAO<Consignacion, Long> {

}
