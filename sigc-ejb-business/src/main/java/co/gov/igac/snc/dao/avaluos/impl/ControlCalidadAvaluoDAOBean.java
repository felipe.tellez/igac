package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadAvaluoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IControlCalidadAvaluoDAO
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class ControlCalidadAvaluoDAOBean extends GenericDAOWithJPA<ControlCalidadAvaluo, Long>
    implements
    IControlCalidadAvaluoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControlCalidadAvaluo.class);

    /**
     * @see IControlCalidadAvaluoDAO#obtenerPorIdConAtributos(Long)
     * @author felipe.cadena
     */
    @Override
    public ControlCalidadAvaluo obtenerPorIdConAtributos(
        Long idControlCalidad) {

        ControlCalidadAvaluo controlCalidad = null;

        if (idControlCalidad != null) {

            controlCalidad = this.findById(idControlCalidad);

            if (controlCalidad != null) {

                Hibernate.initialize(controlCalidad.getProfesionalAvaluos());
                Hibernate.initialize(controlCalidad.getControlCalidadAvaluoRevs());
                if (controlCalidad.getControlCalidadAvaluoRevs() != null) {
                    for (ControlCalidadAvaluoRev ccr : controlCalidad.getControlCalidadAvaluoRevs()) {
                        Hibernate.initialize(ccr.getControlCalidadEspecificacions());
                        Hibernate.initialize(ccr.getSoporteDocumento());

                    }
                }

            }
        }

        return controlCalidad;
    }

    /**
     * @see IControlCalidadAvaluoDAO#consultarPorAvaluoAsignacionActividad(Long)
     * @author felipe.cadena
     */
    @Override
    public ControlCalidadAvaluo consultarPorAvaluoAsignacionActividad(Long idAvaluo,
        Long asignacionId, String actividad) {

        ControlCalidadAvaluo controlCalidad = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT cca " +
                " FROM ControlCalidadAvaluo cca " +
                " WHERE cca.avaluoId = :idAvaluo " +
                " AND cca.actividad = :actividad " +
                " AND cca.asignacion.id = :asignacionId ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);
            query.setParameter("actividad", actividad);
            query.setParameter("asignacionId", asignacionId);

            controlCalidad = (ControlCalidadAvaluo) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return controlCalidad;
    }

    /**
     * @see IControlCalidadAvaluoDAO#consultarTodosPorAvaluoId(Long)
     * @author felipe.cadena
     */
    @Override
    public List<ControlCalidadAvaluo> consultarTodosPorAvaluoId(Long idAvaluo, Long idAsignacion) {

        List<ControlCalidadAvaluo> resultado = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT cca " +
                " FROM ControlCalidadAvaluo cca " +
                " WHERE cca.avaluoId = :idAvaluo " +
                " AND cca.asignacion.id = :idAsignacion ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);
            query.setParameter("idAsignacion", idAsignacion);
            resultado = query.getResultList();
            for (ControlCalidadAvaluo cc : resultado) {
                if (cc != null) {
                    Hibernate.initialize(cc.getProfesionalAvaluos());
                    Hibernate.initialize(cc.getControlCalidadAvaluoRevs());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

}
