package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IConsignacionDAO;
import co.gov.igac.snc.dao.avaluos.IContratoInteradministrativoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Consignacion;

/**
 * @see IContratoInteradministrativoDAO
 * @author felipe.cadena
 */
@Stateless
public class ConsignacionDAOBean extends GenericDAOWithJPA<Consignacion, Long> implements
    IConsignacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(Consignacion.class);

}
