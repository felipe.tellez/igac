/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.IPredioZonaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author felipe.cadena
 */
@Stateless
public class PredioZonaDAOBean extends GenericDAOWithJPA<PredioZona, Long>
    implements IPredioZonaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioZonaDAOBean.class);

    /**
     *
     * @author felipe.cadena
     * @see IPPredioZonaDAO#buscarZonasPorPPredioId
     */
    @Implement
    @Override
    public List<PredioZona> buscarPorPPredioId(Long idPredio) {

        List<PredioZona> answer = null;
        String queryString;
        Query query;

        // Se deben mostrar las zonas de la última vigencia
        queryString = "SELECT pz FROM PredioZona pz " +
            "LEFT JOIN FETCH pz.predio p " +
            "WHERE p.id = :idPredio " +
            "AND pz.vigencia " +
            "IN( SELECT MAX(pz.vigencia) FROM pz WHERE pz.predio.id = :idPredio)";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPredio", idPredio);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PredioZonaDAOBean#buscarPorPPredioId: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioZonaDAO#obtenerZonasPorIdPredio
     */
    @Override
    public List<PredioZona> obtenerZonasPorIdPredio(Long idPredio) {

        List<PredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT pz FROM PredioZona pz " +
            "LEFT JOIN FETCH pz.predio p " +
            "WHERE p.id = :idPredio order by pz.vigencia desc,pz.area desc ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPredio", idPredio);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PredioZonaDAOBean#obtenerZonasPorIdPredio: " +
                ex.getMessage());
        }
        return answer;
    }
    
    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioZonaDAO#obtenerZonasPorIds
     */
    @Override
    public List<PredioZona> obtenerZonasPorIds(List<Long> id) {

        List<PredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT pz FROM PredioZona pz " +
            "WHERE pz.id IN (:id) ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("id", id);
            answer = (List<PredioZona>)query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PredioZonaDAOBean#obtenerZonasPorIds: " +
                ex.getMessage());
        }
        return answer;
    }

}
