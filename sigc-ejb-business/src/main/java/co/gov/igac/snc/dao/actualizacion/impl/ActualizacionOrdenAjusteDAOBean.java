package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionOrdenAjusteDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionOrdenAjuste;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IActualizacionOrdenAjusteDAO
 * @author andres.eslava
 *
 */
@Stateless
public class ActualizacionOrdenAjusteDAOBean extends GenericDAOWithJPA<ActualizacionOrdenAjuste, Long>
    implements IActualizacionOrdenAjusteDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionOrdenAjusteDAOBean.class);

    /**
     * @see IActualizacionOrdenAjusteDAO#getOrdenesAjustePorResponsableSIG(RecursoHumano)
     * @author andres.eslava
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ActualizacionOrdenAjuste> getOrdenesAjustePorResponsableSIG(
        RecursoHumano responsableSig) {

        LOGGER.debug("ActualizacionOrdenAjusteDAOBean#getOrdenesAjustePorResponsableSIG...INICIA");

        List<ActualizacionOrdenAjuste> listaOrdenesAjuste = null;

        String querySQL =
            "select aoa from ActualizacionOrdenAjuste aoa join aoa.recursoHumano rh where rh.id=:rhId";
        Query query = this.getEntityManager().createQuery(querySQL);
        query.setParameter("rhId", responsableSig.getId());

        try {
            listaOrdenesAjuste = (List<ActualizacionOrdenAjuste>) query.getResultList();
            for (ActualizacionOrdenAjuste unaOrdenAjuste : listaOrdenesAjuste) {
                Hibernate.initialize(unaOrdenAjuste.getAjusteManzanaAreas());
            }

            return listaOrdenesAjuste;
        } catch (Exception e) {
            LOGGER.debug(
                " error buscando ordenes de ajuste por Responsable Sig en: ActualizacionOrdenAjusteDAOBean#getOrdenesAjustePorResponsableSIG");
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        } finally {
            LOGGER.debug(
                "ActualizacionOrdenAjusteDAOBean#getOrdenesAjustePorResponsableSIG...FINALIZA");
        }
    }
}
