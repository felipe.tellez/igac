/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos;

import co.gov.igac.snc.comun.colecciones.Par;
import java.text.MessageFormat;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author michael.pena
 */
public class FiltroGenerador {
    
    public static final Logger LOGGER = LoggerFactory.getLogger(FiltroGenerador.class);

    /*
     private String SQL_TAREAS_FILTRO = "SELECT est.estado,bact.urlcasodeuso,bact.nombreactividad,bact.macroproceso,bact.tipo,bact.tipoprocesamiento,bact.proceso,bact.subprocesoactividad,\n"
     + "bact.actividad,bact.trancisiones,bact.macroprocesoproceso,bjt.id AS id_flujo_tramite, bjt.fecha_inicio,bjt.fecha_final,pid.tramite AS idObjetoNegocio,sncts.numero,\n"
     + "tra.numero_radicacion,pre.numero_predial,bjt.usuario_responsable, tpt.tramite, bact.id AS id_actividad, tra.fecha_radicacion FROM procesos.bpm_flujo_tramite bjt \n"
     + "INNER JOIN procesos.bpm_actividad bact ON bact.id = bjt.actividad_id \n"
     + "INNER JOIN procesos.bpm_tipo_tramite tpt ON tpt.id = bjt.tipo_tramite_id \n"
     + "INNER JOIN procesos.bpm_estado est ON est.id = bjt.id_estado \n"
     + "INNER JOIN procesos.bpm_proceso_instancia pid ON pid.id = bjt.proceso_instancia_id \n"
     + "INNER JOIN tramite tra ON tra.id = pid.tramite \n"
     + "LEFT JOIN snc_conservacion.predio pre ON pre.id = tra.predio_id \n"
     + "INNER JOIN snc_tramite.solicitud sncts ON sncts.id = tra.solicitud_id \n";
     //+ "LEFT JOIN snc_tramite.solicitud sncts ON sncts.proceso_instancia_id = pid.ID_INSTANCIA_SNC";
    
    
     private String SQL_TAREAS_FILTRO ="SELECT * FROM PROCESOS.V_PROCESO PROCESO ";*/
    private String SQL_TAREAS_FILTRO = "select * from (SELECT\n"
            + "        est.estado,\n"
            + "        bact.urlcasodeuso,\n"
            + "        bact.nombreactividad,\n"
            + "        bact.macroproceso,\n"
            + "        bact.tipo,\n"
            + "        bact.tipoprocesamiento,\n"
            + "        bact.proceso,\n"
            + "        bact.subprocesoactividad,\n"
            + "        bact.actividad,\n"
            + "        bact.trancisiones,\n"
            + "        bact.macroprocesoproceso,\n"
            + "        bjt.id             AS id_flujo_tramite,\n"
            + "        bjt.fecha_inicio,\n"
            + "        bjt.fecha_final,\n"
            + "        pid.tramite        AS idobjetonegocio,\n"
            + "        sncts.numero,\n"
            + "        tra.numero_radicacion,\n"
            + "        pre.numero_predial,\n"
            + "        bjt.usuario_responsable,\n"
            + "        tpt.tramite,\n"
            + "        bact.id            AS id_actividad,\n"
            + "        tra.fecha_radicacion,\n"
            + "        'TRAMITE' tipo_proceso,\n"
            + "        bjt.territorial,\n"
            + "        bjt.cod_territorial,\n"
            + "        bjt.uoc,\n"
            + "        bjt.cod_uoc,\n"
            + "        bact.description   AS actividad_descripcion,\n"
            + "        pid.id_instancia_snc,\n"
            + "        est.id             id_estado,\n"
            + "        tra.orden_ejecucion\n"
            + "    FROM\n"
            + "        procesos.bpm_flujo_tramite bjt\n"
            + "        INNER JOIN procesos.bpm_actividad bact ON bact.id = bjt.actividad_id\n"
            + "        INNER JOIN procesos.bpm_tipo_tramite tpt ON tpt.id = bjt.tipo_tramite_id\n"
            + "        INNER JOIN procesos.bpm_estado est ON est.id = bjt.id_estado\n"
            + "        INNER JOIN procesos.bpm_proceso_instancia pid ON pid.id = TO_CHAR(bjt.proceso_instancia_id)\n"
            + "        INNER JOIN tramite tra ON tra.id = pid.tramite\n"
            + "                                  AND bjt.tipo_tramite_id > 0\n"
            + "        LEFT JOIN snc_conservacion.predio pre ON pre.id = tra.predio_id\n"
            + "        INNER JOIN snc_tramite.solicitud sncts ON sncts.id = tra.solicitud_id    \n"
            + "    UNION ALL\n"
            + "    SELECT\n"
            + "        est.estado,\n"
            + "        bact.urlcasodeuso,\n"
            + "        bact.nombreactividad,\n"
            + "        bact.macroproceso,\n"
            + "        bact.tipo,\n"
            + "        bact.tipoprocesamiento,\n"
            + "        bact.proceso,\n"
            + "        bact.subprocesoactividad,\n"
            + "        bact.actividad,\n"
            + "        bact.trancisiones,\n"
            + "        bact.macroprocesoproceso,\n"
            + "        bjt.id        AS id_flujo_tramite,\n"
            + "        bjt.fecha_inicio,\n"
            + "        bjt.fecha_final,\n"
            + "        pid.tramite   AS idobjetonegocio,\n"
            + "        sncts.numero,\n"
            + "        sncts.numero,\n"
            + "        sncts.numero,\n"
            + "        bjt.usuario_responsable,\n"
            + "        '-1',\n"
            + "        bact.id       AS id_actividad,\n"
            + "        pid.fecha_inicio,\n"
            + "        'PRODUCTO_CATASTRAL',\n"
            + "        bjt.territorial,\n"
            + "        bjt.cod_territorial,\n"
            + "        bjt.uoc,\n"
            + "        bjt.cod_uoc,\n"
            + "        bact.description,\n"
            + "        pid.id_instancia_snc,\n"
            + "        est.id        idestado,\n"
            + "        NULL\n"
            + "    FROM\n"
            + "        procesos.bpm_flujo_tramite bjt\n"
            + "        INNER JOIN procesos.bpm_actividad bact ON bact.id = bjt.actividad_id\n"
            + "        INNER JOIN procesos.bpm_estado est ON est.id = bjt.id_estado\n"
            + "        INNER JOIN procesos.bpm_proceso_instancia pid ON pid.id = bjt.proceso_instancia_id\n"
            + "                                                         AND bjt.tipo_tramite_id = - 1\n"
            + "        INNER JOIN snc_tramite.solicitud sncts ON sncts.id = pid.tramite) PROCESO ";

    private String SQL_FILTRO_TERRITORIAL = " and PROCESO.TERRITORIAL =''{0}'' and PROCESO.UOC is null";
    private String SQl_FILTROS_UOC = " and PROCESO.UOC =''{0}''";
    private String SQL_FILTRO_OWNER = " and PROCESO.USUARIO_RESPONSABLE = ''{0}''";
    private String SQL_FILTRO_USUARIO_POTENCIAL = " and (PROCESO.USUARIO_RESPONSABLE = ''{0}'' or PROCESO.USUARIO_RESPONSABLE is null)";
    private String SQL_FILTRO_USUARIOS_POTENCIAL = " and PROCESO.USUARIO_RESPONSABLE = ''{0}''";
    private String SQL_FILTROS_ESTADOS = " and PROCESO.ID_ESTADO in (''{0}'')";
    private String SQL_FILTROS_ID_PROCESO = " and PROCESO.ID_INSTANCIA_SNC = ''{0}''";
    private String SQL_FILTROS_IDS_PROCESOS = " and PROCESO.ID_INSTANCIA_SNC in (''{0}'')";

    private String SQL_FILTROS_NUMERO_PREDIAL = " and PROCESO.NUMERO_PREDIAL =''{0}''";
    private String SQL_FILTROS_NUMERO_RADICACION = " and PROCESO.NUMERO_RADICACION =''{0}''";
    private String SQL_FILTROS_NUMERO_SOLICITUD = " and PROCESO.NUMERO =''{0}''";
    private String SQL_FILTROS_TRAMITE_ID = " and PROCESO.IDOBJETONEGOCIO in (''{0}'') ";

    private String TAREAS_VIGENTES = "0,2,8";

    private String SQL_NOMBRE_TAREA = " and PROCESO.NOMBREACTIVIDAD in (''{0}'')";

    private String SQL_TERRITORIAL = " and PROCESO.TERRITORIAL  = ''{0}''";

    private String SQL_TERRITORIAL_UOC = " and PROCESO.UOC  = ''{0}''";

    private String SQL_FILTRO_IDENTIFICADOR_PROCESOS = " and PROCESO.ID_INSTANCIA_SNC in (''{0}'')";

    private String SQL_FILTROS_ULTIMA_ACTIVIVAD = " and PROCESO.IDOBJETONEGOCIO in (''{0}'') and  PROCESO.ID_ESTADO not in (5,6,7,12) ";

    private String SQL_FILTROS_ULTIMA_ACTIVIVAD_NULL = " and PROCESO.IDOBJETONEGOCIO is not null";

    /**
     * Corresponde a tareas que ya fueron realizadas en el proceso de negocio.
     */
    private String TAREAS_REALIZADAS = "5";

    /**
     * Corresponde a tareas de procesos que terminaron antes de que dichas
     * actividades fueran realizadas. Un ejemplo de este caso es cuando un
     * trámite no procede.
     */
    private String TAREAS_TERMINADAS = "7";

    /**
     * Corresponden a tareas que pueden ser realizadas por alguno de los
     * usuarios de un rol. Por ejemplo, el rol de control de digitalización, el
     * cual tiene varios usuarios por territorial y cualquiera de ellos puede
     * realizar la tarea.
     *
     */
    private String TAREAS_LISTAS = "2";

    /**
     * Corresponden a tareas que pueden ser ejecutadas por un usuario en
     * particular (generalmente cuando un rol tiene un sólo usuario, por
     * ejemplo: Un sólo Responsable de Conservación en una territorial).
     *
     */
    private String TAREAS_RECLAMADAS = "8";

    /**
     * Corresponde a todos los estados soportados por las tareas de las
     * instancias de procesos de negocio del Sistema Nacional Catastral.
     */
    private String TAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS = "0,2,8,5,7";

    /**
     * Corresponde a todos los estados soportados por las actividades de los
     * procesos de negocio del Sistema Nacional Catastral.
     */
    private String ACTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS = "0,2,8,5,7";

    private String ACTIVIDADES_SUSPENDIDAS = "1";

    private String ACTIVIDADES_VIGENTES = "0";

    private String ACTIVIDADES_VIGENTES_Y_SUSPENDIDAS = "0,1";

    /**
     * Metodo que desencadena la lista de actividades, indexando los filtros y
     * realizando verificaciones de la petición. Retornando una cadena de texto
     * con sintaxis SQL de filtros.
     *
     * @param filtros
     * @param {List} filtros [Recibe una lista tipo FiltroActividades de
     * filtros]
     * @return {String} queryFiltros [Retorna una cadena de texto con sintaxis
     * SQL]
     */
    public final String QueryFiltroTareas(List<Par> filtros) {

        String queryFiltros = "";
        String queryFiltrosFinal = "";
        try {
            for (int i = 0; i < filtros.size(); i++) {

                if (filtros.get(i).getAtributo().equals("usuarioPotencial")) {
                    queryFiltros += MessageFormat.format(SQL_FILTRO_USUARIO_POTENCIAL, filtros.get(i).getValor());
                }
                if (filtros.get(i).getAtributo().equals("usuarios")) {
                    queryFiltros += MessageFormat.format(SQL_FILTRO_USUARIOS_POTENCIAL, filtros.get(i).getValor());
                }             
                if (filtros.get(i).getAtributo().equals("estados")) {
                    if (filtros.get(i).getValor().contains("8"))
                    {
                     queryFiltros += MessageFormat.format(SQL_FILTROS_ESTADOS, "2','8");
                    }else{
                    String filtroFinal = filtros.get(i).getValor();
                    filtroFinal = filtroFinal.replace(",", "','");
                    queryFiltros += MessageFormat.format(SQL_FILTROS_ESTADOS, filtroFinal);
                    }                    
                }
                if (filtros.get(i).getAtributo().equals("numeroSolicitud")) {
                    queryFiltros += MessageFormat.format(SQL_FILTROS_NUMERO_SOLICITUD,
                            filtros.get(i).getValor());
                }
                if (filtros.get(i).getAtributo().equals("ID_PRO")
                        || filtros.get(i).getAtributo().equals("ID_CONSERVACION")) {
                    queryFiltros += MessageFormat.format(SQL_FILTROS_TRAMITE_ID,
                            filtros.get(i).getValor());
                }
                if (filtros.get(i).getAtributo().equals("ultimaActividad")) {
                    if (filtros.get(i).getValor() != null && !filtros.get(i).getValor().isEmpty()) {
                        queryFiltros += MessageFormat.format(SQL_FILTROS_ULTIMA_ACTIVIVAD,
                                filtros.get(i).getValor().replace(",", "','"));
                    } else {
                        queryFiltros += "";
                    }
                }
                if (filtros.get(i).getAtributo().equals("nombreActividad")) {
                    String filtroFinal = filtros.get(i).getValor();
                    filtroFinal = filtroFinal.replace(",", "','");
                    queryFiltros += MessageFormat.format(SQL_NOMBRE_TAREA, filtroFinal);
                }
                if (filtros.get(i).getAtributo().equals("uoc")) {
                    queryFiltros += MessageFormat.format(SQl_FILTROS_UOC, filtros.get(i).getValor());
                }
                if (filtros.get(i).getAtributo().equals("propitarioActividad")) {
                    queryFiltros += MessageFormat.format(SQL_FILTRO_OWNER, filtros.get(i).getValor());
                }
                if (filtros.get(i).getAtributo().equals("identificadoresProcesos") || filtros.get(i).getAtributo().equals("identificadorProceso")) {
                    //Integer.parseInt(filtros.get(i).getValor());
                    String filtroFinal = filtros.get(i).getValor();
                    filtroFinal = filtroFinal.replace(",", "','");
                    queryFiltros += MessageFormat.format(SQL_FILTRO_IDENTIFICADOR_PROCESOS, filtroFinal);

                }
                if (filtros.get(i).getAtributo().equals("territorial")) {//Sólo cuando viene el filtro territorial
                    if (filtros.get(i).getValor().contains("UOC")) {
                        queryFiltros += MessageFormat.format(SQL_TERRITORIAL_UOC, filtros.get(i).getValor());
                    } else {
                        queryFiltros += MessageFormat.format(SQL_TERRITORIAL, filtros.get(i).getValor());
                    }
                }

            }

        } catch (Exception e) {

            LOGGER.error("ERROR GENERANDO QUERYFILTRO DINAMICO " + e);

        }
        if (queryFiltros.length() > 0) {
            queryFiltrosFinal = SQL_TAREAS_FILTRO + queryFiltros.replaceFirst("and", "where");
        } else {
            return "";
        }

        return queryFiltrosFinal;
    }

    public String getSQL_TAREAS_FILTRO() {
        return SQL_TAREAS_FILTRO;
    }

    public void setSQL_TAREAS_FILTRO(String sQL_TAREAS_FILTRO) {
        SQL_TAREAS_FILTRO = sQL_TAREAS_FILTRO;
    }

    public String getSQL_FILTRO_TERRITORIAL() {
        return SQL_FILTRO_TERRITORIAL;
    }

    public void setSQL_FILTRO_TERRITORIAL(String sQL_FILTRO_TERRITORIAL) {
        SQL_FILTRO_TERRITORIAL = sQL_FILTRO_TERRITORIAL;
    }

    public String getSQL_FILTRO_OWNER() {
        return SQL_FILTRO_OWNER;
    }

    public void setSQL_FILTRO_OWNER(String sQL_FILTRO_OWNER) {
        SQL_FILTRO_OWNER = sQL_FILTRO_OWNER;
    }

    public String getSQL_FILTRO_USUARIO_POTENCIAL() {
        return SQL_FILTRO_USUARIO_POTENCIAL;
    }

    public void setSQL_FILTRO_USUARIO_POTENCIAL(String sQL_FILTRO_USUARIO_POTENCIAL) {
        SQL_FILTRO_USUARIO_POTENCIAL = sQL_FILTRO_USUARIO_POTENCIAL;
    }

    public String getSQL_FILTROS_ESTADOS() {
        return SQL_FILTROS_ESTADOS;
    }

    public void setSQL_FILTROS_ESTADOS(String sQL_FILTROS_ESTADOS) {
        SQL_FILTROS_ESTADOS = sQL_FILTROS_ESTADOS;
    }

    public String getSQL_FILTROS_ID_PROCESO() {
        return SQL_FILTROS_ID_PROCESO;
    }

    public void setSQL_FILTROS_ID_PROCESO(String sQL_FILTROS_ID_PROCESO) {
        SQL_FILTROS_ID_PROCESO = sQL_FILTROS_ID_PROCESO;
    }

    public String getSQL_FILTROS_IDS_PROCESOS() {
        return SQL_FILTROS_IDS_PROCESOS;
    }

    public void setSQL_FILTROS_IDS_PROCESOS(String sQL_FILTROS_IDS_PROCESOS) {
        SQL_FILTROS_IDS_PROCESOS = sQL_FILTROS_IDS_PROCESOS;
    }

    public String getSQL_FILTROS_NUMERO_PREDIAL() {
        return SQL_FILTROS_NUMERO_PREDIAL;
    }

    public void setSQL_FILTROS_NUMERO_PREDIAL(String sQL_FILTROS_NUMERO_PREDIAL) {
        SQL_FILTROS_NUMERO_PREDIAL = sQL_FILTROS_NUMERO_PREDIAL;
    }

    public String getSQL_FILTROS_NUMERO_RADICACION() {
        return SQL_FILTROS_NUMERO_RADICACION;
    }

    public void setSQL_FILTROS_NUMERO_RADICACION(String sQL_FILTROS_NUMERO_RADICACION) {
        SQL_FILTROS_NUMERO_RADICACION = sQL_FILTROS_NUMERO_RADICACION;
    }

    public String getSQL_FILTROS_NUMERO_SOLICITUD() {
        return SQL_FILTROS_NUMERO_SOLICITUD;
    }

    public void setSQL_FILTROS_NUMERO_SOLICITUD(String sQL_FILTROS_NUMERO_SOLICITUD) {
        SQL_FILTROS_NUMERO_SOLICITUD = sQL_FILTROS_NUMERO_SOLICITUD;
    }

    public String getTAREAS_VIGENTES() {
        return TAREAS_VIGENTES;
    }

    public void setTAREAS_VIGENTES(String tAREAS_VIGENTES) {
        TAREAS_VIGENTES = tAREAS_VIGENTES;
    }

    public String getTAREAS_REALIZADAS() {
        return TAREAS_REALIZADAS;
    }

    public void setTAREAS_REALIZADAS(String tAREAS_REALIZADAS) {
        TAREAS_REALIZADAS = tAREAS_REALIZADAS;
    }

    public String getTAREAS_TERMINADAS() {
        return TAREAS_TERMINADAS;
    }

    public void setTAREAS_TERMINADAS(String tAREAS_TERMINADAS) {
        TAREAS_TERMINADAS = tAREAS_TERMINADAS;
    }

    public String getTAREAS_LISTAS() {
        return TAREAS_LISTAS;
    }

    public void setTAREAS_LISTAS(String tAREAS_LISTAS) {
        TAREAS_LISTAS = tAREAS_LISTAS;
    }

    public String getTAREAS_RECLAMADAS() {
        return TAREAS_RECLAMADAS;
    }

    public void setTAREAS_RECLAMADAS(String tAREAS_RECLAMADAS) {
        TAREAS_RECLAMADAS = tAREAS_RECLAMADAS;
    }

    public String getTAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS() {
        return TAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS;
    }

    public void setTAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS(
            String tAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS) {
        TAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS = tAREAS_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS;
    }

    public String getACTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS() {
        return ACTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS;
    }

    public String getSQL_FILTRO_IDENTIFICADOR_PROCESOS() {
        return SQL_FILTRO_IDENTIFICADOR_PROCESOS;
    }

    public void setSQL_FILTRO_IDENTIFICADOR_PROCESOS(String sQL_FILTRO_IDENTIFICADOR_PROCESOS) {
        SQL_FILTRO_IDENTIFICADOR_PROCESOS = sQL_FILTRO_IDENTIFICADOR_PROCESOS;
    }

    public void setACTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS(
            String aCTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS) {
        ACTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS = aCTIVIDADES_VIGENTES_REALIZADAS_FINALIZADAS_TERMINADAS;
    }

    public String getACTIVIDADES_SUSPENDIDAS() {
        return ACTIVIDADES_SUSPENDIDAS;
    }

    public void setACTIVIDADES_SUSPENDIDAS(String aCTIVIDADES_SUSPENDIDAS) {
        ACTIVIDADES_SUSPENDIDAS = aCTIVIDADES_SUSPENDIDAS;
    }

    public String getACTIVIDADES_VIGENTES() {
        return ACTIVIDADES_VIGENTES;
    }

    public void setACTIVIDADES_VIGENTES(String aCTIVIDADES_VIGENTES) {
        ACTIVIDADES_VIGENTES = aCTIVIDADES_VIGENTES;
    }

    public String getACTIVIDADES_VIGENTES_Y_SUSPENDIDAS() {
        return ACTIVIDADES_VIGENTES_Y_SUSPENDIDAS;
    }

    public void setACTIVIDADES_VIGENTES_Y_SUSPENDIDAS(String aCTIVIDADES_VIGENTES_Y_SUSPENDIDAS) {
        ACTIVIDADES_VIGENTES_Y_SUSPENDIDAS = aCTIVIDADES_VIGENTES_Y_SUSPENDIDAS;
    }
    
}
