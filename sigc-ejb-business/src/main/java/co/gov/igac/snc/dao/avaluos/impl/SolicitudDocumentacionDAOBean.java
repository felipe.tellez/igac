package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.ISolicitudDocumentacionDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisitoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;

/**
 * @see ISolicitudDocumentacion
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class SolicitudDocumentacionDAOBean extends GenericDAOWithJPA<SolicitudDocumentacion, Long>
    implements ISolicitudDocumentacionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitudDocumentacionDAOBean.class);

    /**
     *
     * @author felipe.cadena
     * @see ISolicitudDocumentacionDAO#obternerSolicitudesDocumentacionPorSolicitud(java.lang.Long)
     *
     */
    @Override
    public List<SolicitudDocumentacion> obternerSolicitudesDocumentacionPorSolicitud(
        Long idSolicitud, Boolean aportado) {

        List<SolicitudDocumentacion> result = new ArrayList<SolicitudDocumentacion>();
        String queryString;
        Query query;
        String aportadoParam;
        String querySoporteDocumento;

        //Se crea el string para el query
        queryString = "SELECT sd FROM SolicitudDocumentacion sd";

        //Se crea la sentencia para cargar los documentos de soporte
        querySoporteDocumento = " LEFT JOIN FETCH sd.soporteDocumentoId d";

        // Si es el documento ya fue aportado, se carga el soporte del documento
        if (aportado) {
            aportadoParam = ESiNo.SI.getCodigo();
            queryString = queryString + querySoporteDocumento;
        } else {
            aportadoParam = ESiNo.NO.getCodigo();
        }
        queryString = queryString +
            " LEFT JOIN FETCH sd.tipoDocumento td" +
            " LEFT JOIN FETCH sd.solicitud s" +
            " WHERE s.id =:idSolicitud AND sd.aportado =:aportadoStr" +
            " ORDER BY td.id";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("aportadoStr", aportadoParam);
            result = query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Solicitud Documentación", ESeveridadExcepcionSNC.ERROR, e.
                getMessage(), null);
        }

        return result;
    }

    @Override
    public List<SolicitudDocumentacion> obtenerListaDocsBasicosDeSolicitudAvaluo() {
        LOGGER.
            debug("Inicio SolicitudDocumentacionDAOBean#obtenerListaDocsBasicosDeSolicitudAvaluo");

        List<SolicitudDocumentacion> result = new ArrayList<SolicitudDocumentacion>();
        List<TramiteRequisitoDocumento> listaTramiteRequisitoDoc =
            new ArrayList<TramiteRequisitoDocumento>();
        String queryString;
        Query query;

        queryString = "select trd" +
            " from TramiteRequisitoDocumento trd, TramiteRequisito tr" +
            " left join fetch trd.tipoDocumento td" +
            " where" +
            " trd.tramiteRequisito.id = tr.id" +
            " and tr.tipoTramite =:tipoTramite";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tipoTramite",
                ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo());

            listaTramiteRequisitoDoc = query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Solicitud Documentación",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }

        for (TramiteRequisitoDocumento trd : listaTramiteRequisitoDoc) {
            SolicitudDocumentacion solDoc = new SolicitudDocumentacion();
            solDoc.setTipoDocumento(trd.getTipoDocumento());
            result.add(solDoc);
        }

        LOGGER.debug("Fin SolicitudDocumentacionDAOBean#obtenerListaDocsBasicosDeSolicitudAvaluo");

        return result;

    }

    /**
     *
     * @see ISolicitudDocumentacionDAO#obtenerListaDocsFaltantesDeSolicitudAvaluo()
     * @author rodrigo.hernandez
     *
     */
    @Override
    @Implement
    public List<SolicitudDocumentacion> obtenerListaDocsFaltantesDeSolicitudAvaluo() {
        LOGGER.
            debug("Inicio SolicitudDocumentacionDAOBean#obtenerListaDocsBasicosDeSolicitudAvaluo");

        List<SolicitudDocumentacion> result = new ArrayList<SolicitudDocumentacion>();
        List<TramiteRequisitoDocumento> listaTramiteRequisitoDoc =
            new ArrayList<TramiteRequisitoDocumento>();
        String queryString;
        Query query;

        queryString = "select trd" +
            " from TramiteRequisitoDocumento trd, TramiteRequisito tr" +
            " left join fetch trd.tipoDocumento td" +
            " where" +
            " trd.tramiteRequisito.id = tr.id" +
            " and tr.tipoTramite =:tipoTramite";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tipoTramite",
                ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo());

            listaTramiteRequisitoDoc = query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("Solicitud Documentación",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }

        for (TramiteRequisitoDocumento trd : listaTramiteRequisitoDoc) {
            SolicitudDocumentacion solDoc = new SolicitudDocumentacion();
            solDoc.setTipoDocumento(trd.getTipoDocumento());
            result.add(solDoc);
        }

        LOGGER.debug("Fin SolicitudDocumentacionDAOBean#obtenerListaDocsBasicosDeSolicitudAvaluo");

        return result;

    }

    /**
     *
     * @see ISolicitudDocumentacionDAO#obternerSolicitudesDocumentacionPorSolicitud(Long, Boolean)
     * @author rodrigo.hernandez
     *
     */
    @Override
    @Implement
    public List<SolicitudDocumentacion> obtenerListaSolDocsPorSolicitudYTipoDocumento(
        Long solicitudId, Long tipoDocumentoId) {

        LOGGER.debug(
            "Inicio SolicitudDocumentacionDAOBean#obtenerListaSolDocsPorSolicitudYTipoDocumento");

        List<SolicitudDocumentacion> result = new ArrayList<SolicitudDocumentacion>();

        String queryString;
        Query query;

        //Se crea el string para el query
        queryString = "SELECT solDoc " +
            " FROM SolicitudDocumentacion solDoc" +
            " WHERE solDoc.solicitud.id =:idSolicitud " +
            " AND solDoc.tipoDocumento.id =:idTipoDoc" +
            " AND solDoc.aportado =:aportado";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", solicitudId);
            query.setParameter("idTipoDoc", tipoDocumentoId);
            query.setParameter("aportado", ESiNo.SI.getCodigo());

            result = query.getResultList();

            for (SolicitudDocumentacion solDoc : result) {
                Hibernate.initialize(solDoc.getSoporteDocumentoId());
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC(
                "Error en SolicitudDocumentacionDAOBean#obtenerListaSolDocsPorSolicitudYTipoDocumento",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }

        LOGGER.debug(
            "Fin SolicitudDocumentacionDAOBean#obtenerListaSolDocsPorSolicitudYTipoDocumento");

        return result;
    }

}
