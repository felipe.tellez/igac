package co.gov.igac.snc.dao.avaluos.impl;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IGttOfertaInmobiliariaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.GttOfertaInmobiliaria;

@Stateless
public class GttOfertaInmobiliariaDAOBean extends GenericDAOWithJPA<GttOfertaInmobiliaria, Long>
    implements IGttOfertaInmobiliariaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GttOfertaInmobiliariaDAOBean.class);

    @Override
    public Long generarSecuenciaGttOfertaInmobiliaria() {

        Query query = entityManager.createNativeQuery(
            "SELECT GTT_OFERTA_INMOBILIARIA_SE_SEQ.NextVal FROM DUAL");
        Long secuencia = ((BigDecimal) query.getResultList().get(0)).longValue();

        return secuencia;

    }

}
