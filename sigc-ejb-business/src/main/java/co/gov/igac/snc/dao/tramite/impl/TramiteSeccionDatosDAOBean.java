/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteSeccionDatosDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;

/**
 *
 * @author fredy.wilches
 *
 */
@Stateless
public class TramiteSeccionDatosDAOBean extends GenericDAOWithJPA<TramiteSeccionDatos, Long>
    implements
    ITramiteSeccionDatosDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteSeccionDatosDAOBean.class);

    @Resource
    SessionContext sessionContext;

}
