package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradminisAdicion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoAdicion;

import java.util.Date;
import java.util.List;

/**
 * Interface que contiene metodos de consulta sobre entity ProfesionalContratoAdicion
 *
 * @author felipe.cadena
 */
public interface IProfesionalContratoAdicionDAO extends
    IGenericJpaDAO<ProfesionalContratoAdicion, Long> {

    /**
     * Método para recuperar las adiciones relacionadas a un contrato de un avaluador
     *
     * @author felipe.cadena
     *
     * @param idContrato - Id del contrato seleccionado
     * @return
     */
    public List<ProfesionalContratoAdicion> buscarAdicionesPorIdContrato(Long idContrato);

    /**
     * Obtiene el resultado de la sumatoria de los valores de las adiciones relacionadas a un
     * contrato
     *
     *
     * @author felipe.cadena
     * @param idContrato - id del contrato relacionado
     * @return valor double con el valor total de las adiciones
     */
    public Double obtenerTotalAdicionesPorIdContrato(Long idContrato);

    /**
     * Retorna la mayor entre las fechas de las adiciones de un contrato
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato
     * @return Date
     */
    public Date obtenerFechaFinalPAContratoAdiciones(Long idContrato);
}
