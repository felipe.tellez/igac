package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOfertaServicioComunalDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioComunal;

@Stateless
public class OfertaServicioComunalDAOBean extends GenericDAOWithJPA<OfertaServicioComunal, Long>
    implements IOfertaServicioComunalDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OfertaAreaUsoExclusivoDAOBean.class);
}
