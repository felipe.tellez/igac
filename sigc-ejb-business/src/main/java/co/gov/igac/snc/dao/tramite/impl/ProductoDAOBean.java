package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IProductoDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.tramite.Producto;

/**
 * Implementación de los servicios de persistencia del objeto Producto.
 *
 * @author javier.barajas
 */
@Stateless
public class ProductoDAOBean extends GenericDAOWithJPA<Producto, Long> implements IProductoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProductoDAOBean.class);

    /**
     * @see ProductoDAOBean#obtenerListaProductosPorGrupoId(Long)
     * @author javier.aponte
     */
    @Override
    public List<Producto> obtenerListaProductosPorGrupoId(Long idGrupo) {
        LOGGER.debug("Entra en ProductoDAOBean#obtenerListaProducto");

        List<Producto> answer = null;

        //felipe.cadena::20-12-2016::Se agrega condicion de activos para productos
        Query q = entityManager.createQuery(
            "SELECT pr FROM Producto pr WHERE pr.grupoProducto.id = :idGrupo" +
            " AND pr.activo = 'SI'");
        q.setParameter("idGrupo", idGrupo);
        try {
            answer = (List<Producto>) q.getResultList();

        } catch (NoResultException e) {
            LOGGER.error("ProductoDAOBean#obtenerListaProductosPorGrupoId:" +
                "Error al consultar la lista de productos del SNC");
        }
        return answer;

    }

    /**
     * @see ProductoDAOBean#obtenerProductoPorCodigo()
     * @author javier.aponte
     */
    @Override
    public Producto obtenerProductoPorCodigo(String codigo) {
        LOGGER.debug("Entra en ProductoDAOBean#obtenerListaProducto");

        Producto answer = null;

        Query q = entityManager.createQuery(
            "SELECT pr FROM Producto pr WHERE pr.codigo = :codigoProducto");
        q.setParameter("codigoProducto", codigo);
        try {

            answer = (Producto) q.getSingleResult();

        } catch (NoResultException e) {
            LOGGER.error("ProductoDAOBean#obtenerProductoPorCodigo:" +
                "Error al consultar la lista de productos del SNC");
        }
        return answer;

    }

}
