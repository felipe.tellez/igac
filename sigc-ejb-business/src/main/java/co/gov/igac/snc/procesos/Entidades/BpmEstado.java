/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_ESTADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmEstado.findAll", query = "SELECT b FROM BpmEstado b"),
    @NamedQuery(name = "BpmEstado.findById", query = "SELECT b FROM BpmEstado b WHERE b.id = :id"),
    @NamedQuery(name = "BpmEstado.findByEstado", query = "SELECT b FROM BpmEstado b WHERE b.estado = :estado"),
    @NamedQuery(name = "BpmEstado.findByDescripcion", query = "SELECT b FROM BpmEstado b WHERE b.descripcion = :descripcion")})
public class BpmEstado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ESTADO")
    private String estado;
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public BpmEstado() {
    }

    public BpmEstado(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmEstado)) {
            return false;
        }
        BpmEstado other = (BpmEstado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmEstado[ id=" + id + " ]";
    }
    
}
