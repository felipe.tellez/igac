/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IRegionCapturaOfertaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.FuncionarioRecolector;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class RegionCapturaOfertaDAOBean extends GenericDAOWithJPA<RegionCapturaOferta, Long>
    implements IRegionCapturaOfertaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegionCapturaOfertaDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IRegionCapturaOfertaDAO#buscarRegionesPorIdsYEstados(List,List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<RegionCapturaOferta> buscarRegionesPorIdsYEstados(List<Long> regionesCapturaIds,
        List<String> estados) {

        LOGGER.debug("on RegionCapturaOfertaDAOBean#buscarNoComisionadasPorIds ...");

        List<RegionCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT rco " +
            " FROM RegionCapturaOferta rco " +
            " JOIN FETCH rco.areaCapturaOferta aco" +
            " LEFT JOIN FETCH aco.departamento " +
            " LEFT JOIN FETCH aco.municipio " +
            " LEFT JOIN FETCH aco.zona " +
            " WHERE rco.estado IN :estados " +
            " AND rco.id IN :regionesCapturaIds";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("regionesCapturaIds", regionesCapturaIds);
            query.setParameter("estados", estados);

            answer = (List<RegionCapturaOferta>) query.getResultList();

            for (RegionCapturaOferta regionCapturaOferta : answer) {
                regionCapturaOferta.getDetalleCapturaOfertas().size();
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "RegionCapturaOfertaDAOBean#buscarNoComisionadasPorIdAreaCaptura");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IRegionCapturaOfertaDAO#buscarPorIdComision(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<RegionCapturaOferta> buscarPorIdComision(Long comisionId) {

        LOGGER.debug("on RegionCapturaOfertaDAOBean#buscarPorIdComision ...");

        List<RegionCapturaOferta> answer = null;
        String queryString;
        Query query;

        //D: se hace fetch del AreaCapturaOferta porque se necesita para datos de ésta
        queryString = "SELECT rco " +
            " FROM RegionCapturaOferta rco " +
            " JOIN FETCH rco.areaCapturaOferta aco" +
            " JOIN FETCH aco.departamento " +
            " JOIN FETCH aco.municipio " +
            " WHERE rco.comisionOferta.id = :comisionIdP ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("comisionIdP", comisionId);

            answer = (List<RegionCapturaOferta>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "RegionCapturaOfertaDAOBean#buscarPorIdComision");
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------  
    /**
     * // * @see IRegionCapturaOferta#getInformacionRecolectores(List<String>)
     *
     * // * @author rodrigo.hernandez
	 * */
    @Override
    public List<FuncionarioRecolector> getInformacionRecolectores(
        List<String> listaRecolectores, Long idAreaRecoleccion) {

        LOGGER.debug("on RegionCapturaOfertaDAOBean#getInformacionRecolectores ...");

        StringTokenizer tokenizer = null;

        //Variable para asignar id a un elemento FuncionarioRecolector dentro de la lista
        int idRecolector = -1;

        //Variable para almacenar el id del recolector y su nombre a partir de un String
        List<String> recolectorCampos = null;

        //Variable auxiliar para almacenar datos de un recolector
        FuncionarioRecolector recolector = null;

        //Lista con datos de los recolectores
        List<FuncionarioRecolector> recolectores = new ArrayList<FuncionarioRecolector>();

        //Query para consultar si un recolector tiene asignaciones
        String stringQueryAsignaciones;
        Query queryAsignaciones;

        Integer resultadoConteo;

        for (String recolectorStr : listaRecolectores) {

            recolectorCampos = new ArrayList<String>();
            recolector = new FuncionarioRecolector();

            stringQueryAsignaciones = "" +
                " SELECT COUNT (*)" +
                " FROM RegionCapturaOferta rco" +
                " WHERE rco.asignadoRecolectorId = :recolectorId" +
                " AND rco.estado IS NOT NULL " +
                " AND rco.areaCapturaOferta.id = :idAreaRecoleccion";

            try {

                tokenizer = new StringTokenizer(recolectorStr, "-");
                while (tokenizer.hasMoreElements()) {
                    String token = tokenizer.nextToken();
                    LOGGER.debug(token);
                    recolectorCampos.add(token);
                }

                queryAsignaciones = this.entityManager.createQuery(stringQueryAsignaciones);
                queryAsignaciones.setParameter("recolectorId", recolectorCampos.get(0));
                queryAsignaciones.setParameter("idAreaRecoleccion", idAreaRecoleccion);

                resultadoConteo = Integer.valueOf(String.
                    valueOf(queryAsignaciones.getSingleResult()));

                idRecolector = idRecolector + 1;
                recolector.setId(idRecolector);
                recolector.setLogin(recolectorCampos.get(0));
                recolector.setNombreFuncionario(recolectorCampos.get(1));
                recolector.setSeleccionado(false);

                if (resultadoConteo > 0) {
                    recolector.setTieneAsignaciones(ESiNo.SI.getCodigo());
                } else {
                    recolector.setTieneAsignaciones(ESiNo.NO.getCodigo());
                }

                recolectores.add(recolector);

            } catch (Exception e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                    LOGGER, e, e.getMessage());
            }
        }

        LOGGER.debug("out of RegionCapturaOfertaDAOBean#getInformacionRecolectores ...");
        return recolectores;
    }

    /**
     * @see IRegionCapturaOfertaDAO#getIdUltimaRegionRegistrada()
     *
     * @author rodrigo.hernandez
     */
    @Override
    public Long getIdUltimaRegionRegistrada() {
        LOGGER.debug("on RegionCapturaOfertaDAOBean#getIdUltimaRegionRegistrada ...");

        Long answer = null;
        Long secuencia = 0L;
        Query query = null;
        try {
            query = entityManager.createNativeQuery(
                "SELECT REGION_CAPTURA_OFERTA_ID_SEQ.NextVal FROM DUAL");
            secuencia = ((BigDecimal) query.getResultList().get(0)).longValue();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "RegionCapturaOfertaDAOBean#getIdUltimaRegionRegistrada");
        }
        return secuencia;
    }

    /**
     * @see IRegionCapturaOfertaDAO#getRegionCapturaOfertaPorId()
     *
     * @author rodrigo.hernandez
     */
    @Override
    public RegionCapturaOferta getRegionCapturaOfertaPorId(
        Long regionCapturaOfertaId) {
        LOGGER.debug("on RegionCapturaOfertaDAOBean#buscarPorIdComision ...");

        RegionCapturaOferta answer = null;
        String queryString;
        Query query;

        queryString = "SELECT rco " +
            " FROM RegionCapturaOferta rco " +
            " LEFT JOIN FETCH rco.areaCapturaOferta aco" +
            " LEFT JOIN FETCH aco.departamento " +
            " LEFT JOIN FETCH aco.municipio " +
            " LEFT JOIN FETCH aco.zona " +
            " WHERE rco.id = :rcoId ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("rcoId", regionCapturaOfertaId);

            answer = (RegionCapturaOferta) query.getSingleResult();
            answer.getDetalleCapturaOfertas().size();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "RegionCapturaOfertaDAOBean#getRegionCapturaOfertaPorId");
        }
        return answer;
    }

    @Override
    public List<RegionCapturaOferta> cargarRegionCapturaOfertaPorIdAreaYRecolector(
        Long idAreaAsignada, String usuario) {

        LOGGER.debug(
            "on RegionCapturaOfertaDAOBean#cargarRegionCapturaOfertaPorIdAreaYRecolector ...");

        List<RegionCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "Select rco" +
            " FROM RegionCapturaOferta rco" +
            " WHERE rco.areaCapturaOferta.id = :idAreaAsignada" +
            " AND rco.asignadoRecolectorId = :idRecolector";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", usuario);
            query.setParameter("idAreaAsignada", idAreaAsignada);

            answer = (List<RegionCapturaOferta>) query.getResultList();
            for (RegionCapturaOferta region : answer) {
                LOGGER.debug("" + region.getDetalleCapturaOfertas().size());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    //--------------------------------------------------------------------------------------------------	
    /**
     * @see IRegionCapturaOfertaDAO#cargarRegionCapturaOfertaPorIdArea(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<RegionCapturaOferta> cargarRegionCapturaOfertaPorIdArea(Long idAreaAsignada) {

        LOGGER.debug("on RegionCapturaOfertaDAOBean#cargarRegionCapturaOfertaPorIdArea ...");

        List<RegionCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT rco" +
            " FROM RegionCapturaOferta rco" +
            " WHERE rco.areaCapturaOferta.id = :idAreaAsignada";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAreaAsignada", idAreaAsignada);

            answer = (List<RegionCapturaOferta>) query.getResultList();
            for (RegionCapturaOferta region : answer) {
                LOGGER.debug("" + region.getDetalleCapturaOfertas().size());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IRegionCapturaOfertaDAO#getByComisionIdNF(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<RegionCapturaOferta> getByComisionIdNF(Long idComision) {

        LOGGER.debug("on RegionCapturaOfertaDAOBean#getByComisionId ...");

        ArrayList<RegionCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT rco " +
            " FROM RegionCapturaOferta rco " +
            " WHERE rco.comisionOferta.id = :comisionIdP";
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("comisionIdP", idComision);

            answer = (ArrayList<RegionCapturaOferta>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "RegionCapturaOfertaDAOBean#getByComisionIdNF");
        }

        return answer;
    }
    //-------------------------------------------------------------------------------------------------   

    /**
     * @see IRegionCapturaOfertaDAO#getRegionesCapturaOfertaPorAreaCapturaOfertaYRecolector(Long,
     * String)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<RegionCapturaOferta> getRegionesCapturaOfertaPorAreaCapturaOfertaYRecolector(
        Long areaCapturaOfertaId, String recolectorLogin) {

        LOGGER.debug(
            "on RegionCapturaOfertaDAOBean#getRegionesCapturaOfertaPorAreaCapturaOfertaYRecolector ...");

        ArrayList<RegionCapturaOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT rco " + " FROM RegionCapturaOferta rco " +
            " WHERE rco.areaCapturaOferta.id = :areaId" +
            " AND rco.asignadoRecolectorId = :recolectorId";
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("areaId", areaCapturaOfertaId);
            query.setParameter("recolectorId", recolectorLogin);

            answer = (ArrayList<RegionCapturaOferta>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER,
                    e,
                    "RegionCapturaOfertaDAOBean#getRegionesCapturaOfertaPorAreaCapturaOfertaYRecolector");
        }

        return answer;
    }

//end of class
}
