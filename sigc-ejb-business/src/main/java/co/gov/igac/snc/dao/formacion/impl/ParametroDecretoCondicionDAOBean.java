package co.gov.igac.snc.dao.formacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.formacion.IParametroDecretoCondicionDAO;
import co.gov.igac.snc.persistence.entity.formacion.ParametroDecretoCondicion;

/**
 * Implementación de métodos para los servicios de persistencia del objeto
 * {@link ParametroDecretoCondicion}.
 *
 * @author david.cifuentes
 */
@Stateless
public class ParametroDecretoCondicionDAOBean extends
    GenericDAOWithJPA<ParametroDecretoCondicion, Long> implements
    IParametroDecretoCondicionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ParametroDecretoCondicionDAOBean.class);

    // ------------------------- //
    /**
     * @author david.cifuentes
     * @see IParametroDecretoCondicion#buscarParametroDecretoCondicionAll()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ParametroDecretoCondicion> buscarParametroDecretoCondicionAll() {
        List<ParametroDecretoCondicion> answer = null;
        try {
            String sql = "SELECT pdc FROM ParametroDecretoCondicion pdc" +
                " ORDER BY pdc.nombre";
            Query query = this.entityManager.createQuery(sql);
            answer = query.getResultList();
            return answer;
        } catch (NoResultException e) {
            LOGGER.error(
                "Error en ParametroDecretoCondicionDAOBean#buscarParametroDecretoCondicionAll");
            return null;
        }
    }
}
