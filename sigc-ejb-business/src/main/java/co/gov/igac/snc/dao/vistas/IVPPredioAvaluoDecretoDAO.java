package co.gov.igac.snc.dao.vistas;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;

/**
 * Interfaz para los servicios de persistencia del objeto {@link VPPredioAvaluoDecreto}.
 *
 * @author david.cifuentes
 */
@Local
public interface IVPPredioAvaluoDecretoDAO extends
    IGenericJpaDAO<VPPredioAvaluoDecreto, Long> {

    /**
     * Método que busca los avalúos históricos de un pPredio por su id en la vista
     * VPPredioAvaluoDecreto
     *
     * @author david.cifuentes
     * @param fechaDelCalculo
     * @param Long
     * @return
     */
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(
        Long pPredioId, Date fechaDelCalculo);

    /**
     * Método que busca los avalúos históricos de un pPredio por su id.
     *
     * @author david.cifuentes
     * @modified christian.rodriguez
     * @param Long id del predio
     * @return historico de los predios
     *
     */
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredio(
        Long pPredioId);
}
