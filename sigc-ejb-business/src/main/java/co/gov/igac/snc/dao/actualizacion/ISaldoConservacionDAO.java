package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.SaldoConservacion;

/**
 * Servicios de persistencia para el objeto SaldoConservacion
 *
 * @author franz.gamba
 */
@Local
public interface ISaldoConservacionDAO extends IGenericJpaDAO<SaldoConservacion, Long> {

    /**
     * Método que trae los saldos de conservacion de una actualizacion
     *
     * @param actualizacionId
     * @return
     */
    public List<SaldoConservacion> getSaldosConservacionByActualizacionId(Long actualizacionId);

    /**
     * Busca todos los saldos referentes a un numero predial.
     *
     * @param numeroPredial
     * @return
     * @author andres.eslava
     */
    public List<SaldoConservacion> getSaldosConservacionByNumeroPredial(String numeroPredial);

}
