/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import javax.ejb.Local;

/**
 *
 * Interface para los métodos de bd de la MUNICIPIO_COMPLEMENTO
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IMunicipioComplementoDAO extends
    IGenericJpaDAO<MunicipioComplemento, String> {

}
