package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IMPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.MPredio;

@Stateless
public class MPredioDAOBean extends GenericDAOWithJPA<MPredio, Long>
    implements IMPredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPredioDAOBean.class);

    /**
     * Metodo que consulta mPredio pero lo trae con todas sus colecciones dependientes
     *
     * @throws Exception
     * @author leidy.gonzalez
     */
    @Override
    public MPredio findPPredioCompletoByIdTramite(Long idTramite) throws Exception {
        String query = "SELECT p" +
            " FROM MPredio p LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.circuloRegistral" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE p.tramite.id = :idTramite";

        Query q = entityManager.createQuery(query);
        q.setParameter("idTramite", idTramite);
        try {
            MPredio p = (MPredio) q.getSingleResult();

            return p;
        } catch (Exception e) {
            LOGGER.debug(
                "No se encontró el predio o hubo problemas recuperando objetos dependientes");
            LOGGER.debug("Error: " + e.getMessage());
        }
        throw new Exception("No se encontró el predio");
    }

    /**
     * @author leidy.gonzalez
     * @see IMPredioDAO#getMPredioByNumeroPredial(String)
     */
    @Override
    public MPredio getMPredioByNumeroPredial(String numeroPredial) {
        LOGGER.debug("getPPredioByNumeroPredial");
        Query q = entityManager.createNamedQuery("findPPredioByNumeroPredial");
        q.setParameter("numeroPredial", numeroPredial);
        try {
            MPredio p = (MPredio) q.getSingleResult();
            return p;
        } catch (Exception e) {
            LOGGER.debug("Error en obtener el predio: " + e.getMessage());
            return null;
        }
    }

//end of class
}
