package co.gov.igac.snc.dao.generales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IGrupoDAO;
import co.gov.igac.snc.persistence.entity.generales.Grupo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class GrupoDAOBean extends GenericDAOWithJPA<Grupo, Long> implements
    IGrupoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GrupoDAOBean.class);

    /**
     * @see IGrupoDAO#buscarGrupos()
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Grupo> buscarGrupos() {

        List<Grupo> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT g FROM Grupo g ORDER BY nombre ASC";

        try {
            query = this.entityManager.createQuery(queryToExecute);

            answer = (ArrayList<Grupo>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("GrupoDAOBean#buscarGrupos: " +
                "La consulta de grupos no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IGrupoDAO#consultarGrupoPorEstadoActivo(String usuarioLogin)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Grupo> consultarGrupoPorEstadoActivo(String activo) {

        List<Grupo> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT g FROM Grupo g where g.activo = :activo";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("activo", activo);

            answer = (ArrayList<Grupo>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("GrupoDAOBean#consultarGrupoPorEstadoActivo: " +
                "La consulta de grupos no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
    
     /**
     * @see IGrupoDAO#consultarGrupoPorEstadoActivo(String usuarioLogin)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> obtenerListaNombresGrupo(String activo) {

        List<String> answer = null;

        Query query;
        String queryToExecute;

        queryToExecute = "SELECT UPPER(g.nombre) FROM Grupo g where g.activo = :activo";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("activo", activo);

            answer = (List<String>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("GrupoDAOBean#obtenerListaNombresGrupo: " +
                "La consulta de listas nombre grupos no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

}
