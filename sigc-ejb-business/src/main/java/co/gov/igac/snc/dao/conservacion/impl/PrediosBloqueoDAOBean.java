/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPrediosBloqueoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IErrorProcesoMasivoDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EErrorProcesoMasivoUsuario;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.util.EProgramaGeneraError;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.FuncionPredioPersonaBloqueoHoy;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author juan.agudelo
 *
 * @deprecated esta clase no cumple con el estándar de codificación del proyecto. Se debe usar
 * PredioBloqueoDAOBean
 *
 */
// TODO :: juan.agudelo :: 21-05-2012 :: usar la clase que corresponde ::
// pedro.garcia
@Stateless
@Deprecated
public class PrediosBloqueoDAOBean extends
    GenericDAOWithJPA<PredioBloqueo, Long> implements IPrediosBloqueoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrediosBloqueoDAOBean.class);

    @EJB
    private IPredioDAO predioService;

    @EJB
    private IDocumentoDAO documentoService;

    @EJB
    private ITipoDocumentoDAO tipoDocumentoService;

    private TipoDocumento tipoDocumento;

    @EJB
    private IErrorProcesoMasivoDAO errorProcesoMasivoService;

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#buscarPrediosBloqueo(FiltroDatosConsultaPrediosBloqueo, int...)
     */
    @SuppressWarnings({"unchecked", "empty-statement"})
    @Implement
    @Override
    public List<PredioBloqueo> buscarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("executing PrediosBloqueoDAOBean#buscarPrediosBloqueo");

        StringBuilder query = new StringBuilder();
        String queryPB;
        List<PredioBloqueo> prediosBloqueo = null;
        StringBuilder pbIdsTemp;

        query.append("SELECT pb FROM PredioBloqueo pb" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " JOIN FETCH pb.predio pbp" + " JOIN FETCH pbp.departamento" +
            " JOIN FETCH pbp.municipio" + " WHERE 1 = 1");

        if (filtroDatosConsultaPredioBloqueo.getDepartamento() != null) {
            query.append(" AND pbp.departamento = :departamento");
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipio() != null) {
            query.append(" AND pbp.municipio = :municipio");
        }
        if (filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo()
                .isEmpty()) {
            query.append(" AND pbp.departamento.codigo = :departamentoCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getMunicipioCodigo()
                .isEmpty()) {
            query.append(" AND pbp.municipio.codigo like :municipioCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getTipoAvaluo() != null &&
            !filtroDatosConsultaPredioBloqueo.getTipoAvaluo().isEmpty()) {
            query.append(" AND pbp.tipoAvaluo= :tipoAvaluo");
        }
        if (filtroDatosConsultaPredioBloqueo.getSectorCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getSectorCodigo()
                .isEmpty()) {
            query.append(" AND pbp.sectorCodigo= :sectorCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getComunaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getComunaCodigo()
                .isEmpty()) {
            query.append(" AND pbp.barrioCodigo like :comunaCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getBarrioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getBarrioCodigo()
                .isEmpty()) {
            query.append(" AND pbp.barrioCodigo like :barrioCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo()
                .isEmpty()) {
            query.append(" AND pbp.manzanaCodigo= :manzanaCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getPredio() != null &&
            !filtroDatosConsultaPredioBloqueo.getPredio().isEmpty()) {
            query.append(" AND pbp.predio= :predio");
        }
        if (filtroDatosConsultaPredioBloqueo.getCondicionPropiedad() != null &&
            !filtroDatosConsultaPredioBloqueo.getCondicionPropiedad()
                .isEmpty()) {
            query.append(" AND pbp.condicionPropiedad= :condicionPropiedad");
        }
        if (filtroDatosConsultaPredioBloqueo.getEdificio() != null &&
            !filtroDatosConsultaPredioBloqueo.getEdificio().isEmpty()) {
            query.append(" AND pbp.edificio= :edificio");
        }
        if (filtroDatosConsultaPredioBloqueo.getPiso() != null &&
            !filtroDatosConsultaPredioBloqueo.getPiso().isEmpty()) {
            query.append(" AND pbp.piso= :piso");
        }
        if (filtroDatosConsultaPredioBloqueo.getUnidad() != null &&
            !filtroDatosConsultaPredioBloqueo.getUnidad().isEmpty()) {
            query.append(" AND pbp.unidad= :unidad");
        }

        query.append(" AND ").append(
            FuncionPredioPersonaBloqueoHoy.getQueryprediopersonabloqueohoy());

        Query q = this.entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());

        if (filtroDatosConsultaPredioBloqueo.getDepartamento() != null) {
            q.setParameter("departamento",
                filtroDatosConsultaPredioBloqueo.getDepartamento());
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipio() != null) {
            q.setParameter("municipio",
                filtroDatosConsultaPredioBloqueo.getMunicipio());
        }
        if (filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo()
                .isEmpty()) {
            q.setParameter("departamentoCodigo",
                filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getMunicipioCodigo()
                .isEmpty()) {
            q.setParameter("municipioCodigo", "%" +
                filtroDatosConsultaPredioBloqueo.getMunicipioCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getTipoAvaluo() != null &&
            !filtroDatosConsultaPredioBloqueo.getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo",
                filtroDatosConsultaPredioBloqueo.getTipoAvaluo());
        }
        if (filtroDatosConsultaPredioBloqueo.getSectorCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getSectorCodigo()
                .isEmpty()) {
            q.setParameter("sectorCodigo",
                filtroDatosConsultaPredioBloqueo.getSectorCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getComunaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getComunaCodigo()
                .isEmpty()) {
            q.setParameter("comunaCodigo",
                filtroDatosConsultaPredioBloqueo.getComunaCodigo() + "%");
        }
        if (filtroDatosConsultaPredioBloqueo.getBarrioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getBarrioCodigo()
                .isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroDatosConsultaPredioBloqueo.getBarrioCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo()
                .isEmpty()) {
            q.setParameter("manzanaCodigo",
                filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getPredio() != null &&
            !filtroDatosConsultaPredioBloqueo.getPredio().isEmpty()) {
            q.setParameter("predio",
                filtroDatosConsultaPredioBloqueo.getPredio());
        }
        if (filtroDatosConsultaPredioBloqueo.getCondicionPropiedad() != null &&
            !filtroDatosConsultaPredioBloqueo.getCondicionPropiedad()
                .isEmpty()) {
            q.setParameter("condicionPropiedad",
                filtroDatosConsultaPredioBloqueo.getCondicionPropiedad());
        }
        if (filtroDatosConsultaPredioBloqueo.getEdificio() != null &&
            !filtroDatosConsultaPredioBloqueo.getEdificio().isEmpty()) {
            q.setParameter("edificio",
                filtroDatosConsultaPredioBloqueo.getEdificio());
        }
        if (filtroDatosConsultaPredioBloqueo.getPiso() != null &&
            !filtroDatosConsultaPredioBloqueo.getPiso().isEmpty()) {
            q.setParameter("piso", filtroDatosConsultaPredioBloqueo.getPiso());
        }
        if (filtroDatosConsultaPredioBloqueo.getUnidad() != null &&
            !filtroDatosConsultaPredioBloqueo.getUnidad().isEmpty()) {
            q.setParameter("unidad",
                filtroDatosConsultaPredioBloqueo.getUnidad());
        }

        try {

            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {

                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    q.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        q.setMaxResults(rowCount);
                    }
                }

                if (rowStartIdx < 1) {
                    List<PredioBloqueo> prediosBloqueoObj = q.getResultList();

                    if (prediosBloqueoObj != null &&
                        !prediosBloqueoObj.isEmpty()) {

                        pbIdsTemp = new StringBuilder();

                        for (PredioBloqueo oTemp : prediosBloqueoObj) {
                            pbIdsTemp.append("'");
                            pbIdsTemp.append(oTemp.getId().toString());
                            pbIdsTemp.append("'");
                            pbIdsTemp.append(",");
                        }

                        queryPB = "SELECT pb FROM PredioBloqueo pb" +
                            " JOIN FETCH pb.documentoSoporteBloqueo" +
                            " JOIN FETCH pb.predio" +
                            " WHERE pb.id IN (" +
                            pbIdsTemp.toString().substring(0,
                                pbIdsTemp.length() - 1) + ")";

                        Query qPB = this.entityManager.createQuery(queryPB);

                        final List<PredioBloqueo> prediosBloqueoDocumento = (super.
                            findInRangeUsingQuery(qPB,
                                rowStartIdxAndCount));
                        if (prediosBloqueoDocumento == null || prediosBloqueoDocumento.isEmpty()) {
                            prediosBloqueo = prediosBloqueoObj;
                        } else {
                            prediosBloqueo = prediosBloqueoDocumento;
                        }
                    }
                } else {
                    List<PredioBloqueo> prediosBloqueoTmp = q.getResultList();

                    if (prediosBloqueoTmp != null &&
                        !prediosBloqueoTmp.isEmpty()) {

                        pbIdsTemp = new StringBuilder();

                        for (PredioBloqueo oTemp : prediosBloqueoTmp) {
                            pbIdsTemp.append("'");
                            pbIdsTemp.append(oTemp.getId().toString());
                            pbIdsTemp.append("'");
                            pbIdsTemp.append(",");
                        }

                        queryPB = "SELECT pb FROM PredioBloqueo pb" +
                            " JOIN FETCH pb.documentoSoporteBloqueo" +
                            " JOIN FETCH pb.predio" +
                            " WHERE pb.id IN (" +
                            pbIdsTemp.toString().substring(0,
                                pbIdsTemp.length() - 1) + ")";
                        ;

                        Query qPB = entityManager.createQuery(queryPB);

                        prediosBloqueo = qPB.getResultList();
                    }

                }
            }
            return prediosBloqueo;

        } catch (IndexOutOfBoundsException ie) {
            LOGGER.debug("Error: " + ie.getMessage());
            return new ArrayList<PredioBloqueo>();
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#actualizarPrediosBloqueo(UsuarioDTO, List)
     */
    @Implement
    @Override
    public ResultadoBloqueoMasivo actualizarPrediosBloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloqueados) {

        List<PredioBloqueo> preBloqueados = null;
        List<PredioBloqueo> preBloqueadosExitosamente = null;
        List<ErrorProcesoMasivo> preNoBloqueados = null;
        ResultadoBloqueoMasivo answer = null;
        PredioBloqueo pbTemp;
        boolean isPredioCancelado;
        Map<String, String> rutasAlfrescoDocumentosSoporte = null;
        List<Predio> predios;
        Documento dr;

        if (prediosBloqueados != null && prediosBloqueados.size() > 0) {

            predios = new ArrayList<Predio>();

            for (PredioBloqueo p : prediosBloqueados) {

                isPredioCancelado = this.predioService
                    .isPredioCanceladoByPredioIdOrNumeroPredial(p
                        .getPredio().getId(), null);

                if (isPredioCancelado == false) {
                    predios.add(p.getPredio());

                    if (preBloqueados == null) {
                        preBloqueados = new ArrayList<PredioBloqueo>();
                    }

                    preBloqueados.add(p);

                } else {
                    ErrorProcesoMasivo epmTmp = new ErrorProcesoMasivo();
                    epmTmp.setErrorTecnico(EPredioEstado.CANCELADO.getCodigo());
                    epmTmp.setTextoFuente(p.getPredio().getNumeroPredial());

                    if (preNoBloqueados == null) {
                        preNoBloqueados = new ArrayList<ErrorProcesoMasivo>();
                    }

                    preNoBloqueados.add(epmTmp);
                }
            }

            if (predios.size() > 0) {

                if (prediosBloqueados.get(0).getDocumentoSoporteBloqueo() != null) {

                    rutasAlfrescoDocumentosSoporte = this.documentoService
                        .almacenarDocumentosBloqueoGestorDocumental(usuario,
                            prediosBloqueados.get(0)
                                .getDocumentoSoporteBloqueo(),
                            null, predios);

                }

                for (PredioBloqueo predioB : preBloqueados) {

                    predioB.setUsuarioLog(usuario.getLogin());
                    predioB.setFechaLog(new Date(System.currentTimeMillis()));

                    if (rutasAlfrescoDocumentosSoporte != null &&
                        !rutasAlfrescoDocumentosSoporte.isEmpty()) {

                        for (String url : rutasAlfrescoDocumentosSoporte
                            .keySet()) {
                            if (url.equals(predioB.getPredio()
                                .getNumeroPredial())) {
                                predioB.getDocumentoSoporteBloqueo()
                                    .setIdRepositorioDocumentos(
                                        rutasAlfrescoDocumentosSoporte
                                            .get(url));
                            }
                        }
                    }

                    predioB.getDocumentoSoporteBloqueo().setUsuarioLog(
                        usuario.getLogin());
                    predioB.getDocumentoSoporteBloqueo().setUsuarioCreador(
                        usuario.getLogin());

                    predioB.getDocumentoSoporteBloqueo().setPredioId(
                        predioB.getPredio().getId());
                    predioB.getDocumentoSoporteBloqueo().setFechaLog(
                        new Date(System.currentTimeMillis()));

                    try {

                        dr = this.documentoService.update(predioB
                            .getDocumentoSoporteBloqueo());
                        predioB.setDocumentoSoporteBloqueo(dr);

                        pbTemp = update(predioB);

                        if (pbTemp.getDocumentoSoporteBloqueo() != null) {
                            pbTemp.getDocumentoSoporteBloqueo().getId();
                        }

                        if (preBloqueadosExitosamente == null) {
                            preBloqueadosExitosamente = new ArrayList<PredioBloqueo>();
                        }
                        preBloqueadosExitosamente.add(pbTemp);

                    } catch (Error e) {
                        LOGGER.error(e.getMessage(), e);
                    } catch (Exception ex) {
                        throw SncBusinessServiceExceptions.EXCEPCION_100010
                            .getExcepcion(LOGGER, ex, ex.getMessage());
                    }
                }
            }
        }

        answer = new ResultadoBloqueoMasivo(preBloqueadosExitosamente, null,
            preNoBloqueados);

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#contarPrediosBloqueoByNumeroPredial(String)
     */
    @Override
    public Integer contarPrediosBloqueoByNumeroPredial(String numeroPredial) {
        LOGGER.debug("executing PrediosBloqueoDAOBean#buscarPrediosBloqueo");

        Integer cPrediosBloqueo = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT COUNT (pb) FROM PredioBloqueo pb" +
            " JOIN pb.documentoSoporteBloqueo" + " JOIN pb.predio pbp" +
            " WHERE  pbp.numeroPredial= :numeroPredial");

        query.append(" AND " +
            FuncionPredioPersonaBloqueoHoy
                .getQueryprediopersonabloqueohoy());

        Query q = entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());

        q.setParameter("numeroPredial", numeroPredial);

        try {
            Long l = (Long) q.getSingleResult();
            cPrediosBloqueo = Integer.valueOf(l.intValue());
            return cPrediosBloqueo;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#buscarPrediosBloqueoByNumeroPredial(String)
     */
    public List<PredioBloqueo> buscarPrediosBloqueoByNumeroPredial(
        String numeroPredial, final int... rowStartIdxAndCount) {
        LOGGER.debug("executing PrediosBloqueoDAOBean#buscarPrediosBloqueo");

        StringBuilder query = new StringBuilder();

        query.append("SELECT pb FROM PredioBloqueo pb" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " JOIN FETCH pb.predio pbp" +
            " WHERE  pbp.numeroPredial= :numeroPredial");

        query.append(" AND " +
            FuncionPredioPersonaBloqueoHoy
                .getQueryprediopersonabloqueohoy());

        Query q = entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());
        q.setParameter("numeroPredial", numeroPredial);

        try {
            List<PredioBloqueo> prediosBloqueo = (super.findInRangeUsingQuery(
                q, rowStartIdxAndCount));
            return prediosBloqueo;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#actualizarPrediosDesbloqueo(UsuarioDTO, List)
     */
    public List<PredioBloqueo> actualizarPrediosDesbloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloq) {

        List<PredioBloqueo> pBloqueo = new ArrayList<PredioBloqueo>();
        Map<String, String> rutasAlfrescoDocumentosSoporte = null;
        List<Predio> predios;
        Documento dr;

        if (prediosBloq != null && !prediosBloq.isEmpty()) {
            try {
                predios = new ArrayList<Predio>();

                for (PredioBloqueo p : prediosBloq) {
                    predios.add(p.getPredio());
                }

                if (prediosBloq.get(0).getDocumentoSoporteDesbloqueo() != null) {
                    rutasAlfrescoDocumentosSoporte = this.documentoService
                        .almacenarDocumentosBloqueoGestorDocumental(usuario,
                            prediosBloq.get(0)
                                .getDocumentoSoporteDesbloqueo(),
                            null, predios);
                }

                for (PredioBloqueo pBTemp : prediosBloq) {

                    pBTemp.setUsuarioLogDesbloqueo(usuario.getLogin());
                    pBTemp.setFechaLogDesbloqueo(new Date(System.currentTimeMillis()));

                    if (rutasAlfrescoDocumentosSoporte != null &&
                        !rutasAlfrescoDocumentosSoporte.isEmpty()) {

                        for (String url : rutasAlfrescoDocumentosSoporte
                            .keySet()) {
                            if (url.equals(pBTemp.getPredio()
                                .getNumeroPredial())) {
                                pBTemp.getDocumentoSoporteDesbloqueo()
                                    .setIdRepositorioDocumentos(
                                        rutasAlfrescoDocumentosSoporte
                                            .get(url));
                            }
                        }
                    }

                    pBTemp.getDocumentoSoporteDesbloqueo().setUsuarioLog(
                        usuario.getLogin());
                    pBTemp.getDocumentoSoporteDesbloqueo().setUsuarioCreador(
                        usuario.getLogin());
                    dr = this.documentoService.update(pBTemp
                        .getDocumentoSoporteDesbloqueo());
                    pBTemp.setDocumentoSoporteDesbloqueo(dr);

                    update(pBTemp);
                    pBloqueo.add(pBTemp);

                }
            } catch (Exception e) {
                LOGGER.debug("Error: " + e.getMessage());
            }
        }
        return pBloqueo;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#actualizarPrediosBloqueoM(UsuarioDTO, ArrayList)
     */
    public List<PredioBloqueo> actualizarPrediosBloqueoM(UsuarioDTO usuario,
        ArrayList<PredioBloqueo> bloqueoMasivo) {

        List<PredioBloqueo> preBloqueados = new ArrayList<PredioBloqueo>();
        Predio predioTemp;

        if (bloqueoMasivo != null && bloqueoMasivo.size() > 0) {

            Documento ds = null;
            Documento tempDoc = bloqueoMasivo.get(0)
                .getDocumentoSoporteBloqueo();
            tempDoc.setUsuarioLog(usuario.getLogin());
            tempDoc.setUsuarioCreador(usuario.getLogin());
            tempDoc.setFechaLog(new Date(System.currentTimeMillis()));

            try {
                this.tipoDocumento = this.tipoDocumentoService.findById(7L);
                tempDoc.setTipoDocumento(this.tipoDocumento);
                ds = this.documentoService.update(tempDoc);
            } catch (Exception e) {
                LOGGER.debug("Error: " + e.getMessage());
            }

            for (int i = 0; i < bloqueoMasivo.size(); i++) {
                PredioBloqueo predioB = bloqueoMasivo.get(i);
                predioB.setUsuarioLog(usuario.getLogin());
                predioB.setFechaLog(new Date(System.currentTimeMillis()));

                try {
                    predioTemp = this.predioService
                        .findByNumeroPredial(bloqueoMasivo.get(i)
                            .getPredio().getNumeroPredial());
                    predioB.setPredio(predioTemp);
                    predioB.setDocumentoSoporteBloqueo(ds);
                    update(predioB);
                    preBloqueados.add(predioB);
                } catch (Exception ex) {
                    LOGGER.debug("Error: " + ex.getMessage());
                }
            }
        }
        return preBloqueados;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#bloquearMasivamentePredios(UsuarioDTO, ArrayList)
     */
    @Override
    public ResultadoBloqueoMasivo bloquearMasivamentePredios(
        UsuarioDTO usuario, ArrayList<PredioBloqueo> bloqueoMasivo) {

        List<PredioBloqueo> preBloqueados = new ArrayList<PredioBloqueo>();
        List<PredioBloqueo> prediosBloqueados;
        List<PredioBloqueo> prediosTemp = null;
        List<ErrorProcesoMasivo> erroresPredioBloqueo = null;
        List<ErrorProcesoMasivo> erroresResultado = null;
        ErrorProcesoMasivo errorPM;
        ErrorProcesoMasivo errorTemp;
        boolean isPredioCancelado;
        Map<String, String> rutasAlfrescoDocumentosSoporte = null;
        Map<String, String> rutasAlfrescoDocumentoBloqueoMasivo = null;
        List<Predio> predios;
        Predio predioTemp = null;
        Documento dr;
        ResultadoBloqueoMasivo answer = null;

        try {
            if (bloqueoMasivo != null && bloqueoMasivo.size() > 0) {

                Documento tempDoc = bloqueoMasivo.get(0)
                    .getDocumentoSoporteBloqueo();
                tempDoc.setUsuarioLog(usuario.getLogin());
                tempDoc.setUsuarioCreador(usuario.getLogin());
                tempDoc.setFechaLog(new Date(System.currentTimeMillis()));
                predios = new ArrayList<Predio>();

                for (PredioBloqueo p : bloqueoMasivo) {

                    p.setUsuarioLog(usuario.getLogin());
                    p.setFechaLog(new Date(System.currentTimeMillis()));

                    predioTemp = this.predioService.getPredioByNumeroPredial(p
                        .getPredio().getNumeroPredial());

                    if (predioTemp != null) {

                        isPredioCancelado = this.predioService
                            .isPredioCanceladoByPredioIdOrNumeroPredial(
                                predioTemp.getId(), null);

                        if (isPredioCancelado == false) {
                            predios.add(predioTemp);
                            p.setPredio(predioTemp);
                            preBloqueados.add(p);
                        } else {
                            errorPM = new ErrorProcesoMasivo();
                            errorPM.setErrorUsuario(EErrorProcesoMasivoUsuario.PREDIO_CANCELADO
                                .getCodigo());
                            errorPM.setPrograma(EProgramaGeneraError.PREDIO_BLOQUEO.getCodigo());
                            errorPM.setFecha(new Date());
                            errorPM.setTextoFuente(p.getPredio()
                                .getNumeroPredial() +
                                ";" +
                                ";" +
                                p.getMotivoBloqueo());

                            if (erroresPredioBloqueo == null) {
                                erroresPredioBloqueo = new ArrayList<ErrorProcesoMasivo>();
                            }
                            erroresPredioBloqueo.add(errorPM);

                        }
                    } else {
                        errorPM = new ErrorProcesoMasivo();
                        errorPM.setErrorUsuario(EErrorProcesoMasivoUsuario.PREDIO_NO_ENCONTRADO
                            .getCodigo());
                        errorPM.setPrograma(EProgramaGeneraError.PREDIO_BLOQUEO.getCodigo());
                        errorPM.setFecha(new Date());
                        errorPM.setTextoFuente(p.getPredio().getNumeroPredial() +
                            ";" + ";" + p.getMotivoBloqueo());

                        if (erroresPredioBloqueo == null) {
                            erroresPredioBloqueo = new ArrayList<ErrorProcesoMasivo>();
                        }
                        erroresPredioBloqueo.add(errorPM);
                    }
                }

                if (!predios.isEmpty()) {

                    if (bloqueoMasivo.get(0) != null &&
                        bloqueoMasivo.get(0).getDocumentoSoporteBloqueo() != null) {

                        rutasAlfrescoDocumentosSoporte = this.documentoService
                            .almacenarDocumentosBloqueoGestorDocumental(usuario,
                                bloqueoMasivo.get(0).getDocumentoSoporteBloqueo(),
                                null, predios);

                        if (bloqueoMasivo.get(0).getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos() != null &&
                            bloqueoMasivo.get(0)
                                .getDocumentoSoporteBloqueo()
                                .getDocumentoArchivoAnexos().get(0) != null &&
                            bloqueoMasivo.get(0)
                                .getDocumentoSoporteBloqueo()
                                .getDocumentoArchivoAnexos().get(0)
                                .getDocumento() != null) {

                            rutasAlfrescoDocumentoBloqueoMasivo = this.documentoService
                                .almacenarDocumentosBloqueoGestorDocumental(
                                    usuario,
                                    bloqueoMasivo
                                        .get(0)
                                        .getDocumentoSoporteBloqueo()
                                        .getDocumentoArchivoAnexos()
                                        .get(0).getDocumento(),
                                    null, predios);

                        }
                    }
                }

                try {

                    prediosTemp = new ArrayList<PredioBloqueo>();
                    prediosTemp.addAll(preBloqueados);
                    prediosBloqueados = new ArrayList<PredioBloqueo>();
                    DocumentoArchivoAnexo documentoSoporteAnexoTmp = null;

                    for (PredioBloqueo predioB : prediosTemp) {

                        if (rutasAlfrescoDocumentosSoporte != null &&
                            !rutasAlfrescoDocumentosSoporte.isEmpty()) {

                            for (String url : rutasAlfrescoDocumentosSoporte
                                .keySet()) {
                                if (url.equals(predioB.getPredio()
                                    .getNumeroPredial())) {

                                    predioB.getDocumentoSoporteBloqueo()
                                        .setIdRepositorioDocumentos(
                                            rutasAlfrescoDocumentosSoporte
                                                .get(url));
                                }
                            }

                            if (rutasAlfrescoDocumentoBloqueoMasivo != null &&
                                !rutasAlfrescoDocumentoBloqueoMasivo
                                    .isEmpty()) {

                                for (String url : rutasAlfrescoDocumentoBloqueoMasivo
                                    .keySet()) {
                                    if (url.equals(predioB.getPredio()
                                        .getNumeroPredial())) {

                                        for (DocumentoArchivoAnexo daa : predioB
                                            .getDocumentoSoporteBloqueo()
                                            .getDocumentoArchivoAnexos()) {

                                            daa.setIdRepositorioDocumentos(
                                                rutasAlfrescoDocumentoBloqueoMasivo
                                                    .get(url));
                                        }
                                    }
                                }
                            }

                        }
                        predioB.getDocumentoSoporteBloqueo().setUsuarioLog(
                            usuario.getLogin());
                        predioB.getDocumentoSoporteBloqueo().setUsuarioCreador(
                            usuario.getLogin());

                        dr = this.documentoService.update(predioB
                            .getDocumentoSoporteBloqueo());

                        if (dr != null &&
                            dr.getDocumentoArchivoAnexos() != null &&
                            !dr.getDocumentoArchivoAnexos().isEmpty()) {
                            documentoSoporteAnexoTmp = dr
                                .getDocumentoArchivoAnexos().get(0);
                        }

                        dr.setPredioId(predioB.getPredio().getId());
                        predioB.setDocumentoSoporteBloqueo(dr);

                        predioB.setDocumentoSoporteBloqueo(dr);

                        predioB = update(predioB);
                        prediosBloqueados.add(predioB);

                    }

                    if (prediosTemp != null && !prediosTemp.isEmpty()) {

                        erroresResultado = new ArrayList<ErrorProcesoMasivo>();

                        if (erroresPredioBloqueo != null &&
                            !erroresPredioBloqueo.isEmpty()) {
                            for (ErrorProcesoMasivo error : erroresPredioBloqueo) {

                                if (documentoSoporteAnexoTmp != null &&
                                    documentoSoporteAnexoTmp
                                        .getDocumento() != null) {
                                    error.setSoporteDocumento(documentoSoporteAnexoTmp
                                        .getDocumento());

                                }
                                errorTemp = this.errorProcesoMasivoService
                                    .update(error);
                                erroresResultado.add(errorTemp);
                            }
                        }
                    }

                    answer = new ResultadoBloqueoMasivo(prediosBloqueados,
                        null, erroresResultado);
                } catch (Exception ex) {

                    throw SncBusinessServiceExceptions.EXCEPCION_100010
                        .getExcepcion(LOGGER, ex, ex.getMessage());
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, ex, ex.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see IPrediosBloqueoDAO#getPredioBloqueosByNumeroPredial(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<PredioBloqueo> getPredioBloqueosByNumeroPredial(
        String numeroPredial) {
        LOGGER.debug("terting: PrediosBloqueoDAOBean#getPredioBloqueosByNumeroPredial");

        List<PredioBloqueo> answer;
        StringBuilder query = new StringBuilder();

        query.append("SELECT pb FROM PredioBloqueo pb" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " JOIN FETCH pb.predio pbp" +
            " LEFT JOIN FETCH pb.municipio" +
            " LEFT JOIN FETCH pb.departamento" +
            " WHERE  pbp.numeroPredial= :numeroPredial");

        query.append(" AND " +
            FuncionPredioPersonaBloqueoHoy
                .getQueryprediopersonabloqueohoy());

        Query q = entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());
        q.setParameter("numeroPredial", numeroPredial);

        try {
            answer = q.getResultList();

        } catch (IndexOutOfBoundsException iobe) {
            return new ArrayList<PredioBloqueo>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PrediosBloqueoDAOBean#getPredioBloqueosByNumeroPredial");
        }

        return answer;
    }

}
