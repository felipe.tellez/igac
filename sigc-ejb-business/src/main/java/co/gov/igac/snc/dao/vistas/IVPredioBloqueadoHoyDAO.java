/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VPredioBloqueadoHoy;

/**
 * Interfaz para el dao de VPredioBloqueadoHoy
 *
 * @author juan.agudelo
 */
@Local
public interface IVPredioBloqueadoHoyDAO extends IGenericJpaDAO<VPredioBloqueadoHoy, Long> {

}
