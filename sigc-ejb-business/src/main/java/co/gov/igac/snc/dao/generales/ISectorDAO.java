package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;
import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 *
 */
@Local
public interface ISectorDAO extends IGenericJpaDAO<Sector, String> {

    public List<Sector> findByZona(String zonaId);

    public Sector getSectorByCodigo(String codigo);
}
