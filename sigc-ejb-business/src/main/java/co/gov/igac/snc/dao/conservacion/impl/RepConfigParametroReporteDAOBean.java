/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepConfigParametroReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementacion metosdos de acceso a datos de la entidad RepConfigParametroReporte
 *
 * @author felipe.cadena
 */
@Stateless
public class RepConfigParametroReporteDAOBean extends GenericDAOWithJPA<RepConfigParametroReporte, Long>
    implements IRepConfigParametroReporteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RepConfigParametroReporteDAOBean.class);

    /**
     * @see IRepConfigParametroReporteDAO#obtenerConfiguracionPorCategoria(int)
     * @author felipe.cadena
     */
    @Override
    public List<RepConfigParametroReporte> obtenerConfiguracionPorCategoria(String categoria) {

        List<RepConfigParametroReporte> answer = null;
        StringBuilder queryString = new StringBuilder();
        Query query;

        try {

            queryString.append("SELECT c FROM RepConfigParametroReporte c WHERE c.categoria = :cat");
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("cat", categoria);
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "RepConfigParametroReporteDAOBean.obtenerConfiguracionPorCategoria",
                "RepConfigParametroReporteDAOBean#obtenerConfiguracionPorCategoria");
        }

        return answer;
    }

    /**
     * @see IRepConfigParametroReporteDAO#consultaConfiguracionReportePredial(int)
     * @author leidy.gonzalez
     */
    @Override
    public List<RepConfigParametroReporte> consultaConfiguracionReportePredial(Long idReporte) {

        List<RepConfigParametroReporte> answer = null;
        StringBuilder queryString = new StringBuilder();
        Query query;

        try {

            queryString.append(
                "SELECT c FROM RepConfigParametroReporte c WHERE c.reporteId = :idReporte");
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idReporte", idReporte);
            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "RepConfigParametroReporteDAOBean.consultaConfiguracionReportePredial",
                "RepConfigParametroReporteDAOBean#consultaConfiguracionReportePredial");
        }

        return answer;
    }

}
