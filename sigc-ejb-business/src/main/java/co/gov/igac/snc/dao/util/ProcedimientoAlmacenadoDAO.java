package co.gov.igac.snc.dao.util;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.EntityManager;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dto.util.ParametroSPDto;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import org.hibernate.Session;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;

import org.hibernate.engine.spi.SessionFactoryImplementor;

/**
 * Clase utilizada para ejecutar funciones y procedimientos almacenados de Oracle
 *
 * @author fredy.wilches
 *
 */
public class ProcedimientoAlmacenadoDAO {

    /**
     * Variable para uso de logs
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProcedimientoAlmacenadoDAO.class);

    private Connection dbConn;

    /**
     *
     * @param conn
     */
    public ProcedimientoAlmacenadoDAO(Connection conn) {
        this.dbConn = conn;
    }

    /**
     *
     * @param em
     */
    public ProcedimientoAlmacenadoDAO(EntityManager em) {
        this.dbConn = getDatabaseConnectionFromEntityManager(em);
    }

    /**
     * Obtiene una conexión de base de datos a partir de un entity manager. La idea de esta
     * implementación es poder reutilizar el pool de conexiones que es manejado intrínsecamente por
     * el EntityManager
     *
     * Advertencia:
     *
     * Esta implementación ha sido probada como prueba unitaria y en el ambiente de integración con
     * Jboss. Probablemente necesite cambios al ser integrada en otros servidores de aplicaciones.
     *
     * En última instancia se puede usar el constructor de la clase que recibe directamente la
     * conexión. En ese caso hay que modificar la forma de ejecutar las pruebas unitarias.
     *
     * Dados los posibles errores de los desarrolladores en cualquier caso debe mantenerse la
     * correcta ejecución de las pruebas unitarias.
     *
     * Articulos Relacionados: - Be careful while using EntityManager.getDelegate()
     * http://weblogs.java.net/blog/2009/05/25/be-careful-while-using-entitymanagergetdelegate -
     * Getting Database connection in pure JPA setup
     * http://stackoverflow.com/questions/3493495/getting-database-connection-in-pure-jpa-setup
     *
     * - Versión para JPA 2.0
     * http://stackoverflow.com/questions/6707115/get-hold-of-a-jdbc-connection-object-from-a-stateless-bean
     *
     * @author juan.mendez
     * @param em
     * @return
     */
    private Connection getDatabaseConnectionFromEntityManager(EntityManager em) {
        Connection connection;
        try {
            Session session = (Session) em.getDelegate();
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider cp = sfi.getConnectionProvider();
            connection = cp.getConnection();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return connection;
    }

    /**
     * Método que ejecuta funciones y procedimientos almacenados de Oracle
     *
     * @param nombreProcedimiento
     * @param parametros
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public Object[] ejecutarSP(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        List<Object> parametros) throws ExcepcionSNC {

        Object[] answer = ejecutarSP(nombreProcedimiento, parametros,
            EProcedimientoAlmacenadoFuncionTipo.PROCEDIMIENTO);
        return answer;
    }

    /**
     * Método que ejecuta funciones y procedimientos almacenados de Oracle
     *
     * @param nombreProcedimiento
     * @param parametros
     * @param tipo
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public Object[] ejecutarSP(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        List<Object> parametros, EProcedimientoAlmacenadoFuncionTipo tipo)
        throws ExcepcionSNC {

        Object[] answer = ejecutarSP(nombreProcedimiento, parametros, tipo, null);
        return answer;
    }

    /**
     * Método que ejecuta funciones y procedimientos almacenados de Oracle
     *
     * @modified pedro.garcia 11-04-2012 se valida que el cursor de retorno pueda ser null, en cuyo
     * caso se asigna ese valor directamente a la variable de retorno
     * @modified david.cifuentes 30-10-2014 Ajuste en casting a Bigdecimal.
     *
     * @param nombreProcedimiento
     * @param valoresParametros
     * @param tipo
     * @param schema
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public Object[] ejecutarSP(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        List<Object> valoresParametros,
        EProcedimientoAlmacenadoFuncionTipo tipo,
        String schema) throws ExcepcionSNC {
        LOGGER.debug("ejecutarSP");
        CallableStatement cs = null;

        try {
            DatabaseMetaData dmd = this.dbConn.getMetaData();
            int numeroParametros = 0;
            /** *********************************************** */
            int numeroParametrosResultSet = 0;
            /** *********************************************** */

            // String nombreCatalogo = "";
            String nombreProcedimientoEnCatalogo = null;
            if (nombreProcedimiento.toString().indexOf(".") >= 0) {
                // utiliza un paquete o "catalog" puesto que el nombre contiene
                // un punto
                String[] s = nombreProcedimiento.toString().split("\\.");
                // nombreCatalogo = s[0];
                nombreProcedimientoEnCatalogo = s[1];
            } else {
                nombreProcedimientoEnCatalogo = nombreProcedimiento.toString();
            }

            ///////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////
            /** ************************************************************************ */
            ResultSet rsColumns = dmd.getProcedureColumns(null, null, nombreProcedimientoEnCatalogo,
                "%");

            List<ParametroSPDto> parametrosSP = new ArrayList<ParametroSPDto>();
            List<ParametroSPDto> parametrosSPResultSet = new ArrayList<ParametroSPDto>();

            int parametrosEntradaSP = 0;
            while (rsColumns.next()) {
                // se obtiene nombre columna, tipo de columna (in, out,etc),
                // tipo dato y nombre de tipo
                ParametroSPDto parametro = new ParametroSPDto(rsColumns.getString(4),
                    rsColumns.getShort(5), rsColumns.getInt(6), rsColumns.getString(7));
                LOGGER.debug(parametro.toString());

                if (parametro.isInputParameter()) {
                    parametrosEntradaSP++;
                }
                /** *********************************************** */
                // Verifica  los parámetros que tiene el procedimiento
                boolean isResultSet = false;
                if (parametro.getTypeName().equals("PL/SQL RECORD")) {
                    isResultSet = true;
                    LOGGER.debug("" + isResultSet);
                    continue;
                }
                // Este bloque setea el arrayList con el tipo de los parámetros que devuelve el  ResulSet
                if (isResultSet) {
                    numeroParametrosResultSet++;
                    parametrosSPResultSet.add(parametro);
                } else { // Este el caso por default*/
                    numeroParametros++;
                    parametrosSP.add(parametro);
                }
                /** *********************************************** */

            }
            rsColumns.close();
            /** ************************************************************************ */
            ///////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////

            LOGGER.debug("parametrosEntradaSP " + parametrosEntradaSP);
            int tamanoParametros = (valoresParametros != null) ? valoresParametros.size() : 0;
            if (tamanoParametros < parametrosEntradaSP) {
                throw SncBusinessServiceExceptions.EXCEPCION_100005.getExcepcion(LOGGER, null,
                    parametrosEntradaSP, tamanoParametros);

            }
            LOGGER.debug("numeroParametros " + numeroParametros);
            LOGGER.debug("numeroParametrosResultSet " + numeroParametrosResultSet);
            ///////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////
            String sql;
            if (tipo.equals(EProcedimientoAlmacenadoFuncionTipo.FUNCION)) {
                sql = "{? = call " + nombreProcedimiento + " (";
            } else {
                sql = "{call " + nombreProcedimiento + " (";
            }
            int numParam = numeroParametros - (tipo.equals(
                EProcedimientoAlmacenadoFuncionTipo.FUNCION) ? 1 : 0);
            for (int i = 0; i < numParam; i++) {
                sql += "?";
                if (i < numParam - 1) {
                    sql += ",";
                }
            }
            sql += ")}";
            LOGGER.debug("sql:" + sql);
            cs = this.dbConn.prepareCall(sql);

            int numeroParametrosEntrada = 0;
            int numeroParametrosSalida = 0;

            for (int j = 0; j < parametrosSP.size(); j++) {
                LOGGER.debug("j: " + j);
                int indexSP = j + 1;
                ParametroSPDto parametroSP = (ParametroSPDto) parametrosSP.get(j);
                LOGGER.debug("parametro: " + parametroSP);
                if (!parametroSP.isInputParameter()) {
                    numeroParametrosSalida++;
                    /** *********************************************** */
                    if (parametroSP.getTypeName().equals("REF CURSOR")) {
                        // Es preciso setea el parámetro con un tipo específico de Oracle
                        LOGGER.debug("Cursor " + indexSP + " ORACLE CURSOR");
                        cs.registerOutParameter(indexSP, OracleTypes.CURSOR);
                    } else {
                        // Caso  por  default
                        LOGGER.debug("Cursor " + indexSP + " " + parametroSP.getDataType());
                        cs.registerOutParameter(indexSP, parametroSP.getDataType());
                    }
                    /** *********************************************** */
                    if (parametroSP.getColumnType() != DatabaseMetaData.procedureColumnInOut) {
                        continue;
                    }
                } else {
                    Object valorParametro = valoresParametros.get(numeroParametrosEntrada);
                    numeroParametrosEntrada++;
                    LOGGER.debug("valorParametro: " + valorParametro);
                    if (valorParametro == null) {
                        cs.setNull(indexSP, parametroSP.getDataType());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.CHAR) {
                        cs.setString(indexSP, (String) valorParametro);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.VARCHAR || parametroSP.getDataType() ==
                        Types.LONGVARCHAR) {
                        cs.setString(indexSP, (String) valorParametro);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.NUMERIC ||
                        parametroSP.getDataType() == Types.DECIMAL) {
                        if (valorParametro.getClass().equals(Long.class)) {
                            BigDecimal valorParametroBigDec = new BigDecimal(
                                ((Long) valorParametro).longValue());
                            cs.setBigDecimal(indexSP, valorParametroBigDec);
                        } else {
                            cs.setBigDecimal(indexSP,
                                (BigDecimal) valorParametro);
                        }
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.INTEGER) {
                        cs.setInt(indexSP, (Integer) valorParametro);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.DATE) {
                        cs.setDate(indexSP, (Date) valorParametro);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.TIME) {
                        cs.setTime(indexSP, (Time) valorParametro);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.TIMESTAMP) {
                        cs.setTimestamp(indexSP, (Timestamp) valorParametro);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.BIT) {
                        cs.setBoolean(indexSP, ((Boolean) valorParametro).booleanValue());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.TINYINT) {
                        cs.setByte(indexSP, ((Byte) valorParametro).byteValue());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.SMALLINT) {
                        cs.setShort(indexSP, ((Short) valorParametro).shortValue());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.BIGINT) {
                        cs.setLong(indexSP, ((Long) valorParametro).longValue());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.REAL) {
                        cs.setFloat(indexSP, ((Float) valorParametro).floatValue());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.FLOAT ||
                        parametroSP.getDataType() == Types.DOUBLE) {
                        cs.setDouble(indexSP, ((Double) valorParametro).doubleValue());
                        continue;
                    }

                    if (parametroSP.getDataType() == Types.BOOLEAN) {
                        cs.setBoolean(indexSP, ((Boolean) valorParametro).booleanValue());
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.ARRAY || parametroSP.getTypeName().equals("VARRAY")) {
                        List<Object> lisParam = (List<Object>) valorParametro;
                        Object[] itemsArray = new Long[lisParam.size()];
                        itemsArray = lisParam.toArray(itemsArray);

                        OracleConnection oracleConnection = this.dbConn.unwrap(OracleConnection.class);

                        Array reportsArray = oracleConnection.createARRAY("NUMBER_ARRAY", itemsArray);

                        cs.setArray(indexSP, reportsArray);
                        continue;
                    }
                    if (parametroSP.getDataType() == Types.OTHER) {
                        cs.setObject(indexSP, valorParametro);
                        continue;
                    }


                }
            }

            LOGGER.debug("numeroParametrosEntrada " + numeroParametrosEntrada);
            LOGGER.debug("ejecutando stored procedure en oracle...");
            LOGGER.debug("numeroParametrosSalida " + numeroParametrosSalida);
            cs.execute();
            Object retorno[] = new Object[numeroParametrosSalida];

            int posicion = 0;
            for (int i = 1; i <= parametrosSP.size(); i++) {
                ParametroSPDto parametro = (ParametroSPDto) parametrosSP.get(i - 1);
                // Lee los de salida
                if (parametro.getColumnType() == DatabaseMetaData.procedureColumnOut ||
                    parametro.getColumnType() == DatabaseMetaData.procedureColumnInOut ||
                    parametro.getColumnType() == DatabaseMetaData.procedureColumnReturn) {

                    if (parametro.getDataType() == Types.CHAR ||
                        parametro.getDataType() == Types.VARCHAR ||
                        parametro.getDataType() == Types.LONGVARCHAR) {
                        retorno[posicion] = cs.getString(i);
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.NUMERIC ||
                        parametro.getDataType() == Types.DECIMAL) {
                        retorno[posicion] = cs.getBigDecimal(i);
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.INTEGER) {
                        retorno[posicion] = new Integer(cs.getInt(i));
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.DATE) {
                        retorno[posicion] = cs.getDate(i);
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.TIME) {
                        retorno[posicion] = cs.getTime(i);
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.TIMESTAMP) {
                        retorno[posicion] = cs.getTimestamp(i);
                        posicion++;
                        continue;
                    }

                    if (parametro.getDataType() == Types.BIT) {
                        retorno[posicion] = new Boolean(cs.getBoolean(i));
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.TINYINT) {
                        retorno[posicion] = new Byte(cs.getByte(i));
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.SMALLINT) {
                        retorno[posicion] = new Short(cs.getShort(i));
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.BIGINT) {
                        retorno[posicion] = new Long(cs.getLong(i));
                        posicion++;
                        continue;
                    }
                    if (parametro.getDataType() == Types.REAL) {
                        retorno[posicion] = new Long(cs.getLong(i));
                        continue;
                    }
                    if (parametro.getDataType() == Types.FLOAT ||
                        parametro.getDataType() == Types.DOUBLE) {
                        retorno[posicion] = new Double(cs.getDouble(i));
                        posicion++;
                        continue;
                    }

                    if (parametro.getDataType() == Types.BOOLEAN) {
                        retorno[posicion] = new Boolean(cs.getBoolean(i));
                        posicion++;
                        continue;
                    }

                    /** *********************************************** */
                    if (parametro.getDataType() == Types.OTHER) {
                        LOGGER.debug("OTHER ");
                        if (parametro.getTypeName().equals("REF CURSOR")) {
                            LOGGER.debug("REF CURSOR ");
                            try {
                                ResultSet rsTemp = (ResultSet) cs.getObject(i);
                                // ResultSet rsTemp= cs.getResultSet();

                                if (rsTemp != null) {
                                    numeroParametrosResultSet = (numeroParametrosResultSet == 0) ?
                                        rsTemp.getMetaData().getColumnCount() :
                                        numeroParametrosResultSet;
                                    retorno[posicion] = getResultSet(rsTemp,
                                        numeroParametrosResultSet);
                                } else {
                                    retorno[posicion] = rsTemp;
                                }
                            } catch (SQLException ex) {
                                if (ex.getMessage().equals("Cursor is closed.")) {
                                    retorno[posicion] = null;
                                } else {
                                    throw ex;
                                }
                            }
                            posicion++;
                            continue;
                        } else {
                            retorno[posicion] = cs.getObject(i);
                            LOGGER.debug("Object ");
                            posicion++;
                            continue;
                        }
                    }
                }
            }
            return retorno;
        } catch (SQLException ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, ex, ex.
                getMessage());
        } finally {
            if (cs != null) {
                try {
                    LOGGER.debug("Cerrando Cursor");
                    cs.close();
                } catch (SQLException e) {
                    throw SncBusinessServiceExceptions.EXCEPCION_010002.getExcepcion(LOGGER, e);
                }
            }
            if (this.dbConn != null) {
                try {
                    LOGGER.debug("Cerrando Conexión");
                    this.dbConn.close();
                } catch (SQLException e) {
                    throw SncBusinessServiceExceptions.EXCEPCION_010002.getExcepcion(LOGGER, e);
                }
            }
        }
        // return null;
    }

    /**
     * Método utilizado cuando una función o procedimiento devuelven en un parámetro de salida un
     * Cursor
     *
     * @param result
     * @param numeroParametrosResultSet
     * @return
     * @throws SQLException
     */
    private List<Object> getResultSet(ResultSet result,
        int numeroParametrosResultSet) throws SQLException {
        LOGGER.debug("getResultSet");
        List<Object> lista = new ArrayList<Object>();
        ResultSetMetaData rsmd = result.getMetaData();

        int numeroColumnas = rsmd.getColumnCount();
        while (result.next()) {
            Object[] datos = new Object[numeroParametrosResultSet];
            for (int j = 1; j <= numeroColumnas; j++) {

                int type = rsmd.getColumnType(j);
                // String nombreColumna = rsmd.getColumnName(j);

                if (type == Types.CHAR || type == Types.VARCHAR ||
                    type == Types.LONGVARCHAR) {
                    datos[j - 1] = result.getString(j);
                } else if (type == Types.NUMERIC || type == Types.DECIMAL) {
                    datos[j - 1] = result.getBigDecimal(j);
                } else if (type == Types.FLOAT || type == Types.DOUBLE) {
                    datos[j - 1] = new Double(result.getDouble(j));
                } else if (type == Types.BIGINT || type == Types.REAL) {
                    datos[j - 1] = new Long(result.getLong(j));
                } else if (type == Types.INTEGER) {
                    datos[j - 1] = new Integer(result.getInt(j));
                } else if (type == Types.DATE) {
                    datos[j - 1] = result.getDate(j);
                } else if (type == Types.TIME) {
                    datos[j - 1] = result.getTime(j);
                } else if (type == Types.TIMESTAMP) {
                    datos[j - 1] = result.getTimestamp(j);
                } else if (type == Types.BIT) {
                    datos[j - 1] = new Boolean(result.getBoolean(j));
                } else if (type == Types.TINYINT) {
                    datos[j - 1] = new Byte(result.getByte(j));
                } else if (type == Types.SMALLINT) {
                    datos[j - 1] = new Short(result.getShort(j));
                }

            }
            lista.add(datos);
        }
        result.close();
        return lista;
    }

}
