/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author felipe.cadena
 */
@Local
public interface IPredioZonaDAO extends IGenericJpaDAO<PredioZona, Long> {

    /**
     * Método que busca las zonas homogéneas de un {@link Predio} por su id. Solo retorna las zonas
     * de la vigencia mas reciente
     *
     * @author felipe.cadena
     * @param idPredio
     * @return
     */
    public List<PredioZona> buscarPorPPredioId(Long idPredio);

    /**
     * Obtiene las zonas correspondientes a un predio
     *
     * @author leidy.gonzalez
     * @param predioId
     * @return
     */
    public List<PredioZona> obtenerZonasPorIdPredio(Long predioId);
    
    /**
     * Método para consultar lista PredioZona por sus id's
     *
     * @author leidy.gonzalez
     * @param List<Long>id
     * @return
     */
    public List<PredioZona> obtenerZonasPorIds(List<Long> id);

}
