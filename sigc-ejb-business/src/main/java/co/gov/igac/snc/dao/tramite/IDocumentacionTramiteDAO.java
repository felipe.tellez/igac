package co.gov.igac.snc.dao.tramite;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;

public interface IDocumentacionTramiteDAO extends
    IGenericJpaDAO<TramiteDocumentacion, Long> {

    /**
     * @author juan.agudelo
     * @param numeroRadicacion
     * @return
     */
    public List<TramiteDocumentacion> buscarDocumentosTramitePorNumeroDeRadicacion(
        String numeroRadicacion);

    /**
     * Permite ingresar una lista de documentos adicionales solicitados
     *
     * @author juan.agudelo
     * @param documentosAdicionales
     */
    public void ingresarDocumentosAdicionalesRequeridos(
        List<TramiteDocumentacion> documentosAdicionales);

}
