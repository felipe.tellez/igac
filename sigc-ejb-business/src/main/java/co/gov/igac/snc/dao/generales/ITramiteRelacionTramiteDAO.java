package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRelacionTramite;

@Local
public interface ITramiteRelacionTramiteDAO extends IGenericJpaDAO<TramiteRelacionTramite, Long> {

    /**
     * Retorna el listado de tipos de documentos que son requeridos para un tipo de tramite, clase
     * de mutacion y subtipo
     *
     * @param tramiteId
     * @return
     * @author javier.aponte
     */
    public List<TramiteRelacionTramite> findTramiteRelacionTramiteByIdTramite(Long tramiteId);

}
