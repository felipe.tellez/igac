package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IFotografiaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;

@Stateless
public class FotografiaDAOBean extends GenericDAOWithJPA<Fotografia, Long>
    implements IFotografiaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FotografiaDAOBean.class);

}
