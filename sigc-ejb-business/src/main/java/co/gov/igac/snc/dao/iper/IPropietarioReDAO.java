/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.iper;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.i11n.PropietarioRe;

/**
 * Interfaz para IPER (Interrelación Permanente)
 *
 * @author fredy.wilches
 * @version 2.0
 */
@Local
public interface IPropietarioReDAO extends IGenericJpaDAO<PropietarioRe, Long> {

}
