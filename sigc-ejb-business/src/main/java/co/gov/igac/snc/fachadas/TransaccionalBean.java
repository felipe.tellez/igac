package co.gov.igac.snc.fachadas;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.actualizacion.IMunicipioCierreVigenciaDAO;
import co.gov.igac.snc.dao.conservacion.IColindanteDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IPlantillaSeccionReporteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Colindante;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 * Clase que implementa los métodos de {@link ITransactional} para el uso de transacciones
 * independientes.
 *
 * @author david.cifuentes
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
//@Interceptors(TimeInterceptor.class)
public class TransaccionalBean implements ITransaccional, ITransaccionalLocal {

    /**
     * Interfaces Locales de Servicio
     */
    @EJB
    private ITramiteDAO tramiteDao;

    @EJB
    private IMunicipioCierreVigenciaDAO municipioCierreVigenciaDao;

    @EJB
    private IColindanteDAO colindanteDao;

    @EJB
    private IPredioDAO predioDao;

    @EJB
    private ISolicitudDAO solicitudDao;

    @EJB
    private IPlantillaSeccionReporteDAO plantillaSeccionReporteDAO;

    @Resource
    private SessionContext context;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteBean.class);

    // ------------------------------------------------------------------- //
    /**
     * Método que actualiza un trámite iniciando una nueva transacción y realizando un commit de
     * ella.
     *
     * @author david.cifuentes
     *
     * @see ITransaccional#actualizarTramiteNuevaTransaccion(Tramite)
     */
    @Override
    public Tramite actualizarTramiteNuevaTransaccion(Tramite tramite) {
        try {
            try {

                this.context.getUserTransaction().begin();
                tramite = this.tramiteDao.update(tramite);
                this.context.getUserTransaction().commit();

            } catch (SecurityException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            } catch (RollbackException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            } catch (HeuristicMixedException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            } catch (HeuristicRollbackException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            } catch (IllegalStateException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            } catch (NotSupportedException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            } catch (SystemException e) {
                LOGGER.error("Error en el método ITransaccional#actualizarTramiteNuevaTransaccion" +
                    e.getMessage());
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return tramite;
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que realiza el llamado a método del mismo nombre en IMunicipioCierreVigenciaDAO
     * iniciando una nueva transacción y realizando un commit de ella.
     *
     * @author david.cifuentes
     *
     * @see ITransaccional#validarExistenciaMunicipiosPorDecretoNuevaTransaccion(
     * DecretoAvaluoAnual, UsuarioDTO usuario)
     */
    @Override
    public void validarExistenciaMunicipiosPorDecretoNuevaTransaccion(
        DecretoAvaluoAnual decretoAvaluoAnual, UsuarioDTO usuario)
        throws ExcepcionSNC {
        try {
            this.context.getUserTransaction().begin();
            this.municipioCierreVigenciaDao
                .validarExistenciaMunicipiosPorDecreto(decretoAvaluoAnual,
                    usuario);
            this.context.getUserTransaction().commit();

        } catch (ExcepcionSNC e) {
            LOGGER.debug(
                "ERROR: TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion");
            throw (e);
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#validarExistenciaMunicipiosPorDecretoNuevaTransaccion" +
                e.getMessage());
        }
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que busca los predios colindantes dado un número predial iniciando una nueva
     * transacción.
     *
     * @author david.cifuentes
     *
     * @see ITransaccional#findPrediosColindantesByNumeroPredialNuevaTransaccion(String)
     */
    @Override
    public List<String> findPrediosColindantesByNumeroPredialNuevaTransaccion(
        String numeroPredial) {

        List<String> numerosPredialesColindantes = null;
        try {
            this.context.getUserTransaction().begin();
            numerosPredialesColindantes = this.colindanteDao
                .findPrediosColindantesByNumeroPredial(numeroPredial);;
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug(
                "ERROR: TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion");
            throw (e);
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPrediosColindantesByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        }
        return numerosPredialesColindantes;
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que realiza la búsqueda de una lista de {@link Predio} dada una lista de números
     * prediales y manejando una nueva transacción.
     *
     * @author david.cifuentes
     *
     * @see ITransaccional#getPrediosByNumerosPredialesNuevaTransaccion(List<String>)
     */
    @Override
    public List<Predio> getPrediosByNumerosPredialesNuevaTransaccion(
        List<String> numerosPrediales) {

        List<Predio> prediosByNumerosPrediales = null;
        try {
            this.context.getUserTransaction().begin();
            prediosByNumerosPrediales = this.predioDao.
                getPrediosByNumerosPrediales(numerosPrediales);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion");
            throw (e);
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#getPrediosByNumerosPredialesNuevaTransaccion" +
                e.getMessage());
        }
        return prediosByNumerosPrediales;
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que actualiza un trámite iniciando una nueva transacción y realizando un commit de
     * ella.
     *
     * @author david.cifuentes
     *
     * @see ITransaccional#buscarColindantesNumeroPredialNuevaTransaccion(String)
     */
    @Override
    public List<Colindante> buscarColindantesNumeroPredialNuevaTransaccion(
        String numPredial) {

        List<Colindante> colindantesNumeroPredial = null;
        try {
            this.context.getUserTransaction().begin();
            colindantesNumeroPredial = this.colindanteDao.buscarColindantesNumeroPredial(numPredial);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion");
            throw (e);
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarColindantesNumeroPredialNuevaTransaccion" +
                e.getMessage());
        }
        return colindantesNumeroPredial;
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que realiza una eliminación multiple de {@link Colindante} iniciando una nueva
     * transacción y realizando un commit de ella.
     *
     * @author david.cifuentes
     *
     * @see ITransaccional#deleteMultipleColindantesNuevaTransaccion(List<Colindante>)
     */
    @Override
    public void deleteMultipleColindantesNuevaTransaccion(List<Colindante> colAnteriores) {
        try {
            this.context.getUserTransaction().begin();
            this.colindanteDao.deleteMultiple(colAnteriores);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#deleteMultipleColindantesNuevaTransaccion");
            throw (e);
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#deleteMultipleColindantesNuevaTransaccion" +
                e.getMessage());
        }
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que realiza una eliminación múltiple de {@link Colindante} dada una lista de objetos y
     * creando una transacción independiente
     *
     * @author david.cifuentes
     * @see ITransaccional#updateColindanteNuevaTransaccion(Colindante)
     */
    @Override
    public void updateColindanteNuevaTransaccion(Colindante colindante) {
        try {
            this.context.getUserTransaction().begin();
            this.colindanteDao.update(colindante);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#updateColindanteNuevaTransaccion");
            throw (e);
        } catch (IllegalStateException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateColindanteNuevaTransaccion" +
                e.getMessage());
        }
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que realiza el llamado a método getPredioByNumeroPredial en IPredioDAO iniciando una
     * nueva transacción.
     *
     * @author david.cifuentes
     * @see ITransaccional#findPredioByNumeroPredialNuevaTransaccion(numeroPredial)
     */
    @Override
    public Predio findPredioByNumeroPredialNuevaTransaccion(String numeroPredial) {
        Predio predio = null;
        try {
            this.context.getUserTransaction().begin();
            predio = this.predioDao.getPredioByNumeroPredial(numeroPredial);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion");
            throw (e);
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findPredioByNumeroPredialNuevaTransaccion" +
                e.getMessage());
        }
        return predio;
    }

    // ------------------------------------------------------------------- //
    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite creando
     * una nueva transacción para la consulta.
     *
     * @author david.cifuentes
     * @see ITransaccional#findTramitePruebasByTramiteIdNuevaTransaccion(tramiteId)
     */
    @Override
    public Tramite findTramitePruebasByTramiteIdNuevaTransaccion(Long tramiteId) {
        Tramite tramite = null;
        try {
            this.context.getUserTransaction().begin();
            tramite = this.tramiteDao.findTramitePruebasByTramiteId(tramiteId);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion");
            throw (e);
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#findTramitePruebasByTramiteIdNuevaTransaccion" +
                e.getMessage());
        }
        return tramite;
    }

    // ------------------------------------------------------------------- //
    /**
     * Retorna la solicitud si existe a partir del id de la misma creando una nueva transacción para
     * la consulta.
     *
     * @author david.cifuentes
     * @see ITransaccional#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion(idSolicitud)
     */
    @Override
    public Solicitud buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion(Long idSolicitud) {
        Solicitud solicitud = null;
        try {
            this.context.getUserTransaction().begin();
            solicitud = this.solicitudDao.buscarSolicitudFetchTramitesBySolicitudId(idSolicitud);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug(
                "ERROR: TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion");
            throw e;
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion" +
                e.getMessage());
        }
        return solicitud;
    }

    // ------------------------------------------------------------------- //
    /**
     * Actualiza la solicitud creando una nueva transacción.
     *
     * @author david.cifuentes
     * @see ITransaccional#updateSolicitudNuevaTransaccion(solicitud)
     */
    @Override
    public Solicitud updateSolicitudNuevaTransaccion(Solicitud solicitud) {
        Solicitud answer = null;
        try {
            this.context.getUserTransaction().begin();
            answer = this.solicitudDao.update(solicitud);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug("ERROR: TransaccionalBean#updateSolicitudNuevaTransaccion");
            throw e;
        } catch (IllegalStateException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error("Error en el método TransaccionalBean#updateSolicitudNuevaTransaccion" +
                e.getMessage());
        }
        return answer;
    }

    @Override
    public void actualizarPlantillaSeccionReportesNuevaTransaccion(
        List<PlantillaSeccionReporte> plantillas) {

        try {
            this.context.getUserTransaction().begin();
            this.plantillaSeccionReporteDAO.updateMultiple(plantillas);
            this.context.getUserTransaction().commit();
        } catch (ExcepcionSNC e) {
            LOGGER.debug(
                "ERROR: TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion");
            throw e;
        } catch (IllegalStateException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        } catch (NotSupportedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        } catch (SystemException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        } catch (RollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicMixedException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        } catch (HeuristicRollbackException e) {
            LOGGER.error(
                "Error en el método TransaccionalBean#actualizarPlantillaSeccionReportesNuevaTransaccion" +
                e.getMessage());
        }
    }

    // end of class
}
