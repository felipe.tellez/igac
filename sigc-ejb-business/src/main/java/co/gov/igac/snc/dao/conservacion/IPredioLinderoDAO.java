/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioLindero;
import javax.ejb.Local;

/**
 *
 * @author leidy.gonzalez
 */
@Local
public interface IPredioLinderoDAO extends IGenericJpaDAO<PredioLindero, Long> {

    /**
     * Método que cuenta la cantidad de linderos que existen por numeroPredial.
     *
     * @author leidy.gonzalez
     * @param numeroPredial
     * @return
     */
    public Integer contarLinderosPorNumeroPredial(String numeroPredial);

}
