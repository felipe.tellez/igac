package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import java.util.List;

/**
 *
 * @author fabio.navarrete
 *
 */
@Local
public interface IPUnidadConstruccionCompDAO extends
    IGenericJpaDAO<PUnidadConstruccionComp, Long> {

    /**
     * Actualiza los datos de la tabla para la unidad de construcción que viene como atributo de los
     * objetos de la lista de objetos a insertar. La actualización consiste en borrar todos los
     * registros anteriores que estén relacionados con esa unidad de construcción e insertar los
     * nuevos. Se borran, y no se actualizan, porque es más fácil debido a la forma como se arma el
     * árbol de componentes-elementos-detalles
     *
     * @author pedro.garcia
     * @param unidadesConstruccionComp
     */
    public void updateByUnidadConstruccionId(
        List<PUnidadConstruccionComp> unidadesConstruccionComp);

    /**
     * Elimina los registros de la tabla que correspondan a la unidad de construcción con el id
     * suministrado
     *
     * @param unidadConstruccionId
     * @author pedro.garcia
     */
    public void deleteByUnidadConstruccionId(Long unidadConstruccionId);

    /**
     * Método que retorna las p unidades de construccion comp asociadas a una unidad de construccion
     *
     * @author leidy.gonzalez
     * @param unidadConstruccionId
     * @return
     */
    public List<PUnidadConstruccionComp> buscarUnidadDeConstruccionPorUnidadConstruccionId(
        Long unidadConstruccionId);

}
