package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IGeneracionFormularioSbcDAO;
import co.gov.igac.snc.dao.actualizacion.IPredioFormularioSbcDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.GeneracionFormularioSbc;
import co.gov.igac.snc.persistence.entity.actualizacion.PredioFormularioSbc;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto GeneracionFormularioSbc.
 *
 * @author javier.barajas
 */
@Stateless
public class PredioFormularioSbcDAOBean extends GenericDAOWithJPA<PredioFormularioSbc, Long>
    implements IPredioFormularioSbcDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioFormularioSbcDAOBean.class);
}
