package co.gov.igac.snc.dao.generales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Jurisdiccion;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class JurisdiccionDAOBean extends
    GenericDAOWithJPA<Jurisdiccion, String> implements IJurisdiccionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(JurisdiccionDAOBean.class);

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IJurisdiccionDAO#findMunicipiosbyCodigoEstructuraOrganizacional(String)
     * @author juan.agudelo
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Municipio> findMunicipiosbyCodigoEstructuraOrganizacional(
        String codigoEO) {

        LOGGER.debug("JurisdiccionDAOBean#findMunicipiosbyCodigoEstructuraOrganizacional");
        List<Municipio> answer = null;

        String query;

        query = "SELECT DISTINCT j.municipio FROM Jurisdiccion j" +
            " WHERE  j.estructuraOrganizacional.codigo = :codigoEstructuraOrganizacional";

        Query q = entityManager.createQuery(query);
        q.setParameter("codigoEstructuraOrganizacional", codigoEO);

        try {
            answer = (List<Municipio>) q.getResultList();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see
     * IJurisdiccionDAO#findMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional(String,
     * List)
     * @author juan.agudelo
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Municipio> findMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional(
        String departamentoCodigo,
        List<String> codigosEstructuraOrganizacionalList) {

        LOGGER.debug(
            "JurisdiccionDAOBean#findMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional");
        List<Municipio> answer = null;

        String query;

        StringBuilder codigosEstructuraOrganizacional = new StringBuilder();
        String codigosEO;

        for (String ceoTemp : codigosEstructuraOrganizacionalList) {
            codigosEstructuraOrganizacional.append("'");
            codigosEstructuraOrganizacional.append(ceoTemp);
            codigosEstructuraOrganizacional.append("'");
            codigosEstructuraOrganizacional.append(",");
        }

        codigosEO = codigosEstructuraOrganizacional.toString();
        codigosEO = codigosEO.substring(0, codigosEO.length() - 1);

        query = "SELECT DISTINCT j.municipio FROM Jurisdiccion j" +
            " WHERE  j.estructuraOrganizacional.codigo IN (" + codigosEO +
            ")" + " AND j.municipio.codigo like :departamentoCodigo";

        Query q = entityManager.createQuery(query);
        q.setParameter("departamentoCodigo", departamentoCodigo + "%");

        try {
            answer = (List<Municipio>) q.getResultList();
            LOGGER.debug("Tamaño " + answer.size());
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IJurisdiccionDAO#getCodigoDepartamentoByCodigoEstructuraOrganizacional (List<String>)
     * @author juan.agudelo
     */
    @Override
    public List<String> getCodigoDepartamentoByCodigoEstructuraOrganizacional(
        List<String> codigoEO) {

        LOGGER.debug("JurisdiccionDAOBean#getCodigoDepartamentoByCodigoEstructuraOrganizacional");
        List<String> answer = new ArrayList<String>();

        String query;

        query = "SELECT DISTINCT j.municipio FROM Jurisdiccion j" +
            " WHERE  j.estructuraOrganizacional.codigo IN :codigoEstructuraOrganizacional";

        Query q = entityManager.createQuery(query);
        q.setParameter("codigoEstructuraOrganizacional", codigoEO);

        try {
            @SuppressWarnings("unchecked")
            List<Municipio> munTemp = (List<Municipio>) q.getResultList();

            for (Municipio mTemp : munTemp) {
                if (!answer.contains(mTemp.getCodigo().substring(0, 2))) {
                    answer.add(mTemp.getCodigo().substring(0, 2));
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    /*
     * (non-Javadoc) @see
     * co.gov.igac.persistence.entity.generales.IJurisdiccionDAO#getEstructuraOrganizacionalIdByMunicipioCod(java.lang.String)
     */
    @Override
    public String getEstructuraOrganizacionalIdByMunicipioCod(String municipioCod) {
        LOGGER.debug("JurisdiccionDAOBean#getEstructuraOrganizacionalIdByMunicipioCod");
        String eoNombre = null;
        String query = "SELECT j.estructuraOrganizacional.codigo" +
            " FROM Jurisdiccion j " +
            " WHERE j.municipio.codigo = :municipioCod";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("municipioCod", municipioCod);

            eoNombre = (String) q.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return eoNombre;
    }

    /**
     * @see IJurisdiccionDAO#getEstructuraOrganizacionalByMunicipioCod(String)
     * @author christian.rodriguez
     */
    @Override
    public EstructuraOrganizacional getEstructuraOrganizacionalByMunicipioCod(
        String municipioCod) {

        LOGGER.debug("JurisdiccionDAOBean#getEstructuraOrganizacionalByMunicipioCod");

        EstructuraOrganizacional estructuraOrganizacional = null;

        String query = "SELECT j.estructuraOrganizacional" +
            " FROM Jurisdiccion j " +
            " WHERE j.municipio.codigo = :municipioCod";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("municipioCod", municipioCod);

            estructuraOrganizacional = (EstructuraOrganizacional) q
                .getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }
        return estructuraOrganizacional;
    }

    /*
     * (non-Javadoc) @see
     * co.gov.igac.igac.dao.generales.IJurisdiccionDAO#getMunicipioCodsByEstructuraOrganizacionalCod(java.lang.String)
     */
    @Override
    public List<String> getMunicipioCodsByEstructuraOrganizacionalCod(
        String estructuraOrgcod) {
        LOGGER.debug("JurisdiccionDAOBean#getEstructuraOrganizacionalIdByMunicipioCod");

        List<String> codigos = null;

        String query = "SELECT j.municipio.codigo " +
            " FROM Jurisdiccion j " +
            " WHERE j.estructuraOrganizacional.codigo =:estructuraOrgcod";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("estructuraOrgcod", estructuraOrgcod);

            codigos = (List<String>) q.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return codigos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IJurisdiccionDAO#obtenerCodigoEstructuraOrganizacionalPorMunicipio(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String obtenerCodigoEstructuraOrganizacionalPorMunicipio(String codigoMunicipio) {

        String answer, queryString;
        Query query;
        EstructuraOrganizacional eo;

        answer = "";

        queryString = "" +
            "SELECT j.estructuraOrganizacional FROM Jurisdiccion j " +
            "WHERE j.municipio.codigo = :codMunicipio_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("codMunicipio_p", codigoMunicipio);
            eo = (EstructuraOrganizacional) query.getSingleResult();
            if (eo != null) {
                answer = eo.getCodigo();
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "JurisdiccionDAOBean#obtenerCodigoEstructuraOrganizacionalPorMunicipio");
        }

        return answer;
    }

//end of class    
}
