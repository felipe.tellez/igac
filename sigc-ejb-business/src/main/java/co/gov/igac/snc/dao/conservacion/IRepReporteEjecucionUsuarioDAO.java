/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los metodos de acceso a datos de la entidad RepReporteEjecucionUsuario
 *
 * @author felipe.cadena
 */
@Local
public interface IRepReporteEjecucionUsuarioDAO extends
    IGenericJpaDAO<RepReporteEjecucionUsuario, Long> {

    /**
     * Obtiene todas las subscriciones de un usuario a difeentes reportes
     *
     * @author felipe.cadena
     *
     * @param usuario
     * @return
     */
    public List<RepReporteEjecucionUsuario> obtenerPorUsuario(String usuario);

    /**
     * Obtiene todas las subscriciones de un usuario a diferentes reportes
     *
     * @author leidy.gonzalez
     *
     * @param usuario
     * @return
     */
    public List<RepReporteEjecucionUsuario> obtenerPorUsuarioIdReporte(String usuario,
        Long idReporte);

}
