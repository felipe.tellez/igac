/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.conservacion.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.generales.IEstructuraOrganizacionalDAO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.business.conservacion.IConsultaPredio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 * @modified juan.agudelo
 */
@Stateless
public class ConsultaPredioBean implements IConsultaPredio, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2206531821351787L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaPredioBean.class);

    /**
     * Interfaces locales de servicio
     */
    private IEstructuraOrganizacionalDAO estructOrgService;
    private IPredioDAO predioService;

    @EJB
    private IFichaMatrizPredioDAO fichaMatrizPredioService;

    @EJB
    private IDepartamentoDAO departamentoService;
    @EJB
    private IMunicipioDAO municipioService;
    @EJB
    private IDominioDAO dominioService;

    @EJB
    public void setEstructOrgService(IEstructuraOrganizacionalDAO service) {
        LOGGER.debug("inyección del ejb EstructuraOrganizacionalDAO");
        this.estructOrgService = service;
    }

    @EJB
    public void setPredioService(IPredioDAO service) {
        LOGGER.debug("inyección del ejb IMunicipioDAO");
        this.predioService = service;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConsultaPredioLocal#getTerritorialesDTOList()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<EstructuraOrganizacional> getTerritorialesList() {

        List<EstructuraOrganizacional> answer;
        answer = this.estructOrgService.findTerritorialesAll();
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConsultaPredioLocal#getCacheDepartamentosPorTerritorial(java.lang.String)
     */
    @Override
    public List<Departamento> getDepartamentosList(String territorialCode) {

        List<Departamento> answer;
        answer = this.departamentoService.findByEntidadTerritorial(territorialCode);
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConsultaPredioLocal#getMunicipiosList(java.lang.String)
     */
    @Override
    public List<Municipio> getMunicipiosList(String departamentoCode) {

        List<Municipio> answer;
        answer = this.municipioService.findByDepartamentoNQ(departamentoCode);
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConsultaPredio#getPredioFetchDerechosPropiedadByNumeroPredial(String)
     * @author fabio.navarrete
     */
    @Implement
    @Override
    public Predio getPredioFetchDerechosPropiedadByNumeroPredial(String numPredial) {

        Predio predio;

        try {
            predio = predioService
                .getPredioFetchDerechosPropiedadByNumeroPredial(numPredial);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }
        if (predio != null) {
            List<PersonaPredio> personaPredios = predio.getPersonaPredios();
            for (int i = 0; i < personaPredios.size(); i++) {
                List<PersonaPredioPropiedad> personaPredioPropiedads = personaPredios
                    .get(i).getPersonaPredioPropiedads();
                for (int j = 0; j < personaPredioPropiedads.size(); j++) {
                    PersonaPredioPropiedad ppp = personaPredioPropiedads.get(j);
                    String entidEmis = ppp.getEntidadEmisora();
                    Dominio dom = new Dominio();
                    if (entidEmis != null && !entidEmis.equals("")) {
                        dom = dominioService.findByCodigo(
                            EDominio.DERECHO_PROPIEDAD_ENTIDAD_E, entidEmis);
                        if (dom != null) {
                            ppp.setEntidadEmisoraValor(dom.getValor());
                        }
                    }
                    String modoAdquis = ppp.getModoAdquisicion();
                    if (modoAdquis != null && !modoAdquis.equals("")) {
                        dom = dominioService.findByCodigo(
                            EDominio.DERECHO_PROPIEDAD_MODO_ADQU, modoAdquis);
                        if (dom != null) {
                            ppp.setModoAdquisicionValor(dom.getValor());
                        }
                    }
                    String tipoTit = ppp.getTipoTitulo();
                    if (tipoTit != null && !tipoTit.equals("")) {
                        dom = dominioService.findByCodigo(
                            EDominio.DERECHO_PROPIEDAD_TIPO_TITU, tipoTit);
                        if (dom != null) {
                            ppp.setTipoTituloValor(dom.getValor());
                        }
                    }

                    personaPredioPropiedads.remove(j);
                    personaPredioPropiedads.add(j, ppp);
                }
                personaPredios.get(i).setPersonaPredioPropiedads(personaPredioPropiedads);
            }
            predio.setPersonaPredios(personaPredios);
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#getPredioFetchPersonasByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchPersonasByNumeroPredial(String numPredial) {
        LOGGER.info("ConsultaPredioBean#getPredioFetchPersonasByNumeroPredial");
        Predio predio = predioService
            .getPredioFetchPersonasByNumeroPredial(numPredial);
        if (predio != null) {
            for (int i = 0; i < predio.getPersonaPredios().size(); i++) {
                String tipoId = predio.getPersonaPredios().get(i).getPersona()
                    .getTipoIdentificacion();
                if (tipoId != null && !tipoId.equals("")) {
                    Dominio dom = dominioService.findByCodigo(
                        EDominio.PERSONA_TIPO_IDENTIFICACION, tipoId);
                    if (dom != null) {
                        predio.getPersonaPredios().get(i).getPersona()
                            .setTipoIdentificacionValor(dom.getValor());
                    }
                }
            }
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#getPredioFetchPersonasById(Long)
     */
    @Override
    public Predio getPredioFetchPersonasById(Long predioId) {

        LOGGER.info("ConsultaPredioBean#getPredioFetchPersonasById");

        Predio predio;
        try {
            predio = this.predioService.getPredioFetchPersonasById(predioId);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        if (predio != null) {
            for (int i = 0; i < predio.getPersonaPredios().size(); i++) {
                String tipoId = predio.getPersonaPredios().get(i).getPersona()
                    .getTipoIdentificacion();
                if (tipoId != null && !tipoId.equals("")) {
                    Dominio dom = this.dominioService.findByCodigo(
                        EDominio.PERSONA_TIPO_IDENTIFICACION, tipoId);
                    if (dom != null) {
                        predio.getPersonaPredios().get(i).getPersona()
                            .setTipoIdentificacionValor(dom.getValor());
                    }
                }
            }
        }
        return predio;
    }

    /**
     * @see IConsultaPredio#getPredioFetchAvaluosByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchAvaluosByNumeroPredial(String numPredial) {
        LOGGER.info("ConsultaPredioBean#getPredioFetchAvaluosByNumeroPredial");
        Predio predio = predioService
            .getPredioFetchAvaluosByNumeroPredial(numPredial);
        if (predio != null) {
            for (int i = 0; i < predio.getUnidadConstruccions().size(); i++) {
                String tipificacion = predio.getUnidadConstruccions().get(i)
                    .getTipificacion();
                if (tipificacion != null && !tipificacion.equals("")) {
                    Dominio dom = dominioService.findByCodigo(
                        EDominio.UNIDAD_CONSTRUCION_TIPIFICA, tipificacion);
                    if (dom != null) {
                        predio.getUnidadConstruccions().get(i)
                            .setTipificacionValor(dom.getValor());
                    }
                }
            }
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#getPredioDatosFetchPersonasByNumeroPredial(String)
     */
    @Implement
    @Override
    public Predio getPredioDatosFetchPersonasByNumeroPredial(String numPredial) {
        Predio predio = predioService
            .getPredioDatosFetchPersonasByNumeroPredial(numPredial);
        for (int i = 0; i < predio.getPersonaPredios().size(); i++) {
            String tipoId = predio.getPersonaPredios().get(i).getPersona()
                .getTipoIdentificacion();
            if (tipoId != null && !tipoId.equals("")) {
                Dominio dom = dominioService.findByCodigo(
                    EDominio.PERSONA_TIPO_IDENTIFICACION, tipoId);
                if (dom != null) {
                    predio.getPersonaPredios().get(i).getPersona()
                        .setTipoIdentificacionValor(dom.getValor());
                }
            }
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#getPredioFetchDatosUbicacionByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchDatosUbicacionByNumeroPredial(String numPredial) {

        Predio predio = this.predioService
            .getPredioFetchDatosUbicacionByNumeroPredial(numPredial);

        if (predio != null) {
            String domCod = predio.getEstado();
            Dominio dom = new Dominio();
            if (domCod != null && !domCod.equals("")) {
                dom = dominioService.findByCodigo(EDominio.PREDIO_ESTADO,
                    domCod);
                if (dom != null) {
                    predio.setEstadoValor(dom.getValor());
                }
            }

            domCod = predio.getTipo();
            if (domCod != null && !domCod.equals("")) {
                dom = dominioService.findByCodigo(EDominio.PREDIO_TIPO, domCod);
                if (dom != null) {
                    predio.setTipoValor(dom.getValor());
                }
            }

            domCod = predio.getDestino();
            if (domCod != null && !domCod.equals("")) {
                dom = dominioService.findByCodigo(EDominio.PREDIO_DESTINO,
                    domCod);
                if (dom != null) {
                    predio.setDestinoValor(dom.getValor());
                }
            }

            domCod = predio.getZonaUnidadOrganica();
            if (domCod != null && !domCod.equals("")) {
                dom = dominioService.findByCodigo(
                    EDominio.PREDIO_ZONA_UNIDAD_ORGANICA, domCod);
                if (dom != null) {
                    predio.setZonaUnidadOrganicaValor(dom.getValor());
                }
            }
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#getPredioFetchAvaluosByNumeroPredial(String)
     */
    @Override
    public Predio getPredioFetchAvaluosByPredioId(Long predioId) {
        LOGGER.info("ConsultaPredioBean#getPredioFetchAvaluosByPredioId");

        Predio predio = null;

        try {
            predio = this.predioService.getPredioFetchAvaluosByPredioId(predioId);

            if (predio.isEsPredioEnPH()) {
                Double coeficienteCopropiedad = this.fichaMatrizPredioService
                    .getCoeficienteCopropiedadByNumeroPredial(predio
                        .getNumeroPredial());

                predio.setCoeficienteCopropiedad(coeficienteCopropiedad);

            }
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            throw e;
        }

        if (predio != null) {
            for (int i = 0; i < predio.getUnidadConstruccions().size(); i++) {
                String tipificacion = predio.getUnidadConstruccions().get(i)
                    .getTipificacion();
                if (tipificacion != null && !tipificacion.equals("")) {
                    Dominio dom = this.dominioService.findByCodigo(
                        EDominio.UNIDAD_CONSTRUCION_TIPIFICA, tipificacion);
                    if (dom != null) {
                        predio.getUnidadConstruccions().get(i)
                            .setTipificacionValor(dom.getValor());
                    }
                }
            }
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @see IConsultaPredio#findPredioFetchPersonasByPredioId(Long)
     * @modified pedro.garcia organizar ese código paila. manejo de excepciones
     */
    @Override
    public Predio findPredioFetchPersonasByPredioId(Long predioId) {

        LOGGER.debug("on ConsultaPredioBean#findPredioFetchPersonasByPredioId ...");
        Predio predio = null;
        String tipoId;
        Dominio dom;

        try {
            predio = this.predioService.findPredioFetchPersonasByPredioId(predioId);

            if (predio == null) {
                return null;
            }
            for (int i = 0; i < predio.getPersonaPredios().size(); i++) {
                tipoId = predio.getPersonaPredios().get(i).getPersona().getTipoIdentificacion();
                if (tipoId != null && !tipoId.equals("")) {
                    dom = dominioService.findByCodigo(
                        EDominio.PERSONA_TIPO_IDENTIFICACION, tipoId);
                    if (dom != null) {
                        predio.getPersonaPredios().get(i).getPersona()
                            .setTipoIdentificacionValor(dom.getValor());
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "ConsultaPredioBean#findPredioFetchPersonasByPredioId");
        }
        return predio;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#getPredioFetchDatosUbicacionById(Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Predio getPredioFetchDatosUbicacionById(Long predioId) {

        Predio answer = null;
        try {
            answer = this.predioService.getPredioFetchDatosUbicacionById(predioId);

            if (answer != null) {
                String domCod = answer.getEstado();
                Dominio dom = new Dominio();
                if (domCod != null && !domCod.equals("")) {
                    dom = this.dominioService.findByCodigo(EDominio.PREDIO_ESTADO, domCod);
                    if (dom != null) {
                        answer.setEstadoValor(dom.getValor());
                    }
                }

                domCod = answer.getTipo();
                if (domCod != null && !domCod.equals("")) {
                    dom = this.dominioService.findByCodigo(EDominio.PREDIO_TIPO, domCod);
                    if (dom != null) {
                        answer.setTipoValor(dom.getValor());
                    }
                }

                domCod = answer.getDestino();
                if (domCod != null && !domCod.equals("")) {
                    dom = this.dominioService.findByCodigo(EDominio.PREDIO_DESTINO, domCod);
                    if (dom != null) {
                        answer.setDestinoValor(dom.getValor());
                    }
                }

                domCod = answer.getZonaUnidadOrganica();
                if (domCod != null && !domCod.equals("")) {
                    dom = this.dominioService.findByCodigo(
                        EDominio.PREDIO_ZONA_UNIDAD_ORGANICA, domCod);
                    if (dom != null) {
                        answer.setZonaUnidadOrganicaValor(dom.getValor());
                    }
                }
            }

        } catch (Exception ex) {
            LOGGER.error("excepción en ConsultaPredioBean#getPredioFetchDatosUbicacionById: " +
                ex.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IConsultaPredio#consultarDetalleAvaluoByPredioId(Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Predio consultarDetalleAvaluoPredioByPredioId(Long predioId) {

        LOGGER.info("ConsultaPredioBean#getPredioFetchAvaluosByPredioId");

        Predio predio = null;

        try {
            predio = this.predioService.getDetalleAvaluosById(predioId);

            if (predio != null) {
                if (predio.isEsPredioEnPH()) {
                    Double coeficienteCopropiedad = this.fichaMatrizPredioService
                        .getCoeficienteCopropiedadByNumeroPredial(predio.getNumeroPredial());

                    predio.setCoeficienteCopropiedad(coeficienteCopropiedad);
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            throw e;
        }

        if (predio != null) {
            for (int i = 0; i < predio.getUnidadConstruccions().size(); i++) {
                String tipificacion = predio.getUnidadConstruccions().get(i).getTipificacion();
                if (tipificacion != null && !tipificacion.equals("")) {
                    Dominio dom = this.dominioService.findByCodigo(
                        EDominio.UNIDAD_CONSTRUCION_TIPIFICA, tipificacion);
                    if (dom != null) {
                        predio.getUnidadConstruccions().get(i)
                            .setTipificacionValor(dom.getValor());
                    }
                }
            }
        }
        return predio;
    }

//end of class    
}
