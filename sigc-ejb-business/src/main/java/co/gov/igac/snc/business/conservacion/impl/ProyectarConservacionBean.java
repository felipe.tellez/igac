package co.gov.igac.snc.business.conservacion.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.business.conservacion.IProyectarConservacion;
import co.gov.igac.snc.dao.IGenericDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * Esta clase maneja objetos que implementan IProyeccionObject, y no algún pojo específico. Usado
 * desde la proyección de forma genérica y evitar tener una implementación por cada tabla p
 *
 * @author fredy.wilches
 *
 */
@Stateless
public class ProyectarConservacionBean implements IProyectarConservacion {

    @EJB
    private IPPredioDAO ppredioService;
    @EJB
    private IDominioDAO dominioService;
    @EJB
    private IGenericDAO genericDAOService;

    @Override
    public PPredio findPPredioByIdFetchAvaluos(Long idPredio) {
        PPredio ppredio = ppredioService.findPPredioByIdFetchAvaluos(idPredio);
        if (ppredio != null) {
            if (ppredio.getPUnidadConstruccions() != null) {
                for (int i = 0; i < ppredio.getPUnidadConstruccions().size(); i++) {
                    IModeloUnidadConstruccion puc = ppredio
                        .getPUnidadConstruccions().get(i);
                    String auxDom = puc.getCancelaInscribe();
                    Dominio dom = new Dominio();
                    if (auxDom != null && !auxDom.equals("")) {
                        dom = dominioService.findByCodigo(
                            EDominio.PROYECCION_CANCELA_INSCRIBE, auxDom);
                        if (dom != null) {
                            puc.setCancelaInscribeValor(dom.getValor());
                        }
                    }

                    /* auxDom = puc.getTipificacion(); if (auxDom != null && !auxDom.equals("")) {
                     * dom = dominioService.findByCodigo(EDominio.UNIDAD_CONSTRUCION_TIPIFICA,
                     * auxDom); if (dom != null) { puc.setTipificacionValor(dom.getValor()); } } */
                }
            }
        }
        return ppredio;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public IProyeccionObject deleteProyeccion(UsuarioDTO usuario, IProyeccionObject objP) {

        objP.setUsuarioLog(usuario.getLogin());
        objP.setFechaLog(new Date());

        if (objP.getCancelaInscribe() != null &&
            objP.getCancelaInscribe().equals(
                EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
            objP = (IProyeccionObject) this.genericDAOService.findById(
                objP.getClass(), objP.getId());
            this.genericDAOService.delete(objP);
            return null;
        } else {
            objP.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
            if (objP instanceof IInscripcionCatastral) {
                ((IInscripcionCatastral) objP).setFechaInscripcionCatastral(new Date());
            }
            IProyeccionObject objPTmp = (IProyeccionObject) this.genericDAOService.update(objP);
            objP.setId(objPTmp.getId());

            return objP;
        }
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public Object saveProyeccion(UsuarioDTO usuario, IProyeccionObject objP) {
        // si era null pone M
        // si era I se queda I
        // si era C se queda M lo reactiva
        // si era M queda M

        // asignación del usuario y fecha
        objP.setUsuarioLog(usuario.getLogin());
        objP.setFechaLog(new Date());

        if (objP.getCancelaInscribe() == null ||
            objP.getCancelaInscribe().trim().length() == 0 ||
            objP.getCancelaInscribe().equals(
                EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
            objP.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                .getCodigo());
        }
        IProyeccionObject objPTmp = (IProyeccionObject) this.genericDAOService
            .update(objP);

        objP.setId(objPTmp.getId());
        return objP;
    }

}
