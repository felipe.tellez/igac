package co.gov.igac.snc.dao.vistas.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVPredioAvaluoDecretoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.vistas.VPredioAvaluoDecreto;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto {@link VPredioAvaluoDecreto}.
 *
 * @author david.cifuentes
 */
@Stateless
public class VPredioAvaluoDecretoDAOBean extends
    GenericDAOWithJPA<VPredioAvaluoDecreto, Long> implements
    IVPredioAvaluoDecretoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPredioAvaluoDecretoDAOBean.class);

    /**
     * @see IVPredioAvaluoDecretoDAO#buscarAvaluosCatastralesPorIdPPredio(Long pPredioId)
     * @author david.cifuentes
     */
    /*
     * @modified by leidy.gonzalez :: #12528::26/05/2015::CU_187
     */
    @SuppressWarnings("unchecked")
    @Override
    public ArrayList<VPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredio(
        Long pPredioId) {
        ArrayList<VPredioAvaluoDecreto> predioAvaluosCatastrales = null;
        try {

            Query query = this.entityManager
                .createQuery("SELECT vpad FROM VPredioAvaluoDecreto vpad" +
                    " JOIN FETCH vpad.predio p" +
                    " WHERE p.id = :pPredioId " +
                    " ORDER BY vpad.vigencia ASC");

            query.setParameter("pPredioId", pPredioId);
            predioAvaluosCatastrales = (ArrayList<VPredioAvaluoDecreto>) query
                .getResultList();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predioAvaluosCatastrales;
    }
}
