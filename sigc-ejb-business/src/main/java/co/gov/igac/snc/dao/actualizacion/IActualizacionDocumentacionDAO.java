package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumentacion;

/**
 * Servicios de persistencia para el objeto ActualizacionDocumento
 *
 * @author franz.gamba
 */
@Local
public interface IActualizacionDocumentacionDAO extends
    IGenericJpaDAO<ActualizacionDocumentacion, Long> {

}
