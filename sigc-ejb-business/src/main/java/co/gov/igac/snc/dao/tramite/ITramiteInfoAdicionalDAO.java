package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInfoAdicional;
import javax.ejb.Local;

/*
 * Proyecto SNC 2017
 */
/**
 * Interfaz para la pesstencia de la clase {@link TramiteInfoAdicional}
 *
 * @author felipe.cadena
 */
@Local
public interface ITramiteInfoAdicionalDAO extends IGenericJpaDAO<TramiteInfoAdicional, Long> {

}
