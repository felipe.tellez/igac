package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialPredio;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialPredioPK;

/**
 * @author fredy.wilches
 */
@Local
public interface IEstadisticaGerencialPredioDAO extends
    IGenericJpaDAO<EstadisticaGerencialPredio, EstadisticaGerencialPredioPK> {

    public List<Object[]> consultaPorMunicipio(Long anio, String codigoMunicipio, String zona);

    public List<Object[]> consultaPorDepartamento(Long anio, String codigoDepartamento, String zona);

    public List<Object[]> consultaPorPais(Long anio, String zona);

}
