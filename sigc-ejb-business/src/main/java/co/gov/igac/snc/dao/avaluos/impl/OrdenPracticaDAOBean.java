/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOrdenPracticaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implemantación de la interfaz IOrdenPracticaDAO
 *
 * @author pedro.garcia
 */
@Stateless
public class OrdenPracticaDAOBean extends GenericDAOWithJPA<OrdenPractica, Long>
    implements IOrdenPracticaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrdenPracticaDAOBean.class);

    /**
     * @see IOrdenPracticaDAO#consultarPorAvaluoId(Long)
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Override
    public OrdenPractica consultarPorAvaluoId(Long idAvaluo) {

        OrdenPractica resultado = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT DISTINCT op " +
                " FROM AvaluoAsignacionProfesional aap " +
                " INNER JOIN aap.avaluo ava " +
                " INNER JOIN aap.ordenPractica op" +
                " WHERE ava.id = :avaluo " +
                " AND aap.fechaHasta IS NULL ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluo", idAvaluo);

            resultado = (OrdenPractica) query.getSingleResult();

        } catch (Exception e) {
//TODO :: felipe.cadena :: a esta excepción se le pasa com parámetro la clase#metodo. :: pedro.garcia
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

    /**
     * @see IOrdenPracticaDAO#consultarPorProfesionalAvaluosIdYRangoFechas(Long, Date, Date)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<OrdenPractica> consultarPorProfesionalAvaluosIdYRangoFechas(
        Long profesionalId, Date fechaMinima, Date fechaMaxima) {

        List<OrdenPractica> ordenes = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT DISTINCT ap.ordenPractica" +
                " FROM AvaluoAsignacionProfesional ap " +
                " WHERE ap.ordenPractica.fecha <= :fechaMaxima " +
                " AND ap.ordenPractica.fecha >= :fechaMinima " +
                " AND ap.profesionalAvaluo.id = :profesionalId";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("fechaMaxima", fechaMaxima);
            query.setParameter("fechaMinima", fechaMinima);
            query.setParameter("profesionalId", profesionalId);

            ordenes = (List<OrdenPractica>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return ordenes;
    }

    // ------------------------------------------------------------------
    /**
     * @see IOrdenPracticaDAO#consultarPorEstado(String)
     * @author christian.rodriguez
     */
    @Override
    public List<OrdenPractica> consultarPorEstado(String estadoOP) {

        List<OrdenPractica> ordenes = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT op" + " FROM OrdenPractica op " +
                " WHERE op.estado = :estadoOP ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("estadoOP", estadoOP);

            ordenes = (List<OrdenPractica>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return ordenes;
    }

}
