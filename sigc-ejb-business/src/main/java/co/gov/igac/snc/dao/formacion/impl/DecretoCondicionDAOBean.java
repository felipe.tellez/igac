package co.gov.igac.snc.dao.formacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.formacion.IDecretoCondicionDAO;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;

@Stateless
public class DecretoCondicionDAOBean extends
    GenericDAOWithJPA<DecretoCondicion, Long> implements
    IDecretoCondicionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DecretoCondicionDAOBean.class);

}
