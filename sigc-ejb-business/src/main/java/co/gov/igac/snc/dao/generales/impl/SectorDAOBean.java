package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.ISectorDAO;

@Stateless
public class SectorDAOBean extends GenericDAOWithJPA<Sector, String> implements ISectorDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SectorDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<Sector> findByZona(String zonaId) {
        Query query = this.entityManager.createQuery(
            "SELECT DISTINCT m FROM Sector m WHERE m.zona.codigo = :zonaId");
        query.setParameter("zonaId", zonaId);
        return query.getResultList();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IMunicipioDAO#getSectorByCodigo(String)
     */
    public Sector getSectorByCodigo(String codigo) {
        Query q = entityManager.createNamedQuery("sector.findSectorByCodigo");
        q.setParameter("codigo", codigo);
        Sector m = (Sector) q.getSingleResult();
        return m;
    }
}
