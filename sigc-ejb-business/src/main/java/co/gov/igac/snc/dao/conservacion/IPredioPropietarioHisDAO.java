package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioPropietarioHis;

@Local
public interface IPredioPropietarioHisDAO extends IGenericJpaDAO<PredioPropietarioHis, Long> {

    /**
     * Método encargado de buscar el historico de justificaciones de un predio.
     *
     * @param predioId Long asociado a las justificaciones a obtener.
     * @return Retorna la Lista de justificaciones historicas asociadas a un predio
     */
    public List<PredioPropietarioHis> getPredioPropietarioHisByPredio(Long predioId);
}
