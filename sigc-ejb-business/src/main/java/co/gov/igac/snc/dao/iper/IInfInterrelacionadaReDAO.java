/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.iper;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionadaRe;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;

/**
 * Interfaz para IPER (Interrelación Permanente)
 *
 * @author fredy.wilches
 * @version 2.0
 */
@Local
public interface IInfInterrelacionadaReDAO extends IGenericJpaDAO<InfInterrelacionadaRe, Long> {

    /**
     * Rtorna lista de trámites según filtro
     *
     * @author fredy.wilches
     *
     */
    public List<InfInterrelacionadaRe> getTramites(FiltroDatosConsultaTramitesIper filtro);
}
