/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.actualizacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.PredioActualizacion;
import java.util.List;

/**
 *
 * @author felipe.cadena
 */
public interface IPredioActualizacionDAO extends IGenericJpaDAO<PredioActualizacion, Long> {

    /**
     * Retorna todos los registros asociados a los tramites de la solicitus enviada
     *
     *
     * @param solicitudId
     * @return
     */
    public List<PredioActualizacion> obtenerPorSolicitudId(Long solicitudId);

}
