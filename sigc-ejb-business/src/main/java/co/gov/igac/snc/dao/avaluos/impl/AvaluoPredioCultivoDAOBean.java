package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioCultivoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioCultivo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoPredioCultivoDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoPredioCultivoDAOBean extends
    GenericDAOWithJPA<AvaluoPredioCultivo, Long> implements IAvaluoPredioCultivoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoPredioCultivoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoPredioCultivoDAO#consultarPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioCultivo> consultarPorAvaluo(long avaluoId) {

        LOGGER.debug("on AvaluoPredioCultivoDAOBean#consultarPorAvaluo");

        List<AvaluoPredioCultivo> answer = null;
        String queryString;
        Query query;

        queryString = "" +
            " SELECT apc FROM AvaluoPredioCultivo apc WHERE apc.avaluo.id = :idAvaluo_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo_p", avaluoId);
            answer = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "AvaluoPredioCultivoDAOBean#consultarPorAvaluo");
        }

        return answer;
    }

    /**
     * @see IAvaluoPredioCultivoDAO#calcularCultivosPreviosAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Boolean calcularCultivosPreviosAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "SELECT o FROM AvaluoPredioCultivo o " +
            "JOIN o.avaluo ava WHERE ava.id = :idAvaluo";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            List<AvaluoPredioCultivo> apzs = query.getResultList();
            if (apzs == null || apzs.isEmpty()) {
                return null;
            }

            if (apzs.get(0).getAvaluoPredioId() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#calcularCultivosPreviosAvaluo");
        }
    }

    /**
     * @see IAvaluoPredioCultivoDAO#eliminarCultivosPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public boolean eliminarCultivosPreviosAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "DELETE FROM avaluo_predio_cultivo ctv WHERE ctv.avaluo_id = " + idAvaluo;

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.executeUpdate();
            return true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#eliminarCultivosPreviosAvaluo");
        }
    }

//end of class
}
