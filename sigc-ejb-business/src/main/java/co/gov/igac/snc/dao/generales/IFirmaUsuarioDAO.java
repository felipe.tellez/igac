package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;

@Local
public interface IFirmaUsuarioDAO extends IGenericJpaDAO<FirmaUsuario, Long> {

    public FirmaUsuario consultarUsuarioFirma(String nombreUsuario);

    /**
     * Método que retorna una Firma de documentos de Usuario
     *
     * @author leidy.gonzalez
     * @param nombre
     * @return
     */
    public FirmaUsuario buscarDocumentoFirmaPorUsuario(String nombre);
}
