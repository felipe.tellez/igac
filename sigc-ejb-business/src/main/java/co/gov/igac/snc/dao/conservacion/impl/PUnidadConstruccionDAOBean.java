package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.ITipificacionUnidadConstDAO;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import org.hibernate.Hibernate;

/**
 *
 * @author fabio.navarrete
 *
 */
@Stateless
public class PUnidadConstruccionDAOBean extends
    GenericDAOWithJPA<PUnidadConstruccion, Long> implements
    IPUnidadConstruccionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PUnidadConstruccionDAOBean.class);

    @EJB
    private ITipificacionUnidadConstDAO tipificacionUnidadConstDAO;

    // ------------------------------------------------------------- //
    @Override
    public IModeloUnidadConstruccion findPUnidadConstruccionFetchUsoConstruccionById(
        Long id) {
        String query = "SELECT puc" +
            " FROM PUnidadConstruccion p" +
            " LEFT JOIN FETCH puc.usoConstruccion" +
            " WHERE puc.id = :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("id", id);
        try {
            return (IModeloUnidadConstruccion) q.getSingleResult();
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    @Override
    public PUnidadConstruccion buscarUnidadDeConstruccionPorSuId(
        Long idPUnidadConstruccion) {

        PUnidadConstruccion answer = null;

        String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.PUnidadConstruccionComps pucc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio " +
            " WHERE puc.id = :idPUnidadConstruccion";

        Query query = this.entityManager.createQuery(sql);
        query.setParameter("idPUnidadConstruccion", idPUnidadConstruccion);
        try {
            answer = (PUnidadConstruccion) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#getAllPunidadConstruccionsByPredioId(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccion> getAllPunidadConstruccionsByTramiteId(
        Long tramiteId) {

        List<PUnidadConstruccion> answer = null;

        String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.PUnidadConstruccionComps pucc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio pp" +
            " LEFT JOIN FETCH puc.provienePredio " +
            " LEFT JOIN FETCH pp.tramite t" +
            " WHERE t.id = :tramiteId";

        Query query = this.entityManager.createQuery(sql);
        query.setParameter("tramiteId", tramiteId);
        try {
            answer = (List<PUnidadConstruccion>) query.getResultList();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;

    }

    //------------------------------ //
    /**
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#buscarUnidadesDeConstruccionPorPPredioId(Long)
     * @author david.cifuentes
     *
     * @note Inicialmente solo tenia el filtro de diferencia con Cancelado, pero si estaba en null
     * no los incluía por eso adicioné el IS NULL or.... Pero luego en tercera no se estan
     * incluyendo las construcciones C, y se necesitan para mostrar entre las vigentes, luego
     * adicioné el segundo parámetro que llegaria false en todos los casos anteriores, y true para
     * las de 3 Por favor tener en cuenta que existen en PPredio una serie de metodos que devuelven
     * el listado dependiendo de si están en null, C, M o I
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionPorPPredioId(Long idPPredio,
        boolean incluirCancelados) {

        List<PUnidadConstruccion> answer = null;
        try {
            String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
                " LEFT JOIN FETCH puc.PUnidadConstruccionComps pucc" +
                " LEFT JOIN FETCH puc.usoConstruccion " +
                " LEFT JOIN FETCH puc.PPredio pp" +
                " LEFT JOIN FETCH pp.tramite t" +
                " LEFT JOIN FETCH puc.provienePredio p" +
                " WHERE pp.id = :idPPredio ";
            if (!incluirCancelados) {
                sql += " AND (puc.cancelaInscribe IS NULL or puc.cancelaInscribe != :cancelado)";
            }

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idPPredio", idPPredio);
            if (!incluirCancelados) {
                query.setParameter("cancelado",
                    EProyeccionCancelaInscribe.CANCELA.getCodigo());
            }

            answer = (List<PUnidadConstruccion>) query.getResultList();

            if (answer != null && answer.size() > 0) {
                for (PUnidadConstruccion puc : answer) {
                    if (puc.getUsoConstruccion() != null) {
                        puc.getUsoConstruccion().getNombre();
                    }

                    String tipificacion = puc.getTipificacion();
                    if (tipificacion != null && !tipificacion.equals("")) {

                        if (tipificacion.equals("NA")) {
                            puc.setTipificacionValor("NA");
                        } else {
                            String criterio[] = new String[]{"tipo", "=", tipificacion};
                            List<String[]> criterios = new ArrayList<String[]>();
                            criterios.add(criterio);

                            List<TipificacionUnidadConst> tucs = tipificacionUnidadConstDAO.
                                findByCriteria(criterios);
                            if (tucs != null && !tucs.isEmpty()) {
                                puc.setTipificacionValor(tucs.get(0).getTipificacion());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#getPUnidadsConstruccionByNumeroPredial(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PUnidadConstruccion> getPUnidadsConstruccionByNumeroPredial(
        String numPred) {
        List<PUnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio p" +
            " WHERE p.numeroPredial = :numPred";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("numPred", numPred);

            answer = (List<PUnidadConstruccion>) q.getResultList();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;
    }

    //---------------------------------------------------- //
    /**
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(Long,
     * UnidadConstruccion)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(
        Long idTramite, UnidadConstruccion unidadConstruccionPredioOriginal) {

        List<PUnidadConstruccion> answer = null;
        try {
            String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
                " LEFT JOIN FETCH puc.PPredio pp" +
                " LEFT JOIN FETCH pp.tramite t" +
                " LEFT JOIN FETCH puc.provienePredio p" +
                " WHERE pp.id IN " +
                "(SELECT DISTINCT pt.id FROM PPredio pt WHERE pt.tramite.id = :idTramite)" +
                " AND puc.provieneUnidad = :unidadPredioOriginal";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idTramite", idTramite);
            query.setParameter("unidadPredioOriginal", unidadConstruccionPredioOriginal.getUnidad());

            answer = (List<PUnidadConstruccion>) query.getResultList();

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;
    }

    /**
     * @see co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO
     * #buscarUnidConstPorTramiteYDifNumFichaMatrizEV(Long, String)
     * @author leidy.gonzalez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PUnidadConstruccion> buscarUnidConstPorTramiteYDifNumFichaMatrizEV(
        Long idTramite, String numeroFichaMatriz) {

        List<PUnidadConstruccion> answer = null;
        List<PUnidadConstruccion> pUnidadesConstruccion = new ArrayList<PUnidadConstruccion>();
        try {
            String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
                " LEFT JOIN FETCH puc.PPredio pp" +
                " LEFT JOIN FETCH pp.tramite t" +
                " LEFT JOIN FETCH puc.provienePredio p" +
                " LEFT JOIN FETCH puc.usoConstruccion " +
                " WHERE t.id = :idTramite ";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idTramite", idTramite);

            answer = (List<PUnidadConstruccion>) query.getResultList();

            if (answer != null && !answer.isEmpty()) {

                for (PUnidadConstruccion pUnidadConstruccion : answer) {
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPFichaMatrizs());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().
                        getPPredioAvaluoCatastrals());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioZonas());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioDireccions());
                }

                if (numeroFichaMatriz != null && !numeroFichaMatriz.isEmpty()) {
                    for (PUnidadConstruccion pUnidadConstruccion : answer) {

                        if (!numeroFichaMatriz.equals(pUnidadConstruccion.getPPredio().
                            getNumeroPredial())) {
                            pUnidadesConstruccion.add(pUnidadConstruccion);
                        }
                    }
                } else {
                    pUnidadesConstruccion = answer;
                }

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return pUnidadesConstruccion;
    }

    /**
     * @author leidy.gonzalez
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#obtenerPUnidadsConstruccionPorIdPredio(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadsConstruccionPorIdPredio(
        Long idPred) {
        List<PUnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio pp" +
            " LEFT JOIN FETCH puc.provienePredio " +
            " WHERE pp.id = :idPred";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("idPred", idPred);

            answer = (List<PUnidadConstruccion>) q.getResultList();

            if (answer != null && !answer.isEmpty()) {
                for (PUnidadConstruccion pUnidadConstruccion : answer) {
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPFichaMatrizs());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().
                        getPPredioAvaluoCatastrals());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioZonas());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioDireccions());
                }
            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#obtenerPUnidadesConstConvPorIdPredio(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstConvPorIdPredio(
        Long idPred) {
        List<PUnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio pp" +
            " LEFT JOIN FETCH puc.provienePredio " +
            " WHERE pp.id = :idPred " +
            " AND puc.tipoConstruccion = :tipoConstruccion ";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("idPred", idPred);
            q.setParameter("tipoConstruccion", EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());

            answer = (List<PUnidadConstruccion>) q.getResultList();

            if (answer != null && !answer.isEmpty()) {
                for (PUnidadConstruccion pUnidadConstruccion : answer) {
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPFichaMatrizs());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().
                        getPPredioAvaluoCatastrals());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioZonas());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioDireccions());
                }
            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#obtenerPUnidadesConstNoConvPorIdPredio(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstNoConvPorIdPredio(
        Long idPred) {
        List<PUnidadConstruccion> answer = null;

        String query = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio pp" +
            " LEFT JOIN FETCH puc.provienePredio " +
            " WHERE pp.id = :idPred " +
            " AND puc.tipoConstruccion = :tipoConstruccion ";;

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("idPred", idPred);
            q.setParameter("tipoConstruccion", EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo());

            answer = (List<PUnidadConstruccion>) q.getResultList();

            if (answer != null && !answer.isEmpty()) {
                for (PUnidadConstruccion pUnidadConstruccion : answer) {
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPFichaMatrizs());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().
                        getPPredioAvaluoCatastrals());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioZonas());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioDireccions());
                }
            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;
    }

    /**
     * @see co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO
     * #buscarUnidConstOriginalesCanceladasPorTramite(Long, String)
     * @author leidy.gonzalez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PUnidadConstruccion> buscarUnidConstOriginalesCanceladasPorTramite(
        Long idTramite) {

        List<PUnidadConstruccion> answer = null;
        List<PUnidadConstruccion> pUnidadesConstruccion = new ArrayList<PUnidadConstruccion>();
        try {
            String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
                " LEFT JOIN FETCH puc.PPredio pp" +
                " LEFT JOIN FETCH pp.tramite t" +
                " LEFT JOIN FETCH puc.provienePredio p" +
                " LEFT JOIN FETCH puc.usoConstruccion " +
                " WHERE t.id = :idTramite " +
                " AND pp.estado = :estado ";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idTramite", idTramite);
            query.setParameter("estado", EPredioEstado.CANCELADO.getCodigo());

            answer = (List<PUnidadConstruccion>) query.getResultList();

            if (answer != null && !answer.isEmpty()) {

                for (PUnidadConstruccion pUnidadConstruccion : answer) {
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPFichaMatrizs());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().
                        getPPredioAvaluoCatastrals());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioZonas());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioDireccions());

                    if (pUnidadConstruccion.getPPredio() != null &&
                        (pUnidadConstruccion.getPPredio().isEsPredioFichaMatriz() ||
                        EPredioCondicionPropiedad.CP_0.getCodigo().equals(pUnidadConstruccion.
                            getPPredio().getCondicionPropiedadNumeroPredial()) ||
                        EPredioCondicionPropiedad.CP_5.getCodigo().equals(pUnidadConstruccion.
                            getPPredio().getCondicionPropiedadNumeroPredial())) &&
                        pUnidadConstruccion.getAnioCancelacion() == null) {

                        pUnidadesConstruccion.add(pUnidadConstruccion);

                    }
                }

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return pUnidadesConstruccion;
    }

    /**
     * @see co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO
     * #buscarUnidConstOriginalesCanceladasPorTramite(Long, String)
     * @author leidy.gonzalez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PUnidadConstruccion> buscarUnidConstOriginalesPorIdPredio(
        Long idPredio) {

        List<PUnidadConstruccion> answer = null;
        List<PUnidadConstruccion> pUnidadesConstruccion = new ArrayList<PUnidadConstruccion>();
        try {
            String sql = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
                " LEFT JOIN FETCH puc.PPredio pp" +
                " LEFT JOIN FETCH pp.tramite t" +
                " LEFT JOIN FETCH puc.provienePredio p" +
                " LEFT JOIN FETCH puc.usoConstruccion " +
                " WHERE pp.id = :idPred ";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idPred", idPredio);

            answer = (List<PUnidadConstruccion>) query.getResultList();

            if (answer != null && !answer.isEmpty()) {

                for (PUnidadConstruccion pUnidadConstruccion : answer) {
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPFichaMatrizs());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().
                        getPPredioAvaluoCatastrals());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioZonas());
                    Hibernate.initialize(pUnidadConstruccion.getPPredio().getPPredioDireccions());

                    if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pUnidadConstruccion.
                        getCancelaInscribe()) &&
                        pUnidadConstruccion.getAnioCancelacion() == null) {

                        pUnidadesConstruccion.add(pUnidadConstruccion);

                    }
                }

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return pUnidadesConstruccion;
    }

    /**
     * @author leidy.gonzalez
     * @see
     * co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO#obtenerPUnidadsConstruccionPorIdPredioYUnidad(java.lang.Long)
     */
    @Override
    public PUnidadConstruccion obtenerPUnidadsConstruccionPorIdPredioYUnidad(
        Long idPred, String unidad) {
        PUnidadConstruccion answer = null;

        String query = "SELECT DISTINCT puc FROM PUnidadConstruccion puc" +
            " LEFT JOIN FETCH puc.usoConstruccion " +
            " LEFT JOIN FETCH puc.PPredio pp" +
            " LEFT JOIN FETCH puc.provienePredio " +
            " WHERE pp.id = :idPred" +
            " AND puc.unidad = :unidad";

        try {
            Query q = this.entityManager.createQuery(query);

            q.setParameter("idPred", idPred);
            q.setParameter("unidad", unidad);

            answer = (PUnidadConstruccion) q.getSingleResult();

            if (answer != null) {

                Hibernate.initialize(answer.getPPredio().getPFichaMatrizs());
                Hibernate.initialize(answer.getPPredio().getPPredioAvaluoCatastrals());
                Hibernate.initialize(answer.getPPredio().getPPredioZonas());
                Hibernate.initialize(answer.getPPredio().getPPredioDireccions());

            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return answer;
    }
    
    /**
     *
     * @author leidy.gonzalez
     * @see IPUnidadConstruccionDAO#obtenerPUnidadConstruccionPorIds
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadConstruccionPorIds(List<Long> id) {

        List<PUnidadConstruccion> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT pc FROM PUnidadConstruccion pc " +
            "WHERE pc.id IN (:id) ";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("id", id);
            answer = (List<PUnidadConstruccion>) query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("Error on PUnidadConstruccionDAOBean#obtenerPUnidadConstruccionPorIds: " +
                ex.getMessage());
        }
        return answer;
    }

}
