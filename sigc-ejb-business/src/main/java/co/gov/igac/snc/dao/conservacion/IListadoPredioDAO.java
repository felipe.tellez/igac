package co.gov.igac.snc.dao.conservacion;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredio;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredioPK;

/**
 *
 * @author david.cifuentes
 *
 */
@Local
public interface IListadoPredioDAO extends
    IGenericJpaDAO<ListadoPredio, ListadoPredioPK> {

    /**
     * Método para obtener en cierre de año los números prediales de los predios de un municipio
     * seleccionado dada una vigencia.
     *
     * @author david.cifuentes
     *
     * @param municipioCodigo
     * @param vigenciaDecreto
     * @return
     */
    public String[] cargarListadoPrediosPorMunicipioVigencia(
        String municipioCodigo, Long vigenciaDecreto);

    /**
     * Método que realiza el conteo de un array de numeros prediales enviados como parametro.
     *
     * @author david.cifuentes
     *
     * @param idsListadoPredios
     * @return
     */
    public int contarListadoPredios(String[] numerosPredialesListadoPredios);

    /**
     * Método que realiza la busqueda de {@link ListadoPredio} asociados en una lista de numeros
     * prediales.
     *
     * @author david.cifuentes
     *
     * @param numerosPredialesListadoPredios
     * @param sortField
     * @param sortOrder
     * @param filters
     * @param first
     * @param pageSize
     * @return
     */
    public List<ListadoPredio> buscarListadoPrediosCierreAnual(
        String[] numerosPredialesListadoPredios, String sortField, String sortOrder,
        Map<String, String> filters, int[] contadores);

    /**
     * Método que realiza la busqueda de {@link ListadoPredio} asociados en una a una vigencia y un
     * codigo de municipio.
     *
     * @author david.cifuentes
     *
     * @param numerosPredialesListadoPredios
     * @param sortField
     * @param sortOrder
     * @param filters
     * @param first
     * @param pageSize
     * @return
     */
    public List<ListadoPredio> buscarListadoPrediosCierreAnualPorMunicipioVigencia(
        String municipioCodigo, Long vigenciaDecreto, String sortField,
        String sortOrder, Map<String, String> filters, int first, int pageSize);

    /**
     * Método que realiza el conteo del listado de predios dada una vigencia y un codigo de
     * municipio
     *
     * @author david.cifuentes
     *
     * @param municipioCodigo
     * @param vigenciaDecreto
     * @param filters
     * @return
     */
    public int contarListadoPredios(String municipioCodigo, Long vigenciaDecreto,
        Map<String, String> filters);

}
