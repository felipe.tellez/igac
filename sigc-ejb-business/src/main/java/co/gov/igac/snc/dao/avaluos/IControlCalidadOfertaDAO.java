package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;

/**
 *
 * Interfaz para los métodos de bd de la tabla CONTROL_CALIDAD_OFERTA
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IControlCalidadOfertaDAO extends IGenericJpaDAO<ControlCalidadOferta, Long> {

    /**
     * Método para cargar lista de ofertas de un recolector seleccionadas para control de calidad
     *
     * @author rodrigo.hernandez
     *
     * @param recolectorId - Id del Recolector
     * @param controlCalidadId - Id del Control de Calidad
     * @param regionId - Id de la región de captura de ofertas
     */
    public List<ControlCalidadOferta> buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId);

    /**
     * Método para cargar lista de control calidad ofertas de un recolector seleccionadas para
     * control de calidad
     *
     * @author christian.rodriguez
     *
     * @param recolectorId - Id del Recolector
     * @param controlCalidadId - Id del Control de Calidad
     */
    public List<ControlCalidadOferta> buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(
        String recolectorId, Long controlCalidadId);

    /**
     * Método para cargar lista de ofertas de un recolector seleccionadas para control de calidad
     *
     * @author christian.rodriguez
     *
     * @param recolectorId - Id del Recolector
     * @param controlCalidadId - Id del Control de Calidad
     * @param regionId - Id de la región de captura de ofertas
     */
    public List<OfertaInmobiliaria> buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId);

    /**
     * Método para cargar lista de todas las ofertas de un recolector
     *
     * @author rodrigo.hernandez
     *
     * @param idRecolector - Id del Recolector
     * @param idControlCalidad - Id del Control de Calidad
     */
    public List<ControlCalidadOferta> buscarOfertasPorIdRecolectorEIdControlCalidad(
        String recolectorId, Long controlCalidadId);

    /**
     * Método que busca los ids de las regiones de captura asociadas a una muestra de control de
     * calidad
     *
     * @author christian.rodriguez
     * @param controlCalidadId ID del control de calidad
     * @return Lista con los ids de las regiones asociadas al control de calidad
     */
    public List<Long> buscarIdsRegionesAsociadasAControlCalidad(Long controlCalidadId);
}
