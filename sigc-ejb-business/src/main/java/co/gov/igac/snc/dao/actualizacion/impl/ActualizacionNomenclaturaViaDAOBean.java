package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionNomenclaturaViaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionNomenclaturaVia;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionNomenclaturaVia.
 *
 * @author david.cifuentes
 */
@Stateless
public class ActualizacionNomenclaturaViaDAOBean extends
    GenericDAOWithJPA<ActualizacionNomenclaturaVia, Long> implements
    IActualizacionNomenclaturaViaDAO {

    /**
     * Servicio de bitácora.
     */
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionNomenclaturaViaDAOBean.class);

}
