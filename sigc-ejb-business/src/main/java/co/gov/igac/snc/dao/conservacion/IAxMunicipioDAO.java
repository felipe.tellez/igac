package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import java.util.List;
import javax.ejb.Local;


/*
 * Proyecto SNC 2016
 */
/**
 * Interfaz del DAO para la clase AxMunicipio
 *
 * @author felipe.cadena
 */
@Local
public interface IAxMunicipioDAO extends IGenericJpaDAO<AxMunicipio, Long> {

    /**
     * Retorna los registros asociados a un municipio y una vigencia en particular
     *
     * @author felipe.cadena
     *
     * @param municipioCodigo
     * @param vigencia -- String con el año correspondiente a la vigencia
     * @return
     */
    public List<AxMunicipio> obtenerPorMunicipioVigencia(String municipioCodigo, String vigencia);

    /**
     * Retorna los registros asociados los municipios que estan en actualizacion
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<AxMunicipio> obtenerMunicipiosEnActualizacion();

}
