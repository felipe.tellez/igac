/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

/**
 * Interfaz para el dao de VComision que corresponde a la vista v_comision
 *
 * OJO: tener en cuenta que no se pueden hacer actualizaciones ni inserciones con objetos de este
 * tipo
 *
 * @author pedro.garcia
 */
@Local
public interface IVComisionDAO extends IGenericJpaDAO<VComision, Long> {

    /**
     * Cuenta las comisiones que estén sin memorando generado y que estén relacionadas con trámites
     * que estén a cargo del jefe de conservación de la territorial dada Estas comisiones son las
     * que el jefe de conservación ve en la pantalla "Manejo de comisiones para trámites" en el tab
     * de comisiones
     *
     * @author pedro.garcia
     * @param idTerritorial
     * @param idTramites ids de los trámites que vienen dados por el proceso, sobre los cuales se
     * hace la búsqueda
     * @param tipoComision nombre del tipo de comisión
     */
    public int countComisionesSinMemo(String idTerritorial, List<Long> idTramites,
        String tipoComision);

    /**
     * Busca las comisiones que estén sin memorando generado y que estén relacionadas con trámites
     * que estén a cargo del jefe de conservación de la territorial dada Estas comisiones son las
     * que el jefe de conservación ve en la pantalla "Manejo de comisiones para trámites" en el tab
     * de comisiones
     *
     * @author pedro.garcia
     *
     * @param idTerritorial id de la territorial
     * @param idTramites ids de los trámites que vienen dados por el proceso, sobre los cuales se
     * hace la búsqueda
     * @param tipoComision nombre del tipo de comisión
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     */
    public List<VComision> findComisionesSinMemo(String idTerritorial, List<Long> idTramites,
        String tipoComision, Map<String, String> filters, int... rowStartIdxAndCount);

    /**
     * Cuenta las comisiones que estén relacionadas con trámites que estén a cargo del jefe de
     * conservación o del director de la territorial dada, cuyo rol se identifica con el parámetro
     * rolUsuario.
     *
     * Estas comisiones son las que se ven en la pantalla "Administración de comisiones"
     *
     * @author pedro.garcia
     * @param idTerritorial
     * @param rolUsuario
     */
    public int countComisionesParaAdmin(String idTerritorial, String rolUsuario);

    /**
     * Busca las comisiones que estén estén relacionadas con trámites que estén a cargo del jefe de
     * conservación o del director de la territorial dada, cuyo rol se identifica con el parámetro
     * rolUsuario. Estas comisiones son las que el jefe de conservación ve en la pantalla
     * "Administración de comisiones"
     *
     * @author pedro.garcia
     *
     * @param idTerritorial id de la territorial
     * @param rolUsuario
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     */
    public List<VComision> findComisionesParaAdmin(String idTerritorial, String rolUsuario,
        String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount);

    /**
     * Método copiado del método findComisionesParaAdmin de pedro.garcia y ajustado para aprobar
     * comisiones.
     *
     * Busca las comisiones que estén estén relacionadas con trámites que estén en la lista de ids y
     * que sean del tipo dado; a cargo del director de la territorial. Estas comisiones son las que
     * el director territorial ve en la pantalla "Aprobar comisiones"
     *
     * @author david.cifuentes
     *
     * @param idTramites id de tramites
     * @param tipoComision tipo de comisiones que se buscan
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     */
    /*
     * @modified pedro.garcia 27-06-2013 adición de parámetro para tener en cuenta el tipo de
     * comisión
     */
    public List<VComision> buscarComisionesParaAprobar(List<Long> idTramites, String tipoComision,
        String sortField, String sortOrder, Map<String, String> filters, int... rowStartIdxAndCount);

    /**
     * Cuenta las comisiones que estén relacionadas con trámites que estén en la lista de ids, y que
     * sean del tipo dado. Estas comisiones son las que el director de la territorial ve en la
     * pantalla "Aprobar comisiones"
     *
     * @note Método copiado del método buscarComisionesParaAdministrar y ajustado para las
     * comisiones para Aprobar.
     *
     * @author david.cifuentes
     *
     * @param idTramites
     * @param tipoComision tipo de comisiones que se buscan
     */
    /*
     * @modified pedro.garcia 27-06-2013 adición de parámetro para tener en cuenta el tipo de
     * comisión
     */
    public int contarComisionesParaAprobar(List<Long> idTramites, String tipoComision);

//end of interface    
}
