/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteInconsistenciaDAO;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase para las operaciones en DB de la entidad TramiteInconsistencia
 *
 * @author felipe.cadena
 */
@Stateless
public class TramiteInconsistenciaDAOBean extends GenericDAOWithJPA<TramiteInconsistencia, Long>
		implements ITramiteInconsistenciaDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(TramiteInconsistenciaDAOBean.class);

	/**
	 * @see ITramiteInconsistenciaDAO#buscarPorIdTramite
	 * @author felipe.cadena
	 */
	@Implement
	@Override
	public List<TramiteInconsistencia> buscarPorIdTramite(Long idTramite) {

		List<TramiteInconsistencia> resultado = null;
		String queryString;
		Query query;

		queryString = "SELECT ti FROM TramiteInconsistencia ti " + "JOIN ti.tramite t WHERE t.id = :idTramite";

		try {
			query = this.entityManager.createQuery(queryString.toString());
			query.setParameter("idTramite", idTramite);

			resultado = query.getResultList();

		} catch (Exception ex) {
			throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(LOGGER, ex,
					"TramiteInconsistencia");
		}

		return resultado;
	}
	// -------------------------------------------------------------------------------------------------

	/**
	 *
	 */
	@Override
	public List<TramiteInconsistencia> buscarDepuradaByTramiteId(Long tramiteId, String condicionDepurada) {

		try {
			List<TramiteInconsistencia> tramiteInconsistencias;

			String query = "SELECT ti FROM TramiteInconsistencia ti " + "where ti.depurada "
					+ "like :condicionDepurada and ti.tramite.id = :tramiteId ";

			Query q = this.entityManager.createQuery(query);
			q.setParameter("condicionDepurada", condicionDepurada);
			q.setParameter("tramiteId", tramiteId);

			tramiteInconsistencias = q.getResultList();
			return tramiteInconsistencias;
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL;
		}
	}

	/**
	 * Procesa los resultados del job de calculo de inconsistencias y guarda los
	 * resultados en la BD
	 *
	 * @author felipe.cadena
	 * @return
	 */
	/*
	 * modified andres.eslava::25-01-2016::Se agrega nuevo formato de xml con
	 * inconsistencias
	 */
	@Override
	public Boolean guardarInconsistenciasAsincronicas(Tramite tramite, String XMLInconsistencias) {

		Document document;

		Map<String, String> inconsistenciasPorPredio = new HashMap<String, String>();
		String numeroPredial = "";
		String valorInconsistencias = "";
		List<TramiteInconsistencia> inconsistenciasAPersistir = new ArrayList<TramiteInconsistencia>();

		try {
			document = DocumentHelper.parseText(XMLInconsistencias);
			List<Node> resultados = document.selectNodes("//Response/Results/ValidacionPredio");
			// Se extraen las inconsistencias del XML
			for (Node node : resultados) {
				Element element = (Element) node;
				numeroPredial = element.attributeValue("Predio");
				valorInconsistencias = element.getText();
				valorInconsistencias = valorInconsistencias.trim();

				if (valorInconsistencias != null && !valorInconsistencias.isEmpty()) {
					inconsistenciasPorPredio.put(numeroPredial, valorInconsistencias);
				}
			}

			// Se crean los registros de las consistencias para persistir
			for (String numeroPredialKey : inconsistenciasPorPredio.keySet()) {

				String igps = inconsistenciasPorPredio.get(numeroPredialKey);

				String[] igpsArray = igps.split(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);

				for (String igp : igpsArray) {
					TramiteInconsistencia ti = new TramiteInconsistencia();

					for (EInconsistenciasGeograficas iEnum : EInconsistenciasGeograficas.values()) {
						if (iEnum.getCodigoSig().equals(igp)) {
							ti.setInconsistencia(iEnum.getNombre());
							ti.setNumeroPredial(numeroPredialKey);
							ti.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
							ti.setFechaLog(new Date());
							ti.setUsuarioLog(tramite.getFuncionarioRadicador());
							ti.setTramite(tramite);
							ti.setDepurada(ESiNo.NO.getCodigo());
							inconsistenciasAPersistir.add(ti);
							break;
						}
					}
				}
			}

			// Si no existen inconsistencias se persiste el registro por defecto para marcar
			// al tramite como sin inconsistencias
			if (inconsistenciasAPersistir.isEmpty()) {
				TramiteInconsistencia ti = new TramiteInconsistencia();
				ti.setInconsistencia(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre());

				if (tramite.getPredio() != null) {
					ti.setNumeroPredial(tramite.getPredio().getNumeroPredial());
				} else {
					ti.setNumeroPredial("00");
				}
				ti.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
				ti.setFechaLog(new Date());
				ti.setTramite(tramite);
				ti.setUsuarioLog(tramite.getFuncionarioRadicador());
				ti.setDepurada(ESiNo.SI.getCodigo());
				inconsistenciasAPersistir.add(ti);
			}

			// Se persisten la inconistencias finales
			this.persistMultiple(inconsistenciasAPersistir);

		} catch (DocumentException e) {
			LOGGER.error("Error al procesar las inconsistencias asincronicas");
			e.printStackTrace();
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	/**
	 * Elimina las inconsistencias de un tramite
	 * 
	 * @param idTramite
	 * @author vsocarras
	 */
	@Override
	public boolean eliminarInconsistenciasPorTramite(Long idTramite) {

		int resultado = 0;
		String queryString;
		Query query;

		queryString = "delete FROM tramite_inconsistencia  WHERE tramite_id = :idTramite";

		try {
			query = this.entityManager.createNativeQuery(queryString.toString());
			query.setParameter("idTramite", idTramite);
			resultado = query.executeUpdate();
			//LOGGER.debug("Se eliminaron "+resultado+" incosistencias");
			return true;
		} catch (Exception ex) {
			throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
			getExcepcion(LOGGER, ex, "TramiteInconsistenciaDAOBean#eliminarInconsistenciasPorTramite",
                    "TramiteInconsistencia", ""+idTramite);
			
		}

	}

}
