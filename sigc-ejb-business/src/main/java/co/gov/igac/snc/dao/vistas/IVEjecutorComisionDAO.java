/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para el dao de VEjecutorComision que corresponde a la vista v_ejecutor_comision
 *
 * OJO: tener en cuenta que no se pueden hacer actualizaciones ni inserciones con objetos de este
 * tipo
 *
 * @author pedro.garcia
 */
@Local
public interface IVEjecutorComisionDAO extends IGenericJpaDAO<VEjecutorComision, Long> {

    /**
     * Busca según un número de comisión
     *
     * @author pedro.garcia
     * @param numeroComision
     * @return
     */
    List<VEjecutorComision> findByNumeroComision(String numeroComision);

}
