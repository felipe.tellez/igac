package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IComiteAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.IComiteAvaluoParticipanteDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoParticipante;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoPonente;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IComiteAvaluoParticipanteDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ComiteAvaluoDAOBean extends GenericDAOWithJPA<ComiteAvaluo, Long>
    implements IComiteAvaluoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ComiteAvaluo.class);

    // ---------------------------------------------------------------------------------
    /**
     * @see IComiteAvaluoDAO#consultarPorIdControlCalidadAvaluo(Long)
     * @author christian.rodriguez
     */
    @Override
    public ComiteAvaluo consultarPorIdControlCalidadAvaluo(Long controlCalidadId) {

        ComiteAvaluo comite = null;
        String queryString;
        Query query;

        queryString = "SELECT ca " +
            " FROM ComiteAvaluo ca " +
            " WHERE ca.controlCalidadAvaluoId = :controlCalidadId";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("controlCalidadId", controlCalidadId);

            comite = (ComiteAvaluo) query.getSingleResult();

            this.inicializarObjetosComite(comite);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex, "ComiteAvaluo");
        }

        return comite;
    }

    // ---------------------------------------------------------------------------------
    /**
     * método que inicializa los objetos asociados a la entidad
     *
     * @author christian.rodriguez
     * @param comite {@link ComiteAvaluo} a la cual se le quieren inicializar los objetos
     */
    public void inicializarObjetosComite(ComiteAvaluo comite) {
        if (comite != null) {

            Hibernate.initialize(comite.getComiteAvaluoParticipantes());
            for (ComiteAvaluoParticipante participante : comite
                .getComiteAvaluoParticipantes()) {
                Hibernate.initialize(participante.getProfesionalAvaluos());
            }

            Hibernate.initialize(comite.getComiteAvaluoPonentes());
            for (ComiteAvaluoPonente ponente : comite.getComiteAvaluoPonentes()) {
                Hibernate.initialize(ponente.getProfesionalAvaluos());
            }
        }
    }

}
