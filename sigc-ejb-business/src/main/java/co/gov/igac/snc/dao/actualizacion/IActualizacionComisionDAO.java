package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionComision;

/**
 * Servicios de persistencia para el objeto ActualizacionComision
 *
 * @author franz.gamba
 */
@Local
public interface IActualizacionComisionDAO extends IGenericJpaDAO<ActualizacionComision, Long> {

    /**
     * Método que retorna una comisión para un proceso de actualización según su objeto
     *
     * @param actualizacionId
     * @param objeto
     * @return
     */
    public ActualizacionComision getActualizacionComisionByActualizacionId(Long actualizacionId,
        String objeto);
}
