package co.gov.igac.snc.dao.tramite.radicacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;

/**
 * Interfaz para los métodos de base de datos de la tabla TRAMITE_RECTIFICACION
 *
 * @author pedro.garcia
 */
@Local
public interface ITramiteRectificacionDAO extends IGenericJpaDAO<TramiteRectificacion, Long> {

    /**
     * Método que obtiene una lista de tramites rectificaciones por el id del trámite
     *
     * @author javier.aponte
     * @param tramiteId
     * @return
     */
    public List<TramiteRectificacion> findTramiteRectificacionesByTramiteId(Long tramiteId);

}
