package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioServidumbreDAO;
import co.gov.igac.snc.dao.generales.impl.DominioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioServidumbre;

@Stateless
public class PredioServidumbreDAOBean extends GenericDAOWithJPA<PredioServidumbre, Long>
    implements IPredioServidumbreDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DominioDAOBean.class);

    /**
     * @see IPredioServidumbreDAO#getPredioServidumbreByPredio(Predio)
     */
    @SuppressWarnings("unchecked")
    public List<PredioServidumbre> getPredioServidumbreByPredio(Predio predio) {
        LOGGER.debug("getPredioServidumbreByPredio");
        Query q = entityManager.createNamedQuery("findServidumbreByPredio");
        q.setParameter("predio", predio);
        return q.getResultList();
    }

}
