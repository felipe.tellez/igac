package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionLevantamientoDAO;
import co.gov.igac.snc.dao.actualizacion.IGeneracionFormularioSbcDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.GeneracionFormularioSbc;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionLevantamiento.
 *
 * @author javier.barajas
 */
@Stateless
public class ActualizacionLevantamientoDAOBean extends GenericDAOWithJPA<ActualizacionLevantamiento, Long>
    implements IActualizacionLevantamientoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionLevantamientoDAOBean.class);
}
