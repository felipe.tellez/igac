package co.gov.igac.snc.business.conservacion;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * Esta clase maneja objetos que implementan IProyeccionObject, y no algún pojo específico. Usado
 * desde la proyección de forma genérica y evitar tener una implementación por cada tabla p
 *
 * @author fredy.wilches
 *
 */
@Local
public interface IProyectarConservacion {

    /**
     *
     * @param idPredio
     * @return
     */
    public PPredio findPPredioByIdFetchAvaluos(Long idPredio);

    /**
     * Almacena un objeto de proyección usando el dao genérico IGenericDAO
     *
     * @author fabio.navarrete
     * @param objP
     * @return
     */
    public Object saveProyeccion(UsuarioDTO usuario, IProyeccionObject objP);

    /**
     * Método que usando el dao genérico IGenericDAO realiza la eliminación de un objeto de
     * proyección
     *
     * @param usuario
     * @param objP
     * @return
     * @author fabio.navarrete
     */
    public IProyeccionObject deleteProyeccion(UsuarioDTO usuario, IProyeccionObject objP);
}
