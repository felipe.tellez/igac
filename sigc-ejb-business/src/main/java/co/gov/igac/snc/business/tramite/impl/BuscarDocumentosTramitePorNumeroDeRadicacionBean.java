package co.gov.igac.snc.business.tramite.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.business.tramite.IBuscarDocumentosTramitePorNumeroDeRadicacion;
import co.gov.igac.snc.dao.tramite.IDocumentacionTramiteDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;

@Stateless
public class BuscarDocumentosTramitePorNumeroDeRadicacionBean implements
    IBuscarDocumentosTramitePorNumeroDeRadicacion, Serializable {

    /**
     * @author juan.agudelo
     */
    private static final long serialVersionUID = -6903219491637196277L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        BuscarDocumentosTramitePorNumeroDeRadicacionBean.class);

    private IDocumentacionTramiteDAO tramiteService;

    @EJB
    public void setTramiteService(IDocumentacionTramiteDAO tramiteService) {
        this.tramiteService = tramiteService;
    }

    // ////////////////////////
    public IDocumentacionTramiteDAO getTramiteService() {
        return tramiteService;
    }

    public List<TramiteDocumentacion> buscarDocumentosTramitePorNumeroDeRadicacion(
        String numeroRadicacion) {
        LOGGER.debug("buscarDocumentosTramitePorNumeroDeRadicacion");
        return this.tramiteService.buscarDocumentosTramitePorNumeroDeRadicacion(numeroRadicacion);
    }

}
