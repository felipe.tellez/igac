package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOfertaInmobiliariaDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IAvaluos;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaAreaUsoExclusivo;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaRecursoHidrico;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioComunal;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioPublico;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.math.BigInteger;

@Stateless
public class OfertaInmobiliariaDAOBean extends GenericDAOWithJPA<OfertaInmobiliaria, Long>
    implements IOfertaInmobiliariaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(OfertaInmobiliariaDAOBean.class);

    /**
     * @see IOfertaInmobiliariaDAO#buscarOfertaInmobiliariaOfertasByOfertaId(Long)
     * @author juan.agudelo
     * @modifiedby christian.rodriguez se modifica para que cargue las ofertaspredio asociados a la
     * oferta
     */
    @Override
    public OfertaInmobiliaria buscarOfertaInmobiliariaOfertasByOfertaId(
        Long ofertaInmobiliariaId) {

        OfertaInmobiliaria answer = null;
        Query query1;
        String q;

        q = "SELECT oi FROM OfertaInmobiliaria oi" +
            " LEFT JOIN FETCH oi.ofertaServicioPublicos" +
            " JOIN FETCH oi.departamento" + " JOIN FETCH oi.municipio" +
            " WHERE oi.id = :ofertaInmobiliariaId";

        query1 = entityManager.createQuery(q);
        query1.setParameter("ofertaInmobiliariaId", ofertaInmobiliariaId);

        try {
            answer = (OfertaInmobiliaria) query1.getSingleResult();

            if (answer != null) {

                if (answer.getOfertaPredios() != null &&
                    !answer.getOfertaPredios().isEmpty()) {
                    for (OfertaPredio ofprTemp : answer.getOfertaPredios()) {
                        ofprTemp.getId();
                    }
                }

                if (answer.getOfertaRecursoHidricos() != null &&
                    !answer.getOfertaRecursoHidricos().isEmpty()) {
                    for (OfertaRecursoHidrico orhTemp : answer
                        .getOfertaRecursoHidricos()) {
                        orhTemp.getId();
                    }
                }

                if (answer.getOfertaServicioComunals() != null &&
                    !answer.getOfertaServicioComunals().isEmpty()) {
                    for (OfertaServicioComunal oscTemp : answer
                        .getOfertaServicioComunals()) {
                        oscTemp.getId();
                    }
                }

                if (answer.getOfertaAreaUsoExclusivos() != null &&
                    !answer.getOfertaAreaUsoExclusivos().isEmpty()) {
                    for (OfertaAreaUsoExclusivo oaueTemp : answer
                        .getOfertaAreaUsoExclusivos()) {
                        oaueTemp.getId();
                    }
                }

                if (answer.getFotografias() != null &&
                    !answer.getFotografias().isEmpty()) {
                    for (Fotografia f : answer.getFotografias()) {
                        f.getId();
                    }
                } else {
                    answer.setFotografias(new ArrayList<Fotografia>());
                }

                if (answer.getOfertaInmobiliarias() != null &&
                    !answer.getOfertaInmobiliarias().isEmpty()) {
                    for (OfertaInmobiliaria oi : answer
                        .getOfertaInmobiliarias()) {
                        oi.getId();
                        if (oi.getFotografias() != null &&
                            !oi.getFotografias().isEmpty()) {
                            for (Fotografia f : oi.getFotografias()) {
                                f.getId();
                            }
                        }
                    }
                }
            }

        } catch (ExcepcionSNC e) {
            throw new ExcepcionSNC("mi codigo de excepcion",
                ESeveridadExcepcionSNC.ERROR, e.getMensaje(), e);
        }

        return answer;
    }

//---------------------------------------------------------------------------------------------
    /**
     * @see
     * IAvaluos#buscarOfertasInmobiliariasPorFiltro(co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob,
     * java.lang.String, java.lang.String, int[])
     * @author juan.agudelo
     * @modified pedro.garcia 14-05-2012 orden en las cosas. Try y catch. Adición de parámetros para
     * ordenamiento de columnas
     */
    @Implement
    @Override
    public List<OfertaInmobiliaria> buscarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob, String sortField,
        String sortOrder, final int... rowStartIdxAndCount) {

        List<OfertaInmobiliaria> answer = null;

        try {
            answer = (List<OfertaInmobiliaria>) this.buscarOContarOfertasInmobiliariasPorFiltro(
                filtroDatosConsultaOfertasInmob, sortField, sortOrder, false, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;
    }
//---------------------------------------------------------------------------------------------

    /**
     * @see IAvaluos#contarOfertasInmobiliariasPorFiltro(FiltroDatosConsultaOfertasInmob)
     * @author juan.agudelo
     */
    @Override
    public Long contarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob) {

        Long answer = new Long(0l);
        BigInteger temp;

        try {
            temp = (BigInteger) this.buscarOContarOfertasInmobiliariasPorFiltro(
                filtroDatosConsultaOfertasInmob, null, null, true);
            answer = temp.longValue();
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see IOfertaInmobiliariaDAO#buscarOfertaInmobiliariaOfertaPadreByOfertaId(Long)
     * @author juan.agudelo
     * @modifiedby christian.rodriguez se modifica para que cargue las ofertaspredio asociados a la
     * oferta
     */
    public OfertaInmobiliaria buscarOfertaInmobiliariaOfertaPadreByOfertaId(
        Long ofertaInmobiliariaId) {

        OfertaInmobiliaria answer = null;
        Query query1;
        String q;

        q = "SELECT oi FROM OfertaInmobiliaria oi" +
            " LEFT JOIN FETCH oi.ofertaServicioPublicos" +
            " LEFT JOIN FETCH oi.ofertaInmobiliaria ooi" +
            " JOIN FETCH oi.departamento" + " JOIN FETCH oi.municipio" +
            " WHERE oi.id = :ofertaInmobiliariaId";

        query1 = entityManager.createQuery(q);
        query1.setParameter("ofertaInmobiliariaId", ofertaInmobiliariaId);

        try {
            answer = (OfertaInmobiliaria) query1.getSingleResult();

            if (answer != null) {

                if (answer.getOfertaPredios() != null &&
                    !answer.getOfertaPredios().isEmpty()) {
                    for (OfertaPredio ofprTemp : answer.getOfertaPredios()) {
                        ofprTemp.getId();
                    }
                }

                if (answer.getOfertaRecursoHidricos() != null &&
                    !answer.getOfertaRecursoHidricos().isEmpty()) {
                    for (OfertaRecursoHidrico orhTemp : answer
                        .getOfertaRecursoHidricos()) {
                        orhTemp.getId();
                    }
                }

                if (answer.getOfertaServicioComunals() != null &&
                    !answer.getOfertaServicioComunals().isEmpty()) {
                    for (OfertaServicioComunal oscTemp : answer
                        .getOfertaServicioComunals()) {
                        oscTemp.getId();
                    }
                }

                if (answer.getOfertaAreaUsoExclusivos() != null &&
                    !answer.getOfertaAreaUsoExclusivos().isEmpty()) {
                    for (OfertaAreaUsoExclusivo oaueTemp : answer
                        .getOfertaAreaUsoExclusivos()) {
                        oaueTemp.getId();
                    }
                }

                if (answer.getFotografias() != null &&
                    !answer.getFotografias().isEmpty()) {
                    for (Fotografia f : answer.getFotografias()) {
                        f.getId();
                    }
                } else {
                    answer.setFotografias(new ArrayList<Fotografia>());
                }

                if (answer.getOfertaInmobiliaria() != null) {

                    if (answer.getOfertaInmobiliaria()
                        .getOfertaPredios() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaPredios().isEmpty()) {
                        for (OfertaPredio ofpTemp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaPredios()) {
                            ofpTemp.getId();
                        }
                    }

                    if (answer.getOfertaInmobiliaria()
                        .getOfertaRecursoHidricos() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaRecursoHidricos().isEmpty()) {
                        for (OfertaRecursoHidrico orhTemp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaRecursoHidricos()) {
                            orhTemp.getId();
                        }
                    }

                    if (answer.getOfertaInmobiliaria()
                        .getOfertaServicioComunals() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaServicioComunals().isEmpty()) {
                        for (OfertaServicioComunal oscTemp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaServicioComunals()) {
                            oscTemp.getId();
                        }
                    }

                    if (answer.getOfertaInmobiliaria()
                        .getOfertaServicioPublicos() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaServicioPublicos().isEmpty()) {
                        for (OfertaServicioPublico ospTemp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaServicioPublicos()) {
                            ospTemp.getId();
                        }
                    }

                    if (answer.getOfertaInmobiliaria()
                        .getOfertaAreaUsoExclusivos() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaAreaUsoExclusivos().isEmpty()) {
                        for (OfertaAreaUsoExclusivo oaueTemp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaAreaUsoExclusivos()) {
                            oaueTemp.getId();
                        }
                    }

                    if (answer.getOfertaInmobiliaria().getFotografias() != null &&
                        !answer.getOfertaInmobiliaria().getFotografias()
                            .isEmpty()) {
                        for (Fotografia f : answer.getOfertaInmobiliaria()
                            .getFotografias()) {
                            f.getId();
                        }
                    } else {
                        answer.setFotografias(new ArrayList<Fotografia>());
                    }
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Se hizo para corregir lo que hizo juan.agudelo de tener dos métodos diferentes: uno para
     * buscar y otro para contar ofertas inmobiilarias.
     *
     * @author pedro.garcia Basado en el de juan.agudelo
     *
     * @param filtro Objeto que contiene los datos por los que se filtra la búsqueda
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     *
     * @param sortOrder nombre del ordenamiento (UNSORTED, DESCENDING, DESCENDING)
     * @param isCount dice si lo que se quiere es contar las filas resultados
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     *
     * @return
     */
    private Object buscarOContarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtro, String sortField, String sortOrder, boolean isCount,
        final int... rowStartIdxAndCount) {

        LOGGER.debug("on OfertaInmobiliariaDAOBean#buscarOContarOfertasInmobiliariasPorFiltro ...");

        List<OfertaInmobiliaria> answer1;
        BigInteger answer2;
        Object answer = null;
        Query query;
        StringBuilder q = new StringBuilder();

        if (!isCount) {
            q.append("SELECT oi FROM OfertaInmobiliaria oi");
            q.append(" JOIN FETCH oi.departamento d");
            q.append(" JOIN FETCH oi.municipio m");
            q.append(" WHERE 1=1");
        } else {
            q.append("SELECT COUNT(oi) FROM OfertaInmobiliaria oi");
            q.append(" JOIN oi.departamento d");
            q.append(" JOIN oi.municipio m");
            q.append(" WHERE 1=1");
        }

        if (filtro.getCodigoOferta() != null && !filtro.getCodigoOferta().isEmpty()) {
            q.append(" AND oi.codigoOferta = :codigoOferta");
        }
        if (filtro.getDepartamentoCodigo() != null &&
            !filtro.getDepartamentoCodigo().isEmpty()) {
            q.append(" AND d.codigo = :departamentoCodigo");
        }
        if (filtro.getMunicipioCodigo() != null && !filtro.getMunicipioCodigo().isEmpty()) {
            q.append(" AND m.codigo = :municipioCodigo");
        }
        if (filtro.getTipoPredio() != null && !filtro.getTipoPredio().isEmpty()) {
            q.append(" AND oi.tipoPredio = :tipoPredio");
        }
        if (filtro.getNumeroPredial() != null && !filtro.getNumeroPredial().isEmpty()) {
            q.append(" AND oi.numeroPredial = :numeroPredial");
        }
        if (filtro.getTipoInmueble() != null &&
            !filtro.getTipoInmueble().isEmpty()) {
            q.append(" AND oi.tipoInmueble = :tipoInmueble");
        }
        if (filtro.getSector() != null && !filtro.getSector().isEmpty()) {
            q.append(" AND oi.sector = :sector");
        }
        if (filtro.getManzanaVereda() != null && !filtro.getManzanaVereda().isEmpty()) {
            q.append(" AND oi.nombreVereda = :nombreVereda");
        }
        if (filtro.getBarrio() != null && !filtro.getBarrio().isEmpty()) {
            q.append(" AND oi.nombreBarrio = :nombreBarrio");
        }
        if (filtro.getDestino() != null && !filtro.getDestino().isEmpty()) {
            q.append(" AND oi.destino = :destino");
        }
        if (filtro.getEstrato() != null && !filtro.getEstrato().isEmpty()) {
            q.append(" AND oi.estratoSocioeconomico = :estratoSocioeconomico");
        }
        if (filtro.getNumeroHabitaciones() != null) {
            q.append(" AND oi.numeroHabitaciones = :numeroHabitaciones");
        }
        if (filtro.getNumeroBanios() != null) {
            if (Integer.valueOf(filtro.getNumeroBanios()) < 4) {
                q.append(" AND oi.numeroBanios = :numeroBanios");
            } else {
                q.append(" AND oi.numeroBanios >= 4");
            }
        }
        if (filtro.getEstado() != null && !filtro.getEstado().isEmpty()) {
            q.append(" AND oi.estado = :estado");
        }
        if (filtro.getResponsableInformacion() != null && !filtro.getResponsableInformacion().
            isEmpty()) {
            q.append(" AND oi.responsableInformacion = :responsableInformacion");
        }
        if (filtro.getRegionCapturaOfertaId() != null) {
            q.append(" AND oi.regionCapturaOfertaId = :regionCapturaOfertaId");
        }
        if (filtro.getTipoInvestigacion() != null && !filtro.getTipoInvestigacion().isEmpty()) {
            q.append(" AND oi.tipoInvestigacion = :tipoInvestigacion");
        }
        if (filtro.getCondicionJuridica() != null && !filtro.getCondicionJuridica().isEmpty()) {
            q.append(" AND oi.condicionJuridica = :condicionJuridica");
        }
        if (filtro.getInvestigacionIngreso() != null && !filtro.getInvestigacionIngreso().isEmpty()) {
            q.append(" AND oi.investigacionIngreso = :investigacionIngreso");
        }
        if (filtro.getVetustez() != null && !filtro.getVetustez().isEmpty()) {
            q.append(" AND oi.vetustez = :vetustez");
        }
        if (filtro.getPrecioOfertaDesde() != null && filtro.getPrecioOfertaHasta() != null) {
            q.append(" AND oi.valorPedido BETWEEN :valorDesde");
            q.append(" AND :valorHasta");
        } else if (filtro.getPrecioOfertaDesde() != null) {
            q.append(" AND oi.valorPedido >= :valorDesde");
        } else if (filtro.getPrecioOfertaHasta() != null) {
            q.append(" AND oi.valorPedido <= :valorHasta");
        }

        if (filtro.getAreaOfertaConstruidaDesde() != null &&
            filtro.getAreaOfertaConstruidaHasta() != null) {
            q.append(" AND oi.areaConstruccion BETWEEN :areaConstruccionDesde");
            q.append(" AND :areaConstruccionHasta");
        } else if (filtro.getAreaOfertaConstruidaDesde() != null) {
            q.append(" AND oi.areaConstruccion >= :areaConstruccionDesde");
        } else if (filtro.getAreaOfertaConstruidaHasta() != null) {
            q.append(" AND oi.areaConstruccion <= :areaConstruccionHasta");
        }

        if (filtro.getAreaOfertaTerrenoDesde() != null &&
            filtro.getAreaOfertaTerrenoHasta() != null) {
            q.append(" AND oi.areaTerreno BETWEEN :areaTerrenoDesde");
            q.append(" AND :areaTerrenoHasta");
        } else if (filtro.getAreaOfertaTerrenoDesde() != null) {
            q.append(" AND oi.areaTerreno >= :areaTerrenoDesde");
        } else if (filtro.getAreaOfertaTerrenoHasta() != null) {
            q.append(" AND oi.areaTerreno <= :areaTerrenoHasta");
        }

        if (filtro.getFechaOfertaDesde() != null &&
            filtro.getFechaOfertaHasta() != null) {
            q.append(" AND oi.fechaOferta BETWEEN :fechaOfertaDesde");
            q.append(" AND :fechaOfertaHasta");
        } else if (filtro.getFechaOfertaDesde() != null) {
            q.append(" AND oi.fechaOferta >= :fechaOfertaDesde");
        } else if (filtro.getFechaOfertaHasta() != null) {
            q.append(" AND oi.fechaOferta <= :fechaOfertaHasta");
        }

        if (filtro.getFechaIngresoOfertaDesde() != null &&
            filtro.getFechaIngresoOfertaHasta() != null) {
            q.append(" AND oi.fechaIngresoOferta BETWEEN :fechaIngresoOfertaDesde");
            q.append(" AND :fechaIngresoOfertaHasta");
        } else if (filtro.getFechaIngresoOfertaDesde() != null) {
            q.append(" AND oi.fechaIngresoOferta >= :fechaIngresoOfertaDesde");
        } else if (filtro.getFechaIngresoOfertaHasta() != null) {
            q.append(" AND oi.fechaIngresoOferta <= :fechaIngresoOfertaHasta");
        }

        //D: usar el ordenamiento
        if (!isCount) {
            if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
                q.append(" ORDER BY oi.").append(sortField).append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }
        }

        query = entityManager.createQuery(q.toString());

        if (filtro.getCodigoOferta() != null && !filtro.getCodigoOferta().isEmpty()) {
            query.setParameter("codigoOferta", filtro.getCodigoOferta());
        }
        if (filtro.getDepartamentoCodigo() != null && !filtro.getDepartamentoCodigo().isEmpty()) {
            query.setParameter("departamentoCodigo", filtro.getDepartamentoCodigo());
        }
        if (filtro.getMunicipioCodigo() != null && !filtro.getMunicipioCodigo().isEmpty()) {
            query.setParameter("municipioCodigo", filtro.getMunicipioCodigo());
        }
        if (filtro.getTipoPredio() != null && !filtro.getTipoPredio().isEmpty()) {
            query.setParameter("tipoPredio", filtro.getTipoPredio());
        }
        if (filtro.getNumeroPredial() != null && !filtro.getNumeroPredial().isEmpty()) {
            query.setParameter("numeroPredial", filtro.getNumeroPredial());
        }
        if (filtro.getTipoInmueble() != null && !filtro.getTipoInmueble().isEmpty()) {
            query.setParameter("tipoInmueble", filtro.getTipoInmueble());
        }
        if (filtro.getEstado() != null && !filtro.getEstado().isEmpty()) {
            query.setParameter("estado", filtro.getEstado());
        }
        if (filtro.getResponsableInformacion() != null && !filtro.getResponsableInformacion().
            isEmpty()) {
            query.setParameter("responsableInformacion", filtro.getResponsableInformacion());
        }
        if (filtro.getRegionCapturaOfertaId() != null) {
            query.setParameter("regionCapturaOfertaId", filtro.getRegionCapturaOfertaId());
        }
        if (filtro.getSector() != null && !filtro.getSector().isEmpty()) {
            query.setParameter("sector", filtro.getSector());
        }
        if (filtro.getManzanaVereda() != null && !filtro.getManzanaVereda().isEmpty()) {
            query.setParameter("nombreVereda", filtro.getManzanaVereda());
        }
        if (filtro.getBarrio() != null && !filtro.getBarrio().isEmpty()) {
            query.setParameter("nombreBarrio", filtro.getBarrio());
        }
        if (filtro.getDestino() != null && !filtro.getDestino().isEmpty()) {
            query.setParameter("destino", filtro.getDestino());
        }
        if (filtro.getEstrato() != null && !filtro.getEstrato().isEmpty()) {
            query.setParameter("estratoSocioeconomico", filtro.getEstrato());
        }
        if (filtro.getNumeroHabitaciones() != null) {
            query.setParameter("numeroHabitaciones", filtro.getNumeroHabitaciones());
        }
        if (filtro.getNumeroBanios() != null) {
            if (filtro.getNumeroBanios().intValue() < 4) {
                query.setParameter("numeroBanios", filtro.getNumeroBanios());
            }
        }
        if (filtro.getTipoInvestigacion() != null &&
            !filtro.getTipoInvestigacion()
                .isEmpty()) {
            query.setParameter("tipoInvestigacion",
                filtro.getTipoInvestigacion());
        }
        if (filtro.getCondicionJuridica() != null && !filtro.getCondicionJuridica().isEmpty()) {
            query.setParameter("condicionJuridica", filtro.getCondicionJuridica());
        }
        if (filtro.getInvestigacionIngreso() != null &&
            !filtro.getInvestigacionIngreso().isEmpty()) {
            query.setParameter("investigacionIngreso", filtro.getInvestigacionIngreso());
        }
        if (filtro.getVetustez() != null && !filtro.getVetustez().isEmpty()) {
            query.setParameter("vetustez", filtro.getVetustez());
        }
        if (filtro.getPrecioOfertaDesde() != null && filtro.getPrecioOfertaHasta() != null) {
            query.setParameter("valorDesde", filtro.getPrecioOfertaDesde());
            query.setParameter("valorHasta", filtro.getPrecioOfertaHasta());
        } else if (filtro.getPrecioOfertaDesde() != null) {
            query.setParameter("valorDesde", filtro.getPrecioOfertaDesde());
        } else if (filtro.getPrecioOfertaHasta() != null) {
            query.setParameter("valorHasta", filtro.getPrecioOfertaHasta());
        }

        if (filtro.getAreaOfertaConstruidaDesde() != null &&
            filtro.getAreaOfertaConstruidaHasta() != null) {
            query.setParameter("valorDesde", filtro.getPrecioOfertaDesde());
            query.setParameter("areaConstruccionDesde", filtro.getAreaOfertaConstruidaDesde());
            query.setParameter("areaConstruccionHasta", filtro.getAreaOfertaConstruidaHasta());
        } else if (filtro.getAreaOfertaConstruidaDesde() != null) {
            query.setParameter("areaConstruccionDesde", filtro.getAreaOfertaConstruidaDesde());
        } else if (filtro.getAreaOfertaConstruidaHasta() != null) {
            query.setParameter("areaConstruccionHasta", filtro.getAreaOfertaConstruidaHasta());
        }

        if (filtro.getAreaOfertaTerrenoDesde() != null &&
            filtro.getAreaOfertaTerrenoHasta() != null) {
            query.setParameter("areaTerrenoDesde", filtro.getAreaOfertaTerrenoDesde());
            query.setParameter("areaTerrenoHasta", filtro.getAreaOfertaTerrenoHasta());
        } else if (filtro.getAreaOfertaTerrenoDesde() != null) {
            query.setParameter("areaTerrenoDesde", filtro.getAreaOfertaTerrenoDesde());
        } else if (filtro.getAreaOfertaTerrenoHasta() != null) {
            query.setParameter("areaTerrenoHasta", filtro.getAreaOfertaTerrenoHasta());
        }

        if (filtro.getFechaOfertaDesde() != null &&
            filtro.getFechaOfertaHasta() != null) {
            query.setParameter("fechaOfertaDesde", filtro.getFechaOfertaDesde());
            query.setParameter("fechaOfertaHasta", filtro.getFechaOfertaHasta());
        } else if (filtro.getFechaOfertaDesde() != null) {
            query.setParameter("fechaOfertaDesde", filtro.getFechaOfertaDesde());
        } else if (filtro.getFechaOfertaHasta() != null) {
            query.setParameter("fechaOfertaHasta", filtro.getFechaOfertaHasta());
        }

        if (filtro.getFechaIngresoOfertaDesde() != null &&
            filtro.getFechaIngresoOfertaHasta() != null) {
            query.setParameter("fechaIngresoOfertaDesde", filtro.getFechaIngresoOfertaDesde());
            query.setParameter("fechaIngresoOfertaHasta", filtro.getFechaIngresoOfertaHasta());
        } else if (filtro.getFechaIngresoOfertaDesde() != null) {
            query.setParameter("fechaIngresoOfertaDesde", filtro.getFechaIngresoOfertaDesde());
        } else if (filtro.getFechaIngresoOfertaHasta() != null) {
            query.setParameter("fechaIngresoOfertaHasta", filtro.getFechaIngresoOfertaHasta());
        }

        try {
            answer = (List<OfertaInmobiliaria>) super.findInRangeUsingQuery(
                query, rowStartIdxAndCount);

            if (!isCount) {
                answer1 = (List<OfertaInmobiliaria>) super.findInRangeUsingQuery(query,
                    rowStartIdxAndCount);
                for (OfertaInmobiliaria ofertaInmobiliaria : answer1) {
                    if (ofertaInmobiliaria.getOfertaPredios() != null) {
                        ofertaInmobiliaria.getOfertaPredios().size();
                    }
                    if (ofertaInmobiliaria.getFotografias() != null) {
                        ofertaInmobiliaria.getFotografias().size();
                    }
                }

                answer = answer1;
            } else {
                answer2 = new BigInteger(query.getSingleResult().toString());
                answer = answer2;
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "OfertaInmobiliariaDAOBean#buscarOContarOfertasInmobiliariasPorFiltro");
        }

        return answer;
    }

    /**
     * @see IOfertaInmobiliariaDAO#buscarListaOfertasInmobiliariasPorRecolector(String, Long,
     * String)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<OfertaInmobiliaria> buscarListaOfertasInmobiliariasPorRecolector(
        String recolectorId, Long regionId, String estado) {
        List<OfertaInmobiliaria> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT oi" +
            " LEFT JOIN FETCH oi.ofertaPredios " +
            " FROM OfertaInmobiliaria oi" +
            " WHERE oi.recolectorId = :idRecolector" +
            " AND oi.regionCapturaOfertaId = :idRegion" +
            " AND oi.estado = :estado";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", recolectorId);
            query.setParameter("idRegion", regionId);
            query.setParameter("estado", estado);

            answer = (List<OfertaInmobiliaria>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

//end of class
}
