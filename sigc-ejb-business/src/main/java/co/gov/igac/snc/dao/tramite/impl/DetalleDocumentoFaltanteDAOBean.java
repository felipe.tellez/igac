package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IDetalleDocumentoFaltanteDAO;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * Bean para los métodos de bd de la DETALLE_DOCUMENTO_FALTANTE
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class DetalleDocumentoFaltanteDAOBean extends
    GenericDAOWithJPA<DetalleDocumentoFaltante, Long> implements
    IDetalleDocumentoFaltanteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        DetalleDocumentoFaltanteDAOBean.class);

    /**
     * @see IDetalleDocumentoFaltanteDAO#obtenerDetallesDocumentosFaltantesPorIdSolicitud(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<DetalleDocumentoFaltante> obtenerDetallesDocumentosFaltantesPorIdSolicitud(
        Long idSolicitud) {

        List<DetalleDocumentoFaltante> result = null;
        Query query;
        String queryString;

        queryString = "SELECT ddf" +
            " FROM DetalleDocumentoFaltante ddf," +
            " SolicitudDocumento solDoc" +
            " LEFT JOIN FETCH ddf.tipoDocumento td" +
            " WHERE" +
            " ddf.solicitudDocumento.id = solDoc.id" +
            " AND" +
            " solDoc.solicitud.id =:idSolicitud";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);

            result = (List<DetalleDocumentoFaltante>) query.getResultList();

            for (DetalleDocumentoFaltante ddf : result) {
                Hibernate.initialize(ddf.getTipoDocumento());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER,
                    e,
                    "DetalleDocumentoFaltanteDAOBean#obtenerDetallesDocumentosFaltantesPorIdSolicitud");

        }

        return result;
    }

}
