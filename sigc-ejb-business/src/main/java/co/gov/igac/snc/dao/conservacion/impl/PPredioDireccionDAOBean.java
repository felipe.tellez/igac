package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDireccionDAO;
import co.gov.igac.snc.dao.generales.impl.DominioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class PPredioDireccionDAOBean extends GenericDAOWithJPA<PPredioDireccion, Long> implements
    IPPredioDireccionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DominioDAOBean.class);

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDireccionDAO#getPPredioDireccionesByPredioId(java.lang.Long)
     */
    @Override
    public List<PPredioDireccion> getPPredioDireccionesByPredioId(Long predioId) {

        List<PPredioDireccion> answer = null;

        String query = "SELECT ppd FROM PPredioDireccion ppd " +
            " JOIN ppd.PPredio pp " +
            " WHERE ppd.PPredio.id =:predioId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("predioId", predioId);

            answer = (List<PPredioDireccion>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDireccionDAOBean#getPPredioDireccionesByPredioId");
        }
        return answer;
    }

    /**
     * @author felipe.cadena
     * @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDireccionDAO#obtenerDireccionPrincipalFichaMatriz(java.lang.String)
     */
    @Override
    public PPredioDireccion obtenerDireccionPrincipalFichaMatriz(String numeroPredial) {

        PPredioDireccion answer = null;

        String query = "SELECT distinct ppd.* FROM" +
            " P_PREDIO_DIRECCION PPD " +
            " INNER JOIN P_PREDIO PP ON PP.ID = PPD.PREDIO_ID" +
            " INNER JOIN P_FICHA_MATRIZ PFM ON PP.ID = PFM.PREDIO_ID" +
            " INNER JOIN P_FICHA_MATRIZ_PREDIO PFMP ON PFM.ID = PFMP.FICHA_MATRIZ_ID" +
            " WHERE PFMP.NUMERO_PREDIAL LIKE '" + numeroPredial + "%'" +
            " AND ppd.principal = 'SI'";

        try {
            Query q = this.entityManager.createNativeQuery(query, PPredioDireccion.class);

            answer = (PPredioDireccion) q.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDireccionDAOBean#getPPredioDireccionesByPredioId");
        }
        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see
     * co.gov.igac.snc.dao.conservacion.IPPredioDireccionDAO#obtenerDireccionPrincipalFichaMatrizId(java.lang.Long)
     */
    @Override
    public PPredioDireccion obtenerDireccionPrincipalFichaMatrizId(Long predioId) {

        PPredioDireccion answer = null;

        String query = "SELECT ppd FROM PPredioDireccion ppd " +
            " WHERE ppd.PPredio.id =:predioId" +
            " AND ppd.principal = 'SI'";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("predioId", predioId);

            answer = (PPredioDireccion) q.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PPredioDireccionDAOBean#obtenerDireccionPrincipalFichaMatrizId");
        }
        return answer;
    }

}
