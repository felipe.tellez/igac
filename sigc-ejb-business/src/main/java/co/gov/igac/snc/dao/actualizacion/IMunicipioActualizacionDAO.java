/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.dao.actualizacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import java.util.List;

/**
 *
 * @author felipe.cadena
 */
public interface IMunicipioActualizacionDAO extends IGenericJpaDAO<MunicipioActualizacion, Long> {

    /**
     * Obtiene los procesos de actualizacion relacionados a un municipio que se encuentran en un
     * estado en paticular, si el paramtro estado es null retorna todos los proceso del municipio
     *
     *
     * @author felipe.cadena
     * @param municpioCodigo
     * @param estado
     * @param activo
     * @param vigencia
     * @return
     */
    public List<MunicipioActualizacion> obtenerPorMunicipioYEstado(String municpioCodigo,
        String estado, Boolean activo, Integer vigencia);
    
    /**
     * @author hector.arias
     * @param municpioCodigo
     * @param estado
     * @param activo
     * @param vigencia
     * @param ubucacionGeografico
     * @return 
     */
    public List <MunicipioActualizacion> obtenerPorMunicipioYEstadoGeografico(String municpioCodigo,
        String estado, Boolean activo, Integer vigencia);

    /**
     * Obtiene los procesos de actualizacion relacionados a un municipio y un departamento que se
     * encuentran en un estado en paticular, si el paramtro estado es null retorna todos los proceso
     * del municipio
     *
     *
     * @author leidy.gonzalez
     * @param municpioCodigo
     * @param departamentoCodigo
     * @return
     */
    public List<MunicipioActualizacion> obtenerEstadoTramitePorDepartamentoMunicipio(
        String municpioCodigo, String departamentoCodigo);

    /**
     * Obtiene los municipios actualizacion asociados a los tramites del parametro
     *
     * @author felipe.cadena
     * @param idsTramites
     * @return
     */
    public List<MunicipioActualizacion> obtenerPorIdsTramites(List<Long> idsTramites);

}
