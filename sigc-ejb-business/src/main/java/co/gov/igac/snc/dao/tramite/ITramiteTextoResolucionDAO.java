package co.gov.igac.snc.dao.tramite;

import javax.ejb.Local;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;

/**
 *
 * @author javier.aponte
 *
 */
@Local
public interface ITramiteTextoResolucionDAO extends
    IGenericJpaDAO<TramiteTextoResolucion, Long> {

    public TramiteTextoResolucion findTramiteTextoResolucionPorIdTramite(Long tramiteId);

}
