package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;

/**
 * Interfaz para los servicios de persistencia del objeto {@link ModeloResolucion}.
 *
 * @author david.cifuentes
 */
@Local
public interface IModeloResolucionDAO extends
    IGenericJpaDAO<ModeloResolucion, Long> {

    /**
     * Método que busca un {@link ModeloResolucion} por el tipo de tramite y la clase de mutación.
     *
     * @author david.cifuentes
     *
     * @param String tipoTramite, String claseMutacion
     * @return
     */
    public List<ModeloResolucion> findModelosResolucionByTramite(
        String tipoTramite, String claseMutacion);

    /**
     * Método que busca un {@link ModeloResolucion} por el tipo de tramite y la clase de mutación y
     * subtipo.
     *
     * @author fredy.wilches
     *
     * @param String tipoTramite, String claseMutacion, String subtipo
     * @return
     */
    public List<ModeloResolucion> findModelosResolucionByTramite(
        String tipoTramite, String claseMutacion, String subtipo);
    
    /**
     * Método que busca un {@link ModeloResolucion} por el tipo de tramite, la clase de mutación,
     * subtipo y la delegada.
     *
     * @author carlos.ferro
     *
     * @param String tipoTramite, String claseMutacion, String subtipo, String delegada
     * @return
     */
    public List<ModeloResolucion> findModelosResolucionByTramite(
        String tipoTramite, String claseMutacion, String subtipo, String delegada);    

}
