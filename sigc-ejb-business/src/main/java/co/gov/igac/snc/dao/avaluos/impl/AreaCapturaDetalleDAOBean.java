/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

/**
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Stateless
@Deprecated
//TODO :: mover los métodos que sirvan y eliminar esta clase

public class AreaCapturaDetalleDAOBean {

//    extends
//		GenericDAOWithJPA<AreaCapturaDetalle, Long> implements
//		IAreaCapturaDetalleDAO {
//
//	private static final Logger LOGGER = LoggerFactory
//			.getLogger(AreaCapturaDetalleDAOBean.class);
//
//	/**
//	 * @see IAreaCapturaDetalleDAOBean#getDetallesAreaByRecolectorIdAreaId(java.lang.String, java.lang.Long)
//	 * @author rodrigo.hernandez
//	 */
//	@Override
//	public List<String> cargarDetallesAreaOfertaMunicipioDescentralizadoRecolector(
//			String recolectorId, Long areaId) {
//		LOGGER.debug("on AreaCapturaDetalleDAOBean#getDetallesAreaByRecolectorIdAreaId ...");
//
//		List<AreaCapturaDetalle> queryAnswer = null;
//		List<String>listaManzanasVeredasArea = new ArrayList<String>();
//		String queryString;
//		Query query;
//
//		queryString = "SELECT acd FROM AreaCapturaDetalle acd"
//				+ " WHERE acd.asignadoRecolectorId = :recolectorId"
//				+ " AND acd.areaCapturaOferta.id = :areaId";
//
//		LOGGER.debug(queryString);
//
//		try {
//			query = this.entityManager.createQuery(queryString);
//			query.setParameter("recolectorId", recolectorId);
//			query.setParameter("areaId", areaId);
//
//			queryAnswer = query.getResultList();
//
//			for(AreaCapturaDetalle detalle : queryAnswer){
//				listaManzanasVeredasArea.add(detalle.getManzanaVeredaCodigo());
//			}
//
//		} catch (Exception e) {
//			throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
//					LOGGER, e, e.getMessage());
//		}
//
//		return listaManzanasVeredasArea;
//	}
//
//	/**
//	 * @see IAreaCapturaOfertaDAO#getInformacionRecolectores(List<String>)
//	 * @author rodrigo.hernandez
//	 *
//	 **/
//
//	@Override
//	public List<FuncionarioRecolector> getInformacionRecolectores(
//			List<String> listaRecolectores) {
//
//		LOGGER.debug("on AreaCapturaDetalleDAOBean#getInformacionRecolectores ...");
//
//		StringTokenizer tokenizer= null;
//
//		//Variable para asignar id a un elemento FuncionarioRecolector dentro de la lista
//		int idRecolector = -1;
//
//		//Variable para almacenar el id del recolector y su nombre a partir de un String
//		List<String> recolectorCampos = null;
//
//		//Variable auxiliar para almacenar datos de un recolector
//		FuncionarioRecolector recolector = null;
//
//		//Lista con datos de los recolectores
//		List<FuncionarioRecolector> recolectores = new ArrayList<FuncionarioRecolector>();
//
//		//Query para consultar si un recolector tiene asignaciones
//		String stringQueryAsignaciones;
//		Query queryAsignaciones;
//
//		Integer resultadoConteo;
//
//
//
//		for(String recolectorStr : listaRecolectores){
//
//			recolectorCampos = new ArrayList<String>();
//			recolector = new FuncionarioRecolector();
//
//			stringQueryAsignaciones = "" +
//					" SELECT COUNT (*)" +
//					" FROM AreaCapturaDetalle acd" +
//					" WHERE acd.asignadoRecolectorId = :recolectorId" +
//					" AND (acd.estado = :estado1 or acd.estado = :estado2)";
//
//			try {
//
//				tokenizer= new StringTokenizer(recolectorStr, "-");
//				while (tokenizer.hasMoreElements()) {
//			            String token = tokenizer.nextToken();
//			            LOGGER.debug(token);
//			            recolectorCampos.add(token);
//			     }
//
//				queryAsignaciones = this.entityManager.createQuery(stringQueryAsignaciones);
//				queryAsignaciones.setParameter("recolectorId", recolectorCampos.get(0));
//				queryAsignaciones.setParameter("estado1", EEstadoOfertaCapturaDetalle.EN_EJECUCION.getCodigo());
//				queryAsignaciones.setParameter("estado2", EEstadoOfertaCapturaDetalle.CONTROL_CALIDAD.getCodigo());
//
//				resultadoConteo = Integer.valueOf(String.valueOf(queryAsignaciones.getSingleResult()));
//
//				idRecolector = idRecolector + 1;
//				recolector.setId(idRecolector);
//				recolector.setLogin(recolectorCampos.get(0));
//				recolector.setNombreFuncionario(recolectorCampos.get(1));
//				recolector.setSeleccionado(false);
//
//				if(resultadoConteo > 0){
//					recolector.setTieneAsignaciones(ESiNo.SI.getCodigo());
//				}else{
//					recolector.setTieneAsignaciones(ESiNo.NO.getCodigo());
//				}
//
//				recolectores.add(recolector);
//
//			} catch (Exception e) {
//				throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
//						LOGGER, e, e.getMessage());
//			}
//		}
//
//		LOGGER.debug("out of AreaCapturaDetalleDAOBean#getInformacionRecolectores ...");
//		return recolectores;
//	}
//
//	//TODO Eliminar metodo :: rodrigo.hernandez
//	@Override
//	public AreaCapturaDetalle getAreaDetalleByManzanaVeredaCodigo(
//			String manzanaVeredaCodigo, Long areaOfertaId) {
//
//		AreaCapturaDetalle detalle = null;
//
//		String queryString = "SELECT acd " +
//				"FROM AreaCapturaDetalle acd " +
//				"WHERE acd.manzanaVeredaCodigo = :codigoMV " +
//				"AND acd.areaCapturaOferta.id = :areaOfertaCod";
//
//		Query query;
//
//		try {
//
//
//			query = this.entityManager.createQuery(queryString);
//			query.setParameter("codigoMV", manzanaVeredaCodigo);
//			query.setParameter("areaOfertaCod", areaOfertaId);
//			detalle = (AreaCapturaDetalle) query.getSingleResult();
//
//		} catch (Exception e) {
//			throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
//					LOGGER, e, e.getMessage());
//		}
//
//		return detalle;
//	}
//
//	/**
//	 * @see IAreaCapturaDetalleDAO#getAreasCapturaDetalleConRecolectorSinComisionByAreaId(Long)
//	 * @author christian.rodriguez
//	 *
//	 **/
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<AreaCapturaDetalle> getAreasCapturaDetalleConRecolectorSinComisionByAreaId(Long areaCapturaId) {
//
//		LOGGER.debug("on AreaCapturaDetalleDAO#getAreasCapturaDetalleConRecolectorSinComisionByAreaId ...");
//
//		List<AreaCapturaDetalle> answer = null;
//		String queryString;
//		Query query;
//
//		queryString = "SELECT acd " +
//				"FROM AreaCapturaDetalle acd " +
//				"WHERE acd.comisionOferta = null " +
//				"AND acd.asignadoRecolectorId != null " +
//				"AND acd.areaCapturaOferta.id = :areaCapturaId";
//
//		LOGGER.debug(queryString);
//
//		try {
//			query = this.entityManager.createQuery(queryString);
//			query.setParameter("areaCapturaId", areaCapturaId);
//
//			answer = (List<AreaCapturaDetalle>) query.getResultList();
//
//		} catch (Exception e) {
//			throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
//					.getExcepcion(LOGGER, e,
//							"AreaCapturaDetalleDAO#getAreasCapturaDetalleConRecolectorSinComisionByAreaId");
//		}
//
//		return answer;
//	}
//
//	/**
//	 * @see IAreaCapturaDetalleDAO#getAreasCapturaDetalleByComisionOfertaID(Long)
//	 * @author christian.rodriguez
//	 *
//	 **/
//	@SuppressWarnings("unchecked")
//	@Implement
//	@Override
//	public List<AreaCapturaDetalle> getAreasCapturaDetalleByComisionOfertaID(Long comisionOfertaId) {
//
//		LOGGER.debug("on AreaCapturaDetalleDAO#getAreasCapturaDetalleByComisionOfertaID ...");
//
//		List<AreaCapturaDetalle> answer = null;
//		String queryString;
//		Query query;
//
//		queryString = "SELECT acd " +
//				"FROM AreaCapturaDetalle acd " +
//				"LEFT JOIN FETCH acd.areaCapturaOferta aco " +
//				"LEFT JOIN FETCH aco.departamento " +
//				"LEFT JOIN FETCH aco.municipio " +
//				"LEFT JOIN FETCH aco.zona " +
//				"WHERE acd.comisionOferta.id = :idComision";
//
//		LOGGER.debug(queryString);
//
//		try {
//			query = this.entityManager.createQuery(queryString);
//			query.setParameter("idComision", comisionOfertaId);
//
//			answer = (List<AreaCapturaDetalle>) query.getResultList();
//
//		} catch (Exception e) {
//			throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
//					"AreaCapturaDetalleDAO#getAreasCapturaDetalleByComisionOfertaID");
//		}
//
//		return answer;
//	}
//
//	/**
//	 * @see IAreaCapturaDetalleDAO#getDetallesAreaByRecolectorId(String, String, String)
//	 * @author rodrigo.hernandez
//	 *
//	 **/
//	@Override
//	public List<String> getDetallesAreaByRecolectorId(String idRecolector) {
//		List<String> answer = null;
//		String queryString;
//		Query query;
//
//		queryString = "SELECT acd.manzanaVeredaCodigo" +
//				" FROM AreaCapturaDetalle acd, AreaCapturaOferta aco " +
//				" WHERE acd.asignadoRecolectorId = :idRecolector" +
//			    " AND acd.areaCapturaOferta.id = aco.id";
//
//		LOGGER.debug(queryString);
//
//		try {
//			query = this.entityManager.createQuery(queryString);
//			query.setParameter("idRecolector", idRecolector);
//
//			answer = (List<String>) query.getResultList();
//
//		} catch (Exception e) {
//			throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, e,
//					e.getMessage());
//		}
//
//		return answer;
//	}
//
//	@Override
//	public List<AreaCapturaDetalle> getDetallesAreaByRecolectorIdAreaId(
//			String recolectorId, Long areaId) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
}
