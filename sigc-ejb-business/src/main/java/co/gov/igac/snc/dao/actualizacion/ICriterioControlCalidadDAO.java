package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.CriterioControlCalidad;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;

/**
 * Servicios de persistencia para el objeto LevantamientoAsignacion
 *
 * @author javier.barajas
 */
@Local
public interface ICriterioControlCalidadDAO extends IGenericJpaDAO<CriterioControlCalidad, Long> {

    /**
     * Método que consulta los Criterios de Control de Calidad
     *
     * @param criterioControlCalidadId
     * @return CriterioControlCalidad
     */
    public CriterioControlCalidad consultarCriterioControlCalidad(Long criterioControlCalidadId);

}
