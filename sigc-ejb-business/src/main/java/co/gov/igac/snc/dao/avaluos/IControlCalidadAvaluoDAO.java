package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity ProfesionalAvaluo
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IControlCalidadAvaluoDAO extends IGenericJpaDAO<ControlCalidadAvaluo, Long> {

    /**
     * Método para obtener un control de calidad de avalúos con el profesional asociados y las
     * revisiones relacionadas
     *
     * @author felipe.cadena
     * @param idControlCalidad
     * @return
     */
    public ControlCalidadAvaluo obtenerPorIdConAtributos(
        Long idControlCalidad);

    /**
     * Método para cargar todo los controles de calidad relacionados a un avalúo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param idAsignacion - Asignación actual del avalúo
     * @return
     */
    public List<ControlCalidadAvaluo> consultarTodosPorAvaluoId(Long idAvaluo, Long idAsignacion);

    /**
     * Método para consultar un control de calidad relacionado a un avalúo, una asignación y una
     * actividad en particular
     *
     * @param idAvaluo
     * @param asignacionId
     * @param actividad {@link EAvaluoAsignacionProfActi}
     * @return
     */
    public ControlCalidadAvaluo consultarPorAvaluoAsignacionActividad(Long idAvaluo,
        Long asignacionId, String actividad);

}
