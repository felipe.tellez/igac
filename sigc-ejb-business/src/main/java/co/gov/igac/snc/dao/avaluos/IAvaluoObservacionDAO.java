package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoObservacion;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoObservacion
 *
 * @author felipe.cadena
 */
@Local
public interface IAvaluoObservacionDAO extends IGenericJpaDAO<AvaluoObservacion, Long> {

    /**
     * Método para consultar las observaciones relacionadas a un avalúo
     *
     * @param idAvaluo
     * @return
     */
    public List<AvaluoObservacion> buscarPorAvaluo(Long idAvaluo);

}
