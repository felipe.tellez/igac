package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioZonaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioZona;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoPredioZonaDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoPredioZonaDAOBean extends
    GenericDAOWithJPA<AvaluoPredioZona, Long> implements IAvaluoPredioZonaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaluoPredioZonaDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoPredioZonaDAO#calcularZonasPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Boolean calcularZonasPreviasAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "SELECT o FROM AvaluoPredioZona o " +
            "JOIN o.avaluo ava WHERE ava.id = :idAvaluo";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            List<AvaluoPredioZona> apzs = query.getResultList();
            if (apzs == null || apzs.isEmpty()) {
                return null;
            }

            if (apzs.get(0).getAvaluoPredioId() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#calcularZonasPreviasAvaluo");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoPredioZonaDAO#consultarPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioZona> consultarPorAvaluo(long avaluoId) {

        LOGGER.debug("on AvaluoPredioZonaDAOBean#consultarPorAvaluo");

        List<AvaluoPredioZona> answer = null;
        String queryString;
        Query query;

        queryString = "" +
            " SELECT apz FROM AvaluoPredioZona apz WHERE apz.avaluo.id = :idAvaluo_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo_p", avaluoId);
            answer = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "AvaluoPredioZonaDAOBean#consultarPorAvaluo");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoPredioZonaDAO#eliminarZonasPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public boolean eliminarZonasPreviasAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "DELETE FROM avaluo_predio_zona apz WHERE apz.avaluo_id = " + idAvaluo;

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.executeUpdate();
            return true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#eliminarZonasPreviasAvaluo");
        }
    }

//end of class
}
