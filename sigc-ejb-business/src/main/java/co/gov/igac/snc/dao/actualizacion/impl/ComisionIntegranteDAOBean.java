package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IComisionIntegranteDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ComisionIntegrante;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto ComisionIntegrante.
 *
 * @author franz.gamba
 */
@Stateless
public class ComisionIntegranteDAOBean extends GenericDAOWithJPA<ComisionIntegrante, Long>
    implements IComisionIntegranteDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ComisionIntegranteDAOBean.class);

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see IComisionIntegranteDAO#getcomisionIntegrantesByActualizacionIdAndObjeto(Long, String)
     */
    @Override
    public List<ComisionIntegrante> getcomisionIntegrantesByActualizacionIdAndObjeto(
        Long actualizacionId, String objeto) {
        List<ComisionIntegrante> integrantes = null;

        String query = "SELECT ci FROM ComisionIntegrante ci " +
            " JOIN FETCH ci.actualizacionComision ac " +
            " JOIN ac.actualizacion a " +
            " JOIN FETCH ci.recursoHumano" +
            " WHERE ac.objeto = :objeto " +
            " AND a.id = :actualizacionId";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("objeto", objeto);
            q.setParameter("actualizacionId", actualizacionId);

            integrantes = (List<ComisionIntegrante>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e,
                    "ActualizacionComisionDAOBean#getActualizacionComisionByActualizacionId");
        }
        return integrantes;
    }

}
