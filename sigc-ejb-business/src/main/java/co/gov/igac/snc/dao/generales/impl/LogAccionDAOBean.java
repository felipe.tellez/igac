package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ILogAccionDAO;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementaciôn del DAO para LogAccion
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class LogAccionDAOBean extends GenericDAOWithJPA<LogAccion, Long> implements ILogAccionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogAccionDAOBean.class);

}
