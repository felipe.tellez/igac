package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDocumentacionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumentacion;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionDocumentacion.
 *
 * @author franz.gamba
 */
@Stateless
public class ActualizacionDocumentacionDAOBean extends GenericDAOWithJPA<ActualizacionDocumentacion, Long>
    implements IActualizacionDocumentacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDocumentacionDAOBean.class);

}
