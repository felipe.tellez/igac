/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IColindanteDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Colindante;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los métodos de bd de la tabla COLINDANTE
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Stateless
public class ColindanteDAOBean extends GenericDAOWithJPA<Colindante, Long>
    implements IColindanteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ColindanteDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IColindanteDAO#isAdjacent(java.lang.String, java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean isAdjacent(String este, List<String> estosOtros) {

        boolean answer = false;

        Query query;
        StringBuilder queryString;
        BigInteger count;

        queryString = new StringBuilder();
        queryString.append(
            "SELECT COUNT(c) FROM Colindante c WHERE c.numeroPredial = :numPredialP ");
        queryString.append(" AND c.numeroPredialColindante IN (");
        for (String np : estosOtros) {
            queryString.append("'").append(np).append("',");
        }

        queryString.deleteCharAt(queryString.lastIndexOf(","));
        queryString.append(")");

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("numPredialP", este);
            count = new BigInteger(query.getSingleResult().toString());

            if (count.intValue() == estosOtros.size()) {
                answer = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción en el query para saber si los predios son colindantes: " +
                ex.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, ex,
                "ColindanteDAOBean#isAdjacent");
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see IColindanteDAO#findPrediosColindantesByNumeroPredial(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> findPrediosColindantesByNumeroPredial(
        String numeroPredial) {

        List<String> answer = null;
        Query query;
        String q;

        q = "SELECT DISTINCT c.numeroPredialColindante FROM Colindante c" +
            " WHERE c.numeroPredial = :numeroPredial";

        try {
            query = entityManager.createQuery(q);
            query.setParameter("numeroPredial", numeroPredial);

            answer = (List<String>) query.getResultList();
            String numOrg = null;
            for (String num : answer) {
                if (num.equals(numeroPredial)) {
                    numOrg = num;
                }
            }
            answer.remove(numOrg);

        } catch (IndexOutOfBoundsException ne) {
            return new ArrayList<String>();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see IColindanteDAO#buscarColindantesNumeroPredial(String)
     * @author felipe.cadena
     * @version 2.0
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Colindante> buscarColindantesNumeroPredial(
        String numeroPredial) {

        List<Colindante> answer = null;
        Query query;
        String q;

        q = "SELECT DISTINCT c FROM Colindante c" +
            " WHERE c.numeroPredial = :numeroPredial";

        query = entityManager.createQuery(q);
        query.setParameter("numeroPredial", numeroPredial);
        try {
            answer = (List<Colindante>) query.getResultList();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

//end of class
}
