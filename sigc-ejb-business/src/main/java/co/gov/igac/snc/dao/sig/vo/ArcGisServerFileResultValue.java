/**
 *
 */
package co.gov.igac.snc.dao.sig.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author juan.mendez
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArcGisServerFileResultValue implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5180713599721459334L;

    /**
     *
     */
    private ArcGisServerFileUrl value;

    public ArcGisServerFileUrl getValue() {
        return value;
    }

    public void setValue(ArcGisServerFileUrl value) {
        this.value = value;
    }

    public String getUrl() {
        return this.value.getUrl();
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
