package co.gov.igac.snc.fachadas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.formacion.ICalificacionAnexoDAO;
import co.gov.igac.snc.dao.formacion.IDecretoAvaluoAnualDAO;
import co.gov.igac.snc.dao.formacion.IDecretoCondicionDAO;
import co.gov.igac.snc.dao.formacion.IParametroDecretoCondicionDAO;
import co.gov.igac.snc.dao.formacion.ITablaTerrenoDAO;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.vistas.IVPPredioAvaluoDecretoDAO;
import co.gov.igac.snc.dao.vistas.IVPredioAvaluoDecretoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;
import co.gov.igac.snc.persistence.entity.formacion.ParametroDecretoCondicion;
import co.gov.igac.snc.persistence.entity.formacion.TablaTerreno;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.entity.vistas.VPredioAvaluoDecreto;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
//@Interceptors(TimeInterceptor.class)
public class FormacionBean implements IFormacion, IFormacionLocal {

    @EJB
    private IDecretoAvaluoAnualDAO decretoAvaluoAnualService;

    @EJB
    private IParametroDecretoCondicionDAO parametroDecretoCondicionService;

    @EJB
    private IDecretoCondicionDAO decretoCondicionService;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;

    @EJB
    private IVPPredioAvaluoDecretoDAO vPPredioAvaluoDecretoService;

    @EJB
    private IVPredioAvaluoDecretoDAO vPredioAvaluoDecretoService;

    @EJB
    private ITablaTerrenoDAO tablaTerrenoDao;

    @EJB
    private ICalificacionAnexoDAO calificacionAnexoDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(FormacionBean.class);

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#findDecretosAll()
     */
    public List<DecretoAvaluoAnual> buscarDecretos() {

        List<DecretoAvaluoAnual> answer = null;
        try {
            answer = this.decretoAvaluoAnualService.findAllOrderedByColumn2("vigencia", false);
            for (DecretoAvaluoAnual d : answer) {
                d.getDecretoCondicions().size();
            }
        } catch (Exception e) {
            LOGGER.error("ERROR " + e.getMessage());
        }
        return answer;
    }

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#guardarDecreto(DecretoAvaluoAnual decretoNuevo)
     */
    public DecretoAvaluoAnual guardarDecreto(DecretoAvaluoAnual decreto) {
        return this.decretoAvaluoAnualService.update(decreto);
    }

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#borrarDecretos(DecretoAvaluoAnual[] selectedDecretos)
     */
    public void eliminarDecretos(DecretoAvaluoAnual[] selectedDecretos) {
        LOGGER.debug("FormacionBean - Borrando Decretos");
        int i;
        for (i = 0; i < selectedDecretos.length; i++) {
            this.decretoAvaluoAnualService.delete(selectedDecretos[i]);
        }
    }

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#consultarParametrosDecretoCondicion()
     */
    public List<ParametroDecretoCondicion> consultarParametrosDecretoCondicion() {
        List<ParametroDecretoCondicion> answer = null;
        try {
            answer = this.parametroDecretoCondicionService.buscarParametroDecretoCondicionAll();;
        } catch (Exception e) {
            LOGGER.error("ERROR " + e.getMessage());
        }
        return answer;
    }

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#eliminarCondicional(DecretoCondicion condicional)
     */
    public void eliminarCondicional(DecretoCondicion condicional) {
        LOGGER.debug("FormacionBean - Eliminando condicional");
        this.decretoCondicionService.delete(condicional);
    }

    //------------------------------------------------ //
    /**
     * @author javier.aponte
     * @see IFormacionLocal#validarCondicional(DecretoCondicion condicional)
     */
    public Object[] validarCondicional(Long decretoCondicionId) {

        Object[] answer = null;

        try {
            answer = sncProcedimientoDao.validarCondicionalDecretoCondicion(decretoCondicionId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#guardarCondicional(DecretoCondicion condicional)
     */
    public DecretoCondicion guardarCondicional(DecretoCondicion condicional) {
        return this.decretoCondicionService.update(condicional);
    }

    //------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#buscarCalculoDePrediosAfectadoPorDecreto(Long decretoId)
     */
    public Object[] buscarCalculoDePrediosAfectadoPorDecreto(Long decretoId) {
        Object[] prediosAfectados = null;
        try {
            prediosAfectados = sncProcedimientoDao.calcularAfectadosDecreto(decretoId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return prediosAfectados;
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#recalcularAvaluosDecretoParaUnPredio(Long tramiteId, Long predioId, Date
     * fechaDelCalculo)
     */
    public Object[] recalcularAvaluosDecretoParaUnPredio(Long tramiteId,
        Long predioId, Date fechaDelCalculo, UsuarioDTO usuario) {
        Object[] errors = null;
        try {
            errors = sncProcedimientoDao.reproyectarAvaluos(tramiteId,
                predioId, fechaDelCalculo, usuario);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @see IFormacionLocal#buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(Long
     * pPredioId,Date fechaDelCalculo)
     */
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(
        Long pPredioId, Date fechaDelCalculo) {
        List<VPPredioAvaluoDecreto> predioAvaluosCatastrales = null;
        try {
            predioAvaluosCatastrales = vPPredioAvaluoDecretoService
                .buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(pPredioId,
                    fechaDelCalculo);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predioAvaluosCatastrales;
    }

    // ------------------------------------------------ //
    /**
     * @author david.cifuentes
     * @modified christian.rodriguez se cambia la vista que se consulta de VPredioAvaluoDecreto a
     * VPPredioAvaluoDecreto
     * @see IFormacionLocal#buscarAvaluosCatastralesPorIdPPredio(Long pPredioId)
     */
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredio(
        Long pPredioId) {
        List<VPPredioAvaluoDecreto> predioAvaluosCatastrales = null;
        try {
            predioAvaluosCatastrales = vPPredioAvaluoDecretoService
                .buscarAvaluosCatastralesPorIdPPredio(pPredioId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predioAvaluosCatastrales;
    }

    // ------------------------------------------------ //
    /**
     * @see IFormacionLocal#revertirProyeccionAvaluosParaUnPredio(Long tramiteId,Long predioId)
     * @author javier.aponte
     */
    @Override
    public Object[] revertirProyeccionAvaluosParaUnPredio(Long tramiteId, Long predioId) {
        Object[] errors = null;
        try {
            errors = sncProcedimientoDao.revertirProyeccionAvaluos(tramiteId,
                predioId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    /**
     * @see IFormacionLocal#findByZonaVigencia(String zona, Date vigencia)
     * @author fredy.wilches
     */
    @Override
    public List<TablaTerreno> findByZonaVigencia(String zona, Date vigencia) {
        try {
            return this.tablaTerrenoDao.findByZonaVigencia(zona, vigencia);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IFormacionLocal#buscarDecretoPorVigencia()
     * @author david.cifuentes
     */
    @Override
    public DecretoAvaluoAnual buscarDecretoPorVigencia(String anioVigencia) {
        return this.decretoAvaluoAnualService.buscarDecretoPorVigencia(anioVigencia);
    }

    /**
     * @see IFormacionLocal#buscarDecretoActual()
     * @author david.cifuentes
     */
    @Override
    public DecretoAvaluoAnual buscarDecretoActual() {
        return this.decretoAvaluoAnualService.buscarDecretoActual();
    }

    // ------------------------------------------------ //
    /**
     * @author javier.aponte
     * @see IFormacionLocal#liquidarAvaluosParaUnPredio(Long, Long, Date, UsuarioDTO)
     */
    public Object[] liquidarAvaluosParaUnPredio(Long tramiteId,
        Long predioId, Date fechaDelCalculo, UsuarioDTO usuario) {
        Object[] errors = null;
        try {
            errors = sncProcedimientoDao.liquidarAvaluosParaUnPredio(tramiteId,
                predioId, fechaDelCalculo, usuario);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    /**
     * @see IFormacion#getAllCalificacionAnexo()
     * @author javier.aponte
     */
    @Override
    public List<CalificacionAnexo> getAllCalificacionAnexo() {

        List<CalificacionAnexo> answer = null;
        try {
            answer = this.calificacionAnexoDAO.getAllCalificacionAnexo();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    /**
     * @author leidy.gonzalez :: #12528::26/05/2015::CU_187
     *
     * @see IFormacionLocal#buscarAvaluosCatastralesPorIdPPredio(Long predioId)
     */
    public ArrayList<VPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPredio(
        Long pPredioId) {
        ArrayList<VPredioAvaluoDecreto> predioAvaluosCatastrales = null;
        try {
            predioAvaluosCatastrales = vPredioAvaluoDecretoService
                .buscarAvaluosCatastralesPorIdPPredio(pPredioId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return predioAvaluosCatastrales;
    }
}
