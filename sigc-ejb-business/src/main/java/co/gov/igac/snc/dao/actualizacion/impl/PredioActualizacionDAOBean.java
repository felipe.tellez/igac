/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.actualizacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IMunicipioActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.IPredioActualizacionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.PredioActualizacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementación de los servicios de persistencia del objeto PredioActualizacion.
 *
 * @author felipe.cadena
 */
@Stateless
public class PredioActualizacionDAOBean extends GenericDAOWithJPA<PredioActualizacion, Long>
    implements IPredioActualizacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDAOBean.class);

    /**
     * @author felipe.cadena
     * @see IMunicipioActualizacionDAO#obtenerPorMunicipioYEstado(Long, String, Boolean)
     */
    @Override
    public List<PredioActualizacion> obtenerPorSolicitudId(Long solicitudId) {

        List<PredioActualizacion> result = new ArrayList<PredioActualizacion>();
        HashMap<String, PredioActualizacion> rads = new HashMap<String, PredioActualizacion>();

        String query = "SELECT pa FROM PredioActualizacion pa " +
            " JOIN FETCH pa.tramite t " +
            " WHERE t.solicitud.id = :solicitudId ";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("solicitudId", solicitudId);

            result = (List<PredioActualizacion>) (List<PredioActualizacion>) q.getResultList();

            //Se descartan los radicados repetidos
            for (PredioActualizacion predioActualizacion : result) {
                rads.
                    put(predioActualizacion.getTramite().getNumeroRadicacion(), predioActualizacion);
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioActualizacionDAOBean#obtenerPorSolicitudId");
        }

        return new ArrayList<PredioActualizacion>(rads.values());

    }
}
