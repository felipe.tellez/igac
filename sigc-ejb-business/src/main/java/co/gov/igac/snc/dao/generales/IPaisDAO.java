package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.dao.IGenericJpaDAO;

@Local
public interface IPaisDAO extends IGenericJpaDAO<Pais, String> {

}
