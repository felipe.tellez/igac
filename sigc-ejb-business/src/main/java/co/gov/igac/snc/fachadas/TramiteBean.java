package co.gov.igac.snc.fachadas;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.ESubreporteResoluciones;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.actualizacion.IMunicipioActualizacionDAO;
import co.gov.igac.snc.dao.avaluos.ISolicitudDocumentacionDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizPredioTerrenoDAO;
import co.gov.igac.snc.dao.conservacion.IPManzanaVeredaDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.IPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.ITramiteDepuracionDAO;
import co.gov.igac.snc.dao.conservacion.IUnidadConstruccionDAO;
import co.gov.igac.snc.dao.generales.ICodigoHomologadoDAO;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.IParametroDAO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.generales.ITramiteRelacionTramiteDAO;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.tramite.IDocumentacionTramiteDAO;
import co.gov.igac.snc.dao.tramite.IDuracionTramiteDAO;
import co.gov.igac.snc.dao.tramite.IModeloResolucionDAO;
import co.gov.igac.snc.dao.tramite.IMotivoEstadoTramiteDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDAO;
import co.gov.igac.snc.dao.tramite.IProductoCatastralDetalleDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteTramiteDAO;
import co.gov.igac.snc.dao.tramite.ISolicitudDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ISolicitudPredioDAO;
import co.gov.igac.snc.dao.tramite.ITipoSolicitudTramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDetallePredioDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDigitalizacionDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentacionDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteEstadoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteInconsistenciaDAO;
import co.gov.igac.snc.dao.tramite.ITramiteInfoAdicionalDAO;
import co.gov.igac.snc.dao.tramite.ITramitePredioAvaluoDAO;
import co.gov.igac.snc.dao.tramite.ITramitePruebaDAO;
import co.gov.igac.snc.dao.tramite.ITramitePruebaEntidadDAO;
import co.gov.igac.snc.dao.tramite.ITramiteReasignacionDAO;
import co.gov.igac.snc.dao.tramite.ITramiteRequisitoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteTextoResolucionDAO;
import co.gov.igac.snc.dao.tramite.IVSolicitudPredioDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionEstadoDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDatoDAO;
import co.gov.igac.snc.dao.tramite.radicacion.IAvisoRegistroRechazoDAO;
import co.gov.igac.snc.dao.tramite.radicacion.IDatoRectificarDAO;
import co.gov.igac.snc.dao.tramite.radicacion.IDeterminaTramitePHDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudAvisoRegistroDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ITramitePredioEnglobeDAO;
import co.gov.igac.snc.dao.tramite.radicacion.ITramiteRectificacionDAO;
import co.gov.igac.snc.dao.tramite.radicacion.IVRadicacionDAO;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.dao.vistas.IVComisionDAO;
import co.gov.igac.snc.dao.vistas.IVEjecutorComisionDAO;
import co.gov.igac.snc.dao.vistas.IVTramitePredioDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.RepConsultaPredial;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.DeterminaTramitePH;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudAvisoRegistro;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInfoAdicional;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRelacionTramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.entity.tramite.VRadicacion;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.persistence.entity.vistas.VTramitePredio;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioZonaUnidadOrganica;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESistema;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.DetallesDTO;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.EventoScheduleComisionesDTO;
import co.gov.igac.snc.util.FiltroDatosConsultaCancelarTramites;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.ISolicitanteSolicitudOTramite;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.TramiteConEjecutorAsignadoDTO;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.conservacion.ConservacionUtil;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;
import co.gov.igac.snc.vo.ActividadVO;
import co.gov.igac.snc.vo.FichaMatrizPisoVO;
import co.gov.igac.snc.vo.FichaMatrizPredioVO;
import co.gov.igac.snc.vo.FichaMatrizSotanoVO;
import co.gov.igac.snc.vo.FichaMatrizTorreVO;
import co.gov.igac.snc.vo.FichaMatrizVO;
import co.gov.igac.snc.vo.ManzanaVO;
import co.gov.igac.snc.vo.PPredioDireccionVO;
import co.gov.igac.snc.vo.PUnidadConstruccionVO;
import co.gov.igac.snc.vo.PredioDireccionVO;
import co.gov.igac.snc.vo.PredioInfoVO;
import co.gov.igac.snc.vo.ResultadoVO;
import co.gov.igac.snc.vo.TramiteInconsistenciaVO;
import co.gov.igac.snc.vo.UnidadConstruccionVO;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;
import java.util.HashSet;
import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import javax.persistence.NoResultException;

@Stateless
//@Interceptors(TimeInterceptor.class)
public class TramiteBean implements ITramite, ITramiteLocal {

    // ///////////////////////////////////////////////////////////////////////////////
    // ///////////////////////////////////////////////////////////////////////////////
    /**
     * Interfaces Locales de Servicio
     */
    @EJB
    private IGeneralesLocal generalesService;

    @EJB
    private IConservacionLocal conservacionService;

    @EJB
    private IProcesos procesosService;

    @EJB
    private ITransaccionalLocal transaccionalService;

    @EJB
    private IGenerales generalesServiceIG;

    // ///////////////////////////////////////////////////////////////////////////////
    // ///////////////////////////////////////////////////////////////////////////////
    /**
     * Interfaces de acceso a Datos (DAO)
     */
    @EJB
    private ISolicitudDAO solicitudDao;

    @EJB
    private IVRadicacionDAO vRadicacionDao;

    @EJB
    private ISolicitanteDAO solicitanteDao;

    @EJB
    private ITramiteDAO tramiteDao;

    @EJB
    private ITramiteDetallePredioDAO tramiteDetallePredioDao;

    @EJB
    private ITramiteEstadoDAO tramiteEstadoDao;

    @EJB
    private IDocumentacionTramiteDAO documentacionTramiteDao;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;

    @EJB
    private ITramitePredioAvaluoDAO tramitePredioAvaluoDao;

    @EJB
    private IComisionDAO comisionDao;

    @EJB
    private IVComisionDAO vcomisionDao;

    @EJB
    private IComisionTramiteDAO comisionTramiteDao;

    @EJB
    private IDocumentoDAO documentoDao;

    @EJB
    private IDominioDAO dominioDao;

    @EJB
    private IAvisoRegistroRechazoDAO avisoRegistroRechazoDao;

    @EJB
    private ISolicitudAvisoRegistroDAO solicitudAvisoRegistroDao;

    @EJB
    private ISolicitanteSolicitudDAO solicitanteSolicitudDao;

    @EJB
    private ISolicitudPredioDAO solicitudPredioDao;

    @EJB
    private ISolicitanteTramiteDAO solicitanteTramiteDao;

    @EJB
    private ITramiteTextoResolucionDAO tramiteTextoResolucionDao;

    @EJB
    private IModeloResolucionDAO modeloResolucionDao;

    @EJB
    private ITramiteDocumentoDAO tramiteDocumentoDao;

    @EJB
    private ITramiteRequisitoDAO tramiteRequisitoDao;

    @EJB
    private IPredioDAO predioDao;

    @EJB
    private IVEjecutorComisionDAO vEjecutorComisionDAo;

    @EJB
    private IVSolicitudPredioDAO vSolicitudPredioDao;

    @EJB
    private ITramitePredioEnglobeDAO tramitePredioEnglobeDao;

    @EJB
    private IDuracionTramiteDAO duracionTramiteDao;

    @EJB
    private IDatoRectificarDAO datoRectificarDao;

    @EJB
    private ITipoSolicitudTramiteDAO tipoSolicitudTramiteDao;

    @EJB
    private ITramitePruebaDAO tramitePruebaDao;

    @EJB
    private ITramiteDocumentacionDAO tramiteDocumentacionDao;

    @EJB
    private IMotivoEstadoTramiteDAO motivoEstadoTramiteDao;

    @EJB
    private ITramitePruebaEntidadDAO tramitePruebaEntidadDao;

    @EJB
    private ICodigoHomologadoDAO codigoHomologadoDao;

    @EJB
    private IPPredioDAO pPredioDao;

    @EJB
    private ITramiteInfoAdicionalDAO infoAdicionalDao;

    @EJB
    private IUnidadConstruccionDAO unidadConstDao;

    @EJB
    private IPUnidadConstruccionDAO pUnidadConstDao;

    @EJB
    private IComisionTramiteDatoDAO comisionTramiteDatoDao;

    @EJB
    private IPFichaMatrizDAO pFichaMatrizDao;

    @EJB
    private IComisionEstadoDAO comisionEstadoDao;

    @EJB
    private IVTramitePredioDAO vTramitePredioDao;

    @EJB
    private IJurisdiccionDAO jurisdiccionDao;

    @EJB
    private ITipoDocumentoDAO tipoDocumentoDao;

    @EJB
    private IDepartamentoDAO departamentoDao;

    @EJB
    private IMunicipioDAO municipioDao;

    @EJB
    private ISolicitudDocumentacionDAO solicitudDocumentacionDAO;

    @EJB
    private ISolicitudDocumentoDAO solicitudDocumentoDAO;

    @EJB
    private IProductoCatastralDAO productoCatastralDAO;

    @EJB
    private IProductoCatastralDetalleDAO productoCatastralDetalleDAO;

    @EJB
    private ITramiteDepuracionDAO tramiteDepuracionDAO;

    @EJB
    private ITramiteInconsistenciaDAO tramiteInconsistenciaDAO;

    @EJB
    private ITramiteDigitalizacionDAO tramiteDigitalizacionDAO;

    @EJB
    private IPPersonaPredioDAO pPersonaPredioDAO;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private IPPredioZonaDAO pPredioZonaDao;

    @EJB
    private IPredioZonaDAO predioZonaDao;

    @EJB
    private IPManzanaVeredaDAO pManzanaVeredaDao;

    @EJB
    private IProductoCatastralJobDAO productoCatastralJobDAO;

    @EJB
    private IDeterminaTramitePHDAO determinaTramitePHDAO;

    @EJB
    private IMunicipioActualizacionDAO municipioActualizacionDAO;

    @EJB
    private ITramiteReasignacionDAO tramiteReasignacionDAO;

    @EJB
    private IParametroDAO parametroDAO;

    @EJB
    private ITramiteRelacionTramiteDAO tramiteRelacionTramiteDAO;

    @EJB
    private ITramiteRectificacionDAO tramiteRectificacionDAO;

    @EJB
    private IPFichaMatrizPredioTerrenoDAO pfichaMatrizPredioTerrenoDAO;

    // ///////////////////////////////////////////////////////////////////////////////
    // ///////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteBean.class);

    // ////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void lanzarExcepcionEntreCapasTest() {

        throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, null,
            "excepción lanzada desde la capa de negocio");

    }

    /**
     * Método utilizado para actualizar una Solicitud.
     *
     * @param usuario
     * @param solicitud Solicitud
     * @return Solicitud con información actualizada.
     * @throws ExcepcionSNC
     */
    @Deprecated
    @Override
    public Solicitud guardarActualizarSolicitud(UsuarioDTO usuario,
        Solicitud solicitud, List<Long> tramitesPrevios) {

        String fileSeparator = System.getProperty("file.separator");

//TODO :: fredy.wilches :: llamar SP o WS de correspondencia
        if (solicitud.getNumero() == null) {
            Object[] resultado = this.obtenerNumeroSolicitud(solicitud, usuario);
            if (resultado != null && resultado.length > 0 && !((new BigDecimal(-1)).equals(
                resultado[0]))) {
                solicitud.setNumero(resultado[5].toString());
            } else {
                LOGGER.error("Solicitud no numerada por error en la radicación con correspondencia");
                LOGGER.error("" + resultado[1]);
                LOGGER.error("" + resultado[4]);
            }
        }

        if (solicitud.getTipo() == null) {
            solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo()); // Conservación
        }

        if (solicitud.getFecha() == null) {
            solicitud.setFecha(new Date());
        }

        if (solicitud.getEstado() == null) {
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.getCodigo()); // Recibida
        }

        if (solicitud.getFinalizado() == null) {
            solicitud.setFinalizado(ESiNo.NO.getCodigo());
        }

        solicitud.setFechaLog(new Date());
        solicitud.setUsuarioLog(usuario.getLogin());

        if (solicitud.getSolicitanteSolicituds() != null) {
            for (SolicitanteSolicitud solSol : solicitud
                .getSolicitanteSolicituds()) {
                solSol.setFechaLog(new Date());
                solSol.setUsuarioLog(usuario.getLogin());
            }
        }

        if (solicitud.getTramites() != null &&
            !solicitud.getTramites().isEmpty()) {

            for (Tramite t : solicitud.getTramites()) {

                boolean existePrevio = false;
                if (tramitesPrevios != null) {
                    for (Long tp : tramitesPrevios) {
                        if (tp.equals(t.getId())) {
                            existePrevio = true;
                            break;
                        }
                    }
                }

                if (existePrevio) {
                    continue;
                }

                t.setSolicitud(solicitud);
                if (this.actualizarEstadoTramite(t, ETramiteEstado.RECIBIDO, usuario) == null) {
                    continue;
                }
                //t.setEstado(ETramiteEstado.RECIBIDO.getCodigo()); // Recibido
                t.setArchivado(ESiNo.NO.getCodigo());
                t.setUsuarioLog(usuario.getLogin());
                t.setFechaLog(new Date());

                if (t.getNumeroRadicacion() == null) {
                    Object[] resultado;
                    if (t.getTipoTramite().equals(
                        ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
                        resultado = this.generalesService
                            .generarNumeracion(
                                ENumeraciones.NUMERACION_RADICACION_DE_REVISION_DE_AVALUO,
                                "0",
                                t.getDepartamento().getCodigo(),
                                t.getMunicipio().getCodigo(),
                                Constantes.NUMERACION_AVALUO_TIPO_DOCUMENTO);
                    } else if (t.getTipoTramite().equals(
                        ETramiteTipoTramite.AUTOESTIMACION.getCodigo())) {
                        resultado = null;
                    } else {
                        resultado = this.generalesService.generarNumeracion(
                            ENumeraciones.NUMERACION_RADICACION_CATASTRAL,
                            "0", t.getDepartamento().getCodigo(), t
                            .getMunicipio().getCodigo(), 0);

                    }
                    if (resultado != null && resultado.length > 0) {
                        t.setNumeroRadicacion(resultado[0].toString());
                    }
                }
                //#7760 consultar la documentación completa del tramite para validaciones 
                Tramite tempTramite = this.consultarDocumentacionCompletaDeTramite(t.getId());

                if (tempTramite.getTramiteDocumentacions() != null &&
                    !tempTramite.getTramiteDocumentacions().isEmpty()) {
                    for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                        td.setTramite(t);
                        td.setUsuarioLog(usuario.getLogin());
                        td.setFechaLog(new Date());

                        if (td.getDocumentoSoporte() != null) {
                            Documento d = td.getDocumentoSoporte();

                            if (d.getArchivo() == null ||
                                d.getArchivo().isEmpty()) {
                                d.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                            }

                            if (t.getPredio() != null) {
                                d.setPredioId(t.getPredio().getId());
                            }

                            d.setTramiteId(t.getId());
                            // descripcion
                            // observaciones
                            d.setBloqueado(ESiNo.NO.getCodigo());
                            d.setUsuarioCreador(usuario.getLogin());
                            d.setFechaRadicacion(t.getFechaRadicacion());
                            d.setUsuarioLog(usuario.getLogin());
                            d.setFechaLog(new Date());

                            Documento newD = null;

                            try {
                                newD = this.documentoDao.update(d);

                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                            if (newD != null && newD.getId() != null) {
                                d.setId(newD.getId());
                            }
                        }
                    }
                }

                if (t.getTramitePredioEnglobes() != null &&
                    t.getTramitePredioEnglobes().size() > 0) {

                    for (TramitePredioEnglobe tpe : t
                        .getTramitePredioEnglobes()) {
                        tpe.setTramite(t);
                        tpe.setUsuarioLog(usuario.getLogin());
                        tpe.setFechaLog(new Date());
                    }
                }

                if (t.getSolicitanteTramites() != null) {
                    for (SolicitanteTramite solTram : t
                        .getSolicitanteTramites()) {
                        solTram.setFechaLog(new Date());
                        solTram.setUsuarioLog(usuario.getLogin());
                    }
                }

            }
        }

        Solicitud sol = null;
        List<Long> tramitesAlmacenados = new ArrayList<Long>();
        //v1.1.7
        if (solicitud.getTramites() != null) {
            for (Tramite t : solicitud.getTramites()) {
                if (t.getId() != null) {
                    tramitesAlmacenados.add(t.getId());
                }
            }
        }
        try {
            // D: al hacer el update se pierde la relación con los objetos a los que les hizo fetch
            // por lo que hay que manejar un objeto temporal para hacer el
            // update y con el original se puede seguir manajendo los objetos relacionados
            sol = this.transaccionalService.updateSolicitudNuevaTransaccion(solicitud);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {

            if (sol != null) {
                solicitud.setId(sol.getId());
                if (solicitud.getSolicitudAvisoRegistro() != null) {
                    solicitud.getSolicitudAvisoRegistro().setId(
                        sol.getSolicitudAvisoRegistro().getId());
                    if (solicitud.getSolicitudAvisoRegistro().getId() != null) {
                        List<AvisoRegistroRechazo> avisoRegistroRechazosAux =
                            avisoRegistroRechazoDao
                                .findBySolicitudAvisoRegistroId(solicitud
                                    .getSolicitudAvisoRegistro().getId());
                        solicitud.getSolicitudAvisoRegistro()
                            .setAvisoRegistroRechazos(
                                avisoRegistroRechazosAux);
                    }
                }

                solicitud.setSolicitanteSolicituds(solicitanteSolicitudDao
                    .findBySolicitudId(sol.getId()));
            }

            if (sol.getTramites() != null) {
                TramiteEstado te = null;

                Solicitud solTraTemp = this.transaccionalService
                    .buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion(solicitud
                        .getId());
                if (solTraTemp != null) {
                    solicitud.setId(solTraTemp.getId());
                    solicitud.setTramites(solTraTemp.getTramites());

                    if (solTraTemp.getTramites() != null &&
                        solTraTemp.getTramites().size() > 0) {

                        for (int i = 0; i < solTraTemp.getTramites().size(); i++) {

                            if (solTraTemp.getTramites().get(i).getId() != null &&
                                !tramitesAlmacenados.contains(solTraTemp
                                    .getTramites().get(i).getId())) {

                                Tramite auxTram = solTraTemp.getTramites().get(
                                    i);
                                if (auxTram.getSolicitanteTramites() != null &&
                                    !auxTram.getSolicitanteTramites()
                                        .isEmpty()) {
                                    solTraTemp
                                        .getTramites()
                                        .get(i)
                                        .setSolicitanteTramites(
                                            this.solicitanteTramiteDao
                                                .findBySolicitudId(sol
                                                    .getId()));
                                }

                                if (solTraTemp.getTramites().get(i)
                                    .getTramiteDocumentacions() != null &&
                                    solTraTemp.getTramites().get(i)
                                        .getTramiteDocumentacions()
                                        .size() > 0) {

                                    @SuppressWarnings("unused")
                                    int paraDigitalizar = 0;

                                    for (TramiteDocumentacion td : solTraTemp
                                        .getTramites().get(i)
                                        .getTramiteDocumentacions()) {

                                        if (td.getDocumentoSoporte() != null &&
                                            td.getDocumentoSoporte()
                                                .getId() != null &&
                                            td.getDocumentoSoporte()
                                                .getArchivo() != null &&
                                            !td.getDocumentoSoporte()
                                                .getArchivo().isEmpty() &&
                                            !td.getDocumentoSoporte()
                                                .getArchivo()
                                                .equals(Constantes.CONSTANTE_CADENA_VACIA_DB) &&
                                            (td.getDocumentoSoporte()
                                                .getIdRepositorioDocumentos() == null || td
                                                .getDocumentoSoporte()
                                                .getIdRepositorioDocumentos()
                                                .isEmpty())) {

                                            String nombreDepartamento = "";
                                            String nombreMunicipio = "";
                                            String numeroPredial = "";

                                            if (td.getDepartamento() != null) {
                                                nombreDepartamento = td
                                                    .getDepartamento()
                                                    .getNombre();
                                            } else {
                                                nombreDepartamento = solTraTemp
                                                    .getTramites().get(i)
                                                    .getDepartamento()
                                                    .getNombre();
                                            }

                                            if (td.getMunicipio() != null) {
                                                nombreMunicipio = td
                                                    .getMunicipio()
                                                    .getNombre();
                                            } else {
                                                nombreMunicipio = solTraTemp
                                                    .getTramites().get(i)
                                                    .getMunicipio()
                                                    .getNombre();
                                            }

                                            if (solTraTemp.getTramites().get(i)
                                                .getPredio() != null) {
                                                numeroPredial = solTraTemp
                                                    .getTramites().get(i)
                                                    .getPredio()
                                                    .getNumeroPredial();
                                            }

                                            DocumentoTramiteDTO documentoTramiteDTO =
                                                new DocumentoTramiteDTO(
                                                    FileUtils.getTempDirectory().getAbsolutePath() +
                                                    fileSeparator +
                                                    td.getDocumentoSoporte()
                                                        .getArchivo(),
                                                    td.getDocumentoSoporte()
                                                        .getTipoDocumento()
                                                        .getNombre(),
                                                    solTraTemp
                                                        .getTramites()
                                                        .get(i)
                                                        .getFechaRadicacion(),
                                                    nombreDepartamento,
                                                    nombreMunicipio,
                                                    numeroPredial, solTraTemp
                                                        .getTramites()
                                                        .get(i)
                                                        .getTipoTramite(),
                                                    solTraTemp.getTramites()
                                                        .get(i).getId());

                                            // Cuando es una solicitud de via gubernativa o es un trámite de mutación de quinta,
                                            // el campo número predial del documentoTramiteDTO puede ir nulo.
                                            if (!solTraTemp.isViaGubernativa() &&
                                                !solTraTemp.getTramites().get(i).isQuinta()) {
                                                this.documentoDao
                                                    .guardarDocumentoGestorDocumental(
                                                        usuario,
                                                        documentoTramiteDTO,
                                                        td.getDocumentoSoporte());
                                            } else {
                                                this.documentoDao
                                                    .guardarDocumentoGestorDocumentalSinNumeroPredial(
                                                        usuario,
                                                        documentoTramiteDTO,
                                                        td.getDocumentoSoporte());
                                            }

                                        }
                                    }

                                    te = new TramiteEstado();
                                    te.setEstado(ETramiteEstado.RECIBIDO.getCodigo());

                                    te.setTramite(solTraTemp.getTramites().get(i));
                                    te.setResponsable(usuario.getLogin());
                                    te.setFechaInicio(new Date());
                                    te.setUsuarioLog(usuario.getLogin());
                                    te.setFechaLog(new Date());

                                    try {

                                        @SuppressWarnings("unused")
                                        boolean r = this.tramiteEstadoDao
                                            .crearActualizarTramiteEstado(
                                                te, usuario);
                                    } catch (Exception e) {
                                        LOGGER.error(e.getMessage(), e);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new ExcepcionSNC("", ESeveridadExcepcionSNC.ERROR,
                ex.getMessage(), ex);
        }

        return solicitud;

    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<TramiteDocumentacion> buscarDocumentosTramitePorNumeroDeRadicacion(
        String numeroRadicacion) {
        return this.documentacionTramiteDao
            .buscarDocumentosTramitePorNumeroDeRadicacion(numeroRadicacion);
    }

    /**
     * @author fredy.wilches Retorna el listado de trámites a partir del id de la solicitud
     * @param numeroSolicitud
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public List<Tramite> consultarTramitesSolicitud(Long solicitudId) {
//TODO :: fredy.wilches :: hace manejo de excepciones :: pedro.garcia
        return this.tramiteDao.findBySolicitud(solicitudId);
    }

    /**
     * @author fredy.wilches Retorna el listado de trámites a partir del numero de la solicitud
     * @param numeroSolicitud
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public List<Tramite> consultarTramitesSolicitud(String numeroSolicitud) {
        return this.tramiteDao.findBySolicitud(numeroSolicitud);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarPredioEnglobesPorTramite(java.lang.Long)
     */
    /*
     * @modified pedro.garcia 21-08-2012 Orden. Manejo de excepciones. Documentación
     */
    @Implement
    @Override
    public List<TramiteDetallePredio> buscarPredioEnglobesPorTramite(Long tramiteId) {

        List<TramiteDetallePredio> answer = null;

        try {
            answer = this.tramiteDao.buscarPredioEnglobesPorTramite(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarPredioEnglobesPorTramite: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarSolicitudes(co.gov.igac.snc.util.FiltroDatosSolicitud)
     */
    @Override
    public List<Solicitud> buscarSolicitudes(
        FiltroDatosSolicitud filtroDatosSolicitud, int desde, int cantidad) {
        LOGGER.debug("TramiteBean#buscarSolicitudes");
        return solicitudDao.buscarSolicitudes(filtroDatosSolicitud, desde,
            cantidad);
    }

    /**
     * @see ITramite#buscarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud, int, int)
     */
    @Override
    public List<VRadicacion> buscarSolicitudesCotizacionAvaluos(
        FiltroDatosSolicitud filtroDatosSolicitud, int desde, int cantidad) {
        LOGGER.debug("TramiteBean#buscarSolicitudesCotizacionAvaluos");
        return vRadicacionDao.buscarSolicitudesCotizacionAvaluos(
            filtroDatosSolicitud, desde, cantidad);
    }

    /**
     * @see ITramite#contarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud)
     */
    @Override
    public long contarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro) {
        LOGGER.debug("TramiteBean#contarSolicitudesCotizacionAvaluos");
        return vRadicacionDao.contarSolicitudesCotizacionAvaluos(filtro);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     * @see ITramite#guardarActualizarSolicitante(Solicitante)
     */
    @Override
    public Solicitante guardarActualizarSolicitante(Solicitante solicitante) {
        try {
            return this.solicitanteDao.update(solicitante);
        } catch (RuntimeException rtex) {
            LOGGER.error(rtex.getMessage(), rtex);
            throw new ExcepcionSNC("", ESeveridadExcepcionSNC.ERROR,
                rtex.getMessage(), rtex);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author christian.rodriguez
     * @see ITramite#guardarActualizarSolicitanteSolicitud(SolicitanteSolicitud)
     */
    @Override
    public SolicitanteSolicitud guardarActualizarSolicitanteSolicitud(
        SolicitanteSolicitud solicitante) {
        try {
            return solicitanteSolicitudDao.update(solicitante);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author christian.rodriguez
     * @see ITramiteLocal#eliminarSolicitanteSolicitud(SolicitanteSolicitud)
     */
    @Override
    public void eliminarSolicitanteSolicitud(SolicitanteSolicitud solicitante) {
        try {
            this.solicitanteSolicitudDao.delete(solicitante);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarSolicitantePorTipoYNumeroDeDocumento(String,String)
     */
    @Override
    public Solicitante buscarSolicitantePorTipoYNumeroDeDocumento(
        String numeroDocumento, String tipoDocumento) {

        Solicitante answer = null;

        try {
            answer = this.solicitanteDao.buscarSolicitantePorTipoYNumeroDeDocumento(
                numeroDocumento, tipoDocumento);
        } catch (ExcepcionSNC snc) {
            LOGGER.error("Error en TramiteBean#buscarSolicitantePorTipoYNumeroDeDocumento: " +
                snc.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @return @see ITramite#actualizarTramites(List)
     */
    @Implement
    @Override
    public void actualizarTramites(Tramite[] tramitesReclasificados) {

        LOGGER.debug("TramiteBean#actualizarTramite");

        for (int i = 0; i < tramitesReclasificados.length; i++) {
            Tramite tramite = tramitesReclasificados[i];
            this.tramiteDao.update(tramite);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#contarComisionesSinMemo(java.lang.String)
     * @author pedro.garcia
     * @version 2.0
     */
    @Implement
    @Override
    public int contarComisionesSinMemo(String idTerritorial, List<Long> idTramites,
        String tipoComision) {

        LOGGER.debug("en TramiteBean#contarComisionesSinMemo");
        int answer = 0;

        try {
            answer = this.vcomisionDao.countComisionesSinMemo(idTerritorial, idTramites,
                tipoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("excepción contando las comisiones sin memorando de la territorial " +
                idTerritorial + ": " + ex.getMensaje());
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarComisionesSinMemo(java.lang.String, java.util.List, java.lang.String,
     * java.util.Map, int[])
     * @author pedro.garcia
     * @version 2.0
     */
    @Implement
    @Override
    public List<VComision> buscarComisionesSinMemo(String territorialId, List<Long> idTramites,
        String tipoComision, Map<String, String> filters, int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteBean#buscarComisionesSinMemo");

        List<VComision> answer = null;
        try {
            answer = this.vcomisionDao.findComisionesSinMemo(territorialId, idTramites,
                tipoComision, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("excepción buscando las comisiones sin memorando de la territorial " +
                territorialId + ": " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author jamir.avila
     * @see ITramite#adicionarPredioAAvaluo(Long, TramitePredioAvaluo)
     */
    @Override
    public TramitePredioAvaluo adicionarPredioAAvaluo(
        TramitePredioAvaluo predioAvaluo) {
        LOGGER.info("TramiteBean#adicionarPredioAAvaluo");
        return this.tramitePredioAvaluoDao.adicionarPredioAAvaluo(predioAvaluo);
    }

    /**
     * @author jamir.avila
     * @see ITramite#removerPredioDeAvaluo(Long)
     */
    @Override
    public void removerPredioDeAvaluo(Long idTramitePredioAvaluo) {
        LOGGER.info("TramiteBean#removerPredioDeAvaluo");
        tramitePredioAvaluoDao.removerPredioDeAvaluo(idTramitePredioAvaluo);
    }

    /**
     * @author jamir.avila
     * @see ITramite#actualizarPredioDeAvaluo(TramitePredioAvaluo)
     */
    @Override
    public void actualizarPredioDeAvaluo(TramitePredioAvaluo predioAvaluo) {
        LOGGER.info("TramiteBean#actualizarPredioDeAvaluo");
        tramitePredioAvaluoDao.actualizarPredioDeAvaluo(predioAvaluo);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramiteTramitesPorTramiteId(java.lang.Long)
     * @author juan.agudelo
     */
    @Implement
    @Override
    public Tramite buscarTramiteTramitesPorTramiteId(Long tramiteId) {

        Tramite answer = null;
        answer = this.tramiteDao.buscarTramiteTramitesPorTramiteId(tramiteId);
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramiteTramitesPorTramiteId(java.lang.Long)
     */
    @Implement
    @Override
    public Tramite buscarTramitePorTramiteIdProyeccion(Long tramiteId) {

        Tramite answer = null;
        answer = this.tramiteDao.buscarTramitePorTramiteIdProyeccion(tramiteId);
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author jamir.avila
     * @see ITramite#crearSolicitudCotizacionAvaluo(Tramite)
     */
    @Override
    public Tramite crearSolicitudCotizacionAvaluo(Tramite tramite) {
        return tramiteDao.crearSolicitudCotizacionAvaluo(tramite);
    }

    /**
     * @see Itramite#findSolicitudPorId(Long)
     */
    @Override
    public Solicitud findSolicitudPorId(Long id) {
        LOGGER.info("encontrar solicitud por id");
        Solicitud solicitud = solicitudDao.findById(id);
        return solicitud;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Genera un número de solicitud desde correspondencia. Actualmente se está simulando. (22 de
     * Julio de 2011)
     *
     * @param solicitud
     * @return
     */
    private Object[] obtenerNumeroSolicitud(Solicitud solicitud, UsuarioDTO usuario) {

        //Validación de Precondiciones
        if (solicitud.getSolicitanteSolicituds() == null || solicitud.getSolicitanteSolicituds().
            isEmpty()) {
            throw SncBusinessServiceExceptions.EXCEPCION_NEGOCIO_0001.getExcepcion(LOGGER);
        }

        //
        List<Object> parametros = new ArrayList<Object>();

        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Territorial=compania
        parametros.add(usuario.getLogin()); // Usuario
        parametros.add("ER"); // tipo correspondencia
        parametros.add(solicitud.getTipo()); // tipo contenido solicitud.getTipo()
        parametros.add(new BigDecimal(solicitud.getFolios()));
        parametros.add(new BigDecimal(solicitud.getAnexos()));
        parametros.add(solicitud.getAsunto()); // Asunto
        parametros.add(solicitud.getObservaciones()); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add(solicitud.getSistemaEnvio()); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("1"); // presentacion, 6 internet
        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Destino lugar
        parametros.add(""); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(""); // Direccion destino
        parametros.add(""); // Tipo identificacion destino
        parametros.add(""); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).
            getTipoIdentificacionSolicitante()); // Solicitante tipo documento
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).
            getNumeroIdentificacionSolicitante()); // Solicitante identificacion
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getNombreCompleto()); //
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getTelefonoPrincipal()); //
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getCorreoElectronico()); //

        Object[] resultado = this.generalesService.radicarEnCorrespondencia(parametros);
        return resultado;
    }

    /**
     * @see ITramite#crearSolicitudTramite(UsuarioDTO, Solicitud)
     */
    @Override
    public Solicitud crearSolicitudTramite(UsuarioDTO usuario,
        Solicitud solicitud) {
        return solicitudDao.crearSolicitudTramite(usuario, solicitud);
    }

    /**
     * @author fabio.navarrete
     * @see ITramite#buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(String, String)
     */
    @Override
    public Solicitante buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
        String numeroDocumento, String tipoDocumento) {
        return this.solicitanteDao
            .findSolicitanteByTipoIdentAndNumDocFetchRelacions(
                tipoDocumento, numeroDocumento);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#insertarComision(co.gov.igac.snc.persistence.entity.tramite.Comision,
     * co.gov.igac.generales.dto.UsuarioDTO)
     */
    @Implement
    @Override
    public Comision insertarComision(Comision nuevaComision, UsuarioDTO datosUsuario) {

        Comision answer = null;

        try {
            answer = this.comisionDao.update(nuevaComision);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#insertarComision: " + ex.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#ingresarDocumentosAdicionalesRequeridos(List)
     */
    @Override
    public void ingresarDocumentosAdicionalesRequeridos(
        List<TramiteDocumentacion> documentosAdicionales) {

        try {
            this.documentacionTramiteDao.ingresarDocumentosAdicionalesRequeridos(
                documentosAdicionales);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#ingresarDocumentosAdicionalesRequeridos: " +
                ex.getMensaje());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @modified felipe.cadena -- Se agrega el parametro usuario
     * @see ITramite#obtenerSolicitudDeCorrespondencia(String)
     */
    @Override
    public Solicitud obtenerSolicitudDeCorrespondencia(String numeroRadicacionCorrespondencia,
        UsuarioDTO usuario) {
        Solicitud resultado = null;
        try {
            resultado = sncProcedimientoDao
                .obtenerRadicacionCorrespondencia(numeroRadicacionCorrespondencia, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#obtenerSolicitudDeCorrespondencia: " +
                e.getMensaje());
        }
        return resultado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#insertarRegistrosComisionTramite(java.util.List)
     * @author pedro.garcia
     * @version 2.0
     */
    @Implement
    @Override
    public void insertarRegistrosComisionTramite(List<ComisionTramite> registros) {
        try {
            this.comisionTramiteDao.persistMultiple(registros);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(
                LOGGER, ex, "ComisionTramite");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarTramitesDeComision(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesDeComision(Long comisionId, String tipoComision) {

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.findByComisionId(comisionId);

            if (answer != null && !answer.isEmpty()) {
                // D: asignar el valor del campo numeroComisionesHaEstado
                for (Tramite tramite : answer) {
                    calcAndSetNumeroComisionesHaEstado(tramite, tipoComision);
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarTramitesDeComision: " + ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarTramitesDeComisiones(List<Long>)
     * @author david.cifuentes
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesDeComisiones(List<Long> comisionesId) {

        List<Tramite> answer = null;
        try {
            answer = this.tramiteDao.buscarTramitesDeComisiones(comisionesId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarTramitesDeComisiones: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#actualizarComision(co.gov.igac.generales.dto.UsuarioDTO,
     * co.gov.igac.snc.persistence.entity.vistas.VComision)
     */
    @Implement
    @Override
    public void actualizarComision(UsuarioDTO loggedInUser, VComision selectedComision) {

        try {
            this.comisionDao.updateAsVComision(loggedInUser, selectedComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#actualizarComision: " + ex.getMensaje());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#contarTramitesDeEjecutorAsignado(java.lang.String)
     */
    @Override
    public Integer contarTramitesDeEjecutorAsignado(String ejecutor) {
        LOGGER.debug("en TramiteBean#contarTramitesDeEjecutorAsignado");
        int answer;
        answer = this.tramiteDao.contarTramitesDeEjecutorAsignado(ejecutor);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitesDeEjecutorAsignado
     */
    @Override
    public List<Tramite> buscarTramitesDeEjecutorAsignado(String territorialId,
        String ejecutor, final int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteBean#buscarTramitesParaAsignacion");

        List<Tramite> answer;
        answer = this.tramiteDao.buscarTramitesDeEjecutorAsignado(
            territorialId, ejecutor, rowStartIdxAndCount);

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitesAsignadosAEjecutor
     */
    @Override
    public List<Tramite> buscarTramitesAsignadosAEjecutor(String territorialId,
        String ejecutor, final int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteBean#buscarTramitesParaAsignacion");

        List<Tramite> answer;
        answer = this.tramiteDao.buscarTramitesAsignadosAEjecutor(
            territorialId, ejecutor, rowStartIdxAndCount);

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo para consultar la configuracion de las secciones para la ejecucion de un tramite
     *
     * @author fredy.wilches
     */
    /*
     * @modified juan.agudelo @modified pedro.garcia 10-08-2012 manejo de excepciones @modified
     * pedro.garcia 19-09-2012 Se eliminó el parámetro tipoInscripcion ya que ese campo no se usa en
     * los trámites.
     */
    @Implement
    @Override
    public TramiteSeccionDatos consultarConfiguracionTramite(
        String tipoTramite, String claseMutacion, String subtipo, String radicacionEspecial) {

        LOGGER.debug("en TramiteBean#consultarConfiguracionTramite");

        TramiteSeccionDatos answer = null;
        try {
            answer = this.tramiteDao.getConfiguracionTramite(tipoTramite, claseMutacion, subtipo,
                radicacionEspecial);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#consultarConfiguracionTramite: " + ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo para consultar un tramite por llave primaria, junto con la solicitud
     *
     * @author fredy.wilches
     */
    @Override
    public Tramite consultarTramite(Long tramiteId) {
        LOGGER.debug("en TramiteBean#consultarTramite");
        return this.tramiteDao.findById(tramiteId);
    }

    /**
     * Método que ejecuta un SP para generar la proyección de un trámite
     */
    @Override
    public Object[] generarProyeccion(Long tramiteId) {
        try {
            return this.sncProcedimientoDao.generarProyeccion(tramiteId);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see ITramite#generarProyeccionCancelacionBasica(Long, String)
     */
    @Override
    public Object[] generarProyeccionCancelacionBasica(Long tramiteId, String usuario) {
        try {
            return this.sncProcedimientoDao.generarProyeccionCancelacionBasica(tramiteId, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#borrarComisionesTramites(UsuarioDTO, List<ComisionTramite>)
     */
    @Implement
    @Override
    public boolean borrarComisionesTramites(UsuarioDTO loggedInUser,
        List<ComisionTramite> comisionTramiteRels) {

        LOGGER.debug("en TramiteBean#borrarComisionesTramites");

        boolean answer = true;

        try {
            for (ComisionTramite comisionTramite : comisionTramiteRels) {
                this.comisionTramiteDao.delete(comisionTramite);
            }
        } catch (Exception ex) {
            answer = false;
            LOGGER.error("Error en TramiteBean#borrarComisionesTramites: " + ex.getMessage());
        }

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#borrarRelacionTramiteComision(co.gov.igac.generales.dto.UsuarioDTO, Long, Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void borrarRelacionTramiteComision(UsuarioDTO loggedInUser,
        Long tramiteId, Long comisionId) {

        LOGGER.debug("en TramiteBean#borrarRelacionTramiteComision ...");

        try {
            this.comisionTramiteDao.deleteRow(tramiteId, comisionId);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "TramiteBean#borrarRelacionTramiteComision");
        }
        LOGGER.debug("TramiteBean#borrarRelacionTramiteComision finished.");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author fabio.navarrete
     * @modified felipe.cadena -- se agrega el parametro usuario
     * @see ITramite#obtenerSolicitudDeCorrespondencia(String)
     */
    @Override
    public Solicitud obtenerSolicitudAvaluosCorrespondencia(
        String numeroRadicacionCorrespondencia, UsuarioDTO usuario) {
        return this.sncProcedimientoDao
            .obtenerRadicacionCorrespondencia(numeroRadicacionCorrespondencia, usuario);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see ITramite#actualizarComision(UsuarioDTO, Comision)
     */
    @Override
    public Comision actualizarComision(UsuarioDTO loggedInUser, Comision comision) {
        return this.comisionDao.updateComision(loggedInUser, comision);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see ITramite#contarTramitesDeFuncionarioEjecutor(java.lang.String)
     */
    @Override
    public Integer contarTramitesDeFuncionarioEjecutor(String ejecutor) {
        LOGGER.debug("en TramiteBean#contarTramitesDeFuncionarioEjecutor");
        int answer;
        answer = this.tramiteDao.contarTramitesDeFuncionarioEjecutor(ejecutor);
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @see ITramite#buscarComisionesPorIdTerritorialAndEstadoComision(String, String)
     */
    @Override
    public List<Comision> buscarComisionesPorIdTerritorialAndEstadoComision(
        String territorialId, String estadoComision) {

        LOGGER.debug("en TramiteBean#buscarComisionPorIdTerritorialAndEstadoComision");

        List<Comision> comisiones = null;
        List<String> funcionariosEjecutoresComision;

        try {
            comisiones = this.comisionDao
                .findComisionesPorIdTerritorialAndEstadoComision(territorialId,
                    estadoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarComisionesPorIdTerritorialAndEstadoComision " +
                "buscando las comisiones para la territorial con id " + territorialId + " y " +
                "con estado " + estadoComision + ": " + ex.getMensaje());
            return comisiones;
        }

        try {
            for (int i = 0; i < comisiones.size(); i++) {
                funcionariosEjecutoresComision = this.comisionDao
                    .findFuncionariosEjecutoresByComision(comisiones.get(i)
                        .getId(), territorialId, estadoComision);
                comisiones.get(i).setEjecutores(funcionariosEjecutoresComision);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarComisionesPorIdTerritorialAndEstadoComision " +
                "buscando los funcionarios ejecutores por comisión: " + ex.getMensaje());
        }
        return comisiones;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     * @modified felipe.cadena -- Se agrega el parametro usuario
     * @see ITramite#obtenerSolicitudInternaCorrespondencia(String)
     */
    @Override
    public Solicitud obtenerSolicitudInternaCorrespondencia(
        String numeroSolicitud, UsuarioDTO usuario) {
        FiltroDatosSolicitud filtroDatosSolicitud = new FiltroDatosSolicitud();
        filtroDatosSolicitud.setNumero(numeroSolicitud);
        Solicitud sol = solicitudDao
            .findSolicitudByNumSolicitudFetchGestionarSolicitudData(numeroSolicitud);
        if (sol == null) {
            sol = sncProcedimientoDao
                .obtenerRadicacionCorrespondencia(numeroSolicitud, usuario);
        }
        return sol;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarSolicitudAvisoRegistro(SolicitudAvisoRegistro)
     */
    @Override
    public void guardarSolicitudAvisoRegistro(
        SolicitudAvisoRegistro solicitudAviso) {

        LOGGER.debug("TramiteBean#guardarSolicitudAvisoRegistro begins...");
        this.solicitudAvisoRegistroDao.persist(solicitudAviso);
        LOGGER.debug("... ends TramiteBean#guardarSolicitudAvisoRegistro");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el listado de solicitantes de una solicitud
     *
     * @author fredy.wilches
     */
    @Override
    public List<SolicitanteSolicitud> obtenerSolicitantesSolicitud(
        Long solicitudId) {
        LOGGER.debug("TramiteBean#guardarSolicitudAvisoRegistro begins...");
        return this.solicitanteSolicitudDao.findBySolicitudId(solicitudId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#obtenerInfoAvisosRechazadosPorSolicitud(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvisoRegistroRechazo> obtenerInfoAvisosRechazadosPorSolicitud(
        String numeroSolicitud) {
        LOGGER.debug("TramiteBean#obtenerInfoAvisosRechazadosPorSolicitud begins...");

        List<AvisoRegistroRechazo> answer = null;
        List<AvisoRegistroRechazoPredio> prediosRechazo;
        Predio predio;
        String numeroPredial;

        answer = this.avisoRegistroRechazoDao
            .findByNumeroSolicitud(numeroSolicitud);
        for (AvisoRegistroRechazo arr : answer) {
            prediosRechazo = arr.getAvisoRegistroRechazoPredios();
            for (AvisoRegistroRechazoPredio arrp : prediosRechazo) {
                numeroPredial = arrp.getNumeroPredial();
                predio = this.predioDao.findByNumeroPredial(numeroPredial);
                arrp.setPredio(predio);
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author david.cifuentes
     * @see ITramite#findTextoMotivoByNumeroMotivo(String numeroMotivo)
     */
    @Override
    public String findTextoMotivoByNumeroMotivo(String numeroMotivo) {
//TODO :: david.cifuentes :: hacer manejo de excepciones según el estándar del proyecto :: pedro.garcia  
        return this.tramiteDao.findByNumeroMotivo(numeroMotivo);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#guardarTramiteTextoResolucion(TramiteTextoResolucion)
     */
    @Override
    public TramiteTextoResolucion guardarTramiteTextoResolucion(
        TramiteTextoResolucion tramiteTextoResolucion) {

        LOGGER.debug("en TramiteBean#guardarTramiteTextoResolucion");
        TramiteTextoResolucion answer = null;

        try {
            answer = this.tramiteTextoResolucionDao.update(tramiteTextoResolucion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#guardarTramiteTextoResolucion: " +
                ex.getMensaje());
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramite#archivarTramite(UsuarioDTO, Tramite)
     */
    @Override
    public boolean archivarTramite(UsuarioDTO usuario,
        Tramite tramiteParaArchivar) {
        return this.tramiteDao.archivarTramite(usuario, tramiteParaArchivar);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * calcula la duración de un trámite -que se supone que como se va a comisionar es de tipo
     * MUTACIÓN- con base en los parámetros de la tabla DURACION_TRAMITE.
     *
     * OJO: en esa tabla (DURACION_TRAMITE) puede haber varios registros para los tipos de trámites,
     * que se van insertando dependiendo de la vigencia de la norma, por lo que hay que tener en
     * cuenta las fechas desde y hasta.
     *
     * Los trámites de comisión son todos de tipo MUTACIÓN
     *
     * Pre: los trámites que lleguen a este método deben haber sido consultados haciendo fetch del
     * predio (jpql)
     *
     * @author pedro.garcia
     */
    /*
     * @modified pedro.garcia 11-09-2012 Cambia la regla de que todos los trámites son de tipo
     * 'mutación'. Ahora se tiene en cuenta cualquier tipo de trámite
     */
    private void calcAndSetDuracionTramiteDeComision(Tramite tramite, String idTerritorial) {

        double duracionTramite;
        String tipoAvaluo, codigoMunicipio, esSedeString, numeroPredialMutacionQuinta;
        Predio predioTramite;
        boolean esSede, esRural;
        ArrayList<TramitePredioEnglobe> tramitePrediosEnglobe;
        double areaPredios = 0.0;
        Integer normaDia = null;
        String mensaje = "";

        esRural = false;
        esSede = false;
        codigoMunicipio = "";
        tipoAvaluo = "";
        esSedeString = "";
        duracionTramite = 0.0;

        try {

            // D: los trámites de comisión son todos de tipo MUTACIÓN
            if (tramite.getTipoTramite().equalsIgnoreCase(ETramiteTipoTramite.MUTACION.getCodigo())) {

                // D: hay que diferenciar si se trata de un englobe (caso 1) o no:
                // si es englobe se pueden dar dos casos:
                // 1.1. se mantiene uno de los números prediales como el resultante
                // 1.2. se crea un nuevo predio
                // => en el caso de englobe se deben sumar las áreas de los predios de la tabla
                // TRAMITE_PREDIO_ENGLOBE y se toma alguno de los predios para
                // sacar el número predial y los datos derivados de éste.
                // D: por definición de la tabla el campo 'clase mutación' del trámite puede ser
                //    null, por lo que hay que validar eso también; aunque se supone que
                // si es mutación no debería pasar eso
                if (tramite.getClaseMutacion() != null) {

                    // D: como se trabaja con mutaciones, hay que tener en cuenta los casos de
                    //    segunda o de quinta:
                    // si es de segunda, puede ser englobe, por lo que hay que revisar los predios
                    // del englobe (tabla TRAMITE_PREDIO_ENGLOBE).
                    // si es de quinta los datos del predio vienen en el trámite
                    if (tramite.getClaseMutacion().equalsIgnoreCase(
                        EMutacionClase.SEGUNDA.getCodigo())) {

                        if (tramite.getSubtipo() != null &&
                            tramite.getSubtipo().equalsIgnoreCase(
                                EMutacion2Subtipo.ENGLOBE.getCodigo())) {

                            tramitePrediosEnglobe =
                                (ArrayList<TramitePredioEnglobe>) this.tramitePredioEnglobeDao.
                                    getByTramiteId(tramite.getId());

                            // D: debe tener al menos dos tramite-predio-englobe
                            predioTramite = tramitePrediosEnglobe.get(0).getPredio();
                            tipoAvaluo = predioTramite.getTipoAvaluo();
                            codigoMunicipio = predioTramite.getCodigoMunicipio();

                            // D: si es rural, se tienen en cuenta las áreas de los predios
                            if (tipoAvaluo.equalsIgnoreCase(ETipoAvaluo.RURAL.getCodigo())) {
                                for (TramitePredioEnglobe tpe : tramitePrediosEnglobe) {
                                    predioTramite = tpe.getPredio();
                                    if (predioTramite.getAreaTerreno() != null) {
                                        areaPredios += predioTramite.getAreaTerreno();
                                    }
                                }
                                esRural = true;
                            }
                        }
                        // D: cuando es un Desenglobe, el predio es el asociado al trámite
                        // (ver caso más abajo)
                    } // D: para el caso de mutaciones de quinta los datos del predio se toman de los
                    //    campos especiales adicionados en la tabla TRAMITE para este tipo de casos
                    else if (tramite.getClaseMutacion().equalsIgnoreCase(
                        EMutacionClase.QUINTA.getCodigo())) {
                        numeroPredialMutacionQuinta = tramite.getNumeroPredial();

                        // D: por la estructura del número predial se sabe en
                        // qué parte están estos datos
                        tipoAvaluo = numeroPredialMutacionQuinta.substring(5, 7);
                        codigoMunicipio = numeroPredialMutacionQuinta.substring(0, 5);

                        // D: si es rural, se tienen en cuenta el área del predio
                        if (tipoAvaluo.equalsIgnoreCase(ETipoAvaluo.RURAL.getCodigo())) {
                            if (tramite.getAreaTerreno() != null) {
                                areaPredios += tramite.getAreaTerreno();
                            }
                            esRural = true;
                        }
                    } // D: si no es de segunda ni de quinta, o es de segunda Desenglobe,
                    // los datos del predio vienen en el predio asociado al trámite
                    else {
                        if (tramite.getPredio() != null) {
                            predioTramite = tramite.getPredio();
                            tipoAvaluo = predioTramite.getTipoAvaluo();
                            codigoMunicipio = predioTramite.getCodigoMunicipio();

                            // D: si es rural, se tienen en cuenta el área del predio
                            if (tipoAvaluo.equalsIgnoreCase(ETipoAvaluo.RURAL.getCodigo())) {
                                if (predioTramite.getAreaTerreno() != null) {
                                    areaPredios += predioTramite.getAreaTerreno();
                                }
                                esRural = true;
                            }
                        } else {
                            mensaje = "El trámite no tiene un Predio asociado";
                        }
                    }

                    // D: en este punto ya se sabe si es rural o no
                    // Si es urbano hay que discriminar si es en la sede o no
                    if (!esRural) {
                        esSede = this.generalesService.
                            averiguarSiMunicipioEsSede(codigoMunicipio, idTerritorial);
                        esSedeString = (esSede) ? ESiNo.SI.getCodigo() : ESiNo.NO.getCodigo();
                    }

                    // D: ya tengo los 3 datos que necesito para la consulta:
                    // tipo_avaluo, si es sede, área
                    normaDia = this.duracionTramiteDao.getNormaDia(tipoAvaluo, esSedeString,
                        areaPredios);
                }

                if (normaDia == null) {
                    LOGGER.error("La norma de duración por día para el trámite dio null");
                }
                if (normaDia != null && normaDia.intValue() != 0) {
                    duracionTramite = 1 / (double) normaDia.intValue();
                    duracionTramite = Utilidades.roundToDecimalsAndCeil(duracionTramite, 2);
                }

            }// end of if es mutacion
            //D: si el trámite no es de mutación. En este caso se supone que siempre hay un predio
            //   asociado al trámite
            else {
                if (tramite.getPredio() != null) {
                    predioTramite = tramite.getPredio();
                    tipoAvaluo = predioTramite.getTipoAvaluo();
                    codigoMunicipio = predioTramite.getCodigoMunicipio();

                    // D: si es rural, se tienen en cuenta el área del predio
                    if (tipoAvaluo.equalsIgnoreCase(ETipoAvaluo.RURAL.getCodigo())) {
                        if (predioTramite.getAreaTerreno() != null) {
                            areaPredios += predioTramite.getAreaTerreno();
                        }
                        esRural = true;
                    }

                    if (!esRural) {
                        esSede = this.generalesService.
                            averiguarSiMunicipioEsSede(codigoMunicipio, idTerritorial);
                        esSedeString = (esSede) ? ESiNo.SI.getCodigo() : ESiNo.NO.getCodigo();
                    }

                    // D: ya tengo los 3 datos que necesito para la consulta:
                    // tipo_avaluo, si es sede, área
                    normaDia = this.duracionTramiteDao.getNormaDia(tipoAvaluo, esSedeString,
                        areaPredios);

                    if (normaDia != null && normaDia.intValue() != 0) {
                        duracionTramite = 1 / (double) normaDia.intValue();
                        duracionTramite = Utilidades.roundToDecimalsAndCeil(duracionTramite, 2);
                    }

                } else {
                    mensaje = "El trámite no tiene un Predio asociado";
                }

            }

            //D: se setea el dato en el trámite que llega como parámetro
            tramite.setDuracionTramite(duracionTramite);

        }// end of try
        catch (Exception e) {
            LOGGER.error("Error en TramiteBean#calcAndSetDuracionTramite: " + mensaje + ": " +
                e.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see ITramite#buscarTramiteSolicitudPorId(Long)
     */
    @Override
    public Tramite buscarTramiteSolicitudPorId(Long tramiteId) {
        return this.tramiteDao.buscarTramiteSolicitudPorId(tramiteId);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see ITramite#buscarTramiteTextoResolucionPorIdTramite(Long)
     */
    @Override
    public TramiteTextoResolucion buscarTramiteTextoResolucionPorIdTramite(
        Long id) {
        return this.tramiteTextoResolucionDao
            .findTramiteTextoResolucionPorIdTramite(id);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see ITramite#buscarTramiteEstadoVigentePorTramiteId(Long)
     */
    @Override
    public TramiteEstado buscarTramiteEstadoVigentePorTramiteId(Long id) {
        return this.tramiteEstadoDao
            .obtenerTramiteEstadoVigentePorTramiteId(id);
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#findModelosResolucionByTramite(String tipoTramite, String claseMutacion)
     */
    @Override
    public List<ModeloResolucion> findModelosResolucionByTipoTramite(
        String tipoTramite, String claseMutacion, String subtipo) {
//TODO :: david.cifuentes :: hacer manejo de errores según se definió en el estándar :: pedro.garcia
        return this.modeloResolucionDao.findModelosResolucionByTramite(
            tipoTramite, claseMutacion, subtipo);
    }
    
    /**
     * @author carlos.ferro
     * @see ITramite#findModelosResolucionByTramite(String tipoTramite, String claseMutacion, String subtipo, String delegada)
     */
    @Override
    public List<ModeloResolucion> findModelosResolucionByTipoTramite(
        String tipoTramite, String claseMutacion, String subtipo, String delegada) {
        
        return this.modeloResolucionDao.findModelosResolucionByTramite(
            tipoTramite, claseMutacion, subtipo, delegada);
    }    

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarEjecutoresPorNumeroComision(java.lang.String)
     */
    @Implement
    @Override
    public List<VEjecutorComision> buscarEjecutoresPorNumeroComision(String numeroComision) {

        LOGGER.debug("on TramiteBean#buscarEjecutoresPorNumeroComision ...");
        List<VEjecutorComision> answer = null;

        try {
            answer = this.vEjecutorComisionDAo.findByNumeroComision(numeroComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error: " + ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramitesDeEjecutorEnComision(String, Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesDeEjecutorEnComision(
        String idFuncionarioEjecutor, Long idComision) {

        LOGGER.debug("en TramiteBean#buscarTramitesDeEjecutorEnComision ...");
        List<Tramite> answer = null;
        try {
            answer = this.tramiteDao.findByEjecutorAndComision(idFuncionarioEjecutor, idComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.debug("Error en TramiteBean#buscarTramitesDeEjecutorEnComision: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#actualizarEstadoTramites(java.util.List, java.lang.String)
     *
     * Se hizo separado, pero utilizando el método que ya estaba implementado para actuialización
     * masiva de trámites
     */
    @Override
    public void actualizarEstadoTramites(
        List<Tramite> tramitesEjecutorComision, String estado) {

        Tramite[] tramitesArray = new Tramite[tramitesEjecutorComision.size()];
        int i = 0;

        for (Tramite t : tramitesEjecutorComision) {
            t.setEstado(estado);
            tramitesArray[i] = t;
            i++;
        }
        actualizarTramites(tramitesArray);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerDuracionComision(java.lang.Long)
     */
    @Implement
    @Override
    public Double obtenerDuracionComision(Long idComision) {

        Double answer = null;
        try {
            answer = this.comisionTramiteDao.getComisionDuration(idComision);
        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la duración de una comisión: " + ex.getMessage());
        }

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see ITramite#crearTramiteDocumento(TramiteDocumento)
     */
    @Override
    public void crearTramiteDocumento(TramiteDocumento tramiteDocumento) {

        LOGGER.debug("en TramiteBean#crearTramiteDocumento");

        try {
            this.tramiteDocumentoDao.persist(tramiteDocumento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#crearTramiteDocumento: " +
                ex.getMensaje());
        }

    }

    /**
     * @author david.cifuentes
     * @see ITramite#buscarSolicitudPorFiltros(VSolicitudPredio datosFiltroSolicitud )
     */
    @Override
    public List<VSolicitudPredio> buscarSolicitudPorFiltros(
        VSolicitudPredio datosFiltroSolicitud, int desde, int cantidad) {
//TODO :: david.cifuentes :: hacer manejo de errores según se definió en el estándar :: pedro.garcia
        return this.vSolicitudPredioDao.buscarSolicitudPorFiltros(
            datosFiltroSolicitud, desde, cantidad);
    }

    /**
     * @author david.cifuentes
     * @see ITramite#buscarSolicitudPorFiltros(VSolicitudPredio datosFiltroSolicitud )
     */
    @Override
    public List<Solicitante> buscarSolicitantesPorFiltro(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante, boolean cargarPaisDeptoYMuni) {
//TODO :: david.cifuentes :: hacer manejo de errores según se definió en el estándar :: pedro.garcia
        return this.solicitanteDao
            .buscarSolicitantesPorFiltro(datosFiltroSolicitante, cargarPaisDeptoYMuni, null);
    }

    /**
     * @author christian.rodriguez
     * @see
     * ITramite#buscarSolicitantesPorFiltroConContratoInteradministrativo(FiltroDatosConsultaSolicitante)
     */
    @Override
    public List<Solicitante> buscarSolicitantesPorFiltroConContratoInteradministrativo(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante) {

        List<Solicitante> solicitantes = null;
        try {
            solicitantes = this.solicitanteDao
                .buscarSolicitantesPorFiltroConContratoInteradministrativo(datosFiltroSolicitante);
        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo los solcitantes con contrato interadministrativo: " +
                ex.getMessage());
        }
        return solicitantes;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarPrediosDeTramite(java.lang.String)
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosDeTramite(Long tramiteId) {

        List<Predio> answer = null;
        try {
            answer = this.tramiteDao.getPrediosOfTramite(tramiteId);
        } catch (Exception ex) {
            LOGGER.error("Excepción consultando los predios de un trámite: " + ex.getMessage());

        }

        return answer;
    }

    /**
     * @see ITramite#buscarPrediosDeTramite(java.lang.String)
     */
    @Implement
    @Override
    public List<Predio> buscarPrediosDeTramiteQuintaNuevo(Long tramiteId) {

        List<Predio> answer = null;
        try {
            answer = this.tramiteDao.getPrediosOfTramiteQuintaNuevo(tramiteId);
        } catch (Exception ex) {
            LOGGER.error("Excepción consultando los predios de un trámite: " + ex.getMessage());

        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerDatosRectificarPorTipo(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<DatoRectificar> obtenerDatosRectificarPorTipo(String tipoDatoRectifCompl) {

        List<DatoRectificar> answer;
        answer = this.datoRectificarDao.findByTipo(tipoDatoRectifCompl);
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerDatosRectificarPorTipoYNaturaleza(String, String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Implement
    @Override
    public List<DatoRectificar> obtenerDatosRectificarPorTipoYNaturaleza(
        String tipoDatoRectifCompl, String naturaleza) {

        LOGGER.debug("En TramiteBean#obtenerDatosRectificarPorTipoYNaturaleza ");
        List<DatoRectificar> answer = null;

        try {
            answer = this.datoRectificarDao.findByTipoAndNaturaleza(tipoDatoRectifCompl, naturaleza);
        } catch (ExcepcionSNC snc) {
            LOGGER.error("Error en TramiteBean#obtenerDatosRectificarPorTipoYNaturaleza: " +
                snc.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarActualizarAvisoRegistroRechazo(UsuarioDTO, Solicitud)
     */
    @Override
    public AvisoRegistroRechazo guardarActualizarAvisoRegistroRechazo(
        UsuarioDTO usuario, AvisoRegistroRechazo avisoRechazo) {
        avisoRechazo.setUsuarioLog(usuario.getLogin());
        avisoRechazo.setFechaLog(new Date());
        return this.avisoRegistroRechazoDao.update(avisoRechazo);
    }

    @Override
    public DetallesDTO detallesPredioDeTramite(Long tramiteId) {
        // TODO Auto-generated method stub
        return this.tramiteDao.detallesPredioDeTramite(tramiteId);
    }

    /**
     * @author fabio.navarrete
     * @modified felipe.cadena -- se agrega el parametro usuario
     * @see ITramite#obtenerSolicitudCompletaInternaCorrespondencia(String)
     */
    @Override
    public Solicitud obtenerSolicitudCompletaInternaCorrespondencia(
        String numeroSolicitud, UsuarioDTO usuario) {
        FiltroDatosSolicitud filtroDatosSolicitud = new FiltroDatosSolicitud();
        filtroDatosSolicitud.setNumero(numeroSolicitud);
        Solicitud sol = solicitudDao
            .findSolicitudByNumSolicitudFetchGestionarSolicitudesData(numeroSolicitud);
        if (sol == null) {
            sol = this.sncProcedimientoDao
                .obtenerRadicacionCorrespondencia(numeroSolicitud, usuario);
        }
        return sol;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#conseguirDocumentacionTramiteRequerida(Tramite)
     * @author juan.agudelo
     */
    @Override
    public List<TramiteRequisito> conseguirDocumentacionTramiteRequerida(Tramite tramite) {

        List<TramiteRequisito> answer = null;
        boolean predioTemporal = false;
        boolean zonaTemporal = false;
        boolean condicionTemporal = false;

        try {

            // Si el trámite no cuenta con un predio asociado, se crea uno
            // ficticio para recuperar los documentos del trámite, pues
            // la consulta cuenta con un filtro de zona unidad organica.
            // ADICIÓN PREDIO TEMPORAL
            if (tramite.getPredio() == null) {
                Predio predioTemp = new Predio();
                predioTemp
                    .setZonaUnidadOrganica(EPredioZonaUnidadOrganica.RURAL
                        .getCodigo());
                tramite.setPredio(predioTemp);
                predioTemporal = true;
            } else if (tramite.getPredio().getZonaUnidadOrganica() == null ||
                tramite.getPredio().getZonaUnidadOrganica().isEmpty()) {
                // Revisión de información de predio asociado
                tramite.getPredio().setZonaUnidadOrganica(EPredioZonaUnidadOrganica.RURAL.
                    getCodigo());
                if ("Sin registro".equals(tramite.getPredio().getCondicionPropiedad())) {
                    tramite.getPredio().setCondicionPropiedad(EPredioCondicionPropiedad.CP_0.
                        getCodigo());
                    condicionTemporal = true;
                }
                zonaTemporal = true;
            }

            answer = this.tramiteRequisitoDao
                .conseguirDocumentacionTramiteRequerida(tramite);

            // REMOVER PREDIO TEMPORAL
            if (predioTemporal) {
                tramite.setPredio(null);
            }
            if (zonaTemporal) {
                tramite.getPredio().setZonaUnidadOrganica(null);
            }
            if (condicionTemporal) {
                tramite.getPredio().setCondicionPropiedad("Sin registro");
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#conseguirDocumentacionTramiteRequerida: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerTiposDeTramitePorSolicitud(java.lang.String)
     */
    @Implement
    @Override
    public List<TipoSolicitudTramite> obtenerTiposDeTramitePorSolicitud(
        String tipoSolicitud) {

        List<TipoSolicitudTramite> answer = null;
        answer = tipoSolicitudTramiteDao.findByTipoSolicitud(tipoSolicitud);

        if (!answer.isEmpty()) {
            for (TipoSolicitudTramite tsts : answer) {
                setValorTipoTramite(tsts);
            }
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * método que asigna el valor (tomado de la tabla DOMINIO) de tipo de trámite para un
     * TipoSolicitudTramite
     *
     * @author pedro.garcia
     * @param tipoSolTram
     */
    private void setValorTipoTramite(TipoSolicitudTramite tipoSolTram) {

        String auxDom = tipoSolTram.getTipoTramite();

        if (auxDom != null && !auxDom.equals("")) {
            Dominio dom = dominioDao.findByCodigo(
                EDominio.TRAMITE_TIPO_TRAMITE, auxDom);
            if (dom != null) {
                tipoSolTram.setTipoTramiteValor(dom.getValor());
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author david.cifuentes
     * @see ITramite#actualizarTramite(Tramite tramite)
     */
    @Override
    public boolean actualizarTramite(Tramite tramite, UsuarioDTO usuario) {
        LOGGER.debug("TramiteBean#actualizarTramite");

        boolean answer = true;
        tramite.setFechaLog(new Date());
        tramite.setUsuarioLog(usuario.getLogin());
        try {
            if (tramite.getTramites() != null && tramite.getSolicitud() != null) {
                // Se actualiza la fechaLog y usuarioLog de los
                // trámites asociados al trámite.
                for (Tramite t : tramite.getTramites()) {
                    t.setFechaLog(new Date());
                    t.setUsuarioLog(usuario.getLogin());
                }
            }
        } catch (org.hibernate.LazyInitializationException mfLazyEx) {
            LOGGER.error("Algo que no fue inicializado en la consulta trató de ser obtenido del " +
                "trámite: " + mfLazyEx.getMessage());
        } finally {
            try {
                this.tramiteDao.update(tramite);
            } catch (Exception ex) {
                LOGGER.error("Error en TramiteBean#actualizarTramite: " + ex.getMessage());
                answer = false;
            }

            return answer;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#actualizarTramite(co.gov.igac.snc.persistence.entity.tramite.Tramite)
     * @ modified by leidy.gonzalez 23/07/2014 Se valida que el campo tramite no llegue nulo
     */
    @Override
    public boolean actualizarTramite(Tramite tramite) {

        LOGGER.debug("TramiteBean#actualizarTramite");

        boolean answer = true;
        tramite.setFechaLog(new Date(System.currentTimeMillis()));

        if (null != tramite && null != tramite.getTramites() && !tramite.getTramites().isEmpty()) {
            for (Tramite t : tramite.getTramites()) {
                t.setFechaLog(new Date());
            }
        }
        try {
            this.tramiteDao.update(tramite);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#actualizarTramite: " + ex.getMessage());
            answer = false;
        }

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramiteLocal#actualizarTramiteDocumentos(Tramite, UsuarioDTO)
     */
    /*
     * @modified pedro.garcia 04-06-2012 + se valida si el nombre de archivo que traen los
     * documentos contiene la ruta completa. Si no, se le adiciona porque alfresco la requiere +
     * estaba enviando el TipoDocumento completo como string
     */
    @Implement
    @Override
    public Tramite actualizarTramiteDocumentos(Tramite tramite, UsuarioDTO usuario) {
        LOGGER.debug("TramiteBean#actualizarTramiteDocumentos");

        Documento documentoTemp;
        String rutaCompletaArchivo;
        String rutaArchivosTemporales;
        String archivoDocumento;

        DocumentoTramiteDTO documentoTramiteDTO;

        tramite.setFechaLog(new Date());
        tramite.setUsuarioLog(usuario.getLogin());
        String fileSeparator = System.getProperty("file.separator");

        try {

            rutaArchivosTemporales =
                FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator;

            if (tramite.getTramiteDocumentacions() != null &&
                !tramite.getTramiteDocumentacions().isEmpty()) {

                for (TramiteDocumentacion tdTmp : tramite.getTramiteDocumentacions()) {

                    documentoTemp = this.documentoDao.update(tdTmp.getDocumentoSoporte());

                    archivoDocumento = tdTmp.getDocumentoSoporte().getArchivo();

                    if (!archivoDocumento.equals(Constantes.CONSTANTE_CADENA_VACIA_DB)) {

                        rutaCompletaArchivo =
                            (archivoDocumento.contains(rutaArchivosTemporales)) ? archivoDocumento :
                            rutaArchivosTemporales.concat(archivoDocumento);

                        String nPredial;
                        if (!tramite.isQuinta()) {
                            nPredial = tramite.getPredio().getNumeroPredial();
                        } else {
                            nPredial = tramite.getNumeroPredial();
                        }
                        documentoTramiteDTO = new DocumentoTramiteDTO(
                            rutaCompletaArchivo,
                            tdTmp.getTipoDocumento().getNombre(),
                            tramite.getFechaRadicacion(),
                            tramite.getDepartamento().getNombre(),
                            tramite.getMunicipio().getNombre(),
                            nPredial,
                            tramite.getTipoTramite().toString(),
                            tramite.getId());

                        try{
                            if (!tramite.isViaGubernativa() &&
                                !tramite.isQuinta()) {

                                    documentoTemp = this.documentoDao.guardarDocumentoGestorDocumental(
                                        usuario, documentoTramiteDTO, documentoTemp);
                            } else {
                                this.documentoDao.guardarDocumentoGestorDocumentalSinNumeroPredial(
                                    usuario, documentoTramiteDTO,
                                    documentoTemp);
                            }
                        } catch (ExcepcionSNC ex) {
                            LOGGER.error("Ocurrió un error guardando documento  en el FTP");
                        }
                    }

                    tdTmp.setDocumentoSoporte(documentoTemp);

                }
            }

            if (tramite.getTramiteDocumentos() != null &&
                !tramite.getTramiteDocumentos().isEmpty()) {
                for (TramiteDocumento tdocTmp : tramite.getTramiteDocumentos()) {

                    if (tdocTmp.getId() == null &&
                        tdocTmp.getDocumento() != null) {

                        documentoTemp = this.documentoDao.update(tdocTmp.getDocumento());

                        archivoDocumento = tdocTmp.getDocumento().getArchivo();

                        if (!archivoDocumento.equals(Constantes.CONSTANTE_CADENA_VACIA_DB)) {

                            rutaCompletaArchivo = (archivoDocumento
                                .contains(rutaArchivosTemporales)) ? archivoDocumento :
                                rutaArchivosTemporales
                                    .concat(archivoDocumento);

                            // TODO:: al que corresponda::06-06-2012::remover cuando
                            // el api de alfresco solo consuma el número predial
                            Predio predioTmp;

                            if (!tramite.isQuinta()) {
                                predioTmp = this.tramiteDao
                                    .getPredioDptoMuniByTramiteId(tramite
                                        .getId());
                            } else {
                                predioTmp = new Predio();
                                predioTmp.setDepartamento(new Departamento());
                                predioTmp.setMunicipio(new Municipio());
                                predioTmp.getDepartamento().setCodigo(
                                    tramite.getNumeroPredial().substring(0, 2));
                                predioTmp.getMunicipio().setCodigo(
                                    tramite.getNumeroPredial().substring(2, 5));
                            }

                            documentoTramiteDTO = new DocumentoTramiteDTO(
                                rutaCompletaArchivo, tdocTmp.getDocumento()
                                    .getTipoDocumento().getNombre(),
                                tramite.getFechaRadicacion(), predioTmp
                                .getDepartamento().getNombre(),
                                predioTmp.getMunicipio().getNombre(),
                                predioTmp.getNumeroPredial(), tramite
                                .getTipoTramite().toString(),
                                tramite.getId());

                            try{
                                documentoTemp = this.documentoDao
                                    .guardarDocumentoGestorDocumental(usuario,
                                        documentoTramiteDTO, documentoTemp);
                            } catch (ExcepcionSNC ex) {
                                LOGGER.error("Ocurrió un error guardando documento en el FTP");
                            }

                        }

                        tdocTmp.setDocumento(documentoTemp);

                    }
                }
            }
            //modified by franz.gamba :: se traen los documentos del tramite ya que el
            //update solo retorna persistentBags
            Tramite answer = this.tramiteDao.update(tramite);
            answer.setTramiteDocumentos(
                this.obtenerTramiteDocumentosPorTramiteId(answer.getId()));
            answer.setTramiteDocumentacions(
                this.obtenerTramiteDocumentacionPorIdTramite(answer.getId()));
            return answer;

        } catch (Exception ex) {
            LOGGER.error("error en TramiteBean#actualizarTramiteDocumentos: " + ex.getMessage());
            return null;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramiteLocal#actualizarTramiteDocumentosNotificacion(Tramite, Documento, Documento,
     * UsuarioDTO)
     * @deprecated ver documentación en la interfaz
     */
    /*
     * @documented pedro.garcia @modified pedro.garcia 17-07-2012 deprecated
     */
    @Override
    @Deprecated
    public Tramite actualizarTramiteDocumentosNotificacion(Tramite tramite,
        Documento documentoAutorizacion, Documento documentoNotificacion,
        TramiteDocumento tramiteDocumentoNotificacion, UsuarioDTO usuario) {
        LOGGER.debug("TramiteBean#actualizarTramiteDocumentosNotificacion");

        Documento documentoAutTemp = null;
        Documento documentoNotTemp = null;
        DocumentoTramiteDTO documentoTramiteDTO;
        Tramite answer;

        tramite.setFechaLog(new Date());
        tramite.setUsuarioLog(usuario.getLogin());

        String fileSeparator = System.getProperty("file.separator");

        try {

            Predio predioTmp;
            if (!tramite.isQuinta()) {
                predioTmp = this.tramiteDao.getPredioDptoMuniByTramiteId(tramite.getId());
            } else {
                predioTmp = new Predio();
                predioTmp.setDepartamento(new Departamento());
                predioTmp.setMunicipio(new Municipio());
                predioTmp.getDepartamento().setCodigo(tramite.getNumeroPredial().substring(0, 2));
                predioTmp.getMunicipio().setCodigo(tramite.getNumeroPredial().substring(2, 5));
            }

            //D: si el documento de autorización no tiene id significa que es nuevo, por lo que
            //   se debe armar el DTO para guardarlo en el gestor documental
            if (documentoAutorizacion != null && documentoAutorizacion.getId() == null) {

                String tipoDocumento = "Default";

                for (ETipoDocumento tdTmp : ETipoDocumento.values()) {
                    if (tdTmp.getId().equals(documentoNotificacion.getTipoDocumento().getId())) {
                        tipoDocumento = tdTmp.getNombre();
                    }
                }

                documentoTramiteDTO = new DocumentoTramiteDTO(
                    FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                    documentoAutorizacion.getArchivo(),
                    tipoDocumento,
                    tramite.getFechaRadicacion(),
                    predioTmp.getDepartamento().getNombre(), predioTmp
                    .getMunicipio().getNombre(),
                    predioTmp.getNumeroPredial(), tramite.getTipoTramite()
                    .toString(), tramite.getId());

                documentoAutTemp = this.documentoDao.update(documentoAutorizacion);
                documentoAutTemp = this.documentoDao.guardarDocumentoGestorDocumental(
                    usuario, documentoTramiteDTO, documentoAutTemp);
            }

            //D: ... lo mismo se hace para el documento de notificación: si no tiene id significa
            //   que es nuevo, por lo que se debe armar el DTO para guardarlo en el gestor documental
            if (documentoNotificacion != null && documentoNotificacion.getId() == null) {

                String tipoDocumento = "Default";

                for (ETipoDocumento tdTmp : ETipoDocumento.values()) {
                    if (tdTmp.getId().equals(documentoNotificacion.getTipoDocumento().getId())) {
                        tipoDocumento = tdTmp.getNombre();
                    }
                }

                documentoTramiteDTO = new DocumentoTramiteDTO(
                    FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                    documentoNotificacion.getArchivo(), tipoDocumento,
                    tramite.getFechaRadicacion(),
                    predioTmp.getDepartamento().getNombre(),
                    predioTmp.getMunicipio().getNombre(),
                    predioTmp.getNumeroPredial(),
                    tramite.getTipoTramiteCadenaCompleto(), tramite.getId());

                documentoNotTemp = this.documentoDao.guardarDocumentoGestorDocumental(
                    usuario, documentoTramiteDTO, documentoNotificacion);
            }

            //D: relacionar el Documento de autorización al TramiteDocumento del documento de notificación
            if (documentoAutorizacion != null && documentoAutTemp != null &&
                documentoAutTemp.getId() != null) {

                tramiteDocumentoNotificacion.setAutorizacionDocumentoId(documentoAutTemp.getId());
            }

            //D: relacionar el Documento de notificación guardado con el TramiteDocumento del documento de notificación
            if (documentoNotificacion != null && documentoNotTemp != null &&
                documentoNotTemp.getId() != null) {

                tramiteDocumentoNotificacion.setNotificacionDocumentoId(documentoNotTemp.getId());

                // Por el momento el documento de notificación se duplicara como
                // documento ya que documento es obligatorio
                tramiteDocumentoNotificacion.setDocumento(documentoNotTemp);

            }

//TODO :: validar con Julio: esta fecha no debería cambiar
            tramiteDocumentoNotificacion.setfecha(new Date(System.currentTimeMillis()));

            tramiteDocumentoNotificacion.setUsuarioLog(usuario.getLogin());
            tramiteDocumentoNotificacion.setFechaLog(new Date(System.currentTimeMillis()));

            tramite.setTramiteDocumentos(this.tramiteDao.
                getTramiteDocumentosByTramiteId(tramite.getId()));

//TODO :: validar con Julio:
            tramite.setTramiteDocumentacions(this.tramiteDao
                .getTramiteDocumentacionsByTramiteId(tramite.getId()));

            if (tramite.getTramiteDocumentos() == null) {
                tramite.setTramiteDocumentos(new ArrayList<TramiteDocumento>());
            } else {
                int indice = tramite.getTramiteDocumentos().indexOf(tramiteDocumentoNotificacion);

                /*
                 * for (TramiteDocumento tdTmp : tramite.getTramiteDocumentos()) { if
                 * (tdTmp.getId().equals( tramiteDocumentoNotificacion.getId())) { indice =
                 * tramite.getTramiteDocumentos().indexOf(tdTmp); } }
                 */
                if (indice > -1) {
                    if (!tramite
                        .getTramiteDocumentos()
                        .get(indice)
                        .getPersonaNotificacion()
                        .contains(tramiteDocumentoNotificacion.getPersonaNotificacion())) {
                        tramite.getTramiteDocumentos().get(indice).getPersonaNotificacion()
                            .concat(tramiteDocumentoNotificacion.getPersonaNotificacion());
                    }

                    if (tramiteDocumentoNotificacion
                        .getAutorizacionDocumentoId() != null) {
                        tramite.getTramiteDocumentos()
                            .get(indice)
                            .setAutorizacionDocumentoId(
                                tramiteDocumentoNotificacion
                                    .getAutorizacionDocumentoId());
                    }

                    tramite.getTramiteDocumentos()
                        .get(indice)
                        .setNotificacionDocumentoId(
                            tramiteDocumentoNotificacion
                                .getNotificacionDocumentoId());

                    tramite.getTramiteDocumentos()
                        .get(indice)
                        .setRenunciaRecursos(
                            tramiteDocumentoNotificacion
                                .getRenunciaRecursos());

                    tramite.getTramiteDocumentos()
                        .get(indice)
                        .setMetodoNotificacion(
                            tramiteDocumentoNotificacion
                                .getMetodoNotificacion());

                }
            }

            answer = actualizarTramiteDocumentos(tramite, usuario);

            if (answer != null && answer.getTramiteDocumentos() != null &&
                !answer.getTramiteDocumentos().isEmpty()) {
                answer.getTramiteDocumentos().size();
            }

            return answer;

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#consultarDocumentacionDeTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Tramite consultarDocumentacionDeTramite(Long tramiteId) {
        Tramite answer = null;

        try {
            answer = this.tramiteDao.getDocsOfTramite(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#consultarDocumentacionDeTramite: " + ex.getMensaje());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#consultarDocumentacionCompletaDeTramite(java.lang.Long)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public Tramite consultarDocumentacionCompletaDeTramite(Long tramiteId) {
        Tramite answer = null;

        try {
            answer = this.tramiteDao.getDocumentacionCompletaTramite(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#consultarDocumentacionCompletaDeTramite: " + ex.
                getMensaje());
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#getTramitesByResolucion (String resolucion, String documentoEstado, String
     * documentoEstadoAdicional)
     */
    @Override
    public List<Tramite> getTramitesByResolucion(String resolucion,
        String documentoEstado, String documentoEstadoAdicional) {
        List<Tramite> answer = null;
        try {
            answer = this.tramiteDao.getTramitesByResolucion(resolucion,
                documentoEstado, documentoEstadoAdicional);
        } catch (ExcepcionSNC e) {
            LOGGER.error("ERROR en TramiteBean#getTramitesByResolucion");
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#buscarTramitesConResolucionYNumeroPredial(String,String)
     */
    @Override
    public List<Tramite> buscarTramitesConResolucionYNumeroPredialPredio(
        String resolucion, String numeroPredial) {

        List<Tramite> answer = null;
        try {
            answer = this.tramiteDao.buscarTramitesConResolucionYNumeroPredialPredio(
                resolucion, numeroPredial);
        } catch (Exception e) {
            LOGGER.error("Error en la búsqueda de tramites con resolución y número predial");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author david.cifuentes
     * @see ITramite#buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(Long)
     */
    @Override
    public Tramite buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(
        Long tramiteId) {
        Tramite answer = null;
        try {
            answer = this.tramiteDao
                .buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(tramiteId);
        } catch (Exception e) {
            LOGGER.error(
                "Error en la búsqueda de tramites conS olicitantes Documentacion Y Predios sor TramiteId");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @see ITramite#buscarFuncionariosEjecutoresPorTerritorial(String)
     */
    @Override
    public List<SEjecutoresTramite> buscarFuncionariosEjecutoresPorTerritorial(
        String idTerritorial) {
        List<UsuarioDTO> atributosEjecutores = LdapDAO
            .getUsuariosEstructuraRol2(idTerritorial, ERol.EJECUTOR_TRAMITE);
        List<SEjecutoresTramite> answer = tramiteDao
            .obtenerFuncionariosEjecutoresConNumeroDeTramitesDeTerrenoYOficinaPorFuncionarioEjecutor(
                atributosEjecutores);
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarFuncionariosTerritorialConTipoTramite(String)
     * @author franz.gamba
     */
    /*
     * @modified pedro.garcia 28-06-2012 orden. Factorización de código
     */
    @Override
    public List<SEjecutoresTramite> buscarFuncionariosTerritorialConClasificacionTramite(
        String idTerritorial) {

        List<Actividad> actividades;
        List<UsuarioDTO> ejecutores;
        List<SEjecutoresTramite> sEjecutores;
        List<Long> tramiteIds;
        SEjecutoresTramite ejecutor;

        ejecutores = LdapDAO.getUsuariosEstructuraRol2(idTerritorial, ERol.EJECUTOR_TRAMITE);

        sEjecutores = new ArrayList<SEjecutoresTramite>();

        try {
            for (UsuarioDTO user : ejecutores) {

                //felipe.cadena::#15864::01-03-2016::Se valida que no esten disponibles
                //para asignacion los usuarios expirados o inactivos
                if (user.getFechaExpiracion().before(new Date()) ||
                    !ELDAPEstadoUsuario.ACTIVO.codeExist(user.getEstado())) {
                    continue;
                }

                tramiteIds = new ArrayList<Long>();

                //Debido a que el metodo de ldapDao no trae toda la información de los usuarios
                //se utiliza getcache usuario para que pueda traer las actividades.
//                actividades = this.procesosService
//                    .consultarListaActividades(
//                    this.generalesService.getCacheUsuario(user.getLogin()), ERol.EJECUTOR_TRAMITE);
//                for (Actividad a : actividades) {
//                    tramiteIds.add(a.getIdObjetoNegocio());
//                }
//
//                if (tramiteIds.size() > 0) {
//                    ejecutor = this.tramiteDao.getEjecutorTramiteClasificacion(tramiteIds);
//                } else {
//                    ejecutor = new SEjecutoresTramite();
//                }
                ejecutor = new SEjecutoresTramite();
                ejecutor.setFuncionario(user.getNombreCompleto());
                ejecutor.setFuncionarioId(user.getLogin());
                ejecutor.setOficina(this.tramiteDao.obtenerTramitesPorEjecutorYClasificacion(user.
                    getLogin(), ETramiteClasificacion.OFICINA.toString()));
                ejecutor.setTerreno(this.tramiteDao.obtenerTramitesPorEjecutorYClasificacion(user.
                    getLogin(), ETramiteClasificacion.TERRENO.toString()));
                sEjecutores.add(ejecutor);

            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return sEjecutores;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see ITramite#buscarFuncionariosEjecutoresPorTerritorialDTO(String)
     */
    @Override
    public List<UsuarioDTO> buscarFuncionariosEjecutoresPorTerritorialDTO(
        String idTerritorial) {
//TODO :: franz.gamba :: usar try y catch
        List<UsuarioDTO> atributosEjecutores = LdapDAO
            .getUsuariosEstructuraRol2(idTerritorial, ERol.EJECUTOR_TRAMITE);

        return atributosEjecutores;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramite#buscarFuncionariosEjecutoresPorTerritorialDTO(String)
     * @author felipe.cadena
     */
    @Override
    public List<UsuarioDTO> buscarFuncionariosDigitalizadoresPorTerritorialDTO(
        String idTerritorial) {

        List<UsuarioDTO> resultado = null;

        try {
            resultado = LdapDAO
                .getUsuariosEstructuraRol2(idTerritorial, ERol.DIGITALIZADOR_DEPURACION);
        } catch (Exception ex) {
            LOGGER.error(
                "error en TramiteBean#buscarFuncionariosDigitalizadoresPorTerritorialDTO: " + ex.
                    getMessage());
            return null;
        }

        return resultado;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramite#buscarFuncionariosTopografosPorTerritorialDTO(String)
     * @author felipe.cadena
     */
    @Override
    public List<UsuarioDTO> buscarFuncionariosTopografosPorTerritorialDTO(
        String idTerritorial) {

        List<UsuarioDTO> resultado = null;

        try {
            resultado = LdapDAO
                .getUsuariosEstructuraRol2(idTerritorial, ERol.TOPOGRAFO);
        } catch (Exception ex) {
            LOGGER.error("error en TramiteBean#buscarFuncionariosTopografosPorTerritorialDTO: " +
                ex.getMessage());
            return null;
        }

        return resultado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#obtenerTramiteRectificacionesDeTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<TramiteRectificacion> obtenerTramiteRectificacionesDeTramite(Long idTramite) {

        List<TramiteRectificacion> answer = null;

        try {
            answer = this.tramiteDao.getTramiteRectificaciones(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en ITramiteLocal#obtenerTramiteRectificacionesDeTramite: " +
                ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#obtenerSolicitantesDeTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<SolicitanteTramite> obtenerSolicitantesDeTramite(Long idTramite) {

        List<SolicitanteTramite> answer = null;

        try {
            answer = this.solicitanteTramiteDao.findByTramiteId(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#obtenerSolicitantesDeTramite: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#obtenerSolicitantesSolicitud2(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<SolicitanteSolicitud> obtenerSolicitantesSolicitud2(
        Long solicitudId) {

        return this.solicitanteSolicitudDao
            .getSolicitantesBySolicitud2(solicitudId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#findTramitePruebasByTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public Tramite findTramitePruebasByTramiteId(Long tramiteId) {
        return this.tramiteDao.findTramitePruebasByTramiteId(tramiteId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#obtenerTramiteCargarPruebasByTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public Tramite obtenerTramiteCargarPruebasByTramiteId(Long tramiteId) {

        return this.tramiteDao.findTramiteCargarPruebasByTramiteId(tramiteId);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#findTramiteConTramitePruebaEntidadByTramiteId(Long)
     * @author franz.gamba
     */
    @Override
    public Tramite findTramiteConTramitePruebaEntidadByTramiteId(Long tramiteId) {

        try {
            return this.tramiteDao.findTramiteConTramitePruebaEntidadByTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#findTramiteConTramitePruebaEntidadByTramiteId: " +
                ex.getMensaje());
        }

        return null;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#findTramitePrediosSimplesByTramiteId(Long)
     * @author juan.agudelo
     */
    @Override
    public Tramite findTramitePrediosSimplesByTramiteId(Long tramiteId) {

        try {
            return this.tramiteDao.findTramitePrediosSimplesByTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramityeBean#findTramitePrediosSimplesByTramiteId: " +
                ex.getMensaje());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarActualizarTramitePruebas(TramitePrueba, UsuarioDTO)
     * @author juan.agudelo
     */
    @Override
    public TramitePrueba guardarActualizarTramitePruebas(TramitePrueba tp,
        UsuarioDTO usuario, String estadoFuturoTramite) {
        return this.tramitePruebaDao.guardarActualizarTramitePruebas(tp,
            usuario, estadoFuturoTramite);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#actualizarTramiteDocumentacion(List<TramiteDocumentacion>)
     */
    @Override
    public void actualizarTramiteDocumentacion(
        List<TramiteDocumentacion> tramiteDocumentacionActual) {
//TODO :: david.cifuentes :: hacer manejo de excepciones según el estándar definido :: pedro.garcia
        this.tramiteDocumentacionDao.updateMultiple(tramiteDocumentacionActual);
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#eliminarTramitesDocumentacion(List<TramiteDocumentacion>)
     */
    @Override
    public void eliminarTramitesDocumentacion(
        List<TramiteDocumentacion> tramiteDocumentacionEliminar) {
//TODO :: david.cifuentes :: hacer manejo de excepciones según el estándar definido :: pedro.garcia        
        this.tramiteDocumentacionDao
            .deleteMultiple(tramiteDocumentacionEliminar);
    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#actualizarTramiteDocumento(TramiteDocumento)
     */
    @Override
    public TramiteDocumento actualizarTramiteDocumento(
        TramiteDocumento tramiteDocumento) {
        TramiteDocumento td = this.tramiteDocumentoDao.update(tramiteDocumento);
        td.getDocumento();
        return td;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarSolicitudFetchTramitesBySolicitudId(Long)
     * @author juan.agudelo
     */
    @Override
    public Solicitud buscarSolicitudFetchTramitesBySolicitudId(Long solicitudId) {

        LOGGER.debug("En TramiteBean#buscarSolicitudFetchTramitesBySolicitudId ");
        Solicitud answer = null;

        try {
            answer = this.transaccionalService
                .buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion(solicitudId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#buscarSolicitudFetchTramitesBySolicitudId: " +
                ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#consultarTramitesParaAlistarInfo(Long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> consultarTramitesParaAlistarInfo(long idTramite[]) {

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.getByIdsFetchComisionParaAlistarInfo(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#obtenerTramiteDocumentacionPorIdTramite(Long)
     */
    @Override
    public List<TramiteDocumentacion> obtenerTramiteDocumentacionPorIdTramite(Long idTramite) {

        List<TramiteDocumentacion> answer = null;

        try {
            answer = tramiteDocumentacionDao.findByTramiteId(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#obtenerTramiteDocumentacionPorIdTramite: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramitesParaAsignacionDeEjecutor(long[], int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesParaAsignacionDeEjecutor(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("en TramiteBean#buscarTramitesParaAsignacion");

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.findTramitesParaAsignacionDeEjecutor(
                idsTramites, sortField, sortOrder, filters, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarTramitesParaAsignacion: " + ex.getMensaje());
        }
        return answer;

    }

    /**
     * @see ITramiteLocal#buscarTramitesDepuracionParaAsignacionDeFuncionario(long[])
     * @param idsTramites
     */
    @Implement
    @Override
    public List<TramiteDepuracion> buscarTramitesDepuracionParaAsignacionDeFuncionario(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("en TramiteBean#buscarTramitesParaAsignacion");

        List<TramiteDepuracion> resultado = null;

        try {
            resultado = this.tramiteDepuracionDAO.
                buscarPorIds(idsTramites, sortField, sortOrder, filters, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarTramitesParaAsignacion: " + ex.getMensaje());
        }
        return resultado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramitesConEjecutorAsignado(long[], int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesConEjecutorAsignado(long[] idsTramites,
        int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteBean#buscarTramitesConEjecutorAsignado");

        List<Tramite> answer = null;
        try {
            answer = this.tramiteDao.findTramitesConEjecutorAsignado(
                idsTramites, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarTramitesConEjecutorAsignado : " +
                ex.getMensaje());
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#contarTramitesParaAsignacionDeEjecutor(long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarTramitesParaAsignacionDeEjecutor(long[] idsTramites) {
        LOGGER.debug("en TramiteBean#contarTramitesParaAsignacion");

        int answer = 0;

        try {
            answer = this.tramiteDao.countTramitesParaAsignacionDeEjecutor(idsTramites);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#contarTramitesParaAsignacionDeEjecutor : " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#contarTramitesConEjecutorAsignado(long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarTramitesConEjecutorAsignado(long[] idsTramites) {
        LOGGER.debug("en TramiteBean#contarTramitesConEjecutorAsignado");

        int answer = 0;

        try {
            answer = this.tramiteDao.countTramitesEjecutorAsignado(idsTramites);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#contarTramitesConEjecutorAsignado : " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @see ITramite#actualizarTramites(List)
     */
    @Implement
    @Override
    public boolean actualizarTramites(List<Tramite> tramites) {

        LOGGER.debug("TramiteBean#actualizarTramites");
        boolean answer = true;

        for (Tramite tramite : tramites) {
            try {
                this.tramiteDao.update(tramite);
            } catch (ExcepcionSNC ex) {
                answer = false;
                LOGGER.error("error en TramiteBean#actualizarTramites, actualizando trámite con " +
                    "id: " + tramite.getId() + ": " + ex.getMensaje());
            }
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarTramitesPorComisionar(long[], java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.util.Map, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesPorComisionar(long[] idsTramites, String idTerritorial,
        String tipoComision, String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteBean#buscarTramitesPorComisionar");

        List<Tramite> answer = null;
        List<TramiteDepuracion> tramitesDepuracion;

        try {
            if (tipoComision.equals(EComisionTipo.CONSERVACION.getCodigo())) {
                filters.put("clasificacion", ETramiteClasificacion.TERRENO.toString());
            }

            answer = this.tramiteDao.findTramitesPorComisionar(idsTramites, sortField, sortOrder,
                filters, rowStartIdxAndCount);
            if (answer != null && !answer.isEmpty()) {
                // D: 1. asignar a cada trámite su duración según las reglas definidas en
                // la tabla DURACION_TRAMITE
                //    2. asignar el valor del campo numeroComisionesHaEstado
                for (Tramite tramite : answer) {
                    //1:
                    calcAndSetDuracionTramiteDeComision(tramite, idTerritorial);

                    //2:
                    calcAndSetNumeroComisionesHaEstado(tramite, tipoComision);
                }

                //D: si se trata de trámites de depuración (por lo tanto de comisiones de depuración),
                //  se llenan los datos de nombre ejecutor y ejecutor de depuración para cada trámite.
                //Para las comisiones de depuración hay o ejecutor o topógrafo
                tramitesDepuracion = this.tramiteDepuracionDAO.buscarPorIds(idsTramites, null, null,
                    null);
                for (TramiteDepuracion td : tramitesDepuracion) {
                    for (Tramite tr : answer) {
                        if (tr.getId().equals(td.getTramite().getId())) {
                            if (td.getEjecutor() != null) {
                                tr.setEjecutorDepuracion(td.getEjecutor());
                                tr.setNombreEjecutorDepuracion(td.getNombreEjecutor());
                            } else if (td.getTopografo() != null) {
                                tr.setEjecutorDepuracion(td.getTopografo());
                                tr.setNombreEjecutorDepuracion(td.getNombreTopografo());
                            }
                            break;
                        }
                    }
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción consultando trámites para comisionar de territorial " +
                idTerritorial + ": " + ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#contarTramitesPorComisionar(long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarTramitesPorComisionar(long[] idsTramites, String tipoComision) {
        LOGGER.debug("en TramiteBean#contarTramitesPorComisionar");

        int answer = 0;

        try {
            answer = this.tramiteDao.countTramitesPorComisionar(idsTramites, tipoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción contando trámites para comisionar: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarComisionesTramitePorIdTramite(Long)
     * @author javier.aponte
     */
    @Override
    public List<ComisionTramite> buscarComisionesTramitePorIdTramite(
        Long idTramite) {

        List<ComisionTramite> answer = null;

        try {
            answer = this.comisionTramiteDao.buscarComisionesTramitePorIdTramite(idTramite);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarComisionesTramitePorIdTramite: " +
                ex.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<UsuarioDTO> buscarFuncionariosPorRolYTerritorial(String idTerritorial, ERol rol) {

        List<UsuarioDTO> answer = null;

        try {
            answer = setEstructuraOrganizacional(LdapDAO.getUsuariosRolTerritorial(
                idTerritorial, rol));
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarFuncionariosPorRolYTerritorial: " +
                ex.getMessage());
        }

        return answer;
    }

    /**
     * @author felipe.cadena
     * @see ITramite#buscarFuncionariosPorTerritorial(String)
     */
    @Override
    public List<UsuarioDTO> buscarFuncionariosPorTerritorial(String idTerritorial, String idUoc) {

        List<UsuarioDTO> answer = null;

        try {
            answer =
                setEstructuraOrganizacional(LdapDAO.getUsuariosEstructura(idTerritorial, idUoc));
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarFuncionariosPorTerritorial: " +
                ex.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Busca los usuarios en ldap para el rol y la territorial a partir de los tipo user
     *
     * @author fredy.wilches
     * @see ITramite#buscarFuncionariosPorRolYTerritorial(String, )
     */
    private List<UsuarioDTO> setEstructuraOrganizacional(
        List<UsuarioDTO> usuarios) {
        for (UsuarioDTO usuarioDto : usuarios) {
            if (usuarioDto.getDescripcionTerritorial() != null) {
                usuarioDto.setCodigoTerritorial(codigoHomologadoDao
                    .obtenerCodigoHomologado(
                        ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
                        ESistema.LDAP, ESistema.SNC,
                        usuarioDto.getDescripcionTerritorial(), null));
            }
            if (usuarioDto.getDescripcionUOC() != null) {
                usuarioDto.setCodigoUOC(codigoHomologadoDao
                    .obtenerCodigoHomologado(
                        ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
                        ESistema.LDAP, ESistema.SNC,
                        usuarioDto.getDescripcionUOC(), null));
            }
        }
        return usuarios;

    }

    /**
     * Busca los usuarios en ldap para el rol y la territorial a partir de los tipo group
     *
     * @author fredy.wilches
     * @see ITramite#buscarFuncionariosPorRolYTerritorial(String, )
     */
    /*
     * @Override public List<UsuarioDTO> buscarFuncionariosPorRolYTerritorial2( String
     * idTerritorial, ERol rol) { return setEstructuraOrganizacional(LdapDAO
     * .getUsuariosRolTerritorial(idTerritorial, rol)); }
     */
    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see ITramite#eliminarTramiteEstado(TramiteEstado)
     */
    @Override
    public void eliminarTramiteEstado(TramiteEstado estadoTramite) {
        LOGGER.debug("en TramiteBean#eliminarTramiteEstado");
        this.tramiteEstadoDao.delete(estadoTramite);
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<MotivoEstadoTramite> getCacheMotivoEstadoTramites() {
        return this.motivoEstadoTramiteDao.findAll();
    }

    /**
     * @see ITramite#obtenerTramitesPorIds(List)
     */
    @Override
    public List<Tramite> obtenerTramitesPorIds(List<Long> tramiteIds) {
        return this.tramiteDao.findTramitesByIds(tramiteIds);
    }

    @Implement
    @Override
    public List<Tramite> obtenerTramitesPorIds(
        List<Long> tramiteIds, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("en TramiteBean#obtenerTramitesPorIds");

        List<Tramite> answer;
        answer = this.tramiteDao.findTramitesByIds(
            tramiteIds, sortField, sortOrder, filters, contadoresDesdeYCuantos);
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarActualizarTramites(List)
     */
    @Override
    @Deprecated
    public List<Tramite> guardarActualizarTramites(List<Tramite> tramites) {

        for (Tramite tram : tramites) {
            List<TramiteEstado> auxTramEstados = null;
            if (tram.getId() != null) {
                auxTramEstados = tramiteEstadoDao.findByTramiteId(tram.getId());

                //N: si los Tramite_Estado del trámite no cambian para qué hace esto. Esta mezclando actualización con consulta de otras vainas
                tram.setTramiteEstados(auxTramEstados);
                Tramite auxTram = this.tramiteDao.update(tram);
                tram.setId(auxTram.getId());
            } else {
                Tramite auxTram = this.tramiteDao.update(tram);
                tram.setId(auxTram.getId());
                auxTramEstados = new LinkedList<TramiteEstado>();

                TramiteEstado tEstado = new TramiteEstado();
                tEstado.setTramite(auxTram);
                tEstado.setEstado(ETramiteEstado.POR_APROBAR.getCodigo());
                tEstado.setFechaLog(new Date());
                tEstado.setFechaInicio(new Date());
                tEstado.setUsuarioLog(tram.getUsuarioLog());
                tEstado = this.tramiteEstadoDao.update(tEstado);

                auxTramEstados.add(tEstado);
                tram.setTramiteEstados(auxTramEstados);
            }
        }

        return tramites;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#guardarActualizarTramitesConDocumentos(java.util.List,
     * co.gov.igac.generales.dto.UsuarioDTO)
     * @author juan.agudelo
     */
    @Override
    public List<Tramite> guardarActualizarTramitesConDocumentos(
        List<Tramite> tramites, UsuarioDTO usuario) {

        List<Tramite> tramiteResultadolist = new ArrayList<Tramite>();

        String fileSeparator = System.getProperty("file.separator");

        for (Tramite t : tramites) {

            for (Tramite tram : t.getTramites()) {

                tram.setFechaLog(new Date());
                tram.setUsuarioLog(usuario.getLogin());

                if (tram.getTramiteDocumentacions() != null &&
                    !tram.getTramiteDocumentacions().isEmpty()) {
                    for (TramiteDocumentacion td : tram
                        .getTramiteDocumentacions()) {
                        td.setTramite(tram);
                        td.setUsuarioLog(usuario.getLogin());
                        td.setFechaLog(new Date());

                        if (td.getDocumentoSoporte() != null) {
                            Documento d = td.getDocumentoSoporte();

                            if (d.getArchivo() == null ||
                                d.getArchivo().isEmpty()) {
                                d.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                            }

                            // Si es un documento adicional necesariamente fue
                            // aportado
                            if (td.getAdicional().equals(ESiNo.SI.getCodigo())) {
                                d.setEstado(EDocumentoEstado.RECIBIDO
                                    .getCodigo());

                            } else if (td.getRequerido().equals(
                                ESiNo.SI.getCodigo()) &&
                                td.getAportado().equals(
                                    ESiNo.SI.getCodigo())) {
                                d.setEstado(EDocumentoEstado.RECIBIDO
                                    .getCodigo());
                            }

                            if (tram.getPredio() != null) {
                                d.setPredioId(tram.getPredio().getId());
                            }

                            d.setTramiteId(tram.getId());
                            d.setBloqueado(ESiNo.NO.getCodigo());
                            d.setUsuarioCreador(usuario.getLogin());
                            d.setFechaRadicacion(tram.getFechaRadicacion());
                            d.setUsuarioLog(usuario.getLogin());
                            d.setFechaLog(new Date());

                            Documento newD = null;

                            try {
                                newD = this.documentoDao.update(d);

                            } catch (Exception e) {
                                LOGGER.error(
                                    "error en TramiteBean#guardarActualizarTramitesConDocumentos: " +
                                    e.getMessage(), e);
                            }
                            if (newD != null && newD.getId() != null) {
                                d.setId(newD.getId());
                            }
                        }
                    }
                }

                if (tram.getTramites() != null && !tram.getTramites().isEmpty()) {
                    for (Tramite tTemp : tram.getTramites()) {
                        tTemp.setFechaLog(new Date());
                        tTemp.setUsuarioLog(usuario.getLogin());
                    }
                }

                try {
                    Tramite auxTram = this.tramiteDao.update(tram);
                    auxTram = this.tramiteDao
                        .findTramiteTramiteDocumentacionDocumentoById(auxTram
                            .getId());
                    tram.setId(auxTram.getId());
                    tram.setTramiteDocumentacions(auxTram.getTramiteDocumentacions());
                    if (auxTram.getTramiteRectificacions() != null) {
                        tram.setTramiteRectificacions(auxTram.getTramiteRectificacions());
                    }

                    for (TramiteDocumentacion td : auxTram
                        .getTramiteDocumentacions()) {

                        td.getDocumentoSoporte().setTramiteId(auxTram.getId());
                        this.documentoDao.update(td.getDocumentoSoporte());

                        if (td.getDocumentoSoporte() != null &&
                            td.getDocumentoSoporte().getId() != null &&
                            td.getDocumentoSoporte().getArchivo() != null &&
                            !td.getDocumentoSoporte().getArchivo()
                                .isEmpty() &&
                            !td.getDocumentoSoporte()
                                .getArchivo()
                                .equals(Constantes.CONSTANTE_CADENA_VACIA_DB) &&
                            (td.getDocumentoSoporte()
                                .getIdRepositorioDocumentos() == null || td
                                .getDocumentoSoporte()
                                .getIdRepositorioDocumentos().isEmpty())) {

                            String nombreDepartamento = "";
                            String nombreMunicipio = "";
                            String numeroPredial = "";

                            if (td.getDepartamento() != null) {
                                nombreDepartamento = td.getDepartamento()
                                    .getNombre();
                            } else {
                                nombreDepartamento = auxTram.getDepartamento()
                                    .getNombre();
                            }

                            if (td.getMunicipio() != null) {
                                nombreMunicipio = td.getMunicipio().getNombre();
                            } else {
                                nombreMunicipio = auxTram.getMunicipio()
                                    .getNombre();
                            }

                            if (auxTram.getTramitePredioEnglobes() != null && !auxTram.
                                getTramitePredioEnglobes().isEmpty()) {
                                numeroPredial = auxTram.getTramitePredioEnglobes().get(0).
                                    getPredio().getNumeroPredial();
                            }
                            if (auxTram.getPredio() != null) {
                                numeroPredial = auxTram.getPredio()
                                    .getNumeroPredial();
                            }

                            DocumentoTramiteDTO documentoTramiteDTO = new DocumentoTramiteDTO(
                                FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                                td.getDocumentoSoporte()
                                    .getArchivo(), td
                                    .getDocumentoSoporte()
                                    .getTipoDocumento().getNombre(),
                                auxTram.getFechaRadicacion(),
                                nombreDepartamento, nombreMunicipio,
                                numeroPredial, auxTram.getTipoTramite(),
                                auxTram.getId());

                            //N: se supone que el llamado a este método hace update del Documento
                            //  con el id del repositorio con el que
                            this.documentoDao.guardarDocumentoGestorDocumental(usuario,
                                documentoTramiteDTO, td.getDocumentoSoporte());
                        }

                    }
                } catch (Exception e) {
                    LOGGER.error("error en TramiteBean#guardarActualizarTramitesConDocumentos: " +
                        e.getMessage(), e);

                }
            }

            try {
                Tramite auxTram = this.tramiteDao.update(t);
                tramiteResultadolist.add(auxTram);
            } catch (Exception e) {
                LOGGER.error("error en TramiteBean#guardarActualizarTramitesConDocumentos: " +
                    e.getMessage(), e);
            }
        }

        return tramiteResultadolist;
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * Método que busca un TramiteDocumento con soporte en Alfresco por su id
     *
     * @author david.cifuentes
     * @see ITramite#buscarTramiteDocumentoPorIdTramiteYDocumentoEstado(Long idTramite)
     */
    @Implement
    @Override
    public TramiteDocumento buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(
        Long idTramite) {
        TramiteDocumento answer = null;
        try {
            answer = this.tramiteDocumentoDao
                .buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(idTramite);
        } catch (Exception e) {
            LOGGER.error("Error en buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#actualizarTramiteEstado(TramiteEstado)
     * @author javier.aponte
     */
    @Override
    public TramiteEstado actualizarTramiteEstado(
        TramiteEstado tramiteEstadoActual, UsuarioDTO usuario) {

        TramiteEstado answer = null;

        try {
            answer = this.tramiteEstadoDao.actualizarTramiteEstado(tramiteEstadoActual, usuario);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("... error en TramiteBean#actualizarTramiteEstado: " + ex);
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @deprecated ver documentación en la interfaz
     */
    /*
     * @modified pedro.garcia 02-09-2013 validación de nulidad de los estados consultados y los
     * documentos que supuestamente tiene, porque se está totiando.
     */
    @Override
    public Tramite guardarActualizarTramite(Tramite tramite) {

        LOGGER.debug("guardarActualizarTramite");

        Tramite auxTramite = this.tramiteDao.update(tramite);
        tramite.setId(auxTramite.getId());
        List<TramiteEstado> tramiteEstados = this.tramiteEstadoDao.findByTramiteId(tramite.getId());

        if (tramiteEstados != null) {
            tramite.setTramiteEstados(tramiteEstados);
        }
        if (tramite.getTramiteDocumentos() != null) {
            //N: esto no debería hacerse aquí, a menos que se haya hecho fetch de los documentos del 
            // trámite que se va actualizar. Por eso se marcó como deprecated, porque si no se hizo así,
            // la validación de que no sea null no es suficiente y genera un error
            tramite.getTramiteDocumentos().size();
        }
        LOGGER.debug("guardarActualizarTramite fin");
        return tramite;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que consulta los tramites asociados a un tramite padre
     *
     * @author franz.gamba
     *
     */
    @Override
    public List<Tramite> consultarTramitesAsociadosATramiteId(Long tramiteId) {
        List<Tramite> answer = this.tramiteDao.getTramitesAsociados(tramiteId);
        return answer;
    }

    /**
     * @see ITramite#findTramiteTramiteDocumentacionDocumentoById(Long)
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    @Override
    public Tramite findTramiteTramiteDocumentacionDocumentoById(Long tramiteId) {

        Tramite answer = null;

        try {
            answer = this.tramiteDao.findTramiteTramiteDocumentacionDocumentoById(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#findTramiteTramiteDocumentacionDocumentoById: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitePorTramiteIdAsignacion(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Tramite buscarTramitePorTramiteIdAsignacion(Long tramiteId) {

        LOGGER.debug("En TramiteBean#buscarTramitePorTramiteIdAsignacion ...");

        Tramite answer = null;

        try {
            answer = this.tramiteDao.findTramiteByIdForAsignacion(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#buscarTramitePorTramiteIdAsignacion: " +
                ex.getMensaje());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#guardarDocumento(UsuarioDTO, Documento)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 22-05-2012 manejo de excepciones, orden, logs
     */
    @Implement
    @Override
    public Documento guardarDocumento(UsuarioDTO usuario, Documento documento) {
        LOGGER.debug("on TramiteBean#guardarDocumento ...");

        Documento answer = null;

        try {
            answer = this.documentoDao.clasificarAlmacenamientoDocumentoGestorDocumental(usuario,
                documento);
        } catch (ExcepcionSNC e) {
            LOGGER.error("error en TramiteBean#guardarDocumento: " + e.getMessage(), e);
            //throw(e);
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * @see ITramite#eliminarSolicitanteTramite(SolicitanteTramite)
     */
    @Override
    public void eliminarSolicitanteTramite(SolicitanteTramite solicitanteTramite) {
        this.solicitanteTramiteDao.delete(solicitanteTramite);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#eliminarSolicitanteTramites(List)
     */
    @Override
    public void eliminarSolicitanteTramites(List<SolicitanteTramite> solicitanteTramites) {
        this.solicitanteTramiteDao.deleteMultiple(solicitanteTramites);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarActualizarSolicitanteTramite(SolicitanteTramite)
     */
    @Override
    public SolicitanteTramite guardarActualizarSolicitanteTramite(
        SolicitanteTramite solicitanteTramite) {
        return this.solicitanteTramiteDao.update(solicitanteTramite);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramitesParaCargueInfo(long[], int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesParaCargueInfo(long[] idsTramites, String tipoComision,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("on TramiteBean#buscarTramitesParaCargueInfo ...");
        List<Tramite> answer = null;

        try {
            answer =
                this.tramiteDao.findTramitesParaCargueInfo(idsTramites, tipoComision,
                    contadoresDesdeYCuantos);
            LOGGER.debug("TramiteBean#buscarTramitesParaCargueInfo finished");
        } catch (ExcepcionSNC sncEx) {
            LOGGER.error("Error on TramiteBean#buscarTramitesParaCargueInfo: " + sncEx.getMessage());
        }

        return answer;
    }
//---------------------------------------------------------------------------------------------

    /**
     * @see ITramite#obtenerDocumentoPruebaCorrespondencia(String)
     * @author juan.agudelo
     * @modified felipe.cadena -- se agrega el parametro usuario
     */
    @Override
    public Documento obtenerDocumentoPruebaCorrespondencia(
        String numeroRadicacion, UsuarioDTO usuario) {
        Documento documento = null;
        try {
            documento = this.sncProcedimientoDao
                .consultarRadicado(numeroRadicacion, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
            return null;
        }

        return documento;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramitesPorEstado(String)
     * @author david.cifuentes
     */
    @Override
    public List<Tramite> buscarTramitesPorEstado(String estado) {
        return this.tramiteDao.buscarTramitesPorEstado(estado);
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see ITramite#guardarActualizarTramiteDocumentos(Tramite, UsuarioDTO)
     * @author juan.agudelo
     */
    @Implement
    @Override
    public Tramite guardarActualizarTramiteDocumentos(Tramite tramite,
        List<TramiteDocumento> documentosPruebasAEliminar,
        UsuarioDTO usuario) {

        Tramite newTramite = null;

        tramite.setFechaLog(new Date());
        tramite.setUsuarioLog(usuario.getLogin());

        if (tramite.getTramiteDocumentos() != null &&
            !tramite.getTramiteDocumentos().isEmpty()) {
            for (TramiteDocumento td : tramite.getTramiteDocumentos()) {
                td.setUsuarioLog(usuario.getLogin());
                td.setFechaLog(new Date());

                if (td.getDocumento() != null) {
                    Documento d = td.getDocumento();

                    if (tramite.getPredio() != null) {
                        d.setPredioId(tramite.getPredio().getId());
                    }
                    d.setTramiteId(tramite.getId());
                    d.setBloqueado(ESiNo.NO.getCodigo());
                    d.setUsuarioCreador(usuario.getLogin());
                    d.setUsuarioLog(usuario.getLogin());
                    d.setFechaLog(new Date());

                    Documento newD = null;

                    try {
                        newD = this.guardarDocumento(usuario, d);

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw (new ExcepcionSNC("Mi codigo de excepción",
                            ESeveridadExcepcionSNC.ERROR, e.getMessage(), e));
                    }
                    if (newD != null && newD.getId() != null) {
                        d.setId(newD.getId());

                        Tramite tTemporal = new Tramite();
                        tTemporal.setId(d.getTramiteId());
                        td.setTramite(tTemporal);
                        if (newD.getIdRepositorioDocumentos() != null &&
                            !newD.getIdRepositorioDocumentos().isEmpty()) {
                            td.setIdRepositorioDocumentos(newD
                                .getIdRepositorioDocumentos());
                        }
                    }
                }

                if (documentosPruebasAEliminar != null && !documentosPruebasAEliminar.isEmpty()) {
                    for (TramiteDocumento de : documentosPruebasAEliminar) {
                        if (de.getId() != null && de.getDocumento() != null) {

                            try {

                                this.tramiteDocumentoDao.delete(de);
                                this.documentoDao.delete(de.getDocumento());
                                tramite.getTramiteDocumentos().remove(de);

                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                throw (new ExcepcionSNC(
                                    "Mi codigo de excepción",
                                    ESeveridadExcepcionSNC.ERROR,
                                    e.getMessage(), e));
                            }
                        }
                    }
                }
            }
            try {
                newTramite = this.guardarActualizarTramite(tramite);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                throw (new ExcepcionSNC("Mi codigo de excepción",
                    ESeveridadExcepcionSNC.ERROR, e.getMessage(), e));
            }
        }
        return newTramite;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerSolicitsParaEnvioComunicNotif(idTramite,idSolicitud, tipoSolicitantes)
     * @author pedro.garcia
     *
     * @modified david.cifuentes - Adición de un nuevo estado para devolver los solicitantes de la
     * solicitud y los afectados del trámite.
     */
    @Implement
    @Override
    public List<Solicitante> obtenerSolicitsParaEnvioComunicNotif(
        long idTramite, long idSolicitud, int tipoSolicitantes) {

        LOGGER.debug("on TramiteBean#obtenerSolicitsParaEnvioComunicNotif ...");
        List<Solicitante> answer = null;
        Solicitante tempSolicitante;
        ArrayList<ISolicitanteSolicitudOTramite> solicitantesTramiteWrapped, solicitantesSolicitudWrapped;

        List<SolicitanteTramite> solicitantesTramite = null;
        List<SolicitanteSolicitud> solicitantesSolicitud = null;

        try {
            solicitantesTramite = this.solicitanteTramiteDao.findByTramiteId(idTramite);
            solicitantesSolicitud = this.solicitanteSolicitudDao.findBySolicitudId(idSolicitud);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "TramiteBean#obtenerSolicitsParaEnvioComunicNotif");
        }

        try {
            solicitantesTramiteWrapped = this.wrapSolicitantes(solicitantesTramite);
            solicitantesSolicitudWrapped = this.wrapSolicitantes(solicitantesSolicitud);

            // D: S. de ambos
            if (tipoSolicitantes == 3) {
                if (solicitantesSolicitud == null ||
                    solicitantesTramite == null ||
                    (solicitantesSolicitud.isEmpty() && solicitantesTramite.isEmpty())) {
                    return null;
                } else {
                    answer = new ArrayList<Solicitante>();
                }
                tempSolicitante =
                    this.selectSolicitanteParaEnvioComunicNotif2(solicitantesSolicitudWrapped);
                if (tempSolicitante != null) {
                    answer.add(tempSolicitante);
                }
                tempSolicitante =
                    this.selectSolicitanteParaEnvioComunicNotif2(solicitantesTramiteWrapped);
                if (tempSolicitante != null) {
                    answer.add(tempSolicitante);
                }
            } // D: S. del trámite
            else if (tipoSolicitantes == 2) {
                if (solicitantesTramite == null ||
                    solicitantesTramite.isEmpty()) {
                    return null;
                } else {
                    answer = new ArrayList<Solicitante>();
                }
                tempSolicitante =
                    this.selectSolicitanteParaEnvioComunicNotif2(solicitantesTramiteWrapped);
                if (tempSolicitante != null) {
                    answer.add(tempSolicitante);
                }
            } // D: S. de la solicitud
            else if (tipoSolicitantes == 1) {
                if (solicitantesSolicitud == null || solicitantesSolicitud.isEmpty()) {
                    return null;
                } else {
                    answer = new ArrayList<Solicitante>();
                }
                tempSolicitante =
                    this.selectSolicitanteParaEnvioComunicNotif2(solicitantesSolicitudWrapped);
                if (tempSolicitante != null) {
                    answer.add(tempSolicitante);
                }
            } // D: Todos los Solicitantes de la solicitud y los afectados del trámite
            else if (tipoSolicitantes == 4) {
                if (solicitantesSolicitud == null || solicitantesSolicitud.isEmpty()) {
                    return null;
                } else {
                    answer = new ArrayList<Solicitante>();
                }

                // Adición de los solicitantes de la solicitud
                if (solicitantesSolicitudWrapped != null && !solicitantesSolicitudWrapped.isEmpty()) {
                    for (ISolicitanteSolicitudOTramite tempS : solicitantesSolicitudWrapped) {
                        if (tempS.getSolicitante() != null) {
                            answer.add(tempS.getSolicitante());
                        }
                    }
                }

                // Adición de los afectados del trámite
                if (solicitantesTramite != null &&
                    !solicitantesTramite.isEmpty()) {

                    for (SolicitanteTramite st : solicitantesTramite) {
                        if (st.getSolicitante() != null &&
                            st.getRelacion() != null &&
                            st.getRelacion().equals(
                                ESolicitanteSolicitudRelac.AFECTADO.toString()) &&
                            (st.getTipoSolicitante() == null || st
                            .getTipoSolicitante().trim().isEmpty())) {

                            answer.add(st.getSolicitante());
                        }
                    }
                }
            } // D: Todos los solicitantes de la solicitud y del trámite
            else if (tipoSolicitantes == 5) {
                if (solicitantesSolicitud == null ||
                    solicitantesTramite == null ||
                    (solicitantesSolicitud.isEmpty() && solicitantesTramite.isEmpty())) {
                    return null;
                } else {
                    answer = new ArrayList<Solicitante>();
                }
                // Adición de los solicitantes de la solicitud
                if (solicitantesSolicitudWrapped != null && !solicitantesSolicitudWrapped.isEmpty()) {
                    for (ISolicitanteSolicitudOTramite tempS : solicitantesSolicitudWrapped) {
                        if (tempS.getSolicitante() != null) {
                            answer.add(tempS.getSolicitante());
                        }
                    }
                }
                // Adición de los solicitantes del trámite
                if (solicitantesTramiteWrapped != null && !solicitantesTramiteWrapped.isEmpty()) {
                    for (ISolicitanteSolicitudOTramite tempS : solicitantesTramiteWrapped) {
                        if (tempS.getSolicitante() != null && !answer.contains(tempS.
                            getSolicitante())) {
                            answer.add(tempS.getSolicitante());
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#obtenerSolicitsParaEnvioComunicNotif: " +
                e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitePorId(Long)
     * @author juan.agudelo
     */
    @Override
    public Tramite buscarTramitePorId(Long tramiteId) {
        return this.tramiteDao.findById(tramiteId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarActualizarTramitePruebaEntidad(TramitePruebaEntidad)
     * @author fabio.navarrete
     */
    @Override
    public TramitePruebaEntidad guardarActualizarTramitePruebaEntidad(
        TramitePruebaEntidad tramitePruebaEntidad) {

        return this.tramitePruebaEntidadDao.update(tramitePruebaEntidad);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerTramiteDocumentosDeTramitePorTipoDoc(long, long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<TramiteDocumento> obtenerTramiteDocumentosDeTramitePorTipoDoc(
        long tramiteId, long idTipoDocumento) {

        LOGGER.debug("on TramiteBean#obtenerTramiteDocumentosDeTramitePorTipoDoc ...");
        List<TramiteDocumento> answer = null;

        try {
            answer = this.tramiteDocumentoDao.findByTramiteAndTipoDocumento(
                tramiteId, idTipoDocumento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#obtenerTramiteDocumentosDeTramitePorTipoDoc: " +
                ex.getMessage());
        }

        LOGGER.debug("... TramiteBean#obtenerTramiteDocumentosDeTramitePorTipoDoc finished.");
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#obtenerTramiteDocumentosDePruebasPorTramiteId(long)
     * @author franz.gamba
     */
    @Implement
    @Override
    public List<TramiteDocumento> obtenerTramiteDocumentosDePruebasPorTramiteId(long tramiteId) {

        LOGGER.debug("on TramiteBean#obtenerTramiteDocumentosDePruebasPorTramiteId ...");
        List<TramiteDocumento> answer = null;
        answer = this.tramiteDocumentoDao.
            findByTramiteDocumentosDePruebasAsociadas(tramiteId);

        LOGGER.debug("... TramiteBean#obtenerTramiteDocumentosDePruebasPorTramiteId finished.");
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#obtenerSolicitantesTodosDeTramite
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Solicitante> obtenerSolicitantesTodosDeTramite(long tramiteId,
        long solicitudId, String tipoSolicitud) {

        List<Solicitante> answer;
        List<SolicitanteTramite> solicitantesTramite;
        List<SolicitanteSolicitud> solicitantesSolicitud = null;
        Solicitante tempSolicitante;

        solicitantesTramite = this.obtenerSolicitantesDeTramite(tramiteId);

        // D: si la solicitud es de Avisos, el trámite debe tener solicitantes
        if (tipoSolicitud.equalsIgnoreCase(ESolicitudTipo.AVISOS.getCodigo()) &&
            solicitantesTramite == null) {
            return null;
        }

        answer = new ArrayList<Solicitante>();
        boolean solicitanteAgregado = false;
        for (SolicitanteTramite solicTram : solicitantesTramite) {
            tempSolicitante =
                this.armarSolicitanteDeSolicitanteX(solicTram, SolicitanteTramite.class);
            if (!answer.isEmpty()) {
                for (Solicitante solTemp : answer) {
                    if (solTemp.getId().equals(tempSolicitante.getId())) {
                        solicitanteAgregado = true;
                    }
                }
            }

            if (!solicitanteAgregado) {
                answer.add(tempSolicitante);
            }
        }

        // D: si la solicitud es de Avisos solo se deben traer los solicitantes del trámite; en caso
        // contrario se traen también los de la solicitud (el caso más común es
        // que sea la solicitud la que tiene solicitantes)
        if (!tipoSolicitud.equals(ESolicitudTipo.AVISOS.getCodigo())) {
            solicitantesSolicitud = this.obtenerSolicitantesSolicitud(solicitudId);
        }

        solicitanteAgregado = false;
        if (solicitantesSolicitud != null && !solicitantesSolicitud.isEmpty()) {
            for (SolicitanteSolicitud solicSolicitud : solicitantesSolicitud) {
                tempSolicitante = this.armarSolicitanteDeSolicitanteX(
                    solicSolicitud, SolicitanteSolicitud.class);
                if (!answer.isEmpty()) {
                    for (Solicitante solTemp : answer) {
                        if (solTemp.getId().equals(tempSolicitante.getId())) {
                            solicitanteAgregado = true;
                        }
                    }
                }

                if (!solicitanteAgregado) {
                    answer.add(tempSolicitante);
                }
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma un Solicitante con los mínimos datos requeridos -para mostrar- a partir de un
     * SolicitanteSolicitud o SolicitanteTramite, que se suponen que tienen métodos con el mismo
     * nombre para los getters de los atributos que se quieren.
     *
     * @author pedro.garcia
     * @param solicTram
     * @return
     */
    private Solicitante armarSolicitanteDeSolicitanteX(Object solicitante, Class<?> clazz) {

        Solicitante answer = new Solicitante();
        Method getNombre;
        Method getTipoId;
        Method getNumeroId;
        Method getTipoSolicit;
        Method getSolicitante;
        Method getDireccion;
        Method getRelacion;
        String tempValue;

        getNombre = getTipoId = getNumeroId = getTipoSolicit = getSolicitante = getDireccion =
            getRelacion = null;
        try {
            getNombre = clazz.getMethod("getNombreCompleto");
            getTipoId = clazz.getMethod("getTipoIdentificacion");
            getNumeroId = clazz.getMethod("getNumeroIdentificacion");
            getTipoSolicit = clazz.getMethod("getTipoSolicitante");
            getDireccion = clazz.getMethod("getDireccion");
            getSolicitante = clazz.getMethod("getSolicitante");
            getRelacion = clazz.getMethod("getRelacion");
        } catch (NoSuchMethodException ex) {
            LOGGER.error("error on TramiteBean#armarSolicitenteDeSolicitanteX: ", ex);
        } catch (SecurityException ex) {
            LOGGER.error("error on TramiteBean#armarSolicitenteDeSolicitanteX: ", ex);
        }

        try {
            tempValue = (String) getNombre.invoke(clazz.cast(solicitante), (Object[]) null);
            answer.setNombreCompleto(tempValue);

            tempValue = (String) getTipoId.invoke(clazz.cast(solicitante), (Object[]) null);
            answer.setTipoIdentificacion(tempValue);

            tempValue = (String) getNumeroId.invoke(clazz.cast(solicitante), (Object[]) null);
            answer.setNumeroIdentificacion(tempValue);

            tempValue = (String) getTipoSolicit.invoke(clazz.cast(solicitante), (Object[]) null);
            answer.setTipoSolicitante(tempValue);

            tempValue = (String) getDireccion.invoke(clazz.cast(solicitante), (Object[]) null);
            answer.setDireccion(tempValue);

            Solicitante sol = (Solicitante) getSolicitante.invoke(clazz.cast(solicitante),
                (Object[]) null);
            answer.setId(sol.getId());

            tempValue = (String) getRelacion.invoke(clazz.cast(solicitante), (Object[]) null);
            answer.setRelacion(tempValue);

        } catch (IllegalAccessException ex) {
            LOGGER.error("error on TramiteBean#armarSolicitenteDeSolicitanteX: ", ex);
        } catch (IllegalArgumentException ex) {
            LOGGER.error("error on TramiteBean#armarSolicitenteDeSolicitanteX: ", ex);
        } catch (InvocationTargetException ex) {
            LOGGER.error("error on TramiteBean#armarSolicitenteDeSolicitanteX: ", ex);
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see ITramite#recuperarTramitesConDocumentosFaltantes(List<Actividad>)
     * @author david.cifuentes
     */
    @Override
    public List<Tramite> recuperarTramitesConDocumentosFaltantes(
        List<Actividad> actividades) {
        List<Tramite> answer = null;
        answer = this.tramiteDao
            .recuperarTramitesConDocumentosFaltantes(actividades);
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see ITramiteLocal#obtenerTareasGeograficas(UsuarioDTO)
     */
    @Override
    public ResultadoVO obtenerTareasGeograficas(UsuarioDTO usuario) {

        List<Actividad> actividades = new ArrayList<Actividad>();

        List<Actividad> actividadesE = procesosService
            .consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA);

        List<Actividad> actividadesV = procesosService
            .consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA);

        if (actividadesE != null && !actividadesE.isEmpty()) {
            for (Actividad a : actividadesE) {
                actividades.add(a);
            }
        }
        if (actividadesV != null && !actividadesV.isEmpty()) {
            for (Actividad a : actividadesV) {
                actividades.add(a);
            }
        }

        if (actividades.isEmpty()) {
            ResultadoVO vo = new ResultadoVO(usuario, new ArrayList<ActividadVO>());
            return vo;
        }

        List<Long> idsObjetoNeg = new LinkedList<Long>();

        if (!actividades.isEmpty()) {
            for (Actividad act : actividades) {
                idsObjetoNeg.add(act.getIdObjetoNegocio());
            }
        }
        //Trae los tramites haciendole fetch solo a los tramitePredioEnglobes y a sus predios
        List<Tramite> trams = this.tramiteDao.getTramitesDeActGeograficas(idsObjetoNeg);
        //ActividadesVO para mostrar en el XML
        List<ActividadVO> actividadesVo = new LinkedList<ActividadVO>();
        try {

            if (trams != null) {
                for (Tramite t : trams) {
                    ActividadVO aVo = new ActividadVO();

                    for (Actividad act : actividades) {
                        if (act.getIdObjetoNegocio() == t.getId()) {
                            aVo.setIdActividad(act.getId());
                            aVo.setActividadActual(act.getRutaActividad().getSubprocesoActividad());
                            aVo.setNumeroSolicitud(act.getNumeroSolicitud());
                            if (act.getFechaInicio() != null) {
                                aVo.setFechaCreacion(act.getFechaInicio()
                                    .getTime());
                            }
                            if (act.getFechaAsignacion() != null) {
                                aVo.setFechaAsignacion(act.getFechaAsignacion()
                                    .getTime());
                            }
                            break;
                        }
                    }

                    aVo.setIdTramite(t.getId());
                    aVo.setNumeroRadicacion(t.getNumeroRadicacion());
                    aVo.setCodigoDepartamento(t.getDepartamento().getCodigo());
                    aVo.setNombreDepartamento(t.getDepartamento().getNombre());
                    aVo.setCodigoMunicipio(t.getMunicipio().getCodigo());
                    aVo.setNombreMunicipio(t.getMunicipio().getNombre());
                    aVo.setEstadoTramite(t.getEstado());

                    if (t.getPredio() != null) {

                        PredioInfoVO predioInfo = new PredioInfoVO();
                        predioInfo.setNumeroPredial(t.getPredio()
                            .getNumeroPredial());
                        if (t.getPredio().getNumeroPredialAnterior() != null) {
                            predioInfo.setNumeroPredialAnterior(t.getPredio()
                                .getNumeroPredialAnterior());
                        }
                        if (t.getPredio().getPredioDireccions() != null) {
                            List<PredioDireccionVO> dirs = new ArrayList<PredioDireccionVO>();
                            String dirPrincipal = null;

                            if (t.getPredio().getDireccionPrincipal() != null) {
                                dirs.add(
                                    new PredioDireccionVO(t.getPredio().getDireccionPrincipal()));
                                dirPrincipal = t.getPredio().getDireccionPrincipal();
                            }
                            for (PredioDireccion pd : t.getPredio()
                                .getPredioDireccions()) {
                                if (!pd.getDireccion().equals(dirPrincipal)) {
                                    dirs.add(new PredioDireccionVO(pd.getDireccion()));
                                }
                            }
                            predioInfo.setDirecciones(dirs);
                        }

                        List<PPredioDireccion> direccionesProy = this.conservacionService.
                            obtenerPpredioDirecionesPorPredioId(t.getPredio().getId());

                        if (direccionesProy != null) {
                            List<PPredioDireccionVO> pdirs = new ArrayList<PPredioDireccionVO>();

                            for (PPredioDireccion pd : direccionesProy) {
                                pdirs.add(new PPredioDireccionVO(pd.getDireccion()));
                            }
                            predioInfo.setpDirecciones(pdirs);
                        }

                        if (t.getPredio().getUnidadConstruccions() != null) {
                            List<UnidadConstruccionVO> unidadescontruccions =
                                new ArrayList<UnidadConstruccionVO>();
                            for (UnidadConstruccion uc : t.getPredio()
                                .getUnidadConstruccions()) {

                                UnidadConstruccionVO ucv = new UnidadConstruccionVO();
                                ucv.setAreaConstruccionUnidad(uc
                                    .getAreaConstruida());
                                ucv.setNumeroBanios(uc.getTotalBanios());
                                ucv.setNumeroHabitaciones(uc
                                    .getTotalHabitaciones());
                                ucv.setNumeroLocales(uc.getTotalLocales());
                                ucv.setNumeroPisos(uc
                                    .getTotalPisosConstruccion());
                                ucv.setTipoConstruccion(uc
                                    .getTipoConstruccion());
                                ucv.setTipoDominio(uc
                                    .getTipoDominio());
                                ucv.setUnidad(uc.getUnidad());
                                ucv.setPuntaje(uc.getTotalPuntaje());
                                ucv.setPisoUbicacion(uc.getPisoUbicacion());
                                // ucv.setUsoConstruccion(uc.getUsoConstruccion().getNombre());

                                unidadescontruccions.add(ucv);
                            }

                            predioInfo
                                .setUnidadConstruccions(unidadescontruccions);
                        }

                        List<PUnidadConstruccionVO> unidadescontruccions =
                            new ArrayList<PUnidadConstruccionVO>();
                        List<PUnidadConstruccion> pUnidadesConstruccions = null;
                        if (!t.isSegundaDesenglobe()) {
                            pUnidadesConstruccions = this
                                .obtenerPUnidadesConstruccionsByTramiteId(t
                                    .getId());
                        } else {
                            pUnidadesConstruccions = this
                                .obtenerPUnidadesConstruccionsByPredioId(t.getPredio()
                                    .getId(), false);
                        }
                        if (pUnidadesConstruccions != null) {
                            for (PUnidadConstruccion puc : pUnidadesConstruccions) {

                                PUnidadConstruccionVO punC = new PUnidadConstruccionVO();
                                if (puc.getAreaConstruida() != null) {
                                    punC.setAreaConstruccionUnidad(puc
                                        .getAreaConstruida());
                                }
                                if (puc.getCancelaInscribe() != null) {
                                    punC.setCancelaInscribe(puc
                                        .getCancelaInscribe());
                                }
                                if (puc.getTotalBanios() != null) {
                                    punC.setNumeroBanios(puc.getTotalBanios());
                                }
                                if (puc.getTotalHabitaciones() != null) {
                                    punC.setNumeroHabitaciones(puc
                                        .getTotalHabitaciones());
                                }
                                if (puc.getTotalLocales() != null) {
                                    punC.setNumeroLocales(puc.getTotalLocales());
                                }
                                if (puc.getTotalPisosConstruccion() != null) {
                                    punC.setNumeroPisos(puc
                                        .getTotalPisosConstruccion());
                                }
                                if (puc.getTipoConstruccion() != null) {
                                    punC.setTipoConstruccion(puc
                                        .getTipoConstruccion());
                                }
                                punC.setTipoDominio(puc.getTipoDominio());
                                if (puc.getUnidad() != null) {
                                    punC.setUnidad(puc.getUnidad());
                                }
                                if (puc.getTotalPuntaje() != null) {
                                    punC.setPuntaje(puc.getTotalPuntaje());
                                }
                                if (puc.getProvienePredio() != null) {
                                    punC.setProvienePredio(puc
                                        .getProvienePredio()
                                        .getNumeroPredial());
                                }
                                if (puc.getProvienePredio() != null &&
                                    puc.getProvienePredio()
                                        .getDireccionPrincipal() != null) {
                                    punC.setProvienePredioDireccion(puc
                                        .getProvienePredio()
                                        .getDireccionPrincipal());
                                }
                                if (puc.getProvieneUnidad() != null) {
                                    punC.setProvieneUnidad(puc
                                        .getProvieneUnidad());
                                }
                                punC.setPisoUbicacion(puc.getPisoUbicacion());

                                unidadescontruccions.add(punC);
                            }

                            predioInfo
                                .setpUnidadConstruccions(unidadescontruccions);
                        }

                        //Si el predio es un predio en PH
                        if (t.getPredio().isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(t.getPredio().getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioInfo.setFichaMatriz(fichaVO);
                            }
                        }

                        aVo.setPredio(predioInfo);
                    }

                    aVo.setTipoTramite(t.getTipoTramiteCadenaCompleto());
                    if (t.getClaseMutacion() != null) {
                        if (t.getClaseMutacion().equals(
                            EMutacionClase.PRIMERA.getCodigo())) {
                            aVo.setClaseMutacion("Mutación " +
                                EMutacionClase.PRIMERA.getNombre());
                        }
                        if (t.getClaseMutacion().equals(
                            EMutacionClase.SEGUNDA.getCodigo())) {
                            aVo.setClaseMutacion("Mutación " +
                                EMutacionClase.SEGUNDA.getNombre());
                        }
                        if (t.getClaseMutacion().equals(
                            EMutacionClase.TERCERA.getCodigo())) {
                            aVo.setClaseMutacion("Mutación " +
                                EMutacionClase.TERCERA.getNombre());
                        }
                        if (t.getClaseMutacion().equals(
                            EMutacionClase.CUARTA.getCodigo())) {
                            aVo.setClaseMutacion("Mutación " +
                                EMutacionClase.CUARTA.getNombre());
                        }
                        if (t.getClaseMutacion().equals(
                            EMutacionClase.QUINTA.getCodigo())) {
                            aVo.setClaseMutacion("Mutación " +
                                EMutacionClase.QUINTA.getNombre());
                            if (t.getOmitidoNuevo() != null &&
                                !t.getOmitidoNuevo().isEmpty()) {
                                aVo.setOmitidoNuevo(t.getOmitidoNuevo());
                            }

                            PredioInfoVO predioInfo = new PredioInfoVO();
                            List<PPredio> predopProy = this.conservacionService
                                .obtenerPPrediosPorTramiteIdActAvaluos(t
                                    .getId());
                            if (predopProy != null && !predopProy.isEmpty() &&
                                predopProy.size() < 2) {

                                predioInfo.setNumeroPredial(predopProy.get(0)
                                    .getNumeroPredial());
                                if (predopProy.get(0)
                                    .getNumeroPredialAnterior() != null) {
                                    predioInfo
                                        .setNumeroPredialAnterior(predopProy
                                            .get(0)
                                            .getNumeroPredialAnterior());
                                }
                                if (predopProy.get(0).getPPredioDireccions() != null) {
                                    List<PredioDireccionVO> dirs =
                                        new ArrayList<PredioDireccionVO>();

                                    for (PPredioDireccion pd : predopProy
                                        .get(0).getPPredioDireccions()) {
                                        dirs.add(new PredioDireccionVO(pd.getDireccion()));
                                    }
                                    predioInfo.setDirecciones(dirs);
                                }

                                List<PUnidadConstruccionVO> unidadescontruccions =
                                    new ArrayList<PUnidadConstruccionVO>();

                                List<PUnidadConstruccion> pUnidadesConstruccions = this
                                    .obtenerPUnidadesConstruccionsByTramiteId(t
                                        .getId());
                                if (pUnidadesConstruccions != null) {
                                    for (PUnidadConstruccion puc : pUnidadesConstruccions) {

                                        PUnidadConstruccionVO punC = new PUnidadConstruccionVO();
                                        if (puc.getAreaConstruida() != null) {
                                            punC.setAreaConstruccionUnidad(puc
                                                .getAreaConstruida());
                                        }
                                        if (puc.getCancelaInscribe() != null) {
                                            punC.setCancelaInscribe(puc
                                                .getCancelaInscribe());
                                        }
                                        if (puc.getTotalBanios() != null) {
                                            punC.setNumeroBanios(puc
                                                .getTotalBanios());
                                        }
                                        if (puc.getTotalHabitaciones() != null) {
                                            punC.setNumeroHabitaciones(puc
                                                .getTotalHabitaciones());
                                        }
                                        if (puc.getTotalLocales() != null) {
                                            punC.setNumeroLocales(puc
                                                .getTotalLocales());
                                        }
                                        if (puc.getTotalPisosConstruccion() != null) {
                                            punC.setNumeroPisos(puc
                                                .getTotalPisosConstruccion());
                                        }
                                        if (puc.getTipoConstruccion() != null) {
                                            punC.setTipoConstruccion(puc
                                                .getTipoConstruccion());
                                        }
                                        if (puc.getUnidad() != null) {
                                            punC.setUnidad(puc.getUnidad());
                                        }
                                        if (puc.getTotalPuntaje() != null) {
                                            punC.setPuntaje(puc
                                                .getTotalPuntaje());
                                        }
                                        if (puc.getProvienePredio() != null) {
                                            punC.setProvienePredio(puc
                                                .getProvienePredio()
                                                .getNumeroPredial());
                                        }
                                        if (puc.getProvienePredio() != null &&
                                            puc.getProvienePredio()
                                                .getDireccionPrincipal() != null) {
                                            punC.setProvienePredioDireccion(puc
                                                .getProvienePredio()
                                                .getDireccionPrincipal());
                                        }
                                        if (puc.getProvieneUnidad() != null) {
                                            punC.setProvieneUnidad(puc
                                                .getProvieneUnidad());
                                        }
                                        punC.setPisoUbicacion(puc.getPisoUbicacion());
                                        punC.setTipoDominio(puc.getTipoDominio());

                                        unidadescontruccions.add(punC);
                                    }

                                    predioInfo
                                        .setpUnidadConstruccions(unidadescontruccions);
                                }
                                aVo.setPredio(predioInfo);
                            }

                        }
                    }

                    if (t.getSubtipo() != null) {
                        aVo.setSubtipoMutacion(t.getSubtipo());
                        List<String> numPrediales = new LinkedList<String>();
                        List<PredioDireccionVO> direccionesR = new ArrayList<PredioDireccionVO>();

                        List<PredioInfoVO> predios = new ArrayList<PredioInfoVO>();

                        if (t.getSubtipo().equals(
                            EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                            List<PPredio> ppredios = this.pPredioDao
                                .findPPrediosResultantesByTramiteId(t
                                    .getId());
                            if (ppredios != null) {
                                for (PPredio pp : ppredios) {
                                    PredioInfoVO pre = new PredioInfoVO();
                                    pre.setNumeroPredial(pp.getNumeroPredial());
                                    pre.setNumeroPredialAnterior(pp
                                        .getNumeroPredialAnterior());
                                    // numPrediales.add(pp.getNumeroPredial());
                                    List<PPredioDireccion> direccionesProy =
                                        this.conservacionService.
                                            obtenerPpredioDirecionesPorPredioId(pp.getId());

                                    if (direccionesProy != null) {
                                        List<PPredioDireccionVO> pdirs =
                                            new ArrayList<PPredioDireccionVO>();

                                        for (PPredioDireccion pd : direccionesProy) {
                                            pdirs.add(new PPredioDireccionVO(pd.getDireccion()));
                                        }
                                        pre.setpDirecciones(pdirs);
                                    }
//									if (pp.getPPredioDireccions() != null) {
//										List<PredioDireccionVO> dirs = new LinkedList<PredioDireccionVO>();
//										for (PPredioDireccion ppd : pp
//												.getPPredioDireccions()) {
//											if (ppd.getPrincipal() != null
//													&& ppd.getPrincipal()
//															.equals(ESiNo.SI
//																	.getCodigo())) {
//												dirs.add(new PredioDireccionVO(ppd.getDireccion()));
//											}
//										}
//										pre.setDirecciones(dirs);
//									}

                                    List<PUnidadConstruccionVO> unidadescontruccions =
                                        new ArrayList<PUnidadConstruccionVO>();

                                    List<PUnidadConstruccion> pUnidadesConstruccions = this
                                        .obtenerPUnidadesConstruccionsByPredioId(pp
                                            .getId(), false);
                                    if (pUnidadesConstruccions != null) {
                                        for (PUnidadConstruccion puc : pUnidadesConstruccions) {

                                            PUnidadConstruccionVO punC = new PUnidadConstruccionVO();
                                            if (puc.getAreaConstruida() != null) {
                                                punC.setAreaConstruccionUnidad(puc
                                                    .getAreaConstruida());
                                            }
                                            if (puc.getCancelaInscribe() != null) {
                                                punC.setCancelaInscribe(puc
                                                    .getCancelaInscribe());
                                            }
                                            if (puc.getTotalBanios() != null) {
                                                punC.setNumeroBanios(puc
                                                    .getTotalBanios());
                                            }
                                            if (puc.getTotalHabitaciones() != null) {
                                                punC.setNumeroHabitaciones(puc
                                                    .getTotalHabitaciones());
                                            }
                                            if (puc.getTotalLocales() != null) {
                                                punC.setNumeroLocales(puc
                                                    .getTotalLocales());
                                            }
                                            if (puc.getTotalPisosConstruccion() != null) {
                                                punC.setNumeroPisos(puc
                                                    .getTotalPisosConstruccion());
                                            }
                                            if (puc.getTipoConstruccion() != null) {
                                                punC.setTipoConstruccion(puc
                                                    .getTipoConstruccion());
                                            }
                                            if (puc.getUnidad() != null) {
                                                punC.setUnidad(puc.getUnidad());
                                            }
                                            if (puc.getTotalPuntaje() != null) {
                                                punC.setPuntaje(puc
                                                    .getTotalPuntaje());
                                            }
                                            if (puc.getProvienePredio() != null) {
                                                punC.setProvienePredio(puc
                                                    .getProvienePredio()
                                                    .getNumeroPredial());
                                            }
                                            if (puc.getProvienePredio() != null &&
                                                puc.getProvienePredio()
                                                    .getDireccionPrincipal() != null) {
                                                punC.setProvienePredioDireccion(puc
                                                    .getProvienePredio()
                                                    .getDireccionPrincipal());
                                            }
                                            if (puc.getProvieneUnidad() != null) {
                                                punC.setProvieneUnidad(puc
                                                    .getProvieneUnidad());
                                            }
                                            punC.setPisoUbicacion(puc.getPisoUbicacion());
                                            punC.setTipoDominio(puc.getTipoDominio());

                                            unidadescontruccions.add(punC);
                                        }

                                        pre.setpUnidadConstruccions(unidadescontruccions);
                                    }

                                    //Si el predio es un predio en PH
                                    if (pp.isEsPredioEnPH()) {
                                        PFichaMatriz ficha = this.conservacionService.
                                            buscarPFichaMatrizPorPredioId(pp.getId());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            pre.setFichaMatriz(fichaVO);
                                        }
                                    }

                                    predios.add(pre);
                                }

                                List<PredioDireccionVO> direccions =
                                    new ArrayList<PredioDireccionVO>();
                                if (t.getPredio() != null &&
                                    t.getPredio().getPredioDireccions() != null) {
                                    for (PredioDireccion pd : t.getPredio()
                                        .getPredioDireccions()) {
                                        if (pd.getPrincipal() != null &&
                                            pd.getPrincipal().equals(
                                                ESiNo.SI.getCodigo())) {
                                            direccions.add(new PredioDireccionVO(pd.getDireccion()));
                                        }
                                    }
                                }

                                List<PPredioDireccion> direccionesProy = this.conservacionService.
                                    obtenerPpredioDirecionesPorPredioId(t.getPredio().getId());

                                List<PPredioDireccionVO> pdirs = new ArrayList<PPredioDireccionVO>();
                                if (direccionesProy != null) {
                                    for (PPredioDireccion pd : direccionesProy) {
                                        pdirs.add(new PPredioDireccionVO(pd.getDireccion()));
                                    }
                                }

                                PredioInfoVO pInfo = new PredioInfoVO(t
                                    .getPredio().getNumeroPredial(),
                                    t.getPredio()
                                        .getNumeroPredialAnterior(),
                                    direccions, pdirs);

                                //Si el predio es un predio en PH
                                if (t.getPredio().isEsPredioEnPH()) {
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(
                                            t.getPredio().getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        pInfo.setFichaMatriz(fichaVO);
                                    }
                                }

                                aVo.setPredioOriginal(pInfo);

                                aVo.setPrediosResultantes(predios);

                            }
                        } else if (t.getSubtipo().equals(
                            EMutacion2Subtipo.ENGLOBE.getCodigo())) {
                            if (t.getTramitePredioEnglobes() != null) {

                                for (TramitePredioEnglobe tpe : t
                                    .getTramitePredioEnglobes()) {
                                    if (tpe.getPredio().getPredioDireccions() != null) {
                                        for (PredioDireccion pd : tpe
                                            .getPredio()
                                            .getPredioDireccions()) {
                                            if (pd.getPrincipal() != null &&
                                                pd.getPrincipal()
                                                    .equals(ESiNo.SI
                                                        .getCodigo())) {
                                                direccionesR.add(new PredioDireccionVO(pd
                                                    .getDireccion()));
                                            }
                                        }
                                    }
                                    List<UnidadConstruccionVO> unidadescontruccions =
                                        new ArrayList<UnidadConstruccionVO>();

                                    List<UnidadConstruccion> unidadesConstruccions = this
                                        .obtenerUnidadesConstruccionsPorPredioId(tpe
                                            .getPredio().getId());
                                    if (unidadesConstruccions != null) {
                                        for (UnidadConstruccion puc : unidadesConstruccions) {

                                            UnidadConstruccionVO punC = new UnidadConstruccionVO();
                                            if (puc.getAreaConstruida() != null) {
                                                punC.setAreaConstruccionUnidad(puc
                                                    .getAreaConstruida());
                                            }
                                            if (puc.getTotalBanios() != null) {
                                                punC.setNumeroBanios(puc
                                                    .getTotalBanios());
                                            }
                                            if (puc.getTotalHabitaciones() != null) {
                                                punC.setNumeroHabitaciones(puc
                                                    .getTotalHabitaciones());
                                            }
                                            if (puc.getTotalLocales() != null) {
                                                punC.setNumeroLocales(puc
                                                    .getTotalLocales());
                                            }
                                            if (puc.getTotalPisosConstruccion() != null) {
                                                punC.setNumeroPisos(puc
                                                    .getTotalPisosConstruccion());
                                            }
                                            if (puc.getTipoConstruccion() != null) {
                                                punC.setTipoConstruccion(puc
                                                    .getTipoConstruccion());
                                            }
                                            if (puc.getUnidad() != null) {
                                                punC.setUnidad(puc.getUnidad());
                                            }
                                            if (puc.getTotalPuntaje() != null) {
                                                punC.setPuntaje(puc
                                                    .getTotalPuntaje());
                                            }
                                            punC.setPisoUbicacion(puc.getPisoUbicacion());
                                            punC.setTipoDominio(puc.getTipoDominio());

                                            unidadescontruccions.add(punC);
                                        }

                                    }

                                    List<PPredioDireccion> direccionesProy =
                                        this.conservacionService.
                                            obtenerPpredioDirecionesPorPredioId(tpe.getPredio().
                                                getId());

                                    List<PPredioDireccionVO> pdirs =
                                        new ArrayList<PPredioDireccionVO>();
                                    if (direccionesProy != null) {
                                        for (PPredioDireccion pd : direccionesProy) {
                                            pdirs.add(new PPredioDireccionVO(pd.getDireccion()));
                                        }
                                    }

                                    PredioInfoVO predioIn = new PredioInfoVO(
                                        tpe.getPredio().getNumeroPredial(),
                                        tpe.getPredio()
                                            .getNumeroPredialAnterior(),
                                        direccionesR, pdirs);

                                    //Si el predio es un predio en PH
                                    if (tpe.getPredio().isEsPredioEnPH()) {
                                        FichaMatriz ficha = this.conservacionService.
                                            getFichaMatrizByNumeroPredialPredio(
                                                tpe.getPredio().getNumeroPredial());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            predioIn.setFichaMatriz(fichaVO);
                                        }
                                    }

                                    predioIn.setUnidadConstruccions(unidadescontruccions);
                                    predios.add(predioIn);

                                    direccionesR = new ArrayList<PredioDireccionVO>();
                                }

                                aVo.setPrediosOriginales(predios);

                                List<PPredio> predioResultante = this.pPredioDao
                                    .findPPrediosResultantesByTramiteId(t
                                        .getId());
                                if (predioResultante != null &&
                                    !predioResultante.isEmpty()) {
                                    // En el caso de un Englobe SOLO deberia
                                    // quedar
                                    // un predio resultante
                                    // de las proyecciones
                                    //
                                    PPredio predioRes = new PPredio();
                                    for (PPredio pp : predioResultante) {
                                        if (!pp.getCancelaInscribe()
                                            .equals(EProyeccionCancelaInscribe.CANCELA
                                                .getCodigo())) {
                                            predioRes = pp;
                                        }
                                    }
                                    if (predioRes.getPredio() != null &&
                                        predioRes.getPPredioDireccions() != null) {
                                        for (PPredioDireccion ppd : predioRes
                                            .getPPredioDireccions()) {
                                            if (ppd.getPrincipal() != null &&
                                                ppd.getPrincipal()
                                                    .equals(ESiNo.SI
                                                        .getCodigo())) {
                                                direccionesR.add(new PredioDireccionVO(ppd
                                                    .getDireccion()));
                                            }
                                        }
                                    }

                                    List<PPredioDireccion> direccionesProy =
                                        this.conservacionService.
                                            obtenerPpredioDirecionesPorPredioId(predioRes.getId());

                                    List<PPredioDireccionVO> pdirs =
                                        new ArrayList<PPredioDireccionVO>();
                                    if (direccionesProy != null) {
                                        for (PPredioDireccion pd : direccionesProy) {
                                            pdirs.add(new PPredioDireccionVO(pd.getDireccion()));
                                        }
                                    }

                                    PredioInfoVO predioIn = new PredioInfoVO(
                                        predioRes.getNumeroPredial(),
                                        predioRes
                                            .getNumeroPredialAnterior(),
                                        direccionesR, pdirs);

                                    List<PUnidadConstruccionVO> unidadescontruccions =
                                        new ArrayList<PUnidadConstruccionVO>();

                                    List<PUnidadConstruccion> pUnidadesConstruccions = this
                                        .obtenerPUnidadesConstruccionsByPredioId(predioRes
                                            .getId(), false);
                                    if (pUnidadesConstruccions != null) {
                                        for (PUnidadConstruccion puc : pUnidadesConstruccions) {

                                            PUnidadConstruccionVO punC = new PUnidadConstruccionVO();
                                            if (puc.getAreaConstruida() != null) {
                                                punC.setAreaConstruccionUnidad(puc
                                                    .getAreaConstruida());
                                            }
                                            if (puc.getCancelaInscribe() != null) {
                                                punC.setCancelaInscribe(puc
                                                    .getCancelaInscribe());
                                            }
                                            if (puc.getTotalBanios() != null) {
                                                punC.setNumeroBanios(puc
                                                    .getTotalBanios());
                                            }
                                            if (puc.getTotalHabitaciones() != null) {
                                                punC.setNumeroHabitaciones(puc
                                                    .getTotalHabitaciones());
                                            }
                                            if (puc.getTotalLocales() != null) {
                                                punC.setNumeroLocales(puc
                                                    .getTotalLocales());
                                            }
                                            if (puc.getTotalPisosConstruccion() != null) {
                                                punC.setNumeroPisos(puc
                                                    .getTotalPisosConstruccion());
                                            }
                                            if (puc.getTipoConstruccion() != null) {
                                                punC.setTipoConstruccion(puc
                                                    .getTipoConstruccion());
                                            }
                                            if (puc.getUnidad() != null) {
                                                punC.setUnidad(puc.getUnidad());
                                            }
                                            if (puc.getTotalPuntaje() != null) {
                                                punC.setPuntaje(puc
                                                    .getTotalPuntaje());
                                            }
                                            if (puc.getProvienePredio() != null) {
                                                punC.setProvienePredio(puc
                                                    .getProvienePredio()
                                                    .getNumeroPredial());
                                            }
                                            if (puc.getProvienePredio() != null &&
                                                puc.getProvienePredio()
                                                    .getDireccionPrincipal() != null) {
                                                punC.setProvienePredioDireccion(puc
                                                    .getProvienePredio()
                                                    .getDireccionPrincipal());
                                            }
                                            if (puc.getProvieneUnidad() != null) {
                                                punC.setProvieneUnidad(puc
                                                    .getProvieneUnidad());
                                            }
                                            punC.setPisoUbicacion(puc.getPisoUbicacion());
                                            punC.setTipoDominio(puc.getTipoDominio());

                                            unidadescontruccions.add(punC);
                                        }
                                        predioIn.setpUnidadConstruccions(unidadescontruccions);
                                    }
                                    //Si el predio es un predio en PH
                                    if (predioRes.isEsPredioEnPH()) {
                                        PFichaMatriz ficha = this.conservacionService.
                                            buscarPFichaMatrizPorPredioId(predioRes.getId());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            predioIn.setFichaMatriz(fichaVO);
                                        }
                                    }

                                    aVo.setPredioResultante(predioIn);
                                }
                            }
                        }
                    }
                    actividadesVo.add(aVo);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        ResultadoVO vo = new ResultadoVO(usuario, actividadesVo);
        return vo;

    }
    //------------------------------------------------------------------------------------------------      

    /**
     * @see ITramiteLocal#obtenerTareasGeograficas(UsuarioDTO)
     * @author andres.eslava
     *
     * @modified andres.eslava::Modifica recuperacion M3 recupera proyectados con cancela inscribe
     * en NULL::14/Nov/2013
     * @modified andres.eslava::Refs #7036::06/Mar/2014
     */
    @Override
    public ResultadoVO obtenerTareasGeograficas2(UsuarioDTO usuario) {

        LOGGER.info("Inicio generacion XML de lista de tareas para el usuario: " + usuario.
            getLogin());

        ResultadoVO resultadoTareasPendientes;

        List<Actividad> actividades = new ArrayList<Actividad>();
        List<Actividad> actividadesE = procesosService.
            consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA);
        List<Actividad> actividadesV = procesosService.
            consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA);
        List<Actividad> actividadesDD = procesosService.
            consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION);
        List<Actividad> actividadesDE = procesosService.
            consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR);
        List<Actividad> actividadesDT = procesosService.
            consultarListaActividadesFiltroNombreActividad(usuario,
                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA);

        if (actividadesE != null && !actividadesE.isEmpty()) {
            actividades.addAll(actividadesE);
        }
        if (actividadesV != null && !actividadesV.isEmpty()) {
            actividades.addAll(actividadesV);
        }
        if (actividadesDD != null && !actividadesDD.isEmpty()) {
            actividades.addAll(actividadesDD);
        }
        if (actividadesDE != null && !actividadesDE.isEmpty()) {
            actividades.addAll(actividadesDE);
        }
        if (actividadesDT != null && !actividadesDT.isEmpty()) {
            actividades.addAll(actividadesDT);
        }

        // No hay actividades para edicion geografica
        if (actividades.isEmpty()) {
            ResultadoVO vo = new ResultadoVO(usuario, new ArrayList<ActividadVO>());
            return vo;
        }

        LOGGER.info("Las actividades pendientes para edicion geografica son: " + actividades.size());
        //ActividadesVO para mostrar en el XML
        List<ActividadVO> actividadesVo = new LinkedList<ActividadVO>();
        try {
            for (Actividad actividad : actividades) {

                Tramite tramiteAsociado = this.tramiteDao.getTramiteParaEdicionGeografica(actividad.
                    getIdObjetoNegocio());
                ActividadVO actividadVo = new ActividadVO(actividad, tramiteAsociado);
                //Construye la informacion necesaria para el editor terminos del Tipo tramite, clase mutacion y subtipo

                //Caso Depuracion
                if (actividad.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                    actividad.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                    actividad.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {

                    PredioInfoVO predioAsociadoTramite;
                    List<PredioInfoVO> prediosAsociadosTramite;
                    List<TramiteInconsistencia> inconsistencias;
                    List<TramiteInconsistenciaVO> inconsistenciasVo =
                        new ArrayList<TramiteInconsistenciaVO>();

                    if (tramiteAsociado.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.
                        getCodigo())) {
                        Predio predioOriginal;
                        predioOriginal = tramiteAsociado.getPredio();
                        PredioInfoVO predioResultante = new PredioInfoVO(predioOriginal,
                            tramiteAsociado);
                        if (predioOriginal.isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(predioOriginal.
                                    getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioResultante.setFichaMatriz(fichaVO);
                            }
                        }
                        actividadVo.setPredio(predioResultante);
                    }
                    if (tramiteAsociado.getSubtipo() != null && tramiteAsociado.getSubtipo().equals(
                        EMutacion2Subtipo.ENGLOBE.getCodigo())) {
                        prediosAsociadosTramite = new ArrayList<PredioInfoVO>();
                        for (Predio predio : predioDao.obtenerPrediosCompletosEnglobePorTramiteId(
                            tramiteAsociado.getId())) {
                            PredioInfoVO predioVo = new PredioInfoVO(predio, tramiteAsociado);
                            if (predio.isEsPredioEnPH()) {
                                FichaMatriz ficha = this.conservacionService.
                                    getFichaMatrizByNumeroPredialPredio(predio.getNumeroPredial());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    predioVo.setFichaMatriz(fichaVO);
                                }
                            }
                            prediosAsociadosTramite.add(predioVo);
                        }
                        actividadVo.setPrediosOriginales(prediosAsociadosTramite);
                    } else {
                        predioAsociadoTramite = new PredioInfoVO(tramiteAsociado.getPredio(),
                            tramiteAsociado);
                        if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(tramiteAsociado.getPredio().
                                    getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioAsociadoTramite.setFichaMatriz(fichaVO);
                            }
                        }
                        actividadVo.setPredioOriginal(predioAsociadoTramite);
                    }
                    inconsistencias = this.tramiteInconsistenciaDAO.buscarDepuradaByTramiteId(
                        tramiteAsociado.getId(), "NO");
                    for (TramiteInconsistencia tramiteInconsistencia : inconsistencias) {
                        TramiteInconsistenciaVO inconsistenciaVO = new TramiteInconsistenciaVO(
                            tramiteInconsistencia);
                        inconsistenciasVo.add(inconsistenciaVO);
                    }
                    actividadVo.setInconsistenciaTramiteVO(inconsistenciasVo);
                } //Caso Mutaciones
                else {
                    if (tramiteAsociado.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.
                        getCodigo())) {

                        if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.TERCERA.
                            getCodigo())) {
                            PPredio infoPredioProyectado;
                            Predio infoPredioOriginal;
                            PredioInfoVO infPredialResultante;
                            PredioInfoVO infPredialOriginal;
                            infoPredioProyectado = this.pPredioDao.getPPrediosByTramiteId(
                                tramiteAsociado.getId()).get(0);
                            infoPredioOriginal = tramiteAsociado.getPredio();
                            infPredialResultante = new PredioInfoVO(infoPredioProyectado,
                                tramiteAsociado);
                            infPredialOriginal = new PredioInfoVO(infoPredioOriginal,
                                tramiteAsociado);

                            if (infoPredioProyectado.isEsPredioEnPH()) {
                                PFichaMatriz ficha = this.conservacionService.
                                    buscarPFichaMatrizPorPredioId(infoPredioProyectado.getId());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    infPredialResultante.setFichaMatriz(fichaVO);
                                }
                            }
                            if (infoPredioOriginal.isEsPredioEnPH()) {
                                FichaMatriz ficha = this.conservacionService.
                                    getFichaMatrizByNumeroPredialPredio(infoPredioOriginal.
                                        getNumeroPredial());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    infPredialOriginal.setFichaMatriz(fichaVO);
                                }
                            }
                            //Se agrega a la actividad la informacion necesaria para la edicion de una mutacion de tercera
                            actividadVo.setPredioOriginal(infPredialOriginal);
                            actividadVo.setPredioResultante(infPredialResultante);
                        } else if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.QUINTA.
                            getCodigo())) {
                            PPredio infoPredioProyectado;
                            PredioInfoVO infPredialResultante;

                            infoPredioProyectado = this.pPredioDao.
                                getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId()).get(
                                0);
                            infPredialResultante = new PredioInfoVO(infoPredioProyectado,
                                tramiteAsociado);
                            if (infoPredioProyectado.isEsPredioEnPH()) {
                                PFichaMatriz ficha = this.conservacionService.
                                    buscarPFichaMatrizPorPredioId(infoPredioProyectado.getId());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    infPredialResultante.setFichaMatriz(fichaVO);
                                }
                            }

                            actividadVo.setPredio(infPredialResultante);
                        } else if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.SEGUNDA.
                            getCodigo())) {
                            if (tramiteAsociado.getSubtipo().equals("ENGLOBE")) {
                                List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                                PPredio predioProyectado;
                                PredioInfoVO predioProyectadoResultante = new PredioInfoVO();
                                List<Predio> listaPrediosEnglobe = this.predioDao.
                                    obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.
                                        getId());
                                for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                                    PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                        unPredioOriginalEnglobe, tramiteAsociado);
                                    if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                                        FichaMatriz ficha = this.conservacionService.
                                            getFichaMatrizByNumeroPredialPredio(tramiteAsociado.
                                                getPredio().getNumeroPredial());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            unPredioInfoVo.setFichaMatriz(fichaVO);
                                        }
                                    }
                                    listaPrediosOriginales.add(unPredioInfoVo);
                                }
                                predioProyectado = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId()).
                                    get(0);
                                predioProyectadoResultante = new PredioInfoVO(predioProyectado,
                                    tramiteAsociado);
                                if (this.conservacionService.obtenerPPredioCompletoByIdTramite(
                                    tramiteAsociado.getId()).isEsPredioEnPH()) {
                                    PFichaMatriz ficha = this.conservacionService.
                                        buscarPFichaMatrizPorPredioId(predioProyectado.getId());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioProyectadoResultante.setFichaMatriz(fichaVO);
                                    }
                                }
                                actividadVo.setPrediosOriginales(listaPrediosOriginales);
                                actividadVo.setPredioResultante(predioProyectadoResultante);
                            } else {
                                List<PredioInfoVO> listaPrediosResultantes =
                                    new ArrayList<PredioInfoVO>();
                                PredioInfoVO predioOriginalDesenglobe = new PredioInfoVO();

                                List<PPredio> listaPPrediosDesenglobe = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId());
                                for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {
                                    PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                        unPredioProyectado, tramiteAsociado);
                                    if (unPredioProyectado.isEsPredioEnPH()) {
                                        PFichaMatriz ficha = this.conservacionService.
                                            buscarPFichaMatrizPorPredioId(unPredioProyectado.getId());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            predioProyectadoVO.setFichaMatriz(fichaVO);
                                        }
                                    }
                                    listaPrediosResultantes.add(predioProyectadoVO);
                                }
                                predioOriginalDesenglobe = new PredioInfoVO(tramiteAsociado.
                                    getPredio(), tramiteAsociado);
                                if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(tramiteAsociado.
                                            getPredio().getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioOriginalDesenglobe.setFichaMatriz(fichaVO);
                                    }
                                }
                                actividadVo.setPredioOriginal(predioOriginalDesenglobe);
                                actividadVo.setPrediosResultantes(listaPrediosResultantes);
                            }
                        }
                    } // Caso Rectificacion
                    else if (tramiteAsociado.getTipoTramite().equals(
                        ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
                        Predio predioOriginal;
                        List<Predio> prediosAsociadosRectificacion = new ArrayList<Predio>();
                        List<PredioInfoVO> prediosAsociados = new ArrayList();
                        predioOriginal = tramiteAsociado.getPredio();
                        PredioInfoVO predioResultante = new PredioInfoVO(predioOriginal,
                            tramiteAsociado);
                        if (predioOriginal.isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(predioOriginal.
                                    getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioResultante.setFichaMatriz(fichaVO);
                            }
                        }
                        //Se debe quitar cuando se haga despliegue del editor
                        if (tramiteAsociado.isRectificacionArea()) {
                            prediosAsociados.add(new PredioInfoVO(tramiteAsociado.getPredio(),
                                tramiteAsociado));
                        }
                        //Agrega Predios si es una rectificacion de area con linderos                                
                        if (tramiteAsociado.getTramitePredioEnglobes() != null) {
                            prediosAsociadosRectificacion = this.predioDao.
                                obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.getId());
                            for (Predio unPredio : prediosAsociadosRectificacion) {
                                PredioInfoVO unPredioVo =
                                    new PredioInfoVO(unPredio, tramiteAsociado);
                                if (unPredio.isEsPredioEnPH()) {
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(unPredio.
                                            getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        unPredioVo.setFichaMatriz(fichaVO);
                                    }
                                }
                                prediosAsociados.add(unPredioVo);
                            }
                            actividadVo.setPrediosOriginales(prediosAsociados);
                        }

                        actividadVo.setPredio(predioResultante);
                    } //Caso Revision de Avaluo
                    else if (tramiteAsociado.getTipoTramite().equals(
                        ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
                        Predio predioOriginal;
                        PPredio predioProyectado;
                        PredioInfoVO predioAsociadoTramite;
                        PredioInfoVO predioResultante;

                        predioOriginal = tramiteAsociado.getPredio();
                        predioAsociadoTramite = new PredioInfoVO(predioOriginal, tramiteAsociado);

                        predioProyectado = this.pPredioDao.getPPrediosByTramiteIdActualizarAvaluo(
                            tramiteAsociado.getId()).get(0);
                        predioResultante = new PredioInfoVO(predioProyectado, tramiteAsociado);

                        actividadVo.setPredio(predioAsociadoTramite);
                        actividadVo.setPredioOriginal(predioResultante);
                    }
                }
                actividadesVo.add(actividadVo);
            }
            resultadoTareasPendientes = new ResultadoVO(usuario, actividadesVo);
            return resultadoTareasPendientes;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA.
                getExcepcion(LOGGER, e);
        }
    }

    /**
     * @see ITramiteLocal#obtenerInformacionPredial
     * @author andres.eslava
     */
    @Override
    public ActividadVO obtenerInformacionPredialProyectadaTramite(Long idTramite) {

        try {
            Tramite tramiteAsociado = this.tramiteDao
                .getTramiteParaEdicionGeografica(idTramite);
            ActividadVO actividadVo = new ActividadVO(tramiteAsociado);

            if (tramiteAsociado.getTipoTramite().equals(
                EMutacionClase.PRIMERA.getCodigo())) {

                if (tramiteAsociado.getClaseMutacion().equals(
                    EMutacionClase.TERCERA.getCodigo())) {
                    PPredio infoPredioProyectado;
                    Predio infoPredioOriginal;
                    PredioInfoVO infPredialResultante;
                    PredioInfoVO infPredialOriginal;
                    infoPredioProyectado = this.pPredioDao
                        .getPPrediosByTramiteId(tramiteAsociado.getId())
                        .get(0);
                    infoPredioOriginal = tramiteAsociado.getPredio();
                    infPredialResultante = new PredioInfoVO(
                        infoPredioProyectado, tramiteAsociado);
                    infPredialOriginal = new PredioInfoVO(infoPredioOriginal,
                        tramiteAsociado);

                    if (infoPredioProyectado.isEsPredioEnPH()) {
                        PFichaMatriz ficha = this.conservacionService
                            .buscarPFichaMatrizPorPredioId(infoPredioProyectado
                                .getId());
                        if (ficha != null) {
                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                            fichaVO = this
                                .loadUnidadesConstruccionPorPredioPH(fichaVO);
                            infPredialResultante.setFichaMatriz(fichaVO);
                        }
                    }
                    if (infoPredioOriginal.isEsPredioEnPH()) {
                        FichaMatriz ficha = this.conservacionService
                            .getFichaMatrizByNumeroPredialPredio(infoPredioOriginal
                                .getNumeroPredial());
                        if (ficha != null) {
                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                            fichaVO = this
                                .loadUnidadesConstruccionPorPredioPH(fichaVO);
                            infPredialOriginal.setFichaMatriz(fichaVO);
                        }
                    }
                    // Se agrega a la actividad la informacion necesaria para la
                    // edicion de una mutacion de tercera
                    actividadVo.setPredioOriginal(infPredialOriginal);
                    actividadVo.setPredioResultante(infPredialResultante);

                } else if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.QUINTA.
                    getCodigo())) {
                    PredioInfoVO predioOriginal = new PredioInfoVO();
                    predioOriginal.setNumeroPredial(tramiteAsociado.getNumeroPredial());
                    PPredio infoPredioProyectado;
                    PredioInfoVO infPredialResultante;
                    infoPredioProyectado = this.pPredioDao
                        .getPPrediosByTramiteIdActualizarAvaluo(
                            tramiteAsociado.getId()).get(0);
                    infPredialResultante = new PredioInfoVO(
                        infoPredioProyectado, tramiteAsociado);
                    if (infoPredioProyectado.isEsPredioEnPH()) {
                        PFichaMatriz ficha = this.conservacionService
                            .buscarPFichaMatrizPorPredioId(infoPredioProyectado
                                .getId());
                        if (ficha != null) {
                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                            fichaVO = this
                                .loadUnidadesConstruccionPorPredioPH(fichaVO);
                            infPredialResultante.setFichaMatriz(fichaVO);
                        }
                    }
                    actividadVo.setPredioOriginal(predioOriginal);
                    actividadVo.setPredioResultante(infPredialResultante);
                } else if (tramiteAsociado.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo())) {
                    if (tramiteAsociado.getSubtipo().equals("ENGLOBE")) {
                        List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                        PPredio predioProyectado;
                        PredioInfoVO predioProyectadoResultante = new PredioInfoVO();
                        List<Predio> listaPrediosEnglobe = this.predioDao
                            .obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado
                                .getId());
                        for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                            PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                unPredioOriginalEnglobe, tramiteAsociado);
                            if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                                FichaMatriz ficha = this.conservacionService
                                    .getFichaMatrizByNumeroPredialPredio(tramiteAsociado
                                        .getPredio().getNumeroPredial());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(
                                        ficha);
                                    fichaVO = this
                                        .loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    unPredioInfoVo.setFichaMatriz(fichaVO);
                                }
                            }
                            listaPrediosOriginales.add(unPredioInfoVo);
                        }
                        predioProyectado = this.pPredioDao
                            .getPPrediosByTramiteIdActualizarAvaluo(
                                tramiteAsociado.getId()).get(0);
                        predioProyectadoResultante = new PredioInfoVO(
                            predioProyectado, tramiteAsociado);
                        if (this.conservacionService
                            .obtenerPPredioCompletoByIdTramite(
                                tramiteAsociado.getId())
                            .isEsPredioEnPH()) {
                            PFichaMatriz ficha = this.conservacionService
                                .buscarPFichaMatrizPorPredioId(predioProyectado
                                    .getId());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this
                                    .loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioProyectadoResultante
                                    .setFichaMatriz(fichaVO);
                            }
                        }
                        actividadVo
                            .setPrediosOriginales(listaPrediosOriginales);
                        actividadVo
                            .setPredioResultante(predioProyectadoResultante);
                    } else {
                        List<PredioInfoVO> listaPrediosResultantes = new ArrayList<PredioInfoVO>();
                        PredioInfoVO predioOriginalDesenglobe = new PredioInfoVO();

                        List<PPredio> listaPPrediosDesenglobe = this.pPredioDao
                            .getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId());
                        for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {
                            PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                unPredioProyectado, tramiteAsociado);
                            if (unPredioProyectado.isEsPredioEnPH()) {
                                PFichaMatriz ficha = this.conservacionService
                                    .buscarPFichaMatrizPorPredioId(unPredioProyectado
                                        .getId());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(
                                        ficha);
                                    fichaVO = this
                                        .loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    predioProyectadoVO.setFichaMatriz(fichaVO);
                                }
                            }
                            listaPrediosResultantes.add(predioProyectadoVO);
                        }
                        predioOriginalDesenglobe = new PredioInfoVO(
                            tramiteAsociado.getPredio(), tramiteAsociado);
                        if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService
                                .getFichaMatrizByNumeroPredialPredio(tramiteAsociado
                                    .getPredio().getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this
                                    .loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioOriginalDesenglobe
                                    .setFichaMatriz(fichaVO);
                            }
                        }
                        actividadVo.setPredioOriginal(predioOriginalDesenglobe);
                        actividadVo
                            .setPrediosResultantes(listaPrediosResultantes);
                    }
                }
            } // Caso Rectificacion
            else if (tramiteAsociado.getTipoTramite().equals(
                ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
                Predio predioOriginal;
                List<Predio> prediosAsociadosRectificacion = new ArrayList<Predio>();
                List<PredioInfoVO> prediosAsociados = new ArrayList();
                predioOriginal = tramiteAsociado.getPredio();
                PredioInfoVO predioResultante = new PredioInfoVO(
                    predioOriginal, tramiteAsociado);
                if (predioOriginal.isEsPredioEnPH()) {
                    FichaMatriz ficha = this.conservacionService
                        .getFichaMatrizByNumeroPredialPredio(predioOriginal
                            .getNumeroPredial());
                    if (ficha != null) {
                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                        fichaVO = this
                            .loadUnidadesConstruccionPorPredioPH(fichaVO);
                        predioResultante.setFichaMatriz(fichaVO);
                    }
                }

                if (tramiteAsociado.getTramitePredioEnglobes() != null) {
                    prediosAsociadosRectificacion = this.predioDao
                        .obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado
                            .getId());
                    for (Predio unPredio : prediosAsociadosRectificacion) {
                        PredioInfoVO unPredioVo = new PredioInfoVO(unPredio,
                            tramiteAsociado);
                        if (unPredio.isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService
                                .getFichaMatrizByNumeroPredialPredio(unPredio
                                    .getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this
                                    .loadUnidadesConstruccionPorPredioPH(fichaVO);
                                unPredioVo.setFichaMatriz(fichaVO);
                            }
                        }
                        prediosAsociados.add(unPredioVo);
                    }
                    actividadVo.setPrediosOriginales(prediosAsociados);
                }

                actividadVo.setPredio(predioResultante);
            } // Caso Revision de Avaluo
            else if (tramiteAsociado.getTipoTramite().equals(
                ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
                Predio predioOriginal;
                PPredio predioProyectado;
                PredioInfoVO predioAsociadoTramite;
                PredioInfoVO predioResultante;

                predioOriginal = tramiteAsociado.getPredio();
                predioAsociadoTramite = new PredioInfoVO(predioOriginal,
                    tramiteAsociado);

                predioProyectado = this.pPredioDao
                    .getPPrediosByTramiteIdActualizarAvaluo(
                        tramiteAsociado.getId()).get(0);
                predioResultante = new PredioInfoVO(predioProyectado,
                    tramiteAsociado);

                actividadVo.setPredio(predioAsociadoTramite);
                actividadVo.setPredioOriginal(predioResultante);
            }

            return actividadVo;
        } catch (Exception e) {
            LOGGER.error("Error en TramiteBean#obtenerInformacionPredialProyectadaTramite" +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA
                .getExcepcion(LOGGER, e);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#recuperarRutaGestorDocumentalImagenInicialYfinalDelTramite(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 06-08-2012 manejo de errores. Orden.
     */
    @Implement
    @Override
    public String[] recuperarRutaAlfrescoImagenInicialYfinalDelTramite(Long tramiteId) {

        LOGGER.debug("en TramiteBean#recuperarRutaAlfrescoImagenInicialYfinalDelTramite");

        String[] answer = null;
        try {
            answer = this.documentoDao
                .recuperarRutaGestorDocumentalImagenInicialYfinalDelTramite(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en TramiteBean#recuperarRutaAlfrescoImagenInicialYfinalDelTramite:" +
                " " + ex.getMensaje());
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#buscarModeloResolucionPorId(Long idModeloResolucion)
     */
    @Override
    public ModeloResolucion buscarModeloResolucionPorId(Long idModeloResolucion) {
        return this.modeloResolucionDao.findById(idModeloResolucion);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#buscarTramitesPorFiltros(String estadoTramite, String numeroSolicitudBusqueda,
     * String numeroRadicacionBusqueda, String numeroPredialBusqueda, FiltroDatosConsultaSolicitante
     * solicitanteBusqueda)
     */
    @Override
    public List<Tramite> buscarTramitesPorFiltros(String estadoTramite,
        String numeroSolicitudBusqueda, String numeroRadicacionBusqueda,
        FiltroDatosConsultaPredio datosConsultaPredio,
        FiltroDatosConsultaSolicitante solicitanteBusqueda) {

        List<Tramite> answer = null;
        List<VTramitePredio> vtpList = this.vTramitePredioDao
            .buscarTramitesPorFiltros(estadoTramite,
                numeroSolicitudBusqueda, numeroRadicacionBusqueda,
                datosConsultaPredio, solicitanteBusqueda);
        if (vtpList != null && !vtpList.isEmpty()) {
            answer = new ArrayList<Tramite>();
            for (VTramitePredio vtp : vtpList) {
                answer.add(vtp.getTramite());
            }
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @see ITramite#buscarTramiteTraerDocumentoResolucionPorIdTramite
     */
    @Override
    public Tramite buscarTramiteTraerDocumentoResolucionPorIdTramite(
        Long currentTramiteId) {
        Tramite tramite = null;

        try {

            tramite = this.tramiteDao.buscarTramiteTraerDocumentoResolucionPorIdTramite(
                currentTramiteId);

        } catch (ExcepcionSNC e) {
            LOGGER.error(
                "Error en TramiteBean#buscarTramiteTraerDocumentoResolucionPorIdTramite: " +
                e.getMensaje());
        }
        return tramite;
    }

    /**
     * @see ITramite#eliminarSolicitudAvisoRegistro(SolicitudAvisoRegistro)
     */
    @Override
    public void eliminarSolicitudAvisoRegistro(
        SolicitudAvisoRegistro solicitudAvisoRegistro) {
        this.solicitudAvisoRegistroDao.delete(solicitudAvisoRegistro);
    }

    /**
     * @see ITramite#generarProyeccion(Tramite)
     * @author fabio.navarrete
     */
    @Override
    public Object[] generarProyeccion(Tramite tramite) {
        try {

            tramite = this.transaccionalService.actualizarTramiteNuevaTransaccion(tramite);
            return this.sncProcedimientoDao.generarProyeccion(tramite.getId());

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see ITramite#eliminarProyeccion(Tramite)
     * @author fabio.navarrete
     */
    @Override
    public void eliminarProyeccion(Tramite tramite) {
        this.sncProcedimientoDao.eliminarProyeccion(tramite.getId());
    }

    /**
     * @see ITramite#generarProyeccionActualizarCondicionPropiedad(Tramite, String)
     * @author fabio.navarrete
     */
    @Override
    public Object[] generarProyeccionActualizarCondicionPropiedad(Tramite tramite,
        String condicionPropiedad) {
        try {
            //this.tramiteDao.update(tramite);
            Object mensajes[] = this.generarProyeccion(tramite);
            if (mensajes != null) {
                int numeroErrores = 0;
                if (mensajes.length > 0) {
                    for (Object o : mensajes) {
                        List l = (List) o;
                        if (l.size() > 0) {
                            String mensaje = "";
                            for (Object obj : l) {
                                Object[] obje = (Object[]) obj;
                                if (!((Object[]) obje)[0].equals("0")) {
                                    numeroErrores++;
                                }
                            }
                            if (numeroErrores > 0) {
                                return mensajes;
                            }
                        }
                    }
                }
            }
            List<PPredio> pPredios = this.pPredioDao
                .findPPrediosByTramiteId(tramite.getId());
            if (tramite.isSegundaEnglobe()) {
                auxUpdatePPredio(tramite, pPredios, condicionPropiedad);
            }

            if (tramite.isSegundaDesenglobe()) {
                pPredios.get(0).setCondicionPropiedadNumeroPredial(
                    condicionPropiedad);
                pPredios.get(0).setCancelaInscribe(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                this.pPredioDao.update(pPredios.get(0));
            }
            return null;
        } catch (Exception e) {
            LOGGER.error(
                "Fallo al generar la proyección o actualizar pPredios",
                Level.FATAL, e);
        }
        return null;
    }

    private void auxUpdatePPredio(Tramite tramite, List<PPredio> pPredios,
        String condicionPropiedad) {
        for (TramitePredioEnglobe tpe : tramite.getTramitePredioEnglobes()) {
            for (PPredio pPredio : pPredios) {
                if (pPredio.getId().equals(tpe.getPredio().getId()) &&
                    tpe.getEnglobePrincipal().equals(
                        ESiNo.SI.getCodigo())) {
                    pPredio.setCondicionPropiedadNumeroPredial(condicionPropiedad);
                    //pPredio.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                    // NO PUEDE HACERSE PORQUE DANA LA LOGICA DEFINIDA PARA Y ENTRE SP Y APP
                    // fredy.wilches
                    this.pPredioDao.update(pPredio);
                }
            }
        }
    }

    /**
     * @see ITramite#buscarTodosSolicitantes(int, int)
     */
    @Override
    public List<Solicitante> buscarTodosSolicitantes(int startIdx, int rowNum) {
        return this.solicitanteDao.findAll(startIdx, rowNum);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#insertarRegistrosComisionTramiteDato(java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void insertarRegistrosComisionTramiteDato(
        List<ComisionTramiteDato> registros) {

        LOGGER.debug("on TramiteBean#borrarComisionTramiteDatosPorComisionTramite ...");

        try {
            this.comisionTramiteDatoDao.persistMultiple(registros);
            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#borrarComisionTramiteDatosPorComisionTramite: " +
                e.getMessage());
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#actualizarComisionTramite(ComisionTramite)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ComisionTramite actualizarComisionTramite(ComisionTramite comisionTramite) {

        LOGGER.debug("on TramiteBean#actualizarComisionTramite ...");

        ComisionTramite answer = null;

        try {
            answer = this.comisionTramiteDao.update(comisionTramite);
            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#actualizarComisionTramite: " + e.getMessage());
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#borrarComisionTramiteDatosPorComisionTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void borrarComisionTramiteDatosPorComisionTramite(
        Long idComisionTramite) {

        LOGGER.debug("on TramiteBean#borrarComisionTramiteDatosPorComisionTramite ...");

        List<String[]> criteria = new ArrayList<String[]>();
        String[] criterion = new String[3];

        criterion[0] = "comisionTramite.id";
        criterion[1] = "number";
        criterion[2] = idComisionTramite.toString();
        criteria.add(criterion);

        try {
            this.comisionTramiteDatoDao.deleteByExactCriteria(criteria);
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#borrarComisionTramiteDatosPorComisionTramite: " +
                e.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitesParaCancelacion(FiltroDatosConsultaCancelarTramites)
     * @author fabio.navarrete
     */
    @Override
    public List<Tramite> buscarTramitesParaCancelacion(String estructuraOrgCod,
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize) {

        List<String> municipiosCods = this.jurisdiccionDao.
            getMunicipioCodsByEstructuraOrganizacionalCod(estructuraOrgCod);

        filtro.setMunicipiosJurisdiccion(municipiosCods);

        return this.tramiteDao.findTramitesCancelacion(filtro, first, pageSize);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitesParaCancelacion(FiltroDatosConsultaCancelarTramites)
     * @author cesar.vega
     */
    @Override
    public List<Tramite> buscarTramitesParaCancelacion(
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize) {

        return this.tramiteDao.findTramitesCancelacionOtraTerritorial(filtro, first, pageSize);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramiteCompletoConResolucion(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public Tramite buscarTramiteCompletoConResolucion(Long tramiteId) {

        LOGGER.debug("on TramiteBean#buscarTramiteCompletoConResolucion ...");
        Tramite answer = null;

        try {
            answer = this.tramiteDao.buscarTramiteConResolucionFull(tramiteId);
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#buscarTramiteConResolucion: " + e.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarTramitesCompletosConResolucion(long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesCompletosConResolucion(
        long[] tramitesIds, int... contadoresDesdeYCuantos) {

        LOGGER.debug("on TramiteBean#buscarTramitesCompletosConResolucion ...");
        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao
                .buscarTramitesConResolucionFull(tramitesIds);
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#buscarTramitesCompletosConResolucion: " +
                e.getMessage());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#contarComisionesParaAdministrar(java.lang.String, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int contarComisionesParaAdministrar(String idTerritorial, String rolUsuario) {
        LOGGER.debug("en TramiteBean#contarComisionesParaAdministrar");
        int answer = 0;

        try {
            answer = this.vcomisionDao.countComisionesParaAdmin(idTerritorial, rolUsuario);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#contarComisionesParaAdministrar: " + ex.getMensaje());
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#buscarComisionesParaAdministrar(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.util.Map, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<VComision> buscarComisionesParaAdministrar(String idTerritorial, String rolUsuario,
        String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("en TramiteBean#buscarComisionesParaAdministrar");

        List<VComision> answer = null;

        try {
            answer = this.vcomisionDao.findComisionesParaAdmin(idTerritorial, rolUsuario, sortField,
                sortOrder, filters, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarComisionesParaAdministrar: " + ex.getMensaje());
        }

        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    @Override
    public int contarResultadosBusquedaTramiteCancelacion(String estructuraOrgCod,
        FiltroDatosConsultaCancelarTramites filtro) {

        List<String> municipiosCods = this.jurisdiccionDao.
            getMunicipioCodsByEstructuraOrganizacionalCod(estructuraOrgCod);

        if (municipiosCods.isEmpty()) {
            return -1;
        }

        filtro.setMunicipiosJurisdiccion(municipiosCods);

        return this.tramiteDao.countTramitesCancelacion(filtro);
    }

    // --------------------------------------------------------------------------------------------------
    @Override
    public int contarResultadosBusquedaTramiteCancelacion(
        FiltroDatosConsultaCancelarTramites filtro) {

        return this.tramiteDao.countTramitesCancelacionOtraTerritorial(filtro);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#insertarComisionEstado(ComisionEstado, UsuarioDTO)
     * @author pedro.garcia
     */
    @Override
    public void insertarComisionEstado(ComisionEstado nuevaComisionEstado,
        UsuarioDTO loggedInUser) {
        LOGGER.debug("on TramiteBean#insertarComisionEstado ...");

        try {
            this.comisionEstadoDao.persist(nuevaComisionEstado);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(
                LOGGER, e, "ComisionEstado");
        }
        LOGGER.debug("... TramiteBean#insertarComisionEstado finished.");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#generarProyeccionActualizarCondicionPropiedadYFichaMatriz(Tramite, String,
     * UsuarioDTO)
     * @author fabio.navarrete
     */
    @Override
    public void generarProyeccionActualizarCondicionPropiedadYFichaMatriz(
        Tramite tramite, String condicionPropiedad, UsuarioDTO usuario) {
        try {
            if (tramite.isSegundaDesenglobe()) {
                //this.tramiteDao.update(tramite);
                this.generarProyeccion(tramite);

                //felipe.cadena::23-06-2015::Rectificacion de ficha matriz::para tramites sobre ficha matriz no se crea nueva ficha matriz porque esta ya existe.
                if ((tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz())) {
                    return;
                }

                List<PPredio> pPredios = this.pPredioDao
                    .findPPrediosByTramiteId(tramite.getId());

                pPredios.get(0).setCondicionPropiedadNumeroPredial(
                    condicionPropiedad);
                PPredio pPredio = pPredios.get(0);
                pPredio.setCondicionPropiedad(condicionPropiedad);
                if (pPredio.getPUnidadConstruccions() != null && !pPredio.getPUnidadConstruccions().
                    isEmpty()) {
                    for (PUnidadConstruccion pu : pPredio.getPUnidadConstruccions()) {
                        pu.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    }
                }
                pPredio.setId(this.pPredioDao.update(pPredio).getId());
                pPredio.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                    .getCodigo());
                PFichaMatriz fichaMatriz = ConservacionUtil
                    .createBlankPFichaMatriz(pPredio, usuario);
                fichaMatriz.setId(this.pFichaMatrizDao.update(fichaMatriz)
                    .getId());

                // Se asume que es unicamente: desenglobe con condicion 8 o 9
                // Se cancelan los propietarios del matriz
                for (PPersonaPredio ppp : fichaMatriz.getPPredio().getPPersonaPredios()) {
                    ppp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                    this.pPersonaPredioDAO.update(ppp);
                }
            }

        } catch (Exception e) {
            LOGGER.error(
                "Fallo al generar la proyección, actualizar pPredios o crear la ficha matriz",
                Level.FATAL, e);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @see ITramiteDAO#getDocsOfTramite(java.lang.Long)
     * @author juan.agudelo
     */
    @Implement
    @Override
    public Tramite getDocsOfTramite(Long tramiteId) {
        return this.tramiteDao.getDocsOfTramite(tramiteId);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#consultarUltimoCambioEstadoComision(java.lang.Long)
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-07
     */
    @Implement
    @Override
    public ComisionEstado consultarUltimoCambioEstadoComision(Long idComision) {

        LOGGER.debug("on TramiteBean#consultarUltimoCambioEstadoComision ...");

        ComisionEstado answer = null;
        List<ComisionEstado> tempEstados;

        try {
            tempEstados = this.comisionEstadoDao.getHistoricalRecord(idComision, 1);
            if (tempEstados != null && !tempEstados.isEmpty()) {
                answer = tempEstados.get(0);
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "TramiteBean#consultarUltimoCambioEstadoComision");
        }
        LOGGER.debug("... TramiteBean#consultarUltimoCambioEstadoComision finished.");

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#consultarHistoricoCambiosEstadoComision(java.lang.Long)
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-07
     */
    @Implement
    @Override
    public List<ComisionEstado> consultarHistoricoCambiosEstadoComision(Long idComision) {

        LOGGER.debug("on TramiteBean#consultarHistoricoCambioEstadoComision ...");

        List<ComisionEstado> answer = null;
        try {
            answer = this.comisionEstadoDao.getHistoricalRecord(idComision, -1);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
                "TramiteBean#consultarHistoricoCambioEstadoComision");
        }
        LOGGER.debug("... TramiteBean#consultarHistoricoCambioEstadoComision finished.");

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#borrarComisionTramitesDeComision(UsuarioDTO, Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean borrarComisionTramitesDeComision(UsuarioDTO currentUser, Long idComision) {

        LOGGER.debug("on TramiteBean#borrarComisionTramitesDeComision ...");
        boolean answer = true;
        try {
            this.comisionTramiteDao.deleteRowsByComisionId(idComision);
        } catch (ExcepcionSNC sncEx) {
//			throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, e,
//                "TramiteBean#borrarComisionTramitesDeComision");
            answer = false;
            LOGGER.error("Error en TramiteBean#borrarComisionTramitesDeComision: " +
                sncEx.getMensaje());
        }
        LOGGER.debug("TramiteBean#borrarComisionTramitesDeComision finished.");

        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#eliminarSolicitante(co.gov.igac.snc.persistence.entity.tramite.Solicitante)
     */
    @Override
    public void eliminarSolicitante(Solicitante solicitante) {
        this.solicitanteDao.delete(solicitante);
    }

    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#eliminarSolicitantes(java.util.List)
     */
    @Override
    public void eliminarSolicitantes(List<Solicitante> solicitantes) {
        for (Solicitante sol : solicitantes) {
            this.solicitanteDao.delete(sol);
        }
    }

    /*
     * (non-Javadoc) @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#eliminarTramitePruebaEntidad(co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad)
     */
    @Override
    public void eliminarTramitePruebaEntidad(
        TramitePruebaEntidad tramitePruebaEntidad) {
        this.tramitePruebaEntidadDao.delete(tramitePruebaEntidad);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Convierte una lista de SolicitanteSolicitud o SolicitanteTramite enn una lista de
     * ISolicitanteSolicitudOTramite que es una interfaz de las dos
     *
     * @author pedro.garcia
     * @param solicitantes
     * @return
     */
    private ArrayList<ISolicitanteSolicitudOTramite> wrapSolicitantes(List<?> solicitantes) {

        ArrayList<ISolicitanteSolicitudOTramite> answer = null;
        ISolicitanteSolicitudOTramite solicitanteAdded;

        answer = new ArrayList<ISolicitanteSolicitudOTramite>();
        for (Object solicitanteTemp : solicitantes) {
            solicitanteAdded = (ISolicitanteSolicitudOTramite) solicitanteTemp;
            answer.add(solicitanteAdded);
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Aplica la regla para determinar a qué solicitante se le envía la comunicación de
     * notificación:
     *
     * prioridad 1: Si es apoderado a este debe dirigirse la comunicación. prioridad 2: Si la
     * relación del solicitante es representante legal a este debe dirigirse la comunicación.
     * prioridad 3: Si existe solicitante en la solicitud y en el trámite debe enviarse a ambos,
     * exceptuando aquellos que son de aviso (en cuyo caso se le envía al del trámite). El que se
     * seleccione debe ser el PRIMERO que se haya ingresado como solicitante (se supone que el que
     * tenga el menor id en la tabla)
     *
     * @author pedro.garcia
     * @param solicitantes lista de objetos que pueden ser de tipo SolicitanteSolicitud o
     * SolicitanteTramite.
     * @return
     */
    private Solicitante selectSolicitanteParaEnvioComunicNotif2(
        List<ISolicitanteSolicitudOTramite> solicitantes) {

        Solicitante answer = null;
        Solicitante solicitante;
        long minId, tempId;
        String relacion;

        if (solicitantes == null || solicitantes.isEmpty()) {
            return null;
        }

        minId = (Long) solicitantes.get(0).getId();
        for (ISolicitanteSolicitudOTramite tempSolicitante : solicitantes) {

            //solicitante = (Solicitante) tempSolicitante.getSolicitante();
            solicitante = new Solicitante(tempSolicitante.getSolicitante());
            relacion = (String) tempSolicitante.getRelacion();

            // D: no debería suceder porque por defecto es 'propietario'
            if (relacion == null) {
                break;
            }

            if (relacion.equalsIgnoreCase(ESolicitanteSolicitudRelac.APODERADO.toString())) {
                answer = solicitante;
                break;
            } else if (relacion.equalsIgnoreCase(ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                .toString())) {
                answer = solicitante;
                break;
            } else {
                tempId = (Long) tempSolicitante.getId();
                if (tempId <= minId) {
                    minId = tempId;
                    answer = solicitante;
                }
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#buscarTramitesPendientesDePredio(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesPendientesDePredio(Long predioId) {
        LOGGER.debug("on TramiteBean#buscarTramitesPendientesDePredio ...");

        ArrayList<Tramite> answer = null;
        try {
            answer = (ArrayList<Tramite>) this.tramiteDao.lookForCurrentOnPredio(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en TramiteBean#buscarTramitesPendientesDePredio: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#buscarTramitesFinalizadosDePredio(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesFinalizadosDePredio(Long predioId) {
        LOGGER.debug("on TramiteBean#buscarTramitesFinalizadosDePredio ...");

        ArrayList<Tramite> answer = null;

        try {
            answer = (ArrayList<Tramite>) this.tramiteDao.lookForFinishedOnPredio(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción en TramiteBean#buscarTramitesFinalizadosDePredio: " +
                ex.getMensaje());
        }

        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#getTramitePruebaEntidadsByTramiteId(Long)
     * @author franz.gamba
     */
    @Override
    public List<TramitePruebaEntidad> getTramitePruebaEntidadsByTramiteId(Long tramiteId) {

        List<TramitePruebaEntidad> answer = null;
        try {
            answer = this.tramitePruebaEntidadDao.findTramitePruebaEntidadByTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#getTramitePruebaEntidadsByTramiteId: " +
                ex.getMensaje());
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramite#cancelarTramite(Tramite, TramiteEstado)
     * @author juan.agudelo
     * @version 2.0
     * @cu CU-NP-CO-166
     */
    /*
     * @modified by juanfelipe.garcia :: 23-10-2013 :: adiciono información faltante en registro de
     * TramiteDocumentacion
     */
    @Implement
    @Override
    public boolean cancelarTramite(Tramite selectedTramite,
        TramiteEstado selectedTramiteEstado, UsuarioDTO usuario,
        Documento documentoSoporteCancelacion) {

        selectedTramiteEstado.setTramite(selectedTramite);
        selectedTramiteEstado.setEstado(ETramiteEstado.CANCELADO.getCodigo());
        selectedTramiteEstado.setResponsable(usuario.getLogin());
        selectedTramiteEstado.setFechaLog(new Date(System.currentTimeMillis()));
        selectedTramiteEstado.setUsuarioLog(usuario.getLogin());

        String fileSeparator = System.getProperty("file.separator");

        try {

            selectedTramite = this.tramiteDao
                .findTramitePredioSolicitudByTramiteId(selectedTramite
                    .getId());

            if (documentoSoporteCancelacion != null) {

                String tipoDocumento = "";
                for (ETipoDocumento tdTmp : ETipoDocumento.values()) {
                    if (tdTmp.getId().equals(documentoSoporteCancelacion.getTipoDocumento().getId())) {
                        tipoDocumento = tdTmp.getNombre();
                    }
                }
                documentoSoporteCancelacion.setUsuarioCreador(usuario
                    .getLogin());
                documentoSoporteCancelacion.setFechaLog(new Date(System
                    .currentTimeMillis()));
                documentoSoporteCancelacion.setUsuarioLog(usuario.getLogin());

                DocumentoTramiteDTO documentoTramiteDTO;
                if (selectedTramite.getDepartamento() != null &&
                    selectedTramite.getMunicipio() != null &&
                    selectedTramite.getPredio() != null) {
                    documentoTramiteDTO = new DocumentoTramiteDTO(
                        FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                        documentoSoporteCancelacion.getArchivo(),
                        tipoDocumento,
                        new Date(System.currentTimeMillis()),
                        selectedTramite.getDepartamento().getNombre(),
                        selectedTramite.getMunicipio().getNombre(),
                        selectedTramite.getPredio().getNumeroPredial(),
                        selectedTramite.getTipoTramite(),
                        selectedTramite.getId());

                    documentoSoporteCancelacion = this.documentoDao
                        .guardarDocumentoGestorDocumental(usuario, documentoTramiteDTO,
                            documentoSoporteCancelacion);
                } else {
                    documentoTramiteDTO = new DocumentoTramiteDTO(
                        FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                        documentoSoporteCancelacion.getArchivo(),
                        tipoDocumento,
                        new Date(System.currentTimeMillis()),
                        selectedTramite.getTipoTramite(),
                        selectedTramite.getId());
                    documentoSoporteCancelacion = this.documentoDao
                        .guardarDocumentoGestorDocumentalSinNumeroPredial(
                            usuario, documentoTramiteDTO, documentoSoporteCancelacion);
                }

                TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
                TipoDocumento td = new TipoDocumento();

                td.setId(ETipoDocumento.DOCUMENTO_SOPORTE_DE_CANCELACION_DE_TRAMITE.getId());
                tramiteDocumentacion.setTipoDocumento(td);
                tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
                tramiteDocumentacion.setCantidadFolios(0);
                tramiteDocumentacion.setAdicional(ESiNo.SI.getCodigo());
                tramiteDocumentacion
                    .setDocumentoSoporte(documentoSoporteCancelacion);
                tramiteDocumentacion.setFechaAporte(new Date(System
                    .currentTimeMillis()));
                tramiteDocumentacion.setFechaLog(new Date(System
                    .currentTimeMillis()));
                tramiteDocumentacion.setUsuarioLog(usuario.getLogin());
                tramiteDocumentacion.setTramite(selectedTramite);
                tramiteDocumentacion.setMunicipio(selectedTramite.getMunicipio());
                tramiteDocumentacion.setDepartamento(selectedTramite.getDepartamento());
                tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());
                tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());

                selectedTramite.getTramiteDocumentacions().add(
                    tramiteDocumentacion);

            }

            selectedTramite.setEstado(ETramiteEstado.CANCELADO.getCodigo());
            selectedTramite.setFechaLog(new Date(System.currentTimeMillis()));
            selectedTramite.setUsuarioLog(usuario.getLogin());

            List<PPredio> predioProyectado = this.pPredioDao
                .findExistingPPrediosByTramiteId(selectedTramite.getId());

            if ((selectedTramite.getFuncionarioEjecutor() != null && !selectedTramite
                .getFuncionarioEjecutor().isEmpty()) ||
                (predioProyectado != null && !predioProyectado.isEmpty())) {

                String tipoTramiteCompleto = selectedTramite
                    .getTipoTramiteCadenaCompleto();

                enviarCorreoElectronicoCancelacionPredio(
                    selectedTramite.getFuncionarioEjecutor(), usuario,
                    selectedTramite.getNumeroRadicacion(),
                    tipoTramiteCompleto, selectedTramiteEstado.getMotivo());

                selectedTramite.setFuncionarioEjecutor(null);
                selectedTramite.setNombreFuncionarioEjecutor(null);

            }

            //D: se revisa si el trámite está en alguna comisión. Si es así, y es el único, esta se debe cancelar
            if (selectedTramite.getComisionado() != null &&
                !selectedTramite.getComisionado().isEmpty() &&
                selectedTramite.getComisionado().equals(
                    ESiNo.SI.getCodigo())) {

                List<ComisionTramite> cListTemp = this.comisionTramiteDao
                    .buscarComisionesTramitePorIdTramite(selectedTramite.getId());

                if (cListTemp.isEmpty()) {

                    for (ComisionTramite cTemp : cListTemp) {

                        if (!cTemp.getRealizada().equals(ESiNo.SI.getCodigo())) {

                            List<ComisionTramite> cListTramitesAsignadosTemp =
                                this.comisionTramiteDao
                                    .findComisionesTramiteByComisionId(cTemp
                                        .getComision().getId());

                            //N: si es el único trámite asociado a la comisión a la que pertenece el trámite
                            if (cListTramitesAsignadosTemp.size() == 1) {

                                Comision comTmp = cTemp.getComision();
                                comTmp.setEstado(EComisionEstado.CANCELADA.getCodigo());

                                this.comisionDao.update(comTmp);

                                ComisionEstado comEst = new ComisionEstado();

                                comEst.setComision(comTmp);
                                comEst.setFecha(new Date(System.currentTimeMillis()));
                                comEst.setEstado(EComisionEstado.CANCELADA.getCodigo());
                                comEst.setMotivo(
                                    EComisionEstadoMotivo.CANCELACION_TODOS_LOS_TRAMITES
                                        .getCodigo());
                                comEst.setFechaLog(new Date(System
                                    .currentTimeMillis()));
                                comEst.setUsuarioLog(usuario.getLogin());

                                this.comisionEstadoDao.update(comEst);

                            }
                            this.comisionTramiteDao.delete(cTemp);

                        }
                    }
                }
            }

            Tramite tTemp = this.tramiteDao.update(selectedTramite);
            TramiteEstado teTemp = this.tramiteEstadoDao
                .actualizarTramiteEstado(selectedTramiteEstado, usuario);

            //javier.aponte Se agrega esto para borrar la réplica de consulta codificada con el número 2000
            //Si existen,  borrar las réplicas, inicial  y final, correspondientes a los documentos 97 y 98 
            //asociadas al trámite que se está cancelando. :: 07/07/2014
            this.generalesService.borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                selectedTramite.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());
            this.generalesService.borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                selectedTramite.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());
            this.generalesService.borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                selectedTramite.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());

            //se borra la "versión" del trámite en el servidor arcgis
            SigDAOv2.getInstance().enviarJobEliminarVersion(
                this.productoCatastralJobDAO, selectedTramite.getMunicipio().getCodigo(),
                selectedTramite,
                usuario);

            if (tTemp == null || teTemp == null) {
                return false;
            }

        } catch (ExcepcionSNC sncEx) {

            if (sncEx.getCodigoDeExcepcion().equals(SncBusinessServiceExceptions.EXCEPCION_40001.
                getCodigoDeExcepcion())) {
                throw SncBusinessServiceExceptions.EXCEPCION_40001.getExcepcion(LOGGER,
                    SncBusinessServiceExceptions.EXCEPCION_40001,
                    SncBusinessServiceExceptions.EXCEPCION_40001.getMessage());

            } else {
                LOGGER.error(
                    "Ocurrió un error en el borrado de la versión del trámite en el ARCGIS sever: " +
                    sncEx);
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return true;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que permite estructurar correo electrónico para su envío
     *
     * @author juan.agudelo
     *
     * @param usuarioDestino usuario destino del correo electrónico
     * @param usuario usuario logueado en el sistema y remitente del correo electrónico
     * @param numeroRadicacion número de radicación del trámite
     * @param tipoTramiteCompleto tipo de trámite completo incluye clase mutación y subtipo
     * @param motivo motivo de la cancelación del trámite
     */
    private void enviarCorreoElectronicoCancelacionPredio(
        String usuarioDestino, UsuarioDTO usuario, String numeroRadicacion,
        String tipoTramiteCompleto, String motivo) {

        UsuarioDTO usuarioDestinoDto = this.generalesService.getCacheUsuario(usuarioDestino);
        String cuerpoCE;
        MessageFormat mf;
        Object[] datosCorreo = new Object[7];

        Calendar hoy = Calendar.getInstance();
        Date fecha = new java.util.Date(hoy.getTimeInMillis());

        datosCorreo[0] = usuario.getEmail();
        datosCorreo[1] = usuario.getCodigoTerritorial();
        datosCorreo[2] = usuarioDestinoDto.getEmail();
        datosCorreo[3] = numeroRadicacion;
        datosCorreo[4] = tipoTramiteCompleto;
        datosCorreo[5] = fecha;
        datosCorreo[6] = motivo;

        try {
            Plantilla plantilla = this.generalesService
                .recuperarPlantillaPorCodigo(EPlantilla.MENSAJE_DE_CANCELACION_TRAMITE
                    .getCodigo());

            if (plantilla != null) {
                cuerpoCE = plantilla.getHtml();
                if (cuerpoCE != null) {

                    Locale colombia = new Locale("ES", "es_CO");
                    mf = new MessageFormat(cuerpoCE, colombia);

                    cuerpoCE = mf.format(datosCorreo);

                    try {

                        this.generalesService
                            .enviarCorreo(
                                usuarioDestinoDto.getEmail(),
                                EPlantilla.MENSAJE_DE_CANCELACION_TRAMITE
                                    .getMensaje(), cuerpoCE, null,
                                null);

                    } catch (Exception e) {
                        throw SncBusinessServiceExceptions.EXCEPCION_100013
                            .getExcepcion(LOGGER, e, e.getMessage());
                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarTramiteSolicitudPorFiltroTramite(FiltroDatosConsultaTramite, int...)
     * @author juan.agudelo
     * @version 2.0
     */
    @Implement
    @Override
    public List<Tramite> buscarTramiteSolicitudPorFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        final int... rowStartIdxAndCount) {
        return this.tramiteDao.findTramiteSolicitudByFiltroTramite(
            filtroBusquedaTramite, rowStartIdxAndCount);
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#contarTramiteSolicitudPorFiltroTramite(FiltroDatosConsultaTramite)
     * @author juan.agudelo
     * @version 2.0
     */
    @Implement
    @Override
    public Integer contarTramiteSolicitudPorFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite) {

        try {
            return this.tramiteDao.countTramiteSolicitudByFiltroTramite(filtroBusquedaTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#contarTramiteSolicitudPorFiltroTramite: " +
                ex.getMensaje());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * @see ITramiteLocal#getSolicitanteSolicitudAleatorio
     * @author fredy.wilches
     * @version 2.0
     * @return
     */
    @Override
    public List<SolicitanteSolicitud> getSolicitanteSolicitudAleatorio() {
        return this.solicitanteSolicitudDao.getSolicitanteSolicitudAleatorio();
    }

    /**
     * @author franz.gamba
     * @see co.gov.igac.snc.fachadas.ITramiteLocal#obtenerSaldosDeConservacion(java.util.List)
     */
    @Override
    public List<Tramite> obtenerSaldosDeConservacion(List<Long> tramiteIds) {

        return this.tramiteDao.getTramitesSaldosDeConservacion(tramiteIds);
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * @see ITramiteLocal#getComisionParaTests
     * @author fredy.wilches
     * @version 2.0
     * @return
     */
    @Override
    public Comision getComisionParaTests() {
        return this.comisionDao.getComisionParaTests();
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ITramiteLocal#buscarSolicitudesPaginadasPorFiltro(VSolicitudPredio, int...)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public List<VSolicitudPredio> buscarSolicitudesPaginadasPorFiltro(
        VSolicitudPredio datosFiltroSolicitud, String sortField, String sortOrder,
        Map<String, String> filters, final int... rowStartIdxAndCount) {

        try {
            return this.vSolicitudPredioDao.findSolicitudesPaginadasByFiltro(
                datosFiltroSolicitud, sortField, sortOrder, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#buscarSolicitudesPaginadasPorFiltro: " +
                ex.getMensaje());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#contarSolicitudesPaginadasPorFiltro(VSolicitudPredio)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Integer contarSolicitudesPaginadasPorFiltro(VSolicitudPredio datosFiltroSolicitud) {

        try {
            return this.vSolicitudPredioDao
                .countSolicitudesPaginadasByFiltro(datosFiltroSolicitud);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#contarSolicitudesPaginadasPorFiltro: " +
                ex.getMensaje());
        }
        return null;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#obtenerSolicitantesParaNotificar(long)
     * @author juan.agudelo
     * @version 2.0
     */
    /*
     * @modified pedro.garcia 19-07-2012 manejo de excepciones
     */
    @Implement
    @Override
    public List<Solicitante> obtenerSolicitantesParaNotificarPorTramiteId(long tramiteId) {

        List<Solicitante> answer = null;

        try {
            answer = this.tramiteDao.getSolicitantesParaNotificarByTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#obtenerSolicitantesParaNotificarPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#obtenerDuracionComision2(java.lang.Long)
     */
    @Implement
    @Override
    public Double obtenerDuracionComision2(Long idComision) {

        LOGGER.debug("on TramiteBean#obtenerDuracionComision2 ...");

        Double answer = null;
        try {
            answer = this.comisionTramiteDao.getComisionDuration2(idComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Excepción obteniendo la duración de una comisión: " + ex.getMessage());
        }

        return answer;
    }

    // ----------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see ITramite#actualizarTramiteDocumentacion(List<TramiteDocumentacion>)
     */
    @Override
    public TramiteDocumentacion guardarTramiteDocumentacion(
        TramiteDocumentacion tramiteDocumentacion) {
        TramiteDocumentacion answer = this.tramiteDocumentacionDao.update(tramiteDocumentacion);
        answer.setTipoDocumento(this.tipoDocumentoDao.findById(answer.getTipoDocumento().getId()));
        return answer;
    }

    // ----------------------------------------------- //
    /**
     * @author david.cifuentes
     * @see ITramite#buscarSolicitudConSolicitantesSolicitudPorId(Long)
     */
    @Override
    public Solicitud buscarSolicitudConSolicitantesSolicitudPorId(
        Long solicitudId) {
        return solicitudDao.buscarSolicitudConSolicitantesSolicitudPorId(solicitudId, true);
    }

    // ----------------------------------------------- //
    /**
     * @author felipe.cadena
     * @see ITramite#buscarSolicitudConSolicitantesSolicitudPorIdNoTramites(Long)
     */
    @Override
    public Solicitud buscarSolicitudConSolicitantesSolicitudPorIdNoTramites(
        Long solicitudId) {
        return solicitudDao.buscarSolicitudConSolicitantesSolicitudPorId(solicitudId, false);
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#guardarDocumentoSinPredio(UsuarioDTO, Documento)
     * @author david.cifuentes
     */
    @Implement
    @Override
    public Documento guardarDocumentoSinPredio(UsuarioDTO usuario, Documento documento) {
        LOGGER.debug("on TramiteBean#guardarDocumentoSinPredio ...");

        Documento answer = null;
        try {
            answer = this.documentoDao.almacenarDocumentoGestorDocumentalSinPredio(usuario,
                documento);
        } catch (ExcepcionSNC e) {
            LOGGER.error("... error: " + e.getMensaje());
            throw (e);
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#buscarTramitesPorNumeroDeResolucion (String resolucion)
     */
    @Override
    public List<Tramite> buscarTramitesPorNumeroDeResolucion(String resolucion) {
        List<Tramite> answer = null;
        answer = this.tramiteDao.buscarTramitesPorNumeroDeResolucion(resolucion);
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramite#obtenerTramiteDocumentosPorTramiteId(Long)
     */
    @Override
    public List<TramiteDocumento> obtenerTramiteDocumentosPorTramiteId(Long tramiteId) {

        List<TramiteDocumento> answer = null;

        try {
            answer = this.tramiteDocumentoDao.findTramiteDocumentosByTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en obteniendo los documentos de un trámite: " + ex.getMensaje());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#guardarActualizarComisionEstado(ComisionEstado)
     * @author david.cifuentes
     */
    @Override
    public ComisionEstado guardarActualizarComisionEstado(ComisionEstado comisionEstado) {

        try {
            comisionEstado = this.comisionEstadoDao.update(comisionEstado);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_INSERCION.getExcepcion(
                LOGGER, e, "ComisionEstado");
        }
        return comisionEstado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     * @see ITramite#actualizarTramiteDocumentacionConAlfresco(List<
     *      TramiteDocumentacion>, Solicitud solicitud, UsuarioDTO usuario)
     */
    /*
     * @modified juanfelipe.garcia :: 29-07-2013 :: se adiciona validación para documento soporte
     * con ruta vacía
     */
 /*
     * @modified leidy.gonzalez :: 20-01-2015 :: #7485 :: se actualiza tramiteDocumentacion despues
     * de agregar id reporsitorio
     */
    @Override
    public List<TramiteDocumentacion> actualizarTramiteDocumentacionConAlfresco(
        List<TramiteDocumentacion> tramiteDocumentacion,
        Solicitud solicitud, UsuarioDTO usuario) {

        List<TramiteDocumentacion> answer = null;
        String fileSeparator = System.getProperty("file.separator");

        try {

            answer = this.tramiteDocumentacionDao
                .updateMultipleCompleto(tramiteDocumentacion);

            Documento doc = new Documento();
            for (TramiteDocumentacion td : answer) {

                String nombreDepartamento = "";
                String nombreMunicipio = "";
                String numeroPredial = "";

                if (td.getDepartamento() != null) {
                    nombreDepartamento = td.getDepartamento().getNombre();
                } else {
                    nombreDepartamento = td.getTramite().getDepartamento()
                        .getNombre();
                }

                if (td.getMunicipio() != null) {
                    nombreMunicipio = td.getMunicipio().getNombre();
                } else {
                    nombreMunicipio = td.getTramite().getMunicipio()
                        .getNombre();
                }

                if (td.getTramite().getPredio() != null) {
                    numeroPredial = td.getTramite().getPredio()
                        .getNumeroPredial();
                }

                DocumentoTramiteDTO documentoTramiteDTO = new DocumentoTramiteDTO(
                    FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                    td.getDocumentoSoporte().getArchivo(), td
                    .getDocumentoSoporte().getTipoDocumento()
                    .getNombre(), td.getTramite()
                        .getFechaRadicacion(), nombreDepartamento,
                    nombreMunicipio, numeroPredial, td.getTramite()
                        .getTipoTramite(), td.getTramite().getId());

                // Cuando es una solicitud de via gubernativa o es un trámite de
                // mutación de quinta, el campo número predial del
                // documentoTramiteDTO puede ir nulo.
                if (!td.getDocumentoSoporte().getArchivo().equals(" ")) {
                    if (!solicitud.isViaGubernativa() &&
                        !answer.get(0).getTramite().isQuinta()) {

                        doc = this.documentoDao.guardarDocumentoGestorDocumental(usuario,
                            documentoTramiteDTO, td.getDocumentoSoporte());
                    } else {
                        doc = this.documentoDao
                            .guardarDocumentoGestorDocumentalSinNumeroPredial(usuario,
                                documentoTramiteDTO,
                                td.getDocumentoSoporte());
                    }

                    // Asociar el documento guardado en Alfesco al
                    // tramiteDocumentacion
                    if (doc != null && doc.getId() != null) {
                        td.setDocumentoSoporte(doc);
                        td.setIdRepositorioDocumentos(doc.getIdRepositorioDocumentos());

                    }
                }

            }
            answer = this.tramiteDocumentacionDao
                .updateMultipleCompleto(answer);

        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new ExcepcionSNC("", ESeveridadExcepcionSNC.ERROR,
                ex.getMessage(), ex);
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author lorena.salamanca
     * @see ITramite#getNumerosPredialesByTramiteId(List<Long>)
     */
    @Override
    public List<String> getNumerosPredialesByTramiteId(List<Long> tramiteIds) {
        try {
            return this.tramitePredioEnglobeDao.getNumerosPredialesByTramiteId(tramiteIds);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#getNumerosPredialesByTramiteId: " + ex.getMensaje());
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public List<TramitePruebaEntidad> buscarTramitePruebaEntidadsconNumeroDocumentos(
        Long tramiteId) {
        List<TramitePruebaEntidad> answer =
            this.tramitePruebaEntidadDao.findTramitePruebaEntidadByTramiteId(tramiteId);

        if (answer != null && answer.size() > 0) {
            List<TramitePruebaEntidad> temp = new LinkedList<TramitePruebaEntidad>();
            for (TramitePruebaEntidad tpe : answer) {
                if (!tpe.getTipoSolicitante().equals(
                    ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION.getCodigo())) {
                    temp.add(tpe);
                }
            }
            answer = temp;
            for (TramitePruebaEntidad tpe : answer) {
                String identificacion = tpe.getNumeroIdentificacion();
                tpe.setDocumentosPruebas(
                    this.tramiteDocumentoDao.countDocumentosPruebasPorEntidad(
                        tramiteId, identificacion));
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerDocumentosPruebasDeEntidad(java.lang.Long,
     * java.lang.String)
     */
    @Override
    public List<TramiteDocumento> obtenerDocumentosPruebasDeEntidad(
        Long tramRiteId, String identificacion) {
        return this.tramiteDocumentoDao.getDocumentosPruebasDeEntidad(tramRiteId, identificacion);
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#guardarYactualizarDocumento(co.gov.igac.snc.persistence.entity.generales.Documento)
     */
    @Override
    public Documento guardarYactualizarDocumento(Documento documento) {

        LOGGER.debug(("En TramiteBean#guardarYactualizarDocumento "));

        Documento answer;

        answer = null;
        try {
            answer = this.documentoDao.update(documento);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#guardarYactualizarDocumento: " + ex.getMessage());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#eliminarTramiteDocumentoConDocumentoAsociado(TramiteDocumento)
     * @modified franz.gamba :: se modifica la eliminacion del documento para qeu lo borre tambn de
     * alfresco
     */
    @Override
    public boolean eliminarTramiteDocumentoConDocumentoAsociado(TramiteDocumento tramiteDocumento) {
        boolean answer = false;
        try {
            this.tramiteDocumentoDao.delete(tramiteDocumento);
            this.documentoDao.deleteDocumentoAlsoGestorDocumental(tramiteDocumento.getDocumento());

            answer = true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.
                getExcepcion(LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @author felipe.cadena
     * @see ITramiteLocal#actualizarTramiteDocumentacionEnAlfresco
     */
    @Override
    public TramiteDocumentacion actualizarTramiteDocumentacionEnAlfresco(
        TramiteDocumentacion tramiteDocumentacion, UsuarioDTO usuario) {
        Documento docActualizar = tramiteDocumentacion.getDocumentoSoporte();

        File f = new File(FileUtils.getTempDirectoryPath(), docActualizar.getArchivo());
        boolean isUploadOk = false;
        DocumentoTramiteDTO dtDto = new DocumentoTramiteDTO(
            f.getAbsolutePath(),
            docActualizar.getTipoDocumento().getNombre(),
            tramiteDocumentacion.getTramite().getFechaRadicacion(),
            tramiteDocumentacion.getTramite().getDepartamento().getNombre(),
            tramiteDocumentacion.getTramite().getMunicipio().getNombre(),
            tramiteDocumentacion.getTramite().getPredio().getNumeroPredial(),
            tramiteDocumentacion.getTramite().getTipoTramite(),
            tramiteDocumentacion.getTramite().getId());

        try {
            docActualizar = this.documentoDao.guardarDocumentoGestorDocumental(usuario, dtDto,
                docActualizar);
            isUploadOk = true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_40001.getExcepcion(LOGGER, e,
                SncBusinessServiceExceptions.EXCEPCION_40001.getMessage(),
                "TramiteBean#actualizarTramiteDocumentacionEnAlfresco");
        }
        try {
            if (isUploadOk) {
                tramiteDocumentacion.setDocumentoSoporte(docActualizar);
                this.tramiteDocumentacionDao.update(tramiteDocumentacion);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(LOGGER, ex, ex.
                getMessage(),
                "TramiteBean#actualizarTramiteDocumentacionEnAlfresco");
        }
        return tramiteDocumentacion;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#eliminarTramiteDocumentacionConDocumentoAsociado(co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion)
     */
    @Override
    public boolean eliminarTramiteDocumentacionConDocumentoAsociado(
        TramiteDocumentacion tramiteDocumentacion) {
        boolean answer = false;
        try {
            this.tramiteDocumentacionDao.delete(tramiteDocumentacion);
            this.documentoDao.deleteDocumentoAlsoGestorDocumental(tramiteDocumentacion.
                getDocumentoSoporte());

            answer = true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.
                getExcepcion(LOGGER, e, e.getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @author javier.aponte
     * @see ITramiteLocal#verVisorGISPorTipoTramite
     */
    @Override
    public boolean verVisorGISPorTipoTramite(String tipoTramite, String claseMutacion,
        String subtipoTramite) {

        boolean answer = true;

//REVIEW :: javier.aponte :: está comparando una enumeración con un string. ¿se va a cumplir esa condición? :: pedro.garcia     
        if (ETramiteTipoTramite.MUTACION.equals(tipoTramite) && EMutacionClase.PRIMERA.getCodigo().
            equals(claseMutacion)) {
            answer = false;
        }

        return answer;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     *
     * @see co.gov.igac.snc.fachadas.ITramiteLocal# obtenerTramiteIdsParaNotificarPorFiltro
     * (co.gov.igac.snc.util.FiltroDatosConsultaTramite, int[])
     */
    /**
     * @modified by leidy.gonzalez::08-08-2018::#70681::Reclamar actividad por parte del usuario autenticado
     */ 
    @Override
    public List<Long> obtenerTramiteIdsParaNotificarPorFiltro(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String nombreActividadSelected, List<Long> iDsTramitesProc,int... rowStartIdxAndCount) {
        List<Long> answer = null;
        try {
            answer = this.tramiteDao.findTramiteIdsDeTramitesParaNotificar(
                filtroBusquedaTramite, nombreActividadSelected,iDsTramitesProc,
                rowStartIdxAndCount);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex, "Tramite");
        }
        return answer;
    }
    //---------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerTramitesParaNotificarPorFiltro(java.util.List,
     * java.lang.String, co.gov.igac.snc.util.FiltroDatosConsultaTramite, java.lang.String,
     * java.util.Map, int[])
     */
//TODO :: franz.gamba :: hacer manejo de excepciones :: pedro.garcia
    @Override
    public List<Tramite> obtenerTramitesParaNotificarPorFiltro(
        List<Long> tramiteIds, String sortField,
        FiltroDatosConsultaTramite filtroBusquedaTramite, String sortOrder,
        Map<String, String> filters, int... contadoresDesdeYCuantos) {

        if (tramiteIds.size() > 0) {
            // Se deben excluir los trámites que tengan interpuesta y activa una
            // vía gubernativa.
            tramiteIds = this.excluirIdsTramitesConViaGubernativaInterpuesta(tramiteIds);

            return this.tramiteDao.findTramitesParaNotificarByIds(tramiteIds,
                sortField, filtroBusquedaTramite, sortOrder, filters,
                contadoresDesdeYCuantos);
        } else {
            return new LinkedList<Tramite>();
        }
    }
    //---------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerIdsDeTramitesAsociadosPorTramiteId(java.lang.Long)
     */
    @Override
    public List<Long> obtenerIdsDeTramitesAsociadosPorTramiteId(Long tramiteId) {

        return this.tramiteDao.getIdsDeTramitesAsociadosByTramiteId(tramiteId);
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerTramitesParaAprobarPorTramiteIds(java.util.List)
     * @modified juan.cruz asignación variable validación
     */
    @Override
    public List<Tramite> obtenerTramitesParaAprobarPorTramiteIds(List<Long> tramiteIds) {
        //TODO :: franz.gamba :: 19-07-2012 :: hacer manejo de excepciones  :: pedro.garcia
        List<Long> tramitesID = new ArrayList<Long>();
        List<Tramite> tramitesParaAprobar = this.tramiteDao.findTramitesByIdsParaAprobar(tramiteIds);

        for (Tramite unTramite : tramitesParaAprobar) {
            tramitesID.add(unTramite.getId());
        }
        HashSet<Long> tramitesIdValidados = this.tramiteEstadoDao.
            validarSiEstadoFuePorCancelarTramite(tramitesID);

        //llenar la variable porCancelar que valida si el tramite ha sido seleccionado para cancelar.
        for (Tramite unTramite : tramitesParaAprobar) {
            unTramite.setPorCancelar(tramitesIdValidados.contains(unTramite.getId()));
        }

        return tramitesParaAprobar;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#guardarDocumentosRegistroNotificacion(java.lang.Long,
     * co.gov.igac.snc.persistence.entity.generales.Documento,
     * co.gov.igac.snc.persistence.entity.generales.Documento,
     * co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento,
     * co.gov.igac.generales.dto.UsuarioDTO)
     *
     * 1. se busca el trámite para obtener los datos de éste que son necesarios para guardar los
     * Documento en el gestor documental 2. se guardan los Documento en el gestor documental 3.
     * guardar los registros de TramiteDocumentacion para los documentos de notificación y
     * autorización 4. relacionar los id de Documento con los que quedaron los documentos de
     * notificación y autorización con el TramiteDocumento y actualizarlo
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean guardarDocumentosRegistroNotificacion(Long idTramite,
        Documento documentoNotificacion, Documento documentoAutorizacion,
        TramiteDocumento tramiteDocumentoNotificacion, UsuarioDTO usuario) {

        LOGGER.debug("TramiteBean#guardarDocumentosRegistroNotificacion");

        String fileSeparator = System.getProperty("file.separator");

        Documento documentoAutTemp = null;
        Documento documentoNotTemp = null;
        DocumentoTramiteDTO documentoTramiteDTO;
        boolean error;
        Tramite tramite;
        TipoDocumento tipoDocumento;
        Departamento departamento;
        Municipio municipio;
        Date fechaLog;
        TramiteDocumentacion trDoccion;

        error = false;
        fechaLog = new Date(System.currentTimeMillis());

        // 1.
        try {
            tramite = this.tramiteDao.findById(idTramite);
        } catch (Exception ex) {
            error = true;
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, ex, "Tramite",
                    idTramite.toString());
        }

        try {
            Predio predioTmp;
            if (!tramite.isQuinta()) {
                predioTmp = this.tramiteDao.getPredioDptoMuniByTramiteId(tramite.getId());
                departamento = predioTmp.getDepartamento();
                municipio = predioTmp.getMunicipio();
            } else {
                PPredio ppredioTmp;
                predioTmp = new Predio();
                
                ppredioTmp = this.pPredioDao.getPPrediosByTramiteId(tramite.getId()).get(0);
                departamento = this.departamentoDao.findById(
                    tramite.getNumeroPredial().substring(0, 2));
                municipio = this.municipioDao.findById(tramite.getNumeroPredial().substring(0, 5));
                predioTmp.setDepartamento(departamento);
                predioTmp.setMunicipio(municipio);
                predioTmp.setNumeroPredial(ppredioTmp.getNumeroPredial());
            }

            // 2.
            //D: Guardar en el gestor documental y en BD los documentos de notificación y autorización
            if (documentoAutorizacion != null && documentoAutorizacion.getId() == null) {
                documentoTramiteDTO = new DocumentoTramiteDTO(
                    FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                    documentoAutorizacion.getArchivo(),
                    documentoAutorizacion.getTipoDocumento().getNombre(),
                    tramite.getFechaRadicacion(),
                    departamento.getNombre(), municipio.getNombre(),
                    predioTmp.getNumeroPredial(),
                    tramite.getTipoTramiteCadenaCompleto(), tramite.getId());

                documentoAutTemp = new Documento();

                try {
                    documentoAutTemp = this.documentoDao.guardarDocumentoGestorDocumental(
                        usuario, documentoTramiteDTO, documentoAutorizacion);
                } catch (Exception ex) {
                    error = true;
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.
                        getExcepcion(LOGGER, ex,
                            "TramiteBean#guardarDocumentosRegistroNotificacion");
                }
            }

            if (documentoNotificacion != null && documentoNotificacion.getId() == null) {
                documentoTramiteDTO = new DocumentoTramiteDTO(
                    FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
                    documentoNotificacion.getArchivo(),
                    documentoNotificacion.getTipoDocumento().getNombre(),
                    tramite.getFechaRadicacion(),
                    departamento.getNombre(), municipio.getNombre(),
                    predioTmp.getNumeroPredial(),
                    tramite.getTipoTramiteCadenaCompleto(), tramite.getId());

                documentoNotTemp = new Documento();

                try {
                    if (!tramite.isQuinta()) {
                        documentoNotTemp = this.documentoDao
                            .guardarDocumentoGestorDocumental(usuario,
                                documentoTramiteDTO,
                                documentoNotificacion);
                    } else {
                        documentoNotTemp = this.documentoDao.
                            guardarDocumentoGestorDocumentalSinNumeroPredial(
                                usuario, documentoTramiteDTO, documentoNotificacion);
                    }
                } catch (Exception ex) {
                    error = true;
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_GESTORDOCUMENTAL.
                        getExcepcion(LOGGER, ex,
                            "TramiteBean#guardarDocumentosRegistroNotificacion");
                }
            }

            // 3.
            if (documentoNotTemp != null) {
                trDoccion = new TramiteDocumentacion();
                trDoccion.setTramite(tramite);
                trDoccion.setAportado(ESiNo.SI.getCodigo());
                trDoccion.setFechaAporte(fechaLog);

                tipoDocumento = this.tipoDocumentoDao.findById(
                    ETipoDocumento.DOCUMENTO_DE_NOTIFICACION.getId());
                trDoccion.setTipoDocumento(tipoDocumento);

                trDoccion.setFechaAporte(documentoNotTemp.getFechaDocumento());
                trDoccion.setDocumentoSoporte(documentoNotTemp);
                trDoccion.setCantidadFolios(0);
                trDoccion.setAdicional(ESiNo.NO.getCodigo());
                trDoccion.setDepartamento(departamento);
                trDoccion.setMunicipio(municipio);
                trDoccion.setIdRepositorioDocumentos(documentoNotTemp.getIdRepositorioDocumentos());
                trDoccion.setUsuarioLog(usuario.getLogin());
                trDoccion.setFechaLog(fechaLog);

                try {
                    this.tramiteDocumentacionDao.persist(trDoccion);
                } catch (Exception ex) {
                    error = true;
                    throw new Exception("error insertando registro del docuemnto de notificación " +
                        "en la tabla TramiteDocumentacion: " + ex.getMessage());
                }
            }

            if (documentoAutTemp != null) {
                trDoccion = new TramiteDocumentacion();
                trDoccion.setTramite(tramite);
                trDoccion.setAportado(ESiNo.SI.getCodigo());
                trDoccion.setFechaAporte(fechaLog);

                tipoDocumento = this.tipoDocumentoDao.findById(
                    ETipoDocumento.DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION.getId());
                trDoccion.setTipoDocumento(tipoDocumento);

                if (documentoNotTemp.getFechaDocumento() != null) {
                    trDoccion.setFechaAporte(documentoNotTemp.getFechaDocumento());
                }
                trDoccion.setDocumentoSoporte(documentoAutTemp);
                trDoccion.setCantidadFolios(0);
                trDoccion.setAdicional(ESiNo.NO.getCodigo());
                trDoccion.setDepartamento(departamento);
                trDoccion.setMunicipio(municipio);
                trDoccion.setIdRepositorioDocumentos(documentoNotTemp.getIdRepositorioDocumentos());
                trDoccion.setUsuarioLog(usuario.getLogin());
                trDoccion.setFechaLog(fechaLog);

                try {
                    this.tramiteDocumentacionDao.persist(trDoccion);
                } catch (Exception ex) {
                    error = true;
                    throw new Exception("error insertando registro del documento de autorización " +
                        " de notificación en la tabla TramiteDocumentacion: " + ex.getMessage());
                }
            }

            // 4.
            //D: relacionar el Documento de autorización al TramiteDocumento del documento de
            //   notificación
            if (documentoAutorizacion != null && documentoAutTemp != null &&
                documentoAutTemp.getId() != null) {
                tramiteDocumentoNotificacion.setAutorizacionDocumentoId(documentoAutTemp.getId());
            }

            //D: relacionar el Documento de notificación guardado con el TramiteDocumento del
            //   documento de notificación
            if (documentoNotificacion != null && documentoNotTemp != null &&
                documentoNotTemp.getId() != null) {
                tramiteDocumentoNotificacion.setNotificacionDocumentoId(documentoNotTemp.getId());
            }

            tramiteDocumentoNotificacion.setUsuarioLog(usuario.getLogin());
            tramiteDocumentoNotificacion.setFechaLog(fechaLog);

            //D: actualizar el TramiteDocumento
            try {
                this.tramiteDocumentoDao.update(tramiteDocumentoNotificacion);
            } catch (Exception ex) {
                error = true;
                throw new Exception("error actualizando registro del TramiteDocumento " +
                    " relacionado con en registro de notificación: " + ex.getMessage());
            }

        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return !error;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#obtenerTramiteDocumentoParaRegistroNotificacion(Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public TramiteDocumento obtenerTramiteDocumentoParaRegistroNotificacion(
        Long idTramite, String docPersonaNotificacion) {

        LOGGER.debug("on TramiteBean#obtenerTramiteDocumentoParaRegistroNotificacion ");

        TramiteDocumento answer = null;

        try {
            answer = this.tramiteDocumentoDao.getForRegistroNotificacion(
                idTramite, docPersonaNotificacion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#obtenerTramiteDocumentoParaRegistroNotificacion: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     *
     * @author lorena.salamanca
     * @param tramiteId
     * @return
     */
    @Override
    public List<Tramite> getTramiteTramitesbyTramiteId(Long tramiteId) {
//TODO :: lorena.salamanca :: hacer manejo de excepciones. seguir estándar de documentación
        return this.tramiteDao.getTramiteTramitesbyTramiteId(tramiteId);
    }

    @Override
    public EReporteServiceSNC obtenerUrlReporteResoluciones(Long tramiteId) {

        EReporteServiceSNC answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_PRIMERA;

        Tramite tramiteTemp = this.tramiteDao.findOnlyTramiteByIdTramite(tramiteId);

        if (tramiteTemp.isTipoTramiteViaGubernativaModificado() || tramiteTemp.
            isTipoTramiteViaGubModSubsidioApelacion()) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_VIA_GUBERNATIVA;
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo())) {
            if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.PRIMERA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_PRIMERA;
            }
            if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.SEGUNDA.getCodigo())) {
                if (tramiteTemp.isEnglobeVirtual()) {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_SEGUNDA_ENGLOBE_PH;
                } else if (ETramiteRadicacionEspecial.POR_ETAPAS.getCodigo().equals(tramiteTemp.
                    getRadicacionEspecial())) {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_SEGUNDA_MASIVA;
                } else {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_SEGUNDA;
                }
            }
            if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.TERCERA.getCodigo())) {
                if (ETramiteRadicacionEspecial.TERCERA_MASIVA.getCodigo().equals(tramiteTemp.
                    getRadicacionEspecial())) {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_TERCERA_MASIVA;
                } else {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_TERCERA;
                }
            }
            if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_CUARTA;
            }
            if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo())) {
                if (ETramiteRadicacionEspecial.QUINTA_MASIVO.getCodigo().equals(tramiteTemp.
                    getRadicacionEspecial())) {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_QUINTA_MASIVA;
                } else {
                    answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_QUINTA;
                }
            }
        } else if (tramiteTemp.getTipoTramite().equals(
            ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_MODIFICACION_INFORMACION_CATASTRAL;
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.
            getCodigo())) {
            if (ETramiteRadicacionEspecial.CANCELACION_MASIVA.getCodigo().equals(tramiteTemp.
                getRadicacionEspecial())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_CANCELACION_MASIVA;
            } else {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_CANCELACION;
            }
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.COMPLEMENTACION.
            getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_COMPLEMENTACION;
        } else if (tramiteTemp.getTipoTramite().
            equals(ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_RECTIFICACION;
            tramiteTemp.setTramiteRectificacions(this.findTramiteRectificacionesByTramiteId(
                tramiteTemp.getId()));
            if (tramiteTemp.isRectificacionCancelacionDobleInscripcion()) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_CANCELACION;
            }
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.
            getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_REVISION_AVALUO;
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.
            getCodigo()) ||
            tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.RECURSO_DE_APELACION.
                getCodigo()) ||
            tramiteTemp.getTipoTramite().equals(
                ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getCodigo()) ||
            tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.RECURSO_DE_QUEJA.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_VIA_GUBERNATIVA;
        }
        return answer;
    }

    /**
     * @see ITramiteLocal#obtenerUrlSubreporteResolucionesResuelve
     * @author javier.aponte
     */
    @Override
    public String obtenerUrlSubreporteResolucionesResuelve(Long tramiteId) {

        String answer = "";

        Tramite tramiteTemp = this.tramiteDao.findOnlyTramiteByIdTramite(tramiteId);

        if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo())) {

            if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.PRIMERA.getCodigo())) {
                answer = ESubreporteResoluciones.RESOLUCION_MUTACION_PRIMERA.getUrlSubreporte();
            } else if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.SEGUNDA.getCodigo())) {
                answer = ESubreporteResoluciones.RESOLUCION_MUTACION_SEGUNDA.getUrlSubreporte();
            } else if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.TERCERA.getCodigo())) {
                answer = ESubreporteResoluciones.RESOLUCION_MUTACION_TERCERA.getUrlSubreporte();
                if (ETramiteRadicacionEspecial.TERCERA_MASIVA.getCodigo().equals(tramiteTemp.
                    getRadicacionEspecial())) {
                    answer = ESubreporteResoluciones.RESOLUCION_MUTACION_TERCERA_MASIVA.
                        getUrlSubreporte();
                }
            } else if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo())) {
                answer = ESubreporteResoluciones.RESOLUCION_MUTACION_CUARTA.getUrlSubreporte();
            } else if (tramiteTemp.getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo())) {
                answer = ESubreporteResoluciones.RESOLUCION_MUTACION_QUINTA.getUrlSubreporte();
            }

        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.
            getCodigo())) {
            answer = ESubreporteResoluciones.RESOLUCION_CANCELACION.getUrlSubreporte();
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.COMPLEMENTACION.
            getCodigo())) {
            answer = ESubreporteResoluciones.RESOLUCION_COMPLEMENTACION.getUrlSubreporte();
        } else if (tramiteTemp.getTipoTramite().
            equals(ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
            answer = ESubreporteResoluciones.RESOLUCION_RECTIFICACION.getUrlSubreporte();
            tramiteTemp.setTramiteRectificacions(this.findTramiteRectificacionesByTramiteId(
                tramiteTemp.getId()));
            if (tramiteTemp.isRectificacionCancelacionDobleInscripcion()) {
                answer = ESubreporteResoluciones.RESOLUCION_CANCELACION.getUrlSubreporte();
            } else if (ETramiteRadicacionEspecial.RECTIFICACION_MATRIZ.getCodigo().equals(
                tramiteTemp.getRadicacionEspecial())) {
                answer = ESubreporteResoluciones.RESOLUCION_RECTIFICACION_MATRIZ.getUrlSubreporte();
            }
        } else if (tramiteTemp.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.
            getCodigo())) {
            answer = ESubreporteResoluciones.RESOLUCION_REVISION_AVALUO.getUrlSubreporte();
        } else if (tramiteTemp.getTipoTramite().equals(
            ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL.getCodigo())) {
            answer = ESubreporteResoluciones.RESOLUCION_REVISION_AVALUO.getUrlSubreporte();
        }
        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#removerTramite(co.gov.igac.snc.persistence.entity.tramite.Tramite)
     */
    @Override
    public boolean removerTramite(Tramite tramite) {
        try {
            this.tramiteDao.delete(tramite);
            return true;
        } catch (Exception e) {
            LOGGER.
                error("Ocurrió un error al borrar el trámite en base de datos: " + e.getMessage());
            return false;
        }
    }

    @Override
    public void enviarTramitesAsociadosAElaborarInforme(String activityId, Tramite tramite,
        UsuarioDTO usuario) {
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuarios = new LinkedList<UsuarioDTO>();

        if (tramite.getFuncionarioEjecutor() != null) {
            usuarios.add(this.generalesService.getCacheUsuario(tramite.getFuncionarioEjecutor()));
        } else {
            usuarios.add(usuario);
        }
        solicitudCatastral.setTransicion(
            ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO);

        solicitudCatastral.setUsuarios(usuarios);

        this.procesosService.avanzarActividad(usuario, activityId,
            solicitudCatastral);

    }

    @Override
    public void enviarTramitesAsociadosAAprobar(String activityId, Tramite tramite,
        UsuarioDTO usuario) {
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuarios = this.buscarFuncionariosPorRolYTerritorial(usuario.
            getDescripcionEstructuraOrganizacional(),
            ERol.COORDINADOR);

        solicitudCatastral.
            setTransicion(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES);

        solicitudCatastral.setUsuarios(usuarios);

        this.procesosService.avanzarActividad(usuario, activityId,
            solicitudCatastral);

    }

    /**
     * @see ITramiteLocal#buscarSolicitantesSolicitudBySolicitudIdRelacion(Long, String)
     * @author christian.rodriguez
     */
    @Override
    public List<SolicitanteSolicitud> buscarSolicitantesSolicitudBySolicitudIdRelacion(
        Long idSolicitud, String solicitanteSolicitudRelacion) {

        LOGGER.debug("en TramiteBean#buscarSolicitantesSolicitudBySolicitudIdRelacion");

        List<SolicitanteSolicitud> answer = null;
        try {
            answer = this.solicitanteSolicitudDao
                .getSolicitanteSolicitudByIdSolicitudRelacion(idSolicitud,
                    solicitanteSolicitudRelacion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error buscando los solicitantes solicitud de la solicitud " +
                idSolicitud + " y estado " +
                solicitanteSolicitudRelacion + ": " + ex.getMensaje());
        }

        return answer;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#avanzarProcesoAModifInfoAlfanumerica(java.lang.Long)
     */
    @Override
    public boolean avanzarProcesoAModifInfoAlfanumerica(Long tramiteId, String actividadActual,
        UsuarioDTO unUsuario) {

        try {
            List<Actividad> actividad = this.procesosService.getActividadesPorIdObjetoNegocio(
                tramiteId);
            if (actividad != null) {
                Tramite tramite = this.buscarTramitePorId(tramiteId);

                List<UsuarioDTO> usuarios = new LinkedList<UsuarioDTO>();
                UsuarioDTO usuario;

                EstructuraOrganizacional estructuraOrganizacionalTramite = generalesService.
                    obtenerEstructuraOrganizacionalPorMunicipioCodigo(tramite.getMunicipio().
                        getCodigo());

                if (estructuraOrganizacionalTramite.getTipo().equals(
                    EEstructuraOrganizacional.DIRECCION_TERRITORIAL.getTipo()) ||
                    estructuraOrganizacionalTramite.getTipo().equals(
                        EEstructuraOrganizacional.DELEGACION.getTipo()) || 
                        estructuraOrganizacionalTramite.getTipo().equals(EEstructuraOrganizacional.HABILITADA.getTipo())) {
                    usuarios.addAll(this.buscarFuncionariosPorRolYTerritorial(
                        estructuraOrganizacionalTramite.getCodigo(), ERol.RESPONSABLE_SIG));
                } else if (estructuraOrganizacionalTramite.getTipo().equals(
                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL.getTipo())) {
                    usuarios.addAll(this.buscarFuncionariosPorRolYTerritorial(
                        estructuraOrganizacionalTramite.getCodigo(), ERol.RESPONSABLE_SIG));
                    if (usuarios.isEmpty()) {
                        usuarios.addAll(this.buscarFuncionariosPorRolYTerritorial(
                            estructuraOrganizacionalTramite.getEstructuraOrganizacionalCod(),
                            ERol.RESPONSABLE_SIG));
                    }
                } else {
                    return false;
                }

                SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setUsuarios(usuarios);

                if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA)) {
                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);
                    usuario = this.generalesService.
                        getCacheUsuario(tramite.getFuncionarioEjecutor());
                    usuarios.clear();
                    usuarios.add(usuario);

                } else if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA)) {
                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA);
                    usuario = this.generalesService.
                        getCacheUsuario(tramite.getFuncionarioEjecutor());
                    usuarios.clear();
                    usuarios.add(usuario);

                } else if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION)) {
                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION);
                    usuarios = this.buscarFuncionariosPorRolYTerritorial(unUsuario.
                        getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_SIG);

                } else if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR)) {
                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION);
                    usuarios = this.buscarFuncionariosPorRolYTerritorial(unUsuario.
                        getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_SIG);

                } else if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {
                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA);
                    usuarios = this.buscarFuncionariosPorRolYTerritorial(unUsuario.
                        getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_SIG);
                }

                solicitudCatastral.setUsuarios(usuarios);

                if (usuarios.get(0).getDescripcionUOC() != null && !usuarios.get(0).
                    getDescripcionUOC().isEmpty()) {
                    solicitudCatastral.setTerritorial(unUsuario.getDescripcionUOC());

                } else {
                    solicitudCatastral.setTerritorial(unUsuario.getDescripcionTerritorial());
                }

                this.procesosService.avanzarActividad(actividad.get(0).getId(), solicitudCatastral);

                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LOGGER.error("error en TramiteBean#avanzarProcesoAModifInfoAlfanumerica: " +
                e.getMessage(), e);
            return false;
        }
    }

    /**
     * @see ITramiteLocal#buscarSolicitudPredioById(Long)
     * @author christian.rodriguez
     */
    @Override
    public SolicitudPredio buscarSolicitudPredioById(Long solicitudPredioId) {
        LOGGER.debug("on TramiteBean#buscarSolicitudPredioById.... ");

        SolicitudPredio answer = null;

        try {
            answer = this.solicitudPredioDao.findById(solicitudPredioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarSolicitudPredioById: " + ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#buscarSolicitudesPredioBySolicitudId(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<SolicitudPredio> buscarSolicitudesPredioBySolicitudId(Long idSolicitud) {
        LOGGER.debug("on TramiteBean#buscarSolicitudesPredioBySolicitudId.... ");

        List<SolicitudPredio> answer = null;

        try {
            answer = this.solicitudPredioDao.buscarSolicitudPredioPorIdSolicitud(idSolicitud);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarSolicitudesPredioBySolicitudId: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#buscarSolicitudPredioSinAvaluoPorSolicitudId(Long)
     * @author christian.rodriguez
     */
    @Override
    public List<SolicitudPredio> buscarSolicitudPredioSinAvaluoPorSolicitudId(Long idSolicitud) {
        LOGGER.debug("on TramiteBean#buscarSolicitudPredioSinAvaluoPorSolicitudId.... ");

        List<SolicitudPredio> answer = null;

        try {
            answer = this.solicitudPredioDao
                .buscarSolicitudPredioSinAvaluoConContactoPorSolicitudId(idSolicitud);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarSolicitudPredioSinAvaluoPorSolicitudId: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#guardarActualizarSolicitudesPredios(List)
     * @author christian.rodriguez
     */
    @Override
    public List<SolicitudPredio> guardarActualizarSolicitudesPredios(
        List<SolicitudPredio> solicitudesPredio) {

        LOGGER.debug("on TramiteBean#guardarActualizarSolicitudesPredios.... ");

        List<SolicitudPredio> answer = new ArrayList<SolicitudPredio>();

        try {
            answer = solicitudPredioDao.updateMultipleReturn(solicitudesPredio);
        } catch (RuntimeException rtex) {
            LOGGER.error(rtex.getMessage(), rtex);
            throw new ExcepcionSNC("error en TramiteBean#guardarActualizarSolicitudesPredios",
                ESeveridadExcepcionSNC.ERROR, rtex.getMessage(), rtex);
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#guardarActualizarSolicitudPredio(SolicitudPredio)
     * @author christian.rodriguez
     */
    @Override
    public SolicitudPredio guardarActualizarSolicitudPredio(SolicitudPredio solicitudesPredio) {

        LOGGER.debug("on TramiteBean#guardarActualizarSolicitudPredio.... ");

        SolicitudPredio answer = new SolicitudPredio();

        try {
            answer = solicitudPredioDao.update(solicitudesPredio);
        } catch (RuntimeException rtex) {
            LOGGER.error(rtex.getMessage(), rtex);
            throw new ExcepcionSNC("error en TramiteBean#guardarActualizarSolicitudPredio",
                ESeveridadExcepcionSNC.ERROR, rtex.getMessage(), rtex);
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#eliminarSolicitudPredios(List)
     * @author christian.rodriguez
     */
    @Override
    public void eliminarSolicitudPredios(List<SolicitudPredio> solicitudPredios) {
        try {
            this.solicitudPredioDao.deleteMultiple(solicitudPredios);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#guardarYactualizarTramitePredioEnglobe(co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe)
     */
    @Override
    public TramitePredioEnglobe guardarYactualizarTramitePredioEnglobe(
        TramitePredioEnglobe tramitePredioEnglobe) {
        TramitePredioEnglobe answer = this.tramitePredioEnglobeDao.update(tramitePredioEnglobe);
        answer.setPredio(
            this.predioDao.getPredioByNumeroPredial(answer.getPredio().getNumeroPredial()));
        return answer;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#updateAreasDePrediosEdicionGeografica(java.util.List,
     * java.util.List)
     * @modified felipe.cadena :: 27/02/2014 :: Se cambia el manejo de zonas
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public boolean updateAreasDePrediosEdicionGeografica(List<String> predios,
        List<Double> areas, Tramite tramite, UsuarioDTO usuario, Boolean isTramiteMasivo) {
        boolean answer = false;
        boolean actualizacionPPredio = this.pPredioDao.actualizacionDeAreasEnPredios(predios, areas);
        boolean actualizacionPPredioZonas = true;
        String numeroPredialP;

        if (tramite.getPredio() != null) {
            numeroPredialP = tramite.getPredio().getNumeroPredial();
        } else {
            try {
                PPredio pp = this.pPredioDao.getPPredioByTramiteId(tramite.getId());
                numeroPredialP = pp.getNumeroPredial();
            } catch (Exception e) {
                LOGGER.error("Ocurrió un error al cargar el predio proyectado del trámite.");
                LOGGER.error(e.getMessage(), e);
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                    e.getMessage());
            }

        }

//		actualizacionPPredioZonas = this.pPredioDao.updatePPredioZonasPrediosInscritos(
//				tramite.getId(), numeroPredialP, usuario, tramite.isQuinta());
        //felipe.cadena :: 27/02/2014 :: Se cambia el manejo de zonas
        actualizacionPPredioZonas = actualizacionPPredioZonas(tramite, usuario, isTramiteMasivo);

        if (actualizacionPPredio && actualizacionPPredioZonas) {
            answer = true;
        }

        return answer;
    }

    /**
     * Actualiza las zonas asociadas a los predios de un tramite
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    /**
     * @modified juan.mendez 2014/04/03 Se modificó la firma del método de SigDao. Ya no requiere el
     * parámetro token.
     * @modified andres.eslava::13/07/2015:: Se modifica el proceso masivo de actualizaciond de
     * zonas refs #13126.
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public boolean actualizacionPPredioZonas(Tramite tramite, UsuarioDTO usuario,
        Boolean isTramiteMasivo) {

        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> zonasGeograficas;
        List<PPredio> prediosTramite;
        List<PPredio> prediosNuevos = new LinkedList<PPredio>();

        try {
            prediosTramite = this.pPredioDao.buscarPPrediosPanelUbicacionPredioPorTramiteId(tramite.
                getId());

            //solo se modifican las zonas de los predios que se modifican o se cancelan
            for (PPredio pPredio : prediosTramite) {
                LOGGER.debug("pPredio Numero Predial: " + pPredio.getFormattedNumeroPredial());
                if ((EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pPredio.
                    getCancelaInscribe()) ||
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pPredio.
                        getCancelaInscribe())) &&
                    !pPredio.isMejora() &&
                    !pPredio.isUnidadEnPH()) {

                    prediosNuevos.add(pPredio);
                }
                //se cancelan las zonas previas de los condominios
                if (tramite.isTipoInscripcionCondominio() && pPredio.isUnidadEnPH()) {
                    List<PPredioZona> zonasPredioPrevias = this.pPredioZonaDao.
                        buscarTodasZonasPorPPredioIdYEstadoP(pPredio.getId(),
                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    for (PPredioZona ppzp : zonasPredioPrevias) {
                        ppzp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                    }
                    this.pPredioZonaDao.updateMultiple(zonasPredioPrevias);
                }
            }

        } catch (ExcepcionSNC es) {
            LOGGER.error("Error consultar predios proyectados del tramite:" + es.getMessage(), es);
            return false;
        }

        try {
            //se obtiene la ruta de la copia geografica final
            String replica = this.documentoDao.obtenerIdRepositorioDocumentosDeReplicaOriginal(
                tramite.getId(), ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());
            if (!isTramiteMasivo) {
                LOGGER.debug("Tramite no masivo ---> obtener zonas");
                zonasGeograficas = SigDAOv2.getInstance().obtenerZonasHomogeneas(prediosNuevos,
                    replica, usuario);
                this.actualizarZonasPorPredio(usuario, zonasGeograficas, tramite);
            } else {
                LOGGER.debug("Tramite masivo ---> obtener zonas");
                SigDAOv2.getInstance().obtenerZonasAsync(productoCatastralJobDAO, prediosNuevos,
                    replica, usuario, tramite, tramite.getClaseMutacion());
            }
        } catch (ExcepcionSNC es) {
            LOGGER.error("Error al consultar zonas geograficas: " + es.getMessage(), es);
            return false;
        }
        return true;
    }

    /**
     * Metodo para actualizar las zonas de los predios de un tramite geografico
     *
     *
     * @author felipe.cadena
     * @param zonasPredio
     * @param usuario
     * @param numPredio
     */
    // @modified by javier.aponte se guarda el valor del área truncado a 3 cifras decimales refs#12022
    // @modified by andres.eslava se modifica los argumentos para procesar a partir de un diccionario de zonas por predio refs#13126.
    @Override
    public void actualizarZonasPorPredio(UsuarioDTO usuario,
        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> zonasGeograficas,
        Tramite tramite) {

        for (String numPredio : zonasGeograficas.keySet()) {
            List<ZonasFisicasGeoeconomicasVO> zonasPredio = zonasGeograficas.get(numPredio);
            List<PPredioZona> zonas = new LinkedList<PPredioZona>();
            Double areaTerreno;
            Calendar vigenciaZona;

            PPredio pp = this.conservacionService.getPPredioByNumeroPredial(numPredio);

            //se calcula la vigencia
            Calendar vigenciaTramite = Calendar.getInstance();
            vigenciaTramite.setTime(pp.getFechaInscripcionCatastral()); //fecha de inscripcion del tramite

            Calendar vigenciaActual = Calendar.getInstance(); // fecha actual
            //v1.1.8
            //Se deben cancelar las zonas de las vigencias a insertar que sean previas 
            this.cancelarPZonasPorPredio(pp.getId(), vigenciaActual, vigenciaTramite, usuario);

            if (vigenciaTramite.get(Calendar.YEAR) == vigenciaActual.get(Calendar.YEAR)) {
                vigenciaActual.set(Calendar.MONTH, 0);
                vigenciaActual.set(Calendar.DATE, 1);
                vigenciaZona = vigenciaActual;
            } else {
                //vigenciaTramite.add(Calendar.YEAR, 1); // Se aumenta en un año para obtener la vigencia
                vigenciaTramite.set(Calendar.MONTH, 0);
                vigenciaTramite.set(Calendar.DATE, 1);
                vigenciaZona = vigenciaTramite;
            }

            //Se deben cancelar las zonas existentes 
            this.cancelarPZonasPorPredio(pp.getId(), null, null, usuario);

            
            if (tramite.isEnglobeVirtual()) {
                this.registrarZonasConActualizacion(pp, usuario.getLogin(), zonasPredio);
                continue;
            }
            
           
            

            //Se inscriben las nuevas zonas del predio
            for (ZonasFisicasGeoeconomicasVO znVO : zonasPredio) {

                //felipe.cadena::09-01-2015::#11058:: El calculo del avaluo se hace directamente en la DB por medio de un proceimeinto posterior
//            valorTerrenoM2 = this.sncProcedimientoDao.obtenerValorUnidadTerreno(numPredio.substring(0, 7),
//                pp.getDestino(), znVO.getCodigoGeoeconomica());
                //area de terreno asociada a la zona
                areaTerreno = Utilidades.redondearNumeroADosCifrasDecimales(Double.valueOf(znVO.
                    getArea()));

                //valor avaluo
                //Long l = (Math.round(valorTerrenoM2 * areaTerreno));
                //valorTerrenoTotal = l.doubleValue();
                //integración version 1.1.6
                //felipe.cadena :: redmine #7637 :: Ajuste del valor de la unidad de terreno para predios rurales
                //se divide por 10.000 para predios rurales.
//            if(pp.getTipoAvaluo().equals(EAvaluoTipoAvaluo.RURAL.getCodigo())){
//                valorTerrenoTotal = valorTerrenoTotal/10000;
//            }
                PPredioZona pz = new PPredioZona();
                pz.setArea(areaTerreno);
                pz.setZonaFisica(znVO.getCodigoFisica());
                pz.setZonaGeoeconomica(znVO.getCodigoGeoeconomica());
                pz.setPPredio(pp);
                pz.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                pz.setValorM2Terreno(0d);
                pz.setVigencia(vigenciaZona.getTime());
                pz.setAvaluo(0d);
                pz.setFechaInscripcionCatastral(pp.getFechaInscripcionCatastral());
                pz.setFechaLog(new Date());
                pz.setUsuarioLog(usuario.getLogin());

                zonas.add(pz);
            }

            // persiste las nuevas zonas
            this.pPredioZonaDao.persistMultiple(zonas);

            //se deben insertar las zonas de la vigencia actual.
            if (vigenciaTramite.get(Calendar.YEAR) != vigenciaActual.get(Calendar.YEAR)) {
                //v1.1.8
                vigenciaActual.set(Calendar.MONTH, 0);
                vigenciaActual.set(Calendar.DATE, 1);
                List<PPredioZona> zonasVigenciaActual = new LinkedList<PPredioZona>();
                for (PPredioZona pPredioZona : zonas) {

                    PPredioZona pz = new PPredioZona();
                    pz.setArea(Utilidades.redondearNumeroADosCifrasDecimales(pPredioZona.getArea()));
                    pz.setZonaFisica(pPredioZona.getZonaFisica());
                    pz.setZonaGeoeconomica(pPredioZona.getZonaGeoeconomica());
                    pz.setPPredio(pPredioZona.getPPredio());
                    pz.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    pz.setValorM2Terreno(pPredioZona.getValorM2Terreno());
                    pz.setVigencia(vigenciaActual.getTime());
                    pz.setAvaluo(pPredioZona.getAvaluo());
                    pz.setFechaInscripcionCatastral(pPredioZona.getFechaInscripcionCatastral());
                    pz.setFechaLog(new Date());
                    pz.setUsuarioLog(pPredioZona.getUsuarioLog());

                    zonasVigenciaActual.add(pz);
                }
                // persiste las zonas de la vigencia actual
                this.pPredioZonaDao.persistMultiple(zonasVigenciaActual);
            }
            
            //Se actualizan las zonas intermedias
            if(tramite.isRectificacionArea())
            actualizarPZonasIntermediasPorPredio(pp, vigenciaActual, vigenciaTramite, usuario,zonasPredio);
           

        }
    }

    /**
     * Rgistra las onas asociadas a un predio considerando toas las vigencias y los procesos de
     * actualizacion
     *
     *
     * @author felipe.cadena
     * @param predio
     */
    public void registrarZonasConActualizacion(PPredio predio, String usuario,
        List<ZonasFisicasGeoeconomicasVO> zonasPredio) {

        Map<Integer, List<PPredioZona>> zonasPorVigencia = new HashMap<Integer, List<PPredioZona>>();
        List<PredioZona> zonasPredioCompletas = this.predioZonaDao.
            buscarPorPPredioId(predio.getId());
        // List<PPredioZona> zonasPredioCompletas = this.pPredioZonaDao.buscarTodasZonasPorPPredioId(predio.getId());
        List<PPredioZona> zonasVigenciaActual;
        List<PPredioZona> zonasMarcadasAInsertar = new ArrayList<PPredioZona>();
        List<PPredioZona> zonasNuevasAInsertar = new ArrayList<PPredioZona>();
        int ultimaVigencia;
        int vigenciaInicial;
        int vigenciaInscripcion;
        boolean existeActualizacion = false;

        Calendar vigenciaTramite = Calendar.getInstance();
        Calendar vigenciaActual = Calendar.getInstance();
        Calendar vigenciaActualizacion = Calendar.getInstance();
        //se calcula la vigencia
        vigenciaTramite.setTime(predio.getFechaInscripcionCatastral()); //fecha de inscripcion del tramite
        vigenciaTramite.set(Calendar.MONTH, Calendar.JANUARY);
        vigenciaTramite.set(Calendar.DATE, 1);
        vigenciaTramite.add(Calendar.YEAR, 1); //La vigencia de tramite es el aÃ±o siguiente a la fecha de inscripcion catastral

        vigenciaActual.set(Calendar.MONTH, Calendar.JANUARY);
        vigenciaActual.set(Calendar.DATE, 1);

        ultimaVigencia = vigenciaActual.get(Calendar.YEAR);
        vigenciaInscripcion = vigenciaTramite.get(Calendar.YEAR);

        //Se calcula el aÃ±o de la ultima actualizacion
        String zona = predio.getNumeroPredial().substring(0, 7);
        Integer year = QueryNativoDAO.anioUltimaActualizacion(this.documentoDao.getEntityManager(),
            zona);
        vigenciaActualizacion.set(year, Calendar.JANUARY, 1);
        if (vigenciaActualizacion.after(vigenciaTramite) &&
            vigenciaActualizacion.before(vigenciaActual)) {
            vigenciaInicial = vigenciaActualizacion.get(Calendar.YEAR);
            existeActualizacion = true;

        } else {
            vigenciaInicial = vigenciaTramite.get(Calendar.YEAR);
        }

        //se inicializan zonas por vigencia
        if (zonasPredioCompletas != null && !zonasPredioCompletas.isEmpty()) {
            for (PredioZona pz : zonasPredioCompletas) {
                Calendar vigenciaZona = Calendar.getInstance();
                vigenciaZona.setTime(pz.getVigencia());
                int vigencia = vigenciaZona.get(Calendar.YEAR);

                PPredioZona ppz = new PPredioZona();
                ppz.setZonaFisica(pz.getZonaFisica());
                ppz.setZonaGeoeconomica(pz.getZonaGeoeconomica());

                if (!zonasPorVigencia.containsKey(vigencia)) {
                    List<PPredioZona> zonas = new ArrayList<PPredioZona>();
                    zonas.add(ppz);
                    zonasPorVigencia.put(vigencia, zonas);
                } else {
                    zonasPorVigencia.get(vigencia).add(ppz);
                }
            }
        }

        zonasVigenciaActual = this.generarZonasDelServicio(predio, zonasPredio, vigenciaActual.
            getTime(), usuario);
        zonasNuevasAInsertar.addAll(zonasVigenciaActual);

        for (int i = ultimaVigencia - 1; i > vigenciaInicial; i--) {
            List<PPredioZona> zonas = zonasPorVigencia.get(i);

            if (zonas != null && !zonas.isEmpty()) {
                if (this.validarZonasIguales(zonas, zonasVigenciaActual)) {
                    //crea nuevas zonas
                    Calendar vi = Calendar.getInstance();
                    vi.set(Calendar.YEAR, i);
                    vi.set(Calendar.MONTH, Calendar.JANUARY);
                    vi.set(Calendar.DATE, 1);
                    zonasNuevasAInsertar.addAll(this.generarCopiaZonas(zonasVigenciaActual, vi.
                        getTime()));

                } else {
                    //marca zonas para modificacion manual
                    PPredioZona ppz = new PPredioZona();

                    Calendar vi = Calendar.getInstance();
                    vi.set(Calendar.YEAR, i);
                    vi.set(Calendar.MONTH, Calendar.JANUARY);
                    vi.set(Calendar.DATE, 1);
                    ppz.setVigencia(vi.getTime());
                    ppz.setArea(0d);
                    ppz.setValorM2Terreno(0d);
                    ppz.setAvaluo(0d);
                    ppz.setPPredio(predio);
                    ppz.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
                    ppz.setZonaFisica("00");
                    ppz.setZonaGeoeconomica("00");
                    ppz.setUsuarioLog(usuario);
                    ppz.setFechaLog(new Date());

                    zonasMarcadasAInsertar.add(ppz);

                }

            }

        }

        //se marcan todas las zonas anteriores a la actualizacion.
        if (existeActualizacion) {
            for (int i = vigenciaInicial; i >= vigenciaInscripcion; i--) {
                PPredioZona ppz = new PPredioZona();

                Calendar vi = Calendar.getInstance();
                vi.set(Calendar.YEAR, i);
                vi.set(Calendar.MONTH, Calendar.JANUARY);
                vi.set(Calendar.DATE, 1);
                ppz.setVigencia(vi.getTime());
                ppz.setArea(0d);
                ppz.setValorM2Terreno(0d);
                ppz.setAvaluo(0d);
                ppz.setPPredio(predio);
                ppz.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
                ppz.setZonaFisica("00");
                ppz.setZonaGeoeconomica("00");
                ppz.setUsuarioLog(usuario);
                ppz.setFechaLog(new Date());

                zonasMarcadasAInsertar.add(ppz);
            }

        } else { //zonas fecha inscripcion
            if (vigenciaActual.get(Calendar.YEAR) != vigenciaTramite.get(Calendar.YEAR)) {
                zonasNuevasAInsertar.addAll(this.generarCopiaZonas(zonasVigenciaActual,
                    vigenciaTramite.getTime()));
            }
        }

        this.pPredioZonaDao.updateMultiple(zonasMarcadasAInsertar);
        this.pPredioZonaDao.updateMultiple(zonasNuevasAInsertar);

    }

    /**
     * Genera una lista copia de la lista de zonas dada con una vigencia determinada
     *
     * @author felipe.cadena
     * @param zonas
     * @return
     */
    private List<PPredioZona> generarCopiaZonas(List<PPredioZona> zonas, Date vigencia) {

        List<PPredioZona> zonasNuevas = new ArrayList<PPredioZona>();

        for (PPredioZona zona : zonas) {

            PPredioZona ppz = zona.clone();
            ppz.setId(null);
            ppz.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            ppz.setVigencia(vigencia);
            zonasNuevas.add(ppz);

        }

        return zonasNuevas;

    }

    /**
     * Genera una lista de zonas basadas en la respuesta del servicio geografico
     *
     * @author felipe.cadena
     * @param zonas
     * @return
     */
    private List<PPredioZona> generarZonasDelServicio(PPredio pp,
        List<ZonasFisicasGeoeconomicasVO> zonasPredio, Date vigencia, String usuario) {

        List<PPredioZona> zonasNuevas = new ArrayList<PPredioZona>();
        Double areaTerreno;

        for (ZonasFisicasGeoeconomicasVO znVO : zonasPredio) {
            areaTerreno = Utilidades.redondearNumeroADosCifrasDecimales(Double.valueOf(znVO.
                getArea()));
            PPredioZona pz = new PPredioZona();
            pz.setArea(areaTerreno);
            pz.setZonaFisica(znVO.getCodigoFisica());
            pz.setZonaGeoeconomica(znVO.getCodigoGeoeconomica());
            pz.setPPredio(pp);
            pz.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            pz.setValorM2Terreno(0d);
            pz.setVigencia(vigencia);
            pz.setAvaluo(0d);
            pz.setFechaInscripcionCatastral(pp.getFechaInscripcionCatastral());
            pz.setFechaLog(new Date());
            pz.setUsuarioLog(usuario);

            zonasNuevas.add(pz);

        }

        return zonasNuevas;

    }

    /**
     * Valida si dos conjuntos de zonas son iguales en sus duplas
     *
     *
     * @author felipe.cadena
     *
     */
    private boolean validarZonasIguales(List<PPredioZona> zona1, List<PPredioZona> zona2) {

        boolean result = true;

        for (PPredioZona ppz1 : zona1) {

            boolean existe = false;
            for (PPredioZona ppz2 : zona2) {

                if (ppz1.getZonaFisica().equals(ppz2.getZonaFisica()) &&
                    ppz1.getZonaGeoeconomica().equals(ppz2.getZonaGeoeconomica())) {
                    existe = true;
                    break;
                }
                if (!existe) {
                    return false;
                }
            }
        }

        return result;

    }
    
    
    
    /**
     * Valida si dos conjuntos de zonas son iguales en sus duplas sin importar el orden
     *
     *
     * @author vsocarras
     *
     */
    private boolean validarZonasIgualesSinorden(List<PPredioZona> zona1, List<PPredioZona> zona2) {

        boolean result = true;
        if(zona1.size()!=zona2.size())
        	  return false;
        for (PPredioZona ppz1 : zona1) {

            boolean existe = false;
            for (PPredioZona ppz2 : zona2) {
                if (ppz1.getZonaFisica().equals(ppz2.getZonaFisica()) &&
                    ppz1.getZonaGeoeconomica().equals(ppz2.getZonaGeoeconomica())) {
                    existe = true;
                    break;
                }
            }
            if (!existe) {
                return false;
            }
        }
        
        

        return result;

    }

    /**
     * Metodo para cancelar las zonas asociadas a un predio
     *
     * @author felipe.cadena
     * @param predioId
     * @param vigenciaActual
     * @param vigenciaTramite
     *
     */
    private boolean cancelarPZonasPorPredio(Long predioId, Calendar vigenciaActual,
        Calendar vigenciaTramite, UsuarioDTO usuario) {

        //se consultan las zonas del predio 
        if (vigenciaTramite != null &&
            (vigenciaTramite.get(Calendar.YEAR) != vigenciaActual.get(Calendar.YEAR))) {
            vigenciaTramite.add(Calendar.YEAR, 1);// Se aumenta en un año para obtener la vigencia real si la vigencia del tramite no es del año actual.
        }
        try {
            List<PPredioZona> zonas = this.pPredioZonaDao.buscarTodasZonasPorPPredioId(predioId);

            for (PPredioZona pPredioZona : zonas) {
                //v1.1.8
                if (vigenciaActual == null && vigenciaTramite == null) {

                    if (pPredioZona.getCancelaInscribe() != null &&
                        !EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pPredioZona.
                            getCancelaInscribe())) {
                        pPredioZona.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
                            getCodigo());
                        this.pPredioZonaDao.update(pPredioZona);
                    }
                } else {
                    if (pPredioZona.getVigencia().getYear() + 1900 == Integer.valueOf(vigenciaActual.get(
                        Calendar.YEAR)).intValue() ||
                        pPredioZona.getVigencia().getYear() + 1900 == Integer.valueOf(vigenciaTramite.get(
                        Calendar.YEAR)).intValue()) {
                        pPredioZona.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
                            getCodigo());
                        pPredioZona.setUsuarioLog(usuario.getLogin());
                        this.pPredioZonaDao.update(pPredioZona);
                    }

                }
            }

            return true;
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al cancelar zonas previas ");
            return false;
        }

    }
    
    
    
    /**
     * Metodo para actualizar las zonas intermedias asociadas a un predio
     *
     * @author vsocarras
     * @param predioId
     * @param vigenciaActual
     * @param vigenciaTramite
     *
     */
    private void actualizarPZonasIntermediasPorPredio(PPredio predio, Calendar vigenciaActual,
        Calendar vigenciaTramite, UsuarioDTO usuario,List<ZonasFisicasGeoeconomicasVO>  zonasPredio) {
    	
        try {
         if (vigenciaActual != null && vigenciaTramite != null) {
        	Map<Integer, List<PPredioZona>> zonasPorVigencia = new HashMap<Integer, List<PPredioZona>>();
            List<PPredioZona> zonasCompletas = this.pPredioZonaDao.buscarTodasZonasPorPPredioId(predio.getId());
            List<PPredioZona> zonasVigenciaActual = this.generarZonasDelServicio(predio, zonasPredio, vigenciaActual.
    	            getTime(), usuario.getLogin());
            
               for (PPredioZona pPredioZona : zonasCompletas) {
            	   int vigencia =pPredioZona.getVigencia().getYear() + 1900;
                 if (vigencia > Integer.valueOf(vigenciaTramite.get(Calendar.YEAR)).intValue()  && vigencia < Integer.valueOf(vigenciaActual.get(Calendar.YEAR)).intValue()) {
                	 if (!zonasPorVigencia.containsKey(vigencia)) {
                         List<PPredioZona> zonas = new ArrayList<PPredioZona>();
                         zonas.add(pPredioZona);
                         zonasPorVigencia.put(vigencia, zonas);
                     } else {
                         zonasPorVigencia.get(vigencia).add(pPredioZona);
                     }
                        
                    }
               }
              
               for (Map.Entry<Integer, List<PPredioZona>> entry  : zonasPorVigencia.entrySet()) {
            	   if(validarZonasIgualesSinorden(entry.getValue(),zonasVigenciaActual)) {
            		  LOGGER.debug("actualizando zona intermdias de la vigencia "+entry.getKey()); 
	            	  for (PPredioZona pPredioZonaInter : entry.getValue()) {
	            		  pPredioZonaInter.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.
	                              getCodigo());
	            		  pPredioZonaInter.setUsuarioLog(usuario.getLogin());
	            		  for (PPredioZona ppz2 : zonasVigenciaActual) {
	                          if (pPredioZonaInter.getZonaFisica().equals(ppz2.getZonaFisica()) &&
	                        	  pPredioZonaInter.getZonaGeoeconomica().equals(ppz2.getZonaGeoeconomica())) {
	                        	  pPredioZonaInter.setArea(Utilidades.redondearNumeroADosCifrasDecimales(Double.valueOf(ppz2.
                    getArea())));
	                              break;
	                          }
	                      }
	            		  
	                      this.pPredioZonaDao.update(pPredioZonaInter);   
					 }
            	   }else {
            		   for (PPredioZona pPredioZonaInter : entry.getValue()) {
 	            		  pPredioZonaInter.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
 	                              getCodigo());
 	            		  pPredioZonaInter.setUsuarioLog(usuario.getLogin());
 	            		  this.pPredioZonaDao.update(pPredioZonaInter);   
 					 } 
            	   }
            	   
               }
               
               
            }
         
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al actualizando  zonas intermedias ");
           
        }

    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @modifiedBy fredy.wilches
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerPUnidadesConstruccionsByPredioId(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstruccionsByPredioId(
        Long predioId, boolean incluirCancelados) {

        return this.pUnidadConstDao.buscarUnidadesDeConstruccionPorPPredioId(predioId,
            incluirCancelados);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerPUnidadesConstruccionsByTramiteId(java.lang.Long)
     */
    @Override
    public List<PUnidadConstruccion> obtenerPUnidadesConstruccionsByTramiteId(
        Long tramiteId) {

        return this.pUnidadConstDao.getAllPunidadConstruccionsByTramiteId(tramiteId);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author lorena.salamanca
     * @see ITramite#eliminarTramiteDocumentacion(TramiteDocumentacion)
     */
    @Override
    public void eliminarTramiteDocumentacion(TramiteDocumentacion tramiteDocumentacionEliminar) {
        try {
            this.tramiteDocumentacionDao.delete(tramiteDocumentacionEliminar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

    /**
     * @see
     * ITramite#guardarActualizarSolicitudDocumentacion(co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion)
     * @author felipe.cadena
     * @param solicitudDocumentacion
     *
     * @return
     */
    @Override
    public SolicitudDocumentacion guardarActualizarSolicitudDocumentacion(
        SolicitudDocumentacion solicitudDocumentacion) {

        return solicitudDocumentacionDAO.update(solicitudDocumentacion);
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#existeElTramite(java.lang.Long)
     */
    @Override
    public boolean existeElTramite(Long tramiteId) {
        try {
            boolean b = this.tramiteDao.tramiteExists(tramiteId);
            return b;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }

    }

    /**
     * @see ITramiteLocal#buscarTramitePorFiltroTramiteAdministracionAsignaciones(
     * FiltroDatosConsultaTramite, String, String, int...)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public List<Tramite> buscarTramitePorFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount) {
        return this.tramiteDao.findTramiteByFiltroTramiteAdministracionAsignaciones(
            filtroBusquedaTramite, sortField, sortOrder, rowStartIdxAndCount);
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#contarTramitePorFiltroTramiteAdministracionAsignaciones
     * (FiltroDatosConsultaTramite)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public Integer contarTramitePorFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite) {
        return this.tramiteDao
            .countTramiteByFiltroTramiteAdministracionAsignaciones(
                filtroBusquedaTramite);
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerUnidadesConstruccionsPorPredioId(java.lang.Long)
     */
    @Override
    public List<UnidadConstruccion> obtenerUnidadesConstruccionsPorPredioId(
        Long predioId) {
        try {
            List<UnidadConstruccion> answer = this.unidadConstDao.getUnidadsConstruccionByPredioId(
                predioId);
            return answer;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#obtenerImagenDespuesEdicionPorTramiteId(java.lang.Long)
     */
    @Override
    public Documento obtenerImagenDespuesEdicionPorTramiteId(Long tramiteId) {
        try {
            Documento answer = this.documentoDao.getImagenAfterEdicionByTramiteId(tramiteId);
            return answer;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

    //---------------------------------------------------------------------------------------------
    /*
     * @author javier.barajas @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#buscarNoRadicadosTramiteporNumeroPredial(java.lang.String)
     */
    @Override
    public List<Tramite> buscarNoRadicadosTramiteporNumeroPredial(String numPredial) {

        return this.tramiteDao.obtenerTramitesByNumeroPredialPredio(numPredial);

    }

    /*
     * @author dumar.penuela @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#buscarTramitesTiposTramitesByNumeroRadicado(java.lang.String)
     */
    @Override
    public List<Tramite> buscarTramitesTiposTramitesByNumeroPredial(String numPredial) {

        return this.tramiteDao.obtenerTramitesTiposTramitesByNumeroPredial(numPredial);
    }

    /*
     * @author dumar.penuela @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#buscarTramitesTiposTramitesByNumeroIdentificacion(java.lang.String)
     */
    @Override
    public List<Tramite> buscarTramitesTiposTramitesByNumeroIdentificacion(String numIdentificacion,
        String tipoIdentificacion) {

        return this.tramiteDao.obtenerTramitesTiposTramitesByNumeroIdentificacion(numIdentificacion,
            tipoIdentificacion);
    }

    // -------------------------------------------------------------------------
    /**
     *
     * @author lorena.salamanca
     * @param idTramite, idTipoDocumento
     *
     */
    @Override
    public String getIdRepositorioDocumentosDeReplicaGDBdeTramite(Long tramiteId,
        Long idTipoDocumento) {
        String respuesta = this.tramiteDao
            .getIdRepositorioDocumentosDeReplicaGDBdeTramite(tramiteId, idTipoDocumento);
        return respuesta;
    }
    // --------------------------------------------------------------------------------------------

    /*
     * @author franz.gamba @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#existeImagenInicialTramitePorIdTramite(java.lang.Long)
     */
    @Override
    public boolean existeImagenInicialTramitePorIdTramite(Long tramiteId) {
        try {
            return this.documentoDao.existsImagenTramiteInicialByTramiteId(tramiteId);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     * @see
     * co.gov.igac.snc.fachadas.ITramiteLocal#guardarActualizarSolicitudDocumento(SolicitudDocumento)
     */
    @Override
    public SolicitudDocumento guardarActualizarSolicitudDocumento(
        SolicitudDocumento solicitudDocumento) {
        try {
            return solicitudDocumentoDAO.update(solicitudDocumento);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarComisionTramiteDatoPorTramiteId(Long)
     * @author javier.aponte
     */
    @Implement
    @Override
    public List<ComisionTramiteDato> buscarComisionTramiteDatoPorTramiteId(
        Long tramiteId) {

        LOGGER.debug("on TramiteBean#buscarComisionTramiteDatoPorTramiteId ...");

        List<ComisionTramiteDato> answer = null;

        try {
            answer = this.comisionTramiteDatoDao.buscarComisionTramiteDatoPorTramiteId(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#buscarComisionTramiteDatoPorTramiteId: " +
                ex.getMensaje());
        }

        return answer;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see ITramiteLocal#removerTramiteDetallePredio(TramiteDetallePredio)
     */
    @Override
    public boolean removerTramiteDetallePredio(TramiteDetallePredio tdp) {
        boolean answer;
        try {
            this.tramiteDetallePredioDao.delete(tdp);
            answer = true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @see ITramiteLocal#validarProyeccion(Long tramiteId, Long tramiteSeccionDatosId)
     * @author david.cifuentes
     */
    @Override
    public Object[] validarProyeccion(Long tramiteId, Long tramiteSeccionDatosId) {
        Object[] validarProyeccionObject = null;
        try {
            validarProyeccionObject = sncProcedimientoDao.validarProyeccion(
                tramiteId, tramiteSeccionDatosId);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return validarProyeccionObject;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método qeu carga las unidades de construccion a cada uno de los predios en las torres en una
     * ficha matriz
     *
     * @author franz.gamba
     * @param ficha
     * @return
     */
    private FichaMatrizVO loadUnidadesConstruccionPorPredioPH(FichaMatrizVO ficha) {

        if (ficha.getFichaMatrizTorres() != null && !ficha.getFichaMatrizTorres().isEmpty()) {
            for (FichaMatrizTorreVO torre : ficha.getFichaMatrizTorres()) {
                for (FichaMatrizPisoVO piso : torre.getFichaMatrizPisos()) {
                    if (piso.getPredios() != null && !piso.getPredios().isEmpty()) {
                        for (FichaMatrizPredioVO predio : piso.getPredios()) {

                            List<UnidadConstruccion> unidadesConst = this.conservacionService.
                                obtenerUnidadesConstruccionsPorNumeroPredial(predio.
                                    getNumeroPredial());

                            if (unidadesConst != null && !unidadesConst.isEmpty()) {
                                List<UnidadConstruccionVO> unidadsVO =
                                    new ArrayList<UnidadConstruccionVO>();
                                for (UnidadConstruccion unidad : unidadesConst) {
                                    unidadsVO.add(new UnidadConstruccionVO(unidad));
                                }
                                predio.setUnidadesConstruccions(unidadsVO);
                            }

                            List<PUnidadConstruccion> pUnidadesConst = this.conservacionService.
                                obtenerPUnidadesConstruccionsPorNumeroPredial(predio.
                                    getNumeroPredial());

                            if (pUnidadesConst != null && !pUnidadesConst.isEmpty()) {
                                List<PUnidadConstruccionVO> pUnidadsVO =
                                    new ArrayList<PUnidadConstruccionVO>();
                                for (PUnidadConstruccion pUnidad : pUnidadesConst) {
                                    pUnidadsVO.add(new PUnidadConstruccionVO(pUnidad));
                                }
                                predio.setpUnidadesConstruccions(pUnidadsVO);
                            }
                        }
                    }
                }
                if (torre.getFichaMatrizSotanos() != null) {
                    for (FichaMatrizSotanoVO sotano : torre.getFichaMatrizSotanos()) {
                        if (sotano.getPredios() != null && !sotano.getPredios().isEmpty()) {
                            for (FichaMatrizPredioVO predio : sotano.getPredios()) {

                                List<UnidadConstruccion> unidadesConst = this.conservacionService.
                                    obtenerUnidadesConstruccionsPorNumeroPredial(predio.
                                        getNumeroPredial());

                                if (unidadesConst != null && !unidadesConst.isEmpty()) {
                                    List<UnidadConstruccionVO> unidadsVO =
                                        new ArrayList<UnidadConstruccionVO>();
                                    for (UnidadConstruccion unidad : unidadesConst) {
                                        unidadsVO.add(new UnidadConstruccionVO(unidad));
                                    }
                                    predio.setUnidadesConstruccions(unidadsVO);
                                }

                                List<PUnidadConstruccion> pUnidadesConst = this.conservacionService.
                                    obtenerPUnidadesConstruccionsPorNumeroPredial(predio.
                                        getNumeroPredial());

                                if (pUnidadesConst != null && !pUnidadesConst.isEmpty()) {
                                    List<PUnidadConstruccionVO> pUnidadsVO =
                                        new ArrayList<PUnidadConstruccionVO>();
                                    for (PUnidadConstruccion pUnidad : pUnidadesConst) {
                                        pUnidadsVO.add(new PUnidadConstruccionVO(pUnidad));
                                    }
                                    predio.setpUnidadesConstruccions(pUnidadsVO);
                                }
                            }
                        }
                    }
                }
            }
        } else if ((ficha.getFichaMatrizTorres() == null || ficha.getFichaMatrizTorres().isEmpty()) &&
            (ficha.getFichaMatrizPredios() != null &&
            !ficha.getFichaMatrizPredios().isEmpty())) {
            for (FichaMatrizPredioVO predio : ficha.getFichaMatrizPredios()) {

                List<UnidadConstruccion> unidadesConst = this.conservacionService.
                    obtenerUnidadesConstruccionsPorNumeroPredial(
                        predio.getNumeroPredial());

                if (unidadesConst != null && !unidadesConst.isEmpty()) {
                    List<UnidadConstruccionVO> unidadsVO =
                        new ArrayList<UnidadConstruccionVO>();
                    for (UnidadConstruccion unidad : unidadesConst) {
                        unidadsVO.add(new UnidadConstruccionVO(unidad));
                    }
                    predio.setUnidadesConstruccions(unidadsVO);
                }

                List<PUnidadConstruccion> pUnidadesConst =
                    this.conservacionService.
                        obtenerPUnidadesConstruccionsPorNumeroPredial(
                            predio.getNumeroPredial());

                if (pUnidadesConst != null && !pUnidadesConst.isEmpty()) {
                    List<PUnidadConstruccionVO> pUnidadsVO =
                        new ArrayList<PUnidadConstruccionVO>();
                    for (PUnidadConstruccion pUnidad : pUnidadesConst) {
                        pUnidadsVO.add(new PUnidadConstruccionVO(pUnidad));
                    }
                    predio.setpUnidadesConstruccions(pUnidadsVO);
                }
            }

        }

        return ficha;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     * @see ITramiteLocal#guardarYActualizarTramiteDetallePredio(TramiteDetallePredio)
     */
    @Override
    public TramiteDetallePredio guardarYActualizarTramiteDetallePredio(
        TramiteDetallePredio tpe) {
        TramiteDetallePredio answer = null;
        try {
            answer = this.tramiteDetallePredioDao.update(tpe);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @author felipe.cadena
     * @see ITramiteLocal#guardarYActualizarTramiteDetallePredioMultiple(List<TramiteDetallePredio>)
     */
    @Override
    public void guardarYActualizarTramiteDetallePredioMultiple(
        List<TramiteDetallePredio> tdps) {
        try {
            this.tramiteDetallePredioDao.updateMultiple(tdps);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#buscarComisionesParaAprobar: " + e);
        }
    }

    // ------------------------------------------------ //
    /**
     * @see ITramiteLocal#consultaDeTramitesPorFiltros(boolean, FiltroDatosConsultaTramite,
     * FiltroDatosConsultaPredio, FiltroDatosConsultaSolicitante, boolean filtroProcessBool, String,
     * EstructuraOrganizacional, EstructuraOrganizacional )
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia :: 02-09-2013 :: ajustes en las comparaciones para mejorar
     * tiempos de respuesta
     */
    @Override
    public List<Tramite> consultaDeTramitesPorFiltros(
        boolean filtroBaseDeDatosBool,
        FiltroDatosConsultaTramite filtrosTramite, FiltroDatosConsultaPredio filtrosPredio,
        FiltroDatosConsultaSolicitante filtrosSolicitante, boolean filtroProcessBool,
        String filtroFuncionarioEjecutor, EstructuraOrganizacional filtroTerritorial,
        EstructuraOrganizacional filtroUOC) {

        HashMap<Long, Tramite> tramitesResultadoBD = new HashMap<Long, Tramite>();
        List<Tramite> tramitesReturn = new ArrayList<Tramite>();
        int maxResults = this.parametroDAO.getParametroPorNombre(
            EParametro.MAX_RESULTS_CONSULTA_TRAMITE.toString()).getValorNumero().intValue();

        try {

            if (filtroBaseDeDatosBool) {
                String codigoMunicipioTerritorial = null;
                if (filtroTerritorial != null && filtroTerritorial.getMunicipio() != null) {
                    codigoMunicipioTerritorial = filtroTerritorial.getMunicipio().getCodigo();
                }
                // Búsqueda de trámites filtrada por la base de datos
                tramitesResultadoBD = this.vTramitePredioDao.consultaDeTramitesPorFiltros(
                    filtrosTramite, filtrosPredio,
                    filtrosSolicitante, codigoMunicipioTerritorial, maxResults);
            }
            // Búsqueda de trámites filtrada por parametros en el process
            if (filtroBaseDeDatosBool && tramitesResultadoBD != null && !tramitesResultadoBD.
                isEmpty()) {
                tramitesReturn = consultarTramitesPorFiltrosEnProcess(filtroFuncionarioEjecutor,
                    filtroTerritorial,
                    filtroUOC, tramitesResultadoBD);
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return tramitesReturn;
    }

    /**
     * Retorna el usuario DTO asociado a la actividad de procesos dada, si el usuario no existe en
     * el LDAP crea un objeto nuevo con el nombre y estado de que no existe.
     *
     * @author felipe.cadena
     * @param a
     * @return
     */
    private UsuarioDTO obtenerUsuarioActividadProcesos(Actividad a) {

        UsuarioDTO usuarioEjecutor = null;
        try {
            usuarioEjecutor = this.generalesService.getCacheUsuario(a.getUsuarioEjecutor());
        } catch (ExcepcionSNC e) {
            usuarioEjecutor = new UsuarioDTO();
            usuarioEjecutor.setLogin(a.getUsuarioEjecutor());
            usuarioEjecutor.setEstado(ELDAPEstadoUsuario.NO_EXISTE_LDAP.getValue());
            LOGGER.warn(e.getMensaje(), e);
        }

        return usuarioEjecutor;

    }
    // ------------------------------------------------ //

    /**
     * Método que obtiene una lista de trámites búscada en el process basada en ciertos parametros
     * de búsqueda.
     *
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia :: 15-08-2013 :: ajustes en las comparaciones para mejorar
     * tiempos de respuesta
     */
    private List<Tramite> consultarTramitesPorFiltrosEnProcess(String filtroFuncionarioEjecutor,
        EstructuraOrganizacional filtroTerritorial, EstructuraOrganizacional filtroUOC,
        HashMap<Long, Tramite> tramitesResultadoBD) {

        List<Actividad> actividadesFiltro = new ArrayList<Actividad>();
        List<Actividad> activsTramAsociadosFiltro = new ArrayList<Actividad>();
        List<Tramite> answer = new ArrayList<Tramite>();
        HashMap<Long, Tramite> tramitesResultadoBDSalida = new HashMap<Long, Tramite>();
        HashMap<String, UsuarioDTO> listaUsuarios = new HashMap<String, UsuarioDTO>();

        try {
            // Contenedor con filtros para la consulta en el componente de
            // procesos
            Map<EParametrosConsultaActividades, String> filtros =
                new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);

            Map<EParametrosConsultaActividades, String> filtrosTramAsociados =
                new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);

            filtros.put(EParametrosConsultaActividades.ACTIVIDADES_SUSPENDIDAS,
                EEstadoActividad.ACTIVIDADES_VIGENTES_Y_SUSPENDIDAS);

            // Filtro por funcionario ejecutor
            if (filtroFuncionarioEjecutor != null && !filtroFuncionarioEjecutor.trim().isEmpty()) {
                UsuarioDTO usuarioEjecutor = this.generalesService.getCacheUsuario(
                    filtroFuncionarioEjecutor);
                if (usuarioEjecutor != null) {
                    filtros.put(EParametrosConsultaActividades.PROPIETARIO_ACTIVIDAD,
                        usuarioEjecutor.getLogin());
                }
            }

            // OBTENER TRÁMITES
            String ids = "";

            int cont = 0;
            int limiteTemp = 400;
            String separador = ",";
            for (Map.Entry t : tramitesResultadoBD.entrySet()) {
                if (cont < limiteTemp) {
                    ids += t.getKey().toString() + separador;
                    cont++;
                }
            }
            ids = ids.substring(0, ids.length() - 1);

            filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, ids);
            actividadesFiltro.addAll(this.procesosService.consultarListaActividades(filtros));

            if (filtroFuncionarioEjecutor != null && !filtroFuncionarioEjecutor.trim().isEmpty()) {
                for (Actividad a : actividadesFiltro) {
                    tramitesResultadoBDSalida.put(a.getIdObjetoNegocio(),
                        (Tramite) tramitesResultadoBD.get(a.getIdObjetoNegocio()));
                }
            } else {
                tramitesResultadoBDSalida = (HashMap<Long, Tramite>) tramitesResultadoBD.clone();
            }

            // if(buscarporTerritorial){
            // String territorial = codigoHomologadoDao.obtenerCodigo(
            // ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
            // ESistema.LDAP, ESistema.SNC,
            // filtroTerritorial.getCodigo(), null);
            // // Filtro por territorial
            // if (filtroTerritorial != null && filtroTerritorial.getCodigo()!=
            // null) {
            // filtros.put( EParametrosConsultaActividades.TERRITORIAL,
            // territorial);
            //
            // }
            // // Filtro por unidad operativa de catastro
            // if (filtroUOC != null && filtroTerritorial.getCodigo()!= null){
            // filtros.put( EParametrosConsultaActividades.TERRITORIAL,
            // territorial);
            // }
            // }
            // actividadesFiltro =
            // this.procesosService.consultarListaActividades(filtros);
            // if(estados.isEmpty() && (actividadesFiltro == null ||
            // actividadesFiltro.isEmpty())){
            // filtros.put( EParametrosConsultaActividades.ESTADOS, "5");
            // actividadesFiltro =
            // this.procesosService.consultarListaActividades(filtros);
            // }

            /*
             * Generación del hashmap de retorno de los trámites con las actividades (incluyendo las
             * de los asociados)
             */
            if (!actividadesFiltro.isEmpty()) {
                for (Actividad a : actividadesFiltro) {
                    if (tramitesResultadoBDSalida.containsKey(a.getIdObjetoNegocio())) {
                        Tramite tramCopia = new Tramite();
                        try {
                            tramCopia = (Tramite) tramitesResultadoBDSalida.get(a.
                                getIdObjetoNegocio())
                                .clone();
                        } catch (Exception e) {
                            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER,
                                e, e.getMessage());
                        }
                        if (tramCopia != null) {

                            tramCopia.setActividadActualTramite(a);

                            if (a.getUsuarioEjecutor() != null) {
                                if (!listaUsuarios.containsKey(a.getUsuarioEjecutor())) {
                                    //felipe.cadena::20-06-2016::#18055:: Se maneja la excepcion para cuando el usuario asociado a la actividad no existe en el ldap.
                                    UsuarioDTO usuarioEjecutor = this.
                                        obtenerUsuarioActividadProcesos(a);
                                    listaUsuarios.put(a.getUsuarioEjecutor(), usuarioEjecutor);
                                }
                                tramCopia.setUsuarioActividadTramite(listaUsuarios.get(a.
                                    getUsuarioEjecutor()));
                            }
                            if (a.getRoles() != null && !a.getRoles().isEmpty()) {
                                tramCopia.setNombreRolUsuarioActual(a.getRoles().toString().
                                    replaceAll("\\[|\\]", ""));
                            }

                            if (tramCopia.getTramites() != null && !tramCopia.getTramites().
                                isEmpty()) {
                                String idTramitesAsociados = "";
                                HashMap<Long, Tramite> mapaTramitesAsociados =
                                    new HashMap<Long, Tramite>();
                                List<Tramite> tramitesAsociadosnEstado = new ArrayList<Tramite>();

                                for (Tramite tAsoc : tramCopia.getTramites()) {
                                    cont = 0;
                                    if (cont < limiteTemp) {
                                        idTramitesAsociados += tAsoc.getId().toString() + separador;
                                        cont++;
                                    }
                                    /*
                                     * Condiciones para trámites que se comportan como asociados
                                     * pero no lo son
                                     */
                                    if (!tAsoc.getTipoTramite().equals("8") && !tAsoc.
                                        getTipoTramite().equals("9") &&
                                        !tAsoc.getTipoTramite().equals("10") && !tAsoc.
                                        getTipoTramite().equals("11") &&
                                        !tAsoc.getTipoTramite().equals("13") &&
                                        !tAsoc.getEstado().equals(ETramiteEstado.NO_APROBADO.
                                            getCodigo()) &&
                                        !tAsoc.getEstado().equals(ETramiteEstado.POR_APROBAR.
                                            getCodigo())) {
                                        mapaTramitesAsociados.put(tAsoc.getId(), tAsoc);
                                    }
                                }

                                idTramitesAsociados = idTramitesAsociados.substring(0,
                                    idTramitesAsociados.length() - 1);
                                filtrosTramAsociados.put(
                                    EParametrosConsultaActividades.ULTIMA_ACTIVIDAD,
                                    idTramitesAsociados);
                                activsTramAsociadosFiltro
                                    .addAll(this.procesosService.consultarListaActividades(
                                        filtrosTramAsociados));

                                for (Actividad actAsoc : activsTramAsociadosFiltro) {
                                    if (mapaTramitesAsociados.containsKey(actAsoc.
                                        getIdObjetoNegocio())) {
                                        Tramite tramAsociadoCopia = new Tramite();
                                        try {
                                            tramAsociadoCopia = (Tramite) mapaTramitesAsociados
                                                .get(actAsoc.getIdObjetoNegocio()).clone();
                                        } catch (Exception e) {
                                            throw SncBusinessServiceExceptions.EXCEPCION_100001.
                                                getExcepcion(LOGGER, e,
                                                    e.getMessage());
                                        }
                                        if (tramAsociadoCopia != null) {
                                            tramAsociadoCopia.setActividadActualTramite(actAsoc);
                                            if (actAsoc.getUsuarioEjecutor() != null) {

                                                if (!listaUsuarios.containsKey(actAsoc.
                                                    getUsuarioEjecutor())) {
                                                    //felipe.cadena::20-06-2016::#18055:: Se maneja la excepcion para cuando el usuario asociado a la actividad no existe en el ldap.
                                                    UsuarioDTO usuarioEjecutor = this.
                                                        obtenerUsuarioActividadProcesos(actAsoc);
                                                    listaUsuarios.put(actAsoc.getUsuarioEjecutor(),
                                                        usuarioEjecutor);
                                                }
                                                tramAsociadoCopia.setUsuarioActividadTramite(
                                                    listaUsuarios.get(actAsoc.getUsuarioEjecutor()));
                                            }
                                            if (actAsoc.getRoles() != null && !actAsoc.getRoles().
                                                isEmpty()) {
                                                tramAsociadoCopia.setNombreRolUsuarioActual(
                                                    actAsoc.getRoles().toString().replaceAll(
                                                        "\\[|\\]", ""));
                                            }
                                            mapaTramitesAsociados.put(tramAsociadoCopia.getId(),
                                                tramAsociadoCopia);
                                        }

                                    }
                                }

                                tramitesAsociadosnEstado = new ArrayList<Tramite>(
                                    mapaTramitesAsociados.values());
                                tramCopia.setTramites(new ArrayList<Tramite>());
                                tramCopia.setTramites(tramitesAsociadosnEstado);
                            }

                            tramitesResultadoBDSalida.put(tramCopia.getId(), tramCopia);
                        }
                    }
                }
            }
            answer = new ArrayList<Tramite>(tramitesResultadoBDSalida.values());
            for (Tramite tramFetch : answer) {
                Hibernate.initialize(tramFetch.getTramiteEstados());
                // juan.cruz::15/06/2017::#19345:: Se inicializa "TramitesPeriodoEnglobe" para evitar inconvenientes en validaciones 
                //                                 al momento de realizar la consulta de documentos desde el número de radicación de un trámite de auto estimación.
                Hibernate.initialize(tramFetch.getTramitePredioEnglobes());
                for (Tramite tramAsoc : tramFetch.getTramites()) {
                    Hibernate.initialize(tramAsoc.getResultadoDocumento());
                    Hibernate.initialize(tramAsoc.getActividadActualTramite());
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ITramiteLocal#borrarSolicitudDocumentacion(SolicitudDocumentacion)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public void borrarSolicitudDocumentacion(
        SolicitudDocumentacion solicitudDocumentacion) {

        LOGGER.debug("Inicio TramiteBean#borrarSolicitudDocumentacion");

        try {
            this.solicitudDocumentacionDAO.delete(solicitudDocumentacion);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "TramiteBean#borrarSolicitudDocumentacion");
        }

        LOGGER.debug("Fin ITramiteLocal#borrarSolicitudDocumentacion");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#guardarActualizarTramiteDocumentacion(TramiteDocumentacion)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public void guardarActualizarListaTramiteDocumentacions(
        List<TramiteDocumentacion> listaTramiteDocumentacions) {
        LOGGER.debug("Inicio TramiteBean#guardarActualizarTramiteDocumentacion");

        try {

            for (TramiteDocumentacion traDoc : listaTramiteDocumentacions) {
                this.tramiteDocumentacionDao.update(traDoc);
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "TramiteBean#borrarSolicitudDocumentacion");
        }

        LOGGER.debug("Fin ITramiteLocal#guardarActualizarTramiteDocumentacion");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#borrarTramiteDocumentacion(TramiteDocumentacion)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public void borrarListaTramiteDocumentacions(
        List<TramiteDocumentacion> listaTramiteDocumentacions) {
        LOGGER.debug("Inicio TramiteBean#borrarTramiteDocumentacion");

        try {
            for (TramiteDocumentacion traDoc : listaTramiteDocumentacions) {
                this.tramiteDocumentacionDao.delete(traDoc);
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "TramiteBean#borrarSolicitudDocumentacion");
        }

        LOGGER.debug("Fin ITramiteLocal#borrarTramiteDocumentacion");

    }

    // ------------------------------------------------ //
    /**
     * Método que obtiene una lista comisiones para aprobar de una territorial.
     *
     * @see ITramiteLocal#buscarComisionesParaAprobar(java.util.List, java.lang.String,
     * java.lang.String, java.lang.String, java.util.Map, int[])
     *
     * @author david.cifuentes
     */
    @Implement
    @Override
    public List<VComision> buscarComisionesParaAprobar(List<Long> idTramites, String tipoComision,
        String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos) {

        LOGGER.debug("TramiteBean#buscarComisionesParaAprobar");

        List<VComision> answer = null;
        try {
            answer = this.vcomisionDao.buscarComisionesParaAprobar(idTramites, tipoComision,
                sortField, sortOrder, filters, contadoresDesdeYCuantos);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#buscarComisionesParaAprobar: " + ex.getMensaje());
        }
        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @see ITramite#contarComisionesParaAprobar(java.util.List, java.lang.String)
     * @author david.cifuentes
     */
    @Implement
    @Override
    public int contarComisionesParaAprobar(List<Long> idTramites, String tipoComision) {
        LOGGER.debug("en TramiteBean#contarComisionesParaAprobar");
        int answer = 0;

        try {
            answer = this.vcomisionDao.contarComisionesParaAprobar(idTramites, tipoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#contarComisionesParaAprobar: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ITramiteLocal#buscarSolicitudConProductosYDocsSoporte(Long)
     * @author javier.barajas
     */
    @Implement
    @Override
    public Solicitud buscarSolicitudConProductosYDocsSoporte(Long solicitudId) {

        Solicitud sol = new Solicitud();
        try {
            sol = this.solicitudDao.obtenerSolicitudPorIdconProductosYDocsSoporte(solicitudId);
            return sol;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        } finally {
            return sol;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#guardaYActualizaSolicitud(Solicitud)
     * @author javier.barajas
     */
    @Implement
    @Override
    public Solicitud guardaYActualizaSolicitud(Solicitud solicitud) {

        Solicitud answer = null;
        try {
            answer = this.solicitudDao.update(solicitud);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Ocurrió en error al guardar o actualizar la solictud");
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramiteLocal#insertarTramiteEstados(List<TramiteEstado> estados)
     * @author fredy.wilches
     */
    @Implement
    @Override
    public void insertarTramiteEstados(List<TramiteEstado> estados) {
        for (TramiteEstado te : estados) {
            this.tramiteEstadoDao.update(te);
        }
    }

    // ------------------------------------------------ //
    /**
     * @see ITramiteLocal#buscaPrediosPorManzana(String codManzana)
     * @author javier.barajas
     */
    @Override
    public List<Predio> buscaPrediosPorManzana(String codManzana) {
        List<Predio> resultado;
        try {
            resultado = this.predioDao.obtenerPrediosPorNumeroManzana(codManzana);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ITramiteLocal#guardarYactualizarProductosCatastralesTramite(List<ProductoCatastral>)
     * @author javier.barajas
     */
    @Override
    public List<ProductoCatastral> guardarYactualizarProductosCatastralesTramite(
        List<ProductoCatastral> listaProductos) {

        List<ProductoCatastral> resultado;
        try {
            resultado = this.productoCatastralDAO.guardarYactualizarProductosCatastrales(
                listaProductos);

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return resultado;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ITramiteLocal#buscarProductosCatastralesporId(Long)
     * @author javier.barajas
     */
    @Override
    public ProductoCatastral buscarProductosCatastralesporId(Long productoCatastralId) {
        ProductoCatastral resultado;
        try {
            resultado = this.productoCatastralDAO.consultarProductosCatastralesporId(
                productoCatastralId);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see ITramiteLocal#guardarYactualizarProductoCatastralTramite(ProductoCatastral)
     * @author javier.barajas
     */
    @Override
    public ProductoCatastral guardarYactualizarProductoCatastralTramite(ProductoCatastral producto) {
        ProductoCatastral resultado;
        try {
            resultado = this.productoCatastralDAO.guardarYactualizarProductoCatastral(producto);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see ITramiteLocal#buscaProductoCatastralDetalle(Long productoCatastralDetalleId)
     * @author javier.barajas
     */
    @Override
    public ProductoCatastralDetalle buscaProductoCatastralDetalle(Long productoCatastralDetalleId) {
        ProductoCatastralDetalle resultado = new ProductoCatastralDetalle();
        try {
            resultado = this.buscaProductoCatastralDetalle(productoCatastralDetalleId);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    // ------------------------------------------------ //
    /**
     * @see ITramite#buscarComisionPorId
     * @author david.cifuentes
     */
    @Implement
    @Override
    public Comision buscarComisionPorId(Long idComision) {
        Comision answer;
        answer = this.comisionDao.findById(idComision);
        return answer;
    }

    /**
     * @see ITramite#buscarTramiteDepuracionPorIdTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDepuracion> buscarTramiteDepuracionPorIdTramite(Long idTramite) {

        List<TramiteDepuracion> resultado;

        try {
            resultado = this.tramiteDepuracionDAO.buscarPorIdTramite(idTramite);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see ITramite#buscarUltimoTramiteDepuracionPorIdTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDepuracion buscarUltimoTramiteDepuracionPorIdTramite(Long idTramite) {

        TramiteDepuracion resultado = null;
        List<TramiteDepuracion> tds;

        try {
            tds = this.tramiteDepuracionDAO.buscarPorIdTramite(idTramite);
            if (tds != null && !tds.isEmpty()) {
                resultado = tds.get(0);
            }
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see ITramite#actualizarTramiteDepuracion
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDepuracion actualizarTramiteDepuracion(TramiteDepuracion tramiteDepuracion) {
        TramiteDepuracion resultado;

        try {
            resultado = this.tramiteDepuracionDAO.update(tramiteDepuracion);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#guardarActualizarTramiteInconsistencia
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteInconsistencia guardarActualizarTramiteInconsistencia(
        TramiteInconsistencia tramiteInconsistencia) {
        TramiteInconsistencia resultado;

        try {
            resultado = this.tramiteInconsistenciaDAO.update(tramiteInconsistencia);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#guardarActualizarTramiteInconsistencia
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteInconsistencia> guardarActualizarTramiteInconsistencia(
        List<TramiteInconsistencia> tramiteInconsistencias) {
        List<TramiteInconsistencia> resultado;

        try {
            resultado = this.tramiteInconsistenciaDAO.updateMultipleReturn(tramiteInconsistencias);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Haya y define el número de comisiones el número de comisiones efectivas en que ha estado un
     * trámite. Una comisión efectiva es la que ha llegado hasta el estado 'finalizada'
     *
     * @author pedro.garcia
     * @param tramite
     * @param tipoComision tipo de comisión que se va a usar para el cálculo del número de
     * comisiones en las que ha estado el trámite
     */
    private void calcAndSetNumeroComisionesHaEstado(Tramite tramite, String tipoComision) {

        int numComisiones = 0;

        try {
            numComisiones = this.comisionTramiteDao.contarComisionesEfectivasDeTramite(
                tramite.getId(), tipoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#calcAndSetNumeroComisionesHaEstado: " +
                ex.getMensaje());
        }

        tramite.setNumeroComisionesHaEstado(numComisiones);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#consultarComisionesTramiteEfectivasTramite(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<ComisionTramite> consultarComisionesTramiteEfectivasTramite(Long idTramite) {

        List<ComisionTramite> answer = null;

        try {
            answer = this.comisionTramiteDao.consultarComisionesEfectivasDeTramite(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#consultarComisionesTramiteEfectivasTramite: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#eliminarTramiteComisionPorTramitesYComision(java.util.List, java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean eliminarTramiteComisionPorTramitesYComision(
        List<Long> idsTramites, Long idComision) {

        LOGGER.debug("on TramiteBean#eliminarTramiteComisionPorTramitesYComision ");
        boolean answer;

        try {
            this.comisionTramiteDao.deleteByTramiteIdsAndComisionId(idsTramites, idComision);
            answer = true;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#eliminarTramiteComisionPorTramitesYComision: " +
                ex.getMensaje());
            answer = false;
        }

        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @see ITramite#consultarActividadesDeTramites(List<Tramite>)
     * @author david.cifuentes
     */
    @Override
    public List<Tramite> consultarActividadesDeTramites(List<Tramite> tramites) {

        try {

// Esto no me funcionó, lo dejo por si es útil cuando se vayan a integrar
// los historicos, me dí cuenta que la consulta a process se hace por un
// objeto de negocio a la vez (a la fecha), más adelante es posible que 
// pueda ser usable.
//			// Se instancian los parámetros para realizar la consulta por
//			// el process de las actividades de los trámites.
//			MultiMap<EParametrosConsultaActividades, String> parametros = new MultiHashMap<EParametrosConsultaActividades, String>();
//			for (Tramite t : tramites) {
//				if (t.getId() != null) {
//					parametros.put(EParametrosConsultaActividades.ID_TRAMITE, t.getId().toString());
//				}
//			}
//			
//			List<Actividad> actividades = this.procesosService
//					.consultarListaActividadesPorParametrosMultiMap(parametros);
//
//			for (Actividad actividad : actividades) {
//
//				// Instanciar las variables Actividades encontradas para los trámites 
//				// enviados como parámetro.
//				for (Tramite t : tramites) {
//					if (t.getId().longValue() == actividad.getIdObjetoNegocio()) {
//						t.setActividadActualTramite(actividad);
//						break;
//					}
//				}
//			}
            // IMPORTANTE:
            // Las siguientes líneas de código permiten que se consulte la
            // actividad, pero es ineficiente, pues el uso de los métodos está
            // pensado para traer una única actividad a la vez, lo ideal sería
            // hacer escalable los métodos del API de process.
            List<Actividad> actividades = null;
            // Se instancian los parámetros para realizar la consulta por
            // el process de la actividad de cada uno de los trámites
            // individualmente.
            for (Tramite t : tramites) {
                if (t.getId() != null) {
                    actividades = this.procesosService
                        .getActividadesPorIdObjetoNegocio(t.getId());
                    if (actividades != null && actividades.size() > 0 &&
                        actividades.get(0) != null) {
                        t.setActividadActualTramite(actividades.get(0));
//REVIEW :: david.cifuentes :: 26-06-2013 :: ¿por qué era innecesario el 'break'?. Acaso una actividad puede estar relacionada con varios trámites :: pedro.garcia                        
                    }
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return tramites;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * @see ITramite#getTramiteDetallePredioByTramiteId(Long)
     * @author javier.aponte
     */
    @Override
    public List<TramiteDetallePredio> getTramiteDetallePredioByTramiteId(Long idTramite) {

        LOGGER.debug("en TramiteBean#getTramiteDetallePredioByTramiteId");

        List<TramiteDetallePredio> answer = null;

        try {
            answer = this.tramiteDetallePredioDao.getByTramiteId(idTramite);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#getTramiteDetallePredioByTramiteId: " +
                ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#armarEventosScheduleComisiones(java.lang.String, java.lang.String,
     * java.lang.String)
     * @author pedro.garcia
     *
     * Se basó en los métodos de franz.gamba, pero reorganizando paa que todo se hiciera en la capa
     * de negocio, en vez de hacer muchas consultas desde la capa web.
     */
    @Implement
    @Override
    public List<EventoScheduleComisionesDTO> armarEventosScheduleComisiones(
        String codigoEstructuraOrganizacional, String tipoComision, String estadoComision) {

        List<EventoScheduleComisionesDTO> answer = null;
        EventoScheduleComisionesDTO eventToAdd;
        List<Comision> comisiones;
        String nombreFuncionarioEjecutor, tituloEvento;
        Integer numeroTramitesFuncionarioEjecutor;

        comisiones = this.buscarComisionesPorTerritorialYTipoYEstado(
            codigoEstructuraOrganizacional, tipoComision, estadoComision);

        if (comisiones == null) {
            return answer;
        }

        answer = new ArrayList<EventoScheduleComisionesDTO>();
        for (Comision c : comisiones) {
            for (String ejecutor : c.getEjecutores()) {
                eventToAdd = new EventoScheduleComisionesDTO();

                nombreFuncionarioEjecutor = ejecutor;
                numeroTramitesFuncionarioEjecutor =
                    this.contarTramitesDeFuncionarioEjecutor(nombreFuncionarioEjecutor);
                tituloEvento = "C. de " + tipoComision + " :: " + nombreFuncionarioEjecutor + " (" +
                    numeroTramitesFuncionarioEjecutor + "): " + c.getEstado() +
                    " " + c.getDuracion() + " día(s)";

                eventToAdd.setFechaInicio(c.getFechaInicio());
                eventToAdd.setFechaFin(c.getFechaFin());
                eventToAdd.setTitulo(tituloEvento);

                answer.add(eventToAdd);
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#buscarComisionesPorTerritorialYTipoYEstado(java.lang.String, java.lang.String,
     * java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Comision> buscarComisionesPorTerritorialYTipoYEstado(
        String territorialId, String tipoComision, String estadoComision) {

        LOGGER.debug("en TramiteBean#buscarComisionesPorTerritorialYEstadoYTipo");

        List<Comision> comisiones = null;
        List<String> funcionariosEjecutoresComision;

        try {
            comisiones = this.comisionDao.findComisionesPorTerritorialYTipoYEstado(
                territorialId, tipoComision, estadoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarComisionesPorTerritorialYEstadoYTipo " +
                "buscando las comisiones para la territorial con id " + territorialId + ", " +
                "de tipo " + tipoComision + ", " +
                "con estado " + estadoComision + ": " + ex.getMensaje());
            return comisiones;
        }

        try {
            for (int i = 0; i < comisiones.size(); i++) {
                funcionariosEjecutoresComision = this.comisionDao
                    .findFuncionariosEjecutoresByComision(comisiones.get(i)
                        .getId(), territorialId, estadoComision);
                comisiones.get(i).setEjecutores(funcionariosEjecutoresComision);
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarComisionesPorIdTerritorialAndEstadoComision " +
                "buscando los funcionarios ejecutores por comisión: " + ex.getMensaje());
        }
        return comisiones;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#actualizarDocumento(Documento)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public boolean actualizarDocumento(Documento documento) {
        LOGGER.debug("on TramiteBean#actualizarDocumento ...");

        boolean answer = false;

        try {
            answer = this.documentoDao.actualizarDocumentoSinGestorDocumental(documento);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("... error: " + ex.getMensaje());
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#actualizarTramiteDocumentacionSimple(TramiteDocumentacion)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public void actualizarTramiteDocumentacionSimple(
        TramiteDocumentacion tramiteDocumentacionActual) {
        try {
            this.tramiteDocumentacionDao.update(tramiteDocumentacionActual);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#actualizarTramiteDocumentacionSimple " + ex.
                getMensaje());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarDocumentoPorId(Long)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public Documento buscarDocumentoPorId(Long id) {
        Documento resultado = null;
        try {
            //resultado = this.documentoDao.findById(id);
            resultado = this.documentoDao.buscarDocumentoCompletoPorId(id);
            return resultado;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarDocumentoPorId " + ex.getMensaje());
            return resultado;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarDocumentosPorSolicitudId(Long)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public List<Documento> buscarDocumentosPorSolicitudId(Long id) {
        List<Documento> resultado = null;
        try {
            resultado = this.documentoDao.buscarDocumentosPorSolicitudId(id);
            return resultado;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarDocumentosPorSolicitudId " + ex.getMensaje());
            return resultado;
        }
    }

    /**
     * @see ITramiteLocal#getVComisionById(Long)
     * @author javier.aponte
     */
    @Implement
    @Override
    public VComision getVComisionById(Long idVComision) {
        VComision answer = null;
        try {
            answer = this.vcomisionDao.findById(idVComision);
        } catch (Exception e) {
            LOGGER.error("Error al consultar la VComision por id");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramiteLocal#buscarTramiteInconsistenciaPorTramiteId(Long)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteInconsistencia> buscarTramiteInconsistenciaPorTramiteId(Long idTramite) {
        List<TramiteInconsistencia> resultado = null;
        try {
            resultado = this.tramiteInconsistenciaDAO.buscarPorIdTramite(idTramite);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al consultar la TramiteInconsistencia por id: " + e.getMensaje());
        }

        return resultado;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramiteLocal#buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(Long)
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public List<TramiteInconsistencia> buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(
        Long idTramite) {
        List<TramiteInconsistencia> resultado = null;
        try {
            resultado = new ArrayList<TramiteInconsistencia>();
            for (TramiteInconsistencia ti : this.tramiteInconsistenciaDAO.buscarPorIdTramite(
                idTramite)) {
                if (!ti.getInconsistencia().equals(Constantes.NO_HAY_INCONSISTENCIAS_GEOGRAFICAS)) {
                    resultado.add(ti);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al consultar la TramiteInconsistencia por id");
        }

        return resultado;

    }

    /**
     * @see ITramiteLocal#buscarTramitePorFiltroTramiteDerechoPeticionOTutela(
     * FiltroDatosConsultaTramite, String, String, int...)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public List<Tramite> buscarTramitePorFiltroTramiteDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount) {
        return this.tramiteDao.findTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(
            filtroBusquedaTramite, filtrosSolicitante, sortField, sortOrder, rowStartIdxAndCount);
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#contarTramitePorFiltroTramiteDerechoPeticionOTutela
     * (FiltroDatosConsultaTramite)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public Integer contarTramitePorFiltroTramiteDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante) {
        return this.tramiteDao
            .countTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(
                filtroBusquedaTramite, filtrosSolicitante);
    }
//--------------------------------------------------------------------------------------------------        

    /**
     * @see ITramiteLocal#obtenerInformacionTramiteDepuracion(java.lang.Long)
     */
    @Override
    public ResultadoVO obtenerInformacionTramiteDepuracion(UsuarioDTO usuario, Long idTramite) {

        LOGGER.debug("TramiteBean#obtenerInformacionTramiteDepuracion...INICIA");

        List<ActividadVO> listaActividadesTramite = new ArrayList<ActividadVO>();
        List<PredioInfoVO> listaPrediosVoManzanaDepuracion = new ArrayList<PredioInfoVO>();

        try {
            // Agrega la información necesaria del tramite a la actividad de edición geográfica
            Tramite informacionTramite = this.tramiteDao.getTramiteParaEdicionGeografica(idTramite);
            ActividadVO unaActividadEdicionGeografica = new ActividadVO(informacionTramite);

            // Obtiene las inconsistencias del tramite .
            List<TramiteInconsistencia> listaInconsistenciasTramite = this.tramiteInconsistenciaDAO.
                buscarPorIdTramite(idTramite);
            List<TramiteInconsistenciaVO> listaInconsistenciasTramiteVO =
                new ArrayList<TramiteInconsistenciaVO>();
            if ((listaInconsistenciasTramite.size() >= 1) && !(listaInconsistenciasTramite.get(0).
                getInconsistencia().
                equals(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre()))) {
                for (TramiteInconsistencia unaInconsistencia : listaInconsistenciasTramite) {
                    listaInconsistenciasTramiteVO.add(new TramiteInconsistenciaVO(
                        (unaInconsistencia)));
                }
            }
            unaActividadEdicionGeografica.setInconsistenciaTramite(listaInconsistenciasTramiteVO);

            // Construye la informacin del predio o predios asociados al tramite.
            if (informacionTramite.getSubtipo() != null && informacionTramite.getSubtipo().equals(
                EMutacion2Subtipo.ENGLOBE.getCodigo())) {
                List<Predio> prediosEnglobe = this.predioDao.
                    obtenerPrediosCompletosEnglobePorTramiteId(idTramite);
                List<PredioInfoVO> prediosEnglobeVO = new ArrayList<PredioInfoVO>();

                String numeroManzana = prediosEnglobe.get(0).getNumeroPredial().substring(0, 17);
                for (Predio unPredio : this.predioDao.obtenerPrediosPorNumeroManzana(numeroManzana)) {
                    PredioInfoVO unPredioManzana = new PredioInfoVO();
                    unPredioManzana.setNumeroPredial(unPredio.getNumeroPredial());
                    unPredioManzana.setNumeroPredialAnterior(unPredio.getNumeroPredialAnterior());
                    listaPrediosVoManzanaDepuracion.add(unPredioManzana);
                }

                for (Predio unPredioEnglobe : prediosEnglobe) {
                    PredioInfoVO unPredioInfoVo = new PredioInfoVO(unPredioEnglobe,
                        informacionTramite);
                    if (unPredioEnglobe.isEsPredioEnPH()) {
                        FichaMatriz ficha = this.conservacionService.
                            getFichaMatrizByNumeroPredialPredio(informacionTramite.getPredio().
                                getNumeroPredial());
                        if (ficha != null) {
                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                            unPredioInfoVo.setFichaMatriz(fichaVO);
                        }
                    }
                    prediosEnglobeVO.add(unPredioInfoVo);
                }
                unaActividadEdicionGeografica.setPrediosOriginales(prediosEnglobeVO);
            } else if (!informacionTramite.isQuinta()) {
                PredioInfoVO predioAsociadoTramite =
                    new PredioInfoVO(informacionTramite.getPredio(), informacionTramite);
                if (informacionTramite.getPredio().isEsPredioEnPH()) {
                    FichaMatriz ficha = this.conservacionService.
                        getFichaMatrizByNumeroPredialPredio(informacionTramite.getPredio().
                            getNumeroPredial());
                    if (ficha != null) {
                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                        predioAsociadoTramite.setFichaMatriz(fichaVO);
                    }
                }
                String numeroManzana = informacionTramite.getPredio().getNumeroPredial().
                    substring(0, 17);
                for (Predio unPredio : this.predioDao.obtenerPrediosPorNumeroManzana(numeroManzana)) {
                    PredioInfoVO unPredioManzana = new PredioInfoVO();
                    unPredioManzana.setNumeroPredial(unPredio.getNumeroPredial());
                    unPredioManzana.setNumeroPredialAnterior(unPredio.getNumeroPredialAnterior());
                    listaPrediosVoManzanaDepuracion.add(unPredioManzana);
                }
                unaActividadEdicionGeografica.setPredioOriginal(predioAsociadoTramite);
            } else if (informacionTramite.getClaseMutacion().equals(EMutacionClase.QUINTA.
                getCodigo())) {
                PredioInfoVO predioAsociadoTramite = new PredioInfoVO();
                predioAsociadoTramite.setNumeroPredial(informacionTramite.getNumeroPredial());
                unaActividadEdicionGeografica.setPredioOriginal(predioAsociadoTramite);

                String numeroManzana = informacionTramite.getNumeroPredial().substring(0, 17);
                for (Predio unPredio : this.predioDao.obtenerPrediosPorNumeroManzana(numeroManzana)) {
                    PredioInfoVO unPredioManzana = new PredioInfoVO();
                    unPredioManzana.setNumeroPredial(unPredio.getNumeroPredial());
                    unPredioManzana.setNumeroPredialAnterior(unPredio.getNumeroPredialAnterior());
                    listaPrediosVoManzanaDepuracion.add(unPredioManzana);
                }
            }

            unaActividadEdicionGeografica.setPrediosManzanaDepuracion(
                listaPrediosVoManzanaDepuracion);
            listaActividadesTramite.add(unaActividadEdicionGeografica);

            ResultadoVO resultadoConsultaTramite = new ResultadoVO(listaActividadesTramite);
            return resultadoConsultaTramite;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_RECUPERANDO_INFORMACION_TRAMITE_PARA_EDICION_GEOGRAFICA.
                getExcepcion(LOGGER, e);
        } finally {
            LOGGER.debug("TramiteBean#obtenerInformacionTramiteDepuracion...FINALIZA");
        }

    }

    /**
     * @see ITramiteLocal#consultarPredioDepuracion(co.gov.igac.generales.dto.UsuarioDTO,
     * java.lang.String)
     */
    @Override
    public ResultadoVO consultarPredioDepuracion(UsuarioDTO usuario, String numeroPredial) {

        try {
            List<ActividadVO> listaActividades = new ArrayList<ActividadVO>();
            ActividadVO unaActividadVo = new ActividadVO();

            Predio unPredio = this.predioDao.obtenerPredioCompletoPorNumeroPredial(numeroPredial);
            PredioInfoVO unPredioInfoVo = new PredioInfoVO(unPredio, new Tramite());

            unaActividadVo.setPredio(unPredioInfoVo);
            listaActividades.add(unaActividadVo);
            ResultadoVO resultadoVo = new ResultadoVO(listaActividades);

            return resultadoVo;

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_RECUPERANDO_INFORMACION_TRAMITE_PARA_EDICION_GEOGRAFICA.
                getExcepcion(LOGGER, e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#actualizarComision2(co.gov.igac.generales.dto.UsuarioDTO,
     * co.gov.igac.snc.persistence.entity.tramite.Comision)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean actualizarComision2(UsuarioDTO usuarioActual, Comision comision) {

        boolean answer = false;

        try {
            answer = this.comisionDao.updateComision2(usuarioActual, comision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#actualizarComision2: " + ex.getMensaje());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#actualizarTramite2(co.gov.igac.snc.persistence.entity.tramite.Tramite,
     * co.gov.igac.generales.dto.UsuarioDTO)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean actualizarTramite2(Tramite tramite, UsuarioDTO usuario) {

        LOGGER.debug("TramiteBean#actualizarTramite");

        boolean answer;
        tramite.setFechaLog(new Date());
        tramite.setUsuarioLog(usuario.getLogin());
        try {
            if (tramite.getTramites() != null && tramite.getSolicitud() != null) {
                // Se actualiza la fechaLog y usuarioLog de los trámites asociados al trámite.
                for (Tramite t : tramite.getTramites()) {
                    t.setFechaLog(new Date());
                    t.setUsuarioLog(usuario.getLogin());
                }
            }
        } catch (org.hibernate.LazyInitializationException mfLazyEx) {
            LOGGER.error("Algo que no fue inicializado en la consulta trató de ser obtenido del " +
                "trámite: " + mfLazyEx.getMessage());
        } finally {

            try {
                this.tramiteDao.update(tramite);
                answer = true;
            } catch (Exception ex) {
                LOGGER.error("Error actualizando el trámite con id " + tramite.getId() + " : " +
                    ex.getMessage());
                answer = false;
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * ITramiteLocal#guardarActualizarComisionEstado2(co.gov.igac.snc.persistence.entity.tramite.ComisionEstado)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean guardarActualizarComisionEstado2(ComisionEstado comisionEstado) {

        ComisionEstado ce;
        boolean answer;
        String id = "no id";

        try {
            id = (comisionEstado.getId() != null) ? comisionEstado.getId().toString() : "";
            ce = this.comisionEstadoDao.update(comisionEstado);
            answer = true;
        } catch (Exception e) {
            LOGGER.error("Error guardando o actualizando el ComisionEstado con id " + id +
                " :" + e.getMessage());
            answer = false;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#decidirActualizacionEstadoComision1(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean decidirActualizacionEstadoComision1(Long idComision) {

        LOGGER.debug("En TramiteBean#decidirActualizacionEstadoComision1 ...");

        boolean answer = true;
        ArrayList<ComisionTramite> comisionTramites;

        try {
            comisionTramites = (ArrayList<ComisionTramite>) this.comisionTramiteDao.
                buscarPorIdComision(idComision);
            if (comisionTramites == null) {
                return false;
            }
            for (ComisionTramite ct : comisionTramites) {
                if (ct.getRealizada().equals(ESiNo.NO.getCodigo())) {
                    answer = false;
                    break;
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#decidirActualizacionEstadoComision1: " +
                ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#eliminarTramitesDepuracion(java.lang.Long)
     */
    @Override
    //TODO :: andres.eslava :: OJO: usar tabs de 4 espacios para que no quede descuadrado el código. Usar @author en la documentación de la implementación de los métodos             
    public void eliminarTramitesInconsistencia(Long tramiteId) {
        try {
            List<TramiteInconsistencia> inconsistencias = this.
                buscarTramiteInconsistenciaPorTramiteId(tramiteId);
            this.tramiteInconsistenciaDAO.deleteMultiple(inconsistencias);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION;
        }

    }

    /**
     * @see ITramiteLocal#eliminarTramitesDepuracion(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public void eliminarTramiteInconsistencia(List<TramiteInconsistencia> inconsistencias) {
        try {
            this.tramiteInconsistenciaDAO.deleteMultiple(inconsistencias);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION;
        }

    }
//--------------------------------------------------------------------------------------------------        

    /**
     * @see ITramiteLocal#actualizarTramitesDepuracion(java.util.List, java.lang.String,
     * java.lang.Long)
     *
     */
    @Override
    public void actualizarTramitesDepuracion(List<TramiteInconsistencia> inconsistencias,
        String actividadActual, Long tramiteId, UsuarioDTO usuario) {

        try {
            //Elimina las inconsistencias anteriores
            if (!actividadActual.equals(
                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) &&
                !actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) &&
                !actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {
                this.eliminarTramitesInconsistencia(tramiteId);
            } else {
                List<TramiteInconsistencia> inconsistenciasControlCalidad =
                    this.tramiteInconsistenciaDAO.buscarDepuradaByTramiteId(tramiteId, "CC");
                this.tramiteInconsistenciaDAO.deleteMultiple(inconsistenciasControlCalidad);
            }

            //Persisten las inconsistencias enviadas desde el editor
            for (TramiteInconsistencia unaInconsistencia : inconsistencias) {
                unaInconsistencia.setTramite(this.tramiteDao.findById(tramiteId));
                unaInconsistencia.setUsuarioLog(usuario.getLogin());
                unaInconsistencia.setFechaLog(new Date());
                unaInconsistencia.setTramiteDepuracion(this.
                    buscarUltimoTramiteDepuracionPorIdTramite(tramiteId));
                if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) ||
                    actividadActual.equals(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) ||
                    actividadActual.equals(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {
                    unaInconsistencia.setDepurada("CC");
                } else {
                    unaInconsistencia.setDepurada("NO");
                }
                this.guardarActualizarTramiteInconsistencia(unaInconsistencia);
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see
     * ITramiteLocal#guardarActualizarTramite2(co.gov.igac.snc.persistence.entity.tramite.Tramite)
     * @author pedro.garcia
     */
    /*
     * OJO: no adicionar nada a este método!!
     */
    @Implement
    @Override
    public Tramite guardarActualizarTramite2(Tramite tramite) {

        LOGGER.debug("Inicia En TramiteBean#guardarActualizarTramite2 ...");

        try {
            Tramite auxTramite = this.tramiteDao.update(tramite);
            tramite.setId(auxTramite.getId());
            tramite.setTramiteInfoAdicionals(auxTramite.getTramiteInfoAdicionals());
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#guardarActualizarTramite2: " + ex.getMessage());
        }
        LOGGER.debug("... TramiteBean#guardarActualizarTramite2 termina.");

        return tramite;
    }

    /**
     * @see ITramite#buscarTramiteDigitalizacionPorTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDigitalizacion> buscarTramiteDigitalizacionPorTramite(Long idTramite) {
        List<TramiteDigitalizacion> resultado;

        try {
            resultado = this.tramiteDigitalizacionDAO.buscarPorIdTramite(idTramite);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#buscarTramiteDigitalizacionPorDigitalizador
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDigitalizacion> buscarTramiteDigitalizacionPorDigitalizador(
        String digitalizador) {
        List<TramiteDigitalizacion> resultado;

        try {
            resultado = this.tramiteDigitalizacionDAO.buscarPorDigitalizador(digitalizador);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#contarTramiteDigitalizacionPorDigitalizador
     * @author felipe.cadena
     */
    @Implement
    @Override
    public Integer contarTramiteDigitalizacionPorDigitalizador(String digitalizador) {
        List<TramiteDigitalizacion> resultado;

        try {
            resultado = this.tramiteDigitalizacionDAO.buscarPorDigitalizador(digitalizador);
            if (resultado != null) {
                return resultado.size();
            } else {
                return 0;
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#guardarActualizarTramiteDigitalizacion
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDigitalizacion guardarActualizarTramiteDigitalizacion(
        TramiteDigitalizacion tramiteDigitalizacion) {
        TramiteDigitalizacion resultado;

        try {
            resultado = this.tramiteDigitalizacionDAO.update(tramiteDigitalizacion);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#guardarActualizarTramiteDigitalizacion
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDigitalizacion buscarTramiteDigitalizacionActivaTramite(String usuario,
        Long idTramite) {
        TramiteDigitalizacion resultado;

        try {
            resultado = this.tramiteDigitalizacionDAO.buscarPorDigitalizadorYTramite(usuario,
                idTramite);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see ITramite#buscarTramiteDigitalizacionPorEjecutor
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDigitalizacion buscarTramiteDigitalizacionPorEjecutor(String ejecutor,
        Long idTramite) {
        TramiteDigitalizacion resultado;

        try {
            resultado = this.tramiteDigitalizacionDAO.buscarPorEjecutorYTramite(ejecutor, idTramite);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramite#buscarTramiteDigitalizacionActivaTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDetallePredio buscarTramiteDetallePredioPorTramiteIdYPredioId(Long idTramite,
        Long idpredio) {
        TramiteDetallePredio resultado = null;

        try {
            resultado = this.tramiteDetallePredioDao.getByTramiteIdYPredioId(idTramite, idpredio);

        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error al buscar el TramiteDetallePredio");
        }

        return resultado;

    }

    //--------------------------------------------------------------------------------------------------    
    /**
     * @see ITramite#buscarComisionesTramitePorComisionId
     * @author juanfelipe.garcia
     */
    @Implement
    @Override
    public List<ComisionTramite> buscarComisionesTramitePorComisionId(Long idComision) {
        List<ComisionTramite> resultado;
        try {
            resultado = this.comisionTramiteDao.findComisionesTramiteByComisionId(idComision);
            return resultado;
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author juanfelipe.garcia
     * @see ITramite#actualizarTramitesAdicionales(List)
     */
    @Implement
    @Override
    public List<Tramite> actualizarTramitesAdicionales(List<Tramite> tramites) {

        LOGGER.debug("TramiteBean#actualizarTramitesAdicionales");
        List<Tramite> tramitesResult = new ArrayList<Tramite>();
        for (Tramite tramite : tramites) {
            try {
                if (tramite.getId() == null && tramite.isSegundaEnglobe()) {
                    List<TramitePredioEnglobe> predioListTemp = tramite.getTramitePredioEnglobes();
                    List<TramitePredioEnglobe> traDetPredioList =
                        new ArrayList<TramitePredioEnglobe>();
                    tramite.setTramitePredioEnglobes(null);
                    tramite = this.tramiteDao.update(tramite);

                    for (TramitePredioEnglobe tpe : predioListTemp) {
                        tpe.setTramite(tramite);
                        traDetPredioList.add(tpe);
                    }
                    tramite.setTramitePredioEnglobes(traDetPredioList);
                }
                Tramite tramiteTemp = this.tramiteDao.update(tramite);
                tramitesResult.add(tramiteTemp);

                //tramite.setId(tramiteTemp.getId());
            } catch (ExcepcionSNC ex) {
                LOGGER.error(
                    "error en TramiteBean#actualizarTramitesAdicionales, actualizando trámite con " +
                    "id: " + tramite.getId() + ": " + ex.getMensaje(), ex);
            }
        }
        return tramitesResult;
    }

    // ------------------------------------------------------------------- //
    /**
     * @see ITramiteLocal#enviarTramitesAsociadosAModificarInformacionAlfanumerica(String, Tramite,
     * UsuarioDTO)
     * @author david.cifuentes
     */
    @Override
    public void enviarTramitesAsociadosAModificarInformacionAlfanumerica(
        String activityId, Tramite tramite, UsuarioDTO usuario) {

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuarios = new LinkedList<UsuarioDTO>();

        if (tramite.getFuncionarioEjecutor() != null) {
            usuarios.add(this.generalesService.getCacheUsuario(tramite
                .getFuncionarioEjecutor()));
        } else {
            usuarios.add(usuario);
        }
        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);

        solicitudCatastral.setUsuarios(usuarios);

        this.procesosService.avanzarActividad(usuario, activityId,
            solicitudCatastral);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#obtenerUltimoComisionEstadoDeComision(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ComisionEstado obtenerUltimoComisionEstadoDeComision(Long idComision,
        String estadoComision) {

        ComisionEstado answer = null;

        try {
            answer = this.comisionEstadoDao.obtenerUltimoDeComision(idComision, estadoComision);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#obtenerUltimoComisionEstadoDeComision: " +
                ex.getMensaje());
        }
        return answer;
    }

    /**
     * @see ITramiteLocal#obtenerTramitesGeograficosPorManzana(java.lang.String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Tramite> obtenerTramitesGeograficosPorManzana(String numeroManzana) {

        List<Tramite> resultadoFinal = new ArrayList<Tramite>();

        try {
            List<Tramite> resultado = this.tramiteDao.obtenerTramitesPorManzanaEnEdicion(
                numeroManzana);

            for (Tramite tramite : resultado) {
                if (tramite.isTramiteGeografico()) {
                    resultadoFinal.add(tramite);
                }
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#obtenerTramitesGeograficosPorManzana: " +
                ex.getMensaje());
        }
        return resultadoFinal;
    }

    /**
     * @see ITramiteLocal#obtenerTramitesPorManzanaPosteriores(java.lang.String, Actividad)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Tramite> obtenerTramitesPorManzanaPosteriores(String numeroManzana) {

        List<Tramite> tramitesManzana = new ArrayList<Tramite>();

        try {
            //se obtienen los tramites geograficos de la manzana, los cuales 
            //son los tramites de la manzana que tienen replica de edicion
            tramitesManzana = this.obtenerTramitesGeograficosPorManzana(numeroManzana);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#obtenerTramitesPorManzanaPosteriores: " +
                ex.getMensaje());
        }
        return tramitesManzana;
    }

    /**
     * @see ITramiteLocal#obtenerTramitesPorManzanaPosteriores(java.lang.String)
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<Tramite> obtenerTramitesPorManzanaDepuracion(String numeroManzana) {

        List<Tramite> tramitesManzana;
        List<Tramite> tramitesPosteriores = new ArrayList<Tramite>();
        String idsInstanciaProcesosTramitesManzana = "";
        List<Long> idsTramitesPosteriores = new ArrayList<Long>();
        Map<EParametrosConsultaActividades, String> filtros;

        try {
            //se obtienen los tramites de depuracion de la manzana que tienen replica de edicion
            tramitesManzana = this.tramiteDao.obtenerTramitesPorManzanaDepuracionEnEdicion(
                numeroManzana);

            if (tramitesManzana.isEmpty()) {
                return tramitesPosteriores;
            }

            for (Tramite t : tramitesManzana) {

                idsInstanciaProcesosTramitesManzana = t.getProcesoInstanciaId().toString();
                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(EParametrosConsultaActividades.IDS_INSTANCIAS_PROCESOS,
                    idsInstanciaProcesosTramitesManzana);
                //filtros.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD, actividad.getNombre());

                //Se obtienen solo las actividades que actualmente estan en depuracion
                List<Actividad> actividades = this.procesosService
                    .consultarListaActividades(filtros);

                if (actividades != null && !actividades.isEmpty()) {
//                    int len = actividades.size();
//                    Actividad act = actividades.get(len - 1);
                    //@modified by hector.arias::18773::30/08/2017::Se consulta la actividad además                    
                    //de ser de depuración también por estado
                    for (Actividad actividadTemp : actividades) {
                    	if (actividadTemp!=null && actividadTemp.getNombre() != null && actividadTemp.getNombre().contains(
                            "Depuración") && (actividadTemp.getEstado()!=null &&
                            actividadTemp.getEstado().equals("Reclamada") || actividadTemp.
                            getEstado().equals("Por Reclamar"))) {
                            idsTramitesPosteriores.add(actividadTemp.getIdObjetoNegocio());
                        }
                    }
                }
            }

            //se consulta la informacion de los tramites
            if (idsTramitesPosteriores.size() > 0) {
                tramitesPosteriores = this.tramiteDao.findTramitesByIds(idsTramitesPosteriores);
            }

        } catch (Exception ex) {
        	ex.printStackTrace();
            LOGGER.error("Error en TramiteBean#obtenerTramitesPorManzanaPosteriores: " +
                ex.getMessage());
        } 
        return tramitesPosteriores;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @see ITramiteLocal#guardarActualizarSolicitud2(co.gov.igac.generales.dto.UsuarioDTO,
     * co.gov.igac.snc.persistence.entity.tramite.Solicitud)
     * @author pedro.garcia
     */
    @Override
    public Object[] guardarActualizarSolicitud2(UsuarioDTO usuario, Solicitud solicitud) {

        String fileSeparator = System.getProperty("file.separator");

        Object[] answer = null;
        HashMap<Integer, ArrayList<String>> mensajes;
        StringBuilder mensajesError, mensajesWarning;
        boolean errorBD;
        String mensajeTemp;

        mensajes = new HashMap<Integer, ArrayList<String>>();
        answer = new Object[2];
        mensajesError = new StringBuilder();
        mensajesWarning = new StringBuilder();
        errorBD = false;

//TODO :: fredy.wilches :: llamar SP o WS de correspondencia
        if (solicitud.getNumero() == null) {
            Object[] resultado = this.obtenerNumeroSolicitud(solicitud, usuario);
            if (resultado != null && resultado.length > 0 && !((new BigDecimal(-1)).equals(
                resultado[0]))) {
                solicitud.setNumero(resultado[5].toString());
            } else {
                mensajeTemp = "Solicitud no numerada por error en la radicación con correspondencia";
                LOGGER.error(mensajeTemp);
                LOGGER.error("" + resultado[1]);
                LOGGER.error("" + resultado[4]);
                mensajesError.append(mensajeTemp);
            }
        }

        if (solicitud.getTipo() == null) {
            solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo()); // Conservación
        }

        if (solicitud.getFecha() == null) {
            solicitud.setFecha(new Date());
        }

        if (solicitud.getEstado() == null) {
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.getCodigo()); // Recibida
        }

        if (solicitud.getFinalizado() == null) {
            solicitud.setFinalizado(ESiNo.NO.getCodigo());
        }

        solicitud.setFechaLog(new Date());
        solicitud.setUsuarioLog(usuario.getLogin());

        if (solicitud.getSolicitanteSolicituds() != null) {
            for (SolicitanteSolicitud solSol : solicitud
                .getSolicitanteSolicituds()) {
                solSol.setFechaLog(new Date());
                solSol.setUsuarioLog(usuario.getLogin());
            }
        }

        if (solicitud.getTramites() != null &&
            !solicitud.getTramites().isEmpty()) {

            for (Tramite t : solicitud.getTramites()) {

                t.setSolicitud(solicitud);
                t.setEstado(ETramiteEstado.RECIBIDO.getCodigo()); // Recibido
                t.setArchivado(ESiNo.NO.getCodigo());
                t.setUsuarioLog(usuario.getLogin());
                t.setFechaLog(new Date());

                if (t.getNumeroRadicacion() == null) {
                    Object[] resultado;
                    if (t.getTipoTramite().equals(
                        ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
                        resultado = this.generalesService
                            .generarNumeracion(
                                ENumeraciones.NUMERACION_RADICACION_DE_REVISION_DE_AVALUO,
                                "0", t.getDepartamento().getCodigo(),
                                t.getMunicipio().getCodigo(),
                                Constantes.NUMERACION_AVALUO_TIPO_DOCUMENTO);
                    } else if (t.getTipoTramite().equals(
                        ETramiteTipoTramite.AUTOESTIMACION.getCodigo())) {
                        resultado = null;
                    } else {
                        resultado = this.generalesService.generarNumeracion(
                            ENumeraciones.NUMERACION_RADICACION_CATASTRAL,
                            "0", t.getDepartamento().getCodigo(), t
                            .getMunicipio().getCodigo(), 0);

                    }
                    if (resultado != null && resultado.length > 0) {
                        t.setNumeroRadicacion(resultado[0].toString());
                    }
                }

                if (t.getTramiteDocumentacions() != null &&
                    !t.getTramiteDocumentacions().isEmpty()) {
                    for (TramiteDocumentacion td : t.getTramiteDocumentacions()) {
                        td.setTramite(t);
                        td.setUsuarioLog(usuario.getLogin());
                        td.setFechaLog(new Date());

                        if (td.getDocumentoSoporte() != null) {
                            Documento d = td.getDocumentoSoporte();

                            if (d.getArchivo() == null ||
                                d.getArchivo().isEmpty()) {
                                d.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                            }

                            if (t.getPredio() != null) {
                                d.setPredioId(t.getPredio().getId());
                            }

                            d.setTramiteId(t.getId());
                            d.setBloqueado(ESiNo.NO.getCodigo());
                            d.setUsuarioCreador(usuario.getLogin());
                            d.setFechaRadicacion(t.getFechaRadicacion());
                            d.setUsuarioLog(usuario.getLogin());
                            d.setFechaLog(new Date());

                            Documento newD = null;

                            try {
                                newD = this.documentoDao.update(d);

                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                errorBD = true;
                            }
                            if (newD != null && newD.getId() != null) {
                                d.setId(newD.getId());
                            }
                        }
                    }
                }

                if (t.getTramitePredioEnglobes() != null &&
                    t.getTramitePredioEnglobes().size() > 0) {

                    for (TramitePredioEnglobe tpe : t
                        .getTramitePredioEnglobes()) {
                        tpe.setTramite(t);
                        tpe.setUsuarioLog(usuario.getLogin());
                        tpe.setFechaLog(new Date());
                    }
                }

                if (t.getSolicitanteTramites() != null) {
                    for (SolicitanteTramite solTram : t
                        .getSolicitanteTramites()) {
                        solTram.setFechaLog(new Date());
                        solTram.setUsuarioLog(usuario.getLogin());
                    }
                }

            }
        }

        Solicitud sol = null;
        List<Long> tramitesAlmacenados = new ArrayList<Long>();

        for (Tramite t : solicitud.getTramites()) {
            if (t.getId() != null) {
                tramitesAlmacenados.add(t.getId());

            } else if (t.getPredio() != null &&
                "Sin registro".equals(t.getPredio()
                    .getCondicionPropiedad())) {
                // Ajuste realizado debido a que se estaba intentando persistir
                // predios vacíos, utilizados como comodín en unas consultas.
                t.setPredio(null);
            }
        }
        try {
            // D: al hacer el update se pierde la relación con los objetos a los que les hizo fetch
            // por lo que hay que manejar un objeto temporal para hacer el
            // update y con el original se puede seguir manajendo los objetos relacionados
            sol = this.transaccionalService.updateSolicitudNuevaTransaccion(solicitud);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            errorBD = true;
        }

        try {

            if (sol != null) {
                solicitud.setId(sol.getId());
                if (solicitud.getSolicitudAvisoRegistro() != null) {
                    solicitud.getSolicitudAvisoRegistro().setId(
                        sol.getSolicitudAvisoRegistro().getId());
                    if (solicitud.getSolicitudAvisoRegistro().getId() != null) {
                        List<AvisoRegistroRechazo> avisoRegistroRechazosAux =
                            this.avisoRegistroRechazoDao
                                .findBySolicitudAvisoRegistroId(solicitud
                                    .getSolicitudAvisoRegistro().getId());
                        solicitud.getSolicitudAvisoRegistro()
                            .setAvisoRegistroRechazos(avisoRegistroRechazosAux);
                    }
                }

                solicitud.setSolicitanteSolicituds(solicitanteSolicitudDao
                    .findBySolicitudId(sol.getId()));
            }

            if (sol.getTramites() != null) {
                TramiteEstado te = null;

                Solicitud solTraTemp = this.transaccionalService
                    .buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion(solicitud
                        .getId());

                if (solTraTemp != null) {
                    solicitud.setId(solTraTemp.getId());
                    solicitud.setTramites(solTraTemp.getTramites());

                    if (solTraTemp.getTramites() != null &&
                        solTraTemp.getTramites().size() > 0) {

                        for (int i = 0; i < solTraTemp.getTramites().size(); i++) {

                            Tramite tramiteAux = solTraTemp.getTramites().get(i);

                            if (tramiteAux.getId() != null &&
                                !tramitesAlmacenados.contains(solTraTemp
                                    .getTramites().get(i).getId())) {

                                if (tramiteAux.getSolicitanteTramites() != null &&
                                    !tramiteAux.getSolicitanteTramites().isEmpty()) {
                                    tramiteAux.setSolicitanteTramites(
                                        this.solicitanteTramiteDao.findBySolicitudId(sol.getId()));
                                }

                                if (tramiteAux.getTramiteDocumentacions() != null &&
                                    tramiteAux.getTramiteDocumentacions().size() > 0) {

                                    for (TramiteDocumentacion td : solTraTemp
                                        .getTramites().get(i).getTramiteDocumentacions()) {

                                        if (td.getDocumentoSoporte() != null &&
                                            td.getDocumentoSoporte().getId() != null &&
                                            td.getDocumentoSoporte().getArchivo() != null &&
                                            !td.getDocumentoSoporte().getArchivo().isEmpty() &&
                                            !td.getDocumentoSoporte().getArchivo()
                                                .equals(Constantes.CONSTANTE_CADENA_VACIA_DB) &&
                                            (td.getDocumentoSoporte()
                                                .getIdRepositorioDocumentos() == null ||
                                            td.getDocumentoSoporte()
                                                .getIdRepositorioDocumentos().isEmpty())) {

                                            String nombreDepartamento = "";
                                            String nombreMunicipio = "";
                                            String numeroPredial = "";

                                            if (td.getDepartamento() != null) {
                                                nombreDepartamento = td.getDepartamento().
                                                    getNombre();
                                            } else {
                                                nombreDepartamento = tramiteAux
                                                    .getDepartamento().getNombre();
                                            }

                                            if (td.getMunicipio() != null) {
                                                nombreMunicipio = td.getMunicipio().getNombre();
                                            } else {
                                                nombreMunicipio = tramiteAux
                                                    .getMunicipio().getNombre();
                                            }

                                            if (tramiteAux.getPredio() != null) {
                                                numeroPredial = tramiteAux.getPredio().
                                                    getNumeroPredial();
                                            }

                                            DocumentoTramiteDTO documentoTramiteDTO =
                                                new DocumentoTramiteDTO(
                                                    FileUtils.getTempDirectory().getAbsolutePath() +
                                                    fileSeparator +
                                                    td.getDocumentoSoporte().getArchivo(),
                                                    td.getDocumentoSoporte().getTipoDocumento().
                                                        getNombre(),
                                                    tramiteAux.getFechaRadicacion(),
                                                    nombreDepartamento, nombreMunicipio,
                                                    numeroPredial,
                                                    tramiteAux.getTipoTramite(),
                                                    tramiteAux.getId());

                                            // Cuando es una solicitud de via gubernativa o es un trámite de mutación de quinta,
                                            // el campo número predial del documentoTramiteDTO puede ir nulo.
                                            if (!solTraTemp.isViaGubernativa() &&
                                                !tramiteAux.isQuinta()) {
                                                try {
                                                    this.documentoDao.
                                                        guardarDocumentoGestorDocumental(
                                                            usuario, documentoTramiteDTO,
                                                            td.getDocumentoSoporte());
                                                } catch (ExcepcionSNC ex) {
                                                    mensajeTemp =
                                                            "Ocurrió un error guardando documento de la solicitud, por favor adjúntelo nuevamente en control de calidad";
                                                    LOGGER.error(mensajeTemp);
                                                    mensajesWarning.append(mensajeTemp);
                                                }
                                            } else {
                                                try {
                                                    this.documentoDao
                                                        .guardarDocumentoGestorDocumentalSinNumeroPredial(
                                                            usuario, documentoTramiteDTO,
                                                            td.getDocumentoSoporte());
                                                } catch (ExcepcionSNC ex) {
                                                    mensajeTemp =
                                                            "Ocurrió un error guardando documento de la solicitud, por favor adjúntelo nuevamente en control de calidad";
                                                    LOGGER.error(mensajeTemp);
                                                    mensajesWarning.append(mensajeTemp);
                                                }
                                            }

                                        }
                                    }

                                    te = new TramiteEstado();
                                    te.setEstado(tramiteAux.getEstado());

                                    te.setTramite(tramiteAux);
                                    te.setResponsable(usuario.getLogin());
                                    te.setFechaInicio(new Date());
                                    te.setUsuarioLog(usuario.getLogin());
                                    te.setFechaLog(new Date());

                                    try {

                                        @SuppressWarnings("unused")
                                        boolean r = this.tramiteEstadoDao
                                            .crearActualizarTramiteEstado(te, usuario);
                                    } catch (Exception e) {
                                        LOGGER.error(e.getMessage(), e);
                                        errorBD = true;
                                    }

                                }
                            }
                        }
                    }
                }
            }
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        if (errorBD) {
            mensajesError.append(";");
            mensajesError.append("Ocurrió un error de actualización de datos.");
        }
        if (!mensajesError.toString().isEmpty()) {
            //david.murcia Se quita la siguiente línea debido a que se presenta un error por el cast
            //mensajes.put(new Integer(Level.ERROR_INT), (ArrayList<String>) Arrays.asList(mensajesError.toString().split(";")));
            ArrayList<String> msg = new ArrayList<String>();
            Collections.addAll(msg, mensajesError.toString().split(";"));
            mensajes.put(new Integer(Level.ERROR_INT), msg);
        }
        if (!mensajesWarning.toString().isEmpty()) {

            //david.murcia Se quita la siguiente línea debido a que se presenta un error por el cast
            //mensajes.put(new Integer(Level.WARN_INT), (ArrayList<String>) Arrays.asList(mensajesWarning.toString().split(";")));
            ArrayList<String> msg = new ArrayList<String>();
            Collections.addAll(msg, mensajesWarning.toString().split(";"));
            mensajes.put(new Integer(Level.WARN_INT), msg);
        }

        answer[0] = solicitud;
        answer[1] = mensajes;

        return answer;

    }

    // ----------------------------------------- //
    /**
     * @see ITramite#buscarTramiteDepuracionPorDigitalizador
     * @author david.cifuentes
     */
    @Implement
    @Override
    public List<TramiteDepuracion> buscarTramiteDepuracionPorDigitalizador(
        String digitalizador) {

        List<TramiteDepuracion> resultado;

        try {
            resultado = this.tramiteDepuracionDAO
                .buscarTramiteDepuracionPorDigitalizador(digitalizador);
            return resultado;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    // ----------------------------------------- //
    /**
     * Método que clasifica los id's de trámites que no tienen interpuesta una vía gubernativa.
     *
     * @author david.cifuentes
     */
    public List<Long> excluirIdsTramitesConViaGubernativaInterpuesta(List<Long> tramiteIds) {

        List<Tramite> tramitesConViaGubernativa;

        try {

            tramitesConViaGubernativa = this.tramiteDao
                .buscarTramiteConViaGubernativaPorIdsTramites(tramiteIds);

            if (tramitesConViaGubernativa != null && !tramitesConViaGubernativa.isEmpty()) {
                for (Tramite t : tramitesConViaGubernativa) {
                    tramiteIds.remove(Long.valueOf(t.getId()));
                }
                return tramiteIds;
            }
            return tramiteIds;

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see ITramite#buscarDocumentacionPorSolicitudId(Long)
     * @author javier.aponte
     */
    @Implement
    @Override
    public List<Documento> buscarDocumentacionPorSolicitudId(Long id) {
        List<Documento> resultado = null;
        try {
            resultado = this.documentoDao.buscarDocumentacionPorSolicitudId(id);
            return resultado;
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("error en TramiteBean#buscarDocumentacionPorSolicitudId " + ex.getMensaje());
            return resultado;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#consultarTramiteDevuelto(java.lang.Long)
     * @author pedro.garcia
     */
    @Override
    public Tramite consultarTramiteDevuelto(Long tramiteId) {

        LOGGER.debug("en TramiteBean#consultarTramiteDevuelto ...");

        Tramite answer = null;

        try {
            answer = this.tramiteDao.consultarParaTramiteDevuelto(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#consultarTramiteDevuelto: " + ex.getMensaje());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ITramiteLocal#consultarComisionesActivasTramite(java.lang.Long, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<Comision> consultarComisionesActivasTramite(Long idTramite, String tipoComision) {

        LOGGER.debug("En TramiteBean#consultarComisionesActivasTramite ");

        List<Comision> answer = null;

        try {
            answer = this.comisionDao.consultarActivasPorTramite(idTramite, tipoComision);
        } catch (ExcepcionSNC sncEx) {
            LOGGER.error("Error en TramiteBean#consultarComisionesActivasTramite: " +
                sncEx.getMensaje());
        }

        return answer;
    }

    /**
     * Método que ejecuta un SP para generar la proyección de un trámite
     */
    @Override
    public Object[] recurrirProyeccion(Long tramiteId, Long tramiteRecurridoId, String tipoRecurso,
        String usuario) {
        try {
            return this.sncProcedimientoDao.recurrirProyeccion(tramiteId, tramiteRecurridoId,
                tipoRecurso, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    // --------------------------------------------------------- //
    /**
     * @see ITramiteLocal#eliminarTramiteInconsistencia(TramiteInconsistencia)
     * @author david.cifuentes
     */
    @Override
    public boolean eliminarTramiteInconsistencia(
        TramiteInconsistencia tramiteInconsistencia) {
        try {
            this.tramiteInconsistenciaDAO.delete(tramiteInconsistencia);
            return true;
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    // --------------------------------------------------------- //
    /**
     * @see ITramiteLocal#verificarTramitesConRegistroTramiteDepuracion(List<Long>)
     * @author david.cifuentes
     */
    public List<Long> verificarTramitesConRegistroTramiteDepuracion(
        List<Long> tramiteIds) {

        List<Long> tramiteIdsProvDepuracion = null;
        try {
            tramiteIdsProvDepuracion = this.tramiteDepuracionDAO
                .verificarTramitesConRegistroTramiteDepuracion(tramiteIds);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return tramiteIdsProvDepuracion;
    }

    // -------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#contarSolicitudesByFiltroSolicitud (filtroDatosSolicitud)
     * @author javier.aponte
     * @version 2.0
     */
    @Override
    public Integer contarSolicitudesByFiltroSolicitud(
        FiltroDatosSolicitud filtroDatosSolicitud) {
        return this.solicitudDao.contarSolicitudesByFiltroSolicitud(filtroDatosSolicitud);
    }

    /**
     * Obtener lista de tareas geograficas pendientes.
     *
     * @author andres.eslava
     */
    @Override
    public ResultadoVO obtenerTareasGeograficas3(UsuarioDTO usuario) {

        LOGGER.info("Inicio generacion XML de lista de tareas para el usuario: " +
            usuario.getLogin());

        ResultadoVO resultadoTareasPendientes;

        List<Actividad> actividades = new ArrayList<Actividad>();

        //List<Actividad> lstActividades = this.procesosService.consultarListaActividades(usuario);
        List<Actividad> lstActividades = this.procesosService.
            consultarListaActividadesPorActividades(usuario);

        if (lstActividades != null) {
            for (Actividad act : lstActividades) {
                if (act.getNombre()
                    .equals(ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA) ||
                    act.getNombre()
                        .equals(
                            ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA) ||
                    act.getNombre()
                        .equals(ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                    act.getNombre()
                        .equals(ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                    act.getNombre()
                        .equals(ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {
                    actividades.add(act);
                }
            }
        }

        // No hay actividades para edicion geografica
        if (actividades.isEmpty() || (lstActividades == null || lstActividades.isEmpty())) {
            ResultadoVO vo = new ResultadoVO(usuario,
                new ArrayList<ActividadVO>());
            return vo;
        }

        LOGGER.info("Las actividades pendientes para edicion geografica son: " +
            actividades.size() + ". Usuario: " + usuario.getLogin());
        // ActividadesVO para mostrar en el XML
        List<ActividadVO> actividadesVo = new LinkedList<ActividadVO>();
        try {
            for (Actividad actividad : actividades) {
                Tramite tramiteAsociado = this.tramiteDao.getTramiteParaEdicionGeografica(actividad.
                    getIdObjetoNegocio());

                ProductoCatastralJob pcj = productoCatastralJobDAO.
                    obtenerUltimoJobObtenerZonasSinTerminarGeograficamente(tramiteAsociado.getId());
                if (pcj == null) {
                    ActividadVO actividadVo = new ActividadVO(actividad, tramiteAsociado);
                    actividadesVo.add(actividadVo);
                }
            }
            resultadoTareasPendientes = new ResultadoVO(usuario, actividadesVo);
            return resultadoTareasPendientes;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA
                .getExcepcion(LOGGER, e);
        }
    }

    /**
     * @see ITramiteLocal#obtenerInformacionDetalladaEdicionGeografica(java.lang.Long)
     * @modified andres.eslava::refs #7036::06/Mar/2014
     * @modified andres.eslava::refs #9272::11/sept/2014 se agrega informacion en el caso
     * rectificacion
     * @modified andres.eslava::refs #12603::15/05/2015 se agrega soporte a tramite desenglobe por
     * etapas.
     * @modified andres.eslava::refs #14041::26/08/2015 se agrega a soporte quintas masivos
     */
    @Override
    public ResultadoVO obtenerInformacionDetalladaEdicionGeografica(Long tramiteId,
        UsuarioDTO usuario) {
        LOGGER.info("Inicio generacion XML de lista de tareas para el usuario: " + usuario.
            getLogin());

        //Lista de actividades obtenidas desde el id tramite.
        List<Actividad> actividades = null;
        //ActividadesVO para mostrar en el XML
        List<ActividadVO> actividadesVo = new LinkedList<ActividadVO>();
        //objeto serializable que contiene la respuesta en este caso la lista de actividades pendientes.
        ResultadoVO resultadoTareasPendientes;

        //se obtiene las actividades del tramite
        actividades = this.procesosService.getActividadesPorIdObjetoNegocio(tramiteId);

        Actividad actividad = null;
        for (Actividad actividadEnLista : actividades) {
            //verificacion que la actividad se la edicion geografica
            if (actividadEnLista.getNombre().equals(
                ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA) ||
                actividadEnLista.getNombre().equals(
                    ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA) ||
                actividadEnLista.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                actividadEnLista.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                actividadEnLista.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {

                actividad = actividadEnLista;
            }
        }

        //Caso de excepcion por no encontrar la tarea en la actividad correspondiente.
        if (actividad == null) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA.
                getExcepcion(LOGGER,
                    new Exception("No se encuentra en la actividad correspondiente"));
        }

        // Empieza el levantamiento de informacion de acuerdo al tramite.
        try {
            if (actividad != null) {

                Tramite tramiteAsociado = this.tramiteDao.getTramiteParaEdicionGeografica(actividad.
                    getIdObjetoNegocio());
                ActividadVO actividadVo = new ActividadVO(actividad, tramiteAsociado);
                //Construye la informacion necesaria para el editor terminos del Tipo tramite, clase mutacion y subtipo

                //Caso Depuracion
                if (actividad.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                    actividad.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                    actividad.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {

                    PredioInfoVO predioAsociadoTramite;
                    List<PredioInfoVO> prediosAsociadosTramite;
                    List<TramiteInconsistencia> inconsistencias;
                    List<TramiteInconsistenciaVO> inconsistenciasVo =
                        new ArrayList<TramiteInconsistenciaVO>();

                    if (tramiteAsociado.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.
                        getCodigo())) {
                        Predio predioOriginal;
                        predioOriginal = tramiteAsociado.getPredio();
                        PredioInfoVO predioResultante = new PredioInfoVO(predioOriginal,
                            tramiteAsociado);
                        if (predioOriginal.isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(predioOriginal.
                                    getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioResultante.setFichaMatriz(fichaVO);
                            }
                        }
                        actividadVo.setPredio(predioResultante);
                    }
                    if (tramiteAsociado.getSubtipo() != null && tramiteAsociado.getSubtipo().equals(
                        EMutacion2Subtipo.ENGLOBE.getCodigo())) {

                        prediosAsociadosTramite = new ArrayList<PredioInfoVO>();
                        for (Predio predio : predioDao.obtenerPrediosCompletosEnglobePorTramiteId(
                            tramiteAsociado.getId())) {
                            PredioInfoVO predioVo = new PredioInfoVO(predio, tramiteAsociado);
                            if (predio.isEsPredioEnPH()) {
                                FichaMatriz ficha = this.conservacionService.
                                    getFichaMatrizByNumeroPredialPredio(predio.getNumeroPredial());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    predioVo.setFichaMatriz(fichaVO);
                                }
                            }
                            prediosAsociadosTramite.add(predioVo);
                        }
                        actividadVo.setPrediosOriginales(prediosAsociadosTramite);
                    } else if (!(tramiteAsociado.getClaseMutacion() != null && tramiteAsociado.
                        getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo()))) {
                        predioAsociadoTramite = new PredioInfoVO(tramiteAsociado.getPredio(),
                            tramiteAsociado);
                        if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(tramiteAsociado.getPredio().
                                    getNumeroPredial());
                            if (ficha != null) {
                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                predioAsociadoTramite.setFichaMatriz(fichaVO);
                            }
                        }
                        actividadVo.setPredioOriginal(predioAsociadoTramite);
                    } else {
                        //Casi quinta en depuracion
                        predioAsociadoTramite = new PredioInfoVO();
                        predioAsociadoTramite.setNumeroPredial(tramiteAsociado.getNumeroPredial());
                        actividadVo.setPredioOriginal(predioAsociadoTramite);

                    }
                    inconsistencias = this.tramiteInconsistenciaDAO.buscarDepuradaByTramiteId(
                        tramiteAsociado.getId(), "NO");
                    for (TramiteInconsistencia tramiteInconsistencia : inconsistencias) {
                        TramiteInconsistenciaVO inconsistenciaVO = new TramiteInconsistenciaVO(
                            tramiteInconsistencia);
                        inconsistenciasVo.add(inconsistenciaVO);
                    }
                    actividadVo.setInconsistenciaTramiteVO(inconsistenciasVo);
                } //Caso Mutaciones
                else {
                    if (tramiteAsociado.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.
                        getCodigo())) {

                        if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.TERCERA.
                            getCodigo())) {
                            //caso Tercera no masivo
                            if (!tramiteAsociado.isTerceraMasiva()) {
                                PPredio infoPredioProyectado;
                                Predio infoPredioOriginal;
                                PredioInfoVO infPredialResultante;
                                PredioInfoVO infPredialOriginal;
                                infoPredioProyectado = this.pPredioDao.getPPrediosByTramiteId(
                                    tramiteAsociado.getId()).get(0);
                                infoPredioOriginal = tramiteAsociado.getPredio();
                                infPredialResultante = new PredioInfoVO(infoPredioProyectado,
                                    tramiteAsociado);
                                infPredialOriginal = new PredioInfoVO(infoPredioOriginal,
                                    tramiteAsociado);

                                if (infoPredioProyectado.isEsPredioEnPH()) {
                                    PFichaMatriz ficha = this.conservacionService.
                                        buscarPFichaMatrizPorPredioId(infoPredioProyectado.getId());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        infPredialResultante.setFichaMatriz(fichaVO);
                                    }
                                }
                                if (infoPredioOriginal.isEsPredioEnPH()) {
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(infoPredioOriginal.
                                            getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        infPredialOriginal.setFichaMatriz(fichaVO);
                                    }
                                }
                                //Se agrega a la actividad la informacion necesaria para la edicion de una mutacion de tercera
                                actividadVo.setPredioOriginal(infPredialOriginal);
                                actividadVo.setPredioResultante(infPredialResultante);
                            } //tercera masiva
                            else if (tramiteAsociado.isTerceraMasiva()) {
                                List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                                List<PredioInfoVO> listaPrediosResultantes =
                                    new ArrayList<PredioInfoVO>();
                                List<Predio> listaPrediosEnglobe = this.predioDao.
                                    obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.
                                        getId());
                                //obtiene la ficha del matriz
                                if (tramiteAsociado.getPredio() != null) {
                                    PredioInfoVO predioMatriz = new PredioInfoVO(tramiteAsociado.
                                        getPredio(), tramiteAsociado);
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(tramiteAsociado.
                                            getPredio().getNumeroPredial());
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    predioMatriz.setFichaMatriz(fichaVO);
                                    listaPrediosOriginales.add(predioMatriz);
                                }
                                for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                                    PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                        unPredioOriginalEnglobe, tramiteAsociado);
                                    listaPrediosOriginales.add(unPredioInfoVo);
                                }
                                //Determina los cancela i
                                List<EProyeccionCancelaInscribe> ci =
                                    new ArrayList<EProyeccionCancelaInscribe>();
                                ci.add(EProyeccionCancelaInscribe.CANCELA);
                                ci.add(EProyeccionCancelaInscribe.INSCRIBE);
                                ci.add(EProyeccionCancelaInscribe.MODIFICA);
                                List<PPredio> listaPPrediosDesenglobe = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId(),
                                        ci);
                                for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {
                                    PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                        unPredioProyectado, tramiteAsociado);
                                    if (unPredioProyectado.getId().equals(tramiteAsociado.
                                        getPredio().getId())) {
                                        if (unPredioProyectado.isEsPredioEnPH()) {
                                            PFichaMatriz ficha = this.conservacionService.
                                                buscarPFichaMatrizPorPredioId(unPredioProyectado.
                                                    getId());
                                            if (ficha != null) {
                                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                    fichaVO);
                                                predioProyectadoVO.setFichaMatriz(fichaVO);
                                            }
                                        }
                                    }
                                    listaPrediosResultantes.add(predioProyectadoVO);

                                }
                                actividadVo.setPrediosOriginales(listaPrediosOriginales);
                                actividadVo.setPrediosResultantes(listaPrediosResultantes);
                            }
                        } else if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.QUINTA.
                            getCodigo())) {
                            // caso quinta no masivo
                            if (!tramiteAsociado.isQuintaMasivo()) {
                                PPredio infoPredioProyectado;
                                PredioInfoVO infPredialResultante;
                                PredioInfoVO infPredioOriginal;

                                infoPredioProyectado = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId()).
                                    get(0);
                                infPredialResultante = new PredioInfoVO(infoPredioProyectado,
                                    tramiteAsociado);
                                if (infoPredioProyectado.isEsPredioEnPH()) {
                                    PFichaMatriz ficha = this.conservacionService.
                                        buscarPFichaMatrizPorPredioId(infoPredioProyectado.getId());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        infPredialResultante.setFichaMatriz(fichaVO);
                                    }
                                }
                                infPredioOriginal = new PredioInfoVO();
                                infPredioOriginal.setNumeroPredial(tramiteAsociado.
                                    getNumeroPredial());
                                actividadVo.setPredioOriginal(infPredioOriginal);
                                actividadVo.setPredioResultante(infPredialResultante);
                            } //Caso quinta  masivo
                            else if (tramiteAsociado.isQuintaMasivo()) {
                                List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                                List<PredioInfoVO> listaPrediosResultantes =
                                    new ArrayList<PredioInfoVO>();
                                //Predios Originales
                                List<Predio> listaPrediosEnglobe = this.predioDao.
                                    obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.
                                        getId());

                                if (tramiteAsociado.getPredio() != null) {
                                    PredioInfoVO predioMatriz = new PredioInfoVO(tramiteAsociado.
                                        getPredio(), tramiteAsociado);
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(predioMatriz.
                                            getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioMatriz.setFichaMatriz(fichaVO);
                                    }
                                    listaPrediosOriginales.add(predioMatriz);
                                }
                                for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                                    PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                        unPredioOriginalEnglobe, tramiteAsociado);
                                    listaPrediosOriginales.add(unPredioInfoVo);
                                }

                                List<EProyeccionCancelaInscribe> ci =
                                    new ArrayList<EProyeccionCancelaInscribe>();
                                ci.add(EProyeccionCancelaInscribe.CANCELA);
                                ci.add(EProyeccionCancelaInscribe.INSCRIBE);
                                ci.add(EProyeccionCancelaInscribe.MODIFICA);
                                //predios Resultantes
                                List<PPredio> listaPPrediosQuintaMasiva = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId(),
                                        ci);
                                for (PPredio unPredioProyectado : listaPPrediosQuintaMasiva) {
                                    PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                        unPredioProyectado, tramiteAsociado);
                                    if (unPredioProyectado.getNumeroPredial().substring(22, 30).
                                        equals("00000000")) {
                                        if (unPredioProyectado.isEsPredioEnPH()) {
                                            PFichaMatriz ficha = this.conservacionService.
                                                buscarPFichaMatrizPorPredioId(unPredioProyectado.
                                                    getId());
                                            if (ficha != null) {
                                                FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                                fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                    fichaVO);
                                                predioProyectadoVO.setFichaMatriz(fichaVO);
                                            }
                                        }
                                    }
                                    listaPrediosResultantes.add(predioProyectadoVO);
                                }
                                actividadVo.setPrediosOriginales(listaPrediosOriginales);
                                actividadVo.setPrediosResultantes(listaPrediosResultantes);
                            }
                        } else if (tramiteAsociado.getClaseMutacion().equals(EMutacionClase.SEGUNDA.
                            getCodigo())) {
                            if (tramiteAsociado.getSubtipo().equals("ENGLOBE")) {
                                List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                                PPredio predioProyectado;
                                PredioInfoVO predioProyectadoResultante = new PredioInfoVO();
                                List<Predio> listaPrediosEnglobe = this.predioDao.
                                    obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.
                                        getId());
                                for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                                    PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                        unPredioOriginalEnglobe, tramiteAsociado);
                                    if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                                        FichaMatriz ficha = this.conservacionService.
                                            getFichaMatrizByNumeroPredialPredio(tramiteAsociado.
                                                getPredio().getNumeroPredial());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            unPredioInfoVo.setFichaMatriz(fichaVO);
                                        }
                                    }
                                    listaPrediosOriginales.add(unPredioInfoVo);
                                }
                                predioProyectado = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId()).
                                    get(0);
                                predioProyectadoResultante = new PredioInfoVO(predioProyectado,
                                    tramiteAsociado);
                                if (this.conservacionService.obtenerPPredioCompletoByIdTramite(
                                    tramiteAsociado.getId()).isEsPredioEnPH()) {
                                    PFichaMatriz ficha = this.conservacionService.
                                        buscarPFichaMatrizPorPredioId(predioProyectado.getId());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioProyectadoResultante.setFichaMatriz(fichaVO);
                                    }
                                }
                                actividadVo.setPrediosOriginales(listaPrediosOriginales);
                                actividadVo.setPredioResultante(predioProyectadoResultante);
                            } else if (!tramiteAsociado.isDesenglobeEtapas()) { //Desenglobe simple
                                List<PredioInfoVO> listaPrediosResultantes =
                                    new ArrayList<PredioInfoVO>();
                                PredioInfoVO predioOriginalDesenglobe = new PredioInfoVO();
                                List<ManzanaVO> ManzanasResultantes = new ArrayList<ManzanaVO>();
                                List<PManzanaVereda> manzanasDesenglobadasAsociadas;

                                List<PPredio> listaPPrediosDesenglobe = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId());
                                for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {
                                    PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                        unPredioProyectado, tramiteAsociado);
                                    if (unPredioProyectado.isEsPredioEnPH()) {
                                        PFichaMatriz ficha = this.conservacionService.
                                            buscarPFichaMatrizPorPredioId(unPredioProyectado.getId());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            predioProyectadoVO.setFichaMatriz(fichaVO);
                                        }
                                    }
                                    listaPrediosResultantes.add(predioProyectadoVO);
                                }
                                predioOriginalDesenglobe = new PredioInfoVO(tramiteAsociado.
                                    getPredio(), tramiteAsociado);
                                if (tramiteAsociado.getPredio().isEsPredioEnPH()) {
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(tramiteAsociado.
                                            getPredio().getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioOriginalDesenglobe.setFichaMatriz(fichaVO);
                                    }
                                }

                                //Obtienes las manzanas desenglobadas
                                manzanasDesenglobadasAsociadas = pManzanaVeredaDao.
                                    obtenerPorTramiteId(tramiteId);
                                if (!manzanasDesenglobadasAsociadas.isEmpty() &&
                                    manzanasDesenglobadasAsociadas != null) {
                                    actividadVo.setDesenglobeManzana(Boolean.TRUE);
                                    for (PManzanaVereda manzana : manzanasDesenglobadasAsociadas) {
                                        ManzanasResultantes.add(new ManzanaVO(manzana.getCodigo(),
                                            manzana.getCancelaInscribe()));
                                    }
                                    actividadVo.setManzanasResultantes(ManzanasResultantes);
                                } else {
                                    actividadVo.setDesenglobeManzana(Boolean.FALSE);

                                }
                                actividadVo.setPredioOriginal(predioOriginalDesenglobe);
                                actividadVo.setPrediosResultantes(listaPrediosResultantes);
                            } //Caso especial por etapas
                            else if (tramiteAsociado.isDesenglobeEtapas()) {
                                List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                                List<PredioInfoVO> listaPrediosResultantes =
                                    new ArrayList<PredioInfoVO>();

                                List<Predio> listaPrediosEnglobe = this.predioDao.
                                    obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.
                                        getId());
                                if (tramiteAsociado.getPredio() != null) {
                                    PredioInfoVO predioMatriz = new PredioInfoVO(tramiteAsociado.
                                        getPredio(), tramiteAsociado);
                                    FichaMatriz ficha = this.conservacionService.
                                        getFichaMatrizByNumeroPredialPredio(tramiteAsociado.
                                            getPredio().getNumeroPredial());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioMatriz.setFichaMatriz(fichaVO);
                                    }
                                    listaPrediosOriginales.add(predioMatriz);
                                }
                                for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                                    PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                        unPredioOriginalEnglobe, tramiteAsociado);
                                    if (!unPredioInfoVo.getNumeroPredial().equals(tramiteAsociado.
                                        getPredio().getNumeroPredial())) {
                                        listaPrediosOriginales.add(unPredioInfoVo);
                                    }
                                }
                                //Determina los cancela i
                                List<EProyeccionCancelaInscribe> ci =
                                    new ArrayList<EProyeccionCancelaInscribe>();
                                ci.add(EProyeccionCancelaInscribe.CANCELA);
                                ci.add(EProyeccionCancelaInscribe.INSCRIBE);
                                ci.add(EProyeccionCancelaInscribe.MODIFICA);
                                List<PPredio> listaPPrediosDesenglobe = this.pPredioDao.
                                    getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId(),
                                        ci);
                                for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {
                                    PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                        unPredioProyectado, tramiteAsociado);
                                    //Obtiene la ficha matriz proyectada para el predio matriz proyectado

                                    //Se considera el caso del englobe vitual para traiga la ficha proyectada y las asociadas
                                    if (tramiteAsociado.isEnglobeVirtual()) {
                                        String fichaMatrizResultante = tramiteAsociado.
                                            obtenerInfoCampoAdicional(
                                                EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV);
                                        PFichaMatriz pFichaResultante = this.conservacionService.
                                            buscarFichaPorNumeroPredialProyectada(
                                                fichaMatrizResultante);
                                        if (unPredioProyectado.getNumeroPredial().equals(
                                            fichaMatrizResultante)) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(
                                                pFichaResultante);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            predioProyectadoVO.setFichaMatriz(fichaVO);
                                            List<FichaMatrizTorreVO> fmtvo = fichaVO.
                                                getFichaMatrizTorres();
                                            if (fmtvo != null) {
                                                for (FichaMatrizTorreVO fichaMatrizTorreVO : fmtvo) {
                                                    if (fichaMatrizTorreVO.getProvieneTorre() ==
                                                        null) {
                                                        //Se encuentra necesario diferenciar el campo poriene torre vacio solo para este tipo de tramite
                                                        //debido a un error en visual studio :: REDIMINE 50380
                                                        fichaMatrizTorreVO.setProvieneTorre(0L);
                                                    }
                                                }
                                            }
                                        } else {
                                            if (unPredioProyectado.isEsPredioEnPH()) {
                                                PFichaMatriz ficha = this.conservacionService.
                                                    buscarPFichaMatrizPorPredioId(
                                                        unPredioProyectado.getId());
                                                if (ficha != null) {
                                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                                    fichaVO = this.
                                                        loadUnidadesConstruccionPorPredioPH(fichaVO);
                                                    predioProyectadoVO.setFichaMatriz(fichaVO);
                                                    List<FichaMatrizTorreVO> fmtvo = fichaVO.
                                                        getFichaMatrizTorres();
                                                    if (fmtvo != null) {
                                                        for (FichaMatrizTorreVO fichaMatrizTorreVO
                                                            : fmtvo) {
                                                            if (fichaMatrizTorreVO.
                                                                getProvieneTorre() == null) {
                                                                //Se encuentra necesario diferenciar el campo poriene torre vacio solo para este tipo de tramite
                                                                //debido a un error en visual studio :: REDIMINE 50380
                                                                fichaMatrizTorreVO.setProvieneTorre(
                                                                    99L);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (unPredioProyectado.getId().equals(tramiteAsociado.
                                            getPredio().getId())) {
                                            if (unPredioProyectado.isEsPredioEnPH()) {
                                                PFichaMatriz ficha = this.conservacionService.
                                                    buscarPFichaMatrizPorPredioId(
                                                        unPredioProyectado.getId());
                                                if (ficha != null) {
                                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                                    fichaVO = this.
                                                        loadUnidadesConstruccionPorPredioPH(fichaVO);
                                                    predioProyectadoVO.setFichaMatriz(fichaVO);
                                                }
                                            }
                                        }
                                    }
                                    listaPrediosResultantes.add(predioProyectadoVO);
                                }
                                actividadVo.setPrediosResultantes(listaPrediosResultantes);
                                actividadVo.setPrediosOriginales(listaPrediosOriginales);
                            }
                        }
                    } // Caso Rectificacion
                    else if (tramiteAsociado.getTipoTramite().equals(
                        ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
                        Boolean banderaRectificacionAreaLinderos = false;
                        //rectificaciones de area y rectificaciones area con linderos no masivas
                        if (!tramiteAsociado.isRectificacionMatriz()) {
                            Predio predioOriginal;
                            PPredio predioProyectado;
                            List<Predio> prediosAsociadosRectificacion = new ArrayList<Predio>();
                            List<PredioInfoVO> prediosAsociados = new ArrayList();

                            predioOriginal = tramiteAsociado.getPredio();
                            predioProyectado = this.pPredioDao.
                                getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId()).get(
                                0);

                            PredioInfoVO predioOriginalVo = new PredioInfoVO(predioOriginal,
                                tramiteAsociado);
                            PredioInfoVO PredioResultanteVo = new PredioInfoVO(predioProyectado,
                                tramiteAsociado);

                            //Informacion de ficha matriz del original
                            if (predioOriginal.isEsPredioEnPH()) {
                                FichaMatriz ficha = this.conservacionService.
                                    getFichaMatrizByNumeroPredialPredio(predioOriginal.
                                        getNumeroPredial());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    predioOriginalVo.setFichaMatriz(fichaVO);
                                }
                            }

                            //Informacion de ficha matriz del proyectado
                            if (predioProyectado.isEsPredioEnPH()) {
                                PFichaMatriz ficha = this.conservacionService.
                                    buscarPFichaMatrizPorPredioId(predioProyectado.getId());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    PredioResultanteVo.setFichaMatriz(fichaVO);
                                }
                            }

                            //Agrega Predios si es una rectificacion de area con linderos
                            if (tramiteAsociado.getTramitePredioEnglobes() != null &&
                                !tramiteAsociado.getTramitePredioEnglobes().isEmpty()) {
                                prediosAsociadosRectificacion = this.predioDao.
                                    obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.
                                        getId());
                                for (Predio unPredio : prediosAsociadosRectificacion) {
                                    PredioInfoVO unPredioVo = new PredioInfoVO(unPredio,
                                        tramiteAsociado);
                                    if (unPredio.isEsPredioEnPH()) {
                                        FichaMatriz ficha = this.conservacionService.
                                            getFichaMatrizByNumeroPredialPredio(unPredio.
                                                getNumeroPredial());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            unPredioVo.setFichaMatriz(fichaVO);
                                        }
                                    }
                                    prediosAsociados.add(unPredioVo);
                                    banderaRectificacionAreaLinderos = true;
                                }
                                //Agrega los predios linderos a los tramites de rectificacion de area con linderos.
                                //@modified by leidy.gonzalez::18554::18/07/2016:: Se saca del FOR el SUBTIPO_RECTIFICACION_GEOGRAFICA
                                actividadVo.setDatosRectificacion(
                                    Constantes.SUBTIPO_RECTIFICACION_GEOGRAFICA);
                                actividadVo.setPrediosOriginales(prediosAsociados);

                            }
                            actividadVo.setPredioOriginal(predioOriginalVo);
                            actividadVo.setPredioResultante(PredioResultanteVo);
                        } //rectificacion masiva
                        //Se considera que el predio matriz es el unico que tiene ficha matriz
                        else if (tramiteAsociado.isRectificacionMatriz()) {

                            List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                            List<PredioInfoVO> listaPrediosResultantes =
                                new ArrayList<PredioInfoVO>();
                            List<Predio> listaPrediosEnglobe = this.predioDao.
                                obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.getId());
                            //obtiene la ficha del matriz
                            if (tramiteAsociado.getPredio() != null) {
                                PredioInfoVO predioMatriz = new PredioInfoVO(tramiteAsociado.
                                    getPredio(), tramiteAsociado);
                                FichaMatriz ficha = this.conservacionService.
                                    getFichaMatrizByNumeroPredialPredio(predioMatriz.
                                        getNumeroPredial());
                                if (ficha != null) {
                                    FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                    fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                    predioMatriz.setFichaMatriz(fichaVO);
                                }
                                listaPrediosOriginales.add(predioMatriz);
                            }
                            for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                                PredioInfoVO unPredioInfoVo = new PredioInfoVO(
                                    unPredioOriginalEnglobe, tramiteAsociado);
                                listaPrediosOriginales.add(unPredioInfoVo);
                            }
                            //Determina los cancela i
                            List<EProyeccionCancelaInscribe> ci =
                                new ArrayList<EProyeccionCancelaInscribe>();
                            ci.add(EProyeccionCancelaInscribe.CANCELA);
                            ci.add(EProyeccionCancelaInscribe.INSCRIBE);
                            ci.add(EProyeccionCancelaInscribe.MODIFICA);
                            List<PPredio> listaPPrediosDesenglobe = this.pPredioDao.
                                getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId(), ci);
                            for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {

                                PredioInfoVO predioProyectadoVO = new PredioInfoVO(
                                    unPredioProyectado, tramiteAsociado);
                                if (unPredioProyectado.getNumeroPredial().substring(22, 30).equals(
                                    "00000000")) {
                                    if (unPredioProyectado.isEsPredioEnPH()) {
                                        PFichaMatriz ficha = this.conservacionService.
                                            buscarPFichaMatrizPorPredioId(unPredioProyectado.getId());
                                        if (ficha != null) {
                                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(
                                                fichaVO);
                                            predioProyectadoVO.setFichaMatriz(fichaVO);
                                        }
                                    }
                                }
                                listaPrediosResultantes.add(predioProyectadoVO);

                            }
                            actividadVo.setPrediosOriginales(listaPrediosOriginales);
                            actividadVo.setPrediosResultantes(listaPrediosResultantes);
                        }
                        //TODO juanFelipe.garci::04/08/2015::Implementar caso rectificaciones 
                        //masivas con correccion de linderos Nota: no se implementa por problemas 
                        //con la identificacion del tramite en el modelo actual.
                        if (tramiteAsociado.isRectificacionGeograficaAreaConstruccion()) {
                            actividadVo.setDatosRectificacion(EDatoRectificarNombre.AREA_CONSTRUIDA.
                                getCodigo());
                        }
                        if (tramiteAsociado.isRectificacionGeograficaAreaTerreno()) {
                            if (tramiteAsociado.getTramitePredioEnglobes() == null ||
                                tramiteAsociado.getTramitePredioEnglobes().isEmpty()) {
                                actividadVo.setDatosRectificacion(
                                    EDatoRectificarNombre.AREA_TERRENO.getCodigo());
                            }
                        }
                        if (tramiteAsociado.isRectificacionCancelacionDobleInscripcion()) {
                            actividadVo.setDatosRectificacion(
                                EDatoRectificarNombre.CANCELACION_DOBLE_INSCRIPCION.getCodigo());
                        }

                    } //Caso Revision de Avaluo
                    else if (tramiteAsociado.getTipoTramite().equals(
                        ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
                        Predio predioOriginal;
                        PPredio predioProyectado;
                        PredioInfoVO predioAsociadoTramite;
                        PredioInfoVO predioResultante;

                        predioOriginal = tramiteAsociado.getPredio();
                        predioAsociadoTramite = new PredioInfoVO(predioOriginal, tramiteAsociado);

                        predioProyectado = this.pPredioDao.getPPrediosByTramiteIdActualizarAvaluo(
                            tramiteAsociado.getId()).get(0);
                        predioResultante = new PredioInfoVO(predioProyectado, tramiteAsociado);

                        actividadVo.setPredio(predioAsociadoTramite);
                        actividadVo.setPredioOriginal(predioResultante);
                    }
                }
                actividadesVo.add(actividadVo);

                //CASO CANCELACION MASIVA PH-CONDOMINIO
                if (tramiteAsociado.getTipoTramite().equals(
                    ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo()) && tramiteAsociado.
                    getRadicacionEspecial().equals("CANCELACION_MASIVA")) {
                    LOGGER.info("CASO CANCELACION MASIVA PH-CONDOMINIO");
                    if (tramiteAsociado.getPredio().isEsPredioEnPH()) {

                        List<PredioInfoVO> listaPrediosOriginales = new ArrayList();
                        List<PredioInfoVO> listaPrediosResultantes = new ArrayList<PredioInfoVO>();
                        List<Predio> listaPrediosEnglobe = this.predioDao.
                            obtenerPrediosCompletosEnglobePorTramiteId(tramiteAsociado.getId());
                        //obtiene la ficha del matriz
                        if (tramiteAsociado.getPredio() != null) {
                            PredioInfoVO predioMatriz =
                                new PredioInfoVO(tramiteAsociado.getPredio(), tramiteAsociado);
                            FichaMatriz ficha = this.conservacionService.
                                getFichaMatrizByNumeroPredialPredio(tramiteAsociado.getPredio().
                                    getNumeroPredial());
                            FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                            fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                            predioMatriz.setFichaMatriz(fichaVO);
                            listaPrediosOriginales.add(predioMatriz);
                        }
                        for (Predio unPredioOriginalEnglobe : listaPrediosEnglobe) {
                            PredioInfoVO unPredioInfoVo = new PredioInfoVO(unPredioOriginalEnglobe,
                                tramiteAsociado);
                            listaPrediosOriginales.add(unPredioInfoVo);
                        }
                        //Determina los cancela i
                        List<EProyeccionCancelaInscribe> ci =
                            new ArrayList<EProyeccionCancelaInscribe>();
                        ci.add(EProyeccionCancelaInscribe.CANCELA);
                        ci.add(EProyeccionCancelaInscribe.INSCRIBE);
                        ci.add(EProyeccionCancelaInscribe.MODIFICA);
                        List<PPredio> listaPPrediosDesenglobe = this.pPredioDao.
                            getPPrediosByTramiteIdActualizarAvaluo(tramiteAsociado.getId(), ci);
                        for (PPredio unPredioProyectado : listaPPrediosDesenglobe) {
                            PredioInfoVO predioProyectadoVO = new PredioInfoVO(unPredioProyectado,
                                tramiteAsociado);
                            if (unPredioProyectado.getId().equals(tramiteAsociado.getPredio().
                                getId())) {
                                if (unPredioProyectado.isEsPredioEnPH()) {
                                    PFichaMatriz ficha = this.conservacionService.
                                        buscarPFichaMatrizPorPredioId(unPredioProyectado.getId());
                                    if (ficha != null) {
                                        FichaMatrizVO fichaVO = new FichaMatrizVO(ficha);
                                        fichaVO = this.loadUnidadesConstruccionPorPredioPH(fichaVO);
                                        predioProyectadoVO.setFichaMatriz(fichaVO);
                                    }
                                }
                            }
                            listaPrediosResultantes.add(predioProyectadoVO);

                        }
                        //actividadVo.setPrediosOriginales(listaPrediosOriginales); //Al parecer para la edicion en el editor solo se necesitan los predios Proyectados.
                        actividadVo.setPrediosResultantes(listaPrediosResultantes);

                    }

                }
            }

            resultadoTareasPendientes = new ResultadoVO(usuario, actividadesVo);
            return resultadoTareasPendientes;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA.
                getExcepcion(LOGGER, e);
        }
    }

    /**
     * @see ITramiteLocal#recibirRadicado(java.lang.String,java.lang.String)
     * @author juanfelipe.garcia
     */
    @Override
    public Object[] recibirRadicado(String pradicacion, String usuario) {
        try {
            return this.sncProcedimientoDao.recibirRadicado(pradicacion, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see ITramiteLocal#finalizarRadicacion(java.lang.String,java.lang.String,java.lang.String)
     * @author juanfelipe.garcia
     */
    @Override
    public Object[] finalizarRadicacion(String pradicacion, String pradicacionResponde,
        String usuario) {
        try {
            return this.sncProcedimientoDao.finalizarRadicacion(pradicacion, pradicacionResponde,
                usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see
     * ITramiteLocal#calcularAreasUnidadesPrediales(java.lang.String,java.lang.String,java.lang.String)
     * @author felipe.cadena
     */
    //@modified by javier.aponte se guarda el valor del área truncado a 3 cifras decimales refs#12022
    @Override
    public void calcularAreasUnidadesPrediales(Tramite tramite){

        LOGGER.debug("Inicio calculo areas PH , Co, TID " + tramite.getId());

        List<PPredio> prediosAsociados = new ArrayList<PPredio>();
        List<PFichaMatriz> fichaMatrizAsociadaAlTramite = null;
        PPredio predioAsociadoAFicha = null;
        List<PPredioZona> zonasPredioFM;
        List<PPredioZona> zonasUnidad;

        double areaTerrenoComun = 0.0;
        double areaTerrenoUnidad = 0.0;
        double coeficienteCopropiedad = 0.0;

        //Solo es valido para tramites de ph o condominios y masivos por etapas o rectificaciones
        if(!tramite.isTipoInscripcionCondominio()
                && !tramite.isRectificacionMatriz()
                && !tramite.isDesenglobeEtapas()
                && !tramite.isTipoInscripcionPH()){
            return;
        }

        //Se considera que el trámite pueda tener varias fichas matrices
        fichaMatrizAsociadaAlTramite = this.pFichaMatrizDao.obtenerFichasAsociadasATramite(tramite.getId());
        for (PFichaMatriz fmTramite : fichaMatrizAsociadaAlTramite) {
            predioAsociadoAFicha = fmTramite.getPPredio();
            zonasPredioFM = this.pPredioZonaDao.buscarTodasZonasPorPPredioIdYEstadoP(predioAsociadoAFicha.getId(), EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            prediosAsociados = this.pPredioDao.obtenerPrediosProyectadosAsociadosAFicha(fmTramite.getId());

            LOGGER.debug("Calculo ficha matriz ID " + fmTramite.getId());

            areaTerrenoComun = predioAsociadoAFicha.getAreaTerreno();

            for (PPredio pp : prediosAsociados) {

                //No se procesan los predios cancelados
                if(EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pp.getCancelaInscribe())){
                    continue;
                }

                LOGGER.debug(">>>>>>>>>>>>Calculo predio ID " + pp.getId());

                coeficienteCopropiedad = this.obtenerCoeficienteCopropiedad(fmTramite.getPFichaMatrizPredios(), pp.getNumeroPredial());
                zonasUnidad = new ArrayList<PPredioZona>();

                if (pp.isUnidadEnPH()) {

                    List<PPredioZona> zonasPredioPrevias;

                    // *********************************** //
                    // *** Cancelación de zonas previas ** //
                    // *********************************** //
                    if (tramite.isDesenglobeEtapas() || tramite.isRectificacionMatriz()) {

                        Map<Integer, List<PPredioZona>> zonasPorVigencia = new HashMap<Integer, List<PPredioZona>>();
                        zonasPredioPrevias = this.pPredioZonaDao.buscarTodasZonasPorPPredioId(pp.getId());
                        Calendar vigenciaTramite = Calendar.getInstance();
                        vigenciaTramite.setTime(pp.getFechaInscripcionCatastral());
                        Calendar vigenciaActual = Calendar.getInstance();
                        vigenciaActual.set(Calendar.MONTH, 0);
                        vigenciaActual.set(Calendar.DATE, 1);

                        for (PPredioZona ppz : zonasPredioPrevias) {
                            Calendar vigenciaZona = Calendar.getInstance();
                            vigenciaZona.setTime(ppz.getVigencia());

                            if(zonasPorVigencia.containsKey(vigenciaZona.get(Calendar.YEAR))){
                                zonasPorVigencia.get(vigenciaZona.get(Calendar.YEAR)).add(ppz);
                            }else{
                                List<PPredioZona> zns = new ArrayList<PPredioZona>();
                                zns.add(ppz);
                                zonasPorVigencia.put(vigenciaZona.get(Calendar.YEAR), zns);
                            }

                        }

                        //Se cancelan las zonas de la vigencia actual
                        List<PPredioZona> zonasVigenciaActual = zonasPorVigencia.get(vigenciaActual.get(Calendar.YEAR));
                        if (zonasVigenciaActual != null) {
                            for (PPredioZona ppz : zonasPorVigencia.get(vigenciaActual.get(Calendar.YEAR))) {
                                ppz.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                            }
                            this.pPredioZonaDao.updateMultiple(zonasPorVigencia.get(vigenciaActual.get(Calendar.YEAR)));
                        }

                        //Se cancelan las zonas historicas

                        Integer initYear = vigenciaActual.get(Calendar.YEAR)-1;
                        while (initYear > vigenciaTramite.get(Calendar.YEAR)) {
                            List<PPredioZona> zonasVigencia = zonasPorVigencia.get(initYear);

                            if (zonasVigencia != null) {
                                if (zonasVigencia.size() == 1) {
                                    double areaUnidad = 0;
                                    for (PPredioZona ppz : zonasPredioFM) {
                                        areaUnidad += coeficienteCopropiedad * ppz.getArea();
                                    }
                                    zonasVigenciaActual = zonasPorVigencia.get(vigenciaActual.get(Calendar.YEAR));
                                    if (zonasVigenciaActual != null) {
                                        for (PPredioZona ppz : zonasVigenciaActual) {
                                            //Se redondea el área de terreno a 2 cifras decimales
                                            areaUnidad = Utilidades.redondearNumeroADosCifrasDecimales(areaUnidad);
                                            ppz.setArea(areaUnidad);
                                            ppz.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                                        }
                                        this.pPredioZonaDao.updateMultiple(zonasVigenciaActual);
                                    }
                                } else {
                                    //TODO::felipe.cadena:: definir zonas historicas multiples
                                }
                            }
                            initYear--;
                        }

                    } else {
                        zonasPredioPrevias = this.pPredioZonaDao.buscarTodasZonasPorPPredioIdYEstadoP(pp.getId(), EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                        for (PPredioZona ppzp : zonasPredioPrevias) {
                            ppzp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                        }
                        this.pPredioZonaDao.updateMultiple(zonasPredioPrevias);
                    }

                } else {
                    // felipe.cadena::27-09-2017::Se retira por decision de analisis
                    // pp.setAreaTerreno(Utilidades.redondearNumeroADosCifrasDecimales(pp.getAreaTerreno()+ areaTerrenoUnidad));
                }

                // 64776 :: Ajustes CC-NP-CO-276 Ajustes en calculo de áreas de condominios, dejar como vienen para condominios
                if(!tramite.isTipoInscripcionCondominio()) {

                    //actualizacion de areas.
                    areaTerrenoUnidad = coeficienteCopropiedad * areaTerrenoComun;

                    //Se redondea el área de terreno a 2 cifras decimales
                    areaTerrenoUnidad = Utilidades.redondearNumeroADosCifrasDecimales(areaTerrenoUnidad);
                    pp.setAreaTerreno(areaTerrenoUnidad);

                    //actualizacion de zonas
                    for (PPredioZona ppz : zonasPredioFM) {
                        if (pp.isUnidadEnPH()) {

                            //Las unidades de PH nunca tienen zonas asociadas.
                            PPredioZona zonaPredio = ppz.clone();
                            if (zonaPredio != null) {
                                areaTerrenoUnidad = coeficienteCopropiedad * ppz.getArea();
                                //Se redondea el área de terreno a 2 cifras decimales
                                areaTerrenoUnidad = Utilidades.redondearNumeroADosCifrasDecimales(areaTerrenoUnidad);
                                zonaPredio.setArea(areaTerrenoUnidad);
                                zonaPredio.setAvaluo(coeficienteCopropiedad * ppz.getAvaluo());
                                zonaPredio.setPPredio(pp);
                                zonaPredio.setId(null);
                                zonasUnidad.add(zonaPredio);
                            }
                        } else {
                            zonasUnidad = this.pPredioZonaDao.buscarTodasZonasPorPPredioIdYEstadoP(pp.getId(), EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                            Boolean existeZona = false;
                            //Se actualizan las áreas ya existentes
                            for (PPredioZona ppzUnidad : zonasUnidad) {
                                if (ppzUnidad.getZonaFisica().equals(ppz.getZonaFisica())
                                        && ppzUnidad.getZonaGeoeconomica().equals(ppz.getZonaGeoeconomica())) {
                                    areaTerrenoUnidad = coeficienteCopropiedad * ppz.getArea();
                                    //Se redondea el área de terreno a 2 cifras decimales
                                    areaTerrenoUnidad = Utilidades.redondearNumeroADosCifrasDecimales(areaTerrenoUnidad);
                                    //ppzUnidad.setArea(ppz.getArea() + areaTerrenoUnidad);
                                    ppzUnidad.setArea(ppzUnidad.getArea());
                                    existeZona = true;
                                    break;
                                }
                            }
                            //se agregan las nuevas zonas si no existian
                            if (!existeZona) {
                                PPredioZona zonaPredio = ppz.clone();
                                if (zonaPredio != null) {
                                    areaTerrenoUnidad = coeficienteCopropiedad * ppz.getArea();
                                    //Se redondea el área de terreno a 2 cifras decimales
                                    areaTerrenoUnidad = Utilidades.redondearNumeroADosCifrasDecimales(areaTerrenoUnidad);
                                    zonaPredio.setArea(areaTerrenoUnidad);
                                    zonaPredio.setAvaluo(coeficienteCopropiedad * ppz.getAvaluo());
                                    zonaPredio.setPPredio(pp);
                                    zonaPredio.setId(null);
                                    zonasUnidad.add(zonaPredio);
                                }

                            }

                        }
                    }
                    this.pPredioZonaDao.updateMultiple(zonasUnidad);
                }
            }
            this.pPredioDao.updateMultiple(prediosAsociados);
        }
    }

    /**
     * Retorna el coeficiente de copropiedad de un predio asociado a la ficha matriz
     *
     * @author felipe.cadena
     *
     * @param predioFicha
     * @param numeroPredial
     * @return
     */
    private double obtenerCoeficienteCopropiedad(List<PFichaMatrizPredio> predioFicha,
        String numeroPredial) {

        for (PFichaMatrizPredio pfm : predioFicha) {
            if (pfm.getNumeroPredial().equals(numeroPredial)) {
                return pfm.getCoeficiente();
            }
        }

        return 0.0;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramitePorNumeroRadicacion(String)
     * @author juanfelipe.garcia
     */
    @Override
    public Tramite buscarTramitePorNumeroRadicacion(String numeroRadicacion) {
        return this.tramiteDao.findTramiteByNoRadicacion(numeroRadicacion);
    }

    /**
     * @see ITramite#guardarActualizarTramiteEstado(TramiteEstado)
     * @author felipe.cadena
     */
    @Override
    public TramiteEstado guardarActualizarTramiteEstado(TramiteEstado te) {

        TramiteEstado resultado = null;
        try {
            resultado = this.tramiteEstadoDao.update(te);
        } catch (ExcepcionSNC e) {
            LOGGER.error(" Error al actualizar o insertar un TramiteEstado " + e.getMessage());
        }
        return resultado;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramiteLocal#buscarTramitesParaDeterminarProcedenciaSolicitud(long[], String, String,
     * Map, int...)
     * @author javier.aponte
     */
    @Implement
    @Override
    public List<Tramite> buscarTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites,
        String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        LOGGER.debug("en TramiteBean#buscarTramitesParaDeterminarProcedenciaSolicitud");

        List<Tramite> answer = null;

        try {

            answer = this.tramiteDao.findTramitesParaDeterminarProcedenciaSolicitud(idsTramites,
                sortField, sortOrder,
                filters, rowStartIdxAndCount);

            //felipe.cadena::ACTUALIZACION::08/10/2015::Determina si los predios del tramite estan en actualizacion
            if (answer != null && !answer.isEmpty()) {
                Tramite tramiteMuestra = answer.get(0);

                List<MunicipioActualizacion> mus = this.municipioActualizacionDAO
                    .obtenerEstadoTramitePorDepartamentoMunicipio(tramiteMuestra.getMunicipio().
                        getCodigo(), tramiteMuestra.getDepartamento().getCodigo());
                //Solo se valida si existen procesos de actualizacion en el municipio 
                if (mus != null && !mus.isEmpty()) {
                    for (Tramite t : answer) {
                        if (!t.isQuinta()) {
                            List<Long> ids = new ArrayList<Long>();
                            ids.add(t.getPredio().getId());
                            List<Tramite> tramiteActualizacion = this.tramiteDao.
                                buscarTramitesEnActualizacion(ids);
                            if (tramiteActualizacion != null && !tramiteActualizacion.isEmpty()) {
                                t.setTramiteActualizacionAsociado(tramiteActualizacion.get(0));
                            }
                        }
                    }
                }
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Excepción consultando trámites para determinar procedencia de la solicitud: " + ex.
                    getMensaje());
        }

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#contarTramitesParaDeterminarProcedenciaSolicitud(long[])
     * @author javier.aponte
     */
    @Implement
    @Override
    public int contarTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites) {
        LOGGER.debug("en TramiteBean#contarTramitesParaDeterminarProcedenciaSolicitud");

        int answer = 0;

        try {
            answer = this.tramiteDao.countTramitesParaDeterminarProcedenciaSolicitud(idsTramites);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "Excepción contando trámites para determinar procedencia de la solicitud: " + ex.
                    getMensaje());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see ITramite#buscarDocmentoFTP
     */
    @Override
    public List<Documento> buscarDocmentoFTP() {
        List<Documento> answer = null;

        try {
            answer = this.documentoDao.buscarDocumentosFTP();

            Tramite tramite;
            UsuarioDTO responsableConservacion;
            File resolucion = null;

            TramiteDocumento tramiteDocumento = null;
            /*
             * for(Documento docTemp : answer){
             *
             * responsableConservacion =
             * this.generalesService.getCacheUsuario(docTemp.getUsuarioLog());
             *
             * tramite = this.tramiteDao.findById(docTemp.getTramiteId());
             * if(!ETramiteEstado.FINALIZADO_APROBADO.getCodigo().equals(tramite.getEstado())){
             * resolucion = this.generarReporteResolucion(tramite, responsableConservacion,
             * docTemp.getNumeroDocumento(), docTemp.getFechaDocumento()); } if(resolucion != null){
             * docTemp.setArchivo(resolucion.getAbsolutePath()); // Subo el archivo a Alfresco y
             * actualizo el documento docTemp = this.guardarDocumento(responsableConservacion,
             * docTemp);
             *
             * tramiteDocumento = this.obtenerTramiteDocumentosDeTramitePorTipoDoc(tramite.getId(),
             * ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.getId()).get(0);
             *
             * tramiteDocumento.setIdRepositorioDocumentos(docTemp.getIdRepositorioDocumentos());
             *
             * tramiteDocumento = this.actualizarTramiteDocumento(tramiteDocumento);
             *
             * if (docTemp != null) { tramite.setResultadoDocumento(docTemp); tramite =
             * this.guardarActualizarTramite(tramite); }
             *
             * }
             * }
             */

        } catch (ExcepcionSNC ex) {
            LOGGER.error("... error: " + ex.getMensaje());
        }
        return answer;
    }

    private File generarReporteResolucion(Tramite tramite, UsuarioDTO responsableConservacion,
        String numeroRes, Date fechaRes) {

        EReporteServiceSNC enumeracionReporte = this.obtenerUrlReporteResoluciones(tramite.getId());

        if (enumeracionReporte != null) {

            if (numeroRes != null) {

                IReporteService reportesService = ReporteServiceFactory.getService();
                IDocumentosService documentosService = DocumentalServiceFactory.getService();

                EstructuraOrganizacional estructuraOrganizacional = this.generalesService
                    .buscarEstructuraOrganizacionalPorCodigo(
                        responsableConservacion.getCodigoEstructuraOrganizacional());

                Reporte reporte = new Reporte(responsableConservacion, estructuraOrganizacional);
                reporte.setUrl(enumeracionReporte.getUrlReporte());

                reporte.addParameter("TRAMITE_ID", "" + tramite.getId());

                reporte.addParameter("COPIA_USO_RESTRINGIDO", "false");

                reporte.addParameter("NOMBRE_RESPONSABLE_CONSERVACION", responsableConservacion.
                    getNombreCompleto());

                if (tramite.isTipoTramiteViaGubernativaModificado() || tramite.
                    isTipoTramiteViaGubModSubsidioApelacion() ||
                    tramite.isRecursoApelacion()) {

                    if (!tramite.getNumeroRadicacionReferencia().isEmpty()) {

                        Tramite tramiteReferencia = this.buscarTramitePorNumeroRadicacion(tramite.
                            getNumeroRadicacionReferencia());

                        if (tramiteReferencia != null) {
                            reporte.addParameter("NOMBRE_SUBREPORTE_RESUELVE", this.
                                obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.getId()));
                        }
                    }

                }

                SolicitudCatastral objNegocio;

                UsuarioDTO coordinador = null;
                List<UsuarioDTO> coordinadores = null;

                if (coordinadores == null) {
                    objNegocio = this.procesosService
                        .obtenerObjetoNegocioConservacion(tramite.getProcesoInstanciaId(),
                            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);

                    if (objNegocio != null) {
                        coordinadores = (List<UsuarioDTO>) objNegocio.getUsuarios();
                    }
                }
                if (coordinadores != null && !coordinadores.isEmpty() &&
                    coordinadores.get(0) != null && coordinadores.get(0).getLogin() != null) {
                    coordinador = this.generalesService.getCacheUsuario(coordinadores.get(0).
                        getLogin());
                }

                if (coordinador != null && coordinador.getNombreCompleto() != null) {
                    reporte.addParameter("NOMBRE_REVISOR", coordinador.getNombreCompleto());
                } else {
                    LOGGER.error("No se pudo obtener el nombre del revisor");
                }

                reporte.addParameter("NUMERO_RESOLUCION", numeroRes);
                reporte.addParameter("FECHA_RESOLUCION", Constantes.FORMAT_FECHA_RESOLUCION.format(
                    fechaRes));
                reporte.addParameter("FECHA_RESOLUCION_CON_FORMATO",
                    Constantes.FORMAT_FECHA_RESOLUCION_2.format(fechaRes));

                File resolucionConservacion = null;

                resolucionConservacion = reportesService.getReportAsFile(reporte,
                    new LogMensajesReportesUtil(this.generalesServiceIG, responsableConservacion));
                return resolucionConservacion;
            }

        } else {
            LOGGER.error("No se pudo obtener el url " +
                "en el servidor de reportes asociada a la resolución ");
        }

        return null;

    }

    /**
     * @see ITramiteLocal#findIdsClasificacionTramitesConEjecutor(long)
     * @author javier.aponte
     */
    @Override
    public List<TramiteConEjecutorAsignadoDTO> findIdsClasificacionTramitesConEjecutor(
        long[] idsTramites) {
        try {
            return this.tramiteDao.findIdsClasificacionTramitesConEjecutor(idsTramites);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see ITramiteLocal#buscarTramitesParaRecuperacion
     *
     * @author felipe.cadena
     */
    @Override
    public List<Tramite> buscarTramitesParaRecuperacion(FiltroDatosConsultaTramite filtroTramite,
        FiltroDatosConsultaSolicitante filtroSolicitante) {
        List<Tramite> resultado = null;
        try {
            resultado = this.tramiteDao.bucarTramiteByFiltroTramiteRecuperacionTamite(filtroTramite,
                filtroSolicitante, null, null);
        } catch (Exception e) {
            LOGGER.error("Error consultando los tramites para recuperar");
        }
        return resultado;
    }

    /**
     * @see ITramiteLocal#buscarTramitesParaRecuperacion
     *
     * @author felipe.cadena
     */
    @Override
    public Object[] buscarTramitesParaAdminAsignaciones(FiltroDatosConsultaTramite filtroTramite,
        FiltroDatosConsultaSolicitante filtroSolicitante) {
        List<Tramite> resultado = null;
        String mensaje = null;
        Object[] answer = new Object[2];

        Map<EParametrosConsultaActividades, String> filtros =
            new HashMap<EParametrosConsultaActividades, String>();
        List<Actividad> actividades = null;
        String idsInstanciaProcesosTramitesManzana = "";
        try {

            //Se consultan los tramites de manera alfanumerica
            resultado = this.tramiteDao.buscarTramiteByFiltroTramiteAdminAsignaciones(filtroTramite,
                filtroSolicitante, null, null);

            //No hay resultados
            if (resultado.isEmpty()) {
                answer[0] = mensaje;
                answer[1] = resultado;
                return answer;
            }
            //Si el numero de resultados es mayor a mil retorna un error
            if (resultado.size() >= 500) {
                resultado = null;
                mensaje =
                    "El número de tramites en la consulta es mayor a 500 resgistros debe adicionar mas criterios de busqueda";
            } else {
                for (Tramite t : resultado) {
                    idsInstanciaProcesosTramitesManzana += t.getProcesoInstanciaId() + ",";
                }
                idsInstanciaProcesosTramitesManzana = idsInstanciaProcesosTramitesManzana.substring(
                    0, idsInstanciaProcesosTramitesManzana.lastIndexOf(','));
                //se obtienen las actividades asociadas a los tramites
                filtros.put(EParametrosConsultaActividades.IDS_INSTANCIAS_PROCESOS,
                    idsInstanciaProcesosTramitesManzana);
                filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, null);
                filtros.put(EParametrosConsultaActividades.ESTADOS,
                    EEstadoActividad.TAREAS_RECLAMADAS);

                actividades = this.procesosService.consultarListaActividades(filtros);

                //Se descartarn las actividades del rol ejecutor
                for (Iterator<Actividad> iter = actividades.listIterator(); iter.hasNext();) {
                    Actividad a = iter.next();
                    if (a.getRoles().get(0).toUpperCase().equals(ERol.EJECUTOR_TRAMITE.getRol().
                        toUpperCase())) {
                        iter.remove();
                    }
                }

                for (Tramite t : resultado) {
                    for (int i = 0; i < actividades.size(); i++) {
                        Actividad act = actividades.get(i);
                        if (t.getId().equals(act.getIdObjetoNegocio())) {
                            t.setActividadActualTramite(act);
                            actividades.remove(i);
                            break;
                        }
                    }
                }

                //Se descartan los tramites que no tienen actividad
                for (Iterator<Tramite> iter = resultado.listIterator(); iter.hasNext();) {
                    Tramite t = iter.next();
                    if (t.getActividadActualTramite() == null ||
                        t.getActividadActualTramite().getId() == null ||
                        t.getActividadActualTramite().getId().isEmpty()) {
                        iter.remove();
                    }
                }

                //Se descartan los tramites estan en el usuario tiempos muertos
                for (Iterator<Tramite> iter = resultado.listIterator(); iter.hasNext();) {
                    Tramite t = iter.next();
                    if (t.getActividadActualTramite().getUsuarioEjecutor().equals(
                        ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS)) {
                        iter.remove();
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error consultando los tramites para recuperar");
            mensaje = "Error consultando los tramites para recuperar";
        }
        answer[0] = mensaje;
        answer[1] = resultado;
        return answer;
    }

    /**
     * @see ITramiteLocal#buscarActividadesParaAdminAsignaciones(java.lang.String, java.util.List)
     *
     * @author felipe.cadena
     */
    /*
     * @modified by leidy.gonzalez:: 06/05/2016 :: #14898 :: Se modifica filtro de la consulta
     * process para que se visualicen las tareas vigentes del usuario, no solo las activas. Se
     * modifica la consulta por actividad para cuando supere los 500 tramites los visualice para ser
     * reasignados.
     */
    @Override
    public Object[] buscarActividadesParaAdminAsignaciones(String codigoOficina,
        List<String> usuarios) {

        List<Actividad> resultado = new LinkedList<Actividad>();
        List<Tramite> tramites = new LinkedList<Tramite>();

        String mensaje = null;
        Object[] answer = new Object[2];
        Map<EParametrosConsultaActividades, String> filtros =
            new HashMap<EParametrosConsultaActividades, String>();
        int maximaCantidadActividades = 500;
        String usuariosStr = "";
        int contadorActivad = 0;
        String rolEjecutor = ERol.EJECUTOR_TRAMITE.getRol();
        try {

            for (String usuario : usuarios) {

                usuariosStr += usuario + ",";

            }

            usuariosStr = usuariosStr.substring(0, usuariosStr.lastIndexOf(','));
            filtros.put(EParametrosConsultaActividades.USUARIOS, usuariosStr);
            filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, null);
            filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);

            resultado = this.procesosService.consultarListaActividades(filtros);

            if (resultado == null || resultado.isEmpty()) {
                mensaje = "No se encontraron resultados para la consulta";
            } else if (resultado.size() > maximaCantidadActividades) {

                //Se descartarn las actividades del rol ejecutor
                for (Iterator<Actividad> iter = resultado.listIterator(); contadorActivad <=
                    maximaCantidadActividades; iter.hasNext()) {
                    contadorActivad++;
                    Actividad a = iter.next();
                    if (a.getRoles().get(0).equalsIgnoreCase(rolEjecutor)) {
                        iter.remove();
                    }
                }

                //consultar tramites asociados a las actividades
                List<Long> idsTramite = new ArrayList<Long>();
                for (Actividad act : resultado) {
                    idsTramite.add(act.getIdObjetoNegocio());
                }

                tramites = this.tramiteDao.findTramitesByIds(idsTramite);

                for (Tramite t : tramites) {
                    for (int i = 0; i < resultado.size(); i++) {
                        Actividad act = resultado.get(i);
                        if (t.getId().equals(act.getIdObjetoNegocio())) {
                            t.setActividadActualTramite(act);
                            resultado.remove(i);
                            break;
                        }
                    }
                }

            } else {

                //Se descartarn las actividades del rol ejecutor
                for (Iterator<Actividad> iter = resultado.listIterator(); iter.hasNext();) {
                    Actividad a = iter.next();
                    if (a.getRoles().get(0).equalsIgnoreCase(rolEjecutor)) {
                        iter.remove();
                    }
                }

                //consultar tramites asociados a las actividades
                List<Long> idsTramite = new ArrayList<Long>();
                for (Actividad act : resultado) {
                    idsTramite.add(act.getIdObjetoNegocio());
                }

                tramites = this.tramiteDao.findTramitesByIds(idsTramite);

                for (Tramite t : tramites) {
                    for (int i = 0; i < resultado.size(); i++) {
                        Actividad act = resultado.get(i);
                        if (t.getId().equals(act.getIdObjetoNegocio())) {
                            t.setActividadActualTramite(act);
                            resultado.remove(i);
                            break;
                        }
                    }
                }

            }

        } catch (Exception e) {
            LOGGER.error("Error consultando los tramites para recuperar");
            mensaje = "Error consultando los tramites para recuperar";
        }

        answer[0] = mensaje;
        answer[1] = tramites;
        return answer;
    }

    /**
     * @see ITramiteLocal#obtenerDeterminaTramitePHPorParametrosTramite(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     *
     * @author felipe.cadena
     */
    @Override
    public List<DeterminaTramitePH> obtenerDeterminaTramitePHPorParametrosTramite(String tipoTramite,
        String claseMutacion, String subtipo, String consicionPropiedad) {

        List<DeterminaTramitePH> resultado = null;

        try {
            resultado = this.determinaTramitePHDAO.obtenerPorParametrosTramite(tipoTramite,
                claseMutacion, subtipo, consicionPropiedad);
        } catch (Exception e) {
            LOGGER.error("Error consultado determinaTamite ", e);
        }
        return resultado;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarComisionesTramitePorIdTramite(Long)
     * @author javier.aponte
     */
    @Override
    public List<ComisionTramite> buscarComisionesTramiteNoRealizadasPorIdTramite(
        Long idTramite) {

        List<ComisionTramite> answer = null;

        try {
            answer = this.comisionTramiteDao.buscarComisionesTramiteNoRealizadasPorIdTramite(
                idTramite);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarComisionesTramitePorIdTramite: " +
                ex.getMessage());
        }

        return answer;
    }

    /**
     * @see ITramite#obtenerSolicitantesParaNotificacion(Long)
     * @author felipe.cadena
     */
    @Override
    public List<Solicitante> obtenerSolicitantesParaNotificacion(Long tramiteId) {
        List<Solicitante> resultado = new ArrayList<Solicitante>();

        try {
            resultado = this.solicitanteDao.obtenerSolicitantesPropietariosTramiteMasivo(tramiteId);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarComisionesTramitePorIdTramite: " +
                ex.getMessage());
        }

        return resultado;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ITramite#buscarTramiteCompletoConResolucion(java.lang.Long)
     * @author javier.aponte
     */
    @Implement
    @Override
    public Tramite buscarTramiteConResolucionSimple(Long tramiteId) {

        LOGGER.debug("on TramiteBean#buscarTramiteConResolucionSimple ...");
        Tramite answer = null;

        try {
            answer = this.tramiteDao.buscarTramiteConResolucionSimple(tramiteId);
        } catch (Exception e) {
            LOGGER.error("error on TramiteBean#buscarTramiteConResolucionSimple: " + e.getMessage());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see ITramite#buscarTramitesActualizacionAgrupadoPorPredio
     */
    @Override
    public List<Tramite> buscarTramitesActualizacionAgrupadoPorPredio(
        long[] tramitesIds) {
        List<Tramite> tramites = null;

        try {

            tramites = this.tramiteDao.buscarTramitesActualizacionAgrupadoPorPredio(tramitesIds);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#buscarTramitesActualizacionAgrupadoPorPredio: " +
                e.getMensaje());
        }
        return tramites;
    }

    /**
     * @author felipe.cadena
     * @see ITramite#buscarTramitesActualizacionPorManzana
     */
    @Override
    public List<Tramite> buscarTramitesActualizacionPorManzana(String numeroManzana) {

        List<Tramite> tramites = null;
        List<Predio> prediosManzana;

        try {
            prediosManzana = this.buscaPrediosPorManzana(numeroManzana);
            List<Long> prediosIds = new ArrayList<Long>();
            for (int i = 0; i < prediosManzana.size(); i++) {
                Predio p = prediosManzana.get(i);
                prediosIds.add(p.getId());
            }
            tramites = this.tramiteDao.buscarTramitesEnActualizacion(prediosIds);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#buscarTramitesActualizacionPorManzana: " +
                e.getMensaje());
        }
        return tramites;
    }

    /**
     * @author leidy.gonzalez
     * @see ITramite#buscarTramitesConservacionPorIdTramites
     */
    @Override
    public List<Tramite> buscarTramitesConservacionPorIdTramites(List<Long> tramiteIds) {
        List<Tramite> tramites = null;

        try {

            tramites = this.tramiteDao.buscarTramitesConservacionPorIdTramites(tramiteIds);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#buscarTramitesConservacionPorIdTramites: " +
                e.getMensaje());
        }
        return tramites;
    }

    /**
     * @author @author felipe.cadena
     * @see ITramite#radicarActualizacion
     */
    @Override
    public Object[] radicarActualizacion(Long idMunicipioActualizacion, Long idSolicitud,
        String usuario) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.radicarActualizacion(idMunicipioActualizacion,
                idSolicitud, usuario);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#radicarActualizacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @author @author felipe.cadena
     * @see ITramite#radicarActualizacion
     */
    @Override
    public int validarRadicacionActualizacion(Long idMunicipioActualizacion) {
        int resultado = 0;

        try {

            resultado = this.sncProcedimientoDao.validarRadicacionActualizacion(
                idMunicipioActualizacion);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#validarRadicacionActualizacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @author @author felipe.cadena
     * @see ITramite#validarActualizacion
     */
    @Override
    public Object[] validarActualizacion(Long idMunicipioActualizacion, String usuario) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.validarActualizacion(idMunicipioActualizacion,
                usuario);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#radicarActualizacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @author @author felipe.cadena
     * @see ITramite#eliminarActualizacion
     */
    @Override
    public Object[] eliminarActualizacion(Long idMunicipioActualizacion, String usuario) {
        Object[] resultado = null;

        try {

            resultado = this.sncProcedimientoDao.eliminarActualizacion(idMunicipioActualizacion,
                usuario);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#eliminarActualizacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @see ITramiteLocal#obtenerUrlReporteResolucionesActualizacion
     * @author javier.aponte
     */
    @Override
    public EReporteServiceSNC obtenerUrlReporteResolucionesActualizacion(String tipoTramite,
        String claseMutacion, String subtipo,
        String radicacionEspecial, boolean isRectificacionCancelacionDobleInscripcion) {

        EReporteServiceSNC answer =
            EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_PRIMERA_ACTUALIZACION;

        if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo())) {
            if (claseMutacion.equals(EMutacionClase.PRIMERA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_PRIMERA_ACTUALIZACION;
            }
            if (claseMutacion.equals(EMutacionClase.SEGUNDA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_SEGUNDA_ACTUALIZACION;
            }
            if (claseMutacion.equals(EMutacionClase.TERCERA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_TERCERA_ACTUALIZACION;
                if (ETramiteRadicacionEspecial.TERCERA_MASIVA.getCodigo().equals(radicacionEspecial)) {
                    answer =
                        EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_TERCERA_MASIVA_ACTUALIZACION;
                }
            }
            if (claseMutacion.equals(EMutacionClase.CUARTA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_CUARTA_ACTUALIZACION;
            }
            if (claseMutacion.equals(EMutacionClase.QUINTA.getCodigo())) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_MUTACION_QUINTA_ACTUALIZACION;
            }
        } else if (tipoTramite.equals(ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL.
            getCodigo())) {
            answer = EReporteServiceSNC.MODELO_MODIFICACION_INFORMACION_CATASTRAL_ACTUALIZACION;
        } else if (tipoTramite.equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_CANCELACION_ACTUALIZACION;
        } else if (tipoTramite.equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_COMPLEMENTACION_ACTUALIZACION;
        } else if (tipoTramite.equals(ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_RECTIFICACION_ACTUALIZACION;
            if (isRectificacionCancelacionDobleInscripcion) {
                answer = EReporteServiceSNC.MODELO_RESOLUCION_CANCELACION_ACTUALIZACION;
            }
        } else if (tipoTramite.equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_REVISION_AVALUO_ACTUALIZACION;
        } else if (tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo()) ||
            tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_APELACION.getCodigo()) ||
            tipoTramite.equals(ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.
                getCodigo()) ||
            tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_QUEJA.getCodigo())) {
            answer = EReporteServiceSNC.MODELO_RESOLUCION_VIA_GUBERNATIVA_ACTUALIZACION;
        }
        return answer;
    }

    /**
     * @author @author felipe.cadena
     * @see
     * ITramite#guardarActualizarTramiteReasignacion(co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion)
     */
    @Override
    public TramiteReasignacion guardarActualizarTramiteReasignacion(TramiteReasignacion reasignacion) {
        TramiteReasignacion resultado = null;

        try {

            resultado = this.tramiteReasignacionDAO.update(reasignacion);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#guardarActualizarTramiteReasignacion: " +
                e.getMensaje());
        }
        return resultado;
    }

    /**
     * @author @author felipe.cadena
     * @see ITramite#guardarActualizarTramiteReasignacionMultiple(java.util.List)
     */
    @Override
    public void guardarActualizarTramiteReasignacionMultiple(
        List<TramiteReasignacion> reasignaciones) {

        try {

            this.tramiteReasignacionDAO.updateMultiple(reasignaciones);

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#guardarActualizarTramiteReasignacionMultiple: " +
                e.getMensaje());
        }

    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @see ITramite#findTramitePruebasByTramiteIdNuevaTransaccion(Long)
     * @author david.cifuentes
     */
    @Override
    public Tramite findTramitePruebasByTramiteIdNuevaTransaccion(Long tramiteId) {
        return this.transaccionalService.findTramitePruebasByTramiteIdNuevaTransaccion(tramiteId);
    }

    /**
     * @see ITramite#consultarDocumentacionDeTramiteGenerada(java.lang.Long)
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public List<Tramite> consultarDocumentacionDeTramiteGenerada(Long tramiteId) {
        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.obtenerDocumentosGeneradosPorIdTramite(tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en TramiteBean#consultarDocumentacionDeTramiteGenerada: " + ex);
        }
        return answer;
    }

    /**
     * @see
     * ITramiteLocal#consultarResolucionesPorNumeroResolucionFinalEInicial(productoCatastralDetalle)
     *
     * @author leidy.gonzalez
     */
    @Override
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.consultarResolucionesPorNumeroResolucionFinalEInicial(
                productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    @Override
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.
                consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
                    productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucion(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.consultarResolucionesPorNumeroResolucion(
                productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial que
     * aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.consultarResolucionesPorNumeroResolucionAplicaCambios(
                productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     * en que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.
                consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
                    productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.consultarResolucionesPorFechaResolucionFinalEInicial(
                productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucion(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.consultarResolucionesPorFechaResolucion(
                productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion en
     * que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle) {

        LOGGER.debug(("En TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial"));

        List<Tramite> answer = null;

        try {
            answer = this.tramiteDao.consultarResolucionesPorFechaResolucionAplicaCambios(
                productoCatastralDetalle);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#consultarResolucionesPorNumeroResolucionFinalEInicial: " + ex.
                    getMessage());
        }

        return answer;
    }

    /**
     * @see ITramite#reversarProyeccion(java.lang.Long)
     * @author javier.aponte
     * @param tramite
     */
    @Implement
    @Override
    public boolean reversarProyeccion(Tramite tramite) {

        boolean resultado = false;
        List<PPredio> predioProyectado = this.pPredioDao.findExistingPPrediosByTramiteId(tramite.
            getId());
        if (predioProyectado != null && !predioProyectado.isEmpty()) {

            try {
                resultado = sncProcedimientoDao.reversarProyeccion(tramite.getId());
            } catch (Exception ex) {
                LOGGER.debug("on TramiteBean#reversarProyeccion " +
                    "Ocurrió un error al reversar la proyección para el trámite " + tramite.getId() +
                    ex);
            }

        } else {
            resultado = true;
        }

        if (resultado && tramite.getSolicitud().getTipo().equals(ESolicitudTipo.VIA_GUBERNATIVA.
            getCodigo())) {

            List<TramiteRelacionTramite> tramitesRelacionTramites = this.tramiteRelacionTramiteDAO.
                findTramiteRelacionTramiteByIdTramite(tramite.getId());
            if (tramitesRelacionTramites != null && !tramitesRelacionTramites.isEmpty()) {
                for (TramiteRelacionTramite trt : tramitesRelacionTramites) {

                    this.sncProcedimientoDao.confirmarProyeccion(trt.getTramite1().getId());

                }
            }

        }

        return resultado;
    }

    /**
     * @see ITramite#cancelarTramiteEnProcess(Tramite, String)
     * @author javier.aponte
     * @param tramite
     * @param motivo
     */
    @Override
    public boolean cancelarTramiteEnProcess(Tramite tramite, String motivo) {

        boolean answer = true;

        String motivoCancelacion =
            (motivo.length() > Constantes.TAMANIO_MAXIMO_MOTIVO_CANCELACION) ?
            motivo.substring(0, Constantes.TAMANIO_MAXIMO_MOTIVO_CANCELACION) : motivo;

        try {
            this.procesosService.cancelarProcesoPorIdProceso(
                tramite.getProcesoInstanciaId(),
                motivoCancelacion);
        } catch (Exception ex) {
            answer = false;
            LOGGER.error("Error en TramiteBean#cancelarTramiteEnProcess: " + ex);
        }
        return answer;

    }

    /**
     * Método que elimina los documentos que se relacionaron al TramiteDocumento, cuando la acción
     * no es confirmada por el usuario.
     *
     * @author dumar.penuela
     *
     * @param documentos
     * @param tramitesDocumentacion
     * @param solicitudesDocumentacion
     *
     */
    @Override
    public void eliminarDeTramiteDocumentoDocumentosAsociado(
        List<Documento> documentos,
        List<TramiteDocumentacion> tramitesDocumentacion,
        List<SolicitudDocumentacion> solicitudesDocumentacion) {
        try {

            for (Documento documento : documentos) {
                this.documentoDao
                    .deleteDocumentoAlsoGestorDocumental(documento);
            }

            this.solicitudDocumentacionDAO
                .deleteMultiple(solicitudesDocumentacion);
            this.tramiteDocumentacionDao.deleteMultiple(tramitesDocumentacion);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see ITramite#findTramiteRectificacionesByTramiteId(Long)
     * @author javier.aponte
     */
    public List<TramiteRectificacion> findTramiteRectificacionesByTramiteId(Long tramiteId) {

        LOGGER.debug(("En TramiteBean#findTramiteRectificacionesByTramiteId"));

        List<TramiteRectificacion> answer = null;

        try {
            answer = this.tramiteRectificacionDAO.findTramiteRectificacionesByTramiteId(tramiteId);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#findTramiteRectificacionesByTramiteId: " + ex.
                getMessage());
        }

        return answer;
    }

    /**
     * @author fabio.navarrete
     * @see ITramite#buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(String, String)
     */
    @Override
    public List<Solicitante> buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacionsList(
        String tipoDocumento, String numeroDocumento) {
        return this.solicitanteDao
            .findSolicitanteByTipoIdentAndNumDocFetchRelacionsList(tipoDocumento, numeroDocumento);
    }

    /**
     * @see ITramiteLocal#buscarPredioEnglobesPorPredio(java.lang.Long)
     */
    @Implement
    @Override
    public List<TramiteDetallePredio> buscarPredioEnglobesPorPredio(Long predioId) {

        List<TramiteDetallePredio> answer = null;

        try {
            answer = this.tramiteDao.buscarPredioEnglobesPorPredio(predioId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#buscarPredioEnglobesPorTramite: " + ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#eliminarTramitePredioEnglobe(java.lang.Long)
     */
    @Override
    public void eliminarTramitePredioEnglobe(TramiteDetallePredio registro) {

        try {
            this.tramiteDetallePredioDao.delete(registro);
        } catch (ExcepcionSNC ex) {
            LOGGER.
                error("error en TramiteBean#eliminarTramitePredioEnglobe: " + ex.getMensaje(), ex);
        }

    }

    /**
     * @author dumar.penuela
     * @ITramiteLocal#buscaPredio(String)
     */
    @Override
    public Predio buscaPredio(String numPredial) {
        Predio answer = null;
        try {
            answer = this.predioDao.getPredioByNumeroPredial(numPredial);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscaPredio: " + ex);
        }

        return answer;
    }

    @Override
    public Object[] finalizarRadicacionInterno(String pradicacion, String pradicacionResponde,
        String usuario) {
        try {
            //Ya se finalizan desde TramiteEstadoDAOBean
            //return this.sncProcedimientoDao.finalizarRadicacionInterno(usuario, pradicacion, pradicacionResponde);
            return null;
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Documento> obtenerDocumentosComisionPorTramiteId(final Long idTramite) {
        return this.documentoDao.obtenerDocumentosComisionPorTramiteId(idTramite);
    }

    /**
     * @see
     * ITramiteLocal#actualizarEstadoTramite(co.gov.igac.snc.persistence.entity.tramite.Tramite,
     * co.gov.igac.snc.persistence.util.ETramiteEstado)
     * @author felipe.cadena
     */
    @Override
    public TramiteEstado actualizarEstadoTramite(Tramite tramite, ETramiteEstado nuevoEstado,
        UsuarioDTO usuario) {
        TramiteEstado tramiteEstado;
        if (tramite.isEnEstadoFinal()) {
            LOGGER.warn("No se puede actualizar el estado del tramite " + tramite.getEstado() +
                " -->> " + nuevoEstado.getCodigo());
            return null;
        }
        try {
            tramiteEstado = new TramiteEstado(nuevoEstado.getCodigo(), tramite, usuario.getLogin());
            tramiteEstado = this.tramiteEstadoDao.update(tramiteEstado);
            LOGGER.info("Tramite estado Actualizado" + tramiteEstado);
            tramite.setTramiteEstado(tramiteEstado);

        } catch (Exception ex) {
            LOGGER.error("No se puede actualizar el estado del tramite " +
                tramite.getEstado() + " -->> " + nuevoEstado.getCodigo(), ex);
            return null;
        }
        return tramiteEstado;
    }

    //--------------------------------------------------------------------------------------------------
    @Override
    public List<UsuarioDTO> buscarFuncionariosPorRolYEstructuraOrganizacional(
        String nombreEstructuraOrganizacional, ERol rol) {

        List<UsuarioDTO> answer = null;
        try {
            answer = setEstructuraOrganizacional(LdapDAO.getUsuariosEstructuraRolYUOC(
                nombreEstructuraOrganizacional, rol));
        } catch (Exception ex) {
            LOGGER.error(
                "Error en TramiteBean#buscarFuncionariosPorRolYEstructuraOrganizacional: " +
                ex.getMessage());
        }
        return answer;
    }

    // -------------------------------------- //
    /**
     * @author david.cifuentes
     * @see ITramite#buscarTodosFuncionarios()
     */
    @Override
    public List<UsuarioDTO> buscarTodosFuncionarios() {
        List<UsuarioDTO> answer = null;

        try {
            answer = setEstructuraOrganizacional(LdapDAO.listAllUsers());
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#buscarFuncionarios: " +
                ex.getMessage());
        }

        return answer;
    }

    /**
     * Metodo para consultar la configuracion de los reportes prediales por tipo de informe, para
     * visualizar en
     *
     * @author leidy.gonzalez
     */
    @Implement
    @Override
    public RepConsultaPredial getConfiguracionReportePredial(Long tipoInforme) {

        LOGGER.debug("en TramiteBean#getConfiguracionReportePredial");

        RepConsultaPredial answer = null;
        try {
            answer = this.tramiteDao.getConfiguracionReportePredial(tipoInforme);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#getConfiguracionReportePredial: " + ex.getMensaje(),
                ex);
        }
        return answer;
    }

    /**
     * @see ITramiteLocal#obtenerTareasGeograficasPaginado(UsuarioDTO)
     * @description Optimización del método {@link
     *              TramiteBean.obtenerTareasGeograficas3(UsuarioDTO)}
     * @author david.cifuentes
     */
    @Override
    public ResultadoVO obtenerTareasGeograficasPaginado(UsuarioDTO usuario) {

        LOGGER.info("Inicio generacion XML de lista de tareas para el usuario: " + usuario.
            getLogin());

        ResultadoVO resultadoTareasPendientes = null;
        List<Actividad> actividades = new ArrayList<Actividad>();
        List<Long> idsTramite = new ArrayList<Long>();

        //List<Actividad> lstActividades = this.procesosService.consultarListaActividades(usuario);
        List<Actividad> lstActividades = this.procesosService.
            consultarListaActividadesPorActividades(usuario);

        if (lstActividades != null) {
            for (Actividad act : lstActividades) {
                if (act.getNombre()
                    .equals(ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {
                    actividades.add(act);
                    idsTramite.add(act.getIdObjetoNegocio());
                }
            }
        }
        
        

        // No hay actividades para edicion geografica
        if (actividades.isEmpty() || (lstActividades == null || lstActividades.isEmpty())) {
            LOGGER.warn("Lista de actividades vacia!!!: procesosService.consultarListaActividadesPorActividades");
            ResultadoVO vo = new ResultadoVO(usuario, new ArrayList<ActividadVO>());
            return vo;
        }
        
        
        for (Actividad lact : lstActividades) {
            LOGGER.debug(lact == null ? "Actividad nula ":String.valueOf(lact.getIdObjetoNegocio()));
        }

        LOGGER.info("Las actividades pendientes para edicion geografica son: " +
            actividades.size() + ". Usuario: " + usuario.getLogin());

        // ActividadesVO para mostrar en el XML
        List<ActividadVO> actividadesVo = new LinkedList<ActividadVO>();
        try {
            // Búsqueda de tramites para edición geográfica por su id, sin que tengan un producto catatral job sin terminar geográficamente.
            List<Tramite> tramitesEdicionGeoSinPCJ = this.tramiteDao.
                getTramiteParaEdicionGeograficaSinProductoCatastralJobTerminado(idsTramite);

            LOGGER.info("Cantidad de tramites: " + tramitesEdicionGeoSinPCJ.size());

            if (tramitesEdicionGeoSinPCJ != null && !tramitesEdicionGeoSinPCJ.isEmpty()) {

                for (Tramite tramite : tramitesEdicionGeoSinPCJ) {

                    EqualPredicate nameEqlPredicate = new EqualPredicate(tramite.getId());
                    BeanPredicate beanPredicate = new BeanPredicate("idObjetoNegocio",
                        nameEqlPredicate);
                    Actividad actividad = (Actividad) CollectionUtils.find(actividades,
                        beanPredicate);
                    ActividadVO actividadVo = new ActividadVO(actividad, tramite);
                    actividadesVo.add(actividadVo);

                }
                resultadoTareasPendientes = new ResultadoVO(usuario, actividadesVo);
            }
            LOGGER.info("Resultado de obtenerTareasGeograficasPaginado: " + actividadesVo.size());
            return resultadoTareasPendientes;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_GENERACION_LISTA_TAREAS_EDICION_GEOGRAFICA
                .getExcepcion(LOGGER, e);
        }
    }

    @Override
    public void asociarInfoCampoAdicional(EInfoAdicionalCampo campo, String valor) {

        try {

            this.tramiteDao.asociarInfoCampoAdicional(campo, valor);

        } catch (NoResultException nre) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nre);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "TramiteDAOBean#asociarInfoCampoAdicional");
        }

    }

    /**
     * @see ITramiteLocal#eliminarPFichaMatrizPT(java.lang.Long)
     * @author fabio.navarrete
     */
    @Override
    public void eliminarPFichaMatrizPT(PFichaMatrizPredioTerreno registro) {

        try {
            this.pfichaMatrizPredioTerrenoDAO.delete(registro);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#eliminarPFichaMatrizPT: " + ex.getMensaje(), ex);
        }

    }

    /**
     * @see ITramite#generarProyeccionCancelacionBasica(Long, String)
     * @author felipe.cadena
     */
    @Override
    public Object[] aceptarEnglobeVirtual(Long tramiteId, String usuario) {
        try {
            return this.sncProcedimientoDao.aceptarEnglobeVirtual(tramiteId, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @see ITramite#generarNumeroEnglobeVirtual(String, String)
     * @author felipe.cadena
     */
    @Override
    public Object[] generarNumeroEnglobeVirtual(Long tramiteId, String condicionPropiedad,
        String usuario) {
        try {
            return this.sncProcedimientoDao.generarNumeroEnglobeVirtual(tramiteId,
                condicionPropiedad, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * @author felipe.cadena
     * @see ITramiteLocal#eliminarTramiteDetallePredioMultiple(List<TramiteDetallePredio>)
     */
    @Override
    public void eliminarTramiteDetallePredioMultiple(List<TramiteDetallePredio> tdps) {
        try {
            this.tramiteDetallePredioDao.deleteMultiple(tdps);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#eliminarTramiteDetallePredioMultiple: " + e);
        }
    }

    /**
     * @see ITramiteLocal#obtenerDocNotificacion(Long,String,String)
     * @author leidy.gonzalez
     */
    @Override
    public TramiteDocumento obtenerDocNotificacion(
        Long idTramite, String docPersonaNotificacion, String nombrePersonaNotif) {

        LOGGER.debug("on TramiteBean#obtenerDocNotificacion ");

        TramiteDocumento answer = null;

        try {
            answer = this.tramiteDocumentoDao.obtenerDocNotificacion(
                idTramite, docPersonaNotificacion, nombrePersonaNotif);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#obtenerDocNotificacion: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @see ITramiteLocal#obtenerTramiteDocumentoParaRegistrosNotificacion(Long,List<String>)
     * @author leidy.gonzalez
     */
    @Override
    public List<TramiteDocumento> obtenerTramiteDocumentosParaRegistrosNotificacion(
        Long idTramite, List<String> docsPersonaNotificacion) {

        LOGGER.debug("on TramiteBean#obtenerTramiteDocumentosParaRegistrosNotificacion ");

        List<TramiteDocumento> answer = null;

        try {
            answer = this.tramiteDocumentoDao.obtenerTramiteDocumentosParaRegistrosNotificacion(
                idTramite, docsPersonaNotificacion);
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en TramiteBean#obtenerTramiteDocumentosParaRegistrosNotificacion: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author leidy.gonzalez
     * @see ITramiteLocal#guardarYActualizarTramiteDocumentoMultiple(List<TramiteDetallePredio>)
     */
    @Override
    public void guardarYActualizarTramiteDocumentoMultiple(
        List<TramiteDocumento> tds) {
        try {
            this.tramiteDocumentoDao.updateMultiple(tds);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#guardarYActualizarTramiteDocumentoMultiple: " + e);
        }
    }

    /**
     * @see ITramiteLocal#obtenerDocComunicacionYAutorizacion(Long,String,String)
     * @author leidy.gonzalez
     */
    @Override
    public List<TramiteDocumento> obtenerDocsComunicacionYAutorizacion(
        Long idTramite, String docPersonaNotificacion, String nombrePersonaNotif) {

        LOGGER.debug("on TramiteBean#obtenerDocNotificacion ");

        List<TramiteDocumento> answer = null;

        try {
            answer = this.tramiteDocumentoDao.obtenerDocsComunicacionYAutorizacion(
                idTramite, docPersonaNotificacion, nombrePersonaNotif);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#obtenerDocNotificacion: " +
                ex.getMensaje());
        }

        return answer;
    }

    /**
     * @author felipe.cadena
     * @see ITramiteLocal#eliminarTramiteInfoMultiple(List<TramiteInfoAdicional>)
     */
    @Override
    public void eliminarTramiteInfoMultiple(List<TramiteInfoAdicional> tias) {
        try {
            this.infoAdicionalDao.deleteMultiple(tias);
        } catch (ExcepcionSNC e) {
            LOGGER.error("Error en TramiteBean#eliminarTramiteInfoMultiple: " + e, e);
        }
    }

    /**
     * @author leidy.gonzalez
     * @see ITramiteLocal#obtenerFichasAsociadasATramite(Long tramiteId)
     */
    @Override
    public List<PFichaMatriz> obtenerFichasAsociadasATramite(Long tramiteId) {
        List<PFichaMatriz> fichaMatrizAsociadaAlTramite = null;

        try {
            fichaMatrizAsociadaAlTramite = this.pFichaMatrizDao.obtenerFichasAsociadasATramite(
                tramiteId);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en TramiteBean#obtenerFichasAsociadasATramite: " +
                ex.getMensaje());
        }

        return fichaMatrizAsociadaAlTramite;
    }

    /**
     * @see ITramite#obtenerSolicitantesPropietariosCond0Y5(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<Solicitante> obtenerSolicitantesParaNotificacionEV(Long tramiteId) {
        List<Solicitante> resultado = new ArrayList<Solicitante>();

        try {
            resultado = this.solicitanteDao.obtenerSolicitantesPropietariosEV(tramiteId);
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#obtenerSolicitantesPropietariosCond0Y5: " +
                ex.getMessage());
        }

        return resultado;

    }

    /**
     * @see ITramite#obtenerTipoTramiteCompuestoCodigo()
     * @author felipe.cadena
     */
    @Override
    public List<String[]> obtenerTipoTramiteCompuestoCodigo() {
        List<String[]> resultado = new ArrayList<String[]>();

        try {
            resultado = this.tramiteDao.getTipoTramiteCompuestoCodigo();
        } catch (Exception ex) {
            LOGGER.error("Error en TramiteBean#obtenerTipoTramiteCompuestoCodigo: " +
                ex.getMessage());
        }

        return resultado;

    }

// end of class    
}
