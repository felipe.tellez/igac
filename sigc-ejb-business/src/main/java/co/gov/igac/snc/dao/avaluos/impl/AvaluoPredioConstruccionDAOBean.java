package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioConstruccionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoPredioConstruccionDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoPredioConstruccionDAOBean extends
    GenericDAOWithJPA<AvaluoPredioConstruccion, Long> implements IAvaluoPredioConstruccionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        AvaluoPredioConstruccionDAOBean.class);

    /**
     * @see IAvaluoPredioConstruccionDAO#calcularConstruccionesPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Boolean calcularConstruccionesPreviasAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "SELECT o FROM AvaluoPredioConstruccion o " +
            "JOIN o.avaluo ava WHERE ava.id = :idAvaluo";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            List<AvaluoPredioConstruccion> apzs = query.getResultList();
            if (apzs == null || apzs.isEmpty()) {
                return null;
            }

            if (apzs.get(0).getAvaluoPredioId() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#calcularCultivosPreviosAvaluo");
        }
    }

    /**
     * @see IAvaluoPredioConstruccionDAO#eliminarConstruccionesPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public boolean eliminarConstruccionesPreviasAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "DELETE FROM avaluo_predio_construccion ctv WHERE ctv.avaluo_id = " + idAvaluo;

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.executeUpdate();
            return true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#eliminarConstruccionesPreviasAvaluo");
        }
    }

}
