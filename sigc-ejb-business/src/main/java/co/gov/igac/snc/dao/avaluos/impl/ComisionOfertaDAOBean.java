/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IComisionOfertaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Stateless
public class ComisionOfertaDAOBean extends GenericDAOWithJPA<ComisionOferta, Long> implements
    IComisionOfertaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaCapturaOfertaDAOBean.class);

//--------------    methods   ----------------
    /**
     * @see IComisionOfertaDAO#getComisionesOfertaPorEstadoAndRCO(String, List)
     * @author pedro.garcia
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<ComisionOferta> getComisionesOfertaPorEstadoAndRCO(String estadoComision,
        List<Long> regionesCapturaOfertaIds) {

        LOGGER.debug("on ComisionOfertaDAO#getComisionesOfertaPorEstadoAndRCO");

        List<ComisionOferta> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT co " +
            " FROM RegionCapturaOferta rco " +
            " JOIN rco.comisionOferta co " +
            " JOIN rco.areaCapturaOferta aco " +
            " WHERE co.estado = :estadoComisionP " +
            " AND rco.id IN :regionesCapturaOfertaIds";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("estadoComisionP", estadoComision);
            query.setParameter("regionesCapturaOfertaIds", regionesCapturaOfertaIds);

            answer = (List<ComisionOferta>) query.getResultList();

            for (ComisionOferta comisionOferta : answer) {
                Hibernate.initialize(comisionOferta.getRegionCapturaOfertas());
                for (RegionCapturaOferta region : comisionOferta.getRegionCapturaOfertas()) {
                    Hibernate.initialize(region.getAreaCapturaOferta());
                    Hibernate.initialize(region.getAreaCapturaOferta().getDepartamento());
                    Hibernate.initialize(region.getAreaCapturaOferta().getMunicipio());
                }
                Hibernate.initialize(comisionOferta.getOficioDocumento());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ComisionOfertaDAOBean#getComisionesOfertaPorEstadoAndACO");
        }

        return answer;

    }

//end of class
}
