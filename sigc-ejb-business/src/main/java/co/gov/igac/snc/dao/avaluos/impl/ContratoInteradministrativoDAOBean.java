package co.gov.igac.snc.dao.avaluos.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IContratoInteradministrativoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IContratoInteradministrativoDAO
 * @author felipe.cadena
 */
@Stateless
public class ContratoInteradministrativoDAOBean extends
    GenericDAOWithJPA<ContratoInteradministrativo, Long> implements
    IContratoInteradministrativoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ContratoInteradministrativoDAOBean.class);

    /**
     * @see IContratoInteradministrativoDAO#buscarContratosPorFiltro(FiltroDatosConsultaSolicitante,
     * FiltroDatosConsultaContratoInteradministrativo)
     *
     * @author felipe.cadena
     *
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<ContratoInteradministrativo> buscarContratosPorFiltro(
        FiltroDatosConsultaSolicitante filtroSolicitante,
        FiltroDatosConsultaContratoInteradministrativo filtroContrato) {

        List<ContratoInteradministrativo> resultado;

        LOGGER.debug("on ContratoInteradministrativoDAO#buscarContratosPorFiltro");

        StringBuilder query = new StringBuilder();

        query.append("SELECT DISTINCT c FROM ContratoInteradministrativo c " +
            " LEFT JOIN FETCH c.entidadSolicitante es " +
            " LEFT JOIN FETCH c.interventor i ");

        query.append(" WHERE 1=1 ");

        if (filtroSolicitante != null) {
            if (filtroSolicitante.getTipoPersona() != null &&
                !filtroSolicitante.getTipoPersona().isEmpty()) {
                query.append(" AND es.tipoPersona LIKE :tipoPersona");
            }

            if (filtroSolicitante.getSigla() != null &&
                !filtroSolicitante.getSigla().isEmpty()) {
                query.append(" AND es.sigla LIKE :sigla");
            }

            if (filtroSolicitante.getRazonSocial() != null &&
                !filtroSolicitante.getRazonSocial().isEmpty()) {
                query.append(" AND es.razonSocial LIKE :razonSocial");
            }

            if (filtroSolicitante.getTipoIdentificacion() != null &&
                !filtroSolicitante.getTipoIdentificacion().isEmpty()) {
                query.append(" AND es.tipoIdentificacion LIKE :tipoIdentificacion");
            }

            if (filtroSolicitante.getNumeroIdentificacion() != null &&
                !filtroSolicitante.getNumeroIdentificacion().isEmpty()) {
                query.append(" AND es.numeroIdentificacion LIKE :numeroIdentificacion");
            }

            if (filtroSolicitante.getDigitoVerificacion() != null &&
                !filtroSolicitante.getDigitoVerificacion().isEmpty()) {
                query.append(" AND es.digitoVerificacion LIKE :digitoVerificacion");
            }

            if (filtroSolicitante.getPrimerNombre() != null &&
                !filtroSolicitante.getPrimerNombre().isEmpty()) {
                query.append(
                    " AND CONCAT (es.primerNombre, es.segundoNombre, es.primerApellido, es.segundoApellido) " +
                    " LIKE CONCAT('%',:nombreCompleto,'%')");
            }
        }

        if (filtroContrato != null) {
            if (filtroContrato.getNumeroContrato() != null &&
                !filtroContrato.getNumeroContrato().isEmpty()) {
                query.append(" AND c.numero LIKE :numeroContrato");
            }

            if (filtroContrato.getNombreCompletoInterventor() != null &&
                !filtroContrato.getNombreCompletoInterventor().isEmpty()) {
                query.append(
                    " AND CONCAT (i.primerNombre, i.segundoNombre, i.primerApellido, i.segundoApellido) " +
                    " LIKE CONCAT('%',:nombreCompletoInterventor,'%')");
            }

            if (filtroContrato.getNumeroDocumentoInterventor() != null &&
                !filtroContrato.getNumeroDocumentoInterventor().isEmpty()) {
                query.append(" AND i.numeroIdentificacion LIKE :interventorNumeroIdentificacion");
            }

            if (filtroContrato.getIdInterventor() != null) {
                query.append(" AND i.id LIKE :idInterventor");
            }

            if (filtroContrato.getVigencia() != null) {
                query.append(" AND c.vigencia = :vigencia");
            }

            if (filtroContrato.isActivo()) {
                query.append(" AND c.fechaTerminacion > :fechaSistema");
            }

            if (filtroContrato.getEntidadSolicitanteSolicitudId() != null) {
                query.append(" AND es.id = (" +
                    " SELECT ss.solicitante.id " +
                    " FROM SolicitanteSolicitud ss " +
                    " WHERE ss.id = :entidadSolicitanteSolicitudId)");
            }

        }

        LOGGER.debug("Query: " + query.toString());
        Query q = entityManager.createQuery(query.toString());

        if (filtroSolicitante != null) {
            if (filtroSolicitante.getTipoPersona() != null && !filtroSolicitante.getTipoPersona().
                isEmpty()) {
                q.setParameter("tipoPersona", filtroSolicitante.getTipoPersona());
            }

            if (filtroSolicitante.getSigla() != null && !filtroSolicitante.getSigla().isEmpty()) {
                q.setParameter("sigla", filtroSolicitante.getSigla());
            }

            if (filtroSolicitante.getRazonSocial() != null && !filtroSolicitante.getRazonSocial().
                isEmpty()) {
                q.setParameter("razonSocial", filtroSolicitante.getRazonSocial());
            }

            if (filtroSolicitante.getTipoIdentificacion() != null && !filtroSolicitante.
                getTipoIdentificacion().isEmpty()) {
                q.setParameter("tipoIdentificacion", filtroSolicitante.getTipoIdentificacion());
            }

            if (filtroSolicitante.getNumeroIdentificacion() != null && !filtroSolicitante.
                getNumeroIdentificacion().isEmpty()) {
                q.setParameter("numeroIdentificacion", filtroSolicitante.getNumeroIdentificacion());
            }

            if (filtroSolicitante.getDigitoVerificacion() != null && !filtroSolicitante.
                getDigitoVerificacion().isEmpty()) {
                q.setParameter("digitoVerificacion", filtroSolicitante.getDigitoVerificacion());
            }

            if (filtroSolicitante.getPrimerNombre() != null && !filtroSolicitante.getPrimerNombre().
                isEmpty()) {
                String param = filtroSolicitante.getPrimerNombre();
                param = param.replace(" ", "%");
                q.setParameter("nombreCompleto", param);
            }
        }

        if (filtroContrato != null) {

            if (filtroContrato.getNumeroContrato() != null &&
                !filtroContrato.getNumeroContrato().isEmpty()) {
                q.setParameter("numeroContrato",
                    filtroContrato.getNumeroContrato());
            }

            if (filtroContrato.getNombreCompletoInterventor() != null && !filtroContrato.
                getNombreCompletoInterventor().isEmpty()) {
                String param = filtroContrato.getNombreCompletoInterventor();
                param = param.replace(" ", "%");
                q.setParameter("nombreCompletoInterventor", param);
            }

            if (filtroContrato.getNumeroDocumentoInterventor() != null &&
                !filtroContrato.getNumeroDocumentoInterventor().isEmpty()) {
                q.setParameter("interventorNumeroIdentificacion",
                    filtroContrato.getNumeroDocumentoInterventor());
            }

            if (filtroContrato.getIdInterventor() != null) {
                q.setParameter("idInterventor",
                    filtroContrato.getIdInterventor());
            }

            if (filtroContrato.getVigencia() != null) {
                q.setParameter("vigencia", filtroContrato.getVigencia());
            }

            if (filtroContrato.isActivo()) {
                q.setParameter("fechaSistema", new Date());
            }

            if (filtroContrato.getEntidadSolicitanteSolicitudId() != null) {
                q.setParameter("entidadSolicitanteSolicitudId",
                    filtroContrato.getEntidadSolicitanteSolicitudId());
            }
        }

        try {
            resultado = (List<ContratoInteradministrativo>) q.getResultList();

            for (ContratoInteradministrativo contratoInteradministrativo : resultado) {
                Hibernate.initialize(contratoInteradministrativo
                    .getInterventor());
            }

        } catch (Exception ex) {
            LOGGER.error("Error en la consulta de contratos interadministrativos: " +
                ex.getMessage());
            return null;
        }
        return resultado;
    }

    /**
     * @see IContratoInteradministrativoDAO#buscarPorIdConEntidadSolicitante(Long)
     * @author christian.rodriguez
     */
    @Override
    public ContratoInteradministrativo buscarPorIdConEntidadSolicitante(Long idContrato) {

        ContratoInteradministrativo answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ci " +
            " FROM ContratoInteradministrativo ci " +
            " JOIN FETCH ci.entidadSolicitante" +
            " WHERE ci.id = :idContrato ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);

            answer = (ContratoInteradministrativo) query.getSingleResult();

            Hibernate.initialize(answer.getInterventor());

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, e,
                    e.getMessage());
        }

        return answer;
    }

    /**
     * @see IContratoInteradministrativoDAO#buscarPorIdConConsignaciones(Long)
     * @author christian.rodriguez
     */
    @Override
    public ContratoInteradministrativo buscarPorIdConConsignaciones(Long idContrato) {

        ContratoInteradministrativo answer = null;
        String queryString;
        Query query;

        queryString = "SELECT ci " +
            " FROM ContratoInteradministrativo ci " +
            " JOIN FETCH ci.consignaciones" +
            " WHERE ci.id = :idContrato ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);

            answer = (ContratoInteradministrativo) query.getSingleResult();
            Hibernate.initialize(answer.getInterventor());

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, e,
                    e.getMessage());
        }

        return answer;
    }

    /**
     * @see IContratoInteradministrativoDAO#obtenerTotalAdicionesPorIdContrato(Long)
     * @author christian.rodriguez
     */
    @Override
    public Double obtenerTotalAdicionesPorIdContrato(Long idContrato) {
        LOGGER.debug(
            "Iniciando ContratoInteradminisAdicionDAOBean#obtenerTotalAdicionesPorIdContrato");

        Double resultadoSumatoriaAdiciones = null;

        String queryString;
        Query query;

        queryString = "SELECT SUM(cia.valor) " + " FROM ContratoInteradminisAdicion cia " +
            " WHERE cia.contratoInteradminsitrativoId = :idContrato ";

        try {

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            resultadoSumatoriaAdiciones = (Double) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ContratoInteradminisAdicionDAOBean#obtenerTotalAdicionesPorIdContrato");

        }
        return resultadoSumatoriaAdiciones;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IContratoInteradministrativoDAO#obtenerPorId(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ContratoInteradministrativo obtenerPorId(Long idContrato) {

        ContratoInteradministrativo answer = null;

        try {
            answer = this.findById(idContrato);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, ex,
                    "ContratoInteradministrativo", idContrato.toString());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IContratoInteradministrativoDAO#obtenerPorIdConFetching(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public ContratoInteradministrativo obtenerPorIdConFetching(Long idContrato) {

        ContratoInteradministrativo answer = null;

        try {
            answer = this.findById(idContrato);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.
                getExcepcion(LOGGER, ex,
                    "ContratoInteradministrativo", idContrato.toString());
        }

        try {
            Hibernate.initialize(answer.getInterventor());
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_HIBERNATE_INITIALIZE.getExcepcion(LOGGER,
                ex, "ContratoInteradministrativoDAOBean#obtenerPorIdConFetching",
                "ContratoInteradministrativo");
        }

        return answer;

    }

//end of class
}
