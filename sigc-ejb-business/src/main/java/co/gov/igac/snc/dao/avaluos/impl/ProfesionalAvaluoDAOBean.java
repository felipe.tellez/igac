package co.gov.igac.snc.dao.avaluos.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IProfesionalAvaluoDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class ProfesionalAvaluoDAOBean extends GenericDAOWithJPA<ProfesionalAvaluo, Long> implements
    IProfesionalAvaluoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfesionalAvaluo.class);

    /**
     * @see IProfesionalAvaluoDAO#obtenerByIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Override
    public ProfesionalAvaluo obtenerByIdConAtributos(Long idAvaluador) {

        ProfesionalAvaluo avaluador = new ProfesionalAvaluo();

        try {

            avaluador = this.findById(idAvaluador);

            if (avaluador != null) {
                Hibernate.initialize(avaluador.getDireccionDepartamento());
                Hibernate.initialize(avaluador.getDireccionMunicipio());
                Hibernate.initialize(avaluador.getTerritorial());
                Hibernate.initialize(avaluador.getProfesion());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }
        return avaluador;
    }

    /**
     * @see IProfesionalAvaluoDAO#consultarPorSecRadicadoYActividad(String, String)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalAvaluo> consultarPorSecRadicadoYActividad(String secRadicado,
        String actividad) {
        LOGGER.debug("on ProfesionalAvaluoDAOBean#consultarPorSecRadicadoYActividad ");

        List<ProfesionalAvaluo> resultado = null;
        String queryString;
        Query query;

        queryString = "SELECT pa " +
            " FROM ProfesionalAvaluo pa " +
            " WHERE pa IN " +
            " (SELECT aap.profesionalAvaluo.id " +
            " FROM AvaluoAsignacionProfesional aap " +
            " WHERE aap.avaluo.secRadicado = :secRadicado " +
            " AND aap.actividad = :actividad" +
            " AND aap.fechaHasta IS NULL)";

        LOGGER.debug(queryString);
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("secRadicado", secRadicado);
            query.setParameter("actividad", actividad);

            resultado = (List<ProfesionalAvaluo>) query.getResultList();

            for (ProfesionalAvaluo profesionalAvaluo : resultado) {
                Hibernate.initialize(profesionalAvaluo
                    .getProfesionalAvaluosContratos());

                for (ProfesionalAvaluosContrato contrato : profesionalAvaluo
                    .getProfesionalAvaluosContratos()) {
                    Hibernate.initialize(contrato.getInterventor());
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ProfesionalAvaluoDAOBean#consultarPorSecRadicadoYActividad");

        }

        return resultado;
    }

    /**
     * @see IProfesionalAvaluoDAO#buscarProfesionalesPorNombre(String)
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProfesionalAvaluo> buscarProfesionalesPorNombreYterritorial(String nombreProfesional,
        String codigoTerritorial) {
        LOGGER.debug("on ProfesionalAvaluoDAOBean#buscarProfesionalesPorNombre ");
        List<ProfesionalAvaluo> resultado = null;

        StringBuilder queryString;
        Query query;

        queryString = new StringBuilder();
        queryString.append("SELECT pa " +
            " FROM ProfesionalAvaluo pa " +
            " LEFT JOIN pa.territorial territorial" +
            " WHERE pa.activo = 'SI'");

        if (codigoTerritorial != null && !"".equals(codigoTerritorial)) {
            queryString.append(" AND territorial.codigo = :territorial");
        }
        if (nombreProfesional != null && !"".equals(nombreProfesional)) {
            queryString.append("CONCAT (UPPER(pa.primerNombre), " +
                " UPPER(pa.segundoNombre), UPPER(pa.primerApellido), " +
                " UPPER(pa.segundoApellido)) " +
                " LIKE CONCAT('%',:nombreAvaluador,'%')");
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());

            if (codigoTerritorial != null && !"".equals(codigoTerritorial)) {
                query.setParameter("territorial", codigoTerritorial);
            }
            if (nombreProfesional != null && !"".equals(nombreProfesional)) {
                nombreProfesional = nombreProfesional.replace(" ", "%");
                query.setParameter("nombreAvaluador", nombreProfesional.toUpperCase());
            }
            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ProfesionalAvaluoDAOBean#buscarProfesionalesPorNombre");

        }
        return resultado;
    }

    // -------------------------------------------------------------------------------------
    /**
     * @see IProfesionalAvaluoDAO#buscarPorFiltro(FiltroDatosConsultaProfesionalAvaluos)
     * @author christian.rodriguez
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<ProfesionalAvaluo> buscarPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtro) {
        LOGGER.debug("inicio ProfesionalAvaluoDAOBean#buscarPorFiltro");

        List<ProfesionalAvaluo> profesionalesAvaluo = null;
        StringBuilder queryString = new StringBuilder();
        Query query = null;

        queryString.append("SELECT pa" + " FROM ProfesionalAvaluo pa " +
            " WHERE 1=1");

        if (filtro.getTipoIdentificacion() != null &&
            !filtro.getTipoIdentificacion().isEmpty()) {
            queryString
                .append(" AND UPPER(pa.tipoIdentificacion) = UPPER(:tipoIdentificacion)");
        }

        if (filtro.getNumeroIdentificacion() != null &&
            !filtro.getNumeroIdentificacion().isEmpty()) {
            queryString
                .append(" AND pa.numeroIdentificacion = :numeroIdentifiacion");
        }

        if (filtro.getPrimerNombre() != null &&
            !filtro.getPrimerNombre().isEmpty()) {
            queryString
                .append(" AND UPPER(pa.primerNombre) = UPPER(:primerNombre)");
        }

        if (filtro.getSegundoNombre() != null &&
            !filtro.getSegundoNombre().isEmpty()) {
            queryString
                .append(" AND UPPER(pa.segundoNombre) = UPPER(:segundoNombre)");
        }

        if (filtro.getPrimerApellido() != null &&
            !filtro.getPrimerApellido().isEmpty()) {
            queryString
                .append(" AND UPPER(pa.primerApellido) = UPPER(:primerApellido)");
        }

        if (filtro.getSegundoApellido() != null &&
            !filtro.getSegundoApellido().isEmpty()) {
            queryString
                .append(" AND UPPER(pa.segundoApellido) = UPPER(:segundoApellido)");
        }

        if (filtro.getProfesionId() != null) {
            queryString.append(" AND pa.profesion.id = :profesionId");
        }

        if (filtro.getTerritorialCodigoProfesional() != null &&
            !filtro.getTerritorialCodigoProfesional().isEmpty()) {
            queryString
                .append(" AND pa.territorial.codigo = :territorialCodigo");
        }
        if (filtro.getTipoVinculacion() != null &&
            !filtro.getTipoVinculacion().isEmpty()) {
            queryString
                .append(" AND pa.tipoVinculacion = :tipoVinculacion");
        }

        String queryStr = queryString.toString();
        query = this.entityManager.createQuery(queryStr);

        if (filtro.getTipoIdentificacion() != null &&
            !filtro.getTipoIdentificacion().isEmpty()) {
            query.setParameter("tipoIdentificacion",
                filtro.getTipoIdentificacion());
        }

        if (filtro.getNumeroIdentificacion() != null &&
            !filtro.getNumeroIdentificacion().isEmpty()) {
            query.setParameter("numeroIdentifiacion",
                filtro.getNumeroIdentificacion());
        }
        if (filtro.getPrimerNombre() != null &&
            !filtro.getPrimerNombre().isEmpty()) {
            query.setParameter("primerNombre", filtro.getPrimerNombre());
        }
        if (filtro.getSegundoNombre() != null &&
            !filtro.getSegundoNombre().isEmpty()) {
            query.setParameter("segundoNombre", filtro.getSegundoNombre());
        }
        if (filtro.getPrimerApellido() != null &&
            !filtro.getPrimerApellido().isEmpty()) {
            query.setParameter("primerApellido", filtro.getPrimerApellido());
        }

        if (filtro.getSegundoApellido() != null &&
            !filtro.getSegundoApellido().isEmpty()) {
            query.setParameter("segundoApellido", filtro.getSegundoApellido());
        }

        if (filtro.getProfesionId() != null) {
            query.setParameter("profesionId", filtro.getProfesionId());
        }

        if (filtro.getTerritorialCodigoProfesional() != null &&
            !filtro.getTerritorialCodigoProfesional().isEmpty()) {
            query.setParameter("territorialCodigo",
                filtro.getTerritorialCodigoProfesional());
        }
        if (filtro.getTipoVinculacion() != null &&
            !filtro.getTipoVinculacion().isEmpty()) {
            query.setParameter("tipoVinculacion",
                filtro.getTipoVinculacion());
        }

        profesionalesAvaluo = (List<ProfesionalAvaluo>) query.getResultList();

        if (profesionalesAvaluo != null) {
            for (ProfesionalAvaluo profesionalAvaluo : profesionalesAvaluo) {
                Hibernate.initialize(profesionalAvaluo
                    .getDireccionDepartamento());
                Hibernate.initialize(profesionalAvaluo.getDireccionMunicipio());
                Hibernate.initialize(profesionalAvaluo.getTerritorial());
                Hibernate.initialize(profesionalAvaluo.getProfesion());
                Hibernate.initialize(profesionalAvaluo.getContratoInteradministrativos());
                Hibernate.initialize(profesionalAvaluo.getProfesionalAvaluosContratos());

                for (ContratoInteradministrativo ci : profesionalAvaluo.
                    getContratoInteradministrativos()) {
                    Hibernate.initialize(ci.getEntidadSolicitante());
                }
            }
        }

        LOGGER.debug("fin ProfesionalAvaluoDAOBean#buscarPorFiltro");

        return profesionalesAvaluo;
    }

    /**
     * @see IProfesionalAvaluoDAO#obtenerPorIdentificacion(Long)
     * @author felipe.cadena
     */
    @Override
    public ProfesionalAvaluo obtenerPorIdentificacion(String identificacion) {

        LOGGER.debug("On ProfesionalAvaluoDAOBean#obtenerPorIdentificacion");

        ProfesionalAvaluo resultado = null;
        String queryString;
        Query query;

        queryString = "SELECT pa " +
            " FROM ProfesionalAvaluo pa " +
            " WHERE pa.numeroIdentificacion = :identificacion";

        LOGGER.debug(queryString);
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("identificacion", identificacion);

            resultado = (ProfesionalAvaluo) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ProfesionalAvaluoDAOBean#obtenerPorIdentificacion");

        }

        return resultado;
    }

    /**
     * @see IProfesionalAvaluoDAO#consultarPorUsuarioSistemaYNumeroIdentificacion(String, String)
     * @author christian.rodriguez
     */
    @Override
    public ProfesionalAvaluo consultarPorUsuarioSistemaYNumeroIdentificacion(
        String usuarioSistema, String numeroIdentificacion) {

        LOGGER.debug("On ProfesionalAvaluoDAOBean#consultarPorUsuarioSistemaYNumeroIdentificacion");

        ProfesionalAvaluo profesionalEncontrado = null;
        String queryString;
        Query query;

        queryString = "SELECT pa " +
            " FROM ProfesionalAvaluo pa " +
            " WHERE pa.numeroIdentificacion = :numeroIdentificacion " +
            " AND pa.usuarioSistema = :usuarioSistema ";

        LOGGER.debug(queryString);
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("usuarioSistema", usuarioSistema);
            query.setParameter("numeroIdentificacion", numeroIdentificacion);

            profesionalEncontrado = (ProfesionalAvaluo) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e,
                    "ProfesionalAvaluoDAOBean#consultarPorUsuarioSistemaYNumeroIdentificacion");

        }

        return profesionalEncontrado;
    }

    /**
     * @see IProfesionalAvaluoDAO#obtenerAvaluadoresPorInterventorId(Long, boolean)
     *
     * @author rodrigo.hernandez
     */
    @Override
    public List<ProfesionalAvaluo> obtenerAvaluadoresPorInterventorId(
        Long idInterventor, boolean banderaEsAvaluadorExterno) {

        String tipoVinculacion = "";
        List<ProfesionalAvaluo> result = new ArrayList<ProfesionalAvaluo>();

        String queryString;
        Query query;

        if (banderaEsAvaluadorExterno) {
            tipoVinculacion = EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS
                .getCodigo();
        } else {
            tipoVinculacion = EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA
                .getCodigo();
        }

        queryString = "SELECT pa" +
            " FROM" +
            " ProfesionalAvaluo pa," +
            " ProfesionalAvaluosContrato pac" +
            " WHERE" +
            " pac.profesionalAvaluo.id = pa.id" +
            " AND" +
            " pac.interventor.id = :idInterventor" +
            " AND" +
            " pa.tipoVinculacion = :tipoVinculacion";

        LOGGER.debug(queryString);
        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idInterventor", idInterventor);
            query.setParameter("tipoVinculacion", tipoVinculacion);

            result = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e,
                    "ProfesionalAvaluoDAOBean#obtenerAvaluadoresPorInterventorId");

        }

        return result;
    }
}
