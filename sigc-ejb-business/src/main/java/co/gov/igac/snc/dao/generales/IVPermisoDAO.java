package co.gov.igac.snc.dao.generales;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.VPermiso;
import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;

/**
 * Interfaz para los servicios de persistencia del objeto {@link VPermiso}.
 *
 * @author david.cifuentes
 */
@Local
public interface IVPermisoDAO extends IGenericJpaDAO<VPermiso, BigDecimal> {

    /**
     * @author fredy.wilches
     * @param roles
     * @return
     */
    public List<VPermisoGrupo> getMenu(String[] roles);
}
