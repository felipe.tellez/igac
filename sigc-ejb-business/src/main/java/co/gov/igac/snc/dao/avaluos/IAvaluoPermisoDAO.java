/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPermiso;

/**
 * Interface que contiene metodos de consulta sobre entity {@link AvaluoPermiso}
 *
 * @author christian.rodriguez
 */
@Local
public interface IAvaluoPermisoDAO extends
    IGenericJpaDAO<AvaluoPermiso, Long> {

}
