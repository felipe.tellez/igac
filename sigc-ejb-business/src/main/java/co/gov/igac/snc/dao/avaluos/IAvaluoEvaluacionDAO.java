package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacion;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoEvaluacion
 *
 * @author felipe.cadena
 */
@Local
public interface IAvaluoEvaluacionDAO extends
    IGenericJpaDAO<AvaluoEvaluacion, Long> {

    /**
     * Método para consultar los AvaluoEvaluacion relacionados con un avalúo y con una asignación
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param idAsignacion
     * @return
     */
    public AvaluoEvaluacion consultarPorAvaluoAsignacionConAtributos(
        Long idAvaluo, Long idAsignacion);

}
