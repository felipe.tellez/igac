/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ISolicitanteTramiteDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class SolicitanteTramiteDAOBean extends GenericDAOWithJPA<SolicitanteTramite, Long>
    implements ISolicitanteTramiteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitanteTramiteDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see ISolicitanteTramiteDAO#findByTramiteId(java.lang.Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<SolicitanteTramite> findByTramiteId(Long tramiteId) {

        LOGGER.debug("on SolicitanteTramiteDAOBean#findByTramiteId ...");
        List<SolicitanteTramite> answer = null;

        String queryString;
        Query query;

        queryString = "SELECT st FROM SolicitanteTramite st " +
            " JOIN FETCH st.solicitante s " +
            " LEFT JOIN FETCH s.direccionPais LEFT JOIN FETCH s.direccionDepartamento " +
            " LEFT JOIN FETCH s.direccionMunicipio" +
            " LEFT JOIN FETCH st.direccionPais " +
            " LEFT JOIN FETCH st.direccionDepartamento " +
            " LEFT JOIN FETCH st.direccionMunicipio " +
            " WHERE st.tramite.id = :tramIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramIdP", tramiteId);
            answer = query.getResultList();
            LOGGER.debug("... finished.");
        } catch (Exception e) {
            LOGGER.error("SolicitanteTramiteDAOBean#findByTramiteId error: " + e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "SolicitanteTramiteDAOBean#findByTramiteId");
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    @SuppressWarnings("unchecked")
    @Override
    public List<SolicitanteTramite> findBySolicitudId(Long solicitudId) {
        LOGGER.debug("SolicitanteSolicitudDAOBean#getSolicitantesBySolicitud");

        String queryString = "SELECT st FROM SolicitanteTramite st " +
            " LEFT JOIN FETCH st.direccionPais " +
            " LEFT JOIN FETCH st.direccionDepartamento " +
            " LEFT JOIN FETCH st.direccionMunicipio " +
            " WHERE st.tramite.solicitud.id = :idSolicitud";

        Query q = entityManager.createQuery(queryString);
        q.setParameter("idSolicitud", solicitudId);
        try {
            return (List<SolicitanteTramite>) q.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Error al consultar los solicitantes dl trámite: " + e.getMessage());
            return null;
        }
    }

//end of class
}
