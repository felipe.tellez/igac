/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.ITipificacionUnidadConstDAO;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class TipificacionUnidadConstDAOBean extends
    GenericDAOWithJPA<TipificacionUnidadConst, Long> implements ITipificacionUnidadConstDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TipificacionUnidadConstDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see ITipificacionUnidadConstDAO#findByPoints(int)
     */
    @Override
    public TipificacionUnidadConst findByPoints(int puntos) {

        TipificacionUnidadConst answer = null;
        Query query;

        try {
            query = this.entityManager.createNamedQuery("findByPoints");
            query.setParameter("puntosP", puntos);
            answer = (TipificacionUnidadConst) query.getSingleResult();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL_NAMED_QUERY.getExcepcion(
                LOGGER, ex, "TipificacionUnidadConst.findByPoints",
                "TipificacionUnidadConstDAOBean#findByPoints");
        }

        return answer;
    }

}
