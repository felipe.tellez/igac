package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;

@Local
public interface IUnidadConstruccionCompDAO extends IGenericJpaDAO<UnidadConstruccionComp, Long> {

    public List<UnidadConstruccionComp> findUnidadConstruccionCompByUnidadConstruccion(
        UnidadConstruccion unidad);
}
