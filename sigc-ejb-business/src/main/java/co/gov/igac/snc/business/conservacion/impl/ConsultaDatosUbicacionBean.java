package co.gov.igac.snc.business.conservacion.impl;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import co.gov.igac.snc.business.conservacion.IConsultaDatosUbicacionLocal;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;

@Stateless
public class ConsultaDatosUbicacionBean implements
    IConsultaDatosUbicacionLocal, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7407203761193823486L;

    private IPredioDAO predioDAO;

    @EJB
    public void setPredioDAO(IPredioDAO predioDAO) {
        this.predioDAO = predioDAO;
    }
}
