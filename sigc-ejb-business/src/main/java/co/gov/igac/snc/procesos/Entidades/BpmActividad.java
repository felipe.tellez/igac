/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_ACTIVIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmActividad.findAll", query = "SELECT b FROM BpmActividad b"),
    @NamedQuery(name = "BpmActividad.findById", query = "SELECT b FROM BpmActividad b WHERE b.id = :id"),
    @NamedQuery(name = "BpmActividad.findByNombreactividad", query = "SELECT b FROM BpmActividad b WHERE b.nombreactividad = :nombreactividad"),
    @NamedQuery(name = "BpmActividad.findByTipo", query = "SELECT b FROM BpmActividad b WHERE b.tipo = :tipo"),
    @NamedQuery(name = "BpmActividad.findByTipoprocesamiento", query = "SELECT b FROM BpmActividad b WHERE b.tipoprocesamiento = :tipoprocesamiento"),
    @NamedQuery(name = "BpmActividad.findByUrlcasodeuso", query = "SELECT b FROM BpmActividad b WHERE b.urlcasodeuso = :urlcasodeuso"),
    @NamedQuery(name = "BpmActividad.findByTrancisiones", query = "SELECT b FROM BpmActividad b WHERE b.trancisiones = :trancisiones"),
    @NamedQuery(name = "BpmActividad.findByMacroproceso", query = "SELECT b FROM BpmActividad b WHERE b.macroproceso = :macroproceso"),
    @NamedQuery(name = "BpmActividad.findByProceso", query = "SELECT b FROM BpmActividad b WHERE b.proceso = :proceso"),
    @NamedQuery(name = "BpmActividad.findByMacroprocesoproceso", query = "SELECT b FROM BpmActividad b WHERE b.macroprocesoproceso = :macroprocesoproceso"),
    @NamedQuery(name = "BpmActividad.findBySubprocesoactividad", query = "SELECT b FROM BpmActividad b WHERE b.subprocesoactividad = :subprocesoactividad"),
    @NamedQuery(name = "BpmActividad.findByActividad", query = "SELECT b FROM BpmActividad b WHERE b.actividad = :actividad"),
    @NamedQuery(name = "BpmActividad.findByDescription", query = "SELECT b FROM BpmActividad b WHERE b.description = :description"),
    @NamedQuery(name = "BpmActividad.findByDuracion", query = "SELECT b FROM BpmActividad b WHERE b.duracion = :duracion"),
    @NamedQuery(name = "BpmActividad.findByEstado", query = "SELECT b FROM BpmActividad b WHERE b.estado = :estado"),
    @NamedQuery(name = "BpmActividad.findByTipoAct", query = "SELECT b FROM BpmActividad b WHERE b.tipoAct = :tipoAct"),
    @NamedQuery(name = "BpmActividad.findByVarName", query = "SELECT b FROM BpmActividad b WHERE b.varName = :varName")})

public class BpmActividad implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "NOMBREACTIVIDAD")
    private String nombreactividad;
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "TIPOPROCESAMIENTO")
    private String tipoprocesamiento;
    @Column(name = "URLCASODEUSO")
    private String urlcasodeuso;
    @Column(name = "TRANCISIONES")
    private String trancisiones;
    @Column(name = "MACROPROCESO")
    private String macroproceso;
    @Column(name = "PROCESO")
    private String proceso;
    @Column(name = "MACROPROCESOPROCESO")
    private String macroprocesoproceso;
    @Column(name = "SUBPROCESOACTIVIDAD")
    private String subprocesoactividad;
    @Column(name = "ACTIVIDAD")
    private String actividad;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "DURACION")
    private String duracion;
    @Column(name = "ESTADO")
    private Short estado;
    @Column(name = "TIPO_ACT")
    private String tipoAct;
    @Column(name = "VAR_NAME")
    private String varName;

    public BpmActividad() {
    }

    public BpmActividad(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombreactividad() {
        return nombreactividad;
    }

    public void setNombreactividad(String nombreactividad) {
        this.nombreactividad = nombreactividad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoprocesamiento() {
        return tipoprocesamiento;
    }

    public void setTipoprocesamiento(String tipoprocesamiento) {
        this.tipoprocesamiento = tipoprocesamiento;
    }

    public String getUrlcasodeuso() {
        return urlcasodeuso;
    }

    public void setUrlcasodeuso(String urlcasodeuso) {
        this.urlcasodeuso = urlcasodeuso;
    }

    public String getTrancisiones() {
        return trancisiones;
    }

    public void setTrancisiones(String trancisiones) {
        this.trancisiones = trancisiones;
    }

    public String getMacroproceso() {
        return macroproceso;
    }

    public void setMacroproceso(String macroproceso) {
        this.macroproceso = macroproceso;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public String getMacroprocesoproceso() {
        return macroprocesoproceso;
    }

    public void setMacroprocesoproceso(String macroprocesoproceso) {
        this.macroprocesoproceso = macroprocesoproceso;
    }

    public String getSubprocesoactividad() {
        return subprocesoactividad;
    }

    public void setSubprocesoactividad(String subprocesoactividad) {
        this.subprocesoactividad = subprocesoactividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public Short getEstado() {
        return estado;
    }

    public void setEstado(Short estado) {
        this.estado = estado;
    }

    public String getTipoAct() {
        return tipoAct;
    }

    public void setTipoAct(String tipoAct) {
        this.tipoAct = tipoAct;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmActividad)) {
            return false;
        }
        BpmActividad other = (BpmActividad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmActividad[ id=" + id + " ]";
    }
    
}
