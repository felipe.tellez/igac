package co.gov.igac.snc.dao.generales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.IZonaDAO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class ZonaDAOBean extends GenericDAOWithJPA<Zona, String> implements IZonaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZonaDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<Zona> findByMunicipio(String municipioCodigo) {

        List<Zona> answer = null;
        Query query = this.entityManager
            .createQuery("SELECT DISTINCT m FROM Zona m WHERE m.municipio.codigo = :municipioCodigo");
        query.setParameter("municipioCodigo", municipioCodigo);

        try {
            answer = query.getResultList();
        } catch (IndexOutOfBoundsException iobe) {
            return answer = new ArrayList<Zona>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(), "ZonaDAOBean#findByMunicipio");
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IMunicipioDAO#getZonaByCodigo(String)
     */
    public Zona getZonaByCodigo(String codigo) {
        Query q = entityManager.createNamedQuery("zona.findZonaByCodigo");
        q.setParameter("codigo", codigo);
        Zona m = (Zona) q.getSingleResult();
        return m;
    }
}
