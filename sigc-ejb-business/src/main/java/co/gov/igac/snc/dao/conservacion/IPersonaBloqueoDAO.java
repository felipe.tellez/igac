/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface IPersonaBloqueoDAO extends IGenericJpaDAO<PersonaBloqueo, Long> {

    /**
     * Retorna la información asociada al posible bloqueo actual de una persona
     *
     * Regla: (la misma que se definió en las funciones de la bd) bloqueada si fecha actual GE fecha
     * inicio bloqueo y ((fecha actual LE fecha desbloqueo o (fecha desbloqueo is null y fecha
     * actual LE fecha termina bloqueo) o (fecha desbloqueo is null o fecha termina bloqueo is
     * null))
     *
     * @param idPersona id de la persona
     * @return
     */
    public PersonaBloqueo findCurrentPersonaBloqueoByPersonaId(Long idPersona);

    /**
     * Busca una persona bloqueada por número de cedula
     *
     * @author juan.agudelo
     * @param numeroCedula
     * @return
     */
    public List<PersonaBloqueo> buscarPersonaBloqueoByNumeroIdentificacion(
        String numeroIdentificacion, String tipoIdentificacion);

    /**
     * Retorna la lista de bloqueos vigentes para una persona dada
     *
     * @juan.agudelo
     * @version 2.0
     * @param idPersona
     * @return
     */
    public List<PersonaBloqueo> getPersonaBloqueosByPersonaId(Long idPersona);

    /**
     * Obtiene la lista de personas bloqueadas de acuerdo a los parametros de la busqueda
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPersonasBloqueo
     * @return
     */
    public List<PersonaBloqueo> buscarPersonasBloqueo(
        FiltroDatosConsultaPersonasBloqueo filtroDatosConsultaPersonasBloqueo);

    /**
     * Actualiza una lista de personas bloqueadas solo si el usuario es el jefe de conservación
     *
     * @author juan.agudelo
     * @param usuario
     * @param personasBloqueados
     * @return
     */
    public List<PersonaBloqueo> actualizarPersonasBloqueo(UsuarioDTO usuario,
        List<PersonaBloqueo> personasBloqueadas);

    /**
     * Método que verifica si una entidad se encuentra o no asociada al bloqueo de una @{link
     * Persona}.
     *
     * @param entidad
     * @return
     */
    public boolean existeAsociacionEntidadBloqueo(Entidad entidad);

}
