package co.gov.igac.snc.business.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;

/**
 *
 *
 * @author juan.agudelo
 *
 */
@Local
//TODO::juan.agudelo::10-08-2011::una clase no debería tener nombre de verbo en infinitivo::pedro.garcia
public interface IBuscarDocumentosTramitePorNumeroDeRadicacion {

//TODO::juan.agudelo::10-08-2011::documentar!!!::pedro.garcia
    public List<TramiteDocumentacion> buscarDocumentosTramitePorNumeroDeRadicacion(
        String numeroRadicacion);

}
