package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOfertaServicioPublicoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioPublico;

@Stateless
public class OfertaServicioPublicoDAOBean extends GenericDAOWithJPA<OfertaServicioPublico, Long>
    implements IOfertaServicioPublicoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OfertaServicioPublicoDAOBean.class);

}
