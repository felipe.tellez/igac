package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.GttOfertaInmobiliaria;

@Local
public interface IGttOfertaInmobiliariaDAO extends IGenericJpaDAO<GttOfertaInmobiliaria, Long> {

    public Long generarSecuenciaGttOfertaInmobiliaria();
}
