/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IUsoConstruccionDAO;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class UsoConstruccionDAOBean extends GenericDAOWithJPA<UsoConstruccion, Long>
    implements IUsoConstruccionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioDAOBean.class);

    //-------------------------------------------------------------------------------------------
    @Override
    public List<UsoConstruccion> buscarUsosConstruccionPorNumerosPredialesPredio(
        List<String> numerosPrediales) {
        List<UsoConstruccion> answer = new ArrayList<UsoConstruccion>();
        StringBuilder queryString = new StringBuilder();
        Query query;
        queryString.append("SELECT DISTINCT usocons" +
            " FROM UnidadConstruccion uc JOIN uc.predio p JOIN uc.usoConstruccion usocons" +
            " WHERE ");

        for (int i = 0; i < numerosPrediales.size(); i++) {
            queryString.append(" p.numeroPredial = :numeroPredial").append(i);
            queryString.append(" OR ");
        }
        if (numerosPrediales.size() > 0) {
            queryString = new StringBuilder(queryString.substring(0, queryString.length() - 4));
        }
        query = this.entityManager.createQuery(queryString.toString());

        for (int i = 0; i < numerosPrediales.size(); i++) {
            query.setParameter("numeroPredial" + i, numerosPrediales.get(i));
        }
        try {
            answer = (ArrayList<UsoConstruccion>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return answer;
    }

}
