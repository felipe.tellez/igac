package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioPropietarioHisDAO;
import co.gov.igac.snc.dao.generales.impl.DominioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioPropietarioHis;

@Stateless
public class PredioPropietarioHisDAOBean extends GenericDAOWithJPA<PredioPropietarioHis, Long>
    implements
    IPredioPropietarioHisDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DominioDAOBean.class);

    /**
     * @see IPredioPropietarioHisDAO#getPredioPropietarioHisByPredio(Predio)
     */
    @SuppressWarnings("unchecked")
    public List<PredioPropietarioHis> getPredioPropietarioHisByPredio(Long predioId) {
        LOGGER.debug("getPredioPropietarioHisByPredio");
        Query q = entityManager.createNamedQuery("findPredioPropietarioHisByPredio");
        q.setParameter("predioId", predioId);
        return q.getResultList();
    }

}
