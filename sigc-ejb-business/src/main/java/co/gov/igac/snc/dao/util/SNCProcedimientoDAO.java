package co.gov.igac.snc.dao.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.IEstructuraOrganizacionalDAO;
import co.gov.igac.snc.dao.tramite.ISolicitanteDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.Usuario;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ETipoEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaRadicacionesCorrespondencia;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import org.apache.commons.lang.StringUtils;

/**
 * Clase que agrupa los métodos que hacen llamados a procedimientos almacenados de la base de datos
 *
 * @author pedro.garcia
 * @author juan.mendez
 *
 */
@Stateless
public class SNCProcedimientoDAO implements ISNCProcedimientoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SNCProcedimientoDAO.class);

    @Resource
    private SessionContext context;

    @EJB
    private ISolicitanteDAO solicitanteDao;

    @EJB
    private IDocumentoDAO documentoService;

    @EJB
    private ITramiteDAO tramiteService;
    
    @EJB
    private IEstructuraOrganizacionalDAO estructuraOrganizacionalDao;

    private EntityManager entityManager;


    /*
     * (non-Javadoc)
     *
     * @see co.gov.igac.snc.dao.util.ISNCProcedimientoDAO#setEntityManager(javax.
     * persistence.EntityManager)
     */
    @Override
    @PersistenceContext(unitName = "SigcEntityManager")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

//--------------------------------------------------------------------------------------------------
    /*
     * (non-Javadoc)
     *
     * @see
     * co.gov.igac.snc.dao.util.ISNCProcedimientoDAO#obtenerRadicacionCorrespondencia(java.lang.String)
     * @modified felipe.cadena se agrega el parametro usuario
     */
    @Override
    public Solicitud obtenerRadicacionCorrespondencia(
        String numeroRadicacionCorrespondencia,
        UsuarioDTO usuario) {
        Solicitud sol = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(numeroRadicacionCorrespondencia);
            parametros.add(usuario.getLogin());

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            sol = parseSolicitud(procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICADO,
                parametros));
            if (sol != null) {
                if (sol.getSolicitanteSolicituds() != null &&
                    !sol.getSolicitanteSolicituds().isEmpty()) {
                    SolicitanteSolicitud ss = sol.getSolicitanteSolicituds()
                        .get(0);
                    Solicitante solicitante = this.solicitanteDao
                        .findSolicitanteByTipoIdentAndNumDocFetchRelacions(
                            ss.getTipoIdentificacion(),
                            ss.getNumeroIdentificacion());
                    if (solicitante != null) {
                        sol.getSolicitanteSolicituds().get(0)
                            .setSolicitante(solicitante);
                    }
                }
            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return sol;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     *
     * @param radicadoCorrespondenciaSP
     * @return
     */
    @SuppressWarnings("unchecked")
    private Solicitud parseSolicitud(Object[] radicadoCorrespondenciaSP) {
        Solicitud solicitud = null;

        boolean hasErrors = (((ArrayList<Object>) radicadoCorrespondenciaSP[1])
            .size() > 0);
        boolean hasResults = (((ArrayList<Object>) radicadoCorrespondenciaSP[0])
            .size() > 0);
        // MANEJO DE ERRORES
        if (hasErrors) {
            Object[] erroresRadicado =
                ((Object[]) ((ArrayList<Object>) radicadoCorrespondenciaSP[1])
                    .get(0));
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, null, erroresRadicado.toString());
        } else {
            // MANEJO DE RESULTADOS
            if (hasResults) {
                Object[] radicadoCorrespondencia =
                    ((Object[]) ((ArrayList<Object>) radicadoCorrespondenciaSP[0])
                        .get(0));
                solicitud = new Solicitud();
                Solicitante solicitante = new Solicitante();
                solicitud
                    .setTerritorialCodigo((String) radicadoCorrespondencia[0]);
                solicitud.setFolios(Integer.parseInt(radicadoCorrespondencia[4]
                    .toString()));
                solicitud.setAnexos(Integer.parseInt(radicadoCorrespondencia[5]
                    .toString()));
                solicitud.setAsunto((String) radicadoCorrespondencia[6]);
                solicitud.setObservaciones((String) radicadoCorrespondencia[6]);
                solicitud.setSistemaEnvio((String) radicadoCorrespondencia[12]);
                solicitud.setNumero((String) radicadoCorrespondencia[30]);
                solicitud.setFecha((Date) radicadoCorrespondencia[31]);

                String auxNombre = (String) radicadoCorrespondencia[27];
                if (auxNombre != null) {
                    String[] auxNombreSplit = auxNombre.trim().split(" ");
                    switch (auxNombreSplit.length) {
                        case 1: {
                            solicitante.setPrimerNombre(auxNombreSplit[0]);
                            break;
                        }
                        case 2: {
                            solicitante.setPrimerNombre(auxNombreSplit[0]);
                            solicitante.setPrimerApellido(auxNombreSplit[1]);
                            break;
                        }
                        case 3: {
                            solicitante.setPrimerNombre(auxNombreSplit[0]);
                            solicitante.setPrimerApellido(auxNombreSplit[1]);
                            solicitante.setSegundoApellido(auxNombreSplit[2]);
                            break;
                        }
                        case 4: {
                            solicitante.setPrimerNombre(auxNombreSplit[0]);
                            solicitante.setSegundoNombre(auxNombreSplit[1]);
                            solicitante.setPrimerApellido(auxNombreSplit[2]);
                            solicitante.setSegundoApellido(auxNombreSplit[3]);
                            break;
                        }

                        default: {
                            solicitante.setPrimerNombre(auxNombre);
                            break;
                        }
                    }
                }
                solicitante
                    .setTipoIdentificacion((String) radicadoCorrespondencia[25]);
                solicitante
                    .setNumeroIdentificacion((String) radicadoCorrespondencia[26]);
                solicitante
                    .setTelefonoPrincipal((String) radicadoCorrespondencia[28]);
                solicitante
                    .setCorreoElectronico((String) radicadoCorrespondencia[29]);

                // TODO: fabio.navarrete reemplazar este usuario por el que se
                // obtiene de la sesión
                solicitante.setFechaLog(new Date());
                solicitante.setUsuarioLog("fabio.navarrete.quemado");
                SolicitanteSolicitud auxSolSol = new SolicitanteSolicitud();
                auxSolSol.setSolicitante(solicitante);
                solicitud.getSolicitanteSolicituds().add(auxSolSol);
            }

        }
        return solicitud;
    }

    // --------------------------------------------------------------------------------------------------
    /*
     * (non-Javadoc)
     *
     * @see co.gov.igac.snc.dao.util.ISNCProcedimientoDAO#generarNumeroPredio(java .lang.String)
     */
    @Override
    public String generarNumeroPredio(String numeroPredialManzana) {
        String numeroRadicadoResultado = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(numeroPredialManzana); // Código manzana
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            Object[] resultado = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_PREDIO,
                parametros);
            numeroRadicadoResultado = resultado[0].toString();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return numeroRadicadoResultado;
    }

    /**
     *
     * @author felipe.cadena
     * @see co.gov.igac.snc.dao.util.ISNCProcedimientoDAO#generarNumeroManzana(java .lang.String)
	 * */
    @Override
    public String generarNumeroManzana(String numeroPredialBarrio) {
        String numeroRadicadoResultado = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(numeroPredialBarrio);
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            Object[] resultado = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MANZANA,
                parametros);
            numeroRadicadoResultado = resultado[0].toString();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return numeroRadicadoResultado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @see ISNCProcedimientoDAO#radicarEnCorrespondencia(java.util.List)
     */
    @Implement
    public Object[] radicarEnCorrespondencia(List<Object> params) {

        Object[] answer = null;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String codigoOrg=params.get(0).toString();
        procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
        if (esCodigoHabilitada(codigoOrg)) {
            answer = procedimientoDAO.ejecutarSP(EProcedimientoAlmacenadoFuncion.RADICAR_AMCO, params);
        } else {
            answer = procedimientoDAO.ejecutarSP(
                    EProcedimientoAlmacenadoFuncion.RADICAR, params);
        }
        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /*
     * (non-Javadoc)
     *
     * @modified felipe.cadena -- se agrega el parametro @see
     * co.gov.igac.snc.dao.util.ISNCProcedimientoDAO#generarProyeccion(java. lang.Long)
     */
    @Override
    public Object[] generarProyeccion(Long tramiteId) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(tramiteId));
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
            entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.GENERAR_PROYECCION,
            parametros);
        int numeroErrores = 0;
        if (mensajes != null) {

            for (Object o : mensajes) {
                List l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!((Object[]) obje)[0].equals("0")) {
                            numeroErrores++;
                        }
                    }
                    if (numeroErrores > 0) {
                        Tramite t = this.tramiteService.findById(tramiteId);
                        if (t!=null && t.getRadicacionEspecial()!=null && !t.getRadicacionEspecial().equals(
                            ETramiteRadicacionEspecial.CANCELACION_MASIVA.getCodigo())) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        }
        return mensajes;

    }
    // --------------------------------------------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see co.gov.igac.snc.dao.util.ISNCProcedimientoDAO#generarProyeccionCancelacionBasica(java.
     * lang.Long, String)
     */
    @Override
    public Object[] generarProyeccionCancelacionBasica(Long tramiteId, String usuario) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(tramiteId));
        parametros.add(usuario);
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
            entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.GENERAR_PROYECCION_PREDIO_BASI,
            parametros);
        int numeroErrores = 0;
        if (mensajes != null) {

            for (Object o : mensajes) {
                List l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!((Object[]) obje)[0].equals("0")) {
                            numeroErrores++;
                        }
                    }
                    if (numeroErrores > 0) {
                        LOGGER.debug("Debe hacer rollback");
                        context.setRollbackOnly();
                    }
                }
            }
        }
        return mensajes;

    }

    // --------------------------------------------------------------------------------------------------
    @Override
    public Documento consultarRadicado(String numeroRadicacion, UsuarioDTO usuario) {
        Documento documento = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(numeroRadicacion);
            parametros.add(usuario.getLogin());
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            Object[] resultado = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICADO,
                parametros);
            documento = documentoService.parseDocumento(resultado);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return documento;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @see ISNCProcedimientoDAO#generarNumeracion(java.util.List)
     */
    @Implement
    @Override
    public Object[] generarNumeracion(List<Object> params) {

        Object[] answer = null;
        ProcedimientoAlmacenadoDAO procedimientoDAO;

        procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

        try {
            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERACION, params);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.GENERAR_NUMERACION.name());
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @see ISNCProcedimientoDAO#consultarRadicadoCorrespondencia(java.util.List)
     */
    @Implement
    @Override
    public Object[] consultarRadicadoCorrespondencia(List<Object> params) {

        Object[] answer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;

        procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
        answer = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICADO, params);

        return answer;
    }

    /**
     * @see ISNCProcedimientoDAO#eliminarProyeccion(Long)
     * @author fabio.navarrete
     */
    @Override
    public void eliminarProyeccion(Long tramiteId) {
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(tramiteId));
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            // TODO:fabio.navarrete::Revisar la enumeración llamada pues aún no
            // está usando el procedimiento adecuado.
            procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#calcularAfectadosDecreto(Long)
     * @author david.cifuentes
     */
    @Override
    public Object[] calcularAfectadosDecreto(Long decretoId) {
        Object[] answer;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(decretoId));
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CALCULAR_AFECTADOS_DECRETO,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#validarCondicionalDecretoCondicion(Long)
     * @author javier.aponte
     */
    @Override
    public Object[] validarCondicionalDecretoCondicion(Long decretoCondicionId) {
        Object[] answer;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(decretoCondicionId));
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.VALIDAR_CONDICION_DECRETO,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#reproyectarAvaluos(Long, Long, Date, Usuario)
     * @author david.cifuentes
     */
    @Override
    public Object[] reproyectarAvaluos(Long tramiteId, Long predioId, Date date, UsuarioDTO usuario) {
        Object[] errors = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(tramiteId));
            parametros.add(new BigDecimal(predioId));
            java.sql.Timestamp fechaDelCalculoTimestamp = new Timestamp(
                date.getTime());
            parametros.add(fechaDelCalculoTimestamp);
            parametros.add(usuario.getLogin());
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REPROYECTAR_AVALUOS,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @see ISNCProcedimientoDAO#buscarPrediosPorCriteriosRangoNumeroPredial(FiltroGenerarReportes,
     * boolean, int...)
     * @author javier.barajas
     *
     * definición del procedimiento (09-04-2012) parametros (ver definición completa en el método
     * armarParametrosBusquedaPredios) ... pminimo IN NUMBER, pmaximo IN NUMBER, pcontador OUT
     * NUMBER, presultados OUT sys_refcursor, perrores OUT sys_refcursor ;
     */
    @Implement
    @Override
    public final List<Object[]> buscarPrediosPorCriteriosRangoNumeroPredial(
        FiltroGenerarReportes datosConsultaPredio, boolean isCount, int datoADesplegar,
        int... rowStartIdxAndCount) {

        List<Object[]> answer = null;
        List<Object> parameters;
        Object[] procedureAnswer = null;
        int rowStartIdx = -1, rowCount = -1;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        ArrayList<Object> erroresSP;
        String cadenaErroresSP;

        // D: si es para contar estos dos parámetros se mandan en 0
        if (isCount) {
            rowStartIdx = 0;
            rowCount = 0;
        } else {
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdxAndCount.length > 1) {
                    rowCount = Math.max(0, rowStartIdxAndCount[1]);
                }
            } // D: si no es para contar, y no vienen los parámetros que delimitan
            // el número de filas,
            // se deben devolver todos los registros.
            else {
                rowStartIdx = -1;
                rowCount = -1;
            }
        }

        // D: se llenan los parámetros anteriores a pminimo
        parameters = this.armarParametrosBusquedaPrediosRangoNumeroPredial(datosConsultaPredio);

        // D: si se van a devolver todos los registros se debe enviar en null
        // los parámetros pminimo y pmaximo
        if (rowStartIdx < 0) {
            parameters.add(null);
            parameters.add(null);
        } else {
            // D: ojo: para prime son first y pageSize, pero para el sp son mínimo y máximo
            parameters.add(new BigDecimal(rowStartIdx));
            parameters.add(new BigDecimal(rowStartIdx + rowCount));
        }

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            // D: el procedimiento tiene 3 variables de retorno: un número y dos cursores
            switch (datoADesplegar) {
                case 0:
                    procedureAnswer = procedimientoDAO.ejecutarSP(
                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DBP, parameters);
                    LOGGER.debug("Procedimiento CONSULTAR_PREDIOS_REPORTE_DBP");
                    break;
                case 1: procedureAnswer = procedimientoDAO.ejecutarSP(
                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DB, parameters);
                    LOGGER.debug("Procedimiento CONSULTAR_PREDIOS_REPORTE_DB");
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4: procedureAnswer = procedimientoDAO.ejecutarSP(
                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DJ, parameters);
                    LOGGER.debug("Procedimiento CONSULTAR_PREDIOS_REPORTE_DJ");
                    break;
                case 5:
                    break;
                case 6: procedureAnswer = procedimientoDAO.ejecutarSP(
                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DECO, parameters);
                    LOGGER.debug("Procedimiento CONSULTAR_PREDIOS_REPORTE_DECO");
                    break;
                case 7: procedureAnswer = procedimientoDAO.ejecutarSP(
                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DCCO, parameters);
                    LOGGER.debug("Procedimiento CONSULTAR_PREDIOS_REPORTE_DCCO");
                    break;
                case 8: procedureAnswer = procedimientoDAO.ejecutarSP(
                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DCNC, parameters);
                    LOGGER.debug("Procedimiento CONSULTAR_PREDIOS_REPORTE_DCNC");
                    break;
                case 9:
                    break;
            }
            /* procedureAnswer = procedimientoDAO.ejecutarSP(
             * EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DB, parameters); */

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                // D: se revisa si el procedimiento retornó errores.
                // A la fecha 13-04-2012 se estaba retornando un arreglo de objetos (string) con 4 posiciones
                erroresSP = (procedureAnswer[2] != null) ? (ArrayList<Object>) procedureAnswer[2] :
                    null;
                cadenaErroresSP = this.getErrorsCursorAsString((ArrayList) erroresSP);
                if (cadenaErroresSP != null) {
                    Exception ex = new Exception(cadenaErroresSP);
                    ExcepcionSNC excepcionProcedimiento = new ExcepcionSNC("", "", "");

                    switch (datoADesplegar) {
                        case 0:
                            excepcionProcedimiento =
                                SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                                    getExcepcion(LOGGER, ex,
                                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DBP.
                                            name());
                            break;
                        case 1:
                            excepcionProcedimiento =
                                SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                                    getExcepcion(LOGGER, ex,
                                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DB.
                                            name());
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4: excepcionProcedimiento =
                                SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                                    getExcepcion(LOGGER, ex,
                                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DJ.
                                            name());
                            break;
                        case 5:
                            break;
                        case 6: excepcionProcedimiento =
                                SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                                    getExcepcion(LOGGER, ex,
                                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DECO.
                                            name());
                            break;
                        case 7: excepcionProcedimiento =
                                SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                                    getExcepcion(LOGGER, ex,
                                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DCCO.
                                            name());
                            break;
                        case 8: excepcionProcedimiento =
                                SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                                    getExcepcion(LOGGER, ex,
                                        EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DCNC.
                                            name());
                            break;
                        case 9:
                            break;
                    }
                    throw excepcionProcedimiento;
                } else {
                    answer = new ArrayList<Object[]>();
                    if (isCount) {
                        Object[] count = new Object[1];
                        count[0] = procedureAnswer[0];
                        answer.add(count);
                    } else {
                        answer = (List<Object[]>) procedureAnswer[1];

                    }
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            ExcepcionSNC excepcionProcedimiento = new ExcepcionSNC("", "", "");
            switch (datoADesplegar) {
                case 0:
                    excepcionProcedimiento =
                        SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                            .getExcepcion(LOGGER, ex,
                                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DBP.name());
                    break;
                case 1:
                    excepcionProcedimiento =
                        SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                            .getExcepcion(LOGGER, ex,
                                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DB.name());
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4: excepcionProcedimiento =
                        SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                            .getExcepcion(LOGGER, ex,
                                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DJ.name());
                    break;
                case 5:
                    break;
                case 6: excepcionProcedimiento =
                        SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                            .getExcepcion(LOGGER, ex,
                                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DECO.
                                    name());
                    break;
                case 7: excepcionProcedimiento =
                        SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                            .getExcepcion(LOGGER, ex,
                                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DCCO.
                                    name());
                    break;
                case 8: excepcionProcedimiento =
                        SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                            .getExcepcion(LOGGER, ex,
                                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_REPORTE_DCNC.
                                    name());
                    break;
                case 9:
                    break;
            }
            throw excepcionProcedimiento;
        }
        LOGGER.debug("resultados son:" + answer.size());
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Se usa para armar la lista de parámetros que se pasan al procedimiento de búsqueda de
     * predios.
     *
     * pdepartamento_codigo IN VARCHAR2, pmunicipio_codigo IN VARCHAR2, pnumero_predial_desde IN
     * VARCHAR2, pnumero_predial_hasta IN VARCHAR2, pcirculo_registral IN VARCHAR2, pnumero_registro
     * IN VARCHAR2, pnumero_registro_anterior IN VARCHAR2, ptipo_identificacion IN VARCHAR2,
     * pnumero_identificacion IN VARCHAR2, pnombre IN VARCHAR2, pdireccion IN VARCHAR2, pdestinos IN
     * VARCHAR2, pusos IN VARCHAR2, pzonas_fisicas IN VARCHAR2, pzonas_geoeconomicas IN VARCHAR2,
     * ptipos_predio IN VARCHAR2, ptipificaciones IN VARCHAR2, pmodos_adquisicon IN VARCHAR2,
     * parea_terreno_desde IN NUMBER, parea_terreno_hasta IN NUMBER, parea_construida_desde IN
     * NUMBER, parea_construida_hasta IN NUMBER, pavaluo_desde IN NUMBER, pavaluo_hasta IN NUMBER,
     * pminimo IN NUMBER, pmaximo IN NUMBER, pcontador OUT NUMBER, presultados OUT sys_refcursor,
     * perrores OUT sys_refcursor );
     */
    private List<Object> armarParametrosBusquedaPrediosRangoNumeroPredial(
        FiltroGenerarReportes datosConsultaPredio) {

        List<Object> parameters = null;

        /**
         * esta se usa para armar los parámetros que deben ir con la forma 'x', 'y', ... 'n' y que
         * llegan del filtro como un arrglo de strings (resultado de un selectManyMenu)
         */
        String tempStringParameters;

        // D: armar la lista de parámetros. Hay que adicionarlos en el orden que
        // el procedimiento almacenado los está esperando (ver definición de éste).
        // Si el criterio de búsqueda viene en null se debe adicionar un null como parámetro para
        // el procedimiento
        parameters = new ArrayList<Object>();

        if (datosConsultaPredio.getDepartamentoId() != null &&
            !datosConsultaPredio.getDepartamentoId().isEmpty()) {
            parameters.add(datosConsultaPredio.getDepartamentoId());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getMunicipioId() != null &&
            !datosConsultaPredio.getMunicipioId().isEmpty()) {
            parameters.add(datosConsultaPredio.getMunicipioId());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        //datosConsultaPredio.assembleNumeroPredialFromSegments();
        if (datosConsultaPredio.getNumeroPredial() != null &&
            !datosConsultaPredio.getNumeroPredial().isEmpty()) {
            parameters.add(datosConsultaPredio.getNumeroPredial());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getNumeroPredialFinal() != null &&
            !datosConsultaPredio.getNumeroPredialFinal().isEmpty()) {
            parameters.add(datosConsultaPredio.getNumeroPredialFinal());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getCirculoRegistral() != null &&
            !datosConsultaPredio.getCirculoRegistral().isEmpty()) {
            parameters.add(datosConsultaPredio.getCirculoRegistral());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getNumeroRegistro() != null &&
            !datosConsultaPredio.getNumeroRegistro().isEmpty()) {
            parameters.add(datosConsultaPredio.getNumeroRegistro());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        //Parametro numero registral anterior no se tiene en cuenta
        parameters.add(null);
        //parameters.add("");

        if (datosConsultaPredio.getTipoIdentificacionPropietario() != null &&
            !datosConsultaPredio.getTipoIdentificacionPropietario().isEmpty()) {
            parameters.add(datosConsultaPredio.getTipoIdentificacionPropietario());

        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getIdentificacionPropietario() != null &&
            !datosConsultaPredio.getIdentificacionPropietario()
                .isEmpty()) {
            parameters.add(datosConsultaPredio.getIdentificacionPropietario());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getNombrePropietario() != null &&
            !datosConsultaPredio.getNombrePropietario().isEmpty()) {
            parameters.add(datosConsultaPredio.getNombrePropietario());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getDireccionPredio() != null &&
            !datosConsultaPredio.getDireccionPredio().isEmpty()) {
            parameters.add(datosConsultaPredio.getDireccionPredio());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getDestinoPredioSelect() != null &&
            !datosConsultaPredio.getDestinoPredioSelect().isEmpty()) {
            parameters.add(datosConsultaPredio.todosDestinos());
        } else {
            parameters.add(null);
            //parameters.add("");
        }
        if (datosConsultaPredio.getUsoConstruccionSelect() != null &&
            !datosConsultaPredio.getUsoConstruccionSelect().isEmpty()) {
            parameters.add(datosConsultaPredio.todosUsoConstruccion());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        //Parametro de zonas homogeneas Fisicas por el momento nulo
        parameters.add(null);
        //parameters.add("");
        //Parametro de zonas homogeneas Geoconomicas por el momento nulo
        parameters.add(null);
        //parameters.add("");

        if (datosConsultaPredio.getTipoPredioSelect() != null &&
            !datosConsultaPredio.getTipoPredioSelect().isEmpty()) {
            parameters.add(datosConsultaPredio.todosTipos());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getTipificacionConstruccionSelect() != null &&
            !datosConsultaPredio.getTipificacionConstruccionSelect().isEmpty()) {
            parameters.add(datosConsultaPredio.todosTipificacion());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        if (datosConsultaPredio.getModoAdquisicionSelect() != null &&
            !datosConsultaPredio.getModoAdquisicionSelect()
                .isEmpty()) {
            parameters.add(datosConsultaPredio.todosModoAdquision());
        } else {
            parameters.add(null);
            //parameters.add("");
        }

        // D: para los rangos:
        // si viene un valo r 'hasta' o máximo, se debe tener en cuenta el mínimo que puede ser
        // efectivamente 0, no solo porque así quede al crear el objeto FiltroDatosConsultaPredio
        // N: al crear un objeto de esos, los atributos que son números (tipo simple) quedan en 0.
        // N: la comparación de 0 con 0.0 da que son iguales
        // OJO: para el procedimiento, los números deben ir como BigDecimal
        /*
         * if (datosConsultaPredio.getAreaTerrenoMax() != 0) { parameters.add(new
         * BigDecimal(datosConsultaPredio.getAreaTerrenoMin())); parameters.add(new
         * BigDecimal(datosConsultaPredio.getAreaTerrenoMax())); } else { if
         * (datosConsultaPredio.getAreaTerrenoMin() == 0) { parameters.add(null);
         * //parameters.add(""); } else { parameters.add(new
         * BigDecimal(datosConsultaPredio.getAreaTerrenoMin())); } parameters.add(null);
         * //parameters.add(""); }
         *
         *
         *
         * if (datosConsultaPredio.getAreaConstruidaMax() != 0) { parameters.add(new
         * BigDecimal(datosConsultaPredio.getAreaConstruidaMin())); parameters.add(new
         * BigDecimal(datosConsultaPredio.getAreaConstruidaMax())); } else { if
         * (datosConsultaPredio.getAreaConstruidaMin() == 0) { parameters.add(null);
         * //parameters.add(""); } else { parameters.add(new
         * BigDecimal(datosConsultaPredio.getAreaConstruidaMin())); } parameters.add(null);
         * //parameters.add(""); }
         *
         * if (datosConsultaPredio.getAvaluoHasta() != 0) { parameters.add(new
         * BigDecimal(datosConsultaPredio.getAvaluoDesde())); parameters.add(new
         * BigDecimal(datosConsultaPredio.getAvaluoHasta())); } else { if
         * (datosConsultaPredio.getAvaluoDesde() == 0) { parameters.add(null); //parameters.add(0);
         * } else { parameters.add(new BigDecimal(datosConsultaPredio.getAvaluoDesde())); }
         * parameters.add(null); //parameters.add(""); }
         */
        return parameters;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#buscarPrediosPorCriterios(FiltroDatosConsultaPredio, boolean,
     * int[])
     * @author pedro.garcia
     *
     * definición del procedimiento (09-04-2012) parametros (ver definición completa en el método
     * armarParametrosBusquedaPredios) ... pminimo IN NUMBER, pmaximo IN NUMBER, pcontador OUT
     * NUMBER, presultados OUT sys_refcursor, perrores OUT sys_refcursor ;
     */
    @Implement
    @Override
    public final List<Object[]> buscarPrediosPorCriterios(
        FiltroDatosConsultaPredio datosConsultaPredio, boolean isCount, int... rowStartIdxAndCount) {

        List<Object[]> answer = null;
        List<Object> parameters;
        Object[] procedureAnswer;
        int rowStartIdx = -1, rowCount = -1;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        ArrayList<Object> erroresSP;
        String cadenaErroresSP;

        // D: si es para contar estos dos parámetros se mandan en 0
        if (isCount) {
            rowStartIdx = 0;
            rowCount = 0;
        } else {
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdxAndCount.length > 1) {
                    rowCount = Math.max(0, rowStartIdxAndCount[1]);
                }
            } // D: si no es para contar, y no vienen los parámetros que delimitan
            // el número de filas,
            // se deben devolver todos los registros.
            else {
                rowStartIdx = -1;
                rowCount = -1;
            }
        }

        // D: se llenan los parámetros anteriores a pminimo
        parameters = this.armarParametrosBusquedaPredios(datosConsultaPredio);

        // D: si se van a devolver todos los registros se debe enviar en null
        // los parámetros pminimo y pmaximo
        if (rowStartIdx < 0) {
            parameters.add(null);
            parameters.add(null);
        } else {
            // D: ojo: para prime son first y pageSize, pero para el sp son mínimo y máximo
            parameters.add(new BigDecimal(rowStartIdx));
            parameters.add(new BigDecimal(rowStartIdx + rowCount));
        }

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            // D: el procedimiento tiene 3 variables de retorno: un número y dos cursores
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                // D: se revisa si el procedimiento retornó errores.
                // A la fecha 13-04-2012 se estaba retornando un arreglo de objetos (string) con 4 posiciones
                erroresSP = (procedureAnswer[2] != null) ? (ArrayList<Object>) procedureAnswer[2] :
                    null;
                cadenaErroresSP = this.getErrorsCursorAsString((ArrayList) erroresSP);
                if (cadenaErroresSP != null) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS.name());
                } else {
                    answer = new ArrayList<Object[]>();
                    if (isCount) {
                        Object[] count = new Object[1];
                        count[0] = procedureAnswer[0];
                        answer.add(count);
                    } else {
                        answer = (List<Object[]>) procedureAnswer[1];
                    }
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS.name());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Se usa para armar la lista de parámetros que se pasan al procedimiento de búsqueda de
     * predios.
     *
     * definición del procedimiento (09-04-2012): pnumero_predial IN VARCHAR2, pnumero_registro IN
     * VARCHAR2, pnip IN VARCHAR2, pterritorial_codigo IN VARCHAR2, pdepartamento_codigo IN
     * VARCHAR2, pmunicipio_codigo IN VARCHAR2, pdireccion IN VARCHAR2, pnumero_identificacion IN
     * VARCHAR2, pprimer_nombre IN VARCHAR2, psegundo_nombre IN VARCHAR2, pprimer_apellido IN
     * VARCHAR2, psegundo_apellido IN VARCHAR2, prazon_social IN VARCHAR2, pdestinos IN VARCHAR2,
     * parea_terreno_desde IN NUMBER, parea_terreno_hasta IN NUMBER, pusos IN VARCHAR2,
     * parea_construida_desde IN NUMBER, parea_construida_hasta IN NUMBER, pavaluo_desde IN NUMBER,
     * pavaluo_hasta IN NUMBER, pzonas_fisicas IN VARCHAR2, pzonas_geoeconomicas IN VARCHAR2,
     * ptipos_predio IN VARCHAR2, ptipificaciones IN VARCHAR2, pminimo IN NUMBER, pmaximo IN NUMBER,
     * pcontador OUT NUMBER, presultados OUT sys_refcursor, perrores OUT sys_refcursor );
     */
    private List<Object> armarParametrosBusquedaPredios(
        FiltroDatosConsultaPredio datosConsultaPredio) {

        List<Object> parameters = null;

        /**
         * esta se usa para armar los parámetros que deben ir con la forma 'x', 'y', ... 'n' y que
         * llegan del filtro como un arrglo de strings (resultado de un selectManyMenu)
         */
        String tempStringParameters;

        // D: armar la lista de parámetros. Hay que adicionarlos en el orden que
        // el procedimiento almacenado los está esperando (ver definición de éste).
        // Si el criterio de búsqueda viene en null se debe adicionar un null como parámetro para
        // el procedimiento
        parameters = new ArrayList<Object>();

        if (datosConsultaPredio.getNumeroPredial() != null &&
            !datosConsultaPredio.getNumeroPredial().isEmpty()) {
            parameters.add(datosConsultaPredio.getNumeroPredial());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getNumeroRegistro() != null &&
            !datosConsultaPredio.getNumeroRegistro().isEmpty()) {
            parameters.add(datosConsultaPredio.getNumeroRegistro());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getNip() != null &&
            !datosConsultaPredio.getNip().isEmpty()) {
            parameters.add(datosConsultaPredio.getNip());

        } else {
            parameters.add(null);
        }
        //prioridad para la UOC
        if (datosConsultaPredio.getUoc() != null &&
            !datosConsultaPredio.getUoc().isEmpty()) {
            parameters.add(datosConsultaPredio.getUoc());
        } else if (datosConsultaPredio.getTerritorialId() != null &&
            !datosConsultaPredio.getTerritorialId().isEmpty()) {
            parameters.add(datosConsultaPredio.getTerritorialId());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getDepartamentoId() != null &&
            !datosConsultaPredio.getDepartamentoId().isEmpty()) {
            parameters.add(datosConsultaPredio.getDepartamentoId());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getMunicipioId() != null &&
            !datosConsultaPredio.getMunicipioId().isEmpty()) {
            parameters.add(datosConsultaPredio.getMunicipioId());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getDireccionPredio() != null &&
            !datosConsultaPredio.getDireccionPredio().isEmpty()) {
            parameters.add(datosConsultaPredio.getDireccionPredio());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getIdentificacionPropietario() != null &&
            !datosConsultaPredio.getIdentificacionPropietario()
                .isEmpty()) {
            parameters.add(datosConsultaPredio.getIdentificacionPropietario());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getPrimerNombrePropietario() != null &&
            !datosConsultaPredio.getPrimerNombrePropietario().isEmpty()) {
            parameters.add(datosConsultaPredio.getPrimerNombrePropietario());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getSegundoNombrePropietario() != null &&
            !datosConsultaPredio.getSegundoNombrePropietario().isEmpty()) {
            parameters.add(datosConsultaPredio.getSegundoNombrePropietario());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getPrimerApellidoPropietario() != null &&
            !datosConsultaPredio.getPrimerApellidoPropietario()
                .isEmpty()) {
            parameters.add(datosConsultaPredio.getPrimerApellidoPropietario());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getSegundoApellidoPropietario() != null &&
            !datosConsultaPredio.getSegundoApellidoPropietario()
                .isEmpty()) {
            parameters.add(datosConsultaPredio.getSegundoApellidoPropietario());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getRazonSocialPropietario() != null &&
            !datosConsultaPredio.getRazonSocialPropietario().isEmpty()) {
            parameters.add(datosConsultaPredio.getRazonSocialPropietario());
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getDestinoPredioId() != null &&
            !datosConsultaPredio.getDestinoPredioId().isEmpty()) {
            parameters.add("'" + datosConsultaPredio.getDestinoPredioId() + "'");
        } else {
            parameters.add(null);
        }

        // D: para los rangos:
        // si viene un valo r 'hasta' o máximo, se debe tener en cuenta el mínimo que puede ser
        // efectivamente 0, no solo porque así quede al crear el objeto FiltroDatosConsultaPredio
        // N: al crear un objeto de esos, los atributos que son números (tipo simple) quedan en 0.
        // N: la comparación de 0 con 0.0 da que son iguales
        // OJO: para el procedimiento, los números deben ir como BigDecimal
        if (datosConsultaPredio.getAreaTerrenoMax() != 0) {
            parameters.add(new BigDecimal(datosConsultaPredio.getAreaTerrenoMin()));
            parameters.add(new BigDecimal(datosConsultaPredio.getAreaTerrenoMax()));
        } else {
            if (datosConsultaPredio.getAreaTerrenoMin() == 0) {
                parameters.add(null);
            } else {
                parameters.add(new BigDecimal(datosConsultaPredio.getAreaTerrenoMin()));
            }
            parameters.add(null);
        }

        if (datosConsultaPredio.getUsoConstruccionId() != null &&
            !datosConsultaPredio.getUsoConstruccionId().isEmpty()) {
            parameters.add("'" + datosConsultaPredio.getUsoConstruccionId() + "'");
        } else {
            parameters.add(null);
        }

        if (datosConsultaPredio.getAreaConstruidaMax() != 0) {
            parameters.add(new BigDecimal(datosConsultaPredio.getAreaConstruidaMin()));
            parameters.add(new BigDecimal(datosConsultaPredio.getAreaConstruidaMax()));
        } else {
            if (datosConsultaPredio.getAreaConstruidaMin() == 0) {
                parameters.add(null);
            } else {
                parameters.add(new BigDecimal(datosConsultaPredio.getAreaConstruidaMin()));
            }
            parameters.add(null);
        }

        if (datosConsultaPredio.getAvaluoHasta() != 0) {
            parameters.add(new BigDecimal(datosConsultaPredio.getAvaluoDesde()));
            parameters.add(new BigDecimal(datosConsultaPredio.getAvaluoHasta()));
        } else {
            if (datosConsultaPredio.getAvaluoDesde() == 0) {
                parameters.add(null);
            } else {
                parameters.add(new BigDecimal(datosConsultaPredio.getAvaluoDesde()));
            }
            parameters.add(null);
        }

        // D: como no se está usando el parámetro zonas físicas de predio se
        // envía null
        parameters.add(null);

        // D: como no se está usando el parámetro zonas geoeconomicas de predio
        // se envía null
        parameters.add(null);

        // D: como no se está usando el parámetro tipos de predio se envía null
        parameters.add(null);

        // D: como no se está usando el parámetro tipificaciones de predio se
        // envía null
        parameters.add(null);

        return parameters;
    }

    // -------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#reversarProyeccion(Long)
     * @author juan.agudelo
     */
    public boolean reversarProyeccion(Long tramiteId) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] procedureAnswer;
        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(tramiteId));

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                this.entityManager);
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION,
                parametros);

            // Verificación de errores
            if (procedureAnswer == null) {
                // Array de errores nulo, no hay errores.
                return true;
            } else if (procedureAnswer.length > 0) {
                // Array de errores > 0
                for (int i = 0; i < procedureAnswer.length; i++) {
                    if (((ArrayList<Object>) procedureAnswer[i]).isEmpty()) {
                        // Objetos ArrayList del Array de errores vacios, no hay
                        // errores.
                        return true;
                    } else {
                        // Objetos ArrayList con errores.
                        return false;
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION
                        .name());
        }

        return false;
    }

    /**
     * @see ISNCProcedimientoDAO#reversarProyeccionPredio(Long , Long)
     * @author felipe.cadena
     */
    @Override
    public boolean reversarProyeccionPredio(Long idTramite, Long idPredio) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] procedureAnswer;
        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(idTramite));
        parametros.add(new BigDecimal(idPredio));

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                this.entityManager);
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION_PREDIO,
                parametros);

            // Verificación de errores
            if (procedureAnswer == null) {
                // Array de errores nulo, no hay errores.
                return true;
            } else if (procedureAnswer.length > 0) {
                // Array de errores > 0
                for (int i = 0; i < procedureAnswer.length; i++) {
                    if (((ArrayList<Object>) procedureAnswer[i]).isEmpty()) {
                        // Objetos ArrayList del Array de errores vacios, no hay
                        // errores.
                        return true;
                    } else {
                        // Objetos ArrayList con errores.
                        return false;
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION
                        .name());
        }

        return false;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccion(Long)
     * @author juan.agudelo
     */
    /*
     * @modified pedro.garcia 20-09-2013 se obliga a que cree una transacción nueva para ejecutar el
     * procedimiento porque si hay un error, y debe hacer rollback, se pierde la transacción para lo
     * que queda del flujo en donde haya sido invocado este procedimiento
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] confirmarProyeccion(Long tramiteId) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(new BigDecimal(tramiteId));

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {
                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");

                            //N: OJO: esto es peligroso porque si hace esto no puede hacer nada que requiera commit (ej: update) más adelante en el mismo flujo
                            this.context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccionActualizacion(Long)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] confirmarProyeccionActualizacion(Long tramiteId) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(new BigDecimal(tramiteId));

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION_ACT, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {
                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");

                            //N: OJO: esto es peligroso porque si hace esto no puede hacer nada que requiera commit (ej: update) más adelante en el mismo flujo
                            this.context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION_ACT.name());
        }
        return mensajes;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Toma una lista de arreglos de objetos, que es lo que devuelve un procedimiento como cursor de
     * errores, y arma una cadena con el contenido de los mismos.
     *
     * @author pedro.garcia
     * @param cursoresError
     * @return
     */
    private String getErrorsCursorAsString(List<Object[]> cursoresError) {

        String answer;
        StringBuilder tempAnswer;
        int counter;
        String tempCadError;

        tempAnswer = new StringBuilder();
        if (cursoresError != null && !cursoresError.isEmpty()) {
            tempAnswer.append("Errores del procedimiento: ");

            for (Object[] errorX : cursoresError) {
                counter = 1;
                if (errorX != null && errorX.length > 0) {
                    tempAnswer.append("Error ").append(counter).append(": ");
                    for (int i = 0; i < errorX.length; i++) {
                        if (errorX[i] != null) {
                            tempCadError = (String) errorX[i];
                            if (tempCadError.compareTo("") != 0) {
                                tempAnswer.append(" :: ").append(tempCadError);
                            } else {
                                break;
                            }
                        }
                    }
                }
                tempAnswer.append(" || ");
                counter++;
            }
        }
        answer = (tempAnswer.toString().compareTo("") != 0) ? tempAnswer.toString() : null;

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author ariel.ortiz
     */
    @Override
    public Object[] calcularMuestrasControl(String municipioSeleccionadoCodigo,
        BigDecimal valorPedidoD, BigDecimal areaTerrenoD,
        BigDecimal areaConstruidaD, BigDecimal valorCalculadoD, String login) {

        List<Object> parameters = new ArrayList<Object>();
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP = null;

        // Contruccion de la lista de parametros

        /*
         * pmunicipio IN VARCHAR2, pvalorpedido IN NUMBER, pareaterreno IN NUMBER, pareaconstruida
         * IN NUMBER, pvalorcalculado IN NUMBER, pusuario IN VARCHAR2,
         */
        if (municipioSeleccionadoCodigo != null) {
            parameters.add(municipioSeleccionadoCodigo);
        } else {
            parameters.add(null);
        }

        parameters.add(valorPedidoD);
        parameters.add(areaTerrenoD);
        parameters.add(areaConstruidaD);
        parameters.add(valorCalculadoD);

        if (login != null) {
            parameters.add(login);
        } else {
            parameters.add(null);
        }
        // Fin de la construccion de la lista de parametros
        LOGGER.debug("PARAM:" + parameters.get(0));
        LOGGER.debug("PARAM:" + parameters.get(1));
        LOGGER.debug("PARAM:" + parameters.get(2));
        LOGGER.debug("PARAM:" + parameters.get(3));
        LOGGER.debug("PARAM:" + parameters.get(4));
        LOGGER.debug("PARAM:" + parameters.get(5));

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                this.entityManager);

            // D: el procedimiento tiene 2 variables de retorno: un número y un
            // cursor de errores
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CALCULAR_MUESTRA_CALIDAD,
                parameters);

            ArrayList<Object> erroresSP = (procedureAnswer[1] != null) ?
                (ArrayList<Object>) procedureAnswer[1] :
                null;
            cadenaErroresSP = this.getErrorsCursorAsString((ArrayList) erroresSP);

            LOGGER.debug(cadenaErroresSP);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(
                    LOGGER,
                    ex,
                    EProcedimientoAlmacenadoFuncion.CALCULAR_MUESTRA_CALIDAD
                        .name());
        }
        return procedureAnswer;

    }
//--------------------------------------------------------------------------------------------------

    @Override
    public Object[] borrarCalculoMuestras(Long controlCalidadId) {
        Object[] errors = null;
        try {
            List<Object> parametros = new ArrayList<Object>();

            parametros.add(new BigDecimal(controlCalidadId));

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.BORRAR_CONTROL_CALIDAD,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    @Override
    public Object[] guardarCalculoMuestras(Long controlId) {
        Object[] errors = null;
        try {
            List<Object> parametros = new ArrayList<Object>();

            parametros.add(new BigDecimal(controlId));

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GUARDAR_CONTROL_CALIDAD,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISNCProcedimientoDAO#generarNumeroMejoraTE(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String generarNumeroMejoraTE(String numeroPredial) {

        LOGGER.debug("on SNCProcedimientoDAO#generarNumeroMejoraTE ....");

        String answer = null;
        List<Object> parameters;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP;

        parameters = new ArrayList<Object>();
        parameters.add(numeroPredial);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            // D: el procedimiento tiene 2 variables de retorno: un varchar con el número generado
            //  y otro con el error (si lo hubo)
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MEJORA_TE, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                // D: se revisa si el procedimiento retornó errores.
                cadenaErroresSP = (procedureAnswer[1] != null) ? (String) procedureAnswer[1] :
                    null;
                if (cadenaErroresSP != null && !cadenaErroresSP.isEmpty()) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MEJORA_TE.name());
                } else {
                    answer = (String) procedureAnswer[0];
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MEJORA_TE.name());
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISNCProcedimientoDAO#generarNumeroMejoraPH(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String generarNumeroMejoraPH(String numeroPredial) {

        LOGGER.debug("on SNCProcedimientoDAO#generarNumeroMejoraPH ....");

        String answer = null;
        List<Object> parameters;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP;

        parameters = new ArrayList<Object>();
        parameters.add(numeroPredial);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            // D: el procedimiento tiene 2 variables de retorno: un varchar con el número generado
            //  y otro con el error (si lo hubo)
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MEJORA_PH, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                // D: se revisa si el procedimiento retornó errores.
                cadenaErroresSP = (procedureAnswer[1] != null) ? (String) procedureAnswer[1] :
                    null;
                if (cadenaErroresSP != null && !cadenaErroresSP.isEmpty()) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MEJORA_PH.name());
                } else {
                    answer = (String) procedureAnswer[0];
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_MEJORA_PH.name());
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ISNCProcedimientoDAO#generarNumeroUnidad(java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public String generarNumeroUnidad(String piso) {
        LOGGER.debug("on SNCProcedimientoDAO#generarNumeroMejoraPH ....");

        String answer = null;
        List<Object> parameters;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP;

        parameters = new ArrayList<Object>();
        parameters.add(piso);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            // D: el procedimiento tiene 2 variables de retorno: un varchar con el número generado
            //  y otro con el error (si lo hubo)
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_UNIDAD, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                // D: se revisa si el procedimiento retornó errores.
                cadenaErroresSP = (procedureAnswer[1] != null) ? (String) procedureAnswer[1] :
                    null;
                if (cadenaErroresSP != null && !cadenaErroresSP.isEmpty()) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_UNIDAD.name());
                } else {
                    answer = (String) procedureAnswer[0];
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_UNIDAD.name());
        }

        return answer;

    }

    /**
     * @see ISNCProcedimientoDAO#buscarRadicacionesNuevasAvaluosComerciales(Date, Date, String,
     * String, String, String, String, String, String)
     * @author rodrigo.hernandez
     *
     * @modified felipe.cadena -- Se agrega el parametro usuario
     */
    @Implement
    @Override
    public List<RadicacionNuevaAvaluoComercialDTO> buscarRadicacionesNuevasAvaluosComerciales(
        FiltroDatosConsultaRadicacionesCorrespondencia filtroDatos, UsuarioDTO usuario) {

        LOGGER.debug("inicio SNCProcedimientoDAO#buscarRadicacionesNuevasAvaluosComerciales....");

        List<RadicacionNuevaAvaluoComercialDTO> listaRadicacionesNuevas =
            new ArrayList<RadicacionNuevaAvaluoComercialDTO>();
        RadicacionNuevaAvaluoComercialDTO radicacionNueva = null;

        List<Object> parameters;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP;

        Timestamp fechaDesdeTS = null;
        Timestamp fechaHastaTS = null;

        if (filtroDatos.getFechaDesde() != null) {
            fechaDesdeTS = new Timestamp(filtroDatos.getFechaDesde().getTime());
        }

        if (filtroDatos.getFechaHasta() != null) {
            fechaHastaTS = new Timestamp(filtroDatos.getFechaHasta().getTime());
        }

        /* Se valida si el numero de radicacion es vacio para convertirlo en null, ya que en el
         * procedimiento se usan los datos null para evitar crear filtros en el select
         */
        if (filtroDatos.getNumeroRadicacion() != null &&
            filtroDatos.getNumeroRadicacion().isEmpty()) {
            filtroDatos.setNumeroRadicacion(null);
        }

        /* Se valida si el dato de solicitante es vacio para convertirlo en null, ya que en el
         * procedimiento se usan los datos null para evitar crear filtros en el select
         */
        if (filtroDatos.getSolicitante() != null && filtroDatos.getSolicitante().isEmpty()) {
            filtroDatos.setSolicitante(null);
        }

        parameters = new ArrayList<Object>();

        parameters.add(fechaDesdeTS);
        parameters.add(fechaHastaTS);
        parameters.add(filtroDatos.getSolicitante());
        parameters.add(filtroDatos.getCodigoTipoTramite());
        parameters.add(filtroDatos.getNumeroRadicacion());
        parameters.add(filtroDatos.getCodigoTipoEmpresa());
        parameters.add(filtroDatos.getCodigoTerritorial());
        parameters.add(filtroDatos.getCodigoDepartamentoOrigen());
        parameters.add(filtroDatos.getCodigoMunicipioOrigen());
        parameters.add(filtroDatos.getCodigoMunicipioOrigen());
        parameters.add(usuario.getLogin());

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICACIONES_NUEVAS, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {

                cadenaErroresSP = (procedureAnswer[1] != null) ? this
                    .getErrorsCursorAsString((ArrayList) procedureAnswer[1]) :
                    null;

                if (cadenaErroresSP != null && !cadenaErroresSP.isEmpty()) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICACIONES_NUEVAS.name());
                } else {
                    for (Object object : (ArrayList) procedureAnswer[0]) {
                        Object[] array = (Object[]) object;
                        radicacionNueva = new RadicacionNuevaAvaluoComercialDTO();

                        radicacionNueva
                            .setNumeroRadicacion(array[0] != null ? (String) array[0] :
                                "");
                        radicacionNueva.setFechaRadicacion((Date) array[1]);
                        radicacionNueva
                            .setTipoDocumentoSolicitante(array[2] != null ? (String) array[2] :
                                "");
                        radicacionNueva
                            .setNumeroDocumentoSolicitante(array[3] != null ? (String) array[3] :
                                "");
                        radicacionNueva
                            .setNombreSolicitante(array[4] != null ? (String) array[4] :
                                "");
                        radicacionNueva
                            .setTipoTramite(array[5] != null ? (String) array[5] :
                                "");
                        radicacionNueva
                            .setNombreTramite(array[6] != null ? (String) array[6] :
                                "");
                        radicacionNueva
                            .setCodigoTerritorial(array[7] != null ? (String) array[7] :
                                "");
                        radicacionNueva
                            .setNombreTerritorial(array[8] != null ? (String) array[8] :
                                "");
                        radicacionNueva
                            .setCodigoDepartamento(array[9] != null ? (String) array[9] :
                                "");
                        radicacionNueva
                            .setNombreDepartamento(array[10] != null ? (String) array[10] :
                                "");
                        radicacionNueva
                            .setCodigoMunicipio(array[11] != null ? (String) array[11] :
                                "");
                        radicacionNueva
                            .setNombreMunicipio(array[12] != null ? (String) array[12] :
                                "");

                        listaRadicacionesNuevas.add(radicacionNueva);
                    }
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICACIONES_NUEVAS.name());
        }

        LOGGER.debug("fin SNCProcedimientoDAO#buscarRadicacionesNuevasAvaluosComerciales ....");

        return listaRadicacionesNuevas;

    }

    /**
     * @see ISNCProcedimientoDAO#reclasificarTramite(String, String)
     * @modified felipe.cadena -- se agrega parametro usuario
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public void reclasificarTramiteODocumento(String numeroRadicacion, String codigoTipoTramite,
        UsuarioDTO usuario) {

        LOGGER.debug("inicio SNCProcedimientoDAO#reclasificarTramite....");

        List<Object> parameters;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP;

        parameters = new ArrayList<Object>();

        parameters.add(numeroRadicacion);
        parameters.add(codigoTipoTramite);
        parameters.add(usuario.getLogin());

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.RECLASIFICAR_RADICACION, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {

                cadenaErroresSP = (procedureAnswer[0] != null) ? this
                    .getErrorsCursorAsString((ArrayList) procedureAnswer[0]) :
                    null;

                if (cadenaErroresSP != null && !cadenaErroresSP.isEmpty()) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.RECLASIFICAR_RADICACION.name());
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.RECLASIFICAR_RADICACION.name());
        }

        LOGGER.debug("fin SNCProcedimientoDAO#reclasificarTramite ....");

    }

    /**
     * @see ISNCProcedimientoDAO#buscarRadicacion(String)
     * @author rodrigo.hernandez
     *
     * @modified felipe.cadena -- se agrega el parametro usuario
     */
    @Implement
    @Override
    public RadicacionNuevaAvaluoComercialDTO buscarRadicacion(String numeroRadicacion,
        UsuarioDTO usuario) {

        LOGGER.debug("inicio SNCProcedimientoDAO#buscarRadicacion....");

        RadicacionNuevaAvaluoComercialDTO radicacionNueva = null;

        List<Object> parameters;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String cadenaErroresSP;

        parameters = new ArrayList<Object>();
        parameters.add(numeroRadicacion);
        parameters.add(usuario.getLogin());

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICACION, parameters);

            if (procedureAnswer != null && procedureAnswer.length > 0) {

                cadenaErroresSP = (procedureAnswer[1] != null) ? this
                    .getErrorsCursorAsString((ArrayList) procedureAnswer[1]) :
                    null;

                if (cadenaErroresSP != null && !cadenaErroresSP.isEmpty()) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + cadenaErroresSP);
                    Exception ex = new Exception(cadenaErroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                        .getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICACION.name());
                } else {
                    for (Object object : (ArrayList) procedureAnswer[0]) {
                        Object[] array = (Object[]) object;
                        radicacionNueva = new RadicacionNuevaAvaluoComercialDTO();

                        radicacionNueva
                            .setNumeroRadicacion(array[0] != null ? (String) array[0] :
                                "");
                        radicacionNueva.setFechaRadicacion((Date) array[1]);
                        radicacionNueva
                            .setTipoDocumentoSolicitante(array[2] != null ? (String) array[2] :
                                "");
                        radicacionNueva
                            .setNumeroDocumentoSolicitante(array[3] != null ? (String) array[3] :
                                "");
                        radicacionNueva
                            .setNombreSolicitante(array[4] != null ? (String) array[4] :
                                "");
                        radicacionNueva
                            .setTipoTramite(array[5] != null ? (String) array[5] :
                                "");
                        radicacionNueva
                            .setNombreTramite(array[6] != null ? (String) array[6] :
                                "");
                        radicacionNueva
                            .setCodigoTerritorial(array[7] != null ? (String) array[7] :
                                "");
                        radicacionNueva
                            .setNombreTerritorial(array[8] != null ? (String) array[8] :
                                "");
                        radicacionNueva
                            .setCodigoDepartamento(array[9] != null ? (String) array[9] :
                                "");
                        radicacionNueva
                            .setNombreDepartamento(array[10] != null ? (String) array[10] :
                                "");
                        radicacionNueva
                            .setCodigoMunicipio(array[11] != null ? (String) array[11] :
                                "");
                        radicacionNueva
                            .setNombreMunicipio(array[12] != null ? (String) array[12] :
                                "");
                    }
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICACION.name());
        }

        LOGGER.debug("fin SNCProcedimientoDAO#buscarRadicacion ....");

        return radicacionNueva;
    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#validarProyeccion(Long, Long)
     * @author david.cifuentes
     */
    @Override
    public Object[] validarProyeccion(Long tramiteId, Long tramiteSeccionDatosId) {
        Object[] answer;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(tramiteId));
            parametros.add(new BigDecimal(tramiteSeccionDatosId));
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.VALIDAR_PROYECCION,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#revertirProyeccionAvaluos(Long, Long)
     * @author javier.aponte
     */
    @Override
    public Object[] revertirProyeccionAvaluos(Long tramiteId, Long predioId) {
        Object[] errors = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(tramiteId));
            parametros.add(new BigDecimal(predioId));
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REVERTIR_PROYECCION_AVALUO,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    //-----------------------------------------------------------------------------------------//
    /**
     * @author franz.gamba
     * @modified felipe.cadena :: se agrega el parametro idPredio necesario a partir de 2015
     * @see ISNCProcedimientoDAO#obtenerValorUnidadTerreno(String, String, String)
     */
    @Override
    public Double obtenerValorUnidadTerreno(String zonacodigo,
        String destino, String zonaGeoEconomica, Long predioId) {
        Object[] errors = null;
        Double valor = new Double(0);
        try {
            List<Object> parametros = new ArrayList<Object>();
            //parametros.add(valor);
            parametros.add(zonacodigo);
            parametros.add(destino);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_YEAR, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            parametros.add(new Timestamp(cal.getTime().getTime()));
            parametros.add(zonaGeoEconomica);
            parametros.add(predioId);

            ProcedimientoAlmacenadoDAO procedimientoDAO =
                new ProcedimientoAlmacenadoDAO(this.entityManager);
            //felipe.cadena::#11077::13-01-22015::Se utiliza la version valida de la funcion para 2015
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.OBTENERVALORUNIDADTERRENO_ACT,
                parametros, EProcedimientoAlmacenadoFuncionTipo.FUNCION);
            if (errors != null) {
                if (errors[0] == null) {
                    return -1.0;
                }
                BigDecimal val = (BigDecimal) errors[0];
                valor = val.doubleValue();
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e);
        }
        return valor;
    }
    
    public Double obtenerValorUnidadTerreno(String zonacodigo,
            String destino, String zonaGeoEconomica, Long predioId,Timestamp vigencia) {
            Object[] errors = null;
            Double valor = new Double(0);
            try {
                List<Object> parametros = new ArrayList<Object>();
                //parametros.add(valor);
                parametros.add(zonacodigo);
                parametros.add(destino);
                parametros.add(vigencia);
                parametros.add(zonaGeoEconomica);
                parametros.add(predioId);

                ProcedimientoAlmacenadoDAO procedimientoDAO =
                    new ProcedimientoAlmacenadoDAO(this.entityManager);
                //felipe.cadena::#11077::13-01-22015::Se utiliza la version valida de la funcion para 2015
                errors = procedimientoDAO.ejecutarSP(
                    EProcedimientoAlmacenadoFuncion.OBTENERVALORUNIDADTERRENO_ACT,
                    parametros, EProcedimientoAlmacenadoFuncionTipo.FUNCION);
                if (errors != null) {
                    if (errors[0] == null) {
                        return -1.0;
                    }
                    BigDecimal val = (BigDecimal) errors[0];
                    valor = val.doubleValue();
                }

            } catch (ExcepcionSNC e) {
                throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                    LOGGER, e);
            }
            return valor;
        }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#liquidarAvaluos(Long, Long, Date, Usuario)
     * @author javier.aponte
     */
    @Override
    public Object[] liquidarAvaluosParaUnPredio(Long tramiteId, Long predioId, Date date,
        UsuarioDTO usuario) {
        Object[] errors = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(tramiteId));
            parametros.add(new BigDecimal(predioId));
            java.sql.Timestamp fechaDelCalculoTimestamp = new Timestamp(
                date.getTime());
            parametros.add(fechaDelCalculoTimestamp);
            parametros.add(usuario.getLogin());
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            LOGGER.debug("tramiteId:"+(new BigDecimal(tramiteId))+" predioId:"+(new BigDecimal(predioId))
            		+" fechaDelCalculoTimestamp: "+fechaDelCalculoTimestamp+" usuario.getLogin():"+usuario.getLogin());
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.LIQUIDAR_AVALUOS,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    // ------------------------------------------------ //
    /**
     * @see ISNCProcedimientoDAO#ejecutarProcedimientoCierreAnual(EProcedimientoAlmacenadoFuncion,
     * String, MunicipioCierreVigencia, Long, String)
     *
     * @author david.cifuentes
     */
    @Override
    public Object[] ejecutarProcedimientoCierreAnual(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        String codDepartamento, MunicipioCierreVigencia municipio,
        Long vigencia, String usuario) {

        Object[] procedureAnswer;
        String mensajeSP;

        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(codDepartamento);
            if (municipio != null) {
                parametros.add(municipio.getMunicipioCodigo());
            } else {
                parametros.add(null);
            }
            parametros.add(vigencia);
            parametros.add(usuario);
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            procedureAnswer = procedimientoDAO.ejecutarSP(nombreProcedimiento,
                parametros);

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                mensajeSP = (procedureAnswer[0] != null) ? ((String) procedureAnswer[0]) : null;
                if (mensajeSP != null) {
                    LOGGER.info("Ejecución del procedimiento: " + mensajeSP);
                }
            } else {
                LOGGER.error("La ejecución del procedimiento retornó null");
                Exception ex = new Exception("La ejecución del procedimiento retornó null: ");
                throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                    getExcepcion(LOGGER, ex,
                        nombreProcedimiento.name());
            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return procedureAnswer;
    }

    /**
     * @see
     * SNCProcedimientoDAO#buscarPrediosWS(co.gov.igac.snc.util.FiltroConsultaCatastralWebService) ¨
     */
    @Override
    public Object[] buscarPrediosWS(FiltroConsultaCatastralWebService datosFiltro, Long numeroPagina) {

        Object[] answer = null;
        List<Object> parametros;
        Object[] procedureAnswer;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        String erroresSP;

        // andres.eslava: se verifica que el numero de pagina sea mayor que 0 en caso contrario se devuelve la primera pagina de resultados
        if (numeroPagina < 0) {
            numeroPagina = 1L;
        }

        // andres.eslava: se llenan los parámetros en terminos del filtro del web service
        parametros = this.generarStringParamDesdeFiltroWS(datosFiltro, numeroPagina);

        // andres.eslava: se ejecuta el procedimiento
        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);

            // andres.eslava: el procedimiento tiene 5 variables de retorno: un cadena de error y 4 cursores
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_WS, parametros);

            if (procedureAnswer != null && procedureAnswer.length > 0) {
                // andres.eslava: se revisa si el procedimiento retornó errores.                                
                erroresSP = (procedureAnswer[0] != null) ? ((String) procedureAnswer[0]) : null;
                if (erroresSP != null) {
                    LOGGER.error("hubo errores en la ejecución del sp: " + erroresSP);
                    Exception ex = new Exception(erroresSP);
                    throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                        getExcepcion(LOGGER, ex,
                            EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS_WS.name());
                } else {
                    answer = new Object[4];
                    answer[0] = ((List<Object[]>) procedureAnswer[1]);
                    answer[1] = ((List<Object[]>) procedureAnswer[2]);
                    answer[2] = ((List<Object[]>) procedureAnswer[3]);
                    answer[3] = ((List<Object[]>) procedureAnswer[4]);
                }
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO.
                getExcepcion(LOGGER, ex, EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS.name());
        }
        return answer;
    }

    /**
     * Genera la cadena para consultar los predios del SP PKG_CONSULTAS_WS.consultar_predios_ws
     *
     * @param filtro contiene los datos enviados en la consulta del WS.
     * @param numeroPagina Numero de la pagina de consulta para optimo rendimiento.
     * @return retorna la lista de objects que representan los argementos de la consulta.
     * @author andres.eslava
     */
    private List<Object> generarStringParamDesdeFiltroWS(FiltroConsultaCatastralWebService filtro,
        Long numeroPagina) {

        List<Object> parametros = new ArrayList<Object>();

        //andres.eslava agrega el numero predial correspondiente de la consulta
        if (filtro.getPredio_numeroPredialNuevo() != null && !filtro.getPredio_numeroPredialNuevo().
            isEmpty()) {
            parametros.add(filtro.getPredio_numeroPredialNuevo());
            parametros.add(null);
        } else if (filtro.getPredio_numeroPredialAnterior() != null && !filtro.
            getPredio_numeroPredialAnterior().isEmpty()) {
            parametros.add(null);
            parametros.add(filtro.getPredio_numeroPredialAnterior());
        } else {
            parametros.add(null);
            parametros.add(null);
        }

        //Valida Parametros documento Identificacion
        if (filtro.getPersona_tipoIdentificacion() != null && !filtro.
            getPersona_tipoIdentificacion().isEmpty()) {
            parametros.add(filtro.getPersona_tipoIdentificacion());
        } else {
            parametros.add(null);
        }

        if (filtro.getPersona_numeroIdentificacion() != null && !filtro.
            getPersona_numeroIdentificacion().isEmpty()) {
            parametros.add(filtro.getPersona_numeroIdentificacion());
        } else {
            parametros.add(null);
        }

        //Valida Nombre o razon social
        if (filtro.getPersona_razonSocial() != null && !filtro.getPersona_razonSocial().isEmpty()) {
            parametros.add(filtro.getPersona_razonSocial());
        } else {
            parametros.add(null);
        }

        if (filtro.getPersona_primerApellido() != null && !filtro.getPersona_primerApellido().
            isEmpty()) {
            parametros.add(filtro.getPersona_primerApellido());
        } else {
            parametros.add(null);
        }

        if (filtro.getPersona_segundoApellido() != null && !filtro.getPersona_segundoApellido().
            isEmpty()) {
            parametros.add(filtro.getPersona_segundoApellido());
        } else {
            parametros.add(null);
        }
        if (filtro.getPersona_primerNombre() != null && !filtro.getPersona_primerNombre().isEmpty()) {
            parametros.add(filtro.getPersona_primerNombre());
        } else {
            parametros.add(null);
        }

        if (filtro.getPersona_segundoNombre() != null && !filtro.getPersona_segundoNombre().
            isEmpty()) {
            parametros.add(filtro.getPersona_segundoNombre());
        } else {
            parametros.add(null);
        }

        //Informacion del predio 
        if (filtro.getPredioDireccion_direccion() != null && !filtro.getPredioDireccion_direccion().
            isEmpty()) {
            parametros.add(filtro.getPredioDireccion_direccion());
        } else {
            parametros.add(null);
        }

        if (filtro.getPredio_matricula() != null && !filtro.getPredio_matricula().isEmpty()) {
            parametros.add(filtro.getPredio_matricula());
        } else {
            parametros.add(null);
        }

        //Numero de pagina
        parametros.add(new BigDecimal(numeroPagina));

        return parametros;

    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#generarReportePredialDatosBasicos(FiltroGenerarReportes,
     * UsuarioDTO)
     * @author javier.aponte
     * @modified by leidy.gonzalez Se cambian parametros para generar el reporte Predial de acuerdo
     * con el nuevo diseño en la Base de Datos.
     */
    @Override
    public Object[] generarReportePredialDatosBasicos(RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            List<Object> parametros = new ArrayList<Object>();

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            if (repReporteEjecucion != null && repReporteEjecucion.getId() != null) {

                parametros.add(new BigDecimal(repReporteEjecucion.getId()));

            }

            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_REPORTE,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#ejecutarSPAislado(java.lang.String, java.lang.String, boolean)
     * @author pedro.garcia
     */
    @Implement
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Object[] ejecutarSPAislado(String codigoPrueba, String mensajePrueba,
        boolean generarError) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        String generaErrores_s;
        List l;

        parametros.add(codigoPrueba);
        parametros.add(mensajePrueba);

        generaErrores_s = (generarError) ? "S" : "N";
        parametros.add(generaErrores_s);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_ERROR_PRUEBAS, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {
                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");

                            //N: OJO: esto es peligroso porque si hace esto no puede hacer nada que requiera commit (ej: update) más adelante en el mismo flujo
                            this.context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.GENERAR_ERROR_PRUEBAS.toString());
        }
        return mensajes;

    }

    /**
     * @see
     * ISNCProcedimientoDAO#registrarAccesoUsuario(co.gov.igac.snc.persistence.entity.generales.LogAcceso)
     * @author felipe.cadena
     */
    @Override
    public long registrarAccesoUsuario(LogAcceso logAcceso) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] procedureAnswer;
        List<Object> parametros = new ArrayList<Object>();
        parametros.add(logAcceso.getLogin());
        parametros.add(logAcceso.getEstadoSesion());
        parametros.add(logAcceso.getMaquinaCliente());
        parametros.add(logAcceso.getExploradorCliente());

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                this.entityManager);
            procedureAnswer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REGISTRAR_ACCESO,
                parametros);

            BigDecimal ans = (BigDecimal) procedureAnswer[0];

            return ans.longValue();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.REGISTRAR_ACCESO
                        .name());
        }

    }

    /**
     * @see
     * ISNCProcedimientoDAO#registrarAccionUsuario(co.gov.igac.snc.persistence.entity.generales.LogAccion)
     * @author felipe.cadena
     */
    @Override
    public void registrarAccionUsuario(LogAccion logAccion) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        List<Object> parametros = new ArrayList<Object>();
        parametros.add(BigDecimal.valueOf(logAccion.getLogAccesoId()));
        parametros.add(logAccion.getEvento());
        parametros.add(logAccion.getEventoMensaje());
        parametros.add(logAccion.getPrograma());
        parametros.add(logAccion.getDatos());

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                this.entityManager);
            procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REGISTRAR_ACCION,
                parametros);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.REGISTRAR_ACCION
                        .name());
        }

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccion(Long)
     * @author juan.agudelo
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] recurrirProyeccion(Long tramiteId, Long tramiteRecurridoId, String tipoRecurso,
        String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(new BigDecimal(tramiteId));// id del tramite actual
        parametros.add(new BigDecimal(tramiteRecurridoId));//id del tramite original
        parametros.add(tipoRecurso);
        parametros.add(usuario);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.RECURRIR_PROYECCION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.RECURRIR_PROYECCION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#contarRegistrosPrediales(List<Object> params)
     * @author leidy.gonzalez
     */
    @Override
    public Object[] contarRegistrosPrediales(List<Object> params) {

        Object[] answer = null;
        ProcedimientoAlmacenadoDAO procedimientoDAO;

        procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
        answer = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.CONTAR_REGISTROS_PREDIALES, params);

        return answer;
    }

    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccion(Long)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] proyectarFichaMigrada(String numeroPredial, Date fecha, String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(numeroPredial);// numero de radicado
        java.sql.Timestamp fechaTimestamp = new Timestamp(
            fecha.getTime());
        parametros.add(fechaTimestamp);//fecha 
        parametros.add(usuario);//usuario

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.PROYECTAR_FICHAMATRIZ_MIGRADA, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.PROYECTAR_FICHAMATRIZ_MIGRADA.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccion(Long)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] reversarProyeccionFichaMigrada(Long predioId) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(new BigDecimal(predioId));// id predio proyectado

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION_FM_MIG, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION_FM_MIG.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccion(Long)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] confirmarProyeccionFichaMigrada(Long predioId) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(new BigDecimal(predioId));// numero de radicado

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION_FM_MIG, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION_FM_MIG.name());
        }
        return mensajes;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#recibirRadicado(java.lang.String, java.lang.String)
     * @author juanfelipe.garcia
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] recibirRadicado(String pradicacion, String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(pradicacion);// es el número de radicado a marcar como recibido
        parametros.add(usuario);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.RECIBIR_RADICADO, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.RECIBIR_RADICADO.name());
        }
        return mensajes;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @see ISNCProcedimientoDAO#finalizarRadicacion(java.lang.String, java.lang.String,
     * java.lang.String)
     * @author juanfelipe.garcia
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] finalizarRadicacion(String pradicacion, String pradicacionResponde,
        String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(pradicacion);// es el número de radicado a marcar como recibido
        parametros.add(pradicacionResponde);// es el número de radicado con el que se da respuesta
        parametros.add(usuario);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.FINALIZAR_RADICACION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.FINALIZAR_RADICACION.name());
        }
        return mensajes;
    }

    //-----------------------------------------------------------------------------------------//
    /**
     * @author franz.gamba
     * @see ISNCProcedimientoDAO#obtenerValorUnidadConstruccion(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.Long)
     */
    @Override
    public Double obtenerValorUnidadConstruccion(String zonacodigo, String sector, String destino,
        String usoConstruccion, String zonaGeoEconomica, Long puntaje, Long predioId) {
        Object[] errors = null;
        Double valor = new Double(0);
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(zonacodigo);
            parametros.add(sector);
            parametros.add(destino);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_YEAR, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            parametros.add(new Timestamp(cal.getTime().getTime()));
            parametros.add(BigDecimal.valueOf(Long.valueOf(usoConstruccion)));
            parametros.add(zonaGeoEconomica);
            parametros.add(BigDecimal.valueOf(puntaje));
            parametros.add(predioId);

            ProcedimientoAlmacenadoDAO procedimientoDAO =
                new ProcedimientoAlmacenadoDAO(this.entityManager);
            //felipe.cadena::#11077::13-01-22015::Se utiliza la version valida de la funcion para 2015
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.OBTENERVALORUNIDADCONST_ACT,
                parametros, EProcedimientoAlmacenadoFuncionTipo.FUNCION);
            if (errors != null) {
                if (errors[0] == null) {
                    return -1.0;
                }
                BigDecimal val = (BigDecimal) errors[0];
                valor = val.doubleValue();
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e);
        }
        return valor;
    }

    // ---------------------------//
    /**
     * @see ISNCProcedimientoDAO#generarReporteCierreAnual(RepReporteEjecucion)
     * @author david.cifuentes
     *
     * @note: Copia del método generarReportePredialDatosBasicos de leidy.gonzalez, usado en la
     * consulta de reportes prediales.
     */
    @Override
    public Object[] generarReporteCierreAnual(
        RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            List<Object> parametros = new ArrayList<Object>();

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            if (repReporteEjecucion != null && repReporteEjecucion.getId() != null) {

                parametros.add(new BigDecimal(repReporteEjecucion.getId()));
            }
            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_REPORTE,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see ISNCProcedimientoDAO#confirmarProyeccionMasivo(Long)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] confirmarProyeccionMasivo(List<Tramite> tramites) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        //TODO::definir procedimiento a usar para la aplicacion de cambios masiva
        //parametros.add(new BigDecimal(tramiteId));
        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {
                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");

                            //N: OJO: esto es peligroso porque si hace esto no puede hacer nada que requiera commit (ej: update) más adelante en el mismo flujo
                            this.context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CONFIRMAR_PROYECCION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#validarActualizacion(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] validarActualizacion(Long idMunicipioActualizacion, String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(idMunicipioActualizacion);// Registro de municipios actualizacion
        parametros.add(usuario);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.VALIDAR, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.VALIDAR.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#radicarActualizacion(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] radicarActualizacion(Long idMunicipioActualizacion, Long idSolicitud,
        String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(idMunicipioActualizacion);// Municipio asociado al proceso de actualizacion
        parametros.add(idSolicitud);// id solicitud asociada al proceso de actualizacion
        parametros.add(usuario);

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.RADICACION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.RADICACION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#validarRadicacionActualizacion(java.lang.Long)
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public int validarRadicacionActualizacion(Long idMunicipioActualizacion) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        BigDecimal l = new BigDecimal(BigInteger.ZERO);

        parametros.add(idMunicipioActualizacion);// Municipio actualizacion asociado al proceso de actualizacion

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.PREDIO_UNICO, parametros);

            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (BigDecimal) o;
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.RADICACION.name());
        }
        return l.intValue();
    }

    /**
     * @see ISNCProcedimientoDAO#eliminarActualizacion
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] eliminarActualizacion(Long idMunicipioActualizacion, String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(idMunicipioActualizacion);// id del registro asociado al proceso de actualizacion que se quiere eliminar
        parametros.add(usuario);// usuario que elimina

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.ELIMINAR_ACTUALIZACION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.ELIMINAR_ACTUALIZACION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#eliminarValoresActualizacion
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] eliminarValoresActualizacion(Long valorActualizacionId, String municipioCodigo) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(municipioCodigo);// usuario que elimina
        parametros.add(valorActualizacionId);// id del valor de actualizacion a eliminar

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.BORRAR_TABLAS_LIQUIDACION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.ELIMINAR_VALORES_ACTUALIZACION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#cargarTablasLiquidacion
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] cargarTablasLiquidacion(String municipioCodigo, Long valorActualizacionId,
        String usuario) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(valorActualizacionId);// id del valor de actualizacion a eliminar

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CARGAR_TABLAS_LIQUIDACION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CARGAR_TABLAS_LIQUIDACION.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#eliminarValoresActualizacion
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] calcularAvaluosTablas(String municipioCodigo) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(municipioCodigo);// codigo del municipio
        // parametros.add(usuario);// usuario que elimina

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CALCULAR_AVALUOS_TABLAS, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.CALCULAR_AVALUOS_TABLAS.name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#eliminarValoresActualizacion
     * @author felipe.cadena
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] aplicarTablasLiquidacion(String municipioCodigo) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();
        List l;

        parametros.add(municipioCodigo);// id del valor de actualizacion a eliminar
        //parametros.add(usuario);// usuario que elimina

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.APLICAR_TABLAS_LIQUIDACION, parametros);

            int numeroErrores = 0;
            if (mensajes != null) {

                for (Object o : mensajes) {
                    l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            LOGGER.debug("Debe hacer rollback");
                            context.setRollbackOnly();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.ELIMINAR_VALORES_ACTUALIZACION.name());
        }
        return mensajes;
    }

    /**
     * @author jonathan.chacon
     *
     * @see ISNCProcedimientoDAO#CalcularPorcentajeIncrementoPredio
     * @param vigencia
     * @param predioId
     * @return
     */
    @Override
    public double CalcularPorcentajeIncrementoPredio(String vigencia, Long predioId) {

        double answer = 0.0;
        Object[] errors;
        try {
            List<Object> parametros = new ArrayList<Object>();
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaDelCalculo = (Date) formatter.parse(vigencia);

            java.sql.Timestamp fecha = new Timestamp(fechaDelCalculo.getTime());

            parametros.add(fecha);
            parametros.add(new BigDecimal(predioId));
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CALCULARPCTINCREMENTOPREDIO,
                parametros, EProcedimientoAlmacenadoFuncionTipo.FUNCION);

            if (errors != null) {
                if (errors[0] == null) {
                    return -1.0;
                }
                BigDecimal val = (BigDecimal) errors[0];
                answer = val.doubleValue();
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(SNCProcedimientoDAO.class.getName()).
                log(Level.SEVERE, null, ex);
        }

        return answer;
    }

    /**
     * @see ISNCProcedimientoDAO#establecerAnioYNumeroUltimaResolucion(Long, int, String, Usuario)
     * @author javier.aponte
     */
    @Override
    public Object[] establecerAnioYNumeroUltimaResolucion(Long tramiteId, int anioUltimaResolucion,
        String numeroUltimaResolucion, UsuarioDTO usuario) {

        Object[] errors = null;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(tramiteId));
            parametros.add(new BigDecimal(anioUltimaResolucion));
            parametros.add(numeroUltimaResolucion);
            parametros.add(usuario.getLogin());

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
            errors = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.ACTUALIZAR_DATOS_ULTIMA_RESOLU,
                parametros);

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return errors;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @SuppressWarnings({"unused", "unchecked"})
    @Override
    public Object[] consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(
        String estructuraOrganizacionalCodigo) {

        ProcedimientoAlmacenadoDAO procedimientoDAO;
        Object[] mensajes;
        List<Object> parametros = new ArrayList<Object>();

        parametros.add(estructuraOrganizacionalCodigo);// es la estructura
        // organizacional

        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.PRC_HORARIO_ATENCION, parametros);

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(
                    LOGGER,
                    ex,
                    EProcedimientoAlmacenadoFuncion.PRC_HORARIO_ATENCION
                        .name());
        }
        return mensajes;
    }

    /**
     * @see ISNCProcedimientoDAO#consultarFechaCreacionPredio(Long)
     * @author leidy.gonzalez
     */
    @Override
    public Date consultarFechaCreacionPredio(Long pPredioId) {

        Date fecha = new Date();
        BigDecimal fechaProcedimiento = new BigDecimal(BigInteger.ZERO);
        List<Object> parametros = new ArrayList<Object>();
        Object[] errors;

        try {
            parametros.add(new BigDecimal(pPredioId));

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            errors = procedimientoDAO
                .ejecutarSP(EProcedimientoAlmacenadoFuncion.FECHA_INSCRIPCION_FICHA_MATRIZ,
                    parametros);

            if (errors != null) {
                for (Object o : errors) {
                    fechaProcedimiento = (BigDecimal) o;
                    fecha = new Date(fechaProcedimiento.longValue());
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return fecha;
    }

    /**
     * @see ISNCProcedimientoDAO#validaNumeroRadicacion(String,UsuarioDTO)
     * @author dumar.penuela
     * @param numeroRadicacionCorrespondencia
     * @param usuario
     * @return
     */
    @Override
    public boolean validaNumeroRadicacion(
        String numeroRadicacionCorrespondencia,
        UsuarioDTO usuario) {
        boolean encontrado = false;
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(numeroRadicacionCorrespondencia);
            parametros.add(usuario.getLogin());

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            Object[] resultado = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_RADICADO,
                parametros);

            if (resultado != null && resultado[0] != null) {
                Object[] radicadoCorrespondencia = ((Object[]) ((ArrayList<Object>) resultado[0])
                    .get(0));
                if (radicadoCorrespondencia[0] != null) {
                    encontrado = true;
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return encontrado;
    }

    /**
     *
     * @see ISNCProcedimientoDAO#consultarJobsPendientesPorProcesar(java.lang.String, int,
     * java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public List<ProductoCatastralJob> consultarJobsPendientesPorProcesar(
        int cantidad,
        String estadoInicial,
        String estadoFinal) {
        LOGGER.debug("Inicio metodo: consultarJobsPendientesPorProcesar");

        List<ProductoCatastralJob> jobsPorProcesar = new ArrayList<ProductoCatastralJob>();
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(BigDecimal.valueOf(cantidad));
            LOGGER.debug("Par 1 Cantidad:" + cantidad);
            parametros.add(estadoInicial);
            LOGGER.debug("Par 2 Estado Inicial:" + estadoInicial);
            parametros.add(estadoFinal);
            LOGGER.debug("Par 3 Estado Inicial:" + estadoFinal);

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            LOGGER.debug("Inicio de ejecucion de Procedimiento: PRC_JOBS_POR_PROCESAR");

            Object[] resultado = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.PRC_JOBS_POR_PROCESAR,
                parametros);

            if (resultado != null && resultado.length > 1) {

                LOGGER.debug("Resultado 0:" + resultado[0]);
                LOGGER.debug("Resultado 1:" + resultado[1]);
            }

            List<Object> resultList = (List<Object>) resultado[0];

            for (Object object : resultList) {
                ProductoCatastralJob job = new ProductoCatastralJob();
                Object[] fields = (Object[]) object;
                job.setId(((BigDecimal) fields[0]).longValue());
                LOGGER.debug("Resultado de ejecucion de Procedimiento: PRC_JOBS_POR_PROCESAR");
                LOGGER.debug("Id job:" + job.getId());
                job.setEstado((String) fields[1]);
                LOGGER.debug("Estado job:" + job.getEstado());
                job.setFechaLog((Date) fields[2]);
                LOGGER.debug("FechaLog job:" + job.getFechaLog());
                job.setTipoProducto((String) fields[3]);
                LOGGER.debug("TipoProducto job:" + job.getTipoProducto());
                job.setUsuarioLog((String) fields[4]);
                if (fields[5] != null) {
                    job.setFechaActualizadoLog((Date) fields[5]);
                }
                if (fields[6] != null) {
                    job.setTramiteId(((BigDecimal) fields[6]).longValue());
                    LOGGER.debug("TramiteId job:" + job.getTramiteId());
                }
                if (fields[7] != null) {
                    job.setNumeroEnvio(((BigDecimal) fields[7]).intValue());
                }
                if (fields[8] != null) {
                    job.setCodigo((String) fields[8]);
                }
                if (fields[9] != null) {
                    job.setResultado((String) fields[9]);
                }
                if (fields[10] != null) {
                    job.setProductoCatastralDetalleId(((BigDecimal) fields[10]).longValue());
                }

                LOGGER.debug("Adicionar job con id: " + job);
                jobsPorProcesar.add(job);
            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return jobsPorProcesar;
    }

    @Override
    public Object[] finalizarRadicacionResoluciones(String usuario, String radicado,
        List<String> resoluciones) {
        Object[] mensajes;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        final List<Object> parametros = new ArrayList<Object>();
        final String strResoluciones = StringUtils.join(resoluciones, ",");
        parametros.add(usuario);
        parametros.add(radicado);
        parametros.add(strResoluciones);
        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.FINALIZAR_RADICACION_RES, parametros);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.FINALIZAR_RADICACION_RES.name());
        }
        return mensajes;
    }

    @Override
    public Object[] finalizarRadicacionInterno(final String usuario,
        final String radicado, final String radicadoResponde) {
        Object[] mensajes;
        ProcedimientoAlmacenadoDAO procedimientoDAO;
        final List<Object> parametros = new ArrayList<Object>();
        parametros.add(usuario);
        parametros.add(radicado);
        parametros.add(radicadoResponde);
        String respuesta = "";
        parametros.add(respuesta);
        try {
            procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.entityManager);
            mensajes = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.FINALIZAR_RADICACION_IE, parametros);
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_GENERAL_PROCEDIMIENTO_ALMACENADO
                .getExcepcion(LOGGER, ex,
                    EProcedimientoAlmacenadoFuncion.FINALIZAR_RADICACION_IE.name());
        }
        return mensajes;
    }

    /**
     *
     * @see ISNCProcedimientoDAO#aceptarEnglobeVirtual(java.lang.Long, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public Object[] aceptarEnglobeVirtual(Long tramiteId, String usuario) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(tramiteId));
        parametros.add(usuario);
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
            entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.GENERAR_PROYECCION_EV,
            parametros);
        int numeroErrores = 0;
        if (mensajes != null) {

            for (Object o : mensajes) {
                List l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!((Object[]) obje)[0].equals("0")) {
                            numeroErrores++;
                        }
                    }
                    if (numeroErrores > 0) {
                        LOGGER.debug("Debe hacer rollback");
                        context.setRollbackOnly();
                    }
                }
            }
        }
        return mensajes;

    }

    /**
     *
     * @see ISNCProcedimientoDAO#generarNumeroEnglobeVirtual(java.lang.String, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public Object[] generarNumeroEnglobeVirtual(Long tramiteId, String condicionPropiedad,
        String usuario) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(tramiteId));
        parametros.add(condicionPropiedad);
        //parametros.add(usuario);  
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
            entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_FM_EV,
            parametros);

        return mensajes;

    }

    /**
     * @see ISNCProcedimientoDAO#generarReporteRadicacion(RepReporteEjecucion)
     * @author david.cifuentes
     */
    @Override
    public Object[] generarReporteRadicacion(RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            List<Object> parametros = new ArrayList<Object>();

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            if (repReporteEjecucion != null && repReporteEjecucion.getId() != null) {
                parametros.add(new BigDecimal(repReporteEjecucion.getId()));
            }

            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_REPORTE,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     *
     * @see ISNCProcedimientoDAO#calculoZonasFisicasYGeoeconomicas(java.lang.Date, java.lang.String)
     * @author leidy.gonzalez
     */
    @Override
    public Object[] calculoZonasFisicasYGeoeconomicas(Date vigencia, String zona) {

        List<Object> parametros = new ArrayList<Object>();

        java.sql.Timestamp fechaVigenciaTimestamp = new Timestamp(
            vigencia.getTime());

        parametros.add(zona);
        parametros.add(fechaVigenciaTimestamp);
       

        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
            entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
            EProcedimientoAlmacenadoFuncion.PRC_ZG_ZHF_NO_AUT,
            parametros);

        return mensajes;

    }
    
    /**
     * @see ISNCProcedimientoDAO#generarReporteEstadistico(RepReporteEjecucion)
     * @param repReporteEjecucion
     * @return
     */
    public Object[] generarReporteEstadistico(
        RepReporteEjecucion repReporteEjecucion) {
        Object[] answer = null;
        try {
            List<Object> parametros = new ArrayList<Object>();

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);

            if (repReporteEjecucion != null && repReporteEjecucion.getId() != null) {
                parametros.add(new BigDecimal(repReporteEjecucion.getId()));
            }

            answer = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.PR_EJECUTARREPORTE,
                parametros);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
    
    private boolean esCodigoHabilitada(String codigo){
        EstructuraOrganizacional estructuraOrg = 
                estructuraOrganizacionalDao
                .buscarEstructuraOrganizacionalPorCodigo(codigo);
        if(estructuraOrg!=null && estructuraOrg.getTipo()!=null ){
            return estructuraOrg.getTipo().equals(ETipoEstructuraOrganizacional.HABILITADA.getCodigo());
        }
        return false;
    }

    /**
     *
     * @see ISNCProcedimientoDAO#iniciarCargueCica(java.lang.Long, java.lang.String)
     * @author felipe.cadena
     */
    @Override
    public Object[] iniciarCargueCica(Long municipioActualizacionId, String usuario) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(new BigDecimal(municipioActualizacionId));
        parametros.add(usuario);
        //parametros.add(usuario);
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.P_CARGAR_ACTUALIZACION,
                parametros);

        return mensajes;

    }
    /**
     *
     * @see ISNCProcedimientoDAO#eliminarProyeccionCica(List<Long>)
     * @author felipe.cadena
     */
    @Override
    public Object[] eliminarProyeccionCica(List<Long> idsPredios) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(idsPredios);
        //parametros.add(usuario);
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.P_REVERSAR_MULTIPLES_PREDIOS,
                parametros);

        return mensajes;

    }

    /**
     *
     * @see ISNCProcedimientoDAO#preliquidarCargueAct(String, Integer)
     * @author felipe.cadena
     */
    @Override
    public Object[] preliquidarCargueAct(String municipioCodigo, Integer vigencia ) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(municipioCodigo);
        parametros.add(new BigDecimal(vigencia));
        //parametros.add(usuario);
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.P_PRELIQUIDAR,
                parametros);

        return mensajes;

    }

 /**
     *
     * @see ISNCProcedimientoDAO#aplicarCambiosAct(String, Integer)
     * @author felipe.cadena
     */
    @Override
    public Object[] aplicarCambiosAct(String municipioCodigo, Integer vigencia ) {

        List<Object> parametros = new ArrayList<Object>();
        parametros.add(municipioCodigo);
        parametros.add(new BigDecimal(vigencia));
        //parametros.add(usuario);
        ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                entityManager);
        Object mensajes[] = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.APLICAR_CAMBIOS_ACT,
                parametros);

        return mensajes;

    }


// end of class    
}
