/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioInconsistencia;

@Local
public interface IPredioInconsistenciaDAO extends IGenericJpaDAO<PredioInconsistencia, Long> {

    /**
     * Método que consulta una lista de tipo PredioInconsistencia por el id del predio
     *
     * @param predioId
     * @return
     * @author lorena.salamanca
     */
    public List<PredioInconsistencia> findByPredioId(Long predioId);
}
