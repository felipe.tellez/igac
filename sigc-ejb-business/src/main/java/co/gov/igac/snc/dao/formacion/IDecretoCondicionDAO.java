package co.gov.igac.snc.dao.formacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;

/**
 * Interfaz para los servicios de persistencia del objeto {@link DecretoCondicion}.
 *
 * @author david.cifuentes
 */
@Local
public interface IDecretoCondicionDAO extends
    IGenericJpaDAO<DecretoCondicion, Long> {

}
