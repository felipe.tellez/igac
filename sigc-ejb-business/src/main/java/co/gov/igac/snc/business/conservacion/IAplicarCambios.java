/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.conservacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.persistence.util.EProcesosAsincronicos;

import java.io.File;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author felipe.cadena
 */
@Local
public interface IAplicarCambios {

    /**
     * Inicia el proceso de generacion de una replica de consulta e inicia el proceso de depuracion
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @param usuario
     * @param responsableSIG -- Es el usuario con rol responsable SIG que inicia el proceso de
     * depuracion.
     */
    public void generarReplicaDeConsultaDepuracion(Long tramiteId, UsuarioDTO usuario,
        UsuarioDTO responsableSIG);

    /**
     * Inicia el proceso de generacion de una replica de consulta
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @param usuario
     */
    public void generarReplicaDeConsulta(Long tramiteId, UsuarioDTO usuario);

    /**
     * Finaliza la generacion de la replica de consulta
     *
     * @author felipe.cadena
     * @param job
     * @return
     */
    public boolean finalizarReplicaConsulta(ProductoCatastralJob job);

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------         proceso aplicar cambios Depuración   -----------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO ACD # 1
     *
     * Metodo para transferir el tramite a tiempos muertos
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public void transferirATiemposMuertosACD(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACD # 2 <br/><br/>
     *
     * Metodo asociado al paso 2 del proceso de aplicar cambios de depuracion. El paso 2 es
     * asincronico, asi que en este metodo solo se inicia el paso.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean enviarJobGeograficoDepuracion(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACD # 3 <br/><br/>
     *
     * Metodo para avanzar renombrar las replicas del proceso de depuración
     *
     * @param tramite
     * @param usuario
     * @return
     *
     */
    public void renombrarReplicasDepuracion(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACD # 4 <br/><br/>
     *
     * Metodo para avanzar el proceso a la actividad corespondiente una vez se termino la aplicación
     * de cambios gograficos
     *
     * @param tramite
     * @param usuario
     * @return
     *
     */
    public UsuarioDTO avanzarProcesoACD(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACD # 5 <br/><br/>
     *
     * Metodo para transferir el tramite desde tiempos muertos al usuario oiginal
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    public void transferirAUsuarioOriginalACD(Tramite tramite, UsuarioDTO usuario);

    /**
     * Metodo para invocar el proceso aplicar cambios de depuracion desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void aplicarCambiosDepuracion(Tramite tramite, UsuarioDTO usuario, int paso);

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------   proceso generar replica de consulta  -----------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO GCC # 1 <br/><br/>
     *
     * Metodo para mover la actividad al proceso de depuración
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param responsableSIG
     * @param usuario
     * @return
     */
    public boolean avanzarTramiteADepuracion(Tramite tramite, UsuarioDTO responsableSIG,
        UsuarioDTO usuario);

    /**
     * PASO GCC # 2 <br/><br/>
     *
     * Metodo para mover transferir la actividad al usuario tiempos muertos mientras se termina la
     * creación de la replica.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean transferirTramiteATiemposMuertosGCC(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO GCC # 3.1 <br/><br/>
     *
     * Metodo asociado al envio del job para generar la replica de consulta relacionada a un tramite
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario -- es el usuario que reclamara la actividad al final de la generacion de la
     * replica
     */
    public void enviarJobGenerarReplicaConsulta(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO GCC # 4 <br/><br/>
     *
     * Guarda el registro en la db relacionado al archivo de la replica.
     *
     * @author felipe.cadena
     *
     * @param jobReplica
     * @return
     */
    public Object[] finalizarGenerarReplicaConsulta(ProductoCatastralJob jobReplica);

    /**
     * PASO GCC # 5 <br/><br/>
     *
     * Metodo para transferir la actividad desde tiempos muertos a un usuario determinado.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    public void transferirTramiteDeTiemposMuertosAUsuarioGCC(Tramite tramite, UsuarioDTO usuario);

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param usuarioResponsableSIG -- Solo se usa para el envio a depuracion(paso 0), puede ser
     * null en los demas casos
     * @param paso
     * @param jobGeografico
     */
    public void generarReplicaConsulta(Tramite tramite, UsuarioDTO usuario,
        UsuarioDTO usuarioResponsableSIG, int paso, ProductoCatastralJob jobGeografico);

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     * @param jobGeografico
     */
    public void generarReplicaEdicion(Tramite tramite, UsuarioDTO usuario, int paso,
        ProductoCatastralJob jobGeografico);

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------   proceso generar replica de edicion  -----------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO GCE # 1 <br/><br/>
     *
     * Metodo para mover la actividad a la actividad de modificacion geografica
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean avanzarTramiteAModificacionGeografica(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO GCC # 2 <br/><br/>
     *
     * Metodo para mover transferir la actividad al usuario tiempos muertos mientras se termina la
     * creación de la replica.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean transferirTramiteATiemposMuertosGCE(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO GCE # 3.1 <br/><br/>
     *
     * Metodo asociado al envio del job para generar la replica de edicion relacionada a un tramite
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario -- es el usuario que reclamara la actividad al final de la generacion de la
     * replica
     */
    public void enviarJobGenerarReplicaEdicion(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO GCE # 4 <br/><br/>
     *
     * Guarda el registro en la db relacionado al archivo de la replica.
     *
     * @author felipe.cadena
     *
     * @param jobReplica
     */
    public Object[] finalizarGenerarReplicaEdicion(ProductoCatastralJob jobReplica);

    /**
     * PASO GCE # 5 <br/><br/>
     *
     * Metodo para transferir la actividad desde tiempos muertos a un usuario determinado.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     */
    public void transferirTramiteDeTiemposMuertosAUsuarioGCE(Tramite tramite, UsuarioDTO usuario);

    /**
     * Determina si el tramite tiene copia geografica de conusulta
     *
     * @author felipe.cadena
     * @param tramite
     * @return --true si el tramite tiene copia de consulta --false si aun no existe copia
     *
     */
    public boolean validarExistenciaCopiaConsulta(Tramite tramite);

    /**
     * Determina si el tramite tiene copia geografica de edicion del tramite
     *
     * @author felipe.cadena
     * @param tramite
     * @return --true si el tramite tiene copia de consulta --false si aun no existe copia
     *
     */
    public boolean validarExistenciaCopiaEdicion(Tramite tramite);

    /**
     * Metodo para actualizar el estado de los tramites que fallaron en la tarea asincronica en el
     * ARCGIS server. Retorna true si la actualizacion se realizó de manera correcta.
     *
     * @param jobs -- jobs a procesar
     * @author felipe.cadena
     * @return
     */
    public boolean actualizarEstadoTramitesAGS(List<ProductoCatastralJob> jobs);

    /**
     * Metodo para obtener los jobs que tienen error en AGS
     *
     * @author felipe.cadena
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsErrorAGS();

    /**
     * Metodo para iniciar la aplicacion de cambios
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param usuario
     */
    public void aplicarCambiosInicio(Long tramiteId, UsuarioDTO usuario);

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //--------------   proceso aplicar cambios conservacion  -----------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    /**
     * PASO ACC # 1.1 <br/><br/>
     *
     * Metodo asociado al paso 1 del proceso de aplicar cambios de conservacion. El paso 1 es
     * asincronico, asi que en este metodo solo se inicia el paso.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean enviarJobGeografico(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACC # 1.2
     *
     * Metodo asociado al paso 1 del proceso de aplicar cambios de conservacion Se actualiza el
     * estado el paso, para confirmar que el job geografico fue exitoso. Se envia el resto del
     * proceso de aplicar cambios, ademas se maneja el proceso cuano se trata de un tramite de
     * Depuracion y finalmente se envia los jobs para eliminar y actualizar las versiones
     * geograficas del tramite.
     *
     *
     * @author felipe.cadena
     * @param registroTrabajoFinalizado
     * @return
     */
    public boolean recibirJobGeografico(ProductoCatastralJob registroTrabajoFinalizado);

    /**
     * PASO ACC # 2 <br/><br/>
     *
     * Metodo asociado al paso 2 del proceso de aplicar cambios de conservacion. Aplica los cambios
     * alfanumericos asociados al tramite
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean aplicarCambiosAlfanumericos(Tramite tramite, UsuarioDTO usuario,
        EProcesosAsincronicos proceso);

    /**
     * PASO ACC # 3<br/><br/>
     *
     * Metodo asociado al paso 3 del proceso de aplicar cambios de conservacion. Finaliza el proceso
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean finalizarProcessAplicarCambios(Tramite tramite, UsuarioDTO usuario,
        EProcesosAsincronicos proceso);

    /**
     * PASO ACC # 4<br/><br/>
     *
     * Metodo asociado al paso 4 del proceso de aplicar cambios de conservacion. Actualiza los
     * estados del tramite y la resolucion cuando se ha terminado la actividad de aplicar cambios.
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean finalizarEstadosAlfanumericos(Tramite tramite, UsuarioDTO usuario,
        EProcesosAsincronicos proceso);

    /**
     * PASO ACC # 5.1 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se invoca el proceso
     * asincronico para la generacion de la ficha predial digital. la parte final de este paso se
     * ejecuta en : IConservacionLocal#procesarFichaPredialDigital <br/><br/>
     *
     * Nota: Los pasos 5 y 6 de aplicar cambios se envian de forma simultanea sin orden especifico
     * de finalizacion
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean generarFichaPredial(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACC # 5.2 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se finaliza la carta
     * de la ficha predial digital <br/><br/>
     *
     *
     *
     * @author felipe.cadena
     * @param job
     * @return
     */
    public boolean finalizarFichaPredial(ProductoCatastralJob job);

    /**
     * PASO ACC # 6.1 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se invoca el proceso
     * asincronico para la generacion de la carta catastral. la parte final de este paso se ejecuta
     * en : IConservacionLocal#procesarCartaCatastralUrbana
     *
     * <br/><br/>
     *
     * Nota: Los pasos 5 y 6 de aplicar cambios se envian de forma simultanea sin orden especifico
     * de finalizacion
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean generarCartaCatastral(Tramite tramite, UsuarioDTO usuario);

    /**
     * PASO ACC # 6.2 <br/><br/>
     *
     * Metodo asociado al paso 5 del proceso de aplicar cambios de conservacion Se finaliza la carta
     * de la carta catastral urbana <br/><br/>
     *
     *
     *
     * @author felipe.cadena
     * @param job
     * @return
     */
    public boolean finalizarCartaCatastral(ProductoCatastralJob job);

    /**
     * Metodo para invocar el aplicar cambios desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez::12/11/2015:: Se modifica tipo de retorno del metodo.
     */
    public boolean aplicarCambiosConservacion(Tramite tramite, UsuarioDTO usuario, int paso);

    /**
     * Actualiza la entidad tramite tarea
     *
     * @author felipe.cadena
     *
     * @param tt
     * @return
     */
    public TramiteTarea actualizarTramiteTarea(TramiteTarea tt);

    /**
     * Metodo para invocar el aplicar cambios del proceso de Actualizacion desde un paso determinado
     * ACC
     *
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez::23/10/2015:: Se modifica tipo de retorno del metodo.
     */
    public boolean aplicarCambiosActualizacion(Tramite tramite, UsuarioDTO usuario, int paso);

    /**
     *
     * ACA Masivo
     *
     * Metodo para invocar el aplicar cambios del proceso de Actualizacion desde un paso determinado
     * ACC
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void aplicarCambiosActualizacionMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        int paso);

    /**
     * PASO ACMasivo # 2 <br/><br/>
     *
     * Metodo asociado al paso 2 del proceso de aplicar cambios. Aplica los cambios alfanumericos
     * asociados una lista de tramites
     *
     * @author felipe.cadena
     * @param tramites
     * @param usuario
     * @return
     */
    public boolean aplicarCambiosAlfanumericosMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        EProcesosAsincronicos proceso);

    /**
     * PASO ACMasivo # 3<br/><br/>
     *
     * Metodo asociado al paso 3 del proceso de aplicar cambios de conservacion. Finaliza el proceso
     * para los tramites de la lista
     *
     * @author felipe.cadena
     * @param tramites
     * @param usuario
     * @return
     */
    public boolean finalizarProcessAplicarCambiosMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        EProcesosAsincronicos proceso);

    /**
     * PASO ACMasivo # 4<br/><br/>
     *
     * Metodo asociado al paso 4 del proceso de aplicar cambios de conservacion. Actualiza los
     * estados del tramite y la resolucion cuando se ha terminado la actividad de aplicar cambios.
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean finalizarEstadosAlfanumericosMasivo(List<Tramite> tramites, UsuarioDTO usuario,
        EProcesosAsincronicos proceso);

    /**
     * Metodo que almacena el documento que se genera en las resoluciones y actualiza el tramite
     * asociandole la resolucion que se genero. antes de ingresar a la actividad de aplicar cambios.
     *
     * @author leidy.gonzalez
     * @param tramite
     * @param usuario
     * @param tramiteDocumento
     * @param documento
     * @param archivoResoluciones
     *
     * @return Tramite
     */
    public Tramite guardaDocumentoTramiteResoluciones(Tramite tramite,
        TramiteDocumento tramiteDocumento, Documento documento,
        UsuarioDTO usuario, File archivoResoluciones, ProductoCatastralJob job, int contador);

    /**
     * Metodo que realiza el la verificación del job para actualizar la colindancia de algunos
     * predios.
     *
     * @author jonathan.chacon
     * @param job
     * @param usuario
     */
    public void verificaJobActualizarColindancia(ProductoCatastralJob job, UsuarioDTO usuario);
    
    /**
     * Metodo que realiza el la verificación del job para actualizar la colindancia de algunos
     * predios.
     *
     * @author leidy.gonzalez
     * @param job
     * @param contador
     */
    public boolean generarResolucionesConservacionEnJasperServerYGuardar(ProductoCatastralJob job,
        int contador);

}
