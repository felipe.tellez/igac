/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla TRAMITE_TAREA
 *
 * @author felipe.cadena
 */
@Local
public interface ITramiteTareaDAO extends IGenericJpaDAO<TramiteTarea, Long> {

    /**
     * Método para obtener las tareas relacionadas a un tramite, si el parametro tarea es nulo se
     * obtendran todas las tareas del tramite, sino se obtendran las espeficadas en el parametro.
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @param tarea
     * @return
     */
    public List<TramiteTarea> obtenerTareasTramite(Long tramiteId, String tarea);

    /**
     * Metódo para obtener los tramites qu se encuantran el procesos geograficos actualmente
     *
     * @author felipe.cadena
     *
     * @return -- Retorna una lista de tareas con el tramite correspondiente.
     */
    public List<TramiteTarea> obtenerTramitesEnJobGeografico();

}
