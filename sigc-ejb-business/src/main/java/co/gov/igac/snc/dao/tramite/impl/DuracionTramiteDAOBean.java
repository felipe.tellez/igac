/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IDuracionTramiteDAO;
import co.gov.igac.snc.persistence.entity.tramite.DuracionTramite;
import co.gov.igac.snc.util.ETipoAvaluo;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class DuracionTramiteDAOBean extends GenericDAOWithJPA<DuracionTramite, Long>
    implements IDuracionTramiteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DuracionTramiteDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IDuracionTramiteDAO#getNormaDia(java.lang.String, java.lang.String, double)
     * @auhor pedro.garcia
     */
    @Implement
    @Override
    public Integer getNormaDia(String tipoAvaluo, String esSede, double area) {

        LOGGER.debug("on DuracionTramiteDAOBean#getNormaDia ...");

        Integer answer = null;
        String queryString;
        Query query;
        Date currentDate;
        boolean esRural = false;
        DuracionTramite duracionTramite;

        currentDate = new Date();
        queryString = "SELECT dt FROM DuracionTramite dt " +
            " WHERE dt.fechaDesde <= :currDateP ";

        if (tipoAvaluo.compareToIgnoreCase(ETipoAvaluo.RURAL.getCodigo()) == 0) {
            queryString += " AND dt.areaDesde <= :areaP AND dt.areaHasta >= :areaP " +
                " AND dt.tipoAvaluo = :tipoAvaluoP";
            esRural = true;
        } else {
            queryString += " AND dt.sede = :sedeP AND dt.tipoAvaluo != :tipoAvaluoP";

            //D: si es urbano, debe buscar tipos de avaluo diferente a '00'
            tipoAvaluo = ETipoAvaluo.RURAL.getCodigo();
        }

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("currDateP", currentDate);
            query.setParameter("tipoAvaluoP", tipoAvaluo);

            if (esRural) {
                query.setParameter("areaP", area);
            } else {
                query.setParameter("sedeP", esSede);
            }

            duracionTramite = (DuracionTramite) query.getSingleResult();
            if (duracionTramite != null) {
                answer = new Integer(duracionTramite.getNormaDia().intValue());
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;
    }

}
