package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionComisionDAOBean;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import java.util.List;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class PFichaMatrizDAOBean extends GenericDAOWithJPA<PFichaMatriz, Long>
    implements IPFichaMatrizDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionComisionDAOBean.class);

    /**
     * @see IPFichaMatrizDAO#findByPredioId(Long)
     * @author fabio.navarrete
     */
    @Override
    public PFichaMatriz findByPredioId(Long idPPredio) {
        try {
            String sql = "SELECT fm FROM PFichaMatriz fm" +
                " LEFT JOIN FETCH fm.PFichaMatrizTorres fmt" +
                " WHERE fm.PPredio.id = :idPPredio";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idPPredio", idPPredio);
            PFichaMatriz pfm = (PFichaMatriz) query.getSingleResult();
            Hibernate.initialize(pfm.getPFichaMatrizModelos());
            Hibernate.initialize(pfm.getPFichaMatrizTorres());
            Hibernate.initialize(pfm.getPFichaMatrizPredios());
            Hibernate.initialize(pfm.getPFichaMatrizPredioTerrenos());
            for (PFichaMatrizPredioTerreno pfmpt : pfm.getPFichaMatrizPredioTerrenos()) {
                Hibernate.initialize(pfmpt.getPredio());
                Hibernate.initialize(pfmpt.getPredio().getCirculoRegistral());
                Hibernate.initialize(pfmpt.getPredio().getPredioDireccions());
            }

            for (PFichaMatrizTorre pfmpt : pfm.getPFichaMatrizTorres()) {
                if (pfmpt.getProvieneTorre() != null) {
                    Hibernate.initialize(pfmpt.getProvieneTorre());
                    Hibernate.initialize(pfmpt.getProvieneTorre().getTorre());
                }

                if (pfmpt.getProvieneFichaMatriz() != null) {
                    Hibernate.initialize(pfmpt.getProvieneFichaMatriz());
                    if (pfmpt.getProvieneFichaMatriz().getPredio() != null) {
                        Hibernate.initialize(pfmpt.getProvieneFichaMatriz().getPredio());
                        Hibernate.initialize(pfmpt.getProvieneFichaMatriz().getPredio().
                            getNumeroPredial());
                    }
                }
            }

            for (PFichaMatrizPredio pfmp : pfm.getPFichaMatrizPredios()) {

                if (pfmp.getProvieneFichaMatriz() != null) {
                    Hibernate.initialize(pfmp.getProvieneFichaMatriz());
                    if (pfmp.getProvieneFichaMatriz().getPredio() != null) {
                        Hibernate.initialize(pfmp.getProvieneFichaMatriz().getPredio());
                        Hibernate.initialize(pfmp.getProvieneFichaMatriz().getPredio().
                            getNumeroPredial());
                    }
                }
            }

            return pfm;
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * @see IPFichaMatrizDAO#findByNumeroPredial(Long)
     * @author felipe.cadena
     */
    @Override
    public PFichaMatriz findByNumeroPredial(String numPredial) {
        try {
            String sql = "SELECT fm FROM PFichaMatriz fm" +
                " LEFT JOIN FETCH fm.PFichaMatrizTorres" +
                " WHERE fm.PPredio.numeroPredial like :numPredial";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("numPredial", numPredial + "%");
            PFichaMatriz pfm = (PFichaMatriz) query.getSingleResult();
            pfm.getPFichaMatrizModelos().size();
            pfm.getPFichaMatrizTorres().size();
            pfm.getPFichaMatrizPredios().size();

            for (PFichaMatrizPredioTerreno pfmpt : pfm.getPFichaMatrizPredioTerrenos()) {
                Hibernate.initialize(pfmpt.getPredio());
                Hibernate.initialize(pfmpt.getPredio().getCirculoRegistral());
                Hibernate.initialize(pfmpt.getPredio().getPredioDireccions());
            }

            return pfm;
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * @see IPFichaMatrizDAO#findByPredioId(Long)
     * @author felipe.cadena
     */
    @Override
    public List<PFichaMatriz> obtenerFichasEnProcesoDeCarga(String usuario) {

        List<PFichaMatriz> result = null;
        try {
            String sql = "SELECT fm FROM PFichaMatriz fm" +
                " WHERE fm.usuarioLog = :usuarioLog " +
                " AND (fm.PPredio.tramite.id = 0 OR fm.PPredio.tramite.id IS null)";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("usuarioLog", usuario);
            result = query.getResultList();

            for (PFichaMatriz pfm : result) {
                pfm.getPFichaMatrizModelos().size();
                pfm.getPFichaMatrizTorres().size();
                pfm.getPFichaMatrizPredios().size();
                pfm.getPPredio();

                Hibernate.initialize(pfm.getPPredio().getDepartamento());
                Hibernate.initialize(pfm.getPPredio().getMunicipio());
            }

            return result;
        } catch (NoResultException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PFichaMatrizDAOBean#obtenerFichasEnProcesoDeCarga");
        }
    }

    /**
     * @see IPFichaMatrizDAO#obtenerFichasAsociadasATramite(Long)
     * @author felipe.cadena
     */
    @Override
    public List<PFichaMatriz> obtenerFichasAsociadasATramite(Long tramiteId) {

        List<PFichaMatriz> result = null;
        try {
            String sql = "SELECT fm FROM PFichaMatriz fm" +
                " WHERE fm.PPredio.tramite.id = :tramiteId ";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("tramiteId", tramiteId);
            result = query.getResultList();

            for (PFichaMatriz pfm : result) {

                pfm.getPFichaMatrizPredios().size();
                pfm.getPPredio();
            }
            return result;
        } catch (NoResultException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PFichaMatrizDAOBean#obtenerFichasAsociadasATramite");
        }
    }

    /**
     * @see IPFichaMatrizDAO#obtenerFichaMatrizTramiteEV(Long)
     * @author leidy.gonzalez
     */
    @Override
    public PFichaMatriz obtenerFichaMatrizTramiteEV(String numeroPredial) {

        PFichaMatriz result = null;
        try {
            String sql = "SELECT fm FROM PFichaMatriz fm" +
                " WHERE fm.PPredio.numeroPredial like :numPredial";

            Query query = this.entityManager.createQuery(sql);
            query.setParameter("numeroPredial", numeroPredial);
            result = (PFichaMatriz) query.getSingleResult();

            return result;
        } catch (NoResultException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PFichaMatrizDAOBean#obtenerFichaMatrizTramiteEV");
        }
    }

}
