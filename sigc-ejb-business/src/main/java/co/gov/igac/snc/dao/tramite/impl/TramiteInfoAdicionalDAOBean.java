package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteInfoAdicionalDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInfoAdicional;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Proyecto SNC 2017
 */
/**
 * Implmentacion operaciones de DB paa la clase {@link TramiteInfoAdicional}
 *
 * @author felipe.cadena
 */
@Stateless
public class TramiteInfoAdicionalDAOBean extends GenericDAOWithJPA<TramiteInfoAdicional, Long>
    implements ITramiteInfoAdicionalDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteInfoAdicionalDAOBean.class);

}
