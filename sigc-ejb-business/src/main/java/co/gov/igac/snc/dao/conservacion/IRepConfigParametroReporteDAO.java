/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface para los metodos de acceso a datos de la entidad RepConfigParametroReporte.
 *
 * @author felipe.cadena
 */
@Local
public interface IRepConfigParametroReporteDAO extends
    IGenericJpaDAO<RepConfigParametroReporte, Long> {

    /**
     * Obtiene la configuracion para una categoria en especial
     *
     * @author felipe.cadena
     * @param categoria
     * @return
     */
    public List<RepConfigParametroReporte> obtenerConfiguracionPorCategoria(String categoria);

    /**
     * Obtiene la configuracion para una categoria en especial
     *
     * @author leidy.gonzalez
     * @param idReporte
     * @return
     */
    public List<RepConfigParametroReporte> consultaConfiguracionReportePredial(Long idReporte);

}
