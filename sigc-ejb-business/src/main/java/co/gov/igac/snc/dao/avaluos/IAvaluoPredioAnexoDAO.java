package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioAnexo;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoPredioAnexo
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IAvaluoPredioAnexoDAO extends IGenericJpaDAO<AvaluoPredioAnexo, Long> {

    /**
     * Hace la consulta por el id del Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioAnexo> consultarPorAvaluo(long avaluoId);

    /**
     * Método para determinar si existen anexos asociados a un avalúo o a los predios del avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public Boolean calcularAnexosPreviosAvaluo(Long idAvaluo);

    /**
     * Metodo para eliminar los anexos relacionados a un avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public boolean eliminarAnexosPreviosAvaluo(Long idAvaluo);

}
