package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionComisionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionComision;
import co.gov.igac.snc.persistence.entity.actualizacion.ComisionIntegrante;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionComision.
 *
 * @author franz.gamba
 */
@Stateless
public class ActualizacionComisionDAOBean extends GenericDAOWithJPA<ActualizacionComision, Long>
    implements IActualizacionComisionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionComisionDAOBean.class);

    /**
     * @author franz.gamba
     * @see IActualizacionComisionDAO#getActualizacionComisionByActualizacionId(Long, String)
     */
    @Override
    public ActualizacionComision getActualizacionComisionByActualizacionId(
        Long actualizacionId, String objeto) {

        ActualizacionComision answer = null;

        String query = "SELECT ac FROM ActualizacionComision ac " +
            " JOIN ac.actualizacion a " +
            " LEFT JOIN FETCH ac.comisionIntegrantes ci " +
            " WHERE a.id = :actualizacionId " +
            " AND ac.objeto = :objeto";
        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("actualizacionId", actualizacionId);
            q.setParameter("objeto", objeto);

            List<ActualizacionComision> comisiones = (List<ActualizacionComision>) q.getResultList();
            answer = comisiones.get(0);

            if (answer != null) {
                if (answer.getComisionIntegrantes() != null &&
                    !answer.getComisionIntegrantes().isEmpty()) {
                    for (ComisionIntegrante ci : answer.getComisionIntegrantes()) {
                        ci.getRecursoHumano().getNombre();
                    }
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "ActualizacionComisionDAOBean#getActualizacionComisionByActualizacionId");
        }

        return answer;
    }

}
