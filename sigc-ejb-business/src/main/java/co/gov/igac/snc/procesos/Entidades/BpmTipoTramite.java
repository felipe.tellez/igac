/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_TIPO_TRAMITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmTipoTramite.findAll", query = "SELECT b FROM BpmTipoTramite b"),
    @NamedQuery(name = "BpmTipoTramite.findById", query = "SELECT b FROM BpmTipoTramite b WHERE b.id = :id"),
    @NamedQuery(name = "BpmTipoTramite.findByTramite", query = "SELECT b FROM BpmTipoTramite b WHERE b.tramite = :tramite"),
    @NamedQuery(name = "BpmTipoTramite.findByDescripcion", query = "SELECT b FROM BpmTipoTramite b WHERE b.descripcion = :descripcion")})
public class BpmTipoTramite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "TRAMITE")
    private String tramite;
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public BpmTipoTramite() {
    }

    public BpmTipoTramite(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTramite() {
        return tramite;
    }

    public void setTramite(String tramite) {
        this.tramite = tramite;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmTipoTramite)) {
            return false;
        }
        BpmTipoTramite other = (BpmTipoTramite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmTipoTramite[ id=" + id + " ]";
    }
    
}
