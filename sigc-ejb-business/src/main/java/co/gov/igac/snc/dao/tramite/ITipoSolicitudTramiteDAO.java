/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla TIPO_TRAMITE_SOLICITUD
 *
 * @author pedro.garcia
 */
@Local
public interface ITipoSolicitudTramiteDAO extends IGenericJpaDAO<TipoSolicitudTramite, Long> {

    /**
     * Retorna la lista objetos según el tipo de solicitud
     *
     * @author pedro.garcia
     * @param tipoSolicitud
     * @return
     */
    public List<TipoSolicitudTramite> findByTipoSolicitud(String tipoSolicitud);

}
