/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de un AvisoRegistroRechazo
 *
 * @author pedro.garcia
 */
@Local
public interface IAvisoRegistroRechazoDAO extends IGenericJpaDAO<AvisoRegistroRechazo, Long> {

    public List<AvisoRegistroRechazo> findByNumeroSolicitud(String numeroSolicitud);

    /**
     * Retorna una lista de AvisoRegistroRechazo a partir de la llave foranea SolicitudAvisoRegistro
     *
     * @param solicitudAvisoRegistroId
     * @return
     * @author fabio.navarrete
     */
    public List<AvisoRegistroRechazo> findBySolicitudAvisoRegistroId(Long solicitudAvisoRegistroId);
}
