/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteDigitalizacionDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase para las operaciones en DB de la entidad TramiteDigitalizacion
 *
 * @author felipe.cadena
 */
@Stateless
public class TramiteDigitalizacionDAOBean extends GenericDAOWithJPA<TramiteDigitalizacion, Long>
    implements ITramiteDigitalizacionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDigitalizacionDAOBean.class);

    /**
     * @see ITramiteDigitalizacionDAO#buscarPorIdTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDigitalizacion> buscarPorIdTramite(Long idTramite) {

        List<TramiteDigitalizacion> resultado = null;
        String queryString;
        Query query;

        queryString = "SELECT td FROM TramiteDigitalizacion td " +
            "JOIN td.tramite t WHERE t.id = :idTramite";

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idTramite", idTramite);

            resultado = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex, "TramiteDigitalizacion");
        }

        return resultado;
    }

    /**
     * @see ITramiteDigitalizacionDAO#buscarPorDigitalizador
     * @author felipe.cadena
     */
    @Implement
    @Override
    public List<TramiteDigitalizacion> buscarPorDigitalizador(String digitalizador) {

        try {
            List<TramiteDigitalizacion> resultado;

            String query = "SELECT td FROM TramiteDigitalizacion td " +
                "where td.usuarioDigitaliza " +
                "like :digitalizador " +
                " and td.fechaDigitalizacion is null";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("digitalizador", digitalizador);

            resultado = q.getResultList();
            return resultado;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, e, "TramiteDigitalizacion");
        }
    }

    /**
     * @see ITramiteDigitalizacionDAO#buscarPorDigitalizador
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDigitalizacion buscarPorDigitalizadorYTramite(String digitalizador, Long idTramite) {

        try {
            TramiteDigitalizacion resultado;

            String query = "SELECT td FROM TramiteDigitalizacion td " +
                "where td.usuarioDigitaliza " +
                "like :digitalizador " +
                " and td.tramite.id =:idtramite" +
                " and td.fechaDigitalizacion is null";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("digitalizador", digitalizador);
            q.setParameter("idtramite", idTramite);

            resultado = (TramiteDigitalizacion) q.getSingleResult();
            return resultado;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, e, "TramiteDigitalizacion");
        }
    }

    /**
     * @see ITramiteDigitalizacionDAO#buscarPorEjecutorYTramite
     * @author felipe.cadena
     */
    @Implement
    @Override
    public TramiteDigitalizacion buscarPorEjecutorYTramite(String ejecutor, Long idTramite) {

        try {
            TramiteDigitalizacion resultado;

            String query = "SELECT MAX(td.id) FROM TramiteDigitalizacion td " +
                "where td.usuarioEnvia " +
                "like :ejecutor " +
                " and td.tramite.id =:idtramite";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("ejecutor", ejecutor);
            q.setParameter("idtramite", idTramite);

            Long idLast = (Long) q.getSingleResult();
            if (idLast == null) {
                return null;
            }
            resultado = this.findById(idLast);
            return resultado;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, e, "TramiteDigitalizacion");
        }
    }

}
