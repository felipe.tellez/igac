package co.gov.igac.snc.dao.avaluos;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;

/**
 * Interface que contiene metodos de consulta sobre entity Avaluo
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IAvaluoDAO extends IGenericJpaDAO<Avaluo, Long> {

    /**
     * Método que consulta la lista de Sec. Radicados asociadas a una solicitud de avaluo de un
     * solicitante que se va a revisar o impugnar
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitante - Id del Solicitante
     * @param banderaParaRevision - Bandera que indica si la solicitud de avaluo es para revisar o
     * impugnar<br/>
     * <b>true:</b> La solicitud de avaluo es para revisar<br/>
     * <b>false:</b> La solicitud de avaluo es para impugnar<br/>
     * @return Lista de Sec. Radicados asociados
     */
    public List<String> consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(
        Long idSolicitante, boolean banderaParaRevision);

    /**
     * Método que consulta la lista de Sec. Radicados asociados a una solicitud de cotizacion de un
     * solicitante, para asociar el Sec.Radicado seleccionado, a una solicitud de avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitante - Id del Solicitante
     * @return Lista de Sec. Radicados asociados
     */
    public List<String> consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(
        Long idSolicitante);

    /**
     * Método para obterner un avaluo correspondiente a un número de sec_radicado determinado.
     *
     * @author felipe.cadena
     *
     * @param secRadicado - numero de sec_radicado
     * @return Avaluo relacionado al número del sec_radicado
     */
    public Avaluo buscarAvaluoPorSecRadicado(String secRadicado);

    /**
     * Método que consulta los avalúos asociados a un trámite. Trae el trámite y los predios
     * asociados al avaluo
     *
     * @author christian.rodriguez
     * @param idSolicitud Identificador de la solicitud
     * @return lista con los avalúos asociados a la solicitud
     */
    public List<Avaluo> buscarAvaluosPorSolicitudId(Long idSolicitud);

    /**
     * Método que busca el siguiente digito de la secuencia para una solicitud existente
     *
     * @author christian.rodriguez
     * @param solicitud Solicitud a la cual se le quiere generar el siguiente sec radicado
     * @param contarConProcesoId Bandera que determina si se debe obtener el siguiente digito
     * teniendo en cuenta únicamente los avaluos avanzados en el process (con process id)
     *
     * @return siguiente digito del sec radicado
     */
    public Long obtenerSiguienteDigitoSecRadicadoPorSolicitud(Solicitud solicitud,
        boolean contarConProcesoId);

    /**
     * Método que genera el siguiente sec radicado para una solicitud existente
     *
     * @author christian.rodriguez
     * @param solicitud Solicitud a la cual se le quiere generar el siguiente sec radicado
     * @param contarConProcesoId Bandera que determina si se debe obtener el siguiente digito
     * teniendo en cuenta únicamente los avaluos avanzados en el process (con process id)
     * @return siguiente sec radicaod (Compuesto del número de solicitud concatenado con un guion y
     * un número secuencial)
     */
    public String obtenerSiguienteSecRadicadoPorSolicitud(Solicitud solicitud,
        boolean contarConProcesoId);

    /**
     * Método que busca una avalúo por id y lo trae con los avaluo predios
     *
     * @author christian.rodriguez
     * @param idAvaluo id del avaluo a buscar
     * @return
     */
    public Avaluo buscarAvaluoPorIdConAvaluoPredio(Long idAvaluo);

    /**
     * Obtiene el Avaluo dado su id, haciendo fetch del trámite relacionado, de la solicitud, y de
     * los solicitantes (que en el caso de un avalúo es uno solo)
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return null si no lo encuentra
     */
    public Avaluo getInfoHeaderAvaluoById(Long avaluoId);

    /**
     * Devuelve una lista de avaluos filtrada por los datos del parametro filtro
     *
     * @author felipe.cadena
     *
     * @param filtro
     * @return - Lista de avalúos con Fetch de todos los datos relacionados (Tramite, Solicitud,
     * Predios, Avaluadores, Solicitantes)
     */
    public List<Avaluo> buscarAvaluosPorFiltro(FiltroDatosConsultaAvaluo filtro);

    /**
     *
     * Busca los avalúos que estén relacionados con las solicitudes cuyos ids vienen como parámetro
     * y que cumplan con los criterios de búsqueda dados por el filtro Hace fetch de los predios
     * relacionados con el avalúo. Hace fetch de los avaluadores relacionados con el avalúo. Hace
     * fetch de la liquidación relacionada con el avalúo.
     *
     * OJO: no se ha hecho la consulta completa para todos los posibles filtros
     *
     * @author pedro.garcia
     * @param idsSolicitudes
     * @param filtroConsulta Filtro para la consulta
     * @param cargarObjetosExtra bandera que indica si se debe hacer el fetch de lso objetos
     * asociados
     * @return
     */
    public List<Avaluo> buscarPorSolicitudes(List<Long> idsSolicitudes,
        FiltroDatosConsultaAvaluo filtroConsulta, boolean cargarObjetosExtra);

    /**
     * Consulta los avaluos relacionados a un avaluador(ProfesionalAvaluos) que esten en proceso y
     * no aprobados.
     *
     * @author felipe.cadena
     * @param idAvaluador
     * @return
     */
    public List<Avaluo> consultarAvaluosEnProcesoAvaluador(Long idAvaluador);

    /**
     * Método para consultar los avalúos relacionados a un contrato de profesional de avalúos. Si
     * tipoActividad tiene valor null no se toma en cuenta este parametro para la consulta
     *
     * @author felipe.cadena
     * @param idContrato
     * @param tipoActividad - Actividad por la cual esta relacionado el profesional al avalúo
     * @param estadoTramite
     * @return
     */
    public List<Avaluo> consultarAvaluosRealizadosContratoPA(Long idContrato,
        String tipoActividad, String estadoTramite);

    /**
     * Método para consultar los avalúos relacionados a un profesional de avalúos en un año
     * determinado.
     *
     * @author felipe.cadena
     * @param idContrato
     * @param tipoActividad - Actividad por la cual esta relacionado el profesional al avalúo
     * @param estadoTramite
     * @param anio - año de la vigencia que se desea consultar
     * @return
     */
    public List<Avaluo> consultarAvaluosProfesionalPorAnio(Long idAvaluador,
        String tipoActividad, String estadoTramite, Integer anio);

    /**
     * Método para consultar el historico de avalúos realizados por un profesional agrupados por
     * años
     *
     * @param idAvaluador
     * @return - Una lista
     */
    public List<Object[]> consultarHistoricoAvaluos(Long idAvaluador);

    /**
     * Consulta los avalúos asociados a un {@link ProfesionalAvaluosContrato} y a un
     * {@link ProfesionalAvaluo} que no esten asociados a ningún {@link ProfesionalContratoPago}
     *
     * @author christian.rodriguez
     * @param idContrato id del {@link ProfesionalAvaluosContrato}
     * @param idAvaluador id del avaluador {@link ProfesionalAvaluo}
     * @return Lista con los {@link Avaluo} que cumplan las condiciones dadas
     */
    public List<Avaluo> consultarSinPagoPorAvaluadorYContrato(Long idContrato, Long idAvaluador);

    /**
     * método que consulta los avaluos que están asociados a un pago de un profesional
     * {@link ProfesionalContratoPago}
     *
     * @author christian.rodriguez
     * @param idContratoPago id del pago {@link ProfesionalContratoPago}
     * @return Lista con los {@link Avaluo} asociados al pago
     */
    public List<Avaluo> consultarPorProfesionalContratoPagoId(
        Long idContratoPago);

    /**
     * Obtiene los avalúos que tienen relacionados los trámites cuyos ids vienen en la lista. Se
     * cargan los avaluadores y los predios de cada avalúo.}
     *
     * @author pedro.garcia
     * @param idsTramites Lista de ids de trámites
     * @return
     */
    public List<Avaluo> obtenerPorIdsTramite(List<Long> idsTramites);

    /**
     * Método para consultar avalúos relacionados a un profesional y un sec_radicado que tengan
     * asignación y aun no tengan orden de practica.
     *
     * @author felipe.cadena
     * @param idProfesional
     * @param secRadicado
     * @return
     */
    public List<Avaluo> consultarAvaluosAsignacion(Long idProfesional, String secRadicado);

    /**
     * Método que obtiene un {@link Avaluo} por su id. Carga los siguientes objetos asociados
     * <ul>
     * <li>{@link Tramite}</li>
     * <li>{@link Solicitud} del tramite</li>
     * <li>{@link SolicitanteSolicitud} de la solicitud</li>
     * <li>{@link AvaluoPredio}</li>
     * <li>{@link Departamento} de los predios</li>
     * <li>{@link Municipio} de los predios</li>
     * <li>{@link AvaluoAsignacionProfesional}</li>
     * <li>{@link ProfesionalAvaluo} de las asignaciones</li>
     * </ul>
     * Asi
     *
     * @author christian.rodriguez
     * @param idAvaluo identificador del avaluo
     * @return {@link Avaluo} con los objetos asociados cargados
     */
    public Avaluo obtenerPorIdConAtributos(Long idAvaluo);

    /**
     * Método que obtiene un {@link Avaluo} por su secRadicado. Carga los siguientes objetos
     * asociados
     * <ul>
     * <li>{@link Tramite}</li>
     * <li>{@link Solicitud} del tramite</li>
     * <li>{@link SolicitanteSolicitud} de la solicitud</li>
     * <li>{@link AvaluoPredio}</li>
     * <li>{@link Departamento} de los predios</li>
     * <li>{@link Municipio} de los predios</li>
     * <li>{@link AvaluoAsignacionProfesional}</li>
     * <li>{@link ProfesionalAvaluo} de las asignaciones</li>
     * </ul>
     * Asi
     *
     * @author christian.rodriguez
     * @param secRadicado sec radicado del avaluo
     * @return {@link Avaluo} con los objetos asociados cargados
     */
    public Avaluo consultarPorSecRadicadoConAtributos(String secRadicado);

    /**
     * Busca los avalúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites. Hace fetch del trámite, solicitantes del trámite, predios, avaluadores y
     * el contrato activo de cada avaluador.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-009
     *
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Avaluo> obtenerParaOrdenPractica(long[] idsTramites, String sortField,
        String sortOrder, Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Obtiene un Avaluo dado su id, haciendo fetch del trámite asociado y la solicitud asociada al
     * trámite.
     *
     * @author pedro.garcia
     * @param idAvaluo
     * @return
     */
    public Avaluo obtenerPorIdConTramiteYSolicitud(Long idAvaluo);

    /**
     * Consulta el oficio de no aprobación del avalúo que tenga el id especificado
     *
     * @author christian.rodriguez
     * @param idAvaluo id del avaluo
     * @param idTipoDocumento clase del documento a buscar debe ser el id de un valor de la
     * enumeración {@link ETipoDocumento}
     * @return {@link Documento} oficio de no aprobación o null si no se encuentra
     */
    public Documento consultarDocumentoPorIdAvaluoYTipoDocumento(
        Long idAvaluo, Long idTipoDocumento);

    /**
     * Consulta los avalúos asociados a una orden de practica y a un profesional de avalúos
     *
     * @author christian.rodriguez
     * @param profesionalId identificador dle profesional de avaluos
     * @param ordenPracticaId identificador de la orden de practica
     * @return Lista con los {@link Avaluo} o null si no se encontraron resultados
     */
    public List<Avaluo> consultarPorOrdenPracticaIdYProfesionalId(
        Long profesionalId, Long ordenPracticaId);

    /**
     * Consulta los avalúos asociados a una orden de practica
     *
     * @author christian.rodriguez
     * @param ordenPracticaId identificador de la orden de practica
     * @return Lista con los {@link Avaluo} o null si no se encontraron resultados
     */
    public List<Avaluo> consultarPorOrdenPracticaId(Long ordenPracticaId);

    /**
     * Busca los avalúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites. Descarta los avalúos para los que existe una asignación vigente para la
     * actividad para la que se va a hacer la asignación.
     *
     * Hace fetch de los solicitantes.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-003
     *
     * @param idsTramites arreglo con los id de trámite
     * @param actividadCodigo código de la actividad para la que se va a asignar
     * @param asignados determina si se buscan los que están sin asignación o ya asignados
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Avaluo> obtenerParaAsignacion(long[] idsTramites, String actividadCodigo,
        boolean asignados, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Cuenta los avalúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites.
     *
     * Si 'asignados' es false: descarta los avalúos para los que existe una asignación vigente para
     * la actividad para la que se va a hacer la asignación. Si 'asognados' es true: Tiene en cuenta
     * los que aparecen en la actividad de asignación respectiva del proceso, ya se asignaron, pero
     * aún no se mueven a la siguiente actividad; es decir, tiene en cuenta los avalúos para los que
     * existe una asignación vigente para la actividad para la que se va a hacer la asignación.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-003
     *
     * @param idsTramites arreglo con los id de trámite
     * @param actividadCodigo código de la actividad para la que se va a asignar
     * @param asignados determina si se buscan los que están sin asignación o ya asignados
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public int contarParaAsignacion(long[] idsTramites, String actividadCodigo, boolean asignados,
        int... contadoresDesdeYCuantos);

    /**
     * Método que consulta el avaluo al que se le está haciendo revisión o corrección, se usa cuando
     * se esta trabajando con solicitudes de estos tipos
     *
     * @author christian.rodriguez
     * @param avaluoId identificador del {@link Avaluo} al que se le quiere hayar el {@link Avaluo}
     * de revisión o impugnacion
     * @return avaluo que se está impugnando o revisando, null si no se encuentra o si ocurrio algún
     * error
     */
    public Avaluo obtenerPorIdParaImpugnacionRevision(Long avaluoId);

    /**
     * Método que retorna la lista de avaluos de un avaluador con sus respectivos movimientos
     *
     * @author rodrigo.hernandez
     *
     * @param filtro - filtro de datos
     * @return
     */
    public List<Avaluo> obtenerAvaluosConMovimientosPorFiltro(
        FiltroDatosConsultaAvaluo filtro);

}
