package co.gov.igac.snc.dao.tramite;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import java.util.HashSet;

import java.util.List;

@Local
public interface ITramiteEstadoDAO extends IGenericJpaDAO<TramiteEstado, Long> {

    /**
     * Método que actualiza el estado de un trámite, y retorna el trámite estado actualizado
     *
     * @author juan.agudelo
     * @param tramiteEstadoActual
     * @param usuario
     * @return
     */
    public TramiteEstado actualizarTramiteEstado(TramiteEstado tramiteEstadoActual,
        UsuarioDTO usuario);

    /**
     * Método que busca el trámite estado vigente por el id del trámite
     *
     * @author javier.aponte
     * @param idTramite
     * @param estadoTramite
     * @return
     */
    public TramiteEstado obtenerTramiteEstadoVigentePorTramiteId(
        Long idTramite);

    /**
     * Método que permite crear y actualizar un nuevo trámites estado, se encuentra funcionanado
     * para radicar solicitud
     *
     * @author juan.agudelo
     * @param tramiteEstadoActual
     * @param usuario
     * @return
     */
    public Boolean crearActualizarTramiteEstado(
        TramiteEstado tramiteEstadoActual, UsuarioDTO usuario);

    /**
     * Método que actualiza un objeto TramiteEstado si su estado futuro es diferente al actual, y
     * retorna el objeto actualizado. Si el estado ingresado es el mismo al actual retorna el
     * TramiteEstado correspondiente al trámite ingresado. Si el estado Futuro es diferente, cambia
     * la fecha de finalización del objeto preexistente y crea un nuevo TramiteEstado con los datos
     * ingresados
     *
     * @param tramite
     * @param motivo
     * @param tramiteEstadoFuturo
     * @param responsableFuturo
     * @param usuario
     * @author fabio.navarrete
     * @return
     */
    public TramiteEstado cambiarTramiteEstado(Tramite tramite,
        String motivo, ETramiteEstado tramiteEstadoFuturo,
        String responsableFuturo, UsuarioDTO usuario);

    /**
     * @author fabio.navarrete Retorna la lista de TrámiteEstado partiendo de un id de trámite
     * @param id
     * @return
     */
    public List<TramiteEstado> findByTramiteId(Long id);

    /**
     * Retorna la lista de TramiteEstado correspondiente al histórico de observaciones de un predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<TramiteEstado> buscarHistoricoObservacionesPorTramiteId(
        Long tramiteId);

    /**
     * Obtiene la observación hecha de manera temporal antes de avanzar en proceso solo se utiliza
     * para mantener estado en el caso de uso mientras se confirma el rechazo del tramite.
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public TramiteEstado buscarObservacionTemporalPorTramiteId(Long tramiteId);

    /**
     * Validar que el trámite asociado haya sido cancelado en algún momento, para validar si en la
     * tarea de “Aprobar nuevos trámites” deje modificar el estado (o actividad asociada) del predio
     * existente.
     *
     * @author juan.cruz
     * @param tramitesID
     * @return
     */
    public HashSet<Long> validarSiEstadoFuePorCancelarTramite(List<Long> tramitesID);
}
