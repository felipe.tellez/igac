/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface IProfesionDAO extends IGenericJpaDAO<Profesion, Long> {

}
