/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos.Entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author michael.pena
 */
@Entity
@Table(name = "BPM_FLUJO_TRAMITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpmFlujoTramite.findAll", query = "SELECT b FROM BpmFlujoTramite b"),
    @NamedQuery(name = "BpmFlujoTramite.findById", query = "SELECT b FROM BpmFlujoTramite b WHERE b.id = :id"),
    @NamedQuery(name = "BpmFlujoTramite.findByFechaInicio", query = "SELECT b FROM BpmFlujoTramite b WHERE b.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "BpmFlujoTramite.findByFechaFinal", query = "SELECT b FROM BpmFlujoTramite b WHERE b.fechaFinal = :fechaFinal"),
    @NamedQuery(name = "BpmFlujoTramite.findByProcesoInstanciaId", query = "SELECT b FROM BpmFlujoTramite b WHERE b.procesoInstanciaId = :procesoInstanciaId"),
    @NamedQuery(name = "BpmFlujoTramite.findByActividadId", query = "SELECT b FROM BpmFlujoTramite b WHERE b.actividadId = :actividadId"),
    @NamedQuery(name = "BpmFlujoTramite.findByTipoTramiteId", query = "SELECT b FROM BpmFlujoTramite b WHERE b.tipoTramiteId = :tipoTramiteId"),
    @NamedQuery(name = "BpmFlujoTramite.findByIdEstado", query = "SELECT b FROM BpmFlujoTramite b WHERE b.idEstado = :idEstado"),
    @NamedQuery(name = "BpmFlujoTramite.findByUsuarioResponsable", query = "SELECT b FROM BpmFlujoTramite b WHERE b.usuarioResponsable = :usuarioResponsable"),
    @NamedQuery(name = "BpmFlujoTramite.findByTerritorial", query = "SELECT b FROM BpmFlujoTramite b WHERE b.territorial = :territorial"),
    @NamedQuery(name = "BpmFlujoTramite.findByUoc", query = "SELECT b FROM BpmFlujoTramite b WHERE b.uoc = :uoc"),
    @NamedQuery(name = "BpmFlujoTramite.findByTramitePadre", query = "SELECT b FROM BpmFlujoTramite b WHERE b.tramitePadre = :tramitePadre"),
    @NamedQuery(name = "BpmFlujoTramite.findByCodTerritorial", query = "SELECT b FROM BpmFlujoTramite b WHERE b.codTerritorial = :codTerritorial"),
    @NamedQuery(name = "BpmFlujoTramite.findByCodUoc", query = "SELECT b FROM BpmFlujoTramite b WHERE b.codUoc = :codUoc"),
    @NamedQuery(name = "BpmFlujoTramite.findByIdTaskProc", query = "SELECT b FROM BpmFlujoTramite b WHERE b.idTaskProc = :idTaskProc"),
    @NamedQuery(name = "BpmFlujoTramite.findByUsuarioCreador", query = "SELECT b FROM BpmFlujoTramite b WHERE b.usuarioCreador = :usuarioCreador"),
    @NamedQuery(name = "BpmFlujoTramite.findByFechaSuspencion", query = "SELECT b FROM BpmFlujoTramite b WHERE b.fechaSuspencion = :fechaSuspencion"),
    @NamedQuery(name = "BpmFlujoTramite.findByFechaReanudacion", query = "SELECT b FROM BpmFlujoTramite b WHERE b.fechaReanudacion = :fechaReanudacion"),
    @NamedQuery(name = "BpmFlujoTramite.findByFechaVencimiento", query = "SELECT b FROM BpmFlujoTramite b WHERE b.fechaVencimiento = :fechaVencimiento"),
    @NamedQuery(name = "BpmFlujoTramite.findByFechaCreacion", query = "SELECT b FROM BpmFlujoTramite b WHERE b.fechaCreacion = :fechaCreacion")})
//    @NamedQuery(name = "BpmFlujoTramite.insertInto", query = "INSERT INTO BpmFlujoTramite  (fechaInicio, procesoInstanciaId, actividadId "
//            + "tipoTramiteId, idEstado, usuarioResponsable, territorial, uoc, tramitePadre, codTerritorial, codUoc) VALUES (:fechaInicio,:procesoInstanciaId,"
//            + ":actividadId,:tipoTramiteId,:idEstado,:usuarioResponsable,"
//            + " :territorial,:uoc,:tramitePadre,:codTerritorial,:codUoc)")})

public class BpmFlujoTramite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;
    @Column(name = "PROCESO_INSTANCIA_ID")
    private Long procesoInstanciaId;
    @Column(name = "ACTIVIDAD_ID")
    private Long actividadId;
    @Column(name = "TIPO_TRAMITE_ID")
    private Long tipoTramiteId;
    @Column(name = "ID_ESTADO")
    private Long idEstado;
    @Column(name = "USUARIO_RESPONSABLE")
    private String usuarioResponsable;
    @Column(name = "TERRITORIAL")
    private String territorial;
    @Column(name = "UOC")
    private String uoc;
    @Column(name = "TRAMITE_PADRE")
    private Long tramitePadre;
    @Column(name = "COD_TERRITORIAL")
    private Integer codTerritorial;
    @Column(name = "COD_UOC")
    private Integer codUoc;
    @Column(name = "ID_TASK_PROC")
    private String idTaskProc;
    @Column(name = "USUARIO_CREADOR")
    private String usuarioCreador;
    @Column(name = "FECHA_SUSPENCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSuspencion;
    @Column(name = "FECHA_REANUDACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReanudacion;
    @Column(name = "FECHA_VENCIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
//    @ManyToOne
//    @JoinColumn(name = "ACTIVIDAD_ID", referencedColumnName = "ID")
//    private BpmActividad bpmActividad;
    

    public BpmFlujoTramite() {
    }

    public BpmFlujoTramite(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Long getProcesoInstanciaId() {
        return procesoInstanciaId;
    }

    public void setProcesoInstanciaId(Long procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    public Long getActividadId() {
        return actividadId;
    }

    public void setActividadId(Long actividadId) {
        this.actividadId = actividadId;
    }

    public Long getTipoTramiteId() {
        return tipoTramiteId;
    }

    public void setTipoTramiteId(Long tipoTramiteId) {
        this.tipoTramiteId = tipoTramiteId;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public String getUsuarioResponsable() {
        return usuarioResponsable;
    }

    public void setUsuarioResponsable(String usuarioResponsable) {
        this.usuarioResponsable = usuarioResponsable;
    }

    public String getTerritorial() {
        return territorial;
    }

    public void setTerritorial(String territorial) {
        this.territorial = territorial;
    }

    public String getUoc() {
        return uoc;
    }

    public void setUoc(String uoc) {
        this.uoc = uoc;
    }

    public Long getTramitePadre() {
        return tramitePadre;
    }

    public void setTramitePadre(Long tramitePadre) {
        this.tramitePadre = tramitePadre;
    }

    public Integer getCodTerritorial() {
        return codTerritorial;
    }

    public void setCodTerritorial(Integer codTerritorial) {
        this.codTerritorial = codTerritorial;
    }

    public Integer getCodUoc() {
        return codUoc;
    }

    public void setCodUoc(Integer codUoc) {
        this.codUoc = codUoc;
    }

    public String getIdTaskProc() {
        return idTaskProc;
    }

    public void setIdTaskProc(String idTaskProc) {
        this.idTaskProc = idTaskProc;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaSuspencion() {
        return fechaSuspencion;
    }

    public void setFechaSuspencion(Date fechaSuspencion) {
        this.fechaSuspencion = fechaSuspencion;
    }

    public Date getFechaReanudacion() {
        return fechaReanudacion;
    }

    public void setFechaReanudacion(Date fechaReanudacion) {
        this.fechaReanudacion = fechaReanudacion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
//    public BpmActividad getBpmActividad() {
//        return bpmActividad;
//    }
//
//    public void setBpmActividad(BpmActividad bpmActividad) {
//        this.bpmActividad = bpmActividad;
//    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpmFlujoTramite)) {
            return false;
        }
        BpmFlujoTramite other = (BpmFlujoTramite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.igac.snc.procesos.Entidades.BpmFlujoTramite[ id=" + id + " ]";
    }

    public void setId(int idObjeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
