package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;

@Local
public interface ITramiteRequisitoDAO extends IGenericJpaDAO<TramiteRequisito, Long> {

    /**
     * Método que obtiene una lista de documentos requeridos para un trámite con base en las
     * carateristicas especificsa del trámite
     *
     * @author juan.agudelo
     * @param tramite
     * @return
     */
    public List<TramiteRequisito> conseguirDocumentacionTramiteRequerida(
        Tramite tramite);

}
