/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;

/**
 * Interfaz para los métodos de BD de los entities PUnidadConstruccion
 *
 * @author pedro.garcia
 */
@Local
public interface IPUnidadConstruccionDAO extends IGenericJpaDAO<PUnidadConstruccion, Long> {

    public IModeloUnidadConstruccion findPUnidadConstruccionFetchUsoConstruccionById(Long id);

    // ----------------------------------------------------------------- //
    /**
     * Metodo que busca una {@link PUnidadConstruccion} por su id.
     *
     * @author david.cifuentes
     * @param idPUnidadConstruccion
     */
    public PUnidadConstruccion buscarUnidadDeConstruccionPorSuId(Long idPUnidadConstruccion);

    /**
     * Método que retorna las PUnidadConstruccions por el tramiteId asociado
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<PUnidadConstruccion> getAllPunidadConstruccionsByTramiteId(Long tramiteId);

    /**
     * Método que consulta todas las {@link PUnidadConstruccion} de un {@link PPredio}.
     *
     * @author david.cifuentes
     * @modifiedBy fredy.wilches
     * @param idPPredio
     * @param incluirCancelados
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionPorPPredioId(Long idPPredio,
        boolean incluirCancelados);

    /**
     * Método que retorna las p unidades de construccion asociadas a un predio por su numero predial
     *
     * @author franz.gamba
     * @param numPred
     * @return
     */
    public List<PUnidadConstruccion> getPUnidadsConstruccionByNumeroPredial(String numPred);

    /**
     * Método que busca las unidades de construcción que han sido generadas a partir de la
     * asociación de la unidad de construcción y el trámite enviados como parámetros.
     *
     * @author david.cifuentes
     * @param idTramite
     * @param unidadConstruccionPredioOriginal
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(
        Long idTramite, UnidadConstruccion unidadConstruccionPredioOriginal);

    /**
     * Método que busca las unidades de construcción que han sido generadas a partir del trámite y
     * el numero Ficha Matriz enviados como parámetros.
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @param numeroFichaMatriz
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidConstPorTramiteYDifNumFichaMatrizEV(
        Long idTramite, String numeroFichaMatriz);

    /**
     * Método que retorna las p unidades de construccion asociadas a un predio por su id de predio
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadsConstruccionPorIdPredio(
        Long idPred);

    /**
     * Método que retorna las p unidades de construccion CONVENCIONALES asociadas a un predio por su
     * id de predio
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstConvPorIdPredio(
        Long idPred);

    /**
     * Método que retorna las p unidades de construccion CONVENCIONALES asociadas a un predio por su
     * id de predio
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstNoConvPorIdPredio(
        Long idPred);

    /**
     * Método que retorna las p unidades de construccion asociadas a un tramite que se enceuntran
     * canceladas y que las construcciones con condicion de propiedad 8 y 9
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidConstOriginalesCanceladasPorTramite(
        Long idTramite);

    /**
     * Método que retorna las p unidades de construccion asociadas a un tramite que se enceuntran
     * canceladas y que son diferentes a las construcciones con condicion de propiedad 8 y 9
     *
     * @author leidy.gonzalez
     * @param idPredio
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidConstOriginalesPorIdPredio(
        Long idPredio);

    /**
     * Método que retorna las p unidades de construccion asociadas a un predio por su id de predio y
     * unidad
     *
     * @author leidy.gonzalez
     * @param idPred, unidad
     * @return
     */
    public PUnidadConstruccion obtenerPUnidadsConstruccionPorIdPredioYUnidad(
        Long idPred, String unidad);

    /**
     * Método que retorna las p unidades de construccion asociadas a una lista de id's
     *
     * @author leidy.gonzalez
     * @param id
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadConstruccionPorIds(List<Long> id);

}
