package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.fachadas.AvaluosBean;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;

/**
 *
 * Interfaz para los métodos de bd de la tabla COMITE_AVALUO
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IComiteAvaluoDAO extends IGenericJpaDAO<ComiteAvaluo, Long> {

    /**
     * Método para consultar un {@link ComiteAvaluo} por el identificador del
     * {@link ControlCalidadAvaluo} asociado
     *
     * @author christian.rodriguez
     * @param controlCalidadId Identificador del {@link ControlCalidadAvaluo} asociado
     * @return {@link ComiteAvaluo} encontrado o null si no se obtuvieron resultados
     */
    public ComiteAvaluo consultarPorIdControlCalidadAvaluo(Long controlCalidadId);

}
