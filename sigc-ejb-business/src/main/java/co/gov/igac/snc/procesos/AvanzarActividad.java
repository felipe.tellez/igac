/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos;

import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.protocolo.rest.PeticionAvanzarActividad;
import co.gov.igac.snc.procesos.Entidades.BpmActividad;
import co.gov.igac.snc.procesos.Entidades.BpmFlujoTramite;
import co.gov.igac.snc.procesos.Entidades.BpmTipoTramite;
import static co.gov.igac.snc.procesos.FlujoBpmBean.LOGGER;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;

/**
 *
 * @author michael.pena
 */
public class AvanzarActividad {
    
    private static final AvanzarActividad instancia = new AvanzarActividad();
    
    private static final String usuarioTiemposMuertos = "snc_tiempos_muertos";
    
    private AvanzarActividad(){
        
    }
    
    public static AvanzarActividad getInstance(){
        return  instancia;
    }
    
    public synchronized SolicitudCatastral avanzarActividadConservacion(FlujoBpmDAOBean flujoBpmDaoBean, SolicitudCatastral solicitudCatastral, PeticionAvanzarActividad avanceActividad) {
        SolicitudCatastral resultado = null;
        String idObjeto = null;
        List<BpmFlujoTramite> actividadFlujoTramite = null;
        List<ActividadUsuarios> actividadUsuarios = null;
        String usuarioResponsable = null;
        String usuarioAvanzar = null;
        Date FechaFinal = null;
        String actividad = null;
        String codTerritorial = null;
        String codUoc = null;
        String territorial = null;
        String uoc = null;
        Long tipoTramite = null;
        int idEstado = 0;

        try {
            idObjeto = avanceActividad.getIdObjeto();
            actividadFlujoTramite = new ArrayList<BpmFlujoTramite>();
            try{
                BpmFlujoTramite bft =  flujoBpmDaoBean.getEntityManager().find(BpmFlujoTramite.class, Long.valueOf(idObjeto));
                flujoBpmDaoBean.getEntityManager().refresh(bft);
                actividadFlujoTramite.add(bft);
            }catch(NoResultException nre){
                LOGGER.error("ERROR METODO BpmFlujoTramite NoResultException : " + nre);
                actividadFlujoTramite = flujoBpmDaoBean.bpmFlujoTramiteFindById(Long.valueOf(idObjeto));            

            } 
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
            FechaFinal = new Date();
            String fechaFinal = dateFormat.format(FechaFinal);               
            usuarioResponsable = actividadFlujoTramite.get(0).getUsuarioResponsable();
            actividad = solicitudCatastral.getActividadesUsuarios().get(0).getActividad();
            String estadoOld = actividadFlujoTramite.get(0).getIdEstado().toString();
            
            if (actividadFlujoTramite.get(0).getIdEstado()!= 5) {
                flujoBpmDaoBean.getEntityManager().refresh(actividadFlujoTramite.get(0));
                LOGGER.info("ESTADO ACTIVIDAD A AVANZAR ============================ " + estadoOld);

            if (usuarioResponsable != null && !usuarioResponsable.isEmpty()) {
                LOGGER.info("TRAMITE ID " + idObjeto + " TIENE USUARIO " + usuarioResponsable);
                
                if(usuarioResponsable.equals(usuarioTiemposMuertos)){
                    LOGGER.info("TRAMITE ID " + idObjeto + " CON USUARIO SISTEMA SNC " + usuarioResponsable);
                    String usuarioPotencial = avanceActividad.getUsuario();
                    String atributo = "login";
                    String login = FlujoBpmBean.usuarioJson(usuarioPotencial, atributo);
                    LOGGER.info("RECLAMANDO CON USUARIO ====================== " + login);
                    boolean reaclamar = flujoBpmDaoBean.reclamarActividadConservacionDAO(Long.valueOf(idObjeto), login);
                    LOGGER.info("ESTADO ACTIVIDAD RECLAMADA " + reaclamar);
                }

            } else {
                LOGGER.info("TRAMITE ID " + idObjeto + " NO TIENE USUARIO RESPONSABLE " + usuarioResponsable);
                String usuarioPotencial = avanceActividad.getUsuario();
                String atributo = "login";
                String login =  FlujoBpmBean.usuarioJson(usuarioPotencial, atributo);
                LOGGER.info("ACTIVIDAD A RECLAMAR CON USUARIO " + login);
                boolean reaclamar = flujoBpmDaoBean.reclamarActividadConservacionDAO(Long.valueOf(idObjeto), login);
                //flujoBpmDaoBean.testUpdate();
                LOGGER.info("ESTADO ACTIVIDAD RECLAMADA " + reaclamar);
                //usuarioAvanzar = login;

            }

            if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().size() > 1) {
                LOGGER.info("CONTIENE MAS DE UN USUARIO POTENCIAL === " + solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().size());
                idEstado = 2;                
                if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial() != null 
                        && !solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial().isEmpty()) {
                    codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                    codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                }else {
                    codTerritorial = String.valueOf(actividadFlujoTramite.get(0).getCodTerritorial());
                    codUoc = String.valueOf(actividadFlujoTramite.get(0).getCodUoc());
                }              
                territorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionTerritorial();
                uoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionUOC();
            } else if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().isEmpty()) {
                LOGGER.info("=======================OBJETO RETORNA LISTA DE USUARIOS VACIA ========================================");
                idEstado = 2;
                String usuarioPotencial = avanceActividad.getUsuario();
                String codigoTerritorial = "codigoTerritorial";
                String codigoUOC = "codigoUOC";
                String descripcionTerritorial = "descripcionTerritorial";
                String descripcionUOC = "descripcionUOC";
                codTerritorial = FlujoBpmBean.usuarioJson(usuarioPotencial, codigoTerritorial);
                codUoc = FlujoBpmBean.usuarioJson(usuarioPotencial, codigoUOC);
                territorial = FlujoBpmBean.usuarioJson(usuarioPotencial, descripcionTerritorial);
                uoc = FlujoBpmBean.usuarioJson(usuarioPotencial, descripcionUOC);
            } else {
                idEstado = 8;
                usuarioAvanzar = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getLogin();
                if (solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial() != null 
                        && !solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial().isEmpty()) {
                    codTerritorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoTerritorial();
                    codUoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getCodigoUOC();
                }else {
                    codTerritorial = String.valueOf(actividadFlujoTramite.get(0).getCodTerritorial());
                    codUoc = String.valueOf(actividadFlujoTramite.get(0).getCodUoc());
                } 
                territorial = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionTerritorial();
                uoc = solicitudCatastral.getActividadesUsuarios().get(0).getUsuarios().get(0).getDescripcionUOC();
            }

            if (actividad.equals("TERMINAR")) {
                idEstado = 5;
                int idEstadoProceso = 1003;
                String idIntanciaSnc = actividadFlujoTramite.get(0).getProcesoInstanciaId().toString();
                flujoBpmDaoBean.updateBpmProcesoInstancia(fechaFinal, Long.valueOf(idEstadoProceso), idIntanciaSnc);
            }
            if (actividad.equals("Validación.Radicar recurso") || actividad.equals("Validación.Registrar notificación")|| actividad.equals("Validación.Registrar aviso")) {
                idEstado = 8;
                usuarioAvanzar = usuarioTiemposMuertos;
            }
            
            if (solicitudCatastral.getTipoTramite()!= null && !solicitudCatastral.getTipoTramite().isEmpty()){
                List<BpmTipoTramite> bpmTipoTramite = flujoBpmDaoBean.bpmTipoTramitefindByTramite(solicitudCatastral.getTipoTramite());
                tipoTramite = bpmTipoTramite.get(0).getId();
                
            }else{                
                tipoTramite = actividadFlujoTramite.get(0).getTipoTramiteId();
            }

            LOGGER.info("CONSULTA ACTIVIDAD == " + actividad);
            List<BpmActividad> bpmActividadDAO = flujoBpmDaoBean.findByNombreactividad(actividad);
            long valor1 = 13;
            
            LOGGER.info("CREANDO ACTIVIDAD --------------------- IDFLUJOTRAMUITE === " + actividad + " ID ==== " + bpmActividadDAO.get(0).getId().toString());

            flujoBpmDaoBean.crearActividadConservacion(fechaFinal, actividadFlujoTramite.get(0).getProcesoInstanciaId(), Long.valueOf(bpmActividadDAO.get(0).getId().toString()),
                    tipoTramite, idEstado, usuarioAvanzar, territorial, uoc, codTerritorial, codUoc);

            List<BpmFlujoTramite> idBpmFlujoTramite = flujoBpmDaoBean.maxIdBpmFlujoTramite(actividadFlujoTramite.get(0).getProcesoInstanciaId());

            long max = 0;

            for (int i = 0; i < idBpmFlujoTramite.size(); i++) {

                if (idBpmFlujoTramite.get(i).getId() > max) {
                    max = idBpmFlujoTramite.get(i).getId();
                }

            }

            long id0 = 0;

            LOGGER.info("VALOR MAXIMO ID = " + max);
            resultado = new SolicitudCatastral();
            actividadUsuarios = solicitudCatastral.getActividadesUsuarios();
            resultado.setIdentificador(id0);
            resultado.setIdCorrelacion(id0);
            resultado.setResultado(String.valueOf(max));
            resultado.setOperacion(bpmActividadDAO.get(0).getProceso());
            resultado.setActividadesUsuarios(actividadUsuarios);
            resultado.setTransicion(bpmActividadDAO.get(0).getNombreactividad());
                                    
            LOGGER.info("ACTUALIZANDO TAREA OLD --------------------- IDFLUJOTRAMUITE = " + idObjeto);            
            int idEstadoOld = 5;
            flujoBpmDaoBean.updateActividadOldFlujoTramite(fechaFinal, Long.valueOf(idEstadoOld), Long.valueOf(idObjeto));
            
         }else{
                
            LOGGER.info("LA ACTIVIDAD ID == " + idObjeto + " YA SE ENCUENTRA AVANZADA");
            LOGGER.info("ESTADO ACTIVIDAD A AVANZAR (5) ============================ " + estadoOld);
            resultado = new SolicitudCatastral();
            resultado.setResultado(estadoOld);            
          }            

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("ERROR METODO BPM avanzarActividadConservacion : " + e);

        }

        return resultado;

    }
}
