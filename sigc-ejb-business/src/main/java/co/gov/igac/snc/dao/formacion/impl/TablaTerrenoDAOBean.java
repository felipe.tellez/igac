/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.formacion.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.formacion.ITablaTerrenoDAO;
import co.gov.igac.snc.persistence.entity.formacion.TablaTerreno;

/**
 *
 * @author fredy.wilches
 */
@Stateless
public class TablaTerrenoDAOBean extends GenericDAOWithJPA<TablaTerreno, Long>
    implements ITablaTerrenoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TablaTerrenoDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<TablaTerreno> findByZonaVigencia(String zona, Date vigencia) {
        List<TablaTerreno> answer = null;
        Query query;

        try {
            query = this.entityManager.createNamedQuery("findByZonaVigencia");
            query.setParameter("zona", zona);
            query.setParameter("vigencia", vigencia);
            answer = query.getResultList();
        } catch (Exception ex) {
            LOGGER.debug("error en la consulta de tabla_terreno por zona y vigencia: " +
                ex.getMessage());
        }
        return answer;
    }

}
