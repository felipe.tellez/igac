package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;

/**
 * Interfaz para los servicios de persistencia del objeto {@link MunicipioCierreVigencia}.
 *
 * @author david.cifuentes
 */
@Local
public interface IMunicipioCierreVigenciaDAO extends
    IGenericJpaDAO<MunicipioCierreVigencia, Long> {

    /**
     * Método que consulta los {@link MunicipioCierreVigencia} por el código del departamento.
     *
     * @author david.cifuentes
     *
     * @param decretoAvaluoAnual {@link DecretoAvaluoanual} decreto actual.
     *
     * @param codigosMunicipios Lista de {@link String} con los codigos de municipios de un
     * departamento seleccionado previamente.
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     *
     * @return Lista de objetos {@link MunicipioCierreVigencia}
     *
     */
    public List<MunicipioCierreVigencia> consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
        DecretoAvaluoAnual decretoAvaluoAnual,
        List<String> codigosMunicipios) throws ExcepcionSNC;

    // ------------------------------------------------ //
    /**
     * Método que consulta la existencia de {@link MunicipioCierreVigencia} para el
     * {@link DecretoAvaluoAnual} enviado como parámetro. En caso de que no existan registros de
     * municipios para dicho decreto, se insertará un registro por cada municipio existente,
     * asociado el decreto enviado.
     *
     * @author david.cifuentes
     *
     * @param decretoAvaluoAnual {@link DecretoAvaluoanual} decreto actual.
     * @param usuario {@link UsuarioDTO} usuario logueado en sesión.
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     *
     */
    public void validarExistenciaMunicipiosPorDecreto(
        DecretoAvaluoAnual decretoAvaluoAnual, UsuarioDTO usuario)
        throws ExcepcionSNC;

    // ------------------------------------------------ //
    /**
     * Método que actualiza una lista de {@link MunicipioCierreVigencia}. En caso de que realizar la
     * actualización adecuadamente retorna true, de lo contrario retorna false.
     *
     * @author david.cifuentes
     *
     * @param Lista a actualizar de objetos {@link MunicipioCierreVigencia}
     *
     * @eturn true si actualizó correctamente, false si hubo alguna inconsistencia
     */
    public boolean guardarListaMunicipioCierreVigencias(
        List<MunicipioCierreVigencia> listaMunicipioCierreVigencia);

}
