package co.gov.igac.snc.dao.tramite;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;

@Local
public interface IMotivoEstadoTramiteDAO extends IGenericJpaDAO<MotivoEstadoTramite, Long> {

}
