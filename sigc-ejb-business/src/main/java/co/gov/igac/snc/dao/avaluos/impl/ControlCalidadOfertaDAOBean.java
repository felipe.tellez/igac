package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadOfertaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see IControlCalidadOfertaDAO
 *
 * @author rodrigo.hernandez
 *
 */
@Stateless
public class ControlCalidadOfertaDAOBean extends
    GenericDAOWithJPA<ControlCalidadOferta, Long> implements
    IControlCalidadOfertaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadOfertaDAOBean.class);

    /**
     * @see
     * IControlCalidadOfertaDAO#buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(String,
     * Long, Long)
     *
     * @author rodrigo.hernandez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ControlCalidadOferta> buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId) {
        LOGGER.debug(
            "entrando a ControlCalidadOfertaDAOBean#buscarOfertasSeleccionadasPorIdRecolectorEIdControlCalidad ...");

        List<ControlCalidadOferta> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT cco FROM ControlCalidadOferta cco " +
            " JOIN FETCH cco.ofertaInmobiliariaId oferta " +
            " WHERE cco.recolectorId = :idRecolector " +
            " AND cco.controlCalidad.id = :ccId " +
            " AND cco.ofertaSeleccionada = :estaSeleccionada " +
            " AND oferta.regionCapturaOfertaId = :regionId ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", recolectorId);
            query.setParameter("ccId", controlCalidadId);
            query.setParameter("estaSeleccionada", ESiNo.SI.getCodigo());
            query.setParameter("regionId", regionId);

            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                for (ControlCalidadOferta cco : queryAnswer) {
                    cco.getOfertaInmobiliariaId().getId();
                    cco.getOfertaInmobiliariaId().getMunicipio().getCodigo();
                    cco.getOfertaInmobiliariaId().getDepartamento().getCodigo();
                    cco.getOfertaInmobiliariaId().getOfertaPredios().size();
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        LOGGER.debug(
            "saliendo de ControlCalidadOfertaDAOBean#buscarOfertasSeleccionadasPorIdRecolectorEIdControlCalidad ...");

        return queryAnswer;
    }

    /**
     * @see
     * IControlCalidadOfertaDAO#buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(String,
     * Long, Long)
     *
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<OfertaInmobiliaria> buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId) {
        LOGGER.debug(
            "entrando a ControlCalidadOfertaDAOBean#buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion ...");

        List<OfertaInmobiliaria> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT oferta " +
            " FROM OfertaInmobiliaria oferta, ControlCalidadOferta cco " +
            " WHERE oferta.id = cco.ofertaInmobiliariaId " +
            " AND oferta.regionCapturaOfertaId = :regionId " +
            " AND cco.recolectorId = :idRecolector " +
            " AND cco.controlCalidad.id = :ccId " +
            " AND cco.ofertaSeleccionada = :estaSeleccionada " +
            " AND cco.aprobado = :estaAprobada ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", recolectorId);
            query.setParameter("ccId", controlCalidadId);
            query.setParameter("estaSeleccionada", ESiNo.SI.getCodigo());
            query.setParameter("estaAprobada", ESiNo.NO.getCodigo());
            query.setParameter("regionId", regionId);

            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                for (OfertaInmobiliaria oferta : queryAnswer) {
                    oferta.getId();
                    oferta.getMunicipio().getCodigo();
                    oferta.getDepartamento().getCodigo();
                    oferta.getOfertaPredios().size();
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        LOGGER.debug(
            "saliendo de ControlCalidadOfertaDAOBean#buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion ...");

        return queryAnswer;
    }

    /**
     * @see
     * IControlCalidadOfertaDAO#buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidadIdRegion(String,
     * Long)
     *
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ControlCalidadOferta> buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(
        String recolectorId, Long controlCalidadId) {

        LOGGER.debug(
            "entrando a ControlCalidadOfertaDAOBean#buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad ...");

        List<ControlCalidadOferta> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT cco " +
            " FROM ControlCalidadOferta cco " +
            " WHERE cco.recolectorId = :idRecolector " +
            " AND cco.controlCalidad.id = :ccId " +
            " AND cco.ofertaSeleccionada = :estaSeleccionada " +
            " AND cco.aprobado = :estaAprobada ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", recolectorId);
            query.setParameter("ccId", controlCalidadId);
            query.setParameter("estaSeleccionada", ESiNo.SI.getCodigo());
            query.setParameter("estaAprobada", ESiNo.NO.getCodigo());

            queryAnswer = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        LOGGER.debug(
            "saliendo de ControlCalidadOfertaDAOBean#buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad ...");

        return queryAnswer;
    }

    @Override
    public List<ControlCalidadOferta> buscarOfertasPorIdRecolectorEIdControlCalidad(
        String recolectorId, Long controlCalidadId) {
        LOGGER.debug(
            "entrando a ControlCalidadOfertaDAOBean#buscarOfertasPorIdRecolectorEIdControlCalidad ...");

        List<ControlCalidadOferta> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT cco FROM ControlCalidadOferta cco" +
            " JOIN FETCH cco.ofertaInmobiliariaId" +
            " WHERE cco.recolectorId = :idRecolector" +
            " AND cco.controlCalidad.id = :ccId";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idRecolector", recolectorId);
            query.setParameter("ccId", controlCalidadId);

            queryAnswer = query.getResultList();

            if (queryAnswer != null) {
                for (ControlCalidadOferta cco : queryAnswer) {
                    cco.getOfertaInmobiliariaId().getId();
                    cco.getOfertaInmobiliariaId().getMunicipio().getCodigo();
                    cco.getOfertaInmobiliariaId().getDepartamento().getCodigo();
                    cco.getOfertaInmobiliariaId().getOfertaPredios().size();
                }
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        LOGGER.debug(
            "saliendo de ControlCalidadOfertaDAOBean#buscarOfertasPorIdRecolectorEIdControlCalidad ...");

        return queryAnswer;
    }

    /**
     * @see IControlCalidadOfertaDAO#buscarIdsRegionesAsociadasAControlCalidad(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Long> buscarIdsRegionesAsociadasAControlCalidad(Long controlCalidadId) {
        LOGGER.debug(
            "entrando a ControlCalidadOfertaDAOBean#buscarIdsRegionesAsociadasAControlCalidad ...");

        List<Long> queryAnswer = null;
        String queryString;
        Query query;

        queryString = "SELECT DISTINCT oi.regionCapturaOfertaId " +
            " FROM ControlCalidadOferta cc, OfertaInmobiliaria oi " +
            " WHERE cc.controlCalidad.id = :controlCalidadId " +
            " AND cc.ofertaInmobiliariaId = oi.id " +
            " AND oi.regionCapturaOfertaId IS NOT NULL " +
            " AND cc.ofertaSeleccionada = :seleccionada";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("controlCalidadId", controlCalidadId);
            query.setParameter("seleccionada", ESiNo.SI.getCodigo());

            queryAnswer = (List<Long>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        LOGGER.debug(
            "saliendo de ControlCalidadOfertaDAOBean#buscarIdsRegionesAsociadasAControlCalidad ...");

        return queryAnswer;
    }

}
