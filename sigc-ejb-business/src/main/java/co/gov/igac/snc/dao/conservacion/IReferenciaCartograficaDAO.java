package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.ReferenciaCartografica;

@Local
public interface IReferenciaCartograficaDAO extends
    IGenericJpaDAO<ReferenciaCartografica, Long> {

    /**
     * Metodo encargado de buscar las referencias cartográficas asociadas a un predio.
     *
     * @param predio Predio sobre el cual se realiza la búsqueda de referencias cartográficas.
     * @return Retorna las referencias cartográficas asociadas al predio.
     */
    public List<ReferenciaCartografica> getReferenciaCartograficaByPredio(Predio predio);
}
