package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.MPredio;

@Local
public interface IMPredioDAO extends IGenericJpaDAO<MPredio, Long> {

    /**
     * @author leidy.gonzalez Metodo que consulta mPredio pero lo trae con todas sus colecciones
     * dependientes
     * @throws Exception
     */
    public MPredio findPPredioCompletoByIdTramite(Long idTramite) throws Exception;

    /**
     * Retorna un mpredio por el número predial especificado
     *
     * @author leidy.gonzalez
     * @param numPredial número predial del ppredio a obtener
     * @return
     */
    public MPredio getMPredioByNumeroPredial(String numeroPredial);
}
