/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioInconsistenciaDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PredioInconsistencia;

/**
 *
 * @author lorena.salamanca
 */
@Stateless
public class PredioInconsistenciaDAOBean extends GenericDAOWithJPA<PredioInconsistencia, Long>
    implements IPredioInconsistenciaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioInconsistenciaDAOBean.class);

    /**
     * @see IPredioInconsistenciaDAO#findByPredioId(Long)
     * @author lorena.salamanca
     */
    @Override
    public List<PredioInconsistencia> findByPredioId(Long predioId) {
        Query query = this.entityManager.createQuery("SELECT pi" +
            " FROM PredioInconsistencia pi" +
            " WHERE pi.predio.id= :predioId");
        query.setParameter("predioId", predioId);
        try {
            LOGGER.debug("on IPredioInconsistenciaDAO#findByPredioId");
            List<PredioInconsistencia> pi = (List<PredioInconsistencia>) query.getResultList();
            return pi;
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Falló la carga inconsistencias", e);
        }
    }
// end of class
}
