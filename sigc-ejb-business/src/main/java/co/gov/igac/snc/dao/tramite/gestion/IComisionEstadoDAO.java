/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla COMISION_ESTADO
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Local
public interface IComisionEstadoDAO extends IGenericJpaDAO<ComisionEstado, Long> {

    /**
     * Retorna una lista con los estados por los que ha pasado una comisión, ordenada en forma
     * descendente. La lista contendrá un máximo de elementos determinados por el parámetro
     * numRegistros. Esto se usa -en especial- para el caso en el que se necesita obtener datos del
     * último cambio de estado.
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param idComision id de la comisión
     * @param numRegistros Número de registros que se van a retornar. si se pasa un número menor que
     * 0 devuelve todos los registros
     * @return
     */
    public List<ComisionEstado> getHistoricalRecord(Long idComision, int numRegistros);

    /**
     * Obtiene el último registro de cambio de estado para la comisión con id dado. Si el parámetro
     * estadoComision no es nulo, busca el último registro con ese estado. </br>
     *
     * @author pedro.garcia
     * @param idComision id de la comisión
     * @param estadoComision estado de la comisión
     * @return null o el registro buscado
     */
    public ComisionEstado obtenerUltimoDeComision(Long idComision, String estadoComision);

//end of interface
}
