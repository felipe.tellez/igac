/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioBloqueoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.FuncionPredioPersonaBloqueoHoy;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class PredioBloqueoDAOBean extends GenericDAOWithJPA<PredioBloqueo, Long>
    implements IPredioBloqueoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioBloqueoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioBloqueoDAO#findCurrentPredioBloqueo(java.lang.String)
     * @SuppressWarnings("unchecked") /* /
     *
     * @modified juan.agudelo correccion por Nullcheck of value previously dereferenced
     * @modified pedro.garcia 14-05-2012 cambio en la condición de búsqueda por redefinición de la
     * misma para encontrar el bloqueo de un predio
     *
     */
    @Implement
    @Override
    public PredioBloqueo findCurrentPredioBloqueo(String numeroPredial) {
        LOGGER.debug("executing PredioBloqueoDAOBean#findCurrentPredioBloqueo");

        Query query;
        String queryToExecute;
        PredioBloqueo answer = null;
        ArrayList<PredioBloqueo> predioBloqueos = null;
        Date currentDate;

        //D: la comparación de fechas ahora se hace en la consulta directamente
        queryToExecute = "SELECT pb FROM PredioBloqueo pb" +
            " JOIN pb.predio p" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " WHERE p.numeroPredial= :np" +
            " AND FNC_PREDIO_BLOQUEADO (p.id, :fechaActual) = :si";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("np", numeroPredial);
            currentDate = new Date();
            query.setParameter("fechaActual", currentDate);
            query.setParameter("si", "SI");

            predioBloqueos = (ArrayList<PredioBloqueo>) query.getResultList();
            if (predioBloqueos == null || predioBloqueos.isEmpty()) {
                throw new NoResultException();
            } else if (predioBloqueos.size() > 1) {
                LOGGER.error(
                    "La consulta de bloqueo actual de un predio devolvió más de un resultado");
            } else {
                answer = predioBloqueos.get(0);
            }
        } catch (NoResultException nrEx) {
            LOGGER.error("PredioBloqueoDAOBean#findCurrentPredioBloqueo: " +
                "La consulta de bloqueo predios no devolvió resultados");
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioBloqueoDAOBean#findCurrentPredioBloqueo");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see IPrediosBloqueoDAO#contarPrediosBloqueo(FiltroDatosConsultaPrediosBloqueo)
     */
    @SuppressWarnings("finally")
    @Override
    public Integer contarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo) {
        LOGGER.debug("executing PrediosBloqueoDAOBean#contarPrediosBloqueo");

        Integer cPrediosBloqueo = null;
        StringBuilder query = new StringBuilder();

        query.append("SELECT COUNT (pb) FROM PredioBloqueo pb" +
            " JOIN pb.predio pbp" +
            " JOIN pbp.departamento" + " JOIN pbp.municipio" +
            " WHERE 1 = 1");

        if (filtroDatosConsultaPredioBloqueo.getDepartamento() != null) {
            query.append(" AND pbp.departamento = :departamento");
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipio() != null) {
            query.append(" AND pbp.municipio = :municipio");
        }
        if (filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo()
                .isEmpty()) {
            query.append(" AND pbp.departamento.codigo = :departamentoCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getMunicipioCodigo()
                .isEmpty()) {
            query.append(" AND pbp.municipio.codigo like :municipioCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getTipoAvaluo() != null &&
            !filtroDatosConsultaPredioBloqueo.getTipoAvaluo().isEmpty()) {
            query.append(" AND pbp.tipoAvaluo= :tipoAvaluo");
        }
        if (filtroDatosConsultaPredioBloqueo.getSectorCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getSectorCodigo()
                .isEmpty()) {
            query.append(" AND pbp.sectorCodigo= :sectorCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getComunaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getComunaCodigo()
                .isEmpty()) {
            query.append(" AND pbp.barrioCodigo like :comunaCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getBarrioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getBarrioCodigo()
                .isEmpty()) {
            query.append(" AND pbp.barrioCodigo like :barrioCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo()
                .isEmpty()) {
            query.append(" AND pbp.manzanaCodigo= :manzanaCodigo");
        }
        if (filtroDatosConsultaPredioBloqueo.getPredio() != null &&
            !filtroDatosConsultaPredioBloqueo.getPredio().isEmpty()) {
            query.append(" AND pbp.predio= :predio");
        }
        if (filtroDatosConsultaPredioBloqueo.getCondicionPropiedad() != null &&
            !filtroDatosConsultaPredioBloqueo.getCondicionPropiedad()
                .isEmpty()) {
            query.append(" AND pbp.condicionPropiedad= :condicionPropiedad");
        }
        if (filtroDatosConsultaPredioBloqueo.getEdificio() != null &&
            !filtroDatosConsultaPredioBloqueo.getEdificio().isEmpty()) {
            query.append(" AND pbp.edificio= :edificio");
        }
        if (filtroDatosConsultaPredioBloqueo.getPiso() != null &&
            !filtroDatosConsultaPredioBloqueo.getPiso().isEmpty()) {
            query.append(" AND pbp.piso= :piso");
        }
        if (filtroDatosConsultaPredioBloqueo.getUnidad() != null &&
            !filtroDatosConsultaPredioBloqueo.getUnidad().isEmpty()) {
            query.append(" AND pbp.unidad= :unidad");
        }

        query.append(" AND ").append(
            FuncionPredioPersonaBloqueoHoy.getQueryprediopersonabloqueohoy());

        Query q = this.entityManager.createQuery(query.toString());

        q.setParameter(FuncionPredioPersonaBloqueoHoy
            .getNameparameterprediopersonabloqueohoy(),
            FuncionPredioPersonaBloqueoHoy
                .getParameterprediopersonabloqueohoy());

        if (filtroDatosConsultaPredioBloqueo.getDepartamento() != null) {
            q.setParameter("departamento",
                filtroDatosConsultaPredioBloqueo.getDepartamento());
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipio() != null) {
            q.setParameter("municipio",
                filtroDatosConsultaPredioBloqueo.getMunicipio());
        }
        if (filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo()
                .isEmpty()) {
            q.setParameter("departamentoCodigo",
                filtroDatosConsultaPredioBloqueo.getDepartamentoCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getMunicipioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getMunicipioCodigo()
                .isEmpty()) {
            q.setParameter("municipioCodigo", "%" +
                filtroDatosConsultaPredioBloqueo.getMunicipioCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getTipoAvaluo() != null &&
            !filtroDatosConsultaPredioBloqueo.getTipoAvaluo().isEmpty()) {
            q.setParameter("tipoAvaluo",
                filtroDatosConsultaPredioBloqueo.getTipoAvaluo());
        }
        if (filtroDatosConsultaPredioBloqueo.getSectorCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getSectorCodigo()
                .isEmpty()) {
            q.setParameter("sectorCodigo",
                filtroDatosConsultaPredioBloqueo.getSectorCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getComunaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getComunaCodigo()
                .isEmpty()) {
            q.setParameter("comunaCodigo",
                filtroDatosConsultaPredioBloqueo.getComunaCodigo() + "%");
        }
        if (filtroDatosConsultaPredioBloqueo.getBarrioCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getBarrioCodigo()
                .isEmpty()) {
            q.setParameter("barrioCodigo", "%" +
                filtroDatosConsultaPredioBloqueo.getBarrioCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo() != null &&
            !filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo()
                .isEmpty()) {
            q.setParameter("manzanaCodigo",
                filtroDatosConsultaPredioBloqueo.getManzanaVeredaCodigo());
        }
        if (filtroDatosConsultaPredioBloqueo.getPredio() != null &&
            !filtroDatosConsultaPredioBloqueo.getPredio().isEmpty()) {
            q.setParameter("predio",
                filtroDatosConsultaPredioBloqueo.getPredio());
        }
        if (filtroDatosConsultaPredioBloqueo.getCondicionPropiedad() != null &&
            !filtroDatosConsultaPredioBloqueo.getCondicionPropiedad()
                .isEmpty()) {
            q.setParameter("condicionPropiedad",
                filtroDatosConsultaPredioBloqueo.getCondicionPropiedad());
        }
        if (filtroDatosConsultaPredioBloqueo.getEdificio() != null &&
            !filtroDatosConsultaPredioBloqueo.getEdificio().isEmpty()) {
            q.setParameter("edificio",
                filtroDatosConsultaPredioBloqueo.getEdificio());
        }
        if (filtroDatosConsultaPredioBloqueo.getPiso() != null &&
            !filtroDatosConsultaPredioBloqueo.getPiso().isEmpty()) {
            q.setParameter("piso", filtroDatosConsultaPredioBloqueo.getPiso());
        }
        if (filtroDatosConsultaPredioBloqueo.getUnidad() != null &&
            !filtroDatosConsultaPredioBloqueo.getUnidad().isEmpty()) {
            q.setParameter("unidad",
                filtroDatosConsultaPredioBloqueo.getUnidad());
        }

        try {
            Long l = (Long) q.getSingleResult();
            cPrediosBloqueo = Integer.valueOf(l.intValue());
        } catch (NoResultException ne) {
            LOGGER.info("No se encontraron PrediosBloqueo con las características dadas");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "PredioBloqueoDAOBean#contarPrediosBloqueo");
        } finally {
            return cPrediosBloqueo;
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * @see IPredioBloqueoDao#existeAsociacionEntidadBloqueo(Entidad)
     * @author david.cifuentes
     */
    @Override
    public boolean existeAsociacionEntidadBloqueo(Entidad entidad) {

        LOGGER.debug("Executing PredioBloqueoDAOBean#existeAsociacionEntidadBloqueo");

        Query query;
        boolean answer = false;

        try {
            query = this.entityManager.createQuery(
                "SELECT count(pb.id) FROM PredioBloqueo pb WHERE pb.entidadId = :idEntidad");

            query.setParameter("idEntidad", entidad.getId());

            Long conteo = (Long) query.getSingleResult();
            if (conteo > 0) {
                answer = true;
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("PredioBloqueoDAOBean#existeAsociacionEntidadBloqueo: " +
                "La consulta de entidades no devolvió resultados");
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_BUSQUEDA_POR_ID.getExcepcion(LOGGER,
                nrEx, "PredioBloqueoDAOBean#existeAsociacionEntidadBloqueo", "PredioBloqueo",
                entidad.getId());
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, ex,
                    "PredioBloqueoDAOBean#existeAsociacionEntidadBloqueo");
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioBloqueoDao#busquedaPredioBloqueo(Entidad)
     * @author dumar.penuela Método que retorna un predio bloqueado según el tipo de bloqueo
     */
    @Implement
    @Override
    public PredioBloqueo busquedaPredioBloqueoByTipoBloqueo(String numeroPredial, String tipoBloqueo) {
        LOGGER.debug("executing PredioBloqueoDAOBean#busquedaPredioBloqueoByTipoBloqueo");

        Query query;
        PredioBloqueo answer = null;
        ArrayList<PredioBloqueo> predioBloqueos = null;
        StringBuilder queryToExecute = new StringBuilder();

        queryToExecute.append(" SELECT pb FROM PredioBloqueo pb JOIN pb.predio p  " +
            " WHERE " +
            " p.numeroPredial= :numeroPredial " +
            " AND pb.tipoBloqueo= :tipoBloqueo " +
            " AND " + FuncionPredioPersonaBloqueoHoy.getQueryprediopersonabloqueohoy() +
            " ORDER BY pb.fechaInicioBloqueo DESC ");

        try {
            query = this.entityManager.createQuery(queryToExecute.toString());
            query.setParameter("numeroPredial", numeroPredial);
            query.setParameter("tipoBloqueo", tipoBloqueo);

            query.setParameter(FuncionPredioPersonaBloqueoHoy.
                getNameparameterprediopersonabloqueohoy(),
                FuncionPredioPersonaBloqueoHoy.getParameterprediopersonabloqueohoy());

            predioBloqueos = (ArrayList<PredioBloqueo>) query.getResultList();
            if (predioBloqueos != null && !predioBloqueos.isEmpty()) {
                answer = predioBloqueos.get(0);
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioBloqueoDAOBean#busquedaPredioBloqueoByTipoBloqueo");
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioBloqueoDao#busquedaPredioBloqueo(numeroPredial)
     * @author dumar.penuela Método que retorna predio bloqueado más reciente
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public PredioBloqueo busquedaPredioBloqueoReciente(String numeroPredial) {
        LOGGER.debug("executing PredioBloqueoDAOBean#busquedaPredioBloqueoReciente");

        Query q;
        PredioBloqueo answer = null;
        ArrayList<PredioBloqueo> predioBloqueos;
        StringBuilder query = new StringBuilder();

        query.append(" SELECT pb FROM PredioBloqueo pb JOIN pb.predio p " +
            " WHERE " +
            " p.numeroPredial= :numeroPredial " +
            " AND " + FuncionPredioPersonaBloqueoHoy.getQueryprediopersonabloqueohoy() +
            " ORDER BY pb.fechaInicioBloqueo DESC ");

        try {

            q = this.entityManager.createQuery(query.toString());
            q.setParameter("numeroPredial", numeroPredial);

            q.setParameter(FuncionPredioPersonaBloqueoHoy
                .getNameparameterprediopersonabloqueohoy(),
                FuncionPredioPersonaBloqueoHoy
                    .getParameterprediopersonabloqueohoy());

            predioBloqueos = (ArrayList<PredioBloqueo>) q.getResultList();

            if (predioBloqueos != null && !predioBloqueos.isEmpty()) {

                if (predioBloqueos.size() == 1) {
                    answer = predioBloqueos.get(0);
                } else {
                    for (int i = 0; i < predioBloqueos.size(); i++) {
                        if (EPredioBloqueoTipoBloqueo.SUSPENDER.getCodigo().equals((predioBloqueos.
                            get(i).getTipoBloqueo()))) {
                            return predioBloqueos.get(i);
                        }
                    }
                    answer = predioBloqueos.get(0);
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioBloqueoDAOBean#busquedaPredioBloqueoReciente");
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IPredioBloqueoDao#busquedaPredioDesBloqueo(numeroPredial)
     * @author dumar.penuela Método que retorna el predio desbloqueado más reciente
     */
    @Implement
    @Override
    public PredioBloqueo busquedaPredioDesBloqueoReciente(String numeroPredial) {
        LOGGER.debug("executing PredioBloqueoDAOBean#busquedaPredioBloqueoReciente");

        Query q;
        PredioBloqueo answer = null;
        ArrayList<PredioBloqueo> predioBloqueos;
        StringBuilder query = new StringBuilder();

        query.append(" SELECT pb FROM PredioBloqueo pb JOIN pb.predio p " +
            " WHERE " +
            " p.numeroPredial= :numeroPredial " +
            " AND ( " +
            " pb.fechaInicioBloqueo > :fechaActual OR pb.fechaDesbloqueo <= :fechaActual " +
            " OR ( pb.fechaTerminaBloqueo < :fechaActual AND pb.fechaDesbloqueo IS null )) " +
            " ORDER BY pb.fechaInicioBloqueo DESC ");

        try {

            q = this.entityManager.createQuery(query.toString());
            q.setParameter("numeroPredial", numeroPredial);

            q.setParameter(FuncionPredioPersonaBloqueoHoy
                .getNameparameterprediopersonabloqueohoy(),
                FuncionPredioPersonaBloqueoHoy
                    .getParameterprediopersonabloqueohoy());

            predioBloqueos = (ArrayList<PredioBloqueo>) q.getResultList();
            if (predioBloqueos != null && !predioBloqueos.isEmpty()) {
                answer = predioBloqueos.get(0);
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioBloqueoDAOBean#busquedaPredioBloqueoReciente");
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see IPrediosBloqueoDAO#getPredioDesBloqueosByNumeroPredial(String)
     * @author dumar.penuela Método que retorna la lista de predios desbloqueados
     */
    @Override
    public List<PredioBloqueo> getPredioDesBloqueosByNumeroPredial(
        String numeroPredial) {
        LOGGER.debug("terting: PredioBloqueoDAOBean#getPredioDesBloqueosByNumeroPredial");

        List<PredioBloqueo> answer;
        StringBuilder query = new StringBuilder();

        query.append("SELECT pb FROM PredioBloqueo pb" +
            " LEFT JOIN FETCH pb.documentoSoporteBloqueo" +
            " LEFT JOIN FETCH pb.documentoSoporteDesbloqueo" +
            " JOIN FETCH pb.predio pbp" +
            " LEFT JOIN FETCH pb.municipio" +
            " LEFT JOIN FETCH pb.departamento" +
            " LEFT JOIN FETCH pb.entidadDesbloqueo" +
            " WHERE  pbp.numeroPredial= :numeroPredial" +
            " AND (pb.fechaDesbloqueo IS NOT NULL AND CURRENT_DATE > pb.fechaDesbloqueo)");

        Query q = entityManager.createQuery(query.toString());
        q.setParameter("numeroPredial", numeroPredial);

        try {
            answer = q.getResultList();
        } catch (IndexOutOfBoundsException iobe) {
            LOGGER.error("ERROR: PredioBloqueoDAOBean#getPredioDesBloqueosByNumeroPredial", iobe);
            return new ArrayList<PredioBloqueo>();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "PredioBloqueoDAOBean#getPredioDesBloqueosByNumeroPredial");
        }

        return answer;
    }

    /**
     * @author felipe.cadena
     * @see IPrediosBloqueoDAO#obtenerBloqueosPredio(Long String)
     */
    public List<PredioBloqueo> obtenerBloqueosPredio(Long predioId, String estadoBloqueo) {

        List<PredioBloqueo> result = new ArrayList<PredioBloqueo>();
        Query query;

        String queryToExecute = "SELECT pb FROM PredioBloqueo pb" +
            " JOIN pb.predio p" +
            " WHERE p.id= :predioId";

        try {
            query = this.entityManager.createQuery(queryToExecute);

            query.setParameter("predioId", predioId);

            result = (ArrayList<PredioBloqueo>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("PredioBloqueoDAOBean#obtenerBloqueosPredio: " +
                "La consulta de bloqueo predios no retorna resultados");
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, ex, "PredioBloqueoDAOBean#obtenerBloqueosPredio");
        }

        return result;
    }

//end of class
}
