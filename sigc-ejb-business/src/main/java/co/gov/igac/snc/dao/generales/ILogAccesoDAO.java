/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import javax.ejb.Local;

/**
 * Dao para acceder a la entidad LogAcceso
 *
 * @author felipe.cadena
 */
@Local
public interface ILogAccesoDAO extends IGenericJpaDAO<LogAcceso, Long> {

    /**
     * Método para obtener el registro de actual de un usuario
     *
     * @author felipe.cadena
     *
     * @param usuario
     * @return
     */
    public LogAcceso obtenerPorLogin(String usuario);

    /**
     * Retorna el numero de accesos iniciados por un usuario
     *
     * @author felipe.cadena
     * @param usuario
     * @return
     */
    public Long obtenerNumeroAccesosIniciadosPorUsuario(String usuario);

}
