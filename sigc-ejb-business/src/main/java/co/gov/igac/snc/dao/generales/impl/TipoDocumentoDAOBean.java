package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class TipoDocumentoDAOBean extends
    GenericDAOWithJPA<TipoDocumento, Long> implements ITipoDocumentoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TipoDocumentoDAOBean.class);

    public List<TipoDocumento> findByTipoTramite(String tipoTramite,
        String claseMutacion, String subtipo) {
        String query = "SELECT td" +
            " FROM TramiteRequisitoDocumento trd JOIN trd.tramiteRequisito tr JOIN trd.tipoDocumento td" +
            " WHERE tr.tipoTramite = :tipoTramite and" +
            (claseMutacion != null ? "tr.claseMutacion= :claseMutacion and" : "") +
            (subtipo != null ? "tr.subtipo= :subtipo and" : "") +
            " td.generado='NO' and trd.requerido='SI'";
        Query q = entityManager.createQuery(query);
        q.setParameter("tipoTramite", tipoTramite);

        if (claseMutacion != null) {
            q.setParameter("claseMutacion", claseMutacion);
        }
        if (subtipo != null) {
            q.setParameter("subtipo", subtipo);
        }
        try {
            return (List<TipoDocumento>) q.getResultList();
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
            return null;
        }
    }

    @Override
    public TipoDocumento getTipoDocumentoPorClase(String clase) {

        String query = "SELECT tipo" +
            " FROM TipoDocumento tipo" +
            " WHERE tipo.clase= :clase ";
        Query q = entityManager.createQuery(query);
        q.setParameter("clase", clase);

        try {
            return (TipoDocumento) q.getSingleResult();
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see ITipoDocumentoDAO#findTipoDeDocumentosAll(String claseDoc)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TipoDocumento> findTipoDeDocumentosAll(String claseDoc, String generado) {

        String where;

        if (claseDoc.equals("")) {
            where = "";
        } else {
            where = "WHERE td.clase = :claseDoc ";
            if (!generado.equals("")) {
                where = where + " AND td.generado = :generado";
            }

        }
        String query = "SELECT td FROM TipoDocumento td " + where;
        Query q = entityManager.createQuery(query);

        if (!claseDoc.equals("")) {
            q.setParameter("claseDoc", claseDoc);
        }
        if (!generado.equals("")) {
            q.setParameter("generado", generado);
        }
        try {
            return (List<TipoDocumento>) q.getResultList();
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * @see ITipoDocumentoDAO#getAllTipoDocumento()
     * @author juan.agudelo
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TipoDocumento> getAllTipoDocumento() {
        List<TipoDocumento> tiposDocumento = null;
        Query query;
        String q;

        q = "SELECT td FROM TipoDocumento td" +
            " WHERE td.generado = :generado";

        query = entityManager.createQuery(q);
        query.setParameter("generado", ESiNo.NO.getCodigo());

        try {
            tiposDocumento = (List<TipoDocumento>) query.getResultList();
        } catch (ExcepcionSNC e) {

            throw new ExcepcionSNC("mi codigo de excepcion",
                ESeveridadExcepcionSNC.ERROR, e.getMensaje(), e);
        }

        return tiposDocumento;
    }

    /**
     * @see ITipoDocumentoDAO#obtenerTipoDocumentoAvaluoAdicional()
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TipoDocumento> obtenerTipoDocumentoAvaluoAdicional(String tipoTramite) {
        List<TipoDocumento> tiposDocumento = null;
        Query query;
        String q;

        q = "SELECT td FROM TipoDocumento td" +
            " WHERE td.generado = 'NO'" +
            " AND td.id NOT IN (SELECT td.id" +
            " FROM TramiteRequisitoDocumento trd JOIN trd.tramiteRequisito tr JOIN trd.tipoDocumento td" +
            " WHERE tr.tipoTramite = :tipoTramite and" +
            " td.generado='NO' and trd.requerido='SI')";

        query = entityManager.createQuery(q);
        query.setParameter("tipoTramite", tipoTramite);

        try {
            tiposDocumento = (List<TipoDocumento>) query.getResultList();
        } catch (ExcepcionSNC e) {

            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#obtenerTipoDocumentoAvaluoAdicional");
        }

        return tiposDocumento;
    }

    /**
     * @see ITipoDocumentoDAO#obtenerTipoDocumentoSoporteProductos()
     * @author javier.barajas
     */
    @Override
    public List<TipoDocumento> obtenerTipoDocumentoSoporteProductos() {
        List<TipoDocumento> tiposDocumento = null;
        Query query;
        String q;

        q = "SELECT td FROM TipoDocumento td" +
            " WHERE td.clase='SOPORTE_PRODUCTO'";

        query = entityManager.createQuery(q);

        try {
            tiposDocumento = (List<TipoDocumento>) query.getResultList();
        } catch (ExcepcionSNC e) {

            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(
                LOGGER, e, e.getMessage(),
                "DocumentoDAOBean#obtenerTipoDocumentoAvaluoAdicional");
        }

        return tiposDocumento;
    }

}
