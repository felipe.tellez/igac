package co.gov.igac.snc.dao.formacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.formacion.ParametroDecretoCondicion;

/**
 * Interfaz para los servicios de persistencia del objeto {@link ParametroDecretoCondicion}.
 *
 * @author david.cifuentes
 */
@Local
public interface IParametroDecretoCondicionDAO extends
    IGenericJpaDAO<ParametroDecretoCondicion, Long> {

    /**
     * Método que consulta todos los {@link ParametroDecretoCondicion}.
     *
     * @author david.cifuentes
     */
    public List<ParametroDecretoCondicion> buscarParametroDecretoCondicionAll();

}
