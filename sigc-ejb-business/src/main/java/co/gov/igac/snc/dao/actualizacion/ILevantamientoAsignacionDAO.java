package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;

/**
 * Servicios de persistencia para el objeto LevantamientoAsignacion
 *
 * @author javier.barajas
 */
@Local
public interface ILevantamientoAsignacionDAO extends IGenericJpaDAO<LevantamientoAsignacion, Long> {

    /**
     * Método que devuelve un objeto LevantamientoAsignacion por el topografo identificación
     *
     * @param topografoIdentificacion
     * @return LevantamientoAsignacion
     * @cu 212 Efectuar Control de Calidad a Levantamiento Topografico
     * @version 1.0
     * @author javier.barajas
     *
     */
    public LevantamientoAsignacion buscarLevantamientoAsignacionPorTopografoIdentificacion(
        String topografoIdentificacion);

    /**
     * Método que devuelve un objeto LevantamientoAsignacion por el id de recursoHumano
     *
     * @param recursoHumano
     * @return LevantamientoAsignacion
     * @cu 212 Efectuar Control de Calidad a Levantamiento Topografico
     * @version 1.0
     * @author javier.barajas
     *
     */
    public LevantamientoAsignacion buscarLevantamientoAsignacionPorRecursoHumanoId(
        Long recursoHumanoId);

}
