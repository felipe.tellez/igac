/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;

import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface IDetalleCapturaOfertaDAO extends IGenericJpaDAO<DetalleCapturaOferta, Long> {

    /**
     * Método que carga los códigos de las manzanas/veredas asociadas a un recolector determinado.
     *
     *
     * @author ariel.ortiz
     * @param idRecolector - id del recolector del cual se quieren extraer las manzanas veredas.
     * @param idAreaCapturaOferta - id del área asignada al recolector
     * @return
     */
    public List<String> cargarDetallesAreasPorRecolector(String idRecolector, Long idRegion);

    /**
     * Método para cargar todas las manzanas asociadas a una región de captura de ofertas
     *
     * @author ariel.ortiz
     * @param regionCapturaOfertaList
     */
    public List<String> cargarManzanasPorRegionesAsociadas(
        List<Long> regionIdList);

    /**
     * Método que carga los códigos de las manzanas/veredas asociadas a un area.
     *
     *
     * @author rodrigo.hernandez
     * @param idArea - id del AreaCapturaOferta relacionada
     * @return
     */
    List<DetalleCapturaOferta> cargarDetallesCapturaOfertaPorArea(Long idArea);

}
