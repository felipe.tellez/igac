/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IMunicipioComplementoDAO;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import javax.ejb.Stateless;

/**
 *
 * @author felipe.cadena
 */
@Stateless
public class MunicipioComplementoDAOBean extends
    GenericDAOWithJPA<MunicipioComplemento, String> implements
    IMunicipioComplementoDAO {

}
