package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IGrupoProductoDAO;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;

/**
 * Implementación de los servicios de persistencia del objeto GrupoProducto.
 *
 * @author javier.barajas
 */
@Stateless
public class GrupoProductoDAOBean extends GenericDAOWithJPA<GrupoProducto, Long> implements
    IGrupoProductoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GrupoProductoDAOBean.class);

    /**
     * @see GrupoProductoDAOBean#obtenerTodosGrupoProductos
     * @author leidy.gonzalez
     */
    @Override
    public List<GrupoProducto> obtenerTodosGrupoProductos() {
        LOGGER.debug("Entra en GrupoProductoDAOBean#obtenerTodosGrupoProductos");

        List<GrupoProducto> answer = null;

        //felipe.cadena::20-12-2016::Se agrega condicion de activos para productos
        Query q = entityManager.createQuery("SELECT gp FROM GrupoProducto gp " +
            "WHERE gp.activo = 'SI'" +
            " ORDER BY gp.descripcion");
        try {
            answer = (List<GrupoProducto>) q.getResultList();

        } catch (NoResultException e) {
            LOGGER.error("GrupoProductoDAOBean#obtenerTodosGrupoProductos:" +
                "Error al consultar la lista de productos del SNC");
        }
        return answer;

    }

}
