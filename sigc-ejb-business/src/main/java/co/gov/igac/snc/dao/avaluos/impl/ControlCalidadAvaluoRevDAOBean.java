/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IControlCalidadAvaluoRevDAO;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IControlCalidadAvaluoRevDAO
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class ControlCalidadAvaluoRevDAOBean extends
    GenericDAOWithJPA<ControlCalidadAvaluoRev, Long> implements
    IControlCalidadAvaluoRevDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadAvaluoRev.class);

    /**
     * @see IControlCalidadAvaluoRevDAO#obtenerPorIdConAtributos(Long)
     * @author christian.rodriguez
     */
    @Override
    public ControlCalidadAvaluoRev obtenerPorIdConAtributos(
        Long idControlCalidadAvaluoRev) {

        ControlCalidadAvaluoRev revision = null;

        if (idControlCalidadAvaluoRev != null) {

            revision = this.findById(idControlCalidadAvaluoRev);

            if (revision != null) {
                Hibernate.initialize(revision.getControlCalidadAvaluo());
                Hibernate.initialize(revision.getControlCalidadAvaluo().getProfesionalAvaluos());
                Hibernate.initialize(revision.getControlCalidadEspecificacions());
                Hibernate.initialize(revision.getSoporteDocumento());
            }
        }

        return revision;
    }

    /**
     * @see IControlCalidadAvaluoRevDAO#calcularNumeroDevoluciones(Long)
     * @author felipe.cadena
     */
    @Override
    public Long calcularNumeroDevoluciones(Long idAvaluo, Long idAsignacion, String actividadCC) {
        Long resultado = null;

        String queryString;
        Query query;

        try {
            queryString = "SELECT count(ccar) " +
                " FROM ControlCalidadAvaluoRev ccar join ccar.controlCalidadAvaluo cca " +
                " WHERE " +
                " ccar.aprobo like 'OBSERVACIONES'" +
                " AND cca.avaluoId = :avaluoId " +
                " AND cca.asignacion.id = :asignacionId " +
                " AND cca.actividad = :actividad " +
                " GROUP BY cca.avaluoId ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("avaluoId", idAvaluo);
            query.setParameter("asignacionId", idAsignacion);
            query.setParameter("actividad", actividadCC);
            resultado = (Long) query.getSingleResult();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return resultado;
    }

}
