package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionNomenclaturaVia;

/**
 * Servicios de persistencia para el objeto {@link ActualizacionNomenclaturaVia}
 *
 * @author david.cifuentes
 */
@Local
public interface IActualizacionNomenclaturaViaDAO extends
    IGenericJpaDAO<ActualizacionNomenclaturaVia, Long> {

}
