/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAreaRegistrada;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoAreaRegistrada
 *
 * @author felipe.cadena
 */
@Local
public interface IAvaluoAreaRegistradaDAO extends IGenericJpaDAO<AvaluoAreaRegistrada, Long> {

    /**
     * Método para consultar las AvaluoAreaRegistrada relacionadas a un avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public List<AvaluoAreaRegistrada> buscarPorAvaluo(Long idAvaluo);

    /**
     * Método para obterner un area adoptada por is con los atributos
     *
     * @author felipe.cadena
     * @param id
     * @return
     */
    public AvaluoAreaRegistrada buscarPorIdConAtributos(Long id);

}
