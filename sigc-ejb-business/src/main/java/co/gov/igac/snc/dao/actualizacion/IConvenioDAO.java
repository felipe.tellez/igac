package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;

/**
 * Servicios de persistencia del objeto Convenio.
 *
 * @author jamir.avila.
 *
 */
@Local
public interface IConvenioDAO extends IGenericJpaDAO<Convenio, Long> {

    /**
     * Recupera el conjunto de convenios pertenecientes a una actualización dado su identificador.
     *
     * @param idActualizacion identificador de la actualización.
     * @return una lista de objetos Convenio que corresponden a la actualización indicada, mediante
     * su identificador, o null si no hay ninguno.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal durante la
     * operación del método.
     * @author jamir.avila.
     */
    public List<Convenio> recuperarConveniosPorIdActualizacion(long idActualizacion) throws
        ExcepcionSNC;
}
