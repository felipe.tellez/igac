/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.dao.actualizacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IValoresActualizacionDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ValoresActualizacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author felipe.cadena
 */
@Stateless
public class ValoresActualizacionDAOBean extends GenericDAOWithJPA<ValoresActualizacion, Long>
    implements IValoresActualizacionDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ValoresActualizacionDAOBean.class);

    /**
     * @author felipe.cadena
     * @see IValoresActualizacionDAO.obtenerPorMunicipioYDepartamento
     */
    @Override
    public List<ValoresActualizacion> obtenerPorMunicipioYDepartamento(String municipioCodigo,
        String deptoCodigo, String vigencia) {

        List<ValoresActualizacion> resultado = null;

        String query = "SELECT ma FROM ValoresActualizacion ma " +
            " JOIN FETCH ma.departamento d " +
            " JOIN FETCH ma.municipio m " +
            " WHERE m.codigo = :municipioCodigo " +
            " AND d.codigo = :deptoCodigo " +
            " AND to_char(ma.vigencia, 'YYYY') = :vigencia ";

        try {
            Query q = this.entityManager.createQuery(query);
            q.setParameter("municipioCodigo", municipioCodigo);
            q.setParameter("deptoCodigo", deptoCodigo);
            q.setParameter("vigencia", vigencia);

            resultado = (List<ValoresActualizacion>) (List<ValoresActualizacion>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "MunicipioActualizacionDAOBean#obtenerPorMunicipioYEstado");
        }

        return resultado;

    }

}
