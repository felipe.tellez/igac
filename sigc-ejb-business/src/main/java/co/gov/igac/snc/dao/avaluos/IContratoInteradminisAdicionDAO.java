package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradminisAdicion;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * Interfaz para los métodos de bd de la tabla ContratoInteradminisAdicion
 *
 * @author felipe.cadena
 */
@Local
public interface IContratoInteradminisAdicionDAO extends
    IGenericJpaDAO<ContratoInteradminisAdicion, Long> {

    /**
     * Método para recuperar las adiciones relacionadas a un contrato
     *
     * @author felipe.cadena
     *
     * @param idContrato - Id del contrato seleccionado
     * @return
     */
    public List<ContratoInteradminisAdicion> buscarAdicionesPorIdContrato(Long idContrato);

}
