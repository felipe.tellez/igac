package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;

/**
 * Implementaciôn del DAO para LogMensaje
 *
 * @author juan.mendez
 *
 */
@Stateless
public class LogMensajeDAOBean extends GenericDAOWithJPA<LogMensaje, Long> implements ILogMensajeDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogMensajeDAOBean.class);

}
