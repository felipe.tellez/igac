package co.gov.igac.snc.dao.tramite;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;

/**
 * Interfaz para los servicios de persistencia del objeto {@link VSolicitudPredio}.
 *
 * @author david.cifuentes
 */
@Local
public interface IVSolicitudPredioDAO extends
    IGenericJpaDAO<VSolicitudPredio, Long> {

    /**
     * Método que recibe parametros para la busqueda de un {@link VSolicitudPredio}.
     *
     * @author david.cifuentes
     *
     * @param String numeroPredialBusqueda, VSolicitudPredio datosFiltroSolicitud
     * @return VSolicitudPredio
     */
    public List<VSolicitudPredio> buscarSolicitudPorFiltros(
        VSolicitudPredio datosFiltroSolicitud, int desde, int cantidad);

    /**
     * Método que busca una solicitud por filtro de busqueda
     *
     * @author juan.agudelo
     * @version 2.0
     * @param datosFiltroSolicitud
     * @param rowStartIdxAndCount
     * @return
     */
    public List<VSolicitudPredio> findSolicitudesPaginadasByFiltro(
        VSolicitudPredio datosFiltroSolicitud, String sortField, String sortOrder,
        Map<String, String> filters,
        final int... rowStartIdxAndCount);

    /**
     * Método que busca una solicitud por filtro de busqueda
     *
     * @author juan.agudelo
     * @version 2.0
     * @param datosFiltroSolicitud
     * @return
     */
    public Integer countSolicitudesPaginadasByFiltro(
        VSolicitudPredio datosFiltroSolicitud);

}
