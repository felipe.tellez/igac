/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.gestion.IAvisoRegistroRechazoPredioDAO;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class AvisoRegistroRechazoPredioDAOBean
    extends GenericDAOWithJPA<AvisoRegistroRechazoPredio, Long>
    implements IAvisoRegistroRechazoPredioDAO {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(AvisoRegistroRechazoPredioDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvisoRegistroRechazoPredioDAO#findByPredioId(java.lang.Long)
     */
    public List<AvisoRegistroRechazoPredio> findByPredioId(Long predioId) {

        LOGGER.debug("on AvisoRegistroRechazoPredioDAOBean#findByPredioId ...");
        List<AvisoRegistroRechazoPredio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT arrp FROM AvisoRegistroRechazoPredio arrp " +
            " JOIN FETCH arrp.predio" +
            " WHERE arrp.predio.id = :predioIdP";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("predioIdP", predioId);
            answer = query.getResultList();

            LOGGER.error("... finished");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return answer;

    }

}
