package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.ILevantamientoAsignacionInfDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacionInf;

/**
 * Implementación de los servicios de persistencia del objeto LevantamientoAsignacionInf.
 *
 * @author javier.barajas
 */
@Stateless
public class LevantamientoAsignacionInfDAOBean extends GenericDAOWithJPA<LevantamientoAsignacionInf, Long>
    implements ILevantamientoAsignacionInfDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(LevantamientoAsignacionInfDAOBean.class);

    /**
     * @seeILevantamientoAsignacionInfDAO#obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId(String)
     * @author javier.barajas
     */
    @Override
    public LevantamientoAsignacionInf obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId(
        Long levantamientoAsignacionId) {
        LOGGER.debug(
            "Entra en LevantamientoAsignacionDAOBean#obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId");

        LevantamientoAsignacionInf answer = null;

        Query q = entityManager
            .createQuery("SELECT lai" +
                " FROM LevantamientoAsignacionInf lai " +
                " WHERE lai.levantamientoAsignacion.id = :levantamientoAsignacionId");
        q.setParameter("levantamientoAsignacionId", levantamientoAsignacionId);

        try {
            answer = (LevantamientoAsignacionInf) q.getSingleResult();

            return answer;
        } catch (NoResultException e) {
            LOGGER.error(
                "LevantamientoAsignacionDAOBean#obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId:" +
                "Error al consultar el informe de  levantamiento asignacion ");
            return null;
        }

    }

}
