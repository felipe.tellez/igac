package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.sigc.documental.DocumentosServiceExceptions;
import co.gov.igac.snc.comun.anotaciones.Implement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IFotoDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class FotoDAOBean extends GenericDAOWithJPA<Foto, Long> implements IFotoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(FotoDAOBean.class);

    @EJB
    private IDocumentoDAO documentoService;

//--------------------------------------------------------------------------------------------------
    /**
     * Recupera la lista de fotos asociadas a un Predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<Foto> buscarFotografiasPredio(Long predioId) {

        String queryString;
        Query q;

        queryString = "SELECT f FROM Foto f" + " JOIN FETCH f.documento" +
            " LEFT JOIN FETCH f.predio fp LEFT JOIN FETCH f.unidadConstruccion uc " +
            " WHERE fp.id = :predioId";
        q = this.entityManager.createQuery(queryString);
        q.setParameter("predioId", predioId);
        try {
            List<Foto> fotos = (List<Foto>) q.getResultList();

            if (fotos != null && !fotos.isEmpty()) {

                for (Foto fTmp : fotos) {
                    if (fTmp.getDocumento() != null &&
                        fTmp.getDocumento().getIdRepositorioDocumentos() != null &&
                        !fTmp.getDocumento()
                            .getIdRepositorioDocumentos().isEmpty()) {
                    }
                }
            }
            return fotos;
        } catch (ExcepcionSNC e) {

            if (e.getCodigoDeExcepcion().equals(DocumentosServiceExceptions.EXCEPCION_0003.
                getCodigoDeExcepcion())) {
                throw DocumentosServiceExceptions.EXCEPCION_0003.getExcepcion(
                    LOGGER, e, "Error al consultar documentos en alfresco");

            } else {
                throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                    LOGGER, e, e.getMessage(),
                    "FotoDAOBean#buscarFotografiasPredio");
            }

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IFotoDAO#buscarFotosPredioPorComponenteDeConstruccion(Long, String)
     * @author juan.agudelo
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<String> buscarFotosPredioPorComponenteDeConstruccion(
        Long predioId, String componenteConstruccion) {

        List<String> imagenesUC = null;
        List<Documento> documentos = null;
        Query query;
        String q, ruta;

        q = "SELECT d FROM Foto f" + " JOIN f.documento d" + " JOIN f.predio p" +
            " WHERE p.id = :predioId" +
            " AND f.tipo = :componenteConstruccion";

        query = entityManager.createQuery(q);
        query.setParameter("predioId", predioId);
        if (componenteConstruccion == null) {
            query.setParameter("componenteConstruccion",
                EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo());
        } else {
            query.setParameter("componenteConstruccion", componenteConstruccion);
        }

        try {
            documentos = (List<Documento>) query.getResultList();

            if (documentos != null && !documentos.isEmpty()) {
                imagenesUC = new ArrayList<String>();

                for (Documento d : documentos) {

                    if (d.getIdRepositorioDocumentos() != null &&
                        !d.getIdRepositorioDocumentos().isEmpty()) {

//TODO :: hacer que retorne la url publica del archivo ñluergo de descargarlo en la publica                        
                        ruta = this.documentoService
                            .descargarOficioDeGestorDocumentalATempLocalMaquina(d
                                .getIdRepositorioDocumentos());
                        if (ruta != null) {
                            imagenesUC.add(ruta);
                        }
                    }
                }
            }

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FotoDAOBean#buscarFotosPredioPorComponenteDeConstruccion");
        }

        return imagenesUC;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IFotoDAO#guardarFotos(List)
     * @author juan.agudelo
     */
    @Implement
    @Override
    public List<Foto> guardarFotos(List<Foto> fotos) {

        List<Foto> fotosResultado = new ArrayList<Foto>();
        try {
            for (Foto foto : fotos) {
                fotosResultado.add(update(foto));
            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(), "FotoDAOBean#guardarFotos");
        }

        return fotosResultado;
    }
//-------------------------------------------------------------------------

    /**
     * @see IFotoDAO#buscarFotografiaPorIdPredioAndFotoTipo(Long, String)
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<Foto> buscarFotografiaPorIdPredioAndFotoTipo(Long predioId, String codigoTipoFoto) {

        List<Foto> fotos = null;

        String query = "SELECT f FROM Foto f JOIN FETCH f.documento" +
            " INNER JOIN FETCH f.predio fp WHERE fp.id = :predioId" +
            " AND f.tipo = :codigoTipoFoto ";

        try {

            Query q = entityManager.createQuery(query);

            q.setParameter("predioId", predioId);
            q.setParameter("codigoTipoFoto", codigoTipoFoto);

            fotos = (List<Foto>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FotoDAOBean#buscarFotografiaPorIdPredioAndFotoTipo");
        }
        return fotos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IFotoDAO#buscarPorIdUnidadConstruccionYTipo(Long, String)
     * @author felipe.cadena
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Foto> buscarPorIdUnidadConstruccionYTipo(Long unidadId, String codigoTipoFoto) {

        List<Foto> fotos = null;

        String query = "SELECT f FROM Foto f " +
            " WHERE f.unidadConstruccion.id = :unidadId" +
            " AND f.tipo = :codigoTipoFoto ";

        try {

            Query q = this.entityManager.createQuery(query);

            q.setParameter("unidadId", unidadId);
            q.setParameter("codigoTipoFoto", codigoTipoFoto);

            fotos = (List<Foto>) q.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FotoDAOBean#buscarFotografiaPorIdPredioAndFotoTipo");
        }
        return fotos;
    }
}
