package co.gov.igac.snc.dao.sig;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.util.SNCPropertiesUtil;
import co.gov.igac.snc.util.exceptions.RemoteServiceException;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.ImagenPredioVO;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;

/**
 * Dao para acceso a los servicios SIG asociados al sistema nacional catastral.
 * El acceso se realiza a través de REST
 * 
 * Los servicios SIG de SNC pueden ser de dos tipos:
 * 
 * 1. Servicios SIG publicados en Arcgis Server. (Este tipo de servicios
 * realizan geoprocessing para validar reglas de negocio o para realizar
 * exportación de datos)
 * 
 * 2. Servicios SIG publicados en el servidor de productos de SNC. (Este tipo de
 * servicios generan imágenes con cartografía avanzada).
 * 
 * @author juan.mendez
 * 
 */
public class SigDAOv2 {

	private static final Logger LOGGER = LoggerFactory.getLogger(SigDAOv2.class);

	private static final int LONG_JOB_EXECUTION_DELAY = 20000;// milisegundos
	private static final int SHORT_JOB_EXECUTION_DELAY = 5000;// milisegundos

	/**
	 * Máxima cantidad de veces que intenta ejecutar el geoservicio
	 */
	private final int MAX_ATTEMPTS = 10;

	private static SigDAOv2 instance;

	private String agsServerUrl;
	private String agsServerEnv;

	private String agsTokenUrl;
	private String agsTokenUser;
	private String agsTokenPwd;
	private int agsTokenMinutesRenew;

	/**
	 * 
	 */
	private SigDAOv2() {
		try {
			this.agsServerUrl = SNCPropertiesUtil.getInstance().getProperty("arcgis1031.server.url");
			this.agsServerEnv = SNCPropertiesUtil.getInstance().getProperty("arcgis1031.server.env");
			this.agsTokenUrl = agsServerUrl + "tokens/generateToken?";
			this.agsTokenUser = SNCPropertiesUtil.getInstance().getProperty("arcgis1031.server.token.user");
			this.agsTokenPwd = SNCPropertiesUtil.getInstance().getProperty("arcgis1031.server.token.pwd");
			this.agsTokenMinutesRenew = Integer.parseInt(SNCPropertiesUtil.getInstance().getProperty("arcgis1031.server.token.minutes.renew"));

			LOGGER.debug("agsServerUrl:" + this.agsServerUrl);
			LOGGER.debug("agsServerEnv:" + this.agsServerEnv);
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100006.getExcepcion(LOGGER, e, e.getMessage());
		}
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * 
	 * @return
	 * @throws ExcepcionSNC
	 */
	public static synchronized SigDAOv2 getInstance() throws ExcepcionSNC {
		if (instance == null) {
			instance = new SigDAOv2();
		}
		return instance;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Obtiene el token de seguridad del servidor SIG (Arquitectura de Arcgis
	 * Server, Usuario configurado en la plataforma) El método utiliza el
	 * servicio obtenerToken de ArcGisServerTokenRESTClient. Dicho método
	 * intenta obtener varias veces el token en el caso de error.
	 * 
	 * @param usuario
	 * @return
	 */
	public ArcgisServerTokenDTO obtenerToken(UsuarioDTO usuario) {
		ArcgisServerTokenDTO token = null;
		try {
			if (usuario == null || usuario.getLogin().trim().equals("")) {
				throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "usuario");
			}
			ArcGisServerTokenRESTClient client = new ArcGisServerTokenRESTClient(this.agsTokenUrl, this.agsTokenUser,this.agsTokenPwd);
			token = client.obtenerToken();
			token.setMinutesBeforeRenewal(agsTokenMinutesRenew);
		} catch (ExcepcionSNC e) {
			throw e;
		}
		return token;
	}

    /**
     *
     * @param codigosPrediales
     * @param usuario
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<ImagenPredioVO> obtenerImagenSimplePredio(String codigosPrediales,
        UsuarioDTO usuario) {
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Validaciones
        if (codigosPrediales == null || codigosPrediales.trim().equals("")) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                "codigosPrediales");
        }
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        List<ImagenPredioVO> datos = null;
        Document document = callObtenerImagenSimple(codigosPrediales, usuario);
        datos = new ArrayList<ImagenPredioVO>();
        List versiones = document.selectNodes("//Image");
        for (Iterator iter = versiones.iterator(); iter.hasNext();) {
            Element image = (Element) iter.next();
            String numeroPredial = image.attributeValue("codigo_predio");
            String nombreArchivo = image.attributeValue("filename");
            String imagenStringBase64 = image.getText();
            ImagenPredioVO vo = new ImagenPredioVO();
            vo.setNumeroPredial(numeroPredial);
            vo.setNombreArchivo(nombreArchivo);
            vo.setImagenStringBase64(imagenStringBase64);
            datos.add(vo);
        }
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        return datos;
    }

    /**
     *
     * @param numerosPrediales
     * @param usuario
     * @return
     */
    private Document callObtenerImagenSimple(final String numerosPrediales, final UsuarioDTO usuario) {
        // https://github.com/rholder/guava-retrying
        Callable<Document> callable = new Callable<Document>() {
            public Document call() throws Exception {
                LOGGER.debug("call");
                ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
                ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,
                    agsServerEnv, SHORT_JOB_EXECUTION_DELAY, token);
                Document document = client.obtenerImagenSimplePredio(numerosPrediales);
                return document;
            }
        };

        Retryer<Document> retryer = RetryerBuilder.<Document>newBuilder()
            .retryIfExceptionOfType(IOException.class)
            .retryIfExceptionOfType(SocketException.class)
            .retryIfExceptionOfType(RemoteServiceException.class)
            .withWaitStrategy(WaitStrategies.fixedWait(5, TimeUnit.SECONDS))
            .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
            .build();
        Document document = null;
        try {
            document = retryer.call(callable);
        } catch (RetryException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return document;
    }

    /**
     *
     * @param codigosPrediales
     * @param usuario
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<ImagenPredioVO> obtenerImagenAvanzadaPredio(String codigosPrediales,
        UsuarioDTO usuario) {
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Validaciones
        if (codigosPrediales == null || codigosPrediales.trim().equals("")) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                "codigosPrediales");
        }
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        Document document = callObtenerImagenAvanzada(codigosPrediales, usuario);
        List<ImagenPredioVO> datos = null;
        datos = new ArrayList<ImagenPredioVO>();
        List versiones = document.selectNodes("//Image");
        for (Iterator iter = versiones.iterator(); iter.hasNext();) {
            Element image = (Element) iter.next();
            String numeroPredial = image.attributeValue("codigo_predio");
            String nombreArchivo = image.attributeValue("filename");
            String imagenStringBase64 = image.getText();
            ImagenPredioVO vo = new ImagenPredioVO();
            vo.setNumeroPredial(numeroPredial);
            vo.setNombreArchivo(nombreArchivo);
            vo.setImagenStringBase64(imagenStringBase64);
            datos.add(vo);
        }
        return datos;
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    /**
     *
     * @param numerosPrediales
     * @param usuario
     * @return
     */
    private Document callObtenerImagenAvanzada(final String numerosPrediales,
        final UsuarioDTO usuario) {
        // https://github.com/rholder/guava-retrying
        Callable<Document> callable = new Callable<Document>() {
            public Document call() throws Exception {
                LOGGER.debug("call");
                ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
                ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,
                    agsServerEnv, SHORT_JOB_EXECUTION_DELAY, token);
                Document document = client.obtenerImagenAvanzadaPredio(numerosPrediales);
                return document;
            }
        };

        Retryer<Document> retryer = RetryerBuilder.<Document>newBuilder()
            .retryIfExceptionOfType(IOException.class)
            .retryIfExceptionOfType(SocketException.class)
            .retryIfExceptionOfType(RemoteServiceException.class)
            .withWaitStrategy(WaitStrategies.fixedWait(5, TimeUnit.SECONDS))
            .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
            .build();
        Document document = null;
        try {
            document = retryer.call(callable);
        } catch (RetryException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return document;
    }

    /**
     * Envía Job para la eliminación de la versión SDE asociada a un trámite (Ejecución asincrónica)
     *
     * @param jobDAO
     * @param codigoMunicipio
     * @param tramite
     * @param usuario
     */
    public void enviarJobEliminarVersion(IProductoCatastralJobDAO jobDAO, String codigoMunicipio,
        Tramite tramite, UsuarioDTO usuario) {
        if (codigoMunicipio == null || "".equals(codigoMunicipio)) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                "codigoMunicipio");
        }
        if (tramite == null) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                "numeroTramite");
        }
        if (!(codigoMunicipio.length() == 5)) {
            throw SncBusinessServiceExceptions.EXCEPCION_100014.getExcepcion(LOGGER, null,
                "codigoMunicipio", codigoMunicipio,
                "El código de municipio debe tener 5 caracteres");
        }
        String nombreVersion = "tr_" + codigoMunicipio + "_" + tramite.getId().toString();
        ProductoCatastralJob job = jobDAO.
            crearJobEliminarVersionSDE(nombreVersion, usuario, tramite);
        callEliminarVersion(job.getId(), usuario);
    }

    /**
     *
     * @param idJob
     * @param usuario
     */
    private void callEliminarVersion(final Long idJob, final UsuarioDTO usuario) {
        // https://github.com/rholder/guava-retrying
        Callable<Boolean> callable = new Callable<Boolean>() {
            public Boolean call() throws Exception {
                LOGGER.debug("call");
                ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
                ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,
                    agsServerEnv, SHORT_JOB_EXECUTION_DELAY, token);
                client.enviarJobEliminarVersionTramite(idJob);
                return true;
            }
        };

        Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
            .retryIfExceptionOfType(IOException.class)
            .retryIfExceptionOfType(SocketException.class)
            .retryIfExceptionOfType(RemoteServiceException.class)
            .withWaitStrategy(WaitStrategies.fixedWait(5, TimeUnit.SECONDS))
            .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
            .build();
        try {
            retryer.call(callable);
        } catch (RetryException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        }
    }

    /**
     * Obtiene las zonas homogeneas de los predios que se encuentren en el archivo de la réplica
     * para edición
     *
     * @param predios
     * @param nombreZipReplica
     * @param usuario
     * @return
     */
    public HashMap<String, List<ZonasFisicasGeoeconomicasVO>> obtenerZonasHomogeneas(
        List<PPredio> predios, String nombreZipReplica, UsuarioDTO usuario) {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Validaciones
        String identificadoresPredios = "";
        for (PPredio pp : predios) {
            identificadoresPredios += pp.getNumeroPredial() + ",";
        }
        if (identificadoresPredios == null || identificadoresPredios.equals("")) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                "identificadoresPredios");
        }
        return callObtenerZonas(identificadoresPredios, nombreZipReplica, usuario);
    }

    /**
     * Llamado al servicio que obtiene las zonas utilizando reintentos en el caso que ocurra error
     * en el servidor remoto
     *
     * @param identificadoresPredios
     * @param nombreZipReplica
     * @param usuario
     * @return
     */
    private HashMap<String, List<ZonasFisicasGeoeconomicasVO>> callObtenerZonas(
        final String identificadoresPredios,
        final String nombreZipReplica, final UsuarioDTO usuario) {
        // https://github.com/rholder/guava-retrying
        Callable<HashMap<String, List<ZonasFisicasGeoeconomicasVO>>> callable =
            new Callable<HashMap<String, List<ZonasFisicasGeoeconomicasVO>>>() {
            public HashMap<String, List<ZonasFisicasGeoeconomicasVO>> call() throws Exception {
                LOGGER.debug("call");
                ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
                ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,
                    agsServerEnv, SHORT_JOB_EXECUTION_DELAY, token);
                Document xmlDocument = client.obtenerZonasHomogeneas(identificadoresPredios,
                    nombreZipReplica);
                HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos = SigDAOUtil.
                    procesarRespuestaObtenerZonasHomogeneas(identificadoresPredios, xmlDocument);
                return datos;
            }
        };

        Retryer<HashMap<String, List<ZonasFisicasGeoeconomicasVO>>> retryer = RetryerBuilder.
            <HashMap<String, List<ZonasFisicasGeoeconomicasVO>>>newBuilder()
            .retryIfExceptionOfType(IOException.class)
            .retryIfExceptionOfType(SocketException.class)
            .retryIfExceptionOfType(RemoteServiceException.class)
            .withWaitStrategy(WaitStrategies.fixedWait(5, TimeUnit.SECONDS))
            .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
            .build();

        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> datos = null;
        try {
            datos = retryer.call(callable);
        } catch (RetryException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return datos;
    }

    /**
     *
     * @param jobDAO
     * @param predios
     * @param nombreZipReplica
     * @param usuario
     * @param tramite
     * @param tipoMutacion
     * @return
     */
    public ProductoCatastralJob obtenerZonasAsync(IProductoCatastralJobDAO jobDAO,
        List<PPredio> predios, String nombreZipReplica, UsuarioDTO usuario, Tramite tramite,
        String tipoMutacion) {
        ProductoCatastralJob job = null;
        String identificadoresPredios = "";
        for (PPredio pp : predios) {
			identificadoresPredios += pp.getNumeroPredial() + ",";
		}
        try {            
            
            // Valida el tramite y la lista de predios
            if (tramite == null) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "numeroTramite");
            }
            if (identificadoresPredios == null || identificadoresPredios.trim().equals("") ||
                identificadoresPredios.split(",").length == 0) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                    "identificadoresPredios");
            }
            
            if (nombreZipReplica == null || nombreZipReplica.trim().equals("")|| nombreZipReplica.split(",").length == 0) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "nombreZipReplica");
            }
            //Crea el job
            job = jobDAO.crearJobObtenerZonasHomogeneas(identificadoresPredios, tramite, usuario, tipoMutacion,nombreZipReplica);
            
            // Envia el job al servidor geografico
            job = obtenerZonasAsync(jobDAO, job, usuario);
                        
            return job;            
        } catch (ExcepcionSNC e) {
            throw e;
        }
    }
	
	/**
	 * 
	 * @param jobDAO
	 * @param job
	 * @param usuario
	 * @return
	 */
	public ProductoCatastralJob obtenerZonasAsync(IProductoCatastralJobDAO jobDAO, ProductoCatastralJob job, UsuarioDTO usuario) {
		callObtenerZonasAsync(job, usuario);
		return job;
	}
	
	/**
	 * 
	 * @param job
	 * @param usuario
	 */
	private void callObtenerZonasAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		// https://github.com/rholder/guava-retrying
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.obtenerZonasHomogeneasAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
	
		
	
	/**
	 * Crea y envía un job para la generación asincrónica de la Carta Catastral Urbana de Conservación
	 *  
	 * @param jobDAO
	 * @param codigoManzana
	 * @param generarEnPDF
	 * @param tramite
         * @param productoCatastralDetalle
	 * @param usuario
	 */
	public void generarCCUAsync(IProductoCatastralJobDAO jobDAO, String codigoManzana, boolean generarEnPDF, Tramite tramite,
			ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario) {
		LOGGER.debug("generarCartaCatastralUrbanaConservacionAsync");
		// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Validaciones
		if (codigoManzana == null || codigoManzana.trim().equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "codigoManzana");
		}
		ProductoCatastralJob job = jobDAO.crearJobGenerarCartaCatastralUrbana(codigoManzana,generarEnPDF,tramite,productoCatastralDetalle, usuario);
		callGenerarCCUAsync(job, usuario);
	}

	/**
	 * Llamado al servicio remoto teniendo en cuenta reintentos por problemas en la red.
	 * 
	 * @param job
	 * @param usuario
	 */
	private void callGenerarCCUAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.generarCCUAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
	
	
    /**
     * Crea y envía un job para la generación asincrónica de la Imagen del predio
     *
     * @param jobDAO
     * @param codigoPredio
     * @param tramite
     * @param productoCatastralDetalle
     * @param usuario
     */
    public void generarImagenPredial(IProductoCatastralJobDAO jobDAO, String codigoPredio,Tramite tramite,
			ProductoCatastralDetalle productoCatastralDetalle,  UsuarioDTO usuario) {
        LOGGER.debug("generarImagenPredial");
	
        ProductoCatastralJob job = jobDAO.crearJobGenerarImagenPredial(codigoPredio, tramite, productoCatastralDetalle, usuario);
        callGenerarImagenPredioAsync(job, usuario);
    }       
    
    /**
     * Crea y envía un job para la generación asincrónica de la Imagen del predio
     *
     * @param jobDAO
     * @param codigoPredio
     * @param tramite
     * @param productoCatastralDetalle
     * @param usuario
     */
    public void reSendGenerarImagenPredioAsync(ProductoCatastralJob job, UsuarioDTO usuario) {
        LOGGER.debug("reSendGenerarImagenPredioAsync");

        callGenerarImagenPredioAsync(job, usuario);
    }
    
    /**
     * Llamado al servicio remoto teniendo en cuenta reintentos por problemas en la red.
     *
     * @param job
     * @param usuario
     */
    private void callGenerarImagenPredioAsync(final ProductoCatastralJob job, final UsuarioDTO usuario) {
        Callable<Boolean> callable = new Callable<Boolean>() {
            public Boolean call() throws Exception {
                LOGGER.debug("call");
                ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
                ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl, agsServerEnv, LONG_JOB_EXECUTION_DELAY, token);
                client.generarImagenPredioAsync(job.getId());
                return true;
            }
        };
        Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
                .retryIfExceptionOfType(IOException.class)
                .retryIfExceptionOfType(SocketException.class)
                .retryIfExceptionOfType(RemoteServiceException.class)
                .withWaitStrategy(WaitStrategies.fixedWait(5, TimeUnit.SECONDS))
                .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
                .build();
        try {
            retryer.call(callable);
        } catch (RetryException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
        }
    }
	
    
	
	
	/**
	 * Crea y envía un job para la generación asincrónica de la Ficha Predial Digital
	 * 
	 * @param jobDAO
	 * @param codigoPredio
         * @param tramite
         * @param productoCatastralDetalle
	 * @param usuario
	 */
	public void generarFPDAsync(IProductoCatastralJobDAO jobDAO, String codigoPredio,Tramite tramite,
			ProductoCatastralDetalle productoCatastralDetalle,  UsuarioDTO usuario) {
		LOGGER.debug("generarFPDAsync");
		// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Validaciones
		if (codigoPredio == null || codigoPredio.trim().equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "codigoPredio");
		}
		ProductoCatastralJob job = jobDAO.crearJobGenerarFichaPredialDigital( codigoPredio,tramite, productoCatastralDetalle, usuario);
		callGenerarFPDAsync(job, usuario);
	}
        
	/**
	 * Llamado al servicio remoto teniendo en cuenta reintentos por problemas en la red.
	 * 
	 * @param job
	 * @param usuario
	 */
	private void callGenerarFPDAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.generarFPDAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
	
	
	/**
	 * Crea y envía un job para la generación asincrónica de la Ficha Predial Digital
	 * 
	 * @param jobDAO
	 * @param codigoPredio
         * @param tramite
         * @param productoCatastralDetalle
	 * @param usuario
	 */
	public void generarCPPAsync(IProductoCatastralJobDAO jobDAO, String codigoPredio,Tramite tramite,
			ProductoCatastralDetalle productoCatastralDetalle,  UsuarioDTO usuario) {
		LOGGER.debug("generarCPPAsync");
		// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Validaciones
		if (codigoPredio == null || codigoPredio.trim().equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "codigoPredio");
		}
		ProductoCatastralJob job = jobDAO.crearJobGenerarCertificadoPlanoPredial( codigoPredio,tramite, productoCatastralDetalle, usuario);
		callGenerarCPPAsync(job, usuario);
	}
	
	/**
	 * Llamado al servicio remoto teniendo en cuenta reintentos por problemas en la red.
	 * 
	 * @param job
	 * @param usuario
	 */
	private void callGenerarCPPAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.generarCPPAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
	
	/**
	 * Obtiene la colindancia para uno o más predios
	 * 
	 * @param numerosPrediales
	 * @param usuario
	 * @return
	 */
    public List<String> obtenerColindantesPredio(String numeroPredialPredio,  UsuarioDTO usuario, String jobID) {
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Validaciones
		if (numeroPredialPredio == null || numeroPredialPredio.trim().equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009	.getExcepcion(LOGGER, null, "numeroPredialPredio");
		}
		//Nota: El servicio AGS soporta varios códigos prediales pero los clientes del servicio solo pueden procesar la respuesta de un predio
        return callObtenerColindantes(numeroPredialPredio, ProductoCatastralJob.TIPO_COLINDANCIA_PREDIO, usuario, jobID);
	}
	
	/**
	 * Obtiene la colindancia para una Construcción
	 * 
	 * @param numerosPrediales
	 * @param usuario
	 * @return
	 */
    public List<String> obtenerColindantesConstruccion(String numeroPredialConstruccion,  UsuarioDTO usuario, String jobID) {
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Validaciones
		if (numeroPredialConstruccion == null || numeroPredialConstruccion.trim().equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009	.getExcepcion(LOGGER, null, "numeroPredialConstruccion");
		}
		//Nota: El servicio AGS soporta varios códigos prediales pero los clientes del servicio solo pueden procesar la respuesta de un predio
         return callObtenerColindantes(numeroPredialConstruccion, ProductoCatastralJob.TIPO_COLINDANCIA_CONSTRUCCIONES, usuario, jobID);
	}
	
	/**
	 * Llamado al servicio que obtiene las zonas utilizando reintentos en el caso que ocurra error en el servidor remoto
	 * 
	 * @param identificadoresPredios
	 * @param nombreZipReplica
	 * @param usuario
	 * @return
	 */
	
    private List<String> callObtenerColindantes(final String identificadoresPredios,final String tipoColindancia,  final UsuarioDTO usuario, final String jobID){

		// https://github.com/rholder/guava-retrying
		Callable<List<String>> callable = new Callable<List<String>>() {
		    public List<String> call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
				ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,SHORT_JOB_EXECUTION_DELAY, token);
                Document xmlDocument = client.obtenerColindantes(identificadoresPredios, tipoColindancia, jobID);
				//LOGGER.debug(xmlDocument.asXML());
				List<String> datos = SigDAOUtil.procesarRespuestaColindancia(xmlDocument);
		        return datos;
		    }
		};
		
		Retryer<List<String>> retryer = RetryerBuilder.<List<String>>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();

		List<String> datos =  null;
		try {
			datos = retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
		return datos;
	}
	
    
    /**
	 * Genera el job y envia la solicitud para calcular asincronamente las inconsistencias geograficas.
	 * @param jobDAO 
	 * @param predios lista de numeros prediales separada por comas	 
	 * @param usuario Usuario que esta solicitando la tarea geografica
	 * @param tramite tramite que asocia el job geografico 
	 * @param tipoMutacion identifica el tipo de mutacion correspondiente.
	 * @return un Producto_Catastral_Job
     * 
     * @author andres.eslava
	 */
	public ProductoCatastralJob validarInconsistenciasAsync(IProductoCatastralJobDAO jobDAO,String predios,UsuarioDTO usuario, Tramite tramite,String tipoMutacion,String radicacionEspecial) {
        ProductoCatastralJob job = null;
    
        try {            
    
            // Valida el tramite y la lista de predios
            if (tramite == null) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "numeroTramite");
            }
            if (predios == null || predios.trim().equals("")|| predios.split(",").length == 0) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "identificadoresPredios");
            }
            //Crea el job
            job = jobDAO.crearJobValidacionInconsistencias(predios, tramite, usuario, tipoMutacion, radicacionEspecial);
                       
            // Envia el job al servidor geografico
            job = validarInconsistenciasAsync(jobDAO, job, usuario);
                        
            return job;            
        } catch (ExcepcionSNC e) {
            throw e;
        }
    }
        
    /**
	 * Hace el envio de un job geografico
	 * @param jobDAO
	 * @param job
	 * @param usuario
	 * @return
     * 
     * @author andres.eslava
	 */
	public ProductoCatastralJob validarInconsistenciasAsync(IProductoCatastralJobDAO jobDAO, ProductoCatastralJob job, UsuarioDTO usuario) {
		callValidarInconsistenciasAsync(job, usuario);
		return job;
	}
	
	/**
	 * Valida las inconsistencias geograficas a partir de un job geografico      
	 * @param job job a procesar.
	 * @param usuario  usuario que esta enviando el job a ejecucion   
     * @author andres.eslava
	 */
	private void callValidarInconsistenciasAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		// https://github.com/rholder/guava-retrying
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("callValidarInconsistenciasAsync");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.validarInconsistenciasAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
    
    /**
	 * Obtiene las zonas homogeneas de los predios que se encuentren en el archivo de la réplica para edición
	 * 
	 * @param predios
	 * @param nombreZipReplica
	 * @param usuario
	 * @return
     * 
     * @author andres.eslava
	 */
	public HashMap<String,String> validarInconsistencias(String predios,  UsuarioDTO usuario) {				
		if (predios == null || predios.trim().equals("")|| predios.split(",").length == 0) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "identificadoresPredios");
            }
		if (predios == null || predios.equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009	.getExcepcion(LOGGER, null, "identificadoresPredios");
		}
		return callValidarInconsistencias(predios, usuario);
	}
	
	
	/**
	 * Llamado al servicio que obtiene las zonas utilizando reintentos en el caso que ocurra error en el servidor remoto
	 * 
	 * @param identificadoresPredios
	 * @param nombreZipReplica
	 * @param usuario
	 * @return
     * 
     * @author andres.eslava
	 */
	private HashMap<String,String>  callValidarInconsistencias(final String identificadoresPredios, final UsuarioDTO usuario){
		// https://github.com/rholder/guava-retrying
		Callable<HashMap<String,String>> callable = new Callable<HashMap<String,String>>() {
		    public HashMap<String, String> call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
				ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,SHORT_JOB_EXECUTION_DELAY, token);
				Document xmlDocument = client.validarInconsistencias(identificadoresPredios);
				HashMap<String, String> datos = SigDAOUtil.procesarRespuestasValidarInconsistencias(xmlDocument);
		        return datos;
		    }
		};
		
		Retryer<HashMap<String, String>> retryer = RetryerBuilder.<HashMap<String,String>>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();

		HashMap<String, String> datos =  null;
		try {
			datos = retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
		return datos;
	}
	/**
	 * Obtiene las zonas homogeneas de los predios que se encuentren en el archivo de la réplica para edición
	 * 
	 * @param predios
	 * @param usuario
	 * @return
     * 
     * @author andres.eslava
	 */
	public HashMap<String,String> validarGeometriaUnidadesConstruccion(String predios,  UsuarioDTO usuario) {
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Validaciones
		if (predios == null || predios.trim().equals("")|| predios.split(",").length == 0) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "identificadoresPredios");
            }
		if (predios == null || predios.equals("")) {
			throw SncBusinessServiceExceptions.EXCEPCION_100009	.getExcepcion(LOGGER, null, "identificadoresPredios");
		}
		return callValidarGeometriaUnidadesConstruccion(predios, usuario);
	}
	
	
	/**
	 * Llamado al servicio que obtiene las zonas utilizando reintentos en el caso que ocurra error en el servidor remoto
	 * 
	 * @param identificadoresPredios
	 * @param usuario
	 * @return
     * 
     * @author andres.eslava
	 */
	private HashMap<String,String>  callValidarGeometriaUnidadesConstruccion(final String identificadoresPredios, final UsuarioDTO usuario){
		// https://github.com/rholder/guava-retrying
		Callable<HashMap<String,String>> callable = new Callable<HashMap<String,String>>() {
		    public HashMap<String, String> call() throws Exception {
		    	LOGGER.debug("call");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
				ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,SHORT_JOB_EXECUTION_DELAY, token);
				Document xmlDocument = client.validarGeometriaUnidadConstruccion(identificadoresPredios);
				HashMap<String, String> datos = SigDAOUtil.procesarRespuestasValidarGeometriaUnidadConstruccion(xmlDocument);
		        return datos;
		    }
		};
		
		Retryer<HashMap<String, String>> retryer = RetryerBuilder.<HashMap<String,String>>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();

		HashMap<String, String> datos =  null;
		try {
			datos = retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
		return datos;
	}
	
	
	
	/**
	 * Crea un Job para exportación de datos y lo envía a los servicios SIG
	 * 
	 * @param jobDAO
	 * @param tramite
	 * @param identificadoresPredios
	 * @param tipoMutacion
	 * @param formatoArchivo
	 * @param usuario
	 * @return
	 * 
	 * @author juan.mendez
	 */
	public ProductoCatastralJob exportarDatosAsync(IProductoCatastralJobDAO jobDAO, Tramite tramite, String identificadoresPredios,
            String tipoMutacion, String formatoArchivo, UsuarioDTO usuario) {
        ProductoCatastralJob job = null;
        try {            
            // Valida el tramite y la lista de predios
            if (tramite == null) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "tramite");
            }
            if (identificadoresPredios == null || identificadoresPredios.trim().equals("") ||
                identificadoresPredios.split(",").length == 0) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null,
                    "identificadoresPredios");
            }
            //Crea el job
            job = jobDAO.crearJobExportarDatosEdicion(tramite, identificadoresPredios, tipoMutacion, formatoArchivo, usuario);
            // Envia el job al servidor geografico
            job = exportarDatosAsync(jobDAO, job, usuario);
            return job;            
        } catch (ExcepcionSNC e) {
            throw e;
        }
    }
	
    /**
     * Realiza el envío de un job geográfico ya existente
     * 
     * @param jobDAO
     * @param job
     * @param usuario
     * @return
     * 
     * @author juan.mendez
     */
	public ProductoCatastralJob exportarDatosAsync(IProductoCatastralJobDAO jobDAO, ProductoCatastralJob job, UsuarioDTO usuario) {
		callExportarDatosAsync(job, usuario);
		return job;
	}
	
	/**
	 * 
	 * @param job
	 * @param usuario
	 *      
	 * @author juan.mendez 
	 */
	private void callExportarDatosAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		// https://github.com/rholder/guava-retrying
		//TODO
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("callExportarDatosAsync");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.exportarDatosAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
	
	
	/**
	 * Crea y envía job para la aplicación de los cambios geográficos de un trámite 
	 * 
	 * @param jobDAO
	 * @param tramite
	 * @param idArchivoSistemaDocumental
	 * @param usuario
	 * @return
	 * 
	 * @author juan.mendez
	 */
	public ProductoCatastralJob aplicarCambiosAsync(IProductoCatastralJobDAO jobDAO, Tramite tramite, String idArchivoSistemaDocumental, 
			UsuarioDTO usuario) {
        ProductoCatastralJob job = null;
        try {            
            if (tramite == null) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "tramite");
            }
            if (idArchivoSistemaDocumental == null || idArchivoSistemaDocumental.trim().equals("")) {
                throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "idArchivoSistemaDocumental");
            }
            //Crea el job
            job = jobDAO.crearJobFinalizarEdicionTramite(tramite, idArchivoSistemaDocumental, usuario);
            LOGGER.debug("job:" + job);
            job = aplicarCambiosAsync(jobDAO, job, usuario);
            return job;            
        } catch (ExcepcionSNC e) {
            throw e;
        }
    }
	
    /**
     * Realiza el envío de un job geográfico de aplicación geográfico ya existente en la base de datos 
     * 
     * 
     * @param jobDAO
     * @param job
     * @param usuario
     * @return
     * 
     * @author juan.mendez
     */
	public ProductoCatastralJob aplicarCambiosAsync(IProductoCatastralJobDAO jobDAO, ProductoCatastralJob job, UsuarioDTO usuario) {
		callAplicarCambiosAsync(job, usuario);
		return job;
	}
	
	/**
	 * 
	 * @param job
	 * @param usuario
	 *      
	 * @author juan.mendez 
	 */
	private void callAplicarCambiosAsync(final ProductoCatastralJob job, final UsuarioDTO usuario){
		// https://github.com/rholder/guava-retrying
		//TODO
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("callExportarDatosAsync");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.aplicarCambiosAsync(job.getId());
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
	
	 
   /**
    * Envía el job de actualizar colindancia
    * @param jobDAO
    * @param predios
    * @param tramite
    * @param usuario 
    */
    public ProductoCatastralJob enviarJobActualizarColindancia(IProductoCatastralJobDAO jobDAO,String predios, Tramite tramite, UsuarioDTO usuario) {
        if (predios == null || "".equals(predios)) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "predios");
        }
        if (tramite == null) {
            throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "numeroTramite");
        }
        ProductoCatastralJob job = jobDAO.crearJobActualizarColindancia(predios, usuario, tramite);
        callActualizarColindancia(predios, usuario, job);
        
        return job;
    }

   /**
    * 
    * @param predios
    * @param usuario
    * @param job 
    */
    private void callActualizarColindancia(final String predios, final UsuarioDTO usuario, final ProductoCatastralJob job){
        // https://github.com/rholder/guava-retrying
        Callable<Boolean> callable = new Callable<Boolean>() {
            public Boolean call() throws Exception {
                LOGGER.debug("call");
                ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
                ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv, SHORT_JOB_EXECUTION_DELAY, token);
                client.enviarJobActualizarColindancia(predios, ProductoCatastralJob.TIPO_COLINDANCIA_ACTUALIZAR, job.getId().toString());
                return true;
            }
        };

        Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
                .retryIfExceptionOfType(IOException.class)
                .retryIfExceptionOfType(SocketException.class)
                .retryIfExceptionOfType(RemoteServiceException.class)
                .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
                .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
                .build();
        try {
            retryer.call(callable);
        } catch (RetryException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
        }
    }
    
    /**
	 * Hace el envio de un job geografico
	 * @param environment
	 * @param ubicacionArchivo
	 * @param validacionZonas
         * @param validacionPredios
	 * @return
     * 
     * @author hector.arias
	 */
        public ProductoCatastralJob validarInconsistenciasActualizacion(IProductoCatastralJobDAO jobDAO, String departamentoSeleccionado, String municipioSeleccionado,
                String ubicacionArchivo, String validacionZonas, String validacionPredios, UsuarioDTO usuario){
            ProductoCatastralJob job = null;
            
            String environment = this.agsServerEnv;
            
            try {
                if (environment == null) {
                    throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "environment");
                }
                if (ubicacionArchivo == null) {
                    throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "ubicacionArchivo");
                }
                if (validacionZonas == null) {
                    throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "validacionZonas");
                }
                if (validacionPredios == null) {
                    throw SncBusinessServiceExceptions.EXCEPCION_100009.getExcepcion(LOGGER, null, "validacionPredios");
                }
                
                //Crea el job
                job = jobDAO.crearJobValidacionInconsistenciasActualizacion(environment, ubicacionArchivo, validacionZonas, validacionPredios, usuario, departamentoSeleccionado, municipioSeleccionado);

                job = validarInconsistenciasActualizacion(jobDAO, job, usuario, job.getId(), environment, ubicacionArchivo, validacionZonas, validacionPredios);
                
                return job;
            } catch (ExcepcionSNC e) {
                throw  e;
            }
        }
	
        /**
	 * Hace el envio de un job geografico
         * @param jobDAO
         * @param job
         * @param usuario
         * @param idJob
	 * @param environment
	 * @param ubicacionArchivo
	 * @param validacionZonas
         * @param validacionPredios
	 * @return
     * 
     * @author hector.arias
	 */
	public ProductoCatastralJob validarInconsistenciasActualizacion(IProductoCatastralJobDAO jobDAO, ProductoCatastralJob job, UsuarioDTO usuario, Long idJob, String environment, String ubicacionArchivo, String validacionZonas, String validacionPredios) {
		callvalidarInconsistenciasActualizacion(job, usuario, environment, ubicacionArchivo, validacionZonas, validacionPredios);
		return job;
        }
        
        /**
         * 
         * @param job
         * @param usuario
         * @param environment
         * @param ubicacionArchivo
         * @param validacionZonas
         * @param validacionPredios 
         * 
         * @author hector.arias
         */
	private void callvalidarInconsistenciasActualizacion(final ProductoCatastralJob job, final UsuarioDTO usuario, final String environment, final String ubicacionArchivo, final String validacionZonas, final String validacionPredios){
		// https://github.com/rholder/guava-retrying
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	LOGGER.debug("callValidarInconsistenciasAsync");
		    	ArcgisServerTokenDTO token = SigDAOv2.getInstance().obtenerToken(usuario);
		    	ArcGisServer1031RESTClient client = new ArcGisServer1031RESTClient(agsServerUrl,  agsServerEnv,LONG_JOB_EXECUTION_DELAY, token);
				client.validarInconsistenciasActualizacion(job.getId(), environment,ubicacionArchivo, validacionZonas, validacionPredios);
		        return true;
		    }
		};
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfExceptionOfType(SocketException.class)
		        .retryIfExceptionOfType(RemoteServiceException.class)
		        .withWaitStrategy(WaitStrategies.fixedWait(5,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(MAX_ATTEMPTS))
		        .build();
		try {
			retryer.call(callable);
		} catch (RetryException e) {
			LOGGER.error(e.getMessage());
			throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		} catch (ExecutionException e) {
		    LOGGER.error(e.getMessage());
		    throw SncBusinessServiceExceptions.EXCEPCION_100007.getExcepcion(LOGGER, e, e.getMessage());
		}
	}
}
