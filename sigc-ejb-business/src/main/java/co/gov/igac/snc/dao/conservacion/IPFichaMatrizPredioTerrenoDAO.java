/*
 * Proyecto SNC 2016
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import javax.ejb.Local;

/**
 * Interfaz para las operacione en base de datos sobre la entidad PFichaMatrizPredioTerreno
 *
 * @author felipe.cadena
 */
@Local
public interface IPFichaMatrizPredioTerrenoDAO extends
    IGenericJpaDAO<PFichaMatrizPredioTerreno, Long> {

}
