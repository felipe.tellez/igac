package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IModeloResolucionDAO;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;

@Stateless
public class ModeloResolucionDAOBean extends GenericDAOWithJPA<ModeloResolucion, Long> implements
    IModeloResolucionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ModeloResolucionDAOBean.class);

    @SuppressWarnings("unchecked")
    /**
     * Metodo que consulta un {@link ModeloResolucion} teniendo la clase de tramite y tipo de
     * mutacion
     *
     * @see ITramiteDAO#findModelosResolucionByTramite(String tipoTramite , String claseMutacion)
     * @author david.cifuentes
     */
    @Override
    public List<ModeloResolucion> findModelosResolucionByTramite(
        String tipoTramite, String claseMutacion) {

        LOGGER.debug("findModelosResolucionByTramite");
        List<ModeloResolucion> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT mr FROM ModeloResolucion mr " +
            " WHERE mr.tipoTramite = :tipoTramite " +
            " AND (mr.claseMutacion = :claseMutacion OR mr.claseMutacion IS NULL) " +
            " ORDER BY mr.id ASC ";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("tipoTramite", tipoTramite);
        query.setParameter("claseMutacion", claseMutacion);

        answer = (List<ModeloResolucion>) query.getResultList();

        return answer;
    }

    @SuppressWarnings("unchecked")
    /**
     * Metodo que consulta un {@link ModeloResolucion} teniendo el tipo de tramite, clase de
     * mutacion y subtipo
     *
     * @see ITramiteDAO#findModelosResolucionByTramite(String tipoTramite , String claseMutacion,
     * String subtipo)
     * @author fredy.wilches
     */
    @Override
    public List<ModeloResolucion> findModelosResolucionByTramite(
        String tipoTramite, String claseMutacion, String subtipo) {

        LOGGER.debug("findModelosResolucionByTramite");
        List<ModeloResolucion> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT mr FROM ModeloResolucion mr " +
            " WHERE mr.tipoTramite = :tipoTramite " +
            (claseMutacion != null ?
                " AND (mr.claseMutacion = :claseMutacion OR mr.claseMutacion IS NULL) " : "") +
            (subtipo != null ? " AND mr.subtipo= :subtipo" : "") +
            " AND mr.delegada is null ORDER BY mr.id ASC ";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("tipoTramite", tipoTramite);
        if (claseMutacion != null) {
            query.setParameter("claseMutacion", claseMutacion);
        }
        if (subtipo != null) {
            query.setParameter("subtipo", subtipo);
        }
        answer = (List<ModeloResolucion>) query.getResultList();

        return answer;
    }
    
    @SuppressWarnings("unchecked")
    /**
     * Metodo que consulta un {@link ModeloResolucion} teniendo el tipo de tramite, clase de
     * mutacion, subtipo y delegada
     *
     * @see ITramiteDAO#findModelosResolucionByTramite(String tipoTramite , String claseMutacion,
     * String subtipo, String delegada)
     * @author fredy.wilches
     */
    @Override
    public List<ModeloResolucion> findModelosResolucionByTramite(
        String tipoTramite, String claseMutacion, String subtipo, String delegada) {

        LOGGER.debug("findModelosResolucionByTramite");
        List<ModeloResolucion> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT mr FROM ModeloResolucion mr " +
            " WHERE mr.tipoTramite = :tipoTramite " +
            (claseMutacion != null ?
                " AND (mr.claseMutacion = :claseMutacion OR mr.claseMutacion IS NULL) " : "") +
            (subtipo != null ? " AND mr.subtipo= :subtipo" : "") +
            (delegada != null ? " AND mr.delegada= :delegada" : "") +
            " ORDER BY mr.id ASC ";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("tipoTramite", tipoTramite);
        if (claseMutacion != null) {
            query.setParameter("claseMutacion", claseMutacion);
        }
        if (subtipo != null) {
            query.setParameter("subtipo", subtipo);
        }
        if (delegada != null) {
            query.setParameter("delegada", delegada);
        }
        answer = (List<ModeloResolucion>) query.getResultList();

        return answer;
    }
}
