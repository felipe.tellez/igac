package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IReconocimientoNovedadDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ReconocimientoNovedad;

/**
 * Implementación de los servicios de persistencia del objeto ReconocimientoNovedad.
 *
 * @author javier.barajas
 */
@Stateless
public class ReconocimientoNovedadDAOBean extends GenericDAOWithJPA<ReconocimientoNovedad, Long>
    implements IReconocimientoNovedadDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReconocimientoNovedadDAOBean.class);

}
