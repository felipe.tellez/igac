package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class RepReporteDAOBean extends GenericDAOWithJPA<RepReporte, Long> implements 
		IRepReporteDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepReporteDAOBean.class);

	
	/**
	 * @see RepReporteDAOBean#obtenerListaTipoReportePorCategoria(String)
	 * @author leidy.gonzalez
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RepReporte> obtenerListaTipoReportePorCategoria(String categoria) {
		LOGGER.debug("Entra en RepReporteDAOBean#obtenerListaTipoReportePorCategoria");
		
		List<RepReporte> answer = null;
		
		Query q = entityManager.createQuery("SELECT rr FROM RepReporte rr WHERE rr.categoria = :categoria ORDER BY nombre ASC");
		q.setParameter("categoria", categoria);
		try {
			answer = (List<RepReporte>) q.getResultList();
		} catch (NoResultException e) {
			LOGGER.error("RepReporteDAOBean#obtenerListaTipoReportePorCategoria:" +
					"Error al consultar la lista de reportes");
		}
		return answer;
		
	}
	
	/**
	 * @see RepReporte#buscarReportePorId(Long idReporteConsultar)
	 * @author leidy.gonzalez
	 */
	@Override
	public RepReporte buscarReportePorId(Long idReporteConsultar){
		RepReporte answer = null;
		
		Query query;
		String queryToExecute;
		
		queryToExecute = "SELECT rre FROM RepReporte rre " +
				"where rre.id = :idReporteConsultar ";
		
		try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idReporteConsultar", idReporteConsultar);
            
			answer = (RepReporte) query.getSingleResult();
			
			
		} catch (NoResultException nrEx) {
			LOGGER.warn("RepReporteDAOBean#buscarReportePorId: "
					+ "La consulta de reporte de ejecucion no devolvió resultados");
		} catch (Exception e) {
			throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
					LOGGER, e, e.getMessage());
		}
		
		return answer;
	}
	
	/**
	 * @see RepReporteDAOBean#obtenerListaNombreTipoReporte()
	 * @author leidy.gonzalez
	 */
	@Override
	public List<String> obtenerListaNombreTipoReporte() {
		LOGGER.debug("Entra en RepReporteDAOBean#obtenerListaTipoReporte");
		
		List<String> answer = null;
		Query query;
		String q;
		
		q = "SELECT distinct rr.tipoReporte FROM RepReporte rr"
			+ " WHERE rr.categoria = :tipoReporte order by rr.tipoReporte asc";
		
		query = this.entityManager.createQuery(q);
		query.setParameter("tipoReporte", ERepReporteCategoria.REPORTES_PREDIALES.getCategoria());
		
		try {
			answer = (List<String>) query.getResultList();
		} catch (NoResultException e) {
			LOGGER.error("RepReporteDAOBean#obtenerListaTipoReporte:" +
					"Error al consultar la lista de reportes del SNC", e);
		}
		return answer;
		
	}
	
	/**
	 * @see RepReporteDAOBean#obtenerListaNombreTipoReporte()
	 * @author leidy.gonzalez
	 */
	@Override
	public List<RepReporte> consultarListaTipoReportePorNombre(List<String> nombresTiposReportes) {
		LOGGER.debug("Entra en RepReporteDAOBean#obtenerListaTipoReporte");
		
		List<RepReporte> answer = null;
		Query query;
		String q;
		
		q = "SELECT rr.id FROM RepReporte rr"
			+ " WHERE rr.tipoInforme IN (:nombresTiposReportes) ";
		
		query = this.entityManager.createQuery(q);
		query.setParameter("nombresTiposReportes", nombresTiposReportes);
		
		try {
			answer = (List<RepReporte>) query.getResultList();
		} catch (NoResultException e) {
			LOGGER.error("RepReporteDAOBean#obtenerListaTipoReporte:" +
					"Error al consultar la lista de reportes del SNC",e);
		}
		return answer;
		
	}
	
	/**
	 * @see RepReporteDAOBean#obtenerListaTipoReportePorTipoReporte(String)
	 * @author leidy.gonzalez
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RepReporte> obtenerListaTipoReportePorTipoReporte(String tipoReporte) {
		LOGGER.debug("Entra en RepReporteDAOBean#obtenerListaTipoReportePorCategoria");
		
		List<RepReporte> answer = null;
		
		Query q = entityManager.createQuery("SELECT rr FROM RepReporte rr WHERE rr.tipoReporte = :tipoReporte");
		q.setParameter("tipoReporte", tipoReporte);
		try {
			answer = (List<RepReporte>) q.getResultList();
		} catch (NoResultException e) {
			LOGGER.error("RepReporteDAOBean#obtenerListaTipoReportePorTipoReporte:" +
					"Error al consultar la lista de reportes");
		}
		return answer;
		
	}

}
