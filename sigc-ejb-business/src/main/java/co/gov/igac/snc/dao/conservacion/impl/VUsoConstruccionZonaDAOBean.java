/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IVUsoConstruccionZonaDAO;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;

/**
 * Implementación de los servicios de persistencia del objeto {@link VUsoConstruccionZona}.
 *
 * @author pedro.garcia
 */
@Stateless
public class VUsoConstruccionZonaDAOBean extends
    GenericDAOWithJPA<VUsoConstruccionZona, Long> implements
    IVUsoConstruccionZonaDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VUsoConstruccionZonaDAOBean.class);

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IVUsoConstruccionZonaDAO#buscarVUsosConstruccionPorZona(String)
     * @author fredy.wilches
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VUsoConstruccionZona> buscarVUsosConstruccionPorZona(String zona) {
        List<VUsoConstruccionZona> answer = new ArrayList<VUsoConstruccionZona>();
        try {
            Query query = this.entityManager
                .createQuery(
                    "SELECT uuz FROM VUsoConstruccionZona uuz WHERE uuz.zonaCodigo= :zona ORDER BY uuz.nombre");
            query.setParameter("zona", zona);
            answer = (ArrayList<VUsoConstruccionZona>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return answer;
    }

}
