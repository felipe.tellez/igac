package co.gov.igac.snc.dao.avaluos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoCapituloDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class AvaluoCapituloDAOBean extends
    GenericDAOWithJPA<AvaluoCapitulo, Long> implements IAvaluoCapituloDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoCapituloDAOBean.class);

    /**
     * @see IAvaluoCapituloDAO#obtenerCapitulosPorAvaluoId(Long)
     * @author rodrigo.hernandez
     */
    @Implement
    @Override
    public List<AvaluoCapitulo> obtenerCapitulosPorAvaluoId(Long idAvaluo) {

        List<AvaluoCapitulo> result = null;

        String queryString;
        Query query;

        queryString = " SELECT ac" + " FROM AvaluoCapitulo ac" +
            " WHERE ac.avaluoId = :idAvaluo";

        /*
         * Se ejecuta el query para buscar los capitulos de la proyeccion de cotizacion del avaluo
         */
        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idAvaluo", idAvaluo);

            result = query.getResultList();

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_BUSQUEDA_ENTIDAD_POR_ID.getExcepcion(
                LOGGER, ex,
                "AvaluoCapituloDAOBean#obtenerCapitulosPorAvaluoId");
        }

        return result;
    }
}
