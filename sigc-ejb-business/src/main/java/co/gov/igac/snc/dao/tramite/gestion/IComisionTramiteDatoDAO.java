/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion;

import java.util.List;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import javax.ejb.Local;

/**
 * interfaz para los métodos de base de datos de la tabla COMISION_TRAMITE_DATO
 *
 * @author pedro.garcia
 */
@Local
public interface IComisionTramiteDatoDAO extends IGenericJpaDAO<ComisionTramiteDato, Long> {

    /**
     * Método que busca las comisiones trámite dato por el id del trámite
     *
     * @param tramiteId
     * @return
     * @author javier.aponte
     */
    public List<ComisionTramiteDato> buscarComisionTramiteDatoPorTramiteId(Long tramiteId);

}
