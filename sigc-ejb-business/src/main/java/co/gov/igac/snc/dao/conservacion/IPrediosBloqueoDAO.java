/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;

/**
 *
 * @author juan.agudelo
 * @deprecated desde 28-10-2013 el entity se llama PredioBloqueo, por lo cual esta interfaz no
 * cumple con el estándar. Se debe usar IPredioBloqueoDAO
 */
/*
 * @modified pedro.garcia 28-10-2013 deprecated
 */
@Deprecated
@Local
public interface IPrediosBloqueoDAO extends IGenericJpaDAO<PredioBloqueo, Long> {

    /**
     * Obtiene la lista de predios bloqueados de acuerdo a los parametros de la busqueda
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPredioBloqueo
     * @return
     */
    public List<PredioBloqueo> buscarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo,
        final int... rowStartIdxAndCount);

    /**
     * Actualiza una lista de predios bloqueados solo si el usuario es el jefe de conservación
     *
     * @author juan.agudelo
     * @param usuario
     * @param prediosBloqueados
     */
    public ResultadoBloqueoMasivo actualizarPrediosBloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloqueados);

    /**
     * Cuenta una lista de prediosBloqueo por número predial
     *
     * @author juan.agudelo
     * @param numeroPredial
     * @return
     */
    public Integer contarPrediosBloqueoByNumeroPredial(String numeroPredial);

    /**
     * Busca los bloqueos asociados a un predio por numero predial
     *
     * @author juan.agudelo
     * @param numeroPredial
     * @return
     */
    public List<PredioBloqueo> buscarPrediosBloqueoByNumeroPredial(
        String numeroPredial, final int... rowStartIdxAndCount);

    /**
     * Actualiza una lista de predios a desbloquear
     *
     * @author juan.agudelo
     * @param usuario
     * @param prediosBloqueados
     */
    public List<PredioBloqueo> actualizarPrediosDesbloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloqueados);

    /**
     * Actualiza una lista de predios bloqueados solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public List<PredioBloqueo> actualizarPrediosBloqueoM(UsuarioDTO usuario,
        ArrayList<PredioBloqueo> bloqueoMasivo);

    /**
     * Actualiza una lista de predios bloqueados solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public ResultadoBloqueoMasivo bloquearMasivamentePredios(
        UsuarioDTO usuario, ArrayList<PredioBloqueo> bloqueoMasivo);

    /**
     * Obtiene la lista de bloqueos asociados a un predio por el número predial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public List<PredioBloqueo> getPredioBloqueosByNumeroPredial(
        String numeroPredial);
}
