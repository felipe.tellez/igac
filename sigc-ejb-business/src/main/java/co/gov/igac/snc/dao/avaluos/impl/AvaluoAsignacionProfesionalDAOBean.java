package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoAsignacionProfesionalDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;

/**
 * @see IAvaluoAsignacionProfesionalDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class AvaluoAsignacionProfesionalDAOBean extends
    GenericDAOWithJPA<AvaluoAsignacionProfesional, Long> implements
    IAvaluoAsignacionProfesionalDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoAsignacionProfesionalDAOBean.class);

    /**
     * @see IAvaluoAsignacionProfesionalDAO#consultarPorSolicitudConOrdenPractica(String)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AvaluoAsignacionProfesional> consultarPorSolicitudConOrdenPractica(
        String radicado) {

        List<AvaluoAsignacionProfesional> asignaciones = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT asp " +
                " FROM AvaluoAsignacionProfesional asp " +
                " WHERE asp.ordenPractica IS NOT NULL " +
                " AND asp.profesionalAvaluo IS NOT NULL " +
                " AND asp.avaluo.tramite.solicitud.numero = :radicado " +
                " AND asp.fechaHasta IS NULL ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("radicado", radicado);

            asignaciones = (List<AvaluoAsignacionProfesional>) query
                .getResultList();

            for (AvaluoAsignacionProfesional asignacion : asignaciones) {
                Hibernate.initialize(asignacion.getAvaluo());
                if (asignacion.getAvaluo() != null &&
                    asignacion.getAvaluo().getAvaluoPredios() != null) {
                    Hibernate.initialize(asignacion.getAvaluo()
                        .getAvaluoPredios());
                    for (AvaluoPredio predio : asignacion.getAvaluo()
                        .getAvaluoPredios()) {
                        Hibernate.initialize(predio.getDepartamento());
                        Hibernate.initialize(predio.getMunicipio());
                    }
                }
                Hibernate.initialize(asignacion.getProfesionalAvaluo());
                Hibernate.initialize(asignacion.getOrdenPractica());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return asignaciones;
    }

    // ------------------------------------------------------------------------------
    /**
     * @see IAvaluoAsignacionProfesionalDAO#consultarPorProfesionalId(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AvaluoAsignacionProfesional> consultarPorProfesionalId(
        Long idProfesional) {

        List<AvaluoAsignacionProfesional> asignaciones = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT asp " +
                " FROM AvaluoAsignacionProfesional asp " +
                " WHERE asp.profesionalAvaluo.id = :idProfesional ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("idProfesional", idProfesional);

            asignaciones = (List<AvaluoAsignacionProfesional>) query
                .getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return asignaciones;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoAsignacionProfesionalDAO#actualizarOrdenPractica(java.lang.String,
     * java.util.List)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void actualizarOrdenPractica(String numeroOrdenPractica, List<Long> avaluosOP) {

        String queryString;
        Query query;

        queryString = "" +
            " UPDATE AvaluoAsignacionProfesional aap " +
            //N: OJO: esto no funciona porque se está refiriendo a un campo diferente al que se usa
            //        para armar la llave foránea
            //" SET aap.ordenPractica.numero = :nop_p " +

            // ... pero sí funciona hacer el set o usar una condición donde se refiera al atributo
            //     del objeto que sirve como llave en la tabla
            " SET aap.ordenPractica.id = " +
            "   (SELECT op.id FROM OrdenPractica op WHERE op.numero = :nop_p) " +
            " WHERE aap.avaluo.id = :avaluoId_p";

        try {
            query = this.entityManager.createQuery(queryString);

            for (Long idAvaluo : avaluosOP) {
                query.setParameter("avaluoId_p", idAvaluo);
                query.setParameter("nop_p", numeroOrdenPractica);
                query.executeUpdate();
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoAsignacionProfesionalDAOBean#actualizarOrdenPractica");
        }

    }

    /**
     * @see IAvaluoAsignacionProfesionalDAO#consultarPorOrdenPracticaId(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AvaluoAsignacionProfesional> consultarPorOrdenPracticaId(
        Long ordenPracticaId) {

        List<AvaluoAsignacionProfesional> asignaciones = null;
        String queryString;
        Query query;

        try {

            queryString = "SELECT asp " +
                " FROM AvaluoAsignacionProfesional asp " +
                " WHERE asp.ordenPractica.id = :ordenPracticaId " +
                " AND asp.fechaHasta IS NULL ";

            query = this.entityManager.createQuery(queryString);
            query.setParameter("ordenPracticaId", ordenPracticaId);

            asignaciones = (List<AvaluoAsignacionProfesional>) query
                .getResultList();

            for (AvaluoAsignacionProfesional avaluoAsignacionProfesional : asignaciones) {
                Hibernate.initialize(avaluoAsignacionProfesional.getAvaluo());
            }

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, e.getMessage());
        }

        return asignaciones;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoAsignacionProfesionalDAO#existeAsignacionDeProfesional(long, java.lang.String,
     * long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean existeAsignacionDeProfesional(long profesionalAvaluosId,
        String codActividad, long[] idsAvaluos) {

        LOGGER.debug("on AvaluoAsignacionProfesionalDAOBean#existeAsignacionDeProfesional");
        boolean answer = false;
        List<AvaluoAsignacionProfesional> asignaciones;

        try {
            asignaciones =
                buscarPorProfesionalYActividadYAvaluos(profesionalAvaluosId, codActividad,
                    idsAvaluos);
            if (asignaciones == null || asignaciones.isEmpty()) {
                answer = true;
            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error(
                "error en AvaluoAsignacionProfesionalDAOBean#existeAsignacionDeProfesional" +
                ": " + ex.getMensaje());
            throw ex;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoAsignacionProfesionalDAO#buscarPorProfesionalYActividadYAvaluos(long,
     * java.lang.String, long[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoAsignacionProfesional> buscarPorProfesionalYActividadYAvaluos(
        long profesionalAvaluosId, String codActividad, long[] idsAvaluos) {

        LOGGER.debug("on AvaluoAsignacionProfesionalDAOBean#buscarPorProfesionalYActividadYAvaluos");

        List<AvaluoAsignacionProfesional> answer = null;
        ArrayList<Long> idsAvaluosP;
        String queryString;
        Query query;

        queryString = "" +
            " SELECT aap FROM AvaluoAsignacionProfesional aap " +
            " WHERE aap.actividad = :codActividad_p " +
            " AND aap.profesionalAvaluo.id = :profAvId_p " +
            " AND aap.id IN (:idsAvaluos_p)";

        idsAvaluosP = new ArrayList<Long>();
        for (int i = 0; i < idsAvaluos.length; i++) {
            idsAvaluosP.add(new Long(idsAvaluos[i]));
        }

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("codActividad_p", codActividad);
            query.setParameter("profAvId_p", profesionalAvaluosId);
            query.setParameter("idsAvaluos_p", idsAvaluosP);

            answer = query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoAsignacionProfesionalDAOBean#buscarPorProfesionalYActividadYAvaluos");
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoAsignacionProfesionalDAO#obtenerDeUltimaAsignacion(String, Long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoAsignacionProfesional> obtenerDeUltimaAsignacion(
        String codActividad, Long idAvaluo) {

        List<AvaluoAsignacionProfesional> tempAnswer, answer = null;
        String queryString;
        Query query;
        Long idAsignacion;

        queryString = "" +
            " SELECT aap FROM AvaluoAsignacionProfesional aap " +
            " JOIN FETCH aap.profesionalAvaluo " +
            " JOIN FETCH aap.asignacion asignacion " +
            " WHERE aap.actividad = :codActividad_p " +
            " AND aap.fechaHasta IS NOT NULL " +
            " AND aap.avaluo.id = :idAvaluo_p " +
            " ORDER BY asignacion.id DESC";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("codActividad_p", codActividad);
            query.setParameter("idAvaluo_p", idAvaluo.longValue());

            tempAnswer = query.getResultList();

            //N: como se ordenaron en forma descendente por el id de la asignación, se toman los
            //  primeros elementos del arreglo que están en el primer grupo.
            answer = new ArrayList<AvaluoAsignacionProfesional>();
            if (tempAnswer != null && !tempAnswer.isEmpty()) {
                idAsignacion = tempAnswer.get(0).getAsignacion().getId();
                for (AvaluoAsignacionProfesional aap : tempAnswer) {
                    if (idAsignacion.longValue() == aap.getAsignacion().getId().longValue()) {
                        answer.add(aap);
                    } else {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "AvaluoAsignacionProfesionalDAOBean#buscarPorProfesionalYActividadYAvaluos");

        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoAsignacionProfesionalDAO#borrarDeAvaluo(Long, String, boolean)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean borrarDeAvaluo(Long idAvaluo, String codActividad, boolean vigentes) {

        boolean answer = false;
        StringBuffer queryString;
        Query query;

        queryString = new StringBuffer();
        queryString.append(
            " DELETE FROM AvaluoAsignacionProfesional aap " +
            " WHERE aap.avaluo.id = :idAvaluo_p ");

        if (codActividad != null && !codActividad.isEmpty()) {
            queryString.append(" AND aap.actividad = :actividad_p ");
        }

        if (vigentes) {
            queryString.append(" AND aap.fechaHasta IS NULL");
        }

        try {
            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idAvaluo_p", idAvaluo);

            if (codActividad != null && !codActividad.isEmpty()) {
                query.setParameter("actividad_p", codActividad);
            }
            query.executeUpdate();
            answer = true;
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_ACTUALIZACION_O_ELIMINACION.
                getExcepcion(LOGGER, ex, "AvaluoAsignacionProfesionalDAOBean#borrarDeAvaluo",
                    "AvaluoAsignacionProfesional", "varios ids");
        }

        return answer;

    }

//end of class
}
