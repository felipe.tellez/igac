package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IErrorProcesoMasivoDAO;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class ErrorProcesoMasivoDAOBean extends GenericDAOWithJPA<ErrorProcesoMasivo, Long>
    implements IErrorProcesoMasivoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DominioDAOBean.class);

}
