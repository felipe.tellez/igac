package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOfertaOb;

/**
 *
 * Interfaz para los métodos de bd de la tabla CONTROL_CALIDAD_OFERTA_OBS
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IControlCalidadOfertaObDAO extends
    IGenericJpaDAO<ControlCalidadOfertaOb, Long> {

    /**
     * Método para cargar las observaciones asociadas a un ControlCalidadOferta
     *
     * @author rodrigo.hernandez
     *
     * @param controlCalidadOfertaId - Id del ControlCalidadOferta
     *
     * @return Lista de Observaciones asociadas a un ControlCalidadOferta
     */
    public List<ControlCalidadOfertaOb> cargarObservacionesPorControlCalidadOfertaId(
        Long controlCalidadOfertaId);

    /**
     * Método para cargar las observaciones asociadas a una oferta inmobiliaria
     *
     * @author christian.rodriguez
     * @param ofertaId Id de la oferta inmobiliaria
     *
     * @return Lista de Observaciones asociadas a una oferta inmobiliaria
     */
    public List<ControlCalidadOfertaOb> cargarObservacionesPorOfertaId(Long ofertaId);

}
