/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.IPredioLinderoDAO;
import co.gov.igac.snc.dao.conservacion.IPredioZonaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioLindero;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author leidy.gonzalez
 */
@Stateless
public class PredioLinderoDAOBean extends GenericDAOWithJPA<PredioLindero, Long>
    implements IPredioLinderoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioLinderoDAOBean.class);

    /**
     *
     * @author leidy.gonzalez
     * @see IPredioLinderoDAO#contarLinderosPorNumeroPredial
     */
    @Implement
    @Override
    public Integer contarLinderosPorNumeroPredial(String numeroPredial) {

        Integer conteoPredioLindero = 0;

        // Se deben mostrar las zonas de la última vigencia
        Query q = entityManager
            .createQuery("SELECT COUNT (pl.numeroPredial)" +
                " FROM PredioLindero pl" +
                " WHERE pl.numeroPredial= :numeroPredial");

        q.setParameter("numeroPredial", numeroPredial);

        try {
            conteoPredioLindero = (Integer.parseInt(q.getSingleResult()
                .toString()));
            return conteoPredioLindero;

        } catch (Exception ex) {
            LOGGER.error("Error on PredioZonaDAOBean#buscarPorPPredioId: " +
                ex.getMessage());

            return null;
        }

    }
}
