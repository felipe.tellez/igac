package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;

/**
 * Servicios de persistencia para el objeto GrupoProducto
 *
 * @author javier.barajas
 */
@Local
public interface IGrupoProductoDAO extends IGenericJpaDAO<GrupoProducto, Long> {

    /**
     * Método que devuelve la lista de grupos de productos existentes exceptuando ordenado por el
     * campo descripcion.
     *
     * @return List<GrupoProducto>
     * @version:1.0
     * @author leidy.gonzalez
     */
    public List<GrupoProducto> obtenerTodosGrupoProductos();
}
