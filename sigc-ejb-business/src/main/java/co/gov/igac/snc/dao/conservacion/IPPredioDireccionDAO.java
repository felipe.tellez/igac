package co.gov.igac.snc.dao.conservacion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;

@Local
public interface IPPredioDireccionDAO extends IGenericJpaDAO<PPredioDireccion, Long> {

    /**
     * Método que retorna las direcciones proyectadas para un predio segun su id
     *
     * @author franz.gamba
     * @param predioId
     * @return
     */
    public List<PPredioDireccion> getPPredioDireccionesByPredioId(Long predioId);

    /**
     * Metodo para obtener la direccion pincipal del predio matriz de un PH o Condominio
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public PPredioDireccion obtenerDireccionPrincipalFichaMatriz(String numeroPredial);

    /**
     * Metodo para obtener la direccion pincipal del predio matriz de un PH o Condominio
     *
     * @author leidy.gonzalez
     * @param predioId
     * @return
     */
    public PPredioDireccion obtenerDireccionPrincipalFichaMatrizId(Long predioId);

}
