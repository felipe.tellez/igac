/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
//import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaDetalle;
import co.gov.igac.snc.util.FuncionarioRecolector;

/**
 * interfaz para los métodos de bd de la tabla AREA_CAPTURA_DETALLE
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Local
@Deprecated
//TODO :: mover los métodos que sirvan y eliminar esta clase

public interface IAreaCapturaDetalleDAO {

//    extends	IGenericJpaDAO<AreaCapturaDetalle, Long> {
//
//	/**
//	 * Método para consultar las areas asignadas a un recolector, buscando por el id del recolector
//	 * @author rodrigo.hernandez
//	 * @param idRecolector - Id del recolector
//     * @return Lista de manzanas/veredas asignadas a un recolector
//	 */
//	public List<String> getDetallesAreaByRecolectorId(String idRecolector);
//
//	/**
//	 * Método para consultar las areas asignadas a un recolector, buscando por el id del recolector y el id
//	 * del area
//	 * @author rodrigo.hernandez
//	 * @param recolectorId
//	 * @param areaId
//	 * @return
//	 */
//	public List<AreaCapturaDetalle> getDetallesAreaByRecolectorIdAreaId(String recolectorId, Long areaId);
//
//	/**
//	 * Método para consultar el listado de Areas Detalle para Áreas sin
//	 * comisionar
//	 *
//	 * @author christian.rodriguez
//	 * @param idAreaCaptura
//	 *            Id del area de captura
//	 * @return lista de las áreas Detalle del área seleccionada
//	 */
//	public List<AreaCapturaDetalle> getAreasCapturaDetalleConRecolectorSinComisionByAreaId(Long areaCapturaId);
//
//	/**
//	 * Método para consulta el listado de areas captura detalles asociadas a una
//	 * comisión de oferta. Trae los detalles del Área Captura Oferta asociado
//	 * con Departamento, municipio y Zona
//	 *
//	 * @author christian.rodriguez
//	 * @param comisionOfertaId
//	 *            identificador de la comisión
//	 * @return lista con las áreas captura detalles asociadas a al comisión
//	 */
//	public List<AreaCapturaDetalle> getAreasCapturaDetalleByComisionOfertaID(Long comisionOfertaId);
//
//	/**
//	 * Método para consultar las areas asignadas a un recolector, buscando por el id del recolector y el id
//	 * del area
//	 * @author rodrigo.hernandez
//	 * @param recolectorId
//	 * @param areaId
//	 * @return
//	 */
//	public List<String> cargarDetallesAreaOfertaMunicipioDescentralizadoRecolector(String recolectorId, Long areaId);
//
//		/**
//	 *  Método para consultar si los recolectores disponibles tienen alguna asignación y
//	 *  se encuentran recolectando
//	 *  @author rodrigo.hernandez
//	 *  @param listaRecolectores Lista con ids y nombres de los recolectores
//	 * @return lista de recolectores
//	 */
//	public List<FuncionarioRecolector> getInformacionRecolectores(List<String>listaRecolectores);
//
//	/**
//	 * Metodo que trae un AreaCapturaDetalle por codigo de la Manzana/Vereda
//	 * @param manzanaVeredaCodigo Codigo de la Manzana/Vereda
//	 * @return AreaCapturaDetalle
//	 */
//	public AreaCapturaDetalle getAreaDetalleByManzanaVeredaCodigo(String manzanaVeredaCodigo, Long areaOfertaId);
//
}
