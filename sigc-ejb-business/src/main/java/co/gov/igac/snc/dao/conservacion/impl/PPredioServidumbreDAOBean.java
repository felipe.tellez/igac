package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;

import co.gov.igac.snc.dao.conservacion.IPPredioServidumbreDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioServidumbre;

@Stateless
public class PPredioServidumbreDAOBean extends GenericDAOWithJPA<PPredioServidumbre, Long>
    implements
    IPPredioServidumbreDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioServidumbreDAOBean.class);

    /**
     *
     * @author leidy.gonzalez
     * @see IPPredioServidumbreDAO#delete
     */
    @Implement
    @Override
    public void delete1(Long idServidumbre) {

        String queryString;
        Query query;

        queryString = "DELETE P_PREDIO_SERVIDUMBRE" +
            " WHERE id = " + idServidumbre;

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error on PPredioServidumbre#delete: " +
                ex.getMessage());
        }

    }

}
