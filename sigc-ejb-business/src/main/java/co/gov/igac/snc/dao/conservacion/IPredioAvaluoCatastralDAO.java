/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import java.util.List;

/**
 *
 * @author juan.agudelo
 * @version 2.0
 *
 */
@Local
public interface IPredioAvaluoCatastralDAO extends
    IGenericJpaDAO<PredioAvaluoCatastral, Long> {

    /**
     * Busca el valor de avalúo por número predial, especificamente utilizado en FichaMatrizPredio
     * en el atributo transient valorAvaluo
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public Double findValorAvaluoByNumeroPredial(String numeroPredial);

    /**
     * Consulta los registros para el predio dado agrupados por el año de la vigencia
     *
     * @author pedro.garcia
     * @param predioId
     * @return
     */
    public List<PredioAvaluoCatastral> obtenerHistoricoPorPredio(Long predioId);

//end of interface    
}
