package co.gov.igac.snc.fachadas;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.conservacion.IPredioColindanteDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioColindante;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

/**
 * Bean destinado únicamente para ser utilizado por las pruebas de integración (Este Bean no se
 * utiliza desde la capa Web)
 *
 * @author juan.mendez
 * @version 2.0
 */
@Stateless
//@Interceptors(TimeInterceptor.class)
public class PruebasBean implements IPruebas, IPruebasLocal {

    /**
     *
     */
    private static final long serialVersionUID = 5381993332155922866L;
    /**
     * Servicio de bitácora
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PruebasBean.class);

    /**
     * Servicios de Acceso a datos
     */
    @EJB
    private IPredioDAO predioDao;

    @EJB
    private ISolicitudDAO solicitudDao;

    @EJB
    private IPredioColindanteDAO predioColindanteDao;

    @EJB
    private IDocumentoDAO documentoDao;

    @EJB
    private ISNCProcedimientoDAO sncProcedimientoDao;

    @EJB
    private ILogMensajeDAO logMensajeDAO;

    @EJB
    private IProductoCatastralJobDAO productoCatastralJobDAO;

    /**
     *
     * @author juan.mendez (non-Javadoc)
     * @see co.gov.igac.snc.fachadas.IPruebas#cargarPredioAleatorio()
     */
    @Override
    public final Predio cargarPredioAleatorio() throws ExcepcionSNC {
        return predioDao.loadRandomObject();
    }

    /**
     * @author juan.mendez (non-Javadoc)
     * @see
     * co.gov.igac.snc.fachadas.IPruebas#getPredioColindanteByNumeroPredialYTipo(java.lang.String,
     * java.lang.String)
     */
    @Override
    public final List<PredioColindante> getPredioColindanteByNumeroPredialYTipo(String numeroPredial,
        String tipo) {
        return predioColindanteDao.getPredioColindanteByNumeroPredialYTipo(numeroPredial, tipo);
    }

    @Override
    public final Solicitud cargarSolicitudAleatoria() {
        return solicitudDao.loadRandomObject();
    }

    @Override
    public Predio cargarPredioAleatorio(String numeroPredialIniciaCon) throws ExcepcionSNC {
        return predioDao.loadRandomObject(numeroPredialIniciaCon);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IConservacion#buscarPredioLibreTramite()
     * @author fredy.wilches
     * @version 2.0
     *
     * @return Predio
     */
    @Override
    public Predio buscarPredioLibreTramite(Integer codigoEstructuraOrganizacional,
        String codigoMunicipio) {
        LOGGER.info("buscarPredioLibreTramite");
        return this.predioDao.buscarPredioLibreTramite(codigoEstructuraOrganizacional,
            codigoMunicipio);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IPruebasLocal#ejecutarSPAislado(java.lang.String, java.lang.String, boolean)
     * @author pedro.garcia
     *
     */
    @Override
    public Object[] ejecutarSPAislado(String codigoPrueba, String mensajePrueba,
        boolean generarError) {

        LOGGER.debug("unit test PruebasBean#testEjecutarSPAislado ...");

        Documento doc;

        Long idDocumento;
        Object[] answer = null;

        //N: queda quemado aquó porque es solo un prueba
        idDocumento = 50482L;
        doc = null;

        try {
            doc = this.documentoDao.buscarDocumentoCompletoPorId(idDocumento);
        } catch (Exception ex) {
            LOGGER.error("no se pudo encontrar el Documento con id " + idDocumento + " : " +
                ex.getMessage());
        }

        try {
            //D: esto debe generarr un error en el sp y que se trate de hacer rollback en esa transacción ...
            answer = this.sncProcedimientoDao.ejecutarSPAislado("0", "unit test", true);

            // ... y se supone, ya que se hizo en una transacción nueva, que esto debería funcionar
            this.documentoDao.update(doc);

            LOGGER.debug("... passed!");
        } catch (Exception ex) {
            LOGGER.error("error en sl sp: " + ex.getMessage());
        }

        return answer;

    }

    /**
     * Prueba de integración para el método de sig enviarJobExportarDatosGeograficosTramite
     *
     * @author juan.mendez
     *
     * @param tramite
     * @param identificadoresPredios
     * @param tipoMutacion
     * @param formatoArchivo
     * @param usuario
     */
    @Override
    public void enviarJobExportarDatosGeograficosTramite(Tramite tramite,
        String identificadoresPredios,
        String tipoMutacion, String formatoArchivo, UsuarioDTO usuario) {
        LOGGER.debug("enviarJobExportarDatosGeograficosTramite  numeroTramite: " + tramite.getId().
            toString());
        SigDAOv2.getInstance().exportarDatosAsync(productoCatastralJobDAO, tramite,
            identificadoresPredios, tipoMutacion,
            ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_FILEGEODB, usuario);

    }

//end of class    
}
