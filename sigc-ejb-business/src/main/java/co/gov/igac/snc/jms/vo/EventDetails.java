package co.gov.igac.snc.jms.vo;

import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juan.mendez
 *
 */
//TODO :: juan.mendez :: documentar la clase :: pedro.garcia
public class EventDetails {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventDetails.class);

    public static final String EVENT_TYPE = "EVENT_TYPE";
    public static final String EVENT_TIMESTAMP = "EVENT_TIMESTAMP";
    public static final String EVENT_USER_LOGIN = "EVENT_USER_LOGIN";
    public static final String EVENT_TRAMITE_NUMERO = "EVENT_TRAMITE_NUMERO";
    public static final String EVENT_PREDIOS_CODIGOS = "EVENT_PREDIOS_CODIGOS";
    public static final String EVENT_ACTIVIDAD_ID = "EVENT_ACTIVIDAD_ID";
    public static final String EVENT_JOB_ID = "EVENT_JOB_ID";
    public static final String EVENT_TIPO_TRAMITE = "EVENT_TIPO_TRAMITE";
    public static final String EVENT_ACTIVIDAD_ACTUAL = "EVENT_ACTIVIDAD_ACTUAL";

    private String type;
    private long timestamp;
    private String userLogin;
    private Long idTramite;
    private String codigosPredios;
    private String actividadId;
    private String tipoTramite;
    private Long idJob;
    private String actividadActual;

    /**
     *
     * @param msg
     * @throws JMSException
     */
    public EventDetails(Message msg) throws JMSException {

        if (msg.getObjectProperty(EventDetails.EVENT_ACTIVIDAD_ACTUAL) != null) {
            this.actividadActual = msg.getStringProperty(EventDetails.EVENT_ACTIVIDAD_ACTUAL);
        } else {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_ACTIVIDAD_ACTUAL +
                " en el mensaje JMS");
        }

        if (msg.getObjectProperty(EventDetails.EVENT_TIMESTAMP) != null) {
            this.timestamp = msg.getLongProperty(EventDetails.EVENT_TIMESTAMP);
        } else {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_TIMESTAMP +
                " en el mensaje JMS");
        }

        if (msg.getStringProperty(EventDetails.EVENT_USER_LOGIN) != null) {
            this.userLogin = msg.getStringProperty(EventDetails.EVENT_USER_LOGIN);
        } else {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_USER_LOGIN +
                " en el mensaje JMS");
        }

        try {
            this.tipoTramite = msg.getStringProperty(EventDetails.EVENT_TIPO_TRAMITE);
        } catch (Exception e) {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_TIPO_TRAMITE +
                " en el mensaje JMS");
        }

        try {
            this.actividadId = msg.getStringProperty(EventDetails.EVENT_ACTIVIDAD_ID);
        } catch (Exception e) {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_ACTIVIDAD_ID +
                " en el mensaje JMS");
        }

        try {
            this.codigosPredios = msg.getStringProperty(EventDetails.EVENT_PREDIOS_CODIGOS);
        } catch (Exception e) {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_PREDIOS_CODIGOS +
                " en el mensaje JMS");
        }

        try {
            this.type = msg.getStringProperty(EventDetails.EVENT_TYPE);
        } catch (Exception e) {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_TYPE +
                " en el mensaje JMS");
        }

        try {
            this.idTramite = msg.getLongProperty(EventDetails.EVENT_TRAMITE_NUMERO);
        } catch (Exception e) {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_TRAMITE_NUMERO +
                " en el mensaje JMS");
        }

        try {
            this.idJob = msg.getLongProperty(EventDetails.EVENT_JOB_ID);
        } catch (NumberFormatException e) {
            LOGGER.warn("No se encontro la propiedad " + EventDetails.EVENT_JOB_ID +
                " en el mensaje JMS");
        }

    }

    public String getCodigosPredios() {
        return this.codigosPredios;
    }

    public void setCodigosPredios(String codigosPredios) {
        this.codigosPredios = codigosPredios;
    }

    public String getUserLogin() {
        return this.userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getIdTramite() {
        return this.idTramite;
    }

    public void setIdTramite(Long idTramite) {
        this.idTramite = idTramite;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getActividadId() {
        return actividadId;
    }

    public void setActividadId(String actividadId) {
        this.actividadId = actividadId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public Long getIdJob() {
        return this.idJob;
    }

    public void setIdJob(Long idJob) {
        this.idJob = idJob;
    }

    public String getActividadActual() {
        return this.actividadActual;
    }

    public void setActividadActual(String actividadActual) {
        this.actividadActual = actividadActual;
    }

}
