/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VPropiedadTierra;
import co.gov.igac.snc.persistence.entity.vistas.VPropiedadTierraPK;
import co.gov.igac.snc.vo.EstadisticasRegistroVO;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author franz.gamba
 *
 */
@Local
public interface IVPropiedadTierraDAO extends IGenericJpaDAO<VPropiedadTierra, VPropiedadTierraPK> {

    /**
     * metodo qeu retorna la inpormacion por tipo de propiedad segun departamento, municipio y/o
     * zona unidad organica.
     *
     * @param departamento
     * @param municipio
     * @param zonaUnidadOrganica
     * @return
     * @author franz.gamba
     */
    public List<EstadisticasRegistroVO> getEstadisticas(String departamento, String municipio,
        String zonaUnidadOrganica);
}
