package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoPagoAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;

/**
 * Interface que contiene metodos de consulta sobre entity ProfesionalContratoPago
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IProfesionalContratoPagoDAO extends IGenericJpaDAO<ProfesionalContratoPago, Long> {

    /**
     * Método que busca los pagos realizados por un avalúador para un contrato especifico
     *
     * @author christian.rodriguez
     * @param idContrato identificador de contrato {@link ProfesionalAvaluosContrato} al cual estaá
     * asociado el avalúador
     * @return lista con los pagos realizados por un avalúador para un contrato especifico
     */
    public List<ProfesionalContratoPago> consultarPorIdContrato(Long idContrato);

    /**
     * Método que busca los pagos des salud y pensión nrealizados por un avalúador para un contrato
     * especifico
     *
     * @author christian.rodriguez
     * @param idContrato identificador de contrato {@link ProfesionalAvaluosContrato} al cual estaá
     * asociado el avalúador
     * @return lista con los pagos de pensión y salud realizados por un avalúador para un contrato
     * especifico
     */
    public List<ProfesionalContratoPago> consultarDePensionSaludPorIdContrato(Long idContrato);

    /**
     * Método que consulta los pagos proyectados (sin número de orden de pago) asociados al contrato
     * especificado
     *
     * @author christian.rodriguez
     * @param idContrato identificador del contrato
     * @return lista con los {@link ProfesionalAvaluosContrato} proyectados (sin número de orden de
     * pago)
     */
    public List<ProfesionalContratoPago> consultarProyectadosPorIdContrato(Long idContrato);

    /**
     * Método que consulta los pagos proyectados (sin número de orden de pago) asociados al contrato
     * especificado
     *
     * @author christian.rodriguez
     * @param idContrato identificador del contrato
     * @return lista con los {@link ProfesionalAvaluosContrato} proyectados (sin número de orden de
     * pago)
     */
    public List<ProfesionalContratoPago> consultarCausadosPorIdContrato(Long idContrato);

    /**
     * Método que calcula el siguiente secuencial de pago para un cotnrato especifico
     *
     * @author christian.rodriguez
     * @param idContrato identificador del contrato
     * @return {@link Long} con el siguiente secuencial de pago calculado dependiendo del número de
     * pagos que tenga el contrato asociado
     */
    public Long calcularSiguienteSecPago(Long idContrato);

    /**
     * Método que carga los {@link ContratoPagoAvaluo} asociados al pago
     *
     * @author christian.rodriguez
     * @param pagoId id del {@link ProfesionalContratoPago}
     * @return {@link ProfesionalContratoPago} con los {@link ContratoPagoAvaluo} cargados
     */
    public ProfesionalContratoPago obtenerContratoPagoAvaluosPorId(
        Long pagoId);

}
