package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacionInf;

/**
 * Servicios de persistencia para el objeto LevantamientoAsignacionInf
 *
 * @author javier.barajas
 */
@Local
public interface ILevantamientoAsignacionInfDAO extends
    IGenericJpaDAO<LevantamientoAsignacionInf, Long> {

    /**
     * Método que devuelve un objeto LevantamientoAsignacion por el id de recursoHumano
     *
     * @param recursoHumano
     * @return LevantamientoAsignacion
     * @cu 212 Efectuar Control de Calidad a Levantamiento Topografico
     * @version 1.0
     * @author javier.barajas
     *
     */
    public LevantamientoAsignacionInf obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId(
        Long levantamientoAsignacionId);
}
