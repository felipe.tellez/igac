/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos sobre la tabla TRAMITE_PREDIO_ENGLOBE que relaciona
 * trámites con predios (para los casos de englobe)
 *
 * @author pedro.garcia
 */
@Local
public interface ITramiteDetallePredioDAO extends IGenericJpaDAO<TramiteDetallePredio, Long> {

    /**
     * retorna los registros relacionados con un trámite
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<TramiteDetallePredio> getByTramiteId(Long idTramite);

    /**
     * Método para buscar un tramite detallePredio asociado a un predio y un tramite.
     *
     * @author felipe.cadena
     * @param idTramite
     * @param idPredio
     * @return
     */
    public TramiteDetallePredio getByTramiteIdYPredioId(Long idTramite, Long idPredio);

    /**
     *
     * retorna los numeros prediales relacionados con uno o varios trámites
     *
     * @author lorena.salamanca
     * @param tramiteIds
     * @return
     */
    public List<String> getNumerosPredialesByTramiteId(List<Long> tramiteIds);

    /**
     * Retorna el numero de predios asociados a un tramite en el momento de la radicacion
     *
     * @author felipe.cadena
     * @param idTramite
     * @return
     */
    public Integer countByTramiteId(Long idTramite);

}
