package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionDetalleDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepConsultaPredial;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionDetalle;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;

@Stateless
public class RepReporteEjecucionDetalleDAOBean extends GenericDAOWithJPA<RepReporteEjecucionDetalle, Long>
    implements
    IRepReporteEjecucionDetalleDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RepReporteEjecucionDetalleDAOBean.class);

    /**
     * @see RepReporteEjecucion#buscarReportePorFiltrosAvanzados(FiltroGenerarReportes
     * datosConsultaPredio,Long idReporteEjecucion, RepConsultaPredial configuracionAvanzada)
     * @author leidy.gonzalez
     */
    @Override
    public List<RepReporteEjecucionDetalle> buscarReportePorFiltrosAvanzados(
        FiltroGenerarReportes datosConsultaPredio, String selectedMunicipioCod,
        Long idReporteEjecucion, RepConfigParametroReporte configuracionAvanzada) {
        List<RepReporteEjecucionDetalle> answer = null;
        int contadorRegistrosMultiplesAnzada = 0;

        Query query;
        StringBuilder queryString = new StringBuilder();

        queryString
            .append("SELECT rre FROM RepReporteEjecucionDetalle rre " +
                "JOIN FETCH rre.repReporteEjecucion rr " +
                "WHERE rr.id = :idReporteEjecucion ");

        //Consulta Avanzada-Parametros
        if (selectedMunicipioCod != null && !selectedMunicipioCod.isEmpty()) {

            queryString.append(" AND rre.municipioCodigo = :selectedMunicipioCod");

        }

        if (datosConsultaPredio.getNumeroPredial() != null &&
            !datosConsultaPredio.getNumeroPredial().isEmpty()) {

            queryString.append(" AND rre.numeroPredialDesde = :numeroPredial");

        }

        if (datosConsultaPredio.getTipoIdentificacionPropietario() != null &&
            !datosConsultaPredio.getTipoIdentificacionPropietario().isEmpty()) {

            queryString.append(" AND rre.tipoIdentificacion = :tipoIdentificacion");

        }

        if (datosConsultaPredio.getIdentificacionPropietario() != null &&
            !datosConsultaPredio.getIdentificacionPropietario().isEmpty()) {

            queryString.append(" AND rre.numeroIdentificacion = :numeroIdentificacion");

        }

        if (datosConsultaPredio.getModoAdquisicionId() != null &&
            !datosConsultaPredio.getModoAdquisicionId().isEmpty()) {

            queryString.append(" AND rre.modoAdquisicion = :modoAdquisicion");

        }

        if (datosConsultaPredio.getTipoTitulo() != null &&
            !datosConsultaPredio.getTipoTitulo().isEmpty()) {

            queryString.append(" AND rre.tipoTitulo = :tipoTitulo");

        }

        if (datosConsultaPredio.getEntidad() != null && !datosConsultaPredio.getEntidad().isEmpty()) {

            queryString.append(" AND rre.entidad = :entidad");
        }

        if (datosConsultaPredio.getDireccion() != null &&
            !datosConsultaPredio.getDireccion().isEmpty()) {

            queryString.append(" AND rre.direccion = :direccion");

        }

        if (datosConsultaPredio.getNumeroRegistro() != null &&
            !datosConsultaPredio.getNumeroRegistro().isEmpty()) {

            queryString.append(" AND rre.matriculaInmobiliaria = :matriculaInmobiliaria");

        }

        if (datosConsultaPredio.getTipoPredioId() != null &&
            !datosConsultaPredio.getTipoPredioId().isEmpty()) {

            queryString.append(" AND rre.predioTipo = :tipoPredio");
        }

        if (datosConsultaPredio.getDestinoPredioId() != null &&
            !datosConsultaPredio.getDestinoPredioId().isEmpty()) {

            queryString.append(" AND rre.predioDestino = :destinoEconomico");

        }

        if (datosConsultaPredio.getAreaTerrenoMin() != null) {
            queryString.append(" AND rre.areaTerrenoDesde = :areaTerrenoDesde");
        }
        if (datosConsultaPredio.getAreaTerrenoMax() != null) {
            queryString.append(" AND rre.areaTerrenoHasta = :areaTerrenoHasta");
        }

        if (datosConsultaPredio.getAreaConstruidaMin() != null) {
            queryString.append(" AND rre.areaConstruccionDesde = :areaConstruccionDesde");
        }
        if (datosConsultaPredio.getAreaConstruidaMax() != null) {
            queryString.append(" AND rre.areaConstruccionHasta = :areaConstruccionHasta");
        }

        if (datosConsultaPredio.getAvaluoDesde() != null) {
            queryString.append(" AND rre.avaluoDesde = :avaluoDesde");
        }
        if (datosConsultaPredio.getAvaluoHasta() != null) {
            queryString.append(" AND rre.avaluoHasta = :avaluoHasta");
        }

        if (datosConsultaPredio.getAvaluoDesde() != null) {
            queryString.append(" AND rre.avaluoHasta = :avaluoHasta");
        }
        if (datosConsultaPredio.getAvaluoHasta() != null) {
            queryString.append(" AND rre.avaluoHasta = :avaluoHasta");
        }

        if (datosConsultaPredio.getZonaFisicaId() != null &&
            !datosConsultaPredio.getZonaFisicaId().isEmpty()) {

            queryString.append(" AND rre.zonaFisica = :zonaFisica");

        }

        if (datosConsultaPredio.getZonaEconomicaId() != null &&
            !datosConsultaPredio.getZonaEconomicaId().isEmpty()) {

            queryString.append(" AND rre.zonaGeoeconomica = :zonaGeoeconomica");

        }

        if (datosConsultaPredio.getUsoConstruccionSelect() != null &&
            !datosConsultaPredio.getUsoConstruccionSelect().isEmpty()) {

            queryString.append(" AND rre.construccionUso = :usoConstruccion");
        }

        if (datosConsultaPredio.getFechaInicioBloqueo() != null) {

            queryString.append(" AND rre.fechaInicioBloqueo = :fechaInicioBloqueo");

        }

        if (datosConsultaPredio.getFechaDesbloqueo() != null) {

            queryString.append(" AND rre.fechaDesbloqueo = :fechaDesbloqueo");

        }

        try {

            query = this.entityManager.createQuery(queryString.toString());
            query.setParameter("idReporteEjecucion", idReporteEjecucion);

            //Consulta Avanzada-Parametros
            if (selectedMunicipioCod != null && !selectedMunicipioCod.isEmpty()) {

                query.setParameter("selectedMunicipioCod", selectedMunicipioCod);

            }

            if (datosConsultaPredio.getNumeroPredial() != null &&
                !datosConsultaPredio.getNumeroPredial().isEmpty()) {

                query.setParameter("numeroPredial", datosConsultaPredio.getNumeroPredial());

            }

            if (datosConsultaPredio.getTipoIdentificacionPropietario() != null &&
                !datosConsultaPredio.getTipoIdentificacionPropietario().isEmpty()) {

                query.setParameter("tipoIdentificacion", datosConsultaPredio.
                    getTipoIdentificacionPropietario());

            }

            if (datosConsultaPredio.getIdentificacionPropietario() != null &&
                !datosConsultaPredio.getIdentificacionPropietario().isEmpty()) {

                query.setParameter("numeroIdentificacion", datosConsultaPredio.
                    getIdentificacionPropietario());

            }

            if (datosConsultaPredio.getModoAdquisicionId() != null &&
                !datosConsultaPredio.getModoAdquisicionId().isEmpty()) {

                query.setParameter("modoAdquisicion", datosConsultaPredio.getModoAdquisicionId());

            }

            if (datosConsultaPredio.getTipoTitulo() != null &&
                !datosConsultaPredio.getTipoTitulo().isEmpty()) {

                query.setParameter("tipoTitulo", datosConsultaPredio.getTipoTitulo());

            }

            if (datosConsultaPredio.getEntidad() != null &&
                !datosConsultaPredio.getEntidad().isEmpty()) {

                query.setParameter("entidad", datosConsultaPredio.getEntidad());

            }

            if (datosConsultaPredio.getDireccion() != null &&
                !datosConsultaPredio.getDireccion().isEmpty()) {

                query.setParameter("direccion", datosConsultaPredio.getDireccion());

            }

            if (datosConsultaPredio.getNumeroRegistro() != null &&
                !datosConsultaPredio.getNumeroRegistro().isEmpty()) {

                query.setParameter("matriculaInmobiliaria", datosConsultaPredio.getNumeroRegistro());

            }

            if (datosConsultaPredio.getTipoPredioId() != null &&
                !datosConsultaPredio.getTipoPredioId().isEmpty()) {

                String tipoPredio = " ";
                contadorRegistrosMultiplesAnzada = 0;

                for (String tipoPredioTemp : datosConsultaPredio.getTipoPredioId()) {

                    contadorRegistrosMultiplesAnzada++;

                    if (contadorRegistrosMultiplesAnzada > 1) {

                        tipoPredio = datosConsultaPredio.getTipoPredioId() + "," + "'" +
                            tipoPredioTemp + "'";

                    } else {
                        tipoPredio = "'" + tipoPredioTemp + "'";
                    }

                }

                query.setParameter("tipoPredio", tipoPredio);

            }

            if (datosConsultaPredio.getDestinoPredioId() != null &&
                !datosConsultaPredio.getDestinoPredioId().isEmpty()) {

                query.setParameter("destinoEconomico", datosConsultaPredio.getDestinoPredioId());

            }

            if (datosConsultaPredio.getAreaTerrenoMin() != null) {
                query.setParameter("areaTerrenoDesde", datosConsultaPredio.getAreaTerrenoMin());
            }
            if (datosConsultaPredio.getAreaTerrenoMax() != null) {
                query.setParameter("areaTerrenoHasta", datosConsultaPredio.getAreaTerrenoMax());
            }

            if (datosConsultaPredio.getAreaConstruidaMin() != null) {
                query.setParameter("areaConstruccionDesde", datosConsultaPredio.
                    getAreaConstruidaMax());
            }
            if (datosConsultaPredio.getAreaConstruidaMax() != null) {
                query.setParameter("areaConstruccionHasta", datosConsultaPredio.
                    getAreaConstruidaMax());
            }

            if (datosConsultaPredio.getAvaluoDesde() != null) {
                query.setParameter("avaluoDesde", datosConsultaPredio.getAvaluoDesde());
            }
            if (datosConsultaPredio.getAvaluoHasta() != null) {
                query.setParameter("avaluoHasta", datosConsultaPredio.getAvaluoDesde());
            }

            if (datosConsultaPredio.getAvaluoDesde() != null) {
                query.setParameter("avaluoDesde", datosConsultaPredio.getAvaluoDesde());
            }
            if (datosConsultaPredio.getAvaluoHasta() != null) {
                query.setParameter("avaluoHasta", datosConsultaPredio.getAvaluoDesde());
            }

            if (datosConsultaPredio.getZonaFisicaId() != null &&
                !datosConsultaPredio.getZonaFisicaId().isEmpty()) {

                query.setParameter("zonaFisica", datosConsultaPredio.getZonaFisicaId());

            }

            if (datosConsultaPredio.getZonaEconomicaId() != null &&
                !datosConsultaPredio.getZonaEconomicaId().isEmpty()) {

                query.setParameter("zonaGeoeconomica", datosConsultaPredio.getZonaEconomicaId());

            }

            if (datosConsultaPredio.getUsoConstruccionSelect() != null &&
                !datosConsultaPredio.getUsoConstruccionSelect().isEmpty()) {

                query.
                    setParameter("usoConstruccion", datosConsultaPredio.getUsoConstruccionSelect());

            }

            if (datosConsultaPredio.getFechaInicioBloqueo() != null) {

                query.
                    setParameter("fechaInicioBloqueo", datosConsultaPredio.getFechaInicioBloqueo());

            }

            if (datosConsultaPredio.getFechaDesbloqueo() != null) {

                query.setParameter("fechaDesbloqueo", datosConsultaPredio.getFechaDesbloqueo());

            }

            answer = (ArrayList<RepReporteEjecucionDetalle>) query.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.warn("RepReporteEjecucionDAOBean#buscarReportePorFiltrosAvanzados: " +
                "La consulta de reporte de ejecucion no devolvió resultados", nrEx);
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

}
