package co.gov.igac.snc.dao.tramite.radicacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.tramite.VRadicacion;
import co.gov.igac.snc.util.FiltroDatosSolicitud;

/**
 * Interfaz para el manejo de radicaciones de solicitudes.
 *
 * @author jamir.avila
 *
 */
@Local
public interface IVRadicacionDAO {

    /**
     * Regresa las solicitudes de cotización que cumplen con los criterios de búsqueda dados.
     *
     * @param filtro objeto de transporte con los criterios de búsqueda.
     * @param desde posición a partir de la cual se deben recuperar los registros resultantes.
     * @param cantidad número de registros a retornar, un valor igual o menor a cero indica que se
     * deben regresar todos los registros cuya posición sea igual o mayor al parámetro desde.
     * @return una lista de objetos Solicitud.
     */
    public List<VRadicacion> buscarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro,
        int desde, int cantidad);

    /**
     * Obtiene el número de solicitudes de cotización que cumplen con los criterios de búsqueda
     * dados.
     *
     * @param filtro objeto de transporte con los criterios de búsqueda.
     * @return la cantidad de solicitudes que coinciden con los criterios de búsqueda.
     */
    public long contarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro);
}
