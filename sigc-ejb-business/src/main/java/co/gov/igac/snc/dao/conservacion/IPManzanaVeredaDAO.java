/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author felipe.cadena
 */
@Local
public interface IPManzanaVeredaDAO extends IGenericJpaDAO<PManzanaVereda, String> {

    /**
     * Retorna las manzanas proyectadas asociadas a un tramite.
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public List<PManzanaVereda> obtenerPorTramiteId(Long tramiteId);
}
