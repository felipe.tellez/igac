/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVComisionDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.HashMap;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class VComisionDAOBean extends GenericDAOWithJPA<VComision, Long> implements IVComisionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(VComisionDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IVComisionDAO#countComisionesSinMemo(java.lang.String, java.util.List, java.lang.String)
     */
    @Implement
    @Override
    public int countComisionesSinMemo(String idTerritorial, List<Long> idTramites,
        String tipoComision) {

        int answer;
        Long tempAnswer;

        try {
            tempAnswer = (Long) this.findOrCountComisionesSinMemo(idTerritorial, true, idTramites,
                tipoComision, null, null);
            answer = (tempAnswer != null) ? tempAnswer.intValue() : 0;
        } catch (ExcepcionSNC ex) {
            LOGGER.error("excepción en VComisionDAOBean#countComisionesSinMemo:");
            throw ex;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVComisionDAO#findComisionesSinMemo(java.lang.String, java.util.List, java.lang.String,
     * java.util.Map, int[])
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<VComision> findComisionesSinMemo(String idTerritorial, List<Long> idTramites,
        String tipoComision, Map<String, String> filters, int... rowStartIdxAndCount) {

        List<VComision> answer = null;

        try {
            answer = (List<VComision>) this.findOrCountComisionesSinMemo(idTerritorial, false,
                idTramites, tipoComision, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("excepción en VComisionDAOBean#findComisionesSinMemo");
            throw ex;
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVComisionDAO#countComisionesParaAdmin(java.lang.String, java.lang.String)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int countComisionesParaAdmin(String idTerritorial, String rolUsuario) {

        int answer = 0;
        Long howMany;

        try {
            howMany = (Long) this.findOrCountComisionesParaAdmin(idTerritorial, rolUsuario, true,
                null, null, null);
            answer = howMany.intValue();
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IVComisionDAO#findComisionesParaAdmin(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.util.Map, int[])
     * @author pedro.garcia
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<VComision> findComisionesParaAdmin(String idTerritorial, String rolUsuario,
        String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        LOGGER.debug("on VComisionDAOBean#findComisionesParaAdmin ...");

        List<VComision> answer = null;

        try {
            answer = (List<VComision>) this.
                findOrCountComisionesParaAdmin(idTerritorial, rolUsuario,
                    false, sortField, sortOrder, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace la búsqueda de comisionesIds que estén en un determinado grupo de estados que las hacen
     * suceptibles de administración por parte del tipo de usuario dado
     *
     * @author pedro.garcia
     *
     * @param idTerritorial id de la territorial
     * @param rolUsuario
     * @param isCount indica si el resultado va a ser el conteo de filas (si es true) o las filas
     * como tal
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     */
    private Object findOrCountComisionesParaAdmin(String idTerritorial, String rolUsuario,
        boolean isCount, String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        Object answer = null;
        List<VComision> answer1;
        Long answer2;

        Query query;
        StringBuilder queryString;
        queryString = new StringBuilder();

        if (isCount) {
            queryString.append("SELECT COUNT(vc) ");
        } else {
            queryString.append("SELECT vc ");
        }

        queryString.append("FROM VComision vc" +
            " WHERE vc.territorialCodigo = :idTerritorialP AND vc.estado IN (");

        //D: el criterio de búsqueda diferente para cada rol son los estados de las comisiones
        if (rolUsuario.equals(ERol.RESPONSABLE_CONSERVACION.getRol())) {
            queryString.append("'").append(EComisionEstado.EN_EJECUCION.getCodigo());
            queryString.append("','").append(EComisionEstado.POR_EJECUTAR.getCodigo());
            queryString.append("','").append(EComisionEstado.SUSPENDIDA.getCodigo());
            queryString.append("','").append(EComisionEstado.APLAZADA.getCodigo()).append("')");
        } else if (rolUsuario.equals(ERol.DIRECTOR_TERRITORIAL.getRol())) {
            queryString.append("'").append(EComisionEstado.POR_APROBAR_AMPLIACION.getCodigo());
            queryString.append("','").append(EComisionEstado.POR_APROBAR_APLAZAMIENTO.getCodigo());
            queryString.append("','").append(EComisionEstado.POR_APROBAR_CANCELACION.getCodigo());
            queryString.append("','").append(EComisionEstado.POR_APROBAR_REACTIVACION.getCodigo());
            queryString.append("','").append(EComisionEstado.POR_APROBAR_SUSPENSION.getCodigo()).
                append("')");
        }

        if (filters != null) {
            for (String campo : filters.keySet()) {
                queryString.append(" AND vc.").append(campo).append(" like :").append(campo);
            }
        }

        if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
            queryString.append(" ORDER BY vc.").append(sortField).append(
                sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
        }

        query = this.entityManager.createQuery(queryString.toString());
        try {
            query.setParameter("idTerritorialP", idTerritorial);
            if (filters != null) {
                for (String campo : filters.keySet()) {
                    query.setParameter(campo, "%" + filters.get(campo) + "%");
                }
            }

            if (isCount) {
                answer2 = (Long) query.getSingleResult();
                answer = answer2;
            } else {
                answer1 = (List<VComision>) this.findInRangeUsingQuery(query, rowStartIdxAndCount);
                answer = answer1;
            }

        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "VComisionDAOBean#countComisionesParaAdmin");
        }

        return answer;
    }

    // ------------------------------------------------ //
    /**
     * @see IVComisionDAO#buscarComisionesParaAprobar(java.util.List, java.lang.String,
     * java.lang.String, java.lang.String, java.util.Map, int[])
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Implement
    @Override
    public List<VComision> buscarComisionesParaAprobar(List<Long> idTramites, String tipoComision,
        String sortField, String sortOrder, Map<String, String> filters, int... rowStartIdxAndCount) {

        LOGGER.debug("VComisionDAOBean#buscarComisionesParaAprobar ...");

        List<VComision> answer = null;
        try {
            answer = (List<VComision>) this.buscarOContarComisionesParaAprobar(idTramites,
                tipoComision, false, sortField, sortOrder, filters, rowStartIdxAndCount);
        } catch (ExcepcionSNC ex) {
            throw ex;
        }
        return answer;
    }

    // ------------------------------------------------ //
    /**
     *
     * Método copiado del método findOrCountComisionesParaAdmin de pedro.garcia y ajustado para
     * administrar comisionesIds.
     *
     * Hace la búsqueda de comisionesIds que estén asociadas con trámites cuyos ids estén en el
     * arreglo que se pasa como parametro y que estén en el estado de aprobar comisión.
     *
     * @author david.cifuentes
     *
     * @param idTramites lista de ids de trámites que se buscan
     * @param tipoComision tipo de comisiones que se buscan
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters Parámetros usados cuando se hacen filtros en las tablas
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     */
    /*
     * @modified juanfelipe.garcia - Adición de logica para manejar los filtros de las tablas en la
     * capa web. @modified pedro.garcia 26-06-2013 manejo de excepciones en la consulta de ids de
     * comisión 27-06-2013 adición de parámetro para tener en cuenta el tipo de comisión arreglo de
     * documentación
     */
    @SuppressWarnings("unchecked")
    private Object buscarOContarComisionesParaAprobar(List<Long> idTramites, String tipoComision,
        boolean isCount, String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        Object answer = null;
        List<VComision> answer1;
        Long answer2;
        String queryStringComisiones;

        Query query, queryComisiones;
        StringBuilder queryString;

        List<Long> comisionesIds = null;
        queryStringComisiones = "" +
            "SELECT tc.comision.id FROM ComisionTramite tc " +
            "WHERE tc.tramite.id IN (:idTramites) " +
            "AND tc.comision.tipo = :tipoComisionP";

        queryComisiones = this.entityManager.createQuery(queryStringComisiones);

        try {
            queryComisiones.setParameter("idTramites", idTramites);
            queryComisiones.setParameter("tipoComisionP", tipoComision);

            comisionesIds = queryComisiones.getResultList();
        } catch (Exception ex) {
            LOGGER.error(
                "Error en VComisionDAOBean#buscarOContarComisionesParaAprobar consultando " +
                "los ids de comisiones relacionadas con los trámites: " + ex.getMessage());
            return answer;
        }

        if (comisionesIds != null && !comisionesIds.isEmpty()) {

            queryString = new StringBuilder();

            String comisionEstado = EComisionEstado.POR_APROBAR.getCodigo();

            if (isCount) {
                queryString.append("SELECT COUNT(vc) ");
            } else {
                queryString.append("SELECT vc ");
            }

            queryString.append("FROM VComision vc WHERE vc.estado = :comisionEstado AND " +
                " vc.numero IS NOT NULL" +
                " AND vc.id in :idComisiones");

            //variable que almacena los valores de los filtros
            Map<String, Object> valoresFiltros = new HashMap<String, Object>();

            if (filters != null && !filters.isEmpty()) {
                String condicion = "";
                //recorre los campos y asigna el fragmento del query requerido por cada filtro 
                for (String campo : filters.keySet()) {
                    if (campo.equals("numero") || campo.equals("municipio") || campo.equals(
                        "nombreFuncionarioEjecutor")) {
                        if (campo.equals("numero")) {
                            condicion += " AND vc." + campo + " like :numero";
                            valoresFiltros.put("numero", "%" + filters.get(campo) + "%");
                        }
                        if (campo.equals("municipio")) {
                            condicion += " AND vc." + campo + " like :municipio";
                            valoresFiltros.put(campo, "%" + filters.get(campo) + "%");
                        }
                        if (campo.equals("nombreFuncionarioEjecutor")) {
                            condicion += " AND vc." + campo + " like :nombreFuncionarioEjecutor";
                            valoresFiltros.put(campo, "%" + filters.get(campo) + "%");
                        }
                    }
                }
                queryString.append(condicion);
            }

            //-------------------------
            if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
                queryString.append(" ORDER BY vc.").append(sortField).append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

            query = this.entityManager.createQuery(queryString.toString());
            try {
                query.setParameter("comisionEstado", comisionEstado);
                query.setParameter("idComisiones", comisionesIds);

                //setParameter de los parametros del filtro
                if (valoresFiltros.size() > 0) {
                    for (String campo : valoresFiltros.keySet()) {
                        query.setParameter(campo, valoresFiltros.get(campo));
                    }
                }

                if (isCount) {
                    answer2 = (Long) query.getSingleResult();
                    answer = answer2;
                } else {
                    answer1 = (List<VComision>) this.findInRangeUsingQuery(query,
                        rowStartIdxAndCount);
                    answer = answer1;
                }
            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "VComisionDAOBean#buscarOContarComisionesParaAprobar");
            }
        }

        return answer;
    }

    /**
     * @see IVComisionDAO#contarComisionesParaAprobar(java.util.List, java.lang.String)
     * @author david.cifuentes
     */
    @Implement
    @Override
    public int contarComisionesParaAprobar(List<Long> idTramites, String tipoComision) {
        int answer = 0;
        Long howMany;

        try {
            howMany = (Long) this.buscarOContarComisionesParaAprobar(idTramites, tipoComision,
                true, null, null, null);
            if (howMany != null) {
                answer = howMany.intValue();
            }
        } catch (ExcepcionSNC ex) {
            throw ex;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * @param idTerritorial id de la territorial
     * @param idTramites ids de los trámites que vienen dados por el proceso, sobre los cuales se
     * hace la búsqueda
     * @param isCount indica si el resultado va a ser el conteo de filas (si es true) o las filas
     * como tal
     * @param tipoComision nombre del tipo de comisión
     * @param filters filtros de búsqueda de la consulta
     * @param rowStartIdxAndCount Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return Un objeto que es un Long si se quiere contar, o una List<VComision> si se quieren las
     * filas, o null si la consulta no trae resultados.
     */
    private Object findOrCountComisionesSinMemo(String idTerritorial, boolean isCount,
        List<Long> idTramites, String tipoComision, Map<String, String> filters,
        int... rowStartIdxAndCount) {

        Object answer = null;
        Long answer2;
        List<VComision> answer1 = null;
        Query query;
        Query queryComisiones;
        String comisionState, stringQuery;
        StringBuilder queryStringB;

        //D: Se buscan las comisionesIds unicamente relacionadas con los tramites que vienen del proceso.
        List<Long> comisiones;

        stringQuery = "" +
            "SELECT tc.comision.id FROM ComisionTramite tc " +
            "WHERE tc.tramite.id IN :idTramites " +
            "AND tc.comision.tipo = :tipoComision_p";

        queryComisiones = this.entityManager.createQuery(stringQuery);

        queryComisiones.setParameter("idTramites", idTramites);
        queryComisiones.setParameter("tipoComision_p", tipoComision);

        try {
            comisiones = queryComisiones.getResultList();
        } catch (Exception ex) {
            LOGGER.error("excepción obteniendo las comisiones relacionadas con los trámites en " +
                "VComisionDAOBean#findOrCountComisionesSinMemo: " + ex.getMessage());
            return null;
        }

        if (comisiones != null && !comisiones.isEmpty()) {
            queryStringB = new StringBuilder();

            if (isCount) {
                queryStringB.append("SELECT COUNT(vc) ");
            } else {
                queryStringB.append("SELECT vc ");
            }

            queryStringB.append(" FROM VComision vc WHERE vc.estado = :estadoComision AND " +
                " vc.territorialCodigo = :idTerritorial AND vc.numero IS NOT NULL" +
                " AND vc.id IN :idComisiones");

            if (filters != null) {
                if (filters.containsKey("numero")) {
                    queryStringB.append(" AND vc.numero LIKE :numero");
                }
                if (filters.containsKey("nombreFuncionarioEjecutor")) {
                    queryStringB.append(
                        " AND vc.nombreFuncionarioEjecutor LIKE :nombreFuncionarioEjecutor");
                }
            }

            query = this.entityManager.createQuery(queryStringB.toString());

            comisionState = EComisionEstado.CREADA.name();
            query.setParameter("idTerritorial", idTerritorial);
            query.setParameter("estadoComision", comisionState);
            query.setParameter("idComisiones", comisiones);

            if (filters != null) {
                if (filters.containsKey("numero")) {
                    query.setParameter("numero", "%" + filters.get("numero") + "%");
                }
                if (filters.containsKey("nombreFuncionarioEjecutor")) {
                    query.setParameter("nombreFuncionarioEjecutor", "%" +
                        filters.get("nombreFuncionarioEjecutor") + "%");
                }
            }

            try {
                if (isCount) {
                    answer2 = (Long) query.getSingleResult();
                    answer = answer2;
                } else {
                    answer1 = (List<VComision>) this.findInRangeUsingQuery(query,
                        rowStartIdxAndCount);
                    answer = answer1;
                }
            } catch (Exception ex) {
                throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                    "VComisionDAOBean#findOrCountComisionesSinMemo");
            }

        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------    

//end of class
}
