package co.gov.igac.snc.dao.generales.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ILogAccesoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.util.EEstadoSesion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementaciôn del DAO para LogAcceso
 *
 * @author felipe.cadena
 *
 */
@Stateless
public class LogAccesoDAOBean extends GenericDAOWithJPA<LogAcceso, Long> implements ILogAccesoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogAccesoDAOBean.class);

    /**
     * @see ILogAccesoDAO#obtenerPorLogin(java.lang.String)
     * @author felipe.cadena
     * @return
     */
    @Implement
    @Override
    public LogAcceso obtenerPorLogin(String usuario) {

        LogAcceso resultado = null;
        String queryString = "SELECT la FROM LogAcceso la WHERE LOWER(la.login) = LOWER(:usuario)" +
            " AND UPPER(la.estadoSesion) = UPPER(:estado)";

        try {
            Query query = entityManager.createQuery(queryString);
            query.setParameter("usuario", usuario);
            query.setParameter("estado", EEstadoSesion.INICIADA.toString());

            resultado = (LogAcceso) query.getSingleResult();
        } catch (NoResultException e) {
            //N: esto no se debe tratar como error
            LOGGER.info(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("LogAccesoDAOBean#obtenerPorLogin", ESeveridadExcepcionSNC.ERROR,
                e.getMessage(), null);
        }

        return resultado;
    }

    /**
     * @see ILogAccesoDAO#obtenerPorLogin(java.lang.String)
     * @author felipe.cadena
     * @return
     */
    @Implement
    @Override
    public Long obtenerNumeroAccesosIniciadosPorUsuario(String usuario) {

        Long resultado = null;
        String queryString = "SELECT COUNT(la) FROM LogAcceso la WHERE LOWER(la.login) = LOWER(:usuario)" +
            " AND UPPER(la.estadoSesion) = UPPER(:estado)";

        try {
            Query query = entityManager.createQuery(queryString);
            query.setParameter("usuario", usuario);
            query.setParameter("estado", EEstadoSesion.INICIADA.toString());

            resultado = (Long) query.getSingleResult();
        } catch (NoResultException e) {
            //N: esto no se debe tratar como error
            LOGGER.info(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("LogAccesoDAOBean#obtenerPorLogin", ESeveridadExcepcionSNC.ERROR,
                e.getMessage(), null);
        }

        return resultado;
    }

}
