package co.gov.igac.snc.dao.actualizacion.impl;/*
 * Proyecto SNC 2018 
 */

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.VCargueCica;
import co.gov.igac.snc.util.FiltroCargueDTO;

import javax.ejb.Local;
import java.util.List;

/**
  * Persistencia asociada a las clase undefined 
 * 
 * @author felipe.cadena 
*/
@Local
public interface IVCargueCicaDAO extends IGenericJpaDAO<VCargueCica, Long> {

    /**
     * Busca los predios proyectados en un cargue cica-snc
     *
     * @author felipe.cadena
     *
     * @param filtroCargueDTO
     * @return
     */
    public List<VCargueCica> buscarProyeccionCargueCica(FiltroCargueDTO filtroCargueDTO);

     /**
     * Calcula resumen de los predios agrupando por zona-sector-manzana
     *
     * @author felipe.cadena
     *
     * @param filtroCargueDTO
     * @return
     */
     public List<Object[]> buscarResumenPredios(FiltroCargueDTO filtroCargueDTO);

     /**
     * Calcula los rangos para los filtros avanzados
     *
     * @author felipe.cadena
     *
     * @param filtroCargueDTO
     * @return
     */
     public Object[] buscarRangosFiltros(FiltroCargueDTO filtroCargueDTO);

    /**
     * Retorna el total de los predios proyectados en un cargue cica-snc
     *
     * @author felipe.cadena
     *
     * @param filtroCargueDTO
     * @return
     */
    public int countProyeccionCargueCica(FiltroCargueDTO filtroCargueDTO);

    /**
     * Retorna los items existentes segun los items dados
     *
     * @author felipe.cadena
     *
     * @param predios
     * @return
     */
    public List<VCargueCica> buscarPrediosProyectados(List<String> predios);

    /**
     * Retorna los predios asociados a los enviados en la proyeccion de cargue
     *
     * @author felipe.cadena
     *
     * @param predios
     * @return
     */
    public List<VCargueCica> buscarPrediosAsociadosCargue(List<VCargueCica> predios);

}