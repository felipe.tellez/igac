package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IVariableEvaluacionAvaluoDAO;
import co.gov.igac.snc.persistence.entity.avaluos.VariableEvaluacionAvaluo;
import javax.ejb.Stateless;

/**
 * @see IVariableEvaluacionAvaluoDAO
 *
 * @author felipe.cadena
 */
@Stateless
public class VariableEvaluacionAvaluoDAOBean extends
    GenericDAOWithJPA<VariableEvaluacionAvaluo, Long> implements
    IVariableEvaluacionAvaluoDAO {

}
