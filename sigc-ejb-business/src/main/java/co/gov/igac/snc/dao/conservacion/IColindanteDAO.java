/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Colindante;

/**
 * Interfaz para los métodos de bd de la tabla COLINDANTE
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Local
public interface IColindanteDAO extends IGenericJpaDAO<Colindante, Long> {

    /**
     * Determina si el predio con número predial 'este' es colindante con TODOS los predios cuyos
     * números prediales están en la lista 'estosOtros'
     *
     * @author pedro.garcia
     *
     * @param este número predial del predio que se quiere saber si es colindante
     * @param estosOtros números prediales de los predios con que se va a comparar
     * @return true si sí
     */
    public boolean isAdjacent(String este, List<String> estosOtros);

    /**
     * Método que recupera la lista de predios colindantes de un predio por número predial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public List<String> findPrediosColindantesByNumeroPredial(
        String numeroPredial);

    /**
     * Método que recupera la lista entidades tipo Colindante relacionadas al numero predial
     * enviado.
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public List<Colindante> buscarColindantesNumeroPredial(
        String numeroPredial);

}
