/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;

/**
 * @author david.cifuentes
 */
@Local
public interface IEntidadDAO extends IGenericJpaDAO<Entidad, Long> {

    /**
     * Método que realiza la búsqueda de entidades por un nombre y un estado enviados como
     * parámetros.
     *
     * @author david.cifuentes
     *
     * @param nombreEntidad
     * @param estadoEntidad
     * @return
     */
    public List<Entidad> buscarEntidadesPorNombreYEstado(String nombreEntidad,
        String estadoEntidad);

    /**
     * Método que realiza la búsqueda de entidades por un nombre, estado, departamento y municipio
     * enviados como parámetros.
     *
     * @author dumar.penuela
     *
     * @param nombreEntidad
     * @param estadoEntidad
     * @param departamento
     * @param municipio
     * @return
     */
    public List<Entidad> buscarEntidades(String nombreEntidad,
        String estadoEntidad, String departamento, String municipio);

    /**
     * Método que realiza la búsqueda de las {@link Entidad} por estado
     *
     * @author david.cifuentes
     *
     * @param estado
     */
    public List<Entidad> buscarEntidadesPorEstado(String estado);

    /**
     * Método que realiza la búsqueda de las {@link Entidad}
     *
     * @param nombre
     * @param codDepartamento
     * @param codMunicipio
     * @param correo
     * @return
     */
    public Entidad buscarEntidad(String nombre, String codDepartamento, String codMunicipio,
        String correo);

    /**
     * Método que realiza la verificación de la existencia de entidades{@link Entidad}
     *
     * @param nombreEntidad
     * @param estadoEntidad
     * @param codDepartamento
     * @param codMunicipio
     * @return
     */
    public boolean validaExistenciaEntidad(String nombreEntidad, String estadoEntidad,
        String codDepartamento, String codMunicipio);
}
