package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IEstadisticaGerencialManzanaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialManzana;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialManzanaPK;

@Stateless
public class EstadisticaGerencialManzanaDAOBean extends GenericDAOWithJPA<EstadisticaGerencialManzana, EstadisticaGerencialManzanaPK>
    implements IEstadisticaGerencialManzanaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        EstadisticaGerencialManzanaDAOBean.class);

    public List<Object[]> consultaPorMunicipio(Long anio, String codigoMunicipio, String zona) {

        String queryString =
            "select m.nombre, egp.anio, egp.zona, sum(cantidad_manzanas_veredas) as cantidad_manzanas_veredas, sum(cantidad_predios) as cantidad_predios, sum(cantidad_propietarios) as cantidad_propietarios, sum(area_terreno) as area_terreno, sum(area_construccion) as area_construccion, sum(valor_avaluo) as valor_avaluo, sum(predios_condicion_0) as predios_condicion_0, sum(predios_condicion_1) as predios_condicion_1, sum(predios_condicion_2) as predios_condicion_2, sum(predios_condicion_3) as predios_condicion_3, sum(predios_condicion_4) as predios_condicion_4, sum(predios_condicion_5) as predios_condicion_5, sum(predios_condicion_6) as predios_condicion_6, sum(predios_condicion_7) as predios_condicion_7, sum(predios_condicion_8) as predios_condicion_8, sum(predios_condicion_9) as predios_condicion_9 " +
            "from estadistica_gerencial_manzana egp " +
            "inner join municipio m on egp.municipio_codigo = m.codigo " +
            "where m.codigo= :codigoMun  and egp.anio= :anio " + (zona != null ?
                " and egp.zona = '" + zona + "'" : "") +
            "group by m.nombre, egp.anio, egp.zona";

        Query q = this.entityManager.createNativeQuery(queryString);
        q.setParameter("codigoMun", codigoMunicipio);
        q.setParameter("anio", anio);

        return q.getResultList();

    }

}
