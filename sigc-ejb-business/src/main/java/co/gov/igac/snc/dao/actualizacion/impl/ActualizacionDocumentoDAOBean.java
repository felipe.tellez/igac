package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDocumentoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;

/**
 * Implementación de los servicios de persistencia del objeto ActualizaciónDocumento.
 *
 * @author franz.gamba
 */
@Stateless
public class ActualizacionDocumentoDAOBean extends GenericDAOWithJPA<ActualizacionDocumento, Long>
    implements IActualizacionDocumentoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionDocumentoDAOBean.class);

    @Override
    public List<ActualizacionDocumento> findByActualizacionId(
        long actualizacionId) {
        Query consulta = entityManager.createNamedQuery("findByActualizacionId");
        consulta.setParameter("actualizacionId", actualizacionId);

        @SuppressWarnings("unchecked")
        List<ActualizacionDocumento> actualizacionDocumentos = consulta.getResultList();
        return actualizacionDocumentos;
    }

}
