package co.gov.igac.snc.dao.avaluos.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IOfertaPredioDAO;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;

@Stateless
public class OfertaPredioDAOBean extends GenericDAOWithJPA<OfertaPredio, Long>
    implements IOfertaPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OfertaPredioDAOBean.class);

}
