package co.gov.igac.snc.util.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Excepción lanzada durante la ejecución de un servicio Remoto
 *
 * @author juan.mendez
 *
 */
public class RemoteServiceException extends Exception {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteServiceException.class);

    /**
     *
     */
    private static final long serialVersionUID = 2864791211933108731L;

    /**
     *
     * @param message
     * @param exception
     */
    public RemoteServiceException(String message, Throwable exception) {
        super(message, exception);
        LOGGER.error(message);
        LOGGER.error(exception.toString());
    }

    public RemoteServiceException(String message) {
        super(message);
        LOGGER.error(message);
    }

}
