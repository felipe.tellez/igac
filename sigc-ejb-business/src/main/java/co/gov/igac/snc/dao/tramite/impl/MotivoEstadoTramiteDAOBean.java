/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.IMotivoEstadoTramiteDAO;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;

@Stateless
public class MotivoEstadoTramiteDAOBean extends
    GenericDAOWithJPA<MotivoEstadoTramite, Long> implements
    IMotivoEstadoTramiteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MotivoEstadoTramiteDAOBean.class);

}
