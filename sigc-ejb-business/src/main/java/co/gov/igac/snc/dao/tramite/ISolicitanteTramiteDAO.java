/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla SOLICITANTE_TRAMITE
 *
 * @author pedro.garcia
 */
@Local
public interface ISolicitanteTramiteDAO extends IGenericJpaDAO<SolicitanteTramite, Long> {

    /**
     * busca los registros donde el id del trámite sea el buscado. Hace fetch del solicitante, país,
     * departamento y municipio
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public List<SolicitanteTramite> findByTramiteId(Long tramiteId);

    /**
     * Retorna una lista de solicitanteTramite a partir del id de la solicitud
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public List<SolicitanteTramite> findBySolicitudId(Long solicitudId);

//end of interface
}
