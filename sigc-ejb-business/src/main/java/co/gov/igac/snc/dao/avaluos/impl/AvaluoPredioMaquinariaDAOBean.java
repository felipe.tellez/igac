package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioMaquinariaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IAvaluoPredioMaquinariaDAO
 * @author felipe.cadena
 */
@Stateless
public class AvaluoPredioMaquinariaDAOBean extends
    GenericDAOWithJPA<AvaluoPredioMaquinaria, Long> implements IAvaluoPredioMaquinariaDAO {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(AvaluoPredioMaquinariaDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvaluoPredioMaquinariaDAO#calcularMaquinariaPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public Boolean calcularMaquinariasPreviasAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "SELECT o FROM AvaluoPredioMaquinaria o " +
            "JOIN o.avaluo ava WHERE ava.id = :idAvaluo";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo", idAvaluo);

            List<AvaluoPredioMaquinaria> apzs = query.getResultList();
            if (apzs == null || apzs.isEmpty()) {
                return null;
            }

            if (apzs.get(0).getAvaluoPredioId() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#calcularCultivosPreviosAvaluo");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IAvaluoPredioMaquinariaDAO#consultarPorAvaluo(long)
     * @author pedro.garcia
     */
    @Implement
    @Override
    public List<AvaluoPredioMaquinaria> consultarPorAvaluo(long avaluoId) {

        LOGGER.debug("on AvaluoPredioMaquinariaDAOBean#consultarPorAvaluo");

        List<AvaluoPredioMaquinaria> answer = null;
        String queryString;
        Query query;

        queryString = "" +
            " SELECT apm FROM AvaluoPredioMaquinaria apm WHERE apm.avaluo.id = :idAvaluo_p";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idAvaluo_p", avaluoId);
            answer = query.getResultList();
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "AvaluoPredioMaquinariaDAOBean#consultarPorAvaluo");
        }

        return answer;

    }

//end of class
    /**
     * @see IAvaluoPredioMaquinariaDAO#eliminarMaquinariaPreviasAvaluo(Long)
     * @author felipe.cadena
     */
    @Override
    public boolean eliminarMaquinariaPreviasAvaluo(Long idAvaluo) {

        String queryString;
        Query query;

        queryString = "DELETE FROM avaluo_predio_maquinaria ctv WHERE ctv.avaluo_id = " + idAvaluo;

        try {
            query = this.entityManager.createNativeQuery(queryString);
            query.executeUpdate();
            return true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(LOGGER, e, "AvaluoDAOBean#eliminarMaquinariaPreviasAvaluo");
        }
    }

}
