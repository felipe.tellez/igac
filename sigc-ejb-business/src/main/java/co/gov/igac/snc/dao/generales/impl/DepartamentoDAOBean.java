package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class DepartamentoDAOBean extends GenericDAOWithJPA<Departamento, String> implements
    IDepartamentoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartamentoDAOBean.class);

//--------------------------------------------------------------------------------------------------    
    @Override
    public List<Departamento> findByPais(String codigoPais) {
        Query query = this.entityManager.createQuery(
            "Select d from Departamento d where d.pais.codigo = :paisId");
        query.setParameter("paisId", codigoPais);
        return query.getResultList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IDepartamentoDAO#findByEntidadTerritorial(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public List<Departamento> findByEntidadTerritorial(String entidadTerritorialCode) {
        Query query;
        String queryString;
        List<Departamento> answer;

        queryString = "SELECT * FROM DEPARTAMENTO D WHERE D.CODIGO IN ( " +
            "SELECT M.DEPARTAMENTO_ID FROM MUNICIPIO M WHERE M.CODIGO IN (" +
            "SELECT j.municipio_codigo FROM jurisdiccion j WHERE j.estructura_organizacional_cod IN (" +
            "SELECT codigo FROM estructura_organizacional WHERE codigo= :territorialId OR estructura_organizacional_cod= :territorialId )" +
            ") GROUP BY M.DEPARTAMENTO_ID" +
            ")";

        query = this.entityManager.createNativeQuery(queryString, Departamento.class);
        query.setParameter("territorialId", entidadTerritorialCode);
        answer = (List<Departamento>) query.getResultList();

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see IDepartamentoDAO#getDepartamentoByCodigo(String)
     */
    @Override
    public Departamento getDepartamentoByCodigo(String codigo) {
        Query q = entityManager.createNamedQuery("findDepartamentoByCodigo");
        q.setParameter("codigo", codigo);
        Departamento d = (Departamento) q.getSingleResult();
        return d;
    }

//----------------------------------------------------------------------------------------------//
    /**
     * @see IDepartamentoDAO#buscarDepartamentosConCatastroCentralizadoPorCodigoPais(String)
     */
    @Override
    public List<Departamento> buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
        String codigoPais) {

        String sqlQuery = "SELECT DISTINCT d FROM Departamento d, Jurisdiccion j, Municipio m " +
            "WHERE m.departamento.codigo = d.codigo AND m.codigo = j.municipio.codigo AND d.pais.codigo = :codigoPais";

        Query query = this.entityManager.createQuery(sqlQuery);

        try {
            query.setParameter("codigoPais", codigoPais);
            return query.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "DepartamentoDAOBean#buscarDepartamentosConCatastroCentralizadoPorCodigoPais");
        }
    }
//----------------------------------------------------------------------------------------------	

    /**
     * Metodo que retorna los departamentos de una UOC
     *
     * @param codigoUoc
     * @return
     */
    @Override
    public List<Departamento> buscarDepartamentosByUOC(String codigoUoc) {
        String sql =
            "SELECT distinct d FROM Jurisdiccion j INNER JOIN j.municipio m INNER JOIN m.departamento d WHERE j.estructuraOrganizacional.codigo = :codigoUoc ";
        Query query = this.entityManager.createQuery(sql);
        query.setParameter("codigoUoc", codigoUoc);
        return query.getResultList();
    }

}
