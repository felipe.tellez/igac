package co.gov.igac.snc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class NetClient {

    public static boolean GET(String baseUrl, Map<String, String> params) {

        if (params != null) {
            //TODO:: implementar uso con parametros en url
        }

        try {

            URL url = new URL(baseUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " +
                    conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();
            return false;

        } catch (IOException e) {

            e.printStackTrace();
            return false;

        }

        return true;

    }

    // http://localhost:8080/RESTfulExample/json/product/get
    public static void main(String[] args) {

        SNCPropertiesUtil props = SNCPropertiesUtil.getInstance();
        String baseUrl = props.getProperty("reports.server.async.url");

        GET(baseUrl + "3272", null);

    }

}
