/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;

/**
 *
 * Interfaz para el bean de consulta predio
 *
 * @author pedro.garcia
 */
@Local
public interface IConsultaPredio {

    /**
     * Retorna la lista de Estructuras Organizacionales que son Dirección Territorial
     *
     * @author pedro.garcia
     */
    public List<EstructuraOrganizacional> getTerritorialesList();

    /**
     * Retorna la lista de Departamentos dependiendo de la Territorial seleccionada
     *
     * @param territorialCode Código de la Territorial
     */
    public List<Departamento> getDepartamentosList(String territorialCode);

    /**
     * Retorna la lista de Municipios dependiendo del departamento seleccionado
     *
     * @param departamentoCode Código del Departamento
     */
    public List<Municipio> getMunicipiosList(String departamentoCode);

    /**
     * Retorna un predio por su número predial y además obtiene los datos de la tabla
     * persona_predio_propiedad asociados.
     *
     * @author fabio.navarrete
     * @param numPredial número predial del predio a obtener.
     * @return
     */
    public Predio getPredioFetchDerechosPropiedadByNumeroPredial(String numPredial);

    /**
     * Retorna un predio por el número predial especificado y además obtiene los datos de la tabla
     * persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se requiera inicializar
     * los datos personPredio asociados al predio.
     *
     * @param numPredial número predial del predio a obtener
     * @return
     */
    public Predio getPredioFetchPersonasByNumeroPredial(String numPredial);

    /**
     * Retorna un predio por el id del predio especificado y además obtiene los datos de la tabla
     * persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se requiera inicializar
     * los datos personPredio asociados al predio.
     *
     * @param predioId número predial del predio a obtener
     * @return
     */
    public Predio getPredioFetchPersonasById(Long predioId);

    /**
     * Retorna un predio por el número predial especificado y además obtiene los datos de la tabla
     * predio_avaluo_catastral asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos predioAvaluoCatastral asociados al predio.
     *
     * @param numPredial número predial del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchAvaluosByNumeroPredial(String numPredial);

    /**
     * Retorna un predio según su identificador especificado y además obtiene los datos de la tabla
     * predio_avaluo_catastral asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos predioAvaluoCatastral asociados al predio.
     *
     * @param predioId identificador del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchAvaluosByPredioId(Long predioId);

    /**
     * @author fabio.navarrete Retorna un predio con los datos de ubicación asociados según el
     * número predial ingresado
     * @param numPredial número predial del predio a obtener
     * @return Predio requerido con las listas inicializadas
     */
    public Predio getPredioFetchDatosUbicacionByNumeroPredial(String numPredial);

    /**
     * @author juan.agudelo Retorna un predio por el número predial especificado y además obtiene
     * los datos de la tabla departamento, municipio y persona_predio asociados a éste.
     * @param numPredial número predial del predio a obtener
     * @return Predio requerido con las listas inicializadas
     */
    public Predio getPredioDatosFetchPersonasByNumeroPredial(String numPredial);

    /**
     * Retorna un predio por el identificador del predio especificado y además obtiene los datos de
     * la tabla persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos personaPredio asociados al predio.
     *
     * @param predioId
     * @return
     */
    public Predio findPredioFetchPersonasByPredioId(Long predioId);

    /**
     * Busca un predio dado su id haciendo fetch de los datos relacionados con la ubicación del
     * predio (pestaña "ubicación" en detalles del predio)
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId id del predio buscado
     * @return
     */
    public Predio getPredioFetchDatosUbicacionById(Long predioId);

    /**
     * Retorna un predio y obtiene los datos de la tabla predio_avaluo_catastral asociados a éste.
     * Sólo debe usarse para la consulta del detalle del avalúo en la consulta de detalles del
     * predio.
     *
     * @param predioId id del predio que se consulta
     * @return
     * @author pedro.garcia
     */
    public Predio consultarDetalleAvaluoPredioByPredioId(Long predioId);

}
