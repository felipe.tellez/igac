package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;

@Local
public interface IPFichaMatrizTorreDAO extends IGenericJpaDAO<PFichaMatrizTorre, Long> {

    /**
     * Método que consulta todas las entidades PFichaMatrizTorre según el id de la PFichaMatriz
     * asociada.
     *
     * @param fichaMatrizId
     * @return
     * @author fabio.navarrete
     */
    public List<PFichaMatrizTorre> findByFichaMatrizId(Long fichaMatrizId);

    /**
     * Método que consulta todas las entidades PFichaMatrizTorre según el id de la PFichaMatriz
     * asociada.
     *
     * @param fichaMatrizId
     * @return
     * @author leidy.gonzalez
     */
    public List<PFichaMatrizTorre> buscarPFichaMatrizTorreporFichaMatrizId(Long fichaMatrizId);

}
