package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import java.util.Date;

@Local
public interface IPPredioZonaDAO extends IGenericJpaDAO<PPredioZona, Long> {

    /**
     * Método que busca las zonas homogéneas de un {@link PPredio} por su id.
     *
     * @author david.cifuentes
     * @param idPPredio
     * @return
     */
    public List<PPredioZona> buscarZonasHomogeneasPorPPredioId(Long idPPredio);

    /**
     * Método que actualiza el area de las predioZonas (en caso de que solo exista una por predio)
     *
     * @author franz.gamba
     * @param predio
     * @param area
     * @return
     */
    public boolean updateAreaPorPredioZonaByPredioId(String predio, Double area);

    /**
     * Método para traer las zonas de la ultima vigencia
     *
     * @author felipe.cadena
     * @param idPPredio
     * @return
     */
    public List<PPredioZona> buscarZonasPorPPredioId(Long idPPredio);

    /**
     * Método para consultar las zonas dependiendo en estado de proyección, solo consulta las de la
     * vigencia actual
     *
     * @author felipe.cadena
     * @param idPPredio
     * @param cancelaInscribe
     * @return
     */
    public List<PPredioZona> buscarZonasPorPPredioIdYEstadoP(Long idPPredio, String cancelaInscribe);

    /**
     * Método para consultar las zonas dependiendo en estado de proyección, consulta todas las
     * vigencias
     *
     * @author felipe.cadena
     * @param idPPredio
     * @param cancelaInscribe
     * @return
     */
    public List<PPredioZona> buscarTodasZonasPorPPredioIdYEstadoP(Long idPPredio,
        String cancelaInscribe);

    /**
     * Método para consultar las zonas de un predio
     *
     * @author felipe.cadena
     * @param idPPredio
     * @return
     */
    public List<PPredioZona> buscarTodasZonasPorPPredioId(Long idPPredio);

    /**
     * Método que busca las zonas homogéneas de un {@link PPredio} por su número predial.
     *
     * @author javier.aponte
     * @param numeroPredial
     * @return
     */
    public List<PPredioZona> buscarZonasHomogeneasPorNumeroPredialPPredio(String numeroPredial);

    /**
     * Método para consultar las zonas de un predio
     *
     * @author felipe.cadena
     * @param idPPredio,vigencia
     * @return
     */
    public List<PPredioZona> buscarZonasPorPPredioIdYVigencia(Long idPPredio, Date vigencia);

    /**
     * Método para consultar las zonas de un predio que no esten canceladas ni la fecha de
     * inscripcion sea mayor a la actual
     *
     * @author leidy.gonzalez
     * @param idPPredio
     * @return
     */
    public List<PPredioZona> buscarZonasNoCanceladaPorPPredioId(Long idPPredio);

    /**
     * Método para consultar las zonas de un tramite que esten con cancela/inscribe Temp
     *
     * @author leidy.gonzalez
     * @param idTram
     * @return
     */
    public List<PPredioZona> buscarZonasTempPorIdTramite(Long idTram);
    
    /**
     * Método para consultar las zonas de acuerdo a una lista de id's
     *
     * @author leidy.gonzalez
     * @param id
     * @return
     */
    public List<PPredioZona> obtenerPZonasPorIds(List<Long> id);

}
