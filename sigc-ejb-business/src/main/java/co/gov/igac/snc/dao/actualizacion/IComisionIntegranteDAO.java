package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ComisionIntegrante;

/**
 * Servicios de persistencia para el objeto ComisionIntegrante
 *
 * @author franz.gamba
 */
@Local
public interface IComisionIntegranteDAO extends IGenericJpaDAO<ComisionIntegrante, Long> {

    /**
     * Método qeu retorna los integraqntes de una comision para una actualizaicon y con un objeto
     *
     * @param actualizacionId
     * @param objeto
     * @return
     */
    public List<ComisionIntegrante> getcomisionIntegrantesByActualizacionIdAndObjeto(
        Long actualizacionId, String objeto);
}
