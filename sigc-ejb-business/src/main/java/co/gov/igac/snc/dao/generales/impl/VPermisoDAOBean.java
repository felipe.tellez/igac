package co.gov.igac.snc.dao.generales.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IVPermisoDAO;
import co.gov.igac.snc.persistence.entity.generales.VPermiso;
import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;

/**
 * Implementación de los servicios de persistencia del objeto {@link VPermiso}.
 *
 * @author fedy.wilches
 */
@Stateless
public class VPermisoDAOBean extends GenericDAOWithJPA<VPermiso, BigDecimal>
    implements IVPermisoDAO {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPermisoDAOBean.class);

    // ------------------------------------------------ //
    /**
     * @author fredy.wilches
     * @see IVPermisoDAO#getMenu(String[] roles)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VPermisoGrupo> getMenu(String[] roles) {

        // camino, sistema_id, max(consulta), max(crea), max(edita)
        // ,max(elimina) , max(ejecuta)
        String sql =
            "select NEW co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo(nivel, id, tipo, nombre, url, urlAyuda, orden, padreComponenteId, camino, sistemaId, ambito, max(consulta), max(crea), max(edita), max(elimina), max(ejecuta)) " +
            " from VPermisoGrupo v" +
            " where UPPER(v.grupo) in (" + getRoles(roles) + ")" +
            " group by nivel, id, tipo, nombre, url, urlAyuda, orden, padreComponenteId, camino, sistemaId, ambito" +
            " order by camino";
        Query query = this.entityManager.createQuery(sql);
        List<VPermisoGrupo> opciones = (List<VPermisoGrupo>) query
            .getResultList();
        return opciones;
    }

    // ------------------------------------------------ //
    /**
     * @author fredy.wilches
     * @see IVPermisoDAO#getRoles(String[] roles)
     */
    private String getRoles(String[] roles) {
        String rolesStr = "";
        for (String r : roles) {
            rolesStr += "'" + r + "',";
        }
        if (roles.length > 0) {
            rolesStr = rolesStr.substring(0, rolesStr.length() - 1);
        }
        return rolesStr;
    }
}
