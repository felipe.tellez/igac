package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;

/**
 * @author juan.agudelo
 */
@Local
public interface IFotoDAO extends IGenericJpaDAO<Foto, Long> {

    /**
     * Recupera la lista de fotos asociadas a un Predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Foto> buscarFotografiasPredio(Long predioId);

    /**
     * Permite recuperar las url (en la máquina donde se ejecuta la aplicación) de las fotos de un
     * Componente de Construcción esprecífico en un Predio, si no se envía ningun parámetro de
     * Componente de Construcción se retornaran la lista correspondiente a la fachada del predio
     *
     * @author juan.agudelo
     * @param predioId
     * @param componenteConstruccion
     * @return
     */
    public List<String> buscarFotosPredioPorComponenteDeConstruccion(
        Long predioId, String componenteConstruccion);

    /**
     * Método que permite guardar una lista de fotos
     *
     * @author juan.agudelo
     * @param fotos
     * @return
     */
    public List<Foto> guardarFotos(List<Foto> fotos);

    /**
     * Método que busca las fotos de un predio por el tipo de la foto
     *
     * @param predioId
     * @param fotoTipo
     * @author javier.aponte
     * @return List<Foto>
     */
    public List<Foto> buscarFotografiaPorIdPredioAndFotoTipo(Long predioId, String codigoTipoFoto);

    /**
     * Método para buscar fotos relacionadas a una unidad de construcción y a un tipo
     *
     * @author felipe.cadena
     * @param unidadId
     * @param codigoTipoFoto
     * @return
     */
    public List<Foto> buscarPorIdUnidadConstruccionYTipo(Long unidadId, String codigoTipoFoto);

}
