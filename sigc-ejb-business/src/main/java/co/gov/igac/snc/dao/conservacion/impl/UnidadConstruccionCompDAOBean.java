package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IUnidadConstruccionCompDAO;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class UnidadConstruccionCompDAOBean extends GenericDAOWithJPA<UnidadConstruccionComp, Long>
    implements IUnidadConstruccionCompDAO {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(UnidadConstruccionCompDAOBean.class);

    @Override
    public List<UnidadConstruccionComp> findUnidadConstruccionCompByUnidadConstruccion(
        UnidadConstruccion unidad) {
        LOGGER.debug("findUnidadConstruccionCompByUnidadConstruccion");

        List<UnidadConstruccionComp> answer = null;
        try {
            Query q = entityManager.createNamedQuery(
                "findUnidadConstruccionCompByUnidadConstruccion");
            q.setParameter("unidadConstruccion", unidad);
            answer = q.getResultList();
        } catch (Exception ex) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, ex,
                "UnidadConstruccionCompDAOBean#findUnidadConstruccionCompByUnidadConstruccion");
        }
        return answer;
    }

}
