package co.gov.igac.snc.dao.actualizacion;

import java.util.List;
import javax.ejb.Local;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 * Servicios de persistencia para el objeto ActualizacionSedeComision
 *
 * @author franz.gamba
 */
@Local
public interface IActualizacionSedeComisionDAO extends IGenericJpaDAO<ActualizacionContrato, Long> {

}
