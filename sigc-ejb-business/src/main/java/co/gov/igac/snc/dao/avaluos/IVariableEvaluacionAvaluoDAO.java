package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.VariableEvaluacionAvaluo;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity VariableEvaluacionAvaluo
 *
 * @author felipe.cadena
 */
@Local
public interface IVariableEvaluacionAvaluoDAO extends
    IGenericJpaDAO<VariableEvaluacionAvaluo, Long> {

}
