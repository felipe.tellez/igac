package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;

/**
 * Interfaz para el dao de VProfesionalAvaluosContrato que corresponde a la vista
 * v_profesional_avaluos_contrato
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IVProfesionalAvaluosContratoDAO extends
    IGenericJpaDAO<VProfesionalAvaluosContrato, Long> {

    public List<VProfesionalAvaluosContrato> buscarAvaluadoresPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtroDatos);

}
