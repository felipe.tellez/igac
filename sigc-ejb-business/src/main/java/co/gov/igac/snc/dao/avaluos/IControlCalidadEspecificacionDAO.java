/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadEspecificacion;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity ControlCalidadEspecificacion
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IControlCalidadEspecificacionDAO extends
    IGenericJpaDAO<ControlCalidadEspecificacion, Long> {

}
