package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IHPersonaPredioPropiedadDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.HPersonaPredioPropiedad;

@Stateless
public class HPersonaPredioPropiedadDAOBean extends
    GenericDAOWithJPA<HPersonaPredioPropiedad, Long> implements
    IHPersonaPredioPropiedadDAO {

    /**
     * @see IHPersonaPredioPropiedadDAO#findByPredioId(Long)
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<HPersonaPredioPropiedad> findByPredioId(Long predioId) {
        try {
            String sql = "SELECT hppp FROM HPersonaPredioPropiedad hppp" +
                " LEFT JOIN FETCH hppp.departamento" +
                " LEFT JOIN FETCH hppp.municipio" +
                " LEFT JOIN FETCH hppp.documentoSoporte" +
                " WHERE hppp.HPredio.predioId.id = :predioId";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("predioId", predioId);
            return query.getResultList();
        } catch (Exception e) {
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Falla en la búsqueda de históricos de justificación", e);
        }
    }
}
