package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Stateless
public class DominioDAOBean extends GenericDAOWithJPA<Dominio, Long> implements IDominioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DominioDAOBean.class);

    @SuppressWarnings("unchecked")
    public List<Dominio> findByNombre(Enum d) {
        //log.debug("findByNombre");
        String queryToExecute = "SELECT d FROM Dominio d" +
            " WHERE d.nombre = :nombre " +
            " AND d.activo = :activo" +
            " ORDER BY d.codigo";
        Query query = this.entityManager.createQuery(queryToExecute);
        query.setParameter("nombre", d.toString());
        query.setParameter("activo", ESiNo.SI.getCodigo());
        return query.getResultList();
    }

    public Dominio findByCodigo(EDominio nombre, String codigo) {
        try {
            Dominio dominio = null;
            Query query = this.entityManager.createQuery(
                "Select d from Dominio d where d.nombre = :nombre and d.codigo = :codigo");
            query.setParameter("nombre", nombre.toString());
            query.setParameter("codigo", codigo);
            dominio = (Dominio) query.getSingleResult();
            return dominio;
        } catch (Exception e) {
            LOGGER.debug("DominioDAOBean#findByCodigo -> Error: " + e.getMessage());
            return null;
        }
    }

    /**
     * Método que retorna una lista de {@link Dominio} buscandolos por su nombre. Éste método es una
     * copia del método findByNombre, y retornando la lista ordenada por éste mismo parametro.
     *
     * @author david.cifuentes
     * @param d
     * @return
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Dominio> findByNombreOrderByNombre(Enum d) {
        String queryToExecute = "SELECT d FROM Dominio d" +
            " WHERE d.nombre = :nombre " +
            " AND d.activo = :activo" +
            " ORDER BY d.nombre";
        Query query = this.entityManager.createQuery(queryToExecute);
        query.setParameter("nombre", d.toString());
        query.setParameter("activo", ESiNo.SI.getCodigo());
        return query.getResultList();
    }

    @Override
    public Map<String, List<Dominio>> obtenerSolicitudesYTramites() {
        Map<String, List<Dominio>> resultado = new HashMap<String, List<Dominio>>();

        List<Dominio> solicitudes = this.findByNombre(EDominio.SOLICITUD_TIPO);
        List<Dominio> tramites = this.findByNombre(EDominio.TRAMITE_TIPO_TRAMITE);
        List<Dominio> mutaciones = this.findByNombre(EDominio.MUTACION_CLASE);
        List<Dominio> subQuinta = this.findByNombre(EDominio.MUTACION_5_SUBTIPO);
        List<Dominio> subSegunda = this.findByNombre(EDominio.MUTACION_2_SUBTIPO);
        List<TipoSolicitudTramite> relacion;

        String queryToExecute = " SELECT * FROM TIPO_SOLICITUD_TRAMITE order by TIPO_SOLICITUD";
        Query query = this.entityManager.createNativeQuery(queryToExecute,
            TipoSolicitudTramite.class);
        relacion = query.getResultList();

        for (TipoSolicitudTramite tst : relacion) {
            String key = EDominio.SOLICITUD_TIPO.toString() + tst.getTipoSolicitud();
            for (Dominio t : tramites) {
                if (tst.getTipoTramite().equals(t.getCodigo())) {
                    if (!resultado.containsKey(key)) {
                        List<Dominio> ts = new ArrayList<Dominio>();
                        ts.add(t);
                        resultado.put(key, ts);
                    } else {
                        resultado.get(key).add(t);
                    }
                    break;
                }
            }
        }

        //se inicializan los tipos de mutaciones
        for (Dominio td : tramites) {

            if (td.getCodigo().equals(ETramiteTipoTramite.MUTACION.getCodigo())) {
                String key = EDominio.MUTACION_CLASE.toString() + td.getCodigo();
                resultado.put(key, mutaciones);
            }
        }

        //se inicializan los sub-tipos de mutaciones
        resultado.put(EDominio.MUTACION_SUBTIPO.toString() + EMutacionClase.SEGUNDA.getCodigo(),
            subSegunda);
        resultado.put(EDominio.MUTACION_SUBTIPO.toString() + EMutacionClase.QUINTA.getCodigo(),
            subQuinta);

        //se agrega la lista de solicitudes
        resultado.put(EDominio.SOLICITUD_TIPO.toString(), solicitudes);

        return resultado;
    }

    @SuppressWarnings("unchecked")
    public List<Dominio> buscarNombreOrdenadoPorId(Enum d) {

        String queryToExecute = "SELECT d FROM Dominio d" +
            " WHERE d.nombre = :nombre " +
            " AND d.activo = :activo" +
            " ORDER BY d.id";
        Query query = this.entityManager.createQuery(queryToExecute);
        query.setParameter("nombre", d.toString());
        query.setParameter("activo", ESiNo.SI.getCodigo());
        return query.getResultList();
    }

	/**
	 * Método que retorna una lista de {@link Dominio} buscandolos por su nombre.
	 * Éste método es una copia del método findByNombre, y retornando la lista
	 * ordenada por el valor del dominio.
	 *
	 * @author felipe.cadena
	 * @param d
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Dominio> findByNombreOrderByValor(Enum d) {
		String queryToExecute = "SELECT d FROM Dominio d"
				+ " WHERE d.nombre = :nombre "
				+ " AND d.activo = :activo"
				+ " ORDER BY d.valor ASC";
		Query query =  this.entityManager.createQuery(queryToExecute);
		query.setParameter("nombre", d.toString());
		query.setParameter("activo", ESiNo.SI.getCodigo());
		return query.getResultList();
	}

}
