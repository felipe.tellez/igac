/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;

/**
 * interfaz para los métodos de bd de la tabla COMISION_OFERTA
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Local
public interface IComisionOfertaDAO extends IGenericJpaDAO<ComisionOferta, Long> {

    /**
     * Retorna la lista de comisiones de ofertas que tengan el estado indicado y que esten
     * relacionadas a las regiones dadas
     *
     * @author pedro.garcia
     * @param estadoComision estado de la comisión debería corresponder a un estado de la
     * enumeración EOfertaComisionEstado
     * @param List<Long> regionesCapturaOfertaIds
     * @return lista de comisiones de ofertas que cumplan los criterios
     */
    public List<ComisionOferta> getComisionesOfertaPorEstadoAndRCO(String estadoComision,
        List<Long> regionesCapturaOfertaIds);

//end of interface
}
