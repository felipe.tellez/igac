package co.gov.igac.snc.dao.conservacion.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IListadoPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredio;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredioPK;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class ListadoPredioDAOBean extends GenericDAOWithJPA<ListadoPredio, ListadoPredioPK>
    implements IListadoPredioDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ListadoPredioDAOBean.class);

    /**
     * @see IListadoPredioDAO#cargarListadoPrediosPorMunicipioVigencia(String, Long)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public String[] cargarListadoPrediosPorMunicipioVigencia(
        String municipioCodigo, Long vigencia) {

        LOGGER.debug("Inicio ListadoPredioDAOBean#cargarListadoPrediosPorMunicipioVigencia");

        List<String> queryResult = null;
        String[] answer = null;

        String queryString = "select lp.numero_predial from listado_predio lp " +
            "where lp.municipio_codigo = :municipioCodigo and lp.vigencia= :vigencia";

        Query q = this.entityManager.createNativeQuery(queryString);
        q.setParameter("municipioCodigo", municipioCodigo);
        q.setParameter("vigencia", vigencia);
        queryResult = q.getResultList();
        if (queryResult != null) {
            answer = new String[queryResult.size()];
            queryResult.toArray(answer);
        }
        LOGGER.debug("Fin ListadoPredioDAOBean#cargarListadoPrediosPorMunicipioVigencia");
        return answer;
    }

    /**
     * @see ListadoPredioDAOBean#contarListadoPredios(String[])
     * @author david.cifuentes
     */
    @Override
    public int contarListadoPredios(String[] numerosPredialesListadoPredios) {

        LOGGER.debug("Inicio ListadoPredioDAOBean#contarListadoPredios");
        BigInteger answer;
        try {
            Query query = this.entityManager
                .createQuery(
                    "SELECT count(*) FROM ListadoPredio lp WHERE lp.numeroPredial IN (:numerosPredialesListadoPredios)");
            query.setParameter("numerosPredialesListadoPredios",
                Arrays.asList(numerosPredialesListadoPredios));
            answer = new BigInteger(query.getSingleResult().toString());

        } catch (Exception e) {
            LOGGER.error("Error on ListadoPredioDAOBean#contarListadoPredios: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "ListadoPredioDAOBean#contarListadoPredios");
        }
        LOGGER.debug("Fin ListadoPredioDAOBean#contarListadoPredios");
        return answer.intValue();
    }

    /**
     * @see ListadoPredioDAOBean#buscarListadoPrediosCierreAnual(String[], String, String,
     * Map<String, String>, int[])
     * @author david.cifuentes
     */
    @Override
    public List<ListadoPredio> buscarListadoPrediosCierreAnual(
        String[] numerosPredialesListadoPredios, String sortField, String sortOrder,
        Map<String, String> filters, int[] contadores) {

        LOGGER.debug("Inicio ListadoPredioDAOBean#buscarListadoPrediosCierreAnual");

        try {
            List<ListadoPredio> resultado = null;
            Query query = this.entityManager
                .createQuery(
                    "SELECT distinct lp FROM ListadoPredio lp WHERE lp.numeroPredial IN (:numerosPredialesListadoPredios)");

            query.setParameter("numerosPredialesListadoPredios",
                Arrays.asList(numerosPredialesListadoPredios));

            resultado = (List<ListadoPredio>) super.findInRangeUsingQuery(
                query, contadores);
            LOGGER.debug("Fin ListadoPredioDAOBean#buscarListadoPrediosCierreAnual");
            return resultado;
        } catch (Exception e) {
            LOGGER.error("ListadoPredioDAOBean#buscarListadoPrediosCierreAnual: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e,
                "ListadoPredioDAOBean#buscarListadoPrediosCierreAnual");
        }
    }

    /**
     * @see ListadoPredioDAOBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia(String, Long,
     * String, String, Map<String, String>, int[])
     * @author david.cifuentes
     */
    @Override
    public List<ListadoPredio> buscarListadoPrediosCierreAnualPorMunicipioVigencia(
        String municipioCodigo, Long vigencia, String sortField,
        String sortOrder, Map<String, String> filters, int first, int pageSize) {
        LOGGER.debug("ListadoPredioDAOBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia");

        try {
            List<ListadoPredio> resultado = null;
            StringBuilder queryString = new StringBuilder();
            queryString.append(
                "SELECT distinct lp FROM ListadoPredio lp WHERE lp.id.vigencia = :vigencia and lp.id.municipioCodigo = :municipioCodigo");

            if (filters != null) {
                for (String campo : filters.keySet()) {
                    if (campo.equals("numeroPredial")) {
                        queryString.append(" AND lp.numeroPredial like :numeroPredial");
                    }
                    if (campo.equals("tipo")) {
                        queryString.append(" AND lp.tipo = :tipo");
                    }
                    if (campo.equals("destino")) {
                        queryString.append(" AND lp.destino = :destino");
                    }
                    if (campo.equals("condicionPropiedad")) {
                        queryString.append(" AND lp.condicionPropiedad = :condicionPropiedad");
                    }
                    if (campo.equals("tipoAvaluo")) {
                        queryString.append(" AND lp.tipoAvaluo = :tipoAvaluo");
                    }
                    if (campo.equals("areaConstruccionPrecierre")) {
                        queryString.append(
                            " AND lp.areaConstruccionPrecierre like :areaConstruccionPrecierre");
                    }
                    if (campo.equals("areaTerrenoPrecierre")) {
                        queryString.
                            append(" AND lp.areaTerrenoPrecierre like :areaTerrenoPrecierre");
                    }
                    if (campo.equals("valorAvaluoPrecierre")) {
                        queryString.
                            append(" AND lp.valorAvaluoPrecierre like :valorAvaluoPrecierre");
                    }
                    if (campo.equals("areaConstruccionCierre")) {
                        queryString.append(
                            " AND lp.areaConstruccionCierre like :areaConstruccionCierre");
                    }
                    if (campo.equals("areaTerrenoCierre")) {
                        queryString.append(" AND lp.areaTerrenoCierre like :areaTerrenoCierre");
                    }
                    if (campo.equals("valorAvaluoCierre")) {
                        queryString.append(" AND lp.valorAvaluoCierre like :valorAvaluoCierre");
                    }
                    LOGGER.debug("Campo filtro " + campo);
                    LOGGER.debug("Tamaño " + filters.get(campo).length());
                }

                if (filters.containsKey("tipoAvaluoFiltro")) {
                    String filter = filters.get("tipoAvaluoFiltro");
                    String operador = "=";
                    if (filter.equals("1")) {
                        // Mayor que
                        operador = ">";
                    } else if (filter.equals("-1")) {
                        // Menor que
                        operador = "<";
                    } else if (filter.equals("0")) {
                        // Exáctamente igual que
                        operador = "=";
                    }
                    queryString.append(" AND (lp.valorAvaluoCierre - lp.valorAvaluoPrecierre) " +
                        operador + " (lp.valorAvaluoPrecierre * :porcentajeAvaluoFiltro / 100)");
                }
            }

            if (sortField != null && sortOrder != null && !sortOrder.equals("UNSORTED")) {
                queryString.append(" ORDER BY lp.").append(sortField).append(
                    sortOrder.equals("DESCENDING") ? " DESC" : " ASC");
            }

            Query query = this.entityManager
                .createQuery(queryString.toString());

            if (filters != null) {
                for (String campo : filters.keySet()) {
                    if (campo.equals("numeroPredial") ) {
                        query.setParameter(campo, filters.get(campo).toUpperCase() + "%");
                    } else if (campo.equals("porcentajeAvaluoFiltro") ||
                            campo.equals("areaConstruccionPrecierre") ||
                            campo.equals("areaTerrenoPrecierre") ||
                            campo.equals("valorAvaluoPrecierre") ||
                            campo.equals("areaConstruccionCierre") ||
                            campo.equals("areaTerrenoCierre") ||
                            campo.equals("valorAvaluoCierre")) {
                        Long paramLong = Long.parseLong(filters.get(campo));
                        query.setParameter(campo, paramLong);
                    } else if (campo.equals("tipoAvaluoFiltro")) {
                        // No hacer nada, éste campo se usa arriba en la consulta para modificar los operadores
                    } else {
                        query.setParameter(campo, filters.get(campo).toUpperCase());
                    }
                }
            }

            query.setParameter("vigencia", vigencia);
            query.setParameter("municipioCodigo", municipioCodigo);
//			query.setFirstResult(1);
//			query.setMaxResults(100);

            int[] contadores = new int[2];
            contadores[0] = first;
            contadores[1] = pageSize;
            resultado = (List<ListadoPredio>) super.findInRangeUsingQuery(
                query, contadores);

            LOGGER.debug(
                "Fin ListadoPredioDAOBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia");
            return resultado;
        } catch (Exception e) {
            LOGGER.error(
                "ListadoPredioDAOBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e,
                "ListadoPredioDAOBean#buscarListadoPrediosCierreAnualPorMunicipioVigencia");
        }
    }

    /**
     * @see ListadoPredioDAOBean#contarListadoPredios(String, Long)
     * @author david.cifuentes
     */
    @Override
    public int contarListadoPredios(String municipioCodigo, Long vigencia,
        Map<String, String> filters) {
        BigInteger answer;
        StringBuilder queryString = new StringBuilder();

        try {
            LOGGER.debug("Inicio ListadoPredioDAOBean#contarListadoPredios");
            queryString.append(
                "SELECT count(*) FROM ListadoPredio lp WHERE lp.id.municipioCodigo = :municipioCodigo and lp.id.vigencia = :vigencia");

            if (filters != null) {
                for (String campo : filters.keySet()) {
                    if (campo.equals("numeroPredial")) {
                        queryString.append(" AND lp.numeroPredial like :numeroPredial");
                    }
                    if (campo.equals("tipo")) {
                        queryString.append(" AND lp.tipo = :tipo");
                    }
                    if (campo.equals("destino")) {
                        queryString.append(" AND lp.destino = :destino");
                    }
                    if (campo.equals("condicionPropiedad")) {
                        queryString.append(" AND lp.condicionPropiedad = :condicionPropiedad");
                    }
                    if (campo.equals("tipoAvaluo")) {
                        queryString.append(" AND lp.tipoAvaluo = :tipoAvaluo");
                    }
                    if (campo.equals("areaConstruccionPrecierre")) {
                        queryString.append(
                            " AND lp.areaConstruccionPrecierre like :areaConstruccionPrecierre");
                    }
                    if (campo.equals("areaTerrenoPrecierre")) {
                        queryString.
                            append(" AND lp.areaTerrenoPrecierre like :areaTerrenoPrecierre");
                    }
                    if (campo.equals("valorAvaluoPrecierre")) {
                        queryString.
                            append(" AND lp.valorAvaluoPrecierre like :valorAvaluoPrecierre");
                    }
                    if (campo.equals("areaConstruccionCierre")) {
                        queryString.append(
                            " AND lp.areaConstruccionCierre like :areaConstruccionCierre");
                    }
                    if (campo.equals("areaTerrenoCierre")) {
                        queryString.append(" AND lp.areaTerrenoCierre like :areaTerrenoCierre");
                    }
                    if (campo.equals("valorAvaluoCierre")) {
                        queryString.append(" AND lp.valorAvaluoCierre like :valorAvaluoCierre");
                    }
                    LOGGER.debug("Campo filtro " + campo);
                    LOGGER.debug("Tamaño " + filters.get(campo).length());
                }

                if (filters.containsKey("tipoAvaluoFiltro")) {
                    String filter = filters.get("tipoAvaluoFiltro");
                    String operador = "=";
                    if (filter.equals("1")) {
                        // Mayor que
                        operador = ">";
                    } else if (filter.equals("-1")) {
                        // Menor que
                        operador = "<";
                    } else if (filter.equals("0")) {
                        // Exáctamente igual que
                        operador = "=";
                    }
                    queryString.append(" AND (lp.valorAvaluoCierre - lp.valorAvaluoPrecierre) " +
                        operador + " (lp.valorAvaluoPrecierre * :porcentajeAvaluoFiltro / 100)");
                }
            }

            Query query = this.entityManager
                .createQuery(queryString.toString());

            if (filters != null) {
                for (String campo : filters.keySet()) {
                    if (campo.equals("numeroPredial") ) {
                        query.setParameter(campo, filters.get(campo).toUpperCase() + "%");
                    } else if (campo.equals("porcentajeAvaluoFiltro") ||
                            campo.equals("areaConstruccionPrecierre") ||
                            campo.equals("areaTerrenoPrecierre") ||
                            campo.equals("valorAvaluoPrecierre") ||
                            campo.equals("areaConstruccionCierre") ||
                            campo.equals("areaTerrenoCierre") ||
                            campo.equals("valorAvaluoCierre")) {
                        Long tipoAvaluoFiltro = Long.parseLong(filters.get(campo));
                        query.setParameter(campo, tipoAvaluoFiltro);
                    } else if (campo.equals("tipoAvaluoFiltro")) {
                        // No hacer nada, éste campo se usa arriba en la consulta para modificar los operadores
                    } else {
                        query.setParameter(campo, filters.get(campo).toUpperCase());
                    }
                }
            }

            query.setParameter("municipioCodigo", municipioCodigo);
            query.setParameter("vigencia", vigencia);

            answer = new BigInteger(query.getSingleResult().toString());

        } catch (Exception e) {
            LOGGER.error("Error on ListadoPredioDAOBean#contarListadoPredios: " +
                e.getMessage());
            throw SncBusinessServiceExceptions.EXCEPCION_0005.getExcepcion(
                LOGGER, e, "ListadoPredioDAOBean#contarListadoPredios");
        }
        LOGGER.debug("Fin ListadoPredioDAOBean#contarListadoPredios");
        return answer.intValue();
    }

}
