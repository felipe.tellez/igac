package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPredioDireccionDAO;
import co.gov.igac.snc.dao.generales.impl.DominioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;

@Stateless
public class PredioDireccionDAOBean extends GenericDAOWithJPA<PredioDireccion, Long> implements
    IPredioDireccionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioDireccionDAOBean.class);

    /**
     * @see IPredioDireccionDAO#getPredioDireccionByPredio(Predio)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PredioDireccion> getPredioDireccionByPredio(Predio predio) {
        LOGGER.debug("getPredioDireccionByPredio");
        Query q = entityManager.createNamedQuery("findPredioDireccionByPredio");
        q.setParameter("predio", predio);
        return q.getResultList();
    }

}
