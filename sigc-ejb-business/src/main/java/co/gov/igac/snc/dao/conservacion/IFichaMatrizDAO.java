package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;

/**
 * @author javier.aponte
 */
@Local
public interface IFichaMatrizDAO extends IGenericJpaDAO<FichaMatriz, Long> {

    /**
     * Recupera la ficha matriz asociada a un predio por el número predial
     *
     * @author javier.aponte
     * @param numeroPredial
     * @return
     */
    public FichaMatriz getFichaMatrizByNumeroPredialPredio(String numeroPredial);

    /**
     * Recupera la ficha matriz de origen de un predio
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public FichaMatriz obtenerPorNumeroPredial(String numPredial);

    /**
     * Método que recupera la ficha matriz por id
     *
     * @author javier.aponte
     * @version 2.0
     * @param fichaMatrizId
     * @return
     */
    public FichaMatriz obtenerFichaMatrizPorId(Long fichaMatrizId);

    /**
     * Método que recupera la ficha matriz por el id del predio
     *
     * @author javier.aponte
     * @version 2.0
     * @param predioId
     * @return
     */
    public FichaMatriz obtenerFichaMatrizPorPredioId(Long predioId);

    /**
     * Método que consulta una proyección de ficha matriz según el id del Predio al cual está
     * asociada
     *
     * @param id
     * @param id
     * @return
     * @author javier.aponte
     */
    /*
     * @modified felipe.cadena :: 29-06-2017 :: Se agrega parametro para evitar cargar datos
     * innecesarios.
     */
    public FichaMatriz findByPredioId(Long idPPredio, boolean asociaciones);
}
