package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;

/**
 * Servicios de persistencia para el objeto RecursoHumano
 *
 * @author franz.gamba
 */
@Local
public interface IRecursoHumanoDAO extends IGenericJpaDAO<RecursoHumano, Long> {

    /**
     * Método que trae el recurso humano asignado a un responsable de actualizacion
     *
     * @param responsableActId
     * @return
     */
    public List<RecursoHumano> obtenerRecursoPorResponsableActualizacion(Long responsableActId);

    /**
     * Método que trae el recurso humano por id de la actualización
     *
     * @param actualizacionId
     * @return
     */
    public List<RecursoHumano> obtenerRecursoPorActualizacionId(Long actualizacionId);

    /**
     * Método que devuelve una lista de recurso humano por actividad
     *
     * @param actividad
     * @author javier.aponte
     */
    public List<RecursoHumano> obtenerRecursoHumanoPorActividad(String actividad);

    /**
     * Método que devuelve una lista de topgrafos segun actualizacion Id
     *
     * @param actualizacionId
     * @author javier.barajas
     */
    public List<RecursoHumano> obtenerTopografosporActualizacionId(Long actualizacionId);

    /**
     * Método que devuelve una lista de recurso humano por actualizacion id
     *
     * @param actualizacionId
     * @param actividad
     * @author javier.barajas
     *
     */
    public List<RecursoHumano> obtenerRecuroHumanoporActualizacionId(Long actualizacionId,
        String actividad);

    /**
     * Buscar el recurso humano por el numéro de identificacion y el identificador del tramite de
     * actualización
     *
     * @param identificacionRecursoHumano identificacion del recurso humano
     * @param actualizacioId identificar del tramite de actualizacion
     * @return
     * @author andres.eslava
     */
    public RecursoHumano obtenerRecursoHumanoByIdentificacionYActualizacionId(
        String identificacionRecursoHumano, Long actualizacioId);

}
