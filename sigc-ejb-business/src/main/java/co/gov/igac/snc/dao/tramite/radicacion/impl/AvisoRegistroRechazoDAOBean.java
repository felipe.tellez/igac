/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.radicacion.IAvisoRegistroRechazoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class AvisoRegistroRechazoDAOBean extends GenericDAOWithJPA<AvisoRegistroRechazo, Long>
    implements IAvisoRegistroRechazoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvisoRegistroRechazoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @see IAvisoRegistroRechazoDAO#findByNumeroSolicitud(java.lang.String)
     */
    public List<AvisoRegistroRechazo> findByNumeroSolicitud(String numeroSolicitud) {

        String queryString;
        Query query;
        List<AvisoRegistroRechazo> results = null;

        queryString = "select arr from AvisoRegistroRechazo arr " +
            " join fetch arr.avisoRegistroRechazoPredios arrp " +
            " where arr.solicitudAvisoRegistro.solicitud.numero = :numSol";

        query = this.entityManager.createQuery(queryString);
        query.setParameter("numSol", numeroSolicitud);
        try {
            results = query.getResultList();
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

        return results;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AvisoRegistroRechazo> findBySolicitudAvisoRegistroId(
        Long solicitudAvisoRegistroId) {
        try {
            String sql = "SELECT arr FROM AvisoRegistroRechazo arr " +
                " LEFT JOIN FETCH arr.avisoRegistroRechazoPredios arrp " +
                " LEFT JOIN FETCH arrp.predio p " +
                " LEFT JOIN FETCH p.departamento " +
                " LEFT JOIN FETCH p.municipio " +
                " WHERE arr.solicitudAvisoRegistro.id = :solicitudAvisoRegistroId";
            Query query = entityManager.createQuery(sql);
            query.setParameter("solicitudAvisoRegistroId",
                solicitudAvisoRegistroId);
            return (List<AvisoRegistroRechazo>) query.getResultList();
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage());
            return null;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al buscar AvisoRegistroRechazo por id de SolicitudAvisoRegistro");
        }
    }

}
