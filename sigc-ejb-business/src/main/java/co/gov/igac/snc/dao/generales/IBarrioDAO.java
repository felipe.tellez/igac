package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.snc.dao.IGenericJpaDAO;

/**
 *
 */
@Local
public interface IBarrioDAO extends IGenericJpaDAO<Barrio, String> {

    public List<Barrio> findByComuna(String comunaId);

    public Barrio getBarrioByCodigo(String codigo);

    /**
     * Herramientas para visor processo Actualizacion
     *
     * @param Codigo del Municipio
     * @return numero de predios
     */
    public List<Barrio> numeroBarriosMunicipio(String codigoMunicipio);

}
