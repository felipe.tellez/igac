package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IRepTerritorialReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;

@Stateless
public class RepTerritorialReporteDAOBean extends GenericDAOWithJPA<RepTerritorialReporte, Long> implements 
	IRepTerritorialReporteDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepTerritorialReporteDAOBean.class);

	
	/**
	 * @see RepTerritorialReporteDAOBean#obtenerRepTerritorialReporte(String)
	 * @author leidy.gonzalez
	 */
	@SuppressWarnings("unchecked")
	@Override
        public List<Departamento> obtenerRepTerritorialReporte(String codigoTerritorial, String codigoUoc){
		LOGGER.debug("Entra en RepReporteDAOBean#obtenerRepTerritorialReporte");
		
		List<Departamento> answer = null;
        
        String whereCode = "";
        
        if(codigoUoc!=null
                && !codigoUoc.isEmpty()){
            whereCode = "rtr.UOC.codigo = :codigoUoc";
        }else  if(codigoTerritorial!=null
                && !codigoTerritorial.isEmpty()){
            whereCode = "rtr.territorial.codigo = :codigoTerritorial";
        }
		
		Query q = entityManager.createQuery("SELECT DISTINCT rtr.departamento" +
				" FROM RepTerritorialReporte rtr " +
				"WHERE "+ whereCode);
		
        if(codigoUoc!=null
                && !codigoUoc.isEmpty()){
            q.setParameter("codigoUoc", codigoUoc);
        }else{
            q.setParameter("codigoTerritorial", codigoTerritorial);
        }
		
		
		try {
			answer = (List<Departamento>) q.getResultList();
		} catch (NoResultException e) {
			LOGGER.error("RepReporteDAOBean#obtenerRepTerritorialReporte:" +
					"Error al consultar la lista de territoriales de reportes");
		}
		return answer;
		
	}
	
}
