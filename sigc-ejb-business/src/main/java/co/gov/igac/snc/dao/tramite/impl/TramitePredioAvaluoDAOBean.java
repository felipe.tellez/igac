/**
 *
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramitePredioAvaluoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @author jamir.avila
 *
 */
@Stateless
public class TramitePredioAvaluoDAOBean extends GenericDAOWithJPA<TramitePredioAvaluo, Long>
    implements ITramitePredioAvaluoDAO {

    @Resource
    private SessionContext contexto;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramitePredioAvaluoDAOBean.class);

    /* (non-Javadoc) @see
     * co.gov.igac.snc.dao.tramite.ITramitePredioAvaluoDAO#adicionarPredioAAvaluo(co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo)
     */
    @Override
    public TramitePredioAvaluo adicionarPredioAAvaluo(TramitePredioAvaluo tramitePredioAvaluo) {
        LOGGER.info("TramitePredioAvaluoDAOBean#adicionarPredioAAvaluo(...)");

        Tramite tramite = tramitePredioAvaluo.getTramite();
        if (tramite == null) {
            final String MENSAJE = "El trámite no se pudo recuperar.";
            LOGGER.error(MENSAJE);
            //TODO: Jamir, 20/05/2011, lanzar excepción.
        }

        List<TramitePredioAvaluo> predios = tramite.getTramitePredioAvaluos();
        predios.add(tramitePredioAvaluo);

        String userName = "[JAMIR]";
        try {
            userName = contexto.getCallerPrincipal().getName();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        tramitePredioAvaluo.setUsuarioLog(userName);
        tramitePredioAvaluo.setFechaLog(new java.util.Date());

        entityManager.persist(tramitePredioAvaluo);
        //entityManager.merge(predios);

        return tramitePredioAvaluo;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.dao.tramite.ITramitePredioAvaluoDAO#removerPredioDeAvaluo(java.lang.Long)
     */
    @Override
    public void removerPredioDeAvaluo(Long idTramitePredioAvaluo) {
        LOGGER.info("TramitePredioAvaluoDAOBean#removerPredioDeAvaluo(...)");

        TramitePredioAvaluo tramitePredioAvaluo = (TramitePredioAvaluo) entityManager.find(
            TramitePredioAvaluo.class, idTramitePredioAvaluo);
        if (tramitePredioAvaluo == null) {
            final String MENSAJE = "La relación entre predio y avalúo de id: " +
                idTramitePredioAvaluo + " no se pudo recuperar.";
            LOGGER.error("TramitePredioAvaluoDAOBean#removerPredioDeAvaluo(...)" + MENSAJE);
            throw new java.lang.IllegalStateException(MENSAJE);
        }

        Tramite tramite = tramitePredioAvaluo.getTramite();
        tramite.getTramitePredioAvaluos().remove(tramitePredioAvaluo);

        entityManager.remove(tramitePredioAvaluo);
        entityManager.merge(tramite);
    }

    @Override
    public void actualizarPredioDeAvaluo(TramitePredioAvaluo predioAvaluo) {
        LOGGER.info("TramitePredioAvaluoDAOBean#removerPredioDeAvaluo(...)");

        TramitePredioAvaluo tramitePredioAvaluo = (TramitePredioAvaluo) entityManager.find(
            TramitePredioAvaluo.class, predioAvaluo.getId());
        if (tramitePredioAvaluo == null) {
            final String MENSAJE = "La relación entre predio y avalúo de id: " + predioAvaluo.
                getId() + " no se pudo recuperar.";
            LOGGER.error("TramitePredioAvaluoDAOBean#removerPredioDeAvaluo(...)" + MENSAJE);
            throw new java.lang.IllegalStateException(MENSAJE);
        }

        entityManager.merge(tramitePredioAvaluo);
    }

    @SuppressWarnings("unchecked")
    public List<TramitePredioAvaluo> buscarPrediosPorTramiteId(Long tramiteId) {

        LOGGER.debug("En TtramitePredioAvaluoDAOBean#buscarPrediosPorTramiteId");
        List<TramitePredioAvaluo> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT tpa " +
            " FROM TramitePredioAvaluo tpa" +
            " WHERE tpa.tramite.id = :tramiteId";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("tramiteId", tramiteId);

            answer = (List<TramitePredioAvaluo>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }
}
