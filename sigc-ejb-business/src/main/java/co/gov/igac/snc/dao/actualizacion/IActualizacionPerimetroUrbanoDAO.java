package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionPerimetroUrbano;

/**
 * Operaciones CRUD sobre la entidad Actualizacion de Perimetro Urbano
 *
 * @author andres.eslava
 *
 */
@Local
public interface IActualizacionPerimetroUrbanoDAO extends
    IGenericJpaDAO<ActualizacionPerimetroUrbano, Long> {

}
