package co.gov.igac.snc.dao.conservacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IFichaMatrizDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.FmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.ModeloConstruccionFoto;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import javax.persistence.NoResultException;
import org.hibernate.Hibernate;

@Stateless
public class FichaMatrizDAOBean extends GenericDAOWithJPA<FichaMatriz, Long>
    implements IFichaMatrizDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FichaMatrizDAOBean.class);

    /**
     * Recupera la ficha matriz asociada a un predio
     *
     * @author javier.aponte
     * @param numeroPredial
     * @modified juan.agudelo
     * @return
     */
    @SuppressWarnings("unchecked")
    public FichaMatriz getFichaMatrizByNumeroPredialPredio(String numeroPredial) {

        FichaMatriz fichaMatriz = new FichaMatriz();
        List<FichaMatriz> resultQuery = new ArrayList<FichaMatriz>();

        String query = "SELECT fm" +
            " FROM FichaMatriz fm" +
            " JOIN FETCH fm.predio p" +
            " LEFT JOIN FETCH p.departamento" +
            " LEFT JOIN FETCH p.municipio" +
            " WHERE p.numeroPredial = :numeroPredial";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroPredial", numeroPredial);

        try {

            resultQuery = q.getResultList();
            if (resultQuery != null && !resultQuery.isEmpty()) {
                fichaMatriz = (FichaMatriz) resultQuery.get(0);
            } else {
                return null;
            }

            if (fichaMatriz.getPredio().getPredioZonas() != null &&
                !fichaMatriz.getPredio().getPredioZonas().isEmpty()) {
                fichaMatriz.getPredio().getPredioZonas().size();
            }

            if (fichaMatriz.getFichaMatrizModelos() != null &&
                !fichaMatriz.getFichaMatrizModelos().isEmpty()) {
                fichaMatriz.getFichaMatrizModelos().size();

                for (FichaMatrizModelo fmm : fichaMatriz
                    .getFichaMatrizModelos()) {

                    if (fmm.getFmModeloConstruccions() != null &&
                        !fmm.getFmModeloConstruccions().isEmpty()) {

                        for (FmModeloConstruccion fmc : fmm
                            .getFmModeloConstruccions()) {

                            if (fmc.getUsoConstruccion() != null) {
                                fmc.getUsoConstruccion().getNombre();
                            }
                        }
                    }
                }
            }

            if (fichaMatriz.getPredio().getPredioDireccions() != null &&
                !fichaMatriz.getPredio().getPredioDireccions().isEmpty()) {
                fichaMatriz.getPredio().getPredioDireccions().size();
            }

            if (fichaMatriz.getFichaMatrizPredios() != null &&
                !fichaMatriz.getFichaMatrizPredios().isEmpty()) {
                fichaMatriz.getFichaMatrizPredios().size();
            }

            if (fichaMatriz.getFichaMatrizTorres() != null &&
                !fichaMatriz.getFichaMatrizTorres().isEmpty()) {

                fichaMatriz.getFichaMatrizTorres().size();
            }

            if (fichaMatriz.getPredio().getUnidadConstruccions() != null &&
                !fichaMatriz.getPredio().getUnidadConstruccions()
                    .isEmpty()) {

                fichaMatriz.getPredio().getUnidadConstruccions().size();

                for (UnidadConstruccion uc : fichaMatriz.getPredio()
                    .getUnidadConstruccions()) {

                    if (uc.getUsoConstruccion() != null) {
                        uc.getUsoConstruccion().getNombre();
                    }

                    if (uc.getUnidadConstruccionComps() != null &&
                        !uc.getUnidadConstruccionComps().isEmpty()) {

                        uc.getUnidadConstruccionComps().size();

                        for (UnidadConstruccionComp ucc : uc
                            .getUnidadConstruccionComps()) {

                            ucc.getComponente();

                            if (ucc.getUnidadConstruccion() != null) {
                                ucc.getUnidadConstruccion().getId();
                            }
                        }
                    }
                }

            }

            return fichaMatriz;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FichaMatrizDAOBean#getFichaMatrizByNumeroPredialPredio");
        }
    }

    /**
     *
     * @see IFichaMatrizDAO#obtenerPorNumeroPredial(java.lang.String)
     * @author felipe.cadena
     * @param numPredial
     * @return
     */
    @Override
    public FichaMatriz obtenerPorNumeroPredial(String numPredial) {

        List<FichaMatriz> answer = null;
        FichaMatriz fm = null;

        /*
         * //obtener FichaMatrizPredio FichaMatrizPredio fmp; String q1 = "SELECT fmp FROM
         * FichaMatrizPredio fmp" + " JOIN FETCH fmp.fichaMatriz fm " + " WHERE fmp.numeroPredial
         * LIKE :numeroPredial";
         *
         * try{
         *
         * Query q = entityManager.createQuery(q1);
         *
         * q.setParameter("numeroPredial", numPredial+"%"); * fmp = (FichaMatrizPredio)
         * q.getSingleResult(); * if(fmp==null){ return null; }
         *
         * resultado = fmp.getFichaMatriz();
         */
        try {

            String query = "SELECT DISTINCT fm FROM FichaMatriz fm" +
                " JOIN FETCH fm.fichaMatrizPredios fmp" +
                " WHERE fmp.numeroPredial LIKE :numeroPredial";

            Query q = entityManager.createQuery(query);
            q.setParameter("numeroPredial", numPredial + "%");

            answer = q.getResultList();

            if (answer != null && answer.size() > 0) {
                fm = answer.get(0);
            } else {
                return fm;
            }

            Hibernate.initialize(fm.getPredio());
            Hibernate.initialize(fm.getFichaMatrizPredios());
            Hibernate.initialize(fm.getFichaMatrizModelos());

            if (fm.getFichaMatrizModelos() != null) {
                for (FichaMatrizModelo fmm : fm.getFichaMatrizModelos()) {
                    Hibernate.initialize(fmm.getFmModeloConstruccions());
                    if (fmm.getFmModeloConstruccions() != null) {
                        for (FmModeloConstruccion fmmc : fmm.getFmModeloConstruccions()) {
                            Hibernate.initialize(fmmc.getUsoConstruccion());
                            Hibernate.initialize(fmmc.getFmConstruccionComponentes());
                            Hibernate.initialize(fmmc.getModeloConstruccionFotos());
                            if (fmmc.getModeloConstruccionFotos() != null) {
                                for (ModeloConstruccionFoto foto : fmmc.getModeloConstruccionFotos()) {
                                    Hibernate.initialize(foto.getFotoDocumento());
                                }
                            }
                        }
                    }
                }
            }

        } catch (NoResultException e) {
            LOGGER.error("No existen resultados para la consulta");
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FichaMatrizDAOBean#obtenerPorNumeroPredial");
        }
        return fm;
    }

    /**
     *
     * @see IFichaMatrizDAO#obtenerFichaMatrizPorId(java.lang.Long)
     * @author javier.aponte
     * @param numPredial
     * @return
     */
    @Override
    public FichaMatriz obtenerFichaMatrizPorId(Long fichaMatrizId) {

        FichaMatriz resultado = null;

        String q1 = "SELECT fm FROM FichaMatriz fm" +
            " WHERE fm.id = :fichaMatrizId";

        try {

            Query q = entityManager.createQuery(q1);

            q.setParameter("fichaMatrizId", fichaMatrizId);

            resultado = (FichaMatriz) q.getResultList().get(0);

            Hibernate.initialize(resultado.getPredio());

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FichaMatrizDAOBean#obtenerFichaMatrizPorId");
        }

        return resultado;

    }

    /**
     *
     * @see IFichaMatrizDAO#obtenerFichaMatrizPorPredioId(java.lang.Long)
     * @author javier.aponte
     * @param predioId
     * @return
     */
    @Override
    public FichaMatriz obtenerFichaMatrizPorPredioId(Long predioId) {

        FichaMatriz resultado = null;

        String q1 = "SELECT fm FROM FichaMatriz fm JOIN FETCH fm.predio p " +
            " WHERE p.id = :predioId";

        try {

            Query q = entityManager.createQuery(q1);

            q.setParameter("predioId", predioId);

            resultado = (FichaMatriz) q.getResultList().get(0);

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage(),
                "FichaMatrizDAOBean#obtenerFichaMatrizPorId");
        }

        return resultado;

    }

    /**
     * @see IFichaMatrizDAO#findByPredioId(Long)
     * @author javier.aponte
     */
    @Override
    public FichaMatriz findByPredioId(Long idPPredio, boolean asociaciones) {

        try {
            String sql = "SELECT fm FROM FichaMatriz fm " +
                " JOIN FETCH fm.predio p " +
                " WHERE p.id = :idPPredio ";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("idPPredio", idPPredio);

            FichaMatriz fm = (FichaMatriz) query.getSingleResult();
            if (asociaciones) {
                Hibernate.initialize(fm.getPredio());
                Hibernate.initialize(fm.getFichaMatrizPredios());
                Hibernate.initialize(fm.getFichaMatrizModelos());

                if (fm.getFichaMatrizModelos() != null) {
                    for (FichaMatrizModelo fmm : fm.getFichaMatrizModelos()) {
                        Hibernate.initialize(fmm.getFmModeloConstruccions());
                        if (fmm.getFmModeloConstruccions() != null) {
                            for (FmModeloConstruccion fmmc : fmm.getFmModeloConstruccions()) {
                                Hibernate.initialize(fmmc.getUsoConstruccion());
                                Hibernate.initialize(fmmc.getFmConstruccionComponentes());
                                Hibernate.initialize(fmmc.getModeloConstruccionFotos());
                                if (fmmc.getModeloConstruccionFotos() != null) {
                                    for (ModeloConstruccionFoto foto : fmmc.
                                        getModeloConstruccionFotos()) {
                                        Hibernate.initialize(foto.getFotoDocumento());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return fm;
        } catch (NoResultException e) {
            return null;
        }
    }

}
