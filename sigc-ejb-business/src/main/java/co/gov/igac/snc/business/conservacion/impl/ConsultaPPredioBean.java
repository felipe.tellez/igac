/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.conservacion.impl;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.business.conservacion.IConsultaPPredio;
import co.gov.igac.snc.dao.conservacion.IPPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;

/**
 *
 * @author pedro.garcia
 */
@Stateless
public class ConsultaPPredioBean implements IConsultaPPredio, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5978435667607788501L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaPPredioBean.class);

    /**
     * Interfaces locales de servicio
     */
    @EJB
    private IPPredioDAO pPredioService;

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConsultaPPredio#getPPredioFetchPPersonaPredioPorID(Long)
     */
    public PPredio getPPredioFetchPPersonaPredioPorId(Long id) {
        return pPredioService.getPPredioFetchPPersonaPredioPorId(id);
    }

}
