package co.gov.igac.snc.dao.avaluos;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoRequisitoDocumentacion;

@Local
public interface IAvaluoRequisitoDocumentacionDAO extends
    IGenericJpaDAO<AvaluoRequisitoDocumentacion, Long> {

    /**
     * Metodo que retorna los documentos requisitos de la proyeccion de cotizacion de un avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param avaluoId - Id Avaluo
     * @return
     */
    public List<AvaluoRequisitoDocumentacion> obtenerDocsRequisitoPorAvaluoId(
        Long avaluoId);

}
