/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.impl;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.tramite.ITramiteDocumentacionDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class TramiteDocumentacionDAOBean extends
    GenericDAOWithJPA<TramiteDocumentacion, Long> implements
    ITramiteDocumentacionDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteDocumentacionDAOBean.class);

    @EJB
    private ITipoDocumentoDAO tipoDocumentoDAOService;

//--------------------------------------------------------------------------------------------------    
    /**
     * @see ITramiteDocumentacionDAO#findByTramiteId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TramiteDocumentacion> findByTramiteId(Long tramiteId) {
        Query query = this.entityManager.createQuery("SELECT td" +
            " FROM TramiteDocumentacion td" +
            " JOIN FETCH td.tipoDocumento" +
            " LEFT JOIN FETCH td.documentoSoporte" +
            " WHERE td.tramite.id= :tramiteId");
        query.setParameter("tramiteId", tramiteId);
        try {
            return (List<TramiteDocumentacion>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteDocumentacionDAOBean#findByTramiteId");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author lorena.salamanca
     * @see
     * co.gov.igac.snc.dao.tramite.ITramiteDocumentacionDAO#updateMultipleCompleto(java.util.List)
     */
    @Override
    public List<TramiteDocumentacion> updateMultipleCompleto(
        List<TramiteDocumentacion> tramiteDocumentacions) {
        List<TramiteDocumentacion> answer = new LinkedList<TramiteDocumentacion>();
        if (tramiteDocumentacions != null && !tramiteDocumentacions.isEmpty()) {
            for (TramiteDocumentacion td : tramiteDocumentacions) {
                TramiteDocumentacion tdTemp = this.update(td);
                tdTemp.setTipoDocumento(this.tipoDocumentoDAOService
                    .findById(td.getTipoDocumento().getId()));
                tdTemp.getDocumentoSoporte().setTipoDocumento(tdTemp.getTipoDocumento());
                answer.add(tdTemp);
            }
        }
        return answer;
    }

    /**
     * @see ITramiteDocumentacionDAO#buscarTramiteDocumentacionsPorTramiteId(Long)
     * @author rodrigo.hernandez
     */
    @Override
    @Implement
    public List<TramiteDocumentacion> buscarTramiteDocumentacionsPorTramitesIds(
        List<Long> listaIdsTramites) {

        List<TramiteDocumentacion> result = null;

        String queryStr;
        Query query;

        queryStr = "SELECT traDoc" +
            " FROM TramiteDocumentacion traDoc" +
            " WHERE" +
            " traDoc.tramite.id in :listaIdsTramites";

        query = this.entityManager.createQuery(queryStr);
        query.setParameter("listaIdsTramites", listaIdsTramites);

        try {
            result = (List<TramiteDocumentacion>) query.getResultList();

            for (TramiteDocumentacion traDoc : result) {
                Hibernate.initialize(traDoc.getTramite());
                Hibernate.initialize(traDoc.getDocumentoSoporte());
                Hibernate.initialize(traDoc.getDocumentoSoporte().getTipoDocumento());
            }

            return result;
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Falló la carga de documentación del trámite", e);
        }

    }

}
