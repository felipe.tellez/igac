package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IDocumentoArchivoAnexoDAO;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;

/**
 *
 * @author juan.agudelo
 *
 */
@Stateless
public class DocumentoArchivoAnexoDAOBean extends GenericDAOWithJPA<DocumentoArchivoAnexo, Long>
    implements IDocumentoArchivoAnexoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DominioDAOBean.class);

}
