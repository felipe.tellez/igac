/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PModeloConstruccionFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;

/**
 *
 * @author juan.agudelo
 *
 */
@Local
public interface IPPersonaPredioPropiedadDAO extends
    IGenericJpaDAO<PPersonaPredioPropiedad, Long> {

    /**
     * Retorna un PPersonaPredioPropiedad por Id
     *
     * @author juan.agudelo
     * @return PPredio
     */
    public PPersonaPredioPropiedad buscarPPersonaPredioPropiedadPorId(Long id);

    /**
     * Actualiza una lsita de PPErsonaPredioPropiedad
     *
     * @author juan.agudelo
     * @return
     */
    public void gestionarSolicitud(
        List<PPersonaPredioPropiedad> gestionarSolicitud);

    /**
     * Retorna una lista de objetos PPersonaPredioPropiedad según el id del objeto PPersonaPredio
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public List<PPersonaPredioPropiedad> findByPPersonaPredioId(Long id);

    /**
     * Retorna la lista de pPersonaPredioPropiedad de un PPersonaPredio.
     *
     * @author fredy.wilches
     * @param PPersonaPredio
     * @return
     */
    public List<PPersonaPredioPropiedad> buscarTitulosPorPPersonaPredio(PPersonaPredio pp);

    public List<PPersonaPredioPropiedad> consultarPorPPersonaPredioId(Long idPPersonaPredio);
}
