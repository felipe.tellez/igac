package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import java.util.List;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.dao.IGenericJpaDAO;

import co.gov.igac.snc.excepciones.ExcepcionSNC;

/**
 * Servicios de persistencia para el objeto ActualizacionDocumento
 *
 * @author franz.gamba
 */
@Local
public interface IActualizacionDocumentoDAO extends IGenericJpaDAO<ActualizacionDocumento, Long> {

    public List<ActualizacionDocumento> findByActualizacionId(long actualizacionId);
}
