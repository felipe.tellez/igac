package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;

/**
 * Servicios de persistencia del objeto {@link PFichaMatrizModelo}.
 *
 * @author david.cifuentes
 */
@Local
public interface IPFichaMatrizModeloDAO extends IGenericJpaDAO<PFichaMatrizModelo, Long> {

    /**
     * Método que búsca un {@link PFichaMatrizModelo} por su id.
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    public PFichaMatrizModelo cargarModeloDeConstruccion(Long pFichaMatrizModeloId);

}
