package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IAxMunicipioDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Proyecto SNC 2016
 */
/**
 * Clase con metodos para acceso a datos de la entidad AX_MUNICIPIO
 *
 * @author {author}
 */
@Stateless
public class AxMunicipioDAOBean extends GenericDAOWithJPA<AxMunicipio, Long> implements
    IAxMunicipioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AxMunicipioDAOBean.class);

    /**
     * @see IAxMunicipioDAO#obtenerPorMunicipioVigencia()
     * @author felipe.cadena
     */
    @Override
    public List<AxMunicipio> obtenerPorMunicipioVigencia(String municipioCodigo, String vigencia) {

        List<AxMunicipio> resultado = new ArrayList<AxMunicipio>();

        Query query;
        String q;

        q = "SELECT m FROM AxMunicipio m" +
            " WHERE m.municipioCodigo = :municipioCodigo" +
            " AND YEAR(m.vigencia) = :vigencia";

        try {
            query = entityManager.createQuery(q);
            query.setParameter("municipioCodigo", municipioCodigo);
            query.setParameter("vigencia", Integer.valueOf(vigencia));

            resultado = (List<AxMunicipio>) query.getResultList();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return resultado;

    }

    /**
     * @see IAxMunicipioDAO#obtenerMunicipiosEnActualizacion()
     * @author felipe.cadena
     */
    @Override
    public List<AxMunicipio> obtenerMunicipiosEnActualizacion() {

        List<AxMunicipio> resultado = new ArrayList<AxMunicipio>();

        Query query;
        String q;

        q = "SELECT m FROM AxMunicipio m" +
            " WHERE m.fechaInicio <= :fechaActual" +
            " AND m.fechaFin >= :fechaActual";

        try {
            query = entityManager.createQuery(q);
            query.setParameter("fechaActual", Calendar.getInstance().getTime(), TemporalType.DATE);

            resultado = (List<AxMunicipio>) query.getResultList();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return resultado;

    }

}
