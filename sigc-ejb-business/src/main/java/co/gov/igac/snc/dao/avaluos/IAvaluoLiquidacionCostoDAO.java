package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoLiquidacionCosto;

/**
 * Interface que contiene metodos de consulta sobre entity {@link AvaluoLiquidacionCosto}
 *
 * @author rodrigo.hernandez
 *
 */
@Local
public interface IAvaluoLiquidacionCostoDAO extends
    IGenericJpaDAO<AvaluoLiquidacionCosto, Long> {

    /**
     *
     * Método para calcular el valor ejecutado de los avaluos aprobados y finalizados, asociados a
     * un profesional_avaluos_contrato
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato
     */
    public Double calcularValorEjecutadoAvaluosAprobadosPorContratoId(
        Long idContrato);

}
