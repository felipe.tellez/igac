package co.gov.igac.snc.dao.generales.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IFirmaUsuarioDAO;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;

@Stateless
public class FirmaUsuarioDAOBean extends GenericDAOWithJPA<FirmaUsuario, Long>
    implements IFirmaUsuarioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirmaUsuarioDAOBean.class);

    public FirmaUsuario consultarUsuarioFirma(String nombreUsuario) {

        LOGGER.debug("executing FirmaUsuarioDAOBean#consultarUsuarioFirma");

        FirmaUsuario answer = null;
        List<FirmaUsuario> firmas;

        Query query;
        String queryToExecute = "SELECT fu FROM FirmaUsuario fu " +
            " WHERE fu.nombreFuncionarioFirma = :nombreFuncionarioFirma ";

        query = entityManager.createQuery(queryToExecute);
        query.setParameter("nombreFuncionarioFirma", nombreUsuario);

        try {
            firmas = (List<FirmaUsuario>) query.getResultList();
            if (firmas != null && !firmas.isEmpty()) {
                answer = firmas.get(0);
            }

            for (FirmaUsuario firmaUsuario : firmas) {
                if (firmaUsuario.getDocumentoId() != null) {
                    Hibernate.initialize(firmaUsuario.getDocumentoId());
                    if (firmaUsuario.getDocumentoId().getId() != null) {
                        Hibernate.initialize(firmaUsuario.getDocumentoId().getId());
                    }
                }
            }

        } catch (NoResultException nrEx) {
            LOGGER.error("FirmaUsuarioDAOBean#consultarUsuarioFirma: " +
                "La consulta del id de la firma de usuario no retorno datos");
            return answer;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta del documento: " +
                ex.getMessage());
        }
        return answer;
    }

    /**
     * @see IGeneralesLocal#buscarDocumentoFirmaPorUsuario(String)
     * @author leidy.gonzalez
     */
    @Override
    public FirmaUsuario buscarDocumentoFirmaPorUsuario(String nombre) {

        LOGGER.debug("on FirmaUsuarioDAOBean#buscarDocumentoFirmaPorUsuario...");
        FirmaUsuario answer = null;
        String queryString;
        Query query;

        queryString = " SELECT fu from FirmaUsuario fu " +
            "LEFT JOIN FETCH fu.documentoId doc " +
            " WHERE fu.nombreFuncionarioFirma = :nombre";
        query = this.entityManager.createQuery(queryString);
        query.setParameter("nombre", nombre);

        try {
            answer = (FirmaUsuario) query.getSingleResult();
            LOGGER.debug("... ok.");
        } catch (Exception e) {
            LOGGER.error("... error. " + e.getMessage());
        }

        return answer;
    }
}
