package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IEventoAsistenteDAO;
import co.gov.igac.snc.fachadas.IActualizacionLocal;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionNomenclaturaVia;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto ActualizacionEvento.
 *
 * @author franz.gamba
 */
@Stateless
public class EventoAsistenteDAOBean extends GenericDAOWithJPA<EventoAsistente, Long> implements
    IEventoAsistenteDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(EventoAsistenteDAOBean.class);

    /**
     * @see IEventoAsistenteDAO#obtenerAsistenteEventoporActualizacionEvento(Long)
     * @author javier.barajas
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EventoAsistente> obtenerAsistenteEventoporActualizacionEvento(
        Long actualizacionEventoId) {
        LOGGER.debug("obtenerAsistenteEventoporActualizacionEvento");

        List<EventoAsistente> answer = null;
        try {
            Query q = entityManager
                .createQuery("SELECT ea" +
                    " FROM EventoAsistente ea" +
                    " WHERE ea.actualizacionEvento.id= :actualizacionEventoId");
            q.setParameter("actualizacionEventoId", actualizacionEventoId);

            answer = (List<EventoAsistente>) q.getResultList();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

}
