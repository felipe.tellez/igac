/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 * Interface para las operaciones DB de la entidad TramiteDepuracion
 *
 * @author felipe.cadena
 */
@Local
public interface ITramiteDepuracionDAO extends IGenericJpaDAO<TramiteDepuracion, Long> {

    /**
     * Consulta las depuraciones asociadas a un Tramite por el id de este.
     *
     * @author felipe.cadena
     * @param idTramite
     * @return TramiteDepuracion
     *
     */
    public List<TramiteDepuracion> buscarPorIdTramite(Long idTramite);

    /**
     * Consulta los tramites de depuración asociados a los ids de trámites dados.
     *
     * @author felipe.cadena
     * @param idsTramites
     * @param sortField
     * @param sortOrder
     * @param filters
     * @param rowStartIdxAndCount
     * @return List<TramiteDepuracion>
     *
     */
    public List<TramiteDepuracion> buscarPorIds(long[] idsTramites,
        String sortField, String sortOrder, Map<String, String> filters,
        int... rowStartIdxAndCount);

    /**
     * Método que realiza la consulta de los trámites depuración asociados a un digitalizador.
     *
     * @author david.cifuentes
     * @param digitalizador
     * @return
     */
    public List<TramiteDepuracion> buscarTramiteDepuracionPorDigitalizador(
        String digitalizador);

    /**
     * Método que dada una lista de Id's de {@link Tramite} verifica los que tengan un
     * {@link TramiteDepuracion} asociado.
     *
     * @author david.cifuentes
     * @param tramiteIds
     * @return
     */
    public List<Long> verificarTramitesConRegistroTramiteDepuracion(
        List<Long> tramiteIds);

}
