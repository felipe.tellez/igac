package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;

/**
 * Interfaz para los servicios de persistencia del objeto {@link FichaMatrizTorre}.
 *
 * @author david.cifuentes
 */
@Local
public interface IFichaMatrizTorreDAO extends
    IGenericJpaDAO<FichaMatrizTorre, Long> {

    /**
     * Método para verificar si una {@link PFichaMatrizTorre} que se encuentra proyectada, viene de
     * una {@link FichaMatrizTorre} en firme de la {@link FichaMatriz}
     *
     * @param torre
     * @param id
     * @return
     */
    boolean existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long numeroTorre,
        Long idFichaMatriz);

    /**
     * Método para consultar {@link PFichaMatrizTorre} por id {@link FichaMatriz}
     *
     * @param id
     * @return
     */
    public FichaMatrizTorre getFichaMatrizTorreById(Long id);

    /**
     * Método para consultar una {@link PFichaMatrizTorre} que se encuentra proyectada, viene de una
     * {@link FichaMatrizTorre} en firme de la {@link FichaMatriz}
     *
     * @param torre
     * @param id
     * @return
     */
    public FichaMatrizTorre buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
        Long numeroTorre, Long idFichaMatriz);
}
