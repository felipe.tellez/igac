package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPPredioAvaluoCatastralDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class PPredioAvaluoCatastralDAOBean extends
    GenericDAOWithJPA<PPredioAvaluoCatastral, Long> implements
    IPPredioAvaluoCatastralDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioAvaluoCatastralDAOBean.class);

    // -------------------------------------------------------------------------
    /**
     * @see IPPredioAvaluoCatastralDAO#findValorAvaluoByNumeroPredial(String)
     * @author juan.agudelo
     * @version 2.0
     */
    @Override
    public Double findValorAvaluoByNumeroPredial(String numeroPredial) {

        LOGGER.debug("executing PredioDAOBean#findValorAvaluoByNumeroPredial");

        Query query;
        String q;

        q = "SELECT pac.valorTotalAvaluoCatastral " +
            " FROM PPredioAvaluoCatastral pac" +
            " JOIN pac.PPredio p" +
            " WHERE p.numeroPredial = :numeroPredial" +
            " ORDER BY pac.vigencia DESC";

        query = entityManager.createQuery(q);
        query.setParameter("numeroPredial", numeroPredial);
        query.setMaxResults(1);
        try {

            Double answer = (Double) query.getSingleResult();
            return answer;

        } catch (NoResultException ne) {
            return -1.0;
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    // -------------------------------------------------------------------------
    /**
     * @see IPPredioAvaluoCatastralDAO#obtenerListaPPredioAvaluoCatastralPorIdPPredio(Long)
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<PPredioAvaluoCatastral> obtenerListaPPredioAvaluoCatastralPorIdPPredio(
        Long pPredioId) {

        Query query;
        String q;

        q = "SELECT ppac FROM PPredioAvaluoCatastral ppac" +
            " JOIN FETCH ppac.PPredio p" +
            " WHERE p.id = :pPredioId" +
            " ORDER BY ppac.vigencia DESC";

        query = entityManager.createQuery(q);
        query.setParameter("pPredioId", pPredioId);
        try {

            List<PPredioAvaluoCatastral> answer = query.getResultList();
            return answer;
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }
}
