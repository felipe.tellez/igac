package co.gov.igac.snc.dao.generales;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import java.util.Map;

@Local
public interface IDominioDAO extends IGenericJpaDAO<Dominio, Long> {

    /**
     * @documented pedro.garcia retorna la lista de objetos Dominio según el nombre de un dominio en
     * esa tabla
     * @param d Enumeración de la que que se obtiene el nombre del dominio convirtiéndola en String
     * @return
     */
    public List<Dominio> findByNombre(Enum d);

    /**
     * Método que retorna un valor específico dentro de un dominio según su código y el nombre del
     * dominio.
     *
     * @param nombre Nombre del dominio
     * @param codigo Código específico del valor del dominio a buscar.
     * @return
     */
    public Dominio findByCodigo(EDominio nombre, String codigo);

    /**
     * Método que retorna una lista de {@link Dominio} búscandolos por su nombre. Éste método es una
     * copia del método findByNombre, y retornando la lista ordenada por este mismo parametro.
     *
     * @author david.cifuentes
     * @param d Enumeración de la que que se obtiene el nombre del dominio convirtiéndola en String
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<Dominio> findByNombreOrderByNombre(Enum d);

    /**
     *
     * @return
     */
    public Map<String, List<Dominio>> obtenerSolicitudesYTramites();

    /**
     * retorna la lista de objetos Dominio según el nombre de un dominio en esa tabla
     *
     * @param d Enumeración de la que que se obtiene el nombre del dominio convirtiéndola en String
     * @return
     */
    public List<Dominio> buscarNombreOrdenadoPorId(Enum d);

	/**
	 * Método que retorna una lista de {@link Dominio} buscandolos por su nombre.
	 * Éste método es una copia del método findByNombre, y retornando la lista
	 * ordenada por el valor del dominio.
	 *
	 * @author felipe.cadena
	 * @param d
	 * @return
	 */
	public  List<Dominio> findByNombreOrderByValor(Enum d);
}
