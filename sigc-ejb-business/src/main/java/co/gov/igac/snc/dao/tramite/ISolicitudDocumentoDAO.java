package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;

@Local
public interface ISolicitudDocumentoDAO extends
    IGenericJpaDAO<SolicitudDocumento, Long> {

    /**
     * Método para obtener un objeto SolicitudDocumento a partir del id de la Solicitud. Retorna el
     * ultimo objeto creado.
     *
     * @author rodrigo.hernandez
     *
     * @param solicitudId Id de la Solicitud
     * @param documentoId Id del documento
     * @return
     */
    public SolicitudDocumento obtenerSolicitudDocumentoPorSolicitudId(
        Long solicitudId);

    /**
     * Método para buscar un documento por id de solicitud y por el id del tipo de documento
     *
     * @param idSolicitud
     * @param idTipoDocumento
     * @author javier.aponte
     */
    public SolicitudDocumento buscarDocumentoPorSolicitudIdAndTipoDocumentoId(Long idSolicitud,
        Long idTipoDocumento);

}
