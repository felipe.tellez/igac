/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.procesos;


import co.gov.igac.snc.apiprocesos.protocolo.rest.RespuestaConteoActividad;
import co.gov.igac.snc.procesos.Entidades.BpmActividad;
import co.gov.igac.snc.procesos.Entidades.BpmFlujoTramite;
import co.gov.igac.snc.procesos.Entidades.BpmProcesoInstancia;
import co.gov.igac.snc.procesos.Entidades.BpmRol;
import co.gov.igac.snc.procesos.Entidades.BpmRolActividad;
import co.gov.igac.snc.procesos.Entidades.BpmTipoTramite;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author michael.pena
 */
@Stateless
public class FlujoBpmDAOBean {
    
    @PersistenceContext(unitName = "test-oradb")
    private EntityManager entityManager;
    
    public static final Logger LOGGER = LoggerFactory.getLogger(FlujoBpmDAOBean.class);
    
//    public static String LISTA_ACTIVIDADES_TAREAS = "SELECT est.estado,bact.urlcasodeuso,bact.nombreactividad,bact.macroproceso,bact.tipo,bact.tipoprocesamiento,bact.proceso,bact.subprocesoactividad,\n"
//            + "bact.actividad,bact.trancisiones,bact.macroprocesoproceso,bjt.id AS id_flujo_tramite, bjt.fecha_inicio,bjt.fecha_final,pid.tramite AS idObjetoNegocio,sncts.numero,\n"
//            + "tra.numero_radicacion,pre.numero_predial,bjt.usuario_responsable, tpt.tramite, bact.id AS id_actividad, tra.fecha_radicacion FROM procesos.bpm_flujo_tramite bjt \n"
//            + "INNER JOIN procesos.bpm_actividad bact ON bact.id = bjt.actividad_id \n"
//            + "INNER JOIN procesos.bpm_tipo_tramite tpt ON tpt.id = bjt.tipo_tramite_id \n"
//            + "INNER JOIN procesos.bpm_estado est ON est.id = bjt.id_estado \n"
//            + "INNER JOIN procesos.bpm_proceso_instancia pid ON pid.id = bjt.proceso_instancia_id \n"
//            + "INNER JOIN tramite tra ON tra.id = pid.tramite \n"
//            + "LEFT JOIN snc_conservacion.predio pre ON pre.id = tra.predio_id \n"
//            + "INNER JOIN snc_tramite.solicitud sncts ON sncts.id = tra.solicitud_id \n";
//            //+ "INNER JOIN snc_tramite.solicitud sncts ON sncts.proceso_instancia_id = pid.ID_INSTANCIA_SNC";
    
private String LISTA_ACTIVIDADES_TAREAS ="select * from (SELECT\n" +
"        est.estado,\n" +
"        bact.urlcasodeuso,\n" +
"        bact.nombreactividad,\n" +
"        bact.macroproceso,\n" +
"        bact.tipo,\n" +
"        bact.tipoprocesamiento,\n" +
"        bact.proceso,\n" +
"        bact.subprocesoactividad,\n" +
"        bact.actividad,\n" +
"        bact.trancisiones,\n" +
"        bact.macroprocesoproceso,\n" +
"        bjt.id             AS id_flujo_tramite,\n" +
"        bjt.fecha_inicio,\n" +
"        bjt.fecha_final,\n" +
"        pid.tramite        AS idobjetonegocio,\n" +
"        sncts.numero,\n" +
"        tra.numero_radicacion,\n" +
"        pre.numero_predial,\n" +
"        bjt.usuario_responsable,\n" +
"        tpt.tramite,\n" +
"        bact.id            AS id_actividad,\n" +
"        tra.fecha_radicacion,\n" +
"        'TRAMITE' tipo_proceso,\n" +
"        bjt.territorial,\n" +
"        bjt.cod_territorial,\n" +
"        bjt.uoc,\n" +
"        bjt.cod_uoc,\n" +
"        bact.description   AS actividad_descripcion,\n" +
"        pid.id_instancia_snc,\n" +
"        est.id             id_estado,\n" +
"        tra.orden_ejecucion\n" +
"    FROM\n" +
"        procesos.bpm_flujo_tramite bjt\n" +
"        INNER JOIN procesos.bpm_actividad bact ON bact.id = bjt.actividad_id\n" +
"        INNER JOIN procesos.bpm_tipo_tramite tpt ON tpt.id = bjt.tipo_tramite_id\n" +
"        INNER JOIN procesos.bpm_estado est ON est.id = bjt.id_estado\n" +
"        INNER JOIN procesos.bpm_proceso_instancia pid ON pid.id = TO_CHAR(bjt.proceso_instancia_id)\n" +
"        INNER JOIN tramite tra ON tra.id = pid.tramite\n" +
"                                  AND bjt.tipo_tramite_id > 0\n" +
"        LEFT JOIN snc_conservacion.predio pre ON pre.id = tra.predio_id\n" +
"        INNER JOIN snc_tramite.solicitud sncts ON sncts.id = tra.solicitud_id    \n" +
"    UNION ALL\n" +
"    SELECT\n" +
"        est.estado,\n" +
"        bact.urlcasodeuso,\n" +
"        bact.nombreactividad,\n" +
"        bact.macroproceso,\n" +
"        bact.tipo,\n" +
"        bact.tipoprocesamiento,\n" +
"        bact.proceso,\n" +
"        bact.subprocesoactividad,\n" +
"        bact.actividad,\n" +
"        bact.trancisiones,\n" +
"        bact.macroprocesoproceso,\n" +
"        bjt.id        AS id_flujo_tramite,\n" +
"        bjt.fecha_inicio,\n" +
"        bjt.fecha_final,\n" +
"        pid.tramite   AS idobjetonegocio,\n" +
"        sncts.numero,\n" +
"        sncts.numero,\n" +
"        sncts.numero,\n" +
"        bjt.usuario_responsable,\n" +
"        '-1',\n" +
"        bact.id       AS id_actividad,\n" +
"        pid.fecha_inicio,\n" +
"        'PRODUCTO_CATASTRAL',\n" +
"        bjt.territorial,\n" +
"        bjt.cod_territorial,\n" +
"        bjt.uoc,\n" +
"        bjt.cod_uoc,\n" +
"        bact.description,\n" +
"        pid.id_instancia_snc,\n" +
"        est.id        idestado,\n" +
"        NULL\n" +
"    FROM\n" +
"        procesos.bpm_flujo_tramite bjt\n" +
"        INNER JOIN procesos.bpm_actividad bact ON bact.id = bjt.actividad_id\n" +
"        INNER JOIN procesos.bpm_estado est ON est.id = bjt.id_estado\n" +
"        INNER JOIN procesos.bpm_proceso_instancia pid ON pid.id = bjt.proceso_instancia_id\n" +
"                                                         AND bjt.tipo_tramite_id = - 1\n" +
"        INNER JOIN snc_tramite.solicitud sncts ON sncts.id = pid.tramite) PROCESO ";
    
    public static String SP_CREAR_PROCESO_CONSERVACION = "DECLARE\n"
            + "  PFECHA_INICIO TIMESTAMP;\n"
            + "  PTRAMITE_ID NUMBER;\n"
            + "  PESTADO_FT_INICIAL VARCHAR2(200);\n"
            + "  PESTADO_PI_INICIAL VARCHAR2(200);\n"
            + "  PACTIVIDAD_INICIAL VARCHAR2(200);\n"
            + "  PUSUARIO VARCHAR2(200);\n"
            + "  OBSERVACIONES VARCHAR2(200);\n"
            + "  ERRORES SYS_REFCURSOR;\n"
            + "BEGIN\n"
            + "   BPM_PKG_FLUJO_PROCESO.CREAR_PROCESO_CONSERVACION(\n"
            + "    PFECHA_INICIO => TO_DATE(:fechaRadiacion,'DD-MM-YYYY HH12:MI:SS PM'),\n"
            + "    PTRAMITE_ID => :idTramite,\n"
            + "    PESTADO_FT_INICIAL => :estadoActividad,\n"
            + "    PESTADO_PI_INICIAL => :estadoProceso,\n"
            + "    PACTIVIDAD_INICIAL => :actividad,\n"
            + "    PUSUARIO => :usuarioPropietario,\n"
            + "    OBSERVACIONES => :observaciones,\n"
            + "    ERRORES => ERRORES\n"
            + "  );\n"
            + "END;";
    
     public static String SP_CREAR_PROCESO_CONSERVACION2 = "DECLARE\n"
            + "  PFECHA_INICIO TIMESTAMP;\n"
            + "  PTRAMITE_ID NUMBER;\n"
            + "  PESTADO_FT_INICIAL VARCHAR2(200);\n"
            + "  PESTADO_PI_INICIAL VARCHAR2(200);\n"
            + "  PACTIVIDAD_INICIAL VARCHAR2(200);\n"
            + "  PUSUARIO VARCHAR2(200);\n"
            + "  PTIPO_PROCESO VARCHAR2(1);\n"
            + "  PCODIGO_TERRITORIAL VARCHAR2(200);\n"
            + "  PCODIGO_UOC VARCHAR2(200);\n"
            + "  OBSERVACIONES VARCHAR2(200);\n"
            + "  ERRORES SYS_REFCURSOR;\n"
            + "BEGIN\n"
            + "   BPM_PKG_FLUJO_PROCESO.CREAR_PROCESO_CONSERVACION2(\n"
            + "    PFECHA_INICIO => TO_DATE(:fechaRadiacion,'DD-MM-YYYY HH12:MI:SS PM'),\n"
            + "    PTRAMITE_ID => :idTramite,\n"
            + "    PESTADO_FT_INICIAL => :estadoActividad,\n"
            + "    PESTADO_PI_INICIAL => :estadoProceso,\n"
            + "    PACTIVIDAD_INICIAL => :actividad,\n"
            + "    PUSUARIO => :usuarioPropietario,\n"
            + "    PTIPO_PROCESO => :tipoProceso, \n"
            + "    PCODIGO_TERRITORIAL => :codigoTerritorial, \n"
            + "    PCODIGO_UOC => :codigoUOC, \n"
            + "    OBSERVACIONES => :observaciones,\n"
            + "    ERRORES => ERRORES\n"
            + "  );\n"
            + "END;";

    private static String LISTA_ACTIVIDAD_DEPURACION ="select bft.ID , bft.FECHA_INICIO, bft.FECHA_FINAL, bft.PROCESO_INSTANCIA_ID, bft.USUARIO_RESPONSABLE,\n" +
"bft.ACTIVIDAD_ID, bft.ID_ESTADO, bft.TERRITORIAL, bft.UOC, bact.NOMBREACTIVIDAD from PROCESOS.BPM_FLUJO_TRAMITE bft\n" +
"inner join procesos.bpm_actividad bact on bft.ACTIVIDAD_ID = bact.ID\n" +
"inner join PROCESOS.BPM_PROCESO_INSTANCIA PI  on PI.ID = bft.PROCESO_INSTANCIA_ID";

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    //findByTramite
    //-------------------------------------------------------------------------------------------------
    //METODOS DE PERSISTENCIA DAO
    
    
    public void testUpdate() {        
        
        String update = "UPDATE BpmFlujoTramite SET usuarioResponsable =:usuario WHERE id =:id";
            long valor = 5479;
            Query query = entityManager.createQuery(update);
            query.setParameter("usuario", "DEVELOPER");
            query.setParameter("id",valor);
            query.executeUpdate();

    }
    
    
    public void crearProceoConservacionDAO(String idTramite, String actividad,
            String fechaRadiacion, String estadoActividad, String estadoProceso, String usuarioPropietario, 
            String tipoProceso, String codigoTerritorial, String codigoUOC, String observaciones) {
       
        try {
            Query q = null;
            if (tipoProceso.equals("P")){
                q = entityManager.createNativeQuery(SP_CREAR_PROCESO_CONSERVACION2);
                q.setParameter("tipoProceso", tipoProceso);
                q.setParameter("codigoTerritorial", codigoTerritorial);
                q.setParameter("codigoUOC", codigoUOC);
            }else{
               q = entityManager.createNativeQuery(SP_CREAR_PROCESO_CONSERVACION);
            }            
            q.setParameter("fechaRadiacion", fechaRadiacion);
            q.setParameter("idTramite", idTramite);
            q.setParameter("estadoActividad", estadoActividad);
            q.setParameter("estadoProceso", estadoProceso);
            q.setParameter("actividad", actividad);
            q.setParameter("usuarioPropietario", usuarioPropietario);
            
            q.setParameter("observaciones", observaciones);
            int resultado =  q.executeUpdate();              

        } catch (Exception e) {

            LOGGER.error("ERROR UPDATE DAO crearProceoConservacionDAO: " + e);

        }

    }
    
    public boolean reclamarActividadConservacionDAO(Long idActividad, String loginUser) {
            
            try {
                
            String update = "UPDATE BPM_FLUJO_TRAMITE SET usuario_responsable =:usuarioResponsable, ID_ESTADO = 8 WHERE id =:id";
            Query query = entityManager.createNativeQuery(update);
            query.setParameter("usuarioResponsable", loginUser);
            query.setParameter("id", idActividad);
            query.executeUpdate();
                
            
            }catch (Exception e){
                
                LOGGER.info("ERROR UPDATE DAO reclamarActividadConservacionDAO" + e);
            
                return false;
            
            }          
            
            return true;

    }
    
    public void updateActividadOldFlujoTramite(String fechaFinal, Long idEstado, Long idActividad) {
        
        String update = "UPDATE BPM_FLUJO_TRAMITE SET fecha_final = TO_DATE(:fechaFinal,'DD-MM-YYYY HH12:MI:SS PM'), id_estado =:idEstado WHERE id =:id";
        
        try {
            
            Query query = entityManager.createNativeQuery(update);
            query.setParameter("fechaFinal", fechaFinal);
            query.setParameter("idEstado", idEstado);
            query.setParameter("id", idActividad);
            query.executeUpdate();            
            //entityManager.getTransaction().commit();
        }catch (Exception e){
           
            LOGGER.info("ERROR UPDATE DAO updateActividadOldFlujoTramite" + e);
            
        }
        
    }
    
    public void updateBpmProcesoInstancia(String fechaFinal, Long idEstado, String idIntanciaSnc) {
        
        String update = "UPDATE BPM_PROCESO_INSTANCIA SET FECHA_FIN = TO_DATE(:fechaFinal,'DD-MM-YYYY HH12:MI:SS PM'), id_estado =:idEstado WHERE id =:id";
        
        try {
            
            Query query = entityManager.createNativeQuery(update);
            query.setParameter("fechaFinal", fechaFinal);
            query.setParameter("idEstado", idEstado);
            query.setParameter("id", idIntanciaSnc);
            query.executeUpdate();
        
        }catch (Exception e){
           
            LOGGER.info("ERROR UPDATE DAO updateActividadOldFlujoTramite" + e);
            
        }
        
    }
    
    public void crearActividadConservacion(String fechaInicio, Long procesoInstanciaId, Long actividadId,
            Long tipoTramiteId, int idEstado, String usuarioResponsable, String territorial, String uoc, String codTerritorial, String codUoc) {
        String uocInsert = null;
        String uocValue = null;
        String fechaFinal =  null;
        
        try {
           
           if (codUoc != null && !codUoc.isEmpty()) {
                uocInsert = ", COD_UOC";
                uocValue = ","+codUoc;
            } else {
                uocInsert = "";
                uocValue = "";
            }
           
           if (actividadId == 79){
               fechaFinal = fechaInicio;
           }
              
           Query query = entityManager.createNativeQuery("INSERT INTO BPM_FLUJO_TRAMITE (FECHA_INICIO, FECHA_FINAL, PROCESO_INSTANCIA_ID, ACTIVIDAD_ID, "
                   + "TIPO_TRAMITE_ID, ID_ESTADO, USUARIO_RESPONSABLE, TERRITORIAL, UOC, COD_TERRITORIAL"+uocInsert+") VALUES "
                   + "(TO_DATE(:fechaInicio,'DD-MM-YYYY HH12:MI:SS PM'),TO_DATE(:fechaFinal,'DD-MM-YYYY HH12:MI:SS PM'),:procesoInstanciaId,:actividadId,:tipoTramiteId,:idEstado,:usuarioResponsable,:territorial,:uoc,:codTerritorial"+uocValue+")");
           query.setParameter("fechaInicio", fechaInicio);
           query.setParameter("fechaFinal", fechaFinal);
           query.setParameter("procesoInstanciaId", procesoInstanciaId);
           query.setParameter("actividadId", actividadId);
           query.setParameter("tipoTramiteId", tipoTramiteId);
           query.setParameter("idEstado", idEstado);
           query.setParameter("usuarioResponsable", usuarioResponsable);
           query.setParameter("territorial", territorial);
           query.setParameter("uoc", uoc);
//           query.setParameter("tramitePadre", tramitePadre);
           query.setParameter("codTerritorial", Integer.valueOf(codTerritorial));
           query.executeUpdate();

        } catch (Exception e) {
            
            LOGGER.error("ERROR UPDATE DAO crearActividadConservcion: " + e);
        }
    }
    
    public void transferirActividadConservacionDao (List<Long> idActividades, String usuarioResponsable){
             LOGGER.info("TRANSFIRIENDO ACTIVIDAD CONSERVACION  PARA USUARIO " + usuarioResponsable);
             String update = "UPDATE BpmFlujoTramite SET usuarioResponsable =:usuarioResponsable WHERE id in (:id)";
        try {
            
             Query query = entityManager.createQuery(update);
             query.setParameter("usuarioResponsable", usuarioResponsable);
             query.setParameter("id",idActividades);
             query.executeUpdate();
        
        }catch (Exception e) {
            LOGGER.error("ERROR UPDATE DAO transferirActividadConservacionDao: " + e);
        } 
        
    }

    
    public void cancelarProcesoPorIdProcesoDao (String idProceso, String motivo, Long idEstado, Long idProcesoInstancia, Date fechaFin){
             LOGGER.info("CANCELANDO PROCESO ID " + idProceso);
             String updateProceso = "UPDATE BpmProcesoInstancia SET idEstado =:idEstado, observaciones =:observaciones, fechaFin =:fechaFin WHERE idInstanciaSnc =:idInstanciaSnc";
             String updateFlujo = "UPDATE BpmFlujoTramite SET idEstado =:idEstado, fechaFinal =:fechaFinal WHERE procesoInstanciaId =:idProcesoInstancia AND idEstado in (2,8)";
             long idEstadoTramite = 7;
        try {
            
             Query query = entityManager.createQuery(updateProceso);
             query.setParameter("idEstado", idEstado);
             query.setParameter("fechaFin", fechaFin);
             query.setParameter("observaciones", motivo);
             query.setParameter("idInstanciaSnc", idProceso);
             query.executeUpdate();
             
             Query q = entityManager.createQuery(updateFlujo);
             q.setParameter("idEstado", idEstadoTramite);
             q.setParameter("fechaFinal", fechaFin);
             q.setParameter("idProcesoInstancia", idProcesoInstancia);
             q.executeUpdate();
        
        }catch (Exception e) {
            LOGGER.error("ERROR UPDATE DAO cancelarProcesoPorIdProcesoDao: " + e);
        } 
        
    }
    
    public void cancelarTramiteId (String idTramite){
             LOGGER.info("CANCELANDO TABLA_TRAMITE == " + idTramite);
             String update = "UPDATE TRAMITE SET ESTADO = 'CANCELADO' WHERE ID =:idTramite";
        try {
            
             Query query = entityManager.createNativeQuery(update);
             query.setParameter("idTramite",idTramite);
             query.executeUpdate();
        
        }catch (Exception e) {
            LOGGER.error("ERROR UPDATE DAO cancelarTramiteId: " + e);
        } 
        
    }
    
    
    //-------------------------------------------------------------------------------------------------
    //METODOS DE FILTRADO DAO
    public List<BpmActividad> bpmActividadFindById(BigDecimal idActividad) {

        List<BpmActividad> bpmActividad = null;

        try {            
            Query queryRol = entityManager.createNamedQuery("BpmActividad.findById");
            queryRol.setParameter("id", idActividad);
            bpmActividad = (List<BpmActividad>) queryRol.getResultList();

        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO BpmActividadFindById: " + e);
        }

        return bpmActividad;
    }
    
    public List<BpmProcesoInstancia> findByIdInstanciaSnc(String  idProceso) {

        List<BpmProcesoInstancia> bpmProceso = null;

        try {            
            Query queryRol = entityManager.createNamedQuery("BpmProcesoInstancia.findByIdInstanciaSnc");
            queryRol.setParameter("idInstanciaSnc", idProceso);
            bpmProceso = (List<BpmProcesoInstancia>) queryRol.getResultList();

        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO findByIdInstanciaSnc: " + e);
        }

        return bpmProceso;
    }
    
    public List<BpmFlujoTramite> maxIdBpmFlujoTramite(Long procesoInstanciaId) {

        List<BpmFlujoTramite> resultado = null;
        //String query = "SELECT max(id) as MAX_ID FROM BpmFlujoTramite WHERE procesoInstanciaId =:procesoInstanciaId";
        try {            
            Query q = entityManager.createNamedQuery("BpmFlujoTramite.findByProcesoInstanciaId");
            q.setParameter("procesoInstanciaId", procesoInstanciaId);
            resultado = (List<BpmFlujoTramite>)q.getResultList();
            
        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO maxIdBpmFlujoTramite: " + e);
        }

        return resultado;
    }
    
    
     public List<BpmFlujoTramite> bpmFlujoTramiteFindById(Long idFlujoTramite) {

        List<BpmFlujoTramite> bpmFlujoTramite = null;

        try {            
            Query queryRol = entityManager.createNamedQuery("BpmFlujoTramite.findById");
            queryRol.setParameter("id", idFlujoTramite);            
            bpmFlujoTramite = (List<BpmFlujoTramite>)queryRol.getResultList();
           
        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO BpmFlujoTramiteFindById: " + e);
        }

        return bpmFlujoTramite;
    }
    
    
    public List<Object[]> obtenerConteoActividadesUsuarioDAO(String usuario, String codTerritorial, String codUoc, List<Long> idActividad) {

        List<Object[]> respuesta = null;

        String conteo = "SELECT Count(*) conteo, bact.description, bact.urlcasodeuso, bact.tipoprocesamiento, bact.tipo, bact.trancisiones \n"
                + "FROM procesos.bpm_actividad bact INNER JOIN procesos.bpm_flujo_tramite bjt ON bact.id = bjt.actividad_id WHERE bjt.id_estado in (2,8) AND";

        String orderBy = "GROUP BY bact.description, bact.urlcasodeuso, bact.tipoprocesamiento, bact.tipo, bact.trancisiones";
        String orderByFinal = null;

        try {
            LOGGER.info("USUARIO DAO " + usuario);
            LOGGER.info("CODIGO TERRIDORIAL  " + codTerritorial);
            LOGGER.info("ROLES OBTENIDOS = " + Arrays.toString(idActividad.toArray()));

            if (codUoc != null && !codUoc.isEmpty()) {
                orderByFinal = "AND bjt.cod_uoc= "+codUoc +" "+ orderBy;
            } else {
                orderByFinal = orderBy;
            }
            Query query = entityManager.createNativeQuery(conteo + " bjt.actividad_id in "
                    + "(:actividadId) AND bjt.cod_territorial = :codTerritorial AND (bjt.usuario_responsable IS NULL OR bjt.usuario_responsable = :usuarioResponsable) " + orderByFinal);
            query.setParameter("actividadId", idActividad);
            query.setParameter("codTerritorial", codTerritorial);
            query.setParameter("usuarioResponsable", usuario);
            respuesta = (List<Object[]>) query.getResultList();

            LOGGER.info("CONTEO ACTIVIDADES RESULTADO DAO  SIZE " + respuesta.size());

        } catch (Exception e) {

            LOGGER.error("ERROR EN EL CONTEO DE ACTIVIDADES DAO: " + e);
        }

        return respuesta;

    }

    public List<BpmRol> bpmRolDao(List<String> rolesCadena) {

        List<BpmRol> bpmRol = null;

        try {            
            Query queryRol = entityManager.createNamedQuery("BpmRol.findByListRol");
            queryRol.setParameter("rol", rolesCadena);
            bpmRol = (List<BpmRol>) queryRol.getResultList();

        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO bpmRolDao: " + e);
        }

        return bpmRol;
    }
    
    public List<BpmRolActividad> findByListIdRol(List<Long> idRol) {

        List<BpmRolActividad> bpmRolActividad = null;
        
        try {   
        Query queryRolActividad = entityManager.createNamedQuery("BpmRolActividad.findByListIdRol");
        queryRolActividad.setParameter("idRol",idRol);
        bpmRolActividad = (List<BpmRolActividad>)queryRolActividad.getResultList();

        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO bpmRolActividadDao : " + e);
        }

        return bpmRolActividad;
    }
    
    public List<BpmRolActividad> rolActividadDAO(Long idActividad, List<Long> idRol) {

        List<BpmRolActividad> bpmRolActividad = null;
        
        try {   
        Query query = entityManager.createNamedQuery("BpmRolActividad.findByIdActividadList");
        query.setParameter("idActividad",idActividad);
        query.setParameter("idRol", idRol);
        bpmRolActividad = (List<BpmRolActividad>)query.getResultList();

        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO bpmRolActividadDao : " + e);
        }

        return bpmRolActividad;
    }
       
    public  List<BpmActividad> findByNombreactividad(String actividad) {

         List<BpmActividad> bpmRolActividad = null;
        
        try {   
        Query queryRolActividad = entityManager.createNamedQuery("BpmActividad.findByNombreactividad");
        queryRolActividad.setParameter("nombreactividad",actividad);
        bpmRolActividad = ( List<BpmActividad>)queryRolActividad.getResultList();

        } catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO bpmRolActividadDao : " + e);
        }

        return bpmRolActividad;
    }
    
    public List<Object[]> obtenerTareasPorUsuarioDAO(String usuario, String codTerritorial, String codUoc, String actividad) {
    
         List<Object[]> respuesta = null;
         String orderBy = "ORDER BY PROCESO.FECHA_INICIO ASC";
         String orderByFinal= null;
         try {
             if (codUoc != null && !codUoc.isEmpty()) {
                orderByFinal = "AND PROCESO.COD_UOC= "+ codUoc +" "+ orderBy;
            } else {
                orderByFinal = orderBy;                
            }
             
        Query query = entityManager.createNativeQuery(LISTA_ACTIVIDADES_TAREAS + " WHERE PROCESO.ID_ESTADO IN ( 2, 8 ) AND "
              + "( PROCESO.USUARIO_RESPONSABLE IS NULL OR PROCESO.USUARIO_RESPONSABLE =:usuario) AND PROCESO.ACTIVIDAD_DESCRIPCION =:actividad AND PROCESO.COD_TERRITORIAL =:codTerritorial " + orderByFinal);
             query.setParameter("usuario",usuario);
             query.setParameter("actividad",actividad);
             query.setParameter("codTerritorial",codTerritorial);             
             respuesta = (List<Object[]>) query.getResultList();
         }catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO obtenerTareasPorUsuarioDAO: " + e);
        }
         
         return respuesta;
    
    }
    
    
    public List<Object[]> filtroDepuracion(String tramite) {
    
         List<Object[]> respuesta = null;
         String orderBy = "and bact.NOMBREACTIVIDAD not like '%Depura%' order by bft.FECHA_FINAL desc";
         try {
             Query query = entityManager.createNativeQuery(LISTA_ACTIVIDAD_DEPURACION + " where PI.TRAMITE =:tramite " + orderBy);
             query.setParameter("tramite",tramite);         
             respuesta = (List<Object[]>) query.getResultList();
         }catch (Exception e) {
            LOGGER.error("ERROR CONSULTA DAO obtenerTareasPorUsuarioDAO: " + e);
        }
         
         return respuesta;
    
    }
    
    
    public List<Object[]> obtenerListaActividadesConFiltroDAO(String queryFiltros) {

        List<Object[]> resultado = null;
        try {
            Query query = entityManager.createNativeQuery(queryFiltros);            
            resultado = query.getResultList(); 

        } catch (RuntimeException re) {
            LOGGER.error("ERROR CONSULTA DAO obtenerListaActividadesConFiltroDAO " + re);
        } 
        return resultado;
    }
    
    
    public List<Object[]> obtenerObjetoNegocioConservacionDAO(String idProceso, String nombreActividad) {

        List<Object[]> resultado = null;
        try {
            
            Query query = entityManager.createNativeQuery(LISTA_ACTIVIDADES_TAREAS +" WHERE PROCESO.ID_INSTANCIA_SNC =:idProceso AND PROCESO.NOMBREACTIVIDAD =:nombreActividad");
            query.setParameter("idProceso", idProceso);
            query.setParameter("nombreActividad", nombreActividad);
            resultado =(List<Object[]>)query.getResultList();

        } catch (RuntimeException re) {
            LOGGER.error("ERROR CONSULTA DAO obtenerObjetoNegocioConservacionDAO " + re);
        } 
        return resultado;
    }
    
    public List<Object[]> obtenerActividadesProcesoDAO(String idProceso) {

        List<Object[]> resultado = null;
        try {
            
            Query query = entityManager.createNativeQuery(LISTA_ACTIVIDADES_TAREAS +" WHERE PROCESO.ID_INSTANCIA_SNC =:idProceso AND PROCESO.ID_ESTADO IN (2,8)");
            query.setParameter("idProceso", idProceso);
            resultado =(List<Object[]>)query.getResultList();

        } catch (RuntimeException re) {
            LOGGER.error("ERROR CONSULTA DAO obtenerObjetoNegocioConservacionDAO " + re);
        } 
        return resultado;
    }
    
    
    public List<Object[]> rolTarea(String idActividad){
       
        List<Object[]> resultado = null;
        String SQL = "Select ra.id, r.ROL from procesos.bpm_rol_actividad ra inner join procesos.bpm_rol r on ra.ID_ROL = r.ID where ID_ACTIVIDAD =:idActividad";
        
        try{
             Query query = entityManager.createNativeQuery(SQL);             
             query.setParameter("idActividad", idActividad);
             resultado = ( List<Object[]>)query.getResultList();
             
        }catch (Exception e) {
            
            LOGGER.error("ERROR CONSULTA DAO rolTarea: " + e);
      
        }
        return resultado;
    
    }  
    
    public List<BpmProcesoInstancia> bpmProcesoInstanciafindByTramite (long tramite){
        
        List<BpmProcesoInstancia> resultado = null;
        
        try{
            Query query = entityManager.createNamedQuery("BpmProcesoInstancia.findByTramite");
            query.setParameter("tramite", tramite);
            resultado = (List<BpmProcesoInstancia>)query.getResultList();
            
        }catch(Exception e) {
            
            LOGGER.error("ERROR CONSULTA DAO bpmProcesoInstanciafindByTramite: " + e);
      
        }
        return resultado;
        
    }
    
    public List<Object[]> bpmProcesoInstanciaSolicitud (String tramite){
        
        List<Object[]> resultado = null;
        
        try{
            Query query = entityManager.createNativeQuery("select ID, PROCESO_INSTANCIA_ID from SOLICITUD where TIPO =8 and ID =:tramite");
            query.setParameter("tramite", tramite);
            resultado = (List<Object[]>)query.getResultList();
            
        }catch(Exception e) {
            
            LOGGER.error("ERROR CONSULTA DAO bpmProcesoInstanciafindByTramite: " + e);
      
        }
        return resultado;
        
    }
    
    public List<BpmTipoTramite> bpmTipoTramitefindByTramite (String tramite){
        
        List<BpmTipoTramite> resultado = null;
        
        try{
            Query query = entityManager.createNamedQuery("BpmTipoTramite.findByTramite");
            query.setParameter("tramite", tramite);
            resultado = (List<BpmTipoTramite>)query.getResultList();
            
        }catch(Exception e) {
            
            LOGGER.error("ERROR CONSULTA DAO bpmProcesoInstanciafindByTramite: " + e);
      
        }
        return resultado;
        
    }
    
    
    //-------------------------------------------------------------------------------------------------
    //METODOS CONSTRUCCION ATRIBUTOS
    
    public ArrayList<String> construyeArrayList(String cadenaTransicione) {
        ArrayList<String> transiciones = new ArrayList<String>(Arrays.asList(cadenaTransicione.trim().split(";")));
        return transiciones;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    
    
    
}
