package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.AsignacionControlCalidad;

/**
 * Servicios de persistencia para el objeto LevantamientoAsignacion
 *
 * @author javier.barajas
 */
@Local
public interface IAsignacionControlCalidadDAO extends IGenericJpaDAO<AsignacionControlCalidad, Long> {

}
