package co.gov.igac.snc.jms;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 * Interfaz local para el acceso a los servicios de IProcesos
 *
 * Nota: Los servicios definidos en esta interfaz no serán expuestos a la capa web
 *
 * @author juan.mendez
 *
 */
@Local
public interface IIntegracionLocal {

    /**
     * Programa la creación de la réplica de la base de datos Geográfica para un trámite
     *
     * @author juan.mendez
     * @param tramite
     * @param usuario
     */
    @Deprecated
    public void generarReplicaParaTramite(Tramite tramite, Actividad actividad, UsuarioDTO usuario);

    /**
     * Programa la creación de la réplica de la base de datos Geográfica para un trámite de quinta
     *
     * @author fredy.wilches
     * @param tramite
     * @param usuario
     * @param actividad
     * @param predioResultante
     */
    //TODO :: fredy.wilches :: documentar los parámetros del método :: pedro.garcia    
    @Deprecated
    public void generarReplicaParaTramite(Tramite tramite, Actividad actividad, UsuarioDTO usuario,
        PPredio predioResultante);

    /**
     * Método encargado de llamar la validacion de la Geometría de predios de forma asincrónica
     *
     * @param identificadoresPredios numero predial de los predio que se quieren validar separados
     * por coma
     * @param tramiteiD id del trámite
     * @param usuario usuario que desaea valida l a geometría de los predios
     * @param tipoTramite necesario para validar si es una cancelacion en la que se validan las
     * unidades de construccion
     * @author fredy.wilches
     * @return true si no ocurre ningún error
     */
    /*
     * @modified pedro.garcia 03-12-2013 cambio de tipo de retorno del método
     */
    @Deprecated
    public boolean validarGeometriaPredios(String identificadoresPredios, Long tramiteId,
        UsuarioDTO usuario, String tipoTramite);

    /**
     * Método encargado de llamar la validación de la Geometría de unidades de construcción de forma
     * asincrónica. <br />
     *
     * @param identificadoresPredios numeros prediales de las unidades de construcción (separados
     * por coma)
     * @param tramiteiD id del trámite
     * @param usuario usuario que desaea valida l a geometría de los predios
     * @author pedro.garcia
     * @return true si no ocurre ningún error
     */
    @Deprecated
    public boolean validarGeometriaUnidadesConstruccionA(String identificadoresPredios,
        Long tramiteId, UsuarioDTO usuario);

}
