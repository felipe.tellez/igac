/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import java.util.List;
import javax.ejb.Local;

/**
 * interface para el DAO de CirculoRegistral
 *
 * @author pedro.garcia
 */
@Local
public interface ICirculoRegistralDAO extends IGenericJpaDAO<CirculoRegistral, String> {

    /**
     * retorna la lista de círculos registrales de un municipio
     *
     * @author pedro.garcia
     * @param idMunicipio
     * @return
     */
    public List<CirculoRegistral> findByMunicipio(String idMunicipio);

    /**
     * retorna el círculo registral con un código
     *
     * @author christian.rodriguez
     * @param codigo codigo del circulo registral
     * @return
     */
    public CirculoRegistral findByCodigo(String codigo);

    /**
     * Retorna la lista de círculos registrales de un departamento
     *
     * @author fredy.wilches
     * @param idDepartamento
     * @return
     */
    public List<CirculoRegistral> findByDepartamento(String idDepartamento);

}
