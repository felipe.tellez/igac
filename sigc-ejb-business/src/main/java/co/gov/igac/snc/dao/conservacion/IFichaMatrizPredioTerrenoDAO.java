/*
 * Proyecto SNC 2016
 */
package co.gov.igac.snc.dao.conservacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredioTerreno;
import javax.ejb.Local;

/**
 * Interfaz para las operaciones de base de datos de la clase FichaMatrizPredioTerreno
 *
 * @author felipe.cadena
 */
@Local
public interface IFichaMatrizPredioTerrenoDAO extends IGenericJpaDAO<FichaMatrizPredioTerreno, Long> {

}
