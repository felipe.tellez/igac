package co.gov.igac.snc.dao.sig;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.snc.dao.sig.vo.ArcGisServerFileResultValue;
import co.gov.igac.snc.dao.sig.vo.ArcGisServerResultValue;
import co.gov.igac.snc.dao.sig.vo.ArcgisServerResponse;
import co.gov.igac.snc.util.exceptions.RemoteServiceException;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 *
 * @author juan.mendez
 *
 */
public abstract class AbstractArcGisServerClient {

    static final Logger LOGGER = LoggerFactory.getLogger(AbstractArcGisServerClient.class);

    protected String url;
    protected String environment;
    protected int pollingTime;
    protected ArcgisServerTokenDTO token;

    /**
     *
     * @return
     */
    protected Client createClient() {
        DefaultClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);

        Client client = Client.create(clientConfig);

        client.addFilter(new ClientFilter() {
            @Override
            public ClientResponse handle(ClientRequest request) throws ClientHandlerException {
                // LOGGER.debug("****************************************");
                if (token.getCookies() != null) {
                    // LOGGER.debug("Adicionando cookies al siguiente request:"+token.getCookies());
                    request.getHeaders().put("Cookie", token.getCookies());
                }
                ClientResponse response = getNext().handle(request);
                // LOGGER.debug("Headers:"+response.getHeaders());

                List<NewCookie> responseCookies = response.getCookies();
                if (responseCookies != null && responseCookies.size() > 0) {
                    token.getCookies().addAll(response.getCookies());
                    // LOGGER.debug("Response Cookies : "+response.getCookies());
                }
                // LOGGER.debug("****************************************");

                return response;
            }
        });
        return client;

    }

    /**
     * Consulta el estado de un Job hasta que finaliza su ejecución. Hace polling al servidor
     * remoto.
     *
     * @author juan.mendez
     *
     * @param serverResponse objeto con la respuesta de arcgis server
     * @param taskUrl url del servicio remoto
     * @return ArcgisServerResponse objeto con la respuesta de arcgis server
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     * @throws InterruptedException
     * @throws RemoteServiceException
     */
    protected ArcgisServerResponse checkJobStatus(ArcgisServerResponse serverResponse,
        String taskUrl) throws RemoteServiceException {
        // LOGGER.debug("checkJobStatus");
        ArcgisServerResponse job = getCurrentJobStatus(serverResponse, taskUrl);
        while (job.isJobRunning()) {
            try {
                Thread.sleep(pollingTime);
            } catch (InterruptedException e) {
                throw new RemoteServiceException(e.getMessage(), e);
            }
            job = getCurrentJobStatus(serverResponse, taskUrl);
            job.validateJobError();
        }
        return job;
    }

    /**
     * Consulta el servicio REST remoto encargado de entregar el estado actual de un job Utiliza el
     * api jersey para la ejecucion de los servicios rest
     *
     * @param serverResponse objeto con la respuesta de arcgis server
     * @param taskUrl url del servicio remoto
     * @return ArcgisServerResponse objeto con la respuesta de arcgis server
     * @throws RemoteServiceException
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    private ArcgisServerResponse getCurrentJobStatus(ArcgisServerResponse serverResponse,
        String taskUrl) throws RemoteServiceException {
        ArcgisServerResponse jobStatus = null;
        try {
            String checkJobUrl = taskUrl + "jobs/";
            Client client = createClient();
            WebResource resource = client.resource(checkJobUrl);
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("returnMessages", "true");
            params.add("f", "pjson");
            String response = resource.path(serverResponse.getJobId()).queryParams(params).get(
                String.class);
            // LOGGER.debug("response:" + response);

            ObjectMapper mapper = new ObjectMapper();
            jobStatus = mapper.readValue(response, ArcgisServerResponse.class);
            LOGGER.debug("Operation : getCurrentJobStatus , Server : " + this.token.getCookies() +
                "  , JobID : " + serverResponse.getJobId() + " - jobStatus:" + jobStatus.
                getJobStatus());
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return jobStatus;
    }

    /**
     * Obtiene el resultado de la ejecución de un Job Realiza parsing del objeto retornado por
     * Arcgis Server
     *
     * @param serverResponse objeto con la respuesta de arcgis server
     * @param taskUrl url del servicio remoto
     * @return String con el resultado de la ejecucion del proceso
     * @throws RemoteServiceException
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    protected String getJobResult(ArcgisServerResponse serverResult, String taskUrl) throws
        RemoteServiceException {
        String resultValueStr = null;
        LOGGER.debug("getJobResult");
        try {
            String checkJobUrl = taskUrl + "jobs/";
            Client client = createClient();
            WebResource resource = client.resource(checkJobUrl);
            resource.accept(MediaType.APPLICATION_JSON_TYPE);

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            params.add("token", token.getToken());
            params.add("returnMessages", "true");
            params.add("f", "pjson");

            String paramUrl = serverResult.getResults().getResultado().getParamUrl();

            String response = resource.path(serverResult.getJobId()).path(paramUrl).queryParams(
                params).get(String.class);
            // LOGGER.debug("response:" +response);
            if (response.indexOf("GPDataFile") > 0) {
                ObjectMapper mapper = new ObjectMapper();
                ArcGisServerFileResultValue resultValue = mapper.readValue(response,
                    ArcGisServerFileResultValue.class);
                resultValueStr = resultValue.getUrl();
            } else {
                ObjectMapper mapper = new ObjectMapper();
                ArcGisServerResultValue resultValue = mapper.readValue(response,
                    ArcGisServerResultValue.class);
                resultValueStr = resultValue.getValue();
            }

            // LOGGER.debug("resultValueStr:" + resultValueStr);
            if (resultValueStr.startsWith(SncBusinessServiceExceptions.ERROR_SNC_GIS_PREFIX)) {
                LOGGER.error("Ocurrió un error de validación de reglas de negocio: " +
                    resultValueStr);
                throw SncBusinessServiceExceptions.EXCEPCION_100015.getExcepcion(LOGGER, null,
                    resultValueStr);
            }
        } catch (JsonParseException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        } catch (JsonMappingException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        } catch (IOException e) {
            throw new RemoteServiceException(e.getMessage(), e);
        }
        return resultValueStr;
    }

}
