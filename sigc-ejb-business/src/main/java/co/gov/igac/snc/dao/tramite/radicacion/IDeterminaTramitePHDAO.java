/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.tramite.radicacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.DeterminaTramitePH;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla DATO_RECTIFICAR
 *
 * @author felipe.cadena
 */
@Local
public interface IDeterminaTramitePHDAO extends IGenericJpaDAO<DeterminaTramitePH, Long> {

    /**
     * Obtiene una lista de {@link DeterminaTramitePH} basado en los criterios de busqueda
     * especificados, si el valor de alguna es null no se incluira como parametro de la consulta
     *
     * @author felipe.cadena
     *
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @param condicionPropiedad
     * @return
     */
    public List<DeterminaTramitePH> obtenerPorParametrosTramite(String tipoTramite,
        String claseMutacion, String subtipo, String condicionPropiedad);

}
