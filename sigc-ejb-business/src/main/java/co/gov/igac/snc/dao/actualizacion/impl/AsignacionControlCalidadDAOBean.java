package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IAsignacionControlCalidadDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.AsignacionControlCalidad;

/**
 * Implementación de los servicios de persistencia del objeto AsignacionControlCalidad.
 *
 * @author javier.barajas
 */
@Stateless
public class AsignacionControlCalidadDAOBean extends
    GenericDAOWithJPA<AsignacionControlCalidad, Long> implements IAsignacionControlCalidadDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AsignacionControlCalidadDAOBean.class);

}
