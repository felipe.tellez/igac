/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import java.util.List;
import javax.ejb.Local;

/**
 * Interfaz para los métodos de base de datos de la tabla DATO_RECTIFICAR
 *
 * @author pedro.garcia
 */
@Local
public interface IDatoRectificarDAO extends IGenericJpaDAO<DatoRectificar, Long> {

    /**
     * retorna los que tengan el tipo dado
     *
     * @author pedro.garcia
     * @param tipoDatoRectifCompl
     * @return
     */
    public List<DatoRectificar> findByTipo(String tipoDatoRectifCompl);

    /**
     * retorna la lista de DatosRectificar según el tipo y la naturaleza
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tipoDatoRectifCompl
     * @param naturaleza
     * @return
     */
    public List<DatoRectificar> findByTipoAndNaturaleza(
        String tipoDatoRectifCompl, String naturaleza);

}
