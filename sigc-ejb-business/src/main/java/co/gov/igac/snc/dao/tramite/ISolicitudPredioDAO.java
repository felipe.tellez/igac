package co.gov.igac.snc.dao.tramite;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;

/**
 *
 * Interfaz para los métodos de bd de la tabla SolicitudPredio
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface ISolicitudPredioDAO extends IGenericJpaDAO<SolicitudPredio, Long> {

    /**
     * Busca los predios asociados a una solicitud especifica
     *
     * @author christian.rodriguez
     * @param idSolicitud id de la solicitud
     * @return lista de los predios asociados a la solicitud con el contacto, departamento y
     * municipio cargados
     */
    public List<SolicitudPredio> buscarSolicitudPredioPorIdSolicitud(Long idSolicitud);

    /**
     * Busca los predios asociados a una solicitud pero que no estén asociados a ningún avalúo de la
     * solicitud y que tengan un contacto asociadoa
     *
     * @author christian.rodriguez
     * @param idSolicitud id de la solicitud
     * @return lista de los predios asociados a la solicitud con el contacto, departamento y
     * municipio cargados
     */
    public List<SolicitudPredio> buscarSolicitudPredioSinAvaluoConContactoPorSolicitudId(
        Long idSolicitud);

    /**
     * Busca la el preido de la solicitud que tenga el id del predio y del avalúo especificados
     *
     * @author christian.rodriguez
     * @param idPredio id del predio asociado
     * @param idSolicitud id de la solicitud asociada al avalúo
     * @param idAvaluo id del avaluo asociado
     * @return predio que corresponda a los parametros especificados
     */
    public SolicitudPredio buscarSolicitudPredioPorAvaluoIdSolicitudIdPredioId(Long idPredio,
        Long idSolicitud, Long idAvaluo);
}
