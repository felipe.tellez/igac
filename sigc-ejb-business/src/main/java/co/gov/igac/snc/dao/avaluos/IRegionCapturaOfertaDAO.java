/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.util.FuncionarioRecolector;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author pedro.garcia
 */
@Local
public interface IRegionCapturaOfertaDAO extends IGenericJpaDAO<RegionCapturaOferta, Long> {

    /**
     * Busca las regiones no comisionadas que tengan los ids indicados.
     * <b>NOTA:</b>Este método puede presentar problemas cuando se mandan a buscar muchas regiones
     * (se estima en 1000)
     *
     * @author pedro.garcia
     * @param regionesCapturaIds ids de las regiones a buscar
     * @param estados estados de las comisiones que se desean traer
     * @return
     */
    public List<RegionCapturaOferta> buscarRegionesPorIdsYEstados(List<Long> regionesCapturaIds,
        List<String> estados);

    /**
     * Obtiene la lista de regiones captura oferta asociadas a una comisión de ofertas
     *
     * @author pedro.garcia
     * @param comisionId id de la comisión
     * @return
     */
    public List<RegionCapturaOferta> buscarPorIdComision(Long comisionId);

    /**
     * Método para consultar si los recolectores disponibles tienen alguna asignación y se
     * encuentran recolectando
     *
     * @author rodrigo.hernandez
     * @param listaRecolectores Lista con ids y nombres de los recolectores
     * @param idAreaRecoleccion id del área de recolección asociada a los recolectores
     * @return lista de recolectores
     */
    public List<FuncionarioRecolector> getInformacionRecolectores(
        List<String> listaRecolectores, Long idAreaRecoleccion);

    /**
     * Método para consultar el id de la última región guardada en la tabla
     *
     * @author rodrigo.hernandez
     *
     * @return Id de la ultima region guardada en la tabla
     */
    public Long getIdUltimaRegionRegistrada();

    /**
     * Método para consultar un RegionCapturaOferta por id
     *
     * @param regionCapturaOfertaId - Id de la región
     *
     * @author rodrigo.hernandez
     *
     * @return RegionCapturaOferta con los detalles asociados
     */
    public RegionCapturaOferta getRegionCapturaOfertaPorId(Long regionCapturaOfertaId);

    /**
     * Metodo para cargar las regiones de una área asignada asociadas a un recolector
     *
     * @param idAreaAsignada
     * @param usuario
     * @return
     */
    public List<RegionCapturaOferta> cargarRegionCapturaOfertaPorIdAreaYRecolector(
        Long idAreaAsignada, String usuario);

    /**
     * Metodo para cargar las regiones de una área asignada
     *
     * @author christian.rodriguez
     * @param idAreaAsignada id del áre captura oferta
     * @return lista de las regiones asociadas al área de captura
     */
    public List<RegionCapturaOferta> cargarRegionCapturaOfertaPorIdArea(Long idAreaAsignada);

    /**
     * Busca las regiones que estén asociadas a una comisión de ofertas. OJO: NO hace fetch de nada
     * (y no debe modificarse)
     *
     * @author pedro.garcia
     * @version 2.0
     * @param idComision
     * @return
     */
    public List<RegionCapturaOferta> getByComisionIdNF(Long idComision);

    /**
     * Busca las regiones asociadas a un AreaCapturaOferta y un recolector
     *
     * @author rodrigo.hernandez
     *
     * @param areaCapturaOfertaId
     * @return
     */
    public List<RegionCapturaOferta> getRegionesCapturaOfertaPorAreaCapturaOfertaYRecolector(
        Long areaCapturaOfertaId, String recolectorLogin);

//end of interface
}
