package co.gov.igac.snc.dao.generales;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import java.util.List;

@Local
public interface IParametroDAO extends IGenericJpaDAO<Parametro, Long> {

    public Parametro getParametroPorNombre(String nombre);

    /**
     * Obtiene los parametros a partir de un prefijo en el nombre.
     *
     * @param prefijo cadena con la que inicia el nombre de los parametros requeridos
     * @return lista de parametros asociados a ese prefijo
     *
     * @author andres.eslava
     */
    public List<Parametro> getParametroPorPrefijo(String prefijo);
}
