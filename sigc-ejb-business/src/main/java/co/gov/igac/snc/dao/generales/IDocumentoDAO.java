package co.gov.igac.snc.dao.generales;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.generales.dto.DocumentoMemorandoDTO;
import co.gov.igac.generales.dto.DocumentoNombreMimeDTO;
import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

@Local
public interface IDocumentoDAO extends IGenericJpaDAO<Documento, Long> {

    /**
     * Método que almacena un documento en la BD y el gestor documental
     *
     * @author juan.agudelo
     * @param usuario
     * @param documentoTramiteDTO
     *
     * @return
     */
    public Documento guardarDocumentoGestorDocumental(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que almacena una foto asociada a un Predio en la BD y el gestor documental
     *
     * @author juan.agudelo
     * @param usuario
     * @param documentoTramiteDTO
     *
     * @return
     */
    public Documento guardarFotoPredioGestorDocumental(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que recupera el nombre definitivo de un documento almacenado en alfresco
     *
     * @author juan.agudelo
     * @param rutaDocumento ruta temporal del documento generada desde el servicio temporal de
     * documentos en la capa web
     * @param idObjetoAsociado id definitivo del objeto al que se encuentra asociado el documento
     * puede ser el id Trámite o Predio
     * @param tipoDocId id del tipo de documneto
     * @param documentoId id del documento
     * @return
     */
    @Deprecated
    public String nombreFinalDocumento(String rutaDocumento,
        String idObjetoAsociado, String tipoDocId, String documentoId);

    /**
     * Método que permite actualizar un documento y subirlo en alfresco
     *
     * @author juan.agudelo
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     */
    public boolean actualizarDocumentoYGestorDocumental(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que busca un Documento en Alfresco por su id en Alfresco
     *
     * @param idRepositorioDocumentos
     * @return
     */
//	public DocumentoNombreMimeDTO buscarDocumentoAlfresco(
//			String idRepositorioDocumentos);
    /**
     * Método que actualiza un {@link Documento} y sube el archivo a Alfresco
     *
     * @author david.cifuentes
     *
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     * @return
     */
    @Deprecated
    public Documento actualizarDocumentoYSubirArchivoAlfresco(
        UsuarioDTO usuario, DocumentoTramiteDTO documentoTramiteDTO,
        Documento documento);

    /**
     * Método que almacena un documento en la base de datos y en alfresco, debe contener por lo
     * menos un trámite o un predio asociados, un objeto del tipo tipoDocumento, el nombre del
     * archivo, el estado del documento y el identificador de si se encuentra bloqueado
     *
     * @author juan.agudelo
     * @param usuario
     * @param documento
     * @return
     */
    public Documento guardarDocumento(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que permite buscar un documento de prueba radicado en correspondencia
     *
     * @author juan.agudelo
     * @param ejecutarSP
     * @return
     */
    public Documento parseDocumento(Object[] ejecutarSP);

    /**
     * Método que mueve un archivo identificado con el id dado a la carpeta de archivos temporales
     * Retorna la URL de la carpeta temporal donde se descargó el archivo
     *
     *
     * @author david.cifuentes
     * @param idAlfresco
     * @return
     */
    /*
     * @modified juan.agudelo @modified pedro.garcia documentación, cambio de nombre (antes:
     * consultarURLDeOficioEnAlfresco) 21-06-2013 Se renombra de nuevo (antes:
     * descargarOficioDeGestorDocumentalATempLocalMaquina) para dejar definitivamente en claro que
     * mueve el archivo a la carpeta temporal de la máquina donde de ejecuta la aplicación
     */
    public String descargarOficioDeGestorDocumentalATempLocalMaquina(String idAlfresco);

    /**
     * Método que retorna una lista de {@link Documento} con las resoluciones asociadas a un predio
     *
     * @author david.cifuentes
     *
     * @param numeroPredial
     * @return
     */
    public List<Documento> buscarResolucionesByNumeroPredialPredio(String numeroPredial);

    /**
     * Obtiene la ruta en la máquina local de un documentoResultado que está en alfresco y se
     * descarga, buscando por el id del trámite
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public String buscarDocumentoResultadoPorTramiteId(Long tramiteId);

    /**
     * Método que permite recuperar las imagenes inicial y final del procesamiento geográfico del
     * trámite, dondel el primer campo corresponde a la ruta del repositorio documental para la
     * imagen inicial del predio y el segundo campo a la imagen final
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public String[] recuperarRutaGestorDocumentalImagenInicialYfinalDelTramite(
        Long tramiteId);

    /**
     * Método que almacena un documento en la base de datos y en alfresco, debe contener por lo
     * menos un trámite o un predio asociados (el id de alguno de ellos), un objeto del tipo
     * tipoDocumento, el nombre del archivo, el estado del documento y el identificador de si se
     * encuentra bloqueado
     *
     * Si el documento contiene un trámite o un trámite y un predio vinculados al documento este se
     * asociara al tramite, si contiene solo contiene un predio vinculado el documento se asociara
     * al predio, si no contiene ni trámite ni predio vinculados el documento no se cargara
     *
     * @author juan.agudelo
     * @param usuario
     * @param documento
     * @return
     */
    public Documento clasificarAlmacenamientoDocumentoGestorDocumental(UsuarioDTO usuario,
        Documento documento);

    /**
     * Se sobrecarga el metodo public Documento almacenarDocumentoAlfresco(UsuarioDTO usuario,
     * Documento documento);
     *
     * @param documento
     * @param usuario
     * @return
     */
    public Documento almacenarDocumentoFotoEnGestorDocumental(Documento documento,
        UsuarioDTO usuario,
        String rutaLocal);

    /**
     * Método que almacena un memorando de conservación en la BD y en el gestor documental
     *
     * @author javier.aponte
     * @param documentoMemorandoDTO
     * @param documento
     * @return
     */
    public Documento guardarMemorandoConservacionGestorDocumental(
        DocumentoMemorandoDTO documentoMemorandoDTO,
        Documento documento);

    /**
     * Método encargado de almacenar en Alfresco los documentos de soporte de bloqueos de predios y
     * personas
     *
     * @author juan.agudelo
     * @param usuario
     * @param documento
     * @param personas
     * @param predios
     * @return
     */
    public Map<String, String> almacenarDocumentosBloqueoGestorDocumental(UsuarioDTO usuario,
        Documento documento, List<Persona> personas, List<Predio> predios);

    /**
     * Método que almacena un documento en la BD y el gestor documental cuando el
     * documentoTramiteDTO enviado no tiene un numero predial adjunto.
     *
     * @author david.cifuentes
     * @param usuario
     * @param documentoTramiteDTO
     * @return
     *
     * @note: Usar éste método cuando el DocumentoTramiteDTO no lleva número predial, esto puede
     * suceder cuando el trámite es de mutación de quinta o cuando viene de una solicitud de vía
     * gubernativa.
     *
     * Éste es un método copiado del método de juan.agudelo guardarDocumentoGestorDocumental, pero
     * difiere en que hace un llamado a otro método del servicio Documental.
     *
     */
    public Documento guardarDocumentoGestorDocumentalSinNumeroPredial(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que permite actualizar un {@link Documento} y subirlo en alfresco revisando si tiene o
     * no número predial.
     *
     * Éste método es una copia del método actualizarDocumentoYGestorDocumental con las validaciones
     * adicionales para verificar si se tiene o no un número predial.
     *
     * @author david.cifuentes
     *
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     * @param tramite
     */
    public boolean actualizarDocumentoYGestorDocumentalConValidacionNumeroPredial(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento, Tramite tramite);

    /**
     * Método que almacena un {@link Documento} en la base de datos y en alfresco, debe contener por
     * lo menos un {@link Tramite} o un {@link Predio} asociados (el id de alguno de ellos), un
     * objeto del tipo {@link TipoDocumento}, el nombre del archivo, el estado del documento y el
     * identificador de si se encuentra bloqueado.
     *
     * Si el documento contiene un trámite o un trámite y un predio vinculados al documento este se
     * asociara al tramite, si contiene solo contiene un predio vinculado el documento se asociara
     * al predio, si no contiene ni trámite ni predio vinculados el documento no se cargará.
     *
     * Éste método es una copia del método almacenarDocumentoAlfresco ajustado para documentos que
     * no tienen asociados un predio.
     *
     * @author david.cifuentes
     * @param usuario
     * @param documento
     * @return
     */
    public Documento almacenarDocumentoGestorDocumentalSinPredio(UsuarioDTO usuario,
        Documento documento);

    /**
     * Metodo que retorna las imagenes antes y despues del tramite
     *
     * @author fredy.wilches
     * @param t
     * @return
     */
    public List<Documento> buscarImagenesTramite(Tramite t);

    /**
     * Método para guardar un documento en alfresco asociado a una solicitud de avaluo comercial
     * debe existir una solicitud asociada al documento.
     *
     * @param usuario
     * @param documentoSolicitudDTO
     * @param documento
     * @return
     */
    public Documento guardarDocumentosSolicitudAvaluoAlfresco(
        UsuarioDTO usuario, DocumentoSolicitudDTO documentoSolicitudDTO,
        Documento documento);

    /**
     * Método que busca si existe una imagen despues de la edicion geografica y si es asi la
     * retorna.
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public Documento getImagenAfterEdicionByTramiteId(Long tramiteId);

    /**
     * Retorna el id en el repositorio de documentos del documento de tipo idTipoDocumento para el
     * trámite con id tramiteId. <br />
     * Por el nombre dado al método, se usa para obtener documentos que sean réplicas de un trámite.
     *
     * @author lorena.salamanca
     * @param tramiteId, idTipoDocumento
     * @return cadena vacía si no existe tal documento
     */
    /*
     * @documented pedro.garcia
     */
    public String obtenerIdRepositorioDocumentosDeReplicaOriginal(Long tramiteId,
        Long idTipoDocumento);

    /**
     * Método que busca un documento por su id de tramite, si el documento no existe se devuelve un
     * objeto nulo
     *
     * @author lorena.salamanca
     * @param tramiteId, idTipoDocumento
     * @return
     * @modified by javier.aponte
     */
    /*
     * Se realiza un cambio para que cuando no encuentré el documento devuelva un objeto nulo y no
     * un objeto de tipo Documento con todos los campos nulos @author javier.aponte
     */
    public Documento buscarDocumentoPorTramiteIdyTipoDocumento(Long tramiteId, Long idTipoDocumento);

    /**
     * Valida si existe una imagen inicial en el tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public boolean existsImagenTramiteInicialByTramiteId(Long tramiteId);

    /**
     * Método que almacena una ficha predial digital generada en Alfresco
     *
     * @param documento
     * @param usuario
     * @author javier.aponte
     *
     */
    public Documento almacenarFichaPredialDigitalGestorDocumental(Documento documento,
        UsuarioDTO usuario, String rutaLocal);

    /**
     * Método que almacena un certificado plano predial catastral generado en Alfresco
     *
     * @param documento
     * @param usuario
     * @author javier.aponte
     *
     */
    public Documento almacenarCertificadoPlanoPredialCatastral(Documento documento,
        UsuarioDTO usuario, String rutaLocal);

    /**
     * Método que borra un documento de la base de datos y en alfresco si es posible
     *
     * @author franz.gamba
     * @param documento
     * @return
     */
    public boolean deleteDocumentoAlsoGestorDocumental(Documento documento);

    /**
     * Método para borrar archivo de alfresco utilizando el uuid asociado al Documento
     *
     * @param documento
     * @return retorna verdadero si se realiza el borrado con exito, retorna falso si el Documento
     * no tienen uuid de alfresco asociado.
     */
    public boolean borrarDocumentoGestorDocumental(Documento documento);

    /**
     * método que busca un formato por su nombre en alfresco
     *
     * @param nombre
     * @return
     */
    /*
     * @modified pedro.garcia 23-05-2013 Cambio de nombre (no se sube al ftp). No se devuelve la
     * ruta en la que quedó público el archivo porque en realidad no se usa.
     */
    //public String buscarFormato(String nombre);
    /**
     * método busca el ultimo documento relacionado a una solicitud de avaluos por fecha
     *
     * @author felipe.cadena
     * @param idSolicitud
     * @return
     */
    public Documento obtenerUltimoDocSolicitud(Long idSolicitud);

    /**
     * Método para buscar la lista de documentos relacionados con los TramiteDocumentacion de un
     * radicado/solicitud de Avaluo Comercial. </br>
     * <b>TENER EN CUENTA:</b></br>
     * Solo trae un documento sin importar el número de TramiteDocumentacions con los que se
     * encuentre asociado
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitud
     *
     * @return
     */
    public List<Documento> buscarDocsDeTramiteDocumentacionsPorSolicitudId(
        Long idSolicitud);

    /**
     * Método que busca un documento por el predioId asociado y el id del tipo de documento
     *
     * @param predioId
     * @param idTipoDocumento
     * @return Documento
     * @author javier.aponte
     */
    public Documento buscarDocumentoPorPredioIdAndTipoDocumento(Long predioId, Long idTipoDocumento);

    /**
     * Método que almacena un documento del proceso de actualizacion en alfresco y actualiza en la
     * base de datos el documento una vez se haya establecido el id de repositorio de documentos
     *
     * @param documento
     * @param actualizacion
     * @param usuario
     * @author javier.aponte
     */
    public Documento almacenarDocumentoMemoriaTecnicaActualizacion(Documento documento,
        Actualizacion actualizacion, UsuarioDTO usuario);

    /**
     * Método que busca un documento por su numero de documento.
     *
     * @param numResolucion
     * @return
     */
    public Documento buscarDocumentoPorNumeroDocumento(String numResolucion);

    /**
     * Consulta los documentos asociados al numero y al tipo dados en el parametro
     *
     * @author felipe.cadena
     * @param numero
     * @param tipoDocumento
     * @return
     */
    public List<Documento> buscarPorNumeroDocumentoYTipo(String numero, Long tipoDocumentoId);
    
    /**
     * @author hector.arias
     * @param tipoDocumentoId
     * @return 
     */
    public List<Documento> buscarDocumentoPorTipoActualizacion(String numeroDocumento, Long tipoDocumentoId);
    
    /**
     * @author hector.arias
     * @param numero
     * @param tipoDocumentoId
     * @return 
     */
    public Documento buscarPorNumeroUltimoDocumentoYTipo(String numero, Long tipoDocumentoId);

    /**
     * Método que actualiza un documento en la base de datos
     *
     * @author juanfelipe.garcia
     * @param documento
     * @return
     */
    public boolean actualizarDocumentoSinGestorDocumental(Documento documento);

    /**
     * Método para buscar un documento por su id con toda su información asociada
     *
     * @author juanfelipe.garcia
     * @param documento
     * @return
     */
    public Documento buscarDocumentoCompletoPorId(Long documentoId);

    /**
     * Método que busca un documento por su numero predial asociado y el id del tipo de documento
     *
     * @param numeroPredial
     * @param idTipoDocumento
     * @return Documento
     * @author javier.aponte
     */
    public Documento buscarDocumentoPorNumeroPredialAndTipoDocumento(String numeroPredial,
        Long idTipoDocumento);

    /**
     * Método que busca una lista de documentos por el id de la solicitud
     *
     * @param idSolicitud
     * @return List<Documento>
     * @author javier.aponte
     */
    public List<Documento> buscarDocumentosPorSolicitudId(Long idSolicitud);

    /**
     * Método que busca una lista de documentos por el id de la solicitud
     *
     * @param idSolicitud
     * @return List<Documento>
     * @author javier.aponte
     */
    public List<Documento> buscarDocumentacionPorSolicitudId(Long idSolicitud);

    /**
     * Método que dada una lista de Id's de {@link Tramite} verifica los documentos asociados para
     * cada uno de ellos y si existe o no un documento generado de tipo COPIA_BD_GEOGRAFICA_ORIGINAL
     * (97) o COPIA_BD_GEOGRAFICA_FINAL (98) para estos.
     *
     * @author david.cifuentes
     * @param tramiteIds
     * @return
     */
    public List<Long> verificarTramitesCopiaDBDepuracion(
        List<Long> tramiteIds);

    /**
     * Método encargado de borrar un tràmite documento y el documento por tramite id y tipo de
     * documento id, en la base de datos y en el gestor documental, en caso que el borrado sea
     * exitoso retorna una variable booleana con el valor verdadero
     *
     * @author leidy.gonzalez
     * @param usuario
     * @param productoCatastralDetalle
     * @return
     */
    public Documento guardarDocumentoProductosCatastrales(UsuarioDTO usuario,
        ProductoCatastralDetalle productoCatastralDetalle, Object[] numeracionCertificado);

    /**
     * Método encargado de borrar un tràmite documento y el documento por tramite id y tipo de
     * documento id, en la base de datos y en el gestor documental, en caso que el borrado sea
     * exitoso retorna una variable booleana con el valor verdadero
     *
     * @author leidy.gonzalez
     * @param usuario
     * @param productoCatastralDetalle
     * @return
     */
    public List<Documento> buscarDocumentosFTP();

    /**
     * Metodo para validar si un documento determinado se encuentra almacenado en el gestor
     * documental
     *
     * @author felipe.cadena
     * @param documento
     * @return
     */
    public boolean validarExistenciaGestorDocumental(Documento documento);

    /**
     * Metodo que permite consultar el documento de firma escaneada por el id
     *
     * @author leidy.gonzalez
     * @param idDocumento
     * @return
     */
    public Documento buscarFirmaUsuario(Long idDocumento);

    /**
     * Metodo para busca un documento por el numero documento y tipo de documento
     *
     * @author leidy.gonzalez :: #12528::26/05/2015::CU_187
     * @param numeroDocumento
     * @return
     */
    public Documento buscarDocumentoPorNumeroDocumentoAndTipoDocumento(String numeroDocumento);

    /**
     * Metodo que permite consultar el documento por el id de la solicitud y que sea generado dada
     * cierta canditad de documentos
     *
     * @author leidy.gonzalez
     * @param idDocumento
     * @return
     */
    public List<Documento> buscarDocumentosPorSolicitudIdSinRepositorio(Long idSolicitud,
        int cantidadDocumentos);

    /**
     * Retorna los documentos con la unidad Multiproyectada a la que se encuentra asociada en el
     * campo pUnidadConstruccion
     *
     * @author felipe.cadena
     *
     * @param idSolicitud
     * @param cantidadDocumentos
     * @return
     */
    public List<Documento> obtenerMUnidadesConstruccionDocumento(List<Documento> documentos);

    /**
     * Metodo que permite consultar el documento por el id de la solicitud y que sea generado dada
     * cierta canditad de documentos y que el tipo de documento sean fotos
     *
     * @author leidy.gonzalez
     * @param idDocumento
     * @return
     */
    public List<Documento> buscarDocumentosPorSolicitudIdSinRepositorioFotos(Long idSolicitud,
        int cantidadDocumentos);

    /**
     * Metodo que permite contar la cantidad de documentos que han sido cargados
     *
     * @author leidy.gonzalez
     * @param idSolicitud
     * @return
     */
    public Integer contarDocumentosPorSolicitudIdSinRepositorio(Long idSolicitud);

    /**
     * Metodo que permite contar la cantidad de documentos tipo fotos que han sido cargados
     *
     * @author leidy.gonzalez
     * @param idSolicitud
     * @return
     */
    public Integer contarDocumentosPorSolicitudIdSinRepositorioFotos(Long idSolicitud);

    /**
     * Método que retorna los números prediales que no tienen ficha predial entre los ids de predios
     * que se pasan como parámetros
     *
     * @author javier.aponte
     * @param idPredios
     * @return numerosPrediales correspondientes a los predios que no tienen ficha predial dígital
     */
    public String obtenerNumerosPredialesSinFichaPredialDigital(String numerosPrediales);

    /**
     * Metodo para cargar en el ftp los archivos asociados al proceso de actualizacion. Retorno el
     * directorio base del ftp donde se descomprimieron los archivos.
     *
     *
     * @author felipe.cadena
     * @param rutaZip
     * @param municipioActId
     * @param usuarioDTO
     * @param municipio
     * @return
     */
    public String guardarDocumentosActualizacionX(String rutaZip, String municipioActId,
        UsuarioDTO usuarioDTO, String municipio);

    public Documento buscarDocumentoPorTipoDocumentoYUsuario(String usuario, Long tipoDocumentoId);

    /**
     * Método que retorna los documentos asociados a una comision, asociados a un tramite.
     *
     * @author wilmanjose.vega
     * @param idTramite
     * @return Lista de documentos asociados a la comision del tramite respectivo.
     */
    List<Documento> obtenerDocumentosComisionPorTramiteId(final Long idTramite);

    /**
     * Método que busca un documento por su id de tramite, si el documento no existe se devuelve un
     * objeto nulo
     *
     * @author leidy.gonzalez
     * @param tramiteId, idTipoDocumentos
     * @return
     */
    public List<Documento> buscarDocumentosPorTramiteIdyTiposDocumento(Long tramiteId,
        List<Long> idTipoDocumentos);

}
