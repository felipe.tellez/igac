package co.gov.igac.snc.dao.conservacion;

/**
 * Clase para las consultas de la clase reporte control reporte
 *
 * @author javier.aponte
 */
import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionDetalle;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import java.util.List;

@Local
public interface IRepReporteEjecucionDetalleDAO extends
    IGenericJpaDAO<RepReporteEjecucionDetalle, Long> {

    List<RepReporteEjecucionDetalle> buscarReportePorFiltrosAvanzados(
        FiltroGenerarReportes datosConsultaPredio, String selectedMunicipioCod,
        Long idReporteEjecucion, RepConfigParametroReporte configuracionAvanzada);

}
