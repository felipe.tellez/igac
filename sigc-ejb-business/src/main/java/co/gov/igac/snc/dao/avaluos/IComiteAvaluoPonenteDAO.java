package co.gov.igac.snc.dao.avaluos;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoPonente;

/**
 *
 * Interfaz para los métodos de bd de la tabla COMITE_AVALUO_PONENTE
 *
 * @author christian.rodriguez
 *
 */
@Local
public interface IComiteAvaluoPonenteDAO extends IGenericJpaDAO<ComiteAvaluoPonente, Long> {

}
