package co.gov.igac.snc.dao.actualizacion.impl;

import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IActualizacionPerimetroUrbanoDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionPerimetroUrbano;

/**
 * @see IActualizacionPerimetroUrbanoDAO
 * @author andres.eslava
 *
 */
@Stateless
public class ActualizacionPerimetroUrbanoDAOBean extends GenericDAOWithJPA<ActualizacionPerimetroUrbano, Long>
    implements IActualizacionPerimetroUrbanoDAO {

}
