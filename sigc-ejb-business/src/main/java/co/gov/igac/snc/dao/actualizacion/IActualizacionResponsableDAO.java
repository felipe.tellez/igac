package co.gov.igac.snc.dao.actualizacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable;

/**
 * Servicios de persistencia para el objeto ActualizacionResponsable
 *
 * @author franz.gamba
 */
@Local
public interface IActualizacionResponsableDAO extends IGenericJpaDAO<ActualizacionResponsable, Long> {

}
