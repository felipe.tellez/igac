package co.gov.igac.snc.dao.actualizacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.GeneracionFormularioSbc;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;

/**
 * Servicios de persistencia para el objeto Actualizacion.
 *
 * @author jamir.avila.
 */
@Local
public interface IActualizacionDAO extends IGenericJpaDAO<Actualizacion, Long> {

    /**
     * Crea un nuevo registro de diagnóstico de municipio en actualización.
     *
     * @param actualizacion objeto de transporte con los datos a almacenar.
     * @throws ExcepcionSNC en caso de que se presente una situación anormal.
     * @author jamir.avila.
     */
    public Actualizacion crear(Actualizacion actualizacion) throws ExcepcionSNC;

    /**
     * Actualiza un objeto Actualizacion existente en el sistema.
     *
     * @param actualizacion objeto de transporte con los datos que se deben actualizar.
     * @throws ExcepcionSNC lanzada en caso de que se presente alguna situación anormal durante la
     * ejecución del método.
     * @author jamir.avila.
     */
    public void actualizar(Actualizacion actualizacion) throws ExcepcionSNC;

    /**
     * Recupera una actualización dado su identificador.
     *
     * @param idActualizacion identificador de la actualización a recuperar.
     * @return el objeto Actualizacion correspondiente o null en caso de que no haya
     * correspondencia.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal durante la
     * operación del método.
     * @author jamir.avila.
     */
    public Actualizacion recuperar(Long idActualizacion) throws ExcepcionSNC;

    /**
     * Método que busca una {@link Actualizacion} por su id, trayendo sus convenios y entidades.
     *
     * @author david.cifuentes
     * @param idActualizacion
     */
    public Actualizacion buscarActualizacionPorIdConConveniosYEntidades(Long idActualizacion) throws
        ExcepcionSNC;

    /**
     * Retorna el objeto actualizaicon con sus respectivos responsables
     *
     * @param actualizacionId
     * @return
     */
    public Actualizacion getActualizacionConResponsablesByActualizacionId(Long actualizacionId);

    /**
     * Método que devuelve la ultima actualización por el municipio y la zona
     *
     * @return
     * @author javier.aponte
     */
    public Actualizacion obtenerUltimaActualizacionPorMunicipioYZona(
        String codigoMunicipio, String zona);

    /**
     * Agrega un nuevo objeto ActualizacionEvento a la actualización actual.
     *
     * @param actualizacionEvento objeto de transporte con los datos de la actualizacion evento a
     * agregar. Se requiere que actualizacionEvento tenga la relación con la actualización.
     * @author javier.barajas
     */
    public ActualizacionEvento agregarActualizacionEvento(ActualizacionEvento actualizacionEvento);

    /**
     * Agrega un nuevo objeto GenerarFormularioSbc a la actualización actual.
     *
     * @param GeneracionFormularioSbc objeto de transporte con los datos de la Generacion Formulario
     * Sbc a agregar. Se requiere que Generacion Formulario Sbc tenga la relación con la
     * actualización.
     * @author javier.barajas
     */
    public GeneracionFormularioSbc agregarGenerarFormularioSbc(
        GeneracionFormularioSbc generarFormularioSbc);

    /**
     * Obtiene una lista de objetos ActualizacionLevantamienro de la actualización actual.
     *
     * @param Actualizacion Seleccionada, Id de levantamiento asignacion
     * @return ActualizacionLevantamienro objeto de transporte con los datos de la para mostrar las
     * manzanas o zonas a levantar. Se requiere que ActualizacionLevantamiento tenga la relación con
     * la actualización.
     * @author javier.barajas
     */
    public List<ActualizacionLevantamiento> obtenerActualizacionLevantamiento(
        Long idActualizacionSeleccionada, Long levantamientoId);

    /**
     * Consulta de topografos de levantamiento a asignacion
     *
     * @param Lista de ActualizacionLevantamiento
     * @return Lista de topografos encargados de la actualizacion y levantamiento de las manzanas
     * @cu 209 Exportar informacion de manzanas
     * @author javier.barajas
     */
    public List<LevantamientoAsignacion> obtenerTopografosActualizacion(
        List<ActualizacionLevantamiento> listaLevantamiento);

    /**
     * Obtiene la actualizacion con el municipio asociado
     *
     * @param actulizacionId identificador de la entidad actualización.
     * @author andres.eslava
     */
    public Actualizacion obtenerActualizacionConMunicipio(Long actualizacionId);

    /**
     * Consulta de manzanas asignadas a los topografos
     *
     * @param actualizacionId, recursoHumanoId
     * @return Lista de manzanas por topografos
     * @cu 212 Control de calidad a las manzanas
     * @author javier.barajas
     */
    public List<ActualizacionLevantamiento> obtenerManazanasporTopografosActualizacion(
        Long actualizacionId, Long levantamientoAsignacionId);

}
