package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IHFichaMatrizDAO;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatriz;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import org.hibernate.Hibernate;

@Stateless
public class HFichaMatrizDAOBean extends
    GenericDAOWithJPA<HFichaMatriz, Long> implements
    IHFichaMatrizDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(HFichaMatrizDAOBean.class);

    /**
     * @see IHFichaMatrizDAO#findByNumeroPredial(Long)
     * @author leidy.gonzalez
     */
    @Override
    public List<HFichaMatriz> findByNumeroPredial(String numPredial) {
        try {
            String sql = "SELECT fm FROM HFichaMatriz fm" +
                " LEFT JOIN FETCH fm.HFichaMatrizTorres" +
                " WHERE fm.HPredio.numeroPredial like :numPredial";
            Query query = this.entityManager.createQuery(sql);
            query.setParameter("numPredial", numPredial + "%");
            List<HFichaMatriz> pfm = query.getResultList();

            return pfm;
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * @see IHFichaMatrizDAO#buscarFichasMatrizporListadoprediosId(List<Long>)
     * @author juan.cruz
     * @param prediosId
     * @return
     */
    @Override
    public List<HFichaMatriz> buscarFichasMatrizporListadoprediosId(List<Long> prediosId) {
        try {
            String query = "SELECT hfm " +
                "FROM HFichaMatriz hfm " +
                "JOIN FETCH hfm.HPredio " +
                "WHERE hfm.HPredio.id in (:prediosId) ";

            Query q = this.entityManager.createQuery(query);
            q.setParameter("prediosId", prediosId);

            List<HFichaMatriz> resultado = q.getResultList();

            return resultado;

        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "IHFichaMatrizDAO#buscarFichasMatrizporListadoprediosId");
        }

    }

}
