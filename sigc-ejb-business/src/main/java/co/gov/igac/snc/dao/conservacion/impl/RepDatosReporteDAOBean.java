package co.gov.igac.snc.dao.conservacion.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IReporteDatosReporteDAO;
import co.gov.igac.snc.persistence.entity.conservacion.RepDatosReporte;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

@Stateless
public class RepDatosReporteDAOBean extends GenericDAOWithJPA<RepDatosReporte, Long> implements
    IReporteDatosReporteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepDatosReporteDAOBean.class);

    /**
     * @see IReporteDatosReporteDAO#contarRegistrosReporteDatosReportePorControlReporteId(Long
     * idReporteControlReporte)
     * @author javier.aponte
     */
    @Override
    public Integer contarRegistrosReporteDatosReportePorControlReporteId(
        Long idReporteControlReporte) {
        Integer answer = null;

        Query query;
        String queryToExecute;

        queryToExecute =
            "SELECT COUNT(rdr) FROM RepDatosReporte rdr where rdr.repControlReporte.id = :idReporteControlReporte";

        try {
            query = this.entityManager.createQuery(queryToExecute);
            query.setParameter("idReporteControlReporte", idReporteControlReporte);

            answer = (Integer) query.getSingleResult();

        } catch (NoResultException nrEx) {
            LOGGER.warn(
                "RepDatosReporteDAOBean#contarRegistrosReporteDatosReportePorControlReporteId: " +
                "La consulta de reporte datos reporte no devolvió resultados");
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100012.getExcepcion(
                LOGGER, e, e.getMessage());
        }

        return answer;
    }

}
