/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.impl;

import java.util.List;
import javax.ejb.Stateless;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.impl.SolicitanteDAOBean;
import co.gov.igac.snc.dao.vistas.IVComisionDAO;
import co.gov.igac.snc.dao.vistas.IVPropiedadTierraDAO;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VPropiedadTierra;
import co.gov.igac.snc.persistence.entity.vistas.VPropiedadTierraPK;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.vo.EstadisticasRegistroVO;

import java.util.ArrayList;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class VPropiedadTierraDAOBean extends GenericDAOWithJPA<VPropiedadTierra, VPropiedadTierraPK>
    implements IVPropiedadTierraDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPropiedadTierraDAOBean.class);

    @Override
    /**
     * @see IVPropiedadTierraDAOBean#getEstadisticas(String departamento,String municipio, String
     * zonaUnidadOrganica)
     * @author franz.gamba
     */
    public List<EstadisticasRegistroVO> getEstadisticas(String departamento,
        String municipio, String zonaUnidadOrganica) {

        LOGGER.debug("Entra en VPropiedadTierraDAOBean#getEstadisticas");

        StringBuilder query = new StringBuilder();

        query.append("SELECT vp.tipoPropiedad, " +
            "SUM(vp.propietarios), SUM(vp.areaTerrenoHectareas) " +
            "FROM VPropiedadTierra vp WHERE 1=1 ");

        if (departamento != null && !departamento.trim().equals("")) {
            query.append(" AND vp.departamentoCodigo =:departamento ");
        }

        if (municipio != null && !municipio.trim().equals("")) {
            query.append(" AND vp.municipioCodigo =:municipio ");
        }

        if (zonaUnidadOrganica != null && !zonaUnidadOrganica.trim().equals("")) {
            query.append(" AND vp.zonaUnidadOrganica =:zonaUnidadOrganica ");
        }

        query.append(" GROUP BY vp.tipoPropiedad");

        Query q = entityManager.createQuery(query.toString());

        if (departamento != null && !departamento.trim().equals("")) {
            q.setParameter("departamento", departamento);
        }

        if (municipio != null && !municipio.trim().equals("")) {
            q.setParameter("municipio", municipio);
        }

        if (zonaUnidadOrganica != null && !zonaUnidadOrganica.trim().equals("")) {
            q.setParameter("zonaUnidadOrganica", zonaUnidadOrganica);
        }

        try {
            return (List<EstadisticasRegistroVO>) q.getResultList();

        } catch (NoResultException nrEx) {
            LOGGER.error("VPropiedadTierraDAOBean#getEstadisticas: " +
                "La consulta de estadísticas no devolvió resultados");
            return null;
        } catch (Exception ex) {
            LOGGER.error("Error no determinado en la consulta de estadísticas: " +
                ex.getMessage());
        }
        return null;

    }

}
