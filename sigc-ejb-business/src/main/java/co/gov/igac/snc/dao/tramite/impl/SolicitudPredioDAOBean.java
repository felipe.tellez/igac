package co.gov.igac.snc.dao.tramite.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ISolicitudPredioDAO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * @see ISolicitudPredioDAO
 *
 * @author christian.rodriguez
 *
 */
@Stateless
public class SolicitudPredioDAOBean extends GenericDAOWithJPA<SolicitudPredio, Long> implements
    ISolicitudPredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudPredioDAOBean.class);

    /**
     * @see ISolicitudPredioDAO#buscarSolicitudPredioPorIdSolicitud(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<SolicitudPredio> buscarSolicitudPredioPorIdSolicitud(Long idSolicitud) {

        List<SolicitudPredio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT sp " +
            " FROM SolicitudPredio sp " +
            " JOIN FETCH sp.departamento" +
            " JOIN FETCH sp.municipio" +
            " LEFT JOIN FETCH sp.contacto" +
            " WHERE sp.solicitud.id = :idSolicitud ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);

            answer = (List<SolicitudPredio>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;
    }

    /**
     * @see ISolicitudPredioDAO#buscarSolicitudPredioSinAvaluoConContactoPorSolicitudId(Long)
     * @author christian.rodriguez
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<SolicitudPredio> buscarSolicitudPredioSinAvaluoConContactoPorSolicitudId(
        Long idSolicitud) {

        List<SolicitudPredio> answer = null;
        String queryString;
        Query query;

        queryString = "SELECT sp " +
            " FROM SolicitudPredio sp " +
            " JOIN FETCH sp.departamento " +
            " JOIN FETCH sp.municipio " +
            " LEFT JOIN FETCH sp.contacto " +
            " WHERE sp.solicitud.id = :idSolicitud " +
            " AND sp.avaluoPredioId IS NULL " +
            " AND sp.contacto IS NOT NULL";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idSolicitud", idSolicitud);

            answer = (List<SolicitudPredio>) query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;
    }

    /**
     * @see ISolicitudPredioDAO#buscarSolicitudPredioPorAvaluoIdSolicitudIdPredioId(Long, Long,
     * Long)
     * @author christian.rodriguez
     */
    @Override
    public SolicitudPredio buscarSolicitudPredioPorAvaluoIdSolicitudIdPredioId(Long idPredio,
        Long idSolicitud, Long idAvaluoPredio) {

        SolicitudPredio answer = null;
        String queryString;
        Query query;

        queryString = "SELECT sp " +
            " FROM SolicitudPredio sp " +
            " WHERE sp.predioId  = :idPredio " +
            " AND sp.solicitud.id  = :idSolicitud " +
            " AND sp.avaluoPredioId  = :idAvaluoPredio ";

        LOGGER.debug(queryString);

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idPredio", idPredio);
            query.setParameter("idSolicitud", idSolicitud);
            query.setParameter("idAvaluoPredio", idAvaluoPredio);

            answer = (SolicitudPredio) query.getSingleResult();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                e.getMessage());
        }

        return answer;
    }

}
