package co.gov.igac.snc.dao.generales.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.generales.IConfiguracionReporteDAO;
import co.gov.igac.snc.persistence.entity.generales.ConfiguracionReporte;

@Stateless
public class ConfiguracionReporteDAOBean extends GenericDAOWithJPA<ConfiguracionReporte, Long>
    implements IConfiguracionReporteDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConfiguracionReporteDAOBean.class);

    // --------------------------------------------------------------------------------------------------
    /**
     * @see IConfiguracionReporteDAO#obtenerConfiguracionReportePorId
     * @author javier.aponte
     */
    public ConfiguracionReporte obtenerConfiguracionReportePorId(Long configuracionReporteId) {
        LOGGER.debug("DocumentoDAOBean#obtenerConfiguracionReportePorId");

        Query q = entityManager
            .createQuery("SELECT cr" +
                " FROM ConfiguracionReporte cr" +
                " WHERE cr.id = :configuracionReporteId");
        q.setParameter("configuracionReporteId", configuracionReporteId);

        try {
            ConfiguracionReporte configuracionReporte = (ConfiguracionReporte) q.getSingleResult();
            return configuracionReporte;
        } catch (NoResultException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }

    }

}
