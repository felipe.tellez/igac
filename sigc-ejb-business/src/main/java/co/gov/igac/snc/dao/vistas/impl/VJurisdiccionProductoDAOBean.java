package co.gov.igac.snc.dao.vistas.impl;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.vistas.IVJurisdiccionProductoDAO;
import co.gov.igac.snc.persistence.entity.vistas.VJurisdiccionProducto;

/**
 * Implementación de los servicios de persistencia de la vista JurisdicciónProducto del esquema
 * SNC_APLICACION.
 *
 * @author jamir.avila
 */
@Stateless
public class VJurisdiccionProductoDAOBean extends
    GenericDAOWithJPA<VJurisdiccionProducto, String> implements
    IVJurisdiccionProductoDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VJurisdiccionProductoDAOBean.class);

    /**
     * {@link #recuperarJurisdiccionProductoPorCodigoMunicipio(String)}
     */
    @Override
    public VJurisdiccionProducto recuperarJurisdiccionProductoPorCodigoMunicipio(
        String codigoMunicipio) {
        LOGGER.debug("codigoMunicipio: " + codigoMunicipio);

        Query consulta = entityManager
            .createNamedQuery("recuperarPorCodigoMunicipio");
        consulta.setParameter("codigoMunicipio", codigoMunicipio);

        VJurisdiccionProducto resultado = null;
        try {
            resultado = (VJurisdiccionProducto) consulta
                .getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info(e.getMessage());
        }

        return resultado;
    }

}
