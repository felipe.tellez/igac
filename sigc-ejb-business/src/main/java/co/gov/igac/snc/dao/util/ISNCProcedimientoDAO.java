package co.gov.igac.snc.dao.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaRadicacionesCorrespondencia;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;

/**
 * Interfaz de servicio para acceso a procedimientos almacenados Aagrupa los métodos que hacen
 * llamados a procedimientos almacenados de la base de datos
 *
 * @author pedro.garcia
 * @author juan.mendez
 *
 */
@Local
public interface ISNCProcedimientoDAO {

    @PersistenceContext(unitName = "test-oradb")
    public abstract void setEntityManager(EntityManager entityManager);

    /**
     * Retorna nulo si la radicación no existe
     *
     * @param numeroRadicacionCorrespondencia
     * @param usuario
     * @return
     */
    public abstract Solicitud obtenerRadicacionCorrespondencia(
        String numeroRadicacionCorrespondencia, UsuarioDTO usuario);

    /**
     *
     * @param numeroPredialManzana
     * @return
     */
    public abstract String generarNumeroPredio(String numeroPredialManzana);

    /**
     * Genera el siguiente numero de manzana en el barrio dado
     *
     * @author felipe.cadena
     *
     * @param numeroPredialBarrio
     * @return
     */
    public String generarNumeroManzana(String numeroPredialBarrio);

    /**
     *
     * @param tramiteId
     */
    public abstract Object[] generarProyeccion(Long tramiteId);

    /**
     * Genera la proyeccion de los tramites originales asociados a la cancelacion masiva
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param usuario
     * @return
     */
    public Object[] generarProyeccionCancelacionBasica(Long tramiteId, String usuario);

    /**
     *
     * @param numeroRadicacion
     * @param usuario
     * @return
     */
    public abstract Documento consultarRadicado(String numeroRadicacion, UsuarioDTO usuario);

    /**
     * Ejecuta el SP provisional que permite radicar en correspondencia cualquier documento
     *
     * @author pedro.garcia
     * @param params
     * @return
     */
    public abstract Object[] radicarEnCorrespondencia(List<Object> params);

    /**
     * Ejecuta el SP para generacion de numeraciones y devuelve en dos formatos OJO: los parámetros
     * que no se usen para generar la numeración deben ir como cadenas vacías o como 0s (ej: para
     * generar una basada solo en la territorial se pasan ENumeraciones, idTerritorial, "", "", 0
     *
     * @param params
     * @return
     */
    public abstract Object[] generarNumeracion(List<Object> params);

    /**
     * LLama al procedimiento almacenado que permite obtener los datos de radicación asociados al
     * número de radicación ingresado.
     *
     * @author pedro.garcia
     * @param params
     * @return
     */
    public abstract Object[] consultarRadicadoCorrespondencia(
        List<Object> params);

    /**
     * Elimina una proyección llamando al procedimiento almacenado existente para tal fin
     *
     * @param tramiteId
     * @author fabio.navarrete
     */
    public abstract void eliminarProyeccion(Long tramiteId);

    /**
     * LLama al procedimiento almacenado que calcula los predios afectados por un decreto.
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    public abstract Object[] calcularAfectadosDecreto(Long decretoId);

    /**
     * LLama al procedimiento almacenado que valida una condición a partir del id de un
     * decretoCondicion
     *
     * @author javier.aponte
     * @param Long
     * @return
     */
    public Object[] validarCondicionalDecretoCondicion(Long decretoCondicionId);

    /**
     * LLama al procedimiento almacenado que recalcula los avaluos para un predio. Se agrega usuario
     * dto para pasarlo como parametro al procedimiento almacenado
     *
     * @author david.cifuentes
     * @modified javier.aponte
     *
     * @param tramiteId
     * @param predioId
     * @param fechaDelCalculo
     * @param usuario
     * @return
     */
    public abstract Object[] reproyectarAvaluos(Long tramiteId, Long predioId,
        Date fechaDelCalculo, UsuarioDTO usuario);

    /**
     * Ejecuta el procedimiento almacenado que busca predios según criterios de búsqueda
     *
     * @author pedro.garcia
     * @version 2.0
     * @param datosConsultaPredio
     * @param isCount para definir si solo se quiere el número de filas resultado
     * @param rowStartIdxAndCount indices opcionales de las filas que trae como resultado (la
     * primera y la última), para restringir el conjunto de filas resultado
     * @return Lista de objetos. Si el método se llama para conteo retorna arreglo de un único
     * elemento que es un BigDecimal con el resultado del conteo. Si el método se usa para búsqueda
     * retorna una lista donde cada elemento es el nombre del departamento, del municipio, y el
     * predio (todos los campos) como tal
     */
    public List<Object[]> buscarPrediosPorCriterios(
        FiltroDatosConsultaPredio datosConsultaPredio, boolean isCount,
        int... rowStartIdxAndCount);

    /**
     * Ejecuta el procedimiento almacenado que reversa una proyección por el trámite id
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public boolean reversarProyeccion(Long tramiteId);

    /**
     * Ejecuta el procedimiento almacenado que confirma una proyección por el trámite id
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Object[] confirmarProyeccion(Long tramiteId);

    /**
     * Ejecuta el procedimiento almacenado que confirma una proyección para un tramite del proceso
     * de conservacion
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @return
     */
    public Object[] confirmarProyeccionActualizacion(Long tramiteId);

    /**
     * Ejecuta el procedimiento almacenado que calcula las muestras de ofertas para el control de
     * calidad.
     *
     * @return
     */
//TODO :: ariel.ortiz :: documentar método. Renombrar a 'calcularMuestrasControlCalidad'
    public Object[] calcularMuestrasControl(String municipioSeleccionadoCodigo,
        BigDecimal valorPedidoD, BigDecimal areaTerrenoD, BigDecimal areaConstruidaD,
        BigDecimal valorCalculadoD, String login);

    /**
     * Ejecuta el procedimiento almacenado que borra el calculo de muestras para un control de
     * calidad determinado
     *
     * @author ariel.ortiz
     * @param controlCalidadId
     * @return
     */
    public Object[] borrarCalculoMuestras(Long controlCalidadId);

    /**
     * Ejecuta el procedimiento almacenado que guarda definitivamente las muestras seleccionadas
     * para control de calidad
     *
     * @author ariel.ortiz
     * @param controlId
     * @return
     */
    public Object[] guardarCalculoMuestras(Long controlId);

    /**
     * Se le puso el mismo nombre del sp que ejecuta: el que genera un número predial
     * (específicamente el segmento número de unidad) a partir de una cadena que contiene el número
     * predial hasta el segmento 'terreno'
     *
     * @author pedro.garcia
     * @version 2.0
     * @param terreno número predial hasta el segmento 'terreno'
     * @return
     */
    public String generarNumeroMejoraTE(String terreno);

    /**
     * Se le puso el mismo nombre del sp que ejecuta: el que genera un número predial
     * (específicamente el segmento número de unidad) a partir de una cadena que contiene el número
     * predial hasta el segmento 'piso' cuando es una mejora
     *
     * @author pedro.garcia
     * @version 2.0
     * @param piso número predial hasta el segmento 'piso'
     * @return
     */
    public String generarNumeroMejoraPH(String piso);

    /**
     * Se le puso el mismo nombre del sp que ejecuta: el que genera un número predial
     * (específicamente el segmento número de unidad) a partir de una cadena que contiene el número
     * predial hasta el segmento 'piso' cuando no es una mejora.
     *
     * @author pedro.garcia
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public String generarNumeroUnidad(String piso);

    /**
     * Ejecuta el procedimiento almacenado que busca predios según criterios de búsqueda segun rango
     * numero predial
     *
     * @author javier.barajas
     * @version 2.0
     * @param datosConsultaPredio
     * @param isCount para definir si solo se quiere el número de filas resultado
     * @param rowStartIdxAndCount indices opcionales de las filas que trae como resultado (la
     * primera y la última), para restringir el conjunto de filas resultado
     * @return Lista de objetos. Si el método se llama para conteo retorna arreglo de un único
     * elemento que es un BigDecimal con el resultado del conteo. Si el método se usa para búsqueda
     * retorna una lista donde cada elemento es el nombre del departamento, del municipio, y el
     * predio (todos los campos) como tal
     */
    public List<Object[]> buscarPrediosPorCriteriosRangoNumeroPredial(
        FiltroGenerarReportes datosConsultaPredio, boolean isCount, int datosADesplegar,
        int... rowStartIdxAndCount);

    /**
     * Método para consultar las radicaciones nuevas
     * <i><b>(Usado en avaluos comerciales)</b></i>
     *
     * @author rodrigo.hernandez
     *
     * @param filtroDatos -- filtro con los parametros de la consulta
     * @param usuario -- usuario autenticado en el sistema
     *
     * @return - Lista de radicaciones nuevas.
     */
    public List<RadicacionNuevaAvaluoComercialDTO> buscarRadicacionesNuevasAvaluosComerciales(
        FiltroDatosConsultaRadicacionesCorrespondencia filtroDatos, UsuarioDTO usuario);

    /**
     * Método para realizar la reclasificación de una radicacion
     * <i><b>(Usado en avaluos comerciales)</b></i>
     *
     * @author rodrigo.hernandez
     *
     * @param numeroRadicacion - Numero de la radicacion a reclasificar
     * @param codigoTipoTramite - Codigo del tramite nuevo
     * @param usuario - Usuario autenticado en el sistema
     *
     *
     */
    public void reclasificarTramiteODocumento(String numeroRadicacion, String codigoTipoTramite,
        UsuarioDTO usuario);

    /**
     * Método para realizar búsqueda de una radicacion de correspondencia en correspondencia a
     * partir de su número de radicacion
     *
     * @author rodrigo.hernandez
     *
     * @param numeroRadicacion - Numero de la radicacion
     * @param usuario
     * @return
     */
    public RadicacionNuevaAvaluoComercialDTO buscarRadicacion(String numeroRadicacion,
        UsuarioDTO usuario);

    /**
     * Ejecuta el procedimiento almacenado que valida la proyeccion hecha a un trámite y sus predios
     * asociados para las secciones a las que aplique dicho trámite.
     *
     * @author david.cifuentes
     * @param tramiteId - Id del trámite al cual se va a ejecutar la validación de su proyección.
     * @param tramiteSeccionDatosId - Id del tramiteSeccionDatos donde se especifica que secciones
     * aplican para dicho trámite.
     *
     * @return Arreglo de objetos con el id del trámite, el id del predio, el nombre de la sección y
     * el mensaje de error dirigido al usuario.
     */
    public abstract Object[] validarProyeccion(Long tramiteId, Long tramiteSeccionDatosId);

    /**
     * LLama al procedimiento almacenado que revierte la proyección de un avalúo para un predio
     *
     * @param Long
     * @param Long
     * @return
     * @author javier.aponte
     */
    public Object[] revertirProyeccionAvaluos(Long tramiteId, Long predioId);

    /**
     * Llama a la funcion de oracle que calcula el valor de una unidad de terreno
     *
     * @author franz.gamba
     * @param zonacodigo
     * @param destino
     * @param zonaGeoEconomica
     * @param predioId
     * @return
     */
    public Double obtenerValorUnidadTerreno(String zonacodigo, String destino,
        String zonaGeoEconomica, Long predioId);
    
    
    /**
     * Llama a la funcion de oracle que calcula el valor de una unidad de terreno
     *
     * @author vsocarras
     * @param zonacodigo
     * @param destino
     * @param zonaGeoEconomica
     * @param predioId
     * @param vigencia
     * @return
     */
    public Double obtenerValorUnidadTerreno(String zonacodigo, String destino,
            String zonaGeoEconomica, Long predioId,Timestamp vigencia);

    /**
     * Método para reversar la proyeccción de un predio y un trámite y particular.
     *
     * @author felipe.cadena
     * @param idTramite
     * @param idPredio
     * @return
     */
    public boolean reversarProyeccionPredio(Long idTramite, Long idPredio);

    /**
     * LLama al procedimiento almacenado que liquida los avaluos para un predio.
     *
     * @author javier.aponte
     *
     * @param tramiteId
     * @param predioId
     * @param fechaDelCalculo
     * @param usuario
     * @return
     */
    public abstract Object[] liquidarAvaluosParaUnPredio(Long tramiteId, Long predioId,
        Date fechaDelCalculo, UsuarioDTO usuario);

    /**
     * Método que realiza el llamado al procedimiento enviado como parámetro para las diferentes
     * acciones a realizar en el cierre anual.
     *
     * @author david.cifuentes
     *
     * @param nombreProcedimientoFuncion {@link EProcedimientoAlmacenadoFuncion} con el nombre del
     * procedimiento a ejecutar.
     *
     * @param codDeptoAValidar {@link String} con el código del {@link Departamento} a ejecutar el
     * procedimiento.
     *
     * @param municipio {@link MunicipioCierreVigencia} con el municipio para el cual va a aplicar
     * la ejcución del procedimiento.
     *
     * @param vigencia {@link Long} con el año de la vigencia del decreto actual
     *
     * @param usuario {@link String} con el login del usuario que realiza el cierre
     *
     * @return Array de {@link Object} que tiene la información del cursor con los resultados de la
     * ejecución del procedimiento.
     */
    public Object[] ejecutarProcedimientoCierreAnual(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        String codDepartamento, MunicipioCierreVigencia municipio,
        Long vigencia, String usuario);

    /**
     * Ejecuta el procedimiento almacenado que busca predios según criterios de busqueda del WS de
     * consulta servicios catastrales
     *
     * @author andres.eslava
     * @version 1.0
     * @param datosFiltro datos sobre los cuales se hara el filtro.
     * @return Lista de objetos que contiene la informacion predial, construcciones, zonas y
     * propietarios.
     */
    public Object[] buscarPrediosWS(FiltroConsultaCatastralWebService datosFiltro, Long numeroPagina);

    /**
     * Método para llamar el procedimiento almacenado de reportes prediales con datos básicos
     *
     * @param datosConsultaPredio
     * @param usuario
     * @author javier.aponte
     * @modified by leidy.gonzalez
     */
    public Object[] generarReportePredialDatosBasicos(RepReporteEjecucion repReporteEjecucion);

    /**
     * Prueba. Ejecuta un procedimiento de prueba para ver cómo se aisla la transacción para que en
     * caso de que haya que hacer rollback no afecte el flujo del caso de uso.
     *
     * @author pedro.garcia
     *
     * @param codigoPrueba un código de error para diferenciar la prueba
     * @param mensajePrueba un mensaje de error para diferenciar la prueba
     * @param generarError para decidir si el procedimiento debe generar un error en su ejecución
     */
    public Object[] ejecutarSPAislado(String codigoPrueba, String mensajePrueba,
        boolean generarError);

    /**
     * Método registrar el acceso de un usuario.
     *
     * @author felipe.cadena
     * @param logAcceso Objeto con la información del usuario y la maquina cliente.
     * @return -- id del registro creado
     */
    public long registrarAccesoUsuario(LogAcceso logAcceso);

    /**
     * Método registrar la accion registrada por un usuario acceso de un usuario.
     *
     * @author felipe.cadena
     * @param logAcceso
     * @return
     */
    public void registrarAccionUsuario(LogAccion logAccion);

    /**
     * Método registrar la accion registrada por un usuario acceso de un usuario.
     *
     * @author juanfelipe.garcia
     * @param tramiteId un código de error para diferenciar la prueba
     * @param tramiteRecurridoId un mensaje de error para diferenciar la prueba
     * @param tipoRecurso para decidir si el procedimiento debe generar un error en su ejecución
     * @return
     */
    public Object[] recurrirProyeccion(Long tramiteId, Long tramiteRecurridoId, String tipoRecurso,
        String usuario);

    /**
     * Método para proyecta un ficha matriz de predios migrados
     *
     * @author felipe.cadena
     *
     * @param numeroPredial
     * @param fecha
     * @param usuario
     * @return
     */
    public Object[] proyectarFichaMigrada(String numeroPredial, Date fecha, String usuario);

    /**
     * Ejecuta el SP provisional que permite contar registros prediales para generar los productos
     * catastrales
     *
     * @author leidy.gonzalez
     * @param params
     * @return
     */
    public abstract Object[] contarRegistrosPrediales(List<Object> params);

    /**
     * Método para reversar una poyeccion de una ficha migrada
     *
     * @author felipe.cadena
     * @param predioId
     * @return
     */
    public Object[] reversarProyeccionFichaMigrada(Long predioId);

    /**
     * Método que aplica los cambios proyectados de una ficha
     *
     * @author felipe.cadena
     * @param predioId
     * @return
     */
    public Object[] confirmarProyeccionFichaMigrada(Long predioId);

    /**
     * Método para recibir radicado.
     *
     * @author juanfelipe.garcia
     * @param pradicacion es el número de radicado a marcar como recibido
     * @return
     */
    public Object[] recibirRadicado(String pradicacion, String usuario);

    /**
     * Método para finalizar radicado.
     *
     * @author juanfelipe.garcia
     * @param pradicacion es el número de radicado a marcar como recibido
     * @param pradicacionresponde es el número de radicado con el que se da repuesta
     * @return
     */
    public Object[] finalizarRadicacion(String pradicacion, String pradicacionResponde,
        String usuario);

    /**
     * Metodo para calcular el valor de la unidad(m2) de una unidad de construccion
     *
     * @author felipe.cadena
     *
     * @param zonacodigo
     * @param sector
     * @param destino
     * @param usoConstruccion
     * @param zonaGeoEconomica
     * @param puntaje
     * @return
     */
    public Double obtenerValorUnidadConstruccion(String zonacodigo, String sector, String destino,
        String usoConstruccion, String zonaGeoEconomica, Long puntaje, Long predioId);

    /**
     * Método para llamar al procedimiento almacenado que genera los reportes para cierre anual.
     *
     * @note: Copia del método generarReportePredialDatosBasicos de leidy.gonzalez, usado en la
     * consulta de reportes prediales.
     *
     * @author david.cifuentes
     *
     * @param repReporteEjecucion
     * @return
     */
    public abstract Object[] generarReporteCierreAnual(
        RepReporteEjecucion repReporteEjecucion);

    /**
     * Ejecuta el procedimiento almacenado que confirma una proyección de la lista de tramites dada
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public Object[] confirmarProyeccionMasivo(List<Tramite> tramites);

    /**
     * Ejecuta el procedimiento para iniciar la validacion del proceso de actualizacion
     *
     * @author felipe.cadena
     * @param idMunicipioActualizacion
     * @param usuario
     * @return
     */
    public Object[] validarActualizacion(Long idMunicipioActualizacion, String usuario);

    /**
     * Ejecuta el procedimiento para iniciar la validacion del proceso de actualizacion
     *
     * @author felipe.cadena
     * @param idMunicipioActualizacion
     * @param usuario
     * @return
     */
    public Object[] radicarActualizacion(Long idMunicipioActualizacion, Long idSolicitud,
        String usuario);

    /**
     * Ejecuta el procedimiento para validar si el proceso de actualizacion se puede radicar
     *
     * @author felipe.cadena
     * @param idMunicipioActualizacion
     * @return
     */
    public int validarRadicacionActualizacion(Long idMunicipioActualizacion);

    /**
     * Ejecuta el procedimiento para eliminar un proceso de actualizacion
     *
     * @author felipe.cadena
     * @param idMunicipioActualizacion
     * @param usuario
     * @return
     */
    public Object[] eliminarActualizacion(Long idMunicipioActualizacion, String usuario);

    /**
     * Ejecuta el procedimiento para eliminar valor de tablas de avaluos de actualizacion
     *
     * @author felipe.cadena
     * @param valorActualizacionId
     * @param municipioCodigo
     * @return
     */
    public Object[] eliminarValoresActualizacion(Long valorActualizacionId, String municipioCodigo);

    /**
     * Ejecuta el procedimiento para cargar tablas de actualizacion
     *
     * @author felipe.cadena
     *
     * @param municipioCodigo
     * @param valorActualizacionId
     * @param usuario
     * @return
     */
    public Object[] cargarTablasLiquidacion(String municipioCodigo, Long valorActualizacionId,
        String usuario);

    /**
     * Ejecuta el procedimiento para calcular los avaluos del municipio basado en las tablas
     * temporales de liquidacion
     *
     * @author felipe.cadena
     * @param municipioCodigo
     * @return
     */
    public Object[] calcularAvaluosTablas(String municipioCodigo);

    /**
     * Ejecuta el procedimiento para poner en firme las tablas de actualizacion
     *
     * @author felipe.cadena
     * @param municipioCodigo
     * @return
     */
    public Object[] aplicarTablasLiquidacion(String municipioCodigo);

    /**
     * Método que utiliza la funcion para obtener el procentaje de incremento de un predio en una
     * vigencia especifica.
     *
     * @param vigencia
     * @param predioId
     * @return
     */
    public double CalcularPorcentajeIncrementoPredio(String vigencia, Long predioId);

    /**
     * Método para finalizar radicado.
     *
     * @author leidy.gonzalez
     * @param pradicacion es el número de radicado a marcar como recibido
     * @return
     */
    public Object[] consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(
        String estructuraOrganizacionalCodigo);

    /* Método que llama el procedimiento almacenado para actualizar el año y el número de última
     * resolución de todos los predios asociados a un trámite.
     *
     * @param tramiteId @param anioUltimaResolucion @param numeroUltimaResolucion @param usuario
     * @return
     */
    public Object[] establecerAnioYNumeroUltimaResolucion(Long tramiteId, int anioUltimaResolucion,
        String numeroUltimaResolucion, UsuarioDTO usuario);

    /**
     * Método para obtener la fecha de creacion del predio
     *
     * @author leidy.gonzalez
     * @param pPredioId es el id del predio que se va consultar
     * @return
     */
    public Date consultarFechaCreacionPredio(Long pPredioId);

    /** Método que llama el procedimiento almacenado que valida si existe un número de radicado en
     * CORDIS
     *
     * @param numeroRadicado
     * @param usuario
     * @return
     */
    public boolean validaNumeroRadicacion(String numeroRadicacionCorrespondencia, UsuarioDTO usuario);

    /**
     * Metodo para consultar los jobs pendientes por procesar segun el tipo de producto. Una vez son
     * consultados se cambia directamente en base de datos el estado a ejecucion.
     *
     * @author felipe.cadena
     *
     * @param tipoProducto
     * @param contidad
     * @param estadoInicial
     * @param estadoFinal
     * @return
     */
    public List<ProductoCatastralJob> consultarJobsPendientesPorProcesar(int cantidad,
        String estadoInicial, String estadoFinal);

    /**
     * Método para finalizar trámites con resolución
     *
     * @param usuario usuario que realiza el cierre
     * @param radicado es el número de radicado a marcar como recibido
     * @param resoluciones lista de resoluciones que tiene el trámite a finalizar
     * @return
     */
    public Object[] finalizarRadicacionResoluciones(String usuario,
        String radicado, List<String> resoluciones);

    /**
     * Método para finalizar trámites con correspondencia IE (Ej: memorandos)
     *
     * @param usuario usuario que realiza el cierre
     * @param radicado es el número de radicado cerrar en cordis finalizar
     * @return
     */
    Object[] finalizarRadicacionInterno(String usuario,
        String radicado, String radicadoResponde);

    /**
     * Metodo para invocar el procedimiento que inicializa los datos de los tramites de modificacion
     * de PH
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @param usuario
     * @return
     */
    public Object[] aceptarEnglobeVirtual(Long tramiteId, String usuario);

    /**
     * Metodo para generar un unevo numero de ficha matriz en englobes virtuales
     *
     * @author felipe.cadena
     *
     * @param condicionPropiedad
     * @param usuario
     * @return
     */
    public Object[] generarNumeroEnglobeVirtual(Long tramiteId, String condicionPropiedad,
        String usuario);

    /**
     * Método para llamar el procedimiento almacenado de generación de reportes de radicación
     *
     * @param repReporteEjecucion
     * @author david.cifuentes
     */
    public Object[] generarReporteRadicacion(
        RepReporteEjecucion repReporteEjecucion);

    /**
     * Metodo para calcular las zonas fisicas y geoeconomicas segun una vigencia y zona dada
     *
     * @author leidy.gonzalez
     *
     * @param vigencia
     * @param zona
     * @return
     */
    public Object[] calculoZonasFisicasYGeoeconomicas(Date vigencia, String zona);


	/**
	 * Método para llamar el procedimiento almacenado de generación de reportes estadisticos
	 *
	 * @param repReporteEjecucion
	 * @author felipe.cadena
	 */
	public Object[] generarReporteEstadistico(
			RepReporteEjecucion repReporteEjecucion);


	/**
	 * Método para iniciar el cargue de información del Cica
	 *
	 * @param municipioActualizacionId
	 * @param usuario
	 * @author felipe.cadena
	 */
    public Object[] iniciarCargueCica(Long municipioActualizacionId, String usuario);

	/**
	 * Método para eliminar proyección del Cica
	 *
	 * @param idsPredios
	 * @author felipe.cadena
	 */
    public Object[] eliminarProyeccionCica(List<Long> idsPredios);

    /**
	 * Método para preliquidar proyección del Cica
	 *
	 * @param municipioCodigo
	 * @param vigencia
	 * @author felipe.cadena
	 */
    public Object[] preliquidarCargueAct(String municipioCodigo, Integer vigencia);

    /**
     * Método para aplicar proyección del Cica
     *
     * @param municipioCodigo
     * @param vigencia
     * @author felipe.cadena
     */
    public Object[] aplicarCambiosAct(String municipioCodigo, Integer vigencia);

}
