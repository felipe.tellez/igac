package co.gov.igac.snc.dao.tramite.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.tramite.ITramiteEstadoDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.HashSet;
import org.eclipse.jdt.internal.compiler.ast.ForeachStatement;

@Stateless
public class TramiteEstadoDAOBean extends
    GenericDAOWithJPA<TramiteEstado, Long> implements ITramiteEstadoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteEstadoDAOBean.class);

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramiteEstadoDAO#actualizarTramiteEstado(TramiteEstado)
     */
    @Override
    public TramiteEstado actualizarTramiteEstado(
        TramiteEstado tramiteEstadoActual, UsuarioDTO usario) {
        LOGGER.debug("executing TramiteEstadoDAOBean#actualizarTramiteEstado");

        try {
            tramiteEstadoActual.setUsuarioLog(usario.getLogin());
            tramiteEstadoActual
                .setFechaLog(new Date(System.currentTimeMillis()));
            TramiteEstado te = update(tramiteEstadoActual);
            return te;
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            return null;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     * @see ITramiteEstadoDAO#crearActualizarTramiteEstado(TramiteEstado, UsuarioDTO)
     */
    @Override
    public Boolean crearActualizarTramiteEstado(
        TramiteEstado tramiteEstadoActual, UsuarioDTO usuario) {
        LOGGER.debug("executing TramiteEstadoDAOBean#crearActualizarTramiteEstado");
        Boolean resultado = null;
        String query;

        query = "SELECT te FROM TramiteEstado te" +
            " JOIN FETCH te.tramite tet" +
            " WHERE  tet.id= :tramiteId" +
            " AND te.estado = :estado" +
            " ORDER BY te.fechaInicio DESC";

        try {

            Query q = entityManager.createQuery(query);
            q.setMaxResults(1);
            q.setParameter("tramiteId", tramiteEstadoActual.getTramite().getId());
            q.setParameter("estado", tramiteEstadoActual.getTramite().getEstado());

            tramiteEstadoActual.setFechaLog(new Date(System.currentTimeMillis()));
            tramiteEstadoActual.setUsuarioLog(usuario.getLogin());

            TramiteEstado te = (TramiteEstado) q.getSingleResult();

            if (te != null) {

                if (!te.getResponsable().equals(
                    tramiteEstadoActual.getResponsable())) {
                    update(tramiteEstadoActual);

                }
            }
            resultado = true;
        } catch (NoResultException ne) {
            LOGGER.info("No se encontró el TramiteEstado");
            update(tramiteEstadoActual);
            resultado = true;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL.getExcepcion(LOGGER, e,
                "TramiteEstadoDAOBean#crearActualizarTramiteEstado");
        } finally {
            return resultado;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @see ITramiteEstadoDAO#obtenerTramiteEstadoVigentePorTramiteId(Long)
     * @modified pedro.garcia asignación a variable de retorno para poder ver su contenido antes del
     * return; y por órden
     */
    @Override
    public TramiteEstado obtenerTramiteEstadoVigentePorTramiteId(Long idTramite) {
        LOGGER.debug("TramiteEstadoDAOBean#obtenerTramiteEstadoVigentePorTramiteId");

        TramiteEstado answer = null;
        ArrayList<TramiteEstado> listaTE;
        String query = "SELECT te FROM TramiteEstado te " +
            "WHERE te.tramite.id = :idTramite " + "ORDER BY te.id DESC";

        Query q = entityManager.createQuery(query);

        // OJO: parece que esto se comporta así: (cuando hay un group by)
        // empieza a contar desde el último
        // hacía arriba, por lo que no trae el resultado esperado -al menos en
        // este caso-. Entonces,
        // se trae la lista completa y se tome el primero
        // q.setMaxResults(1);
        q.setMaxResults(1);
        q.setParameter("idTramite", idTramite);

        try {
            listaTE = (ArrayList<TramiteEstado>) q.getResultList();
            // answer = (TramiteEstado) q.getSingleResult();
            if (!listaTE.isEmpty()) {
                answer = listaTE.get(0);
            }

        } catch (Exception e) {
            LOGGER.debug("Error al consultar el tramite estado: " +
                e.getMessage());

        }
        return answer;
    }

    /**
     * @see ITramiteEstadoDAO#cambiarTramiteEstado(Tramite, String, ETramiteEstado, String,
     * UsuarioDTO)
     */
    @SuppressWarnings("unchecked")
    @Override
    public TramiteEstado cambiarTramiteEstado(Tramite tramite, String motivo,
        ETramiteEstado tramiteEstadoFuturo, String responsableFuturo,
        UsuarioDTO usuario) {
        /*
         * String sql = "SELECT te FROM TramiteEstado te" + " WHERE te.tramite.id = :tramiteId" + "
         * AND te.fechaFin IS NULL" + " AND te.estado != :estado"; Query query =
         * this.entityManager.createQuery(sql); query.setParameter("tramiteId", tramite.getId());
         * query.setParameter("estado", tramiteEstadoFuturo.getCodigo()); List<TramiteEstado>
         * tramiteEstadosResult = new ArrayList<TramiteEstado>(); try { tramiteEstadosResult =
         * query.getResultList(); } catch (NoResultException e) { // DO NOTHING } catch (Exception
         * e) { throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR, "Error
         * obteniendo los estados del trámite", e); } if (tramiteEstadosResult != null &&
         * !tramiteEstadosResult.isEmpty()) { Date auxFechaFin = new Date(); sql = "UPDATE
         * TramiteEstado te SET te.fechaFin = :fechaFin" + " WHERE te.tramite.id = :tramiteId" + "
         * AND te.fechaFin IS NULL" + " AND te.estado != :estado"; query =
         * this.entityManager.createQuery(sql); query.setParameter("fechaFin", auxFechaFin);
         * query.setParameter("tramiteId", tramite.getId()); query.setParameter("estado",
         * tramiteEstadoFuturo.getCodigo()); query.executeUpdate(); }
         *
         * TramiteEstado nuevoTramiteEstado = new TramiteEstado();
         * nuevoTramiteEstado.setEstado(tramiteEstadoFuturo.getCodigo());
         * nuevoTramiteEstado.setMotivo(motivo); nuevoTramiteEstado.setFechaInicio(new Date()); if
         * (responsableFuturo != null && !responsableFuturo.trim().isEmpty()) {
         * nuevoTramiteEstado.setResponsable(responsableFuturo); }
         * nuevoTramiteEstado.setUsuarioLog(usuario.getLogin()); nuevoTramiteEstado.setFechaLog(new
         * Date());
         *
         * TramiteEstado auxTramEst = this.update(nuevoTramiteEstado);
         * nuevoTramiteEstado.setId(auxTramEst.getId()); return nuevoTramiteEstado;
         */
        throw new ExcepcionSNC("algúnCódigo",
            ESeveridadExcepcionSNC.ADVERTENCIA,
            "implementación del método fallida por cambios en el manejo de estados");

    }

    /**
     * @see ITramiteEstadoDAO#findByTramiteId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TramiteEstado> findByTramiteId(Long id) {
        try {
            String sql = "SELECT te FROM TramiteEstado te" +
                " WHERE te.tramite.id = :idTramite";
            Query q = entityManager.createQuery(sql);
            q.setParameter("idTramite", id);
            return q.getResultList();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al buscar Trámite Estado por Id de trámite", e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author juan.agudelo
     * @see ITramiteEstadoDAO#buscarHistoricoObservacionesPorTramiteId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TramiteEstado> buscarHistoricoObservacionesPorTramiteId(
        Long tramiteId) {
        LOGGER.debug("executing TramiteEstadoDAOBean#buscarHistoricoObservacionesPorTramiteId");
        List<TramiteEstado> resultado = null;
        String query;

        query = "SELECT te FROM TramiteEstado te" +
            " JOIN  te.tramite tet" +
            " WHERE  tet.id= :tramiteId" +
            " AND te.estado = :estado" +
            " ORDER BY te.fechaLog ASC";

        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        q.setParameter("estado",
            ETramiteEstado.DEVUELTO_REVISION_PROYECCION.getCodigo());

        try {
            resultado = (List<TramiteEstado>) q.getResultList();

        } catch (Exception e) {
            throw new ExcepcionSNC("mi codigo de excepcion",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), e);
        }
        return resultado;
    }

    /**
     * @author felipe.cadena
     * @see ITramiteEstadoDAO#buscarHistoricoObservacionesPorTramiteId(Long)
     */
    @SuppressWarnings("unchecked")
    @Override
    public TramiteEstado buscarObservacionTemporalPorTramiteId(
        Long tramiteId) {
        LOGGER.debug("executing TramiteEstadoDAOBean#buscarHistoricoObservacionesPorTramiteId");
        TramiteEstado resultado = null;
        String query;

        query = "SELECT te FROM TramiteEstado te" +
            " JOIN  te.tramite tet" +
            " WHERE  tet.id= :tramiteId" +
            " AND te.estado = :estado" +
            " ORDER BY te.fechaLog DESC";

        Query q = entityManager.createQuery(query);
        q.setParameter("tramiteId", tramiteId);
        q.setParameter("estado",
            ETramiteEstado.ESPERANDO_CONFIRMAR_DEVOLUCION.getCodigo());

        try {
            resultado = (TramiteEstado) q.getSingleResult();

        } catch (Exception e) {
            return null;
        }
        return resultado;
    }

    /**
     * @author juan.cruz
     * @see ITramiteEstadoDAO#validarSiEstadoFueCancelarTramite(Long)
     *
     */
    @Override
    public HashSet<Long> validarSiEstadoFuePorCancelarTramite(List<Long> tramitesID) {
        LOGGER.debug("TramiteEstadoDAOBean#validarSiEstadoFueCancelarTramite");

        HashSet<Long> resultado = new HashSet<Long>();
        List<TramiteEstado> listaTramites;
        //necesito es el id de tramite y no me deja traerlo con te.tramite.id
        String query = "SELECT DISTINCT te " +
            "FROM TramiteEstado te " +
            "WHERE te.tramite.id IN (:idTramite) " +
            "AND te.estado = :estadoPorCancelar " +
            "ORDER BY te.id DESC";

        Query q = entityManager.createQuery(query);

        q.setParameter("idTramite", tramitesID);
        q.setParameter("estadoPorCancelar", ETramiteEstado.POR_CANCELAR.getCodigo());

        try {
            listaTramites = (List<TramiteEstado>) q.getResultList();
            /* juan.cruz: se crea esta iteración debido a que no se identifica actualmente por que
             * la consulta no deja capturar directamente el identificador del tramite desde la tabla
             * tramite estado.
             */
            for (TramiteEstado unTramite : listaTramites) {
                Long tramiteID = unTramite.getTramite().getId();
                if (!resultado.contains(tramiteID)) {
                    resultado.add(tramiteID);
                }
            }
        } catch (Exception e) {
            LOGGER.debug(
                "Error al consultar el tramite estado por el método validarSiEstadoFuePorCancelarTramite: " +
                e.getMessage());
            return null;
        }
        return resultado;
    }
}
