/*
 * Proyecto SNC 2016
 */
package co.gov.igac.snc.dao.conservacion.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IPFichaMatrizPredioTerrenoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementacion operaciones de base de datos sobre la entidad PFichaMatrizPredioTerreno
 *
 * @author felipe.cadena
 */
@Stateless
public class PFichaMatrizPredioTerrenoDAOBean extends GenericDAOWithJPA<PFichaMatrizPredioTerreno, Long>
    implements IPFichaMatrizPredioTerrenoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        PFichaMatrizPredioTerrenoDAOBean.class);

}
