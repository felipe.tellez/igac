/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;

import javax.ejb.Local;

/**
 *
 * @author juan.agudelo
 *
 */
@Local
public interface IPersonaDAO extends IGenericJpaDAO<Persona, Long> {

    /**
     * Busca una persona por primer nombre, segundo nombre, primer apellido, segundo apellido, tipo
     * documento, número documento y DV
     *
     * @author juan.agudelo
     * @param personaDatos
     * @return
     */
    public Persona buscarPersonaByNombreDocumentoDV(Persona personaDatos);

    /**
     * Método que retorna una lista de objetos de tipo Persona según su número de identificación y
     * tipo de documento.
     *
     * @param tipoIdentificacion
     * @param numeroIdentificacion
     * @return
     * @author fabio.navarrete
     */
    public List<Persona> findByTipoNumeroIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion);

    /**
     * Método que permite recuperar la información de una persona por el número y tipo de
     * identificación
     *
     * @author juan.agudelo
     * @param tipoIdentificacion
     * @param numeroIdentificacion
     * @return
     */
    public List<Persona> buscarPersonaPorNumeroYTipoIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion);

    /**
     * Método que permite buscar una persona por tipo de documento de identificación, número de
     * identificación, primer nombre, segundo nombre, primer apellido y segundo apellido, soporta
     * campos nulos exceptuando el tipo de documento de identificación
     *
     * @author juan.agudelo
     * @param Persona
     * @return
     */
    public Persona buscarPersonaPorNombrePartesTipoDocNumDoc(
        Persona persona);

    /**
     * Temporal por datos migrados de cobol Método que permite buscar una persona por tipo de
     * documento de identificación, número de identificación, primer nombre, segundo nombre, primer
     * apellido y segundo apellido, soporta campos nulos exceptuando el tipo de documento de
     * identificación
     *
     * @author juan.agudelo
     * @param persona
     * @return
     */
    public List<Persona> buscarPersonaPorNombrePartesTipoDocNumDocTemp(
        Persona persona);

    /**
     * Método que retorna el registro único por tipo de identificacion y número
     *
     * @param tipoIdentificacion
     * @param numeroIdentificacion
     * @return
     */
    public Persona getPersonabyTipoIdentificacionNumeroIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion);
}
