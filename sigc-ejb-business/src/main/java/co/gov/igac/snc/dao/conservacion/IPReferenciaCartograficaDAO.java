package co.gov.igac.snc.dao.conservacion;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;

@Local
public interface IPReferenciaCartograficaDAO extends
    IGenericJpaDAO<PReferenciaCartografica, Long> {

}
