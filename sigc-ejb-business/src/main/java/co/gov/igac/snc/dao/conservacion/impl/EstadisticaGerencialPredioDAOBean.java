package co.gov.igac.snc.dao.conservacion.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.conservacion.IEstadisticaGerencialPredioDAO;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialPredio;
import co.gov.igac.snc.persistence.entity.conservacion.EstadisticaGerencialPredioPK;

@Stateless
public class EstadisticaGerencialPredioDAOBean extends GenericDAOWithJPA<EstadisticaGerencialPredio, EstadisticaGerencialPredioPK>
    implements IEstadisticaGerencialPredioDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        EstadisticaGerencialPredioDAOBean.class);

    public List<Object[]> consultaPorMunicipio(Long anio, String codigoMunicipio, String zona) {

        String queryString =
            "select m.nombre, egp.anio, egp.zona, sum(cantidad_predios) as cantidad_predios, sum(cantidad_propietarios) as cantidad_propietarios, sum(area_terreno) as area_terreno, sum(area_construccion) as area_construccion, sum(valor_avaluo) as valor_avaluo " +
            "from estadistica_gerencial_predio egp " +
            "inner join municipio m on egp.municipio_codigo = m.codigo " +
            "where m.codigo= :codigoMun  and egp.anio= :anio " + (zona != null ?
                " and egp.zona = '" + zona + "'" : "") +
            "group by m.nombre, egp.anio, egp.zona";

        Query q = this.entityManager.createNativeQuery(queryString);
        q.setParameter("codigoMun", codigoMunicipio);
        q.setParameter("anio", anio);

        return q.getResultList();

        /* String queryString="SELECT e FROM EstadisticaGerencialPredio e JOIN FETCH e.id.municipio
         * m JOIN FETCH m.departamento d WHERE e.id.anio= :anio ";
         *
         * if (codigoMunicipio!=null && !codigoMunicipio.equals("")) queryString += " AND m.codigo =
         * :codigoMunicipio "; *
         *
         * Query q = this.entityManager.createQuery(queryString); q.setParameter("anio",
         * anio.intValue()); if (codigoMunicipio!=null && !codigoMunicipio.equals(""))
         * q.setParameter("codigoMunicipio", codigoMunicipio);
         *
         * return q.getResultList(); */
    }

    public List<Object[]> consultaPorDepartamento(Long anio, String codigoDepartamento, String zona) {

        String queryString =
            "select m.nombre, egp.anio, egp.zona, sum(cantidad_predios) as cantidad_predios, sum(cantidad_propietarios) as cantidad_propietarios, sum(area_terreno) as area_terreno, sum(area_construccion) as area_construccion, sum(valor_avaluo) as valor_avaluo " +
            "from estadistica_gerencial_predio egp " +
            "inner join municipio m on egp.municipio_codigo = m.codigo " +
            "inner join departamento d on m.departamento_id = d.codigo " +
            "where d.codigo= :codigoDpto  and egp.anio= :anio " + (zona != null ?
                " and egp.zona = '" + zona + "'" : "") +
            "group by m.nombre, egp.anio, egp.zona order by m.nombre, egp.anio, egp.zona";

        Query q = this.entityManager.createNativeQuery(queryString);
        q.setParameter("codigoDpto", codigoDepartamento);
        q.setParameter("anio", anio);

        return q.getResultList();

    }

    public List<Object[]> consultaPorPais(Long anio, String zona) {

        String queryString =
            "select d.nombre, egp.anio, egp.zona, sum(cantidad_predios) as cantidad_predios, sum(cantidad_propietarios) as cantidad_propietarios, sum(area_terreno) as area_terreno, sum(area_construccion) as area_construccion, sum(valor_avaluo) as valor_avaluo " +
            "from estadistica_gerencial_predio egp " +
            "inner join municipio m on egp.municipio_codigo = m.codigo " +
            "inner join departamento d on m.departamento_id = d.codigo " +
            "where egp.anio= :anio " + (zona != null ? " and egp.zona = '" + zona + "'" : "") +
            "group by d.nombre, egp.anio, egp.zona order by d.nombre, egp.anio, egp.zona ";

        Query q = this.entityManager.createNativeQuery(queryString);
        q.setParameter("anio", anio);

        return q.getResultList();

    }

}
