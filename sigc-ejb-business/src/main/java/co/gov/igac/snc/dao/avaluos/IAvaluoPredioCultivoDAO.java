package co.gov.igac.snc.dao.avaluos;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioCultivo;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface que contiene metodos de consulta sobre entity AvaluoPredioCultivo
 *
 * @author felipe.cadena
 *
 */
@Local
public interface IAvaluoPredioCultivoDAO extends IGenericJpaDAO<AvaluoPredioCultivo, Long> {

    /**
     * Hace la consulta por el id del Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioCultivo> consultarPorAvaluo(long avaluoId);

    /**
     * Método para determinar si existen cultivos asociados a un avalúo o a los predios del avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public Boolean calcularCultivosPreviosAvaluo(Long idAvaluo);

    /**
     * Método para eliminar los cultivos previos asociados a un avalúo
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return
     */
    public boolean eliminarCultivosPreviosAvaluo(Long idAvaluo);

//end of interface
}
