package co.gov.igac.snc.dao.conservacion;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;

/**
 * Servicios de persistencia del objeto {@link PFichaMatrizPredio}.
 *
 * @author david.cifuentes
 */
@Local
public interface IPFichaMatrizPredioDAO extends
    IGenericJpaDAO<PFichaMatrizPredio, Long> {

    /**
     * Retorna la lista de {@link PFichaMatrizPredio} a partir del ide de la ficha matriz
     * relacionada
     *
     * @param id
     * @return
     * @author david.cifuentes
     */
    public List<PFichaMatrizPredio> findByPFichaMatrizId(Long id);

    /**
     * Retorna la lista de {@link PFichaMatrizPredio} a partir de un numero predial
     *
     * @param numeroPredial
     * @return
     * @author juanfelipe.garcia
     */
    public PFichaMatrizPredio obtenerPFichaMatrizPredioPorNumeroPredial(String numeroPredial);

    /**
     * Elimina directamente el registro desde la base de datos (Query nativo)
     *
     * @author felipe.cadena
     * @param fichaMatrizPredio
     */
    public void eliminar(PFichaMatrizPredio fichaMatrizPredio);

    /**
     * Método para consultar los {@link PFichaMatrizPredio} de un valor cancela inscribe para una
     * proyección y de una {@link PFichaMatriz}
     *
     * @param valorCancelaInscribe
     * @param fichaMatrizId
     * @return
     */
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(
        String valorCancelaInscribe, Long fichaMatrizId);

    /**
     * Método para consultar los {@link PFichaMatrizPredio} por su estado y el id de la
     * {@link PFichaMatriz}
     *
     * @author david.cifuentes
     * @param estadoPredio
     * @param fichaMatrizId
     * @return
     */
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(
        String estadoPredio, Long fichaMatrizId);

    /**
     * Retorna la lista de {@link PFichaMatrizPredio} a partir de un numero predial
     *
     * @param numeroPredial
     * @return
     * @author leidy.gonzalez
     */
    public List<PFichaMatrizPredio> obtenerPFichaMatrizPredioContieneNumeroPredial(
        String numeroPredial);

}
