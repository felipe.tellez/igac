package co.gov.igac.snc.dao.avaluos.impl;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.avaluos.IContratoInteradminisAdicionDAO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradminisAdicion;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see IContratoInteradminisAdicionDAO
 * @author felipe.cadena
 */
@Stateless
public class ContratoInteradminisAdicionDAOBean extends
    GenericDAOWithJPA<ContratoInteradminisAdicion, Long> implements IContratoInteradminisAdicionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ContratoInteradminisAdicionDAOBean.class);

    /**
     * @see IContratoInteradminisAdicionDAO#buscarAdicionesPorIdContrato(java.lang.Long)
     * @author felipe.cadena
     */
    @Override
    public List<ContratoInteradminisAdicion> buscarAdicionesPorIdContrato(Long idContrato) {

        LOGGER.debug("Iniciando ContratoInteradminisAdicionDAOBean#buscarAdicionesPorIdContrato");

        List<ContratoInteradminisAdicion> resultado = null;

        String queryString;
        Query query;

        queryString = "SELECT a FROM ContratoInteradminisAdicion a " +
            "WHERE a.contratoInteradminsitrativoId=:idContrato";

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("idContrato", idContrato);
            resultado = query.getResultList();

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER, e,
                    "ContratoInteradminisAdicionDAOBean#buscarAdicionesPorIdContrato");

        }

        return resultado;
    }

}
