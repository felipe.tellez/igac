package co.gov.igac.snc.jms.vo;

//TODO :: juan.mendez :: documentar la enumeración :: pedro.garcia
public enum EventType {
    GENERAR_CERTIFICADO_PLANO_PREDIAL_CATASTRAL,
    SIG_COPIA_TRABAJO_CREAR,
    SIG_COPIA_TRABAJO_CARGAR_DATOS_EDITADOS,
    VALIDAR_GEOMETRIA_PREDIOS
}
