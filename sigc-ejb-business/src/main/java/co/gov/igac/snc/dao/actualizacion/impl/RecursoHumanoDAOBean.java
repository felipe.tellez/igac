package co.gov.igac.snc.dao.actualizacion.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.actualizacion.IRecursoHumanoDAO;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.util.EActualizacionContratoActividad;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Implementación de los servicios de persistencia del objeto RecursoHumano.
 *
 * @author franz.gamba
 */
@Stateless
public class RecursoHumanoDAOBean extends
    GenericDAOWithJPA<RecursoHumano, Long> implements IRecursoHumanoDAO {

    /**
     * Servicio de bitácora.
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(RecursoHumanoDAOBean.class);

    @Override
    public List<RecursoHumano> obtenerRecursoPorResponsableActualizacion(
        Long responsableActId) {
        LOGGER.debug("obtenerRecursoPorResponsableActualizacion");

        List<RecursoHumano> answer = null;
        try {
            Query q = entityManager
                .createQuery("SELECT rh" +
                    " FROM RecursoHumano rh" +
                    " WHERE rh.actualizacionResponsable.id= :responsableActId");
            q.setParameter("responsableActId", responsableActId);

            answer = (List<RecursoHumano>) q.getResultList();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    @Override
    public List<RecursoHumano> obtenerRecursoPorActualizacionId(
        Long actualizacionId) {
        LOGGER.debug("RecursoHumanoDAOBean#obtenerRecursoPorActualizacionId");

        List<RecursoHumano> answer = null;
        try {
            Query q = entityManager
                .createQuery("SELECT rh " +
                    " FROM RecursoHumano rh JOIN FETCH rh.actualizacionResponsable ar " +
                    " JOIN FETCH ar.actualizacion a " +
                    " WHERE a.id = :actualizacionId ");
            q.setParameter("actualizacionId", actualizacionId);

            answer = (List<RecursoHumano>) q.getResultList();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see IRecursoHumanoDAO#obtenerRecursoHumanoPorActividad(String)
     * @author javier.aponte
     */
    @Override
    public List<RecursoHumano> obtenerRecursoHumanoPorActividad(
        String actividad) {
        LOGGER.debug("RecursoHumanoDAOBean#obtenerRecursoHumanoPorActividad");

        List<RecursoHumano> answer = null;
        try {
            Query q = entityManager
                .createQuery("SELECT rh " +
                    " FROM RecursoHumano rh " +
                    " WHERE rh.actividad = :actividad ");
            q.setParameter("actividad", actividad);

            answer = (List<RecursoHumano>) q.getResultList();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see IRecursoHumanoDAO#obtenerTopografosporActualizacionId(String)
     * @author javier.barajas
     */
    @Override
    public List<RecursoHumano> obtenerTopografosporActualizacionId(
        Long actualizacionId) {
        List<RecursoHumano> answer = new ArrayList<RecursoHumano>();

        try {
            Query q = entityManager
                .createQuery("SELECT rh " +
                    " FROM RecursoHumano rh JOIN FETCH rh.actualizacionResponsable ar " +
                    " JOIN FETCH ar.actualizacion a " +
                    " WHERE a.id = :actualizacionId " +
                    " AND rh.actividad = :actividad ");
            q.setParameter("actualizacionId", actualizacionId);
            q.setParameter("actividad", EActualizacionContratoActividad.TOPOGRAFO.getCodigo());

            answer = (List<RecursoHumano>) q.getResultList();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
    }

    /**
     * @see IRecursoHumanoDAO#obtenerRecuroHumanoporActualizacionId(String)
     * @author javier.barajas
     */
    @Override
    public List<RecursoHumano> obtenerRecuroHumanoporActualizacionId(
        Long actualizacionId, String actividad) {

        List<RecursoHumano> answer = new ArrayList<RecursoHumano>();

        try {
            Query q = entityManager
                .createQuery("SELECT rh " +
                    " FROM RecursoHumano rh JOIN FETCH rh.actualizacionResponsable ar " +
                    " JOIN FETCH ar.actualizacion a " +
                    " WHERE a.id = :actualizacionId " +
                    " AND rh.actividad = :actividad ");
            q.setParameter("actualizacionId", actualizacionId);
            q.setParameter("actividad", actividad);

            answer = (List<RecursoHumano>) q.getResultList();

            return answer;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @see IRecursoHumanoDAO#obtenerRecursoHumanoByIdentificacionYActualizacionId(String, Long)
     * @author andres.eslava
     */
    public RecursoHumano obtenerRecursoHumanoByIdentificacionYActualizacionId(
        String identificacionRecursoHumano, Long actualizacioId) {

        LOGGER.debug(
            "RecursoHumanoDAOBean#obtenerRecursoHumanoByIdentificacionYActualizacionId...INICIA");

        try {
            String stringQuery = "select distinct rh " +
                "from RecursoHumano rh " +
                "join rh.actualizacionResponsable ar " +
                "join ar.actualizacion act " +
                "where act.id=:actualizacionId  " +
                "and rh.documentoIdentificacion=:identificacion";
            Query query = this.getEntityManager().createQuery(stringQuery);
            query.setParameter("identificacion", identificacionRecursoHumano);
            query.setParameter("actualizacionId", actualizacioId);
            RecursoHumano unRecursoHumano = (RecursoHumano) query.getSingleResult();
            return unRecursoHumano;
        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        } finally {
            LOGGER.debug(
                "RecursoHumanoDAOBean#obtenerRecursoHumanoByIdentificacionYActualizacionId...FINALIZA");
        }

    }

}
