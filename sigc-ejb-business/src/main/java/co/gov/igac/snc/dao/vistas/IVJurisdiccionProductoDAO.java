package co.gov.igac.snc.dao.vistas;

import javax.ejb.Local;

import co.gov.igac.snc.persistence.entity.vistas.VJurisdiccionProducto;

/**
 * Servicio de persistencia de la vista V_JURISDICCION_PRODUCTO del esquema SNC_APLICACION
 *
 * @author jamir.avila
 */
@Local
public interface IVJurisdiccionProductoDAO {

    /**
     * Este método permite obtener la JurisdicciónProducto, incluyendo la territorial,
     * correspondiente a un municipio a partir de su código.
     *
     * @param codigoMunicipio código del municipio para el que se desea la JurisdiccionProducto.
     * @return el objeto VJurisdiccionProducto correspondiente al código de municipio proporcionado
     * o null en caso de que no haya correspondencia.
     * @author jamir.avila.
     */
    public VJurisdiccionProducto recuperarJurisdiccionProductoPorCodigoMunicipio(
        String codigoMunicipio);
}
