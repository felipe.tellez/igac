package co.gov.igac.snc.dao.formacion.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.formacion.IDecretoAvaluoAnualDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IFormacionLocal;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;

/**
 * Implementación de los métodos para los servicios de persistencia del objeto
 * {@link DecretoAvaluoAnual} .
 *
 * @author david.cifuentes
 */
@Stateless
public class DecretoAvaluoAnualDAOBean extends
    GenericDAOWithJPA<DecretoAvaluoAnual, Long> implements
    IDecretoAvaluoAnualDAO {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DecretoAvaluoAnualDAOBean.class);

    /**
     * @see IFormacionLocal#buscarDecretoActual
     * @author david.cifuentes
     */
    @Override
    public DecretoAvaluoAnual buscarDecretoActual() {
        LOGGER.debug("DecretoAvaluoAnualDAOBean#buscarDecretoActual");

        DecretoAvaluoAnual answer = null;
        String queryString;
        Query query;

        try {
            // Capturar actual.
            Date fechaActual = new Date(System.currentTimeMillis());
            SimpleDateFormat formatoAnio = new SimpleDateFormat("yyyy");
            String anio = formatoAnio.format(fechaActual);

            // Cálculo de intervalo de fechas
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat(
                "dd/MM/yyyy");
            String strFechaInicial = "01/01/" + anio;
            String strFechaFinal = "31/12/" + anio;

            Date fechaInicioVigencia = formatoDelTexto.parse(strFechaInicial);
            Date fechaFinalVigencia = formatoDelTexto.parse(strFechaFinal);

            queryString = "SELECT DISTINCT daa FROM DecretoAvaluoAnual daa" +
                " LEFT JOIN FETCH daa.decretoCondicions" +
                " WHERE daa.vigencia BETWEEN :fechaInicioVigencia AND :fechaFinalVigencia";

            query = entityManager.createQuery(queryString);
            query.setParameter("fechaInicioVigencia", fechaInicioVigencia);
            query.setParameter("fechaFinalVigencia", fechaFinalVigencia);

            LOGGER.debug(queryString);
            answer = (DecretoAvaluoAnual) query.getSingleResult();

        } catch (ExcepcionSNC e) {
            LOGGER.error("ERROR: DecretoAvaluoAnualDAOBean#buscarDecretoActual: " +
                e.getMessage());
            throw (e);
        } catch (ParseException e) {
            LOGGER.error("Error realizando la conversión de fechas en " +
                "DecretoAvaluoAnualDAOBean#buscarDecretoActual: " +
                e.getMessage());
        } catch (NoResultException nre) {
            LOGGER.error("Error: " +
                "DecretoAvaluoAnualDAOBean#buscarDecretoActual: " +
                nre.getMessage());
        }
        return answer;
    }

    /** ---------------------------------------------------------- */
    /**
     * @see IFormacionLocal#buscarDecretoActual()
     * @author david.cifuentes
     */
    @Override
    public DecretoAvaluoAnual buscarDecretoPorVigencia(String anioVigencia) {
        LOGGER.debug("DecretoAvaluoAnualDAOBean#buscarDecretoPorVigencia");

        DecretoAvaluoAnual answer = null;
        String queryString;
        Query query;

        try {
            // Cálculo de intervalo de fechas
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat(
                "dd/MM/yyyy");
            String strFechaInicial = "01/01/" + anioVigencia;
            String strFechaFinal = "31/12/" + anioVigencia;

            Date fechaInicioVigencia = formatoDelTexto.parse(strFechaInicial);
            Date fechaFinalVigencia = formatoDelTexto.parse(strFechaFinal);

            queryString = "SELECT DISTINCT daa FROM DecretoAvaluoAnual daa" +
                " LEFT JOIN FETCH daa.decretoCondicions" +
                " WHERE daa.vigencia BETWEEN :fechaInicioVigencia AND :fechaFinalVigencia";

            query = entityManager.createQuery(queryString);
            query.setParameter("fechaInicioVigencia", fechaInicioVigencia);
            query.setParameter("fechaFinalVigencia", fechaFinalVigencia);

            LOGGER.debug(queryString);
            answer = (DecretoAvaluoAnual) query.getSingleResult();

        } catch (ExcepcionSNC e) {
            LOGGER.error("ERROR: DecretoAvaluoAnualDAOBean#buscarDecretoPorVigencia: " +
                e.getMessage());
            throw (e);
        } catch (ParseException e) {
            LOGGER.error("Error realizando la conversión de fechas en " +
                "DecretoAvaluoAnualDAOBean#buscarDecretoPorVigencia: " +
                e.getMessage());
        } catch (NoResultException nre) {
            LOGGER.error("Error: " +
                "DecretoAvaluoAnualDAOBean#buscarDecretoPorVigencia: " +
                nre.getMessage());
        }
        return answer;
    }

}
