/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.iper.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.iper.IInfInterrelacionadaReDAO;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionadaRe;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;

/**
 * Implementación de los métodos de bd de la tabla COLINDANTE
 *
 * @author pedro.garcia
 * @version 2.0
 */
@Stateless
public class InfInterrelacionadaReDAOBean extends GenericDAOWithJPA<InfInterrelacionadaRe, Long>
    implements IInfInterrelacionadaReDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfInterrelacionadaReDAOBean.class);

//--------------------------------------------------------------------------------------------------
    public List<InfInterrelacionadaRe> getTramites(FiltroDatosConsultaTramitesIper filtro) {
        try {
            String sql = "SELECT i FROM InfInterrelacionadaRe i " +
                " WHERE i.resultado='RECHAZADO' and i.estado<>'En proceso' and i.estadoInterrelacion='Interrelacionado' ";

            if (filtro.getDepartamentoId() != null && !filtro.getDepartamentoId().equals("")) {
                sql += " AND i.codigoDepartamento = :codigoDepartamento ";
            }
            if (filtro.getMunicipioId() != null && !filtro.getMunicipioId().equals("")) {
                sql += " AND i.codigoMunicipio = :codigoMunicipio ";
            }
            if (filtro.getCirculoRegistral() != null && !filtro.getCirculoRegistral().equals("")) {
                sql += " AND i.circuloRegistral = :circuloRegistral ";
            }
            if (filtro.getMatricula() != null && !filtro.getMatricula().equals("")) {
                sql += " AND i.matricula = :matricula ";
            }

            Query query = entityManager.createQuery(sql);

            if (filtro.getDepartamentoId() != null && !filtro.getDepartamentoId().equals("")) {
                query.setParameter("codigoDepartamento", filtro.getDepartamentoId());
            }
            if (filtro.getMunicipioId() != null && !filtro.getMunicipioId().equals("")) {
                query.setParameter("codigoMunicipio", filtro.getMunicipioId());
            }
            if (filtro.getCirculoRegistral() != null && !filtro.getCirculoRegistral().equals("")) {
                query.setParameter("circuloRegistral", filtro.getCirculoRegistral());
            }
            if (filtro.getMatricula() != null && !filtro.getMatricula().equals("")) {
                query.setParameter("matricula", filtro.getMatricula());
            }

            return (List<InfInterrelacionadaRe>) query.getResultList();

        } catch (NoResultException e) {
            LOGGER.error(e.getMessage());
            return null;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Error al buscar InfInterrelacionada");
        }
    }

//end of class
}
