/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.generales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.generales.IEstructuraOrganizacionalDAO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import org.hibernate.Hibernate;

/**
 * En la tabla Estructura_Organizacional se encuentran las Territoriales Esta clase es el DAO para
 * esa tabla
 *
 * @author pedro.garcia
 * @modified juan.agudelo ubicación en el paquete correspondiente al esquema de DB
 *
 */
@Stateless
public class EstructuraOrganizacionalDAOBean extends GenericDAOWithJPA<EstructuraOrganizacional, String>
    implements IEstructuraOrganizacionalDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        EstructuraOrganizacionalDAOBean.class);

    /**
     * @see IEstructuraOrganizacionalDAO#findAll()
     */
    public List<EstructuraOrganizacional> findAll() {

        ArrayList<EstructuraOrganizacional> estructurasOrganizacionales;
        estructurasOrganizacionales = (ArrayList<EstructuraOrganizacional>) super.findAll();

        return estructurasOrganizacionales;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> getDependencias(
        EEstructuraOrganizacional tipo, String codigoPadre) {
        Query query = this.entityManager
            .createQuery(
                "Select eo from EstructuraOrganizacional eo where eo.tipo = :tipo and eo.estructuraOrganizacionalCod= :codigoPadre");
        query.setParameter("tipo", tipo.getTipo());
        query.setParameter("codigoPadre", codigoPadre);
        return query.getResultList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IEstructuraOrganizacionalDAO#findTerritorialesAll()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> findTerritorialesAll() {
        Query query;
        String queryToExec;
        List<EstructuraOrganizacional> answer;

        LOGGER.debug(LOGGER.getClass().getName() + "::" + "findTerritorialesAll");
        queryToExec = "Select eo from EstructuraOrganizacional eo where eo.tipo = :tipo";
        query = this.entityManager.createQuery(queryToExec);
        query.setParameter("tipo", EEstructuraOrganizacional.DIRECCION_TERRITORIAL.getTipo());
        answer = query.getResultList();

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see IEstructuraOrganizacionalDAO#findOutIfIsSede(java.lang.String)
     */
    @Override
    public boolean findOutIfIsSede(String codigoMunicipio, String idTerritorial) {

        LOGGER.debug("on EstructuraOrganizacionalDAOBean#findIfIsSede ...");
        boolean answer = false;
        String queryString, tipoEO;
        Query query;
        Object count;
        Long countL;

        queryString = "SELECT count(eo) FROM EstructuraOrganizacional eo " +
            " WHERE eo.municipio.codigo = :codMuniP AND eo.tipo = :eoTipoP " +
            " AND eo.codigo = :eoCodP";

        tipoEO = EEstructuraOrganizacional.DIRECCION_TERRITORIAL.getTipo();

        try {
            query = this.entityManager.createQuery(queryString);
            query.setParameter("codMuniP", codigoMunicipio);
            query.setParameter("eoTipoP", tipoEO);
            query.setParameter("eoCodP", idTerritorial);

            count = query.getSingleResult();
            countL = (Long) count;
            if (countL.intValue() >= 1) {
                answer = true;
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
        LOGGER.debug("... finished.");

        return answer;
    }

    /**
     * @see IEstructuraOrganizacionalDAO#findTerritorialesAndDireccionGeneral()
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> findTerritorialesAndDireccionGeneral() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT eo FROM EstructuraOrganizacional eo");
        sql.append(" WHERE eo.tipo = :tipoDireccion");
        sql.append(" OR eo.tipo = :tipoDelegacion");
        sql.append(" OR eo.tipo = :tipoDireccionTerritorial");

        Query query = this.entityManager.createQuery(sql.toString());
        query.setParameter("tipoDireccion", EEstructuraOrganizacional.DIRECCION.getTipo());
        query.setParameter("tipoDelegacion", EEstructuraOrganizacional.DELEGACION.getTipo());
        query.setParameter("tipoDireccionTerritorial",
            EEstructuraOrganizacional.DIRECCION_TERRITORIAL.getTipo());
        return query.getResultList();
    }

    /**
     * @see IEstructuraOrganizacionalDAO#findSubdireccionesOficinas(String)
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> findSubdireccionesOficinas(
        String codigoPadre) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT eo FROM EstructuraOrganizacional eo");
        sql.append(" WHERE eo.estructuraOrganizacionalCod = :codigoPadre");
        sql.append(" AND ( eo.tipo = :tipoSubdireccion");
        sql.append(" OR eo.tipo = :tipoOficinaApoyo )");

        Query query = this.entityManager.createQuery(sql.toString());
        query.setParameter("codigoPadre", codigoPadre);
        query.setParameter("tipoSubdireccion", EEstructuraOrganizacional.SUBDIRECCION.getTipo());
        query.setParameter("tipoOficinaApoyo", EEstructuraOrganizacional.OFICINA_APOYO.getTipo());
        return query.getResultList();
    }

    /**
     * @see IEstructuraOrganizacionalDAO#findGruposTrabajo(String)
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> findGruposTrabajo(
        String estructuraOrganizacionalCodigo) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT eo FROM EstructuraOrganizacional eo");
        sql.append(" LEFT JOIN FETCH eo.funcionario");
        sql.append(" WHERE eo.estructuraOrganizacionalCod = :estructuraOrganizacionalCodigo");
        sql.append(" AND eo.tipo = :tipoGIT");

        Query query = this.entityManager.createQuery(sql.toString());
        query.setParameter("estructuraOrganizacionalCodigo", estructuraOrganizacionalCodigo);
        query.setParameter("tipoGIT", EEstructuraOrganizacional.GRUPO_INTERNO_TRABAJO.getTipo());
        return query.getResultList();
    }

    // -------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    /**
     * @see
     * IEstructuraOrganizacionalDAO#findMunicipiosTerritorialByCodigoExtructuraOrganizacional(String)
     * @author juan.agudelo
     */
    @Override
    public List<String> findMunicipiosTerritorialByCodigoExtructuraOrganizacional(
        String codigoEO) {
        LOGGER.debug(
            "EstructuraOrganizacionalDAO#findMunicipiosTerritorialByCodigoExtructuraOrganizacional");

        List<String> answer = null;

        String query;

        query = "SELECT DISTINCT eo.codigo FROM EstructuraOrganizacional eo" +
            " WHERE  eo.estructuraOrganizacionalCod = :codigoEstructuraOrganizacional";

        Query q = entityManager.createQuery(query);
        q.setParameter("codigoEstructuraOrganizacional", codigoEO);

        try {
            answer = (List<String>) q.getResultList();
            answer.add(codigoEO);
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    @Override
    public String findEstructuraOrgByMunicipioCod(String municipioCod) {

        LOGGER.debug("EstructuraOrganizacionalDAO#findEstructuraOrgByMunicipioCod");

        String answer = null;
        String query = "SELECT eo.codigo FROM EstructuraOrganizacional eo " //+ " JOIN FETCH eo.municipio m"
            +
             " WHERE eo.municipio.codigo = :municipioCod";

        Query q = entityManager.createQuery(query);
        q.setParameter("municipioCod", municipioCod);

        try {
            answer = (String) q.getSingleResult();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    @Override
    public String findEstructuraOrgSuperiorByMunicipioCod(String municipioCod) {
        LOGGER.debug("EstructuraOrganizacionalDAO#findEstructuraOrgByMunicipioCod");

        String answer = null;
        String query = "SELECT eo.estructuraOrganizacionalCod FROM EstructuraOrganizacional eo " +
            " WHERE eo.municipio.codigo = :municipioCod";

        Query q = entityManager.createQuery(query);
        q.setParameter("municipioCod", municipioCod);

        try {
            answer = (String) q.getSingleResult();

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * @see
     * IEstructuraOrganizacionalDAO#getCodigosEOAsociadosByCodigoEstructuraOrganizacional(String)
     * @author juan.agudelo
     * @version 2.0
     */
    //@Override
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCodigosEOAsociadosByCodigoEstructuraOrganizacional(
        String codigo) {
        LOGGER.debug(
            "EstructuraOrganizacionalDAO#getCodigosEOAsociadosByCodigoEstructuraOrganizacional");
        List<String> answer;

        String query;

        query = "SELECT DISTINCT eo.codigo FROM EstructuraOrganizacional eo" +
            " WHERE eo.codigo = :codigoEstructuraOrganizacional" +
            " OR eo.estructuraOrganizacionalCod = :codigoEstructuraOrganizacional ";

        Query q = entityManager.createQuery(query);
        q.setParameter("codigoEstructuraOrganizacional", codigo);

        try {
            answer = q.getResultList();
        } catch (IndexOutOfBoundsException ie) {
            answer = new ArrayList<String>();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;

    }

    // --------------------------------- //
    /**
     * @see IEstructuraOrganizacionalDAO#buscarEstructuraOrganizacionalPorId(String)
     * @author david.cifuentes
     */
    @Override
    public EstructuraOrganizacional buscarEstructuraOrganizacionalPorCodigo(
        String codigoEstructuraOrganizacional) {

        EstructuraOrganizacional answer;
        String query;

        query = "SELECT DISTINCT eo FROM EstructuraOrganizacional eo" +
            " JOIN FETCH eo.municipio m" +
            " JOIN FETCH m.departamento d" +
            " JOIN FETCH d.pais p" +
            " WHERE eo.codigo = :codigoEstructuraOrganizacional";

        Query q = entityManager.createQuery(query);
        q.setParameter("codigoEstructuraOrganizacional", codigoEstructuraOrganizacional);

        try {
            answer = (EstructuraOrganizacional) q.getSingleResult();
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }

    /**
     * @see IEstructuraOrganizacionalDAO#buscarUOCporCodigoTerritorial(String)
     * @author felipe.cadena
     */
    /*
     * @modified by leidy.gonzalez:: #16091:: 06/04/2016 Se agrega orden de consulta por nombre de
     * las UOC listadas.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> buscarUOCporCodigoTerritorial(
        String codigoTerritorial) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT eo FROM EstructuraOrganizacional eo");
        sql.append(" WHERE eo.estructuraOrganizacionalCod = :codigoTerritorial");
        sql.append(" AND eo.tipo = 'UOC'");
        sql.append(" ORDER BY eo.nombre ASC");

        Query query = this.entityManager.createQuery(sql.toString());
        query.setParameter("codigoTerritorial", codigoTerritorial);
        return query.getResultList();
    }

    /**
     * @see IEstructuraOrganizacionalDAO#buscarEstructuraOrganizacionalByNombre(String,String)
     * @author dumar.penuela
     * @param oUC
     * @param territorial
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<EstructuraOrganizacional> buscarEstructuraOrganizacionalByNombre(
        String oUC, String territorial) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT eo FROM EstructuraOrganizacional eo  WHERE 1 = 1 ");

        if (oUC != null && !oUC.trim().isEmpty()) {
            sql.append("AND UPPER(eo.nombre)= :oUC ");
        }

        if (territorial != null && !territorial.trim().isEmpty()) {
            sql.append("AND UPPER(eo.nombre)= :territorial ");
        }

        Query query = this.entityManager.createQuery(sql.toString());

        if (oUC != null && !oUC.trim().isEmpty()) {
            query.setParameter("oUC", oUC);
        }

        if (territorial != null && !territorial.trim().isEmpty()) {
            query.setParameter("territorial", territorial);
        }

        return query.getResultList();
    }

    // ------------------------------------------------- //
    @SuppressWarnings("unchecked")
    /**
     * @see IEstructuraOrganizacionalDAO#buscarEstructuraOrgPorMunicipioCod(String)
     * @author david.cifuentes
     */
    @Override
    public String buscarEstructuraOrgPorMunicipioCod(String municipioCod) {

        LOGGER.debug("EstructuraOrganizacionalDAO#buscarEstructuraOrgPorMunicipioCod");

        String answer = null;
        List<EstructuraOrganizacional> eos = null;
        String query = "SELECT eo FROM EstructuraOrganizacional eo " +
            " WHERE eo.municipio.codigo LIKE :municipioCod" +
            " AND eo.tipo IN ('UOC', 'DIRECCION TERRITORIAL', 'DIRECCION')";

        Query q = entityManager.createQuery(query);
        q.setParameter("municipioCod", municipioCod.substring(0, 2) + "%");

        try {
            eos = q.getResultList();

            if (eos != null && !eos.isEmpty()) {
                if (eos.size() == 1) {
                    return eos.get(0).getCodigo();
                } else {
                    for (EstructuraOrganizacional eo : eos) {
                        Hibernate.initialize(eo.getMunicipio());
                        if ("UOC".equals(eo.getTipo()) && municipioCod.equals(eo.getMunicipio().
                            getCodigo())) {
                            answer = eo.getCodigo();
                            break;
                        } else {
                            answer = eo.getEstructuraOrganizacionalCod();
                        }
                    }
                }
            }
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100010.getExcepcion(
                LOGGER, e, e.getMessage());
        }
        return answer;
    }
}
