package co.gov.igac.sigc.business.conservacion.test.unit;

import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IHPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.HPredioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.test.unit.BaseTest;

@Test(groups = {"unit"})
public class HPredioDAOBeanTest extends BaseTest {

    private static final Logger LOG = LoggerFactory
        .getLogger(HPredioDAOBeanTest.class);

    private IHPredioDAO dao = new HPredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        try {
            LOG.info("runBeforeClass");
            setUp(BaseTest.EM_TEST_ORACLEDB);
            dao.setEntityManager(em);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    @AfterClass
    public void runAfterClass() {
        try {
            LOG.info("runAfterClass");
            tearDown();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /*
     * //@Test(groups = { "init" }) public void test01SaveMovie() throws Exception {
     * log.debug("test01SaveMovie"); try { dao.getEntityManager().getTransaction().begin(); for (int
     * i = 0; i < 10; i++) { HPredio hpredio = new HPredio(); hpredio.setNumeroPredial("123456789");
     * dao.persist(hpredio); log.debug("id:" + hpredio.getNumeroPredial());
     * assertNotNull(hpredio.getNumeroPredial()); }
     * dao.getEntityManager().getTransaction().commit(); } catch (Exception e) {
     * log.error(e.getMessage(), e); assertTrue(false); }
     *
     * }
     */
    @Test()
    public void findHPredioListByConsecutivoCatastral() {
        LOG.debug("findHPredioListByConsecutivoCatastral");
        try {
            LOG.debug("prueba para la busqueda de un HPredio, apartir de su consecutivo catastral");
            BigDecimal numero = null;
            List<HPredio> awnser = dao.findHPredioListByConsecutivoCatastral(numero);
            Assert.assertTrue(awnser.isEmpty());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /**
     * Método de test para el método
     * buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId
     *
     * @author javier.aponte
     */
    @Test()
    public void testBuscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId() {
        LOG.debug("testBuscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId");
        try {
            Long idTramite = 124836L;
            List<HPredio> awnser = dao.
                buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId(idTramite);
            Assert.assertTrue(awnser.isEmpty());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /**
     * Método de test para el método
     * buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId
     *
     * @author javier.aponte
     */
    @Test()
    public void testBuscarHPredioPorNumeroPredialYEstado() {
        LOG.debug("testBuscarHPredioPorNumeroPredialYEstado");
        try {
            HPredio awnser = dao.buscarHPredioPorNumeroPredialYEstado(
                "080010102000000040002000000000", "ACTIVO");
            Assert.assertNotNull(awnser);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /**
     * Método de test find By Id
     *
     * @author javier.aponte
     */
    @Test()
    public void testFindById() {
        LOG.debug("testBuscarHPredioPorNumeroPredialYEstado");
        try {
            HPredio awnser = dao.findById(13067L);
            Assert.assertNotNull(awnser);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /**
     * Método de test para el método
     * buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId
     *
     * @author javier.aponte
     */
    @Test()
    public void testobtenerUsuarioLogOrigen() {
        LOG.debug("testobtenerUsuarioLogOrigen");
        try {
            String numeroPredial = "080010101000000070004800000000";
            HPredio answer = dao.obtenerEstadoOrigenNumeroPredial(numeroPredial);
            Assert.assertTrue(answer != null);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /**
     * Método de test para el método
     * buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId
     *
     * @author javier.aponte
     */
    @Test()
    public void testobtenerPrediosOrigenPorTramiteId() {
        LOG.debug("testobtenerUsuarioLogOrigen");
        try {
            Long tramiteID = 661745L;
            List<HPredio> answer = dao.obtenerPrediosOrigenPorTramiteId(tramiteID);
            Assert.assertTrue(answer.size() == 3);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /**
     * Método de test para el método
     * buscarHistoricoPrediosProyectadosInscritosOModificadosPorTramiteId
     *
     * @author javier.aponte
     */
    @Test()
    public void testObtenerHPredioCompletoPorId() {
        LOG.debug("testObtenerHPredioCompletoPorId");
        try {
            Long predioID = 1670704L;
            HPredio answer = dao.obtenerHPredioCompletoPorId(predioID);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            assertTrue(false);
        }
    }
}
