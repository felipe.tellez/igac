package co.gov.igac.sigc.business.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPredioBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioBloqueoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.test.unit.BaseTest;

@Test(groups = {"unit"})
public class PredioBloqueoDAOTest extends BaseTest {

    private static final Logger log = LoggerFactory
        .getLogger(PredioBloqueoDAOTest.class);

    private IPredioBloqueoDAO dao = new PredioBloqueoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        log.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        log.info("runAfterClass");
        super.tearDown();
    }

    // @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 60000)
    /*
     * @Test(groups = { "init" }) public void test01SaveMovie() throws Exception {
     * log.debug("test01SaveMovie");
     *
     * dao.getEntityManager().getTransaction().begin(); for (int i = 0; i < 10; i++) { Movie movie =
     * new Movie(); movie.setTitle("my movie"); dao.persist(movie); movie.setDiscs(5);
     * dao.update(movie); log.debug("id:" + movie.getId()); assertNotNull(movie.getId()); }
     * dao.getEntityManager().getTransaction().commit(); }
     */
    @Test()
    public void testFindCurrentPredioBloqueo() throws Exception {
        log.debug("testFindCurrentPredioBloqueo");
        PredioBloqueo movies = dao.findCurrentPredioBloqueo("");// .loadAll();//
        // .findByTitle("");
        // log.debug("Size:"+movies.size());
        assertNotNull(movies);
    }

}
