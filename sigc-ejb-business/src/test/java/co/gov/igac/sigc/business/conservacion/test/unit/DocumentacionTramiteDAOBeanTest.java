package co.gov.igac.sigc.business.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.IDocumentacionTramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.DocumentacionTramiteDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.test.unit.BaseTest;

//TODO Juan Gabriel cambiar esta clase al paquete que corresponde
public class DocumentacionTramiteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DocumentacionTramiteDAOBeanTest.class);

    private IDocumentacionTramiteDAO dao = new DocumentacionTramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testBuscarDocumentosTramitePorNumeroDeRadicacion() {
        LOGGER.debug("***************************** ");
        LOGGER.debug("testBuscarDocumentosTramitePorNumeroDeRadicacion");

        List<TramiteDocumentacion> tramiteDocumentacion = new ArrayList<TramiteDocumentacion>();

        try {
            tramiteDocumentacion = dao.buscarDocumentosTramitePorNumeroDeRadicacion("111");
            LOGGER.debug("*****************************");

            assertNotNull(tramiteDocumentacion);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }

    }

}
