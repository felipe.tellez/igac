/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.fail;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.impl.DocumentoDAOBean;
import co.gov.igac.snc.dao.tramite.ISolicitanteDAO;
import co.gov.igac.snc.dao.tramite.impl.SolicitanteDAOBean;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.dao.util.ISNCProcedimientoDAO;
import co.gov.igac.snc.dao.util.ProcedimientoAlmacenadoDAO;
import co.gov.igac.snc.dao.util.SNCProcedimientoDAO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import java.sql.Timestamp;
import java.util.LinkedList;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Clase para pruebas de los procedimientos almacenados para SNC
 *
 * @author pedro.garcia
 * @author juan.mendez
 */
public class SNCProcedimientoDAOTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SNCProcedimientoDAOTests.class);

    private ISNCProcedimientoDAO dao = new SNCProcedimientoDAO();
    private ISolicitanteDAO solicitanteDao = new SolicitanteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
        this.solicitanteDao.setEntityManager(this.em);

        //this.dao.solicitanteDao(solicitanteDao);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testObtenerRadicacionCorrespondencia() {
        try {
            String numeroRadicacionCorrespondencia = "8002011ER00003847";
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("pruatlantico2");
            Solicitud resultado = dao
                .obtenerRadicacionCorrespondencia(numeroRadicacionCorrespondencia, usuario);
            assertNotNull(resultado);
            LOGGER.debug("resultado:" + resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testObtenerRadicacionCorrespondenciaRetornaNulo() {
        try {
            String numeroRadicacionCorrespondencia = "54545454654654654676546546";
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("pruatlantico2");
            Solicitud resultado = dao
                .obtenerRadicacionCorrespondencia(numeroRadicacionCorrespondencia, usuario);
            assertNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     */
    @Test
    public void testObtenerRadicacionCorrespondenciaRetornaNuloV2() {
        try {
            Solicitud resultado = dao.obtenerRadicacionCorrespondencia(null, null);
            assertNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de documento en alfresco
     *
     * @author juan.agudelo
     */
    @Test
    public void testGenerarNumeracion() {
        LOGGER.debug("tests: SNCProcedimientoDAOTests#testGenerarNumeracion");

        String codigoTerritorial = "6368";
        String departamento = "25";
        String municipio = "25754";
        Calendar fecha = Calendar.getInstance();
        List<Object> paramtrosCodigo = new ArrayList<Object>();
        BigDecimal bd = new BigDecimal(
            ENumeraciones.NUMERACION_OFERTAS_INMOBILIARIAS.getId());
        BigDecimal anio = new BigDecimal(fecha.get(Calendar.YEAR));

        paramtrosCodigo.add(bd);
        paramtrosCodigo.add(codigoTerritorial);
        paramtrosCodigo.add(departamento);
        paramtrosCodigo.add(municipio);
        paramtrosCodigo.add(anio);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Object[] answer = this.dao.generarNumeracion(paramtrosCodigo);

            if (answer != null && answer.length > 0) {
                LOGGER.debug("N0 " + answer[0]);
                LOGGER.debug("N1 " + answer[1]);
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ---------------------------//
    /**
     * Test que calcula los predios afectados por los condicionales de un decreto.
     *
     * @author david.cifuentes
     */
    @Test
    public void testCalcularAfectadosDecreto() {
        LOGGER.debug("SNCProcedimientoDAOTests#testCalcularAfectadosDecreto");
        try {
            Long decretoId = 2L;
            Object[] resultado = dao.calcularAfectadosDecreto(decretoId);
            if (resultado != null && resultado.length > 0) {
                LOGGER.debug("Hay decretos afectados.");
            }
            assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------//
    /**
     * Test que que recalcula los avaluos para un predio.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testReproyectarAvaluos() {
        LOGGER.debug("SNCProcedimientoDAOTests#testReproyectarAvaluos");
        try {
            Long tramiteId = 19741L;
            Long predioId = 172675L;
            String dateStr = "09/03/2008";
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("javier.aponte");

            Date fechaDelCalculo = (Date) formatter.parse(dateStr);

            Object[] resultado = dao.reproyectarAvaluos(tramiteId, predioId,
                fechaDelCalculo, usuario);
            if (resultado == null) {
                LOGGER.debug("Recálculo satisfactorio!");
            } else if (resultado.length > 0) {
                for (int i = 0; i < resultado.length; i++) {
                    if (((ArrayList<Object>) resultado[i]).size() == 0) {
                        LOGGER.debug("Recálculo satisfactorio!");
                        break;
                    } else {
                        LOGGER.debug("Error en la ejecución del recálculo");
                        break;
                    }
                }
            }
            assertNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarPrediosPorCriterios() {

        LOGGER.debug("unit test SNCProcedimientoDAOTests#testBuscarPrediosPorCriterios ...");
        List<Object[]> answer;

        FiltroDatosConsultaPredio datosConsultaPredio;

        // D: prueba para contar
        datosConsultaPredio = new FiltroDatosConsultaPredio();
        // datosConsultaPredio.setUsoConstruccionId("'1'");
        // datosConsultaPredio.setDestinoPredioId("D");
        datosConsultaPredio.setNip("19453015");
        datosConsultaPredio.setTerritorialId("6040");
        answer = this.dao.buscarPrediosPorCriterios(datosConsultaPredio, true);

        LOGGER.debug("encontró " + answer.size() + " predios");

    }

    /**
     * @author javier.barajas
     */
    @Test
    public void testBuscarPrediosPorCriteriosRangoNumeroPredial() {

        LOGGER.debug(
            "unit test SNCProcedimientoDAOTests#testBuscarPrediosPorCriteriosRangoNumeroPredial ...");
        List<Object[]> answer;
        FiltroGenerarReportes datosConsultaPredio;

        // D: prueba para contar
        datosConsultaPredio = new FiltroGenerarReportes();

        datosConsultaPredio.setDepartamentoId("08");
        datosConsultaPredio.setMunicipioId("08001");

        /*
         * datosConsultaPredio.setNumeroPredialS1("08");
         * datosConsultaPredio.setNumeroPredialS1f("08");
         * datosConsultaPredio.setNumeroPredialS2("001");
         * datosConsultaPredio.setNumeroPredialS2f("001");
         * datosConsultaPredio.setNumeroPredialS3("01");
         * datosConsultaPredio.setNumeroPredialS3f("01");
         * datosConsultaPredio.setNumeroPredialS4("01");
         * datosConsultaPredio.setNumeroPredialS4f("01");
         * datosConsultaPredio.setNumeroPredialS5("00");
         * datosConsultaPredio.setNumeroPredialS5f("00");
         * datosConsultaPredio.setNumeroPredialS6("00");
         * datosConsultaPredio.setNumeroPredialS6f("00");
         * datosConsultaPredio.setNumeroPredialS7("0002");
         * datosConsultaPredio.setNumeroPredialS7f("0002");
         * datosConsultaPredio.setNumeroPredialS8("0001");
         * datosConsultaPredio.setNumeroPredialS8f("0021");
         *
         */
        answer = this.dao.buscarPrediosPorCriteriosRangoNumeroPredial(datosConsultaPredio, false, 0,
            null);
        LOGGER.debug("encontró " + answer.size() + " predios");

    }

    /**
     * @author ariel.ortiz
     */
    @Test
    public void testCalcularMuestrasControl() {

        LOGGER.debug("unit test SNCProcedimientoDAOTests#testCalcularMuestrasControl ...");
        Object[] answer;

        String municipioCod = "08001";
        BigDecimal valorPedidoD = null;
        BigDecimal areaTerrenoD = null;
        BigDecimal areaConstruidaD = new BigDecimal(100);
        BigDecimal valorCalculadoD = null;
        String login = "pruatlantico20";
        try {
            answer = this.dao.calcularMuestrasControl(municipioCod,
                valorPedidoD, areaTerrenoD, areaConstruidaD,
                valorCalculadoD, login);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author ariel.ortiz
     */
    @Test
    public void testGuardarMuestrasControl() {

        LOGGER.debug("unit test SNCProcedimientoDAOTests#testGuardarMuestrasControl ...");

        Object[] errors = null;
        Long controlId = 108L;

        try {
            errors = this.dao.guardarCalculoMuestras(controlId);
        } catch (Exception e) {
            LOGGER.error(errors.toString());
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author ariel.ortiz
     */
    @Test
    public void testBorrarCalculoMuestras() {

        LOGGER.debug("unit test SNCProcedimientoDAOTests#testBorrarMuestrasControl ...");

        Object[] errors = null;
        Long controlId = 171L;

        try {
            errors = this.dao.borrarCalculoMuestras(controlId);
        } catch (Exception e) {
            LOGGER.error(errors.toString());
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // ---------------------------//
    /**
     * Test que valida el condicional de un decreto condicion
     *
     * @author javier.aponte
     */
    @Test
    public void testValidarCondicional() {
        LOGGER.debug("SNCProcedimientoDAOTests#testValidarCondicional");
        try {
            Long decretoId = 63L;
            Object[] resultado = dao.validarCondicionalDecretoCondicion(decretoId);
            if (resultado != null && resultado.length > 0) {
                LOGGER.debug("El objeto de resultado no está vacio");
            }
            assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Metodo para probar la generacion de una proyeccion
     *
     * @author fredy.wilches
     */
    @Test
    public void testGenerarProyeccion() {
        LOGGER.debug("SNCProcedimientoDAOTests#testGenerarProyeccion");
        try {
            Long tramiteId = 17090L;

            // FGWL: ESTA PRUEBA NO FUNCIONA PORQUE EN EL DAO FALTA SETEAR 
            //	@Resource
            //  private SessionContext context;
            Object mensajes[] = dao.generarProyeccion(tramiteId);
            for (Object o : mensajes) {
                List l = (List) o;
                if (l.size() > 0) {
                    assertNotNull(null);
                } else {
                    assertNotNull("");
                }
            }
            /* if (resultado != null && resultado.length > 0) { LOGGER.debug("El objeto de resultado
             * no está vacio"); } assertNotNull(resultado); */
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Método para probar reclasificacion de tramite
     *
     * @author rodrigo.hernandez
     *
     *
     */
    @Test
    public void testReclasificarTramiteODocumento() {
        LOGGER.debug("SNCProcedimientoDAOTests#testReclasificarTramiteODocumento");
        try {
            String codigoTramite = "15";
            String numeroRadicacion = "60402012ER00000005";
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("pruatlantico2");

            dao.reclasificarTramiteODocumento(numeroRadicacion, codigoTramite, usuario);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test para el metodo obtenerValorunidadTerreno
     */
    @Test
    public void testObtenerValorUnidadTerreno() {
        LOGGER.debug("SNCProcedimientoDAOTests#testObtenerValorUnidadTerreno");
        try {
            String zonacodigo = "2575401";
            String destino = "C";
            String zonaGeoEconomica = "01";
            Long predioId = 39145l;

            Double valor = dao.obtenerValorUnidadTerreno(zonacodigo, destino, zonaGeoEconomica,
                predioId);
            assertNotNull(valor);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------//
    /**
     * Test que que liquida los avaluos para un predio.
     *
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testLiquidarAvaluosParaUnPredio() {
        LOGGER.debug("SNCProcedimientoDAOTests#testReproyectarAvaluos");
        try {
            Long tramiteId = 19741L;
            Long predioId = 172675L;
            String dateStr = "09/03/2008";
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("javier.aponte");

            Date fechaDelCalculo = (Date) formatter.parse(dateStr);

            Object[] resultado = dao.liquidarAvaluosParaUnPredio(tramiteId, predioId,
                fechaDelCalculo, usuario);
            if (resultado == null) {
                LOGGER.debug("Liquidación exitosa!");
            } else if (resultado.length > 0) {
                for (int i = 0; i < resultado.length; i++) {
                    if (((ArrayList<Object>) resultado[i]).size() == 0) {
                        LOGGER.debug("Liquidación exitosa!");
                        break;
                    } else {
                        LOGGER.debug("Error en la ejecución de la liquidación");
                        break;
                    }
                }
            }
            assertNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------//
    /**
     * Test que valida el condicional de un decreto condicion
     *
     * @author javier.aponte
     * @modified leidy.gonzalez
     */
    @Test
    public void testDatosBasicosReportePredial() {
        LOGGER.debug("SNCProcedimientoDAOTests#testDatosBasicosReportePredial");
        try {
            Long repReporteId = 19741L;
            RepReporteEjecucion repReporteEjecucion = new RepReporteEjecucion();

            repReporteEjecucion.setDepartamentoCodigo("68");
            repReporteEjecucion.setMunicipioCodigo("68322");
            /* repReporteEjecucion.setReporteId(repReporteId);
             * repReporteEjecucion.setLogProcesoId(logProcesoId); */

            Object[] resultado = dao.generarReportePredialDatosBasicos(repReporteEjecucion);
            if (resultado != null && resultado.length > 0) {
                LOGGER.debug("El objeto de resultado no está vacio");
            }
            assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * la gracia de esta prueba es que se tenga una transacción en el flujo, se ejecute el sp -de
     * forma aislada- y el flujo pueda continuar con la transacción original haciendo operaciones
     * que requieran commit.
     *
     * @author pedro.garcia
     */
    @Test
    public void testEjecutarSPAislado() {

        LOGGER.debug("unit test SNCProcedimientoDAOTests#testEjecutarSPAislado ...");

        Documento doc;
        IDocumentoDAO documentoDao;
        Long idDocumento;
        Object[] answer;

        idDocumento = 50482L;
        documentoDao = new DocumentoDAOBean();
        documentoDao.setEntityManager(this.em);

        doc = null;

        try {
            //this.em.getTransaction();
            doc = documentoDao.buscarDocumentoCompletoPorId(idDocumento);
        } catch (Exception ex) {
            LOGGER.error("no se pudo encontrar el Documento con id " + idDocumento + " : " +
                ex.getMessage());
        }

        try {
            //D: esto debe generarr un error en el sp y que se trate de hacer rollback en esa transacción ...
//            answer = this.dao.ejecutarSPAislado("0", "unit test", true);

            // ... y se supone, ya que se hizo en una transacción nueva, que esto debería funcionar
            documentoDao.update(doc);

            LOGGER.debug("... passed!");
            Assert.assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error("error en sl sp: " + ex.getMessage());
            fail();
        }

        //this.entityManager.getTransaction().begin();
        //this.entityManager.getTransaction().commit();
    }

    /**
     * Ejecuta una prueba del procedimiento almacenado consulta catastral necesario para el WS
     * consulta catastral.
     *
     * @author andres.eslava
     */
    @Test
    public void testBuscarPrediosWS() {
        LOGGER.debug("INICIA ... SNCProcedimientoDAOTests#testBuscarPrediosWS");
        try {

            FiltroConsultaCatastralWebService filtro = new FiltroConsultaCatastralWebService();
            filtro.setPredio_numeroPredialNuevo("257540104000001760001500000004");
            Object[] resultados = dao.buscarPrediosWS(filtro, 1L);

            LOGGER.debug("Datos resultado 1" + String.valueOf(((ArrayList<Object[]>) resultados[0]).
                size()));
            LOGGER.debug("Datos resultado 1" + String.valueOf(((ArrayList<Object[]>) resultados[1]).
                size()));
            LOGGER.debug("Datos resultado 1" + String.valueOf(((ArrayList<Object[]>) resultados[2]).
                size()));
            LOGGER.debug("Datos resultado 1" + String.valueOf(((ArrayList<Object[]>) resultados[3]).
                size()));

            assertNull(resultados);
            Assert.assertTrue(resultados.length > 0);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        } finally {
            LOGGER.debug("FINALIZA ...SNCProcedimientoDAOTests#testBuscarPrediosWS");
        }
    }

    /**
     * @author felipe.cadena
     * */
    @Test
    public void testRegistrarAccesoUsuario() {
        LOGGER.debug("INICIA ... SNCProcedimientoDAOTests#testRegistrarAccesoUsuario");
        try {

            LogAcceso log = new LogAcceso(
                "usuario",
                "INICIADA",
                "192.170.12.5",
                "FIREFOX");

            Long l = this.dao.registrarAccesoUsuario(log);

            LOGGER.debug("res " + l);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        } finally {
            LOGGER.debug("FINALIZA ...SNCProcedimientoDAOTests#testBuscarPrediosWS");
        }
    }

    /**
     * @author felipe.cadena
     * */
    @Test
    public void testRegistrarAccionUsuario() {
        LOGGER.debug("INICIA ... SNCProcedimientoDAOTests#testRegistrarAccionUsuario");
        try {

            LogAccion log = new LogAccion(
                1,
                "CONSULTA",
                "MENSAJE",
                "CONSULTA_PREDIOS",
                "atlantico, 080010101000154");

            this.dao.registrarAccionUsuario(log);

            LOGGER.debug("res ");

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        } finally {
            LOGGER.debug("FINALIZA ...SNCProcedimientoDAOTests#testRegistrarAccionUsuario");
        }
    }

    /**
     * calcula el procentaje de liquidacion de un predio y una vigencia
     */
    @Test
    public void testCalcularPorcentajeIncrementoPredio() {

        LOGGER.debug("TEST DE CalcularPorcentajeIncrementoPredio");

        try {

            //String dateStr = "2009";
            String dateStr = "01/01/2015";
            Long predioId = 33077L;

            double answer = this.dao.CalcularPorcentajeIncrementoPredio(dateStr, predioId);

            LOGGER.debug(Double.toString(answer));
        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * test para establecer el año y número de última resolución a los predios de un trámite
     *
     * @author javier.aponte
     */
    @Test
    public void testEstablecerAnioYNumeroUltimaResolucion() {

        LOGGER.debug("testEstablecerAnioYNumeroUltimaResolucion");
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("pruatlantico3");

        try {

            Object answer[] = this.dao.establecerAnioYNumeroUltimaResolucion(122182L, 2016, "123",
                usuario);

            assertNotNull(answer);

        } catch (ExcepcionSNC e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100001.getExcepcion(
                LOGGER, e, e.getMessage());
        }

    }

    /**
     * @author felipe.cadena
     * */
    @Test
    public void testConsultarJobsPendientesPorProcesar() {
        LOGGER.debug("INICIA ... SNCProcedimientoDAOTests#testConsultarJobsPendientesPorProcesar");
        try {

            List<ProductoCatastralJob> answer = null;

            answer = this.dao.consultarJobsPendientesPorProcesar(
                10,
                ProductoCatastralJob.AGS_TERMINADO,
                ProductoCatastralJob.SNC_EN_EJECUCION);

            LOGGER.debug("res " + answer.size());
            ProductoCatastralJob pcj = answer.get(0);
            LOGGER.debug("Job 0 " + pcj.getResultado());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        } finally {
            LOGGER.debug(
                "FINALIZA ...SNCProcedimientoDAOTests#testConsultarJobsPendientesPorProcesar");
        }
    }

    /**
     * Evalua la ejecución de un procedimiento almacenado pasando un número insuficiente de
     * parámetros
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testEjecutaProcedimientoConsultarZonasFisYGeoPorVigenciaYZona() {
        LOGGER.debug("testEjecutaProcedimientoConsultarZonasFisYGeoPorVigenciaYZona");
        try {
            List<Object> parametros = new ArrayList<Object>();

            Date vigenciaDate = new Date(2013, 01, 01);

            java.sql.Timestamp fechaVigenciaTimestamp = new Timestamp(
                vigenciaDate.getTime());

            LOGGER.info("... ejecutando " + EProcedimientoAlmacenadoFuncion.PRC_ZG_ZHF_NO_AUT);

            Object answer[] = this.dao.calculoZonasFisicasYGeoeconomicas(
                fechaVigenciaTimestamp, "0800101");

            assertTrue(false, "El dao debería lanzar una excepción ");
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            assertEquals(e.getCodigoDeExcepcion(), SncBusinessServiceExceptions.EXCEPCION_100005.
                getCodigoDeExcepcion());
        }
    }

// end of class
}
