/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.formacion.test.unit;

import co.gov.igac.snc.dao.formacion.ICalificacionAnexoDAO;
import co.gov.igac.snc.dao.formacion.impl.CalificacionAnexoDAOBean;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import javax.persistence.Query;
import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class CalificacionAnexoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalificacionAnexoDAOBeanTest.class);

    private ICalificacionAnexoDAO dao = new CalificacionAnexoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia pre:
     */
    @Test
    public void testFindByUsoConstruccionId() {
        LOGGER.debug("unit testing CalificacionAnexoDAOBeanTest#testFindByUsoConstruccionId...");

        //OJO: hay que pasar el tipo de parámetro que se espera... NO es como en sql que no importa
        //     si la columna es long y hago set paramater con String
        Long ucId = 3l;

        List<CalificacionAnexo> results;
        Query query;

        try {
            query = this.dao.getEntityManager().createNamedQuery("findByUsoConstruccionId");
            query.setParameter("idUC", ucId);
            results = query.getResultList();
            Assert.assertTrue(!results.isEmpty());
            LOGGER.debug("passed (partially)!. Results size = " + results.size());

            Assert.assertNotNull(results.get(0).getUsoConstruccion().getId());
            LOGGER.debug("... ok UsoConstruccion fetched");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }

    }

}
