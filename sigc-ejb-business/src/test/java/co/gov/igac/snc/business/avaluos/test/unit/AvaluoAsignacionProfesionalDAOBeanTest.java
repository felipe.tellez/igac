/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import co.gov.igac.snc.dao.avaluos.IAvaluoAsignacionProfesionalDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoAsignacionProfesionalDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Clase para probar metodos de la clase AvaluoAsignacionProfesionalDAOBean
 *
 * @author pedro.garcia
 */
public class AvaluoAsignacionProfesionalDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoAsignacionProfesionalDAOBeanTest.class);

    private IAvaluoAsignacionProfesionalDAO dao = new AvaluoAsignacionProfesionalDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testActualizarOrdenPractica() {
        LOGGER.debug(
            "unit test AvaluoAsignacionProfesionalDAOBeanTest#testActualizarOrdenPractica ...");

        String numeroOrdenPractica;
        ArrayList<Long> avaluos;

        avaluos = new ArrayList<Long>();
        avaluos.add(172l);
        numeroOrdenPractica = "OP-0000118";

        try {
            this.em.getTransaction().begin();
            this.dao.actualizarOrdenPractica(numeroOrdenPractica, avaluos);
            this.em.getTransaction().commit();
            assertTrue(true);
            LOGGER.debug(" ... passed!!");
        } catch (Exception ex) {
            fail("ocurrió un error.");
            LOGGER.error("excepción: " + ex.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerDeUltimaAsignacion() {
        LOGGER.debug(
            "unit test AvaluoAsignacionProfesionalDAOBeanTest#testObtenerDeUltimaAsignacion ...");

        ArrayList<AvaluoAsignacionProfesional> asignaciones;
        long idAvaluo;
        String codActividad;

        codActividad = EAvaluoAsignacionProfActi.AVALUAR.getCodigo();
        idAvaluo = 172l;

        try {
            asignaciones = (ArrayList<AvaluoAsignacionProfesional>) this.dao.
                obtenerDeUltimaAsignacion(codActividad, idAvaluo);
            assertTrue(true);
            LOGGER.debug(" ... passed!!");

            if (asignaciones != null && !asignaciones.isEmpty()) {
                for (AvaluoAsignacionProfesional aap : asignaciones) {
                    LOGGER.debug("asignacion - profesional id = " + aap.getProfesionalAvaluo().
                        getId());
                }
            } else {
                LOGGER.debug("el resultado es nulo o vacío");
            }
        } catch (Exception ex) {
            fail("ocurrió un error.");
            LOGGER.error("excepción: " + ex.getMessage());
        }

    }

}
