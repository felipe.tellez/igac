/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IPFotoDAO;
import co.gov.igac.snc.dao.conservacion.impl.PFotoDAOBean;
import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.generales.impl.TipoDocumentoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.Constantes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class PFotoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PFotoDAOBeanTest.class);

    private IPFotoDAO dao = new PFotoDAOBean();

    private ITipoDocumentoDAO daoAux = new TipoDocumentoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
        this.daoAux.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * prueba de update en cascada
     */
    @Test
    public void testPersist() {

        LOGGER.debug("unit test PFotoDAOBeanTest#testPersist ...");
        Documento doc = new Documento();
        PFoto pFoto = new PFoto();
        TipoDocumento tipoDoc = new TipoDocumento();
        PPredio pPredio = new PPredio();

        List<PFoto> pFotos = new ArrayList<PFoto>();

        //N: -para salir de la galería de la vergüenza-
        //   aquí NO IMPORTA que valor se le ponga a este campo porque no busca el archivo en esa ruta
        doc.setArchivo("c://temp");
        doc.setUsuarioLog("test");

        pPredio.setId(7l);

        try {
            //tipoDoc = daoAux.findById(Constantes.TIPO_DOCUMENTO_FOTO_ID);
            tipoDoc = daoAux.findById(ETipoDocumentoId.FOTOGRAFIA.getId());

            doc.setBloqueado("no");
            doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
            doc.setTipoDocumento(tipoDoc);
            doc.setFechaLog(new Date());
            doc.setUsuarioCreador("lola la loca");
            pFoto.setDocumento(doc);
            pFoto.setUsuarioLog("test");
            pFoto.setPPredio(pPredio);
            pFoto.setTipo("sddd");
            pFoto.setFecha(new Date());
            pFoto.setFechaLog(new Date());

            //doc = new Documento();
            //doc.setId(32l);
            pFoto.setDocumento(doc);

            this.em.getTransaction().begin();
            this.dao.persist(pFoto);
            this.em.getTransaction().commit();

            //pFotos.add(pFoto);
            //D: no sirve probar con estos porque en esa clase se usa un @EJB y parece que no se instancia
            //this.dao.guardarPFoto(pFoto, "slaldldld");
            //this.dao.guardarPFotos(pFotos);
            Assert.assertTrue(true);
        } catch (Exception e) {
            //LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * prueba de delete en cascada. Debe borrar el Documento asociado pre: existen en la BD datos
     * que sirven para la prueba
     *
     * @author pedro.garcia
     *
     * OJO: el delete en cascade NO FUNCIONA si se hace con un query. Hay que hacerlo con el método
     * "remove" del entity manager
     */
    @Test
    public void testDeleteByUnidadConstruccion() {

        LOGGER.debug("unit test PFotoDAOBeanTest#testDeleteByUnidadConstruccion ...");
        Long unidadConstruccionId = 8484L;
        int deleted;

        PFoto pfoto;
        pfoto = this.dao.findById(40l);
        try {

            //this.em.getTransaction().begin();
            //deleted = this.dao.deleteByUnidadConstruccion(unidadConstruccionId);
            //this.em.remove(pfoto);
            //this.em.getTransaction().commit();
            //LOGGER.debug("borrados: " + deleted);
            Assert.assertTrue(true);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

}
