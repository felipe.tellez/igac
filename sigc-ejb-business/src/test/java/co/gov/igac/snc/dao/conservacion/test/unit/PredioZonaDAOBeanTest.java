/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioZonaDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class PredioZonaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioZonaDAOBeanTest.class);

    private IPredioZonaDAO dao = new PredioZonaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testBuscarPorPPredioId() {
        try {
            LOGGER.debug("prueba para obtener zonas por id del predio");
            List<PredioZona> zonas;
            Long predioId = 11006414l;

            zonas = dao.buscarPorPPredioId(predioId);
            if (zonas != null) {
                LOGGER.debug("Zonas: " + zonas.size());
            }
            assertNotNull(zonas);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

}
