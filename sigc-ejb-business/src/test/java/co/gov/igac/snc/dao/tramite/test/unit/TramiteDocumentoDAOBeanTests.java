package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.List;
import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ITramiteDocumentoDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteDocumentoDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.test.unit.BaseTest;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TramiteDocumentoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteDocumentoDAOBeanTests.class);

    private ITramiteDocumentoDAO dao = new TramiteDocumentoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco() {
        LOGGER.debug(
            "unit test: TramiteDocumentoDAOBeanTests#testBuscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco");

        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Long tramiteId = 2019l;

        try {
            tramiteDocumento = this.dao
                .buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(
                    tramiteId);
            LOGGER.debug("Success");
            Assert.assertTrue(true);
            LOGGER.debug("El id del documento es: " +
                tramiteDocumento.getDocumento().getId());
        } catch (Exception e) {
            LOGGER.error(
                "Error on unit test: TramiteDocumentoDAOBeanTests#testBuscarTramiteDocumentoPorIdTramiteYDocumentoEstado: " +
                e.getMessage());
            Assert.assertFalse(true);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindByTramiteAndTipoDocumento() {

        LOGGER.
            debug("unit test: TramiteDocumentoDAOBeanTests#testFindByTramiteAndTipoDocumento ...");
        long tramiteId, idTipoDocumento;
        List<TramiteDocumento> answer = null;

        tramiteId = 96l;
        idTipoDocumento = 31l;

        try {
            answer = this.dao.findByTramiteAndTipoDocumento(tramiteId, idTipoDocumento);
            LOGGER.debug("... passed. fetched " + answer.size() + " rows");
            if (!answer.isEmpty()) {
                LOGGER.debug("tramiteDocumento id = " + answer.get(0).getId());
            }
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error on unit test: TramiteDocumentoDAOBeanTests#testFindByTramiteAndTipoDocumento: " +
                ex.getMessage());
            assertFalse(true);

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     */
    @Test
    public void testFindByTramiteDocumentosDePruebasAsociadas() {

        LOGGER.debug(
            "unit test: TramiteDocumentoDAOBeanTests#testFindByTramiteDocumentosDePruebasAsociadas ...");
        long tramiteId, idTipoDocumento;
        List<TramiteDocumento> answer = null;

        tramiteId = 41926l;
        idTipoDocumento = 31l;

        try {
            answer = this.dao.findByTramiteDocumentosDePruebasAsociadas(tramiteId);
            LOGGER.debug("... passed. fetched " + answer.size() + " rows");
            if (!answer.isEmpty()) {
                LOGGER.debug("tramiteDocumento id = " + answer.get(0).getId());
            }
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error on unit test: TramiteDocumentoDAOBeanTests#testFindByTramiteAndTipoDocumento: " +
                ex.getMessage());
            assertFalse(true);

        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testCountDocumentosPruebasPorEntidad() {

        LOGGER.debug(
            "unit test: TramiteDocumentoDAOBeanTests#testCountDocumentosPruebasPorEntidad ...");
        Integer answer;
        Long tramiteId = new Long(41399);
        String identificacion = "pepe perez _123456";
        try {
            answer = this.dao.countDocumentosPruebasPorEntidad(tramiteId, identificacion);
            LOGGER.debug("***** NUMERO DE DOCUMENTOS :" + answer);
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error on unit test: TramiteDocumentoDAOBeanTests#testFindByTramiteAndTipoDocumento: " +
                ex.getMessage());
            assertFalse(true);

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetForRegistroNotificacion() {

        LOGGER.debug("unit test: TramiteDocumentoDAOBeanTests#testGetForRegistroNotificacion ...");
        long tramiteId;
        String documentoPersona;

        TramiteDocumento answer = null;

        tramiteId = 42354l;
        documentoPersona = "1024545102";

        try {
            answer = this.dao.getForRegistroNotificacion(tramiteId, documentoPersona);
            LOGGER.debug("... passed.");
            if (answer != null) {
                LOGGER.debug("tramiteDocumento id = " + answer.getId());
            } else {
                LOGGER.debug("ocurrió algún error en el query");
            }
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error on unit test: TramiteDocumentoDAOBeanTests#testGetForRegistroNotificacion: " +
                ex.getMessage());
            assertFalse(true);

        }
    }

}
