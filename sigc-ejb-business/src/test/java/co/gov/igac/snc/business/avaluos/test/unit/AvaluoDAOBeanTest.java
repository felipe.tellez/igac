package co.gov.igac.snc.business.avaluos.test.unit;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;

public class AvaluoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoDAOBeanTest.class);

    private IAvaluoDAO dao = new AvaluoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar
     * {@link AvaluoDAOBean#consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(Long, boolean)}
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testConsultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion() {
        LOGGER.debug(
            "iniciando AvaluoDAOBeanTest#testConsultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion");

        List<String> result = null;

        result = dao.consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(944L,
            false);

        for (String str : result) {
            LOGGER.debug(str);
        }

        LOGGER.debug(
            "finalizando AvaluoDAOBeanTest#testConsultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion");
    }

    /**
     * Método para probar
     * {@link AvaluoDAOBean#consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(Long)}
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testconsultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo() {
        LOGGER.debug(
            "iniciando AvaluoDAOBeanTest#testconsultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo");

        List<String> result = null;

        result = dao.consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(856L);

        for (String str : result) {
            LOGGER.debug(str);
        }

        LOGGER.debug(
            "finalizando AvaluoDAOBeanTest#testconsultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo");
    }

    @Test
    public void testCrearAvaluo() {
        LOGGER.debug("iniciando AvaluoDAOBeanTest#testCrearAvaluo");

        Avaluo avaluo = new Avaluo();

        BaseTest.em.getTransaction().begin();

        avaluo.setTramite(new Tramite());
        avaluo.setTipoAvaluo("AVALUO DE PRUEBA");
        avaluo.setUsuarioLog("PRUEBAS");
        avaluo.setFechaLog(new Date());
        avaluo.setPlazoEjecucion(0L);
        avaluo.setCalificacion(0L);
        avaluo.setValorCotizacion(0D);
        avaluo.setSecRadicado("PRUEBA");

        //avaluo = dao.update(avaluo);
        this.em.getTransaction().begin();
        dao.persist(avaluo);
        this.em.getTransaction().commit();
        BaseTest.em.getTransaction().commit();

        Assert.assertTrue(avaluo.getId() > 0);

        LOGGER.debug("Aavaluo ID: " + avaluo.getId() + " - ");
        LOGGER.debug("finalizando AvaluoDAOBeanTest#crearAvaluo");
    }

    /**
     *
     * @author felipe.cadena
     */
    @Test
    public void testBuscarAvaluoPorSecRadicado() {
        LOGGER.debug("iniciando AvaluoDAOBeanTest#testBuscarAvaluoPorSecRadicado");

        List<Avaluo> avaluos = dao.buscarAvaluosPorSolicitudId(42623L);
        LOGGER.debug("lista " + avaluos.size());

        if (!avaluos.isEmpty()) {
            Avaluo ava = dao.buscarAvaluoPorSecRadicado(avaluos.get(0).getSecRadicado());
            Assert.assertSame(avaluos.get(0), ava);
        }
        LOGGER.debug("finalizando AvaluoDAOBeanTest#testBuscarAvaluoPorSecRadicado");

    }

    /**
     *
     * @author felipe.cadena
     */
    @Test
    public void testBuscarAvaluoPredioPorIdAvaluo() {

        LOGGER.debug("iniciando AvaluoDAOBeanTest#testBuscarAvaluoPredioPorIdAvaluo");
        List<Avaluo> avaluos = dao.findAll();
        LOGGER.debug("lista -->" + avaluos.size());

        if (!avaluos.isEmpty()) {
            Avaluo ava = dao.buscarAvaluoPorSecRadicado(avaluos.get(0).getSecRadicado());
            Assert.assertSame(avaluos.get(0), ava);
        }
        LOGGER.debug("finalizando AvaluoDAOBeanTest#testBuscarAvaluoPredioPorIdAvaluo");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetInfoHeaderAvaluoById() {

        LOGGER.debug("unit test AvaluoDAOBeanTest#GetInfoHeaderAvaluoById");

        Long avaluoId = 149l;
        Avaluo answer;

        try {
            answer = this.dao.getInfoHeaderAvaluoById(avaluoId);
            if (answer != null) {
                Assert.assertTrue(true);

                LOGGER.debug("id tramite: " + answer.getTramite().getId());
                if (answer.getTramite().getSolicitud().getSolicitanteSolicituds() != null &&
                    !answer.getTramite().getSolicitud().getSolicitanteSolicituds().isEmpty()) {
                    LOGGER.debug("tiene solicitantes. Uno: " +
                        answer.getTramite().getSolicitud().getSolicitanteSolicituds().get(0).
                            getNombreCompleto());
                }

            }
        } catch (Exception ex) {
            LOGGER.error("error: " + ex.getMessage());
            Assert.fail();
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author felipe.cadena
     */
    @Test
    public void testBuscarAvaluosPorFiltro() {

        LOGGER.debug("unit test AvaluoDAOBeanTest#buscarAvaluosPorFiltro");

        List<Avaluo> answer;
        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();
        Calendar c = Calendar.getInstance();

        filtro.setDepartamento(null);
        filtro.setDireccion(null);
        c.set(2012, 7, 1);
        filtro.setFechaInicial(null);
        c.set(2012, 10, 1);
        filtro.setFechaFinal(null);
        filtro.setMunicipio(null);
        filtro.setNombreRazonSocial(null);
        filtro.setNombreSolicitante(null);
        filtro.setNumeroPredial(null);
        filtro.setNumeroRadicacion(null);
        filtro.setNumeroSecRadicado(null);
        filtro.setTerritorial(null);
        filtro.setTipoEmpresa(null);
        filtro.setTipoTramite(null);
        filtro.setTipoAvaluo(null);
        filtro.setNombreAvaluador("casas");

        try {
            answer = this.dao.buscarAvaluosPorFiltro(filtro);
            if (answer != null) {
                Assert.assertTrue(true);

                LOGGER.debug("Número de avalúos : " + answer.size());

                if (answer.size() > 0) {
                    Avaluo ava = answer.get(0);
                    LOGGER.debug("Predio :" + ava.getAvaluoPredios().size());
                    LOGGER.debug("Solicitante:" + ava.getTramite().getSolicitud().
                        getSolicitanteSolicituds().get(0).getPrimerNombre());
                }

            }
        } catch (Exception ex) {
            LOGGER.error("error: " + ex.getMessage());
            Assert.fail();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarPorSolicitudes() {

        LOGGER.debug("unit test AvaluoDAOBeanTest#testBuscarPorSolicitudes ");

        ArrayList<Long> idsSolicitudes = new ArrayList<Long>();
        ArrayList<Avaluo> answer;
        FiltroDatosConsultaAvaluo filtro;

        filtro = new FiltroDatosConsultaAvaluo();
        filtro.setEstadoTramiteAvaluo(ETramiteEstado.EN_PROCESO.getCodigo());
        idsSolicitudes.add(43210l);

        try {
            answer = (ArrayList<Avaluo>) this.dao.buscarPorSolicitudes(idsSolicitudes, filtro, true);
            Assert.assertTrue(true);
            if (answer == null) {
                LOGGER.debug("retornó null");
            } else if (answer.isEmpty()) {
                LOGGER.debug("retornó lista vacía");
            } else if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("primer avaluo.id: " + answer.get(0).getId());
            }
        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testConsultarAvaluosEnProcesoAvaluador() {
        LOGGER.debug("unit test AvaluoDAOBeanTest#testConsultarAvaluosEnProcesoAvaluador ");

        List<Avaluo> resultado;

        try {
            resultado = this.dao.consultarAvaluosEnProcesoAvaluador(1L);

            LOGGER.debug("Resultado " + resultado.size());

        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testConsultarAvaluosAsignacion() {
        LOGGER.debug("unit test AvaluoDAOBeanTest#testConsultarAvaluosAsignacion ");

        List<Avaluo> resultado;

        try {
            resultado = this.dao.consultarAvaluosAsignacion(null, "60402012ER00041219-5");

            LOGGER.debug("Resultado " + resultado.size());

        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testConsultarAvaluosProfesionalPorAnio() {
        LOGGER.debug("unit test AvaluoDAOBeanTest#testConsultarAvaluosProfesionalPorAnio ");

        List<Avaluo> resultado;

        try {
            resultado = this.dao.consultarAvaluosProfesionalPorAnio(7L,
                null,
                null,
                Calendar.getInstance().get(Calendar.YEAR));

            LOGGER.debug("Resultado " + resultado.size());

        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testConsultarHistoricoAvaluos() {
        LOGGER.debug("unit test AvaluoDAOBeanTest#testConsultarHistoricoAvaluos ");

        List<Object[]> resultado;

        try {

            resultado = this.dao.consultarHistoricoAvaluos(7L);
            LOGGER.debug("Resultado " + resultado.size());
            LOGGER.debug(">>>>>>-------------------------------->>>>>>>>>>>>>");
            if (!resultado.isEmpty()) {
                Object[] d = resultado.get(0);
                LOGGER.debug(">>>>>> " + d[0] + " | " + d[1] + " | " + d[2] + " | " + d[3] + " | " +
                    d[4] + " | " + d[5]);
                d = (Object[]) resultado.get(1);
                LOGGER.debug(">>>>>> " + d[0] + " | " + d[1] + " | " + d[2] + " | " + d[3] + " | " +
                    d[4] + " | " + d[5]);
            }
            LOGGER.debug(">>>>>>-------------------------------->>>>>>>>>>>>>");

        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerParaAsignacionDeAvaluador() {

        LOGGER.debug("unit test AvaluoDAOBeanTest#testObtenerParaAsignacionDeAvaluador ");

        long[] idsTramites = new long[1];
        ArrayList<Avaluo> answer;

        idsTramites[0] = 43327l;

        try {
            answer = (ArrayList<Avaluo>) this.dao.obtenerParaOrdenPractica(idsTramites, null, null,
                null);
            Assert.assertTrue(true);
            if (answer == null) {
                LOGGER.debug("retornó null");
            } else if (answer.isEmpty()) {
                LOGGER.debug("retornó lista vacía");
            } else if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("primer avaluo.id: " + answer.get(0).getId());
            }
        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }

    }

}
