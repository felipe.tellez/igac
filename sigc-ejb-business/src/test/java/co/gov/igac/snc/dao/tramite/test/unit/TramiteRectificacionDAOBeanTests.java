package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.radicacion.impl.TramiteRectificacionDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pedro.garcia
 */
public class TramiteRectificacionDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TramiteRectificacionDAOBeanTests.class);

    private TramiteRectificacionDAOBean dao = new TramiteRectificacionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     */
    @Test
    public void testFindTramiteRectificacionesByTramiteId() {
        LOGGER.debug(
            "unit test: TramiteRectificacionDAOBeanTests#findTramiteRectificacionesByTramiteId");

        List<TramiteRectificacion> tramiteRectificaciones = null;
        Long tramiteId = 2019l;

        try {
            tramiteRectificaciones = this.dao
                .findTramiteRectificacionesByTramiteId(tramiteId);
            Assert.assertNotNull(tramiteRectificaciones);
            LOGGER.debug("Test aprobado ");
        } catch (Exception e) {
            LOGGER.error(
                "Error on unit test: TramiteRectificacionDAOBeanTests#findTramiteRectificacionesByTramiteId: " +
                e.getMessage());
            Assert.assertFalse(true);
        }
    }

}
