package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IPFichaMatrizDAO;
import static org.testng.Assert.assertFalse;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.impl.PFichaMatrizDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author juan.agudelo
 *
 */
public class PFichaMatrizDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PFichaMatrizDAOBeanTest.class);

    private IPFichaMatrizDAO dao = new PFichaMatrizDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerFichasEnProcesoDeCarga() {
        LOGGER.debug("unit testing PFichaMatrizDAOBeanTest#testObtenerFichasEnProcesoDeCarga...");

        String usuario = "pruquindio6";

        try {
            //List<PFichaMatriz> pr = this.dao.obtenerFichasEnProcesoDeCarga(usuario);
            PFichaMatriz pr = this.dao.findByPredioId(1968300l);
            if (pr != null) {
                LOGGER.debug("Fichas = " + pr.getId());
            }

            Assert.assertTrue(pr != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author leidy.gonzalez
     */
    @Test
    public void testObtenerFichaMatrizTramiteEV() {
        LOGGER.debug("unit testing PFichaMatrizDAOBeanTest#testObtenerFichasEnProcesoDeCarga...");

        String numeroPredial = "080010104000000550914100000000";

        try {

            PFichaMatriz pr = this.dao.obtenerFichaMatrizTramiteEV(numeroPredial);
            if (pr != null) {
                LOGGER.debug("Ficha = " + pr.getId());
            }

            Assert.assertTrue(pr != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

}
