/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.generales.test.unit;

import co.gov.igac.snc.dao.generales.IConexionUsuarioDAO;
import co.gov.igac.snc.dao.generales.impl.ConexionUsuarioDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class ConexionUsuarioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConexionUsuarioDAOBeanTest.class);

    private IConexionUsuarioDAO dao = new ConexionUsuarioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * pre: hay datos en la bd
     */
    @Test
    public void testObtenerNumeroConexionesPorUsuario() {

        String usuario;
        Long lista;

        usuario = "pruatlantico2";

        try {
            lista = this.dao.obtenerNumeroConexionesPorUsuario(usuario);

            Assert.assertTrue(lista >= -1);
            LOGGER.debug("passed!. size = " + lista);
        } catch (Exception e) {
            LOGGER.error("error!!:" + e.getMessage(), e);
            Assert.assertFalse(true);
        }

    }

}
