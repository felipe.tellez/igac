package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioTerrenoDAO;
import co.gov.igac.snc.dao.conservacion.impl.FichaMatrizPredioTerrenoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/*
 * Proyecto SNC 2016
 */
/**
 * Clase asociada a los predios virtuales relacionados con una ficha matriz.
 *
 * @author felipe.cadena
 */
public class FichaMatrizPredioTerrenoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        FichaMatrizPredioTerrenoDAOBeanTests.class);

    private IFichaMatrizPredioTerrenoDAO dao = new FichaMatrizPredioTerrenoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author {autor}
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("FichaMatrizPredioTerrenoDAOBeanTests#testUpdate");

        try {

            FichaMatrizPredioTerreno testEntity = new FichaMatrizPredioTerreno();

            FichaMatriz fm = new FichaMatriz();
            fm.setId(1999l);

            Predio p = new Predio();
            p.setId(1999l);

            testEntity.setFichaMatriz(fm);
            testEntity.setPredio(p);
            testEntity.setEstado("str::rddb556d2t9");
            testEntity.setUsuarioLog("str::altqixm9529");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
