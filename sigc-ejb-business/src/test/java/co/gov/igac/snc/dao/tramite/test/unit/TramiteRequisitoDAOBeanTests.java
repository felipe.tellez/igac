/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ITramiteRequisitoDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteRequisitoDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pedro.garcia
 */
public class TramiteRequisitoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteRequisitoDAOBeanTests.class);

    private ITramiteRequisitoDAO dao = new TramiteRequisitoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testConseguirDocumentacionTramiteRequerida() {
        LOGGER.debug(
            "unit test: TramiteRequisitoDAOBeanTests#testConseguirDocumentacionTramiteRequerida ...");

        Tramite tramiteTemp = new Tramite();
        tramiteTemp.setTipoTramite(ETramiteTipoTramite.MUTACION
            .getCodigo());
        tramiteTemp.setClaseMutacion(EMutacionClase.SEGUNDA.getCodigo());
        tramiteTemp.setSubtipo(EMutacion2Subtipo.DESENGLOBE.getCodigo());
        try {
            List<TramiteRequisito> tr = this.dao
                .conseguirDocumentacionTramiteRequerida(tramiteTemp);

            for (int i = 0; i < tr.size(); i++) {
                LOGGER.debug("Documentos requeridos ", tr.get(i)
                    .getTramiteRequisitoDocumentos().size());
                for (int j = 0; j < tr.get(i).getTramiteRequisitoDocumentos()
                    .size(); j++) {
                    if (tr.get(i).getTramiteRequisitoDocumentos().get(j)
                        .getTipoDocumento() != null) {
                        LOGGER.debug("Documento: " +
                            tr.get(i).getTramiteRequisitoDocumentos()
                                .get(j).getTipoDocumento().getNombre());
                    }
                }
            }
            assertNotNull(tr);

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            assertFalse(true);
        }
    }

    // end of class
}
