package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionComisionDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionComisionDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionComision;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.util.EActualizacionComisionObjeto;
import co.gov.igac.snc.test.unit.BaseTest;

public class ActualizacionComisionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ActualizacionComisionDAOBeanTest.class);

    private IActualizacionComisionDAO dao = new ActualizacionComisionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testRecuperarConveniosPorIdActualizacion() {
        Long actualizacionId = 450L;
        String objeto = EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.getCodigo() +
            "BARRANQUILLA (Distrito Especial, Industrial y Portuario)";
        ActualizacionComision comision = this.dao.getActualizacionComisionByActualizacionId(
            actualizacionId, objeto);

        Assert.assertNotNull(comision);

    }

}
