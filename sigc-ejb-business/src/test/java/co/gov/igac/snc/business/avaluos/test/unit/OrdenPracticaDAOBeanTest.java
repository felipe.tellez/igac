/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IOrdenPracticaDAO;
import co.gov.igac.snc.dao.avaluos.impl.OrdenPracticaDAOBean;
import co.gov.igac.snc.dao.avaluos.impl.ProfesionalContratoAdicionDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class OrdenPracticaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OrdenPractica.class);

    private IOrdenPracticaDAO dao = new OrdenPracticaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar {@link OrdenPracticaDAOBean#consultarPorAvaluoId(Long)}
     *
     * @author felipe.cadena
     */
    @Test
    public void testConsultarPorAvaluoId() {
        LOGGER.debug("iniciando OrdenPracticaDAOBeanTest#testConsultarPorAvaluoId");

        OrdenPractica result = null;

        result = dao.consultarPorAvaluoId(172L);

        LOGGER.debug("************************************************");
        LOGGER.debug("Numero orden " + result.getNumero());
        LOGGER.debug("************************************************");

        LOGGER.debug("finalizando OrdenPracticaDAOBeanTest#testConsultarPorAvaluoId");
    }

}
