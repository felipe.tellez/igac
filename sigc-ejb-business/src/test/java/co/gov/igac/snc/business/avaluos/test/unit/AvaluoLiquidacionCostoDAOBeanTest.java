package co.gov.igac.snc.business.avaluos.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IAvaluoLiquidacionCostoDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoLiquidacionCostoDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

public class AvaluoLiquidacionCostoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoLiquidacionCostoDAOBeanTest.class);

    private IAvaluoLiquidacionCostoDAO dao = new AvaluoLiquidacionCostoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Metodo para probar
     * {@link AvaluoLiquidacionCostoDAOBean#calcularValorEjecutadoAvaluosAprobadosPorContratoId(Long)}
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testCalcularValorEjecutadoAvaluosAprobadosPorContratoId() {
        LOGGER.debug(
            "Inicio AvaluoLiquidacionCostoDAOBeanTest#testCalcularValorEjecutadoAvaluosAprobadosPorContratoId");

        Double result;
        try {

            result = dao
                .calcularValorEjecutadoAvaluosAprobadosPorContratoId(2L);

            LOGGER.debug(String.valueOf(result));

        } catch (Exception e) {
            throw SncBusinessServiceExceptions.EXCEPCION_SQL_GENERAL
                .getExcepcion(
                    LOGGER,
                    e,
                    "AvaluoLiquidacionCostoDAOBeanTest#" +
                    "calcularValorEjecutadoAvaluosAprobadosPorContratoId");
        }

        LOGGER.debug(
            "Fin AvaluoLiquidacionCostoDAOBeanTest#testCalcularValorEjecutadoAvaluosAprobadosPorContratoId");
    }

}
