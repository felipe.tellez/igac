package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IAxMunicipioDAO;
import co.gov.igac.snc.dao.conservacion.impl.AxMunicipioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/*
 * Proyecto SNC {year}
 */
/**
 * Tests para la clase AxMunicipio
 *
 * @author felipe.cadena
 */
public class AxMunicipioDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AxMunicipioDAOBeanTests.class);

    private IAxMunicipioDAO dao = new AxMunicipioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("AxMunicipioDAOBeanTests#testUpdate");

        try {

            AxMunicipio testEntity = new AxMunicipio();

            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR, -1);
            testEntity.setDepartamentoCodigo("23");
            testEntity.setMunicipioCodigo("23300");
            testEntity.setVigencia(c.getTime());
            testEntity.setFechaInicio(new Date());
            testEntity.setFechaFin(new Date());
            testEntity.setEstadoTramites("str::cu9nb62mx6r");
            testEntity.setFechaCierreTramites(new Date());
            testEntity.setEstadoTablas("str::h4nnduw61or");
            testEntity.setFechaCierreTablas(new Date());
            testEntity.setUsuarioLog("str::rvpkr2otj4i");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorMunicipioVigencia() {

        String munCodigo = "23300";
        String vigencia = "2016";

        try {
            List<AxMunicipio> results = this.dao.obtenerPorMunicipioVigencia(munCodigo, vigencia);

            assertNotNull(results);
            LOGGER.debug("***********************");
            LOGGER.debug(String.valueOf(results.size()));
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerMunicipiosEnActualizacion() {

        try {
            List<AxMunicipio> results = this.dao.obtenerMunicipiosEnActualizacion();

            assertNotNull(results);
            LOGGER.debug("***********************");
            LOGGER.debug(String.valueOf(results.size()));
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
