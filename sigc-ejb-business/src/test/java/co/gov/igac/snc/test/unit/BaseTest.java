package co.gov.igac.snc.test.unit;

import static org.testng.Assert.fail;

import java.io.File;
import java.net.URL;
import java.sql.Connection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

/**
 * Clase Base para la creación de pruebas unitarias
 *
 *
 * @author juan.mendez
 *
 */
public class BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);

    protected static EntityManagerFactory emf;
    public static EntityManager em;

    public static final String EM_TEST_ORACLEDB = "test-oradb";

    private static final String GRAPH_SNC_FILE_NAME = "graphSNC.dot";
    protected static String GRAPH_SNC_FILE_PATH;

    /**
     *
     */
    static {
        LOGGER.info("***************************************");
        LOGGER.info("Static Init...");
        LOGGER.info("retrieving EntityManager: " + BaseTest.EM_TEST_ORACLEDB);
        emf = Persistence.createEntityManagerFactory(BaseTest.EM_TEST_ORACLEDB);
        LOGGER.info("EntityManagerFactory created");
        em = emf.createEntityManager();
        LOGGER.info("EntityManager Created: " + BaseTest.EM_TEST_ORACLEDB);
        LOGGER.info("***************************************");
    }

    @BeforeSuite
    public void beforeSuite() {
        LOGGER.debug("beforeSuite");
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            URL fileUrl = cl.getResource(GRAPH_SNC_FILE_NAME);
            GRAPH_SNC_FILE_PATH = java.net.URLDecoder.decode(fileUrl.getPath(), "UTF-8");
            GRAPH_SNC_FILE_PATH = new File(GRAPH_SNC_FILE_PATH).getAbsolutePath();
            LOGGER.debug("GRAPH_SNC_FILE_PATH:" + GRAPH_SNC_FILE_PATH);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }

    }

    /**
     *
     * @param emName
     */
    protected void setUp(String emName) {
        LOGGER.info("setUp");
        try {

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     */
    protected void tearDown() {
        /*
         * try { LOGGER.info("closing EntityManagerFactory"); emf.close(); } catch (Exception e) {
         * LOGGER.error(e.getMessage(), e); fail(e.getMessage(), e); }
         */
    }

    @BeforeMethod
    public void beforeMethod() {
        LOGGER.debug(
            "se va a ejecutar un método de pruebas \n***************************************************************************");
    }

    @AfterMethod
    public void afterMethod() {
        LOGGER.debug(
            "*************************************************************************** \n se ejecutó un método de pruebas");
    }

    /**
     * Obtiene un objeto de tipo java.sql.Connection a partir de un Entity Manager (Se utiliza para
     * las pruebas de los stored procedures)
     *
     * http://stackoverflow.com/questions/6707115/get-hold-of-a-jdbc-connection-
     * object-from-a-stateless-bean
     *
     * @author juan.mendez
     * @return
     */
    public Connection getConnection() {
        // jpa 2.0
        Connection connection = em.unwrap(java.sql.Connection.class);
        // jpa 1.0
        // Connection connection = ((HibernateEntityManager)
        // em).getSession().connection();
        return connection;
    }

}
