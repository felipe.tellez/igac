package co.gov.igac.snc.business.avaluos.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IProfesionalContratoCesionDAO;
import co.gov.igac.snc.dao.avaluos.impl.ProfesionalContratoCesionDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoCesion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase de pruebas para ProfesionalContratoCesionDAOBean
 *
 * @author rodrigo.hernandez
 *
 */
public class ProfesionalContratoCesionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProfesionalContratoCesionDAOBeanTest.class);

    private IProfesionalContratoCesionDAO dao = new ProfesionalContratoCesionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar
     * {@link ProfesionalContratoCesionDAOBean#obtenerCesionesDeContratoActivo(Long)}
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testObtenerCesionesDeContratoActivo() {
        LOGGER.debug(
            "iniciando ProfesionalContratoAdicionDAOBeanTest#testObtenerFechaFinalPAContratoAdiciones");

        List<ProfesionalContratoCesion> result = null;

        result = dao.obtenerCesionesDeContratoActivo(1L);

        for (ProfesionalContratoCesion pcc : result) {

            LOGGER.debug(String.valueOf(pcc.getCedeProfesional().getId()));
            LOGGER.debug(String.valueOf(pcc.getCedeProfesional().getPrimerNombre()));
            LOGGER.debug(String.valueOf(pcc.getRecibeProfesional().getId()));
            LOGGER.debug(String.valueOf(pcc.getRecibeProfesional().getNumeroIdentificacion()));

            LOGGER.debug(String.valueOf(pcc.getId()));

        }

        LOGGER.debug(
            "finalizando ProfesionalContratoAdicionDAOBeanTest#testObtenerFechaFinalPAContratoAdiciones");
    }
}
