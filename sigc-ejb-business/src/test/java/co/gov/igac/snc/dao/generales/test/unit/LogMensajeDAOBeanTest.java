package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.impl.LogMensajeDAOBean;
import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.LogMensajeFactory;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;

/**
 * Prueba unitaria para LogMensajeDAOBean
 *
 * @author juan.mendez
 *
 */
public class LogMensajeDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogMensajeDAOBeanTest.class);

    private ILogMensajeDAO dao = new LogMensajeDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author lorena.salamanca
     *
     */
    @Test
    public void createLogReportesTest() {
        LOGGER.debug("createLogReportesTest");
        try {
            UsuarioDTO usuario = LdapDAO.searchUser("pruatlantico8");
            assertNotNull(usuario);
            LogMensaje logReporte = LogMensajeFactory.getLogMensaje(usuario,
                LogMensajesReportesUtil.SERVICIO_REPORTES);
            assertNotNull(logReporte);
            logReporte.setMensaje("Mensaje de Prueba");
            assertNotNull(logReporte.getMensaje());
            this.em.getTransaction().begin();
            this.dao.persist(logReporte);
            this.em.getTransaction().commit();
            assertNotNull(logReporte.getId());
            LOGGER.debug("logReporte:" + logReporte.getId());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

    @Test
    public void createLogSIGTest() {
        LOGGER.debug("createLogSIGTest");
        try {
            UsuarioDTO usuario = LdapDAO.searchUser("pruatlantico8");
            assertNotNull(usuario);
            LogMensaje logReporte = LogMensajeFactory.getLogMensaje(usuario,
                LogMensajesReportesUtil.SERVICIO_SIG);
            assertNotNull(logReporte);
            logReporte.setMensaje("Mensaje de Prueba");
            assertNotNull(logReporte.getMensaje());
            this.em.getTransaction().begin();
            this.dao.persist(logReporte);
            this.em.getTransaction().commit();
            assertNotNull(logReporte.getId());
            LOGGER.debug("logReporte:" + logReporte.getId());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

}
