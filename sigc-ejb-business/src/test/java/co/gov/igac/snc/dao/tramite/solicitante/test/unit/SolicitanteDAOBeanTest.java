package co.gov.igac.snc.dao.tramite.solicitante.test.unit;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.impl.SolicitanteDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;

public class SolicitanteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitanteDAOBeanTest.class);

    private SolicitanteDAOBean dao = new SolicitanteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void buscarSolicitantePorTipoYNumeroDeDocumento() {
        LOGGER.debug("Entrando al test de busqueda de solicitante");
        Solicitante sol = dao.buscarSolicitantePorTipoYNumeroDeDocumento(
            "123456", "NIT");
        LOGGER.debug(sol.getNumeroIdentificacion());
        Assert.assertNotNull(sol);
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void updateSolicitante() {
        dao.getEntityManager().getTransaction().begin();
        Solicitante solicitante = new Solicitante();
        solicitante.setTipoSolicitante("1");
        solicitante.setNumeroIdentificacion("123456");
        solicitante.setTipoIdentificacion("NIT");
        solicitante.setTipoPersona("JURIDICA");
        solicitante.setFechaLog(new Date());
        solicitante.setUsuarioLog("fabio.navarrete.test");
        solicitante.setId(new Long(400));
        try {
            Solicitante s = dao.update(solicitante);
            LOGGER.debug("Tipo Persona: " + s.getTipoPersona());
            dao.getEntityManager().getTransaction().commit();
            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void findSolicitanteByTipoIdentAndNumDocFetchRelacions() {
        LOGGER.debug("Entrando al test de busqueda de solicitante");
        try {
            Solicitante sol = dao
                .findSolicitanteByTipoIdentAndNumDocFetchRelacions("CC",
                    "1234567890");
            Assert.assertNotNull(sol);
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
            Assert.fail();
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerSolicitantesPropietariosTramiteMasivo() {
        LOGGER.debug("Entrando al test de busqueda de solicitante");
        try {
            Long tramiteId = 48227l;
            List<Solicitante> sol = dao
                .obtenerSolicitantesPropietariosTramiteMasivo(tramiteId);
            Assert.assertNotNull(sol);
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
            Assert.fail();
        }
    }

}
