package co.gov.igac.snc.business.generales.test.unit;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.dao.actualizacion.IActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionDAOBean;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioDAOBean;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.impl.DocumentoDAOBean;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.test.unit.BaseTest;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;

/**
 *
 *
 *
 */
public class DocumentoDAOBeanTest extends BaseTest {

    private static final String LOGIN_USUARIO = "pruatlantico16";

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentoDAOBeanTest.class);

    private IDocumentoDAO dao = new DocumentoDAOBean();
    private ITramiteDAO tramiteDao = new TramiteDAOBean();
    private IPredioDAO predioDao = new PredioDAOBean();
    private IActualizacionDAO actualizacionDao = new ActualizacionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
        tramiteDao.setEntityManager(this.em);
        predioDao.setEntityManager(this.em);
        actualizacionDao.setEntityManager(this.em);

        TramiteDAOBean tramiteDao = new TramiteDAOBean();
        tramiteDao.setEntityManager(this.em);
        ((DocumentoDAOBean) dao).setTramiteDAO(tramiteDao);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testBuscarResolucionesByNumeroPredialPredio() {
        LOGGER.debug("tests: DocumentoDAOBeanTest#testBuscarResolucionesByNumeroPredialPredio");
        List<Documento> resoluciones = new ArrayList<Documento>();
        String numPredial = "080010103000000200490900000000";

        try {
            LOGGER.debug("******* COMIENZA TEST ********");
            resoluciones = dao.buscarResolucionesByNumeroPredialPredio(numPredial);
            assertNotNull(resoluciones);
            LOGGER.debug("Tamaño: " + resoluciones.size());
            LOGGER.debug("****** TERMINAMOS TEST *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testDeleteDocumentoAlsoAlfresco() {
        LOGGER.debug("tests: DocumentoDAOBeanTest#testDeleteDocumentoAlsoAlfresco");

        String uuid = "workspace://SpacesStore/72d50fbb-096d-4f13-a2fe-84dfeede4250";

        Documento d = dao.findById(44119L);

        try {

            this.dao.getEntityManager().getTransaction().begin();
            LOGGER.debug("******* Inicio ********");
            LOGGER.debug("******* " + d.getArchivo() + " ********");
            LOGGER.debug("******* " + d.getIdRepositorioDocumentos() + " ********");
            if (!dao.deleteDocumentoAlsoGestorDocumental(d)) {
                fail("No se pudo borrar el documento");
            }
            LOGGER.debug("****** Fin *******");
            dao.getEntityManager().getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de documento en alfresco
     *
     * @author juan.agudelo
     */
    @Test
    public void testConsultarURLDeOficioEnAlfresco() {
        LOGGER.debug("tests: DocumentoDAOBeanTest#testConsultarURLDeOficioEnAlfresco");

        String alfrescoId = "workspace://SpacesStore/72dbda47-0193-4be6-8e26-09eb78f037e9";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String answer = this.dao.descargarOficioDeGestorDocumentalATempLocalMaquina(alfrescoId);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Ruta documento " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de documento en alfresco
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarDocumentoResultadoPorTramiteId() {
        LOGGER.debug("tests: DocumentoDAOBeanTest#testBuscarDocumentoResultadoPorTramiteId");

        Long tramiteId = 2001L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String answer = this.dao.buscarDocumentoResultadoPorTramiteId(tramiteId);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Ruta documento " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de las rutas en Alfresco de las imagenes incial y final del trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testRecuperarRutaAlfrescoImagenInicialYfinalDelTramite() {
        LOGGER.debug(
            "tests: DocumentoDAOBeanTest#testRecuperarRutaAlfrescoImagenInicialYfinalDelTramite");

        Long tramiteId = 2001L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String[] answer = this.dao.recuperarRutaGestorDocumentalImagenInicialYfinalDelTramite(
                tramiteId);

            if (answer != null) {

                LOGGER.debug("Ruta documento inicial: " + answer[0]);
                LOGGER.debug("Ruta documento final: " + answer[1]);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de la imagen despues de un tramite en BD
     *
     * @author franz.gamba
     */
    @Test
    public void testGetImagenAfterEdicionByTramiteId() {
        LOGGER.debug("tests: DocumentoDAOBeanTest#testGetImagenAfterEdicionByTramiteId");

        Long tramiteId = 42617L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Documento answer = this.dao.getImagenAfterEdicionByTramiteId(tramiteId);

            if (answer != null) {

                LOGGER.debug("Documento : " + answer.getArchivo());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    /**
     * Prueba para guardar ficha predial
     */
    @Test
    public void testAlmacenarDocumentoAlfresco() {
        try {
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin(LOGIN_USUARIO);
            Documento documento = new Documento();
            documento.setTramiteId(Long.parseLong("42817"));
            documento.setPredioId(Long.parseLong("505680"));

            TipoDocumento tipoDocumento = new TipoDocumento();
            //TODO TIPO DOCUMENTO FICHA PREDIAL
            documento.setTipoDocumento(tipoDocumento);

            Documento respuesta = this.dao.
                clasificarAlmacenamientoDocumentoGestorDocumental(usuario, documento);
            assertNotNull(respuesta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testObtenerNombreDeReplicaOriginal() {
        LOGGER.debug("tests: DocumentoDAOBeanTest#testObtenerNombreDeReplicaOriginal");

        Long tramiteId = 43418L;
        Long tipoDocumento = 98L;
        try {
            String answer = this.dao.obtenerIdRepositorioDocumentosDeReplicaOriginal(tramiteId,
                tipoDocumento);
            LOGGER.debug(answer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prueba para obtenerUltimoDocSolicitud
     */
    @Test
    public void testObtenerUltimoDocSolicitud() {
        LOGGER.debug("tests: DocumentoDAOBeanTests#testObtenerUltimoDocSolicitud");

        Long tramiteId = 43500L;
        try {
            Documento answer = this.dao.obtenerUltimoDocSolicitud(tramiteId);
            LOGGER.debug("***************************************************");
            LOGGER.debug("*************Doc " + answer.getArchivo() + " *********************");
            LOGGER.debug("***************************************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prueba para guardar Documento Memoria Tecnica Actualizacion
     *
     * @author javier.aponte
     */
    @Test
    public void testAlmacenarDocumentoMemoriaTecnicaActualizacion() {
        try {
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin(LOGIN_USUARIO);
            Documento documento = new Documento();

            Actualizacion actualizacion = this.actualizacionDao.
                buscarActualizacionPorIdConConveniosYEntidades(494L);

            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.OFICIOS_REMISORIOS.getId());
            tipoDocumento.setNombre("PRUEBA");
            documento.setTipoDocumento(tipoDocumento);
            documento.setArchivo("C:\\qr.jpg");

            Documento respuesta = this.dao.almacenarDocumentoMemoriaTecnicaActualizacion(documento,
                actualizacion, usuario);
            assertNotNull(respuesta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testFindByID() {

        LOGGER.debug("unit test DocumentoDAOBeanTest#testFindByID ...");

        Documento doc;
        IDocumentoDAO documentoDao;
        Long idDocumento;

        idDocumento = 50482L;
        documentoDao = new DocumentoDAOBean();
        doc = null;

        try {

            //doc = this.dao.findById(idDocumento);
            //N: esto es redundante (porque se podría hacer directamente como en la línea comentada), pero funciona
            documentoDao.setEntityManager(this.dao.getEntityManager());

            doc = documentoDao.findById(idDocumento);
            Assert.assertNotNull(doc);
        } catch (Exception ex) {
            LOGGER.error("no se pudo encontrar el Documento con id " + idDocumento + " : " +
                ex.getMessage());
            fail();
        }

    }

    @Test
    public void testBuscarPorNumeroDocumentoYTipo() {

        LOGGER.debug("unit test DocumentoDAOBeanTest#testBuscarPorNumeroDocumentoYTipo ...");

        String numeroDoc = "08-001-000019-2015";
        Long idTipoDoc = 3107L;
        List<Documento> docs;

        try {

            docs = this.dao.buscarPorNumeroDocumentoYTipo(numeroDoc, idTipoDoc);

        } catch (Exception ex) {

            fail();
        }

    }

    /**
     * Prueba para método de obtener números prediales sin ficha predial digital
     *
     * @author javier.aponte
     */
    @Test
    public void testObtenerNumerosPredialesSinFichaPredialDigital() {
        try {
            String numerosPrediales =
                "'630010103000006640001000000000','080010003000000001789000000000','080010003000000001790000000000'";

            String resultado;

            resultado = this.dao.obtenerNumerosPredialesSinFichaPredialDigital(numerosPrediales);
            assertNotNull(resultado);
            LOGGER.debug(resultado);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para método de obtener números prediales sin ficha predial digital
     *
     * @author felipe.cadena
     */
    @Test
    public void testGuardarDocumentosActualizacionX() {
        try {

            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setCodigoTerritorial("6040");
            String municipioActId = "1254";

            String path = "C:\\Users\\felipe.cadena\\Downloads\\test\\act.zip";

            String resultado = this.dao.guardarDocumentosActualizacionX(path, municipioActId,
                usuario, "08001");
            assertNotNull(resultado);
            LOGGER.debug(resultado);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Metodo para generar directorios en ftp y cargar docs
     *
     * @author felipe.cadena
     */
    @Test
    public void testCrearDirectoriosDocs() {

        IDocumentosService servicioDocumental = DocumentalServiceFactory
            .getService();

        int idsDocs[] = {5629370};
        ArrayList<Long> idsArray = new ArrayList<Long>();
        for (int idsDoc : idsDocs) {
            idsArray.add(Long.valueOf(idsDoc));
        }

        for (Long docId : idsArray) {

            Documento doc = this.dao.findById(docId);
            Tramite t = this.tramiteDao.findById(doc.getTramiteId());
            Predio p = this.predioDao.findById(doc.getPredioId());

            //ruta local ubicacion archivos
            String ruta = "C:\\2017\\dev\\" + doc.getArchivo();

            DocumentoTramiteDTO documentoTramiteDTO = new DocumentoTramiteDTO(ruta,
                doc.getTipoDocumento().getNombre(),
                t.getFechaRadicacion(),
                p.getDepartamento().getNombre(),
                p.getMunicipio().getNombre(),
                p.getNumeroPredial(),
                t.getTipoTramite(),
                t.getId());
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin(doc.getUsuarioLog());
            usuario.setCodigoTerritorial("6040");
            String rutaFtp = servicioDocumental.cargarDocumentoTramite(
                documentoTramiteDTO, usuario);

            doc.setIdRepositorioDocumentos(rutaFtp);
            String archivo = rutaFtp.substring(rutaFtp.lastIndexOf('/') + 1);
            doc.setArchivo(archivo);

            this.em.getTransaction().begin();
            this.dao.update(doc);
            this.em.getTransaction().commit();

            LOGGER.debug(rutaFtp);
        }

    }

}
