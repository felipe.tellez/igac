/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IControlCalidadAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.impl.ControlCalidadAvaluoDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadEspecificacion;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class ControlCalidadAvaluoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadAvaluoDAOBeanTest.class);

    private IControlCalidadAvaluoDAO dao = new ControlCalidadAvaluoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testcrearActualizarControlCalidadAvaluo() {
        LOGGER.debug(
            "unit test ControlCalidadAvaluoDAOBeanTest#testcrearActualizarControlCalidadAvaluo ");

        ControlCalidadAvaluo cca = new ControlCalidadAvaluo();
        cca.setAvaluoId(172L);
        cca.setActividad("CC_GIT");
        cca.setTerritorialCodigo("6040");
        cca.setFechaLog(new Date());
        cca.setUsuarioLog("pruatlantico02");

        List<ControlCalidadEspecificacion> listaEspecificaciones =
            new ArrayList<ControlCalidadEspecificacion>();

        ControlCalidadEspecificacion cce = new ControlCalidadEspecificacion();
        cce.setCumpleBoolean(false);
        cce.setEspecificacion("ESC 1");
        cce.setFechaLog(new Date());
        cce.setUsuarioLog("pruatlantico02");

        listaEspecificaciones.add(cce);

        ControlCalidadAvaluoRev revisionActual = new ControlCalidadAvaluoRev();
        revisionActual.setAprobo(ESiNo.NO.getCodigo());
        revisionActual.setFecha(new Date());
        revisionActual.setNumero(5L);
        revisionActual.setControlCalidadAvaluo(cca);
        revisionActual.setControlCalidadEspecificacions(listaEspecificaciones);

        for (ControlCalidadEspecificacion cces : listaEspecificaciones) {
            cces.setAvaluoControlCalidadRev(revisionActual);
        }

        cca.setActividad(EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo());
        cca.setControlCalidadAvaluoRevs(new ArrayList<ControlCalidadAvaluoRev>());
        cca.getControlCalidadAvaluoRevs().add(revisionActual);
        cca.setAprobo(revisionActual.getAprobo());

        for (ControlCalidadAvaluoRev ccar : cca.getControlCalidadAvaluoRevs()) {
            ccar.setUsuarioLog("pruatlantico02");
            ccar.setFechaLog(new Date());
            for (ControlCalidadEspecificacion cces : ccar.getControlCalidadEspecificacions()) {
                cces.setUsuarioLog("pruatlantico02");
                cces.setFechaLog(new Date());
            }
        }

        try {

            LOGGER.debug("Inicio-******************************-");
            ControlCalidadAvaluo c = this.dao.update(cca);

            LOGGER.debug("CC_Avaluo " + c.getActividad() + " - " + c.getId());

        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }
    }

}
