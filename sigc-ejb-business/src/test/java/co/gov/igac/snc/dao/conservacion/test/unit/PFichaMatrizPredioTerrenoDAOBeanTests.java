/*
 * Proyecto SNC 2016
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IPFichaMatrizPredioTerrenoDAO;
import co.gov.igac.snc.dao.conservacion.impl.PFichaMatrizPredioTerrenoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test del DAO PFichaMatrizPredioTerreno
 *
 * @author felipe.cadena
 */
public class PFichaMatrizPredioTerrenoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        PFichaMatrizPredioTerrenoDAOBeanTests.class);

    private IPFichaMatrizPredioTerrenoDAO dao = new PFichaMatrizPredioTerrenoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author {autor}
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("PFichaMatrizPredioTerrenoDAOBeanTests#testUpdate");

        try {

            PFichaMatrizPredioTerreno testEntity = new PFichaMatrizPredioTerreno();

            PFichaMatriz fm = new PFichaMatriz();
            fm.setId(1999l);

            Predio p = new Predio();
            p.setId(1999l);

            testEntity.setFichaMatriz(fm);
            testEntity.setPredio(p);
            testEntity.setCancelaInscribe("I");
            testEntity.setEstado("ACTIVO");
            testEntity.setUsuarioLog("str::09k2rqw7b9");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
