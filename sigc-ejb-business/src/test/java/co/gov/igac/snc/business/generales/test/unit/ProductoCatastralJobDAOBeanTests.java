/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.generales.impl.ProductoCatastralJobDAOBean;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author juan.mendez
 */
public class ProductoCatastralJobDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ProductoCatastralJobDAOBeanTests.class);

    private IProductoCatastralJobDAO dao = new ProductoCatastralJobDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     *
     * @author juan.mendez
     *
     */
    /*
     * @Test public void testObtenerSiguienteTrabajoParaAplicarCambios() { LOGGER.debug("unit test:
     * ProductoCatastralJobDAOBeanTests#testObtenerSiguienteTrabajoParaAplicarCambios ..."); try {
     * ProductoCatastralJob resultado = this.dao.obtenerSiguienteTrabajoParaAplicarCambios();
     * assertNotNull(resultado); LOGGER.debug("resultado:"+resultado); } catch (Exception e) {
     * fail(e.getMessage(), e); } }
     */
    @Test
    public void testObtenerJobsPendientesEnSNC() {
        LOGGER.debug(
            "unit test: ProductoCatastralJobDAOBeanTests#testObtenerJobsPendientesEnSNC ...");
        try {
            LOGGER.debug(ProductoCatastralJob.AGS_TERMINADO);
            List<ProductoCatastralJob> resultados = this.dao.obtenerJobsPendientesEnSNC();
            assertNotNull(resultados);
            assertTrue(resultados.size() > 0);
            LOGGER.debug("CANTIDAD:" + resultados.size());
            for (ProductoCatastralJob productoCatastralJob : resultados) {
                LOGGER.debug("resultado:" + productoCatastralJob);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testObtenerJobsPendientesPorCantidad() {
        LOGGER.debug("*#testObtenerJobsPendientesPorCantidad");
        try {
            Long cantidad = 10L;
            List<ProductoCatastralJob> resultados = this.dao.obtenerJobsPendientesEnSNC(cantidad);
            assertNotNull(resultados);
            assertTrue(resultados.size() > 0);
            LOGGER.debug("CANTIDAD:" + resultados.size());
            for (ProductoCatastralJob productoCatastralJob : resultados) {
                LOGGER.debug("resultado:" + productoCatastralJob);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @modified andres.eslava::30/03/2015::modificando nuevos parametros numero de jobs y numero
     * reintentos
     */
    @Test
    public void testObtenerJobsGeograficosEsperando() {
        LOGGER.debug("unit test: ProductoCatastralJobDAOBeanTests#obtenerJobsEsperando ...");
        try {
            int numeroHoras = 12;
            int numeroJobs = 20;
            int numeroReintentos = 4;
            List<ProductoCatastralJob> resultados = this.dao.obtenerJobsGeograficosEsperando(
                numeroHoras, numeroJobs, numeroReintentos);

            assertNotNull(resultados);
            assertTrue(resultados.size() > 0);
            LOGGER.debug("CANTIDAD:" + resultados.size());
            for (ProductoCatastralJob productoCatastralJob : resultados) {
                LOGGER.debug("resultado:" + productoCatastralJob);
            }
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    /**
     * prueba otiene jobs con error
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerJobsGeograficosError() {
        LOGGER.debug("unit test: ProductoCatastralJobDAOBeanTests#obtenerJobsEsperando ...");
        try {
            int numeroHoras = 12;
            int numeroJobs = 20;
            int numeroReintentos = 4;
            List<ProductoCatastralJob> resultados = this.dao.
                obtenerJobsGeograficosError(numeroHoras, numeroJobs, numeroReintentos);

            assertNotNull(resultados);
            assertTrue(resultados.size() > 0);
            LOGGER.debug("CANTIDAD:" + resultados.size());
            for (ProductoCatastralJob productoCatastralJob : resultados) {
                LOGGER.debug("resultado:" + productoCatastralJob);
            }
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerJobsGeograficosPorTramiteTipoEstado() {
        LOGGER.debug(
            "unit test: ProductoCatastralJobDAOBeanTests#obtenerJobsGeograficosPorTramiteTipoEstado ...");
        try {
            long tramiteId = 39697;
            String estado = ProductoCatastralJob.SNC_TERMINADO.toString();
            String tipo = ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS.toString();

            List<ProductoCatastralJob> resultados = this.dao.
                obtenerJobsGeograficosPorTramiteTipoEstado(tramiteId, tipo, estado);
            assertNotNull(resultados);
            assertTrue(resultados.size() > 0);
            LOGGER.debug("CANTIDAD:" + resultados.size());
            for (ProductoCatastralJob productoCatastralJob : resultados) {
                LOGGER.debug("resultado:" + productoCatastralJob);
            }
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testFindById() {
        LOGGER.debug(
            "unit test: ProductoCatastralJobDAOBeanTests#obtenerJobsGeograficosPorTramiteTipoEstado ...");
        try {
            long pcjId = 80942L;
            ProductoCatastralJob resultado = this.dao.findById(pcjId);
            assertNotNull(resultado);

        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test obtener jobs para aplicar cambios por conjuntos y solo un job por manzana
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerJobsEsperandoUnoPorManzana() {
        try {
            List<ProductoCatastralJob> jobsPorEjecutarAplicarCambios = dao.
                obtenerJobsEsperandoUnoPorManzana(20);
            assertNotNull(jobsPorEjecutarAplicarCambios);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test ProductoCatastralJobDAOBeanTests#obtenerXMLResultadoObtenerZonasHomegeneas
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerXMLResultadoObtenerZonasHomegeneas() {
        LOGGER.debug(
            "ProductoCatastralJobDAOBeanTests#testObtenerXMLResultadoObtenerZonasHomegeneas...INICIA");
        try {
            Long idJob = 101026L;
            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> mapaZonas = dao.
                obtenerXMLResultadoObtenerZonasHomegeneas(idJob);
            assertNotNull(mapaZonas);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorEstadoTramiteIdTipo() {
        LOGGER.debug("ProductoCatastralJobDAOBeanTests#testObtenerPorEstadoTramiteIdTipo");
        try {
            Long tramiteId = 45264l;

            List<String> estados = new ArrayList<String>();
            estados.add(ProductoCatastralJob.AGS_EN_EJECUCION);
            estados.add(ProductoCatastralJob.AGS_ESPERANDO);
            estados.add(ProductoCatastralJob.AGS_TERMINADO);
            estados.add(ProductoCatastralJob.AGS_ERROR);
            List<ProductoCatastralJob> jobs = this.dao.obtenerPorEstadoTramiteIdTipo(estados,
                tramiteId, ProductoCatastralJob.SIG_JOB_OBTENER_ZONAS);
            assertNotNull(jobs);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void obtenerUltimoJobPorIdTramites() {
        LOGGER.debug("tests: TramiteDAOBeanTests#obtenerUltimoJobPorIdTramites");

        List<Long> ids = new ArrayList<Long>();
        ids.add(45568l);
        ids.add(46324l);
        ids.add(46350l);
        ids.add(47255l);
        ids.add(47286l);
        ids.add(119221l);
        ids.add(119267l);
        ids.add(119286l);
        ids.add(119287l);
        ids.add(119288l);
        ids.add(119600l);
        ids.add(119604l);
        ids.add(119609l);
        ids.add(121137l);

        /* ids.add(326042l); ids.add(326043l); ids.add(326044l); ids.add(326045l); ids.add(326046l);
         * ids.add(326047l); ids.add(326048l); ids.add(326049l); ids.add(326050l); ids.add(326051l);
         * ids.add(326052l); ids.add(326053l); ids.add(326054l); ids.add(326055l); ids.add(326056l);
         * ids.add(326057l); ids.add(326058l); ids.add(326059l); ids.add(326518l); ids.add(326519l);
         * ids.add(326520l); ids.add(326521l); ids.add(326522l); ids.add(326523l); ids.add(326524l);
         * ids.add(326525l); ids.add(326526l); ids.add(326527l); ids.add(326528l); ids.add(326529l);
         * ids.add(326530l); ids.add(326531l); ids.add(326532l); ids.add(326533l); ids.add(326534l);
         * ids.add(326535l); ids.add(326536l); ids.add(326537l); ids.add(326538l); ids.add(326539l);
         * ids.add(326540l); ids.add(326541l); ids.add(326542l); ids.add(326543l); ids.add(326544l);
         * ids.add(326545l); ids.add(326546l); ids.add(326547l); ids.add(326548l); ids.add(326549l);
         * ids.add(325592l); ids.add(325593l); ids.add(325594l); ids.add(325595l); ids.add(325596l);
         * ids.add(325597l); ids.add(325598l); ids.add(325599l); ids.add(325600l); ids.add(325601l);
         * ids.add(325602l); ids.add(325603l); ids.add(325604l); ids.add(325605l); ids.add(325606l);
         * ids.add(325607l); ids.add(325608l); ids.add(325609l); ids.add(325610l); ids.add(325611l);
         * ids.add(325612l); ids.add(325613l); ids.add(325614l); ids.add(325615l); ids.add(325616l);
         * ids.add(325617l); ids.add(325618l); ids.add(325619l); ids.add(325620l); ids.add(325621l);
         * ids.add(325622l); ids.add(325623l); ids.add(326060l); ids.add(326061l); ids.add(326062l);
         * ids.add(326063l); ids.add(326064l); ids.add(326065l); ids.add(326066l); ids.add(326067l);
         * ids.add(326068l); ids.add(326069l); ids.add(326070l); ids.add(326071l); ids.add(326072l);
         * ids.add(326073l); ids.add(326074l); ids.add(326075l); ids.add(326076l); ids.add(326077l);
         * ids.add(326078l); ids.add(326079l); ids.add(326080l); ids.add(326081l); ids.add(326082l);
         * ids.add(326083l); ids.add(326084l); ids.add(326085l); ids.add(326086l); ids.add(326087l);
         * ids.add(326088l); ids.add(326089l); ids.add(326090l); ids.add(326091l); ids.add(326550l);
         * ids.add(326551l); ids.add(326552l); ids.add(326553l); ids.add(326554l); ids.add(326555l);
         * ids.add(326556l); ids.add(326557l); ids.add(326558l); ids.add(326559l); ids.add(326560l);
         * ids.add(326561l); ids.add(326562l); ids.add(326563l); ids.add(326564l); ids.add(326565l);
         * ids.add(326566l); ids.add(326567l); ids.add(326568l); ids.add(326569l); ids.add(326570l);
         * ids.add(326571l); ids.add(326572l); ids.add(326573l); ids.add(326574l); ids.add(326575l);
         * ids.add(326576l); ids.add(326577l); ids.add(326578l); ids.add(326579l); ids.add(326580l);
         * ids.add(326581l); ids.add(325624l); ids.add(325625l); ids.add(325626l); ids.add(325627l);
         * ids.add(325628l); ids.add(325629l); ids.add(325630l); ids.add(325631l); ids.add(325632l);
         * ids.add(325633l); ids.add(325634l); ids.add(325635l); ids.add(325636l); ids.add(325637l);
         * ids.add(325638l); ids.add(325639l); ids.add(325640l); ids.add(325641l); ids.add(325642l);
         * ids.add(325643l); ids.add(325644l); ids.add(325645l); ids.add(325646l); ids.add(325647l);
         * ids.add(325648l); ids.add(325649l); ids.add(325650l); ids.add(325651l); ids.add(325652l);
         * ids.add(325653l); ids.add(325654l); ids.add(325655l); ids.add(326092l); ids.add(326093l);
         * ids.add(326094l); ids.add(326095l); ids.add(326096l); ids.add(326097l); ids.add(326098l);
         * ids.add(326099l); ids.add(326100l); ids.add(326101l); ids.add(326102l); ids.add(326103l);
         * ids.add(326104l); ids.add(326105l); ids.add(326106l); ids.add(326107l); ids.add(326108l);
         * ids.add(326109l); ids.add(326110l); ids.add(326111l); ids.add(326112l); ids.add(326113l);
         * ids.add(326114l); ids.add(326115l); ids.add(326116l); ids.add(326117l); ids.add(326118l);
         * ids.add(326119l); ids.add(326120l); ids.add(326121l); ids.add(326122l); ids.add(326123l);
         * ids.add(326582l); ids.add(326583l); ids.add(326584l); ids.add(326585l); ids.add(326586l);
         * ids.add(326587l); ids.add(326588l); ids.add(326589l); ids.add(326590l); ids.add(326591l);
         * ids.add(326592l); ids.add(326593l); ids.add(326594l); ids.add(326595l); ids.add(326596l);
         * ids.add(326597l); ids.add(326598l); ids.add(326599l); ids.add(326600l); ids.add(326601l);
         * ids.add(326602l); ids.add(326603l); ids.add(326604l); ids.add(326605l); ids.add(326606l);
         * ids.add(326607l); ids.add(326608l); ids.add(326609l); ids.add(326610l); ids.add(326611l);
         * ids.add(326612l); ids.add(325656l); ids.add(325657l); ids.add(325658l); ids.add(325659l);
         * ids.add(325660l); ids.add(325661l); ids.add(325662l); ids.add(325663l); ids.add(325664l);
         * ids.add(325665l); ids.add(325666l); ids.add(325667l); ids.add(325668l); ids.add(325669l);
         * ids.add(325670l); ids.add(325671l); ids.add(325672l); ids.add(325673l); ids.add(325674l);
         * ids.add(325675l); ids.add(325676l); ids.add(325677l); ids.add(325678l); ids.add(325679l);
         * ids.add(325680l); ids.add(325681l); ids.add(325682l); ids.add(325683l); ids.add(325684l);
         * ids.add(325685l); ids.add(325686l); ids.add(325687l); ids.add(326124l); ids.add(326125l);
         * ids.add(326126l); ids.add(326127l); ids.add(326128l); ids.add(326129l); ids.add(326130l);
         * ids.add(326131l); ids.add(326132l); ids.add(326133l); ids.add(326134l); ids.add(326135l);
         * ids.add(326136l); ids.add(326137l); ids.add(326138l); ids.add(326139l); ids.add(326140l);
         * ids.add(326141l); ids.add(326142l); ids.add(326143l); ids.add(326144l); ids.add(326145l);
         * ids.add(326146l); ids.add(326147l); ids.add(326148l); ids.add(326149l); ids.add(326150l);
         * ids.add(326151l); ids.add(326152l); ids.add(326153l); ids.add(326154l); ids.add(326155l);
         * ids.add(326613l); ids.add(326614l); ids.add(326615l); ids.add(326616l); ids.add(326617l);
         * ids.add(326618l); ids.add(326619l); ids.add(326620l); ids.add(326621l); ids.add(326622l);
         * ids.add(326623l); ids.add(326624l); ids.add(326625l); ids.add(326626l); ids.add(326627l);
         * ids.add(326628l); ids.add(326629l); ids.add(326630l); ids.add(326631l); ids.add(326632l);
         * ids.add(326633l); ids.add(326634l); ids.add(326635l); ids.add(326636l); ids.add(326637l);
         * ids.add(326638l); ids.add(326639l); ids.add(326640l); ids.add(326641l); ids.add(326642l);
         * ids.add(326643l); ids.add(326156l); ids.add(326157l); ids.add(326158l); ids.add(326159l);
         * ids.add(326160l); ids.add(326161l); ids.add(326162l); ids.add(326163l); ids.add(326164l);
         * ids.add(326165l); ids.add(326166l); ids.add(326167l); ids.add(326168l); ids.add(326169l);
         * ids.add(326170l); ids.add(326171l); ids.add(326172l); ids.add(326644l); ids.add(326645l);
         * ids.add(326646l); ids.add(326647l); ids.add(326648l); ids.add(326649l); ids.add(326650l);
         * ids.add(326651l); ids.add(326652l); ids.add(326653l); ids.add(326654l); ids.add(326655l);
         * ids.add(326656l); ids.add(326657l); ids.add(326658l); ids.add(326659l); ids.add(326660l);
         * ids.add(326661l); ids.add(326662l); ids.add(326663l); ids.add(326664l); ids.add(326665l);
         * ids.add(326666l); ids.add(326667l); ids.add(326668l); ids.add(326669l); ids.add(326670l);
         * ids.add(326671l); ids.add(326672l); ids.add(325688l); ids.add(325689l); ids.add(325690l);
         * ids.add(325691l); ids.add(325692l); ids.add(325693l); ids.add(325694l); ids.add(325695l);
         * ids.add(325696l); ids.add(325697l); ids.add(325698l); ids.add(325699l); ids.add(325700l);
         * ids.add(325701l); ids.add(325702l); ids.add(325703l); ids.add(325704l); ids.add(325705l);
         * ids.add(325706l); ids.add(325707l); ids.add(325708l); ids.add(325709l); ids.add(325710l);
         * ids.add(325711l); ids.add(326173l); ids.add(326174l); ids.add(326175l); ids.add(326176l);
         * ids.add(326177l); ids.add(326178l); ids.add(326179l); ids.add(326180l); ids.add(326181l);
         * ids.add(326182l); ids.add(326183l); ids.add(326184l); ids.add(326185l); ids.add(326186l);
         * ids.add(326187l); ids.add(326188l); ids.add(326189l); ids.add(326190l); ids.add(326191l);
         * ids.add(326192l); ids.add(326193l); ids.add(326194l); ids.add(326195l); ids.add(326196l);
         * ids.add(326197l); ids.add(326198l); ids.add(326199l); ids.add(326200l); ids.add(326201l);
         * ids.add(326202l); ids.add(326203l); ids.add(326204l); ids.add(326673l); ids.add(326674l);
         * ids.add(326675l); ids.add(326676l); ids.add(326677l); ids.add(326678l); ids.add(326679l);
         * ids.add(326680l); ids.add(326681l); ids.add(326682l); ids.add(326683l); ids.add(326684l);
         * ids.add(326685l); ids.add(326686l); ids.add(326687l); ids.add(326688l); ids.add(326689l);
         * ids.add(326690l); ids.add(326691l); ids.add(326692l); ids.add(326693l); ids.add(326694l);
         * ids.add(326695l); ids.add(326696l); ids.add(326697l); ids.add(326698l); ids.add(326699l);
         * ids.add(326700l); ids.add(326701l); ids.add(326702l); ids.add(326703l); ids.add(326704l);
         * ids.add(325712l); ids.add(325713l); ids.add(325714l); ids.add(325715l); ids.add(325716l);
         * ids.add(325717l); ids.add(325718l); ids.add(325719l); ids.add(325720l); ids.add(325721l);
         * ids.add(325722l); ids.add(325723l); ids.add(325724l); ids.add(325725l); ids.add(325726l);
         * ids.add(325727l); ids.add(325728l); ids.add(325729l); ids.add(325730l); ids.add(325731l);
         * ids.add(325732l); ids.add(325733l); ids.add(325734l); ids.add(325735l); ids.add(325736l);
         * ids.add(325737l); ids.add(325738l); ids.add(325739l); ids.add(325740l); ids.add(325741l);
         * ids.add(325742l); ids.add(325743l); ids.add(326205l); ids.add(326206l); ids.add(326207l);
         * ids.add(326208l); ids.add(326209l); ids.add(326210l); ids.add(326211l); ids.add(326212l);
         * ids.add(326213l); ids.add(326214l); ids.add(326215l); ids.add(326216l); ids.add(326217l);
         * ids.add(326218l); ids.add(326219l); ids.add(326220l); ids.add(326221l); ids.add(326222l);
         * ids.add(326223l); ids.add(326224l); ids.add(326225l); ids.add(326226l); ids.add(326227l);
         * ids.add(326228l); ids.add(326229l); ids.add(326230l); ids.add(326231l); ids.add(326232l);
         * ids.add(326233l); ids.add(326234l); ids.add(326235l); ids.add(326705l); ids.add(326706l);
         * ids.add(326707l); ids.add(326708l); ids.add(326709l); ids.add(326710l); ids.add(326711l);
         * ids.add(326712l); ids.add(326713l); ids.add(326714l); ids.add(326715l); ids.add(326716l);
         * ids.add(326717l); ids.add(326718l); ids.add(326719l); ids.add(326720l); ids.add(326721l);
         * ids.add(326722l); ids.add(326723l); ids.add(326724l); ids.add(326725l); ids.add(326726l);
         * ids.add(326727l); ids.add(326728l); ids.add(326729l); ids.add(326730l); ids.add(326731l);
         * ids.add(326732l); ids.add(326733l); ids.add(326734l); ids.add(326735l); ids.add(326736l);
         * ids.add(325284l); ids.add(325285l); ids.add(325286l); ids.add(325287l); ids.add(325288l);
         * ids.add(325289l); ids.add(325290l); ids.add(325291l); ids.add(325292l); ids.add(325293l);
         * ids.add(325294l); ids.add(325295l); ids.add(325296l); ids.add(325297l); ids.add(325298l);
         * ids.add(325299l); ids.add(325300l); ids.add(325301l); ids.add(325302l); ids.add(325303l);
         * ids.add(325304l); ids.add(325305l); ids.add(325306l); ids.add(325744l); ids.add(325745l);
         * ids.add(325746l); ids.add(325747l); ids.add(325748l); ids.add(325749l); ids.add(325750l);
         * ids.add(325751l); ids.add(325752l); ids.add(325753l); ids.add(325754l); ids.add(325755l);
         * ids.add(325756l); ids.add(325757l); ids.add(325758l); ids.add(325759l); ids.add(325760l);
         * ids.add(325761l); ids.add(325762l); ids.add(325763l); ids.add(325764l); ids.add(325765l);
         * ids.add(325766l); */
        String tipo = ProductoCatastralJob.TIPO_RESOLUCIONES_CONSERVACION;

        try {
            LOGGER.debug("**************** INICIO *************************");
            List<ProductoCatastralJob> result = dao.obtenerUltimoJobPorIdTramites(ids, tipo);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
    // end of class
}
