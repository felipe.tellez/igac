package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.IDominioDAO;
import co.gov.igac.snc.dao.generales.impl.DominioDAOBean;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Map;

public class DominioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DominioDAOBeanTest.class);

    private IDominioDAO dao = new DominioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author jamir.avila
     */
    @Test
    public void testFindByNombre() {
        LOGGER.info("testFindByNombre");
        List<Dominio> dominios = dao.findByNombre(EDominio.TRAMITE_TIPO_TRAMITE_AVALUO);
        assertNotNull(dominios);
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testFindByCodigo() {
        Dominio dom = dao.findByCodigo(EDominio.DERECHO_PROPIEDAD_ENTIDAD_E, "CATASTRO");
        assertNotNull(dom);
        LOGGER.debug("Descripción del dominio: " + dom.getDescripcion());
    }

    // --------------------------- //
    /**
     * Método test que hace una búsqueda de valores de un dominio y la retorna ordenada por su
     * nombre.
     *
     * @author david.cifuentes
     */
    @Test
    public void testFindByNombreOrderByNombre() {
        LOGGER.debug("DominioDAOBeanTest#testFindByNombreOrderByNombre");
        List<Dominio> dom = dao.findByNombreOrderByNombre(EDominio.DERECHO_PROPIEDAD_ENTIDAD_E);
        assertNotNull(dom);
    }
    // --------------------------- //

    /**
     * Método test que hace una búsqueda de valores de un dominio y la retorna ordenada por su
     * nombre.
     *
     * @author felipe.cadena
     */
    @Test
    public void testObtenerSolicitudesYTramites() {
        LOGGER.debug("DominioDAOBeanTest#testObtenerSolicitudesYTramites");

        Map<String, List<Dominio>> resultado = dao.obtenerSolicitudesYTramites();
        assertNotNull(resultado);
    }
}
