/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.actualizacion.IValoresActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ValoresActualizacionDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ValoresActualizacion;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class ValoresActualizacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AxMunicipioDAOBeanTests.class);

    private IValoresActualizacionDAO dao = new ValoresActualizacionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("ValoresActualizacionDAOBeanTests#testUpdate");

        try {

            ValoresActualizacion testEntity = new ValoresActualizacion();

            Departamento d = new Departamento();
            d.setCodigo("08");
            Municipio m = new Municipio();
            m.setCodigo("08001");
            testEntity.setDepartamento(d);
            testEntity.setMunicipio(m);
            testEntity.setVigencia(new Date());

            testEntity.setNombre("str::cu9nb62mx6r");
            testEntity.setEstado("str::cu9nb62mx6r");
            testEntity.setZona("cu9nb62mx6r");
            testEntity.setSector("cu9nb62mx6r");
            testEntity.setDestino("cu9nb62mx6r");
            testEntity.setUso("cu9nb62mx6r");
            testEntity.setZhf("cu9nb62mx6r");

            testEntity.setUsuarioLog("str::rvpkr2otj4i");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorMunicipioYDepartamento() {

        LOGGER.debug("ValoresActualizacionDAOBeanTests#testObtenerPorMunicipioYDepartamento");

        try {
            List<ValoresActualizacion> result = this.dao.obtenerPorMunicipioYDepartamento("08001",
                "08", "2016");

            assertNotNull(result);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

}
