package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IRecursoHumanoDAO;
import co.gov.igac.snc.dao.actualizacion.impl.RecursoHumanoDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.util.EActualizacionContratoActividad;
import co.gov.igac.snc.test.unit.BaseTest;
import static org.testng.Assert.fail;

public class RecursoHumanoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecursoHumanoDAOBeanTest.class);

    private IRecursoHumanoDAO dao = new RecursoHumanoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testObtenerRecursoPorResponsableActualizacion() {
        Long idResponsable = 450L;

        List<RecursoHumano> recursoHumano = this.dao.
            obtenerRecursoPorResponsableActualizacion(idResponsable);

        for (RecursoHumano rh : recursoHumano) {
            LOGGER.debug("Recurso humano => " + rh.getNombre());
        }
    }

    @Test
    public void testObtenerRecursoPorActualizacionId() {
        Long idResponsable = 450L;

        List<RecursoHumano> recursoHumano = this.dao.
            obtenerRecursoPorActualizacionId(idResponsable);

        for (RecursoHumano rh : recursoHumano) {
            LOGGER.debug("Recurso humano => " + rh.getNombre());
        }
    }

    @Test
    public void testObtenerRecursoHumanoPorActividad() {

        List<RecursoHumano> recursoHumano = this.dao.
            obtenerRecursoHumanoPorActividad(EActualizacionContratoActividad.TOPOGRAFO.getCodigo());

        for (RecursoHumano rh : recursoHumano) {
            LOGGER.debug("Recurso humano => " + rh.getNombre());
        }
    }

    /**
     * test javier.barajas
     */
    @Test
    public void testObtenerTopografosPorActualizacionId() {
        Long idResponsable = 450L;

        List<RecursoHumano> recursoHumano = this.dao.obtenerTopografosporActualizacionId(
            idResponsable);

        for (RecursoHumano rh : recursoHumano) {
            LOGGER.debug("Recurso humano => " + rh.getNombre());
        }
    }

    /**
     * Prueba la recuperacion de recursos humanos por actualizacion Id e identificación
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerRecursoHumanoByIdentificacionYActualizacionId() {
        LOGGER.debug(
            "RecursoHumanoDAOBeanTest#testObtenerRecursoHumanoByIdentificacionYActualizacionId...INICIA");
        try {
            RecursoHumano unRecursoHumano = dao.
                obtenerRecursoHumanoByIdentificacionYActualizacionId("241567456", 450L);
            System.out.println("Prueba recurso humano nombre " + unRecursoHumano.getNombre());
            Assert.assertNotNull(unRecursoHumano);

        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage(), e);
        } finally {
            LOGGER.debug(
                "RecursoHumanoDAOBeanTest#testObtenerRecursoHumanoByIdentificacionYActualizacionId...FINALIZA");
        }
    }
}
