package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IContratoInteradministrativoDAO;
import co.gov.igac.snc.dao.avaluos.impl.ContratoInteradministrativoDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import java.util.List;
import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ContratoInteradministrativoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ContratoInteradministrativoDAOBeanTest.class);
    private IContratoInteradministrativoDAO dao = new ContratoInteradministrativoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testBuscarContratosPorFiltro() {

        LOGGER.debug(
            "unit test: ContratoInteradministrativoDAOBeanTest#testBuscarContratosPorFiltro");

        FiltroDatosConsultaSolicitante filtro = new FiltroDatosConsultaSolicitante();
        filtro.setTipoPersona(null);
        filtro.setNombreCompleto("");
        filtro.setRazonSocial(null);
        filtro.setSigla(null);
        filtro.setDigitoVerificacion(null);
        filtro.setTipoIdentificacion("CC");
        filtro.setNumeroIdentificacion("123456");

        List<ContratoInteradministrativo> result;

        FiltroDatosConsultaContratoInteradministrativo filtroContrato =
            new FiltroDatosConsultaContratoInteradministrativo();
        filtroContrato.setNumeroContrato("123456");
        filtroContrato.setVigencia(2015L);

        try {
            result = this.dao.buscarContratosPorFiltro(filtro, filtroContrato);
            if (result != null) {
                Assert.assertTrue(true);

                LOGGER.debug("Número de contratos : " + result.size());
            }
        } catch (Exception ex) {
            LOGGER.error("error: " + ex.getMessage());
            Assert.fail();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarContratoInteradministrativoPorId() {

        LOGGER.debug(
            "unit test ContratoInteradministrativoDAOBeanTest#testBuscarContratoInteradministrativoPorId");

        Long idContrato = 1l;
        ContratoInteradministrativo answer;

        try {
            answer = this.dao.buscarPorIdConEntidadSolicitante(idContrato);
            Assert.assertTrue(true);

            if (answer != null) {
                LOGGER.debug("solicitante: " + answer.getEntidadSolicitante().getNombreCompleto());
            }
        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }

    }

}
