package co.gov.igac.snc.dao.vistas.test.unit;

import static org.testng.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.vistas.IVTramitePredioDAO;
import co.gov.igac.snc.dao.vistas.impl.VTramitePredioDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VTramitePredio;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;

public class VTramitePredioDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VTramitePredioDAOBeanTests.class);

    private IVTramitePredioDAO dao = new VTramitePredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        this.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        this.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarTramitesPorFiltros() {

        LOGGER.debug("tests: VTramitePredioDAOTests#testBuscarTramitesPorFiltros");

        String estadoTramite = ETramiteEstado.EN_ESPERA_DE_COMPLETAR_DOC
            .getCodigo();
        String numeroSolicitudBusqueda = "";
        numeroSolicitudBusqueda = "";
        String numeroRadicacionBusqueda = "";
        //numeroRadicacionBusqueda = "0875800000012016";
        FiltroDatosConsultaPredio datosPredio = new FiltroDatosConsultaPredio();
        //datosPredio.setNumeroPredial("257540102000011650001500000037");
        // numeroPredialBusqueda = "257540216041711057933105117115";
        FiltroDatosConsultaSolicitante solicitanteBusqueda = new FiltroDatosConsultaSolicitante();
        //solicitanteBusqueda.setPrimerNombre("ARENAS CARDONA PASTORA");
        List<VTramitePredio> vtpList;

        try {
            LOGGER.debug("//----- INICIO TEST -----//");
            vtpList = dao.buscarTramitesPorFiltros(estadoTramite,
                numeroSolicitudBusqueda, numeroRadicacionBusqueda,
                datosPredio, solicitanteBusqueda);
            Assert.assertNotNull(vtpList);

            LOGGER.debug("Numero predial:" + datosPredio.getNumeroPredial());
            LOGGER.debug("Lista de VTramitePredio size:" + vtpList.size());
            for (VTramitePredio vtp : vtpList) {
                LOGGER.debug("Tramite id:" + vtp.getTramite().getId());
            }

            LOGGER.debug("//------ FIN TEST ------//");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método test para consultar trámites por filtros.
     *
     * @author david.cifuentes
     * @modified by juanfelipe.garcia
     */
    @Test
    public void testConsultaDeTramitesPorFiltros() {

        LOGGER.debug("tests: VTramitePredioDAOTests#testConsultaDeTramitesPorFiltros");

        FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
        filtrosTramite.setEstadoTramite(ETramiteEstado.RECIBIDO.getCodigo());
        //filtrosTramite.setNumeroRadicacion("2575400005962012");
        FiltroDatosConsultaPredio filtrosPredio = new FiltroDatosConsultaPredio();
//		filtrosPredio.setNumeroPredialS1("08");
//		filtrosPredio.setNumeroPredialS2("001");
//		filtrosPredio.setNumeroPredialS3("01");
//		filtrosPredio.setNumeroPredialS4("01");
//        filtrosPredio.setNumeroPredialS5("00");
//        filtrosPredio.setNumeroPredialS6("00");
//        filtrosPredio.setNumeroPredialS7("0003");
//        filtrosPredio.setNumeroPredialS8("0003");
//        filtrosPredio.setNumeroPredialS9("5");
//        filtrosPredio.setNumeroPredialS10("00");
//        filtrosPredio.setNumeroPredialS11("00");
//        filtrosPredio.setNumeroPredialS12("0001");

        //filtrosPredio.setNumeroPredial("257540102000001080031000000000");
        FiltroDatosConsultaSolicitante filtrosSolicitante = new FiltroDatosConsultaSolicitante();
        HashMap<Long, Tramite> vtpList;

        try {
            LOGGER.debug("//----- INICIO TEST -----//");
            vtpList = dao.consultaDeTramitesPorFiltros(filtrosTramite,
                filtrosPredio, filtrosSolicitante, "", 500);
            Assert.assertNotNull(vtpList);

            LOGGER.debug("Numero predial:" + filtrosPredio.getNumeroPredial());
            LOGGER.debug("Lista de VTramitePredio size:" + vtpList.size());
            for (Map.Entry dato : vtpList.entrySet()) {
                LOGGER.debug("Tramite id:" + ((Tramite) dato.getValue()).getId());
            }

            LOGGER.debug("//------ FIN TEST ------//");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
