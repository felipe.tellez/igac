/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.generales.test.unit;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.snc.dao.generales.ICirculoRegistralDAO;
import co.gov.igac.snc.dao.generales.impl.CirculoRegistralDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class CirculoRegistralDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CirculoRegistralDAOBeanTest.class);

    private ICirculoRegistralDAO dao = new CirculoRegistralDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * pre: hay datos en la bd
     */
    @Test
    public void testFindByMunicipio() {

        LOGGER.debug("unit test CirculoRegistralDAOBeanTest#testFindByMunicipio...");
        String idMunicipio;
        List<CirculoRegistral> lista;

        idMunicipio = "11001";
        //idMunicipio = "05440";
        try {
            lista = this.dao.findByMunicipio(idMunicipio);

            Assert.assertTrue(lista.size() >= 1);
            LOGGER.debug("passed!. size = " + lista.size());
        } catch (Exception e) {
            LOGGER.error("error!!:" + e.getMessage(), e);
            Assert.assertFalse(true);
        }

    }

//end of class
}
