package co.gov.igac.snc.dao.conservacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IFichaMatrizDAO;
import co.gov.igac.snc.dao.conservacion.impl.FichaMatrizDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class FichaMatrizDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PFichaMatrizTorreDAOBeanTest.class);

    private IFichaMatrizDAO dao = new FichaMatrizDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    //---------------------------------------//
    /**
     * Test para probar el cargue de la lista de fichas matriz asociadas a un predio.
     *
     * @author david.cifuentes
     */
    @Test
    public void testGetFichaMatrizByNumeroPredialPredio() {

        LOGGER.debug("FichaMatrizDAOBeanTest#testGetFichaMatrizByNumeroPredialPredio");

        FichaMatriz fm;
        String numeroPredial = "080010103000002640040900000000";
        try {
            fm = this.dao.getFichaMatrizByNumeroPredialPredio(numeroPredial);
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }
}
