package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.IModeloResolucionDAO;
import co.gov.igac.snc.dao.tramite.IVSolicitudPredioDAO;
import co.gov.igac.snc.dao.tramite.impl.ModeloResolucionDAOBean;
import co.gov.igac.snc.dao.tramite.impl.VSolicitudPredioDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.test.unit.BaseTest;

public class VSolicitudPredioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VSolicitudPredioDAOBeanTest.class);

    private IVSolicitudPredioDAO dao = new VSolicitudPredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testBuscarSolicitudPorFiltros() {
        try {
            VSolicitudPredio filtro = new VSolicitudPredio();
            filtro.setNumeroIdentificacion("764925");
            List<VSolicitudPredio> vSolicitudPredios = this.dao
                .buscarSolicitudPorFiltros(filtro, 0, 0);
            LOGGER.debug("Tamaño: " + vSolicitudPredios.size());
            for (VSolicitudPredio vsp : vSolicitudPredios) {
                LOGGER.debug(vsp.getNumeroRadicacionSolicitud());
            }

            Assert.assertNotNull(vSolicitudPredios);
            Assert.assertTrue(vSolicitudPredios.size() > 0);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            Assert.fail();
        }

    }

}
