/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales.test.unit;

import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.generales.IPlantillaSeccionReporteDAO;
import co.gov.igac.snc.dao.generales.impl.PlantillaSeccionReporteDAOBean;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class PlantillaSeccionReporteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ManzanaVeredaDAOBeanTest.class);

    private IPlantillaSeccionReporteDAO dao = new PlantillaSeccionReporteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void guardarTest() {

        PlantillaSeccionReporte pl = new PlantillaSeccionReporte();
        pl.setEstructuraOrganizacionalCod("7000");
        pl.setUrlCabecera("url");
        pl.setUrlPieDePagina("url");
        pl.setUrlMarcaDeAgua("url");
        pl.setFechaEnFirme(new Date());
        pl.setFechaLog(new Date());
        pl.setUsuarioLog("prudelegada18");

        try {
            this.em.getTransaction().begin();
            this.dao.persist(pl);
            this.em.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            Assert.assertTrue(false);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void buscarTest() {

        PlantillaSeccionReporte pl;

        try {
            pl = this.dao.findById(1l);
            Assert.assertNull(pl);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            Assert.assertTrue(false);
        }
    }

}
