package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.impl.MunicipioDAOBean;
import co.gov.igac.snc.persistence.entity.vistas.VTramitePredio;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;

/**
 * @author david.cifuentes
 */
public class MunicipioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MunicipioDAOBeanTest.class);

    private IMunicipioDAO dao = new MunicipioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento() {
        LOGGER.debug(
            "MunicipioDAOBeanTest#testBuscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento");
        List<Municipio> municipiosConCatasCentralizado;

        // Código del Departamento Valle del Cauca
        String departamentoCodigo = "76";
        try {
            municipiosConCatasCentralizado = this.dao
                .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(departamentoCodigo);
            // Debido a que Cali es un municipio con Catastro desentralizado,
            // éste no debería aparecer en la lista.

            assertNotNull(municipiosConCatasCentralizado);
            assertTrue(municipiosConCatasCentralizado.size() > 0);
            LOGGER.info("Size return municipios: " +
                municipiosConCatasCentralizado.size());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void findByEntidadTerritorialTest() {
        LOGGER.debug("MunicipioDAOBeanTest#findByEntidadTerritorial");
        List<Municipio> municipiosConCatasCentralizado;

        String idTerritorial = "7000";
        try {
            municipiosConCatasCentralizado = this.dao.findByEntidadTerritorial(idTerritorial);

            assertNotNull(municipiosConCatasCentralizado);
            LOGGER.info("Size return municipios: " +
                municipiosConCatasCentralizado.get(0).getJurisdiccions().get(0).
                    getEstructuraOrganizacional().getCodigo());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

}
