/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IAvaluoMovimientoDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoMovimientoDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class AvaluoMovimientoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoMovimientoDAOBeanTest.class);
    private IAvaluoMovimientoDAO dao = new AvaluoMovimientoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar {@link AvaluoMovimientoDAOBean#ConsultarAsignacionActualAvaluo(Long)}
     *
     * @author felipe.cadena
     */
    @Test
    public void testCalcularDiasSuspensionAvaluo() {
        LOGGER.debug("iniciando AvaluoMovimientoDAOBeanTest#testCalcularDiasSuspensionAvaluo");

        Integer result = null;

        result = dao.calcularDiasSuspensionAvaluo(172L);

        LOGGER.debug("***********************************************");
        if (result != null) {
            LOGGER.debug("****Dias suspension********** " + result + " ******************");
        }
        LOGGER.debug("***********************************************");

        result = dao.calcularDiasAmpliacionAvaluo(172L);
        LOGGER.debug("***********************************************");
        if (result != null) {
            LOGGER.debug("****Dias ampliacion********** " + result + " ******************");
        }
        LOGGER.debug("***********************************************");

        LOGGER.debug("finalizando AvaluoMovimientoDAOBeanTest#testCalcularDiasSuspensionAvaluo");
    }

}
