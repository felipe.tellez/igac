/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.impl.PredioInconsistenciaDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class PredioInconsistenciaDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioInconsistenciaDAOBeanTests.class);

    private PredioInconsistenciaDAOBean dao = new PredioInconsistenciaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//--------------------------------------------------------------------------------------------------
    @Test
    public void testFindByPredioId() {
        LOGGER.debug("tests: PredioInconsistenciaDAOBeanTests#testFindByPredioId");

        Long predioId = 7L;

        try {
            this.dao.findByPredioId(predioId);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

// end of class
}
