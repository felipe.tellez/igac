package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.util.LdapDAO;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author juan.mendez
 *
 */
public class LdapDaoTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(LdapDaoTests.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);

    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /*
     * //
     * --------------------------------------------------------------------------------------------------
     * @Test public void testConsultaLdapJefeConservacion() {
     * LOGGER.debug("testConsultaLdapJefeConservacion"); try { List<Attributes> jefes =
     * LdapDAO.getUsuariosJefeConservacion("ATLANTICO");
     *
     * assertNotNull(jefes); for (Attributes attrs : jefes) { if (attrs != null) { for
     * (NamingEnumeration<? extends Attribute> vals = attrs .getAll(); vals.hasMoreElements();) {
     * Attribute atr = vals.nextElement(); LOGGER.debug("Attribute: " + atr); } } LOGGER.debug("
     * ************** "); } } catch (Exception e) { LOGGER.error(e.getMessage(), e);
     * fail(e.getMessage(), e); } }
     */

 /*
     * @Test public void testGetUsuariosJefeConservacion() {
     * LOGGER.debug("testGetUsuariosJefeConservacion"); try { UsuarioDTO usuario = new UsuarioDTO();
     * usuario.setDescripcionTerritorial("ATLANTICO"); usuario.setCodigoTerritorial("6040");
     * List<UsuarioDTO> jefes = LdapDAO.getJefeConservacion(usuario); assertNotNull(jefes); for
     * (UsuarioDTO jefe : jefes) { LOGGER.debug("jefe:" + jefe); } } catch (Exception e) {
     * LOGGER.error(e.getMessage(), e); fail(e.getMessage(), e); } }
     */
    /**
     * @author felipe.cadena
     */
    @Test
    public void testGetUsuariosEstructura() {
        LOGGER.debug("testGetUsuariosRolTerritorial");
        try {
            String estructuraOrganizacional = "DLG_BARRANQUILLA";
            //String uoc = "UOC_Soacha";
            String uoc = null;
            //String estructuraOrganizacional = "ATLANTICO";
            List<UsuarioDTO> users = LdapDAO.getUsuariosEstructura(estructuraOrganizacional, uoc);
            assertNotNull(users);
            assertFalse(users.isEmpty());
            LOGGER.debug("** users.size(): " + users.size());
            //LOGGER.debug("users:" + users);

            for (UsuarioDTO user : users) {
                LOGGER.debug("********************************");
                LOGGER.debug("** user: " + user.getLogin());
                //error prusoacha08
                LOGGER.debug("** uoc: " + user.getCodigoUOC());
                LOGGER.debug("** DescripcionEstructuraOrganizacional: " + user.
                    getDescripcionEstructuraOrganizacional());
                for (String rol : user.getRoles()) {
                    LOGGER.debug("** rol: " + rol);
                }
                assertNotNull(user.getLogin());
                assertNotNull(user.getDescripcionEstructuraOrganizacional());
                assertNotNull(user.getPrimerNombre());
                assertNotNull(user.getRoles());
                assertEquals(user.getDescripcionEstructuraOrganizacional(), estructuraOrganizacional);
            }
            LOGGER.debug("********************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testGetUsuariosRolTerritorial() {
        LOGGER.debug("testGetUsuariosRolTerritorial");
        try {
            String estructuraOrganizacional = "CUNDINAMARCA";
            //String estructuraOrganizacional = "ATLANTICO";
            List<UsuarioDTO> users = LdapDAO.getUsuariosRolTerritorial(estructuraOrganizacional,
                ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(users);
            assertFalse(users.isEmpty());
            LOGGER.debug("** users.size(): " + users.size());
            //LOGGER.debug("users:" + users);

            for (UsuarioDTO user : users) {
                LOGGER.debug("********************************");
                LOGGER.debug("** user: " + user.getLogin());
                //error prusoacha08
                LOGGER.debug("** uoc: " + user.getCodigoUOC());
                LOGGER.debug("** DescripcionEstructuraOrganizacional: " + user.
                    getDescripcionEstructuraOrganizacional());
                for (String rol : user.getRoles()) {
                    LOGGER.debug("** rol: " + rol);
                }
                assertNotNull(user.getLogin());
                assertNotNull(user.getDescripcionEstructuraOrganizacional());
                assertNotNull(user.getPrimerNombre());
                assertNotNull(user.getRoles());
                assertEquals(user.getDescripcionEstructuraOrganizacional(), estructuraOrganizacional);
            }
            LOGGER.debug("********************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testSearchUser() {
        LOGGER.debug("testSearchUser");
        try {

            UsuarioDTO user = LdapDAO.searchUser("prudelegada01");
            assertNotNull(user);
            LOGGER.info("user:" + user);
            LOGGER.info("user:" + user.getNombreCompleto());
            LOGGER.info("user:" + user.getEmail());
            assertNotNull(user.getLogin());
            assertNotNull(user.getEmail());
            assertNotNull(user.getDescripcionEstructuraOrganizacional());
            assertNotNull(user.getPrimerNombre());
            //assertNotNull(user.getSegundoNombre());
            assertNotNull(user.getRoles());
            assertEquals(user.getDescripcionEstructuraOrganizacional(), "ATLANTICO");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Se genera un listado con todos los usuarios del directorio activo
     *
     * @author juan.mendez
     */
    @Test
    public void testListAllUsers() {
        LOGGER.debug("testListAllUsers");
        try {
            List<UsuarioDTO> users = LdapDAO.listAllUsers();
            for (UsuarioDTO user : users) {
                String roles = "";
                if (user.getRoles() != null) {
                    roles = user.getRolesCadena();
                }
                LOGGER.debug(
                    //user.getCodigoEstructuraOrganizacional() + " , "+user.getCodigoUOC()
                    //+ " , "+user.getCodigoTerritorial()+ 
                    " ; " +
                    user.getLogin() + " ; " + user.getNombreCompleto() //+ " ; "+user.getDescripcionTerritorial()+ " ; "+user.getDescripcionUOC()
                    +
                     " ; " + user.getIdentificacion() + " ; " + user.getTipoIdentificacion() +
                    " ; " + roles);

            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testGetCacheUsuarioDirectorTerritorial() {
        LOGGER.debug("testGetCacheUsuarioDirectorTerritorial");
        try {
            UsuarioDTO user = LdapDAO.searchUser("prucundi11");
            assertNotNull(user);
            LOGGER.debug("user:" + user);
            assertNotNull(user.getLogin());
            assertNotNull(user.getDescripcionEstructuraOrganizacional());
            assertEquals(user.getDescripcionEstructuraOrganizacional(), "CUNDINAMARCA");
            assertNotNull(user.getPrimerNombre());
            assertNotNull(user.getRoles());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fredy.wilches
     */
    @Test
    public void testGetFuncionariosTerritorial() {
        LOGGER.debug("testGetFuncionariosTerritorial");
        try {
            List<UsuarioDTO> funcionarios = LdapDAO.getFuncionariosDto("CUNDINAMARCA", "UOC_SOACHA");
            assertNotNull(funcionarios);
            LOGGER.debug("Se encontraron en total " + funcionarios.size() + " funcionarios");
            LOGGER.debug("funcionarios:" + funcionarios);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fredy.wilches
     */
    @Test
    public void testGetUsuariosEstructuraRol2() {
        LOGGER.debug("testGetUsuariosEstructuraRol2");
        try {
            String estructuraOrganizacional = "CUNDINAMARCA";
            ERol rol = ERol.EJECUTOR_TRAMITE;
            List<UsuarioDTO> funcionarios = LdapDAO.getUsuariosEstructuraRol2(
                estructuraOrganizacional, rol);
            assertNotNull(funcionarios);
            LOGGER.debug("funcionarios:" + funcionarios);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testGetUsuariosEstructuraRol2_02() {
        LOGGER.debug("testGetUsuariosEstructuraRol2_02");
        try {
            String estructuraOrganizacional = "ATLANTICO";
            ERol rol = ERol.FUNCIONARIO_RADICADOR;
            List<UsuarioDTO> funcionarios = LdapDAO.getUsuariosEstructuraRol2(
                estructuraOrganizacional, rol);
            assertNotNull(funcionarios);
            LOGGER.debug("Número de funcionarios encontrados:" + funcionarios.size());
            for (UsuarioDTO usuarioDTO : funcionarios) {
                LOGGER.debug(usuarioDTO.getLogin());
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fredy.wilches
     */
    @Test
    public void testGetSubdirectorCatastro() {
        ERol rol = ERol.SUBDIRECTOR_CATASTRO;
        LOGGER.debug("testGetSubdirectorCatastro");
        try {
            UsuarioDTO subdirector = LdapDAO.getSubdirectorCatastro();
            assertNotNull(subdirector);
            LOGGER.debug("Subdirector:" + subdirector);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author lorena.salamanca
     */
    @Test
    public void testGetUsuariosEstructuraRolYUOC() {
        LOGGER.debug("testGetUsuariosEstructuraRol2_02");
        try {
            String estructuraOrganizacional = "CUNDINAMARCA";
            ERol rol = ERol.FUNCIONARIO_RADICADOR;
            List<UsuarioDTO> funcionarios = LdapDAO.getUsuariosEstructuraRolYUOC(
                estructuraOrganizacional, rol);
            assertNotNull(funcionarios);
            LOGGER.debug("Número de funcionarios encontrados:" + funcionarios.size());
            for (UsuarioDTO usuarioDTO : funcionarios) {
                LOGGER.debug(usuarioDTO.getLogin());
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
