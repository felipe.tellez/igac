package co.gov.igac.snc.dao.test.unit;

import static org.testng.Assert.fail;
import static org.testng.AssertJUnit.assertNotNull;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Prueba unitaria para la ejecución del api documental
 *
 * @author juan.mendez
 */
public class DocumentalUnitTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentalUnitTests.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);

    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.mendez
     */
    @Test
    public void testGenerarGrafo() {
        LOGGER.debug("tests: DocumentalUnitTests#testGenerarGrafo");
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            assertNotNull(service);
            String archivoSalida = service.generarImagenGrafo(GRAPH_SNC_FILE_PATH);
            assertNotNull(archivoSalida);
            LOGGER.debug("archivoSalida:" + archivoSalida);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
