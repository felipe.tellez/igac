/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.test.unit;

import co.gov.igac.snc.dao.tramite.gestion.IComisionEstadoDAO;
import co.gov.igac.snc.dao.tramite.gestion.impl.ComisionEstadoDAOBean;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class ComisionEstadoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionDAOBeanTests.class);

    private IComisionEstadoDAO dao = new ComisionEstadoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------{

    /**
     * @author pedro.garcia
     *
     * PRE: existe una comisión con el id 655
     */
    @Test
    public void testGetHistoricalRecord() {

        LOGGER.debug("unit test ComisionEstadoDAOBeanTests#testGetHistoricalRecord ...");

        Long idComision = 655l;
        int numRegistros = 1;
        Documento docMemo;

        List<ComisionEstado> answer = null;

        idComision = 666l;

        try {
            answer = this.dao.getHistoricalRecord(idComision, numRegistros);
            Assert.assertNotNull(answer);
            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug(" ... passed!. fetched " + answer.size() + " rows");

                if (answer.get(0).getMemorandoDocumento() != null) {
                    docMemo = answer.get(0).getMemorandoDocumento();
                    LOGGER.debug("trajo doc con id de repositorio= " +
                        docMemo.getIdRepositorioDocumentos());
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("falló en la prueba ComisionDAOBeanTests#testPersist");
        }

    }

}
