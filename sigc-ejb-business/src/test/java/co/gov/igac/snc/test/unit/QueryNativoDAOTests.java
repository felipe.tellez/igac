package co.gov.igac.snc.test.unit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.util.QueryNativoDAO;
import java.util.Calendar;

/**
 * @author juan.mendez
 */
public class QueryNativoDAOTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(QueryNativoDAOTests.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        // dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.mendez
     */
    @Test
    public void testNormalizarDireccion() {
        LOGGER.debug("unit tests: ProcedimientoAlmacenadoDAOTests#testNormalizarDireccion");

        try {
            String direccion = "carrera 1 a este numero 30-42 apartamento 401 casa 2 interior 16";
            String direccionEsperada = "K 1 A ESTE 30 42 AP 401 IN 16 CS 2";

            String retorno = QueryNativoDAO.normalizarDireccion(this.em, direccion);
            assertNotNull(retorno);
            assertEquals(retorno, direccionEsperada);

            LOGGER.debug("direccion:" + direccion);
            LOGGER.debug("retorno:" + retorno);;
            LOGGER.debug("direccionEsperada:" + direccionEsperada);;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testNormalizarDireccionConAcentos() {
        LOGGER.debug("unit tests: ProcedimientoAlmacenadoDAOTests#testNormalizarDireccion");

        try {
            String direccion = "carrera 1 a Este número 30-42 apartamento 401 casa 2 interior 16";
            String direccionEsperada = "K 1 A ESTE 30 42 AP 401 IN 16 CS 2";

            String retorno = QueryNativoDAO.normalizarDireccion(this.em, direccion);
            assertNotNull(retorno);
            assertEquals(retorno, direccionEsperada);

            LOGGER.debug("direccion:" + direccion);
            LOGGER.debug("retorno:" + retorno);;
            LOGGER.debug("direccionEsperada:" + direccionEsperada);;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testCalcularFechaConDiasHabiles() {
        LOGGER.debug("unit test QueryNativoDAOTests#testCalcularFechaConDiasHabiles ...");

        try {
            Date fechaInicial = new Date();
            fechaInicial.setYear(2011 - 1900);
            fechaInicial.setMonth(11 - 1);
            fechaInicial.setDate(1);
            fechaInicial.setHours(8);
            fechaInicial.setMinutes(0);
            fechaInicial.setSeconds(0);
            int diasHabiles = 10;

            Date retorno = QueryNativoDAO.calcularFechaConDiasHorasHabiles(this.em, fechaInicial,
                diasHabiles);
            assertNotNull(retorno);
            LOGGER.debug("fechaInicial:" + fechaInicial);
            LOGGER.debug("retorno:" + retorno);
            assertEquals(retorno.getYear() + 1900, 2011);
            assertEquals(retorno.getMonth() + 1, 11);
            assertEquals(retorno.getDate(), 18);
            assertEquals(retorno.getHours(), 8);
            assertEquals(retorno.getMinutes(), 0);
            assertEquals(retorno.getSeconds(), 0);

            //2011-11-25
            //LOGGER.debug("direccionEsperada:" + direccionEsperada);;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testCalcularDiasHabilesEntreDosFechas() {
        LOGGER.debug("unit test QueryNativoDAOTests#testCalcularDiasHabilesEntreDosFechas ...");

        try {
            Calendar c = Calendar.getInstance();
            c.set(2012, 9, 31);
            Date fechaDesde = c.getTime();
            c.set(2012, 11, 1);
            Date fechaHasta = c.getTime();

            int retorno = QueryNativoDAO.calcularDiasHabilesEntreDosFechas(this.em, fechaDesde,
                fechaHasta);
            assertNotNull(retorno);
            LOGGER.debug("fechaInicial:" + fechaDesde);
            LOGGER.debug("retorno:" + fechaHasta);

            LOGGER.debug("dias habiles:" + retorno);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testAnioUltimaActualizacion() {
        LOGGER.debug("unit test QueryNativoDAOTests#testAnioUltimaActualizacion ...");

        try {
            Integer retorno = QueryNativoDAO.anioUltimaActualizacion(this.em, "5555500");
            LOGGER.debug("Año vigencia:" + retorno);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorcentajeIncremento() {
        LOGGER.debug("unit test QueryNativoDAOTests#testObtenerPorcentajeIncremento ...");

        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DATE, 1);

        Long predioId = 2798l;

        try {
            Double retorno = QueryNativoDAO.obtenerPorcentajeIncremento(this.em, c.getTime(),
                predioId);
            LOGGER.debug("porcentaje:" + retorno);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
