/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.vistas.IVComisionDAO;
import co.gov.igac.snc.dao.vistas.impl.VComisionDAOBean;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pedro.garcia
 */
public class VComisionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(VComisionDAOBeanTest.class);

    private IVComisionDAO dao = new VComisionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        this.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        this.tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: hay datos de prueba en la tabla COMISIONES que corresponden a los usados en esta prueba
     */
    @Test
    public void testCountComisionesSinMemo() {

        LOGGER.debug("unit tests: VComisionDAOBeanTest#testCountComisionesSinMemo");

        int hm = -1;
        List<String[]> criteria;
        String[] criterion;

        criteria = new ArrayList<String[]>();

        criterion = new String[3];
        criterion[0] = "territorialCodigo";
        criterion[1] = "varchar";
        criterion[2] = "11000";
        criteria.add(criterion);

        criterion[0] = "estado";
        criterion[1] = "varchar";
        criterion[2] = EComisionEstado.CREADA.name();
        criteria.add(criterion);

        try {
            hm = this.dao.countByExactCriteria(criteria);
            LOGGER.debug("passed. count = " + hm);
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Este tiene de especial que llama en un query jqpl a una vista
     *
     * @author pedro.garcia pre: hay datos de prueba en la tabla COMISIONES que corresponden a los
     * usados en esta prueba
     *
     */
    @Test
    public void testFindComisionesSinMemo() {

        LOGGER.debug("unit test VComisionDAOBeanTest#testFindComisionesSinMemo ...");
        List<VComision> answer = null;
        String territorialId = "6040";
        List<Long> idTramites = new ArrayList<Long>();
        idTramites.add(43575l);
        try {
            answer = this.dao.findComisionesSinMemo(territorialId, idTramites,
                EComisionTipo.CONSERVACION.getCodigo(), new HashMap<String, String>());

            LOGGER.debug("... passed. trajo " + answer.size() + " registros");

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindById() {

        LOGGER.debug("unit test VComisionDAOBeanTest#testFindById ...");

        List<VComision> answer = null;
        Long idComision = 140l;
        List<String[]> criteria = new ArrayList<String[]>();
        String[] criterion1 = new String[3];

        criterion1[0] = "id";
        criterion1[1] = "number";
        criterion1[2] = idComision.toString();

        criteria.add(criterion1);

        try {
            answer = this.dao.findByExactCriteria(criteria);

            assertTrue(true);
            LOGGER.debug("... passed. trajo " + answer.size() + " registros");

            if (!answer.isEmpty()) {
                VComision c = answer.get(0);
                LOGGER.debug("duracion = " + c.getDuracion() + " :: duracion total = " + c.
                    getDuracionTotal() +
                    " :: duracion ajuste = " + c.getDuracionAjuste());
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testCountComisionesParaAdmin() {

        LOGGER.debug("unit test VComisionDAOBeanTest#testCountComisionesParaAdmin ...");

        int howMany;
        String idTerritorial;

        idTerritorial = "6040";

        try {
            howMany = this.dao.countComisionesParaAdmin(idTerritorial,
                ERol.RESPONSABLE_CONSERVACION.getRol());
            LOGGER.debug("conteo = " + howMany);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            fail("ocurrió una excepción");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * PRE: existe al menos una comisión para la territorial usada
     */
    @Test
    public void testFindComisionesParaAdmin() {

        LOGGER.debug("unit test VComisionDAOBeanTest#testFindComisionesParaAdmin ...");

        String idTerritorial = "6160";

        List<VComision> answer;

        try {
            answer = this.dao.findComisionesParaAdmin(idTerritorial,
                ERol.RESPONSABLE_CONSERVACION.getRol(), null, null, null);
            assertNotNull(answer);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            fail("ocurrió una excepción");
        }

    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * Test del método buscar comisiones para aprobar
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarComisionesParaAprobar() {

        LOGGER.debug("VComisionDAOBeanTest#testBuscarComisionesParaAprobar ...");

        List<Long> idTramites = new ArrayList<Long>();
        idTramites.add(Long.valueOf(237192));

        List<VComision> answer;
        answer = this.dao.buscarComisionesParaAprobar(idTramites,
            EComisionTipo.CONSERVACION.getCodigo(), null, null, null);

        assertNotNull(answer);
        LOGGER.debug("Comisiones para aprobar: " + answer.size());

    }

//end of class
}
