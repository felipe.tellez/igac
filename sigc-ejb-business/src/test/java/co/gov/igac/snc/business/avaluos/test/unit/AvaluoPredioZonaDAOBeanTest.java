/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IAvaluoPredioZonaDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoPredioZonaDAOBean;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Clase para probar metodos de la clase AvaluoPredioZonaDAOBean
 *
 * @author felipe.cadena
 */
public class AvaluoPredioZonaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoPredioZonaDAOBeanTest.class);
    private IAvaluoPredioZonaDAO dao = new AvaluoPredioZonaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     */
    @Test
    public void testCalcularZonasPreviasAvaluo() {
        LOGGER.debug("unit test AvaluoPredioZonaDAOBeanTest#testCalcularZonasPreviasAvaluo ...");

        Boolean resultado;

        try {

            resultado = this.dao.calcularZonasPreviasAvaluo(172L);
            LOGGER.debug("****************************************");
            LOGGER.debug("*************" + resultado + "**********");
            LOGGER.debug("****************************************");

            assertTrue(true);
            LOGGER.debug(" ... passed!!");
        } catch (Exception ex) {
            fail("ocurrió un error.");
            LOGGER.error("excepción: " + ex.getMessage());
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testEliminarZonasPreviasAvaluo() {
        LOGGER.debug("unit test AvaluoPredioZonaDAOBeanTest#testEliminarZonasPreviasAvaluo ...");

        Boolean resultado;

        try {
            this.em.getTransaction().begin();
            resultado = this.dao.eliminarZonasPreviasAvaluo(172L);
            this.em.getTransaction().commit();
            LOGGER.debug("****************************************");
            LOGGER.debug("*************" + resultado + "**********");
            LOGGER.debug("****************************************");

            assertTrue(true);
            LOGGER.debug(" ... passed!!");
        } catch (Exception ex) {
            fail("ocurrió un error al eliminar.");
            LOGGER.error("excepción: " + ex.getMessage());
        }
    }
}
