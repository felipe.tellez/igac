package co.gov.igac.snc.dao.conservacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPersonaDAO;
import co.gov.igac.snc.dao.conservacion.IPPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPersonaDAOBean;
import co.gov.igac.snc.dao.conservacion.impl.PPersonaPredioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author fabio.navarrete
 *
 */
public class PPersonaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPersonaDAOBeanTest.class);

    private IPPersonaDAO dao = new PPersonaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void insertarPersonaTest() {
        LOGGER.debug("PPersonaDAOBeanTest#insertarPersonaTest");
        try {
            //this.em.getTransaction().begin();

            PPersona ppersona = dao.findById(3L);
            ppersona.setId(null);
            ppersona.setNumeroIdentificacion("123");

            dao.update(ppersona);
            //this.em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
        }
    }

}
