/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IRepReporteEjecucionUsuarioDAO;
import co.gov.igac.snc.dao.conservacion.impl.RepReporteEjecucionUsuarioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test de unidad para la clase RepReporteEjecucionUsuario
 *
 *
 * @author felipe.cadena
 */
public class RepReporteEjecucionUsuarioDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RepReporteEjecucionUsuarioDAOBeanTests.class);

    private IRepReporteEjecucionUsuarioDAO dao = new RepReporteEjecucionUsuarioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("RepReporteEjecucionUsuarioDAOBeanTests#testUpdate");

        try {

            RepReporteEjecucionUsuario testEntity = new RepReporteEjecucionUsuario();

            testEntity.setReporteEjecucionId(52l);
            testEntity.setUsuarioGenera("str::b4wrsy62fr7z4ghw7b9");
            testEntity.setUsuarioLog("str::is2hzgbz5yvehggmn29");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorUsuario() {

        LOGGER.debug("RepReporteEjecucionUsuarioDAOBeanTests#testObtenerPorUsuario");

        try {

            List<RepReporteEjecucionUsuario> testEntity = this.dao.obtenerPorUsuario("PAOLAG");
            assertNotNull(testEntity);
            LOGGER.debug("***********************");
            LOGGER.debug("" + testEntity.size());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
