package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionContratoDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionContratoDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author franz.gamba
 */
public class ActualizacionContratoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ActualizacionContratoDAOBeanTest.class);

    private IActualizacionContratoDAO dao = new ActualizacionContratoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testObtenerContratosProActualizacion() {
        try {
            LOGGER.
                debug("Prueba de obtencion de contratos por un proceso de actualizacion asociado");
            List<ActualizacionContrato> contratos = new ArrayList<ActualizacionContrato>();
            Long idActualizacion = (long) 8;

            contratos = dao.obtenerContratosProActualizacion(idActualizacion);
            assertNotNull(contratos);

            for (ActualizacionContrato ac : contratos) {
                LOGGER.debug("NOMBRE = " + ac.getContratistaNombre());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

}
