package co.gov.igac.snc.dao.util.test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.util.ZipUtil;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 */
public class ZipUtilTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ZipUtilTests.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        // dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     *
     * */
    @Test
    public void testZipFolder() {
        LOGGER.debug("testZipFolder");

        try {
            File tempDir = FileUtils.getTempDirectory();
            File newDir = new File(tempDir, UUID.randomUUID().toString());
            newDir.mkdir();
            LOGGER.debug("newDir:" + newDir);

            InputStream in = new URL(
                "http://www.elespectador.com/noticias/economia/un-empujon-consultas-previas-articulo-459818").
                openStream();
            String content = IOUtils.toString(in);
            IOUtils.closeQuietly(in);

            File textFile = new File(newDir, "ABC_ÁÉÍÓÚ_áéíóú" + UUID.randomUUID().toString() +
                ".txt");
            textFile.createNewFile();

            FileUtils.writeStringToFile(textFile, content, Charset.forName("ISO-8859-1"));

            textFile = new File(newDir, "ABC_ÁÉÍÓÚ_áéíóú" + UUID.randomUUID().toString() + ".txt");
            textFile.createNewFile();

            FileUtils.writeStringToFile(textFile, content, Charset.forName("ISO-8859-1"));

            File destZip = new File(tempDir, "ABC_ÁÉÍÓÚ_áéíóú" + UUID.randomUUID().toString() +
                ".zip");
            LOGGER.debug("destZip:" + destZip);

            ZipUtil.zipFolder(newDir.getAbsolutePath(), destZip.getAbsolutePath());
            assertTrue(destZip.exists());
            LOGGER.debug("destZip:" + destZip.length());
            assertTrue(destZip.length() > 100);

            //limpiar archivos creados
            newDir.delete();
            destZip.delete();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Test
    public void testZipFiles() {
        LOGGER.debug("testZipFiles");

        try {
            File tempDir = FileUtils.getTempDirectory();
            File newDir = new File(tempDir, UUID.randomUUID().toString());
            newDir.mkdir();
            LOGGER.debug("newDir:" + newDir);

            InputStream in = new URL(
                "http://www.elespectador.com/noticias/economia/un-empujon-consultas-previas-articulo-459818").
                openStream();
            String content = IOUtils.toString(in);
            IOUtils.closeQuietly(in);

            List<String> listadoRutasArchivos = new ArrayList<String>();

            File textFile = new File(newDir, "ABC_ÁÉÍÓÚ_áéíóú" + UUID.randomUUID().toString() +
                ".txt");
            textFile.createNewFile();
            LOGGER.debug("file:" + textFile.getAbsolutePath());
            listadoRutasArchivos.add(textFile.getAbsolutePath());
            FileUtils.writeStringToFile(textFile, content, Charset.forName("ISO-8859-1"));

            textFile =
                new File(newDir, "ABC_ÁÉÍÓÚÑ_áéíóúñ_" + UUID.randomUUID().toString() + ".txt");
            textFile.createNewFile();

            LOGGER.debug("file:" + textFile.getAbsolutePath());
            listadoRutasArchivos.add(textFile.getAbsolutePath());
            FileUtils.writeStringToFile(textFile, content, Charset.forName("ISO-8859-1"));

            String zipPath = ZipUtil.zipFiles(listadoRutasArchivos);
            assertNotNull(zipPath);
            LOGGER.debug("zipPath:" + zipPath);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

}
