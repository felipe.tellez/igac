/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.test.unit;

import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDatoDAO;
import co.gov.igac.snc.dao.tramite.gestion.impl.ComisionTramiteDatoDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class ComisionTramiteDatoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ComisionTramiteDatoDAOBeanTests.class);

    private IComisionTramiteDatoDAO dao = new ComisionTramiteDatoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------{

    /**
     * @author javier.aponte
     *
     * PRE: existen datos en la tabla comisión trámite dato
     */
    @Test
    public void testBuscarComisionTramiteDatoPorTramiteId() {

        LOGGER.debug(
            "unit test ComisionTramiteDatoDAOBeanTests#testBuscarComisionTramiteDatoPorTramiteId ...");

        Long tramiteId = 42862L;
        List<ComisionTramiteDato> answer = null;

        try {
            answer = this.dao.buscarComisionTramiteDatoPorTramiteId(tramiteId);
            Assert.assertNotNull(answer);
            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug(" ... Prueba exitosa!. trajó " + answer.size() + " resultados");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail(
                "falló en la prueba ComisionTramiteDatoDAOBeanTests#testBuscarComisionTramiteDatoPorTramiteId");
        }

    }

}
