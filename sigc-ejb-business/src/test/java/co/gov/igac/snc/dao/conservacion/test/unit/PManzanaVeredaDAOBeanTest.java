/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.snc.dao.conservacion.IPManzanaVeredaDAO;
import co.gov.igac.snc.dao.conservacion.impl.PManzanaVeredaDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class PManzanaVeredaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ColindanteDAOBeanTests.class);

    private IPManzanaVeredaDAO dao = new PManzanaVeredaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * Prueba busqueda de manzanas proyectadas por tramite
     *
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorTramiteId() {
        LOGGER.debug("tests: ColindanteDAOBeanTests#testFindPrediosColindantesByNumeroPredial");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Long idTramite = 38672l;
            List<PManzanaVereda> answer = this.dao.obtenerPorTramiteId(idTramite);

            if (answer != null && !answer.isEmpty()) {

                for (PManzanaVereda temp : answer) {
                    LOGGER.debug("Codigo manzana " + temp.getCodigo());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prueba para crear una manzana proyectada
     *
     * @author felipe.cadena
     */
    @Test
    public void testCrearPManzana() {
        LOGGER.debug("tests: ColindanteDAOBeanTests#testFindPrediosColindantesByNumeroPredial");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Long idTramite = 38672l;
            Barrio b = new Barrio();
            b.setCodigo("0800101040000");
            PManzanaVereda mv = new PManzanaVereda("08001010400000157", b, "Manzana 0157",
                "felipe.cadena", new Date());
            Tramite t = new Tramite();
            t.setId(idTramite);
            mv.setTramite(t);

            this.dao.getEntityManager().getTransaction().begin();
            mv = this.dao.update(mv);
            this.dao.getEntityManager().getTransaction().commit();

            assertNotNull(mv);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
