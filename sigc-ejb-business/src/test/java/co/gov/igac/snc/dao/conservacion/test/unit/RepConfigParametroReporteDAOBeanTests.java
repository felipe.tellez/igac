/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IRepConfigParametroReporteDAO;
import co.gov.igac.snc.dao.conservacion.impl.RepConfigParametroReporteDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test para RepConfigParametroReporteDAOBean
 *
 * @author felipe.cadena
 */
public class RepConfigParametroReporteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RepConfigParametroReporteDAOBeanTests.class);

    private IRepConfigParametroReporteDAO dao = new RepConfigParametroReporteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("RepConfigParametroReporteDAOBeanTests#testUpdate");

        try {

            RepConfigParametroReporte testEntity = new RepConfigParametroReporte();

            testEntity.setReporteId(85l);
            testEntity.setCategoria("str::flav8yuy690kv1p0ysyvi");
            testEntity.setNombreParametro("str::8nri6iioj7cjypg5jyvi");
            testEntity.setHabilitado("str::xwdq2pwhdieq74lsor");
            testEntity.setRequerido("str::yk4iasu7q5qxzanbqpvi");
            testEntity.setUsuarioLog("str::1x41m33bwv1az72gwrk9");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerConfiguracionPorCategoria() {

        LOGGER.debug("RepConfigParametroReporteDAOBeanTests#testObtenerConfiguracionPorCategoria");

        try {
            List<RepConfigParametroReporte> result = this.dao.obtenerConfiguracionPorCategoria(
                "REPORTES ESTADISTICOS");
            assertNotNull(result);
            LOGGER.debug("***********************");
            LOGGER.debug(String.valueOf(result.size()));
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
