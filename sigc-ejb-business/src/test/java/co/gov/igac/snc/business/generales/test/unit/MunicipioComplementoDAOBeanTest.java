/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.generales.test.unit;

import co.gov.igac.snc.dao.generales.IMunicipioComplementoDAO;
import co.gov.igac.snc.dao.generales.impl.MunicipioComplementoDAOBean;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class MunicipioComplementoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MunicipioComplementoDAOBeanTest.class);

    private IMunicipioComplementoDAO dao = new MunicipioComplementoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author david.cifuentes
     */
    @Test
    public void testObtenerMunicipioComplemetnoPorId() {
        LOGGER.debug("MunicipioComplementoDAOBeanTest#testObtenerMunicipioComplemetnoPorId");
        MunicipioComplemento resultado;

        // Código del país
        String municipioCodigo = "08001";
        try {
            resultado = this.dao.findById(municipioCodigo);

            assertNotNull(resultado);
            LOGGER.info("Municipio complemento GDB: " + municipioCodigo + " -- " + resultado.
                getConInformacionGdb());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }

}
