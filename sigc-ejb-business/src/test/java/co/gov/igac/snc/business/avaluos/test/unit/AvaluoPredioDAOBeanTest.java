package co.gov.igac.snc.business.avaluos.test.unit;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.avaluos.IAvaluoPredioDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoPredioDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class AvaluoPredioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoPredioDAOBeanTest.class);

    private IAvaluoPredioDAO dao = new AvaluoPredioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar {@link AvaluoPredioDAOBean#buscarDepartamentosMunicipiosAvaluo(Long)}
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testBuscarDepartamentosMunicipiosAvaluo() {
        LOGGER.debug("iniciando AvaluoPredioDAOBeanTest#testBuscarDepartamentosMunicipiosAvaluo");

        List<Municipio> result = new ArrayList<Municipio>();

        result = dao.buscarDepartamentosMunicipiosAvaluo(156L);

        for (Municipio mpio : result) {

            LOGGER.debug(String.valueOf(mpio.getCodigo()));

        }

        LOGGER.debug("fin AvaluoPredioDAOBeanTest#testBuscarDepartamentosMunicipiosAvaluo");
    }

}
