package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionCompDAO;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.impl.PUnidadConstruccionCompDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author leidy.gonzalez
 */
public class PUnidadConstruccionCompDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PUnidadConstruccionDAOBeanTest.class);

    private IPUnidadConstruccionCompDAO dao = new PUnidadConstruccionCompDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // ---------------------------------------//
    /**
     * Test para buscar un PUnidadConstruccion por su id, trayendo las PUnidadConstruccionComp
     * asociadas.
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testBuscarUnidadDeConstruccionPorSuId() {

        LOGGER.debug("PUnidadConstruccionDAOBeanTest#buscarUnidadDelModeloDeConstruccionPorSuId");

        PUnidadConstruccion puc;
        Long idPUnidadConstruccion = 11620907L;
        try {
            List<PUnidadConstruccionComp> unidadesCom =
                this.dao.buscarUnidadDeConstruccionPorUnidadConstruccionId(idPUnidadConstruccion);

            List<PUnidadConstruccionComp> unidadesNuevasConstComp =
                new ArrayList<PUnidadConstruccionComp>();

            if (unidadesCom != null && !unidadesCom.isEmpty()) {

                for (PUnidadConstruccionComp pUnidadConstruccionCompTemp : unidadesCom) {

                    PUnidadConstruccionComp pUnidadConstruccionComp = new PUnidadConstruccionComp();
                    pUnidadConstruccionComp.setId(null);
                    pUnidadConstruccionComp.setPUnidadConstruccion(pUnidadConstruccionCompTemp.
                        getPUnidadConstruccion());
                    pUnidadConstruccionComp.setComponente(pUnidadConstruccionCompTemp.
                        getComponente());
                    pUnidadConstruccionComp.setElementoCalificacion(pUnidadConstruccionCompTemp.
                        getDetalleCalificacion());
                    pUnidadConstruccionComp.setDetalleCalificacion(pUnidadConstruccionCompTemp.
                        getDetalleCalificacion());
                    pUnidadConstruccionComp.setPuntos(pUnidadConstruccionCompTemp.getPuntos());
                    pUnidadConstruccionComp.setFechaLog(new Date(System.currentTimeMillis()));
                    pUnidadConstruccionComp.setUsuarioLog(pUnidadConstruccionCompTemp.
                        getUsuarioLog());
                    pUnidadConstruccionComp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());

                    unidadesNuevasConstComp.add(pUnidadConstruccionComp);
                }
                if (unidadesNuevasConstComp != null && !unidadesNuevasConstComp.isEmpty()) {
                    this.dao.updateMultiple(unidadesNuevasConstComp);
                }

            }

            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

}
