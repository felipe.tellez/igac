package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IFichaMatrizPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.FichaMatrizPredioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase de prueba para FichaMatrizPredio
 *
 * @author juan.agudelo
 *
 */
public class FichaMatrizPredioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FichaMatrizPredioDAOBeanTest.class);

    private IFichaMatrizPredioDAO dao = new FichaMatrizPredioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para conseguir el coeficiente de copropiedad de un predio
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetCoeficienteCopropiedadByNumeroPredial() {
        LOGGER.debug(
            "tests: FichaMatrizPredioDAOBeanTest#testGetCoeficienteCopropiedadByNumeroPredial");

        String numeroPredial = null;

        numeroPredial = "080010103000005510054900000000";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Double answer = this.dao
                .getCoeficienteCopropiedadByNumeroPredial(numeroPredial);

            if (answer != null) {
                LOGGER.debug("Coeficiente de copropiedad: " + answer);
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para conseguir la ficha matriz predio por numero predial
     *
     * @author javier.aponte
     */
    @Test
    public void testObtenerFichaMatrizPredioPorNumeroPredial() {
        LOGGER.debug(
            "tests: FichaMatrizPredioDAOBeanTest#testObtenerFichaMatrizPredioPorNumeroPredial");

        String numeroPredial = "080010112000000730042901010001";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            FichaMatrizPredio answer = this.dao.obtenerFichaMatrizPredioPorNumeroPredial(
                numeroPredial);

            if (answer != null) {
                LOGGER.debug("FIcha Matriz Id: " + answer.getFichaMatriz().getId());
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
