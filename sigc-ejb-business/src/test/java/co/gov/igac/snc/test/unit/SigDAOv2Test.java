package co.gov.igac.snc.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import au.com.bytecode.opencsv.CSVReader;
import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.generales.IProductoCatastralJobDAO;
import co.gov.igac.snc.dao.generales.impl.ProductoCatastralJobDAOBean;
import co.gov.igac.snc.dao.sig.SigDAOv2;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.vo.ImagenPredioVO;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

/**
 * Prueba del cliente Rest utilizado para consumir desde java los geoservicios publicados en ArcGis
 * Server (http://services.arcgisonline.com/ArcGIS/SDK/REST/index.html?overview.html)
 *
 * El cliente java rest utiliza la librería com.sun.jersey.*
 *
 * El cliente Java Rest invoca las siguientes operaciones Envía el Job al servidor:
 * http://services.arcgisonline.com/ArcGIS/SDK/REST/gpsubmit.html Consulta el estado de ejecución
 * del servidor: http://services.arcgisonline.com/ArcGIS/SDK/REST/gpjob.html Obtiene los resultados
 * del job cuando termina la ejecución:
 * http://services.arcgisonline.com/ArcGIS/SDK/REST/gpresult.html
 *
 * @author juan.mendez
 *
 */
public class SigDAOv2Test extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SigDAOv2Test.class);

    private static final String LOGIN_USUARIO = "pruatlantico16";

    private IProductoCatastralJobDAO jobDAO;
    private ITramiteDAO tramiteDao;

    private UsuarioDTO usuario;

    protected List<String> predios;

    /**
     *
     */
    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        try {
            super.setUp(BaseTest.EM_TEST_ORACLEDB);
            usuario = new UsuarioDTO();
            usuario.setLogin(LOGIN_USUARIO);

            jobDAO = new ProductoCatastralJobDAOBean();
            jobDAO.setEntityManager(em);

            tramiteDao = new TramiteDAOBean();
            tramiteDao.setEntityManager(em);

            URL fileUrl = this.getClass().getClassLoader().getResource("predios.csv");
            predios = new ArrayList<String>();
            CSVReader reader = new CSVReader(new FileReader(fileUrl.getPath()));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                predios.add(nextLine[0]);
            }
            reader.close();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     *
     */
    //
    // @Test(threadPoolSize = 4, invocationCount = 500)
    // @Test
    public void testGetToken() {
        LOGGER.debug("***************************************");
        LOGGER.debug("testGetToken");
        try {

            long startTime = System.currentTimeMillis();
            ArcgisServerTokenDTO resultado = SigDAOv2.getInstance().obtenerToken(usuario);
            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
            assertNotNull(resultado);
            LOGGER.debug("resultado:" + resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        LOGGER.debug("***************************************");
    }

    @Test
    //@Test(threadPoolSize = 4, invocationCount = 20)
    public void testObtenerImagenSimplePredio() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerImagenSimplePredio");
        try {
            String codigosPrediales = "080010101000003740022000000000";
            List<ImagenPredioVO> resultados = SigDAOv2.getInstance().obtenerImagenSimplePredio(
                codigosPrediales, usuario);
            assertNotNull(resultados);
            LOGGER.debug("Cantidad  Imagenes Generadas: " + resultados.size());
            assertFalse(resultados.isEmpty());
            for (ImagenPredioVO imagenPredio : resultados) {
                LOGGER.debug(imagenPredio.getNumeroPredial());
                LOGGER.debug(imagenPredio.getNombreArchivo());
                //LOGGER.debug(imagenPredio.getImagenStringBase64());
                assertNotNull(imagenPredio.getImagenStringBase64());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        LOGGER.debug("**************************************************");
    }

    @Test
    // @Test(threadPoolSize = 4, invocationCount = 50)
    public void testObtenerImagenSimplePredioMultiples() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerImagenSimplePredio");
        try {
            String codigosPrediales =
                "080010300000000180008000000000,080010101000001560003000000000";
            List<ImagenPredioVO> resultados = SigDAOv2.getInstance().obtenerImagenSimplePredio(
                codigosPrediales, usuario);
            assertNotNull(resultados);
            assertFalse(resultados.isEmpty());
            LOGGER.debug("Cantidad  Imagenes Generadas: " + resultados.size());
            assertTrue(resultados.size() > 1);
            for (ImagenPredioVO imagenPredio : resultados) {
                LOGGER.debug(imagenPredio.getNumeroPredial());
                LOGGER.debug(imagenPredio.getNombreArchivo());
                //LOGGER.debug(imagenPredio.getImagenStringBase64());
                assertNotNull(imagenPredio.getImagenStringBase64());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        LOGGER.debug("**************************************************");
    }

    @Test
    //@Test(threadPoolSize = 4, invocationCount = 50)
    public void testObtenerImagenAvanzadaPredio() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerImagenAvanzadaPredio");
        try {
            String codigosPrediales = "080010101000000480022000000000";
            List<ImagenPredioVO> resultados = SigDAOv2.getInstance().obtenerImagenAvanzadaPredio(
                codigosPrediales, usuario);
            assertNotNull(resultados);
            LOGGER.debug("Cantidad  Imagenes Generadas: " + resultados.size());
            assertFalse(resultados.isEmpty());
            for (ImagenPredioVO imagenPredio : resultados) {
                LOGGER.debug(imagenPredio.getNumeroPredial());
                LOGGER.debug(imagenPredio.getNombreArchivo());
                //LOGGER.debug(imagenPredio.getImagenStringBase64());
                assertNotNull(imagenPredio.getImagenStringBase64());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        LOGGER.debug("**************************************************");
    }

    //@Test(threadPoolSize = 1, invocationCount = 100)
    @Test
    public void testEnviarJobEliminarVersion() {
        LOGGER.debug("***************************************");
        LOGGER.debug("unit test SigDAOTesy#testEnviarJobEliminarVersion ...");
        em.getTransaction().begin();
        try {
            long startTime = System.currentTimeMillis();
            String codigoMunicipio = "08001";
            String numeroTramite = "118907";
            Tramite tramite = new Tramite();
            tramite.setId(Long.valueOf(numeroTramite));

            LOGGER.debug("codigoMunicipio:" + codigoMunicipio);
            LOGGER.debug("numeroTramite:" + numeroTramite);

            SigDAOv2.getInstance().enviarJobEliminarVersion(jobDAO, codigoMunicipio, tramite,
                usuario);
            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;

            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
    }

    @Test
    public void testObtenerZonasHomogeneas() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerZonasHomogeneas");
        try {
            String nombreZip =
                "/documents/TEST/Gestion_Catastral/Workspace/Tramites/08001/tr/2013/12/23/tr_08001_37066.gdb.zip.enc.zip";

            List<PPredio> predios = new ArrayList<PPredio>();
            PPredio predio = new PPredio();
            predio.setNumeroPredial("080010101000000080015800000000");
            predios.add(predio);

            predio = new PPredio();
            predio.setNumeroPredial("080010101000000080017800000001");
            predios.add(predio);

            predio = new PPredio();
            predio.setNumeroPredial("080010101000000080018800000001");
            predios.add(predio);

            predio = new PPredio();
            predio.setNumeroPredial("080010101000000080019800000001");
            predios.add(predio);

            predio = new PPredio();
            predio.setNumeroPredial("080010101000000080020800000001");
            predios.add(predio);

            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> resultados = SigDAOv2.getInstance().
                obtenerZonasHomogeneas(predios, nombreZip, usuario);
            assertFalse(resultados.size() == 0, "No se obtuvieron resultados");

            for (String key : resultados.keySet()) {
                LOGGER.debug(key);
                List<ZonasFisicasGeoeconomicasVO> zonasVo = resultados.get(key);
                for (ZonasFisicasGeoeconomicasVO zona : zonasVo) {
                    LOGGER.debug("Fisica: " + zona.getCodigoFisica());
                    LOGGER.debug("Economica: " + zona.getCodigoGeoeconomica());
                    LOGGER.debug("Economica: " + zona.getArea());
                    assertTrue(Double.parseDouble(zona.getArea()) > 0,
                        "El valor del área de la zona no es válido");
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        LOGGER.debug("**************************************************");
    }

    @Test
    public void testObtenerZonasHomogeneasFailNumeroPredioNoExiste() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerZonasHomogeneasFailNumeroPredioNoExiste");
        try {
            String nombreZip =
                "/documents/TEST/Gestion_Catastral/Workspace/Tramites/08001/tr/2013/12/23/tr_08001_37066.gdb.zip.enc.zip";

            List<PPredio> predios = new ArrayList<PPredio>();
            PPredio predio = new PPredio();
            predio.setNumeroPredial("080010101000000080015800000999");
            predios.add(predio);

            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> resultados = SigDAOv2.getInstance().
                obtenerZonasHomogeneas(predios, nombreZip, usuario);
            assertTrue(resultados.size() == 0,
                "Se obtuvieron resultados pero la lista debería estar vacía");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        LOGGER.debug("**************************************************");
    }

    @Test
    public void testObtenerZonasHomogeneasFailArchivoNoExiste() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerZonasHomogeneasFailArchivoNoExiste");
        try {
            String nombreZip =
                "/documents/TEST/Gestion_Catastral/Workspace/Tramites/08001/tr/2013/12/24/tr_08001_37066.gdb.zip.enc.zip";

            List<PPredio> predios = new ArrayList<PPredio>();
            PPredio predio = new PPredio();
            predio.setNumeroPredial("080010101000000080019800000001");
            predios.add(predio);

            @SuppressWarnings("unused")
            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> resultados = SigDAOv2.getInstance().
                obtenerZonasHomogeneas(predios, nombreZip, usuario);
            fail("El servicio debería lanzar Excepción");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertNotNull(e);
        }
        LOGGER.debug("**************************************************");
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Test
    public void testObtenerZonasAsync() {
        LOGGER.debug("**************************************************");
        LOGGER.debug("testObtenerZonasAsync");
        em.getTransaction().begin();
        try {
            Tramite unTramite = new Tramite();
            unTramite.setId(23246l);

            String ruta =
                "/documents/TEST/Gestion_Catastral/Workspace/Tramites/08001/2015/07/03/CopiaBDGeograficaFinal/tr_08001_47403.gdb.zip.enc.zip";
            List predios = new LinkedList<PPredio>();
            PPredio p1 = new PPredio();
            PPredio p2 = new PPredio();
            PPredio p3 = new PPredio();
            p1.setNumeroPredial("080010101000000540037000000000");
            p2.setNumeroPredial("080010101000000540015000000000");
            p3.setNumeroPredial("080010101000000540035000000000");
            predios.add(p1);
            predios.add(p2);
            predios.add(p3);
            ProductoCatastralJob pcj = SigDAOv2.getInstance().obtenerZonasAsync(jobDAO, predios,
                ruta, usuario, unTramite, "1");
            LOGGER.debug("Producto Catastral Job ID = " + pcj.getId());
            assertNotNull(pcj);
        } catch (Exception e) {
            em.getTransaction().commit();
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
        LOGGER.debug("**************************************************");
    }

    @Test
    //@Test(threadPoolSize = 11, invocationCount = 100)
    public void testGenerarCCUAsync() {
        LOGGER.debug("testGenerarCCUAsync");

        try {
            //Se requiere para que guarde el job en la base de datos
            em.getTransaction().begin();

            long startTime = System.currentTimeMillis();

            long index = Math.round(Math.random() * predios.size());
            if (index > predios.size() - 1) {
                index = predios.size() - 1;
            }
            String codigoManzana = predios.get(Integer.parseInt("" + index)).substring(0, 17);
            LOGGER.debug("codigoManzana: " + codigoManzana);

            boolean generarEnPDF = true;

            codigoManzana = "08001010200000359";
            ProductoCatastralDetalle productoCatastralDetalle = null;

            SigDAOv2.getInstance().generarCCUAsync(jobDAO, codigoManzana, generarEnPDF, null,
                productoCatastralDetalle, usuario);

            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            em.getTransaction().commit();
        }
    }

    @Test
    //@Test(threadPoolSize = 11, invocationCount = 100)
    public void testGenerarCCUAsyncNoPDF() {
        LOGGER.debug("testGenerarCCUAsyncNoPDF");

        try {
            //Se requiere para que guarde el job en la base de datos
            em.getTransaction().begin();

            long startTime = System.currentTimeMillis();

            long index = Math.round(Math.random() * predios.size());
            if (index > predios.size() - 1) {
                index = predios.size() - 1;
            }
            String codigoManzana = predios.get(Integer.parseInt("" + index)).substring(0, 17);
            LOGGER.debug("codigoManzana: " + codigoManzana);

            boolean generarEnPDF = false;

            codigoManzana = "08001010200000359";
            ProductoCatastralDetalle productoCatastralDetalle = null;

            SigDAOv2.getInstance().generarCCUAsync(jobDAO, codigoManzana, generarEnPDF, null,
                productoCatastralDetalle, usuario);

            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            em.getTransaction().commit();
        }
    }

    @Test
    //@Test(threadPoolSize = 1, invocationCount = 20)
    public void testGenerarFPDAsync() {
        LOGGER.debug("testGenerarFPDAsync");
        try {
            em.getTransaction().begin();
            long startTime = System.currentTimeMillis();
            long index = Math.round(Math.random() * predios.size());
            String predio = predios.get(Integer.parseInt("" + index));
            LOGGER.debug("predio: " + predio);
            ProductoCatastralDetalle productoCatastralDetalle = null;
            SigDAOv2.getInstance().generarFPDAsync(jobDAO, predio, null, productoCatastralDetalle,
                usuario);
            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            em.getTransaction().commit();
        }
    }

    @Test
    //@Test(threadPoolSize = 1, invocationCount = 20)
    public void testGenerarFPDAsync_multiples() {
        LOGGER.debug("testGenerarFPDAsync_multiples");
        try {
            em.getTransaction().begin();
            long startTime = System.currentTimeMillis();

            String[] predios = {
                "080010003000000001762000000000",
                "080010003000000000738000000000",
                "080010003000000000150000000000",
                "080010003000000000190000000000",
                "080010003000000000705000000000"};

            ProductoCatastralDetalle productoCatastralDetalle = null;
            for (String predio : predios) {
                LOGGER.debug("predio: " + predio);
                SigDAOv2.getInstance().generarFPDAsync(jobDAO, predio, null,
                    productoCatastralDetalle, usuario);
            }
            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            em.getTransaction().commit();
        }
    }

    /*
     * //@Test //@Test(threadPoolSize = 11, invocationCount = 200) public void
     * testGenerarCertificadoPlanoPredialCatastral() {
     * LOGGER.debug("testGgenerarCertificadoPlanoPredialCatastral"); try { long startTime =
     * System.currentTimeMillis(); long index = Math.round(Math.random() * predios.size()); String
     * predio = predios.get(Integer.parseInt("" + index)); List<ProductoCatastralResultado>
     * resultado = SigDAO.getInstance(logMensajeDAO).generarCertificadoPlanoPredialCatastral(predio,
     * usuario); assertNotNull(resultado); LOGGER.debug("resultado: "+resultado ); long endTime =
     * System.currentTimeMillis(); long diff = endTime - startTime; double seconds = diff / 1000;
     * double minutes = seconds / 60; LOGGER.debug("That took " + minutes + " minutes (" + seconds +
     * " Seconds ) (" + (diff) + " milliseconds )");
     *
     * } catch (Exception e) { LOGGER.error(e.getMessage(), e); fail(e.getMessage(), e); } }
     */
    @Test
    //@Test(threadPoolSize = 1, invocationCount = 20)
    public void testGenerarCPPAsync() {
        LOGGER.debug("testGenerarCPPAsync");
        try {
            em.getTransaction().begin();
            long startTime = System.currentTimeMillis();
            long index = Math.round(Math.random() * predios.size());
            String predio = predios.get(Integer.parseInt("" + index));
            LOGGER.debug("predio: " + predio);
            ProductoCatastralDetalle productoCatastralDetalle = null;
            SigDAOv2.getInstance().generarCPPAsync(jobDAO, predio, null, productoCatastralDetalle,
                usuario);
            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            em.getTransaction().commit();
        }
    }

    //@Test(threadPoolSize = 3, invocationCount = 10)
    @Test
    public void obtenerColindantesPredio() {
        LOGGER.debug("***************************************");
        LOGGER.debug("obtenerColindantesPredio");
        try {
            List<String> colindantes = SigDAOv2.getInstance().obtenerColindantesPredio(
                "080010101000000100001000000000", usuario, null);
            assertNotNull(colindantes);
            assertFalse(colindantes.isEmpty());
            LOGGER.debug("cantidad colindantes:" + colindantes.size());
            for (Iterator iterator = colindantes.iterator(); iterator.hasNext();) {
                String codigoColindante = (String) iterator.next();
                LOGGER.debug("codigoColindante:" + codigoColindante);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void obtenerColindantesConstruccion() {
        LOGGER.debug("***************************************");
        LOGGER.debug("obtenerColindantesPredio");
        try {
            List<String> colindantes = SigDAOv2.getInstance().obtenerColindantesConstruccion(
                "080010107000005230001500000003", usuario, "null");
            assertNotNull(colindantes);
            assertFalse(colindantes.isEmpty());
            LOGGER.debug("cantidad colindantes:" + colindantes.size());
            for (Iterator iterator = colindantes.iterator(); iterator.hasNext();) {
                String codigoColindante = (String) iterator.next();
                LOGGER.debug("codigoColindante:" + codigoColindante);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * prueba la ejecucion del job de validar inconsistencias asincrona @author andres.eslava
     */
    @Test
    public void testValidarInconsistenciasAsync() {
        LOGGER.debug("testValidarInconsistenciasAsync...INICIA");
        try {
            em.getTransaction().begin();
            Tramite unTramite = new Tramite();
            unTramite.setId(989796l);
            String predios = "080010003000000001789000000000";
            ProductoCatastralJob pcj = SigDAOv2.getInstance().validarInconsistenciasAsync(jobDAO,
                predios, usuario, unTramite, EMutacionClase.QUINTA.getNombre(),
                ETramiteRadicacionEspecial.QUINTA_MASIVO.getCodigo());
            LOGGER.debug("Producto Catastral Job ID = " + pcj.getId());
            assertNotNull(pcj);
        } catch (Exception e) {
            em.getTransaction().commit();
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
        LOGGER.debug("**************************************************");
    }

    /*
     * Prueba la validacion de inconsistencias sicrona @author andres.eslava
     */
    @Test
    public void testValidarInconsistencias() {
        LOGGER.debug("testValidarInconsistenciasAsync...INICIA");
        try {
            Tramite unTramite = new Tramite();
            unTramite.setId(989796l);
            String predios = "080010003000000001789000000000";
            HashMap<String, String> pcj = SigDAOv2.getInstance().validarInconsistencias(predios,
                usuario);
            assertNotNull(pcj);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * Prueba la validacion de inconsistencias sicrona @author andres.eslava
     */
    @Test
    public void testValidarInconsistenciasUnidadesConstruccion() {
        LOGGER.debug("testValidarInconsistenciasAsync...INICIA");
        try {
            Tramite unTramite = new Tramite();
            unTramite.setId(989796l);
            String predios = "080010101000002110041000000000,080010101000002110041000000000";
            HashMap<String, String> resultadoValidacion = SigDAOv2.getInstance().
                validarGeometriaUnidadesConstruccion(predios, usuario);
            assertNotNull(resultadoValidacion);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test(threadPoolSize = 1, invocationCount = 1)
    public void testExportarDatosAsyncFormatShapeCreateNewJob() {
        LOGGER.debug("***************************************");
        LOGGER.debug("testExportarDatosAsyncFormatShapeCreateNewJob");
        //Se requiere para que guarde el job en la base de datos
        em.getTransaction().begin();
        try {
            long startTime = System.currentTimeMillis();
            int max = 100000;
            long number = Math.round(Math.random() * (max));
            String numeroTramite = "" + number;//976;//
            Tramite tramite = new Tramite();
            tramite.setId(number);
            String identificadoresPredios = "080010103000003050012000000000";
            //String identificadoresPredios = "080010103000006070056000000000,080010103000006070901900000000";

            String tipoMutacion = "2";
            LOGGER.debug("numeroTramite:" + numeroTramite);
            LOGGER.debug("identificadoresPredios:" + identificadoresPredios);
            SigDAOv2.getInstance().exportarDatosAsync(jobDAO, tramite, identificadoresPredios,
                tipoMutacion, ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_SHAPE, usuario);

            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
    }

    @Test
    public void testExportarDatosAsyncFormatFGDBCreateNewJob() {
        LOGGER.debug("***************************************");
        LOGGER.debug("testExportarDatosAsyncFormatFGDBCreateNewJob");
        //Se requiere para que guarde el job en la base de datos
        em.getTransaction().begin();
        try {
            long startTime = System.currentTimeMillis();
            //String numeroTramite = "109";
            long number = Math.round(Math.random() * (10000));
            String numeroTramite = "" + number;
            Tramite tramite = new Tramite();
            tramite.setId(number);
            String identificadoresPredios = "080010103000003050012000000000";

            String tipoMutacion = "2";
            LOGGER.debug("numeroTramite:" + numeroTramite);
            LOGGER.debug("identificadoresPredios:" + identificadoresPredios);
            SigDAOv2.getInstance().exportarDatosAsync(jobDAO, tramite,
                identificadoresPredios, tipoMutacion,
                ProductoCatastralJob.FORMATO_DATOS_EXPORTADOS_FILEGEODB, usuario);

            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
    }

    @Test
    public void testExportarDatosAsyncFormatFGDBExistingJob() {
        LOGGER.debug("***************************************");
        LOGGER.debug("testExportarDatosAsyncFormatFGDBCreateNewJob");
        //Se requiere para que guarde el job en la base de datos
        em.getTransaction().begin();
        try {
            long startTime = System.currentTimeMillis();
            ProductoCatastralJob job = new ProductoCatastralJob();
            job.setId(97302L);

            SigDAOv2.getInstance().exportarDatosAsync(jobDAO, job, usuario);

            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
    }

    /**
     * Crea y envía un job para la aplicación de cambios geográficos del trámite
     */
    @Test
    public void testAplicarCambiosAsync() {
        LOGGER.debug("***************************************");
        LOGGER.debug("#testAplicarCambiosAsync ...");
        //Se requiere para que guarde el job en la base de datos
        em.getTransaction().begin();
        try {
            long startTime = System.currentTimeMillis();
            Long idTramite = 660496L;
            Tramite t = tramiteDao.findById(idTramite);
            assertNotNull(t, "No existe el trámite");

            List<TramiteDocumento> td = t.getTramiteDocumentos();
            Long replicaFinalId = ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId();
            String idSistemaDocumental = null;
            for (TramiteDocumento tdTemp : td) {
                if (tdTemp.getDocumento().getTipoDocumento().getId() == replicaFinalId) {
                    idSistemaDocumental = tdTemp.getDocumento().getIdRepositorioDocumentos();
                }
            }

            assertNotNull(idSistemaDocumental, "No existe el documento");
            LOGGER.debug("idSistemaDocumental:" + idSistemaDocumental);
            SigDAOv2.getInstance().aplicarCambiosAsync(jobDAO, t, idSistemaDocumental, usuario);
            long endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            double seconds = diff / 1000;
            double minutes = seconds / 60;
            LOGGER.debug(
                "That took " + minutes + " minutes  (" + seconds + " Seconds ) (" + (diff) +
                " milliseconds )");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
    }

    /**
     *
     */
    @Test
    public void testAplicarCambiosAsyncExistingJob() {
        LOGGER.debug("***************************************");
        LOGGER.debug("testAplicarCambiosAsyncExistingJob");
        // Se requiere para que guarde el job en la base de datos
        em.getTransaction().begin();
        try {
            ProductoCatastralJob pcj = jobDAO.findById(new Long(97025));
            SigDAOv2.getInstance().aplicarCambiosAsync(jobDAO, pcj, usuario);
        } catch (Exception e) {
            em.getTransaction().commit();
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
        em.getTransaction().commit();
    }

}
