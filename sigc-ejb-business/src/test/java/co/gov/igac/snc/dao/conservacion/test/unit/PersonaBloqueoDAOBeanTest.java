package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertFalse;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPersonaBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.impl.PersonaBloqueoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import static org.testng.Assert.assertFalse;

/**
 *
 * @author juan.agudelo
 *
 */
public class PersonaBloqueoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PersonaBloqueoDAOBeanTest.class);

    private IPersonaBloqueoDAO dao = new PersonaBloqueoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaBloqueoByNumeroIdentificacion() {
        LOGGER.debug(
            "unit testing PersonaBloqueoDAOBeanTest#testBuscarPersonaBloqueoByNumeroIdentificacion...");

        String numeroIdentificacion = "764925";
        String tipoIdentificacion = "CC";
        try {
            List<PersonaBloqueo> pb = this.dao
                .buscarPersonaBloqueoByNumeroIdentificacion(numeroIdentificacion, tipoIdentificacion);
            LOGGER.debug("num resultado = " + pb.size());

            for (PersonaBloqueo pbloq : pb) {
                LOGGER.debug("Fecha inicial = " + pbloq.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + pbloq.getMotivoBloqueo());
                LOGGER.debug("Identificación = " + pbloq.getPersona().getNumeroIdentificacion());
                LOGGER.debug("Documento = " + pbloq.getDocumentoSoporteBloqueo().getArchivo());
            }

            Assert.assertTrue(pb != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

//-------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testFindCurrentPersonaBloqueoByPersonaId() {
        LOGGER.debug(
            "unit testing PersonaBloqueoDAOBeanTest#testFindCurrentPersonaBloqueoByPersonaId ...");

        Long idPersona = 4L;

        try {
            PersonaBloqueo pb = this.dao
                .findCurrentPersonaBloqueoByPersonaId(idPersona);

            if (pb != null) {
                LOGGER.debug("Fecha inicial = " + pb.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + pb.getMotivoBloqueo());
                LOGGER.debug("Identificación = " + pb.getPersona().getNumeroIdentificacion());
            }

            Assert.assertTrue(pb != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonasBloqueo() {
        LOGGER.debug("unit testing PersonasBloqueoDAOBeanTest#testBuscarPersonasBloqueo...");

        String tipoIdentificacion = "CC";
        String numeroIdentificacion = "764925";
        FiltroDatosConsultaPersonasBloqueo filtroDatosConsultaPersonasBloqueo =
            new FiltroDatosConsultaPersonasBloqueo(
                tipoIdentificacion, numeroIdentificacion);

        try {
            List<PersonaBloqueo> pb = this.dao
                .buscarPersonasBloqueo(filtroDatosConsultaPersonasBloqueo);
            LOGGER.debug("num resultado = " + pb.size());

            for (PersonaBloqueo pbloq : pb) {
                LOGGER.debug("id " + pbloq.getId());
                LOGGER.debug("Fecha inicial = " + pbloq.getFechaInicioBloqueo());
                LOGGER.debug("Fecha final = " + pbloq.getFechaTerminaBloqueo());
                LOGGER.debug("Motivo = " + pbloq.getMotivoBloqueo());
                LOGGER.debug("Identificación = " +
                    pbloq.getPersona().getNumeroIdentificacion());
                LOGGER.debug("Documento = " +
                    pbloq.getDocumentoSoporteBloqueo().getArchivo());
                if (pbloq.getDocumentoSoporteDesbloqueo() != null) {
                    LOGGER.debug("Documento Desbloqueo = " +
                        pbloq.getDocumentoSoporteDesbloqueo()
                            .getArchivo());
                }
            }

            Assert.assertTrue(pb != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetPersonaBloqueosByPersonaId() {
        LOGGER.debug("unit testing PersonasBloqueoDAOBeanTest#testGetPersonaBloqueosByPersonaId...");

        Long idPersona = 444996L;

        try {
            List<PersonaBloqueo> pb = this.dao
                .getPersonaBloqueosByPersonaId(idPersona);

            LOGGER.debug("num resultado = " + pb.size());

            for (PersonaBloqueo pbloq : pb) {
                LOGGER.debug("id " + pbloq.getId());
                LOGGER.debug("Fecha inicial = " + pbloq.getFechaInicioBloqueo());
                LOGGER.debug("Fecha final = " + pbloq.getFechaTerminaBloqueo());
                LOGGER.debug("Motivo = " + pbloq.getMotivoBloqueo());
                LOGGER.debug("Identificación = " +
                    pbloq.getPersona().getNumeroIdentificacion());

                if (pbloq.getDocumentoSoporteDesbloqueo() != null) {
                    LOGGER.debug("Documento Desbloqueo = " +
                        pbloq.getDocumentoSoporteDesbloqueo()
                            .getArchivo());
                }
            }

            Assert.assertTrue(pb != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

}
