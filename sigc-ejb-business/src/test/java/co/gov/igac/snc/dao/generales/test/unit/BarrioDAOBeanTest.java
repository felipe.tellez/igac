package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.generales.impl.BarrioDAOBean;
import co.gov.igac.snc.dao.generales.impl.ManzanaVeredaDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class BarrioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(BarrioDAOBeanTest.class);

    private BarrioDAOBean dao = new BarrioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author javier.barajas
     *
     */
    @Test
    public void numeroBarriosMunicipioTest() {
        List<Barrio> numeroBarrios = new ArrayList<Barrio>();
        String codigoMunicipio = "08001";
        try {
            LOGGER.debug("Entrando al test de encontrar numero de barrios");
            numeroBarrios = this.dao.numeroBarriosMunicipio(codigoMunicipio);
            LOGGER.debug("numeroBarrios :  " + numeroBarrios.size());
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
            assertTrue(false);
        }
    }

    /**
     * @author javier.barajas
     *
     */
    @Test
    public void findByComunaTest() {
        LOGGER.debug("BarrioDAOBeanTest#findByComunaTest");
        List<Barrio> barriosConsulta = new ArrayList<Barrio>();

        String comunaId = "08001000300";
        try {
            LOGGER.debug("Entrando al test de encontrar numero de barrios");
            barriosConsulta = this.dao.findByComuna(comunaId);
            LOGGER.debug("numeroBarrios por comuna :  " + barriosConsulta.size());
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
            assertTrue(false);
        }
    }

}
