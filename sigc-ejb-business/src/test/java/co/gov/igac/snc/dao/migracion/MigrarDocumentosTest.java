package co.gov.igac.snc.dao.migracion;

import static org.testng.AssertJUnit.fail;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.dao.conservacion.IFotoDAO;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.FotoDAOBean;
import co.gov.igac.snc.dao.conservacion.impl.PredioDAOBean;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.impl.DocumentoDAOBean;
import co.gov.igac.snc.dao.generales.impl.LogMensajeDAOBean;
import co.gov.igac.snc.dao.util.QueryNativoDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.LogMensajeFactory;

/** *
 * Programa encargado de ejecutar la migración de archivos de fichas prediales y fotos a la nueva
 * estructura según lo especificado para SNC
 *
 * @author juan.mendez
 *
 */
public class MigrarDocumentosTest extends BaseTest {

    public static Logger LOGGER = LoggerFactory.getLogger(MigrarDocumentosTest.class);

    private IDocumentoDAO dao = new DocumentoDAOBean();
    private IPredioDAO daoPredio = new PredioDAOBean();
    private IDocumentoDAO daoDocumento = new DocumentoDAOBean();
    private ILogMensajeDAO logMensajeDAO = new LogMensajeDAOBean();
    private IFotoDAO daoFoto = new FotoDAOBean();
    private UsuarioDTO usuario = new UsuarioDTO();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
        this.daoPredio.setEntityManager(this.em);
        this.logMensajeDAO.setEntityManager(this.em);
        this.daoDocumento.setEntityManager(this.em);
        this.daoFoto.setEntityManager(this.em);
        usuario.setLogin("SNC_MIGRACION");
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * Lanza el proceso de migración de fichas
     */
    @Test
    public void testMigrarFichas() {
        LOGGER.debug("testMigrarFachadas");
        try {
            LOGGER.info("***********************************************************");
            LOGGER.info("***********************************************************");
            LOGGER.info("Inicia migración Fichas Escaneadas");

            //172.28.10.26
            //String carpetaBase = "\\\\172.26.0.10\\catastral\\Gestion Documental\\Fichas prediales\\08";
            //172.28.10.22
            //String carpetaBase = "\\\\172.26.0.10\\catastral\\Gestion Documental\\Fichas prediales\\17"
            //Debe ser la carpeta raíz de un departamento
            //String carpetaBase = "Y:\\08\\";
            //LOGGER.info("Buscando subdirectorios para: "+carpetaBase);
            //File directorioEntrada = new File(carpetaBase);
            //String[] files = directorioEntrada.list( DirectoryFileFilter.INSTANCE );
            String[] files = new String[1];
            files[0] = "F:\\snc\\fichas\\41\\020\\";

            LOGGER.info("# Directorios de primer nivel (Municipios) : " + files.length);

            for (int i = 0; i < files.length; i++) {
                LOGGER.info("***********************************************************");
                LOGGER.info("***********************************************************");
                //File subdirectorio = new File(directorioEntrada, files[i]);
                File subdirectorio = new File(files[i]);
                LOGGER.info("Subdirectorio:" + files[i]);
                LOGGER.info("Subdirectorio:" + subdirectorio.getAbsolutePath());

                //TODO: Ignorar los que ya se alcanzaron a cargar
                migrarFichas(subdirectorio);
                /*
                 * String[] directoriosSegundoNivel = subdirectorio.list(
                 * DirectoryFileFilter.INSTANCE ); LOGGER.info("# Directorios de segundo nivel
                 * (Municipios) : "+directoriosSegundoNivel.length); for ( int j = 0; j <
                 * directoriosSegundoNivel.length; j++ ) {
                 * LOGGER.info("***********************************************************"); File
                 * segundoNivel = new File(subdirectorio, directoriosSegundoNivel[j]);
                 * //LOGGER.info("** SegundoNivel: "+segundoNivel.getAbsolutePath()); //TODO:
                 * Ignorar los que ya se alcanzaron a cargar migrarFichas(segundoNivel);
                 * LOGGER.info("***********************************************************"); }
                 */
                //break;
            }
            LOGGER.info("***********************************************************");
            LOGGER.info("***********************************************************");
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }
    }

    /**
     * Este método se encarga de migrar las fichas prediales escaneadas al sistema nacional
     * catastral (SNC) Para algunos archivos no se puede encontrar un predio en la base de datos
     * (Ejm: Cuando el predio ya no existe) En esos casos se guarda la ficha en el sistema
     * documental pero no se le asocia un predio.
     *
     * Este programa lee una carpeta y carga los documentos en el sistema documental. Asocia dichos
     * documentos con los predios de la base de datos.
     *
     */
    @Test
    public void migrarFichas(File subdirectorio) {
        LOGGER.info("migrarFichas: " + subdirectorio.getAbsolutePath());
        try {
            Collection<File> archivos = FileUtils.listFiles(subdirectorio, null, true);
            LOGGER.info("Cantidad Archivos: " + archivos.size());
            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            int cantidadMigrados = 0;
            int totalArchivos = 0;

            Iterator<File> listadoArchivosIter = archivos.iterator();
            this.em.getTransaction().begin();
            while (listadoArchivosIter.hasNext()) {
                //
                //this.em.getTransaction().begin();
                LOGGER.debug("****");
                File archivo = listadoArchivosIter.next();

                String nombreArchivo = FilenameUtils.getBaseName(archivo.getName()).toLowerCase().
                    replace("-", "").trim();
                if ("thumbs".equals(nombreArchivo)) {
                    continue;
                }
                totalArchivos++;
                LOGGER.debug(nombreArchivo);
                if (nombreArchivo.length() < 20) {
                    String observaciones =
                        "Error: El nombre del archivo debe tener minimo 20 caracteres: " +
                        nombreArchivo + "  ,  (  ruta archivo: " + archivo.getAbsolutePath() + " ) ";
                    generateErrorLog(observaciones);
                    continue;
                }

                boolean esFicha = (nombreArchivo.indexOf("f") > -1) ? true : false;//ficha o plano
                String numeroPredialAnterior = nombreArchivo.split("\\D+")[0];
                if (numeroPredialAnterior.length() != 20) {
                    String observaciones =
                        "Error: El numero predial anterior debe tener  20 caracteres: " +
                        numeroPredialAnterior + "  ,  (  ruta archivo: " + archivo.getAbsolutePath() +
                        " ) ";
                    generateErrorLog(observaciones);
                    continue;
                }
                String numeroPredialNuevo = QueryNativoDAO.convertirNumeroPredialDe20A30(this.em,
                    numeroPredialAnterior);

                Predio predio = this.daoPredio.findByNumeroPredial(numeroPredialNuevo);

                //LOGGER.debug(archivo.getAbsolutePath());
                LOGGER.debug(nombreArchivo);
                //LOGGER.debug("numeroPredialAnterior:  "+numeroPredialAnterior);
                LOGGER.debug("esFicha:" + esFicha);
                String idServicioDocumental = null;

                TipoDocumento tipoDocumento = new TipoDocumento();
                Documento documento = new Documento();

                DocumentoTramiteDTO documentoTramiteDTO = null;

                if (esFicha) {
                    documentoTramiteDTO = DocumentoTramiteDTO
                        .crearArgumentoCargarDocumentoPredioFicha(archivo.getAbsolutePath(),
                            numeroPredialNuevo, ETipoDocumento.FICHA_PREDIAL_ESCANEADA.toString());
                    idServicioDocumental = servicioDocumental.cargarDocumentoPredioFicha(
                        documentoTramiteDTO, usuario);
                    tipoDocumento.setId(ETipoDocumento.FICHA_PREDIAL_ESCANEADA.getId());
                } else {
                    //plano anexo ficha
                    documentoTramiteDTO = DocumentoTramiteDTO
                        .crearArgumentoCargarDocumentoPredioFicha(archivo.getAbsolutePath(),
                            numeroPredialNuevo, ETipoDocumento.FICHA_PREDIAL_ESCANEADA_ANEXO_PLANO.
                                toString());
                    idServicioDocumental = servicioDocumental.cargarDocumentoPredioFicha(
                        documentoTramiteDTO, usuario);
                    tipoDocumento.setId(ETipoDocumento.FICHA_PREDIAL_ESCANEADA_ANEXO_PLANO.getId());
                }

                documento.setIdRepositorioDocumentos(idServicioDocumental);

                String observaciones = null;
                if (predio == null) {
                    observaciones = "Error: No se encontró el predio: " + numeroPredialNuevo +
                        "  ,  ( codigo anterior:" + numeroPredialAnterior + " , ruta archivo: " +
                        archivo.getAbsolutePath() + " ) ";
                    generateErrorLog(observaciones);
                } else {
                    documento.setPredioId(predio.getId());
                    observaciones = "Ruta archivo original:" + archivo.getAbsolutePath();
                    cantidadMigrados++;
                }

                documento.setTipoDocumento(tipoDocumento);
                documento.setArchivo(archivo.getName());
                documento.setUsuarioCreador("MIGRACION");
                documento.setUsuarioLog("MIGRACION");
                documento.setBloqueado(ESiNo.NO.getCodigo());
                documento.setFechaLog(new Date(archivo.lastModified()));//fecha última modificación
                documento.setFechaDocumento(new Date(archivo.lastModified()));
                documento.setEstado("MIGRADO");
                documento.setNumeroPredial(numeroPredialNuevo);
                documento.setDescripcion(
                    "Ficha predial escaneada cargada a través del proceso de migración. Número Predial anterior:" +
                    numeroPredialAnterior);
                documento.setObservaciones(observaciones);
                dao.persist(documento);
                //break;
                //this.em.flush();
                //this.em.getTransaction().commit();
            }

            LOGGER.
                info("**************************************************************************");
            LOGGER.info("	Resultados Migración para para: " + subdirectorio.getAbsolutePath());
            LOGGER.info("		Cantidad total de archivos	:	" + totalArchivos);
            LOGGER.info("		Cantidad migrados	:	" + cantidadMigrados);
            LOGGER.info("		Cantidad de archivos que no pudieron migrarse:" + (totalArchivos -
                cantidadMigrados));
            LOGGER.
                info("**************************************************************************");

            if (totalArchivos != cantidadMigrados) {
                String errorMessage = "No se pudieron migrar todos los archivos correctamente. " +
                    "\nCantidad total de archivos:" + totalArchivos +
                    "\nCantidad migrados:" + cantidadMigrados +
                    "\nCantidad sin migrar:" + (totalArchivos - cantidadMigrados);
                generateErrorLog(errorMessage);
            }
            this.em.getTransaction().commit();
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            this.em.getTransaction().rollback();
        } finally {
            //
        }
    }

    /**
     * Este método se encarga de migrar las fotografías de las fachadas al sistema nacional
     * catastral (SNC) Para algunos archivos no se puede encontrar un predio en la base de datos (
     * Ejm: Cuando el predio ya no existe ) En esos casos la foto no se puede cargar en el sistema
     * documental.
     *
     * Este programa lee una carpeta y carga las fotografías en el sistema documental. Asocia dichos
     * documentos con los predios de la base de datos.
     *
     */
    @Test
    public void testMigrarFotografias() {
        LOGGER.debug("testMigrarFachadas");
        try {
            LOGGER.info("***********************************************************");
            LOGGER.info("Inicia migración Fotografías");
            //String carpetaBase = "E:\\Datos_Digitalizacion\\Fotos_Fachadas\\18\\150\\01\\01\\";
            //String carpetaBase = "E:\\Datos_Digitalizacion\\Fotos_Fachadas\\25\\785\\00\\01\\";
            //String carpetaBase = "E:\\Datos_Digitalizacion\\Fotos_Fachadas\\08\\421\\";
            String carpetaBase =
                "F:\\snc\\Migracion_2014\\Fotos\\Fotos_Fachadas\\172_28_10_22\\76\\520\\";

            LOGGER.info("Buscando subdirectorios para: " + carpetaBase);
            File directorioEntrada = new File(carpetaBase);

            String[] files = directorioEntrada.list(DirectoryFileFilter.INSTANCE);

            LOGGER.info("# Directorios de primer nivel (Departamentos) : " + files.length);

            for (int i = 0; i < files.length; i++) {
                LOGGER.info("***********************************************************");
                File subdirectorio = new File(directorioEntrada, files[i]);
                LOGGER.info("Subdirectorio:" + subdirectorio.getAbsolutePath());
                String[] directoriosSegundoNivel = subdirectorio.list(DirectoryFileFilter.INSTANCE);
                LOGGER.info("# Directorios de segundo nivel (Municipios) : " +
                    directoriosSegundoNivel.length);
                for (int j = 0; j < directoriosSegundoNivel.length; j++) {
                    LOGGER.info("***********************************************************");
                    File segundoNivel = new File(subdirectorio, directoriosSegundoNivel[j]);
                    LOGGER.info("** SegundoNivel: " + segundoNivel.getAbsolutePath());
                    migrarFotografias(segundoNivel);
                    LOGGER.info("***********************************************************");
                }
                //break;
            }
            LOGGER.info("***********************************************************");
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }
    }

    /**
     *
     * @param listadoArchivosIter
     */
    private void migrarFotografias(File subdirectorio) {
        LOGGER.info("***********************************************************");
        LOGGER.info("MigrarFotografias para: " + subdirectorio.getAbsolutePath());
        try {
            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            int cantidadMigrados = 0;
            int totalArchivos = 0;

            Collection<File> archivos = FileUtils.listFiles(subdirectorio, null, true);

            Iterator<File> listadoArchivosIter = archivos.iterator();

            this.em.getTransaction().begin();
            while (listadoArchivosIter.hasNext()) {
                LOGGER.debug("****");
                File archivo = listadoArchivosIter.next();
                //LOGGER.debug(archivo.getAbsolutePath());

                //elimina caracteres no alfanumericos en el nombre del archivo
                String nombreArchivo = FilenameUtils.getBaseName(archivo.getName()).toLowerCase().
                    replaceAll("[^a-zA-Z0-9]", "").trim();
                if ("thumbs".equals(nombreArchivo)) {
                    continue;
                }
                totalArchivos++;

                if (nombreArchivo.length() < 20) {
                    LOGGER.error("El archivo se encuentra mal codificado (nombreArchivo): " +
                        archivo.getAbsolutePath());
                    continue;
                }

                String numeroPredialAnterior = nombreArchivo.substring(0, 20);

                //el nombre del archivo debería tener 20 caracteres numericos (codigo del predio anterior)
                //si el archivo no está nombrado correctamente, no se puede migrar
                String digitosEnNombre = numeroPredialAnterior.split("\\D+")[0];
                if (digitosEnNombre.length() < 20) {
                    LOGGER.error("El archivo se encuentra mal codificado: " + archivo.
                        getAbsolutePath());
                    continue;
                }

                String numeroFoto = "";
                if (nombreArchivo.length() > 20) {
                    numeroFoto = nombreArchivo.substring(20);
                }
                String codigoFoto = numeroFoto;

                if (numeroFoto.length() == 4) {
                    numeroFoto = numeroFoto.substring(1);
                } else if (numeroFoto.length() == 5) {
                    numeroFoto = numeroFoto.substring(2);
                } else if (numeroFoto.length() == 6) {
                    numeroFoto = numeroFoto.substring(3);
                }

                String tipoArchivo = numeroFoto.replaceAll("\\d", "");
                LOGGER.debug("nombreArchivo: " + nombreArchivo);
                LOGGER.debug("numeroPredialAnterior: " + numeroPredialAnterior);
                LOGGER.debug("codigoFoto: " + codigoFoto);
                LOGGER.debug("numeroFoto: " + numeroFoto);
                LOGGER.debug("tipoArchivo: " + tipoArchivo);
                LOGGER.debug("archivo: " + archivo.getAbsolutePath());

                String tipoFotografia = null;
                String descripcionFotografia = null;
                if (/* tipoArchivo == null || */"".equals(tipoArchivo) ||
                    tipoArchivo.equals("fac")) {
                    tipoFotografia = EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo();
                    descripcionFotografia = "Fotografía de Fachada";
                } else if (tipoArchivo.equals("ban") || tipoArchivo.equals("b")) {
                    tipoFotografia = EFotoTipo.BANIO.getCodigo();
                    descripcionFotografia = "Fotografía de Baño";
                } else if (tipoArchivo.equals("coc") || tipoArchivo.equals("c")) {
                    tipoFotografia = EFotoTipo.COCINA.getCodigo();
                    descripcionFotografia = "Fotografía de Cocina";
                } else if (tipoArchivo.equals("nom") || tipoArchivo.equals("nm")) {
                    tipoFotografia = EFotoTipo.ACABADOS_PRINCIPALES_NOMENCLATURA.getCodigo();
                    descripcionFotografia = "Fotografía de Nomenclatura";
                } else if (tipoArchivo.equals("est")) {
                    tipoFotografia = EFotoTipo.ESTRUCTURA.getCodigo();
                    descripcionFotografia = "Fotografía de Estructura";
                } else if (tipoArchivo.equals("aca")) {
                    tipoFotografia = EFotoTipo.ACABADOS_PRINCIPALES.getCodigo();
                    descripcionFotografia = "Fotografía de Acabados";
                } else if (tipoArchivo.equals("com")) {
                    tipoFotografia = EFotoTipo.COMPLEMENTO_INDUSTRIA.getCodigo();
                    descripcionFotografia = "Fotografía de Complemento Industrial";
                } else if (tipoArchivo.equals("a")) {
                    tipoFotografia = EFotoTipo.ANEXO.getCodigo();
                    descripcionFotografia = "Fotografía de Anexo";
                } else {
                    LOGGER.error("No se encontró el tipo de Archivo: " + tipoArchivo);
                    LOGGER.error("nombreArchivo: " + nombreArchivo);
                    LOGGER.error("numeroPredialAnterior: " + numeroPredialAnterior);
                    LOGGER.error("codigoFoto: " + codigoFoto);
                    LOGGER.error("numeroFoto: " + numeroFoto);
                    LOGGER.error("tipoArchivo: " + tipoArchivo);
                    LOGGER.error("archivo: " + archivo.getAbsolutePath());
                    continue;
                }

                if (!"".equals(codigoFoto)) {
                    descripcionFotografia += " ( Codigo Foto:" + codigoFoto + " )";
                }

                //LOGGER.debug("tipoFotografia: " +tipoFotografia);
                String numeroPredialNuevo = QueryNativoDAO.convertirNumeroPredialDe20A30(this.em,
                    numeroPredialAnterior);
                //LOGGER.debug("numeroPredialNuevo: " +numeroPredialNuevo);

                //if(true)
                //	break;
                Predio predio = this.daoPredio.findByNumeroPredial(numeroPredialNuevo);

                DocumentoTramiteDTO documentoTramiteDTO = new DocumentoTramiteDTO();
                documentoTramiteDTO.setRutaArchivo(archivo.getAbsolutePath());
                documentoTramiteDTO.setTipoDocumento(ETipoDocumento.FOTOGRAFIA.toString());
                documentoTramiteDTO.setFechaCreacionTramite(new Date());

                String idServicioDocumental = null;
                String observaciones = null;
                Documento documento = new Documento();
                if (predio == null) {
                    //No se pueden subir al repositorio las fotos para las cuales no se encuentre predio asociado
                    //Se genera log de error en la base de datos
                    observaciones = "Error: Predio no Encontrado: " + numeroPredialNuevo +
                        "  ,  ( codigo anterior:" + numeroPredialAnterior + " , ruta archivo: " +
                        archivo.getAbsolutePath() + " ) ";
                    generateErrorLog(observaciones);
                    continue;
                } else {
                    observaciones = "Nombre Archivo Original:" + archivo.getName() +
                        " , Código anterior:" + numeroPredialAnterior + ", codigoFoto:" + codigoFoto +
                        " , Ruta archivo original:" + archivo.getAbsolutePath();
                    documento.setPredioId(predio.getId());
                    documentoTramiteDTO.setNumeroPredial(numeroPredialNuevo);
                    idServicioDocumental = servicioDocumental.cargarDocumentoPredioFoto(
                        documentoTramiteDTO, usuario);
                    cantidadMigrados++;
                }

                TipoDocumento tipoDocumento = new TipoDocumento();
                tipoDocumento.setId(ETipoDocumento.FOTOGRAFIA.getId());

                documento.setIdRepositorioDocumentos(idServicioDocumental);
                //LOGGER.info("idServicioDocumental:" + documento.getIdRepositorioDocumentos());

                documento.setTipoDocumento(tipoDocumento);
                documento.setArchivo(archivo.getName());
                documento.setUsuarioCreador("MIGRACION");
                documento.setUsuarioLog("MIGRACION");
                documento.setBloqueado(ESiNo.NO.getCodigo());
                documento.setFechaLog(new Date());
                documento.setFechaDocumento(new Date(archivo.lastModified()));
                documento.setEstado("MIGRADO");
                documento.setNumeroPredial(numeroPredialNuevo);
                documento.setDescripcion(descripcionFotografia +
                    " . Foto cargada a través del proceso de migración. Número Predial anterior: " +
                    numeroPredialAnterior);
                documento.setObservaciones(observaciones);
                dao.persist(documento);

                Foto foto = new Foto();
                if (predio != null) {
                    foto.setPredio(predio);
                }
                foto.setDescripcion(descripcionFotografia);
                foto.setTipo(tipoFotografia);
                foto.setDocumento(documento);
                foto.setFecha(new Date(archivo.lastModified()));
                foto.setFechaLog(new Date());
                foto.setUsuarioLog("MIGRACION");
                this.daoFoto.persist(foto);
                //break;
            }
            LOGGER.info("****************************************************");
            LOGGER.info("	Resultados Migración para para: " + subdirectorio.getAbsolutePath());
            LOGGER.info("		Directorio:" + subdirectorio.getAbsolutePath());
            LOGGER.info("		Cantidad total de archivos:" + totalArchivos);
            LOGGER.info("		Cantidad migrados:" + cantidadMigrados);
            LOGGER.info("		Cantidad de archivos que no se pudieron migrar:" + (totalArchivos -
                cantidadMigrados));
            LOGGER.info("****************************************************");
            this.em.getTransaction().commit();
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            this.em.getTransaction().rollback();
        } finally {
            //
        }
    }

    /**
     *
     * @param usuario
     * @param message
     */
    private void generateErrorLog(String message) {
        LOGGER.error(
            "**********************************************************************************");
        LOGGER.error(message);
        LogMensaje log = LogMensajeFactory.getLogMensaje(usuario, "MIGRACION_DOCUMENTOS");
        log.setNivel("ERROR");
        log.setMensaje(message);
        logMensajeDAO.persist(log);
        LOGGER.error(
            "**********************************************************************************");
    }

}
