package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.ITipoDocumentoDAO;
import co.gov.igac.snc.dao.generales.impl.TipoDocumentoDAOBean;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.Constantes;

public class TipoDocumentoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TipoDocumentoDAOBeanTest.class);

    private ITipoDocumentoDAO dao = new TipoDocumentoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author fredy.wilches
     */
    @Test
    public void testGetTiposDocumentoByTipoTramite() {
        LOGGER.debug("on GeneralesServiceFacadeTest#getTiposDocumentoByTipoTramite");
        List<TipoDocumento> tiposDocumento;
        try {
            tiposDocumento = this.dao.findByTipoTramite("1", "1", null);

            assertFalse(tiposDocumento.size() == 0);
            tiposDocumento = this.dao.findByTipoTramite("1", "2", "DESENGLOBE");
            assertFalse(tiposDocumento.size() == 0);
            tiposDocumento = this.dao.findByTipoTramite("1", "2", "");
            assertFalse(tiposDocumento.size() > 0);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }

    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetTipoDocumentoPorClase() {

        LOGGER.debug("on GeneralesServiceFacadeTest#testGetTipoDocumentoPorClase");
        String clase = Constantes.OTRO;

        try {
            TipoDocumento d = this.dao.getTipoDocumentoPorClase(clase);
            LOGGER.debug("Tipo documento: " + d.getNombre());
            assertNotNull(d);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }

    }

    /**
     * @author david.cifuentes
     */
    @Test
    public void testFindTipoDeDocumentosAll() {

        LOGGER.debug("Entra a TipoDocumentoDAOBeanTest#testFindTipoDeDocumentosAll");
        String clase = ETipoDocumentoClase.OTRO.name();
        String generado = ESiNo.NO.getCodigo();
        List<TipoDocumento> d = new ArrayList<TipoDocumento>();

        try {
            d = this.dao.findTipoDeDocumentosAll(clase, generado);
            assertNotNull(d);
            LOGGER.debug("Cantidad de registros: " + d.size());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de la busqueda de todos los tipos de documento no generados por el sistema
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetAllTipoDocumento() {
        LOGGER.debug("unit test: TipoDocumentoDAOBeanTest#testGetAllTipoDocumento");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<TipoDocumento> tiposDocumento = this.dao.getAllTipoDocumento();

            if (tiposDocumento != null && !tiposDocumento.isEmpty()) {
                for (TipoDocumento td : tiposDocumento) {
                    LOGGER.debug("Documento: " + td.getNombre());
                }
            }
            assertNotNull(tiposDocumento);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prueba busqueda tipos de documentos avaluos adicionales
     *
     * @author felipe.cadena
     */
    @Test
    public void testObtenerTipoDocumentoAvaluoAdicional() {
        LOGGER.debug("unit test: TipoDocumentoDAOBeanTest#testObtenerTipoDocumentoAvaluoAdicional");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<TipoDocumento> tiposDocumento = this.dao.obtenerTipoDocumentoAvaluoAdicional(
                ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo());

            if (tiposDocumento != null && !tiposDocumento.isEmpty()) {
                for (TipoDocumento td : tiposDocumento) {
                    LOGGER.debug("Documento: " + td.getNombre());
                }
            }
            assertNotNull(tiposDocumento);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prueba busqueda tipos de documentos para el servidor de productos
     *
     * @author javier.barajas
     */
    @Test
    public void testObtenerTipoDocumentoSoporteProductos() {
        LOGGER.debug("unit test: TipoDocumentoDAOBeanTest#testObtenerTipoDocumentoSoporteProductos");

        try {

            List<TipoDocumento> tiposDocumento = this.dao.obtenerTipoDocumentoSoporteProductos();

            if (tiposDocumento != null && !tiposDocumento.isEmpty()) {
                for (TipoDocumento td : tiposDocumento) {
                    LOGGER.debug("Documento: " + td.getNombre());
                }
            }
            assertNotNull(tiposDocumento);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
