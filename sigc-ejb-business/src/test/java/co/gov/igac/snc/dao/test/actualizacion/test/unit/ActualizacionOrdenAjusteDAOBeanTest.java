package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.fail;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityTransaction;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionOrdenAjusteDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionOrdenAjusteDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionOrdenAjuste;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable;
import co.gov.igac.snc.persistence.entity.actualizacion.AjusteManzanaArea;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.test.unit.BaseTest;

public class ActualizacionOrdenAjusteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ActualizacionOrdenAjusteDAOBeanTest.class);
    private IActualizacionOrdenAjusteDAO dao = new ActualizacionOrdenAjusteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Valida la persistencia de la orden de ajuste
     *
     * @author andres.eslava
     */
    @Test
    public void testGuardarActualizacionOrdenAjuste() {
        LOGGER.debug("ActualizacionDAOBeanTes#testGuardarActualizacionOrdenAjuste...INICIA");

        try {
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("andres.eslava");
            usuario.setPrimerNombre("Andres");

            ActualizacionResponsable responsable = new ActualizacionResponsable();
            responsable.setId(20L);

            RecursoHumano unRecursoHumano = new RecursoHumano();
            unRecursoHumano.setId(43L);
            unRecursoHumano.setActualizacionResponsable(responsable);;
            unRecursoHumano.setFechaLog(new Date(System.currentTimeMillis()));
            unRecursoHumano.setCargo("UnCargo");
            unRecursoHumano.setDocumentoIdentificacion("12345678");
            unRecursoHumano.setNombre("Recurso 1000");
            unRecursoHumano.setTipo("CONTRATISTA");
            unRecursoHumano.setUsuarioLog("andres.eslava");

            ActualizacionOrdenAjuste ordenAjuste = new ActualizacionOrdenAjuste();

            AjusteManzanaArea ajusteManzana = new AjusteManzanaArea();
            ajusteManzana.setFechaLog(new Date(System.currentTimeMillis()));
            ajusteManzana.setUsuarioLog(usuario.getLogin());
            ajusteManzana.setIdentificador("08001010800000357");
            ajusteManzana.setActualizacionOrdenAjuste(ordenAjuste);
            ajusteManzana.setLevantamiento("NO");

            List<AjusteManzanaArea> listaAjustes = new LinkedList<AjusteManzanaArea>();
            listaAjustes.add(ajusteManzana);

            ordenAjuste.setActualizacionId(579L);
            ordenAjuste.setAjusteManzanaAreas(listaAjustes);
            ordenAjuste.setDigitalizarNuevasAreas("SI");
            ordenAjuste.setIncorporarNuevasManzanas("SI");
            ordenAjuste.setObservaciones("ObservacionTest");
            ordenAjuste.setRecursoHumano(unRecursoHumano);
            ordenAjuste.setFechaLog(new Date(System.currentTimeMillis()));
            ordenAjuste.setUsuarioLog("andres.eslava");

            EntityTransaction transaccion = this.em.getTransaction();
            transaccion.begin();
            ActualizacionOrdenAjuste unaOrdenAjuste = dao.update(ordenAjuste);
            transaccion.commit();

            Assert.assertNotNull(unaOrdenAjuste);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage(), e);
        } finally {
            LOGGER.debug("ActualizacionDAOBeanTes#testGuardarActualizacionOrdenAjuste...FINALIZA");
        }

    }
}
