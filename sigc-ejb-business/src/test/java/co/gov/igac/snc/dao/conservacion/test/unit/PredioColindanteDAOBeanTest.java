package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.*;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPredioColindanteDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioColindanteDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PredioColindante;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.Constantes;

/**
 *
 * @author juan.mendez
 *
 */
public class PredioColindanteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioColindanteDAOBeanTest.class);

    private IPredioColindanteDAO dao = new PredioColindanteDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.mendez
     */
    @Test
    public void testGetPredioColindanteByNumeroPredial() {
        LOGGER.debug(
            "unit testing PredioColindanteDAOBeanTest#testGetPredioColindanteByNumeroPredial...");

        String numeroPredial = "257400100000000070033000000000";
        try {
            List<PredioColindante> colindantes = this.dao.getPredioColindanteByNumeroPredial(
                numeroPredial);
            assertNotNull(colindantes);
            assertTrue(colindantes.size() > 0);
            for (PredioColindante predioColindante : colindantes) {
                assertEquals(predioColindante.getNumeroPredial(), numeroPredial);
                LOGGER.debug(predioColindante.getId() +
                    " - " + predioColindante.getNumeroPredialColindante() +
                    " - " + predioColindante.getTipo() +
                    " - " + predioColindante.getCardinalidad());
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testGetPredioColindanteByNumeroPredialYTipo() {
        LOGGER.debug(
            "unit testing PredioColindanteDAOBeanTest#testGetPredioColindanteByNumeroPredialYTipo...");

        try {
            String numeroPredial = "257400100000000070033000000000";
            String tipo = Constantes.TIPO_PREDIO_COLINDANTE_PREDIO;

            List<PredioColindante> colindantes = this.dao.getPredioColindanteByNumeroPredialYTipo(
                numeroPredial, tipo);
            assertNotNull(colindantes);
            assertTrue(colindantes.size() > 0);
            for (PredioColindante predioColindante : colindantes) {
                assertEquals(predioColindante.getNumeroPredial(), numeroPredial);
                assertEquals(predioColindante.getTipo(), tipo);
                LOGGER.debug(predioColindante.getId() +
                    " - " + predioColindante.getNumeroPredialColindante() +
                    " - " + predioColindante.getTipo() +
                    " - " + predioColindante.getCardinalidad());
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
