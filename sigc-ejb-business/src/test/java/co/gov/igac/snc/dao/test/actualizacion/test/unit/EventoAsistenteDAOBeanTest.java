package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IEventoAsistenteDAO;
import co.gov.igac.snc.dao.actualizacion.impl.EventoAsistenteDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.test.unit.BaseTest;

public class EventoAsistenteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventoAsistenteDAOBeanTest.class);

    private IEventoAsistenteDAO dao = new EventoAsistenteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testObtenerAsistenteEventoporActualizacionEvento() {
        Long idActualizacionEvento = 1L;

        List<EventoAsistente> eventoAsistente = this.dao.
            obtenerAsistenteEventoporActualizacionEvento(idActualizacionEvento);

        for (EventoAsistente ea : eventoAsistente) {
            LOGGER.debug("Evento Asistente => " + ea.getNombre());
        }
    }

}
