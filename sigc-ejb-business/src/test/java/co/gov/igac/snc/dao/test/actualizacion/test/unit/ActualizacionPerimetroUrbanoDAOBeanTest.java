package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import javax.persistence.EntityTransaction;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionPerimetroUrbanoDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionPerimetroUrbanoDAOBean;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.impl.DocumentoDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionPerimetroUrbano;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Prueba del DAO Actualizacion Perimetro Urbano
 *
 * @author andres.eslava
 *
 */
public class ActualizacionPerimetroUrbanoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ActualizacionPerimetroUrbanoDAOBeanTest.class);

    private IActualizacionPerimetroUrbanoDAO daoActualizacionPerimetroUrbano =
        new ActualizacionPerimetroUrbanoDAOBean();
    private IDocumentoDAO daoDocumento = new DocumentoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        daoActualizacionPerimetroUrbano.setEntityManager(this.em);
        daoDocumento.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testUpdateActualizacionPerimetroUrbano() {
        LOGGER.debug(
            "ActualizacionPerimetroUrbanoDAOBeanTest#testUpdateActualizacionPerimetroUrbano... INICIA");
        try {

            EntityTransaction emt = this.em.getTransaction();

            emt.begin();

            ActualizacionPerimetroUrbano actualizacionPerimetroUrbano =
                new ActualizacionPerimetroUrbano();

            // Establece documento existente en la DB
            Documento documentoOficio = new Documento();
            Documento documentoProyecto = new Documento();
            documentoOficio.setId(174L);
            documentoProyecto.setId(172L);

            // Establece actualizacion existente
            actualizacionPerimetroUrbano.setActualizacionId(507L);

            actualizacionPerimetroUrbano.setNombreAlcalde("AlcaldeTest");

            actualizacionPerimetroUrbano.setAcuerdoDocumento(new Documento());
            actualizacionPerimetroUrbano.setOficioDocumento(new Documento());
            actualizacionPerimetroUrbano
                .setUsuarioLog("usuario Oficial Actualizacion");

            ActualizacionPerimetroUrbano resActualizacionPerimetroUrbano =
                daoActualizacionPerimetroUrbano
                    .update(actualizacionPerimetroUrbano);
            Assert.assertNotNull(resActualizacionPerimetroUrbano);
            System.out.println("Act. Perimetro Urbano:: Nombre Alcalde" +
                resActualizacionPerimetroUrbano.getNombreAlcalde());

            emt.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            LOGGER.debug("ActualizacionPerimetroUrbanoDAOBeanTest#testUpdate... FINALIZA");
        }

    }

    @Test
    public void testPrueba() {
        LOGGER.debug("ActualizacionPerimetroUrbanoDAOBeanTest#testPrueba... INICIA");
        System.out.println("El test se realizo");

    }
}
