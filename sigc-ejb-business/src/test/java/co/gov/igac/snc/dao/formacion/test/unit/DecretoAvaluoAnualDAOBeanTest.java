package co.gov.igac.snc.dao.formacion.test.unit;

import static org.testng.Assert.assertFalse;
import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.formacion.IDecretoAvaluoAnualDAO;
import co.gov.igac.snc.dao.formacion.impl.DecretoAvaluoAnualDAOBean;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase test para los métodos de persistencia de {@link DecretoAvaluoAnual}
 *
 * @author david.cifuentes
 *
 */
public class DecretoAvaluoAnualDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DecretoAvaluoAnualDAOBeanTest.class);

    private IDecretoAvaluoAnualDAO dao = new DecretoAvaluoAnualDAOBean();

    @SuppressWarnings("static-access")
    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("unused")
    /**
     * Test del método buscarDecretoActual
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarDecretoActual() {

        LOGGER.debug("unit test: DecretoAvaluoAnualDAOBeanTest#testBuscarDecretoActual ...");

        DecretoAvaluoAnual decretoAvaluoAnual = new DecretoAvaluoAnual();

        try {
            DecretoAvaluoAnual answer = (DecretoAvaluoAnual) dao
                .buscarDecretoActual();
            Assert.assertNotNull(answer);
            Assert.assertTrue(answer.getVigencia() != null);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

}
