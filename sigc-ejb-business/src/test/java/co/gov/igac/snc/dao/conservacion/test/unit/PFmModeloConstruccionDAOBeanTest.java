package co.gov.igac.snc.dao.conservacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPFmModeloConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.impl.PFmModeloConstruccionDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class PFmModeloConstruccionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        PFmModeloConstruccionDAOBeanTest.class);

    private IPFmModeloConstruccionDAO dao = new PFmModeloConstruccionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // ---------------------------------------//
    /**
     * Test para buscar un PFmModeloConstruccion por su id, trayendo las PFmConstruccionComponente
     * asociadas.
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarUnidadDelModeloDeConstruccionPorSuId() {

        LOGGER.debug("PFmModeloConstruccionDAOBeanTest#buscarUnidadDelModeloDeConstruccionPorSuId");

        PFmModeloConstruccion pFmModeloConstruccion;
        Long idPFmModeloConstruccion = 23L;
        try {
            pFmModeloConstruccion = this.dao
                .buscarUnidadDelModeloDeConstruccionPorSuId(idPFmModeloConstruccion);
            if (pFmModeloConstruccion != null &&
                pFmModeloConstruccion.getPFmConstruccionComponentes() != null) {

                LOGGER.debug("Se cargo satisfactoriamente el PFmModeloConstruccion, éste tiene " +
                    pFmModeloConstruccion.getPFmConstruccionComponentes()
                        .size() + " componentes.");
            }
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }
}
