/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.generales.test.unit;

import co.gov.igac.snc.dao.generales.IComponenteDAO;
import co.gov.igac.snc.dao.generales.impl.ComponenteDAOBean;
import co.gov.igac.snc.persistence.entity.generales.Componente;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class ComponenteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodigoHomologadoDAOBeanTest.class);

    private IComponenteDAO dao = new ComponenteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void getCodigoHomologadoTest() {
        try {
            LOGGER.debug("getCodigoHomologadoTest");
            Componente comp = dao.obtenerComponentePorNombre(
                ERepReporteCategoria.REPORTES_ESTADISTICOS.getNombreComponente());
            Assert.assertNotNull(comp);
            LOGGER.debug("valor:" + comp.getUrl());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }
}
