package co.gov.igac.snc.dao.test.unit;

import static org.testng.Assert.fail;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.GenericDAOBean;
import co.gov.igac.snc.dao.IGenericDAO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author fabio.navarrete
 *
 */
public class GenericDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GenericDAOBeanTest.class);

    private IGenericDAO dao = new GenericDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testFindAll() {
        LOGGER.debug("GenericDAOBeanTest#testFindAll");
        try {
            int numResults = 5;
            @SuppressWarnings("unchecked")
            List<Predio> predios = this.dao
                .findAll(Predio.class, 0, numResults);
            Assert.assertEquals(predios.size(), numResults);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testFindById() {
        LOGGER.debug("GenericDAOBeanTest#testFindById");
        try {
            int numResults = 1;
            @SuppressWarnings("unchecked")
            List<Solicitante> solicitantes = this.dao
                .findAll(Solicitante.class, 0, numResults);
            Solicitante solicitante = (Solicitante) this.dao.findById(Solicitante.class,
                solicitantes
                    .get(0).getId());
            Assert.assertNotNull(solicitante);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testUpdate() {
        LOGGER.debug("GenericDAOBeanTest#testUpdate");
        try {
            this.dao.getEntityManager().getTransaction().begin();
            int numResults = 1;
            @SuppressWarnings("unchecked")
            List<Solicitante> solicitantes = this.dao
                .findAll(Solicitante.class, 0, numResults);
            Solicitante solicitante = solicitantes.get(0);
            Date fechaHoy = new Date();
            String usuario = "fabio.navarrete" + fechaHoy.toString();
            if (usuario.length() > 100) {
                usuario = usuario.substring(0, 100);
            }
            solicitante.setUsuarioLog(usuario);
            solicitante = (Solicitante) this.dao.update(solicitante);
            this.dao.getEntityManager().getTransaction().commit();
            Assert.assertEquals(solicitante.getUsuarioLog(), usuario);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testDelete() {
        LOGGER.debug("GenericDAOBeanTest#testDelete");
        try {
            this.dao.getEntityManager().getTransaction().begin();
            int numResults = 1;
            @SuppressWarnings("unchecked")
            List<Solicitante> solicitantes = this.dao
                .findAll(Solicitante.class, 0, numResults);
            Solicitante solicitante = solicitantes.get(0);
            solicitante.setId(null);
            solicitante = (Solicitante) this.dao.update(solicitante);
            this.dao.delete(solicitante);
            this.dao.getEntityManager().getTransaction().commit();
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
}
