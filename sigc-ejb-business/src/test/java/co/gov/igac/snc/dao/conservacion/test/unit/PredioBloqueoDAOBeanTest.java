package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertFalse;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPredioBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IPrediosBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioBloqueoDAOBean;
import co.gov.igac.snc.dao.conservacion.impl.PrediosBloqueoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;

/**
 *
 * @author juan.agudelo
 *
 */
public class PredioBloqueoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioBloqueoDAOBeanTest.class);

    private IPrediosBloqueoDAO dao = new PrediosBloqueoDAOBean();
    private IPredioBloqueoDAO daoDos = new PredioBloqueoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(em);
        daoDos.setEntityManager(em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testContarPrediosBloqueoByNumeroPredial() {
        LOGGER.debug(
            "unit testing PredioBloqueoDAOBeanTest#testContarPrediosBloqueoByNumeroPredial...");

        String numeroPredial = "257540216041711057933105117118";
        try {
            Integer conteo = this.dao
                .contarPrediosBloqueoByNumeroPredial(numeroPredial);
            LOGGER.debug("num resultado = " + conteo);

            Assert.assertTrue(conteo != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosBloqueoByNumeroPredial() {
        LOGGER.debug(
            "unit testing PredioBloqueoDAOBeanTest#testBuscarPrediosBloqueoByNumeroPredial...");

        String numeroPredial = "257540216041711057933105117115";
        try {
            List<PredioBloqueo> conteo = this.dao
                .buscarPrediosBloqueoByNumeroPredial(numeroPredial);
            LOGGER.debug("num resultado = " + conteo.size());

            for (PredioBloqueo pb : conteo) {
                LOGGER.debug("Fecha inicial = " + pb.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + pb.getMotivoBloqueo());
                LOGGER.debug("Estado predio = " + pb.getPredio().getEstado());
                LOGGER.debug("Documento = " +
                    pb.getDocumentoSoporteBloqueo().getArchivo());
            }

            Assert.assertTrue(conteo != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPredioBloqueoByNumeroPredial() {
        LOGGER.debug(
            "unit testing PredioBloqueoDAOBeanTest#testBuscarPredioBloqueoByNumeroPredial...");

        String numeroPredial = "257540216041711057933105117115";
        try {
            PredioBloqueo conteo = this.daoDos
                .findCurrentPredioBloqueo(numeroPredial);
            LOGGER.debug("num resultado = " + conteo);

            if (conteo != null) {
                LOGGER.debug("Fecha inicial = " +
                    conteo.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + conteo.getMotivoBloqueo());
            }

            Assert.assertTrue(conteo != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * test del método PredioBloqueoDAOBean#findCurrentPredioBloqueo
     *
     * @author pedro.garcia
     */
    @Test
    public void testFindCurrentPredioBloqueo() {

        LOGGER.debug("unit test PredioBloqueoDAOBeanTest#testFindCurrentPredioBloqueo ...");

        String numeroPredial = "257540102000011540001500000003";
        try {
            PredioBloqueo conteo = this.daoDos.findCurrentPredioBloqueo(numeroPredial);
            LOGGER.debug("resultado = " + conteo);

            if (conteo != null) {
                LOGGER.debug("Fecha inicial = " + conteo.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + conteo.getMotivoBloqueo());
            }

            Assert.assertTrue(conteo != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("no encontró bloqueo");
        }

    }

    @Test
    public void testConsultaNumeroPredioBloqueoAtlantico() {

        LOGGER.debug(
            "unit test PredioBloqueoDAOBeanTest#testConsultaNumeroPredioBloqueoAtlantico ...");

        //String numeroPredial = "080780109000003910001000000000";
        FiltroDatosConsultaPrediosBloqueo a = new FiltroDatosConsultaPrediosBloqueo();

        a.setDepartamentoCodigo("08");

        //List<PredioBloqueo> PrediosBloqueos; 
        try {
            //List<PredioBloqueo> conteo = this.dao.buscarPrediosBloqueo(a,1);
            int numprediobloqueo = this.daoDos.contarPrediosBloqueo(a);
            //LOGGER.debug("resultado = " + conteo);
            //LOGGER.debug("tamaño = " + conteo.size());	
            if (numprediobloqueo != 0) {
                //LOGGER.debug("Fecha inicial = "	+ conteo.toString());
                //LOGGER.debug("Motivo = " + conteo.getMotivoBloqueo());
                LOGGER.debug(" Numeros de bloqueos = " + numprediobloqueo);
            }

            //Assert.assertTrue(conteo != null);
            Assert.assertTrue(numprediobloqueo != 0);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("no encontró bloqueo");
        }
    }

    @Test
    public void testConsultaPredioBloqueoNumeroPredial() {

        LOGGER.
            debug("unit test PredioBloqueoDAOBeanTest#testConsultaPredioBloqueoNumeroPredial ...");

        String numeroPredial = "080010109000003910015000000000";

        //FiltroDatosConsultaPrediosBloqueo a= new FiltroDatosConsultaPrediosBloqueo();
        //a.setDepartamentoCodigo("08");
        //List<PredioBloqueo> PrediosBloqueos; 
        try {
            List<PredioBloqueo> conteo = this.dao.buscarPrediosBloqueoByNumeroPredial(numeroPredial,
                1);
            //int numprediobloqueo = this.dao.contarPrediosBloqueo(a);
            LOGGER.debug("resultado = " + conteo);
            LOGGER.debug("id predio bloqueo (0)= " + conteo.get(0).getId());
            LOGGER.debug("tamaño = " + conteo.size());
            //if (numprediobloqueo != 0) {
            if (conteo != null) {
                LOGGER.debug("Fecha inicial = " + conteo.toString());
                //LOGGER.debug("Motivo = " + conteo.getMotivoBloqueo());
                //LOGGER.debug(" Numeros de bloqueos = " + numprediobloqueo);
            }

            Assert.assertTrue(conteo != null);
            //Assert.assertTrue(numprediobloqueo != 0);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("no encontró bloqueo");
        }
    }

    @Test
    public void testConsultaPredioBloqueoAtlantico() {

        LOGGER.debug("unit test PredioBloqueoDAOBeanTest#testConsultaPredioBloqueoAtlantico ...");

        //String numeroPredial = "080780109000003910001000000000";
        FiltroDatosConsultaPrediosBloqueo a = new FiltroDatosConsultaPrediosBloqueo();

        a.setDepartamentoCodigo("08");

        //List<PredioBloqueo> PrediosBloqueos; 
        try {
            List<PredioBloqueo> conteo = this.dao.buscarPrediosBloqueo(a, 1, 100);
            //int numprediobloqueo = this.dao.contarPrediosBloqueo(a);
            LOGGER.debug("resultado = " + conteo);
            LOGGER.debug("tamaño = " + conteo.size());
            LOGGER.debug("id predio bloqueo (0)= " + conteo.get(0).getId());
            if (conteo != null) {
                LOGGER.debug("Fecha inicial = " + conteo.toString());
                //LOGGER.debug("Motivo = " + conteo.);
                //LOGGER.debug(" Numeros de bloqueos = " + numprediobloqueo);
            }

            Assert.assertTrue(conteo != null);
            //Assert.assertTrue(numprediobloqueo != 0);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("no encontró bloqueo");
        }
    }

}
