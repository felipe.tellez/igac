package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionContratoDAO;
import co.gov.igac.snc.dao.actualizacion.IComisionIntegranteDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionContratoDAOBean;
import co.gov.igac.snc.dao.actualizacion.impl.ComisionIntegranteDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;
import co.gov.igac.snc.persistence.entity.actualizacion.ComisionIntegrante;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.util.EActualizacionComisionObjeto;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author franz.gamba
 */
public class ComisionIntegranteDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(ComisionIntegranteDAOBeanTest.class);

    private IComisionIntegranteDAO dao = new ComisionIntegranteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testGetcomisionIntegrantesByActualizacionIdAndObjeto() {
        try {

            List<ComisionIntegrante> integrantes = new ArrayList<ComisionIntegrante>();
            Long actualizacionId = 450L;
            String objeto = EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.getCodigo() +
                "BARRANQUILLA (Distrito Especial, Industrial y Portuario)";

            integrantes = dao.getcomisionIntegrantesByActualizacionIdAndObjeto(actualizacionId,
                objeto);
            assertNotNull(integrantes);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

}
