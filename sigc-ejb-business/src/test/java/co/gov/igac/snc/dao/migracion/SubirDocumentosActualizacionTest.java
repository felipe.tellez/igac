package co.gov.igac.snc.dao.migracion;

import static org.testng.AssertJUnit.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.PUnidadConstruccionDto;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.dao.generales.IDocumentoDAO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.IParametroDAO;
import co.gov.igac.snc.dao.generales.impl.DocumentoDAOBean;
import co.gov.igac.snc.dao.generales.impl.LogMensajeDAOBean;
import co.gov.igac.snc.dao.generales.impl.ParametroDAOBean;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.LogMensajeFactory;

/** *
 * Programa encargado de almacenar las archivos de soporte y fotos de tramites de actualizacion para
 * SNC
 *
 * @author juan.mendez
 *
 */
public class SubirDocumentosActualizacionTest extends BaseTest {

    public static Logger LOGGER = LoggerFactory.getLogger(SubirDocumentosActualizacionTest.class);
    private IDocumentoDAO daoDocumento = new DocumentoDAOBean();
    private ILogMensajeDAO logMensajeDAO = new LogMensajeDAOBean();
    private UsuarioDTO usuario = new UsuarioDTO();
    private IParametroDAO parametroDAO = new ParametroDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.logMensajeDAO.setEntityManager(this.em);
        this.daoDocumento.setEntityManager(this.em);
        this.parametroDAO.setEntityManager(this.em);
        usuario.setLogin("SNC_MIGRACION");
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * Lanza el proceso de carga de documentos de actualizacion
     */
    /*
     * @modified felipe.cadena
     *
     * se prepara el test para que pueda ser ejecutado desde consola con los parametros necesarios
     * ej:mvn -Dtest=*SubirDocumentosActualizacionTest#testCargaDocumentosActualizacion
     * -DrutaUrl=C:\\snc\\FotosCargaActualizacion -DsolicitudId=43289 -DcodigoTerritorial=6040 test
     */
    @Test
    public void testCargaDocumentosActualizacion() {
        LOGGER.debug("testCargaDocumentos");
        try {
            LOGGER.info("***********************************************************");
            LOGGER.info("***********************************************************");
            LOGGER.info("Inicia carga Documentos Actualizacion");

            //Parametros obtenidos desde consola
            String idRepositorio = System.getProperty("rutaUrl");
            Long solicitudId = Long.valueOf(System.getProperty("solicitudId"));
            String codigoTerritorial = System.getProperty("codigoTerritorial");

            System.out.println(idRepositorio);
            System.out.println("--------------------------------------------------------");
            System.out.println(solicitudId);
            System.out.println("--------------------------------------------------------");
            System.out.println(codigoTerritorial);
            System.out.println("--------------------------------------------------------");

            System.out.println("--------------------------------------------------------");

            //Debe ser la carpeta raíz de un departamento
            //String carpetaBase = "C:\\snc\\FotosCargaActualizacion";
            //String carpetaBase = FileUtils.getTempDirectory().getAbsolutePath() + "\\snc\\FotosCargaActualizacion";
            //String carpetaBase = rutaUrl;
            //LOGGER.info("Buscando subdirectorios para: "+carpetaBase);
            //File directorioEntrada = new File(carpetaBase);
            //String[] files = directorioEntrada.list( DirectoryFileFilter.INSTANCE );
            LOGGER.info("***********************************************************");
            LOGGER.info("***********************************************************");

            LOGGER.info("Id Repositorio:" + idRepositorio);

            //Solicitud solicitud = new Solicitud();
            //solicitud.setId(151554L);
            //String codigoTerritorial = "6320";
            //TODO: Ignorar los que ya se alcanzaron a cargar
            Integer contador = daoDocumento.
                contarDocumentosPorSolicitudIdSinRepositorio(solicitudId);

            while (contador > 0) {
                this.cargaDocumentosActualizacion(idRepositorio, solicitudId, codigoTerritorial);
                this.cargarFotografias(idRepositorio, solicitudId, codigoTerritorial);
                contador = daoDocumento.contarDocumentosPorSolicitudIdSinRepositorio(solicitudId);
            }

            LOGGER.info("***********************************************************");
            LOGGER.info("***********************************************************");
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }
    }

    /**
     * Este método se encarga de cargar los documentos soporte de tramites de actualizacion al
     * sistema nacional catastral (SNC) Para algunos archivos no se puede encontrar un predio en la
     * base de datos (Ejm: Cuando el predio ya no existe) En esos casos se guarda la ficha en el
     * sistema documental pero no se le asocia un predio.
     *
     * Este programa lee una carpeta y carga los documentos en el sistema documental. Asocia dichos
     * documentos con los predios de la base de datos.
     *
     */
    ///Pedir como parametro id de la Solicitud
    @Test
    public void cargaDocumentosActualizacion(String idRepositorio, Long solicitudId,
        String codigoTerritorial) {
        LOGGER.info("idRepositorio: " + idRepositorio);

        try {
            IDocumentosService servicioDocumental = DocumentalServiceFactory.getService();
            int cantidadCargados = 0;
            int totalArchivos = 0;
            String idServicioDocumental = null;

            DocumentoTramiteDTO documentoTramiteDTO = new DocumentoTramiteDTO();
            List<Documento> documentos = null;

            Parametro maxEnvioJob = this.parametroDAO.getParametroPorNombre(
                EParametro.CANTIDAD_ARCHIVOS_CARGAR_FTP.toString());
            double cantidaDocumentosCargar = maxEnvioJob.getValorNumero();
            int cantidaDocumentosCargarInt = (int) cantidaDocumentosCargar;

            if (this.em.getTransaction().isActive() == false) {
                this.em.getTransaction().begin();
            }

            // Consulta documentos con solicitud, tramite e idRepositorio en null. Buscar paginador
            // en Oracle que me traiga cantidaDocumentosCargar:500 registros
            documentos = daoDocumento.buscarDocumentosPorSolicitudIdSinRepositorio(solicitudId,
                cantidaDocumentosCargarInt);

            if (documentos != null) {

                for (Documento documento : documentos) {
                    totalArchivos++;

                    if (documento.getArchivo() != null && documento.getNumeroPredial() != null) {

                        String nombreArchivo = idRepositorio + documento.getArchivo();
                        String url = servicioDocumental.descargarDocumento(nombreArchivo);
                        if (url == null) {
                            String errorMessage =
                                "No se encontro el archivo asociado al documento con id :" +
                                documento.getId();
                            generateErrorLog(errorMessage);

                            documento.setIdRepositorioDocumentos(errorMessage);
                            daoDocumento.update(documento);
                            LOGGER.info("Ruta generada:" + documento.getIdRepositorioDocumentos());
                            cantidadCargados++;
                        }

                        documentoTramiteDTO.setRutaArchivo(url);
                        documentoTramiteDTO.setIdTramite(documento.getTramiteId());
                        documentoTramiteDTO.setTipoDocumento(documento.getTipoDocumento().getId().
                            toString());
                        documentoTramiteDTO.setNumeroPredial(documento.getNumeroPredial());
                        documentoTramiteDTO.setFechaCreacionTramite(documento.getFechaLog());
                        documentoTramiteDTO.setNombreDepartamento("");
                        documentoTramiteDTO.setNombreMunicipio("");

                        this.usuario.setLogin(documento.getUsuarioLog());
                        this.usuario.setCodigoTerritorial(codigoTerritorial);

                        if (documentoTramiteDTO != null) {

                            idServicioDocumental = servicioDocumental.cargarDocumentoTramite(
                                documentoTramiteDTO, this.usuario);
                            LOGGER.info("Ruta generada:" + idServicioDocumental);

                            if (idServicioDocumental != null) {

                                documento.setIdRepositorioDocumentos(idServicioDocumental);

                                daoDocumento.update(documento);
                                cantidadCargados++;

                            } else {
                                String errorMessage =
                                    "No se pudo generar la ruta para el documento con id: " +
                                    documento.getId();
                                generateErrorLog(errorMessage);

                                documento.setIdRepositorioDocumentos(errorMessage);
                                daoDocumento.update(documento);
                                LOGGER.info("Ruta generada:" + documento.
                                    getIdRepositorioDocumentos());
                                cantidadCargados++;
                            }
                        } else {
                            String errorMessage =
                                "No se pudo crear el documentoTramiteDTO para el documento con id: " +
                                documento.getId();
                            generateErrorLog(errorMessage);

                            documento.setIdRepositorioDocumentos(errorMessage);
                            daoDocumento.update(documento);
                            LOGGER.info("Ruta generada:" + documento.getIdRepositorioDocumentos());
                            cantidadCargados++;
                        }
                    } else {
                        String errorMessage = "El documento con id:" + documento.getId() +
                            "no tiene asociado un numero predial o no se encontro el nombre del archivo";
                        generateErrorLog(errorMessage);
                        documento.setIdRepositorioDocumentos(errorMessage);
                        daoDocumento.update(documento);
                        LOGGER.info("Ruta generada:" + documento.getIdRepositorioDocumentos());
                        cantidadCargados++;

                    }

                }

            }
            this.em.getTransaction().commit();

            LOGGER.
                info("**************************************************************************");
            LOGGER.info("	Resultados Carga Documentos para: " + idRepositorio);
            LOGGER.info("		Cantidad total de archivos	:	" + totalArchivos);
            LOGGER.info("		Cantidad Cargados	:	" + cantidadCargados);
            LOGGER.info("		Cantidad de archivos que no pudieron cargarse:" + (totalArchivos -
                cantidadCargados));
            LOGGER.
                info("**************************************************************************");

            if (totalArchivos != cantidadCargados) {
                String errorMessage = "No se pudieron cargar todos los archivos correctamente. " +
                    "\nCantidad total de archivos:" + totalArchivos +
                    "\nCantidad cargados:" + cantidadCargados +
                    "\nCantidad sin cargar:" + (totalArchivos - cantidadCargados);
                generateErrorLog(errorMessage);
            }

        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            this.em.getTransaction().rollback();
        } finally {
            //
        }
    }

    /**
     * Este método se encarga de migrar las fotografías de las fachadas al sistema nacional
     * catastral (SNC) Para algunos archivos no se puede encontrar un predio en la base de datos (
     * Ejm: Cuando el predio ya no existe ) En esos casos la foto no se puede cargar en el sistema
     * documental.
     *
     * Este programa lee una carpeta y carga las fotografías en el sistema documental. Asocia dichos
     * documentos con los predios de la base de datos.
     *
     */
    /*
     * se prepara el test para que pueda ser ejecutado desde consola con los parametros necesarios
     * ej:mvn -Dtest=*SubirDocumentosActualizacionTest#testCargarFotografias
     * -DrutaUrl=C:\\snc\\FotosCargaActualizacion -DsolicitudId=43289 -DcodigoTerritorial=6040 test
     */
    @Test
    public void testCargarFotografias() {
        LOGGER.debug("testCargarFotografias");
        try {
            LOGGER.info("***********************************************************");
            LOGGER.info("Inicia carga Fotografías");
            //String carpetaBase = "C:\\snc\\FotosCargaActualizacion";

            //Parametros obtenidos desde consola
            String idRepositorio = System.getProperty("rutaUrl");
            Long solicitudId = Long.valueOf(System.getProperty("solicitudId"));
            String codigoTerritorial = System.getProperty("codigoTerritorial");

            System.out.println(idRepositorio);
            System.out.println("--------------------------------------------------------");
            System.out.println(solicitudId);
            System.out.println("--------------------------------------------------------");
            System.out.println(codigoTerritorial);
            System.out.println("--------------------------------------------------------");

            System.out.println("--------------------------------------------------------");

            LOGGER.info("***********************************************************");
            LOGGER.info("***********************************************************");
            LOGGER.info("** idRepositorio: " + idRepositorio);

            Solicitud solicitud = new Solicitud();
            solicitud.setId(solicitudId);
            //solicitud.setId(43289L);
            //String codigoTerritorial = "08001";

            Integer contadorFotos = daoDocumento.contarDocumentosPorSolicitudIdSinRepositorioFotos(
                solicitudId);

            while (contadorFotos > 0) {
                cargarFotografias(idRepositorio, solicitud.getId(), codigoTerritorial);
                contadorFotos = daoDocumento.contarDocumentosPorSolicitudIdSinRepositorio(
                    solicitudId);
            }

            LOGGER.info("***********************************************************");

            LOGGER.info("***********************************************************");
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }
    }

    /**
     *
     * @param directorioEntrada,solicitudId,codigoTerritorial
     */
    private void cargarFotografias(String idRepositorio, Long solicitudId, String codigoTerritorial) {
        LOGGER.info("***********************************************************");
        LOGGER.info("CargarFotografias para: " + idRepositorio);
        try {
            IDocumentosService servicioDocumentalFoto = DocumentalServiceFactory.getService();
            int cantidadCargadosFoto = 0;
            int totalArchivosFoto = 0;
            List<Documento> documentosFotos = null;
            String idServicioDocumentalFoto = null;
            PUnidadConstruccionDto pUnidadConstruccionDto = new PUnidadConstruccionDto("");

            if (this.em.getTransaction().isActive() == false) {
                this.em.getTransaction().begin();
            }

            LOGGER.debug("****");

            LOGGER.debug("archivo: " + idRepositorio);

            Parametro maxEnvioJob = parametroDAO.getParametroPorNombre(
                EParametro.CANTIDAD_ARCHIVOS_CARGAR_FTP.toString());
            double cantidaDocumentosCargar = maxEnvioJob.getValorNumero();
            int cantidaDocumentosCargarInt = (int) cantidaDocumentosCargar;

            documentosFotos = daoDocumento.buscarDocumentosPorSolicitudIdSinRepositorioFotos(
                solicitudId, cantidaDocumentosCargarInt);

            //documentos = this.asociarUnidadesConstruccion(documentos);
            if (documentosFotos != null && !documentosFotos.isEmpty()) {

                for (Documento documentoFoto : documentosFotos) {
                    totalArchivosFoto++;

                    DocumentoTramiteDTO documentoTramiteFotoDTO = new DocumentoTramiteDTO();

                    documentoTramiteFotoDTO.setTipoDocumento(ETipoDocumento.FOTOGRAFIA.toString());
                    documentoTramiteFotoDTO.setFechaCreacionTramite(new Date());

                    if (documentoFoto.getArchivo() != null &&
                        documentoFoto.getNumeroPredial() != null) {

                        String nombreArchivo = idRepositorio + documentoFoto.getArchivo();
                        String url = servicioDocumentalFoto.descargarDocumento(nombreArchivo);
                        if (url == null) {

                            String errorMessage =
                                "No se encontro el archivo asociado a la foto con id :" +
                                documentoFoto.getId();
                            generateErrorLog(errorMessage);

                            documentoFoto.setIdRepositorioDocumentos(errorMessage);
                            daoDocumento.update(documentoFoto);
                            LOGGER.info("Ruta generada:" + documentoFoto.
                                getIdRepositorioDocumentos());
                            cantidadCargadosFoto++;
                        }

                        documentoTramiteFotoDTO.setRutaArchivo(url);
                        documentoTramiteFotoDTO.setNumeroPredial(documentoFoto.getNumeroPredial());

                        if (documentoFoto.getpUnidadConstruccion() != null) {
                            pUnidadConstruccionDto.setId(documentoFoto.getpUnidadConstruccion().
                                getId());
                            pUnidadConstruccionDto.setUnidad(documentoFoto.getpUnidadConstruccion().
                                getUnidad());
                            pUnidadConstruccionDto.setModelo("");
                            documentoTramiteFotoDTO.setUnidadConstruccion(pUnidadConstruccionDto);
                        }

                        this.usuario.setLogin(documentoFoto.getUsuarioLog());
                        this.usuario.setCodigoTerritorial(codigoTerritorial);

                        if (documentoTramiteFotoDTO != null) {

                            idServicioDocumentalFoto = servicioDocumentalFoto
                                .cargarDocumentoPredioFoto(documentoTramiteFotoDTO, this.usuario);
                            LOGGER.info("Ruta generada:" + idServicioDocumentalFoto);

                            if (idServicioDocumentalFoto != null) {

                                documentoFoto.setIdRepositorioDocumentos(idServicioDocumentalFoto);
                                daoDocumento.update(documentoFoto);
                                LOGGER.info("Ruta generada:" + documentoFoto.
                                    getIdRepositorioDocumentos());
                                cantidadCargadosFoto++;

                            } else {
                                String errorMessage =
                                    "No se pudo  generar la ruta para la foto con id:" +
                                    documentoFoto.getId();
                                generateErrorLog(errorMessage);

                                documentoFoto.setIdRepositorioDocumentos(errorMessage);
                                daoDocumento.update(documentoFoto);
                                LOGGER.info("Ruta generada:" + documentoFoto.
                                    getIdRepositorioDocumentos());
                                cantidadCargadosFoto++;
                            }

                        } else {
                            String errorMessage =
                                "No se pudo crear el documentoTramiteDTO para la foto con id" +
                                documentoFoto.getId();
                            generateErrorLog(errorMessage);

                            documentoFoto.setIdRepositorioDocumentos(errorMessage);
                            daoDocumento.update(documentoFoto);
                            LOGGER.info("Ruta generada:" + documentoFoto.
                                getIdRepositorioDocumentos());
                            cantidadCargadosFoto++;
                        }

                    } else {
                        String errorMessage = "La Foto con id:" + documentoFoto.getId() +
                            "no tiene asociado un numero predial o no se encontro el nombre del archivo";
                        generateErrorLog(errorMessage);

                        documentoFoto.setIdRepositorioDocumentos(errorMessage);
                        daoDocumento.update(documentoFoto);
                        cantidadCargadosFoto++;

                    }
                }
            }
            this.em.getTransaction().commit();

            LOGGER.info("****************************************************");
            LOGGER.info("	Carga para para: " +
                idRepositorio);
            LOGGER.info("		Directorio:" +
                idRepositorio);
            LOGGER.info("		Cantidad total de fotos:" + totalArchivosFoto);
            LOGGER.info("		Cantidad cargados fotos:" + cantidadCargadosFoto);
            LOGGER.info("		Cantidad de fotos que no se pudieron cargar:" +
                (totalArchivosFoto - cantidadCargadosFoto));
            LOGGER.info("****************************************************");

            if (totalArchivosFoto != cantidadCargadosFoto) {
                String errorMessage = "No se pudieron cargar todos los archivos correctamente. " +
                    "\nCantidad total de archivos Foto:" + totalArchivosFoto +
                    "\nCantidad cargados Foto:" + cantidadCargadosFoto +
                    "\nCantidad sin cargar:" + (totalArchivosFoto - cantidadCargadosFoto);
                generateErrorLog(errorMessage);
            }

        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            this.em.getTransaction().rollback();
        } finally {
            //
        }
    }

    /**
     * Asocia las unidades de contruccion a los documentos dados
     *
     * @author felipe.cadena
     * @param documentos
     * @return
     */
    private List<Documento> asociarUnidadesConstruccion(List<Documento> documentos) {

        List<Documento> documentosTemp = new ArrayList<Documento>();

        documentosTemp = this.daoDocumento.obtenerMUnidadesConstruccionDocumento(documentos);

        return documentosTemp;
    }

    /**
     * @author felipe.cadena
     * @param documentos
     * @return
     */
    @Test
    private void testAsociarUnidadesConstruccion() {

        Documento d = this.daoDocumento.findById(560853l);
        List<Documento> documentosTemp = new ArrayList<Documento>();
        documentosTemp.add(d);
        try {
            documentosTemp = this.asociarUnidadesConstruccion(documentosTemp);
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }

    }

    /**
     * @author leidy.gonzalez
     * @param documentos
     * @return
     */
    @Test
    private void testBuscarDocumentosPorSolicitudIdSinRepositorioFotos() {

        List<Documento> documentosTemp = new ArrayList<Documento>();

        Solicitud solicitud = new Solicitud();
        solicitud.setId(43289L);

        Parametro maxEnvioJob = this.parametroDAO.getParametroPorNombre(
            EParametro.CANTIDAD_ARCHIVOS_CARGAR_FTP.toString());
        double cantidaDocumentosCargar = maxEnvioJob.getValorNumero();
        int cantidaDocumentosCargarInt = (int) cantidaDocumentosCargar;

        try {
            documentosTemp = this.daoDocumento.buscarDocumentosPorSolicitudIdSinRepositorioFotos(
                solicitud.getId(), cantidaDocumentosCargarInt);
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }

    }

    /**
     * @author leidy.gonzalez
     * @param documentos
     * @return
     */
    @Test
    private void buscarDocumentosPorSolicitudIdSinRepositorio() {

        List<Documento> documentosTemp = new ArrayList<Documento>();

        Solicitud solicitud = new Solicitud();
        solicitud.setId(43289L);

        Parametro maxEnvioJob = this.parametroDAO.getParametroPorNombre(
            EParametro.CANTIDAD_ARCHIVOS_CARGAR_FTP.toString());
        double cantidaDocumentosCargar = maxEnvioJob.getValorNumero();
        int cantidaDocumentosCargarInt = (int) cantidaDocumentosCargar;

        try {
            documentosTemp = this.daoDocumento.buscarDocumentosPorSolicitudIdSinRepositorio(
                solicitud.getId(), cantidaDocumentosCargarInt);
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }

    }

    /**
     * @author leidy.gonzalez
     * @param documentos
     * @return
     */
    @Test
    private void contarDocumentosPorSolicitudIdSinRepositorio() {

        Integer contador = new Integer(0);
        Solicitud solicitud = new Solicitud();
        solicitud.setId(43289L);

        try {
            contador = this.daoDocumento.contarDocumentosPorSolicitudIdSinRepositorio(solicitud.
                getId());
        } catch (Throwable e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage());
        }

    }

    /**
     *
     * @param usuario
     * @param message
     */
    private void generateErrorLog(String message) {
        LOGGER.error(
            "**********************************************************************************");
        LOGGER.error(message);
        LogMensaje log = LogMensajeFactory.getLogMensaje(usuario, "MIGRACION_DOCUMENTOS");
        log.setNivel("ERROR");
        log.setMensaje(message);
        logMensajeDAO.persist(log);
        LOGGER.error(
            "**********************************************************************************");
    }

}
