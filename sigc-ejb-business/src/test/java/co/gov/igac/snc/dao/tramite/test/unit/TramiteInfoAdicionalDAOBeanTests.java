package co.gov.igac.snc.dao.tramite.test.unit;

import co.gov.igac.snc.dao.tramite.ITramiteInfoAdicionalDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteInfoAdicionalDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInfoAdicional;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/*
 * Proyecto SNC 2017
 */
/**
 * Clase de test para las operaciones de DB de la clase {@link TramiteInfoAdicional}
 *
 * @author felipe.cadena
 */
public class TramiteInfoAdicionalDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TramiteInfoAdicionalDAOBeanTests.class);

    private ITramiteInfoAdicionalDAO dao = new TramiteInfoAdicionalDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("TramiteInfoAdicionalDAOBeanTests#testUpdate");

        try {

            TramiteInfoAdicional testEntity = new TramiteInfoAdicional();

            Tramite t = new Tramite();
            t.setId(135217l);

            testEntity.setTramite(t);
            testEntity.setCampo("str::f1y940kvjges0cuqsemi");
            testEntity.setValor("str::5i2fe6my8l6tln02otj4i");
            testEntity.setUsuarioLog("str::q2su8kxax2csz5tmaemi");
            testEntity.setFechaLog(new Date());

            this.dao.findAll();

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
