/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.tramite.ITramiteTareaDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteTareaDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class TramiteTareaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDepuracionDAOBeanTest.class);

    private ITramiteTareaDAO dao = new TramiteTareaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testObtenerTramitesEnJobGeografico() {

        LOGGER.debug("on TramiteDepuracionDAOBeanTest#testObtenerTramitesEnJobGeografico ...");

        try {
            List<TramiteTarea> tds = dao.obtenerTramitesEnJobGeografico();

            TramiteTarea td = null;
            if (!tds.isEmpty()) {
                td = tds.get(0);
                LOGGER.debug(">>>>> " + td.getId() + " -- t = " + td.getTramite().getId());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

}
