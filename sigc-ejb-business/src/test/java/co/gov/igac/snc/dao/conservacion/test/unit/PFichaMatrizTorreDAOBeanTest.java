package co.gov.igac.snc.dao.conservacion.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPFichaMatrizTorreDAO;
import co.gov.igac.snc.dao.conservacion.impl.PFichaMatrizTorreDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class PFichaMatrizTorreDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PFichaMatrizTorreDAOBeanTest.class);

    private IPFichaMatrizTorreDAO dao = new PFichaMatrizTorreDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    //---------------------------------------//
    /**
     * Test para probar cargue de torres de una ficha matriz.
     *
     * @author david.cifuentes
     */
    @Test
    public void testFindByFichaMatrizId() {

        LOGGER.debug("PFichaMatrizTorreDAOBeanTest#testFindByFichaMatrizId");

        List<PFichaMatrizTorre> pListTorres;
        Long idFihaMatriz = 55L;
        try {
            pListTorres = this.dao.findByFichaMatrizId(idFihaMatriz);
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }
}
