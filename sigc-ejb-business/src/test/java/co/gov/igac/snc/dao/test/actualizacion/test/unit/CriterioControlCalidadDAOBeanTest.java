package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.ICriterioControlCalidadDAO;
import co.gov.igac.snc.dao.actualizacion.IEventoAsistenteDAO;
import co.gov.igac.snc.dao.actualizacion.impl.CriterioControlCalidadDAOBean;
import co.gov.igac.snc.dao.actualizacion.impl.EventoAsistenteDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.CriterioControlCalidad;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.test.unit.BaseTest;

public class CriterioControlCalidadDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        CriterioControlCalidadDAOBeanTest.class);

    private ICriterioControlCalidadDAO dao = new CriterioControlCalidadDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * javier.barajas
     */
    @Test
    public void testConsultarCriterioControlCalidad() {
        Long idCriterioControlCalidad = 1L;

        CriterioControlCalidad temp = this.dao.consultarCriterioControlCalidad(
            idCriterioControlCalidad);
        LOGGER.debug("Criterio Control de Calidad => " + temp.getCategoria());

    }

}
