/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ITramiteDetallePredioDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteDetallePredioDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pedor.garcia
 */
public class TramiteDetallePredioDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TramiteDetallePredioDAOBeanTests.class);

    private ITramiteDetallePredioDAO dao = new TramiteDetallePredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testGetByTramiteId() {

        LOGGER.debug("unit test: TramiteDetallePredioDAOBeanTests#testGetByTramiteId");
        List<TramiteDetallePredio> tpe;

        Long idTramite = 87l;

        try {
            tpe = this.dao.getByTramiteId(idTramite);

            LOGGER.debug("... passed!");
            assertTrue(true);

            if (!tpe.isEmpty()) {
                LOGGER.debug("un predio num-predial: " + tpe.get(0).getPredio().getNumeroPredial());
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testGetNumerosPredialesByTramiteId() {

        LOGGER.debug(
            "unit test: TramiteDetallePredioDAOBeanTests#testGetNumerosPredialesByTramiteId");
        List<String> numsprediales;

        List<Long> idTramites = new LinkedList<Long>();
        idTramites.add(37043L);
        idTramites.add(37049L);

        try {
            numsprediales = this.dao.getNumerosPredialesByTramiteId(idTramites);
            assertNotNull(numsprediales);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
            assertFalse(true);
        }

    }

    /**
     *
     * @author felipe.cadena
     */
    @Test
    public void testCountByTramiteId() {

        LOGGER.debug("unit test: TramiteDetallePredioDAOBeanTests#testCountByTramiteId");
        Integer numsprediales;

        Long idTramite = 37043L;

        try {
            numsprediales = this.dao.countByTramiteId(idTramite);
            assertNotNull(numsprediales);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
            assertFalse(true);
        }

    }

}
