/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.actualizacion.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IReporteActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ReporteActualizacionDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ReporteActualizacion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author javier.aponte
 */
public class ReporteActualizacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReporteActualizacionDAOBeanTest.class);

    private IReporteActualizacionDAO dao = new ReporteActualizacionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author javier.aponte
     */
    @Test
    public void testBuscarReportesActualizacionPorCodigoMunicipio() {
        LOGGER.debug(
            "ReporteActualizacionDAOBeanTest#testBuscarReportesActualizacionPorCodigoMunicipio");

        try {
            List<Documento> ra = dao.buscarReportesActualizacionPorCodigoMunicipio(
                "23300", "2015");

            Assert.assertNotNull(ra);

            for (Documento raTemp : ra) {
                /*LOGGER.debug("Producto catastral job id: " + raTemp.getProductoCatastralJob().
                    getId());*/
                LOGGER.debug("Producto catastral job id: " );
            }

        } catch (Exception e) {
            LOGGER.error(
                "Error en ReporteActualizacionDAOBeanTest#testBuscarReportesActualizacionPorCodigoMunicipio: " +
                e.getMessage(), e);
            Assert.fail();
        }

    }

}
