package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.generales.IDepartamentoDAO;
import co.gov.igac.snc.dao.generales.IMunicipioDAO;
import co.gov.igac.snc.dao.generales.impl.DepartamentoDAOBean;
import co.gov.igac.snc.dao.generales.impl.MunicipioDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class DepartamentoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DepartamentoDAOBeanTest.class);

    private IDepartamentoDAO dao = new DepartamentoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarDepartamentosConCatastroCentralizadoPorCodigoPais() {
        LOGGER.debug(
            "DepartamentoDAOBeanTest#testBuscarDepartamentosConCatastroCentralizadoPorCodigoPais");
        List<Departamento> departamentosConCatasCentralizado;

        // Código del país
        String paisCodigo = "140";
        try {
            departamentosConCatasCentralizado = this.dao.
                buscarDepartamentosConCatastroCentralizadoPorCodigoPais(paisCodigo);
            // Debido a que Antioquia es un departamento con Catastro desentralizado, éste no debería aparecer en la lista.

            assertNotNull(departamentosConCatasCentralizado);
            assertTrue(departamentosConCatasCentralizado.size() > 0);
            LOGGER.info("Size return departamentos: " + departamentosConCatasCentralizado.size());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }
}
