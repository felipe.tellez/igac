/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ITramiteTextoResolucionDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteTextoResolucionDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.test.unit.BaseTest;

public class TramiteTextoResolucionDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteTextoResolucionDAOBeanTests.class);

    private ITramiteTextoResolucionDAO dao = new TramiteTextoResolucionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte pre: hay datos de prueba en la tabla TRAMITE_TEXTO_RESOLUCION que
     * corresponden a los usados en esta prueba
     */
    @Test
    public void testFindTramiteTextoResolucionPorIdTramite() {
        TramiteTextoResolucion tramiteTextoResolucion;
        tramiteTextoResolucion = dao.findTramiteTextoResolucionPorIdTramite(new Long(96));
        Assert.assertNotNull(tramiteTextoResolucion);
    }

}
