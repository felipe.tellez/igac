package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IAsignacionDAO;
import co.gov.igac.snc.dao.avaluos.impl.AsignacionDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class AsignacionDAOTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AsignacionDAOTest.class);
    private IAsignacionDAO dao = new AsignacionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar {@link AsignacionDAOBean#ConsultarAsignacionActualAvaluo(Long)}
     *
     * @author felipe.cadena
     */
    @Test
    public void testConsultarAsignacionActualAvaluo() {
        LOGGER.debug("iniciando AsignacionDAOTest#testConsultarAsignacionActualAvaluo");

        Asignacion result = null;

        result = dao.consultarAsignacionActualAvaluo(172L, EAvaluoAsignacionProfActi.AVALUAR.
            getCodigo());

        LOGGER.debug("***********************************************");
        if (result != null) {
            LOGGER.debug("************** " + result.getFuncionarioAsignador() +
                " ******************");
        }
        LOGGER.debug("***********************************************");

        LOGGER.debug("finalizando AsignacionDAOTest#testConsultarAsignacionActualAvaluo");
    }
}
