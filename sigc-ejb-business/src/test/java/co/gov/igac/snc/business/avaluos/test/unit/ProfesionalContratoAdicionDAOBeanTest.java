package co.gov.igac.snc.business.avaluos.test.unit;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IProfesionalContratoAdicionDAO;
import co.gov.igac.snc.dao.avaluos.impl.ProfesionalContratoAdicionDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase de pruebas para ProfesionalContratoAdicionDAOBean
 *
 * @author rodrigo.hernandez
 *
 */
public class ProfesionalContratoAdicionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProfesionalContratoAdicionDAOBeanTest.class);

    private IProfesionalContratoAdicionDAO dao = new ProfesionalContratoAdicionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar
     * {@link ProfesionalContratoAdicionDAOBean#obtenerFechaFinalPAContratoAdiciones(Long)}
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testObtenerFechaFinalPAContratoAdiciones() {
        LOGGER.debug(
            "iniciando ProfesionalContratoAdicionDAOBeanTest#testObtenerFechaFinalPAContratoAdiciones");

        Date result = null;

        result = dao.obtenerFechaFinalPAContratoAdiciones(3L);

        LOGGER.debug(String.valueOf(result));

        LOGGER.debug(
            "finalizando ProfesionalContratoAdicionDAOBeanTest#testObtenerFechaFinalPAContratoAdiciones");
    }
}
