/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import java.util.List;
import co.gov.igac.snc.dao.tramite.ITipoSolicitudTramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.TipoSolicitudTramiteDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 *
 * @author pedro.garcia
 */
public class TipoSolicitudTramiteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TipoSolicitudTramiteDAOBeanTests.class);

    private ITipoSolicitudTramiteDAO dao = new TipoSolicitudTramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     */
    @Test
    public void testfindByTipoSolicitud() {

        LOGGER.debug("unit test: TipoSolicitudTramiteDAOBeanTests#testfindByTipoSolicitud ...");
        List<TipoSolicitudTramite> cositos = null;
        String tipoSolicitud;

        tipoSolicitud = "1";
        try {
            cositos = this.dao.findByTipoSolicitud(tipoSolicitud);
            LOGGER.debug("... passed!. fetched " + cositos.size() + "rows");

            if (!cositos.isEmpty()) {
                LOGGER.debug("tipos de trámite para la solicitud 1:");
                for (TipoSolicitudTramite tst : cositos) {
                    LOGGER.debug(tst.getTipoTramite() + ", ");
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }

}
