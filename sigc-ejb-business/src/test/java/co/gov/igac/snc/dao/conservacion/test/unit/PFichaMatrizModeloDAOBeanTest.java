package co.gov.igac.snc.dao.conservacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPFichaMatrizModeloDAO;
import co.gov.igac.snc.dao.conservacion.impl.PFichaMatrizModeloDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author david.cifuentes
 *
 */
public class PFichaMatrizModeloDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PFichaMatrizModeloDAOBeanTest.class);

    private IPFichaMatrizModeloDAO dao = new PFichaMatrizModeloDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // ---------------------------------------------------//
    /**
     * Método test para probar el cargue de un modelo de construcción.
     *
     * @author david.cifuentes
     */
    @Test
    public void testCargarModeloDeConstruccion() {

        LOGGER.debug("PFichaMatrizModeloDAOBeanTest#testCargarModeloDeConstruccion");
        Long idPModeloFichaMatriz = 16L;
        try {
            PFichaMatrizModelo pModelo;
            pModelo = this.dao.cargarModeloDeConstruccion(idPModeloFichaMatriz);
            if (pModelo != null) {
                LOGGER.debug("Modelo cargado , cantidad de construcciones = " + pModelo.
                    getPFmModeloConstruccions().size());
                if (pModelo.getPFmModeloConstruccions().size() > 0) {
                    LOGGER.debug(" Los usos de la unidad son:");
                    for (PFmModeloConstruccion p : pModelo
                        .getPFmModeloConstruccions()) {
                        if (p.getUsoConstruccion() != null) {
                            LOGGER.debug(p.getUsoConstruccion().getNombre());
                            LOGGER.debug(p.getPFmConstruccionComponentes().get(0).
                                getElementoCalificacion());
                        }
                    }
                }
            }
            Assert.assertTrue(true);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }
}
