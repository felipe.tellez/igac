/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion.test.unit;

import static org.testng.Assert.*;
import co.gov.igac.snc.dao.tramite.radicacion.ITramitePredioEnglobeDAO;
import co.gov.igac.snc.dao.tramite.radicacion.impl.TramitePredioEnglobeDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.test.unit.BaseTest;

import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedor.garcia
 * @deprecated ver ITramitePredioEnglobeDAO
 */
@Deprecated
public class TramitePredioEnglobeDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TramitePredioEnglobeDAOBeanTests.class);

    private ITramitePredioEnglobeDAO dao = new TramitePredioEnglobeDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testGetByTramiteId() {

        LOGGER.debug("unit test: TramitePredioEnglobeDAOBeanTests#testGetByTramiteId");
        List<TramitePredioEnglobe> tpe;

        Long idTramite = 87l;

        try {
            tpe = this.dao.getByTramiteId(idTramite);

            LOGGER.debug("... passed!");
            assertTrue(true);

            if (!tpe.isEmpty()) {
                LOGGER.debug("un predio num-predial: " + tpe.get(0).getPredio().getNumeroPredial());
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testGetNumerosPredialesByTramiteId() {

        LOGGER.debug(
            "unit test: TramitePredioEnglobeDAOBeanTests#testGetNumerosPredialesByTramiteId");
        List<String> numsprediales;

        List<Long> idTramites = new LinkedList<Long>();
        idTramites.add(37043L);
        idTramites.add(37049L);

        try {
            numsprediales = this.dao.getNumerosPredialesByTramiteId(idTramites);
            assertNotNull(numsprediales);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
            assertFalse(true);
        }

    }

}
