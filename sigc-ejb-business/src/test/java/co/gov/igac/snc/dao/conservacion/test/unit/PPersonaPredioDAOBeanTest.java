package co.gov.igac.snc.dao.conservacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPersonaPredioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author juan.agudelo
 *
 */
public class PPersonaPredioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPersonaPredioDAOBeanTest.class);

    private IPPersonaPredioDAO dao = new PPersonaPredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author juan.agudelo
     */
    @Test
    public void buscarPPersonaPredioPorIdFetchPredioTest() {
        LOGGER.debug("PPersonaPredioDAOBeanTest#buscarPPersonaPredioPorIdFetchPredioTest");

        try {
            PPersonaPredio pp = dao.buscarPPersonaPredioPorIdFetchPredio(1161895L);
            LOGGER.debug("Número predial: " + pp.getPPredio().getNombre());
            Assert.assertNotNull(pp);
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage(), e);
            Assert.fail();
        }

    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testEliminarPPersonaPredio() {
        LOGGER.debug("PPersonaPredioDAOBeanTest#testEliminarPPersonaPredio");
        try {
            this.em.getTransaction().begin();
            PPersonaPredio ppp = dao.findAll(0, 1).get(0);
            LOGGER.debug("id PPersonaPredio: " + ppp.getId());
            ppp.setId(null);
            ppp = dao.update(ppp);
            this.dao.delete(ppp);
            this.em.getTransaction().commit();
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage(), e);
            Assert.fail();
        }
    }

}
