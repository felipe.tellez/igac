package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.util.EActualizacionDocumentacionZona;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class ActualizacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActualizacionDAOBeanTest.class);

    private IActualizacionDAO dao = new ActualizacionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     */
    @Test
    public void testObtenerUltimaActualizacionPorMunicipioYZona() {

        LOGGER.debug(
            "unit tests: ActualizacionDAOBeanTest#testObtenerUltimaActualizacionPorMunicipioYZona");

        String codigoMunicipio = "25754";
        String zona = EActualizacionDocumentacionZona.RURAL.getDescripcion();

        Actualizacion answer;

        try {
            answer = this.dao.obtenerUltimaActualizacionPorMunicipioYZona(codigoMunicipio, zona);

            Assert.assertNotNull(answer);
            LOGGER.debug("Actualización: " + answer.getId());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    @Test
    public void testAgregarActualizacionEvento() {

        LOGGER.debug("unit tests: ActualizacionDAOBeanTest#testAgregarActualizacionEvento");

        ActualizacionEvento actualizacionEvento = new ActualizacionEvento();
        Actualizacion actualizacion = this.dao.buscarActualizacionPorIdConConveniosYEntidades(439L);
        //this.actualizacionEventoSelecionada.setId(2L);
        actualizacionEvento.setNombre("Instalacion de comision de campo y asistentes invitados");
        actualizacionEvento.setActualizacion(actualizacion);
        actualizacionEvento.setTipo("INSTALACION COMISION");

        actualizacionEvento.setDescripcion(
            "Instalacion de comision de campo y generacion de acta de asistentes  para la ");
        //actualizacionEvento.setEventoAsistentes(this.listaAsistentesNuevos);
        //actualizacionEvento.setActaDocumento(this.actaReunion);
        long actualizacionId = actualizacion.getId();

        //Actualizacion answer;
        try {
            this.dao.agregarActualizacionEvento(actualizacionEvento);

            //Assert.assertNotNull(answer);
            //LOGGER.debug("Actualización: " + answer.getId());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testActualizacionporId() {

        LOGGER.debug("unit tests: ActualizacionDAOBeanTest#testActualizacionporId");

        Actualizacion answer;

        try {
            answer = this.dao.findById(450L);

            Assert.assertNotNull(answer);
            LOGGER.debug("Actualización Municipio: " + answer.getMunicipio());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testActualizacionLevantamientoporIdActualizacion() {

        LOGGER.debug(
            "unit tests: ActualizacionDAOBeanTest#testActualizacionLevantamientoporIdActualizacion");

        List<ActualizacionLevantamiento> answer = new ArrayList<ActualizacionLevantamiento>();

        try {
            answer = this.dao.obtenerActualizacionLevantamiento(450L, 1L);

            Assert.assertNotNull(answer);
            if (answer.size() > 0) {
                LOGGER.debug("Actualización levantamiento(tamaño): " + answer.size());
            } else {
                LOGGER.debug("Actualización levantamiento: no arrojo resultados");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testObtenerTopografosActualizacion() {

        LOGGER.debug("unit tests: ActualizacionDAOBeanTest#testObtenerTopografosActualizacion");

        List<LevantamientoAsignacion> answer = new ArrayList<LevantamientoAsignacion>();

        try {
            answer = this.dao.obtenerTopografosActualizacion(this.dao.
                obtenerActualizacionLevantamiento(450L, null));

            Assert.assertNotNull(answer);
            if (answer.size() > 0) {
                LOGGER.debug("Numero topografos: " + answer.size());
                LOGGER.debug("RECURSO HUNMANO" + answer.get(0).getRecursoHumano().getNombre());
            } else {
                LOGGER.debug("no arrojo resultados");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * Valida la busqueda da actualizacion con municipio.
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerActualizacionConMunicipio() {
        LOGGER.debug("ActualizacionDAOBeanTest#obtenerActualizacionConMunicipio...INICIA");
        try {
            Actualizacion unaActualizacion = this.dao.obtenerActualizacionConMunicipio(579L);
            Assert.assertNotNull(unaActualizacion);
            Assert.assertNotNull(unaActualizacion.getMunicipio());
            Assert.assertNotNull(unaActualizacion.getMunicipio().getDepartamento());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            LOGGER.debug("ActualizacionDAOBeanTest#obtenerActualizacionConMunicipio...INICIA");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testObtenerManzanasTopografosActualizacionId() {

        LOGGER.debug(
            "unit tests: ActualizacionDAOBeanTest#testObtenerManzanasTopografosActualizacionId");

        List<ActualizacionLevantamiento> answer = new ArrayList<ActualizacionLevantamiento>();

        try {
            answer = this.dao.obtenerManazanasporTopografosActualizacion(450L, 1L);

            Assert.assertNotNull(answer);
            if (answer.size() > 0) {
                LOGGER.debug("Numero topografos: " + answer.size());

            } else {
                LOGGER.debug("no arrojo resultados");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
