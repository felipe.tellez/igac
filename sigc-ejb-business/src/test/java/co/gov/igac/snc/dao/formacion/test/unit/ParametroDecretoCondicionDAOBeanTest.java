package co.gov.igac.snc.dao.formacion.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import co.gov.igac.snc.dao.formacion.IParametroDecretoCondicionDAO;
import co.gov.igac.snc.dao.formacion.impl.ParametroDecretoCondicionDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class ParametroDecretoCondicionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(
        ParametroDecretoCondicionDAOBeanTest.class);

    private IParametroDecretoCondicionDAO dao = new ParametroDecretoCondicionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -----------------------------//
}
