/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import static org.testng.Assert.*;
import co.gov.igac.snc.dao.tramite.IDuracionTramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.DuracionTramiteDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class DuracionTramiteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DuracionTramiteDAOBeanTests.class);

    private IDuracionTramiteDAO dao = new DuracionTramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetNormaDia() {

        LOGGER.debug("unit test: DuracionTramiteDAOBeanTests#testGetNormaDia ...");
        Integer answer;
        String tipoAvaluo, esSede;
        double area;

        try {
            tipoAvaluo = "00";
            esSede = "mamola";  // no debería importar este dato si es rural
            area = 2000000;
            answer = this.dao.getNormaDia(tipoAvaluo, esSede, area);
            LOGGER.debug("... passed rural.");
            LOGGER.debug("norma día = " + answer.intValue());

            tipoAvaluo = "01";
            esSede = "SI";
            area = -2000000; // no debería importar este dato si es rural
            answer = this.dao.getNormaDia(tipoAvaluo, esSede, area);
            LOGGER.debug("... passed urbana sede.");
            LOGGER.debug("norma día = " + answer.intValue());

            assertTrue(true);

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            assertFalse(true);
        }
    }

//end of class
}
