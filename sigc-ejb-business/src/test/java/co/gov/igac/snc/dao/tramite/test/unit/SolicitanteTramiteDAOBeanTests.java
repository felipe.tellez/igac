package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ISolicitanteTramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.SolicitanteTramiteDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.test.unit.BaseTest;

public class SolicitanteTramiteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitanteTramiteDAOBeanTests.class);

    private ISolicitanteTramiteDAO dao = new SolicitanteTramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author david.cifuentes
     */
    @Test
    public void testFindByTramiteId() {

        LOGGER.debug("tests: SolicitanteTramiteDAOBeanTests#testFindByTramiteId");
        Long idTramite = 96l;

        try {
            LOGGER.debug("*** Inicio Transaccion ****");
            List<SolicitanteTramite> solicitantes = dao.findByTramiteId(idTramite);
            Assert.assertNotNull(solicitantes);
            LOGGER.debug("**** Fin Transaccion ****");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }

    }

}
