package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionDAO;
import co.gov.igac.snc.dao.actualizacion.IActualizacionEventoDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionDAOBean;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionEventoDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author franz.gamba
 */
public class ActualizacionEventoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ActualizacionEventoDAOBeanTest.class);

    private IActualizacionEventoDAO dao = new ActualizacionEventoDAOBean();

    private IActualizacionDAO actDao = new ActualizacionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
        actDao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testActualizarActualizacionEvento() {
        try {
            LOGGER.debug("Prueba de persistencia sobre el objeto ActualizacionEvento");
            ActualizacionEvento evento = new ActualizacionEvento();

            //List<Actualizacion> actualizaciones = this.actDao.findAll();
            int i = this.actDao.countAll();
            Actualizacion actualizacion = this.actDao.findById(new Long(440));

            //evento.setId(new Long(1));
            evento.setActualizacion(actualizacion);
            evento.setTipo("Tipo");
            evento.setNombre("Nombre del evento");
            evento.setFecha(new Date());
            evento.setFechaLog(new Date());
            evento.setUsuarioLog("prusoacha007");

            evento = this.dao.update(evento);
            assertNotNull(evento);

            LOGGER.debug("RESULTADO " + evento.getId());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

}
