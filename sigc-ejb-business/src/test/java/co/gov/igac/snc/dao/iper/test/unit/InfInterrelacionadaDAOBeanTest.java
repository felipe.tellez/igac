package co.gov.igac.snc.dao.iper.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.generales.impl.BarrioDAOBean;
import co.gov.igac.snc.dao.generales.impl.ManzanaVeredaDAOBean;
import co.gov.igac.snc.dao.iper.impl.InfInterrelacionadaDAOBean;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionada;
import co.gov.igac.snc.test.unit.BaseTest;

public class InfInterrelacionadaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfInterrelacionada.class);

    private InfInterrelacionadaDAOBean dao = new InfInterrelacionadaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author fredy.wilches
     *
     */
    @Test
    public void getTramitesPrimeraPendientesTest() {
        List<InfInterrelacionada> mutaciones = new ArrayList<InfInterrelacionada>();
        try {
            LOGGER.debug("Entrando al test getTramitesPrimeraPendientes");
            mutaciones = this.dao.getTramitesPrimeraPendientes();
            LOGGER.debug("mutaciones :  " + mutaciones.size());
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
            assertTrue(false);
        }
    }

}
