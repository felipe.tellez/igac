package co.gov.igac.snc.dao.tramite.test.unit;

import co.gov.igac.snc.dao.tramite.ITramiteInconsistenciaDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteInconsistenciaDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class TramiteInconsistenciaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TramiteInconsistenciaDAOBeanTest.class);

    private ITramiteInconsistenciaDAO dao = new TramiteInconsistenciaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarPorIdTramite() {

        LOGGER.debug("on TramiteInconsistenciaDAOBeanTest#testBuscarPorIdTramite ...");

        try {
            List<TramiteInconsistencia> ti = dao.buscarPorIdTramite(23713l);

            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.size());
            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.get(0).getInconsistencia());

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

    @Test
    public void testBuscarDepuradaByTramiteId() {

        LOGGER.debug("testBuscarDepuradaByTramiteId");

        try {
            List<TramiteInconsistencia> inconsistencias = this.dao.buscarDepuradaByTramiteId(34604L,
                "NO");
            Assert.assertNotNull(inconsistencias);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();

        }

    }

    /**
     * @author felipe.cadena
     */
    /*
     * Modified andres eslava::25/01/2016::Prueba con nuevo formato de XML
     */
    @Test
    public void testGuardarInconsistenciasAsincronicas() {

        LOGGER.debug("testBuscarDepuradaByTramiteId");

        String XML =
            "<Response><Results><ValidacionPredio Predio=\"080010003000000001789000000000\">1,204</ValidacionPredio></Results><Results><ValidacionPredio Predio=\"080010003000000001791000000000\">1,204</ValidacionPredio></Results><Stats><Stat><Name>end</Name><Value>2016-01-22T07:02:18.102000</Value></Stat><Stat>		<Name>server</Name><Value>srcatgis3pro</Value></Stat><Stat><Name>elapsedTime</Name><Value>0:05:24.313000</Value></Stat><Stat><Name>start</Name><Value>2016-01-22T06:56:53.789000</Value></Stat><Stat>	<Name>jobId</Name><Value>78300a11</Value></Stat><Stat><Name>class</Name><Value>ToolValidarInconsistenciasAsync</Value></Stat></Stats><Exceptions/></Response>";

        Tramite t = new Tramite();
        t.setId(0l);
        t.setFuncionarioRadicador("pruabc");
        try {
            this.dao.guardarInconsistenciasAsincronicas(t, XML);
            Assert.assertNotNull(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();

        }

    }

}
