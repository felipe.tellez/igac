package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IUsoHomologadoDAO;
import co.gov.igac.snc.dao.conservacion.impl.UsoHomologadoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.UsoHomologado;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/*
 * Proyecto SNC 2017
 */
/**
 * Tests para los metodos de acceso a datos de la clase UsoHomologado
 *
 * @author felipe.cadena
 */
public class UsoHomologadoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsoHomologadoDAOBeanTests.class);

    private IUsoHomologadoDAO dao = new UsoHomologadoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author {autor}
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("UsoHomologadoDAOBeanTests#testUpdate");

        try {

            UsoHomologado testEntity = new UsoHomologado();

            testEntity.setCodigoOrigen("str::1psre6g4omkc47r885mi");
            testEntity.setCodigoDestino("str::iwrpw1ddziuwxzme7b9");
            testEntity.setEstado("str::90zomslpv7ietguzhncdi");
            testEntity.setUsuarioLog("str::8eyxjsylj0w5fe8tcsor");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
