package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.conservacion.IPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import java.util.TreeMap;

public class PredioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredioDAOBeanTest.class);

    private IPredioDAO dao = new PredioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testGetPredioByNumeroPredial() {
        try {
            LOGGER.debug("prueba de obtencion del predio apartir de su id");
            Predio predio;
            predio = dao.findById(Long.valueOf(7));
            LOGGER.debug("Número predial: " + predio.getNumeroPredial());
            assertNotNull(predio.getNumeroPredial());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testSearchForPredios() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testSearchForPredios ...");

        FiltroDatosConsultaPredio filtro = new FiltroDatosConsultaPredio();

        //filtro.setTerritorialId("6360");
        //N: adicionales para probar búsqueda avanzada
        //filtro.setAreaTerrenoMin(100);
        filtro.setDireccionPredio("K 2 30");
        //filtro.setNombrePropietario("pepito");

        List<Object[]> answer = null;

        try {
            answer = this.dao.searchForPredios(filtro, 0, -1);
            LOGGER.debug("... passed!. fetched " + answer.size() + " rows");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

    }

    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerPorRaizNumeroPredial() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testObtenerPorRaizNumeroPredial ...");

        String raizPredial = "0800101010000004";
        List<Predio> answer = null;

        try {
            answer = this.dao.obtenerPorRaizNumeroPredial(raizPredial);
            LOGGER.debug("... passed!. fetched " + answer.size() + " rows");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    //N: el método probado no se terminó de implementar porque está muy paila

    public void testSearchForPredios2() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testSearchForPredios ...");

//		FiltroDatosConsultaPredio filtro = new FiltroDatosConsultaPredio();
//		filtro.setTerritorialId("6360");
//
//        //N: adicionales para probar búsqueda avanzada
//        filtro.setAreaTerrenoMin(100);
//        filtro.setDireccionPredio("C");
//        filtro.setNombrePropietario("pepito");
//
//
//		List<Predio> answer = null;
//
//		try {
//			answer = this.dao.searchForPredios2(filtro, 0, -1);
//			LOGGER.debug("... passed!. fetched " + answer.size() + " rows");
//			assertTrue(true);
//		}
//        catch (Exception e) {
//			LOGGER.error("... error: " + e.getMessage());
//		}
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchPersonasByNumeroPredial() {
        LOGGER.debug("PredioDAOBeanTest#testGetPredioFetchPersonasByNumeroPredial");
        try {
            Predio answer = new Predio();
            answer = dao
                .getPredioFetchPersonasByNumeroPredial("257540216041711057933105117116");
            LOGGER.debug("*****************************************************" +
                " NO MÁS QUERIES A PARTIR DE ÉSTE PUNTO PARA ÉSTE MÉTODO");
            assertNotNull(answer);
            if (!answer.getPersonaPredios().isEmpty()) {
                assertNotNull(answer.getPersonaPredios());
            }
            if (answer.getPersonaPredios() != null &&
                !answer.getPersonaPredios().isEmpty()) {
                if (!answer.getPersonaPredios().get(0).getPersona()
                    .getPersonaBloqueos().isEmpty()) {
                    assertNotNull(answer.getPersonaPredios().get(0)
                        .getPersona().getPersonaBloqueos());
                }

                LOGGER.debug("Nombre persona: " +
                    answer.getPersonaPredios().get(0).getPersona()
                        .getNombre());

                LOGGER.debug("Id persona bloqueo: " +
                    answer.getPersonaPredios().get(0).getPersona()
                        .getPersonaBloqueos().get(0).getId());
            }
            LOGGER.debug("*****************************************************");
            LOGGER.
                debug("Salida método PredioDAOBeanTest#testGetPredioFetchPersonasByNumeroPredial");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchAvaluosByNumeroPredial() {
        LOGGER.debug("*************testGetPredioFetchAvaluosByNumeroPredial ");
        try {
            Predio answer = new Predio();
            answer = dao
                .getPredioFetchAvaluosByNumeroPredial("257540216041711057933105117116");
            LOGGER.debug("*****************************************************" +
                " NO MÁS QUERIES A PARTIR DE ÉSTE PUNTO PARA ÉSTE MÉTODO");
            assertNotNull(answer);
            assertNotNull(answer.getPredioAvaluoCatastrals());
            assertNotNull(answer.getPredioZonas());
            assertNotNull(answer.getUnidadConstruccions());
            if (!answer.getUnidadConstruccions().isEmpty()) {
                assertNotNull(answer.getUnidadConstruccions().get(0)
                    .getUsoConstruccion());
            }
            LOGGER.debug("*****************************************************");
            if (!answer.getPredioAvaluoCatastrals().isEmpty()) {
                LOGGER.debug("Valor total avalúo: " +
                    answer.getPredioAvaluoCatastrals().get(0)
                        .getValorTotalAvaluoCatastral());
            }
            if (!answer.getPredioZonas().isEmpty()) {
                LOGGER.debug("Área de la zona: " +
                    answer.getPredioZonas().get(0).getArea());
            }
            if (!answer.getUnidadConstruccions().isEmpty()) {
                LOGGER.debug("Uso Construcción: " +
                    answer.getUnidadConstruccions().get(0)
                        .getUsoConstruccion().getNombre());
            }
            LOGGER.debug("Salida método PredioDAOBeanTest#testGetPredioFetchAvaluosByNumeroPredial");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchDerechosPropiedadByNumeroPredial() {
        LOGGER.debug("*************testGetPredioFetchDerechosPropiedadByNumeroPredial ");
        try {
            Predio answer = new Predio();
            answer = dao
                .getPredioFetchDerechosPropiedadByNumeroPredial("257540216041711057933105117116");
            LOGGER.debug("*****************************************************" +
                " NO MÁS QUERIES A PARTIR DE ÉSTE PUNTO PARA ÉSTE MÉTODO");
            assertNotNull(answer);
            assertNotNull(answer.getPersonaPredios());
            if (!answer.getPersonaPredios().isEmpty()) {
                assertNotNull(answer.getPersonaPredios().get(0)
                    .getPersonaPredioPropiedads());
                assertTrue(answer.getPersonaPredios().get(0)
                    .getPersonaPredioPropiedads().size() > 0);
            }
            LOGGER.debug("*****************************************************");
            if (!answer.getPersonaPredios().isEmpty()) {
                LOGGER.debug("Persona Predio id: " +
                    answer.getPersonaPredios().get(0).getId());
                if (!answer.getPersonaPredios().get(0)
                    .getPersonaPredioPropiedads().isEmpty()) {
                    LOGGER.debug("Departamento Derecho Propiedad: " +
                        answer.getPersonaPredios().get(0)
                            .getPersonaPredioPropiedads().get(0)
                            .getDepartamento().getNombre());
                }
            }
            LOGGER.debug(
                "Salida método PredioDAOBeanTest#testGetPredioFetchDerechosPropiedadByNumeroPredial");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchDatosUbicacionByNumeroPredial() {
        LOGGER.debug("*************testGetPredioFetchDatosUbicacionByNumeroPredial ");
        try {
            Predio answer = new Predio();
            answer = dao
                .getPredioFetchDatosUbicacionByNumeroPredial("257540216041711057933105117116");
            LOGGER.debug("*****************************************************" +
                " NO MÁS QUERIES A PARTIR DE ÉSTE PUNTO PARA ÉSTE MÉTODO");

            assertNotNull(answer);
            if (!answer.getPredioDireccions().isEmpty()) {
                assertNotNull(answer.getPredioDireccions().get(0));
            }

            LOGGER.debug("*****************************************************");

            if (!answer.getPredioDireccions().isEmpty()) {
                LOGGER.debug("Dirección: " +
                    answer.getPredioDireccions().get(0).getDireccion());
            }

            LOGGER.debug("Departamento: " +
                answer.getDepartamento().getNombre());

            LOGGER.debug(
                "Salida método PredioDAOBeanTest#testGetPredioFetchDatosUbicacionByNumeroPredial");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetPredioDatosFetchPersonasByNumeroPredial() {
        LOGGER.debug("*************testGetDatosPredioFetchPersonasByNumeroPredial ");
        try {
            Predio answer = new Predio();
            answer = dao
                .getPredioDatosFetchPersonasByNumeroPredial("257540216041711057933105117116");
            LOGGER.debug("*****************************************************" +
                " NO MAS QUERIES A PARTIR DE ESTE PUNTO");
            assertNotNull(answer);

            if (!answer.getPersonaPredios().get(0).getPersona().getNombre()
                .isEmpty()) {
                LOGGER.debug("Nombre persona: " +
                    answer.getPersonaPredios().get(0).getPersona()
                        .getNombre());
            }

            if (!answer.getDepartamento().getNombre().isEmpty()) {
                LOGGER.debug("Departamento: " +
                    answer.getDepartamento().getNombre());
                LOGGER.debug("Municipio: " + answer.getMunicipio().getNombre());
            }
            LOGGER.debug(
                "Salida método PredioDAOBeanTest#testGetPredioDatosFetchPersonasByNumeroPredial");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

//-------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testSearchForPrediosCount() {
        LOGGER.debug("unit testing PredioDAOBeanTest#testSearchForPrediosCount...");

        FiltroDatosConsultaPredio datos;
        int nofPredios;

        datos = new FiltroDatosConsultaPredio();
        datos.setTerritorialId("6360");
        datos.setDepartamentoId("25");
        datos.setMunicipioId("25754");
        datos.setAreaTerrenoMin(1000);
        //datos.setDestinosPredioIds(new String[]);

        try {
            nofPredios = this.dao.searchForPrediosCount(datos);
            LOGGER.debug("passed. num resultado = " + nofPredios);
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindByNumeroPredial() {
        LOGGER.debug("unit testing PredioDAOBeanTest#testFindByNumeroPredial...");

        Predio p;
        String numeroPredial = "257540216041711057933105117117";
        try {

            p = this.dao.findByNumeroPredial(numeroPredial);
            LOGGER.debug("predio con num predial " + numeroPredial +
                " tiene id = " + p.getId() + " y depto = " +
                p.getDepartamento().getNombre());

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindPorNumeroPredial() {
        LOGGER.debug("unit testing PredioDAOBeanTest#testFindPorNumeroPredial...");

        Predio p;
        String numeroPredial = "257540103000000190023000000000";
        try {

            p = this.dao.getPredioPorNumeroPredial(numeroPredial);
            LOGGER.debug("predio con num predial " + numeroPredial +
                " tiene id = " + p.getId() + " y depto = " +
                p.getDepartamento().getNombre());

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testGetPrediosPorCodigo() {

        String codigo = "25754010200001165";
        List<Predio> p;
        try {

            p = this.dao.getPrediosPorCodigo(codigo);
            LOGGER.debug("************************* NO QUERIES");
            LOGGER.debug("la lista mide :  " + p.size());
            LOGGER.debug("codigo :  " + codigo);
            LOGGER.debug("numero predial :  " + p.get(0).getNumeroPredial());
            LOGGER.debug("departamento :  " +
                p.get(0).getDepartamento().getNombre());

            LOGGER.debug("numero predial :  " + p.get(1).getNumeroPredial());
            LOGGER.debug("numero predial :  " + p.get(2).getNumeroPredial());
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testGetPrediosPorMunicipioYMatricula() {

        String municipioCod = "25754";
        String matriculaInm = "050-40153055";
        List<Predio> p;
        try {

            p = this.dao.getPrediosPorMunicipioYMatricula(municipioCod, matriculaInm);
            LOGGER.debug("************************* NO QUERIES");
            LOGGER.debug("la lista mide :  " + p.size());
            //LOGGER.debug("codigo :  " + codigo);
            LOGGER.debug("numero predial :  " + p.get(0).getNumeroPredial());
            LOGGER.debug("departamento :  " +
                p.get(0).getDepartamento().getNombre());

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }
    // --------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     */
    @Test
    public void testGetPrediosPorMunicipioYDireccion() {
        try {
            String municipioCod = "25754";
            String direccion = "carrera 2 numero 30-25 Interior 11 Apartamento 103";
            List<Predio> p = this.dao.getPrediosPorMunicipioYDireccion(municipioCod, direccion);
            assertNotNull(p);
            assertEquals(p.size(), 1);
            LOGGER.debug("la lista mide :  " + p.size());
            //LOGGER.debug("codigo :  " + codigo);
            LOGGER.debug("numero predial :  " + p.get(0).getNumeroPredial());
            LOGGER.debug("departamento :  " + p.get(0).getDepartamento().getNombre());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testExistePredioPorNumeroPredial() {
        LOGGER.debug("testGetPrediosPorNumerosPrediales");
        try {
            String numerosPrediales = "080010109000003960017000000000";
            boolean resultado = dao.existePredioPorNumeroPredial(numerosPrediales);
            assertTrue(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testExistePredioPorNumeroPredialError() {
        LOGGER.debug("testExistePredioPorNumeroPredialError");
        try {
            String numerosPrediales = "088810109000003960017000000000";
            boolean resultado = dao.existePredioPorNumeroPredial(numerosPrediales);
            assertFalse(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testGetPrediosPorNumerosPrediales() {
        LOGGER.debug("testGetPrediosPorNumerosPrediales");
        String numerosPrediales = "080010109000003960017000000000," +
            "080010109000003960015000000000," +
            "080010109000003960020000000000," +
            "080010109000003910011000000000," +
            "080010109000003910016000000000," +
            "080010109000003910008000000000," +
            "080010109000003910005000000000," +
            "080010109000003910019000000000," +
            "080010109000003910002000000000," +
            "080010109000003960022000000000," +
            "080010109000003960005000000000," +
            "080010109000004420047000000000," +
            "080010109000003960013000000000," +
            "080010109000003960014000000000," +
            "080010109000003960002000000000," +
            "080010109000003960016000000000," +
            "080010109000003910012000000000," +
            "080010109000004420044000000000," +
            "080010109000003910020000000000," +
            "080010109000003960007000000000," +
            "080010109000003960008000000000," +
            "080010109000003960012000000000," +
            "080010109000003960010000000000," +
            "080010109000004420002000000000," +
            "080010109000003910009000000000," +
            "080010109000003960018000000000," +
            "080010109000003910017000000000," +
            "080010109000004420001000000000," +
            "080010109000003960023000000000," +
            "080010109000003960019000000000," +
            "080010109000003910013000000000," +
            "080010109000003910015000000000," +
            "080010109000003910018000000000," +
            "080010109000003960003000000000," +
            "080010109000004420003000000000," +
            "080010109000003910010000000000," +
            "080010109000004420045000000000," +
            "080010109000003910014000000000," +
            "080010109000003960011000000000," +
            "080010109000003910007000000000," +
            "080010109000003910006000000000," +
            "080010109000003960006000000000," +
            "080010109000003960009000000000";

        try {
            String[] np = numerosPrediales.split(",");
            List<Predio> p = this.dao.getPrediosPorNumerosPrediales(numerosPrediales);
            assertNotNull(p);
            assertTrue(p.size() < np.length);
            LOGGER.debug("la lista original mide :  " + np.length);
            LOGGER.debug("la lista mide :  " + p.size());
            LOGGER.debug("numeros :  " + numerosPrediales);
            LOGGER.debug("area :  " + p.get(0).getAreaTotalTerreno());
            LOGGER.debug("area :  " + p.get(1).getAreaTotalTerreno());
            LOGGER.debug("area :  " + p.get(2).getAreaTotalTerreno());
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     *
     */
    @Test
    public void testFindAllReturnSet() {

        LOGGER.debug("PredioDAOBeanTest#testFindAllReturnSet");
        //this.dao.findAllReturnSet();
        int i = 0;
        for (Predio p : this.dao.findAllReturnSet()) {
            LOGGER.debug("****ID PREDIO:" + p.getId());
            i++;
            if (i == 10) {
                break;
            }
        }
    }

    /**
     * Prueba carga aleatoria de Predios
     */
    /*
     * //@Test @Test(threadPoolSize = 1, invocationCount = 10, timeOut = 10000) public void
     * testObtenerPredioAleatorio() { LOGGER.debug("unit test:
     * *PredioDAOBeanTest#testObtenerPredioAleatorio "); try { int totalRegistros =
     * this.dao.countAll(); LOGGER.debug("totalRegistros:"+totalRegistros); Predio predio =
     * dao.loadRandomObject(); assertNotNull(predio); } catch (Exception e) {
     * LOGGER.error(e.getMessage(), e); fail(e.getMessage(), e); } }
     */
    //@Test
    @Test(threadPoolSize = 2, invocationCount = 10, timeOut = 15000)
    public void testObtenerPredioAleatorioPorCodigo() {
        LOGGER.debug("***************************************************************** ");
        LOGGER.debug("unit test: *PredioDAOBeanTest#testObtenerPredioAleatorioPorCodigo ");
        try {
            String code = "08001";
            LOGGER.debug("code:" + code);
            Predio predio = dao.loadRandomObject(code);
            assertNotNull(predio);
            LOGGER.debug("Predio id:" + predio.getId() + " - Número Predial:" + predio.
                getNumeroPredial());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo() {
        LOGGER.debug(
            "tests: PredioDAOBeanTest#testBuscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo");

        FiltroDatosConsultaPrediosBloqueo filtro = crearFiltroPredioBloqueo();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.dao.buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
                filtro);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer.size());

                if (!answer.isEmpty()) {

                    for (Predio pb : answer) {

                        LOGGER.debug("Id: " + pb.getId());
                        LOGGER.debug("NP: " + pb.getNumeroPredial());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que crea un FiltroDatosConsultaPrediosBloqueo con los datos base
     *
     * @author juan.agudelo
     */
    private FiltroDatosConsultaPrediosBloqueo crearFiltroPredioBloqueo() {

        FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtro.setDepartamento(departamento);
        filtro.setMunicipio(municipio);
        filtro.setDepartamentoCodigo("25");
        filtro.setMunicipioCodigo("754");
        filtro.setTipoAvaluo("02");
        filtro.setSectorCodigo("16");
        filtro.setComunaCodigo("04");
        filtro.setBarrioCodigo("17");
        filtro.setManzanaVeredaCodigo("1105");
        filtro.setPredio("7934");
        filtro.setCondicionPropiedad("1");
        filtro.setEdificio("05");
        filtro.setPiso("11");
        filtro.setUnidad("7118");

        return filtro;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo() {
        LOGGER.debug(
            "tests: PredioDAOBeanTest#testBuscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo");

        FiltroDatosConsultaPrediosBloqueo filtroInicial = crearBaseHastaManzanaFiltroPredioBloqueo();
        FiltroDatosConsultaPrediosBloqueo filtroFinal = crearBaseHastaManzanaFiltroPredioBloqueo();

        filtroInicial.setPredio("0000");
        filtroFinal.setPredio("9999");
        filtroInicial.setCondicionPropiedad("0");
        filtroFinal.setCondicionPropiedad("9");
        filtroInicial.setEdificio("00");
        filtroFinal.setEdificio("99");
        filtroInicial.setPiso("00");
        filtroFinal.setPiso("99");
        filtroInicial.setUnidad("0000");
        filtroFinal.setUnidad("9999");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.dao
                .buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
                    filtroInicial, filtroFinal);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer.size());

                if (!answer.isEmpty()) {

                    for (Predio pb : answer) {

                        LOGGER.debug("Id: " + pb.getId());
                        LOGGER.debug("NP: " + pb.getNumeroPredial());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que crea la base hasta manzana de un FiltroDatosConsultaPrediosBloqueo
     *
     * @author juan.agudelo
     */
    private FiltroDatosConsultaPrediosBloqueo crearBaseHastaManzanaFiltroPredioBloqueo() {

        FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtro.setDepartamento(departamento);
        filtro.setMunicipio(municipio);
        filtro.setDepartamentoCodigo("25");
        filtro.setMunicipioCodigo("754");
        filtro.setTipoAvaluo("02");
        filtro.setSectorCodigo("16");
        // TODO no crea comuna código pq hasta el momento nos e ha implementado
        // en la entidad, los dos codigos van en barrio código
        // filtro.setComunaCodigo("04");
        filtro.setBarrioCodigo("0417");
        filtro.setManzanaVeredaCodigo("1105");

        return filtro;
    }

    // -------------------------------------//
    /**
     * Método que cuenta los predios nacionales
     *
     * @author david.cifuentes
     */
    @Test
    public void testContarPrediosNacionales() {
        try {
            LOGGER.debug("PredioDAOBeanTest#testContarPrediosNacionales");
            Long conteo = dao.contarPrediosNacionales();
            LOGGER.debug("Conteo de predios nacionales: " + conteo);
            assertNotNull(conteo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetPredioFetchDatosUbicacionById() {

        LOGGER.debug("unit test PredioDAOBeanTest#testGetPredioFetchDatosUbicacionById ...");
        Long idPredio = 157454l;
        Predio answer;

        answer = this.dao.getPredioFetchDatosUbicacionById(idPredio);
        if (answer != null) {
            Assert.assertNotNull(answer.getDireccionPrincipal());
            LOGGER.debug("passed!");
            LOGGER.debug("tiene " + answer.getPredioDireccions().size() + " direcciones");
            if (answer.getCirculoRegistral() != null) {
                LOGGER.debug("círculo registral: " + answer.getCirculoRegistral().getNombre());
            }
            LOGGER.debug("tiene " + answer.getPredioBloqueos().size() + " bloqueos");
            if (!answer.getPredioBloqueos().isEmpty()) {
                if (answer.isEstaBloqueado()) {
                    LOGGER.debug("... y está bloquedo");
                }
            }

        }

    }

    /**
     * @author fredy.wilches
     */
    //@Test
    @Test(threadPoolSize = 1, invocationCount = 2, timeOut = 350000)
    public void testBuscarPredioLibreTramite() {
        LOGGER.debug("unit test PredioDAOBeanTest#testBuscarPredioLibreTramite...");
        String codigoMunicipio = "08001";
        Integer codigoEstructuraOrganizacional = 6040;
        Predio answer = this.dao.buscarPredioLibreTramite(codigoEstructuraOrganizacional,
            codigoMunicipio);
        Assert.assertNotNull(answer.getNumeroPredial());
        LOGGER.debug("Numero Predial:  " + answer.getNumeroPredial());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de predios por números prediales
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetPrediosByNumerosPrediales() {
        LOGGER.debug("tests: PredioDAOBeanTest#testGetPrediosByNumerosPrediales");

        List<String> numerosPrediales = new ArrayList<String>();
        String numeroPredial;

        numeroPredial = new String("080010101000000280012000000000");
        numerosPrediales.add(numeroPredial);
        numeroPredial = new String("080010101000000280013000000000");
        numerosPrediales.add(numeroPredial);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.dao
                .getPrediosByNumerosPrediales(numerosPrediales);

            if (answer != null && !answer.isEmpty()) {

                for (Predio temp : answer) {
                    LOGGER.debug("Predio id: " + temp.getId());
                    LOGGER.debug("Número predial: " + temp.getNumeroPredial());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Prueba para verificar si un predio se encuentra cancelado
     *
     * @author juan.agudelo
     */
    @Test
    public void testIsPredioCanceladoByPredioIdOrNumeroPredial() {
        LOGGER.debug("tests: PredioDAOBeanTest#testIsPredioCanceladoByPredioIdOrNumeroPredial");

        String numeroPredial = null;
        Long predioId = null;

        numeroPredial = "257540102000011650001500000032";
        // predioId = 7L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            boolean answer = this.dao
                .isPredioCanceladoByPredioIdOrNumeroPredial(predioId,
                    numeroPredial);

            String cancelado = answer == true ? ESiNo.SI.getCodigo() : ESiNo.NO
                .getCodigo();
            LOGGER.debug("Predio cancelado: " + cancelado);

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetTerrenoNumbersFromManzanaAndPropertyCondition() {
        LOGGER.debug(
            "unit test: PredioDAOBeanTest#testGetTerrenoNumbersFromManzanaAndPropertyCondition ...");

        String manzana = "1165";
        ArrayList<String> answer, condicionesPropiedad = new ArrayList<String>();

        condicionesPropiedad.add("5");
        condicionesPropiedad.add("6");

        try {
            answer = (ArrayList<String>) this.dao.getTerrenoNumbersFromManzanaAndPropertyCondition(
                manzana, condicionesPropiedad);

            //D: solo es necesario que no se totee en la consulta para que pase el test
            assertTrue(true);

            if (answer != null && !answer.isEmpty()) {
                for (String terreno : answer) {
                    LOGGER.debug("terreno: " + terreno);
                }
            }
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetEdificioTorreNumbersFromTerreno() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testGetEdificioTorreNumbersFromTerreno ...");

        String terreno = "0800101090000039200060";
        ArrayList<String> answer;

        try {
            answer = (ArrayList<String>) this.dao.getEdificioTorreNumbersFromTerreno(terreno);

            //D: solo es necesario que no se totee en la consulta para que pase el test
            assertTrue(true);

            if (answer != null && !answer.isEmpty()) {
                for (String edificio : answer) {
                    LOGGER.debug("edificio: " + edificio);
                }
            }
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testExistsPredio() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testExistsPredio ...");

        String numeroPredial = "080010109000003920006000000000";
        String[] estados;
        boolean existe;

//		try {
//			existe = this.dao.existsPredio(numeroPredial);
//
//            //D: solo es necesario que no se totee en la consulta para que pase el test
//            assertTrue(true);
//            LOGGER.debug("existe? = "  + existe);
//		}
//        catch (Exception e) {
//            fail("error: ");
//			LOGGER.error(e.getMessage(), e);
//		}
        try {
            estados = new String[1];
            estados[0] = "ACTIVO";
            existe = this.dao.existsPredio(numeroPredial, estados);

            //D: solo es necesario que no se totee en la consulta para que pase el test
            assertTrue(true);
            LOGGER.debug("existe activo? = " + existe);
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testBuscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueoDos() {
        LOGGER.debug(
            "tests: PredioDAOBeanTest#testBuscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueoDos");

        FiltroDatosConsultaPrediosBloqueo filtroInicial = new FiltroDatosConsultaPrediosBloqueo();
        FiltroDatosConsultaPrediosBloqueo filtroFinal = new FiltroDatosConsultaPrediosBloqueo();

        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtroInicial.setDepartamento(departamento);
        filtroInicial.setMunicipio(municipio);
        filtroInicial.setDepartamentoCodigo("25");
        filtroInicial.setMunicipioCodigo("754");
        filtroInicial.setTipoAvaluo("00");
        filtroInicial.setSectorCodigo("00");
        // TODO no crea comuna código pq hasta el momento nos e ha implementado
        // en la entidad, los dos codigos van en barrio código
        // filtro.setComunaCodigo("04");
        filtroInicial.setComunaCodigo("00");
        filtroInicial.setBarrioCodigo("00");
        filtroInicial.setManzanaVeredaCodigo("0001");

        filtroFinal.setDepartamento(departamento);
        filtroFinal.setMunicipio(municipio);
        filtroFinal.setDepartamentoCodigo("25");
        filtroFinal.setMunicipioCodigo("754");
        filtroFinal.setTipoAvaluo("00");
        filtroFinal.setSectorCodigo("00");
        // TODO no crea comuna código pq hasta el momento nos e ha implementado
        // en la entidad, los dos codigos van en barrio código
        // filtro.setComunaCodigo("04");

        filtroFinal.setBarrioCodigo("00");
        filtroFinal.setComunaCodigo("00");
        filtroFinal.setManzanaVeredaCodigo("0001");

//		filtroInicial.setPredio("0001");
//		filtroFinal.setPredio("9999");
//		filtroInicial.setCondicionPropiedad("0");
//		filtroFinal.setCondicionPropiedad("9");
//		filtroInicial.setEdificio("00");
//		filtroFinal.setEdificio("99");
//		filtroInicial.setPiso("00");
//		filtroFinal.setPiso("99");
//		filtroInicial.setUnidad("0000");
//		filtroFinal.setUnidad("9999");
//		LOGGER.debug("PROBANDO cuatro...");
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.dao
                .buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
                    filtroInicial, filtroFinal);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer.size());

                if (!answer.isEmpty()) {

                    for (Predio pb : answer) {

                        LOGGER.debug("Id: " + pb.getId());
                        LOGGER.debug("NP: " + pb.getNumeroPredial());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testObtenerPrediosPorNumeroManzana() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testObtenerPrediosPorNumeroManzana ...");

        //String manzanaBuscar = "08001010100000002";
        //List<Predio> answer= new ArrayList<Predio>();
        Predio answer = new Predio();

        try {
            //answer = this.dao.obtenerPrediosPorNumeroManzana(manzanaBuscar);
            answer = this.dao.findByNumeroPredial("080010101000000640397900000000");
            //D: solo es necesario que no se totee en la consulta para que pase el test
            assertTrue(true);

            //if(answer != null && !answer.isEmpty()) {
            //for (String edificio : answer) {
            LOGGER.debug("Unidades Construccion: " + answer.getUnidadConstruccions().size());
            LOGGER.debug("Zonas Homogeneas: " + answer.getPredioZonas().size());
            // }
            //}
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testDatosFisicosByNumeroPredial() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testDatosFisicosByNumeroPredial ...");

        //String manzanaBuscar = "08001010100000002";
        //List<Object[]> answer=new ArrayList<Object[]>();
        List<Object[]> numeroPredialResultados = new ArrayList<Object[]>();
        List<String> numeroPredial = new ArrayList<String>();

        numeroPredial.add("080010103000005510054900000000");
        //Predio answer = new Predio();

        try {
            //answer = this.dao.obtenerPrediosPorNumeroManzana(manzanaBuscar);
            numeroPredialResultados = this.dao.getDatosFisicosByNumeroPredial(numeroPredial, 0, 10);
            //D: solo es necesario que no se totee en la consulta para que pase el test
            //assertTrue(true);

            //if(answer != null && !answer.isEmpty()) {
            //for (String edificio : answer) {
            LOGGER.debug("resultados: " + numeroPredialResultados.size());
            //LOGGER.debug("resultados: "  + numeroPredialResultados.get(0).getFichaMatrizs().get(0).getFichaMatrizPredios().get(0).getCoeficiente());
            // LOGGER.debug("resultados: "  + numeroPredialResultados.get(0).getPredioDireccions().get(0).getDireccion());
            //LOGGER.debug("resultados Ficha Matriz: "  + numeroPredialResultados.);
            //LOGGER.debug("resultados Direccion: "  + numeroPredialResultados.size());

            // }
            //}
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testFichaMatriz() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testFichaMatriz ...");

        List<Object[]> numeroPredialResultados = new ArrayList<Object[]>();
        List<String> numeroPredial = new ArrayList<String>();

        numeroPredial.add("080010103000005510054900000000");
        try {

            numeroPredialResultados = this.dao.getReporteFichaMatriz(numeroPredial, 0, 10);

            //D: solo es necesario que no se totee en la consulta para que pase el test
            LOGGER.debug("resultados: " + numeroPredialResultados.size());
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void getBuscaPredioGenereacionFormularioSBC() {
        LOGGER.debug("unit test: PredioDAOBeanTest#getBuscaPredioGenereacionFormularioSBC ...");

        List<Predio> numeroPredialResultados = new ArrayList<Predio>();

        try {

            numeroPredialResultados = this.dao.getBuscaPredioGenereacionFormularioSBC(
                "0800101030000000");

            //D: solo es necesario que no se totee en la consulta para que pase el test
            LOGGER.debug("resultados: " + numeroPredialResultados.size());
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    public void testGetPredioByNumeroPredial1() {
        try {
            LOGGER.debug("prueba de obtencion del predio apartir de su id");
            Predio predio;
            predio = dao.getPredioByNumeroPredial("470010107000000210003000000000");
            LOGGER.debug("Número predial: " + predio.getNumeroPredial());
            assertNotNull(predio.getNumeroPredial());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertTrue(false);
        }
    }

    /*
     * author andres.eslava
     */
    @Test
    public void testObtenerPredioCompletoPorNumeroPrediall() {
        LOGGER.debug("");
        try {
            Predio predio;
            predio = dao.obtenerPredioCompletoPorNumeroPredial("470010107000000210003000000000");
            assertNotNull(predio);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetPredioFetchAvaluosByPredioId() {

        LOGGER.debug("unit test PredioDAOBeanTest#testGetPredioFetchAvaluosByPredioId ");

        Long idPredio;
        Predio answer;

        idPredio = new Long("3765");

        try {
            answer = this.dao.getPredioFetchAvaluosByPredioId(idPredio);

            LOGGER.debug("passed.");

            if (answer.getPredioZonas() != null && !answer.getPredioZonas().isEmpty()) {
                LOGGER.debug("vigencia más actual de zonas: " + answer.getPredioZonas().get(0).
                    getVigencia());
            }

        } catch (Exception ex) {
            fail("FAILED. cause: " + ex.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetDetalleAvaluosById() {

        LOGGER.debug("unit test PredioDAOBeanTest#testGetDetalleAvaluosById ");

        Long idPredio;
        Predio answer;

        idPredio = new Long("3765");

        try {
            answer = this.dao.getDetalleAvaluosById(idPredio);

            LOGGER.debug("passed.");

            if (answer.getPredioZonas() != null && !answer.getPredioZonas().isEmpty() &&
                answer.getPredioZonas().size() == 1) {
                LOGGER.debug("vigencia más actual de zonas: " + answer.getPredioZonas().get(0).
                    getVigencia());
            } else {
                LOGGER.debug("error: trajo más de una vigencia");
            }

        } catch (Exception ex) {
            fail("FAILED. cause: " + ex.getMessage());
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testCalcularSumaAvaluosTotalesDePrediosFichaMatriz() {

        LOGGER.debug(
            "unit test PredioDAOBeanTest#testCalcularSumaAvaluosTotalesDePrediosFichaMatriz ");

        Long idFicha;
        double[] answer;

        idFicha = new Long("1035");

        try {
            answer = this.dao.calcularSumaAvaluosTotalesDePrediosFichaMatriz(idFicha);

            LOGGER.debug("passed.");

            if (answer != null) {
                LOGGER.debug("total = " + answer);
            } else {
                LOGGER.debug("error al sumar avaluos");
            }
        } catch (Exception ex) {
            fail("FAILED. cause: " + ex.getMessage());
        }
    }

    @Test
    public void testObtenerPrediosCanceladosQuintaCondominioPH() {
        LOGGER.debug(
            "unit test: PredioDAOBeanTest#testObtenerPrediosCanceladosQuintaCondominioPH ...");

        String numeroPredialFichaMatriz = "080010101000002690903900000023";
        List<Predio> answer;

        try {
            answer = this.dao.obtenerPrediosCanceladosQuintaCondominioPH(numeroPredialFichaMatriz);
            LOGGER.debug("... passed!. fetched " + answer.size() + " rows");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testGetNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal() {
        LOGGER.debug(
            "unit test: PredioDAOBeanTest#testGetNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal ...");

        String numeroPredialInicial = "080010101000000010000000000000";
        String numeroPredialFinal = "080010101000000019999999999999";
        List<String> answer;

        try {
            answer = this.dao.
                getNumerosPredialesEntreNumeroPredialInicialYNumeroPredialFinal(numeroPredialInicial,
                    numeroPredialFinal, false);
            LOGGER.debug("... passed!. fetched " + answer.size() + " rows");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testValidarExistenciaYActivos() {
        LOGGER.debug("unit test: PredioDAOBeanTest#testValidarExistenciaYActivos ...");

        List<String> predios = new ArrayList<String>();
        predios.add("080010101000000010000000000000");
        predios.add("080010101000000010000000000000");
        predios.add("080010101000000010000000000000");
        predios.add("080010101000000010000000000000");
        predios.add("080010101000000010008000000000");
        predios.add("080010101000000010004000000000");
        predios.add("080010101000000010002000000000");
        predios.add("080010101000000010009000000000");

        try {
            boolean answer = this.dao.validarExistenciaYActivos(predios);
            LOGGER.debug("... passed!. fetched " + answer + " rows");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage());
        }
    }

    /**
     * Prueba predios mejoras
     *
     * @author felipe.cadena
     */
    @Test
    public void testObtenerMejorasPredio() {

        LOGGER.debug("unit test PPredioDAOBeanTest#testObtenerMejorasPredio");
        try {
            String nPredial = "080010002000000000137000000000";
            List<Predio> answer = this.dao.obtenerMejorasPredio(nPredial);
            LOGGER.debug(answer.toString());
            org.testng.Assert.assertNotNull(answer);
        } catch (Exception ex) {
            org.testng.Assert.fail();
            LOGGER.error(ex.getMessage());
        }
    }

    /**
     * Prueba consulta matriculas por municipio
     *
     * @author felipe.cadena
     */
    @Test
    public void testValidarExistenciaMatriculasPorMunicipio() {

        LOGGER.debug("unit test PPredioDAOBeanTest#testValidarExistenciaMatriculasPorMunicipio");
        try {
            String municipio = "08001";
            List<String> matriculas = new ArrayList<String>();
            for (int i = 0; i < 1500; i++) {
                matriculas.add("1234");
            }

            List<Predio> answer = this.dao.validarExistenciaMatriculasPorMunicipio(matriculas,
                municipio);
            org.testng.Assert.assertNotNull(answer);
            LOGGER.debug(String.valueOf(answer.size()));

        } catch (Exception ex) {
            org.testng.Assert.fail();
            LOGGER.error(ex.getMessage());
        }
    }

}
