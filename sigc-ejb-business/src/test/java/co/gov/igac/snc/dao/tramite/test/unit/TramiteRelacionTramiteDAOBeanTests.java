package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.ITramiteRelacionTramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteRelacionTramiteDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRelacionTramite;
import co.gov.igac.snc.test.unit.BaseTest;

public class TramiteRelacionTramiteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteRelacionTramiteDAOBeanTests.class);

    private ITramiteRelacionTramiteDAO dao = new TramiteRelacionTramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     */
    @Test
    public void testFindTramiteRelacionTramiteByIdTramite() {
        LOGGER.debug(
            "unit test: TramiteRelacionTramiteDAOBeanTests#testFindTramiteRelacionTramiteByIdTramite");

        List<TramiteRelacionTramite> tramitesRelacionTramite;
        Long tramiteId = 2019l;

        try {
            tramitesRelacionTramite = this.dao.findTramiteRelacionTramiteByIdTramite(tramiteId);
            Assert.assertNotNull(tramitesRelacionTramite);
            LOGGER.debug("Success");
            LOGGER.debug("El tamaño de la lista es: " +
                tramitesRelacionTramite.size());
        } catch (Exception e) {
            LOGGER.error(
                "Error on unit test: TramiteRelacionTramiteDAOBeanTests#testFindTramiteRelacionTramiteByIdTramite: " +
                e.getMessage());
            Assert.assertFalse(true);
        }
    }
}
