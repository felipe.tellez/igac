package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPredioDireccionDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPredioDAOBean;
import co.gov.igac.snc.dao.conservacion.impl.PPredioDireccionDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.test.unit.BaseTest;

public class PPredioDireccionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioDAOBean.class);

    private IPPredioDireccionDAO dao = new PPredioDireccionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void findPPredioDireccionByPredioIdTest() {
        try {
            List<PPredioDireccion> direcciones = this.dao.getPPredioDireccionesByPredioId(725015L);
            Assert.assertNotNull(direcciones);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void obtenerDireccionPrincipalFichaMatrizTest() {
        try {

            String numeroPredial = "0800101030000014800349";
            PPredioDireccion direcciones = this.dao.obtenerDireccionPrincipalFichaMatriz(
                numeroPredial);
            Assert.assertNotNull(direcciones);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }
}
