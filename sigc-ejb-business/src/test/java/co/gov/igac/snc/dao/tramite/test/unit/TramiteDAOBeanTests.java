/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaCancelarTramites;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.FiltroDatosConsultaTramites;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.TramiteConEjecutorAsignadoDTO;
import java.util.Calendar;

public class TramiteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDAOBeanTests.class);

    private ITramiteDAO dao = new TramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetTramitesByNumeroPredialPredio() {
        try {
            List<Tramite> tramites;
            tramites = dao.getTramitesByNumeroPredialPredio("081370100000002780003000000000");

            LOGGER.debug("trajo " + tramites.size() + " filas");

            assertNotNull(tramites);
            assertTrue(tramites.size() >= 1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testGetTramitesById() {
        try {
            Tramite tramite = dao.findById(326029L);
            assertNotNull(tramite);
            assertNotNull(tramite.getId());
            LOGGER.debug(tramite.getId() + " - " + tramite.getNumeroRadicacion());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testFindBySolicitud() {
        LOGGER.debug("TramiteDAOBeanTests#testFindBySolicitud");
        try {
            LOGGER.debug("***********************");
            Tramite tramites = dao.findById(42981L);
            LOGGER.debug("***********************" + tramites);
            if (tramites != null) {
                // LOGGER.debug(t.getPredio().getNumeroPredial());
                // LOGGER.debug(t.getPredio().getDepartamento().getNombre());
                // LOGGER.debug(t.getPredio().getMunicipio().getNombre());
                LOGGER.debug(tramites.getTipoTramiteCadenaCompleto());
            }
            assertNotNull(tramites);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     *
     * pre: hay datos de prueba en la tabla TRAMITE que corresponden a los usados en esta prueba
     */
    @Test
    public void testFindTramitesAsignacionDeEjecutor() {
        LOGGER.debug("unit tests: TramiteDAOBeanTests#testFindTramitesAsignacionDeEjecutor ...");

        List<Tramite> tramites;
        long[] idsTramites = {3l, 7l};

        try {
            tramites = this.dao.findTramitesParaAsignacionDeEjecutor(idsTramites, null, null, null);

            LOGGER.debug("... passed!");

            if (!tramites.isEmpty()) {
                LOGGER.debug("fetched " + tramites.size() + " rows");

                LOGGER.
                    debug("municipio = " + tramites.get(0).getPredio().getMunicipio().getNombre());
            } else {
                LOGGER.debug("fetched 0 rows");
            }
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     *
     * PRE: hay datos de prueba en la tabla TRAMITE que corresponden a los usados en esta prueba
     */
    @Test
    public void testFindTramitesPorComisionar() {
        LOGGER.debug("unit tests: TramiteDAOBeanTests#testFindTramitesPorComisionar ...");

        List<Tramite> answer;
        long[] idsTramites = {3l, 7l, 32774l};

        try {
            answer = this.dao.findTramitesPorComisionar(idsTramites, "", "ASCENDING",
                new HashMap<String, String>());

            Assert.assertTrue(true);
            LOGGER.debug("... passed. fetch " + answer.size() + " rows");

            if (answer.size() >= 1) {
                LOGGER.debug("num radic = " + answer.get(0).getNumeroRadicacion() + "\n" +
                    "departamento=" +
                    answer.get(0).getPredio().getDepartamento().getNombre());
            }
            LOGGER.debug("predios englobe del primero que tenga:");
            for (Tramite t : answer) {
                if (t.getTramitePredioEnglobes() != null && !t.getTramitePredioEnglobes().isEmpty()) {
                    for (TramitePredioEnglobe tpe : t.getTramitePredioEnglobes()) {
                        LOGGER.debug("predio de predio englobe:numero predial = " + tpe.getPredio().
                            getNumeroPredial());
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarTramiteTramitesPorTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramiteTramitesPorNumeroRadicacion");

        Long tramiteId = 347L;
        Tramite tramiteT = new Tramite();
        Tramite tramite = new Tramite();
        LOGGER.debug("************************************************");
        try {
            tramiteT = dao.buscarTramitePorTramiteIdProyeccion(tramiteId);

            LOGGER.debug("Tramite tramite numero radicacion: " + tramiteT.getNumeroRadicacion());
            LOGGER.debug("Tramite tramite documentos tamaño: " +
                tramiteT.getTramiteDocumentacions().size());
            if (tramite.getTramite() != null) {
                LOGGER.debug("Tramite tramite numero id: " + tramiteT.getTramite().getId());
            }

            if (tramiteT.getTramiteDocumentacions().size() != 0) {
                LOGGER.debug("Tramite tramite documentos: " +
                    tramiteT.getTramiteDocumentacions().get(0).getTipoDocumento().getNombre());
                if (tramiteT.getTramiteDocumentacions().get(0).getDepartamento() != null) {
                    LOGGER.debug("Tramite tramite documentos departamento: " +
                        tramiteT.getTramiteDocumentacions().get(0).getDepartamento().getNombre());
                }
                if (tramiteT.getTramiteDocumentacions().get(0).getMunicipio() != null) {
                    LOGGER.debug("Tramite tramite documentos municipio: " +
                        tramiteT.getTramiteDocumentacions().get(0).getMunicipio().getNombre());
                }
            }
            if (tramiteT.getPredio() != null) {
                LOGGER.debug("Tramite tramite predio numero predial: " + tramiteT.getPredio().
                    getNumeroPredial());
            }

            Assert.assertNotNull(tramiteT);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);

        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarTramitePredioEnglobe() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramitePredioEnglobe");
        String numeroRadicacion = "02-560-825-2009";
        Tramite tramite = dao.findTramiteByNoRadicacion(numeroRadicacion);
        List<TramiteDetallePredio> tpe;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tpe = dao.buscarPredioEnglobesPorTramite(tramite.getId());
            LOGGER.debug("************************************************");
            for (int i = 0; i < tpe.size(); i++) {
                LOGGER.debug("Predios " + tpe.get(i).getPredio().getNumeroPredial());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * author franz.gamba
     *
     */
    @Test
    public void testExisteElTramite() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testExisteElTramite");

        Long tramiteId = 42592L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            boolean b = dao.tramiteExists(tramiteId);
            Assert.assertTrue(b);
            LOGGER.debug("************************************************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindByComisionId() {

        LOGGER.debug("unit tests: TramiteDAOBeanTests#testFindByComisionId");

        Long idComision = new Long("140");
        List<Tramite> answer;

        try {
            answer = this.dao.findByComisionId(idComision);
            LOGGER.debug("passed. found = " + answer.size());

            if (!answer.isEmpty()) {
                LOGGER.debug("data: " + answer.get(0).getPredio().getDepartamento().getNombre());
            }

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba funcionario no valido
     *
     * @author juan.agudelo
     */
    @Test
    public void testContarTramitesDeEjecutorAsignadoFallo() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testContarTramitesDeEjecutorAsignado");
        // FUNCIONARIO NO VALIDO
        String funcionarioEjecutor = "SENIOR2";
        boolean bandera = false;
        Integer tramitesEjecutor = null;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramitesEjecutor = dao.contarTramitesDeEjecutorAsignado(funcionarioEjecutor);

            if (tramitesEjecutor.equals(null)) {
                fail("Debería tener trámites");
            } else {
                if (tramitesEjecutor >= 0) {
                    bandera = true;
                } else {
                    bandera = false;
                }
                Assert.assertFalse(false);
            }
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba funcionario y estado validos con tramies asignados
     *
     * @author juan.agudelo
     */
    @Test
    public void testContarTramitesDeEjecutorAsignado() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testContarTramitesDeEjecutorAsignado");
        String funcionarioEjecutor = "SENIOR2";
        // FUNCIONARIO CON TRAMITES ASIGNADOS
        boolean bandera = false;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer tramitesEjecutor = dao.contarTramitesDeEjecutorAsignado(funcionarioEjecutor);

            if (tramitesEjecutor >= 0) {
                bandera = true;
            } else {
                bandera = false;
            }

            Assert.assertTrue(bandera);
            LOGGER.debug("************************************************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba funcionario y estado validos con tramies asignados
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarTramitesDeEjecutorAsignado() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramitesDeEjecutorAsignado");
        String idTerritorial = "6360";
        String funcionarioEjecutor = "prusoacha03";
        List<Tramite> tramitesEjecutor;

        // FUNCIONARIO CON TRAMITES ASIGNADOS
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramitesEjecutor = dao.buscarTramitesDeEjecutorAsignado(idTerritorial,
                funcionarioEjecutor);

            LOGGER.info("bleh");
            if (!tramitesEjecutor.isEmpty()) {

                LOGGER.debug("Numero de radicacion: " + tramitesEjecutor.get(0).
                    getNumeroRadicacion());
                LOGGER.debug("" + tramitesEjecutor.get(0).getFuncionarioEjecutor());

                if (tramitesEjecutor.get(0).getTramitePredioEnglobes() != null) {
                    LOGGER.info("");
                    /*
                     * LOGGER.debug("TPE numeroPredial: " + tramitesEjecutor.get(0)
                     * .getTramitePredioEnglobes().get(0) .getPredio().getNumeroPredial());
                     */
                }

                LOGGER.info("nulo");
                if (tramitesEjecutor.get(0).getComisionTramites() != null) {
                    if (tramitesEjecutor.get(0).getComisionTramites().size() > 0) {
                        LOGGER.debug("Comision: " +
                            tramitesEjecutor.get(0).getComisionTramites().get(0).getComision().
                                getNumero());
                    }
                }

            }

            LOGGER.debug("*** CANTIDAD DE REGISTROS:" + tramitesEjecutor.size() + " *** ");
            Assert.assertTrue(true);
            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de archivar trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testArchivarTramite() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testArchivarTramite");
        Tramite tramite = new Tramite();
        UsuarioDTO usuario = new UsuarioDTO();
        Boolean resultadoT = null;

        usuario.setLogin("juan.agudelo.test");
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramite = dao.buscarTramiteTramitesPorTramiteId(7L);

            this.dao.getEntityManager().getTransaction().begin();
            resultadoT = dao.archivarTramite(usuario, tramite);
            this.dao.getEntityManager().getTransaction().commit();

            Assert.assertNotNull(resultadoT);
            LOGGER.debug("Actualización: " + resultadoT);

            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte pre: hay datos de prueba en la tabla TRAMITE que corresponden a los
     * usados en esta prueba
     */
    @Test
    public void testContarTramitesDeFuncionarioEjecutor() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testContarTramitesDeFuncionarioEjecutor");

        String nombreFuncionarioEjecutor = "SENIOR2";
        Integer numeroTramitesFuncionarioEjecutor;

        try {
            numeroTramitesFuncionarioEjecutor = this.dao.contarTramitesDeFuncionarioEjecutor(
                nombreFuncionarioEjecutor);
            Assert.assertTrue(numeroTramitesFuncionarioEjecutor >= 1);
            LOGGER.debug("Paso. Count = " + numeroTramitesFuncionarioEjecutor);
        } catch (Exception e) {
            LOGGER.error("Fallo en el test de contar tramites de funcionario ejecutor " + e.
                getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindByEjecutorAndComision() {

        LOGGER.debug("unit tests: TramiteDAOBeanTests#testFindByEjecutorAndComision ...");

        Long idComision = 1747L;
        String idFuncionarioEjecutor = "prutopo";
        List<Tramite> answer;

        LOGGER.debug("id comisión = " + idComision.toString() + " :: idEjecutor = " +
            idFuncionarioEjecutor);
        try {
            answer = this.dao.findByEjecutorAndComision(idFuncionarioEjecutor, idComision);
            LOGGER.debug("passed !!!!!!. found = " + answer.size());

            if (!answer.isEmpty()) {
                LOGGER.debug("data: " + answer.get(0).getDepartamento().getNombre());

                if (answer.get(0).getPredio() != null) {
                    LOGGER.debug("... tiene predio. num predial = " + answer.get(0).getPredio().
                        getNumeroPredial());
                }
            }
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetPrediosOfTramite() {

        LOGGER.debug("unit tests: TramiteDAOBeanTests#testGetPrediosOfTramite ...");

        Long idTramite = 96l;
        List<Predio> answer;

        try {
            answer = this.dao.getPrediosOfTramite(idTramite);
            LOGGER.debug("passed!. found = " + answer.size());

            if (!answer.isEmpty()) {
                LOGGER.debug("data: " + answer.get(0).getNumeroPredial());
            }

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error("... error:" + e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetDocsOfTramite() {
        LOGGER.debug("unit test: TramiteDAOBeanTests#testGetDocsOfTramite ...");

        Tramite tramite;
        Long tramiteId;
        int i;
        tramiteId = 3l;

        try {
            tramite = this.dao.getDocsOfTramite(tramiteId);
            LOGGER.debug("... passed!!!.");
            Assert.assertTrue(true);

            LOGGER.debug("solicitud data: " + tramite.getSolicitud().getNumero());

            if (!tramite.getTramiteDocumentacions().isEmpty()) {
                LOGGER.debug("fetched " + tramite.getTramiteDocumentacions().size() +
                    " documentacions");

                // D: solo tiene un documento relacionado si el campo 'aportado'
                // es SI
                i = 0;
                for (TramiteDocumentacion td : tramite.getTramiteDocumentacions()) {
                    if (td.getAportado().equals("SI")) {
                        LOGGER.debug("el doc del primer tramite docs: " + td.getDocumentoSoporte().
                            getId());
                        break;
                    }
                }
            } else {
                LOGGER.debug("fetched no documentations");
            }
        } catch (Exception e) {
            LOGGER.error("Error on unit test: TramiteDAOBeanTests#testGetDocsOfTramite: " + e.
                getMessage());
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testBuscarTramitesAsignadosAEjecutor() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramitesAsignadosAEjecutor");
        String idTerritorial = "6360";
        String funcionarioEjecutor = "prusoacha03";
        List<Tramite> tramitesEjecutor;

        // FUNCIONARIO CON TRAMITES ASIGNADOS
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramitesEjecutor = dao.buscarTramitesAsignadosAEjecutor(idTerritorial,
                funcionarioEjecutor);

            LOGGER.info("bleh");
            if (!tramitesEjecutor.isEmpty()) {

                LOGGER.debug("Numero de radicacion: " + tramitesEjecutor.get(0).
                    getNumeroRadicacion());

            }

            LOGGER.debug("*** CANTIDAD DE REGISTROS:" + tramitesEjecutor.size() + " *** ");
            Assert.assertTrue(true);
            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testCountTramitesPorComisionar() {

        LOGGER.debug("unit tests: TramiteDAOBeanTests#testCountTramitesPorComisionar ...");

        int answer;
        long[] idsTramites = {994l};

        try {
            answer = this.dao.countTramitesPorComisionar(idsTramites, EComisionTipo.CONSERVACION.
                toString());

            Assert.assertTrue(true);
            LOGGER.debug("... passed!. count = " + answer);

        } catch (Exception e) {
            LOGGER.error(" error on TramiteDAOBeanTests#testCountTramitesPorComisionar: " + e.
                getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de trámite con documentación y documento
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindTramiteTramiteDocumentacionDocumentoById() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramiteTramiteDocumentacionDocumentoById");

        Long tramiteId = 1120l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Tramite answerT = dao.findTramiteTramiteDocumentacionDocumentoById(tramiteId);

            for (TramiteDocumentacion td : answerT.getTramiteDocumentacions()) {
                LOGGER.debug("Documento" + td.getDocumentoSoporte().getArchivo());
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de trámite con pruebas y prueba entidads
     *
     * @author franz.gamba
     */
    @Test
    public void testFindTramiteCargarPruebasByTramiteId() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramiteCargarPruebasByTramiteId");

        Long tramiteId = 41399L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Tramite answerT = dao.findTramiteCargarPruebasByTramiteId(tramiteId);

            LOGGER.debug("Numero de radicación : " + answerT.getNumeroRadicacion());
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Test
    public void testBorrarSolicitanteTramite() {
        try {
            this.em.getTransaction().begin();
            Tramite tramite = this.dao.findTramitePruebasByTramiteId(628L);
            System.out.println("***********" + tramite.getSolicitanteTramites().size());
            tramite.getSolicitanteTramites().remove(0);
            System.out.println("***********" + tramite.getSolicitanteTramites().size());
            this.dao.update(tramite);
            this.em.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Test para probar el fetch de los tramitePredioEnglobes en CargarSNC
     *
     * @author franz.gamba
     */
    @Test
    public void testFindTramitePruebasByTramiteId() {
        try {
            this.em.getTransaction().begin();
            Tramite tramite = this.dao.findTramitePruebasByTramiteId(42019L);
            if (tramite.getTramitePredioEnglobes() != null && !tramite.getTramitePredioEnglobes().
                isEmpty()) {
                for (TramitePredioEnglobe tpe : tramite.getTramitePredioEnglobes()) {
                    LOGGER.debug("Tramite predio englo be del tramite " + tramite.getId() +
                        " num. predial: " +
                        tpe.getPredio().getNumeroPredial());
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author fabio.chambonete
     */
    @Test
    public void testAgregarSolicitanteTramite() {
        try {
            this.em.getTransaction().begin();
            Tramite tramite = this.dao.findTramitePruebasByTramiteId(628L);
            SolicitanteTramite solTram = new SolicitanteTramite();
            SolicitanteSolicitud auxSolSol = tramite.getSolicitud().getSolicitanteSolicituds().
                get(0);

            solTram.setCorreoElectronico(auxSolSol.getCorreoElectronico());
            solTram.setCorreoElectronicoSecundario(auxSolSol.getCorreoElectronicoSecundario());
            solTram.setDigitoVerificacion(auxSolSol.getDigitoVerificacion());
            solTram.setDireccion(auxSolSol.getDireccion());
            solTram.setDireccionDepartamento(auxSolSol.getDireccionDepartamento());
            solTram.setDireccionMunicipio(auxSolSol.getDireccionMunicipio());
            solTram.setDireccionPais(auxSolSol.getDireccionPais());
            solTram.setFax(auxSolSol.getFax());
            solTram.setFaxExt(auxSolSol.getFaxExt());
            solTram.setNumeroIdentificacion(auxSolSol.getNumeroIdentificacion());
            solTram.setPrimerApellido(auxSolSol.getPrimerApellido());
            solTram.setPrimerNombre(auxSolSol.getPrimerNombre());
            solTram.setRazonSocial(auxSolSol.getRazonSocial());
            solTram.setSegundoApellido(auxSolSol.getSegundoApellido());
            solTram.setSegundoNombre(auxSolSol.getSegundoNombre());
            solTram.setTelefonoCelular(auxSolSol.getTelefonoCelular());
            solTram.setTelefonoPrincipal(auxSolSol.getTelefonoPrincipal());
            solTram.setTelefonoPrincipalExt(auxSolSol.getTelefonoPrincipalExt());
            solTram.setTelefonoSecundario(auxSolSol.getTelefonoSecundario());
            solTram.setTelefonoSecundarioExt(auxSolSol.getTelefonoSecundarioExt());
            solTram.setTipoIdentificacion(auxSolSol.getTipoIdentificacion());
            solTram.setTipoPersona(auxSolSol.getTipoPersona());
            solTram.setTipoSolicitante(auxSolSol.getTipoSolicitante());
            solTram.setRelacion(auxSolSol.getRelacion());
            solTram.setNotificacionEmail(auxSolSol.getNotificacionEmail());
            solTram.setSolicitante(auxSolSol.getSolicitante());

            tramite.setUsuarioLog("pruatlantico");
            tramite.setFechaLog(new Date());
            solTram.setTramite(tramite);
            solTram.setId(null);
            solTram.setFechaLog(new Date());
            solTram.setSolicitante(auxSolSol.getSolicitante());
            tramite.getSolicitanteTramites().add(solTram);
            solTram.setUsuarioLog("fabio.navarrete.test");
            this.dao.update(tramite);
            this.em.getTransaction().commit();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /*
     *
     */
    @Test
    public void testFindTramiteConTramitePruebaEntidadByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramiteConTramitePruebaEntidadByTramiteId");
        Tramite tramiteConPrueba;
        try {
            tramiteConPrueba = this.dao.findTramiteConTramitePruebaEntidadByTramiteId(20113L);
            Assert.assertNotNull(tramiteConPrueba);
            Assert.assertNotNull(tramiteConPrueba.getTramitePruebas());
            Assert.assertNotNull(tramiteConPrueba.getTramitePruebas().getTramitePruebaEntidads());
            LOGGER.debug("***************");
            LOGGER.debug(tramiteConPrueba.getTramitePruebas().getTramitePruebaEntidads().get(0)
                .getDireccionDepartamento().getNombre());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    @Test
    public void testGetTramitesByResolucion() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetTramitesByResolucion");
        List<Tramite> tramites = new ArrayList<Tramite>();
        String resolucion = "25-754-0081-2011";
        String documentoEstado = EDocumentoEstado.RESOLUCION_EN_FIRME.toString();
        String documentoEstadoAdicional = EDocumentoEstado.RESOLUCION_EN_PROYECCION.toString();

        try {
            LOGGER.debug("******* COMIENZA TEST ********");
            tramites = dao.getTramitesByResolucion(resolucion, documentoEstado,
                documentoEstadoAdicional);
            Assert.assertNotNull(tramites);
            LOGGER.debug("Tamaño: " + tramites.size());
            LOGGER.debug("****** TERMINAMOS TEST *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testBuscarTramitesConResolucionYNumeroPredialPredio() {
        LOGGER.debug(
            "tests: TramiteDAOBeanTests#testBuscarTramitesConResolucionYNumeroPredialPredio");

        List<Tramite> tramites = new ArrayList<Tramite>();
        String numPredial = "080010103000000200490900000000";
        String resolucion = "25-754-0081-2011";

        try {
            LOGGER.debug("******* COMIENZA TEST ********");
            tramites = dao.buscarTramitesConResolucionYNumeroPredialPredio(resolucion, numPredial);
            Assert.assertNotNull(tramites);
            LOGGER.debug("Tamaño: " + tramites.size());
            LOGGER.debug("****** TERMINAMOS TEST *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de predios iniciales asociados a un trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPorTramiteId() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarPrediosPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = dao.buscarPrediosPorTramiteId(tramiteId);

            for (Predio p : answerT) {
                LOGGER.debug("Número predial" + p.getNumeroPredial());
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios simples por tramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindTramitePrediosSimplesByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramitePrediosSimplesByTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Tramite answerT = dao.findTramitePrediosSimplesByTramiteId(tramiteId);

            LOGGER.debug("Predio " + answerT.getPredio().getNumeroPredial());
            if (answerT.getTramitePredioEnglobes() != null && !answerT.getTramitePredioEnglobes().
                isEmpty()) {
                for (TramitePredioEnglobe tpe : answerT.getTramitePredioEnglobes()) {
                    LOGGER.debug("Números prediales " + tpe.getPredio().getNumeroPredial());
                }
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel Avaluo en validación
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelAvaluoPorTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarPrediosPanelAvaluoPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = dao.buscarPrediosPanelAvaluoPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    LOGGER.debug("Avaluos " + p.getPredioAvaluoCatastrals().get(0).
                        getValorTotalAvaluoCatastral());
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel justificación de propiedad en
     * validación
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelJPropiedadPorTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarPrediosPanelJPropiedadPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = dao.buscarPrediosPanelJPropiedadPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    if (p.getPersonaPredios() != null && !p.getPersonaPredios().isEmpty()) {
                        for (PersonaPredio pp : p.getPersonaPredios()) {

                            LOGGER.debug("PerosnaP id: " + pp.getId());

                            if (pp.getPersonaPredioPropiedads() != null && !pp.
                                getPersonaPredioPropiedads().isEmpty()) {
                                for (PersonaPredioPropiedad ppp : pp.getPersonaPredioPropiedads()) {
                                    LOGGER.debug("PerosnaPP tipo título: " + ppp.getTipoTitulo());
                                }
                            }
                        }
                    }
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel personas y / o propietarios
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelPropietariosPorTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarPrediosPanelPropietariosPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = dao.buscarPrediosPanelJPropiedadPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    if (p.getPersonaPredios() != null && !p.getPersonaPredios().isEmpty()) {
                        for (PersonaPredio pp : p.getPersonaPredios()) {

                            LOGGER.debug("PerosnaP id: " + pp.getId());

                            if (pp.getPersona() != null) {

                                LOGGER.debug("Nombre: " + pp.getPersona().getNombre());
                            }
                        }
                    }
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel ficha matriz
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelFichaMatrizPorTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarPrediosPanelFichaMatrizPorTramiteId");

        Long tramiteId = 20789l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = dao.buscarPrediosPanelFichaMatrizPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    LOGGER.debug("Número predial " + p.getNumeroPredial());

                    if (p.getFichaMatrizs() != null && !p.getFichaMatrizs().isEmpty()) {
                        for (FichaMatriz fm : p.getFichaMatrizs()) {
                            LOGGER.debug("Unidades " + fm.getId());
                        }
                    }
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel ubicación de predio
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelUbicacionPredioPorTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarPrediosPanelUbicacionPredioPorTramiteId");

        Long tramiteId = 2115l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = dao.buscarPrediosPanelUbicacionPredioPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    LOGGER.debug("Número predial " + p.getNumeroPredial());
                    LOGGER.debug("Zonas " + p.getPredioZonas().size());
                    if (p.getPredioZonas() != null && !p.getPredioZonas().isEmpty()) {
                        LOGGER.debug("ZonaF " + p.getPredioZonas().get(0).getZonaFisica());
                    }
                    LOGGER.debug("Servidumbres " + p.getPredioServidumbres().size());
                    if (p.getPredioServidumbres() != null && !p.getPredioServidumbres().isEmpty()) {
                        LOGGER.debug("Servidumbre " + p.getPredioServidumbres().get(0).
                            getServidumbre());
                    }
                }
            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte pre: hay datos de prueba en la tabla TRAMITE que corresponden a un id
     * de trámite
     */
    @Test
    public void testBuscarTramiteTraerDocumentoResolucionPorIdTramite() {

        Long tramiteId = 1770l;
        try {
            LOGGER.debug(
                "Entra en TramiteDAOBeanTests#testBuscarTramiteTraerDocumentoResolucionPorIdTramite");
            Tramite answer = dao.buscarTramiteTraerDocumentoResolucionPorIdTramite(tramiteId);

            LOGGER.debug("Id Documento Resolución" + answer.getResultadoDocumento().getId());
            assertNotNull(answer);
            LOGGER.debug(
                "Sale de TramiteDAOBeanTests#testBuscarTramiteTraerDocumentoResolucionPorIdTramite");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // /**
    // *
    // * @author juan.mendez
    // */
    // @Test
    // public void testActualizarNombreReplica() {
    // try {
    // this.em.getTransaction().begin();
    // LOGGER.debug("testActualizarNombreReplica");
    // Long tramiteId = 5928l;
    // String nombreReplica = "rt_25754_955_1322280877.gdb.zip.enc.zip";
    // dao.actualizarNombreReplica(tramiteId, nombreReplica);
    // this.em.getTransaction().commit();
    // } catch (Exception e) {
    // LOGGER.error(e.getMessage(), e);
    // fail(e.getMessage(), e);
    // }
    // }
    /**
     * @author lorena.salamanca
     */
    @Test
    public void testGetNombreReplicaGDBdeTramite() {

        LOGGER.debug("tests: TramiteDAOBeanTests#testGetNombreReplicaGDBdeTramite");
        Long idTramite = 6439L;
        Long tipoDocumento = 97L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            String ruta = this.dao.getIdRepositorioDocumentosDeReplicaGDBdeTramite(idTramite,
                tipoDocumento);

            LOGGER.debug("nombre archivo " + ruta);
            // Assert.assertTrue(true);
            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarTramiteConResolucionSimple() {
        LOGGER.debug("unit test TramiteDAOBeanTests#testBuscarTramiteConResolucionSimple ...");

        Tramite answer;
        Long idTramite;
        Documento docResolucion;
        TipoDocumento tipoDoc;

        idTramite = new Long(136l);

        try {
            LOGGER.debug("probando el método buscarTramiteConResolucionSimple ...");
            answer = this.dao.buscarTramiteConResolucionSimple(idTramite);
            if (answer != null) {
                LOGGER.debug("... encontró el trámite.");
                if (answer.getResultadoDocumento() != null) {
                    docResolucion = answer.getResultadoDocumento();
                    LOGGER.debug("Encontró documento resolución, con idRepositorioDocumentos = " +
                        docResolucion.getIdRepositorioDocumentos());

                    // D: esto no debe ser null
                    assertNotNull(docResolucion.getTipoDocumento());

                    if (docResolucion.getTipoDocumento() != null) {
                        tipoDoc = docResolucion.getTipoDocumento();
                        LOGGER.debug("El tipo de documento es: " + tipoDoc.getId() + " :: " +
                            tipoDoc.getNombre());

                        if (tipoDoc.getId() != ETipoDocumentoId.RESOLUCION_CONSERVACION.getId()) {
                            LOGGER.debug("OJO el tipo de documento no corresponde!!!");
                        }
                    }
                } else {
                    LOGGER.debug("OJO el trámite no tiene documento de resolución!!!");
                }
            }
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage(), e);
            fail("error: " + e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia pero el método es de juan.agudelo... que man tan paila!! no hizo pruebas
     *
     * OJO: lo que sucede es que cuando en un método de un DAO se usa otro DAO injectado las pruebas
     * unitarias NO FUNCIONAN porque ese DAO inyectado está en null. Se debe confiar entonces } en
     * las pruebas de integración
     */
    @Test
    public void testBuscarTramiteSolicitudPorId() {
        LOGGER.debug("unit test TramiteDAOBeanTests#testBuscarTramiteSolicitudPorId ...");

        Tramite answer = null;
        Long idTramiteL = new Long(42969L);

        try {
            answer = this.dao.buscarTramiteSolicitudPorId(idTramiteL);
            Assert.assertNotNull(answer);
            if (answer != null) {
                LOGGER.debug("Trámite id : " + answer.getId());
                LOGGER.debug("Trámite estados : " + answer.getTramiteEstados().size());
                LOGGER.debug("Trámite documentacion : " + answer.getTramiteDocumentacions().size());
                LOGGER.debug("Trámite predioEnglobes : " + answer.getTramitePredioEnglobes().size());
                LOGGER.debug("Trámite solicitud solicitantesSolicitud : " +
                    answer.getSolicitud().getSolicitanteSolicituds().size());
            } else {
                LOGGER.debug("No se encontró el trámite.");
            }
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage(), e);
            fail("error: " + e.getMessage(), e);
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testFindTramitesCancelacion() {
        try {
            LOGGER.debug("TramiteDAOBeanTests#testFindTramitesCancelacion");

            List<String> municipioCods = new LinkedList<String>();
            municipioCods.add("25754");

            FiltroDatosConsultaCancelarTramites filtro = new FiltroDatosConsultaCancelarTramites();
            // filtro.setNumeroRadicacion("0000080011800000692012");
            // filtro.setNumeroIdentificacion("1");
            filtro.setMunicipiosJurisdiccion(municipioCods);
            filtro.setDepartamentoNumPredial("25");

            List<Tramite> tramites = this.dao.findTramitesCancelacion(filtro, 0, 10);
            LOGGER.debug("Tamaño lista tràmites: " + tramites.size());

            for (Tramite t : tramites) {
                LOGGER.debug("Numero de Radicacion : " + t.getPredio().getNumeroPredial());
            }

            Assert.assertTrue(!tramites.isEmpty());
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage(), e);
            fail("error: " + e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predio y solicitud por trámite id
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindTramitePredioSolicitudByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramitePredioSolicitudByTramiteId");

        Long tramiteId = 41171l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Tramite answerT = dao.findTramitePredioSolicitudByTramiteId(tramiteId);

            if (answerT != null) {

                LOGGER.debug("Tramite " + answerT.getNumeroRadicacion());
                if (answerT.getPredio() != null) {
                    LOGGER.debug("Predio: " + answerT.getPredio().getNumeroPredial());
                    LOGGER.debug("Departamento: " + answerT.getPredio().getDepartamento().
                        getNombre());
                    LOGGER.debug("Municipio: " + answerT.getPredio().getMunicipio().getNombre());
                }
                LOGGER.debug("Comisionado " + answerT.getComisionado());

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testLookForCurrentOnPredio() {

        LOGGER.debug("unit test TramiteDAOBeanTests#testLookForCurrentOnPredio ...");
        Long predioId = 7l;

        ArrayList<Tramite> resp;

        try {
            resp = (ArrayList<Tramite>) this.dao.lookForCurrentOnPredio(predioId);
            LOGGER.debug("found " + resp.size() + " trámites");
            Assert.assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error("excepción: " + ex.getMessage());
            Assert.fail();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testGetTramitesDeActGeograficas() {

        LOGGER.debug("unit test TramiteDAOBeanTests#testGetTramitesDeActGeograficas ...");
        List<Long> tramiteIds = new ArrayList<Long>();
        tramiteIds.add(42517L);

        List<Tramite> resp = null;

        try {
            resp = (List<Tramite>) this.dao.getTramitesDeActGeograficas(tramiteIds);
            LOGGER.debug("found " + resp.size() + " trámites");
            Assert.assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error("excepción: " + ex.getMessage());
            Assert.fail();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     *
     * PRE: el id del predio se debe relacionar con un trámite que tenga resultadoDocumento not null
     */
    @Test
    public void testLookForFinishedOnPredio() {

        LOGGER.debug("unit test TramiteDAOBeanTests#testLookForFinishedOnPredio ...");
        Long predioId = 550561l;

        ArrayList<Tramite> resp;

        try {
            resp = (ArrayList<Tramite>) this.dao.lookForFinishedOnPredio(predioId);
            LOGGER.debug("found " + resp.size() + " trámites");
            Assert.assertNotNull(resp.get(0).getResultadoDocumento());
            LOGGER.
                debug("el doc del primer trámite: " + resp.get(0).getResultadoDocumento().getId());
        } catch (Exception ex) {
            LOGGER.error("excepción: " + ex.getMessage());
            Assert.fail();
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con solicitud por filtro de busqueda de trámite
     *
     * @author juan.agudelo
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testFindTramiteSolicitudByFiltroTramite() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramiteSolicitudByFiltroTramite");

        FiltroDatosConsultaTramite fb = cargarDatosBaseFiltroBusquedaTramite();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Tramite> answerT = dao.findTramiteSolicitudByFiltroTramite(fb);

            if (answerT != null && !answerT.isEmpty()) {

                LOGGER.debug("Tamaño " + answerT.size());

                for (Tramite t : answerT) {
                    LOGGER.debug("Número de radicación: " + t.getNumeroRadicacion());
                }
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba conteo de tramites con solicitud por filtro de busqueda de trámite
     *
     * @author juan.agudelo
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testCountTramiteSolicitudByFiltroTramite() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testCountTramiteSolicitudByFiltroTramite");

        // FiltroDatosConsultaTramite fb = cargarDatosBaseFiltroBusquedaTramite();
        FiltroDatosConsultaTramite fb = new FiltroDatosConsultaTramite();
        NumeroPredialPartes np = new NumeroPredialPartes();
        // 08 001 00 02 00 00 0000 0008 0 00 00 0000
        np.setDepartamentoCodigo("08");
        np.setMunicipioCodigo("001");
        np.setTipoAvaluo("00");
        np.setSectorCodigo("02");
        np.setComunaCodigo("00");
        np.setBarrioCodigo("00");
        np.setManzanaVeredaCodigo("0000");
        np.setPredio("0008");
        np.setCondicionPropiedad("0");
        np.setEdificio("00");
        np.setPiso("00");
        np.setUnidad("0000");

        fb.setNumeroPredialPartes(np);
        fb.setNumeroSolicitud("60402012ER00039016");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answerT = dao.countTramiteSolicitudByFiltroTramite(fb);

            if (answerT != null) {
                LOGGER.debug("Tamaño " + answerT);
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de saldos de Conservacion
     *
     * @author franz.gamba
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testGetTramitesSaldosDeConservacion() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGgetTramitesSaldosDeConservacion");
        List<Long> tramiteIds = new LinkedList<Long>();
        tramiteIds.add(921L);
        tramiteIds.add(922L);
        tramiteIds.add(923L);
        tramiteIds.add(16012L);
        tramiteIds.add(87L);
        tramiteIds.add(81L);
        try {
            List<Tramite> tramites = this.dao.getTramitesSaldosDeConservacion(tramiteIds);

            LOGGER.debug("************** SALDOS DE CONSERVACION: **************");
            for (Tramite t : tramites) {
                LOGGER.debug(" Numero de Radicacion => " + t.getNumeroRadicacion());
                LOGGER.debug(" Numero de Solicitud => " + t.getSolicitud().getId());
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los datos base de un filtro de busqueda de trámite
     *
     * @author juan.agudelo
     * @version 2.0
     * @throws ParseException
     */
    private FiltroDatosConsultaTramite cargarDatosBaseFiltroBusquedaTramite() throws ParseException {

        FiltroDatosConsultaTramite filtroDatosConsultaTramite = new FiltroDatosConsultaTramite();
        NumeroPredialPartes numeroPredialPartes = new NumeroPredialPartes();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        /*
         * filtroDatosConsultaTramite.setNumeroSolicitud("8002011ER00002369");
         * filtroDatosConsultaTramite.setNumeroRadicacion("00002575400013672011");
         * filtroDatosConsultaTramite.setFechaSolicitud(sdf.parse("11/10/2011"));
         * filtroDatosConsultaTramite.setFechaRadicacion(sdf.parse("11/10/2011"));
         */
        filtroDatosConsultaTramite.setTipoSolicitud(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
        /*
         * filtroDatosConsultaTramite.setTipoTramite(ETramiteTipoTramite.MUTACION .getCodigo());
         * filtroDatosConsultaTramite.setClaseMutacion(EMutacionClase.SEGUNDA .getCodigo());
         * filtroDatosConsultaTramite.setSubtipo(EMutacion2Subtipo.DESENGLOBE .getCodigo());
         * filtroDatosConsultaTramite.setNumeroResolucion("123465789");
         * filtroDatosConsultaTramite.setFechaResolucion(sdf.parse("11/10/2011"));
         */

        // Persona natural
        // filtroDatosConsultaTramite.setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
        // filtroDatosConsultaTramite.setNumeroIdentificacion("1055");
        // persona jurídica
        /*
         * filtroDatosConsultaTramite .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT
         * .getCodigo()); filtroDatosConsultaTramite.setNumeroIdentificacion("909090909090");
         * filtroDatosConsultaTramite.setDigitoVerificacion("1");
         */
        // Número predial por partes
        /*
         * numeroPredialPartes.setDepartamentoCodigo("25");
         * numeroPredialPartes.setMunicipioCodigo("25754"); numeroPredialPartes.setTipoAvaluo("01");
         * numeroPredialPartes.setSectorCodigo("02"); numeroPredialPartes.setComunaCodigo("00");
         * numeroPredialPartes.setBarrioCodigo("00");
         * numeroPredialPartes.setManzanaVeredaCodigo("1165");
         * numeroPredialPartes.setPredio("0001"); numeroPredialPartes.setCondicionPropiedad("5");
         * numeroPredialPartes.setEdificioTorre("00"); numeroPredialPartes.setPiso("00");
         * numeroPredialPartes.setUnidad("0032");
         *
         * filtroDatosConsultaTramite.setNumeroPredialPartes(numeroPredialPartes);
         */
        return filtroDatosConsultaTramite;
    }

    @Test
    public void testFindTramitesByIds() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindTramitesByIds");
        List<Long> tramiteIds = new LinkedList<Long>();
        tramiteIds.add(15260L);
        tramiteIds.add(15466L);
        try {
            HashMap<String, String> filtros = new HashMap<String, String>();
            // filtros.put("numeroRadicacion", "00002575400011142012");
            // filtros.put("solicitud.numero", "8002012ER00015138");
            filtros.put("fechaRadicacion", "12/07/2012");
            // filtros.put("solicitud.numero", "8002012ER00015989");

            List<Tramite> tramites = this.dao.findTramitesByIds(tramiteIds, null, null, filtros);

            LOGGER.debug("************** TRAMITES: **************");

            for (Tramite t : tramites) {
                LOGGER.debug(" Tramite ID => " + t.getId());
                LOGGER.debug(" Numero de Radicacion => " + t.getNumeroRadicacion());
                LOGGER.debug(" Numero de Solicitud => " + t.getSolicitud().getNumero());
                LOGGER.debug(" Fecha radicacion => " + t.getFechaRadicacion());
            }

        } catch (Exception e) {

            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetSolicitantesParaNotificarByTramiteId() {
        LOGGER.debug(
            "unit testing TramiteDAOBeanTests#testGetSolicitantesParaNotificarByTramiteId...");

        Long tramiteId = 35681L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Solicitante> answer = this.dao.getSolicitantesParaNotificarByTramiteId(tramiteId);

            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("Tamaño " + answer.size());

                for (Solicitante sTmp : answer) {
                    LOGGER.debug("Nombre: " + sTmp.getNombreCompleto());
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método test que prueba la consulta de un trámite por su número de resolución.
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarTramitesPorNumeroDeResolucion() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramitesPorNumeroDeResolucion");
        List<Tramite> tramites = new ArrayList<Tramite>();
        String resolucion = "25-754-0083-2011";

        try {
            LOGGER.debug("******* Test ********");
            tramites = dao.buscarTramitesPorNumeroDeResolucion(resolucion);
            Assert.assertNotNull(tramites);
            LOGGER.debug("Tamaño: " + tramites.size());
            LOGGER.debug("****** Sale del test *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para verificar si un predio se encuentra cancelado
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetPredioDptoMuniByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testDetPredioDptoMuniByTramiteId");

        Long tramiteId = 36624L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Predio answer = this.dao.getPredioDptoMuniByTramiteId(tramiteId);

            if (answer != null) {
                LOGGER.debug("Número predial: " + answer.getNumeroPredial());
                LOGGER.debug("Departamento: " + answer.getDepartamento().getNombre());
                LOGGER.debug("Municipio: " + answer.getMunicipio().getNombre());
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para cargar los tramiteDocumentos de un trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetTramiteDocumentosByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetTramiteDocumentosByTramiteId");

        Long tramiteId = 36649L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<TramiteDocumento> answer = this.dao.getTramiteDocumentosByTramiteId(tramiteId);

            if (answer != null) {
                LOGGER.debug("Tamaño: " + answer.size());
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para cargar los tramiteDocumentos de un trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetTramiteDocumentacionsByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetTramiteDocumentacionsByTramiteId");

        Long tramiteId = 36649L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<TramiteDocumentacion> answer = this.dao.getTramiteDocumentacionsByTramiteId(
                tramiteId);

            if (answer != null) {
                LOGGER.debug("Tamaño: " + answer.size());
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para
     *
     * @author franz.gamba *
     */
    @Test
    public void testGetEjecutorTramiteClasificacion() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetEjecutorTramiteClasificacion");

        List<Long> tramiteIds = new LinkedList<Long>();
        tramiteIds.add(37519L);
        tramiteIds.add(36907L);
        tramiteIds.add(36760L);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            SEjecutoresTramite answer = this.dao.getEjecutorTramiteClasificacion(tramiteIds);

            assertNotNull(answer.getOficina());

            LOGGER.debug("tramites terreno: " + answer.getTerreno());
            LOGGER.debug("tramites oficina: " + answer.getOficina());

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para obtener los id de los tramites asociados a un trámite
     *
     * @author franz.gamba *
     */
    @Test
    public void testGetIdsDeTramitesAsociadosByTramiteId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetIdsDeTramitesAsociadosByTramiteId");

        List<Long> tramiteIds = new LinkedList<Long>();
        Long idTramite = 41166L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramiteIds = this.dao.getIdsDeTramitesAsociadosByTramiteId(idTramite);

            assertNotNull(tramiteIds);
            LOGGER.debug("********* TRAMITES ASOCIADOS : " + tramiteIds);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con solicitud por filtro de busqueda de trámite
     *
     * @author javier.aponte
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testFindTramiteByFiltroTramiteAdministracionAsignaciones() throws ParseException {
        LOGGER.debug(
            "tests: TramiteDAOBeanTests#testFindTramiteByFiltroTramiteAdministracionAsignaciones");

        FiltroDatosConsultaTramite fb = new FiltroDatosConsultaTramite();

        fb.setDepartamentoId("08");
        fb.setMunicipioId("08001");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Tramite> answerT = dao.findTramiteByFiltroTramiteAdministracionAsignaciones(fb,
                null, null);

            if (answerT != null && !answerT.isEmpty()) {

                LOGGER.debug("Tamaño " + answerT.size());

                for (Tramite t : answerT) {
                    LOGGER.debug("Trámite Id: " + t.getId());
                }
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba conteo de tramites con solicitud por filtro de busqueda de trámite para el caso de uso
     * de administración de asignaciones
     *
     * @author javier.aponte
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testCountTramiteByFiltroTramiteAdministracionAsignaciones() throws ParseException {
        LOGGER.debug(
            "tests: TramiteDAOBeanTests#testCountTramiteByFiltroTramiteAdministracionAsignaciones");

        FiltroDatosConsultaTramite fb = new FiltroDatosConsultaTramite();

        fb.setDepartamentoId("08");
        fb.setMunicipioId("08001");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answerT = dao.countTramiteByFiltroTramiteAdministracionAsignaciones(fb);

            if (answerT != null) {
                LOGGER.debug("Tamaño " + answerT);
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author javier.barajas
     */
    @Test
    public void testObtenerTramitesByNumeroPredialPredio() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testObtenerTramitesByNumeroPredialPredio");
        try {
            List<Tramite> tramites;
            tramites = dao.obtenerTramitesByNumeroPredialPredio("080010002000000000044000000000");

            LOGGER.debug("trajo " + tramites.toString());

            assertNotNull(tramites);
            assertTrue(tramites.size() >= 1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // ------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindTramitesParaCargueInfo() {

        LOGGER.debug("unit test TramiteDAOBeanTests#testFindTramitesParaCargueInfo ...");

        ArrayList<Tramite> answer;
        long[] idsTramites = new long[]{23990l, 34219l};

        try {
            answer = (ArrayList<Tramite>) this.dao.findTramitesParaCargueInfo(idsTramites,
                EComisionTipo.CONSERVACION.getCodigo());
            LOGGER.debug("passed");
            if (answer != null) {
                for (Tramite t : answer) {
                    LOGGER.debug("tramite id: " + t.getId() + "  ::  comision número: " + t.
                        getNumeroComision());
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error en TramiteDAOBeanTests#testFindTramitesParaCargueInfo: " + ex.
                getMessage());
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con solicitud por filtro de busqueda de trámite
     *
     * @author javier.aponte
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testFindTramiteByFiltroTramiteDerechoPeticionOTutela() throws ParseException {
        LOGGER.debug(
            "tests: TramiteDAOBeanTests#testFindTramiteByFiltroTramiteDerechoPeticionOTutela");

        FiltroDatosConsultaTramite fb = new FiltroDatosConsultaTramite();
        FiltroDatosConsultaSolicitante fs = new FiltroDatosConsultaSolicitante();

        fb.setTerritorialId("6360");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Tramite> answerT = dao.findTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(fb,
                fs, null, null);

            if (answerT != null && !answerT.isEmpty()) {

                LOGGER.debug("Tamaño " + answerT.size());

                for (Tramite t : answerT) {
                    LOGGER.debug("Trámite Id: " + t.getId());
                }
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba conteo de tramites con solicitud por filtro de busqueda de trámite para el caso de uso
     * de administración de asignaciones
     *
     * @author javier.aponte
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testCountTramiteByFiltroTramiteDerechoDePeticionOTutela() throws ParseException {
        LOGGER.debug(
            "tests: TramiteDAOBeanTests#testCountTramiteByFiltroTramiteDerechoDePeticionOTutela");

        FiltroDatosConsultaTramite fb = new FiltroDatosConsultaTramite();
        FiltroDatosConsultaSolicitante fs = new FiltroDatosConsultaSolicitante();

        fb.setTerritorialId("6040");
        fb.setMunicipioId("08001");
        fb.setDepartamentoId("08");
        fb.setNumeroRadicacion("0800100015092013");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answerT = dao.countTramiteByFiltroTramiteRadicarDerechoPeticionOTutela(fb, fs);

            if (answerT != null) {
                LOGGER.debug("Tamaño " + answerT);
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerTramitesPorManzana() {

        LOGGER.debug("unit test TramiteDAOBeanTests#testObtenerTramitesPorManzana ...");

        List<Tramite> resultado;

        String manzana = "0800101010000012";
        try {
            resultado = this.dao.obtenerTramitesPorManzanaEnEdicion(manzana);
            LOGGER.debug("passed");
            if (resultado != null) {
                for (Tramite t : resultado) {
                    LOGGER.debug("tramite id: " + t.getId());
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error en TramiteDAOBeanTests#testFindTramitesParaCargueInfo: " + ex.
                getMessage());
        }

    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testObtenerTramitesPorManzanaDepuracion() {

        LOGGER.debug("unit test TramiteDAOBeanTests#testObtenerTramitesPorManzana ...");

        List<Tramite> resultado;

        String manzana = "080010105000004580002";
        try {
            resultado = this.dao.obtenerTramitesPorManzanaDepuracionEnEdicion(manzana);
            LOGGER.debug("passed");
            if (resultado != null) {
                for (Tramite t : resultado) {
                    LOGGER.debug("tramite id: " + t.getId());
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error en TramiteDAOBeanTests#testFindTramitesParaCargueInfo: " + ex.
                getMessage());
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método test que prueba la consulta de un trámite por su número de resolución.
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarTramiteConViaGubernativaPorIdsTramites() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramiteConViaGubernativaPorIdsTramites");

        List<Tramite> tramites = null;
        List<Long> idsTramite = new ArrayList<Long>();
        idsTramite.add(36709L);

        try {
            LOGGER.debug("******* Test ********");
            tramites = dao.buscarTramiteConViaGubernativaPorIdsTramites(idsTramite);
            Assert.assertNotNull(tramites);
            LOGGER.debug("Tamaño: " + tramites.size());
            LOGGER.debug("****** Sale del test *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método test que prueba la consulta de un trámite por su número de resolución.
     *
     * @author david.cifuentes
     */
    @Test
    public void testObtenerTramitesPorEjecutorYClasificacion() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testObtenerTramitesPorEjecutorYClasificacion");

        Long tramites = null;
        String ejecutor = "pruatlantico17";
        String clasificacion = ETramiteClasificacion.OFICINA.toString();

        try {
            LOGGER.debug("******* Test ********");
            tramites = dao.obtenerTramitesPorEjecutorYClasificacion(ejecutor, clasificacion);
            clasificacion = ETramiteClasificacion.TERRENO.toString();
            tramites = dao.obtenerTramitesPorEjecutorYClasificacion(ejecutor, clasificacion);
            Assert.assertNotNull(tramites);
            LOGGER.debug("Tamaño: " + tramites);
            LOGGER.debug("****** Sale del test *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba conteo de tramites con solicitud por filtro de busqueda de trámite para el caso de uso
     * de administración de asignaciones
     *
     * @author javier.aponte
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testFindTramitesParaDeterminarProcedenciaSolicitud() throws ParseException {
        LOGGER.
            debug("tests: TramiteDAOBeanTests#testFindTramitesParaDeterminarProcedenciaSolicitud");

        long[] idsTramites = new long[20];

        List<Tramite> resultado;

        HashMap<String, String> filters = new HashMap<String, String>();

        for (int i = 0; i < 20; i++) {
            idsTramites[i] = i + 41406;
        }

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            resultado = dao.findTramitesParaDeterminarProcedenciaSolicitud(idsTramites, "",
                "UNSORTED", filters);

            if (resultado != null) {
                LOGGER.debug("Tamaño " + resultado);
            }

            assertNotNull(resultado);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método test que prueba la consulta de ids de trámites con clasificación
     *
     * @author javier.aponte
     */
    @Test
    public void testFindIdsClasificacionTramitesConEjecutor() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindIdsClasificacionTramitesConEjecutor");

        List<TramiteConEjecutorAsignadoDTO> answer;

        long[] idsTramitesActividades = new long[1000];

        for (int i = 0; i < 1000; i++) {
            idsTramitesActividades[i] = 41406 + i;
        }

        try {
            LOGGER.debug("******* Test ********");
            answer = dao.findIdsClasificacionTramitesConEjecutor(idsTramitesActividades);
            Assert.assertNotNull(answer);
            for (TramiteConEjecutorAsignadoDTO currentTramite : answer) {
                LOGGER.debug("CurrentTramiteId: " + currentTramite.getId());
                LOGGER.debug("CurrentTramiteClasificacion: " + currentTramite.getClasificacion());
                LOGGER.debug("CurrentTramiteFuncionarioEjecutor: " + currentTramite.
                    getFuncionarioEjecutor());
            }
            LOGGER.debug("Tamaño: " + answer.size());
            LOGGER.debug("****** Sale del test *******");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     */
    @Test
    public void testBuscarTramiteByFiltroTramiteAdminAsignaciones() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramiteByFiltroTramiteAdminAsignaciones");

        FiltroDatosConsultaTramite fb = new FiltroDatosConsultaTramite();
        FiltroDatosConsultaSolicitante fs = new FiltroDatosConsultaSolicitante();

        fb.setTerritorialId("6040");
        fb.setMunicipioId("08001");
        fb.setDepartamentoId("08");
        Calendar c = Calendar.getInstance();
        c.set(2014, 11, 1);
        fb.setFechaRadicacion(c.getTime());
        fb.setFechaRadicacionFinal(new Date());
        fb.setClasificacion("TERRENO");
        // fb.setNumeroRadicacion("0800100015092013");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Tramite> result = dao.buscarTramiteByFiltroTramiteAdminAsignaciones(fb, fs, null,
                null, null);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     */
    @Test
    public void testBuscarTramitesEnActualizacion() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramitesEnActualizacion");

        List<Long> ids = new ArrayList<Long>();
        ids.add(136029l);
        ids.add(10241l);
        try {
            LOGGER.debug("**************** INICIO *************************");
            List<Tramite> result = dao.buscarTramitesEnActualizacion(ids);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testBuscarTramitesActualizacionAgrupadoPorPredio() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#buscarTramitesActualizacionAgrupadoPorPredio");

        long[] ids = {326028, 326029, 326030, 326031, 326032, 326033, 326034, 326035, 326036, 326037,
            326038, 326039,
            326040, 326041, 326042, 326043, 326044, 326045, 326046, 326047, 326048, 326049, 326050,
            326051, 326052,
            326053, 326054, 326055, 326056, 326057, 326058, 326059, 326518, 326519, 326520, 326521,
            326522, 326523,
            326524, 326525, 326526, 326527, 326528, 326529, 326530, 326531, 326532, 326533, 326534,
            326535, 326536,
            326537, 326538, 326539, 326540, 326541, 326542, 326543, 326544, 326545, 326546, 326547,
            326548, 326549,
            325592, 325593, 325594, 325595, 325596, 325597, 325598, 325599, 325600, 325601, 325602,
            325603, 325604,
            325605, 325606, 325607, 325608, 325609, 325610, 325611, 325612, 325613, 325614, 325615,
            325616, 325617,
            325618, 325619, 325620, 325621, 325622, 325623, 326060, 326061, 326062, 326063, 326064,
            326065, 326066,
            326067, 326068, 326069, 326070, 326071, 326072, 326073, 326074, 326075, 326076, 326077,
            326078, 326079,
            326080, 326081, 326082, 326083, 326084, 326085, 326086, 326087, 326088, 326089, 326090,
            326091, 326550,
            326551, 326552, 326553, 326554, 326555, 326556, 326557, 326558, 326559, 326560, 326561,
            326562, 326563,
            326564, 326565, 326566, 326567, 326568, 326569, 326570, 326571, 326572, 326573, 326574,
            326575, 326576,
            326577, 326578, 326579, 326580, 326581, 325624, 325625, 325626, 325627, 325628, 325629,
            325630, 325631,
            325632, 325633, 325634, 325635, 325636, 325637, 325638, 325639, 325640, 325641, 325642,
            325643, 325644,
            325645, 325646, 325647, 325648, 325649, 325650, 325651, 325652, 325653, 325654, 325655,
            326092, 326093,
            326094, 326095, 326096, 326097, 326098, 326099, 326100, 326101, 326102, 326103, 326104,
            326105, 326106,
            326107, 326108, 326109, 326110, 326111, 326112, 326113, 326114, 326115, 326116, 326117,
            326118, 326119,
            326120, 326121, 326122, 326123, 326582, 326583, 326584, 326585, 326586, 326587, 326588,
            326589, 326590,
            326591, 326592, 326593, 326594, 326595, 326596, 326597, 326598, 326599, 326600, 326601,
            326602, 326603,
            326604, 326605, 326606, 326607, 326608, 326609, 326610, 326611, 326612, 325656, 325657,
            325658, 325659,
            325660, 325661, 325662, 325663, 325664, 325665, 325666, 325667, 325668, 325669, 325670,
            325671, 325672,
            325673, 325674, 325675, 325676, 325677, 325678, 325679, 325680, 325681, 325682, 325683,
            325684, 325685,
            325686, 325687, 326124, 326125, 326126, 326127, 326128, 326129, 326130, 326131, 326132,
            326133, 326134,
            326135, 326136, 326137, 326138, 326139, 326140, 326141, 326142, 326143, 326144, 326145,
            326146, 326147,
            326148, 326149, 326150, 326151, 326152, 326153, 326154, 326155, 326613, 326614, 326615,
            326616, 326617,
            326618, 326619, 326620, 326621, 326622, 326623, 326624, 326625, 326626, 326627, 326628,
            326629, 326630,
            326631, 326632, 326633, 326634, 326635, 326636, 326637, 326638, 326639, 326640, 326641,
            326642, 326643,
            326156, 326157, 326158, 326159, 326160, 326161, 326162, 326163, 326164, 326165, 326166,
            326167, 326168,
            326169, 326170, 326171, 326172, 326644, 326645, 326646, 326647, 326648, 326649, 326650,
            326651, 326652,
            326653, 326654, 326655, 326656, 326657, 326658, 326659, 326660, 326661, 326662, 326663,
            326664, 326665,
            326666, 326667, 326668, 326669, 326670, 326671, 326672, 325688, 325689, 325690, 325691,
            325692, 325693,
            325694, 325695, 325696, 325697, 325698, 325699, 325700, 325701, 325702, 325703, 325704,
            325705, 325706,
            325707, 325708, 325709, 325710, 325711, 326173, 326174, 326175, 326176, 326177, 326178,
            326179, 326180,
            326181, 326182, 326183, 326184, 326185, 326186, 326187, 326188, 326189, 326190, 326191,
            326192, 326193,
            326194, 326195, 326196, 326197, 326198, 326199, 326200, 326201, 326202, 326203, 326204,
            326673, 326674,
            326675, 326676, 326677, 326678, 326679, 326680, 326681, 326682, 326683, 326684, 326685,
            326686, 326687,
            326688, 326689, 326690, 326691, 326692, 326693, 326694, 326695, 326696, 326697, 326698,
            326699, 326700,
            326701, 326702, 326703, 326704, 325712, 325713, 325714, 325715, 325716, 325717, 325718,
            325719, 325720,
            325721, 325722, 325723, 325724, 325725, 325726, 325727, 325728, 325729, 325730, 325731,
            325732, 325733,
            325734, 325735, 325736, 325737, 325738, 325739, 325740, 325741, 325742, 325743, 326205,
            326206, 326207,
            326208, 326209, 326210, 326211, 326212, 326213, 326214, 326215, 326216, 326217, 326218,
            326219, 326220,
            326221, 326222, 326223, 326224, 326225, 326226, 326227, 326228, 326229, 326230, 326231,
            326232, 326233,
            326234, 326235, 326705, 326706, 326707, 326708, 326709, 326710, 326711, 326712, 326713,
            326714, 326715,
            326716, 326717, 326718, 326719, 326720, 326721, 326722, 326723, 326724, 326725, 326726,
            326727, 326728,
            326729, 326730, 326731, 326732, 326733, 326734, 326735, 326736, 325284, 325285, 325286,
            325287, 325288,
            325289, 325290, 325291, 325292, 325293, 325294, 325295, 325296, 325297, 325298, 325299,
            325300, 325301,
            325302, 325303, 325304, 325305, 325306, 325744, 325745, 325746, 325747, 325748, 325749,
            325750, 325751,
            325752, 325753, 325754, 325755, 325756, 325757, 325758, 325759, 325760, 325761, 325762,
            325763, 325764,
            325765, 325766, 325767, 325768, 325769, 325770, 325771, 325772, 325773, 326236, 326237,
            326238, 326239,
            326240, 326241, 326242, 326243, 326244, 326245, 326246, 326247, 326248, 326249, 326250,
            326251, 326252,
            326253, 326254, 326255, 326256, 326257, 326258, 326259, 326260, 326261, 326262, 326263,
            326264, 326265,
            326266, 326267, 326737, 326738, 326739, 326740, 326741, 326742, 326743, 326744, 326745,
            326746, 326747,
            326748, 326749, 326750, 326751, 326752, 326753, 326754, 326755, 326756, 326757, 326758,
            326759, 326760,
            326761, 326762, 326763, 326764, 326765, 326766, 326767, 325307, 325308, 325309, 325310,
            325311, 325312,
            325313, 325314, 325315, 325316, 325317, 325318, 325319, 325320, 325321, 325322, 325323,
            325324, 325325,
            325326, 325327, 325328, 325329, 325330, 325331, 325332, 325333, 325334, 325335, 325336,
            325337, 325774,
            325775, 325776, 325777, 325778, 325779, 325780, 325781, 325782, 325783, 325784, 325785,
            325786, 325787,
            325788, 325789, 325790, 325791, 325792, 325793, 325794, 325795, 325796, 325797, 325798,
            325799, 325800,
            325801, 325802, 325803, 325804, 326268, 326269, 326270, 326271, 326272, 326273, 326274,
            326275, 326276,
            326277, 326278, 326279, 326280, 326281, 326282, 326283, 326284, 326285, 326286, 326287,
            326288, 326289,
            326290, 326291, 326292, 326293, 326294, 326295, 326296, 326297, 326298, 326768, 326769,
            326770, 326771,
            326772, 326773, 326774, 326775, 326776, 326777, 326778, 326779, 326780, 326781, 326782,
            326783, 326784,
            326785, 326786, 326787, 326788, 326789, 326790, 326791, 326792, 326793, 326794, 326795,
            326796, 326797,
            326798, 325338, 325339, 325340, 325341, 325342, 325343, 325344, 325345, 325346, 325347,
            325348, 325349,
            325350, 325351, 325352, 325353, 325354, 325355, 325356, 325357, 325358, 325359, 325360,
            325361, 325362,
            325363, 325364, 325365, 325366, 325367, 325368, 325369, 325805, 325806, 325807, 325808,
            325809, 325810,
            325811, 325812, 325813, 325814, 325815, 325816, 325817, 325818, 325819, 325820, 325821,
            325822, 325823,
            325824, 325825, 325826, 325827, 325828, 325829, 325830, 325831, 325832, 325833, 325834,
            325835, 326299,
            326300, 326301, 326302, 326303, 326304, 326305, 326306, 326307, 326308, 326309, 326310,
            326311, 326312,
            326313, 326314, 326315, 326316, 326317, 326318, 326319, 326320, 326321, 326322, 326323,
            326324, 326325,
            326326, 326327, 326328, 326329, 326330, 326799, 326800, 326801, 326802, 326803, 326804,
            326805, 326806,
            326807, 326808, 326809, 326810, 326811, 326812, 326813, 326814, 326815, 326816, 326817,
            326818, 326819,
            326820, 326821, 326822, 326823, 326824, 326825, 326826, 326827, 326828, 326829, 325370,
            325371, 325372,
            325373, 325374, 325375, 325376, 325377, 325378, 325379, 325380, 325381, 325382, 325383,
            325384, 325385,
            325386, 325387, 325388, 325389, 325390, 325391, 325392, 325393, 325394, 325395, 325396,
            325397, 325398,
            325399, 325400, 325401, 325836, 325837, 325838, 325839, 325840, 325841, 325842, 325843,
            325844, 325845,
            325846, 325847, 325848, 325849, 325850, 325851, 325852, 325853, 325854, 325855, 325856,
            325857, 325858,
            325859, 325860, 325861, 325862, 325863, 325864, 325865, 325866, 325867, 326331, 326332,
            326333, 326334,
            326335, 326336, 326337, 326338, 326339, 326340, 326341, 326342, 326343, 326344, 326345,
            326346, 326347,
            326348, 326349, 326350, 326351, 326352, 326353, 326354, 326355, 326356, 326357, 326358,
            326359, 326360,
            326361, 326830, 326831, 326832, 326833, 326834, 326835, 326836, 326837, 326838, 326839,
            326840, 326841,
            326842, 326843, 326844, 326845, 326846, 326847, 326848, 326849, 326850, 326851, 326852,
            326853, 326854,
            326855, 326856, 326857, 326858, 326859, 326860, 325402, 325403, 325404, 325405, 325406,
            325407, 325408,
            325409, 325410, 325411, 325412, 325413, 325414, 325415, 325416, 325417, 325418, 325419,
            325420, 325421,
            325422, 325423, 325424, 325425, 325426, 325427, 325428, 325429, 325430, 325431, 325432,
            325433, 325868,
            325869, 325870, 325871, 325872, 325873, 325874, 325875, 325876, 325877, 325878, 325879,
            325880, 325881,
            325882, 325883, 325884, 325885, 325886, 325887, 325888, 325889, 325890, 325891, 325892,
            325893, 325894,
            325895, 325896, 325897, 325898, 325899, 326362, 326363, 326364, 326365, 326366, 326367,
            326368, 326369,
            326370, 326371, 326372, 326373, 326374, 326375, 326376, 326377, 326378, 326379, 326380,
            326381, 326382,
            326383, 326384, 326385, 326386, 326387, 326388, 326389, 326390, 326391, 326392, 326861,
            326862, 326863,
            326864, 326865, 326866, 326867, 326868, 326869, 326870, 326871, 326872, 326873, 326874,
            326875, 326876,
            326877, 326878, 326879, 326880, 326881, 326882, 326883, 326884, 326885, 326886, 326887,
            326888, 326889,
            326890, 326891, 326892, 325434, 325435, 325436, 325437, 325438, 325439, 325440, 325441,
            325442, 325443,
            325444, 325445, 325446, 325447, 325448, 325449, 325450, 325451, 325452, 325453, 325454,
            325455, 325456,
            325457, 325458, 325459, 325460, 325461, 325462, 325463, 325464, 325900, 325901, 325902,
            325903, 325904,
            325905, 325906, 325907, 325908, 325909, 325910, 325911, 325912, 325913, 325914, 325915,
            325916, 325917,
            325918, 325919, 325920, 325921, 325922, 325923, 325924, 325925, 325926, 325927, 325928,
            325929, 325930,
            325931, 326393, 326394, 326395, 326396, 326397, 326398, 326399, 326400, 326401, 326402,
            326403, 326404,
            326405, 326406, 326407, 326408, 326409, 326410, 326411, 326412, 326413, 326414, 326415,
            326416, 326417,
            326418, 326419, 326420, 326421, 326422, 326423, 326893, 326894, 326895, 326896, 326897,
            326898, 326899,
            326900, 326901, 326902, 326903, 326904, 326905, 326906, 326907, 326908, 326909, 326910,
            326911, 326912,
            326913, 326914, 326915, 326916, 326917, 326918, 326919, 326920, 326921, 326922, 326923,
            326924, 325465,
            325466, 325467, 325468, 325469, 325470, 325471, 325472, 325473, 325474, 325475, 325476,
            325477, 325478,
            325479, 325480, 325481, 325482, 325483, 325484, 325485, 325486, 325487, 325488, 325489,
            325490, 325491,
            325492, 325493, 325494, 325495, 325496, 325932, 325933, 325934, 325935, 325936, 325937,
            325938, 325939,
            325940, 325941, 325942, 325943, 325944, 325945, 325946, 325947, 325948, 325949, 325950,
            325951, 325952,
            325953, 325954, 325955, 325956, 325957, 325958, 325959, 325960, 325961, 325962, 325963,
            326424, 326425,
            326426, 326427, 326428, 326429, 326430, 326431, 326432, 326433, 326434, 326435, 326436,
            326437, 326438,
            326439, 326440, 326441, 326442, 326443, 326444, 326445, 326446, 326447, 326448, 326449,
            326450, 326451,
            326452, 326453, 326454, 326925, 326926, 326927, 326928, 326929, 326930, 326931, 326932,
            326933, 326934,
            326935, 326936, 326937, 326938, 326939, 325497, 325498, 325499, 325500, 325501, 325502,
            325503, 325504,
            325505, 325506, 325507, 325508, 325509, 325510, 325511, 325512, 325513, 325514, 325515,
            325516, 325517,
            325518, 325519, 325520, 325521, 325522, 325523, 325524, 325525, 325526, 325527, 325528,
            325964, 325965,
            325966, 325967, 325968, 325969, 325970, 325971, 325972, 325973, 325974, 325975, 325976,
            325977, 325978,
            325979, 325980, 325981, 325982, 325983, 325984, 325985, 325986, 325987, 325988, 325989,
            325990, 325991,
            325992, 325993, 325994, 325995, 326455, 326456, 326457, 326458, 326459, 326460, 326461,
            326462, 326463,
            326464, 326465, 326466, 326467, 326468, 326469, 326470, 326471, 326472, 326473, 326474,
            326475, 326476,
            326477, 326478, 326479, 326480, 326481, 326482, 326483, 326484, 326485, 325529, 325530,
            325531, 325532,
            325533, 325534, 325535, 325536, 325537, 325538, 325539, 325540, 325541, 325542, 325543,
            325544, 325545,
            325546, 325547, 325548, 325549, 325550, 325551, 325552, 325553, 325554, 325555, 325556,
            325557, 325558,
            325559, 325996, 325997, 325998, 325999, 326000, 326001, 326002, 326003, 326004, 326005,
            326006, 326007,
            326008, 326009, 326010, 326011, 326012, 326013, 326014, 326015, 326016, 326017, 326018,
            326019, 326020,
            326021, 326022, 326023, 326024, 326025, 326026, 326027, 326486, 326487, 326488, 326489,
            326490, 326491,
            326492, 326493, 326494, 326495, 326496, 326497, 326498, 326499, 326500, 326501, 326502,
            326503, 326504,
            326505, 326506, 326507, 326508, 326509, 326510, 326511, 326512, 326513, 326514, 326515,
            326516, 326517,
            325560, 325561, 325562, 325563, 325564, 325565, 325566, 325567, 325568, 325569, 325570,
            325571, 325572,
            325573, 325574, 325575, 325576, 325577, 325578, 325579, 325580, 325581, 325582, 325583,
            325584, 325585,
            325586, 325587, 325588, 325589, 325590, 325591};

        try {
            LOGGER.debug("**************** INICIO *************************");
            List<Tramite> result = dao.buscarTramitesActualizacionAgrupadoPorPredio(ids);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author leidy.gonzalez
     */
    @Test
    public void testBuscarTramitesConservacionPorIdTramites() throws ParseException {
        LOGGER.debug("tests: TramiteDAOBeanTests#testBuscarTramitesConservacionPorIdTramites");

        List<Long> ids = new ArrayList<Long>();
        ids.add(326028l);
        ids.add(326029l);
        ids.add(326030l);
        ids.add(326031l);
        ids.add(326032l);
        ids.add(326033l);
        ids.add(326034l);
        ids.add(326035l);
        ids.add(326036l);
        ids.add(326037l);
        ids.add(326038l);
        ids.add(326039l);
        ids.add(326040l);
        ids.add(326041l);
        ids.add(326042l);
        ids.add(326043l);
        ids.add(326044l);
        ids.add(326045l);
        ids.add(326046l);
        ids.add(326047l);
        ids.add(326048l);
        ids.add(326049l);
        ids.add(326050l);
        ids.add(326051l);
        ids.add(326052l);
        ids.add(326053l);
        ids.add(326054l);
        ids.add(326055l);
        ids.add(326056l);
        ids.add(326057l);
        ids.add(326058l);
        ids.add(326059l);
        ids.add(326518l);
        ids.add(326519l);
        ids.add(326520l);
        ids.add(326521l);
        ids.add(326522l);
        ids.add(326523l);
        ids.add(326524l);
        ids.add(326525l);
        ids.add(326526l);
        ids.add(326527l);
        ids.add(326528l);
        ids.add(326529l);
        ids.add(326530l);
        ids.add(326531l);
        ids.add(326532l);
        ids.add(326533l);
        ids.add(326534l);
        ids.add(326535l);
        ids.add(326536l);
        ids.add(326537l);
        ids.add(326538l);
        ids.add(326539l);
        ids.add(326540l);
        ids.add(326541l);
        ids.add(326542l);
        ids.add(326543l);
        ids.add(326544l);
        ids.add(326545l);
        ids.add(326546l);
        ids.add(326547l);
        ids.add(326548l);
        ids.add(326549l);
        ids.add(325592l);
        ids.add(325593l);
        ids.add(325594l);
        ids.add(325595l);
        ids.add(325596l);
        ids.add(325597l);
        ids.add(325598l);
        ids.add(325599l);
        ids.add(325600l);
        ids.add(325601l);
        ids.add(325602l);
        ids.add(325603l);
        ids.add(325604l);
        ids.add(325605l);
        ids.add(325606l);
        ids.add(325607l);
        ids.add(325608l);
        ids.add(325609l);
        ids.add(325610l);
        ids.add(325611l);
        ids.add(325612l);
        ids.add(325613l);
        ids.add(325614l);
        ids.add(325615l);
        ids.add(325616l);
        ids.add(325617l);
        ids.add(325618l);
        ids.add(325619l);
        ids.add(325620l);
        ids.add(325621l);
        ids.add(325622l);
        ids.add(325623l);
        ids.add(326060l);
        ids.add(326061l);
        ids.add(326062l);
        ids.add(326063l);
        ids.add(326064l);
        ids.add(326065l);
        ids.add(326066l);
        ids.add(326067l);
        ids.add(326068l);
        ids.add(326069l);
        ids.add(326070l);
        ids.add(326071l);
        ids.add(326072l);
        ids.add(326073l);
        ids.add(326074l);
        ids.add(326075l);
        ids.add(326076l);
        ids.add(326077l);
        ids.add(326078l);
        ids.add(326079l);
        ids.add(326080l);
        ids.add(326081l);
        ids.add(326082l);
        ids.add(326083l);
        ids.add(326084l);
        ids.add(326085l);
        ids.add(326086l);
        ids.add(326087l);
        ids.add(326088l);
        ids.add(326089l);
        ids.add(326090l);
        ids.add(326091l);
        ids.add(326550l);
        ids.add(326551l);
        ids.add(326552l);
        ids.add(326553l);
        ids.add(326554l);
        ids.add(326555l);
        ids.add(326556l);
        ids.add(326557l);
        ids.add(326558l);
        ids.add(326559l);
        ids.add(326560l);
        ids.add(326561l);
        ids.add(326562l);
        ids.add(326563l);
        ids.add(326564l);
        ids.add(326565l);
        ids.add(326566l);
        ids.add(326567l);
        ids.add(326568l);
        ids.add(326569l);
        ids.add(326570l);
        ids.add(326571l);
        ids.add(326572l);
        ids.add(326573l);
        ids.add(326574l);
        ids.add(326575l);
        ids.add(326576l);
        ids.add(326577l);
        ids.add(326578l);
        ids.add(326579l);
        ids.add(326580l);
        ids.add(326581l);
        ids.add(325624l);
        ids.add(325625l);
        ids.add(325626l);
        ids.add(325627l);
        ids.add(325628l);
        ids.add(325629l);
        ids.add(325630l);
        ids.add(325631l);
        ids.add(325632l);
        ids.add(325633l);
        ids.add(325634l);
        ids.add(325635l);
        ids.add(325636l);
        ids.add(325637l);
        ids.add(325638l);
        ids.add(325639l);
        ids.add(325640l);
        ids.add(325641l);
        ids.add(325642l);
        ids.add(325643l);
        ids.add(325644l);
        ids.add(325645l);
        ids.add(325646l);
        ids.add(325647l);
        ids.add(325648l);
        ids.add(325649l);
        ids.add(325650l);
        ids.add(325651l);
        ids.add(325652l);
        ids.add(325653l);
        ids.add(325654l);
        ids.add(325655l);
        ids.add(326092l);
        ids.add(326093l);
        ids.add(326094l);
        ids.add(326095l);
        ids.add(326096l);
        ids.add(326097l);
        ids.add(326098l);
        ids.add(326099l);
        ids.add(326100l);
        ids.add(326101l);
        ids.add(326102l);
        ids.add(326103l);
        ids.add(326104l);
        ids.add(326105l);
        ids.add(326106l);
        ids.add(326107l);
        ids.add(326108l);
        ids.add(326109l);
        ids.add(326110l);
        ids.add(326111l);
        ids.add(326112l);
        ids.add(326113l);
        ids.add(326114l);
        ids.add(326115l);
        ids.add(326116l);
        ids.add(326117l);
        ids.add(326118l);
        ids.add(326119l);
        ids.add(326120l);
        ids.add(326121l);
        ids.add(326122l);
        ids.add(326123l);
        ids.add(326582l);
        ids.add(326583l);
        ids.add(326584l);
        ids.add(326585l);
        ids.add(326586l);
        ids.add(326587l);
        ids.add(326588l);
        ids.add(326589l);
        ids.add(326590l);
        ids.add(326591l);
        ids.add(326592l);
        ids.add(326593l);
        ids.add(326594l);
        ids.add(326595l);
        ids.add(326596l);
        ids.add(326597l);
        ids.add(326598l);
        ids.add(326599l);
        ids.add(326600l);
        ids.add(326601l);
        ids.add(326602l);
        ids.add(326603l);
        ids.add(326604l);
        ids.add(326605l);
        ids.add(326606l);
        ids.add(326607l);
        ids.add(326608l);
        ids.add(326609l);
        ids.add(326610l);
        ids.add(326611l);
        ids.add(326612l);
        ids.add(325656l);
        ids.add(325657l);
        ids.add(325658l);
        ids.add(325659l);
        ids.add(325660l);
        ids.add(325661l);
        ids.add(325662l);
        ids.add(325663l);
        ids.add(325664l);
        ids.add(325665l);
        ids.add(325666l);
        ids.add(325667l);
        ids.add(325668l);
        ids.add(325669l);
        ids.add(325670l);
        ids.add(325671l);
        ids.add(325672l);
        ids.add(325673l);
        ids.add(325674l);
        ids.add(325675l);
        ids.add(325676l);
        ids.add(325677l);
        ids.add(325678l);
        ids.add(325679l);
        ids.add(325680l);
        ids.add(325681l);
        ids.add(325682l);
        ids.add(325683l);
        ids.add(325684l);
        ids.add(325685l);
        ids.add(325686l);
        ids.add(325687l);
        ids.add(326124l);
        ids.add(326125l);
        ids.add(326126l);
        ids.add(326127l);
        ids.add(326128l);
        ids.add(326129l);
        ids.add(326130l);
        ids.add(326131l);
        ids.add(326132l);
        ids.add(326133l);
        ids.add(326134l);
        ids.add(326135l);
        ids.add(326136l);
        ids.add(326137l);
        ids.add(326138l);
        ids.add(326139l);
        ids.add(326140l);
        ids.add(326141l);
        ids.add(326142l);
        ids.add(326143l);
        ids.add(326144l);
        ids.add(326145l);
        ids.add(326146l);
        ids.add(326147l);
        ids.add(326148l);
        ids.add(326149l);
        ids.add(326150l);
        ids.add(326151l);
        ids.add(326152l);
        ids.add(326153l);
        ids.add(326154l);
        ids.add(326155l);
        ids.add(326613l);
        ids.add(326614l);
        ids.add(326615l);
        ids.add(326616l);
        ids.add(326617l);
        ids.add(326618l);
        ids.add(326619l);
        ids.add(326620l);
        ids.add(326621l);
        ids.add(326622l);
        ids.add(326623l);
        ids.add(326624l);
        ids.add(326625l);
        ids.add(326626l);
        ids.add(326627l);
        ids.add(326628l);
        ids.add(326629l);
        ids.add(326630l);
        ids.add(326631l);
        ids.add(326632l);
        ids.add(326633l);
        ids.add(326634l);
        ids.add(326635l);
        ids.add(326636l);
        ids.add(326637l);
        ids.add(326638l);
        ids.add(326639l);
        ids.add(326640l);
        ids.add(326641l);
        ids.add(326642l);
        ids.add(326643l);
        ids.add(326156l);
        ids.add(326157l);
        ids.add(326158l);
        ids.add(326159l);
        ids.add(326160l);
        ids.add(326161l);
        ids.add(326162l);
        ids.add(326163l);
        ids.add(326164l);
        ids.add(326165l);
        ids.add(326166l);
        ids.add(326167l);
        ids.add(326168l);
        ids.add(326169l);
        ids.add(326170l);
        ids.add(326171l);
        ids.add(326172l);
        ids.add(326644l);
        ids.add(326645l);
        ids.add(326646l);
        ids.add(326647l);
        ids.add(326648l);
        ids.add(326649l);
        ids.add(326650l);
        ids.add(326651l);
        ids.add(326652l);
        ids.add(326653l);
        ids.add(326654l);
        ids.add(326655l);
        ids.add(326656l);
        ids.add(326657l);
        ids.add(326658l);
        ids.add(326659l);
        ids.add(326660l);
        ids.add(326661l);
        ids.add(326662l);
        ids.add(326663l);
        ids.add(326664l);
        ids.add(326665l);
        ids.add(326666l);
        ids.add(326667l);
        ids.add(326668l);
        ids.add(326669l);
        ids.add(326670l);
        ids.add(326671l);
        ids.add(326672l);
        ids.add(325688l);
        ids.add(325689l);
        ids.add(325690l);
        ids.add(325691l);
        ids.add(325692l);
        ids.add(325693l);
        ids.add(325694l);
        ids.add(325695l);
        ids.add(325696l);
        ids.add(325697l);
        ids.add(325698l);
        ids.add(325699l);
        ids.add(325700l);
        ids.add(325701l);
        ids.add(325702l);
        ids.add(325703l);
        ids.add(325704l);
        ids.add(325705l);
        ids.add(325706l);
        ids.add(325707l);
        ids.add(325708l);
        ids.add(325709l);
        ids.add(325710l);
        ids.add(325711l);
        ids.add(326173l);
        ids.add(326174l);
        ids.add(326175l);
        ids.add(326176l);
        ids.add(326177l);
        ids.add(326178l);
        ids.add(326179l);
        ids.add(326180l);
        ids.add(326181l);
        ids.add(326182l);
        ids.add(326183l);
        ids.add(326184l);
        ids.add(326185l);
        ids.add(326186l);
        ids.add(326187l);
        ids.add(326188l);
        ids.add(326189l);
        ids.add(326190l);
        ids.add(326191l);
        ids.add(326192l);
        ids.add(326193l);
        ids.add(326194l);
        ids.add(326195l);
        ids.add(326196l);
        ids.add(326197l);
        ids.add(326198l);
        ids.add(326199l);
        ids.add(326200l);
        ids.add(326201l);
        ids.add(326202l);
        ids.add(326203l);
        ids.add(326204l);
        ids.add(326673l);
        ids.add(326674l);
        ids.add(326675l);
        ids.add(326676l);
        ids.add(326677l);
        ids.add(326678l);
        ids.add(326679l);
        ids.add(326680l);
        ids.add(326681l);
        ids.add(326682l);
        ids.add(326683l);
        ids.add(326684l);
        ids.add(326685l);
        ids.add(326686l);
        ids.add(326687l);
        ids.add(326688l);
        ids.add(326689l);
        ids.add(326690l);
        ids.add(326691l);
        ids.add(326692l);
        ids.add(326693l);
        ids.add(326694l);
        ids.add(326695l);
        ids.add(326696l);
        ids.add(326697l);
        ids.add(326698l);
        ids.add(326699l);
        ids.add(326700l);
        ids.add(326701l);
        ids.add(326702l);
        ids.add(326703l);
        ids.add(326704l);
        ids.add(325712l);
        ids.add(325713l);
        ids.add(325714l);
        ids.add(325715l);
        ids.add(325716l);
        ids.add(325717l);
        ids.add(325718l);
        ids.add(325719l);
        ids.add(325720l);
        ids.add(325721l);
        ids.add(325722l);
        ids.add(325723l);
        ids.add(325724l);
        ids.add(325725l);
        ids.add(325726l);
        ids.add(325727l);
        ids.add(325728l);
        ids.add(325729l);
        ids.add(325730l);
        ids.add(325731l);
        ids.add(325732l);
        ids.add(325733l);
        ids.add(325734l);
        ids.add(325735l);
        ids.add(325736l);
        ids.add(325737l);
        ids.add(325738l);
        ids.add(325739l);
        ids.add(325740l);
        ids.add(325741l);
        ids.add(325742l);
        ids.add(325743l);
        ids.add(326205l);
        ids.add(326206l);
        ids.add(326207l);
        ids.add(326208l);
        ids.add(326209l);
        ids.add(326210l);
        ids.add(326211l);
        ids.add(326212l);
        ids.add(326213l);
        ids.add(326214l);
        ids.add(326215l);
        ids.add(326216l);
        ids.add(326217l);
        ids.add(326218l);
        ids.add(326219l);
        ids.add(326220l);
        ids.add(326221l);
        ids.add(326222l);
        ids.add(326223l);
        ids.add(326224l);
        ids.add(326225l);
        ids.add(326226l);
        ids.add(326227l);
        ids.add(326228l);
        ids.add(326229l);
        ids.add(326230l);
        ids.add(326231l);
        ids.add(326232l);
        ids.add(326233l);
        ids.add(326234l);
        ids.add(326235l);
        ids.add(326705l);
        ids.add(326706l);
        ids.add(326707l);
        ids.add(326708l);
        ids.add(326709l);
        ids.add(326710l);
        ids.add(326711l);
        ids.add(326712l);
        ids.add(326713l);
        ids.add(326714l);
        ids.add(326715l);
        ids.add(326716l);
        ids.add(326717l);
        ids.add(326718l);
        ids.add(326719l);
        ids.add(326720l);
        ids.add(326721l);
        ids.add(326722l);
        ids.add(326723l);
        ids.add(326724l);
        ids.add(326725l);
        ids.add(326726l);
        ids.add(326727l);
        ids.add(326728l);
        ids.add(326729l);
        ids.add(326730l);
        ids.add(326731l);
        ids.add(326732l);
        ids.add(326733l);
        ids.add(326734l);
        ids.add(326735l);
        ids.add(326736l);
        ids.add(325284l);
        ids.add(325285l);
        ids.add(325286l);
        ids.add(325287l);
        ids.add(325288l);
        ids.add(325289l);
        ids.add(325290l);
        ids.add(325291l);
        ids.add(325292l);
        ids.add(325293l);
        ids.add(325294l);
        ids.add(325295l);
        ids.add(325296l);
        ids.add(325297l);
        ids.add(325298l);
        ids.add(325299l);
        ids.add(325300l);
        ids.add(325301l);
        ids.add(325302l);
        ids.add(325303l);
        ids.add(325304l);
        ids.add(325305l);
        ids.add(325306l);
        ids.add(325744l);
        ids.add(325745l);
        ids.add(325746l);
        ids.add(325747l);
        ids.add(325748l);
        ids.add(325749l);
        ids.add(325750l);
        ids.add(325751l);
        ids.add(325752l);
        ids.add(325753l);
        ids.add(325754l);
        ids.add(325755l);
        ids.add(325756l);
        ids.add(325757l);
        ids.add(325758l);
        ids.add(325759l);
        ids.add(325760l);
        ids.add(325761l);
        ids.add(325762l);
        ids.add(325763l);
        ids.add(325764l);
        ids.add(325765l);
        ids.add(325766l);
        ids.add(325767l);
        ids.add(325768l);
        ids.add(325769l);
        ids.add(325770l);
        ids.add(325771l);
        ids.add(325772l);
        ids.add(325773l);
        ids.add(326236l);
        ids.add(326237l);
        ids.add(326238l);
        ids.add(326239l);
        ids.add(326240l);
        ids.add(326241l);
        ids.add(326242l);
        ids.add(326243l);
        ids.add(326244l);
        ids.add(326245l);
        ids.add(326246l);
        ids.add(326247l);
        ids.add(326248l);
        ids.add(326249l);
        ids.add(326250l);
        ids.add(326251l);
        ids.add(326252l);
        ids.add(326253l);
        ids.add(326254l);
        ids.add(326255l);
        ids.add(326256l);
        ids.add(326257l);
        ids.add(326258l);
        ids.add(326259l);
        ids.add(326260l);
        ids.add(326261l);
        ids.add(326262l);
        ids.add(326263l);
        ids.add(326264l);
        ids.add(326265l);
        ids.add(326266l);
        ids.add(326267l);
        ids.add(326737l);
        ids.add(326738l);
        ids.add(326739l);
        ids.add(326740l);
        ids.add(326741l);
        ids.add(326742l);
        ids.add(326743l);
        ids.add(326744l);
        ids.add(326745l);
        ids.add(326746l);
        ids.add(326747l);
        ids.add(326748l);
        ids.add(326749l);
        ids.add(326750l);
        ids.add(326751l);
        ids.add(326752l);
        ids.add(326753l);
        ids.add(326754l);
        ids.add(326755l);
        ids.add(326756l);
        ids.add(326757l);
        ids.add(326758l);
        ids.add(326759l);
        ids.add(326760l);
        ids.add(326761l);
        ids.add(326762l);
        ids.add(326763l);
        ids.add(326764l);
        ids.add(326765l);
        ids.add(326766l);
        ids.add(326767l);
        ids.add(325307l);
        ids.add(325308l);
        ids.add(325309l);
        ids.add(325310l);
        ids.add(325311l);
        ids.add(325312l);
        ids.add(325313l);
        ids.add(325314l);
        ids.add(325315l);
        ids.add(325316l);
        ids.add(325317l);
        ids.add(325318l);
        ids.add(325319l);
        ids.add(325320l);
        ids.add(325321l);
        ids.add(325322l);
        ids.add(325323l);
        ids.add(325324l);
        ids.add(325325l);
        ids.add(325326l);
        ids.add(325327l);
        ids.add(325328l);
        ids.add(325329l);
        ids.add(325330l);
        ids.add(325331l);
        ids.add(325332l);
        ids.add(325333l);
        ids.add(325334l);
        ids.add(325335l);
        ids.add(325336l);
        ids.add(325337l);
        ids.add(325774l);
        ids.add(325775l);
        ids.add(325776l);
        ids.add(325777l);
        ids.add(325778l);
        ids.add(325779l);
        ids.add(325780l);
        ids.add(325781l);
        ids.add(325782l);
        ids.add(325783l);
        ids.add(325784l);
        ids.add(325785l);
        ids.add(325786l);
        ids.add(325787l);
        ids.add(325788l);
        ids.add(325789l);
        ids.add(325790l);
        ids.add(325791l);
        ids.add(325792l);
        ids.add(325793l);
        ids.add(325794l);
        ids.add(325795l);
        ids.add(325796l);
        ids.add(325797l);
        ids.add(325798l);
        ids.add(325799l);
        ids.add(325800l);
        ids.add(325801l);
        ids.add(325802l);
        ids.add(325803l);
        ids.add(325804l);
        ids.add(326268l);
        ids.add(326269l);
        ids.add(326270l);
        ids.add(326271l);
        ids.add(326272l);
        ids.add(326273l);
        ids.add(326274l);
        ids.add(326275l);
        ids.add(326276l);
        ids.add(326277l);
        ids.add(326278l);
        ids.add(326279l);
        ids.add(326280l);
        ids.add(326281l);
        ids.add(326282l);
        ids.add(326283l);
        ids.add(326284l);
        ids.add(326285l);
        ids.add(326286l);
        ids.add(326287l);
        ids.add(326288l);
        ids.add(326289l);
        ids.add(326290l);
        ids.add(326291l);
        ids.add(326292l);
        ids.add(326293l);
        ids.add(326294l);
        ids.add(326295l);
        ids.add(326296l);
        ids.add(326297l);
        ids.add(326298l);
        ids.add(326768l);
        ids.add(326769l);
        ids.add(326770l);
        ids.add(326771l);
        ids.add(326772l);
        ids.add(326773l);
        ids.add(326774l);
        ids.add(326775l);
        ids.add(326776l);
        ids.add(326777l);
        ids.add(326778l);
        ids.add(326779l);
        ids.add(326780l);
        ids.add(326781l);
        ids.add(326782l);
        ids.add(326783l);
        ids.add(326784l);
        ids.add(326785l);
        ids.add(326786l);
        ids.add(326787l);
        ids.add(326788l);
        ids.add(326789l);
        ids.add(326790l);
        ids.add(326791l);
        ids.add(326792l);
        ids.add(326793l);
        ids.add(326794l);
        ids.add(326795l);
        ids.add(326796l);
        ids.add(326797l);
        ids.add(326798l);
        ids.add(325338l);
        ids.add(325339l);
        ids.add(325340l);
        ids.add(325341l);
        ids.add(325342l);
        ids.add(325343l);
        ids.add(325344l);
        ids.add(325345l);
        ids.add(325346l);
        ids.add(325347l);
        ids.add(325348l);
        ids.add(325349l);
        ids.add(325350l);
        ids.add(325351l);
        ids.add(325352l);
        ids.add(325353l);
        ids.add(325354l);
        ids.add(325355l);
        ids.add(325356l);
        ids.add(325357l);
        ids.add(325358l);
        ids.add(325359l);
        ids.add(325360l);
        ids.add(325361l);
        ids.add(325362l);
        ids.add(325363l);
        ids.add(325364l);
        ids.add(325365l);
        ids.add(325366l);
        ids.add(325367l);
        ids.add(325368l);
        ids.add(325369l);
        ids.add(325805l);
        ids.add(325806l);
        ids.add(325807l);
        ids.add(325808l);
        ids.add(325809l);
        ids.add(325810l);
        ids.add(325811l);
        ids.add(325812l);
        ids.add(325813l);
        ids.add(325814l);
        ids.add(325815l);
        ids.add(325816l);
        ids.add(325817l);
        ids.add(325818l);
        ids.add(325819l);
        ids.add(325820l);
        ids.add(325821l);
        ids.add(325822l);
        ids.add(325823l);
        ids.add(325824l);
        ids.add(325825l);
        ids.add(325826l);
        ids.add(325827l);
        ids.add(325828l);
        ids.add(325829l);
        ids.add(325830l);
        ids.add(325831l);
        ids.add(325832l);
        ids.add(325833l);
        ids.add(325834l);
        ids.add(325835l);
        ids.add(326299l);
        ids.add(326300l);
        ids.add(326301l);
        ids.add(326302l);
        ids.add(326303l);
        ids.add(326304l);
        ids.add(326305l);
        ids.add(326306l);
        ids.add(326307l);
        ids.add(326308l);
        ids.add(326309l);
        ids.add(326310l);
        ids.add(326311l);
        ids.add(326312l);
        ids.add(326313l);
        ids.add(326314l);
        ids.add(326315l);
        ids.add(326316l);
        ids.add(326317l);
        ids.add(326318l);
        ids.add(326319l);
        ids.add(326320l);
        ids.add(326321l);
        ids.add(326322l);
        ids.add(326323l);
        ids.add(326324l);
        ids.add(326325l);
        ids.add(326326l);
        ids.add(326327l);
        ids.add(326328l);
        ids.add(326329l);
        ids.add(326330l);
        ids.add(326799l);
        ids.add(326800l);
        ids.add(326801l);
        ids.add(326802l);
        ids.add(326803l);
        ids.add(326804l);
        ids.add(326805l);
        ids.add(326806l);
        ids.add(326807l);
        ids.add(326808l);
        ids.add(326809l);
        ids.add(326810l);
        ids.add(326811l);
        ids.add(326812l);
        ids.add(326813l);
        ids.add(326814l);
        ids.add(326815l);
        ids.add(326816l);
        ids.add(326817l);
        ids.add(326818l);
        ids.add(326819l);
        ids.add(326820l);
        ids.add(326821l);
        ids.add(326822l);
        ids.add(326823l);
        ids.add(326824l);
        ids.add(326825l);
        ids.add(326826l);
        ids.add(326827l);
        ids.add(326828l);
        ids.add(326829l);
        ids.add(325370l);
        ids.add(325371l);
        ids.add(325372l);
        ids.add(325373l);
        ids.add(325374l);
        ids.add(325375l);
        ids.add(325376l);
        ids.add(325377l);
        ids.add(325378l);
        ids.add(325379l);
        ids.add(325380l);
        ids.add(325381l);
        ids.add(325382l);
        ids.add(325383l);
        ids.add(325384l);
        ids.add(325385l);
        ids.add(325386l);
        ids.add(325387l);
        ids.add(325388l);
        ids.add(325389l);
        ids.add(325390l);
        ids.add(325391l);
        ids.add(325392l);
        ids.add(325393l);
        ids.add(325394l);
        ids.add(325395l);
        ids.add(325396l);
        ids.add(325397l);
        ids.add(325398l);
        ids.add(325399l);
        ids.add(325400l);
        ids.add(325401l);
        ids.add(325836l);
        ids.add(325837l);
        ids.add(325838l);
        ids.add(325839l);
        ids.add(325840l);
        ids.add(325841l);
        ids.add(325842l);
        ids.add(325843l);
        ids.add(325844l);
        ids.add(325845l);
        ids.add(325846l);
        ids.add(325847l);
        ids.add(325848l);
        ids.add(325849l);
        ids.add(325850l);
        ids.add(325851l);
        ids.add(325852l);
        ids.add(325853l);
        ids.add(325854l);
        ids.add(325855l);
        ids.add(325856l);
        ids.add(325857l);
        ids.add(325858l);
        ids.add(325859l);
        ids.add(325860l);
        ids.add(325861l);
        ids.add(325862l);
        ids.add(325863l);
        ids.add(325864l);
        ids.add(325865l);
        ids.add(325866l);
        ids.add(325867l);
        ids.add(326331l);
        ids.add(326332l);
        ids.add(326333l);
        ids.add(326334l);
        ids.add(326335l);
        ids.add(326336l);
        ids.add(326337l);
        ids.add(326338l);
        ids.add(326339l);
        ids.add(326340l);
        ids.add(326341l);
        ids.add(326342l);
        ids.add(326343l);
        ids.add(326344l);
        ids.add(326345l);
        ids.add(326346l);
        ids.add(326347l);
        ids.add(326348l);
        ids.add(326349l);
        ids.add(326350l);
        ids.add(326351l);
        ids.add(326352l);
        ids.add(326353l);
        ids.add(326354l);
        ids.add(326355l);
        ids.add(326356l);
        ids.add(326357l);
        ids.add(326358l);
        ids.add(326359l);
        ids.add(326360l);
        ids.add(326361l);
        ids.add(326830l);
        ids.add(326831l);
        ids.add(326832l);
        ids.add(326833l);
        ids.add(326834l);
        ids.add(326835l);
        ids.add(326836l);
        ids.add(326837l);
        ids.add(326838l);
        ids.add(326839l);
        ids.add(326840l);
        ids.add(326841l);
        ids.add(326842l);
        ids.add(326843l);
        ids.add(326844l);
        ids.add(326845l);
        ids.add(326846l);
        ids.add(326847l);
        ids.add(326848l);
        ids.add(326849l);
        ids.add(326850l);
        ids.add(326851l);
        ids.add(326852l);
        ids.add(326853l);
        ids.add(326854l);
        ids.add(326855l);
        ids.add(326856l);
        ids.add(326857l);
        ids.add(326858l);
        ids.add(326859l);
        ids.add(326860l);
        ids.add(325402l);
        ids.add(325403l);
        ids.add(325404l);
        ids.add(325405l);
        ids.add(325406l);
        ids.add(325407l);
        ids.add(325408l);
        ids.add(325409l);
        ids.add(325410l);
        ids.add(325411l);
        ids.add(325412l);
        ids.add(325413l);
        ids.add(325414l);
        ids.add(325415l);
        ids.add(325416l);
        ids.add(325417l);
        ids.add(325418l);
        ids.add(325419l);
        ids.add(325420l);
        ids.add(325421l);
        ids.add(325422l);
        ids.add(325423l);
        ids.add(325424l);
        ids.add(325425l);
        ids.add(325426l);
        ids.add(325427l);
        ids.add(325428l);
        ids.add(325429l);
        ids.add(325430l);
        ids.add(325431l);
        ids.add(325432l);
        ids.add(325433l);
        ids.add(325868l);
        ids.add(325869l);
        ids.add(325870l);
        ids.add(325871l);
        ids.add(325872l);
        ids.add(325873l);
        ids.add(325874l);
        ids.add(325875l);
        ids.add(325876l);
        ids.add(325877l);
        ids.add(325878l);
        ids.add(325879l);
        ids.add(325880l);
        ids.add(325881l);
        ids.add(325882l);
        ids.add(325883l);
        ids.add(325884l);
        ids.add(325885l);
        ids.add(325886l);
        ids.add(325887l);
        ids.add(325888l);
        ids.add(325889l);
        ids.add(325890l);
        ids.add(325891l);
        ids.add(325892l);
        ids.add(325893l);
        ids.add(325894l);
        ids.add(325895l);
        ids.add(325896l);
        ids.add(325897l);
        ids.add(325898l);
        ids.add(325899l);
        ids.add(326362l);
        ids.add(326363l);
        ids.add(326364l);
        ids.add(326365l);
        ids.add(326366l);
        ids.add(326367l);
        ids.add(326368l);
        ids.add(326369l);
        ids.add(326370l);
        ids.add(326371l);
        ids.add(326372l);
        ids.add(326373l);
        ids.add(326374l);
        ids.add(326375l);
        ids.add(326376l);
        ids.add(326377l);
        ids.add(326378l);
        ids.add(326379l);
        ids.add(326380l);
        ids.add(326381l);
        ids.add(326382l);
        ids.add(326383l);
        ids.add(326384l);
        ids.add(326385l);
        ids.add(326386l);
        ids.add(326387l);
        ids.add(326388l);
        ids.add(326389l);
        ids.add(326390l);
        ids.add(326391l);
        ids.add(326392l);
        ids.add(326861l);
        ids.add(326862l);
        ids.add(326863l);
        ids.add(326864l);
        ids.add(326865l);
        ids.add(326866l);
        ids.add(326867l);
        ids.add(326868l);
        ids.add(326869l);
        ids.add(326870l);
        ids.add(326871l);
        ids.add(326872l);
        ids.add(326873l);
        ids.add(326874l);
        ids.add(326875l);
        ids.add(326876l);
        ids.add(326877l);
        ids.add(326878l);
        ids.add(326879l);
        ids.add(326880l);
        ids.add(326881l);
        ids.add(326882l);
        ids.add(326883l);
        ids.add(326884l);
        ids.add(326885l);
        ids.add(326886l);
        ids.add(326887l);
        ids.add(326888l);
        ids.add(326889l);
        ids.add(326890l);
        ids.add(326891l);
        ids.add(326892l);
        ids.add(325434l);
        ids.add(325435l);
        ids.add(325436l);
        ids.add(325437l);
        ids.add(325438l);
        ids.add(325439l);
        ids.add(325440l);
        ids.add(325441l);
        ids.add(325442l);
        ids.add(325443l);
        ids.add(325444l);
        ids.add(325445l);
        ids.add(325446l);
        ids.add(325447l);
        ids.add(325448l);
        ids.add(325449l);
        ids.add(325450l);
        ids.add(325451l);
        ids.add(325452l);
        ids.add(325453l);
        ids.add(325454l);
        ids.add(325455l);
        ids.add(325456l);
        ids.add(325457l);
        ids.add(325458l);
        ids.add(325459l);
        ids.add(325460l);
        ids.add(325461l);
        ids.add(325462l);
        ids.add(325463l);
        ids.add(325464l);
        ids.add(325900l);
        ids.add(325901l);
        ids.add(325902l);
        ids.add(325903l);
        ids.add(325904l);
        ids.add(325905l);
        ids.add(325906l);
        ids.add(325907l);
        ids.add(325908l);
        ids.add(325909l);
        ids.add(325910l);
        ids.add(325911l);
        ids.add(325912l);
        ids.add(325913l);
        ids.add(325914l);
        ids.add(325915l);
        ids.add(325916l);
        ids.add(325917l);
        ids.add(325918l);
        ids.add(325919l);
        ids.add(325920l);
        ids.add(325921l);
        ids.add(325922l);
        ids.add(325923l);
        ids.add(325924l);
        ids.add(325925l);
        ids.add(325926l);
        ids.add(325927l);
        ids.add(325928l);
        ids.add(325929l);
        ids.add(325930l);
        ids.add(325931l);
        ids.add(326393l);
        ids.add(326394l);
        ids.add(326395l);
        ids.add(326396l);
        ids.add(326397l);
        ids.add(326398l);
        ids.add(326399l);
        ids.add(326400l);
        ids.add(326401l);
        ids.add(326402l);
        ids.add(326403l);
        ids.add(326404l);
        ids.add(326405l);
        ids.add(326406l);
        ids.add(326407l);
        ids.add(326408l);
        ids.add(326409l);
        ids.add(326410l);
        ids.add(326411l);
        ids.add(326412l);
        ids.add(326413l);
        ids.add(326414l);
        ids.add(326415l);
        ids.add(326416l);
        ids.add(326417l);
        ids.add(326418l);
        ids.add(326419l);
        ids.add(326420l);
        ids.add(326421l);
        ids.add(326422l);
        ids.add(326423l);
        ids.add(326893l);
        ids.add(326894l);
        ids.add(326895l);
        ids.add(326896l);
        ids.add(326897l);
        ids.add(326898l);
        ids.add(326899l);
        ids.add(326900l);
        ids.add(326901l);
        ids.add(326902l);
        ids.add(326903l);
        ids.add(326904l);
        ids.add(326905l);
        ids.add(326906l);
        ids.add(326907l);
        ids.add(326908l);
        ids.add(326909l);
        ids.add(326910l);
        ids.add(326911l);
        ids.add(326912l);
        ids.add(326913l);
        ids.add(326914l);
        ids.add(326915l);
        ids.add(326916l);
        ids.add(326917l);
        ids.add(326918l);
        ids.add(326919l);
        ids.add(326920l);
        ids.add(326921l);
        ids.add(326922l);
        ids.add(326923l);
        ids.add(326924l);
        ids.add(325465l);
        ids.add(325466l);
        ids.add(325467l);
        ids.add(325468l);
        ids.add(325469l);
        ids.add(325470l);
        ids.add(325471l);
        ids.add(325472l);
        ids.add(325473l);
        ids.add(325474l);
        ids.add(325475l);
        ids.add(325476l);
        ids.add(325477l);
        ids.add(325478l);
        ids.add(325479l);
        ids.add(325480l);
        ids.add(325481l);
        ids.add(325482l);
        ids.add(325483l);
        ids.add(325484l);
        ids.add(325485l);
        ids.add(325486l);
        ids.add(325487l);
        ids.add(325488l);
        ids.add(325489l);
        ids.add(325490l);
        ids.add(325491l);
        ids.add(325492l);
        ids.add(325493l);
        ids.add(325494l);
        ids.add(325495l);
        ids.add(325496l);
        ids.add(325932l);
        ids.add(325933l);
        ids.add(325934l);
        ids.add(325935l);
        ids.add(325936l);
        ids.add(325937l);
        ids.add(325938l);
        ids.add(325939l);
        ids.add(325940l);
        ids.add(325941l);
        ids.add(325942l);
        ids.add(325943l);
        ids.add(325944l);
        ids.add(325945l);
        ids.add(325946l);
        ids.add(325947l);
        ids.add(325948l);
        ids.add(325949l);
        ids.add(325950l);
        ids.add(325951l);
        ids.add(325952l);
        ids.add(325953l);
        ids.add(325954l);
        ids.add(325955l);
        ids.add(325956l);
        ids.add(325957l);
        ids.add(325958l);
        ids.add(325959l);
        ids.add(325960l);
        ids.add(325961l);
        ids.add(325962l);
        ids.add(325963l);
        ids.add(326424l);
        ids.add(326425l);
        ids.add(326426l);
        ids.add(326427l);
        ids.add(326428l);
        ids.add(326429l);
        ids.add(326430l);
        ids.add(326431l);
        ids.add(326432l);
        ids.add(326433l);
        ids.add(326434l);
        ids.add(326435l);
        ids.add(326436l);
        ids.add(326437l);
        ids.add(326438l);
        ids.add(326439l);
        ids.add(326440l);
        ids.add(326441l);
        ids.add(326442l);
        ids.add(326443l);
        ids.add(326444l);
        ids.add(326445l);
        ids.add(326446l);
        ids.add(326447l);
        ids.add(326448l);
        ids.add(326449l);
        ids.add(326450l);
        ids.add(326451l);
        ids.add(326452l);
        ids.add(326453l);
        ids.add(326454l);
        ids.add(326925l);
        ids.add(326926l);
        ids.add(326927l);
        ids.add(326928l);
        ids.add(326929l);
        ids.add(326930l);
        ids.add(326931l);
        ids.add(326932l);
        ids.add(326933l);
        ids.add(326934l);
        ids.add(326935l);
        ids.add(326936l);
        ids.add(326937l);
        ids.add(326938l);
        ids.add(326939l);
        ids.add(325497l);
        ids.add(325498l);
        ids.add(325499l);
        ids.add(325500l);
        ids.add(325501l);
        ids.add(325502l);
        ids.add(325503l);
        ids.add(325504l);
        ids.add(325505l);
        ids.add(325506l);
        ids.add(325507l);
        ids.add(325508l);
        ids.add(325509l);
        ids.add(325510l);
        ids.add(325511l);
        ids.add(325512l);
        ids.add(325513l);
        ids.add(325514l);
        ids.add(325515l);
        ids.add(325516l);
        ids.add(325517l);
        ids.add(325518l);
        ids.add(325519l);
        ids.add(325520l);
        ids.add(325521l);
        ids.add(325522l);
        ids.add(325523l);
        ids.add(325524l);
        ids.add(325525l);
        ids.add(325526l);
        ids.add(325527l);
        ids.add(325528l);
        ids.add(325964l);
        ids.add(325965l);
        ids.add(325966l);
        ids.add(325967l);
        ids.add(325968l);
        ids.add(325969l);
        ids.add(325970l);
        ids.add(325971l);
        ids.add(325972l);
        ids.add(325973l);
        ids.add(325974l);
        ids.add(325975l);
        ids.add(325976l);
        ids.add(325977l);
        ids.add(325978l);
        ids.add(325979l);
        ids.add(325980l);
        ids.add(325981l);
        ids.add(325982l);
        ids.add(325983l);
        ids.add(325984l);
        ids.add(325985l);
        ids.add(325986l);
        ids.add(325987l);
        ids.add(325988l);
        ids.add(325989l);
        ids.add(325990l);
        ids.add(325991l);
        ids.add(325992l);
        ids.add(325993l);
        ids.add(325994l);
        ids.add(325995l);
        ids.add(326455l);
        ids.add(326456l);
        ids.add(326457l);
        ids.add(326458l);
        ids.add(326459l);
        ids.add(326460l);
        ids.add(326461l);
        ids.add(326462l);
        ids.add(326463l);
        ids.add(326464l);
        ids.add(326465l);
        ids.add(326466l);
        ids.add(326467l);
        ids.add(326468l);
        ids.add(326469l);
        ids.add(326470l);
        ids.add(326471l);
        ids.add(326472l);
        ids.add(326473l);
        ids.add(326474l);
        ids.add(326475l);
        ids.add(326476l);
        ids.add(326477l);
        ids.add(326478l);
        ids.add(326479l);
        ids.add(326480l);
        ids.add(326481l);
        ids.add(326482l);
        ids.add(326483l);
        ids.add(326484l);
        ids.add(326485l);
        ids.add(325529l);
        ids.add(325530l);
        ids.add(325531l);
        ids.add(325532l);
        ids.add(325533l);
        ids.add(325534l);
        ids.add(325535l);
        ids.add(325536l);
        ids.add(325537l);
        ids.add(325538l);
        ids.add(325539l);
        ids.add(325540l);
        ids.add(325541l);
        ids.add(325542l);
        ids.add(325543l);
        ids.add(325544l);
        ids.add(325545l);
        ids.add(325546l);
        ids.add(325547l);
        ids.add(325548l);
        ids.add(325549l);
        ids.add(325550l);
        ids.add(325551l);
        ids.add(325552l);
        ids.add(325553l);
        ids.add(325554l);
        ids.add(325555l);
        ids.add(325556l);
        ids.add(325557l);
        ids.add(325558l);
        ids.add(325559l);
        ids.add(325996l);
        ids.add(325997l);
        ids.add(325998l);
        ids.add(325999l);
        ids.add(326000l);
        ids.add(326001l);
        ids.add(326002l);
        ids.add(326003l);
        ids.add(326004l);
        ids.add(326005l);
        ids.add(326006l);
        ids.add(326007l);
        ids.add(326008l);
        ids.add(326009l);
        ids.add(326010l);
        ids.add(326011l);
        ids.add(326012l);
        ids.add(326013l);
        ids.add(326014l);
        ids.add(326015l);
        ids.add(326016l);
        ids.add(326017l);
        ids.add(326018l);
        ids.add(326019l);
        ids.add(326020l);
        ids.add(326021l);
        ids.add(326022l);
        ids.add(326023l);
        ids.add(326024l);
        ids.add(326025l);
        ids.add(326026l);
        ids.add(326027l);
        ids.add(326486l);
        ids.add(326487l);
        ids.add(326488l);
        ids.add(326489l);
        ids.add(326490l);
        ids.add(326491l);
        ids.add(326492l);
        ids.add(326493l);
        ids.add(326494l);
        ids.add(326495l);
        ids.add(326496l);
        ids.add(326497l);
        ids.add(326498l);
        ids.add(326499l);
        ids.add(326500l);
        ids.add(326501l);
        ids.add(326502l);
        ids.add(326503l);
        ids.add(326504l);
        ids.add(326505l);
        ids.add(326506l);
        ids.add(326507l);
        ids.add(326508l);
        ids.add(326509l);
        ids.add(326510l);
        ids.add(326511l);
        ids.add(326512l);
        ids.add(326513l);
        ids.add(326514l);
        ids.add(326515l);
        ids.add(326516l);
        ids.add(326517l);
        ids.add(325560l);
        ids.add(325561l);
        ids.add(325562l);
        ids.add(325563l);
        ids.add(325564l);
        ids.add(325565l);
        ids.add(325566l);
        ids.add(325567l);
        ids.add(325568l);
        ids.add(325569l);
        ids.add(325570l);
        ids.add(325571l);
        ids.add(325572l);
        ids.add(325573l);
        ids.add(325574l);
        ids.add(325575l);
        ids.add(325576l);
        ids.add(325577l);
        ids.add(325578l);
        ids.add(325579l);
        ids.add(325580l);
        ids.add(325581l);
        ids.add(325582l);
        ids.add(325583l);
        ids.add(325584l);
        ids.add(325585l);
        ids.add(325586l);
        ids.add(325587l);
        ids.add(325588l);
        ids.add(325589l);
        ids.add(325590l);
        ids.add(325591l);

        try {
            LOGGER.debug("**************** INICIO *************************");
            List<Tramite> result = dao.buscarTramitesConservacionPorIdTramites(ids);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------
    // //
    /**
     * @author felipe.cadena
     */
    @Test
    public void testFindBySolicitudId() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testFindBySolicitud");

        Long predioId = 151255l;

        try {
            LOGGER.debug("**************** INICIO *************************");
            List<Tramite> result = dao.findBySolicitud(predioId);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------
    // //
    @Test
    public void buscarTramitesConservacionPorIdTramites() {
        LOGGER.debug("tests: TramiteDAOBeanTests#buscarTramitesConservacionPorIdTramites");

        List<Long> ids = new ArrayList<Long>();

        ids.add(326028l);
        ids.add(326029l);
        ids.add(326030l);
        ids.add(326031l);
        ids.add(326032l);
        ids.add(326033l);
        ids.add(326034l);
        ids.add(326035l);
        ids.add(326036l);
        ids.add(326037l);
        ids.add(326038l);
        ids.add(326039l);
        ids.add(326040l);
        ids.add(326041l);
        ids.add(326042l);
        ids.add(326043l);
        ids.add(326044l);
        ids.add(326045l);
        ids.add(326046l);
        ids.add(326047l);
        ids.add(326048l);
        ids.add(326049l);
        ids.add(326050l);
        ids.add(326051l);
        ids.add(326052l);
        ids.add(326053l);
        ids.add(326054l);
        ids.add(326055l);
        ids.add(326056l);
        ids.add(326057l);
        ids.add(326058l);
        ids.add(326059l);
        ids.add(326518l);
        ids.add(326519l);
        ids.add(326520l);
        ids.add(326521l);
        ids.add(326522l);
        ids.add(326523l);
        ids.add(326524l);
        ids.add(326525l);
        ids.add(326526l);
        ids.add(326527l);
        ids.add(326528l);
        ids.add(326529l);
        ids.add(326530l);
        ids.add(326531l);
        ids.add(326532l);
        ids.add(326533l);
        ids.add(326534l);
        ids.add(326535l);
        ids.add(326536l);
        ids.add(326537l);
        ids.add(326538l);
        ids.add(326539l);
        ids.add(326540l);
        ids.add(326541l);
        ids.add(326542l);
        ids.add(326543l);
        ids.add(326544l);
        ids.add(326545l);
        ids.add(326546l);
        ids.add(326547l);
        ids.add(326548l);
        ids.add(326549l);
        ids.add(325592l);
        ids.add(325593l);
        ids.add(325594l);
        ids.add(325595l);
        ids.add(325596l);
        ids.add(325597l);
        ids.add(325598l);
        ids.add(325599l);
        ids.add(325600l);
        ids.add(325601l);
        ids.add(325602l);
        ids.add(325603l);
        ids.add(325604l);
        ids.add(325605l);
        ids.add(325606l);
        ids.add(325607l);
        ids.add(325608l);
        ids.add(325609l);
        ids.add(325610l);
        ids.add(325611l);
        ids.add(325612l);
        ids.add(325613l);
        ids.add(325614l);
        ids.add(325615l);
        ids.add(325616l);
        ids.add(325617l);
        ids.add(325618l);
        ids.add(325619l);
        ids.add(325620l);
        ids.add(325621l);
        ids.add(325622l);
        ids.add(325623l);
        ids.add(326060l);
        ids.add(326061l);
        ids.add(326062l);
        ids.add(326063l);
        ids.add(326064l);
        ids.add(326065l);
        ids.add(326066l);
        ids.add(326067l);
        ids.add(326068l);
        ids.add(326069l);
        ids.add(326070l);
        ids.add(326071l);
        ids.add(326072l);
        ids.add(326073l);
        ids.add(326074l);
        ids.add(326075l);
        ids.add(326076l);
        ids.add(326077l);
        ids.add(326078l);
        ids.add(326079l);
        ids.add(326080l);
        ids.add(326081l);
        ids.add(326082l);
        ids.add(326083l);
        ids.add(326084l);
        ids.add(326085l);
        ids.add(326086l);
        ids.add(326087l);
        ids.add(326088l);
        ids.add(326089l);
        ids.add(326090l);
        ids.add(326091l);
        ids.add(326550l);
        ids.add(326551l);
        ids.add(326552l);
        ids.add(326553l);
        ids.add(326554l);
        ids.add(326555l);
        ids.add(326556l);
        ids.add(326557l);
        ids.add(326558l);
        ids.add(326559l);
        ids.add(326560l);
        ids.add(326561l);
        ids.add(326562l);
        ids.add(326563l);
        ids.add(326564l);
        ids.add(326565l);
        ids.add(326566l);
        ids.add(326567l);
        ids.add(326568l);
        ids.add(326569l);
        ids.add(326570l);
        ids.add(326571l);
        ids.add(326572l);
        ids.add(326573l);
        ids.add(326574l);
        ids.add(326575l);
        ids.add(326576l);
        ids.add(326577l);
        ids.add(326578l);
        ids.add(326579l);
        ids.add(326580l);
        ids.add(326581l);
        ids.add(325624l);
        ids.add(325625l);
        ids.add(325626l);
        ids.add(325627l);
        ids.add(325628l);
        ids.add(325629l);
        ids.add(325630l);
        ids.add(325631l);
        ids.add(325632l);
        ids.add(325633l);
        ids.add(325634l);
        ids.add(325635l);
        ids.add(325636l);
        ids.add(325637l);
        ids.add(325638l);
        ids.add(325639l);
        ids.add(325640l);
        ids.add(325641l);
        ids.add(325642l);
        ids.add(325643l);
        ids.add(325644l);
        ids.add(325645l);
        ids.add(325646l);
        ids.add(325647l);
        ids.add(325648l);
        ids.add(325649l);
        ids.add(325650l);
        ids.add(325651l);
        ids.add(325652l);
        ids.add(325653l);
        ids.add(325654l);
        ids.add(325655l);
        ids.add(326092l);
        ids.add(326093l);
        ids.add(326094l);
        ids.add(326095l);
        ids.add(326096l);
        ids.add(326097l);
        ids.add(326098l);
        ids.add(326099l);
        ids.add(326100l);
        ids.add(326101l);
        ids.add(326102l);
        ids.add(326103l);
        ids.add(326104l);
        ids.add(326105l);
        ids.add(326106l);
        ids.add(326107l);
        ids.add(326108l);
        ids.add(326109l);
        ids.add(326110l);
        ids.add(326111l);
        ids.add(326112l);
        ids.add(326113l);
        ids.add(326114l);
        ids.add(326115l);
        ids.add(326116l);
        ids.add(326117l);
        ids.add(326118l);
        ids.add(326119l);
        ids.add(326120l);
        ids.add(326121l);
        ids.add(326122l);
        ids.add(326123l);

        try {
            LOGGER.debug("**************** INICIO *************************");
            List<Tramite> result = dao.buscarTramitesConservacionPorIdTramites(ids);

            if (result != null) {
                LOGGER.debug("Tamaño " + result.size());
            } else {
                assertNotNull(null);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------
    // //
    /**
     * @author juan.mendez
     */
    @Test
    public void testGetTramiteParaEdicionGeografica() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetTramiteParaEdicionGeografica");
        Long tramiteId = 121823L;

        try {
            LOGGER.debug("**************** INICIO *************************");
            Tramite result = dao.getTramiteParaEdicionGeografica(tramiteId);
            assertNotNull(result);
            LOGGER.debug("Tramite " + result);
            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author felipe.cadena
     */
    @Test
    public void testGetTipoTramiteCompuestoCodigo() {
        LOGGER.debug("tests: TramiteDAOBeanTests#testGetTipoTramiteCompuestoCodigo");

        try {
            LOGGER.debug("**************** INICIO *************************");
            List<String[]> result = dao.getTipoTramiteCompuestoCodigo();

            for (String[] str : result) {
                LOGGER.debug(str[0]);
                LOGGER.debug(str[1]);
            }

            LOGGER.debug("********************FIN**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // end of class
}
