package co.gov.igac.snc.dao.conservacion.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPUnidadConstruccionDAO;
import co.gov.igac.snc.dao.conservacion.impl.PUnidadConstruccionDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class PUnidadConstruccionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PUnidadConstruccionDAOBeanTest.class);

    private IPUnidadConstruccionDAO dao = new PUnidadConstruccionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // ---------------------------------------//
    /**
     * Test para buscar un PUnidadConstruccion por su id, trayendo las PUnidadConstruccionComp
     * asociadas.
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarUnidadDeConstruccionPorSuId() {

        LOGGER.debug("PUnidadConstruccionDAOBeanTest#buscarUnidadDelModeloDeConstruccionPorSuId");

        PUnidadConstruccion puc;
        Long idPUnidadConstruccion = 23L;
        try {
            puc = this.dao
                .buscarUnidadDeConstruccionPorSuId(idPUnidadConstruccion);
            if (puc != null && puc.getPUnidadConstruccionComps() != null) {

                LOGGER.debug("Se cargo satisfactoriamente el PUnidadConstruccion, éste tiene " +
                    puc.getPUnidadConstruccionComps().size() +
                    " componentes.");
            }
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }
    // ---------------------------------------//

    /**
     * Test para buscar un listado de PUnidadConstruccion por su tramiteId
     *
     * @author franz.gamba
     */
    @Test
    public void testBuscarPUnidadDeConstruccionPorSuTramiteId() {

        LOGGER.debug("PUnidadConstruccionDAOBeanTest#testBuscarPUnidadDeConstruccionPorSuTramiteId");

        List<PUnidadConstruccion> pucs;
        Long tramiteId = 42395L;
        try {
            pucs = this.dao.getAllPunidadConstruccionsByTramiteId(tramiteId);
            if (pucs != null) {
                for (PUnidadConstruccion p : pucs) {
                    LOGGER.debug("***** Unidad de construcción: " + p.getId());
                }
            }
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

    // ---------------------------------------//
    /**
     * Test para buscar las unidades de construcción asociadas al pPredio.
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarUnidadesDeConstruccionPorPPredioId() {

        LOGGER.debug("PUnidadConstruccionDAOBeanTest#testBuscarUnidadesDeConstruccionPorPPredioId");

        List<PUnidadConstruccion> pucs;
        Long pPredioId = 528098L;
        try {
            pucs = this.dao.buscarUnidadesDeConstruccionPorPPredioId(pPredioId, true);
            if (pucs != null) {
                LOGGER.debug("****** Tamaño: " + pucs.size() + " ********");
                for (PUnidadConstruccion p : pucs) {
                    LOGGER.debug("***** Unidad de construcción: " + p.getId());
                }
            }
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

}
