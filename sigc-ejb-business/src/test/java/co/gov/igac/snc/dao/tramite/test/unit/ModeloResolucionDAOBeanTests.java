package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.IModeloResolucionDAO;
import co.gov.igac.snc.dao.tramite.impl.ModeloResolucionDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.test.unit.BaseTest;

public class ModeloResolucionDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ModeloResolucionDAOBeanTests.class);

    private IModeloResolucionDAO dao = new ModeloResolucionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     */
    @Test
    public void testFindModelosResolucionByTramite() {
        String tipoTramite = "1";
        String claseMutacion = "1";
        List<ModeloResolucion> modelResol = new ArrayList<ModeloResolucion>();

        modelResol = (List<ModeloResolucion>) dao.findModelosResolucionByTramite(tipoTramite,
            claseMutacion);
        Assert.assertNotNull(modelResol);
        Assert.assertTrue(modelResol.size() >= 1);
    }

}
