package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Zona;

import co.gov.igac.snc.dao.generales.impl.ZonaDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class ZonaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ZonaDAOBeanTest.class);

    private ZonaDAOBean dao = new ZonaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author lorena.salamanca
     *
     */
    @Test
    public void findByMunicipioTest() {
        List<Zona> c;
        String codigo = "08001";
        try {
            LOGGER.debug("Entrando al test de encontrar zona por codigo de Municipio");
            c = this.dao.findByMunicipio(codigo);
            LOGGER.debug("la lista mide :  " + c.size());
            LOGGER.debug("nombre zona :  " + c.get(0).getNombre());
            LOGGER.debug("Salida método ZonaDAOBeanTest#findByMunicipioTest");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
            assertTrue(false);
        }
    }

}
