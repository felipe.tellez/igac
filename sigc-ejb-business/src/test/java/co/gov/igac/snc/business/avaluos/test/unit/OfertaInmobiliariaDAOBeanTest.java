package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertNotNull;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IOfertaInmobiliariaDAO;
import co.gov.igac.snc.dao.avaluos.impl.OfertaInmobiliariaDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioPublico;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;
import java.util.Date;

public class OfertaInmobiliariaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(OfertaInmobiliariaDAOBeanTest.class);

    private IOfertaInmobiliariaDAO dao = new OfertaInmobiliariaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de ofertas inmobiliarias por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarOfertasInmobiliariasPorFiltro() {
        LOGGER.debug("tests: OfertaInmobiliariaDAOBeanTest#testBuscarOfertasInmobiliariasPorFiltro");

        FiltroDatosConsultaOfertasInmob foi = new FiltroDatosConsultaOfertasInmob();

        String codigoOferta = "63602575400000022011";
        foi.setCodigoOferta(codigoOferta);

        /*
         * String numeroBanios = "2"; foi.setNumeroBanios(numeroBanios);
         */
        // Double areaOfertaTerrenoDesde = 10.2;
        // foi.setAreaOfertaTerrenoDesde(areaOfertaTerrenoDesde);
        // Double areaOfertaTerrenoHasta = 1000.2;
        // foi.setAreaOfertaTerrenoHasta(areaOfertaTerrenoHasta);

        /*
         * Date fechaOfertaDesde;
         *
         * fechaOfertaDesde = new Date(2011, 11, 16); foi.setFechaOfertaDesde(fechaOfertaDesde);
         */
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<OfertaInmobiliaria> answer = this.dao
                .buscarOfertasInmobiliariasPorFiltro(foi, null, null);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("fetched " + answer.size() + " rows");
                for (OfertaInmobiliaria oi : answer) {
                    LOGGER.debug("Oferta id: " + oi.getId());
                    LOGGER.debug("Oferta código: " + oi.getCodigoOferta());
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de contéo de ofertas inmobiliarias por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testContarOfertasInmobiliariasPorFiltro() {
        LOGGER.debug("tests: OfertaInmobiliariaDAOBeanTest#testContarOfertasInmobiliariasPorFiltro");

        FiltroDatosConsultaOfertasInmob foi = new FiltroDatosConsultaOfertasInmob();
        /*
         * String codigoOferta = "1923392591"; foi.setCodigoOferta(codigoOferta);
         */

 /*
         * String numeroBanios = "2"; foi.setNumeroBanios(numeroBanios);
         */
        Double areaOfertaTerrenoDesde = 10.2;
        foi.setAreaOfertaTerrenoDesde(areaOfertaTerrenoDesde);
        Double areaOfertaTerrenoHasta = 1000.2;
        foi.setAreaOfertaTerrenoHasta(areaOfertaTerrenoHasta);
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Long answer = this.dao.contarOfertasInmobiliariasPorFiltro(foi);

            if (answer != null) {
                LOGGER.debug("Ofertas: " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda de ofertas inmobiliarias con oferta padre por oferta inmobiliaria Id
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarOfertaInmobiliariaOfertaPadreByOfertaId() {
        LOGGER.debug(
            "tests: OfertaInmobiliariaDAOBeanTest#testBuscarOfertaInmobiliariaOfertaPadreByOfertaId");

        Long ofertaInmobiliariaId = 406L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            OfertaInmobiliaria answer = this.dao
                .buscarOfertaInmobiliariaOfertaPadreByOfertaId(ofertaInmobiliariaId);

            if (answer != null) {

                LOGGER.debug("Oferta id: " + answer.getId());

                if (answer.getFotografias() != null &&
                    !answer.getFotografias().isEmpty()) {
                    for (Fotografia foto : answer.getFotografias()) {
                        LOGGER.debug("Foto: " + foto.getArchivoFotografia());
                    }
                }

                if (answer.getOfertaInmobiliaria() != null) {
                    LOGGER.debug("OfertaP id: " +
                        answer.getOfertaInmobiliaria().getId());
                    if (answer.getOfertaInmobiliaria()
                        .getOfertaServicioPublicos() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaServicioPublicos().isEmpty()) {
                        for (OfertaServicioPublico osp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaServicioPublicos()) {
                            LOGGER.debug("OfertaPSP: " +
                                osp.getServicioPublico());
                        }
                    }

                    if (answer.getOfertaInmobiliaria().getFotografias() != null &&
                        !answer.getOfertaInmobiliaria().getFotografias()
                            .isEmpty()) {
                        for (Fotografia foto : answer.getOfertaInmobiliaria()
                            .getFotografias()) {
                            LOGGER.debug("FotoP: " +
                                foto.getArchivoFotografia());
                        }
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    public void testBuscarOfertaInmobiliariaOfertasByOfertaId() {
        LOGGER.debug(
            "tests: OfertaInmobiliariaDAOBeanTest#testBuscarOfertaInmobiliariaOfertasByOfertaId");

        Long ofertaInmobiliariaId = 789L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            OfertaInmobiliaria answer = this.dao
                .buscarOfertaInmobiliariaOfertasByOfertaId(ofertaInmobiliariaId);

            for (Iterator<OfertaPredio> iterator = answer.getOfertaPredios().iterator(); iterator.
                hasNext();) {
                OfertaPredio predio = (OfertaPredio) iterator.next();
                LOGGER.debug(predio.getNumeroPredial());
            }

            if (answer != null) {

                LOGGER.debug("Oferta id: " + answer.getId());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * @author rodrigo.hernandez
     */
    @Test
    public void testBuscarListaOfertasInmobiliariasPorRecolector() {
        LOGGER.debug(
            "tests: OfertaInmobiliariaDAOBeanTest#testBuscarListaOfertasInmobiliariasPorRecolector");

        String recolectorId = "pruatlantico20";
        Long regionId = 5L;
        String estado = EOfertaEstado.CREADA.getCodigo();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<OfertaInmobiliaria> answer = this.dao
                .buscarListaOfertasInmobiliariasPorRecolector(recolectorId, regionId, estado);

            if (answer != null) {
                for (OfertaInmobiliaria oi : answer) {
                    LOGGER.debug("Id del recolector: " + oi.getRecolectorId());
                    LOGGER.
                        debug("Id de la región: " + String.valueOf(oi.getRegionCapturaOfertaId()));
                    LOGGER.debug("Parametro visor GIS: " + oi.getCadenaDeManzanasVeredas());
                    LOGGER.debug(
                        "-------------------------------------------------------------------");
                }
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
