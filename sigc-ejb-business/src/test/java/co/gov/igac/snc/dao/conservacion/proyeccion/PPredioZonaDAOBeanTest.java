package co.gov.igac.snc.dao.conservacion.proyeccion;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPredioZonaDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPredioZonaDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.test.unit.BaseTest;

public class PPredioZonaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioZonaDAOBeanTest.class);

    private IPPredioZonaDAO dao = new PPredioZonaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarZonasHomogeneasPorPPredioId() {
        LOGGER.debug("PPredioZonaDAOBeanTest#testBuscarZonasHomogeneasPorPPredioId");

        Long idPredio = 1970457L;
        try {
            List<PPredioZona> ppz = dao
                .buscarZonasHomogeneasPorPPredioId(idPredio);
            Assert.assertNotNull(ppz);

            for (PPredioZona ppzona : ppz) {
                LOGGER.debug("Número predial: " +
                    ppzona.getPPredio().getNumeroPredial());
            }
        } catch (Exception e) {
            LOGGER.error(
                "Error en PPredioZonaDAOBeanTest#testBuscarZonasHomogeneasPorPPredioId: " +
                e.getMessage(), e);
            Assert.fail();
        }

    }

}
