/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IAvaluoObservacionDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoMovimientoDAOBean;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoObservacionDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoObservacion;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class AvaluoObservacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluoObservacionDAOBeanTest.class);
    private IAvaluoObservacionDAO dao = new AvaluoObservacionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar {@link AvaluoMovimientoDAOBean#buscarPorAvaluo(Long)}
     *
     * @author felipe.cadena
     */
    @Test
    public void testBuscarPorAvaluo() {
        LOGGER.debug("iniciando AvaluoObservacionDAOBeanTest#testBuscarPorAvaluo");

        List<AvaluoObservacion> result = null;

        result = dao.buscarPorAvaluo(172L);

        LOGGER.debug("***********************************************");
        if (result != null) {
            LOGGER.debug("****Observaciones********** " + result + " ******************");
        }
        LOGGER.debug("***********************************************");

        AvaluoObservacion ao = new AvaluoObservacion();
        ao.setFechaLog(new Date());
        ao.setFechaEnvio(new Date());
        ao.setFechaObservacion(new Date());
        ao.setObservacion("Observacion 1");
        ao.setUsuarioLog("TestAvaluos");
        Avaluo ava = new Avaluo();
        ava.setId(172L);
        ao.setAvaluo(ava);
        em.getTransaction().begin();

        dao.update(ao);
        em.getTransaction().commit();

        LOGGER.debug("finalizando AvaluoObservacionDAOBeanTest#testBuscarPorAvaluo");
    }

}
