package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.conservacion.IPersonaBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.IPersonasBloqueoDAO;
import co.gov.igac.snc.dao.conservacion.impl.PersonaBloqueoDAOBean;
import co.gov.igac.snc.dao.conservacion.impl.PersonasBloqueoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author juan.agudelo
 *
 */
public class PersonasBloqueoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PersonasBloqueoDAOBeanTest.class);

    private IPersonasBloqueoDAO dao = new PersonasBloqueoDAOBean();
    private IPersonaBloqueoDAO daodos = new PersonaBloqueoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testActualizarPersonasBloqueo() {
        LOGGER.debug("unit testing PersonasBloqueoDAOBeanTest#testActualizarPersonasBloqueo...");

        UsuarioDTO usuario = new UsuarioDTO();
        Persona p = new Persona();
        Documento d = new Documento();
        List<PersonaBloqueo> pBList = new ArrayList<PersonaBloqueo>();

        usuario.setLogin("juan.agudelo");
        p.setId(3L);
        d.setId(118L);

        PersonaBloqueo pb = new PersonaBloqueo();
        pb.setPersona(p);
        pb.setFechaInicioBloqueo(new Date(System.currentTimeMillis()));
        pb.setFechaRecibido(new Date(System.currentTimeMillis()));
        pb.setUsuarioLog(usuario.getLogin());
        pb.setFechaLog(new Date(System.currentTimeMillis()));
        pb.setDocumentoSoporteBloqueo(d);
        pb.setMotivoBloqueo("Prueba unitaria de bloqueo persona");
        pBList.add(pb);

        try {
            dao.getEntityManager().getTransaction().begin();
            List<PersonaBloqueo> pBResultado = this.daodos
                .actualizarPersonasBloqueo(usuario, pBList);
            dao.getEntityManager().getTransaction().commit();
            LOGGER.debug("num resultado = " + pBResultado.size());

            if (pBResultado.size() > 0) {
                LOGGER.debug("Fecha inicial = " +
                    pBResultado.get(0).getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " +
                    pBResultado.get(0).getMotivoBloqueo());
                LOGGER.debug("Documento = " +
                    pBResultado.get(0).getDocumentoSoporteBloqueo());
            }
            Assert.assertTrue(pBResultado.size() > 0);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

}
