package co.gov.igac.snc.dao.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.GenericDAOWithJPA;
import co.gov.igac.snc.dao.IGenericJpaDAO;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * OJO: las anotaciones BeforeClass y BeforeClass deben ser las de testng para que funcione la
 * ejecución de las pruebas con maven!!!
 *
 * El DAO debe ser creado en cada método para poder definirlo como de una clase específica sobre la
 * que se puedan hacer las pruebas
 *
 * @author pedro.garcia
 */
public class GenericDAOWithJPATests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GenericDAOWithJPATests.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);

    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testCountByExactCriteria() {
        LOGGER.debug("tests: GenericDAOWithJPATests#testCountByExactCriteria");

        IGenericJpaDAO dao = new GenericDAOWithJPA<Tramite, Long>() {
        };
        dao.setEntityManager(this.em);

        assertNotNull(em);
        assertNotNull(dao);

        int answer;
        List<String[]> criteria;
        String[] criterion;

        criteria = new ArrayList<String[]>();
        criterion = new String[3];

        criterion[0] = "clasificacion";
        criterion[1] = "varchar";
        criterion[2] = ETramiteClasificacion.TERRENO.name();
        criteria.add(criterion);

        // OJO: hay que crear uno nuevo. Si no, quedan los mismos datos en las
        // posiciones de la lista
        criterion = new String[3];
        criterion[0] = "estado";
        criterion[1] = "varchar";
        // criterion[2] = ETramiteEstado.POR_ASIGNAR_COMISION.getCodigo();
        criterion[2] = "paila";
        criteria.add(criterion);

        try {
            answer = dao.countByExactCriteria(criteria);
            assertNotNull(answer);
            LOGGER.debug("passed. Count = " + answer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     *
     * pre: existen datos en las tablas COMISION y TRAMITE que correspondan
     */
    @Test
    public void testInsertMultiple() {
        Long idTramite = 2156L;
        Long idComision = 357L;

        @SuppressWarnings("rawtypes")
        IGenericJpaDAO dao = new GenericDAOWithJPA<ComisionTramite, Long>() {
        };
        dao.setEntityManager(this.em);

        assertNotNull(em);
        assertNotNull(dao);

        List<ComisionTramite> rows;
        ComisionTramite insertMe;

        int hm = 2;

        rows = new ArrayList<ComisionTramite>();
        Comision comision = new Comision();
        Tramite tramite = new Tramite();
        tramite.setId(idTramite);

        for (int i = 1; i <= hm; i++) {
            // OJO: hay que crear uno nuevo para adicionarlo a la lista cada
            // vez. Sino, queda como si fuera el mismo objeto
            insertMe = new ComisionTramite();
            comision.setId(idComision);
            insertMe.setComision(comision);
            insertMe.setTramite(tramite);
            insertMe.setFechaLog(new Date());
            insertMe.setUsuarioLog("insertado en pruebas unitarias del generic # " +
                i);
            rows.add(insertMe);
        }

        try {
            dao.getEntityManager().getTransaction().begin();
            dao.persistMultiple(rows);
            dao.getEntityManager().getTransaction().commit();
            LOGGER.debug("passed");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);

        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindDistinctByColumnName() {
        LOGGER.debug("tests: GenericDAOWithJPATests#testFindDistinctByColumnName");

        IGenericJpaDAO dao = new GenericDAOWithJPA<CalificacionConstruccion, Long>() {
        };
        dao.setEntityManager(this.em);

        assertNotNull(em);
        assertNotNull(dao);

        List<CalificacionConstruccion> answer;

        try {
            answer = dao.findDistinctByColumnName("componente");
            assertNotNull(answer);
            LOGGER.debug("passed. Count = " + answer.size());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindAllOrderedByColumns() {
        LOGGER.debug("tests: GenericDAOWithJPATests#testFindAllOrderedByColumns");

        IGenericJpaDAO dao = new GenericDAOWithJPA<CalificacionConstruccion, Long>() {
        };
        dao.setEntityManager(this.em);

        assertNotNull(em);
        assertNotNull(dao);

        List<CalificacionConstruccion> answer;
        List<String> columNames;

        columNames = new ArrayList<String>();
        columNames.add("componente");
        columNames.add("elemento");
        columNames.add("detalleCalificacion");

        try {
            answer = dao.findAllOrderedByColumns(columNames);
            assertNotNull(answer);
            LOGGER.debug("passed. Count = " + answer.size() +
                " data con cambio de type: " +
                answer.get(0).getPuntosResidencial());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia Aquí se pone a prueba si la búsqueda funciona haciendo referencia a
     * atributos de los entities
     *
     * pre: existen datos en la tabla P_UNIDAD_CONSTRUCCION_COMP
     */
    @Test
    public void testFindByExactCriteria() {

        IGenericJpaDAO dao = new GenericDAOWithJPA<PUnidadConstruccionComp, Long>() {
        };
        dao.setEntityManager(this.em);

        assertNotNull(em);

        List<PUnidadConstruccionComp> registersFound;
        List<String[]> criteria;
        String[] criterion;

        criteria = new ArrayList<String[]>();
        criterion = new String[3];

        criterion[0] = "PUnidadConstruccion.id";
        criterion[1] = "number";
        // criterion[2] = "131827";
        criterion[2] = "666";
        criteria.add(criterion);

        try {
            registersFound = dao.findByExactCriteria(criteria);
            LOGGER.debug("trajo " + registersFound.size());
            assertTrue(!registersFound.isEmpty());
            LOGGER.debug("passed!");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testLoadAndInitializeParameters() {
        LOGGER.debug("testLoadAndInitializeParameters ");
        try {
            Long idTramite = 2156L;

            assertNotNull(em);

            IGenericJpaDAO<Tramite, Long> dao = new GenericDAOWithJPA<Tramite, Long>() {
            };
            dao.setEntityManager(this.em);

            // String[] propertiesToInitialize = new String[] ();
            String[] propertiesToInitialize = {"tramiteEstados", "tramites",
                "solicitanteTramites"};

            Tramite tramite = dao.findById(idTramite, propertiesToInitialize);
            // String className =
            // tramite.getTramiteEstados().getClass().getName();
            // LOGGER.debug("className:"+className);
            assertNotNull(tramite);
            assertNotNull(tramite.getTramiteEstados());
            LOGGER.debug("Cantidad Solicitantes: " +
                tramite.getSolicitanteTramites().size());
            LOGGER.debug("Cantidad Estados: " +
                tramite.getTramiteEstados().size());
            LOGGER.debug("Cantidad Tramites: " + tramite.getTramites().size());
            // assertTrue(!registersFound.isEmpty());
            // LOGGER.debug("passed!");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testInitializeParameters() {
        LOGGER.debug("GenericDAOWithJPATests#testInitializeParameters");
        try {
            Long idTramite = 1770L;

            assertNotNull(em);

            IGenericJpaDAO<Tramite, Long> dao = new GenericDAOWithJPA<Tramite, Long>() {
            };
            dao.setEntityManager(this.em);

            // String[] propertiesToInitialize = new String[] ();
            String[] propertiesToInitialize = {"tramites", "tramiteEstados"};

            Tramite tramite = dao.findById(idTramite);
            tramite = dao.initializeLazyList(tramite, propertiesToInitialize);
            LOGGER.debug("************NO QUERIES*****");
            //String className = tramite.getTramiteEstados().getClass().getName();
            //LOGGER.debug("className:"+className);
            assertNotNull(tramite);
            assertNotNull(tramite.getTramites());
            //LOGGER.debug("Cantidad Solicitantes: " + tramite.getSolicitanteTramites().size());
            //LOGGER.debug("Cantidad Estados: " + tramite.getTramiteEstados().size());
            LOGGER.debug("Cantidad Tramites: " + tramite.getTramites().size());
            LOGGER.debug("Cantidad Tramites: " + tramite.getTramiteEstados().size());
            // assertTrue(!registersFound.isEmpty());
            // LOGGER.debug("passed!");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba método genérico para contar los registros de una entidad
     *
     * @author juan.mendez
     */
    @Test
    public void testCountAll() {
        LOGGER.debug("testCountAll");
        try {
            @SuppressWarnings("rawtypes")
            IGenericJpaDAO dao = new GenericDAOWithJPA<Tramite, Long>() {
            };
            dao.setEntityManager(this.em);

            assertNotNull(em);
            assertNotNull(dao);
            int answer = dao.countAll();
            assertNotNull(answer);
            LOGGER.debug("Count = " + answer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba de método que carga registros aleatoriamente
     */
    @Test
    public void testLoadRandomObject() {
        LOGGER.debug("testLoadRandomObject");
        try {
            assertNotNull(em);

            @SuppressWarnings("rawtypes")
            IGenericJpaDAO dao = new GenericDAOWithJPA<Predio, Long>() {
            };
            dao.setEntityManager(this.em);
            assertNotNull(dao);
            for (int i = 0; i < 10; i++) {
                Predio predio = (Predio) dao.loadRandomObject();
                assertNotNull(predio);
                LOGGER.debug("id:" + predio.getId() + " - Número Predial:" + predio.
                    getNumeroPredial());
            }
            int answer = dao.countAll();
            assertNotNull(answer);
            LOGGER.debug("Count = " + answer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Valida los mapeos JPA contra la base de datos utilizando un conteo de registros y la carga de
     * los primeros 10 que se encuentren.
     *
     * @author juan.mendez
     */
    @Test
    public void testValidarMapeosJPA() {
        LOGGER.debug("testValidarMapeosJPA");
        try {
            Map<String, Exception> erroresMapeos = new HashMap<String, Exception>();

            assertNotNull(em);
            @SuppressWarnings("rawtypes")
            //										co.gov.igac.persistence.entity.generales;
            List<Class> persistentClasses = getClasses("co.gov.igac.snc.persistence.entity");
            persistentClasses.addAll(getClasses("co.gov.igac.persistence.entity"));
            assertNotNull(persistentClasses);
            for (@SuppressWarnings("rawtypes") Class persistentClass : persistentClasses) {
                String className = persistentClass.getName();
                String classSimpleName = persistentClass.getSimpleName();//.replaceAll("$1", "");
                LOGGER.debug("Class: " + className);

                Annotation[] annotations = persistentClass.getAnnotations();

                boolean isTable = false;
                for (Annotation annotation : annotations) {
                    String annotationName = annotation.annotationType().getName();
                    //LOGGER.debug("name: " + annotationName);
                    if (annotationName.equals("javax.persistence.Table")) {
                        isTable = true;
                        break;
                    }
                }

                if (!isTable) {
                    continue;
                }
                validarMapeoJPA(className, classSimpleName, erroresMapeos);
            }

            if (erroresMapeos.size() > 0) {
                LOGGER.error("****************************************************************");
                LOGGER.error("Existen errores de mapeo en las entidades", erroresMapeos);
                for (Map.Entry<String, Exception> entry : erroresMapeos.entrySet()) {
                    LOGGER.error("Clase:+" + entry.getKey());
                    Exception error = entry.getValue();
                    LOGGER.error("Error:");
                    LOGGER.error(error.getMessage(), error);
                }
                LOGGER.error("****************************************************************");
                fail("Existen errores de mapeo en las entidades");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     */
    private void validarMapeoJPA(String className, String classSimpleName,
        Map<String, Exception> erroresMapeos) {
        try {
            String jpql = "select count(*) from " + classSimpleName;
            LOGGER.debug("jpql: " + jpql);
            StringBuilder queryString = new StringBuilder(jpql);
            Query query = em.createQuery(queryString.toString());
            Long howMany = (Long) query.getSingleResult();
            LOGGER.debug("Count:" + howMany);

            queryString = new StringBuilder("select model from " + classSimpleName + " model");
            query = em.createQuery(queryString.toString());
            int maxResults = 10;
            query.setFirstResult(0);
            query.setMaxResults(maxResults);
            @SuppressWarnings("rawtypes")
            List results = query.getResultList();
            assertNotNull(results);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            erroresMapeos.put(className, e);
        }
    }

    /**
     *
     * @param packageName
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
    private List<Class> getClasses(String packageName)
        throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread()
            .getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        List<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        //return classes.toArray(new Class[classes.size()]);
        return classes;
    }

    /**
     * Recursive method used to find all classes in a given directory and subdirs.
     *
     * @param directory The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("rawtypes")
    private List<Class> findClasses(File directory, String packageName) throws
        ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.
                    getName().length() - 6)));
            }
        }
        return classes;
    }

    // end of class
}
