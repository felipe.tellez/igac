package co.gov.igac.snc.dao.vistas.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.vistas.IVPredioAvaluoDecretoDAO;
import co.gov.igac.snc.dao.vistas.impl.VPredioAvaluoDecretoDAOBean;
import co.gov.igac.snc.persistence.entity.vistas.VPredioAvaluoDecreto;
import co.gov.igac.snc.test.unit.BaseTest;

public class VPredioAvaluoDecretoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        VPredioAvaluoDecretoDAOBeanTest.class);

    private IVPredioAvaluoDecretoDAO dao = new VPredioAvaluoDecretoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        this.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        this.tearDown();
    }

    // ------------------------------------- //
    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarAvaluosCatastralesPorIdPPredio() {
        LOGGER.
            debug("test VPredioAvaluoDecretoDAOBeanTest#testBuscarAvaluosCatastralesPorIdPPredio");

        Long pPredioId = 172675L;
        try {

            List<VPredioAvaluoDecreto> pPrediosAvaluosCatastrales = this.dao
                .buscarAvaluosCatastralesPorIdPPredio(pPredioId);
            assertNotNull(pPrediosAvaluosCatastrales);
            assertTrue(pPrediosAvaluosCatastrales.size() > 0);
            LOGGER.info("Size return avalúos catastrales: " + pPrediosAvaluosCatastrales.size());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
}
