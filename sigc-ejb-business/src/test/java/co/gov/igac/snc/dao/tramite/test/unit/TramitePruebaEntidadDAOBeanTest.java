/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ITramitePruebaEntidadDAO;
import co.gov.igac.snc.dao.tramite.impl.TramitePruebaEntidadDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.test.unit.BaseTest;

public class TramitePruebaEntidadDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramitePruebaEntidadDAOBeanTest.class);

    private ITramitePruebaEntidadDAO dao = new TramitePruebaEntidadDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testFindTramitePruebaEntidadByTramiteId() {

        Long tramiteId = new Long(41399);
        List<TramitePruebaEntidad> entidades = null;

        try {
            entidades = this.dao.findTramitePruebaEntidadByTramiteId(tramiteId);
            Assert.assertNotNull(entidades);
        } catch (Exception e) {
            LOGGER.error(
                "Error en " +
                "TramitePruebaEntidadDAOBeanTest#testFindTramitePruebaEntidadByTramiteId : " +
                "" + e.getMessage());
        }

    }

}
