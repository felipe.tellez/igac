/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.ITramiteDepuracionDAO;
import co.gov.igac.snc.dao.conservacion.impl.TramiteDepuracionDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class TramiteDepuracionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDepuracionDAOBeanTest.class);

    private ITramiteDepuracionDAO dao = new TramiteDepuracionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarPorIdTramite() {

        LOGGER.debug("on TramiteDepuracionDAOBeanTest#testBuscarPorIdTramite ...");

        try {
            List<TramiteDepuracion> tds = dao.buscarPorIdTramite(34444l);

            TramiteDepuracion td = null;
            if (!tds.isEmpty()) {
                td = tds.get(0);
                LOGGER.debug(">>>>> " + td.getInconsistencias());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarPorIds() {

        LOGGER.debug("on TramiteDepuracionDAOBeanTest#testBuscarPorIds ...");

        long[] idsTramitesActividades = new long[4];
        idsTramitesActividades[0] = 23341;
        idsTramitesActividades[1] = 23342;
        idsTramitesActividades[2] = 23343;
        idsTramitesActividades[3] = 23912;

        Map<String, String> filter = new HashMap<String, String>();
        filter.put("tramite.numeroRadicacion", "080011");

        try {
            List<TramiteDepuracion> td = dao.buscarPorIds(idsTramitesActividades,
                "tramite.predio.municipio.nombre", "DESCENDING", null, null);
            td = dao.buscarPorIds(idsTramitesActividades, "tramite.predio.departamento.nombre",
                "DESCENDING", null, null);
            td = dao.buscarPorIds(idsTramitesActividades, "tramite.predio.numeroPredial",
                "DESCENDING", null, null);

            LOGGER.debug(">>>>> " + td.get(0).getTramite().getNumeroRadicacion());
            LOGGER.debug(">>>>> Len " + td.size());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

}
