package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPersonaPredioDAO;
import co.gov.igac.snc.dao.conservacion.impl.PersonaPredioDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author franz.gamba
 *
 */
public class PersonaPredioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PersonaPredioDAOBeanTest.class);

    private IPersonaPredioDAO dao = new PersonaPredioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Test
    public void testGetPersonabyTipoIdentificacionNumeroIdentificacion() {
        LOGGER.debug(
            "tests: PersonaDAOBeanTest#testGetPersonabyTipoIdentificacionNumeroIdentificacion");

        Long personaId = 121256L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Long answer = this.dao
                .countPersonaPrediosByPersonaId(personaId);

            if (answer != null) {

                LOGGER.debug("Conteo : " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
