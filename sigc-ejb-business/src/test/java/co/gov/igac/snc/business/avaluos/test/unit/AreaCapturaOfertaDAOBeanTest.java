package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IAreaCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.impl.AreaCapturaOfertaDAOBean;

import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaAreaCapturaOfertaEstado;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase para probar metodos del AreaCapturaOfertaDAOBean
 *
 * @author rodrigo.hernandez
 *
 */
public class AreaCapturaOfertaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AreaCapturaOfertaDAOBeanTest.class);

    private IAreaCapturaOfertaDAO dao = new AreaCapturaOfertaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetAreaCapturaOfertaPorEstadoYEstrucOrg() {
        LOGGER.debug(
            "unit test AreaCapturaOfertaDAOBeanTest#testGetAreaCapturaOfertaPorEstadoYEstrucOrg ...");

        List<AreaCapturaOferta> answer;
        String estructuraOrg;

        estructuraOrg = "6040";

        try {
            answer = this.dao.getAreaCapturaOfertaPorEstadoYEstrucOrg(estructuraOrg,
                EOfertaAreaCapturaOfertaEstado.VIGENTE.toString());
            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("fetched " + answer.size() + " rows");
            }
            assertTrue(true);
        } catch (Exception ex) {
            fail("ocurrió un error.");
            LOGGER.error("excepción: " + ex.getMessage());
        }

    }
//-------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * PRE: existe un AreaCapturaOferta con el id usado; esto implica que existe por lo menos un
     * DetalleCapturaOferta
     */
    @Test
    public void testGetByIdConAsociaciones() {

        LOGGER.debug("unit test AreaCapturaOfertaDAOBeanTest#testGetByIdConAsociaciones ...");

        AreaCapturaOferta answer;
        Long idACO;

        idACO = 81l;

        try {
            answer = this.dao.getByIdConAsociaciones(idACO);

            if (answer == null || answer.getDetalleCapturaOfertas() == null ||
                answer.getDetalleCapturaOfertas().isEmpty()) {
                fail("debía obtener un AreaCapturaOferta, y por lo menos un DetalleCapturaOferta");
            } else {
                for (DetalleCapturaOferta dco : answer.getDetalleCapturaOfertas()) {
                    LOGGER.debug("DetalleCapturaOferta.id = " + dco.getId());
                }
            }
        } catch (Exception ex) {
            fail("ocurrió un error.");
            LOGGER.error("excepción: " + ex.getMessage());
        }
    }

    @Test
    public void testGetAreaCapturaOfertaById() {

        LOGGER.debug("unit test AreaCapturaOfertaDAOBeanTest#testGetAreaCapturaOfertaById ...");

        AreaCapturaOferta answer;
        Long idACO;

        idACO = 89l;

        try {
            answer = this.dao.getAreaCapturaOfertaById(idACO);

            if (answer == null) {
                fail("debía obtener un AreaCapturaOferta");
            } else {
                LOGGER.debug(answer.getEstado());
                LOGGER.debug("DetalleCapturaOferta.id = " + answer.getId());

            }
        } catch (Exception ex) {
            fail("ocurrió un error.");
            LOGGER.error("excepción: " + ex.getMessage());
        }
    }

}
