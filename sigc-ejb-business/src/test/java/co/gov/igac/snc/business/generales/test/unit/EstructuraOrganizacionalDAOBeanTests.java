/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.dao.generales.IEstructuraOrganizacionalDAO;
import co.gov.igac.snc.dao.generales.impl.EstructuraOrganizacionalDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pedro.garcia
 */
public class EstructuraOrganizacionalDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EstructuraOrganizacionalDAOBeanTests.class);

    private IEstructuraOrganizacionalDAO dao = new EstructuraOrganizacionalDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     *
     * PRE: los datos se supone que no cambian y que la consulta funciona
     */
    @Test
    public void testFindOutIfIsSede() {

        LOGGER.debug("unit test: EstructuraOrganizacionalDAOBeanTests#testFindOutIfIsSede ...");
        String idTerritorial, codigoMunicipio;
        boolean answer;

        idTerritorial = "6360";
        codigoMunicipio = "25001";

        try {
            answer = this.dao.findOutIfIsSede(codigoMunicipio, idTerritorial);

            assertTrue(answer);
            LOGGER.debug("... passed!");
        } catch (Exception e) {
            LOGGER.error("paila!!. " + e.getMessage(), e);
            assertFalse(true);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindMunicipiosTerritorialByCodigoExtructuraOrganizacional() {
        LOGGER.debug(
            "tests: EstructuraOrganizacionalDAOBeanTests#testFindMunicipiosTerritorialByCodigoExtructuraOrganizacional");

        String estructuraOC = "6360";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<String> answer = this.dao
                .findMunicipiosTerritorialByCodigoExtructuraOrganizacional(estructuraOC);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Municipios " + answer.size());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de estructura organizacional por municipioCod
     *
     * @author franz.gamba
     */
    @Test
    public void testFindEstructuraOrgByMunicipioCod() {
        LOGGER.debug(
            "tests: EstructuraOrganizacionalDAOBeanTests#testFindEstructuraOrgByMunicipioCod");

        String municipioCod = "11001";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String answer = this.dao
                .findEstructuraOrgByMunicipioCod(municipioCod);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Estructura Organizacional : " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de códigos EO por código EO
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetCodigosEOAsociadosByCodigoEstructuraOrganizacional() {
        LOGGER.debug(
            "tests: EstructuraOrganizacionalDAOBeanTests#testGetCodigosEOAsociadosByCodigoEstructuraOrganizacional");

        String codigoEO = "6360";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<String> answer = this.dao
                .getCodigosEOAsociadosByCodigoEstructuraOrganizacional(codigoEO);

            if (answer != null && !answer.isEmpty()) {

                for (String sTmp : answer) {
                    LOGGER.debug("Código  " + sTmp);
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método test que busca una estructura organizacional por su codigo, trayendo el departamento,
     * municipio y pais en fech.
     *
     * @author david.cifuentes
     */
    @Test
    public void buscarEstructuraOrganizacionalPorCodigo() {
        LOGGER.debug(
            "tests: EstructuraOrganizacionalDAOBeanTests#buscarEstructuraOrganizacionalPorCodigo");

        String codigoEO = "6360";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            EstructuraOrganizacional answer = this.dao
                .buscarEstructuraOrganizacionalPorCodigo(codigoEO);

            if (answer != null) {
                LOGGER.debug("Estructura Organizacional : " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // end of class
}
