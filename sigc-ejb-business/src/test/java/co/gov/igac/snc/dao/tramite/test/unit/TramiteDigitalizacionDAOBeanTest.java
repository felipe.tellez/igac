package co.gov.igac.snc.dao.tramite.test.unit;

import co.gov.igac.snc.dao.tramite.ITramiteDigitalizacionDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteDigitalizacionDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class TramiteDigitalizacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TramiteDigitalizacionDAOBeanTest.class);

    private ITramiteDigitalizacionDAO dao = new TramiteDigitalizacionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarPorIdTramite() {

        LOGGER.debug("on TramiteInconsistenciaDAOBeanTest#testBuscarPorIdTramite ...");

        try {
            List<TramiteDigitalizacion> ti = dao.buscarPorIdTramite(34738l);

            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.size());
            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.get(0).getUsuarioEnvia());

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarPorDigitalizador() {

        LOGGER.debug("on TramiteInconsistenciaDAOBeanTest#testBuscarPorIdTramite ...");

        try {
            List<TramiteDigitalizacion> ti = dao.buscarPorDigitalizador("pruatlantico30");

            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.size());
            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.get(0).getUsuarioEnvia());

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarTramiteActiva() {

        LOGGER.debug("on TramiteInconsistenciaDAOBeanTest#testBuscarTramiteActiva ...");

        try {
            TramiteDigitalizacion ti = dao.buscarPorDigitalizadorYTramite("pruatlantico30", 34738l);

            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.getUsuarioEnvia());

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

    /**
     * @author felipe.cadena
     *
     */
    @Test
    public void testBuscarUltimoPorEjecutor() {

        LOGGER.debug("on TramiteInconsistenciaDAOBeanTest#testBuscarUltimoPorEjecutor ...");

        try {
            TramiteDigitalizacion ti = dao.buscarPorEjecutorYTramite("pruatlantico17", 34733l);

            LOGGER.debug(">>>>> Numero >>>>>>>>>> " + ti.getUsuarioEnvia());

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Test fallido ...");
            Assert.assertTrue(false);
        }

        Assert.assertTrue(true);

    }

}
