/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.test.unit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.generales.ILogMensajeDAO;
import co.gov.igac.snc.dao.generales.impl.LogMensajeDAOBean;
import co.gov.igac.snc.dao.tramite.ITramiteMovilesDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteMovilesDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class TramiteMovilesDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteMovilesDAOBeanTests.class);

    private static final String LOGIN_USUARIO = "pruatlantico16";

    private ITramiteMovilesDAO dao = new TramiteMovilesDAOBean();

    private ILogMensajeDAO logMensajeDAO;

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
        logMensajeDAO = new LogMensajeDAOBean();
        logMensajeDAO.setEntityManager(this.em);
        /*
         * IComisionTramiteDAO comisionDao = new ComisionTramiteDAOBean();
         * comisionDao.setEntityManager(this.em); this.dao.setRemoteComisionDAO(comisionDao);
         *
         * IPredioDAO predioDao = new PredioDAOBean(); predioDao.setEntityManager(this.em);
         */
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//--------------------------------------------------------------------------------------------------
    @Test
    public void testObtenerFichaPredialCompleta() {
        LOGGER.debug("tests: TramiteMovilesDAOBeanTests#testObtenerFichaPredialCompleta");
        try {
            Long idTramite = Long.parseLong("42817");
            String archivoGenerado = this.dao.obtenerFichaPredialCompleta(idTramite);
            assertNotNull(archivoGenerado);
            LOGGER.debug("archivoGenerado:" + archivoGenerado);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    public void testGetXmlTramiteByTramiteId() {
        LOGGER.debug("tests: TramiteMovilesDAOBeanTests#testGetXmlTramiteByTramiteId");
        try {
            Long idTramite = 34423L;
            String archivoGenerado = this.dao.getXmlTramiteByTramiteId(idTramite);
            assertNotNull(archivoGenerado);
            LOGGER.debug("archivoGenerado:" + archivoGenerado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    public void testGetXmlPredioByPredioId() {
        LOGGER.debug("tests: TramiteMovilesDAOBeanTests#testGetXmlPredioByPredioId");

        try {
            Long idPredio = 7L;
            String archivoGenerado = this.dao.getXmlPredioByPredioId(idPredio);
            assertNotNull(archivoGenerado);
            LOGGER.debug("archivoGenerado:" + archivoGenerado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

// end of class
}
