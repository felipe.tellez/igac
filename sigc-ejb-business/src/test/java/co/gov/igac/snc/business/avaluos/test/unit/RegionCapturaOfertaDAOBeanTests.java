/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import co.gov.igac.snc.dao.avaluos.IRegionCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.impl.RegionCapturaOfertaDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.test.unit.BaseTest;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class RegionCapturaOfertaDAOBeanTests extends BaseTest {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(RegionCapturaOfertaDAOBeanTests.class);

    private IRegionCapturaOfertaDAO dao = new RegionCapturaOfertaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarNoComisionadasPorIdAreaCaptura() {

        LOGGER.debug(
            "unit test RegionCapturaOfertaDAOBeanTests#testBuscarNoComisionadasPorIdAreaCaptura ...");

        List<RegionCapturaOferta> answer = null;

        List<Long> regionesCapturaIds = new ArrayList<Long>();
        regionesCapturaIds.add(435L);

        try {

            List<String> estados = new ArrayList<String>();
            estados.add(EEstadoRegionCapturaOferta.ASIGNADA.getEstado());
            estados.add(EEstadoRegionCapturaOferta.CREADA.getEstado());
            estados.add(EEstadoRegionCapturaOferta.EN_COMISION.getEstado());

            answer = this.dao.buscarRegionesPorIdsYEstados(regionesCapturaIds, estados);
            assertTrue(true);
            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("fetched " + answer.size() + " rows");
                LOGGER.debug("one RegionCapturaOferta id = " + answer.get(0).getId());
            }
        } catch (Exception ex) {
            fail("error!");
            LOGGER.error("error: " + ex.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarPorIdComision() {

        LOGGER.debug("unit test RegionCapturaOfertaDAOBeanTests#testBuscarPorIdComision ...");

        List<RegionCapturaOferta> answer = null;
        Long comisionId = 13l;

        try {
            answer = this.dao.buscarPorIdComision(comisionId);
            assertTrue(true);
            if (answer != null) {
                LOGGER.debug("fetched " + answer.size() + " rows");
                if (!answer.isEmpty()) {
                    LOGGER.debug("one RegionCapturaOferta id = " + answer.get(0).getId());
                    LOGGER.debug("AreaCapturaOferta fetched :: municipio.nombre = " +
                        answer.get(0).getAreaCapturaOferta().getMunicipio().getNombre());
                }
            } else {
                LOGGER.debug("respuesta es null");
            }
        } catch (Exception ex) {
            fail("error!");
            LOGGER.error("error: " + ex.getMessage());
        }

    }

    /**
     * @author ariel.ortiz
     *
     */
    @Test
    public void testCargarRegionCapturaOfertaPorIdAreaYRecolector() {

        LOGGER.debug(
            "unit test RegionCapturaOfertaDAOBeanTests#testCargarRegionCapturaOfertaPorIdAreaYRecolector ...");

        List<RegionCapturaOferta> answer = null;
        String idRecolector = "pruatlantico20";
        Long idArea = 146L;

        try {
            answer = this.dao.cargarRegionCapturaOfertaPorIdAreaYRecolector(idArea, idRecolector);
            assertTrue(true);
            if (answer != null) {
                LOGGER.debug("fetched " + answer.size() + " rows");
                if (!answer.isEmpty()) {
                    LOGGER.debug("one RegionCapturaOferta id = " + answer.get(0).getId());
                    LOGGER.debug("AreaCapturaOferta fetched :: municipio.nombre = " +
                        answer.get(0).getAreaCapturaOferta().getMunicipio().getNombre());
                }
            } else {
                LOGGER.debug("respuesta es null");
            }
        } catch (Exception ex) {
            fail("error!");
            LOGGER.error("error: " + ex.getMessage());
        }

    }

}
