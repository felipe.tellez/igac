package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IControlCalidadOfertaDAO;
import co.gov.igac.snc.dao.avaluos.IControlCalidadRecolectorDAO;
import co.gov.igac.snc.dao.avaluos.impl.ControlCalidadOfertaDAOBean;
import co.gov.igac.snc.dao.avaluos.impl.ControlCalidadRecolectorDAOBean;

import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadRecolector;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase para probar metodos de AreaCapturaDetalleDAOBean
 *
 * @author rodrigo.hernandez
 *
 */
public class ControlCalidadRecolectorDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadRecolectorDAOBeanTest.class);

    private IControlCalidadRecolectorDAO dao = new ControlCalidadRecolectorDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Prueba la busqueda de ControlCalidadRecolectorpor id de oferta control calidad
     *
     * @author ariel.ortiz
     */
    @Test
    public void testBuscarControlCalidadRecolectorPorIdControl() {
        LOGGER.debug(
            "tests: ControlCalidadRecolectorDAOBeanTest#buscarControlCalidadRecolectorPorIdControl");

        List<ControlCalidadRecolector> answer;

        try {
            LOGGER.debug("Entra a la prueba");

            Long controlId = 34L;

            answer = this.dao
                .buscarControlCalidadRecolectorPorIdControl(controlId);

            if (answer != null) {
                for (ControlCalidadRecolector ccr : answer) {
                    LOGGER.debug("ID " + ccr.getId().toString());
                    LOGGER.debug("RecolectorID " + ccr.getRecolectorId());
                    LOGGER.debug("Ofertas Rec " + ccr.getTotalOfertasRecolectadas());
                    LOGGER.debug("Oferats Muestras " + ccr.getTotalOfertasMuestra());
                }
            }

            assertNotNull(answer);
            LOGGER.debug("Sale de la prueba");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
