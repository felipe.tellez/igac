package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.ILevantamientoAsignacionDAO;
import co.gov.igac.snc.dao.actualizacion.ILevantamientoAsignacionInfDAO;
import co.gov.igac.snc.dao.actualizacion.impl.LevantamientoAsignacionDAOBean;
import co.gov.igac.snc.dao.actualizacion.impl.LevantamientoAsignacionInfDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacionInf;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class LevantamientoAsignacionInfDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        LevantamientoAsignacionInfDAOBeanTest.class);

    private ILevantamientoAsignacionInfDAO dao = new LevantamientoAsignacionInfDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.barajas
     */
    @Test
    public void testObtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId() {

        LOGGER.debug(
            "unit tests: ActualizacionDAOBeanTest#testObtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId");

        LevantamientoAsignacionInf answer;

        try {
            answer = this.dao.obtenerLevantamientoAsignacionInfPorLevantamientoAsignacionId(1L);

            //Assert.assertNotNull(answer);
            LOGGER.debug("levantamiento Id: " + answer.getId());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

}
