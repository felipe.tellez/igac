package co.gov.igac.snc.dao.tramite.radicacion.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.fail;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.tramite.radicacion.ISolicitudDAO;
import co.gov.igac.snc.dao.tramite.radicacion.impl.SolicitudDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.test.unit.BaseTest;

public class SolicitudDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitudDAOBeanTest.class);

    private ISolicitudDAO dao = new SolicitudDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

//    @Test
//	public void testBuscarSolicitudes() {
//    	FltroDatosSolicitud filtroDatosSolicitud = new FiltroDatosSolicitud();
//    	int desde = 0;
//    	int cantidad = 10;
//		
//    	List<Solicitud> solicitudes = dao.buscarSolicitudes(filtroDatosSolicitud, desde, cantidad);
//		for (Solicitud solicitud : solicitudes) {
//			LOGGER.info("testBuscarSolicitudes " + solicitud.getNumero());
//		}
//		
//		Assert.assertNotNull(solicitudes);
//		Assert.assertEquals(true, solicitudes.size() <= cantidad, "Fallo en el número de registros recuperados.");
//    }
    /**
     * Test para probar el tag cascade
     *
     * @author fabio.navarrete
     */
    @Test
    public void testCascadeSolicitudTramitePersist() {

        LOGGER.debug("unit test: SolicitudDAOBeanTest#testCascadeSolicitudTramitePersist");

        em.getTransaction().begin();
        Solicitud sol = this.dao
            .findSolicitudByNumSolicitudFetchGestionarSolicitudesData("666999");
        if (sol != null) {
            this.dao.delete(sol);
        }
        em.getTransaction().commit();

        em.getTransaction().begin();
        Tramite tramite = getAnyTramite();

        Solicitud solicitud = new Solicitud();
        solicitud.setFolios(2);
        solicitud.setFechaLog(new Date());
        solicitud.setUsuarioLog("fabio.navarrete.SolicitudDAOBeanTest");
        solicitud.setFecha(new Date());
        solicitud.setEstado(ESolicitudEstado.RECIBIDA.getCodigo());
        solicitud.setNumero("666999");
        solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
        solicitud.setAnexos(2);
        solicitud.getTramites().add(tramite);

        tramite.setSolicitud(solicitud);

        this.dao.persist(solicitud);

        em.getTransaction().commit();
    }

    /**
     * Test para probar el tag cascade
     *
     * @author fabio.navarrete
     */
    @Test
    public void testCascadeSolicitudTramiteMerge() {

        LOGGER.debug("unit test: SolicitudDAOBeanTest#testCascadeSolicitudTramiteMerge");

        em.getTransaction().begin();
        Tramite tramite = getAnyTramite();
        Solicitud solicitud = this.dao.buscarSolicitudFetchTramitesBySolicitudId(8989L);

        solicitud.setFolios(2);
        solicitud.setFechaLog(new Date());
        solicitud.setUsuarioLog("fabio.navarrete.SolicitudDAOBeanTest");
        solicitud.setFecha(new Date());
        solicitud.setEstado(ESolicitudEstado.RECIBIDA.getCodigo());
        solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
        solicitud.setAnexos(2);
        solicitud.getTramites().add(tramite);

        tramite.setSolicitud(solicitud);

        this.dao.update(solicitud);

        em.getTransaction().commit();
    }

    /**
     * Test para probar el tag cascade
     *
     * @author fabio.navarrete
     */
    @Test
    public void testRemoveFirstTramiteFromListaSolicitud() {
        em.getTransaction().begin();
        Solicitud solicitud = dao.findSolicitudByNumSolicitudFetchGestionarSolicitudData("666999");
        solicitud.getTramites().remove(0);
        this.dao.update(solicitud);
        em.getTransaction().commit();
    }

    /**
     * Test para probar el tag cascade
     *
     * @author fabio.navarrete
     */
    @Test
    public void testRemoveAllTramitesFromListaSolicitud() {

        LOGGER.debug("unit test: SolicitudDAOBeanTest#testRemoveAllTramitesFromListaSolicitud");

        em.getTransaction().begin();
        Solicitud solicitud = dao.findSolicitudByNumSolicitudFetchGestionarSolicitudData(
            "8002008ER450");
        solicitud.getTramites().clear();
        this.dao.update(solicitud);
        em.getTransaction().commit();
    }

    /**
     * Test para probar el tag cascade
     *
     * @author fabio.navarrete
     */
    @Test
    public void testAddTramiteToListaSolicitud() {
        em.getTransaction().begin();
        Solicitud solicitud = dao.findSolicitudByNumSolicitudFetchGestionarSolicitudData("666999");
        Tramite tramite = getAnyTramite();
        tramite.setSolicitud(solicitud);
        solicitud.getTramites().add(tramite);
        this.dao.persist(solicitud);
        em.getTransaction().commit();
    }

    /**
     * Test para probar el tag cascade
     *
     * @author fabio.navarrete
     */
    @Test
    public void testModifyTramiteInListaSolicitud() {

        LOGGER.debug("unit test: SolicitudDAOBeanTest#testModifyTramiteInListaSolicitud");

        try {
            em.getTransaction().begin();
            Solicitud solicitud = dao
                .findSolicitudByNumSolicitudFetchGestionarSolicitudData("666999");
            Tramite tramite = getAnyTramite();
            tramite.setSolicitud(solicitud);
            solicitud.getTramites().get(0)
                .setUsuarioLog("fabio.navarrete.test" + new Date());
            this.dao.persist(solicitud);
            em.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    private Tramite getAnyTramite() {
        Tramite tramite = new Tramite();
        tramite.setFechaLog(new Date());
        tramite.setUsuarioLog("fabio.navarrete.SolicitudDAOBeanTest");
        tramite.setEstado(ETramiteEstado.RECIBIDO.getCodigo());
        tramite.setTipoTramite(ETramiteTipoTramite.MUTACION.getCodigo());
        Departamento depto = new Departamento();
        depto.setCodigo("25");
        tramite.setDepartamento(depto);
        Municipio muni = new Municipio();
        muni.setCodigo("25754");
        tramite.setMunicipio(muni);
        return tramite;
    }

    /**
     * Test para probar la función correspondiente en el dao
     *
     * @author fabio.navarrete
     */
    @Test
    public void testFindSolicitudByNumSolicitudFetchGestionarSolicitudesData() {

        LOGGER.debug(
            "unit testing SolicitudDAOBeanTest#testFindSolicitudByNumSolicitudFetchGestionarSolicitudesData...");
        //8002011ER00000114
        Solicitud sol = this.dao.findSolicitudByNumSolicitudFetchGestionarSolicitudesData("666999");
        Assert.assertNotNull(sol);
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarSolicitudFetchTramitesBySolicitudId() {
        LOGGER.debug(
            "unit testing SolicitudDAOBeanTest#testBuscarSolicitudFetchTramitesBySolicitudId...");

        Long solicitudId = 1L;

        try {
            Solicitud solTemp = this.dao
                .buscarSolicitudFetchTramitesBySolicitudId(solicitudId);

            if (solTemp != null) {
                if (solTemp.getTramites() != null) {
                    LOGGER.debug("Cantidad trámites = " +
                        solTemp.getTramites().size());
                }

                for (int t = 0; t < solTemp.getTramites().size(); t++) {
                    if (solTemp.getTramites().get(t).getTramiteDocumentacions() != null &&
                        solTemp.getTramites().get(t)
                            .getTramiteDocumentacions().size() > 0) {
                        LOGGER.debug("TD tamaño = " +
                            solTemp.getTramites().get(t)
                                .getTramiteDocumentacions().size());
                        LOGGER.debug("Aportado = " +
                            solTemp.getTramites().get(t)
                                .getTramiteDocumentacions().get(0)
                                .getAportado());
                    }
                }

            }

            Assert.assertTrue(solTemp != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------------------------------------- //
    /**
     * Test para probar la búsqueda de una solicitud con sus solicitanteSolicituds.
     *
     * @author david.cifuentes
     */
    @Test
    public void testBuscarSolicitudConSolicitantesSolicitudPorId() {

        LOGGER.debug("SolicitudDAOBeanTests#testBuscarSolicitudConSolicitantesSolicitudPorId");

        Solicitud answer = null;
        Long idSolicitud = new Long(42969L);

        try {
            answer = this.dao.buscarSolicitudConSolicitantesSolicitudPorId(idSolicitud, true);
            Assert.assertNotNull(answer);
            if (answer != null) {
                LOGGER.debug("Solicitud id : " + answer.getId());
                LOGGER.debug("SolicitantesSolicitud size : " + answer.getSolicitanteSolicituds().
                    size());
            } else {
                LOGGER.debug("No se encontró la solicitud.");
            }
        } catch (Exception e) {
            LOGGER.error("error: " + e.getMessage(), e);
            fail("error: " + e.getMessage(), e);
        }
    }

    /**
     * Se prueba la actualizacion de un objeto Solicitud
     */
    @Test
    public void testUpdate() {
        LOGGER.debug("iniciando SolicitudDAOBeanTestsA#testUpdate");
        Solicitud solicitud = (Solicitud) dao.findById(42623L);

        solicitud.setAnexos(1000);
        solicitud = dao.update(solicitud);
        LOGGER.debug("Solicitud id: " + solicitud.getId());

        LOGGER.debug("finalizando SolicitudDAOBeanTests#testUpdate");
    }

}
