/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.tramite.radicacion.test.unit;

import co.gov.igac.snc.dao.tramite.radicacion.IDeterminaTramitePHDAO;
import co.gov.igac.snc.dao.tramite.radicacion.impl.DeterminaTramitePHDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.DeterminaTramitePH;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class DeterminaTramitePHDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(DatoRectificacionDAOBeanTests.class);

    private IDeterminaTramitePHDAO dao = new DeterminaTramitePHDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }

    /**
     *
     * @author felipe.cadena
     */
    @Test
    public void testObtenerPorParametrosTramite() {

        LOGGER.debug("unit test: DeterminaTramitePHDAOBeanTest#testObtenerPorParametrosTramite ...");

        List<DeterminaTramitePH> answer;

        String tipoTramite;
        String claseMutacion;
        String subtipo;
        String condicionPropiedad;

        try {
            //CASO 1
            tipoTramite = "1";
            claseMutacion = "1";
            subtipo = null;
            condicionPropiedad = "0";
            answer = this.dao.obtenerPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                condicionPropiedad);
            assertTrue(true);
            LOGGER.debug("... passed.");

            if (!answer.isEmpty()) {
                LOGGER.debug("dato: " + answer.get(0).toString());
            }
            //CASO 2
            tipoTramite = "1";
            claseMutacion = "2";
            subtipo = "ENGLOBE";
            condicionPropiedad = null;
            answer = this.dao.obtenerPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                condicionPropiedad);
            assertTrue(true);
            LOGGER.debug("... passed.");

            if (!answer.isEmpty()) {
                LOGGER.debug("dato: " + answer.get(0).toString());
            }
            //CASO 3
            tipoTramite = "1";
            claseMutacion = "2";
            subtipo = null;
            condicionPropiedad = null;
            answer = this.dao.obtenerPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                condicionPropiedad);
            assertTrue(true);
            LOGGER.debug("... passed.");

            if (!answer.isEmpty()) {
                LOGGER.debug("dato: " + answer.get(0).toString());
            }
            //CASO 4
            tipoTramite = "1";
            claseMutacion = null;
            subtipo = null;
            condicionPropiedad = null;
            answer = this.dao.obtenerPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                condicionPropiedad);
            assertTrue(true);
            LOGGER.debug("... passed.");

            if (!answer.isEmpty()) {
                LOGGER.debug("dato: " + answer.get(0).toString());
            }

            //CASO 5
            tipoTramite = null;
            claseMutacion = null;
            subtipo = null;
            condicionPropiedad = null;
            answer = this.dao.obtenerPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                condicionPropiedad);
            assertTrue(true);
            LOGGER.debug("... passed.");

            if (!answer.isEmpty()) {
                LOGGER.debug("dato: " + answer.get(0).toString());
            }

        } catch (Exception e) {
            LOGGER.error("... error " + e.getMessage(), e);
            assertFalse(true);
        }

    }

}
