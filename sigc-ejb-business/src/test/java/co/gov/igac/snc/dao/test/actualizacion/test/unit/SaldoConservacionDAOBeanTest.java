package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.ISaldoConservacionDAO;
import co.gov.igac.snc.dao.actualizacion.impl.SaldoConservacionDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.SaldoConservacion;
import co.gov.igac.snc.test.unit.BaseTest;

public class SaldoConservacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SaldoConservacionDAOBeanTest.class);

    private ISaldoConservacionDAO dao = new SaldoConservacionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Prueba obtener SaldoConservacionPorNumeroPredial
     *
     * @author andres.eslava
     */
    @Test
    public void testGetSaldosConservacionByNumeroPredial() {
        LOGGER.debug(
            "SaldoConservacionDAOBeanTest#testGetSaldosConservacionByNumeroPredial...INICIA");
        try {
            List<SaldoConservacion> saldos = dao.getSaldosConservacionByNumeroPredial("");
            Assert.assertNotNull(saldos);
            System.out.println("El primer saldo de conservacion tiene No. Predial:" + saldos.get(0).
                getNumeroPredial());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            LOGGER.debug(
                "SaldoConservacionDAOBeanTest#testGetSaldosConservacionByNumeroPredial...FINALIZA");
        }
    }
}
