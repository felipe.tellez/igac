package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPersonaDAO;
import co.gov.igac.snc.dao.conservacion.impl.PersonaDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author juan.agudelo
 *
 */
public class PersonaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PersonaDAOBeanTest.class);

    private IPersonaDAO dao = new PersonaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaByNombreDocumentoDV() {
        LOGGER.debug("unit testing PersonaDAOBeanTest#testBuscarPersonaByNombreDocumentoDV...");

        Persona persona = new Persona();
        persona.setPrimerNombre("Fredy");
        persona.setSegundoNombre("Segundo");
        persona.setPrimerApellido("Apellido1");
        persona.setSegundoApellido("Apeelido2");
        persona.setDigitoVerificacion("1");
        persona.setTipoIdentificacion("CC");
        persona.setNumeroIdentificacion("764925");

        try {
            Persona pr = this.dao.buscarPersonaByNombreDocumentoDV(persona);
            if (pr != null) {
                LOGGER.debug("Persona id = " + pr.getId());
            }

            Assert.assertTrue(pr != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testFindByTipoNumeroIdentificacion() {
        LOGGER.debug("PersonaDAOBeanTest#testFindByTipoNumeroIdentificacion");
        try {
            List<Persona> personas = this.dao.findByTipoNumeroIdentificacion(
                EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo(),
                "20315226");
            LOGGER.debug("Número de Personas encontradas: " + personas.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de documento en alfresco
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaPorNumeroYTipoIdentificacion() {
        LOGGER.debug("tests: PersonaDAOBeanTest#testBuscarPersonaPorNumeroYTipoIdentificacion");

        String numeroIdentificacion = "764925";
        String tipoIdentificacion = "CC";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Persona> answer = this.dao
                .buscarPersonaPorNumeroYTipoIdentificacion(
                    tipoIdentificacion, numeroIdentificacion);

            if (answer != null && !answer.isEmpty()) {

                for (Persona p : answer) {
                    LOGGER.debug("Nombre completo " + p.getNombre());
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * @author franz.gamba
     */
    @Test
    public void testGetPersonabyTipoIdentificacionNumeroIdentificacion() {
        LOGGER.debug(
            "tests: PersonaDAOBeanTest#testGetPersonabyTipoIdentificacionNumeroIdentificacion");

        String numeroIdentificacion = "48917716";
        String tipoIdentificacion = "CC";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Persona answer = this.dao
                .getPersonabyTipoIdentificacionNumeroIdentificacion(
                    tipoIdentificacion, numeroIdentificacion);

            if (answer != null) {

                LOGGER.debug("Nombre completo " + answer.getNombre());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de personas por nombre, tipo y número de documento
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaPorNombrePartesTipoDocNumDoc() {
        LOGGER.debug("tests: PersonaDAOBeanTest#testBuscarPersonaPorNombrePartesTipoDocNumDoc");

        Persona persona = new Persona();
        String numeroIdentificacion = "764925";
        String tipoIdentificacion = "CC";
        String primerNombre = "Fredy";
        String segundoNombre = "Segundo";
        String primerApellido = "Apellido1";
        String segundoApellido = "Apeelido2";

        persona.setTipoIdentificacionValor(tipoIdentificacion);
        persona.setNumeroIdentificacion(numeroIdentificacion);
        persona.setPrimerNombre(primerNombre);
        persona.setSegundoNombre(segundoNombre);
        persona.setPrimerApellido(primerApellido);
        persona.setSegundoApellido(segundoApellido);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Persona answer = this.dao
                .buscarPersonaPorNombrePartesTipoDocNumDoc(persona);

            if (answer != null) {

                LOGGER.debug("Nombre completo " + answer.getNombre());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
