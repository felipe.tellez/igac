package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.fail;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.impl.CodigoHomologadoDAOBean;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.ESistema;
import co.gov.igac.snc.test.unit.BaseTest;

public class CodigoHomologadoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodigoHomologadoDAOBeanTest.class);

    private CodigoHomologadoDAOBean dao = new CodigoHomologadoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author fredy.wilches
     *
     */
    //@Test(threadPoolSize =1, invocationCount = 1000)
    @Test
    public void getCodigoHomologadoTest() {
        try {
            LOGGER.debug("getCodigoHomologadoTest");
            String valor = dao.obtenerCodigoHomologado(ECodigoHomologado.ESTRUCTURA_ORGANIZACIONAL,
                ESistema.LDAP,
                ESistema.SNC, "ATLANTICO", new Date());
            Assert.assertNotNull(valor);
            LOGGER.debug("valor:" + valor);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }
}
