/**
 * IGAC proyecto SNC
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.dao.tramite.solicitante.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.ISolicitanteSolicitudDAO;
import co.gov.igac.snc.dao.tramite.impl.SolicitanteSolicitudDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pagm
 */
public class SolicitanteSolicitudDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        SolicitanteSolicitudDAOBeanTests.class);

    private ISolicitanteSolicitudDAO dao = new SolicitanteSolicitudDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: existen datos en la bd que sirven para la prueba
     */
    @Test
    public void testGetSolicitantesBySolicitud2() {

        LOGGER.debug(
            "unit test: SolicitanteSolicitudDAOBeanTests#testGetSolicitantesBySolicitud2 ...");

        long solicitudId;
        List<SolicitanteSolicitud> solicitantes;

        solicitudId = 69l;
        try {
            solicitantes = this.dao.getSolicitantesBySolicitud2(solicitudId);
            assertTrue(true);

            LOGGER.debug("... passed!. Fetched " + solicitantes.size() + " rows");
            if (!solicitantes.isEmpty()) {
                LOGGER.debug("un solicitante: " + solicitantes.get(0).getNombreCompleto());
            }

        } catch (Exception e) {
            LOGGER.error(
                "error on SolicitanteSolicitudDAOBeanTests#testGetSolicitantesBySolicitud2: " +
                e.getMessage(), e);
            assertFalse(true);
        }

    }

    @Test
    public void testGetSolicitanteSolicitudAleatorio() {
        LOGGER.debug(
            "unit test: SolicitanteSolicitudDAOBeanTests#testGetSolicitanteSolicitudAleatorio ...");
        List<SolicitanteSolicitud> solicitantes = this.dao.getSolicitanteSolicitudAleatorio();
        Assert.assertNotNull(solicitantes);
        for (SolicitanteSolicitud ss : solicitantes) {
            LOGGER.debug(" " + ss.getId() + "  " + ss.getSolicitante().getId());
        }
    }

    @Test
    public void testFindBySolicitudId() {
        LOGGER.debug(
            "unit test: SolicitanteSolicitudDAOBeanTests#testGetSolicitanteSolicitudAleatorio ...");
        List<SolicitanteSolicitud> solicitantes = this.dao.findBySolicitudId(45978l);
        Assert.assertNotNull(solicitantes);
        for (SolicitanteSolicitud ss : solicitantes) {
            LOGGER.debug(" " + ss.getId() + "  " + ss.getSolicitante().getId());
        }
    }

//end of class
}
