/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;
import co.gov.igac.snc.dao.conservacion.IColindanteDAO;
import co.gov.igac.snc.dao.conservacion.impl.ColindanteDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * pruebas unitarias de la clase ColindanteDAOBean
 *
 * @author pedro.garcia
 */
public class ColindanteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ColindanteDAOBeanTests.class);

    private IColindanteDAO dao = new ColindanteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: con datos de 16-03-2012 (que son de prueba) debe pasar
     */
    @Test
    public void isAdjacentTest() {

        LOGGER.debug("on ColindanteDAOBeanTests#isAdjacentTest ...");

        String numeroPredialPreguntado = "080010101000000280013000000000";

        ArrayList<String> numerosPredialesComparados = new ArrayList<String>();
        numerosPredialesComparados.add("080010101000000280007000000000");
        numerosPredialesComparados.add("080010101000000280009000000000");

        boolean answer;

        answer = this.dao.isAdjacent(numeroPredialPreguntado, numerosPredialesComparados);

        Assert.assertTrue(answer);
        LOGGER.debug("passed!. Son colindates");

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de números prediales colindantes por número predial
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindPrediosColindantesByNumeroPredial() {
        LOGGER.debug("tests: ColindanteDAOBeanTests#testFindPrediosColindantesByNumeroPredial");

        String numeroPredial = "080010101000000280012000000000";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<String> answer = this.dao
                .findPrediosColindantesByNumeroPredial(numeroPredial);

            if (answer != null && !answer.isEmpty()) {

                for (String temp : answer) {
                    LOGGER.debug("Número predial colindante " + temp);
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
