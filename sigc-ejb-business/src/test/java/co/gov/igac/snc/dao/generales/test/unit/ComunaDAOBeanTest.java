package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Comuna;

import co.gov.igac.snc.dao.generales.impl.ComunaDAOBean;

import co.gov.igac.snc.test.unit.BaseTest;

public class ComunaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ComunaDAOBeanTest.class);

    private ComunaDAOBean dao = new ComunaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author lorena.salamanca
     *
     */
    @Test
    public void findBySectorTest() {
        List<Comuna> c;
        String codigo = "080010101";
        try {
            LOGGER.debug("Entrando al test de encontrar comuna por codigo de Sector");
            c = this.dao.findBySector(codigo);
            LOGGER.debug("la lista mide :  " + c.size());
            LOGGER.debug("nombre comuna :  " + c.get(0).getNombre());
            LOGGER.debug("Salida método ComunaDAOBeanTest#findBySectorTest");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción capturada");
        }
    }

}
