package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IFotoDAO;
import co.gov.igac.snc.dao.conservacion.impl.FotoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author juan.agudelo
 *
 */
public class FotoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FotoDAOBeanTest.class);

    private IFotoDAO dao = new FotoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     */
    @Test
    public void testGetTramitesByNumeroPredialPredio() {
        try {
            List<Foto> fotos;
            fotos = dao
                .buscarFotografiaPorIdPredioAndFotoTipo(Long.valueOf(688279),
                    EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo());

            LOGGER.debug("trajo " + fotos.size() + " filas");

            assertNotNull(fotos);
            assertTrue(fotos.size() >= 1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @author felipe.cadena
     */
    @Test
    public void testBuscarPorIdUnidadConstruccionYTipo() {

        LOGGER.debug("iniciando ?#testBuscarPorIdUnidadConstruccionYTipo");
        try {
            List<Foto> fotos;
            fotos = dao
                .buscarPorIdUnidadConstruccionYTipo(9833867l,
                    EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo());

            LOGGER.debug(">>>>> " + fotos.size() + " filas");

            assertNotNull(fotos);
            assertTrue(fotos.size() >= 1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);

        }

        LOGGER.debug("Finalizando AvaluoDAOBeanTest#testBuscarPorIdUnidadConstruccionYTipo");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarFotografiasPredio() {

        LOGGER.debug("unit test FotoDAOBean#BuscarFotografiasPredio ...");

        Long idPredio;
        List<Foto> answer;

        idPredio = 525668L;

        try {
            answer = this.dao.buscarFotografiasPredio(idPredio);
            LOGGER.debug("passed");
            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("   fetched " + answer.size() + " rows");
            }
        } catch (Exception ex) {
            fail("error: " + ex.getMessage());
        }
    }

}
