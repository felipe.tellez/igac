package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.dao.generales.IManzanaVeredaDAO;
import co.gov.igac.snc.dao.generales.impl.ManzanaVeredaDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class ManzanaVeredaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ManzanaVeredaDAOBeanTest.class);

    private IManzanaVeredaDAO dao = new ManzanaVeredaDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author lorena.salamanca
     *
     */
    @Test
    public void findByBarrioTest() {
        List<ManzanaVereda> c;
        String codigo = "0800100010000";
        try {
            LOGGER.debug("Entrando al test de encontrar comuna por codigo de Sector");
            c = this.dao.findByBarrio(codigo);
            LOGGER.debug("la lista mide :  " + c.size());
            LOGGER.debug("nombre ManzanaVereda :  " + c.get(0).getNombre());
            LOGGER.debug("Salida método ManzanaVeredaDAOBeanTest#findByBarrioTest");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
            assertTrue(false);
        }
    }

    /**
     * @author javier.barajas
     *
     */
    @Test
    public void findByTest() {
        List<ManzanaVereda> c;
        String codigo = "0800101080000";
        try {
            LOGGER.debug("Entrando al test de encontrar comuna por codigo de Sector");
            c = this.dao.findByBarrio(codigo);
            LOGGER.debug("la lista mide :  " + c.size());
            LOGGER.debug("nombre ManzanaVereda :  " + c.get(0).getNombre());
            LOGGER.debug("Salida método ManzanaVeredaDAOBeanTest#findByBarrioTest");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
            assertTrue(false);
        }
    }

    /**
     * Test Obtener Manzanas de Municipio
     *
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerManzanasPorMunicipio() {
        LOGGER.debug("testObtenerManzanasPorMunicipio...INICIA");
        List<ManzanaVereda> manzanasMunicipio = null;
        try {
            manzanasMunicipio = dao.obtenerManzanasPorMunicipio("08001");
            Assert.assertNotNull(manzanasMunicipio);
            Assert.assertTrue(manzanasMunicipio.size() > 0);
            System.out.println("La primera manzana recuperada es: " + manzanasMunicipio.get(0).
                getCodigo());
        } catch (Exception e) {
            e.printStackTrace();
            fail("No se encontraron Manzanas");
        } finally {
            LOGGER.debug("testObtenerManzanasPorMunicipio...FINALIZA");
        }

    }

    /**
     * Test Obtener Manzanas por rango
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerManzanasPorRango() {
        LOGGER.debug("testObtenerManzanasPorRango...INICIA");
        try {
            List<ManzanaVereda> manzanasRango = dao.obtenerManzanasPorRango("08001010800000001",
                "08001010800000012");
            Assert.assertNotNull(manzanasRango);
            System.out.println("La cantidad de manzanas en el rango es" + manzanasRango.size());
            System.out.println("La primera Manzana Recuperada es: " + manzanasRango.get(0).
                getCodigo());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            LOGGER.debug("testObtenerManzanasPorRango...FINALIZA");
        }
    }

    /**
     * Test Obtener Manzanas de Municipio para asiganar coordinadores
     *
     *
     * @author javier.barajas
     */
    @Test
    public void testObtenerManazanasporMunicipioAsignarCoordinadorPaginacion() {
        LOGGER.debug("testObtenerManazanasporMunicipioAsignarCoordinadorPaginacion...INICIA");
        List<ManzanaVereda> manzanasMunicipio = null;
        try {

            manzanasMunicipio = dao.
                obtenerManazanasporMunicipioAsignarCoordinadorPaginacion("08001", null);
            Assert.assertNotNull(manzanasMunicipio);
            Assert.assertTrue(manzanasMunicipio.size() > 0);
            System.out.println("La primera manzana recuperada es: " + manzanasMunicipio.size());
        } catch (Exception e) {
            e.printStackTrace();
            fail("No se encontraron Manzanas");
        } finally {
            LOGGER.debug("testObtenerManazanasporMunicipioAsignarCoordinadorPaginacion...FINALIZA");
        }

    }

}
