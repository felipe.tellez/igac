package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPersonaPredioPropiedadDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPersonaPredioPropiedadDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class PPersonaPredioPropiedadDAOBeanTest extends BaseTest {

    private IPPersonaPredioPropiedadDAO dao = new PPersonaPredioPropiedadDAOBean();

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPersonaPredioDAOBeanTest.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

}
