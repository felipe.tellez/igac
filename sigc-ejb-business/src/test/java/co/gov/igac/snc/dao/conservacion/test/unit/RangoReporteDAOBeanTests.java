
/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.IRangoReporteDAO;
import co.gov.igac.snc.dao.conservacion.impl.RangoReporteDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.RangoReporte;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test para el acceso a datos de la entidad RangoReporte
 *
 * @author felipe.cadena
 */
public class RangoReporteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(RangoReporteDAOBeanTests.class);

    private IRangoReporteDAO dao = new RangoReporteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author {autor}
     */
    @Test
    public void testUpdate() {

        LOGGER.debug("RangoReporteDAOBeanTests#testUpdate");

        try {

            RangoReporte testEntity = new RangoReporte();

            testEntity.setReporteEjecucionId(394L);
            testEntity.setZona("str::9qc3y4uqpu5i");
            testEntity.setTipo("str::80vf3gio8vhh");
            testEntity.setValorInicial(15.15d);
            testEntity.setValorFinal(18.15d);
            testEntity.setUsuarioLog("str::rsypb2kpgazdc0v9jatt9");
            testEntity.setFechaLog(new Date());

            this.dao.getEntityManager().getTransaction().begin();
            testEntity = this.dao.update(testEntity);
            this.dao.getEntityManager().getTransaction().commit();
            assertNotNull(testEntity);
            assertNotNull(testEntity.getId());
            LOGGER.debug("***********************");
            LOGGER.debug(testEntity.getId().toString());
            LOGGER.debug("***********************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
}
