package co.gov.igac.snc.dao.tramite.test.unit;

import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.IVSolicitudPredioDAO;
import co.gov.igac.snc.dao.tramite.impl.VSolicitudPredioDAOBean;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.test.unit.BaseTest;

public class VSolicitudPredioDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VSolicitudPredioDAOBeanTests.class);

    private IVSolicitudPredioDAO dao = new VSolicitudPredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarSolicitudPorFiltros() {
        List<VSolicitudPredio> vSolicitudPredio = new ArrayList<VSolicitudPredio>();
        int desde = 1;
        int cantidad = 10;
        VSolicitudPredio datosFiltroSolicitud = new VSolicitudPredio();
        datosFiltroSolicitud.setTipoIdentificacion("CC");
        datosFiltroSolicitud.setNumeroIdentificacion("1052365987");
        datosFiltroSolicitud.setNumeroPredial("257540216041711057933105117115");
        vSolicitudPredio = dao.buscarSolicitudPorFiltros(datosFiltroSolicitud,
            desde, cantidad);
        Assert.assertNotNull(vSolicitudPredio);
        Assert.assertTrue(vSolicitudPredio.size() >= 1);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda de solicitudes por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testCountSolicitudesPaginadasByFiltro() {
        LOGGER.debug("tests: VSolicitudPredioDAOBeanTest#testCountSolicitudesPaginadasByFiltro");

        VSolicitudPredio filtro = cargarDatosBaseFiltroSolicitudPredio();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answer = this.dao.countSolicitudesPaginadasByFiltro(filtro);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método que crea un filtro de solicitud predio con los datos base
     */
    private VSolicitudPredio cargarDatosBaseFiltroSolicitudPredio() {

        VSolicitudPredio answer = new VSolicitudPredio();

        answer.setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
            .getCodigo());
        answer.setNumeroIdentificacion("1055");

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda de solicitudes por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindtSolicitudesPaginadasByFiltro() {
        LOGGER.debug("tests: VSolicitudPredioDAOBeanTest#testFindtSolicitudesPaginadasByFiltro");

        VSolicitudPredio filtro = cargarDatosBaseFiltroSolicitudPredio();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            int[] rowStartIdxAndCount = new int[2];
            rowStartIdxAndCount[0] = 1;
            rowStartIdxAndCount[1] = 10;

//			List<VSolicitudPredio> answer = this.dao
//					.findSolicitudesPaginadasByFiltro(filtro,"",new SortOrder(),
//							rowStartIdxAndCount);
//			if (answer != null) {
//
//				if (answer != null && !answer.isEmpty()) {
//
//					LOGGER.debug("Tamaño: " + answer.size());
//
//					for (VSolicitudPredio vsTmp : answer) {
//						LOGGER.debug("Solicitud: " + vsTmp.getSolicitudId());
//					}
//				}
//			}
//			assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
}
