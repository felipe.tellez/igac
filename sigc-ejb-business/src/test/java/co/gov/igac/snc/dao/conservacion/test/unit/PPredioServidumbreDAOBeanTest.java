package co.gov.igac.snc.dao.conservacion.test.unit;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPredioServidumbreDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPredioDAOBean;
import co.gov.igac.snc.dao.conservacion.impl.PPredioServidumbreDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioServidumbre;
import co.gov.igac.snc.test.unit.BaseTest;

public class PPredioServidumbreDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioDAOBean.class);

    private IPPredioServidumbreDAO dao = new PPredioServidumbreDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }
}
