package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPPredioAvaluoCatastralDAO;
import co.gov.igac.snc.dao.conservacion.impl.PPredioAvaluoCatastralDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class PPredioAvaluoCatastralDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PPredioAvaluoCatastralDAOBeanTest.class);

    private IPPredioAvaluoCatastralDAO dao = new PPredioAvaluoCatastralDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testFindValorAvaluoByNumeroPredial() {
        LOGGER.debug("tests: PPredioAvaluoCatastralDAOBeanTest#testFindValorAvaluoByNumeroPredial");

        String numeroPredial = "257540500000000010019000000000";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Double answer = this.dao
                .findValorAvaluoByNumeroPredial(numeroPredial);

            if (answer != null) {

                LOGGER.debug("Avalúo: " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
