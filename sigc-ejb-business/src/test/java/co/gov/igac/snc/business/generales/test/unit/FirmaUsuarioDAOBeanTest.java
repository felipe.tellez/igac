package co.gov.igac.snc.business.generales.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.IFirmaUsuarioDAO;
import co.gov.igac.snc.dao.generales.impl.FirmaUsuarioDAOBean;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.test.unit.BaseTest;

public class FirmaUsuarioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirmaUsuarioDAOBeanTest.class);

    private IFirmaUsuarioDAO dao = new FirmaUsuarioDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author
     */
    @Test
    public void testFindById() {

        LOGGER.debug("unit test: FirmaUsuarioDAOBeanTest#testFindById ...");
        try {
            long Id = 2L;
            FirmaUsuario firma = dao.findById(Id);
            assertNotNull(firma);

        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author
     */
    @Test
    public void testUpdateId() {

        LOGGER.debug("unit test: FirmaUsuarioDAOBeanTest#testFindById ...");
        this.em.getTransaction().begin();
        try {
            long Id = 2L;
            FirmaUsuario firma = dao.findById(Id);
            firma.setUsuarioConFirma("SI");
            dao.update(firma);
            this.em.getTransaction().commit();

            assertNotNull(firma);

        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testInsertId() {

        LOGGER.debug("unit test: FirmaUsuarioDAOBeanTest#testFindById ...");
        this.em.getTransaction().begin();
        try {
            long Id = 2L;
            FirmaUsuario firma = dao.findById(Id);

            FirmaUsuario firma2 = new FirmaUsuario();
            firma2.setDocumentoId(firma.getDocumentoId());
            firma2.setFechaLog(firma.getFechaLog());
            firma2.setNombreFuncionarioFirma(firma.getNombreFuncionarioFirma());
            firma2.setUsuarioIngresaFirma(firma.getUsuarioIngresaFirma());
            firma2.setUsuarioLog(firma.getUsuarioLog());
            firma2.setUsuarioConFirma("SI");
            firma2.setFechaFirmaDocumento(firma.getFechaFirmaDocumento());

            dao.update(firma2);
            this.em.getTransaction().commit();

            assertNotNull(firma);

        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

}
