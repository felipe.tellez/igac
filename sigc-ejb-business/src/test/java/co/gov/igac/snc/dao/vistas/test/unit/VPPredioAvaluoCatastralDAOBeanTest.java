package co.gov.igac.snc.dao.vistas.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.vistas.IVPPredioAvaluoDecretoDAO;
import co.gov.igac.snc.dao.vistas.impl.VPPredioAvaluoDecretoDAOBean;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.test.unit.BaseTest;

public class VPPredioAvaluoCatastralDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        VPPredioAvaluoCatastralDAOBeanTest.class);

    private IVPPredioAvaluoDecretoDAO dao = new VPPredioAvaluoDecretoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        this.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        this.tearDown();
    }

    // ------------------------------------- //
    /**
     * @author david.cifuentes
     */
    @Test
    public void testBuscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto() {
        LOGGER.debug(
            "test VPPredioAvaluoDecretoDAOBeanTest#testBuscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto");

        Long pPredioId = 725001L;
        Date fechaDeCalculo = new Date(System.currentTimeMillis());
        fechaDeCalculo.setYear(95);
        try {

            List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastrales = this.dao
                .buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(pPredioId,
                    fechaDeCalculo);
            assertNotNull(pPrediosAvaluosCatastrales);
            assertTrue(pPrediosAvaluosCatastrales.size() > 0);
            LOGGER.info("Size return avalúos catastrales: " + pPrediosAvaluosCatastrales.size());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
}
