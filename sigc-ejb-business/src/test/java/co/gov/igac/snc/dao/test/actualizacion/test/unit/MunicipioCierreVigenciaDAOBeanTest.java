package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.dao.actualizacion.IMunicipioCierreVigenciaDAO;
import co.gov.igac.snc.dao.actualizacion.impl.MunicipioCierreVigenciaDAOBean;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase test para los métodos de persistencia de {@link MunicipioCierreVigencia}
 *
 * @author david.cifuentes
 *
 */
public class MunicipioCierreVigenciaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MunicipioCierreVigenciaDAOBeanTest.class);

    private IMunicipioCierreVigenciaDAO dao = new MunicipioCierreVigenciaDAOBean();

    @SuppressWarnings("static-access")
    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Test del método consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios
     *
     * @author david.cifuentes
     */
    @Test
    public void testConsultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios() {

        LOGGER.debug(
            "unit test: MunicipioCierreVigenciaDAOBeanTest#testConsultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios ...");

        DecretoAvaluoAnual decretoAvaluoAnual = new DecretoAvaluoAnual();
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.YEAR, 2016);
        Date vigencia = calendar.getTime();

        decretoAvaluoAnual.setId(77L);
        decretoAvaluoAnual.setVigencia(vigencia);
        List<String> codigosMunicipios = new ArrayList<String>();
        codigosMunicipios.add("08001");

        try {
            List<MunicipioCierreVigencia> answer = (List<MunicipioCierreVigencia>) dao
                .consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
                    decretoAvaluoAnual, codigosMunicipios);
            Assert.assertNotNull(answer);
            Assert.assertTrue(answer.size() >= 1);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Test del método consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios
     *
     * @author david.cifuentes
     */
    @Test
    public void testValidarExistenciaMunicipiosPorDecreto() {

        LOGGER.debug(
            "unit test: MunicipioCierreVigenciaDAOBeanTest#validarExistenciaMunicipiosPorDecreto ...");

        DecretoAvaluoAnual decretoAvaluoAnual = new DecretoAvaluoAnual();
        decretoAvaluoAnual.setId(109L);
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("SNC_TEST");
        try {
            dao.validarExistenciaMunicipiosPorDecreto(decretoAvaluoAnual, usuario);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }
}
