package co.gov.igac.snc.business.avaluos.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.ISolicitudDocumentacionDAO;
import co.gov.igac.snc.dao.avaluos.impl.SolicitudDocumentacionDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.test.unit.BaseTest;

public class SolicitudDocumentacionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitudDocumentacionDAOBeanTest.class);

    private ISolicitudDocumentacionDAO dao = new SolicitudDocumentacionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testObtenerListaDocsBasicosDeSolicitudAvaluo() {

        LOGGER.debug(
            "unit test: SolicitudDocumentacionDAOBeanTest#testCascadeSolicitudTramitePersist");

        List<SolicitudDocumentacion> result = this.dao.obtenerListaDocsBasicosDeSolicitudAvaluo();

        for (SolicitudDocumentacion sd : result) {
            LOGGER.debug(sd.getTipoDocumento().getId() + " - " + sd.getTipoDocumento().getNombre());
        }

    }

    @Test
    public void testObtenerSolicitudDocumentoPorSolicitudId() {

        LOGGER.debug(
            "unit test: SolicitudDocumentacionDAOBeanTest#testCascadeSolicitudTramitePersist");

        List<SolicitudDocumentacion> result = this.dao.
            obtenerListaSolDocsPorSolicitudYTipoDocumento(Long.valueOf(35090),
                ETipoDocumento.CONSTANCIA_DE_RADICACION.getId());

        for (SolicitudDocumentacion sd : result) {
            LOGGER.debug(sd.getTipoDocumento().getId() + " - " + sd.getTipoDocumento().getNombre());
        }

    }
}
