/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IProfesionalAvaluoDAO;
import co.gov.igac.snc.dao.avaluos.impl.ProfesionalAvaluoDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Clase de pruebas para ProfesionalAvaluosDAOBean
 *
 * @author felipe.cadena
 */
public class ProfesionalAvaluoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProfesionalAvaluoDAOBeanTest.class);
    private IProfesionalAvaluoDAO dao = new ProfesionalAvaluoDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar {@link ProfesionalAvaluoDAOBeanTest#testBuscarPorFiltro()}
     *
     * @author felipe.cadena
     */
    @Test
    public void testBuscarPorFiltro() {
        LOGGER.debug("iniciando ProfesionalAvaluoDAOBeanTest#testBuscarPorFiltro");

        List<ProfesionalAvaluo> result = null;
        FiltroDatosConsultaProfesionalAvaluos filtro = new FiltroDatosConsultaProfesionalAvaluos();
        filtro.setTipoVinculacion(EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA.getCodigo());
        try {
            result = dao.buscarPorFiltro(filtro);

            LOGGER.debug("******************************************");
            LOGGER.debug(
                "****************" + result.get(0).getProfesionalAvaluosContratos().size() +
                "*******************");
            LOGGER.debug(
                "****************" + result.get(1).getProfesionalAvaluosContratos().size() +
                "*******************");
            LOGGER.debug("******************************************");
            LOGGER.debug(String.valueOf(result));

        } catch (Exception ex) {
            Assert.fail("error: " + ex.getMessage());
        }

        LOGGER.debug("finalizando ProfesionalAvaluoDAOBeanTest#testBuscarPorFiltro");
    }
}
