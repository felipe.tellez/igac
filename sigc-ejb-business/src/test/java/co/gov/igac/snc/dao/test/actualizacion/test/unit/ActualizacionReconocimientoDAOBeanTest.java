package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IActualizacionReconocimientoDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ActualizacionReconocimientoDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionReconocimiento;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.test.unit.BaseTest;

public class ActualizacionReconocimientoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ActualizacionReconocimientoDAOBeanTest.class);

    private IActualizacionReconocimientoDAO dao = new ActualizacionReconocimientoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author javier.barajas
     */
    @Test
    public void testRecuperarConveniosPorIdActualizacion() {
        Long actualizacionId = 450L;
        boolean rh = false;
        LOGGER.debug(
            "unit tests: ActualizacionReconocimientoDAOBeanTest#getActualizacionReconocimientoByActualizacionId");
        try {
            List<ActualizacionReconocimiento> answer = this.dao.
                getActualizacionReconocimientoByActualizacionId(actualizacionId, null);
            Assert.assertNotNull(answer);

            LOGGER.debug("tamaño de la lista:" + answer.size());

            for (ActualizacionReconocimiento ar : answer) {
                if (ar.getRecursoHumano() != null) {
                    LOGGER.debug("tamaño de la lista:" + ar.getRecursoHumano().getNombre());
                    rh = true;
                }

            }
            LOGGER.debug("hay recurso humano:" + rh);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author javier.barajas
     */
    @Test
    public void testGetActualizacionReconocimientoporRecursoHumanoId() {

        LOGGER.debug(
            "unit tests: ActualizacionReconocimientoDAOBeanTest#getActualizacionReconocimientoporRecursoHumanoId");
        try {
            List<ActualizacionReconocimiento> answer = this.dao.
                getActualizacionReconocimientoporRecursoHumanoId(41L);
            Assert.assertNotNull(answer);

            LOGGER.debug("tamaño de la lista:" + answer.size());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

}
