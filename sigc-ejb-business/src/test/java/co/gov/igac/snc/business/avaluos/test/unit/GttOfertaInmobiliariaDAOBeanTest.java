package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IGttOfertaInmobiliariaDAO;
import co.gov.igac.snc.dao.avaluos.impl.GttOfertaInmobiliariaDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class GttOfertaInmobiliariaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        GttOfertaInmobiliariaDAOBeanTest.class);

    private IGttOfertaInmobiliariaDAO dao = new GttOfertaInmobiliariaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de generación del número de secuencia de la tabla GttOfertaInmobiliaria
     *
     * @author javier.aponte
     */
    @Test
    public void testGenerarSecuenciaGttOfertaInmobiliaria() {
        LOGGER.debug(
            "tests: GttOfertaInmobiliariaDAOBeanTest#testGenerarSecuenciaGttOfertaInmobiliaria");

        Long answer;
        try {
            LOGGER.debug(
                "Entra a la prueba de generar número de secuencia de la tabla gttOfertaInmobiliaria");
            answer = this.dao.generarSecuenciaGttOfertaInmobiliaria();
            if (answer != null) {
                LOGGER.debug("Secuencia: " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug(
                "Sale de la prueba de generar número de secuencia de la tabla gttOfertaInmobiliaria");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
