/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.test.unit;

import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionDAO;
import static org.testng.Assert.*;
import co.gov.igac.snc.dao.tramite.gestion.IComisionTramiteDAO;
import co.gov.igac.snc.dao.tramite.gestion.impl.ComisionDAOBean;
import co.gov.igac.snc.dao.tramite.gestion.impl.ComisionTramiteDAOBean;
import co.gov.igac.snc.dao.tramite.impl.TramiteDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class ComisionTramiteDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionTramiteDAOBeanTests.class);

    private IComisionTramiteDAO dao = new ComisionTramiteDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: la comisión y el trámite existen en la bd. la tabla tiene el pk sobre el id
     *
     * N: se puede usar JPA creando una comisión y un trámite que solo tienen el campo id. Como se
     * supone que esos registros de comisión y trámite ya existen, esa prueba funciona
     */
    @Test
    public void testInsert() {

        LOGGER.debug("unit test ComisionTramiteDAOBeanTests#testInsert ...");
        ComisionTramite insertMe;
        Comision comision;
        Tramite tramite;
        long idComision, idTramite;
        IComisionDAO daoComision = new ComisionDAOBean();
        ITramiteDAO daoTramite = new TramiteDAOBean();

        daoComision.setEntityManager(this.em);
        daoTramite.setEntityManager(this.em);

        insertMe = new ComisionTramite();
        //comision = new Comision();
        //tramite = new Tramite();

        idComision = 10l;
        idTramite = 3l;

        comision = daoComision.findById(idComision);
        tramite = daoTramite.findById(idTramite);

        if (comision != null && comision.getId() != null && tramite != null && tramite.getId() !=
            null) {
            insertMe.setComision(comision);
            insertMe.setTramite(tramite);
            insertMe.setFechaLog(new Date());
            insertMe.setUsuarioLog("insertado en pruebas unitarias. registro solito.");

            try {
                this.dao.getEntityManager().getTransaction().begin();
                this.dao.persist(insertMe);
                this.dao.getEntityManager().getTransaction().commit();

                assertTrue(true);
                LOGGER.debug("passed!");
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                assertFalse(true);
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: la comisión y el trámite existen en la bd. la tabla tiene el pk sobre el id
     *
     * N: se puede usar JPA creando una comisión y un trámite que solo tienen el campo id. Como se
     * supone que esos registros de comisión y trámite ya existen, esa prueba funciona
     */
    @Test
    public void testDelete() {

        LOGGER.debug("unit test ComisionTramiteDAOBeanTests#testDelete ...");

        ComisionTramite deleteMe;
        Comision comision;
        Tramite tramite;

        deleteMe = new ComisionTramite();
        comision = new Comision();
        tramite = new Tramite();

//        comision.setId(10l);
//        tramite.setId(5l);
//        deleteMe.setComision(comision);
//        deleteMe.setTramite(tramite);
        List<String[]> criteria = new ArrayList<String[]>();
        ArrayList<ComisionTramite> found;
        String[] criterion1, criterion2;
        criterion1 = new String[2];
        criterion2 = new String[2];
        criterion1[0] = "TRAMITE_ID";
        criterion1[1] = "5";
        criterion2[0] = "COMISION_ID";
        criterion2[1] = "10";

        try {
            found = (ArrayList<ComisionTramite>) this.dao.findByExactCriteria(criteria);
            if (found != null && !found.isEmpty()) {
                deleteMe = found.get(0);
                this.dao.getEntityManager().getTransaction().begin();
                this.dao.delete(deleteMe);
                this.dao.getEntityManager().getTransaction().commit();
            } else {
                LOGGER.debug("no encontró la de borrar!");
            }
            assertTrue(true);
            LOGGER.debug("passed!");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testGetComisionDuration() {

        LOGGER.debug("unit test ComisionTramiteDAOBeanTests#testGetComisionDuration ...");

        Long idComision = 140l;
        Double duration = 0.0;

        try {
            duration = this.dao.getComisionDuration(idComision);

            assertTrue(true);
            LOGGER.debug("passed!. Duration = " + duration);
        } catch (Exception e) {
            LOGGER.error("failed!" + e.getMessage(), e);
            assertFalse(true);
        }

    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testGetComisionDuration2() {

        LOGGER.debug("unit test ComisionTramiteDAOBeanTests#testGetComisionDuration2 ...");

        Long idComision = 682l;
        Double duration = 0.0;

        try {
            duration = this.dao.getComisionDuration2(idComision);

            assertNotNull(duration);
            LOGGER.debug("passed!. Duration = " + duration);
        } catch (Exception e) {
            LOGGER.error("failed!" + e.getMessage(), e);
            assertFalse(true);
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author ???
     * @modified pedro.garcia no se puede garantizar que el resultado no sea null porque puede no
     * haber datos coincidentes
     */
    @Test
    public void testBuscarComisionesTramitePorIdTramite() {
        LOGGER.debug(
            "unit test ComisionTramiteDAOBeanTests#testBuscarComisionTramitePorIdTramite...");

        Long idTramite = 97l;
        List<ComisionTramite> comisionesTramite;
        try {
            comisionesTramite = dao.buscarComisionesTramitePorIdTramite(idTramite);

            if (comisionesTramite != null) {
                LOGGER.debug("trajo " + comisionesTramite.size() + " ComisionTramite");
                LOGGER.debug("comisión del primero: " + comisionesTramite.get(0).getComision().
                    getNumero());
            } else {
                LOGGER.debug("el resultado fue null. Revise datos!!!");
            }

            //Assert.assertNotNull(comisionesTramite);
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de comisionTramite por comisión Id
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindComisionesTramiteByComisionId() {
        LOGGER.debug("tests: ComisionTramiteDAOBeanTests#testFindComisionesTramiteByComisionId");

        Long comisionId = 655l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<ComisionTramite> answer = dao
                .findComisionesTramiteByComisionId(comisionId);

            if (answer != null && !answer.isEmpty()) {

                for (ComisionTramite cTemp : answer) {

                    LOGGER.debug("ComisionTramite " + cTemp.getId());
                    LOGGER.debug("Comision " + cTemp.getComision().getId());
                    LOGGER.debug("Tramite " + cTemp.getTramite().getId());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * @author pedro.garcia
     */
    @Test
    public void testContarComisionesEfectivasDeTramite() {

        LOGGER.debug(
            "unit tests: ComisionTramiteDAOBeanTests#testContarComisionesEfectivasDeTramite");
        int answer;
        Long tramiteId;
        String tramiteIdDato;

        // DATO
        tramiteIdDato = "22594";  // dede dar 0
        tramiteIdDato = "22450";  // debe dar > 0

        tramiteId = new Long(tramiteIdDato);

        LOGGER.debug("contando las comisiones efectivas del trámite " + tramiteIdDato);
        try {
            answer = this.dao.contarComisionesEfectivasDeTramite(tramiteId,
                EComisionTipo.CONSERVACION.getCodigo());
            Assert.assertTrue(answer >= 0);
            LOGGER.debug("respuesta = " + answer);
        } catch (Exception e) {
            //LOGGER.error("Ocurrió un error contando las comisiones de un trámite: " + e.getMessage());
            fail("Ocurrió un error contando las comisiones de un trámite: " + e.getMessage());
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     * @modified pedro.garcia no se puede garantizar que el resultado no sea null porque puede no
     * haber datos coincidentes
     */
    @Test
    public void testBuscarComisionesTramiteNoRealizadasPorIdTramite() {
        LOGGER.debug(
            "unit test ComisionTramiteDAOBeanTests#testBuscarComisionesTramiteNoRealizadasPorIdTramite...");

        Long idTramite = 46545L;
        List<ComisionTramite> comisionesTramite;
        try {
            comisionesTramite = dao.buscarComisionesTramiteNoRealizadasPorIdTramite(idTramite);

            if (comisionesTramite != null) {
                LOGGER.debug("trajo " + comisionesTramite.size() + " ComisionTramite");
                LOGGER.debug("comisión del primero: " + comisionesTramite.get(0).getComision().
                    getNumero());
            } else {
                LOGGER.debug("el resultado fue null!!!");
            }

            //Assert.assertNotNull(comisionesTramite);
            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

}
