package co.gov.igac.snc.dao.util.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.util.ProcedimientoAlmacenadoDAO;

import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncionTipo;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.test.unit.BaseTest;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 *
 */
public class ProcedimientoAlmacenadoDAOTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProcedimientoAlmacenadoDAOTests.class);

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        // dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba unitaria para probar la ejecuci{on de SP y funciones de oracle
     *
     * @author fredy.wilches
     */
    @Test
    public void testEjecutaProcedimientoSinParametrosError() {
        LOGGER.debug("testEjecutaProcedimientoSinParametrosError");

        try {
            ProcedimientoAlmacenadoDAO procedimientoDAO =
                new ProcedimientoAlmacenadoDAO(getConnection());
            Object resultado[] = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS, null,
                EProcedimientoAlmacenadoFuncionTipo.PROCEDIMIENTO, null);
            assertTrue(false, "El dao debería lanzar una excepción ");
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            assertEquals(e.getCodigoDeExcepcion(), SncBusinessServiceExceptions.EXCEPCION_100005.
                getCodigoDeExcepcion());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Evalua la ejecución de un procedimiento almacenado pasando un número insuficiente de
     * parámetros
     *
     * @author juan.mendez
     */
    @Test
    public void testEjecutaProcedimientoParametrosInsuficientesError() {
        LOGGER.debug("testEjecutaProcedimientoParametrosInsuficientesError");
        try {
            List<Object> parametros = new ArrayList<Object>();
            parametros.add(new BigDecimal(ENumeraciones.NUMERACION_TYA.getId()));
            parametros.add("11000");

            LOGGER.info("... ejecutando " + EProcedimientoAlmacenadoFuncion.GENERAR_NUMERACION);
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(this.
                getConnection());

            Object resultado[] = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERACION, parametros);
            assertTrue(false, "El dao debería lanzar una excepción ");
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            assertEquals(e.getCodigoDeExcepcion(), SncBusinessServiceExceptions.EXCEPCION_100005.
                getCodigoDeExcepcion());
        }
    }

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetFechaDesfijacionEdicto() {
        LOGGER.debug("testGetFechaDesfijacionEdicto");
        try {

            Date fechaFijacionEdicto = new Date(2011 - 1900, 11 - 1, 01);
            int diasFijacionEdicto = 15;

            ArrayList<Object> valoresParametros = new ArrayList<Object>();

            // D: primer parámetro es la fecha desde la cual se calcula
            valoresParametros.add(0, new Timestamp(fechaFijacionEdicto.getTime()));
            // D: segundo parámetro es el número de días hábiles que se le
            // suma a la fecha
            valoresParametros.add(1, new BigDecimal(diasFijacionEdicto));
            // D: tercer parámetro es el código de oficina (por si se debe
            // calcular para alguna en
            // particular). Está definido como '0' por defecto en la
            // función, luego se le pasa ese valor
            valoresParametros.add(2, "0");

            // D: el parámetro schema (el último) se debe enviar como null
            // porque la función no se
            // invoca desde un esquema en particular
            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                getConnection());
            Object[] respuestaEjecucion = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.FNC_SUMAR_DIAS_HABILES,
                valoresParametros, EProcedimientoAlmacenadoFuncionTipo.FUNCION, null);
            assertNotNull(respuestaEjecucion);
            assertTrue(respuestaEjecucion.length == 1);
            Timestamp respuesta = (Timestamp) respuestaEjecucion[0];

            LOGGER.debug("fechaFijacionEdicto:" + fechaFijacionEdicto);
            LOGGER.debug("respuesta:" + respuesta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fredy.wilches
     */
    @Test
    public void testGetNumeroPredio() {
        LOGGER.debug("testGetNumeroPredio");
        try {
            ArrayList<Object> valoresParametros = new ArrayList<Object>();

            // D: primer parámetro es la fecha desde la cual se calcula
            valoresParametros.add(0, "257540102000011650001500000033");

            ProcedimientoAlmacenadoDAO procedimientoDAO = new ProcedimientoAlmacenadoDAO(
                getConnection());
            Object[] respuestaEjecucion = procedimientoDAO.ejecutarSP(
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERO_PREDIO,
                valoresParametros, EProcedimientoAlmacenadoFuncionTipo.PROCEDIMIENTO, null);
            assertNotNull(respuestaEjecucion);
            assertTrue(respuestaEjecucion.length == 2);
            String respuesta = (String) respuestaEjecucion[0];

            LOGGER.debug("respuesta:" + respuesta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

}
