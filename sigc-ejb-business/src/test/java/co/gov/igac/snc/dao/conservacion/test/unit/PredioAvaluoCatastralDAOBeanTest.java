package co.gov.igac.snc.dao.conservacion.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.conservacion.IPredioAvaluoCatastralDAO;
import co.gov.igac.snc.dao.conservacion.impl.PredioAvaluoCatastralDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;

/**
 *
 * @author juan.agudelo
 * @version 2.0
 *
 */
public class PredioAvaluoCatastralDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(PredioAvaluoCatastralDAOBeanTest.class);

    private IPredioAvaluoCatastralDAO dao = new PredioAvaluoCatastralDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testFindValorAvaluoByNumeroPredial() {
        LOGGER.debug("tests: PredioAvaluoCatastralDAOBeanTest#testFindValorAvaluoByNumeroPredial");

        String numeroPredial = "257540102000011650001500000032";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Double answer = this.dao
                .findValorAvaluoByNumeroPredial(numeroPredial);

            if (answer != null) {

                LOGGER.debug("Avalúo: " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerHistoricoPorPredio() {

        LOGGER.debug("unit test PredioAvaluoCatastralDAOBeanTest#testObtenerHistoricoPorPredio ...");

        List<PredioAvaluoCatastral> answer;
        Long idPredio;

        idPredio = 521295L;

        try {
            answer = this.dao.obtenerHistoricoPorPredio(idPredio);
            assertNotNull(answer);
            LOGGER.debug("passed. fetched " + answer.size() + " rows");
        } catch (Exception ex) {
            fail("error: " + ex.getMessage());
        }

    }

}
