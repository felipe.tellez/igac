package co.gov.igac.snc.dao.tramite.test.unit;

/**
 * IGAC proyecto SNC
 */
import java.util.List;

import junit.framework.Assert;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.tramite.ITramiteEstadoDAO;
import co.gov.igac.snc.dao.tramite.impl.TramiteEstadoDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.test.unit.BaseTest;

public class TramiteEstadoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteEstadoDAOBeanTests.class);

    private ITramiteEstadoDAO dao = new TramiteEstadoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte pre: hay datos de prueba en la tabla TRAMITE_ESTADO que corresponden a
     * los usados en esta prueba
     */
    @Test
    public void testObtenerTramiteEstadoVigentePorTramiteId() {
        TramiteEstado answer;
        try {
            answer = dao
                .obtenerTramiteEstadoVigentePorTramiteId(
                    new Long(135));
            LOGGER.debug("passed!. test " + answer.getMotivo());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para buscarHistoricoObservacionesPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarHistoricoObservacionesPorTramiteId() {
        LOGGER.debug(
            "unit test: TramiteEstadoDAOBeanTests#testBuscarHistoricoObservacionesPorTramiteId");

        List<TramiteEstado> observaciones;
        Long tramiteId = 2001l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            observaciones = this.dao
                .buscarHistoricoObservacionesPorTramiteId(tramiteId);

            if (observaciones != null && !observaciones.isEmpty()) {

                for (TramiteEstado te : observaciones) {
                    LOGGER.debug("Motivo " + te.getMotivo());

                }
            }
            Assert.assertNotNull(observaciones);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }

}
