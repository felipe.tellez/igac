package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IDetalleCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.IRegionCapturaOfertaDAO;
import co.gov.igac.snc.dao.avaluos.impl.DetalleCapturaOfertaDAOBean;
import co.gov.igac.snc.dao.avaluos.impl.RegionCapturaOfertaDAOBean;

import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaAreaCapturaOfertaEstado;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase para probar metodos del DetalleCapturaOfertaDAOBeanTest
 *
 * @author ariel.ortiz
 *
 */
public class DetalleCapturaOfertaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DetalleCapturaOfertaDAOBeanTest.class);

    private IDetalleCapturaOfertaDAO dao = new DetalleCapturaOfertaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Prueba de metodo de cargar los codigos de los municipios asociados a un recolector
     *
     * @author ariel.ortiz
     */
    @Test
    public void testCargarDetallesAreasPorRecolector() {
        LOGGER.debug("tests: testCargarDetallesAreasPorRecolector");

        List<String> answer;

        try {
            LOGGER.debug(
                "Entra a la Prueba de cargar los codigos de los municipios asociados a un recolector");

            answer = this.dao.cargarDetallesAreasPorRecolector("pruatlantico20", 614L);
            if (answer != null) {
                for (String codigoManzana : answer) {
                    LOGGER.debug("manzana: " + codigoManzana);
                }
            }
            assertNotNull(answer);
            LOGGER.debug(
                "Sale de la prueba de cargar los codigos de los municipios asociados a un recolector");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prueba de carga de las manzanas dado una region asociada
     */
    @Test
    public void testCargarManzanasPorRegionesAsociadas() {

        List<String> answer = null;
        List<Long> regionPruebaId = null;

        regionPruebaId.add(399L);

        try {
            answer = this.dao.cargarManzanasPorRegionesAsociadas(regionPruebaId);
            assertTrue(true);
            if (answer != null) {
                LOGGER.debug("fetched " + answer.size() + " manzanas");
            } else {
                LOGGER.debug("respuesta es null en manzanas");
            }
        } catch (Exception e) {
            LOGGER.error("Error en la carga de Manzanas: " + e.getMessage());
        }

    }

}
