/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.conservacion.test.unit;

import co.gov.igac.snc.dao.conservacion.ITipificacionUnidadConstDAO;
import co.gov.igac.snc.dao.conservacion.impl.TipificacionUnidadConstDAOBean;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class TipificacionUnidadConstDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TipificacionUnidadConstDAOBeanTest.class);

    private ITipificacionUnidadConstDAO dao = new TipificacionUnidadConstDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: existen datos en la bd que sirven para la prueba
     */
    @Test
    public void testFindByPoints() {

        LOGGER.debug("test: TipificacionUnidadConstDAOBeanTest#testFindByPoints...");
        int points = 5;
        TipificacionUnidadConst answer = null;

        try {
            answer = this.dao.findByPoints(points);
            Assert.assertTrue(answer != null);
            LOGGER.debug("passed");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }

    }

}
