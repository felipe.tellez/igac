/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.radicacion.test.unit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.tramite.radicacion.IDatoRectificarDAO;
import co.gov.igac.snc.dao.tramite.radicacion.impl.DatoRectificarDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EDatoRectificarTipo;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 *
 * @author pedro.garcia
 */
public class DatoRectificacionDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(DatoRectificacionDAOBeanTests.class);

    private IDatoRectificarDAO dao = new DatoRectificarDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: existen datos en la bd que sirven para la prueba
     */
    @Test
    public void testFindByTipo() {

        LOGGER.debug("unit test: DatoRectificacionDAOBeanTests#testFindByTipo ...");

        String tipoDatoRectifCompl;
        List<DatoRectificar> answer;

        tipoDatoRectifCompl = EDatoRectificarTipo.PROPIETARIO.name();
        try {
            answer = this.dao.findByTipo(tipoDatoRectifCompl);
            assertTrue(true);
            LOGGER.debug("... passed.");

            if (!answer.isEmpty()) {
                LOGGER.debug("dato: " + answer.get(0).getNombre());
            }

        } catch (Exception e) {
            LOGGER.error("... error " + e.getMessage(), e);
            assertFalse(true);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de dato rectificación por tipo y naturaleza
     *
     * @author juan.agudelo
     * @version 2.0
     */
    @Test
    public void testFindByTipoAndNaturaleza() {
        LOGGER.debug("tests: DatoRectificacionDAOBeanTests#testFindByTipoAndNaturaleza");

        String tipoDato = EDatoRectificarTipo.PREDIO.name();
        String naturaleza = EDatoRectificarNaturaleza.COMPLEMENTACION.getValor();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<DatoRectificar> answer = this.dao.findByTipoAndNaturaleza(
                tipoDato, naturaleza);

            if (answer != null && !answer.isEmpty()) {

                for (DatoRectificar dTemp : answer) {
                    LOGGER.debug("Dato " + dTemp.getNombre());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
