package co.gov.igac.snc.dao.test.actualizacion.test.unit;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.actualizacion.IConvenioDAO;
import co.gov.igac.snc.dao.actualizacion.IEventoAsistenteDAO;
import co.gov.igac.snc.dao.actualizacion.impl.ConvenioDAOBean;
import co.gov.igac.snc.dao.actualizacion.impl.EventoAsistenteDAOBean;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.test.unit.BaseTest;

public class ConvenioDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvenioDAOBeanTest.class);

    private IConvenioDAO dao = new ConvenioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    @Test
    public void testRecuperarConveniosPorIdActualizacion() {
        Long idActualizacion = 444L;
        String zona = "";
        List<Convenio> convenios = this.dao.recuperarConveniosPorIdActualizacion(idActualizacion);

        for (Convenio c : convenios) {
            LOGGER.debug("Evento Asistente => " + c.getZona());
            if (c.getZona().equals("RURAL")) {
                zona = "00";
            }
            if (c.getZona().equals("URBANA")) {
                zona = "01";
            }
            LOGGER.debug("Evento Asistente => " + zona);
        }
    }

}
