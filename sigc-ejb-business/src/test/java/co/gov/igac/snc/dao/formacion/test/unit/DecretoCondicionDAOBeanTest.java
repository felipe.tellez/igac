package co.gov.igac.snc.dao.formacion.test.unit;

import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.formacion.IDecretoCondicionDAO;
import co.gov.igac.snc.dao.formacion.impl.DecretoCondicionDAOBean;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * @author david.cifuentes
 */
public class DecretoCondicionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecretoCondicionDAOBeanTest.class);

    private IDecretoCondicionDAO dao = new DecretoCondicionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

}
