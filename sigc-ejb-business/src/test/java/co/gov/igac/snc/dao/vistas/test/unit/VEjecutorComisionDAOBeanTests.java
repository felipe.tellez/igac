/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.vistas.test.unit;

import co.gov.igac.snc.dao.vistas.IVEjecutorComisionDAO;
import co.gov.igac.snc.dao.vistas.impl.VEjecutorComisionDAOBean;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class VEjecutorComisionDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.
        getLogger(VEjecutorComisionDAOBeanTests.class);

    private IVEjecutorComisionDAO dao = new VEjecutorComisionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        this.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        this.tearDown();
    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testFindByNumeroComision() {

        LOGGER.debug("unit test: VEjecutorComisionDAOBeanTests#testFindByNumeroComision ...");

        List<VEjecutorComision> answer;
        String numeroComision = "636800000582012";

        try {
            answer = this.dao.findByNumeroComision(numeroComision);

            if (!answer.isEmpty()) {
                LOGGER.debug("... fetched " + answer.size() + " rows");
            } else {
                LOGGER.debug("... fetched nothing!");
            }
            LOGGER.debug("passed!!!");
            Assert.assertTrue(true);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("error en la ejecución");
        }

    }

}
