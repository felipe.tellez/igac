package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.avaluos.IControlCalidadOfertaDAO;
import co.gov.igac.snc.dao.avaluos.impl.ControlCalidadOfertaDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;
import co.gov.igac.snc.test.unit.BaseTest;

/**
 * Clase para probar ControlCalidadOfertaDAOBean
 *
 * @author rodrigo.hernandez
 *
 */
public class ControlCalidadOfertaDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadOfertaDAOBeanTest.class);

    private IControlCalidadOfertaDAO dao = new ControlCalidadOfertaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Prueba la carga de la lista de ofertas de un recolector seleccionadas para control de calidad
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testBuscarOfertasSeleccionadasPorIdRecolectorEIdControlCalidad() {
        LOGGER.debug(
            "tests: AreaCapturaDetalleDAOBeanTest#cargarDetallesAreaOfertaMunicipioDescentralizadoRecolector");

        List<ControlCalidadOferta> answer;

        try {
            LOGGER.debug("Entra a la prueba");

            String recolectorId = "prusoacha07";
            Long ccId = 34L;
            Long regionId = 700L;

            answer = this.dao.buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
                recolectorId, ccId, regionId);

            if (answer != null) {
                for (ControlCalidadOferta cco : answer) {
                    LOGGER.debug("ID OFERTA: " + String.valueOf(cco.getOfertaInmobiliariaId().
                        getId()));
                    LOGGER.debug("FECHA OFERTA: " + cco.getOfertaInmobiliariaId().getFechaOferta());
                    LOGGER.
                        debug("CODIGO OFERTA: " + cco.getOfertaInmobiliariaId().getCodigoOferta());
                    LOGGER.debug("OFERTA SELECCIONADA?: " + cco.getOfertaSeleccionada());
                    LOGGER.debug("APROBADA?: " + cco.getAprobado());
                    LOGGER.debug("OFERTA INMOBILIARIA");
                    cco.getOfertaInmobiliariaId().getId();
                    LOGGER.debug("OFERTAS PREDIOS");

                    for (OfertaPredio op : cco.getOfertaInmobiliariaId().getOfertaPredios()) {
                        LOGGER.debug("NUMERO PREDIAL: " + op.getNumeroPredial());
                    }

                    LOGGER.debug("");
                    LOGGER.debug(
                        "------------------------------------------------------------------------");
                    LOGGER.debug("");
                }
            }

            assertNotNull(answer);
            LOGGER.debug("Sale de la prueba");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
