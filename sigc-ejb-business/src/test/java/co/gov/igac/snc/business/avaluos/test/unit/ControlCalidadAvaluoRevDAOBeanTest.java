package co.gov.igac.snc.business.avaluos.test.unit;

import co.gov.igac.snc.dao.avaluos.IControlCalidadAvaluoRevDAO;
import co.gov.igac.snc.dao.avaluos.impl.AvaluoDAOBean;
import co.gov.igac.snc.dao.avaluos.impl.ControlCalidadAvaluoRevDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.test.unit.BaseTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author felipe.cadena
 */
public class ControlCalidadAvaluoRevDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ControlCalidadAvaluoRevDAOBeanTest.class);
    private IControlCalidadAvaluoRevDAO dao = new ControlCalidadAvaluoRevDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * Método para probar null null     {@link ControlCalidadAvaluoRevDAOBean#
     * calcularNumeroDevoluciones(java.lang.Long, java.lang.Long, java.lang.String) }
     *
     * @author felipe.cadena
     */
    @Test
    public void testCalcularNumeroDevoluciones() {
        LOGGER.debug("iniciando ControlCalidadAvaluoRevDAOBeanTest#testCalcularNumeroDevoluciones");

        Long result = null;

        EAvaluoAsignacionProfActi enu = EAvaluoAsignacionProfActi.valueOf(
            EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.toString());

        LOGGER.debug("Valor enum " + enu.getValor());
        result = dao.calcularNumeroDevoluciones(
            172L, 1L, EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo());

        LOGGER.debug("***********************************************");
        if (result != null) {
            LOGGER.debug("************** " + result + " ******************");
        }
        LOGGER.debug("***********************************************");

        LOGGER.
            debug("finalizando ControlCalidadAvaluoRevDAOBeanTest#testCalcularNumeroDevoluciones");
    }
}
