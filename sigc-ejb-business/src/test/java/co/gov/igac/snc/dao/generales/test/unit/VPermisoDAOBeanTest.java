package co.gov.igac.snc.dao.generales.test.unit;

import static org.testng.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.impl.VPermisoDAOBean;
import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;
import co.gov.igac.snc.test.unit.BaseTest;

public class VPermisoDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VPermisoDAOBeanTest.class);

    private VPermisoDAOBean dao = new VPermisoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    /**
     * @author fredy.wilches
     *
     */
    @Test
    public void getMenuTest() {
        try {
            LOGGER.debug("Entrando al test de menu");
            List<VPermisoGrupo> permisos = dao.getMenu(new String[]{"COORDINADOR"});
            Assert.assertNotNull(permisos);
            LOGGER.debug("Salida método VPermisoDAOBeanTest#getMenu");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            fail("Excepción capturada");
        }
    }

}
