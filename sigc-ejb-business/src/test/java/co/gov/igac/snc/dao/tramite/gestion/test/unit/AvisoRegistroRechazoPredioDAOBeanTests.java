/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.test.unit;

import co.gov.igac.snc.dao.tramite.gestion.IAvisoRegistroRechazoPredioDAO;
import co.gov.igac.snc.dao.tramite.gestion.impl.AvisoRegistroRechazoPredioDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pedro.garcia
 */
public class AvisoRegistroRechazoPredioDAOBeanTests extends BaseTest {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(AvisoRegistroRechazoPredioDAOBeanTests.class);

    private IAvisoRegistroRechazoPredioDAO dao = new AvisoRegistroRechazoPredioDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testFindByPredioId() {

        LOGGER.debug("unit test: AvisoRegistroRechazoPredioDAOBeanTests#testFindByPredioId ...");
        List<AvisoRegistroRechazoPredio> results = null;
        Long predioId = 7l;

        try {
            results = this.dao.findByPredioId(predioId);

            LOGGER.debug("... passed!");

            if (results.size() > 0) {
                LOGGER.info("fetched " + results.size() + " rows.");
                LOGGER.info("predio: " + results.get(0).getNumeroPredial());
            }
            assertTrue(true);

        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            assertFalse(true);
        }
    }

//end of class
}
