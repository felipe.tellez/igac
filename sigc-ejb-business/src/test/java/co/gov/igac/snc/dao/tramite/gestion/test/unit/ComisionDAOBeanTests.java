/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.test.unit;

import co.gov.igac.snc.dao.tramite.ITramiteDAO;
import co.gov.igac.snc.dao.tramite.gestion.IComisionDAO;
import co.gov.igac.snc.dao.tramite.gestion.impl.ComisionDAOBean;
import co.gov.igac.snc.dao.tramite.impl.TramiteDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.testng.Assert;
import static org.testng.Assert.fail;

/**
 *
 * @author pedro.garcia
 */
public class ComisionDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionDAOBeanTests.class);

    private IComisionDAO dao = new ComisionDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * pre: existen datos en la bd que sirven para la prueba
     */
    //N: correrla de forma automática está generando datos basura!!!
    //@Test
    public void testPersist() {

        LOGGER.debug("unit test ComisionDAOBeanTests#testPersist ...");

        Comision nuevaC = new Comision();
        String numComision;
        ITramiteDAO tramiteDAO = new TramiteDAOBean();
        Tramite tramiteAsociado;
        List<ComisionTramite> comisionTramiteRels;
        ComisionTramite tempComisionTramite;
        comisionTramiteRels = new ArrayList<ComisionTramite>();
        Date currentDate;

        currentDate = new Date();
        tramiteDAO.setEntityManager(this.em);

        try {
            tramiteAsociado = tramiteDAO.findById(3l);

            //D: el número de comisión debería ser generado con el llamado al procedimiento almacenado, pero
            //   se complica mucho hacerlo aquí
            numComision = "11000-00001-2011";
            nuevaC.setConductor("otto");
            nuevaC.setDuracion(1.5);
            nuevaC.setDuracionAjuste(new Double(0));
            nuevaC.setDuracionTotal(new Double(1.5));
            nuevaC.setPlacaVehiculo("ooo999");
            //nuevaC.setFechaElaboracion(currentDate);
            nuevaC.setFechaInicio(currentDate);
            nuevaC.setFechaFin(currentDate);
            nuevaC.setEstado(EComisionEstado.CREADA.name());
            nuevaC.setFechaLog(currentDate);
            nuevaC.setUsuarioLog("pagm: creada en pruebas");
            nuevaC.setNumero(numComision);

            tempComisionTramite = new ComisionTramite();
            tempComisionTramite.setComision(nuevaC);
            tempComisionTramite.setTramite(tramiteAsociado);
            tempComisionTramite.setFechaLog(currentDate);
            tempComisionTramite.setUsuarioLog("pagm: insert. pruebas");
            comisionTramiteRels.add(tempComisionTramite);

            nuevaC.setComisionTramites(comisionTramiteRels);

            this.em.getTransaction().begin();
            this.dao.persist(nuevaC);
            this.em.getTransaction().commit();
            //this.em.flush();

            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail("falló en la prueba ComisionDAOBeanTests#testPersist");
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte pre: existen datos en la bd que sirven para la prueba
     */
    @Test
    public void testFindComisionesPorIdTerritorialAndEstadoComision() {
        LOGGER.debug(
            "tests: ComisionDAOBeanTests#testFindComisionesPorIdTerritorialAndEstadoComision");
        List<Comision> comisiones;

        try {
            comisiones = this.dao.findComisionesPorIdTerritorialAndEstadoComision("6360",
                EComisionEstado.CREADA.name());
            Assert.assertNotNull(comisiones);
            Assert.assertTrue(comisiones.size() >= 1);

        } catch (Exception e) {
            LOGGER.error("Fallo la consulta de comisiones por id territorial and estado comision " +
                e.getMessage(), e);
            Assert.assertFalse(true);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte pre: existen datos en la bd que sirven para la prueba
     */
    @Test
    public void testFindFuncionariosEjecutoresByComision() {
        LOGGER.debug("tests: ComisionDAOBeanTests#testFindFuncionariosEjecutoresByComision");
        List<String> funcionariosEjecutoresComision;
        try {
            funcionariosEjecutoresComision = this.dao.findFuncionariosEjecutoresByComision(new Long(
                10), "6360", EComisionEstado.CREADA.name());
            Assert.assertNotNull(funcionariosEjecutoresComision);
            Assert.assertTrue(funcionariosEjecutoresComision.size() >= 1);
        } catch (Exception e) {
            LOGGER.error("Fallo la consulta de funcionarios ejecutores por comision " + e.
                getMessage(), e);
            Assert.assertFalse(true);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testConsultarActivasPorTramite() {

        //the static import declaration imports static members from classes, allowing them to be used without class qualification. 
        LOGGER.debug("unit test: ComisionDAOBeanTests#testConsultarActivasPorTramite");

        Long idTramite;
        String tipoComision;
        ArrayList<Comision> answer;

        idTramite = new Long("36780");
        tipoComision = EComisionTipo.CONSERVACION.getCodigo();

        try {
            answer = (ArrayList<Comision>) this.dao.consultarActivasPorTramite(idTramite,
                tipoComision);
            if (answer != null && !answer.isEmpty()) {
                if (answer.size() > 1) {
                    LOGGER.error("HAY MÁS DE UNA COMISIÓN ACTIVA DEL TIPO BUSCADO");
                } else {
                    LOGGER.debug("comisión = " + answer.get(0).getNumero());
                }
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

}
