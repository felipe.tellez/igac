/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.business.avaluos.test.unit;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import co.gov.igac.snc.dao.avaluos.IComisionOfertaDAO;
import co.gov.igac.snc.dao.avaluos.impl.ComisionOfertaDAOBean;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.util.EOfertaComisionEstado;
import co.gov.igac.snc.test.unit.BaseTest;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class ComisionOfertaDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionOfertaDAOBeanTests.class);

    private IComisionOfertaDAO dao = new ComisionOfertaDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetComisionesOfertaPorEstadoAndACO() {

        LOGGER.debug(
            "unit test: ComisionOfertaDAOBeanTests#testGetComisionesOfertaPorEstadoAndACO ...");

        List<ComisionOferta> answer;
        String estadoComision;
        List<Long> idRegionesCapturaIds = new ArrayList<Long>();

        estadoComision = EOfertaComisionEstado.POR_APROBAR.getCodigo();
        idRegionesCapturaIds.add(435L);

        try {
            answer = this.dao.getComisionesOfertaPorEstadoAndRCO(estadoComision,
                idRegionesCapturaIds);
            assertTrue(true);
            if (answer != null) {
                LOGGER.debug("fetched " + answer.size() + " rows");
                if (!answer.isEmpty()) {
                    LOGGER.debug("the first has id " + answer.get(0).getId());
                }
            }
        } catch (Exception e) {
            fail("error!!");
            LOGGER.error("error: " + e.getMessage());
        }

    }

}
