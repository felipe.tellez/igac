/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.dao.tramite.gestion.test.unit;

import co.gov.igac.snc.dao.tramite.radicacion.IAvisoRegistroRechazoDAO;
import co.gov.igac.snc.dao.tramite.radicacion.impl.AvisoRegistroRechazoDAOBean;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import co.gov.igac.snc.test.unit.BaseTest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pedro.garcia
 */
public class AvisoRegistroRechazoDAOBeanTests extends BaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        AvisoRegistroRechazoDAOBeanTests.class);

    private IAvisoRegistroRechazoDAO dao = new AvisoRegistroRechazoDAOBean();

    @BeforeClass
    public void runBeforeClass() {
        LOGGER.info("runBeforeClass");
        setUp(BaseTest.EM_TEST_ORACLEDB);
        this.dao.setEntityManager(this.em);
    }

    @AfterClass
    public void runAfterClass() {
        LOGGER.info("runAfterClass");
        tearDown();
    }
//--------------------------------------------------------------------------------------------------

    @Test
    public void testFindByNumeroSolicitud() {

        List<AvisoRegistroRechazo> results;
        String numeroSolicitud;

        numeroSolicitud = "769457567";
        try {
            results = this.dao.findByNumeroSolicitud(numeroSolicitud);

            if (results.size() > 0) {
                LOGGER.info("fetched " + results.size() + " rows.");
                List<AvisoRegistroRechazoPredio> arrPredios =
                    results.get(0).getAvisoRegistroRechazoPredios();

                LOGGER.info("... first of which has " + arrPredios.size() + " predios.");
                LOGGER.info("... first of which has numero predial " + arrPredios.get(0).
                    getNumeroPredial());

                Assert.assertTrue(arrPredios != null);
            } else {
                Assert.assertTrue(true);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.assertFalse(true);
        }

    }

}
