package co.gov.igac.persistence.entity.generales.test.unit;

import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import static org.testng.Assert.assertNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.IHorarioAtencionTerritorialDAO;
import co.gov.igac.snc.dao.generales.impl.HorarioAtencionTerritorialDAOBean;
import co.gov.igac.snc.test.unit.BaseTest;

public class HorarioAtencionTerritorialDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(HorarioAtencionTerritorialDAOBeanTest.class);

    private final IHorarioAtencionTerritorialDAO dao = new HorarioAtencionTerritorialDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author lorena.salamanca
     */
    @Test
    public void testBuscarHorarioAtencionByEstructuraOrganizacional() {
        LOGGER.debug(
            "tests: HorarioAtencionTerritorialDAOBean#testBuscarHorarioAtencionByEstructuraOrganizacional");

        String estructuraOC = "6040";
        try {
            LOGGER.debug("**************** Entra al Test *************************");
            HorarioAtencionTerritorial answer = this.dao
                .buscarHorarioAtencionByEstructuraOrganizacional(estructuraOC);

            if (answer != null) {

                LOGGER.debug("HorarioAtencionTerritorial " + answer.toString());

            }
            assertNotNull(answer);
            LOGGER.debug("********************Finaliza el Test**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
