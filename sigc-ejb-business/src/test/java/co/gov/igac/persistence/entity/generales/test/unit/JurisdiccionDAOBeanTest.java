package co.gov.igac.persistence.entity.generales.test.unit;

import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.dao.generales.IJurisdiccionDAO;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.generales.impl.JurisdiccionDAOBean;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.test.unit.BaseTest;
import org.testng.Assert;

public class JurisdiccionDAOBeanTest extends BaseTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(JurisdiccionDAOBeanTest.class);

    private IJurisdiccionDAO dao = new JurisdiccionDAOBean();

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        LOGGER.info("runBeforeClass");
        super.setUp(BaseTest.EM_TEST_ORACLEDB);
        dao.setEntityManager(this.em);
    }

    @AfterClass
    public void tearDownAfterClass() throws Exception {
        LOGGER.info("runAfterClass");
        super.tearDown();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindMunicipiosbyCodigoEstructuraOrganizacional() {
        LOGGER.debug(
            "tests: JurisdiccionDAOBeanTest#testFindMunicipiosbyCodigoEstructuraOrganizacional");

        String estructuraOC = "6200";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Municipio> answer = this.dao
                .findMunicipiosbyCodigoEstructuraOrganizacional(estructuraOC);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Municipios " + answer.size());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional() {
        LOGGER.debug(
            "tests: JurisdiccionDAOBeanTest#testFindMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional");

        String departamentoCodigo = "25";
        List<String> uocCodigos = new ArrayList<String>();

        for (int i = 6360; i <= 6370; i++) {
            uocCodigos.add("" + i);
        }

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Municipio> answer = this.dao
                .findMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional(
                    departamentoCodigo, uocCodigos);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Tamaño " + answer.size());

                for (Municipio mTemp : answer) {
                    LOGGER.debug("Municipio codigo " + mTemp.getCodigo());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetCodigoDepartamentoByCodigoEstructuraOrganizacional() {
        LOGGER.debug(
            "tests: JurisdiccionDAOBeanTest#testGetCodigoDepartamentoByCodigoEstructuraOrganizacional");

        List<String> codigosEO = new ArrayList<String>();
        String codigoEO = "6360";

        codigosEO.add(codigoEO);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<String> answer = this.dao
                .getCodigoDepartamentoByCodigoEstructuraOrganizacional(codigosEO);

            if (answer != null && !answer.isEmpty()) {

                for (String sTmp : answer) {
                    LOGGER.debug("Código municipio " + sTmp);
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Prueba busqueda de código de estructura organizacional por municipio
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetEstructuraOrganizacionalIdByMunicipioCod() {
        LOGGER.debug(
            "tests: JurisdiccionDAOBeanTest#testGetEstructuraOrganizacionalIdByMunicipioCod");

        String codigoMun = "08078";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String answer = this.dao
                .getEstructuraOrganizacionalIdByMunicipioCod(codigoMun);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Código Estructura Organizacional " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Prueba busqueda de codigos de municipios por codigo estructura organizacional
     *
     * @author franz.gamba	*
     */
    @Test
    public void testGetMunicipioCodsByEstructuraOrganizacionalCod() {
        LOGGER.debug(
            "tests: JurisdiccionDAOBeanTest#testGetMunicipioCodsByEstructuraOrganizacionalCod");

        String eoCod = "6368";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<String> answer = this.dao
                .getMunicipioCodsByEstructuraOrganizacionalCod(eoCod);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Municipios bajo jurisdiccion de " + eoCod + " : " + answer.size());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerCodigoEstructuraOrganizacionalPorMunicipio() {

        LOGGER.debug(
            "unit test JurisdiccionDAOBeanTest#testObtenerCodigoEstructuraOrganizacionalPorMunicipio ");

        String codMunicipio, answer;

        codMunicipio = "08001";

        try {
            answer = this.dao.obtenerCodigoEstructuraOrganizacionalPorMunicipio(codMunicipio);
            assertNotNull(answer);
            Assert.assertNotEquals(answer, "");
            LOGGER.debug("... passed!. Estructura org cod = " + answer);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("... failed!. " + ex.getMensaje());
            Assert.fail();
        }

    }

}
