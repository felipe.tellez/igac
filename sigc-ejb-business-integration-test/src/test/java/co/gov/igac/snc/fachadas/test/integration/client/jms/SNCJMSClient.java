package co.gov.igac.snc.fachadas.test.integration.client.jms;

import static org.testng.Assert.fail;

import java.util.Random;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.hornetq.jms.client.HornetQQueue;
import org.hornetq.jms.client.HornetQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import co.gov.igac.snc.jms.vo.EventDetails;
import co.gov.igac.snc.jms.vo.EventType;

/**
 *
 * @author juan.mendez
 *
 */
public class SNCJMSClient {

    public static Logger LOGGER = LoggerFactory.getLogger(JMSClientUtil.class);

    private static final String DEFAULT_USER = "sncJmsUser";
    private static final String DEFAULT_PWD = "igac11.";
    private static final int DEFAULT_MESSAGE_COUNT = 1;
    private static final String predioCodigo = "0002547";
    private static final String login = "pruatlantico";

    private static int messageCounter = 0;

    private Random random;

    @BeforeClass
    public void beforeClass() {
        LOGGER.debug("beforeClass...");
        random = new Random();
    }

    @BeforeTest
    public void beforeTest() {
        LOGGER.debug("beforeTest...");
    }

    @AfterTest
    public void afterTest() {
        LOGGER.debug("afterTest...");
    }

    /**
     *
     */
    // @Test(threadPoolSize = 10, invocationCount = 100, timeOut = 60000)
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 30000)
    public void testSendMessageToQueue() {
        LOGGER.debug("testSendMessageToQueue");

        ConnectionFactory connectionFactory = null;
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        MessageConsumer consumer = null;
        Destination destination = null;
        TextMessage message = null;

        try {
            connectionFactory = JMSClientUtil.getConnectionFactory();
            destination = JMSClientUtil.getDestinationQueue();
            // connection = connectionFactory.createConnection();
            connection = connectionFactory.createConnection(DEFAULT_USER, DEFAULT_PWD);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(destination);
            consumer = session.createConsumer(destination);
            connection.start();

            int count = DEFAULT_MESSAGE_COUNT;

            LOGGER.info("Sending " + count + " messages ");

            for (int i = 0; i < count; i++) {
                message = session.createTextMessage();
                EventType actionType = EventType.SIG_COPIA_TRABAJO_CREAR;
                message.setIntProperty(EventDetails.EVENT_TYPE, actionType.ordinal());
                message.setLongProperty(EventDetails.EVENT_TIMESTAMP, System.currentTimeMillis());
                message.setIntProperty(EventDetails.EVENT_TRAMITE_NUMERO, random.nextInt());
                message.setStringProperty(EventDetails.EVENT_PREDIOS_CODIGOS, predioCodigo);
                message.setStringProperty(EventDetails.EVENT_USER_LOGIN, login);
                LOGGER.info("content: " + message);
                producer.send(message);
            }

            for (int i = 0; i < count; i++) {
                message = (TextMessage) consumer.receive();
                int eventId = message.getIntProperty(EventDetails.EVENT_TYPE);
                EventDetails details = new EventDetails(message);
                LOGGER.info("eventId: " + eventId);
                LOGGER.info("Received message with content " + details);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            JMSClientUtil.closeResources(producer, /* consumer, */ session,
                connection);
        }

    }

    /**
     * Envía un evento de creación de Réplica por JMS
     */
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 30000)
    public void testSendMessageToTopic() {
        LOGGER.debug("testSendMessageToTopic");
        ConnectionFactory connectionFactory = null;
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        MessageConsumer consumer = null;
        Destination destination = null;
        TextMessage message = null;

        try {
            connectionFactory = JMSClientUtil.getConnectionFactory();
            destination = JMSClientUtil.getDestinationTopic();
            // connection = connectionFactory.createConnection();
            connection = connectionFactory.createConnection(DEFAULT_USER, DEFAULT_PWD);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(destination);
            consumer = session.createConsumer(destination);
            connection.start();

            int count = DEFAULT_MESSAGE_COUNT;

            LOGGER.info("Sending " + count + " messages ");

            for (int i = 0; i < count; i++) {
                message = session.createTextMessage();
                EventType actionType = EventType.SIG_COPIA_TRABAJO_CARGAR_DATOS_EDITADOS;
                message.setIntProperty(EventDetails.EVENT_TYPE, actionType.ordinal());
                message.setLongProperty(EventDetails.EVENT_TIMESTAMP, System.currentTimeMillis());
                message.setIntProperty(EventDetails.EVENT_TRAMITE_NUMERO, random.nextInt());
                message.setStringProperty(EventDetails.EVENT_PREDIOS_CODIGOS, predioCodigo);
                message.setStringProperty(EventDetails.EVENT_USER_LOGIN, login);
                LOGGER.info("content: " + message);
                producer.send(message);
            }

            for (int i = 0; i < count; i++) {
                message = (TextMessage) consumer.receive();
                int eventId = message.getIntProperty(EventDetails.EVENT_TYPE);
                EventDetails details = new EventDetails(message);
                LOGGER.info("eventId: " + eventId);
                LOGGER.info("Received message with content " + details);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            JMSClientUtil.closeResources(producer, consumer, session, connection);
        }

    }

    @Test(threadPoolSize = 1, invocationCount = 10, timeOut = 30000)
    public void testSendTextMessages() throws Exception {
        LOGGER.debug("sendTextMessages...");
        Connection connection = null;
        Session session = null;
        ConnectionFactory connectionFactory = null;
        MessageProducer producer = null;

        try {
            connectionFactory = JMSClientUtil.getConnectionFactory();

            // connection = connectionFactory.createConnection();
            connection = connectionFactory.createConnection(DEFAULT_USER, DEFAULT_PWD);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = new HornetQTopic("TestTopic");
            producer = session.createProducer(destination);
            connection.start();

            TextMessage message = session.createTextMessage();
            LOGGER.debug("sending...");
            message.setText("hello....");
            producer.send(message);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            try {
                producer.close();
                connection.close();
                session.close();
            } catch (JMSException e) {
                LOGGER.error(e.getMessage(), e);
            }

        }
    }

    @Test(threadPoolSize = 1, invocationCount = 10, timeOut = 30000)
    public void testSendTextMessagesToQueue() throws Exception {
        LOGGER.debug("testSendTextMessagesToQueue...");
        Connection connection = null;
        Session session = null;
        ConnectionFactory connectionFactory = null;
        MessageProducer producer = null;

        try {
            int counter = messageCounter++;

            connectionFactory = JMSClientUtil.getConnectionFactory();

            // connection = connectionFactory.createConnection();
            connection = connectionFactory.createConnection(DEFAULT_USER, DEFAULT_PWD);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = new HornetQQueue("TestQueue");
            producer = session.createProducer(destination);
            connection.start();

            TextMessage message = session.createTextMessage();
            LOGGER.debug("sending...");
            message.setText("hello: " + counter);
            producer.send(message);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } finally {
            try {
                producer.close();
                connection.close();
                session.close();
            } catch (JMSException e) {
                LOGGER.error(e.getMessage(), e);
            }

        }
    }

}
