package co.gov.igac.snc.fachadas.test.integration.procesos.osmi.asignacion;

import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.fachadas.test.integration.procesos.osmi.MoverProcesoOsmiAbstractTest;

public class MoverProcesoActEstablecerAreaParaOfertas extends
    MoverProcesoOsmiAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActEstablecerAreaParaOfertas.class);

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();

        this.testerId = "rodrigo.hernandez";
        this.cargarDatosPorTransicion("");

    }

    // OJO: Cada vez que se vaya a ejecutar el test, hay que cambiar el areaId
    // en el metodo setUp
    @Test
    public void testEstablecerAreaCapturaOferta() {

        // Se crea el proceso
        try {
            this.crearProcesoYAvanzarActividades(this.ofertaInmobiliaria);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Override
    protected void moverActividades(String idInstanciaProceso,
        OfertaInmobiliaria ofertaInmobiliaria) throws Exception {

        LOGGER.debug("Ya se está en Asignar Recolector");

    }

    @Override
    protected String crearProceso(OfertaInmobiliaria ofertaInmobiliaria)
        throws Exception {

        String id = procesoService.crearProceso(ofertaInmobiliaria);

        LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
        LOGGER.info("ID de proceso: " + id);

        return id;
    }

}
