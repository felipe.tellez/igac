package co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.asignacion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoAbstractTest;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.util.Constantes;

/**
 *
 * Prueba de integración que mueve el proceso hasta "Revisar Trámite Asignado"
 *
 * @author fredy.wilches
 *
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoActRevisarTramiteAsignadoAvaluosTest extends
    MoverProcesoAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActRevisarTramiteAsignadoAvaluosTest.class);

    /**
     *
     */
    //private static final String TIPO_TRAMITE = "1"; // Mutación
    //private static final String CLASE_MUTACION = EMutacionClase.PRIMERA.getCodigo();
    //private static final String CLASIFICACION_TRAMITE = ETramiteClasificacion.OFICINA.toString();	
    //private static final long ID_SOLICITUD = 14724L;
    /// RESPONSABLE CONSERVACION
    private static final String AUTOAVALUO = "12";
    private static final String REVISION_AVALUO = "6";

    /**
     * Método para iniciarlizar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");

            String territorial = MoverProcesoAbstractTest.ATLANTICO;

            RADICADOR = usuarios.get(territorial).get(ERol.FUNCIONARIO_RADICADOR);
            assertNotNull(RADICADOR);
            assertNotNull(RADICADOR.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_CONSERVACION = usuarios.get(territorial).get(ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION.getDescripcionEstructuraOrganizacional());

            EJECUTOR = usuarios.get(territorial).get(ERol.EJECUTOR_TRAMITE);
            assertNotNull(EJECUTOR);
            assertNotNull(EJECUTOR.getDescripcionEstructuraOrganizacional());

            DIRECTOR_TERRITORIAL = usuarios.get(territorial).get(ERol.DIRECTOR_TERRITORIAL);
            assertNotNull(DIRECTOR_TERRITORIAL);
            assertNotNull(DIRECTOR_TERRITORIAL.getDescripcionEstructuraOrganizacional());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * Crea una solicitud con un trámite
     */
    @Test(threadPoolSize = 5, invocationCount = 40, timeOut = 350000)
    // @Test
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearSolicitud");

        try {
            Integer codigoEstructuraOrganizacional = 6040;
            String codigoMunicipio = "08001";
            Predio predio = this.pruebasService.buscarPredioLibreTramite(
                codigoEstructuraOrganizacional, codigoMunicipio);
            assertNotNull(predio);
            String numeroPredial = predio.getNumeroPredial();
            LOGGER.debug("id:" + predio.getId() + " - Número Predial:" + numeroPredial);
            String codigoDepartamento = numeroPredial.substring(0, 2);

            assertNotNull(codigoDepartamento);
            assertNotNull(codigoMunicipio);

            // /////////////////////////////////////////////////////////////////////////////////////////////
            //TODO CARGAR SOLICITUD ALEATORIA
            Solicitud solicitud = new Solicitud();
            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setFecha(new Date());
            solicitud.setUsuarioLog(this.getClass().toString());
            solicitud.setAnexos(0);
            solicitud.setFolios(1);
            solicitud.setFinalizado("NO");
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.toString());

            solicitud.setTipo(Math.random() > 0.5 ? TS_AUTOAVALUO : TS_REVISION_AVALUO);

            solicitud.setSistemaEnvio("1");
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());

            Tramite tramite = new Tramite();
            tramite.setPredio(predio);
            Departamento d = new Departamento();

            d.setCodigo(codigoDepartamento);
            Municipio m = new Municipio();
            m.setCodigo(codigoMunicipio);
            tramite.setDepartamento(d);
            tramite.setMunicipio(m);
            tramite.setNumeroPredial(predio.getNumeroPredial());

            tramite.setTipoTramite(solicitud.getTipo().equals(TS_AUTOAVALUO) ? AUTOAVALUO :
                REVISION_AVALUO);
            tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
            tramite.setFuncionarioEjecutor(EJECUTOR.getLogin());
            tramite.setFuncionarioRadicador(RADICADOR.getLogin());

            List<TramiteDocumentacion> tramitesDocumentacion = new ArrayList<TramiteDocumentacion>();

            TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            //tipoD.setId(Constantes.TIPO_DOCUMENTO_OTRO_ID);
            tipoD.setId(ETipoDocumentoId.OTRO.getId());

            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            doc.setDescripcion("rutaDePruebaDeIntegracion");
            doc.setTipoDocumento(tipoD);
            doc.setUsuarioCreador(RADICADOR.getLogin());
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());

            tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
            tramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            tramiteDocumentacion.setDepartamento(d);
            tramiteDocumentacion.setMunicipio(m);
            tramiteDocumentacion.setCantidadFolios(1);
            tramiteDocumentacion.setTipoDocumento(tipoD);
            tramite.setNumeroPredial(numeroPredial);

            // para que la documentación este completa
            tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());
            // Si se queire avanzar el trámite estado a digitalizar
            tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

            tramiteDocumentacion.setFechaAporte(new Date());
            tramiteDocumentacion.setTramite(tramite);
            tramiteDocumentacion.setUsuarioLog("MoverProcesoActRevisarTramiteAsignadoAvaluosTest");
            tramiteDocumentacion.setFechaLog(new Date());

            tramitesDocumentacion.add(tramiteDocumentacion);
            tramiteDocumentacion.setDocumentoSoporte(doc);
            tramite.setTramiteDocumentacions(tramitesDocumentacion);

            solicitud.getTramites().add(tramite);

            List<SolicitanteSolicitud> solicitantes = tramiteService
                .getSolicitanteSolicitudAleatorio();
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);// forzar crear nuevo objeto
                ss.setSolicitud(solicitud);
            }

            solicitud.setSolicitanteSolicituds(solicitantes);

            solicitud = tramiteService.guardarActualizarSolicitud(RADICADOR,
                solicitud, null);
            assertNotNull(solicitud);

            List<Tramite> tramitesCreados = solicitud.getTramites();
            assertTrue(tramitesCreados.size() > 0);
            Tramite tramiteSerializado = tramitesCreados.get(0);
            assertNotNull(tramiteSerializado);
            assertNotNull(tramiteSerializado.getId());
            LOGGER.debug("Id Solicitud:" + solicitud.getId());
            LOGGER.debug("Id tramite:" + tramiteSerializado.getId());
            // /////////////////////////////////////////////////////////////////////////////////////////////
            SolicitudCatastral solicitudCatastral = testCrearYMoverProceso(
                solicitud, tramiteSerializado, tramite.getTipoTramite().equals(AUTOAVALUO) ?
                "Autoavalúo" : "Revisión de Avalúo");
            assertNotNull(solicitudCatastral);
            assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    @Implement
    protected void moverActividades(String id, SolicitudCatastral solicitudCatastral,
        Tramite tramite)
        throws Exception {
        // /////////////////////////////////////////////////////////////////////////////////////
        moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);
        // /////////////////////////////////////////////////////////////////////////////////////
    }

}
