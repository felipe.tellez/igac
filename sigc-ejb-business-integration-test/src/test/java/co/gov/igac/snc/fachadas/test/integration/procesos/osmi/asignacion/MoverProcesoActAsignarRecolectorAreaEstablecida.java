package co.gov.igac.snc.fachadas.test.integration.procesos.osmi.asignacion;

import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.fachadas.test.integration.procesos.osmi.MoverProcesoOsmiAbstractTest;
import co.gov.igac.snc.persistence.util.ERol;

public class MoverProcesoActAsignarRecolectorAreaEstablecida extends
    MoverProcesoOsmiAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActAsignarRecolectorAreaEstablecida.class);

    /**
     * Se inician los objetos que se necesiten para las pruebas
     *
     * @author rodrigo.hernandez
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        this.testerId = "rodrigo.hernandez";
        this.cargarDatosPorTransicion(
            ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);

    }

    /**
     * Método para probar asignación de región de una area centralizada
     *
     * @author rodrigo.hernandez
     */
    @Test
    public void testCrearYAvanzarAsignacionEnAreaCentralizada() {
        try {
            this.crearProcesoYAvanzarActividades(this.ofertaInmobiliaria);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    /**
     * Mover proceso a siguiente actividad (Proyectar Comision)
     */
    @Override
    protected void moverActividades(String idInstanciaProceso,
        OfertaInmobiliaria ofertaInmobiliaria) throws Exception {

        LOGGER.debug("Ya se está en Proyectar Comisión");

    }

    /**
     * Crea el proceso OSMI
     */
    @Override
    protected String crearProceso(OfertaInmobiliaria ofertaInmobiliaria)
        throws Exception {

        ofertaInmobiliaria.setIdentificador(this.regionCapturaOferta
            .getId());
        String id = procesoService.crearProceso(ofertaInmobiliaria);
        Thread.sleep(TIMER);

        List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();

        LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
        LOGGER.info("ID de proceso: " + id);

        listaUsuario.add(usuariosAtlantico
            .get(ERol.COORDINADOR_GIT_AVALUOS));
        listaUsuario.add(usuariosAtlantico
            .get(ERol.RECOLECTOR_OFERTA_INMOBILIARIA));

        ofertaInmobiliaria.setIdCorrelacion(ofertaInmobiliaria
            .getIdentificador());
        ofertaInmobiliaria.setIdentificador(this.regionCapturaOferta
            .getId());

        ofertaInmobiliaria
            .setTransicion(ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION);

        ofertaInmobiliaria
            .setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION);
        ofertaInmobiliaria.setObservaciones("Avance actividad");

        ofertaInmobiliaria.setUsuarios(listaUsuario);

        //Se crea el 2do proceso y se deja en la transicion PROYECTAR_COMISION
        String idRegion = procesoService.crearProceso(ofertaInmobiliaria);
        LOGGER.info("2do Proceso creado: " + ((idRegion != null) ? "SI" : "NO"));
        LOGGER.info("ID de proceso: " + idRegion);

        return id;
    }

}
