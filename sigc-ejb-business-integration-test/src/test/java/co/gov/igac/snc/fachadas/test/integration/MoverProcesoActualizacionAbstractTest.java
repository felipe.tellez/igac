package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EEstadoProceso;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.utilerias.Duration;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;

/**
 *
 * Prueba de integración que mueve el proceso hasta "Revisar Trámite Asignado"
 *
 * @author juan.mendez
 *
 */
public abstract class MoverProcesoActualizacionAbstractTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActualizacionAbstractTest.class);

    /**
     * Tiempo de espera para la consulta asincrónica del estado de la tarea
     */
    private static final long ASYNC_TIME_SLEEP = 1000;

    protected UsuarioDTO DIRECTOR_TERRITORIAL;
    protected UsuarioDTO RESPONSABLE_ACTUALIZACION;
    protected UsuarioDTO ABOGADO;
    protected UsuarioDTO SUBDIRECTOR_CATASTRO;

    /**
     * Crea un proceso
     *
     * @param actualizacion
     * @param tramite
     * @return
     * @throws Exception
     */
    protected ActualizacionCatastral testCrearYMoverProceso(Actualizacion actualizacion
    ) throws Exception {
        ActualizacionCatastral actualizacionCatastral = null;
        try {
            assertNotNull(RESPONSABLE_ACTUALIZACION);
            assertNotNull(ABOGADO);
            assertNotNull(DIRECTOR_TERRITORIAL);

            List<UsuarioDTO> respActualizacion = new ArrayList<UsuarioDTO>();
            respActualizacion.add(RESPONSABLE_ACTUALIZACION);

            actualizacionCatastral = new ActualizacionCatastral();

            actualizacionCatastral.setIdentificador(actualizacion.getId());
            actualizacionCatastral.setIdMunicipio(actualizacion.getMunicipio().getCodigo());
            actualizacionCatastral.setIdDepartamento(actualizacion.getDepartamento().getCodigo());

            actualizacionCatastral.setObservaciones(
                "Actividad actualizacion instanciada para pruebas");
            // para iniciar el proceso
            actualizacionCatastral.setTransicion(
                ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_INFO_DIAGNOSTICO);
            actualizacionCatastral.setUsuarios(respActualizacion);
            actualizacionCatastral.setTerritorial(respActualizacion.get(0).
                getDescripcionTerritorial());

            String id = procesoService.crearProceso(actualizacionCatastral);
            String estadoProceso = procesoService.obtenerEstadoProceso(id);
            LOGGER.debug("estadoProceso:" + estadoProceso);
            boolean estaProcesando = !(estadoProceso.equals(EEstadoProceso.EJECUCION.toString()));
            while (estaProcesando) {
                LOGGER.debug("Proceso: Obteniendo estado del proceso...");
                Thread.sleep(ASYNC_TIME_SLEEP);
                estadoProceso = procesoService.obtenerEstadoProceso(id);
                LOGGER.debug("estadoProceso:" + estadoProceso);
                estaProcesando = !(estadoProceso.equals(EEstadoProceso.EJECUCION.toString()));
            }
            assertNotNull(id);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            moverActividades(id, actualizacionCatastral);

        } catch (Exception e) {
            throw e;
        }
        return actualizacionCatastral;
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    /* protected abstract void moverActividades(String id, SolicitudCatastral solicitudCatastral)
     * throws Exception ; */
    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario. En este se
     * envía como parámetro el trámite porque para avanzarlo hasta alguna actividad a veces se
     * necesita insertar datos en otras tablas en las que se referencian datos de éste
     *
     * @param idInstanciaProceso
     * @param solicitudCatastral
     * @param tramite
     * @throws Exception
     */
    protected abstract void moverActividades(String idInstanciaProceso,
        ActualizacionCatastral actualizacionCatastral) throws Exception;

    /**
     *
     * @param idProceso
     * @param usuario
     * @param solicitudCatastral
     * @param valorTransicionEsperado
     * @return
     * @throws Exception
     */
    protected final Actividad moverActividad(
        String idProceso, UsuarioDTO usuario,
        ActualizacionCatastral actualizacionCatastral,
        String valorTransicionEsperado) throws Exception {
        return moverActividad(idProceso, usuario, actualizacionCatastral, null,
            valorTransicionEsperado);
    }

    /**
     *
     * @param idProceso
     * @param usuario
     * @param solicitudCatastral
     * @param duracion
     * @param valorTransicionEsperado
     * @return
     * @throws Exception
     */
    //moverActividades(idActividad, transicion, usuarioDestino)
    protected final Actividad moverActividad(
        String idProceso, UsuarioDTO usuario,
        ActualizacionCatastral actualizacionCatastral, Duration duracion,
        String valorTransicionEsperado) throws Exception {
        LOGGER.debug("************************************************");
        LOGGER.debug("moverActividad   Destino:" + valorTransicionEsperado);
        Actividad resultado = null;
        String transicion = null;
        //Obtener actividades del proceso
        List<Actividad> actividades = null;
        int intentos;
        //Se quitó el do-while porque este siempre retrazaba la respuesta 
        //en la primera petición al servidor de procesos.
        actividades = procesoService.obtenerActividadesProceso(idProceso);
        while (actividades == null || actividades.isEmpty()) {
            LOGGER.debug("Proceso: Esperando respuesta del process server...");
            Thread.sleep(ASYNC_TIME_SLEEP);
            actividades = procesoService.obtenerActividadesProceso(idProceso);
        }
        resultado = actividades.get(0);
        assertNotNull(resultado);

        // Verificar si la actividad acepta el estado especificado
        LinkedHashMap<String, UsuarioDTO> transiciones = new LinkedHashMap<String, UsuarioDTO>();
        transiciones.put(valorTransicionEsperado, usuario);
        Iterator<String> iter = transiciones.keySet().iterator();

        while (iter.hasNext()) {
            intentos = 0;
            LOGGER.debug("Actividad Resultado: " + resultado.getId() + " - " + resultado.
                getRutaActividad().toString());
            // Obtener siguiente transición
            transicion = iter.next();
            // Usuario para las siguientes tareas
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(transiciones.get(transicion));
            // Verificar si la actividad acepta el estado especificado
            for (Actividad actividad : actividades) {
                LOGGER.debug("Transición: " + transicion);
                LOGGER.debug("Transiciones: " + ((actividad.getTransiciones() != null) ? actividad.
                    getTransiciones().toString() : "null"));
                if ((actividad.getTransiciones() == null || actividad.getTransiciones().isEmpty())) {
                    actualizacionCatastral.setTransicion(null);
                } else {
                    Set<String> transActividad = new HashSet<String>(
                        actividad.getTransiciones());
                    if (transActividad != null &&
                        transActividad.contains(transicion)) {
                        actualizacionCatastral.setTransicion(transicion);
                    }
                }
                actualizacionCatastral.setUsuarios(usuarios);
                if (duracion != null) {
                    LOGGER.debug("Moverse a: " + actualizacionCatastral.getTransicion());
                    actualizacionCatastral = procesoService.avanzarActividad(actividad.getId(),
                        actualizacionCatastral);
                } else {
                    //En el process el avance es asincrónico
                    //El método solo debería continuar cuando llegue una respuesta 
                    // del servidor avisando que ya terminó el avance 
                    LOGGER.debug("Moverse a: " + actualizacionCatastral.getTransicion());
                    actualizacionCatastral = procesoService.avanzarActividad(actividad.getId(),
                        actualizacionCatastral);
                }
                actividades = procesoService.obtenerActividadesProceso(idProceso);
                //estaEjecutando = estaEjecutando(resultado);
                while (actividades.isEmpty() && intentos <= 10) {
                    LOGGER.debug("Actividades: Esperando respuesta del process server...");
                    Thread.sleep(ASYNC_TIME_SLEEP);
                    actividades = procesoService.obtenerActividadesProceso(idProceso);
                    ++intentos;
                }
                if (intentos > 10) {
                    intentos = 0;
                    Long idTramite = actualizacionCatastral.getIdentificador();
                    actividades = procesoService.getActividadesPorIdObjetoNegocio(idTramite);
                    //estaEjecutando = estaEjecutando(resultado);
                    while (actividades.isEmpty() && intentos < 10) {
                        LOGGER.debug("Actividades: Esperando respuesta del process server...");
                        Thread.sleep(ASYNC_TIME_SLEEP);
                        actividades = procesoService.getActividadesPorIdObjetoNegocio(idTramite);
                        ++intentos;
                    }
                }

                assertNotNull(actividades);
                assertTrue(actividades.size() > 0);
                resultado = actividades.get(0);
            }
        }
        return resultado;
    }

    /**
     * Valida si una tarea está en ejecución
     *
     * @author juan.mendez
     * @param actividad
     * @return
     */
    @SuppressWarnings("unused")
    private boolean estaEjecutando(Actividad actividad) {

        String estadoActividad = procesoService.obtenerEstadoActividad(actividad.getId());
        //LOGGER.debug("estadoActividad:"+estadoActividad);
        boolean estaEjecutando = estadoActividad.equals(EEstadoActividad.RECLAMADA.toString()) ||
            estadoActividad.equals(EEstadoActividad.POR_RECLAMAR.toString());
        return estaEjecutando;

    }

    /**
     * Obtiene de forma aleatoria un predio para la realización de pruebas. (Por el momento solo
     * tiene en cuenta predios de Soacha y Atlántico dado que son los únicos municipios que por
     * ahora tienen información en el entorno de Desarrollo)
     *
     * @return
     */
    /* public Predio obtenerPredioAleatorioParaPruebas(){ boolean esPredioValido = false; Predio
     * predio = null; while(!esPredioValido){ predio = this.pruebasService.cargarPredioAleatorio();
     * String numeroPredial = predio.getNumeroPredial(); LOGGER.debug("id:"+predio.getId()+" -
     * Número Predial:"+numeroPredial); if(numeroPredial.startsWith("25754") ||
     * numeroPredial.startsWith("08001")){ esPredioValido = true; } } return predio; } */
}
