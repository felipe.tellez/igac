package co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.ejecucion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoAbstractTest;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.util.Constantes;

/**
 *
 * Prueba de integración que mueve el proceso hasta "Descargar del SNC"
 *
 * @author fredy.wilches
 *
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoEjeDescargarSNCCatastralAvisoTest extends
    MoverProcesoAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoEjeDescargarSNCCatastralAvisoTest.class);

    private Predio predio;
    private Predio predio2;
    private String codigoDepartamento;
    private String codigoMunicipio;

    /**
     *
     */
    //private static final String TIPO_TRAMITE = "1"; // Mutación
    //private static final String CLASE_MUTACION = EMutacionClase.PRIMERA.getCodigo();
    //private static final String CLASIFICACION_TRAMITE = ETramiteClasificacion.OFICINA.toString();	
    private static String CODIGO_PREDIO;
    //= "257540103000000190023000000000";
    private static String CODIGO_PREDIO2;
    //="257540102000011650001500000035";
    @SuppressWarnings("unused")
    //private static final Long ID_SOLICITANTE_SOLICITUD = 22395L;
    //private static final long ID_SOLICITUD = 6159L;

    private static String CATASTRAL = "1";
    private static String AVISOS = "2";

    private static final int MUTACION = 1;
    private static final int RECTIFICACION = 2;
    private static final int COMPLEMENTACION = 3;
    private static final int CANCELACION_PREDIO = 4;
    private static final int MODIFICACION_INSCRIPCION = 5;

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");

            Integer codigoEstructuraOrganizacional = 6040;
            String territorial = MoverProcesoAbstractTest.ATLANTICO;

            CODIGO_PREDIO = this.pruebasService.buscarPredioLibreTramite(
                codigoEstructuraOrganizacional, "08001").getNumeroPredial();
            CODIGO_PREDIO2 = this.pruebasService.buscarPredioLibreTramite(
                codigoEstructuraOrganizacional, "08001").getNumeroPredial();
            assertNotNull(CODIGO_PREDIO);

            codigoDepartamento = CODIGO_PREDIO.substring(0, 2);
            codigoMunicipio = CODIGO_PREDIO.substring(0, 5);

            assertNotNull(codigoDepartamento);
            assertNotNull(codigoMunicipio);

            RADICADOR = usuarios.get(territorial).get(ERol.FUNCIONARIO_RADICADOR);
            assertNotNull(RADICADOR);
            assertNotNull(RADICADOR.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_CONSERVACION = usuarios.get(territorial).get(ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION.getDescripcionEstructuraOrganizacional());

            EJECUTOR = usuarios.get(territorial).get(ERol.EJECUTOR_TRAMITE);
            assertNotNull(EJECUTOR);
            assertNotNull(EJECUTOR.getDescripcionEstructuraOrganizacional());

            predio = conservacionService.getPredioByNumeroPredial(CODIGO_PREDIO);
            predio2 = conservacionService.getPredioByNumeroPredial(CODIGO_PREDIO2);
            assertNotNull(predio);
            LOGGER.debug("Id predio:" + predio.getId());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * Crea una solicitud con un trámite
     */
    @Test(threadPoolSize = 5, invocationCount = 20, timeOut = 350000)
    // @Test
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearSolicitud");

        try {
            // /////////////////////////////////////////////////////////////////////////////////////////////
            Solicitud solicitud = new Solicitud();
            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setFecha(new Date());
            solicitud.setUsuarioLog(this.getClass().toString());
            solicitud.setAnexos(0);
            solicitud.setFolios(1);
            solicitud.setFinalizado("NO");
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.toString());

            List<DatoRectificar> datosRectificar = tramiteService.obtenerDatosRectificarPorTipo(
                "PROPIETARIO");
            datosRectificar.addAll(tramiteService.obtenerDatosRectificarPorTipo("PREDIO"));
            datosRectificar.addAll(tramiteService.obtenerDatosRectificarPorTipo("GENERALES"));

            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setSistemaEnvio("1");
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());
            solicitud.setTipo(Math.random() > 0.5 ? CATASTRAL : AVISOS); //TRAMITE_CATASTRAL o AVISOS

            Tramite tramite = new Tramite();
            tramite.setPredio(predio);
            Departamento d = new Departamento();

            d.setCodigo(codigoDepartamento);
            Municipio m = new Municipio();
            m.setCodigo(codigoMunicipio);
            tramite.setDepartamento(d);
            tramite.setMunicipio(m);
            tramite.setNumeroPredial(predio.getNumeroPredial());
            tramite.setFuncionarioEjecutor(EJECUTOR.getLogin());
            tramite.setFuncionarioRadicador(RADICADOR.getLogin());

            //int numero=1+Math.abs((int)(Math.random()*5));
            //Para esta prueba es necesario forzar que el trámite sea de terreno
            int numero = 1;
            tramite.setTipoTramite("" + numero);
            tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());

            String descripcion = "";
            switch (Integer.parseInt(tramite.getTipoTramite())) {
                case MUTACION: descripcion = "Mutación";
                    break;
                case RECTIFICACION: descripcion = "Rectificación";
                    break;
                case COMPLEMENTACION: descripcion = "Complementación";
                    break;
                case CANCELACION_PREDIO: descripcion = "Cancelación de Predio";
                    break;
                case MODIFICACION_INSCRIPCION: descripcion = "Modificación Inscripción Catastral";
                    break;
            }
            if (solicitud.getTipo().equals(AVISOS) && (tramite.getTipoTramite().equals("" +
                CANCELACION_PREDIO) || tramite.getTipoTramite().
                    equals("" + MODIFICACION_INSCRIPCION))) {
                tramite.setTipoTramite("" + MUTACION);
                descripcion = "Mutación";
            }

            if (tramite.getTipoTramite().equals(RECTIFICACION) || tramite.getTipoTramite().equals(
                COMPLEMENTACION)) {
                tramite.setTramiteRectificacions(new ArrayList<TramiteRectificacion>());
                for (int i = 0; i < 3; i++) {
                    TramiteRectificacion tr = new TramiteRectificacion();
                    tr.setTramite(tramite);
                    tr.setFechaLog(new Date());
                    tr.setUsuarioLog("MoverProcesoEjeDescargarSNCCatastralAvisoTest");
                    int cual = (int) (Math.random() * datosRectificar.size());
                    tr.setDatoRectificar(datosRectificar.get(cual));
                    tramite.getTramiteRectificacions().add(tr);
                }
            }

            if (tramite.getTipoTramite().equals("" + MUTACION)) {
                int numero2 = 1 + Math.abs((int) (Math.random() * 5));
                //Forzar que el tramite sea de terreno, aplica para mutaciónes 2,3 y 5
                do {
                    numero2 = 1 + Math.abs((int) (Math.random() * 5));
                } while (numero2 == 1 || numero2 == 4);

                descripcion += " " + numero2;
                switch (numero2) {
                    case 1: tramite.setClaseMutacion(EMutacionClase.PRIMERA.getCodigo());
                        tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
                        break;
                    case 2: tramite.setClaseMutacion(EMutacionClase.SEGUNDA.getCodigo());
                        tramite.setClasificacion(ETramiteClasificacion.TERRENO.toString());
                        tramite.setSubtipo(Math.random() > 0.5 ? "ENGLOBE" : "DESENGLOBE");
                        if (tramite.getSubtipo().equals("ENGLOBE")) {
                            TramitePredioEnglobe pe1 = new TramitePredioEnglobe();
                            pe1.setPredio(predio);
                            TramitePredioEnglobe pe2 = new TramitePredioEnglobe();
                            pe2.setPredio(predio2);
                            pe1.setTramite(tramite);
                            pe2.setTramite(tramite);
                            tramite.getTramitePredioEnglobes().add(pe1);
                            tramite.getTramitePredioEnglobes().add(pe2);
                        }
                        break;
                    case 3: tramite.setClaseMutacion(EMutacionClase.TERCERA.getCodigo());
                        tramite.setClasificacion(ETramiteClasificacion.TERRENO.toString());
                        break;
                    case 4: tramite.setClaseMutacion(EMutacionClase.CUARTA.getCodigo());
                        tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
                        break;
                    case 5: tramite.setClaseMutacion(EMutacionClase.QUINTA.getCodigo());
                        tramite.setClasificacion(ETramiteClasificacion.TERRENO.toString());
                        break;
                }
                if (tramite.getClasificacion().equals(ETramiteClasificacion.TERRENO.toString())) {
                    ComisionTramite ct = new ComisionTramite();
                    ct.setTramite(tramite);
                    Comision c = new Comision();
                    c.setId(655l);
                    ct.setComision(c);
                    ct.setUsuarioLog("MoverProcesoEjeDescargarSNCCatastralAvisoTest");
                    ct.setFechaLog(new Date());
                    List<ComisionTramite> cs = new ArrayList<ComisionTramite>();
                    cs.add(ct);
                    tramite.setComisionTramites(cs);
                    //this.tramiteService.actualizarTramite(tramite);
                }

            }

            if (solicitud.getTipo().equals(AVISOS) && tramite.getClaseMutacion() != null &&
                (tramite.getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo()) || tramite.
                getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo()))) {
                tramite.setClaseMutacion(EMutacionClase.PRIMERA.getCodigo());
                tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
            }

            List<TramiteDocumentacion> tramitesDocumentacion = new ArrayList<TramiteDocumentacion>();

            TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            //tipoD.setId(Constantes.TIPO_DOCUMENTO_OTRO_ID);
            tipoD.setId(ETipoDocumentoId.OTRO.getId());

            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            doc.setDescripcion("rutaDePruebaDeIntegracion");
            doc.setTipoDocumento(tipoD);
            doc.setUsuarioCreador(RADICADOR.getLogin());
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());

            tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
            tramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            tramiteDocumentacion.setDepartamento(d);
            tramiteDocumentacion.setMunicipio(m);
            tramiteDocumentacion.setCantidadFolios(1);
            tramiteDocumentacion.setTipoDocumento(tipoD);
            tramite.setNumeroPredial(CODIGO_PREDIO);

            // para que la documentación este completa
            tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());
            // Si se queire avanzar el trámite estado a digitalizar
            tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

            tramiteDocumentacion.setFechaAporte(new Date());
            tramiteDocumentacion.setTramite(tramite);
            tramiteDocumentacion.setUsuarioLog("MoverProcesoEjeDescargarSNCCatastralAvisoTest");
            tramiteDocumentacion.setFechaLog(new Date());

            tramitesDocumentacion.add(tramiteDocumentacion);
            tramiteDocumentacion.setDocumentoSoporte(doc);
            tramite.setTramiteDocumentacions(tramitesDocumentacion);

            solicitud.getTramites().add(tramite);

            List<SolicitanteSolicitud> solicitantes = tramiteService
                .getSolicitanteSolicitudAleatorio();
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);// forzar crear nuevo objeto
                ss.setSolicitud(solicitud);
            }

            solicitud.setSolicitanteSolicituds(solicitantes);

            solicitud = tramiteService.guardarActualizarSolicitud(RADICADOR,
                solicitud, null);
            assertNotNull(solicitud);

            List<Tramite> tramitesCreados = solicitud.getTramites();
            assertTrue(tramitesCreados.size() > 0);
            Tramite tramiteSerializado = tramitesCreados.get(0);
            assertNotNull(tramiteSerializado);
            assertNotNull(tramiteSerializado.getId());
            LOGGER.debug("Id Solicitud:" + solicitud.getId());
            LOGGER.debug("Id tramite:" + tramiteSerializado.getId());
            // /////////////////////////////////////////////////////////////////////////////////////////////
            SolicitudCatastral solicitudCatastral = testCrearYMoverProceso(
                solicitud, tramiteSerializado, descripcion);
            assertNotNull(solicitudCatastral);
            assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    @Implement
    protected void moverActividades(String id, SolicitudCatastral solicitudCatastral,
        Tramite tramite)
        throws Exception {

        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(
            id,
            RESPONSABLE_CONSERVACION,
            solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);

        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES);

        if (tramite.getClasificacion().equals(ETramiteClasificacion.TERRENO.toString())) {
            moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
                ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR);
        }

        // /////////////////////////////////////////////////////////////////////////////////////
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);
        // /////////////////////////////////////////////////////////////////////////////////////

        if (tramite.getClasificacion().equals(ETramiteClasificacion.TERRENO.toString())) {
            moverActividad(id, EJECUTOR, solicitudCatastral,
                ProcesoDeConservacion.ACT_EJECUCION_ALISTAR_INFORMACION);
        }

    }

}
