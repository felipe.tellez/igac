package co.gov.igac.snc.fachadas.test.integration.procesos.actualizacion.planificacion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoActualizacionAbstractTest;
import co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.ejecucion.MoverProcesoActAprobarSolicitudPruebaTest;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.util.EActualizacionDocumentacionZona;
import co.gov.igac.snc.persistence.util.EActualizacionTipoProceso;
import co.gov.igac.snc.persistence.util.EConvenioEstado;

@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoActPlanRevisarConvenio extends
    MoverProcesoActualizacionAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActAprobarSolicitudPruebaTest.class);

    private Long ID_ACTUALIZACION = new Long(250);
    private static final long TIME_SLEEP = 3000;

    private Municipio MUNICIPIO;
    private Departamento DEPARTAMENTO;

    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");

            DIRECTOR_TERRITORIAL = generalesService.getCacheUsuario("prucundi11");
            assertNotNull(DIRECTOR_TERRITORIAL);
            assertNotNull(DIRECTOR_TERRITORIAL.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_ACTUALIZACION = generalesService.getCacheUsuario("prucundi01");
            assertNotNull(RESPONSABLE_ACTUALIZACION);
            assertNotNull(RESPONSABLE_ACTUALIZACION.getDescripcionEstructuraOrganizacional());

            ABOGADO = generalesService.getCacheUsuario("prucundi03");
            assertNotNull(ABOGADO);
            assertNotNull(ABOGADO.getDescripcionEstructuraOrganizacional());

            SUBDIRECTOR_CATASTRO = generalesService.getCacheUsuario("subcatastro");
            assertNotNull(SUBDIRECTOR_CATASTRO);
            assertNotNull(SUBDIRECTOR_CATASTRO.getDescripcionEstructuraOrganizacional());

            MUNICIPIO = generalesService.getCacheMunicipioByCodigo("25754");
            assertNotNull(MUNICIPIO);

            DEPARTAMENTO = generalesService.getCacheDepartamentoByCodigo("25");
            assertNotNull(DEPARTAMENTO);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * Crea un proceso de actualizacion
     */
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 400000)
    // @Test
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearSolicitud");

        try {
            Actualizacion actualizacion = new Actualizacion();
            actualizacion.setFechaLog(new Date());
            actualizacion.setFecha(new Date());
            actualizacion.setUsuarioLog("prucundi00");
            actualizacion.setMunicipio(MUNICIPIO);
            actualizacion.setDepartamento(DEPARTAMENTO);
            actualizacion.setZona(EActualizacionDocumentacionZona.RURAL.getDescripcion());
            actualizacion.setTipoProceso(EActualizacionTipoProceso.ACTUALIZACION.getDescripcion());

            actualizacion = actualizacionService.guardarYActualizarActualizacion(actualizacion);

            Convenio convenio = new Convenio();
            convenio.setActualizacion(actualizacion);
            convenio.setUsuarioLog("prucundi00");
            convenio.setEstado(EConvenioEstado.APROBADO.getCodigo());
            convenio.setNumero("" + Math.floor(Math.random() * 148 + 1));
            convenio.setZona(EActualizacionDocumentacionZona.RURAL.getDescripcion());
            convenio.setFechaLog(new Date());
            convenio.setValor(new Double(10000000));
            convenio.setPlazoEjecucion(new Date(2013, 12, 31));
            convenio.setTipoProyecto("Interadministrativo");

            actualizacion.setConvenios(new LinkedList<Convenio>());
            actualizacion.getConvenios().add(convenio);
            convenio = actualizacionService.guardarConvenio(convenio);

            ActualizacionContrato contratoA = actualizacionService.
                getActualizacionContratoById(101L);

            ActualizacionContrato contratoB = actualizacionService.
                getActualizacionContratoById(102L);

            ActualizacionContrato contratoC = actualizacionService.
                getActualizacionContratoById(103L);

            List<ActualizacionContrato> contratos = new LinkedList<ActualizacionContrato>();
            contratos.add(contratoA);
            contratos.add(contratoB);
            contratos.add(contratoC);

            actualizacion.setActualizacionContratos(contratos);
            contratos = actualizacionService.guardarYActualizarListActualizacionContrato(contratos);

            actualizacion = actualizacionService.guardarYActualizarActualizacion(actualizacion);

            ActualizacionCatastral actualizacionCatastral = super.testCrearYMoverProceso(
                actualizacion);
            assertNotNull(actualizacionCatastral);
            assertNotNull(actualizacionCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Override
    protected void moverActividades(String idInstanciaProceso,
        ActualizacionCatastral actualizacionCatastral) throws Exception {
        /** ************************************************************************************** */
        /** PLANIFICACION ********************************************************************** */
        /** ************************************************************************************** */

        moverActividad(
            idInstanciaProceso,
            RESPONSABLE_ACTUALIZACION,
            actualizacionCatastral,
            ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_ESTUDIO_COSTOS);

        moverActividad(
            idInstanciaProceso,
            DIRECTOR_TERRITORIAL,
            actualizacionCatastral,
            ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_ESTUDIO_COSTOS_PROG);

        moverActividad(
            idInstanciaProceso,
            ABOGADO,
            actualizacionCatastral,
            ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_CONVENIO);
//		
//		 moverActividad(
//		 idInstanciaProceso,
//		 DIRECTOR_TERRITORIAL,
//		 actualizacionCatastral,
//		 ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_CONVENIO);
//		
//		 moverActividad(
//		 idInstanciaProceso,
//		 SUBDIRECTOR_CATASTRO,
//		 actualizacionCatastral,
//		 ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_MONTOS_CUPOS_PRESUPUESTALES);
//		
//		 moverActividad(
//		 idInstanciaProceso,
//		 ABOGADO,
//		 actualizacionCatastral,
//		 "ProyectarResolucionOrdenandoEjecucion");
//		 //TODO::franz.gamba:: cambiar el valor de la tranzicion por el que
//		 //esta en la enumeracion del proceso de actualizacion
//		
//		 moverActividad(
//		 idInstanciaProceso,
//		 DIRECTOR_TERRITORIAL,
//		 actualizacionCatastral,
//		 ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_RESOLUCION_ORDENA_EJECUCION);
//		
//		 moverActividad(
//		 idInstanciaProceso,
//		 DIRECTOR_TERRITORIAL,
//		 actualizacionCatastral,
//		 ProcesoDeActualizacion.ACT_PLANIFICACION_AUTORIZAR_INICIO_ACTUALIZACION);

        //-----------------------------------------------------------------------------------------
        // Actividades Paralelas
        //-----------------------------------------------------------------------------------------
//		moverActividad(
//				idInstanciaProceso,
//				DIRECTOR_TERRITORIAL,
//				actualizacionCatastral,
//				ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_DOC_TRAMITE_MUNICIPAL);
//		moverActividad(
//				idInstanciaProceso,
//				DIRECTOR_TERRITORIAL,
//				actualizacionCatastral,
//				ProcesoDeActualizacion.ACT_PLANIFICACION_ASIGNAR_RESPONSABLE_PROC_ACTUALIZACION);
//		moverActividad(
//				idInstanciaProceso,
//				RESPONSABLE_ACTUALIZACION,
//				actualizacionCatastral,
//				ProcesoDeActualizacion.ACT_PLANIFICACION_REALIZAR_INDUCCION_PERSONAL);		
//		moverActividad(
//				idInstanciaProceso,
//				RESPONSABLE_ACTUALIZACION,
//				actualizacionCatastral,
//				ProcesoDeActualizacion.ACT_PLANIFICACION_PLAN_REGISTRAR_AREA_TRABAJO);
//		//-----------------------------------------------------------------------------------------
//		
//		moverActividad(
//				idInstanciaProceso,
//				DIRECTOR_TERRITORIAL,
//				actualizacionCatastral,
//				ProcesoDeActualizacion.ACT_PLANIFICACION_GARANTIZAR_LUGAR_TRABAJO);
        /** ************************************************************************************** */
        /** ALISTAMIENTO ********************************************************************** */
        /** ************************************************************************************** */
//		moverActividad(
//				idInstanciaProceso,
//				DIRECTOR_TERRITORIAL,
//				actualizacionCatastral,
//				ProcesoDeActualizacion.AC);
    }

}
