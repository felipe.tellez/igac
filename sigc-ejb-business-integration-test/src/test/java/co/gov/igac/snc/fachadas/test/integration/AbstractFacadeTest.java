package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.net.URL;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.fachadas.IActualizacion;
import co.gov.igac.snc.fachadas.IAvaluos;
import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.fachadas.IFormacion;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.IInterrelacion;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.IPruebas;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.util.ERol;

/**
 * Clase Base para las pruebas de Integración Realiza el lookup para Jboss 7.1
 *
 * @author juan.mendez
 * @version 2.0
 */
public class AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFacadeTest.class);

    public static final String ATLANTICO = "Atlantico";
    public static final String SOACHA = "Soacha";

    private static final String APPNAME = "sigc-jee-1.0";
    private static final String MODULE_NAME = "sigc-ejb-business-1.0-SNAPSHOT";

    private static final String FILE_NAME = "log4j.properties";
    protected static String FILE_NAME_PATH;
    protected static String OUTPUT_DIR;

    public static final Integer CODIGO_ESTRUCTURA_O_ATLANTICO = 6040;

    protected UsuarioDTO RADICADOR;
    protected UsuarioDTO RESPONSABLE_CONSERVACION;
    protected UsuarioDTO EJECUTOR;
    protected UsuarioDTO COORDINADOR;
    protected UsuarioDTO DIRECTOR_TERRITORIAL;
    protected UsuarioDTO ABOGADO;
    protected UsuarioDTO SECRETARIA;

    protected Hashtable<String, Hashtable<ERol, UsuarioDTO>> usuarios =
        new Hashtable<String, Hashtable<ERol, UsuarioDTO>>();

    /**
     * Tiempo de espera para la consulta asincrónica del estado de la tarea
     */
    public static final long ASYNC_TIME_SLEEP = 7000;

    protected ITramite tramiteService;

    protected IConservacion conservacionService;

    protected IFormacion formacionService;

    protected IProcesos procesoService;

    protected IGenerales generalesService;

    protected IAvaluos avaluosService;

    protected IPruebas pruebasService;

    protected IActualizacion actualizacionService;

    protected IInterrelacion interrelacionService;

    /*
     * static { Security.addProvider(new JBossSaslProvider()); }
     */
    @BeforeClass
    protected void setUp() throws Exception {
        try {
            LOGGER.debug("**********************************************");
            LOGGER.debug("**	BEFORE CLASS					  *********");
            LOGGER.debug("**********************************************");

            URL fileUrl = this.getClass().getClassLoader().getResource(FILE_NAME);
            FILE_NAME_PATH = java.net.URLDecoder.decode(fileUrl.getPath(), "UTF-8");
            LOGGER.debug("FILE_NAME_PATH:" + FILE_NAME_PATH);

            OUTPUT_DIR = fileUrl.getPath().replace(FILE_NAME, "");
            LOGGER.debug("OUTPUT_DIR:" + OUTPUT_DIR);

            inicializarServiciosEJBRemotos();
            inicializarUsuarios();
            LOGGER.debug("**********************************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     *
     */
    private void inicializarServiciosEJBRemotos() {
        LOGGER.debug("inicializarServiciosEJBRemotos");
        try {
            generalesService = lookupGeneralesEJB();
            assertNotNull(generalesService, "No se Obtuvo el servicio Generales");

            tramiteService = lookupTramitesEJB();
            assertNotNull(tramiteService, "No se Obtuvo el servicio Tramite");

            procesoService = lookupProcesosEJB();
            assertNotNull(procesoService, "No se Obtuvo el servicio Proceso");

            conservacionService = lookupConservacionEJB();
            assertNotNull(conservacionService, "No se Obtuvo el servicio Conservacion");

            formacionService = lookupFormacionEJB();
            assertNotNull(formacionService, "No se Obtuvo el servicio Formacion");

            pruebasService = lookupPruebasEJB();
            assertNotNull(pruebasService, "No se Obtuvo el servicio Pruebas");

            actualizacionService = lookupActualizacionEJB();
            assertNotNull(pruebasService, "No se Obtuvo el servicio Actualizacion");

            avaluosService = lookupAvaluosEJB();
            assertNotNull(avaluosService, "No se Obtuvo el servicio Avaluos");

            interrelacionService = lookupInterrelacionEJB();
            assertNotNull(interrelacionService, "No se Obtuvo el servicio Interrelacion");

        } catch (Exception e) {
            LOGGER.error("Error al iniciar los servicios remotos EJB");
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     */
    private void inicializarUsuarios() {
        LOGGER.debug("inicializarUsuarios");
        try {
            Hashtable<ERol, UsuarioDTO> usuariosAtlantico = new Hashtable<ERol, UsuarioDTO>();
            /*
             * usuariosAtlantico.put(ERol.FUNCIONARIO_RADICADOR,
             * this.generalesService.getCacheUsuario("pruatlantico2"));
             * usuariosAtlantico.put(ERol.RESPONSABLE_CONSERVACION,
             * this.generalesService.getCacheUsuario("pruatlantico3"));
             * usuariosAtlantico.put(ERol.EJECUTOR_TRAMITE,
             * this.generalesService.getCacheUsuario("pruatlantico17"));
             * usuariosAtlantico.put(ERol.SECRETARIA_CONSERVACION,
             * this.generalesService.getCacheUsuario("pruatlantico21"));
             * usuariosAtlantico.put(ERol.COORDINADOR,
             * this.generalesService.getCacheUsuario("pruatlantico4"));
             * usuariosAtlantico.put(ERol.ABOGADO,
             * this.generalesService.getCacheUsuario("pruatlantico27"));
             * usuariosAtlantico.put(ERol.DIRECTOR_TERRITORIAL,
             * this.generalesService.getCacheUsuario("pruatlantico5"));
             *
             * usuarios.put(ATLANTICO, usuariosAtlantico);
             *
             * /*Hashtable<ERol, UsuarioDTO> usuariosSoacha = new Hashtable<ERol, UsuarioDTO>();
             * usuariosSoacha.put(ERol.FUNCIONARIO_RADICADOR,
             * this.generalesService.getCacheUsuario("prusoacha07"));
             * usuariosSoacha.put(ERol.RESPONSABLE_CONSERVACION,
             * this.generalesService.getCacheUsuario("prusoacha08"));
             * usuariosSoacha.put(ERol.ABOGADO,
             * this.generalesService.getCacheUsuario("prusoacha08"));
             * usuariosSoacha.put(ERol.EJECUTOR_TRAMITE,
             * this.generalesService.getCacheUsuario("prusoacha03"));
             * usuariosSoacha.put(ERol.DIRECTOR_TERRITORIAL,
             * this.generalesService.getCacheUsuario("subcatastro"));
             * usuariosSoacha.put(ERol.COORDINADOR,
             * this.generalesService.getCacheUsuario("prusoacha06"));
             * usuariosSoacha.put(ERol.SECRETARIA_CONSERVACION,
             * this.generalesService.getCacheUsuario("prusoacha16"));
             *
             * usuarios.put(SOACHA, usuariosSoacha); */
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error al inicializar los usuarios2");
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @BeforeTest
    protected void beforeTest() throws Exception {
        LOGGER.info("**********************************************");
        LOGGER.info("**	BEFORE TEST 						  *********");
        LOGGER.info("**********************************************");
    }

    @AfterTest
    protected void afterTest() throws Exception {
        LOGGER.info("**********************************************");
        LOGGER.info("**	AFTER TEST 						  *********");
        LOGGER.info("**********************************************");
    }

    @BeforeMethod
    protected void beforeMethod() throws Exception {
        LOGGER.info("**********************************************");
        LOGGER.info("**	BEFORE METHOD					  *********");
        LOGGER.info("**********************************************");
    }

    @AfterMethod
    protected void afterMethod() throws Exception {
        LOGGER.info("**********************************************");
        LOGGER.info("**	AFTER METHOD					  *********");
        LOGGER.info("**********************************************");
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IGenerales lookupGeneralesEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "GeneralesBean";
        final String viewClassName = IGenerales.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IGenerales) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private ITramite lookupTramitesEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "TramiteBean";
        final String viewClassName = ITramite.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (ITramite) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IProcesos lookupProcesosEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "ProcesosBean";
        final String viewClassName = IProcesos.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IProcesos) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IConservacion lookupConservacionEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "ConservacionBean";
        final String viewClassName = IConservacion.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IConservacion) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IFormacion lookupFormacionEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "FormacionBean";
        final String viewClassName = IFormacion.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IFormacion) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IActualizacion lookupActualizacionEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "ActualizacionBean";
        final String viewClassName = IActualizacion.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IActualizacion) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IPruebas lookupPruebasEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "PruebasBean";
        final String viewClassName = IPruebas.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IPruebas) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IAvaluos lookupAvaluosEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "AvaluosBean";
        final String viewClassName = IAvaluos.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IAvaluos) context.lookup(jndiName);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private IInterrelacion lookupInterrelacionEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        final String beanName = "InterrelacionBean";
        final String viewClassName = IInterrelacion.class.getName();
        final String jndiName = "ejb:" + APPNAME + "/" + MODULE_NAME + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        LOGGER.debug("jndiName:" + jndiName);
        return (IInterrelacion) context.lookup(jndiName);
    }

}
