package co.gov.igac.snc.fachadas.test.integration.client;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.fachadas.IGenerales;

/**
 * Prueba de invocación remota de EJB's publicados en Jboss 7.1
 *
 * @author juan.mendez
 * @version 2.0
 */
public class RemoteEJBClient {

    public static Logger LOGGER = LoggerFactory.getLogger(RemoteEJBClient.class);

    // The EJB invocation happens via the JBoss Remoting project, which uses
    // SASL for
    // authentication for client-server authentication. There are various
    // different security algorithms that
    // SASL supports. In this example we use "anonymous" access to the server
    // and for that we register
    // the JBossSaslProvider which provides support for that algorithm.
    // Depending on how which algorithm
    // is used, this piece of code to register JBossSaslProvider, may or may not
    // be required
    static {
        // Security.addProvider(new JBossSaslProvider());
    }

    @BeforeTest
    public void beforeTest() {
        LOGGER.debug("beforeTest...");
    }

    @AfterTest
    public void afterTest() {
        LOGGER.debug("afterTest...");
    }

    /**
     * Looks up a stateless bean and invokes on it
     *
     * @throws NamingException
     */
    // @Test(threadPoolSize = 1, invocationCount = 10, timeOut = 60000)
    @Test
    public void invokeStatelessBean() throws NamingException {
        try {
            LOGGER.debug("invokeStatelessBean");
            final IGenerales remoteEJB = lookupRemoteStatelessEJB();
            assertNotNull(remoteEJB);
            LOGGER.debug("Obtained a remote stateless ejb  for invocation");
            // invoke on the remote calculator
            List<Departamento> deptos = remoteEJB.getCacheDepartamentos();
            assertNotNull(deptos);
            LOGGER.debug("Cantidad Departamentos:" + deptos.size());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Looks up and returns the proxy to remote stateless calculator bean
     *
     * @return
     * @throws NamingException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private IGenerales lookupRemoteStatelessEJB() throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        // The app name is the application name of the deployed EJBs. This is
        // typically the ear name
        // without the .ear suffix. However, the application name could be
        // overridden in the application.xml of the
        // EJB deployment on the server.
        // Since we haven't deployed the application as a .ear, the app name for
        // us will be an empty string
        final String appName = "sigc-jee-1.0";
        // This is the module name of the deployed EJBs on the server. This is
        // typically the jar name of the
        // EJB deployment, without the .jar suffix, but can be overridden via
        // the ejb-jar.xml
        // In this example, we have deployed the EJBs in a
        // jboss-as-ejb-remote-app.jar, so the module name is
        // jboss-as-ejb-remote-app
        final String moduleName = "sigc-ejb-business-1.0-SNAPSHOT";
        // AS7 allows each deployment to have an (optional) distinct name. We
        // haven't specified a distinct name for
        // our EJB deployment, so this is an empty string
        // final String distinctName = "";
        // The EJB name which by default is the simple class name of the bean
        // implementation class
        final String beanName = "GeneralesBean";
        // the remote view fully qualified class name
        final String viewClassName = IGenerales.class.getName();
        final String jndiName = "ejb:" + appName + "/" + moduleName + "/"/*
             * +
             * distinctName + "/"
             */ + beanName + "!" + viewClassName;
        // let's do the lookup
        LOGGER.debug("jndiName:" + jndiName);
        return (IGenerales) context.lookup(jndiName);
    }

}
