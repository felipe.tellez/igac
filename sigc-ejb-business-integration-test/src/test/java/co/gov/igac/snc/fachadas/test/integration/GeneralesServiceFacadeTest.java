package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETipoDocumentoClase;
import co.gov.igac.snc.util.DocumentoResultadoPPRedios;
import java.util.logging.Level;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


/**
 * Clase con las pruebas de integracion para la interaz IGenerales
 *
 * @author pedro.garcia
 *
 */
public class GeneralesServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralesServiceFacadeTest.class);

    // -------------------------------------------------------------------------------------------------
    @Test
    public void testGetTerritorialesList() {
        LOGGER.debug("on GeneralesServiceFacadeTest#testGetTerritorialesList");

        List<EstructuraOrganizacional> territoriales;
        try {
            territoriales = this.generalesService
                .getCacheTerritoriales();
            assertNotNull(territoriales);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /*
     * @Test public void testSPConsultarPredios() {
     * LOGGER.debug("*******GeneralesServiceFacadeTest#testSPConsultarPredios");
     *
     * try { ArrayList p = new ArrayList(); p.add(null); p.add(null); //
     * "257540216041711057933105117116" p.add(null); p.add(null); p.add(null); p.add(null);
     * p.add(null); p.add(null); p.add(null); p.add("25"); // dpto p.add("25754"); // municipio
     * p.add("K 2 30 25 IN 11"); // direccion p.add(null); p.add("51642229"); // docto p.add(null);
     * p.add(null); p.add(null); p.add(null); p.add(null); p.add(null); p.add(null); p.add(null);
     * p.add(null); p.add(null); p.add(null);
     *
     *
     * Object resultado[] = this.generalesService .ejecutarSPFuncion(
     * EProcedimientoAlmacenadoFuncion.CONSULTAR_PREDIOS, p,
     * EProcedimientoAlmacenadoFuncionTipo.PROCEDIMIENTO, null); LOGGER.error("" +
     * resultado.length); LOGGER.error(resultado.toString()); List<Object[]> datos =
     * (List<Object[]>) resultado[0]; List<Object[]> errores = (List<Object[]>) resultado[1]; for
     * (Object[] o : datos) { for (Object o2 : o) LOGGER.error("" + o2); } LOGGER.error("" +
     * resultado[0]); LOGGER.error("" + resultado[1]);
     *
     * assertNotNull(resultado); } catch (Exception e) { LOGGER.error(e.getMessage(), e);
     * fail(e.getMessage(), e); }
     *
     * }
     */
    // --------------------------------------------------------------------------------------------------
    /**
     * prueba del llamado al procedimiento que devuelve enumeraciones
     *
     * @author pedro.garcia OJO: los parámetros que no se usen para generar la numeración deben ir
     * como cadenas vacías o como 0s
     */
    @Test
    public void testGenerarNumeracion() {
        LOGGER.debug("integration tests: ProcedimientoAlmacenadoDAOTests#testGenerarNumeracion");

        Object resultado[];
        String territorialId = "11000";
        try {

            LOGGER.info("... ejecutando " +
                EProcedimientoAlmacenadoFuncion.GENERAR_NUMERACION);
            resultado = this.generalesService.generarNumeracion(
                ENumeraciones.NUMERACION_TYA, territorialId, "", "", 0);
            LOGGER.info("result[0] = " + (String) resultado[0] +
                "\nresult[1] = " + (String) resultado[1]);

            Assert.assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetTipoDocumentoPorClase() {

        LOGGER.debug("on GeneralesServiceFacadeTest#testGetTipoDocumentoPorClase");
        String clase = ETipoDocumentoClase.FOTOGRAFIA.toString();

        try {
            TipoDocumento d = this.generalesService
                .getCacheTipoDocumentoPorClase(clase);
            LOGGER.debug("Tipo documento: " + d.getNombre());
            assertNotNull(d);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testConsultarRadicadoCorrespondencia() {
        LOGGER.debug("testConsultarRadicadoCorrespondencia");
        try {
            String numeroRadicacionCorrespondencia = "8002011ER00000015";
            Object resultado[] = this.generalesService
                .consultarRadicadoCorrespondencia(numeroRadicacionCorrespondencia, this.RADICADOR);
            for (int i = 0; i < 34; i++) {
                LOGGER.debug("resultado " +
                    i +
                    ": " +
                    (((Object[]) ((ArrayList<Object>) resultado[0])
                        .get(0))[i]));
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     */
    @Test
    public void testGetCacheUsuario() {
        LOGGER.debug("testGetCacheUsuario");
        try {
            String login = "prusoacha08";
            UsuarioDTO user = generalesService.getCacheUsuario(login);
            assertNotNull(user, "No se encontró el usuario " + login);
            LOGGER.debug("user:" + user);
            assertNotNull(user.getLogin(), "No se encontró el login");
            assertNotNull(user.getDescripcionEstructuraOrganizacional(),
                "No se encontró la estructura organizacional");
            assertEquals(user.getDescripcionEstructuraOrganizacional(), "UOC_SOACHA");
            assertNotNull(user.getPrimerNombre(), "no se encontró nombre");
            assertNotNull(user.getRoles(), "No se encontraron roles");
            assertNotNull(user.getCodigoEstructuraOrganizacional(),
                "No se encontró código estructura organizacional");
            assertNotNull(user.getDireccionEstructuraOrganizacional(),
                "No se encontró dirección estructura organizacional");
            assertNotNull(user.getTelefonoEstructuraOrganizacional(),
                "No se encontró teléfono estructura organizacional");
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test
    public void testGetCacheUsuarioDirectorTerritorial() {
        LOGGER.debug("testGetCacheUsuarioDirectorTerritorial");
        try {
            UsuarioDTO user = generalesService.getCacheUsuario("prucundi11");
            assertNotNull(user);
            LOGGER.debug("user:" + user);
            assertNotNull(user.getLogin());
            assertNotNull(user.getDescripcionEstructuraOrganizacional());
            assertEquals(user.getDescripcionEstructuraOrganizacional(), "CUNDINAMARCA");
            assertNotNull(user.getPrimerNombre());
            assertNotNull(user.getRoles());
            assertNotNull(user.getCodigoEstructuraOrganizacional());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para testBuscarDocumentoResultadoPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarDocumentoResultadoPorTramiteId() {
        LOGGER.debug("test: GeneralesServiceFacadeTest#testBuscarDocumentoResultadoPorTramiteId");

        DocumentoResultadoPPRedios dp;
        Long tramiteId = 2001L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            dp = this.generalesService
                .buscarDocumentoResultadoPorTramiteId(tramiteId);

            if (dp != null) {

                if (dp.getDocumentoResultado() != null) {
                    LOGGER.debug("Ruta documento para visualizar " +
                        dp.getDocumentoResultado());
                }
                if (dp.getPprediosTramite() != null &&
                    !dp.getPprediosTramite().isEmpty()) {
                    LOGGER.debug("Predios proyectados " +
                        dp.getPprediosTramite().size());
                }
            }

            Assert.assertNotNull(dp);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de documento en alfresco
     *
     * @author juan.agudelo
     */
    @Test
    public void testConsultarURLDeOficioEnAlfresco() {
        LOGGER.debug("tests: GeneralesServiceFacadeTest#testConsultarURLDeOficioEnAlfresco");

        String alfrescoId = "52c644d7-c963-4065-abd8-c642df407b93";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String answer = "";
            //comentado por cambio en el api
            //this.generalesService.consultarURLDeArchivoEnAlfresco(alfrescoId);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Ruta documento " + answer);

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de la busqueda de todos los tipos de documento no generados por el sistema
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetAllTipoDocumento() {
        LOGGER.debug("unit test: GeneralesServiceFacadeTest#testGetAllTipoDocumento");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<TipoDocumento> tiposDocumento = this.generalesService
                .getAllTipoDocumento();

            if (tiposDocumento != null && !tiposDocumento.isEmpty()) {
                for (TipoDocumento td : tiposDocumento) {
                    LOGGER.debug("Documento: " + td.getNombre());
                }
            }
            assertNotNull(tiposDocumento);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetCacheMunicipiosPorCodigoEstructuraOrganizacional() {
        LOGGER.debug(
            "tests: GeneralesServiceFacadeTest#testGetCacheMunicipiosPorCodigoEstructuraOrganizacional");

        String estructuraOC = "6200";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Municipio> answer = this.generalesService
                .getCacheMunicipiosPorCodigoEstructuraOrganizacional(estructuraOC);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Municipios " + answer.size());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testGetCodigoDepartamentoByCodigoEstructuraOrganizacional() {
        LOGGER.debug(
            "GeneralesServiceFacadeTest0#testGetCodigoDepartamentoByCodigoEstructuraOrganizacional");

        String codigoEO = "6360";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Departamento> answer = this.generalesService
                .getDepartamentoByCodigoEstructuraOrganizacional(codigoEO);

            if (answer != null && !answer.isEmpty()) {

                for (Departamento dTemp : answer) {

                    LOGGER.debug("Código departamento " + dTemp.getCodigo());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de municipios por código de estructura organizacional
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional() {
        LOGGER.debug(
            "GeneralesServiceFacadeTest#testFindMunicipiosTerritorialbyDepartametoYCodigosEstructuraOrganizacional");

        String departamentoCodigo = "25";
        String estructuraOrganizacionalCodigo = "6360";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Municipio> answer = this.generalesService
                .getCacheMunicipiosTerritorialPorDepartametoYCodigoEstructuraOrganizacional(
                    departamentoCodigo, estructuraOrganizacionalCodigo);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Tamaño " + answer.size());

                for (Municipio mTemp : answer) {
                    LOGGER.debug("Municipio codigo " + mTemp.getCodigo());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testCargarDocumentosEnAlfresco() {
        LOGGER.debug("testing testCargarDocumentosEnAlfresco#testActualizarPersonasBloqueo...");

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            UsuarioDTO user = generalesService.getCacheUsuario("prusoacha07");

            Documento d = new Documento();
            d.setId(118L);
            d = generalesService.buscarDocumentoPorId(d.getId());
            d.setId(null);

            DocumentoTramiteDTO docT = new DocumentoTramiteDTO(FILE_NAME_PATH, "1", new Date(), "1",
                1L);
            docT.setNumeroPredial("080010101000002270029000000000");
            docT.setNombreMunicipio("Barranquilla");
            docT.setNombreDepartamento("Atlántico");
            docT.setTipoTramite("tipo_0001");

            boolean resultado = this.generalesService.actualizarDocumentoYAlfresco(user, docT, d);

            LOGGER.debug("num resultado = " + resultado);

            Assert.assertNotNull(resultado);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

//-------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testEnviarCorreo() {
        LOGGER.debug("integration test GeneralesServiceFacadeTest#testEnviarCorreo ...");

        String destinatario;
        String asunto = "Prueba de correo - Interno " + new Date();
        String mensaje = "hola. </br>¿cómo te va?<br/>this is a test....<br/>" + new Date();
        String[] adjuntos = null;
        String[] titulos = null;

        destinatario = "pruatlantico17@igac.gov.co";
        destinatario = "lbriceño@igac.gov.co";
        destinatario = "lbrice\u00F1oP@igac.gov.co";
        destinatario = "prunsantander8@igac.gov.co";

        //D: hago esto aquí solo para probar que no se va a totear con un email con caracteres no ascii
        LOGGER.debug("destinatario: " + destinatario);
        InternetAddress[] addresses = null;
        InternetAddress address;
        try {
            addresses = InternetAddress.parse(destinatario, false);
            address = addresses[0];
        } catch (AddressException ex) {
            LOGGER.error("Error haciendo parse de la dirección de correo " + destinatario + ": " +
                ex.getMessage());
            //fail("Error haciendo parse de la dirección de correo");
        }

        try {
            address = new InternetAddress(destinatario);
        } catch (AddressException ex) {
            LOGGER.error("Error creando la dirección: " + ex.getMessage());
            fail();
        }

        mensaje +=
            "<table style=\"width:60%\" border=\"1\"><tr style='text-decoration: underline'><th><b>fruit</b></th>	<th>%</th><tr>  <tr> <td>Apples</td><td>44%</td></tr>	<tr><td>Bananas</td><td>23%</td></tr><tr><td>Oranges</td><td>13%</td></tr><tr><td>Other</td><td>10%</td></tr></table>";
        try {
            this.generalesService.enviarCorreo(destinatario, asunto, mensaje, adjuntos, titulos);
            LOGGER.debug("... ok");
            assertTrue(true);
        } catch (Exception ex) {
            fail("error enviando el correo: " + ex.getMessage());
        }
    }

    /**
     * Prueba de envío a una dirección de correo fuera del igac
     *
     * @author juan.mendez
     */
    @Test
    public void testEnviarCorreoExterno() {
        LOGGER.debug("integration test GeneralesServiceFacadeTest#testEnviarCorreo ...");

        String destinatario = "colombia@gmail.com";
        String objeto = "SNC - Prueba de correo - Externo " + new Date();
        String mensaje = "hola. </br>¿cómo te va?<br/>this is a test....<br/>" + new Date();
        String[] adjuntos = null;
        String[] titulos = null;

        mensaje +=
            "<table style=\"width:60%\" border=\"1\"><tr style='text-decoration: underline'><th><b>fruit</b></th>	<th>%</th><tr>  <tr> <td>Apples</td><td>44%</td></tr>	<tr><td>Bananas</td><td>23%</td></tr><tr><td>Oranges</td><td>13%</td></tr><tr><td>Other</td><td>10%</td></tr></table>";
        try {
            this.generalesService.enviarCorreo(destinatario, objeto, mensaje, adjuntos, titulos);
            LOGGER.debug("... ok");
            assertTrue(true);
        } catch (Exception ex) {
            fail("error enviando el correo: " + ex.getMessage(), ex);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * Prueba de generación de reporte dinamico
     *
     * @author javier.aponte
     */
    @Test
    public void testObtenerReporteDinamico() {
        LOGGER.debug("on GeneralesServiceFacadeTest#testObtenerReporteDinamico");

        File reporteDinamico;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("DEPARTAMENTO_CODIGO", "25");
        parameters.put("MUNICIPIO_CODIGO", "25754");
        parameters.put("MANZANA_CODIGO", "0417");

        String documentoXml =
            "<?xml version='1.0' encoding='UTF-8'?><report><title>Reporte Lista Datos Basicos</title><subtitle>Lista Datos Basicos</subtitle><query>SELECT (predio.departamento_codigo || ' - ' || FNC_NOMBRE_DEPARTAMENTO(predio.DEPARTAMENTO_CODIGO)) as DEPARTAMENTO,	(predio.municipio_codigo || ' - ' || FNC_NOMBRE_MUNICIPIO(predio.MUNICIPIO_CODIGO)) as MUNICIPIO," +

            "predio.numero_predial,predio.numero_registro,persona.tipo_identificacion,persona.numero_identificacion,	persona.nombre,(predio.tipo || ' - ' || FNC_OBTENER_VALOR_DOMINIO_NOM('PREDIO_TIPO', predio.tipo)) as TIPO,		predio_direccion.direccion," +

            "predio.area_terreno,predio.area_construccion,predio_avaluo_catastral.valor_total_avaluo_catastral FROM PREDIO  INNER JOIN PREDIO_AVALUO_CATASTRAL ON predio_avaluo_catastral.predio_id=predio.id INNER JOIN PREDIO_DIRECCION ON predio_direccion.predio_id = predio.id  INNER JOIN PERSONA_PREDIO INNER JOIN PERSONA ON persona_predio.persona_id  =persona.id  ON predio.id = persona_predio.predio_id " +

            "</query><parameter><name>DEPARTAMENTO_CODIGO</name><type>java.lang.String</type><multiple>false</multiple><rank>false</rank></parameter><parameter><name>MUNICIPIO_CODIGO</name><type>java.lang.String</type><multiple>false</multiple><rank>false</rank></parameter><parameter><name>MANZANA_CODIGO</name><type>java.lang.String</type><multiple>false</multiple><rank>false</rank></parameter><page> /sirtcc/par/parPrdMnsDsdHst.xhtml </page></report>";

        try {
            UsuarioDTO user = generalesService.getCacheUsuario("prusoacha07");
            reporteDinamico = this.generalesService
                .obtenerReporteDinamico(parameters, Long.valueOf(2), user);
            assertNotNull(reporteDinamico);
            LOGGER.debug("path del reporte dinamico: " + reporteDinamico.getPath());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // -------------------------------------------------------------------------------------------------
    /**
     * Test de generar un archivo zip
     *
     * @author javier.aponte
     */
    @Test
    public void testGenerarArchivoZIP() {
        LOGGER.debug("on GeneralesServiceFacadeTest#testGenerarArchivoZIP");

        List<String> rutaArchivo = new ArrayList<String>();
        rutaArchivo.add(
            "C:\\Users\\JAVIER~1.APO\\AppData\\Local\\Temp\\e802c274-4dbf-4123-91c0-4bfd6708f148\\snc_doc_5438329792180983956.pdf");
        rutaArchivo.add(
            "C:\\Users\\JAVIER~1.APO\\AppData\\Local\\Temp\\e5029ef-4fc4-473e-9331-e47cf99a2d08\\2013.07.09 11.51.35 Documento_de_notificación.jpg");
        rutaArchivo.add(
            "C:\\Users\\JAVIER~1.APO\\AppData\\Local\\Temp\\745c4a8-d88b-4b24-80ac-4575c9439135\\08-001-01-01-0000-0065-0004.0-00-00-0000.jpg");

        String resultado;
        try {
            resultado = this.generalesService.crearZipAPartirDeRutaDeDocumentos(rutaArchivo);
            LOGGER.info("La carpeta que se creo es: " + resultado);
            assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

//-------------------------------------------------------------------------------------------------
    /**
     * Test de obtener número de radicado en correspondencia
     *
     * @author javier.aponte
     */
    @Test
    public void testObtenerNumeroRadicadoEnCorrespondencia() {
        LOGGER.debug("on GeneralesServiceFacadeTest#testObtenerNumeroRadicadoEnCorrespondencia");

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add("800"); // Territorial=compania
        parametros.add("SC_SNC"); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add("1"); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add(""); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add("800"); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add("5000"); // Destino lugar
        parametros.add(""); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(""); // Direccion destino
        parametros.add(""); // Tipo identificacion destino
        parametros.add(""); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        try {
            numeroRadicado = this.generalesService.obtenerNumeroRadicado(parametros);
            LOGGER.info("El número de radicado es: " + numeroRadicado);
            assertNotNull(numeroRadicado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * para simular la ejecución del cron que revisa trabajos pendientes de tipo sig. Para evitar
     * conflictos con los cron manejados en el cluster de jboss se ejecuta esa prueba de integración
     *
     * PRE: asegurarse de que las entradas en la tabla Producto_Catastral_Job que le interesen estén
     * con valor AGS_FINALIZADO en el campo 'estado'
     *
     * @author pedro.garcia
     */
    @Test
    public void testVerificarEjecucionJobsPendientesEnSNC() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobsPendientesEnSNC ...");

        try {
            this.generalesService.verificarEjecucionJobsPendientesEnSNC();
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobsPendientesEnSNC: " +
                ex.getMessage());
            fail();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * para simular la ejecución del cron que revisa un trabajos pendientes de tipo sig. Para evitar
     * conflictos con los cron manejados en el cluster de jboss se ejecuta esa prueba de integración
     *
     * PRE: asegurarse de que las entradas en la tabla Producto_Catastral_Job que le interesen estén
     * con valor AGS_FINALIZADO en el campo 'estado'
     *
     * @author pedro.garcia
     */
    @Test
    public void testVerificarEjecucionJobPendienteEnSNC() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobPendienteEnSNC ...");

        Long idJob;

        idJob = 3930656L;

        try {
            this.generalesService.verificarEjecucionJobPendienteEnSNC(idJob);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobsPendientesEnSNC: " +
                ex.getMessage());
            fail();
        }

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método para probar la generación de registros prediales en PDF
     *
     * @author javier.aponte
     */
    @Test
    public void testProcesarRegistrosPredialesProductosPDF() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testProcesarRegistrosPredialesProductosPDF ...");

        ProductoCatastral job;

        job = new ProductoCatastral();

        job.setUsuarioLog("pruatlantico41");
        job.setDatosExtendidos("SI");
        job.setDatosPropietarios("SI");
        job.setId(Long.valueOf(366));

        boolean resultado;

        try {
            resultado = this.generalesService.procesarRegistrosPredialesProductosPDF(job);
            LOGGER.debug("... passed!!!");
            assertTrue(resultado);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testProcesarRegistrosPredialesProductosPDF: " +
                ex.getMessage());
            fail();
        }

    }

    /**
     * para simular la ejecución del cron que revisa un trabajos pendientes de los reportes que se
     * encuentran en estado terminado. Para evitar conflictos con los cron manejados en el cluster
     * de jboss se ejecuta esa prueba de integración
     *
     * PRE: asegurarse de que las entradas en la tabla Re._Reporte_Ejecucion que le interesen estén
     * con valor FINALIZADO en el campo 'estado'
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testReenviarJobReportesTerminado() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobPendienteEnSNC ...");

        Long[] currentTramiteIds = new Long[]{1329L};

        try {

            for (Long idJob : currentTramiteIds) {

                this.generalesService.reenviarJobReportesTerminado(idJob);
            }
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobsPendientesEnSNC: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * Prueba obtencion de jobs para aplicar cambios
     *
     * @author andres.eslava
     */
    @Test
    public void testObtenerJobsEsperandoUnoPorManzana() {
        LOGGER.debug("testObtenerJobsEsperandoUnoPorManzana...INICIA");
        try {
            List<ProductoCatastralJob> jobs = this.generalesService.
                obtenerJobsEsperandoUnoPorManzana(20);
            assertNotNull(jobs);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            LOGGER.debug("testObtenerJobsEsperandoUnoPorManzana...FINALIZA");
        }
    }

    /*
     * Prueba Aplicar Cambios Geograficos @author andres.eslava
     */
    @Test
    public void testAplicarCambiosGeograficos() {
        LOGGER.debug("testAplicarCambiosGeograficos...INICIA");
        try {
            List<ProductoCatastralJob> jobs = this.generalesService.
                obtenerJobsEsperandoUnoPorManzana(20);
            if (!jobs.isEmpty() && jobs.size() > 0) {
                for (ProductoCatastralJob job : jobs) {
                    this.generalesService.aplicarCambiosGeograficos(null);
                    //Solo aplica el primer job que encuentra.
                    break;
                }
            } else {
                LOGGER.debug("No funciona la prueba no hay ningun job para aplicar");
                fail();
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            LOGGER.debug("testAplicarCambiosGeograficos...FINALIZA");
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * para simular la ejecución del cron que revisa un trabajos pendientes de tipo sig. Para evitar
     * conflictos con los cron manejados en el cluster de jboss se ejecuta esa prueba de integración
     *
     * PRE: asegurarse de que las entradas en la tabla Producto_Catastral_Job que le interesen estén
     * con valor JPS_ESPERANDO en el campo 'estado'
     *
     * @author javier.aponte
     */
    @Test
    public void testVerificarEjecucionJobJasperServerPendientesEnSNC() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobJasperServerSNC ...");

        Long idJob;

        idJob = 123545L;

        try {
            this.generalesService.verificarEjecucionJobJasperServerPendientesEnSNC(idJob);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobJasperServerPendientesEnSNC: " +
                ex.getMessage());
            fail();
        }

    }

    /**
     * para simular la ejecución del cron que revisa un trabajos pendientes de tipo sig. Para evitar
     * conflictos con los cron manejados en el cluster de jboss se ejecuta esa prueba de integración
     *
     * PRE: asegurarse de que las entradas en la tabla Producto_Catastral_Job que le interesen estén
     * con valor JPS_TERMINADO en el campo 'estado'
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testVerificarEjecucionJobPendienteJasperEnSNC() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobPendienteEnSNC ...");

        Long idJob;

        //idJob = 12399L;
        idJob = 118751L;

        try {
            this.generalesService.verificarEjecucionJobJasperServerPendientesEnSNC(idJob);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobsPendientesEnSNC: " +
                ex.getMessage());
            fail();
        }

    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 18-12-2015 :: SIGDAOv2: Esta prueba comprende el servicio
     * generarCCUAsync generarCCUAsync :: Enviar un ProductoCatastralDetalle y usuario con rol
     * SELECT id FROM PRODUCTO_CATASTRAL_DETALLE WHERE NUMERO_PREDIAL LIKE '08001%';
     */
    @Test
    public void testGenerarCartaCatastralUrbanaProductos() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testGenerarCartaCatastralUrbanaProductos ...");
        try {
            UsuarioDTO user = new UsuarioDTO();
            user.setLogin("pruatlantico2");
            ProductoCatastralDetalle productoCatastralDetalle = this.generalesService.
                buscarProductoCatastralesDetallePorId(1053782L);

            this.generalesService.generarCartaCatastralUrbanaProductos(productoCatastralDetalle,
                user);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testGenerarCartaCatastralUrbanaProductos: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 18-12-2015 :: SIGDAOv2: Esta prueba comprende el servicio
     * generarFPDAsync generarFPDAsync :: Enviar un ProductoCatastralDetalle y usuario con rol
     * SELECT id FROM PRODUCTO_CATASTRAL_DETALLE WHERE NUMERO_PREDIAL LIKE '08001%';
     */
    @Test
    public void testGenerarFichaPredialDigitalProductos() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testGenerarFichaPredialDigitalProductos ...");
        try {
            UsuarioDTO user = new UsuarioDTO();
            user.setLogin("pruatlantico2");
            ProductoCatastralDetalle productoCatastralDetalle = this.generalesService.
                buscarProductoCatastralesDetallePorId(692240L);

            this.generalesService.
                generarFichaPredialDigitalProductos(productoCatastralDetalle, user);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testGenerarFichaPredialDigitalProductos: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 13-01-2016 :: SIGDAOv2: Esta prueba comprende el servicio
     * generarCPPAsync generarFPDAsync :: Enviar un ProductoCatastralDetalle y usuario con rol
     * SELECT id FROM PRODUCTO_CATASTRAL_DETALLE WHERE NUMERO_PREDIAL LIKE '08001%';
     */
    @Test
    public void testGenerarCertificadoPlanoPredialCatastralProductos() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testGenerarCertificadoPlanoPredialCatastralProductos ...");
        try {
            UsuarioDTO user = new UsuarioDTO();
            user.setLogin("pruatlantico2");
            ProductoCatastralDetalle productoCatastralDetalle = this.generalesService.
                buscarProductoCatastralesDetallePorId(692240L);

            this.generalesService.generarCertificadoPlanoPredialCatastralProductos(
                productoCatastralDetalle, user);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testGenerarCertificadoPlanoPredialCatastralProductos: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 22-01-2016 :: SIGDAOv2: Esta prueba comprende el servicio
     * exportarDatosAsync o aplicarCambiosAsync
     */
    @Test
    public void testReenviarJobGeograficoEsperando() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testReenviarJobGeograficoEsperando ...");
        try {
            Long idJob = 97401L;
            this.generalesService.reenviarJobGeograficoEsperando(idJob);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testReenviarJobGeograficoEsperando: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 29-01-2016 :: SIGDAOv2: Esta prueba comprende el servicio
     * validarInconsistenciasAsync
     */
    @Test
    public void testValidarGeometriaPrediosAsincronico() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testValidarGeometriaPrediosAsincronico ...");
        try {
            UsuarioDTO user = new UsuarioDTO();
            user.setLogin("pruatlantico2");
//            Tramite tramite = this.tramiteService.buscarTramitePorId(122469L);
            Tramite tramite = this.tramiteService.buscarTramitePorId(121918L);

            this.generalesService.validarGeometriaPrediosAsincronico(tramite, user);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testValidarGeometriaPrediosAsincronico: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * para simular la ejecución del cron que revisa un trabajos pendientes de tipo FICHA_PREDIAL DE
     * CONSERVACION. Para evitar conflictos con los cron manejados en el cluster de jboss se ejecuta
     * esa prueba de integración
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testGenerarFichaPredialDigitalConservacion() {

        LOGGER.debug("ConservacionServiceFacadeTest#testGenerarFichaPredialDigital");
        try {
            long[] currentTramiteIds = new long[]{127351L, 127352L, 127354L};

            UsuarioDTO usuario = new UsuarioDTO();

            for (long idJob : currentTramiteIds) {

                Tramite tramite = this.tramiteService.buscarTramiteConResolucionSimple(idJob);

                usuario.setLogin(tramite.getUsuarioLog());

                //boolean fpd = this.conservacionService.actualizarFichaPredialDigital(idJob, usuario, false);
                this.conservacionService.aplicarCambiosConservacion(tramite, usuario, 4);

                //assertTrue(fpd);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testConsultarTramiteDocumentoAsociadoADocumento() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testConsultarTramiteDocumentoAsociadoADocumento ...");

        Long idDoc;

        idDoc = 580678L;

        try {
            this.generalesService.consultaTramiteDocumentosPorDocumentoId(idDoc);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testConsultarTramiteDocumentoAsociadoADocumento: " +
                ex.getMessage());
            fail();
        }

    }

    /**
     * @author leidy.gonzalez
     */
    @Test
    public void testObtenerRepTerritorialReporte() {
        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testObtenerRepTerritorialReporte ...");
        try {

            String codigoTerritorial = "6320";
            String codigoUoc = "6001";
            this.generalesService.obtenerRepTerritorialReporte(codigoTerritorial, codigoUoc);

            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testObtenerRepTerritorialReporte: " +
                ex.getMessage());
            fail();
        }
    }

    /**
     * para simular la ejecución del cron que revisa los reportes prediales pendientes Para evitar
     * conflictos con los cron manejados en el cluster de jboss se ejecuta esa prueba de integración
     *
     *
     *
     * @author Juan.cruz
     */
    @Test
    public void testVerificarEjecucionJobReportesPredialesPendientesEnSNC() {

        LOGGER.debug(
            "Integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobJasperServerSNC ...");

        Long idJob;

        idJob = 3039L;

        try {
            this.generalesService.reenviarJobReportesTerminado(idJob);
            LOGGER.debug("... passed!!!");
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testVerificarEjecucionJobJasperServerPendientesEnSNC: " +
                ex.getMessage());
            fail();
        }

    }

    /**
     * Metodo para simular la creación del job de actualización de colindantes.
     */
    @Test
    public void testActualizarColindantes() {

        LOGGER.debug("ConservacionServiceFacadeTest#testActualizarColindantes");
        try {
            long[] currentTramiteIds = new long[]{661761L};

            UsuarioDTO usuario = new UsuarioDTO();

            for (long tramiteID : currentTramiteIds) {

                Tramite tramite = this.tramiteService.buscarTramitePorId(tramiteID);

                usuario.setLogin(tramite.getUsuarioLog());
                this.conservacionService.aplicarCambiosConservacion(tramite, usuario, 4);

                LOGGER.debug("... passed!!!");
                assertTrue(true);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
}
