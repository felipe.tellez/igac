package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.fail;
import static org.testng.AssertJUnit.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ObjetoNegocio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;

public class JSonTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(JSonTest.class);

    @Test
    public void testCrearObjetoDesdeCadenaJson() {

        ObjectMapper jsonMapper;
        jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Random random = new Random();
        ActualizacionCatastral actualizacionMunicipio = new ActualizacionCatastral();
        actualizacionMunicipio.setIdentificador(random.nextInt(54212458));
        String idMunicipio = Integer.toString(random.nextInt(54212458));
        String idDpto = Integer.toString(random.nextInt(54212458));
        actualizacionMunicipio.setIdMunicipio(idMunicipio);
        actualizacionMunicipio.setIdDepartamento(idDpto);
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("alejandro.sanchez");
        usuario.setDescripcionTerritorial("CENTRAL");
        List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
        listaUsuario.add(usuario);
        actualizacionMunicipio.setUsuarios(listaUsuario);
        actualizacionMunicipio
            .setObservaciones("Proceso creado desde pruebas de integracion");
        actualizacionMunicipio
            .setTransicion(ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_INFO_DIAGNOSTICO);
        actualizacionMunicipio.setTerritorial("CUNDINAMARCA");

        try {
            ObjetoNegocio objNegocio = actualizacionMunicipio;
            String actMunicipiostr = jsonMapper.writeValueAsString(actualizacionMunicipio);
            String objNegocioStr = jsonMapper.writeValueAsString(objNegocio);
            assertEquals(actMunicipiostr, objNegocioStr);
            jsonMapper.readValue(actMunicipiostr, Class.forName(
                "co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral"));
            LOGGER.info(actualizacionMunicipio.getClass().getName());
            LOGGER.info(actualizacionMunicipio.getClass().getSimpleName());
            LOGGER.info(objNegocio.getClass().getName());
            assertEquals(actualizacionMunicipio.getClass().getName(), objNegocio.getClass().
                getName());
        } catch (JsonGenerationException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } catch (JsonMappingException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearObjetoDesdeCadenaJsonConArreglo() {

        ObjectMapper jsonMapper;
        jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Random random = new Random();
        ActualizacionCatastral actualizacionMunicipio = new ActualizacionCatastral();
        actualizacionMunicipio.setIdentificador(random.nextInt(54212458));
        String idMunicipio = Integer.toString(random.nextInt(54212458));
        String idDpto = Integer.toString(random.nextInt(54212458));
        actualizacionMunicipio.setIdMunicipio(idMunicipio);
        actualizacionMunicipio.setIdDepartamento(idDpto);
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("alejandro.sanchez");
        usuario.setDescripcionTerritorial("CENTRAL");
        List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
        listaUsuario.add(usuario);
        actualizacionMunicipio.setUsuarios(listaUsuario);
        actualizacionMunicipio
            .setObservaciones("Proceso creado desde pruebas de integracion");
        actualizacionMunicipio
            .setTransicion(ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_INFO_DIAGNOSTICO);
        actualizacionMunicipio.setTerritorial("CUNDINAMARCA");

        List<ActualizacionCatastral> lista = new ArrayList<ActualizacionCatastral>();
        lista.add(actualizacionMunicipio);
        lista.add(actualizacionMunicipio);
        lista.add(actualizacionMunicipio);

        try {
            String actMunicipiostr = jsonMapper.writeValueAsString(lista);
            LOGGER.info(actMunicipiostr);
            //assertEquals(actMunicipiostr, objNegocioStr);			
            //jsonMapper.readValue(actMunicipiostr, Class.forName("co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral"));
            LOGGER.info(lista.getClass().toString());
            @SuppressWarnings("unchecked")
            List<ActualizacionCatastral> recuperado = (List< ActualizacionCatastral>) jsonMapper.
                readValue(actMunicipiostr, Class.forName("java.util.ArrayList"));
            LOGGER.info("Tamaño: " + recuperado.size());
            List<ActualizacionCatastral> recuperado2 = jsonMapper.readValue(actMunicipiostr,
                new TypeReference<List<?>>() {
            });
            LOGGER.info("Tamaño: " + recuperado2.size());

        } catch (JsonGenerationException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } catch (JsonMappingException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

}
