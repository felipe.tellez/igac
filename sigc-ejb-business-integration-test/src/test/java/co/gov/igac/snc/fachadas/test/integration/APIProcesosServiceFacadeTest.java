package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.contenedores.Actividades;
import co.gov.igac.snc.apiprocesos.contenedores.ArbolProcesos;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.contenedores.Macroproceso;
import co.gov.igac.snc.apiprocesos.contenedores.NodoObjetoProceso;
import co.gov.igac.snc.apiprocesos.contenedores.Organizacion;
import co.gov.igac.snc.apiprocesos.contenedores.Proceso;
import co.gov.igac.snc.apiprocesos.contenedores.Subproceso;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaContingencia;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.ParametrosObjetoNegocioConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.util.ERol;
import java.util.Date;

/**
 *
 * @author juan.mendez
 *
 */
public class APIProcesosServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(APIProcesosServiceFacadeTest.class);

    private static final int TIMER = 5000;

    @Test
    public void testCrearProcesoConservacion() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarios = generalesService.getCacheUsuario("pruatlantico2");
            usuarios.setLogin("pruatlantico2");
            usuarioPruebas.add(usuarios);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarioPruebas));
            //transicionesUsuarios.add(new ActividadUsuarios( ProcesoDeConservacion.ACT_RADICACION_VERIFICAR_INCONSISTENCIAS_ESPACIALES, usuarioPruebas ));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);

            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            Thread.sleep(TIMER);

            assertNotNull(actividades);

            //assertEquals(actividades.size(), 2);			
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesosConservacion() {
        LOGGER.debug("Crear procesos de conservación");
        try {
            Random random = new Random();
            List<SolicitudCatastral> solicitudes = new ArrayList<SolicitudCatastral>();
            List<String> tramites = new ArrayList<String>();
            for (int i = 0; i < 20; ++i) {
                SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
                solicitudCatastral.setIdentificador(random.nextInt(1452124));
                solicitudCatastral.setNumeroPredial(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setObservaciones("Ninguna");
                solicitudCatastral
                    .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);

                List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
                UsuarioDTO usuarios = generalesService.getCacheUsuario("pruatlantico2");
                usuarios.setLogin("pruatlantico2");
                usuarioPruebas.add(usuarios);
                List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
                transicionesUsuarios.add(new ActividadUsuarios(
                    ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION_IPER, usuarioPruebas));
                solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
                solicitudCatastral.setTipoTramite("Mutación 1");
                tramites.add(Long.toString(solicitudCatastral.getIdentificador()));
                solicitudes.add(solicitudCatastral);
            }
            /**
             * Se envía lista de solicitudes para crear procesos de manera masiva. Inicialmente se
             * recomienda listas de 50 registros.
             */
            procesoService.crearProcesosConservacion(solicitudes);
            Map<EParametrosConsultaActividades, String> filtros =
                new HashMap<EParametrosConsultaActividades, String>();
            LOGGER.info("Trámites creados:" + StringUtils.join(tramites, ","));
            filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, StringUtils.join(tramites,
                ","));
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.consultarListaActividades(filtros);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 20);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testAvanzarActividadConservacion() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarios = generalesService.getCacheUsuario("alejandro.sanchez");
            usuarios.setLogin("alejandro.sanchez");
            usuarioPruebas.add(usuarios);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarioPruebas));
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_VERIFICAR_INCONSISTENCIAS_ESPACIALES,
                usuarioPruebas));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);

            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            Thread.sleep(TIMER);

            assertNotNull(actividades);

            //assertEquals(actividades.size(), 2);
            solicitudCatastral.getActividadesUsuarios().clear();
            for (Actividad act : actividades) {
                solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setUsuarios(usuarioPruebas);
                solicitudCatastral
                    .setTransicion(
                        ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
                solicitudCatastral
                    .setObservaciones("Actividad instanciada para pruebas");
                solicitudCatastral.setUsuarios(usuarioPruebas);
                LOGGER.info(act.getRutaActividad().toString());
                procesoService
                    .avanzarActividad(act.getId(), solicitudCatastral);
                Thread.sleep(TIMER);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testAvanzarActividadConservacionDepuracion() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarios = generalesService.getCacheUsuario("alejandro.sanchez");
            usuarios.setLogin("alejandro.sanchez");
            usuarioPruebas.add(usuarios);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarioPruebas));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            assertNotNull(actividades);
            //Mover a determinar procedencia
            solicitudCatastral.setTransicion(
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            procesoService.avanzarActividad(actividades.get(0).getId(), solicitudCatastral);
            //Mover a estudiar ajuste espacial
            Thread.sleep(TIMER);
            actividades = procesoService.obtenerActividadesProceso(id);
            assertNotNull(actividades);
            usuarios = generalesService.getCacheUsuario(
                ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS);
            List<UsuarioDTO> usuarioTiemposMuertos = new ArrayList<UsuarioDTO>();
            usuarioTiemposMuertos.add(usuarios);
            transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL, usuarioTiemposMuertos));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            //solicitudCatastral.setObservaciones(ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            procesoService.avanzarActividad(actividades.get(0).getId(), solicitudCatastral);

            //Mover a determinar procedencia
            Thread.sleep(TIMER);

            //Mover a determinar procedencia
            Thread.sleep(TIMER);
            actividades = procesoService.obtenerActividadesProceso(id);
            assertNotNull(actividades);
            assertNotNull(actividades.get(0).getObservacion());
            assertEquals(actividades.get(0).getObservacion(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            solicitudCatastral.getActividadesUsuarios().clear();
            for (Actividad act : actividades) {
                solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setUsuarios(usuarioPruebas);
                solicitudCatastral
                    .setTransicion(act.getObservacion());
                solicitudCatastral
                    .setObservaciones("Actividad instanciada para pruebas");
                solicitudCatastral.setUsuarios(usuarioPruebas);
                LOGGER.info(act.getRutaActividad().toString());
                procesoService
                    .avanzarActividad(act.getId(), solicitudCatastral);
                Thread.sleep(TIMER);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testAvanzarActividadConservacionDepuracionDigitalizacionConservacion() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarios = generalesService.getCacheUsuario("alejandro.sanchez");
            usuarios.setLogin("alejandro.sanchez");
            usuarioPruebas.add(usuarios);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarioPruebas));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            assertNotNull(actividades);
            //Mover a determinar procedencia
            solicitudCatastral.setTransicion(
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            procesoService.avanzarActividad(actividades.get(0).getId(), solicitudCatastral);
            //Mover a estudiar ajuste espacial
            Thread.sleep(TIMER);
            actividades = procesoService.obtenerActividadesProceso(id);
            assertNotNull(actividades);
            solicitudCatastral.setTransicion(
                ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL);
            solicitudCatastral.setObservaciones(
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            procesoService.avanzarActividad(actividades.get(0).getId(), solicitudCatastral);
            //Mover a determinar procedencia
            Thread.sleep(TIMER);
            actividades = procesoService.obtenerActividadesProceso(id);
            assertNotNull(actividades);
            assertNotNull(actividades.get(0).getObservacion());
            assertEquals(actividades.get(0).getObservacion(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);

            //
            solicitudCatastral.getActividadesUsuarios().clear();
            for (Actividad act : actividades) {
                solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setUsuarios(usuarioPruebas);
                solicitudCatastral
                    .setTransicion(
                        ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
                solicitudCatastral
                    .setObservaciones("Actividad instanciada para pruebas");
                solicitudCatastral.setUsuarios(usuarioPruebas);
                LOGGER.info(act.getRutaActividad().toString());
                procesoService
                    .avanzarActividad(act.getId(), solicitudCatastral);
                Thread.sleep(TIMER);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCreacionSuspensionReanudacionProcesos() {
        try {
            String nombrePrueba = new Object() {
            }.getClass().getEnclosingMethod().getName();
            LOGGER.debug(nombrePrueba);
            procesoService.crearProcesosActualizacionExpress("08001");
            procesoService.reanudarProcesosActualizacionExpress("08001");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail();
        }
    }

    @Test
    public void testCrearProcesoActualizacion() {
        try {
            Random random = new Random();

            ActualizacionCatastral actualizacionMunicipio = new ActualizacionCatastral();
            actualizacionMunicipio.setIdentificador(random.nextInt(54212458));
            String idMunicipio = Integer.toString(random.nextInt(54212458));
            String idDpto = Integer.toString(random.nextInt(54212458));
            actualizacionMunicipio.setIdMunicipio(idMunicipio);
            actualizacionMunicipio.setIdDepartamento(idDpto);
            UsuarioDTO usuario = generalesService.getCacheUsuario("alejandro.sanchez");
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            listaUsuario.add(usuario);
            actualizacionMunicipio.setUsuarios(listaUsuario);
            actualizacionMunicipio
                .setObservaciones("Proceso creado desde pruebas de integracion");
            actualizacionMunicipio
                .setTransicion(ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_INFO_DIAGNOSTICO);
            actualizacionMunicipio.setTerritorial("CUNDINAMARCA");
            String id = procesoService.crearProceso(actualizacionMunicipio);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesoOsmi() {
        try {
            LOGGER.debug("testIniciarProcesoOsmi");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            OfertaInmobiliaria ofertaInmobiliaria = new OfertaInmobiliaria();

            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());

            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            ofertaInmobiliaria.setMunicipio(municipio);
            ofertaInmobiliaria.setDepartamento(dpto);
            ofertaInmobiliaria.setObservaciones("Instancia de prueba");
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);
            listaUsuario.add(responsableConservacion);
            ofertaInmobiliaria.setUsuarios(listaUsuario);
            ofertaInmobiliaria.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            ofertaInmobiliaria.setFuncionario("Pepe");
            ofertaInmobiliaria.setIdRegion("Zona de tolerancia");
            String id = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesoOsmiControlCalidad() {
        try {
            LOGGER.debug("testCrearProcesoOsmiControlCalidad");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            ControlCalidad controlCalidad = new ControlCalidad();

            controlCalidad.setIdentificador(new Long(random.nextInt(54212458)).longValue());

            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            controlCalidad.setMunicipio(municipio);
            controlCalidad.setDepartamento(dpto);
            controlCalidad.setObservaciones("Instancia de prueba");
            controlCalidad.setTransicion(
                ProcesoDeControlCalidad.ACT_SELECCION_SELECCIONAR_MUESTRAS_OFERTAS);
            listaUsuario.add(responsableConservacion);
            controlCalidad.setUsuarios(listaUsuario);
            controlCalidad.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            controlCalidad.setFuncionario("Pepe");
            controlCalidad.setIdRegion("Zona de tolerancia");
            controlCalidad.setOperacion(ProcesoDeControlCalidad.OPERACION_INICIAR);
            String id = procesoService.crearProceso(controlCalidad);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testObtenerProcesosCorrelacionadosControlCalidad() {
        try {
            LOGGER.debug("testCrearProcesoOsmiControlCalidad");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            ControlCalidad controlCalidad = new ControlCalidad();

            controlCalidad.setIdentificador(new Long(random.nextInt(54212458)).longValue());

            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            controlCalidad.setMunicipio(municipio);
            controlCalidad.setDepartamento(dpto);
            controlCalidad.setObservaciones("Instancia de prueba");
            controlCalidad.setTransicion(
                ProcesoDeControlCalidad.ACT_SELECCION_SELECCIONAR_MUESTRAS_OFERTAS);
            listaUsuario.add(responsableConservacion);
            controlCalidad.setUsuarios(listaUsuario);
            controlCalidad.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            controlCalidad.setFuncionario("Pepe");
            controlCalidad.setIdRegion("Zona de tolerancia");
            controlCalidad.setOperacion(ProcesoDeControlCalidad.OPERACION_INICIAR);
            String id = procesoService.crearProceso(controlCalidad);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            Thread.sleep(TIMER);
            ControlCalidad objNegocio = procesoService.
                obtenerObjetoNegocioControlCalidadPorIdProceso(id);
            assertEquals(objNegocio.getMuestrasEnEjecucion(), 0);

            assertNotNull(id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesoOsmiDefinirRegion() {
        try {
            LOGGER.debug("testCrearProcesoOsmiDefinirRegion");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            OfertaInmobiliaria ofertaInmobiliaria = new OfertaInmobiliaria();
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setIdCorrelacion(new Long(random.nextInt(54212458)).longValue());
            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            ofertaInmobiliaria.setMunicipio(municipio);
            ofertaInmobiliaria.setDepartamento(dpto);
            ofertaInmobiliaria.setObservaciones("Instancia de prueba");
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);
            listaUsuario.add(responsableConservacion);
            ofertaInmobiliaria.setUsuarios(listaUsuario);
            ofertaInmobiliaria.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            ofertaInmobiliaria.setFuncionario("Pepe");
            ofertaInmobiliaria.setIdRegion("Zona de tolerancia");
            ///////////////////// CREAR PROCESO OSMI ESPECIFICANDO ÁREA/////////////////////////
            String idProcesoPadre = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((idProcesoPadre != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + idProcesoPadre);
            assertNotNull(idProcesoPadre);
            ///////////////////// DEFINIR REGION PARA LA ÁREA CREADA ///////////////////
            Thread.sleep(TIMER);
            //Primera actividad del proceso a crear
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION);
            //Identificador para la correlación
            ofertaInmobiliaria.setIdCorrelacion(ofertaInmobiliaria.getIdentificador());
            //Identificador del proceso a crear
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION);
            String id = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
            ///////////////// OBTENER PROCESOS CORRELACIONADOS ///////////////////
            Thread.sleep(TIMER);
            OfertaInmobiliaria objNegocio = procesoService.
                obtenerObjetoNegocioOfertasInmobiliariasPorIdProceso(idProcesoPadre);
            assertEquals(objNegocio.getOfertasEnEjecucion(), 1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesoOsmiRealizarControlCalidad() {
        try {
            LOGGER.debug("testCrearProcesoOsmiRealizarControlCalidad");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            ControlCalidad controlCalidad = new ControlCalidad();
            controlCalidad.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            controlCalidad.setIdCorrelacion(new Long(random.nextInt(54212458)).longValue());
            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            controlCalidad.setMunicipio(municipio);
            controlCalidad.setDepartamento(dpto);
            controlCalidad.setObservaciones("Instancia de prueba");
            controlCalidad.setTransicion(
                ProcesoDeControlCalidad.ACT_SELECCION_SELECCIONAR_MUESTRAS_OFERTAS);
            listaUsuario.add(responsableConservacion);
            controlCalidad.setUsuarios(listaUsuario);
            controlCalidad.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            controlCalidad.setFuncionario("Pepe");
            controlCalidad.setIdRegion("Zona de tolerancia");
            controlCalidad.setOperacion(ProcesoDeControlCalidad.OPERACION_INICIAR);
            ///////////////////// CREAR PROCESO OSMI ESPECIFICANDO CONTROL CALIDAD/////////////////////////
            String idProcesoPadre = procesoService.crearProceso(controlCalidad);
            LOGGER.info("Proceso creado: " + ((idProcesoPadre != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + idProcesoPadre);
            assertNotNull(idProcesoPadre);
            ///////////////////// DEFINIR REGION PARA LA ÁREA CREADA ///////////////////
            Thread.sleep(TIMER);
            //Primera actividad del proceso a crear
            controlCalidad.setTransicion(
                ProcesoDeControlCalidad.ACT_VALIDACION_REALIZAR_CONTROL_CALIDAD_MUESTRAS);
            //Identificador para la correlación
            controlCalidad.setIdCorrelacion(controlCalidad.getIdentificador() /* Identificador
             * proceso padre */);
            //Identificador del proceso a crear
            controlCalidad.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            controlCalidad.setOperacion(ProcesoDeControlCalidad.OPERACION_CREAR_MUESTRAS);
            String id = procesoService.crearProceso(controlCalidad);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
            ///////////////////// OBTENER PROCESOS CORRELACIONADOS ///////////////////
            Thread.sleep(TIMER);
            ControlCalidad objNegocio = procesoService.
                obtenerObjetoNegocioControlCalidadPorIdProceso(idProcesoPadre);
            assertEquals(objNegocio.getMuestrasEnEjecucion(), 1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesoOsmiDefinirRegionAvanzarActividadCancelarProceso() {
        try {
            LOGGER.debug("testCrearProcesoOsmiDefinirRegionAvanzarActividad");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            OfertaInmobiliaria ofertaInmobiliaria = new OfertaInmobiliaria();
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setIdCorrelacion(new Long(random.nextInt(54212458)).longValue());
            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            ofertaInmobiliaria.setMunicipio(municipio);
            ofertaInmobiliaria.setDepartamento(dpto);
            ofertaInmobiliaria.setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_INICIAR);
            ofertaInmobiliaria.setObservaciones("Instancia de prueba");
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);
            listaUsuario.add(responsableConservacion);
            ofertaInmobiliaria.setUsuarios(listaUsuario);
            ofertaInmobiliaria.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            ofertaInmobiliaria.setFuncionario("Pepe");
            ofertaInmobiliaria.setIdRegion("Zona de tolerancia");
            ///////////////////// CREAR PROCESO OSMI ESPECIFICANDO ÁREA/////////////////////////
            String id = procesoService.crearProceso(ofertaInmobiliaria);
            long idCorrelacion = ofertaInmobiliaria.getIdentificador();
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
            ///////////////////// DEFINIR REGION PARA LA ÁREA CREADA ///////////////////
            Thread.sleep(TIMER);
            //Primera actividad del proceso a crear
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION);
            //Identificador para la correlación
            ofertaInmobiliaria.setIdCorrelacion(idCorrelacion);
            //Identificador del proceso a crear
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION);
            String idRegion = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((idRegion != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + idRegion);
            assertNotNull(idRegion);

            ///////////////////// DEFINIR REGION 2 PARA LA ÁREA CREADA ///////////////////
            Thread.sleep(TIMER);
            //Primera actividad del proceso a crear
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION);
            //Identificador para la correlación
            ofertaInmobiliaria.setIdCorrelacion(idCorrelacion);
            //Identificador del proceso a crear
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION);
            String idRegion2 = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((idRegion2 != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + idRegion2);
            assertNotNull(idRegion2);

            ///////////////////// AVANZAR ACTIVIDAD SOBRE REGION DEFINIDA ///////////////////
            //Se crea el objeto de negocio especificando almenos la transición y los usuarios
            ofertaInmobiliaria
                .setTransicion(
                    ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_APROBAR_AJUSTAR_ORDEN_COMISION);
            ofertaInmobiliaria
                .setObservaciones("Avance actividad");
            ofertaInmobiliaria.setUsuarios(listaUsuario);
            procesoService.avanzarActividadPorIdProceso(idRegion, ofertaInmobiliaria);
            Thread.sleep(TIMER);

            ofertaInmobiliaria
                .setTransicion(
                    ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO);
            ofertaInmobiliaria
                .setObservaciones("Avance 2 de actividad");

            procesoService.avanzarActividadPorIdProceso(idRegion, ofertaInmobiliaria);
            Thread.sleep(TIMER);
            procesoService.cancelarProcesoPorIdProceso(idRegion, "Prueba cancelación proceso");
            procesoService.cancelarProcesoPorIdProceso(idRegion2, "Prueba cancelación proceso");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCrearProcesoOsmiDefinirRegionAvanzarActividad() {
        try {
            LOGGER.debug("testCrearProcesoOsmiDefinirRegionAvanzarActividad");
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            Random random = new Random();
            List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
            OfertaInmobiliaria ofertaInmobiliaria = new OfertaInmobiliaria();
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setIdCorrelacion(new Long(random.nextInt(54212458)).longValue());
            DivisionAdministrativa municipio = new DivisionAdministrativa("11001", "Bogotá D.C.");
            DivisionAdministrativa dpto = new DivisionAdministrativa("11", "Cundinamarca");
            ofertaInmobiliaria.setMunicipio(municipio);
            ofertaInmobiliaria.setDepartamento(dpto);
            ofertaInmobiliaria.setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_INICIAR);
            ofertaInmobiliaria.setObservaciones("Instancia de prueba");
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);
            listaUsuario.add(responsableConservacion);
            ofertaInmobiliaria.setUsuarios(listaUsuario);
            ofertaInmobiliaria.setTerritorial(responsableConservacion.
                getDescripcionEstructuraOrganizacional());
            ofertaInmobiliaria.setFuncionario("Pepe");
            ofertaInmobiliaria.setIdRegion("Zona de tolerancia");
            ///////////////////// CREAR PROCESO OSMI ESPECIFICANDO ÁREA/////////////////////////
            String id = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            assertNotNull(id);
            ///////////////////// DEFINIR REGION PARA LA ÁREA CREADA ///////////////////
            Thread.sleep(TIMER);
            //Primera actividad del proceso a crear
            ofertaInmobiliaria.setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION);
            //Identificador para la correlación
            ofertaInmobiliaria.setIdCorrelacion(ofertaInmobiliaria.getIdentificador());
            //Identificador del proceso a crear
            ofertaInmobiliaria.setIdentificador(new Long(random.nextInt(54212458)).longValue());
            ofertaInmobiliaria.setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION);
            String idRegion = procesoService.crearProceso(ofertaInmobiliaria);
            LOGGER.info("Proceso creado: " + ((idRegion != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + idRegion);
            assertNotNull(idRegion);
            ///////////////////// AVANZAR ACTIVIDAD SOBRE REGION DEFINIDA ///////////////////
            Thread.sleep(TIMER);
            //Se crea el objeto de negocio especificando almenos la transición y los usuarios
            ofertaInmobiliaria
                .setTransicion(
                    ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_APROBAR_AJUSTAR_ORDEN_COMISION);
            ofertaInmobiliaria
                .setObservaciones("Avance actividad");
            ofertaInmobiliaria.setUsuarios(listaUsuario);
            procesoService.avanzarActividadPorIdProceso(idRegion, ofertaInmobiliaria);
            Thread.sleep(TIMER);

            ofertaInmobiliaria
                .setTransicion(
                    ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO);
            ofertaInmobiliaria
                .setObservaciones("Avance 2 de actividad");

            procesoService.avanzarActividadPorIdProceso(idRegion, ofertaInmobiliaria);

            /////////////////// OBTENER PROCESOS CORRELACIONADOS ///////////////////
            Thread.sleep(TIMER);
            //OfertaInmobiliaria objNegocio = procesoService.obtenerObjetoNegocioOfertasInmobiliariasPorIdProceso(id);
            //assertEquals(objNegocio.getOfertasEnEjecucion(), 1);			

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testConsultarCargaUsuarioConFiltro() {

        TablaContingencia cargaUsuario = null;
        try {
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            cargaUsuario = procesoService.getConteoActividadesConFiltro(
                responsableConservacion,
                ParametrosObjetoNegocioConservacion.TIPO_TRAMITE);
            assertNotNull(cargaUsuario);

            if (cargaUsuario != null) {
                Set<String> tuplas = cargaUsuario.getValores()
                    .keySet();
                for (String tupla : tuplas) {
                    assertNotNull(tupla);
                    LOGGER.info(tupla);
                }
                assertNotNull(cargaUsuario.getDominioVariableX());
                assertNotNull(cargaUsuario.getDominioVariableY());
                LOGGER.info("Variable X" + cargaUsuario.getVariableX());
                LOGGER.info("Variable Y" + cargaUsuario.getVariableY());
                LOGGER.info("Valores X: " + cargaUsuario.getDominioVariableX().toString());
                LOGGER.info("Valores Y: " + cargaUsuario.getDominioVariableY().toString());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testArbolActividades() {
        LOGGER.debug("Test consulta tareas (Arbol actividades)");

        try {
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("pruatlantico2");
            ArbolProcesos arbolProcesos = procesoService
                .consultarTareas(responsableConservacion);
            assertNotNull(arbolProcesos);

            if (!arbolProcesos.getRootElement().getChildren().isEmpty()) {

                Organizacion organizacion = (Organizacion) arbolProcesos
                    .getRootElement();
                assertEquals("SNC", organizacion.getData().getId());

                // Recuperación con Objeto padre (genérico)
                NodoObjetoProceso objetoProceso = (NodoObjetoProceso) arbolProcesos
                    .getRootElement();
                assertEquals("SNC", objetoProceso.getData().getId());

                // Obtener macroprocesos
                List<Macroproceso> macroprocesos = organizacion
                    .getMacroprocesos();
                assertTrue(macroprocesos.size() >= 1);
                Macroproceso macroproceso = macroprocesos.get(0);
                // Obtener procesos
                List<Proceso> procesos = macroproceso.getProcesos();

                for (Proceso proceso : procesos) {
                    List<Subproceso> subprocesos = proceso.getSubprocesos();
                    for (Subproceso subproceso : subprocesos) {
                        List<Actividades> actividades = subproceso
                            .getActividades();

                        for (Actividades act : actividades) {

                            assertNotNull(act.getRutaActividad());
                            assertNotNull(act.getTipoActividad());
                            assertNotNull(act.getTipoProcesamiento());
                            assertNotNull(act.getTransiciones());
                            assertTrue(act.getNumeroDeActividades() > 1);
                        }
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testListaActividadesConFiltroNombreActividad() {
        LOGGER.debug("Test consulta tareas (Lista actividades)");

        try {
            UsuarioDTO responsable;
            responsable = generalesService.getCacheUsuario("alejandro.sanchez");
            List<Actividad> actividades = procesoService
                .consultarListaActividadesFiltroNombreActividad(responsable,
                    ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);
            assertNotNull(actividades);
            LOGGER.info("Número de actividades: " + actividades.size());
            for (Actividad actividad : actividades) {
                LOGGER.info(actividad.getIdObjetoNegocio() + ", " +
                    actividad.getNombre() + ", " +
                    actividad.getFechaAsignacion());
                // Id de solicitud
                String solicitud = actividad.getNumeroSolicitud();
                LOGGER.debug(solicitud);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }

    }

    @Test
    /**
     * Prueba para obtener la lista de actividades que se encuentran en una actividad determinada en
     * una territorial. Se deben especificar 2 filtros: - Territorial. Una territorial o unidad
     * operativa. Si se usa el usuario de sesión, debe usar el método @see
     * co.gov.igac.generales.dto.UsuarioDTO#getDescripcionEstructuraOrganizacional().
     *
     * - Estados actividad. Pueden darse 2 casos, actividades finalizadas (5) o actividades en
     * ejecución (2,8). Los estados de una actividad se pueden ver en @see
     * co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad Los filtros válidos se pueden
     * encontrar en @see co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades
     *
     */
    public void testListaActividadesConFiltroTerritorialEstadoActividad() {
        LOGGER.debug("Test consulta tareas (Lista actividades)");

        try {
            Map<EParametrosConsultaActividades, String> filtros =
                new HashMap<EParametrosConsultaActividades, String>();
            UsuarioDTO secretaria;
            secretaria = generalesService.getCacheUsuario("pruatlantico2");

            LOGGER.info(secretaria.getDescripcionTerritorial());

            filtros.put(EParametrosConsultaActividades.TERRITORIAL, secretaria.
                getDescripcionEstructuraOrganizacional());
            filtros.put(EParametrosConsultaActividades.ESTADOS, "2,8");
            //filtros.put( EParametrosConsultaActividades.PROPIETARIO_ACTIVIDAD, "pruatlantico2");

            List<Actividad> actividades = procesoService.consultarListaActividades(filtros);
            LOGGER.info("Actividades: " + actividades.size());
            for (Actividad actividad : actividades) {
                LOGGER.info(actividad.getIdObjetoNegocio() + ", " +
                    actividad.getNombre() + ", " +
                    actividad.getRoles().toString().replaceAll("\\[|\\]", "") +
                    actividad.getUsuarioEjecutor());

                // Id de solicitud
                String solicitud = actividad.getNumeroSolicitud();
                LOGGER.debug(solicitud);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }

    }

    @Test
    public void testListaActividadesPorUsuarioRol() {
        LOGGER.debug("Test consulta tareas (Lista actividades)");

        try {
            UsuarioDTO usuario;
            //usuario = generalesService.getCacheUsuario("yineth.fonseca");
            usuario = generalesService.getCacheUsuario("pruatlantico3");
            List<Actividad> actividades = procesoService.consultarListaActividades(usuario,
                ERol.RESPONSABLE_CONSERVACION);
            LOGGER.info("Actividades: " + actividades.size());

            for (Actividad actividad : actividades) {
                LOGGER.info(actividad.getIdObjetoNegocio() + ", " +
                    actividad.getNombre() + ", " +
                    actividad.getRoles().toString().replaceAll("\\[|\\]", "") + ", " +
                    actividad.getUsuarioEjecutor());

                // Id de solicitud
                String solicitud = actividad.getNumeroSolicitud();
                LOGGER.debug(solicitud);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }

    }

    @Test
    public void testAvanzarActividadPorIdProceso() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarioPruebasReclamar = generalesService.
                getCacheUsuario("alejandro.sanchez");
            usuarioPruebas.add(usuarioPruebasReclamar);

            solicitudCatastral.setUsuarios(usuarioPruebas);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);

            Thread.sleep(TIMER);
            solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            solicitudCatastral
                .setObservaciones("Actividad instanciada para pruebas");
            solicitudCatastral.setUsuarios(usuarioPruebas);
            procesoService.avanzarActividadPorIdProceso(id, solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService
                .obtenerActividadesProceso(id);
            Thread.sleep(TIMER);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 1);
            assertEquals(actividades.get(0).getNombre(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testCancelarProceso() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarioPruebasReclamar = generalesService.
                getCacheUsuario("alejandro.sanchez");
            usuarioPruebas.add(usuarioPruebasReclamar);

            solicitudCatastral.setUsuarios(usuarioPruebas);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);

            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            Thread.sleep(TIMER);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 1);
            //Cancelar el proceso
            procesoService.cancelarProcesoPorIdProceso(id,
                "Cancelación de proceso desde prueba JBoss");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testAvanzarActividadDosVeces() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarioPruebasReclamar = generalesService.
                getCacheUsuario("alejandro.sanchez");
            usuarioPruebas.add(usuarioPruebasReclamar);

            solicitudCatastral.setUsuarios(usuarioPruebas);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);

            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService
                .obtenerActividadesProceso(id);
            Thread.sleep(TIMER);

            assertNotNull(actividades);

            assertEquals(actividades.size(), 1);

            for (Actividad act : actividades) {
                solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral
                    .setTransicion(
                        ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
                solicitudCatastral
                    .setObservaciones("Actividad instanciada para pruebas");
                solicitudCatastral.setUsuarios(usuarioPruebas);
                LOGGER.info(act.getRutaActividad().toString());
                procesoService
                    .avanzarActividad(act.getId(), solicitudCatastral);
                Thread.sleep(TIMER);
                SolicitudCatastral resultado = procesoService
                    .avanzarActividad(act.getId(), solicitudCatastral);

                LOGGER.info(resultado.toString());

                fail();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     */
    @Test
    public void testListaActividades() {
        List<Actividad> actividades = null;
        try {
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("pruatlantico2");
            actividades = procesoService.consultarListaActividades(responsableConservacion);
            assertNotNull(actividades);
            procesoService.consultarTareas(responsableConservacion);
            assertTrue(actividades.size() > 0);
            for (Actividad act : actividades) {
                LOGGER.debug("Actividad:" + act.getId());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testListaActividadesObjetoNegocio() {
        List<Actividad> actividades = null;
        try {
            actividades = procesoService.getActividadesPorIdObjetoNegocio(204404L);
            assertTrue(actividades.size() > 0);
            for (Actividad act : actividades) {
                LOGGER.debug("Actividad:" + act.getId() + ", " + act.getNombre());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testReanudarActividades() {
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarios = generalesService.getCacheUsuario("pruatlantico2");
            usuarios.setLogin("pruatlantico2");
            usuarioPruebas.add(usuarios);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarioPruebas));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            Thread.sleep(TIMER);
            assertNotNull(actividades);
            DateTime start = new DateTime(2006, 11, 10, 12, 10, 0, 0);
            DateTime end = new DateTime(2006, 11, 11, 12, 11, 0, 0);
            Period period = new Period(start, end);
            Calendar nuevaFecha = Calendar.getInstance();
            DateTime fecha = new DateTime(nuevaFecha.getTime());
            for (Actividad act : actividades) {
                fecha = fecha.plus(period);
                nuevaFecha.setTime(fecha.toDate());
                procesoService.suspenderActividad(act.getId(), nuevaFecha,
                    "Motivo: prueba suspensión");
                Thread.sleep(TIMER);
                procesoService.reanudarActividad(act.getId());
                LOGGER.debug("Actividad:" + act.getId());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testSuspenderActividades() {
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarios = generalesService.getCacheUsuario("pruatlantico2");
            usuarios.setLogin("pruatlantico2");
            usuarioPruebas.add(usuarios);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarioPruebas));
            solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService.obtenerActividadesProceso(id);
            Thread.sleep(TIMER);
            assertNotNull(actividades);
            DateTime start = new DateTime(2006, 11, 10, 12, 10, 0, 0);
            DateTime end = new DateTime(2006, 11, 11, 12, 11, 0, 0);
            Period period = new Period(start, end);
            Calendar nuevaFecha = Calendar.getInstance();
            DateTime fecha = new DateTime(nuevaFecha.getTime());
            for (Actividad act : actividades) {
                fecha = fecha.plus(period);
                nuevaFecha.setTime(fecha.toDate());
                procesoService.suspenderActividad(act.getId(), nuevaFecha,
                    "Motivo: prueba suspensión");
                LOGGER.debug("Actividad:" + act.getId());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testListaActividadesTerritorial() {
        List<Actividad> actividades = null;
        try {
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService.getCacheUsuario("alejandro.sanchez");
            actividades = procesoService.consultarListaActividades(responsableConservacion);
            assertNotNull(actividades);
            procesoService.consultarListaActividades(responsableConservacion.
                getDescripcionEstructuraOrganizacional(),
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);
            assertTrue(actividades.size() > 0);
            for (Actividad act : actividades) {
                LOGGER.debug("Actividad:" + act.getId());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testTransferirActividad() {
        LOGGER.debug("Mover proceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarioPruebasReclamar = generalesService.
                getCacheUsuario("alejandro.sanchez");
            usuarioPruebasReclamar.setDescripcionTerritorial(ATLANTICO);
            solicitudCatastral.setTerritorial(ATLANTICO);
            usuarioPruebas.add(usuarioPruebasReclamar);

            solicitudCatastral.setUsuarios(usuarioPruebas);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            ///////////////////// Avanzar actividad por id de proceso //////////////////////////
            solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            solicitudCatastral
                .setObservaciones("Actividad instanciada para pruebas");
            solicitudCatastral.setUsuarios(usuarioPruebas);
            procesoService.avanzarActividadPorIdProceso(id, solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService
                .obtenerActividadesProceso(id);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 1);
            assertEquals(actividades.get(0).getNombre(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            Thread.sleep(TIMER);
            ////////////////////////// Transferir actividad ////////////////////////////
            String idActividad = actividades.get(0).getId(); //Identificador de la actividad
            UsuarioDTO usuario = generalesService.getCacheUsuario("anyel.guzman"); //Usuario al que se transfiere la actividad
            procesoService.transferirActividadConservacion(idActividad, usuario);

            ////////////////////////// Verificación del usuario de la actividad /////////////////////
            Thread.sleep(TIMER);
            actividades = procesoService
                .obtenerActividadesProceso(id);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 1);
            assertEquals(actividades.get(0).getNombre(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testTransferirActividadMasiva() {
        LOGGER.debug("testTransferirActividadMasiva");

        try {
            List<String> idsActividades = new ArrayList<String>();
            for (int i = 0; i < 2; ++i) {
                Random random = new Random();
                SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
                solicitudCatastral.setIdentificador(random.nextInt(1452124));
                solicitudCatastral.setNumeroPredial(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setObservaciones("Ninguna");
                solicitudCatastral
                    .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

                List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
                UsuarioDTO usuarioPruebasReclamar = generalesService
                    .getCacheUsuario("alejandro.sanchez");
                usuarioPruebasReclamar.setDescripcionTerritorial(ATLANTICO);
                solicitudCatastral.setTerritorial(ATLANTICO);
                usuarioPruebas.add(usuarioPruebasReclamar);

                solicitudCatastral.setUsuarios(usuarioPruebas);
                solicitudCatastral.setTipoTramite("Mutación 1");
                String id = procesoService.crearProceso(solicitudCatastral);
                Thread.sleep(TIMER);
                // /////////////////// Avanzar actividad por id de proceso
                // //////////////////////////
                solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral
                    .setTransicion(
                        ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
                solicitudCatastral
                    .setObservaciones("Actividad instanciada para pruebas");
                solicitudCatastral.setUsuarios(usuarioPruebas);
                procesoService.avanzarActividadPorIdProceso(id,
                    solicitudCatastral);
                Thread.sleep(TIMER);
                List<Actividad> actividades = procesoService
                    .obtenerActividadesProceso(id);
                assertNotNull(actividades);
                assertEquals(actividades.size(), 1);
                assertEquals(
                    actividades.get(0).getNombre(),
                    ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
                Thread.sleep(TIMER);
                idsActividades.add(actividades.get(0).getId());
            }
            // //////////////////////// Transferir actividad
            // ////////////////////////////
            UsuarioDTO usuario = generalesService
                .getCacheUsuario("anyel.guzman"); // Usuario al que se
            // transfiere la
            // actividad
            procesoService.transferirActividadConservacion(idsActividades,
                usuario);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testTransferirActividadMasivaControlarCalidadEscaneo() {
        LOGGER.debug("testTransferirActividadMasiva");

        try {
            List<String> idsActividades = new ArrayList<String>();
            for (int i = 0; i < 50; ++i) {
                Random random = new Random();
                SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
                solicitudCatastral.setIdentificador(random.nextInt(1452124));
                solicitudCatastral.setNumeroPredial(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                    .nextInt(1452124)));
                solicitudCatastral.setObservaciones("Ninguna");
                solicitudCatastral
                    .setTransicion(
                        ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO);
                solicitudCatastral.setTerritorial(ATLANTICO);
                solicitudCatastral.setRol(
                    "CN=Control_Digitalizacion,OU=SIGC,OU=APLICACIONES,DC=DCIGAC,DC=LOCAL");
                solicitudCatastral.setTipoTramite("Mutación 1");
                String id = procesoService.crearProceso(solicitudCatastral);
                Thread.sleep(TIMER);
                List<Actividad> actividades = procesoService
                    .obtenerActividadesProceso(id);
                if (actividades != null && !actividades.isEmpty()) {
                    idsActividades.add(actividades.get(0).getId());
                }
            }
            // //////////////////////// Transferir actividad
            // ////////////////////////////
            UsuarioDTO usuario = generalesService
                .getCacheUsuario("alejandro.sanchez"); // Usuario al que se
            // transfiere la
            // actividad
            long currentTimeMillis = System.currentTimeMillis();
            procesoService.transferirActividadConservacion(idsActividades,
                usuario);
            LOGGER.debug("Tiempo de transferencia(s): " +
                ((System.currentTimeMillis() - currentTimeMillis)) / 1000);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testObtenerEjecutorActividad() {
        LOGGER.debug("testObtenerEjecutorActividad");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarioPruebasReclamar = new UsuarioDTO();
            usuarioPruebasReclamar.setLogin("alejandro.sanchez");
            usuarioPruebasReclamar.setDescripcionTerritorial("CUNDINAMARCA");
            usuarioPruebas.add(usuarioPruebasReclamar);

            solicitudCatastral.setUsuarios(usuarioPruebas);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            ///////////////////// Avanzar actividad por id de proceso //////////////////////////
            solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            solicitudCatastral
                .setObservaciones("Actividad instanciada para pruebas");
            solicitudCatastral.setUsuarios(usuarioPruebas);
            procesoService.avanzarActividadPorIdProceso(id, solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService
                .obtenerActividadesProceso(id);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 1);
            assertEquals(actividades.get(0).getNombre(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);

            ////////////////////////// Obtener usuario actividad /////////////////////
            Thread.sleep(TIMER);
            SolicitudCatastral objNegocio = procesoService
                .obtenerObjetoNegocioConservacion(id,
                    ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);
            assertNotNull(objNegocio);
            assertEquals(objNegocio.getUsuarios().size(), 1);
            UsuarioDTO usuario = objNegocio.getUsuarios().get(0);
            assertEquals(usuario.getLogin(), "alejandro.sanchez");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testObtenerFlujoProceso() {
        LOGGER.debug("testObtenerFlujoProceso");
        try {
            Random random = new Random();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setIdentificador(random.nextInt(1452124));
            solicitudCatastral.setNumeroPredial(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroRadicacion(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setNumeroSolicitud(Integer.toString(random
                .nextInt(1452124)));
            solicitudCatastral.setObservaciones("Ninguna");
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            UsuarioDTO usuarioPruebasReclamar = new UsuarioDTO();
            usuarioPruebasReclamar.setLogin("alejandro.sanchez");
            usuarioPruebasReclamar.setDescripcionTerritorial("CUNDINAMARCA");
            usuarioPruebas.add(usuarioPruebasReclamar);

            solicitudCatastral.setUsuarios(usuarioPruebas);
            solicitudCatastral.setTipoTramite("Mutación 1");
            String id = procesoService.crearProceso(solicitudCatastral);
            Thread.sleep(TIMER);
            ///////////////////// Avanzar actividad por id de proceso //////////////////////////
            solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
            solicitudCatastral
                .setObservaciones("Actividad instanciada para pruebas");
            solicitudCatastral.setUsuarios(usuarioPruebas);
            procesoService.avanzarActividadPorIdProceso(id, solicitudCatastral);
            Thread.sleep(TIMER);
            List<Actividad> actividades = procesoService
                .obtenerActividadesProceso(id);
            assertNotNull(actividades);
            assertEquals(actividades.size(), 1);
            assertEquals(actividades.get(0).getNombre(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);

            ////////////////////////// Obtener usuario actividad /////////////////////
            Thread.sleep(TIMER);

            String urlImagenGrafo = procesoService.obtenerFlujoEjecucionProceso(actividades.get(0).
                getId());
            assertNotNull(urlImagenGrafo);
            LOGGER.debug("urlImagenGrafo:" + urlImagenGrafo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }

    /*
     * @Test public void testObtenerFlujoProcesoFinalizado() {
     * LOGGER.debug("testObtenerFlujoProceso"); try { Random random = new Random();
     * SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
     * solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
     * solicitudCatastral.setIdentificador(random.nextInt(1452124));
     * solicitudCatastral.setNumeroPredial(Integer.toString(random .nextInt(1452124)));
     * solicitudCatastral.setNumeroRadicacion(Integer.toString(random .nextInt(1452124)));
     * solicitudCatastral.setNumeroSolicitud(Integer.toString(random .nextInt(1452124)));
     * solicitudCatastral.setObservaciones("Ninguna"); solicitudCatastral
     * .setTransicion(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);
     *
     * List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>(); UsuarioDTO
     * usuarioPruebasReclamar = new UsuarioDTO();
     * usuarioPruebasReclamar.setLogin("alejandro.sanchez");
     * usuarioPruebasReclamar.setDescripcionTerritorial("CUNDINAMARCA");
     * usuarioPruebas.add(usuarioPruebasReclamar);
     *
     * solicitudCatastral.setUsuarios(usuarioPruebas); solicitudCatastral.setTipoTramite("Mutación
     * 1"); String id = procesoService.crearProceso(solicitudCatastral); Thread.sleep(TIMER);
     * ///////////////////// Avanzar actividad por id de proceso //////////////////////////
     * solicitudCatastral = new SolicitudCatastral(); solicitudCatastral
     * .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
     * solicitudCatastral .setObservaciones("Actividad instanciada para pruebas");
     * solicitudCatastral.setUsuarios(usuarioPruebas);
     * procesoService.avanzarActividadPorIdProceso(id, solicitudCatastral); Thread.sleep(TIMER);
     * List<Actividad> actividades = procesoService .obtenerActividadesProceso(id);
     * assertNotNull(actividades); assertEquals(actividades.size(), 1);
     * assertEquals(actividades.get(0).getNombre(),
     * ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
     *
     * ////////////////////////// Obtener usuario actividad /////////////////////
     * Thread.sleep(TIMER);
     *
     * List<Actividad> actividades2 =
     * procesoService.obtenerActividadesProceso("_PI:9003013f.c067936a.6efdee53.f863fdc9");
     * actividades2 = procesoService.getActividadesPorIdObjetoNegocio(23780L); LOGGER.info("Lista de
     * actividades: " + actividades2.size());
     *
     * PeticionObtenerFlujoProceso flujoProceso = new PeticionObtenerFlujoProceso();
     * flujoProceso.setTipoPeticion(ETipoPeticion.PROCESO.getTipo());
     * flujoProceso.setIdObjeto("_PI:9003013f.c067936a.6efdee53.f863fdc9");
     * flujoProceso.setMapaProceso(ProcesoDeConservacion.MAPA_PROCESO); LOGGER.info(
     * procesoService.obtenerImagenFlujoProceso(flujoProceso,"_PI:9003013f.c067936a.6efdee53.f863fdc9"));
     * LOGGER.info("Lista de actividades: " + actividades2.size());
     *
     * } catch (Exception e) { LOGGER.error(e.getMessage(), e); // test fail fail(e.getMessage(),
     * e); } }
     */
    @Test
    public void testListaActividadesConFiltro() {
        LOGGER.debug("Test consulta tareas (Lista actividades)");

        try {
            Map<EParametrosConsultaActividades, String> filtros =
                new HashMap<EParametrosConsultaActividades, String>();
            //secretaria = generalesService.getCacheUsuario("pruatlantico2");
            //LOGGER.info(secretaria.getDescripcionTerritorial());
            filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION, "0800100010282014");
            filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, "39098");
            filtros.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, "jaime.mejia");
            List<Actividad> actividades = procesoService.consultarListaActividades(filtros);

            //filtros.put( EParametrosConsultaActividades.TERRITORIAL, secretaria.getDescripcionEstructuraOrganizacional());
            //filtros.put( EParametrosConsultaActividades.ESTADOS, "2,8");
            //filtros.put( EParametrosConsultaActividades.PROPIETARIO_ACTIVIDAD, "pruatlantico2");
            /*
             *
             * LOGGER.info("Actividades: " + actividades.size());
             */
            for (Actividad actividad : actividades) {
                LOGGER.info(actividad.getIdObjetoNegocio() + ", " +
                    actividad.getNombre() + ", " +
                    actividad.getRoles().toString().replaceAll("\\[|\\]", "") +
                    actividad.getUsuarioEjecutor());

                // Id de solicitud
                String solicitud = actividad.getNumeroSolicitud();
                LOGGER.debug(solicitud);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }

    }

    @Test
    public void testSuspenderActividadesJavier() {

        try {

            Calendar nuevaFecha = Calendar.getInstance();
            nuevaFecha.set(2015, 2, 25);

            Date fecha = nuevaFecha.getTime();

            procesoService.
                suspenderActividad("_TKI:a01b014c.8f878c55.d8fdee53.5146e9d9", nuevaFecha,
                    "Por suspensión o aplazamiento de la comisión");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
