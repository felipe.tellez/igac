package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudAvisoRegistro;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInfoAdicional;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EDatoRectificarTipo;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.vo.ResultadoVO;

/**
 *
 * Clase con las pruebas de integracion para la interaz ITramite
 *
 * @author juan.agudelo
 *
 * OJO: si al ejecutar una prueba sale un error porque no encuentra el applicationContext.xml, puede
 * ser porque no lo encuentra en el build => hacer build o mvn test-compile
 *
 * OJO: quitar la opción de build automatically
 *
 */
public class TramiteServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteServiceFacadeTest.class);

    // -------------------------------------------------------------------------------------------------
    /**
     * @author felipe.cadena
     */

    /*
     * @lorena.salamanca :: 17-12-2015 :: SIGDAOv2: Esta prueba comprende los servicios
     * obtenerZonasHomogeneas y obtenerZonasAsync obtenerZonasAsync :: Enviar un tramite masivo que
     * tenga replica y que el ppredio tenga cancela inscribe en 'i' o 'm' SELECT t.id FROM Documento
     * d join tramite t on t.id = d.TRAMITE_id join p_predio pp on t.id = pp.TRAMITE_id WHERE
     * d.TIPO_DOCUMENTO_ID = 98 and t.RADICACION_ESPECIAL is not null and pp.CANCELA_INSCRIBE is not
     * null;
     *
     * obtenerZonasHomogeneas :: Enviar un tramite no masivo que tenga replica y que el ppredio
     * tenga cancela inscribe en 'i' o 'm' SELECT t.id FROM Documento d join tramite t on t.id =
     * d.TRAMITE_id join p_predio pp on t.id = pp.TRAMITE_id WHERE d.TIPO_DOCUMENTO_ID = 98 and
     * t.RADICACION_ESPECIAL is null and pp.CANCELA_INSCRIBE is not null;
     */
    @Test
    public void testActualizacionPPredioZonas() {
        LOGGER.debug("integration test TramiteServiceFacadeTest#testActualizacionPPredioZonas ...");

        Tramite tramite = this.tramiteService.buscarTramitePorId(276900L);
        UsuarioDTO user = this.generalesService.getCacheUsuario("pruatlantico16");

        boolean result = this.tramiteService.actualizacionPPredioZonas(tramite, user, Boolean.FALSE);

        assertNotNull(result);
        LOGGER.debug("Resultado : " + result);

    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testConsultarDocumentosTramite() {
        LOGGER.debug("on TramiteServiceFacadeTest#testConsultarDocumentosTramite");

        List<TramiteDocumentacion> documentosTramite;

        try {
            documentosTramite = this.tramiteService.buscarDocumentosTramitePorNumeroDeRadicacion(
                "02-560-825-2009");
            assertNotNull(documentosTramite);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author jamir.avila
     */
    @Test
    public void testBuscarSolicitudes() {
        LOGGER.debug("TramiteServiceFacadeTest#testBuscarSolicitudes");

        FiltroDatosSolicitud filtroDatosSolicitud = new FiltroDatosSolicitud();
        int desde = 0;
        int cantidad = 5;
        List<Solicitud> solicitudes = tramiteService.buscarSolicitudes(filtroDatosSolicitud, desde,
            cantidad);

        Assert.assertNotNull(solicitudes);
        Assert.assertEquals(true, solicitudes.size() <= cantidad,
            "Fallo en el número de registros recuperados.");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarTramiteTramitesPorTramiteId() {

        LOGGER.debug("TramiteServiceFacadeTest#testBuscarTramiteTramitesPorTramiteId");

        Long tramiteId = 81L;
        Tramite tramiteT = new Tramite();
        Tramite tramite = new Tramite();
        LOGGER.debug("************************************************");
        try {
            tramiteT = tramiteService.buscarTramiteTramitesPorTramiteId(tramiteId);

            LOGGER.debug("Tramite tramite numero radicacion: " + tramiteT.getNumeroRadicacion());
            LOGGER.debug("Tramite tramite documentos tamaño: " +
                tramiteT.getTramiteDocumentacions().size());
            if (tramite.getTramite() != null) {
                LOGGER.debug("Tramite tramite numero id: " + tramiteT.getTramite().getId());
            }

            if (tramiteT.getTramiteDocumentacions().size() != 0) {
                LOGGER.debug("Tramite tramite documentos: " +
                    tramiteT.getTramiteDocumentacions().get(0).getTipoDocumento().getNombre());
                if (tramiteT.getTramiteDocumentacions().get(0).getDepartamento() != null) {
                    LOGGER.debug("Tramite tramite documentos departamento: " +
                        tramiteT.getTramiteDocumentacions().get(0).getDepartamento().getNombre());
                }
                if (tramiteT.getTramiteDocumentacions().get(0).getMunicipio() != null) {
                    LOGGER.debug("Tramite tramite documentos municipio: " +
                        tramiteT.getTramiteDocumentacions().get(0).getMunicipio().getNombre());
                }
            }
            if (tramiteT.getPredio() != null) {
                LOGGER.debug("Tramite tramite predio numero predial: " + tramiteT.getPredio().
                    getNumeroPredial());
            }

            Assert.assertNotNull(tramiteT);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail(e.getMessage(), e);

        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testBuscarSolicitantePorTipoYNumeroDeDocumento() {
        LOGGER.debug("testBuscarSolicitantePorTipoYNumeroDeDocumento");
        try {
            List<Solicitante> solicitantes = this.tramiteService.buscarTodosSolicitantes(0, 1);
            if (!solicitantes.isEmpty()) {
                Solicitante sol = solicitantes.get(0);
                sol = this.tramiteService.buscarSolicitantePorTipoYNumeroDeDocumento(sol.
                    getNumeroIdentificacion(),
                    sol.getTipoIdentificacion());
                assertNotNull(sol);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGuardarActualizarSolicitante() {
        LOGGER.debug("testGuardarActualizarSolicitante");
        List<Solicitante> solicitantes = this.tramiteService.buscarTodosSolicitantes(0, 1);
        Solicitante solicitante1 = solicitantes.get(0);
        try {
            solicitante1.setFechaLog(new Date());
            solicitante1.setUsuarioLog("fabio.navarrete" + new Date());
            solicitante1 = this.tramiteService.guardarActualizarSolicitante(solicitante1);
            Assert.assertNotNull(solicitante1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    /*
     * @Test public void testGuardarActualizarSolicitanteRelacion(){
     * LOGGER.debug("testGuardarActualizarSolicitanteRelacion");
     *
     * SolicitanteRelacion solicitanteRelacion = new SolicitanteRelacion();
     *
     * Solicitante solicitante1 = new Solicitante(); solicitante1.setTipoSolicitante("1");
     * solicitante1.setNumeroIdentificacion("045"); solicitante1.setTipoIdentificacion("NIT");
     * solicitante1.setTipoPersona("JURIDICA"); solicitante1.setId(801L);
     * solicitante1.setFechaLog(new Date()); solicitante1.setUsuarioLog("fabio.navarrete.test");
     *
     * Solicitante solicitante2 = new Solicitante(); solicitante2.setTipoSolicitante("1");
     * solicitante2.setNumeroIdentificacion("0009"); solicitante2.setTipoIdentificacion("CC");
     * solicitante2.setTipoPersona("NATURAL"); solicitante2.setFechaLog(new Date());
     * solicitante2.setId(40L); solicitante2.setUsuarioLog("fabio.navarrete.test");
     *
     * solicitanteRelacion.setSolicitanteBySolicitanteId(solicitante1);
     * solicitanteRelacion.setSolicitanteBySolicitanteRelacionadoId(solicitante2);
     * solicitanteRelacion.setFechaDesde(new Date());
     * solicitanteRelacion.setTipoRelacion(Constantes.REPRESENTANTE_LEGAL);
     * solicitanteRelacion.setFechaLog(new Date());
     * solicitanteRelacion.setUsuarioLog("fabio.navarrete.test");
     *
     * try { SolicitanteRelacion success =
     * this.tramiteService.guardarActualizarSolicitanteRelacion(solicitanteRelacion) ;
     * Assert.assertNotNull(success); }catch (Exception e) { LOGGER.error(e.getMessage(), e);
     * fail(e.getMessage(), e); }
     *
     *
     * // Assert.assertTrue(true); }
     */
    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testConsultarRadicadoCorrespondencia() {
        LOGGER.debug("testConsultarRadicadoCorrespondencia");
        try {
            String numeroRadicacionCorrespondencia = "8002011ER00000015";
            UsuarioDTO usuario = this.RADICADOR;
            Solicitud resultado = this.tramiteService.obtenerSolicitudDeCorrespondencia(
                numeroRadicacionCorrespondencia,
                usuario);
            Assert.assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testConsultarRadicadoCorrespondenciaError() {
        LOGGER.debug("testConsultarRadicadoCorrespondencia");
        try {
            String numeroRadicacionCorrespondencia = "";
            UsuarioDTO usuario = this.RADICADOR;
            Solicitud resultado = this.tramiteService.obtenerSolicitudDeCorrespondencia(
                numeroRadicacionCorrespondencia,
                usuario);
            Assert.assertNull(resultado);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testObtenerSolicitudAvaluosCorrespondencia() {
        LOGGER.debug("testConsultarRadicadoCorrespondencia");
        try {
            String numeroRadicacionCorrespondencia = "8002011ER00000015";
            UsuarioDTO usuario = this.RADICADOR;
            Solicitud resultado = this.tramiteService
                .obtenerSolicitudAvaluosCorrespondencia(numeroRadicacionCorrespondencia, usuario);
            Assert.assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testObtenerSolicitudAvaluosCorrespondenciaError() {
        LOGGER.debug("testConsultarRadicadoCorrespondencia");
        try {
            String numeroRadicacionCorrespondencia = "";
            UsuarioDTO usuario = this.RADICADOR;
            Solicitud resultado = this.tramiteService
                .obtenerSolicitudAvaluosCorrespondencia(numeroRadicacionCorrespondencia, usuario);
            Assert.assertNull(resultado);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testContarTramitesDeEjecutorAsignado() {
        LOGGER.debug(
            "test integración: TramiteServiceFacadeTest#testContarTramitesDeEjecutorAsignado");

        String funcionarioEjecutor = "SENIOR2";
        // FUNCIONARIO CON TRAMITES ASIGNADOS

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer tramitesEjecutor = tramiteService.contarTramitesDeEjecutorAsignado(
                funcionarioEjecutor);
            Assert.assertNotNull(tramitesEjecutor);
            LOGGER.debug("**************** TERMINAMOS ***********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fredy.wilches
     */
    @Test
    public void testCrearSolicitud() {
        LOGGER.debug("test integración: TramiteServiceFacadeTest#testCrearSolicitud");
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("pruatlantico2");
        usuario.setDescripcionTerritorial("ATLANTICO");
        usuario.setCodigoTerritorial("6040");
        try {
            Solicitud s = tramiteService.findSolicitudPorId(new Long(802));
            s.setId(null);

            // TODO::fredy.wilches::02-08-2011::revisar líneas comentadas.::pedro,garcia
            // s.setFolios((short)4);
            // s.setAnexos((short)4);
            s.setSistemaEnvio("1");
            s.setNumero(null);
            s.setTramites(new ArrayList<Tramite>());
            Tramite t = new Tramite();
            Departamento d = new Departamento();
            d.setCodigo("25");
            Municipio m = new Municipio();
            m.setCodigo("25754");
            t.setDepartamento(d);
            t.setMunicipio(m);
            t.setTipoTramite("1"); // Mutacion
            s.getTramites().add(t);
            List<SolicitanteSolicitud> solicitantes = tramiteService.obtenerSolicitantesSolicitud(
                136L);
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);
                ss.setSolicitud(s);
            }

            s.setSolicitanteSolicituds(solicitantes);

            // Solicitud s=(Solicitud)(Serializador.desSerializar(new
            // File("c:/basura/solicitud.obj")));
            // s=tramiteService.crearSolicitud(usuario, s);
            Assert.assertFalse(false);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Test del método que trae una solicitud del sistema, si no la encuentra busca en
     * correspondencia.
     *
     * @author fabio.navarrete
     */
    @Test
    public void testObtenerSolicitudInternaCorrespondencia() {
        try {
            String numeroSolicitud = "8002008ER450";
            UsuarioDTO usuario = this.RADICADOR;
            Solicitud sol = this.tramiteService.obtenerSolicitudInternaCorrespondencia(
                numeroSolicitud, usuario);

            String numeroSolicitud1 = "8002011ER00000037";
            Solicitud sol1 = this.tramiteService.obtenerSolicitudInternaCorrespondencia(
                numeroSolicitud1, usuario);

            Assert.assertNotNull(sol);
            Assert.assertNotNull(sol1);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerInfoAvisosRechazadosPorSolicitud() {

        LOGGER.debug(
            "integration test TramiteServiceFacadeTest#testObtenerInfoAvisosRechazadosPorSolicitud...");
        String numeroSolicitud;
        List<AvisoRegistroRechazo> list1;

        numeroSolicitud = "769457567";

        try {
            list1 = this.tramiteService.obtenerInfoAvisosRechazadosPorSolicitud(numeroSolicitud);
            LOGGER.debug("fetched " + list1.size() + " rows");

            Assert.assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de archivar trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testArchivarTramite() {

        LOGGER.debug("integration test TramiteServiceFacadeTest#testArchivarTramite");
        Tramite tramite = new Tramite();
        UsuarioDTO usuario = new UsuarioDTO();
        Boolean resultadoT = null;

        usuario.setLogin("juan.agudelo.test");
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramite = this.tramiteService.buscarTramiteTramitesPorTramiteId(7L);
            resultadoT = this.tramiteService.archivarTramite(usuario, tramite);
            Assert.assertNotNull(resultadoT);
            LOGGER.debug("Actualización: " + resultadoT);

            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda completa de un trámite por id
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarTramiteSolicitudPorId() {
        LOGGER.debug("integration test TramiteServiceFacadeTest#testBuscarTramiteSolicitudPorId");

        Tramite tramiteResultado = new Tramite();
        Long tramiteId = 97L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            tramiteResultado = this.tramiteService.buscarTramiteSolicitudPorId(tramiteId);

            LOGGER.debug("Número predial: " + tramiteResultado.getPredio().getNumeroPredial());
            LOGGER.debug(
                "Nº radicación definitivo: " + tramiteResultado.getSolicitud().
                    getNumeroRadicacionDefinitiva());
            LOGGER.debug("Nº solicitantes: " + tramiteResultado.getSolicitanteTramites().size());
            if (tramiteResultado.getSolicitanteTramites().size() > 0) {
                LOGGER.debug("Solicitante: " + tramiteResultado.getSolicitanteTramites().get(0).
                    getNombreCompleto());
            }
            if (tramiteResultado.getTramiteEstados().size() > 0) {
                LOGGER.
                    debug("TramiteEstados tamaño: " + tramiteResultado.getTramiteEstados().size());
            }
            if (tramiteResultado.getTramitePredioEnglobes().size() > 0) {
                LOGGER.debug("TPredioEnglobe tamaño: " +
                    tramiteResultado.getTramitePredioEnglobes().size());
            }
            Assert.assertNotNull(tramiteResultado);
            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assert.fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia pre: hay datos de prueba en la tabla TRAMITE que corresponden a los
     * usados en esta prueba
     */
    @Test
    public void testBuscarTramitesPorComisionar() {

        LOGGER.
            debug("integration test TramiteServiceFacadeTest#testBuscarTramitesPorComisionar ...");

        List<Tramite> tramites = null;
        String idTerritorial;

        idTerritorial = "6360";
        try {
            // N: este método ya no existe
            // tramites = this.tramiteService.buscarTramitesPorComisionar(idTerritorial);

            LOGGER.debug("... passed");

            if (!tramites.isEmpty()) {
                LOGGER.debug("municipio = " + tramites.get(0).getMunicipio().getNombre());
                for (Tramite tramite : tramites) {
                    LOGGER.debug("trámite id = " + tramite.getId());
                }
            }
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Implementación del test para la función BuscarSolicitudPorFiltros
     *
     * @author fabio.navarrete
     */
    @Test
    public void testBuscarSolicitudPorFiltros() {
        try {
            VSolicitudPredio filtro = new VSolicitudPredio();
            filtro.setNumeroIdentificacion("764925");
            List<VSolicitudPredio> solicitudes = this.tramiteService.buscarSolicitudPorFiltros(
                filtro, 0, 0);
            Assert.assertNotNull(solicitudes);
            Assert.assertTrue(solicitudes.size() > 0);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testDescartarAviso() {

        try {
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("fabio.navarrete.TramiteServiceFacadeTest");
            usuario.setDescripcionTerritorial("ATLANTICO");
            usuario.setCodigoTerritorial("6040");

            // Definición de predios
            Predio predio1 = this.conservacionService.obtenerPredioConAvaluosPorId(Long.valueOf(7));
            Predio predio2 = this.conservacionService.obtenerPredioConAvaluosPorId(Long.valueOf(
                157633));
            List<Predio> predios = new ArrayList<Predio>();
            predios.add(predio1);
            predios.add(predio2);

            // Definición de SolicitudAvisoRegsitro
            CirculoRegistral circRegistral = this.generalesService.
                getCacheCirculosRegistralesPorMunicipio("15238")
                .get(0);
            Solicitud solicitud = this.tramiteService.obtenerSolicitudInternaCorrespondencia(
                "666999", usuario);
            if (solicitud.getSolicitudAvisoRegistro() != null) {
                this.tramiteService.eliminarSolicitudAvisoRegistro(solicitud.
                    getSolicitudAvisoRegistro());
            }
            SolicitudAvisoRegistro solAvisoRegistro = new SolicitudAvisoRegistro();
            solAvisoRegistro.setSolicitud(solicitud);
            solAvisoRegistro.setFechaLog(new Date());
            solAvisoRegistro.setCirculoRegistral(circRegistral);
            solAvisoRegistro.setAvisosTotal(10);
            solAvisoRegistro.setAvisosAceptados(0);
            solAvisoRegistro.setAvisosRechazados(0);
            solAvisoRegistro.setUsuarioLog(usuario.getLogin());

            solicitud.setSolicitudAvisoRegistro(solAvisoRegistro);

            List<AvisoRegistroRechazoPredio> avisoRegistroRechazoPredios =
                new ArrayList<AvisoRegistroRechazoPredio>();

            AvisoRegistroRechazo avisoRechazo = new AvisoRegistroRechazo();
            avisoRechazo.setMotivoNoProcede("Motivo1");
            avisoRechazo.setObservaciones("OBSERVACIONES TEST");

            for (Predio pred : predios) {
                AvisoRegistroRechazoPredio avisoRegistroRechazoPredio =
                    new AvisoRegistroRechazoPredio();
                avisoRegistroRechazoPredio.setAvisoRegistroRechazo(avisoRechazo);
                avisoRegistroRechazoPredio.setNumeroPredial(pred.getNumeroPredial());
                avisoRegistroRechazoPredio.setPredio(pred);
                avisoRegistroRechazoPredio.setFechaLog(new Date());
                avisoRegistroRechazoPredio.setUsuarioLog(usuario.getLogin());
                avisoRegistroRechazoPredios.add(avisoRegistroRechazoPredio);
            }
            avisoRechazo.setSolicitudAvisoRegistro(solicitud.getSolicitudAvisoRegistro());
            avisoRechazo.setAvisoRegistroRechazoPredios(avisoRegistroRechazoPredios);
            avisoRechazo.setUsuarioLog(usuario.getLogin());
            avisoRechazo.setFechaLog(new Date());
            // this.tramiteService.guardarActualizarAvisoRegistroRechazo(this.usuario,
            // this.avisoRechazo);
            Integer numRechazos = solicitud.getSolicitudAvisoRegistro().getAvisosRechazados();
            solicitud.getSolicitudAvisoRegistro().setAvisosRechazados(++numRechazos);
            solicitud.getSolicitudAvisoRegistro().getAvisoRegistroRechazos().add(avisoRechazo);
            // this.tramiteService.guardarSolicitudAvisoRegistro(this.solicitudSeleccionada.getSolicitudAvisoRegistro());
            for (Tramite tramite : solicitud.getTramites()) {
                if (tramite.getFechaRadicacion() == null) {
                    tramite.setFechaRadicacion(new Date());
                }
            }

            this.tramiteService.guardarActualizarSolicitud(usuario, solicitud, null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testConseguirDocumentacionTramiteRequerida() {
        LOGGER.debug("TramiteServiceFacadeTest#testConseguirDocumentacionTramiteRequerida ...");

        Tramite tramiteTemp = new Tramite();
        tramiteTemp.setTipoTramite(ETramiteTipoTramite.MUTACION.getCodigo());
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<TramiteRequisito> tr = this.tramiteService.conseguirDocumentacionTramiteRequerida(
                tramiteTemp);

            for (int i = 0; i < tr.size(); i++) {
                LOGGER.debug("Documentos requeridos ", tr.get(i).getTramiteRequisitoDocumentos().
                    size());
                for (int j = 0; j < tr.get(i).getTramiteRequisitoDocumentos().size(); j++) {
                    if (tr.get(i).getTramiteRequisitoDocumentos().get(j).getTipoDocumento() != null) {
                        LOGGER.debug("Documento: " +
                            tr.get(i).getTramiteRequisitoDocumentos().get(j).getTipoDocumento().
                                getNombre());
                    }
                }
            }
            assertNotNull(tr);
            LOGGER.debug("*************** TERMINAMOS ************************");
        } catch (Exception e) {
            LOGGER.error("... error: " + e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios simples por tramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindTramitePrediosSimplesByTramiteId() {
        LOGGER.debug("TramiteServiceFacadeTest#testFindTramitePrediosSimplesByTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Tramite answerT = this.tramiteService.findTramitePrediosSimplesByTramiteId(tramiteId);

            LOGGER.debug("Predio " + answerT.getPredio().getNumeroPredial());
            if (answerT.getTramitePredioEnglobes() != null && !answerT.getTramitePredioEnglobes().
                isEmpty()) {
                for (TramitePredioEnglobe tpe : answerT.getTramitePredioEnglobes()) {
                    LOGGER.debug("Números prediales " + tpe.getPredio().getNumeroPredial());
                }
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerSolicitsParaEnvioComunicNotif() {

        LOGGER.debug("testObtenerSolicitsParaEnvioComunicNotif ...");

        List<Solicitante> answer;
        long idTramite, idSolicitud;
        idTramite = 97l;
        idSolicitud = 62l;

        try {
            answer = this.tramiteService.
                obtenerSolicitsParaEnvioComunicNotif(idTramite, idSolicitud, 3);

            LOGGER.debug("passed!. fetched " + answer.size() + " rows");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(
                "error on TramiteServiceFacadeTest#testObtenerSolicitsParaEnvioComunicNotif: " + e.
                    getMessage());
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.mendez
     */
    @Test
    public void testObtenerTareasGeograficas() {
        LOGGER.debug("testObtenerTareasGeograficas");
        try {
            String login = "prumagdalena03";
            LOGGER.debug("login:" + login);
            UsuarioDTO user = this.generalesService.getCacheUsuario(login);
            assertNotNull(user);
            ResultadoVO resultado = this.tramiteService.obtenerTareasGeograficas(user);
            assertNotNull(resultado);
            assertNotNull(resultado.getActividades());
            assertTrue(resultado.getActividades().size() > 0);
            LOGGER.debug("El usuario no tiene asignadas tareas geográficas");
            LOGGER.debug(resultado.toString());
            LOGGER.debug("# Actividades:" + resultado.getActividades().size());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de las rutas en Alfresco de las imagenes incial y final del trámite
     *
     * @author juan.agudelo
     */
    @Test
    public void testRecuperarRutaAlfrescoImagenInicialYfinalDelTramite() {
        LOGGER.debug(
            "tests: TramiteServiceFacadeTest#testRecuperarRutaAlfrescoImagenInicialYfinalDelTramite");

        Long tramiteId = 2001L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            String[] answer = this.tramiteService.
                recuperarRutaAlfrescoImagenInicialYfinalDelTramite(tramiteId);

            if (answer != null) {

                LOGGER.debug("Ruta documento inicial: " + answer[0]);
                LOGGER.debug("Ruta documento final: " + answer[1]);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testBorrarComisionTramiteDatosPorComisionTramite() {

        LOGGER.debug(
            "integration test: " +
            "TramiteServiceFacadeTest#testBorrarComisionTramiteDatosPorComisionTramite ...");

        Long idComisionTramite = new Long(2034);

        try {
            this.tramiteService.borrarComisionTramiteDatosPorComisionTramite(idComisionTramite);
            LOGGER.debug("... passed!");
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(
                "error on TramiteServiceFacadeTest#testBorrarComisionTramiteDatosPorComisionTramite: " +
                e.getMessage());
            assertTrue(false);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia pre: el trámite -y sus asociados- tiene departamento, municipio y
     * solicitud
     */
    @Test
    public void testBuscarTramiteCompletoConResolucion() {

        LOGGER.debug("integration test: " +
            "TramiteServiceFacadeTest#testBuscarTramiteCompletoConResolucion ...");

        Long idTramite = new Long(136);
        Tramite answer = null;
        Documento docResol;

        try {
            answer = this.tramiteService.buscarTramiteCompletoConResolucion(idTramite);
            LOGGER.debug("... passed!");

            if (answer != null) {
                if (answer.getResultadoDocumento() != null) {
                    docResol = answer.getResultadoDocumento();
                    LOGGER.debug("doc id: " + docResol.getId());
                }
            }

            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(
                "error on TramiteServiceFacadeTest#testBuscarTramiteCompletoConResolucion: " + e.
                    getMessage());
            assertTrue(false);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetDocsOfTramite() {
        LOGGER.debug("test: TramiteServiceFacadeTest#testGetDocsOfTramite ...");

        Tramite tramite;
        Long tramiteId;
        @SuppressWarnings("unused")
        int i;
        tramiteId = 3l;

        try {

            LOGGER.debug("**************** ENTRAMOS *************************");
            tramite = this.tramiteService.getDocsOfTramite(tramiteId);
            LOGGER.debug("... passed!!!.");
            Assert.assertTrue(true);

            LOGGER.debug("solicitud data: " + tramite.getSolicitud().getNumero());

            if (!tramite.getTramiteDocumentacions().isEmpty()) {
                LOGGER.debug("fetched " + tramite.getTramiteDocumentacions().size() +
                    " documentacions");

                // D: solo tiene un documento relacionado si el campo 'aportado'
                // es SI
                i = 0;
                for (TramiteDocumentacion td : tramite.getTramiteDocumentacions()) {
                    if (td.getAportado().equals("SI")) {
                        LOGGER.debug("el doc del primer tramite docs: " + td.getDocumentoSoporte().
                            getArchivo());
                        break;
                    }
                }
            } else {
                LOGGER.debug("fetched no documentations");
            }

            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception e) {
            LOGGER.error("Error on unit test: TramiteDAOBeanTests#testGetDocsOfTramite: " + e.
                getMessage());
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de dato rectificación por tipo y naturaleza
     *
     * @author juan.agudelo
     * @version 2.0
     */
    @Test
    public void testObtenerDatosRectificarPorTipoYNaturaleza() {
        LOGGER.debug("tests: TramiteServiceFacadeTest#testObtenerDatosRectificarPorTipoYNaturaleza");

        String tipoDato = EDatoRectificarTipo.PREDIO.name();
        String naturaleza = EDatoRectificarNaturaleza.COMPLEMENTACION.geCodigo();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<DatoRectificar> answer = this.tramiteService.
                obtenerDatosRectificarPorTipoYNaturaleza(tipoDato,
                    naturaleza);

            if (answer != null && !answer.isEmpty()) {

                for (DatoRectificar dTemp : answer) {
                    LOGGER.debug("Dato " + dTemp.getNombre());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con solicitud por filtro de busqueda de trámite
     *
     * @author juan.agudelo
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testBuscarTramiteSolicitudByFiltroTramite() throws ParseException {
        LOGGER.debug("tests: TramiteServiceFacadeTest#testBuscarTramiteSolicitudByFiltroTramite");

        FiltroDatosConsultaTramite fb = cargarDatosBaseFiltroBusquedaTramite();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Tramite> answerT = this.tramiteService.buscarTramiteSolicitudPorFiltroTramite(fb);

            if (answerT != null && !answerT.isEmpty()) {

                LOGGER.debug("Tamaño " + answerT.size());

                for (Tramite t : answerT) {
                    LOGGER.debug("Número de radicación: " + t.getNumeroRadicacion());
                }
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba conteo de tramites con solicitud por filtro de busqueda de trámite
     *
     * @author juan.agudelo
     * @version 2.0
     * @throws ParseException
     */
    @Test
    public void testContarTramiteSolicitudPorFiltroTramite() throws ParseException {
        LOGGER.debug("tests: TramiteServiceFacadeTest#testContarTramiteSolicitudPorFiltroTramite");

        FiltroDatosConsultaTramite fb = cargarDatosBaseFiltroBusquedaTramite();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answerT = this.tramiteService.contarTramiteSolicitudPorFiltroTramite(fb);

            if (answerT != null) {
                LOGGER.debug("Tamaño " + answerT);
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los datos base de un filtro de busqueda de trámite
     *
     * @author juan.agudelo
     * @version 2.0
     * @throws ParseException
     */
    private FiltroDatosConsultaTramite cargarDatosBaseFiltroBusquedaTramite() throws ParseException {

        FiltroDatosConsultaTramite filtroDatosConsultaTramite = new FiltroDatosConsultaTramite();
        NumeroPredialPartes numeroPredialPartes = new NumeroPredialPartes();
        @SuppressWarnings("unused")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        /*
         * filtroDatosConsultaTramite.setNumeroSolicitud("8002011ER00002369");
         * filtroDatosConsultaTramite.setNumeroRadicacion("00002575400013672011");
         * filtroDatosConsultaTramite.setFechaSolicitud(sdf.parse("11/10/2011"));
         * filtroDatosConsultaTramite.setFechaRadicacion(sdf.parse("11/10/2011"));
         */
        filtroDatosConsultaTramite.setTipoSolicitud(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
        /*
         * filtroDatosConsultaTramite.setTipoTramite(ETramiteTipoTramite.MUTACION .getCodigo());
         * filtroDatosConsultaTramite.setClaseMutacion(EMutacionClase.SEGUNDA .getCodigo());
         * filtroDatosConsultaTramite.setSubtipo(EMutacion2Subtipo.DESENGLOBE .getCodigo());
         * filtroDatosConsultaTramite.setNumeroResolucion("123465789");
         * filtroDatosConsultaTramite.setFechaResolucion(sdf.parse("11/10/2011"));
         */

        // Persona natural
        // filtroDatosConsultaTramite.setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
        // filtroDatosConsultaTramite.setNumeroIdentificacion("1055");
        // persona jurídica
        /*
         * filtroDatosConsultaTramite .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT
         * .getCodigo()); filtroDatosConsultaTramite.setNumeroIdentificacion("909090909090");
         * filtroDatosConsultaTramite.setDigitoVerificacion("1");
         */
        // Número predial por partes
        /*
         * numeroPredialPartes.setDepartamentoCodigo("25");
         * numeroPredialPartes.setMunicipioCodigo("25754"); numeroPredialPartes.setTipoAvaluo("01");
         * numeroPredialPartes.setSectorCodigo("02"); numeroPredialPartes.setComunaCodigo("00");
         * numeroPredialPartes.setBarrioCodigo("00");
         * numeroPredialPartes.setManzanaVeredaCodigo("1165");
         * numeroPredialPartes.setPredio("0001"); numeroPredialPartes.setCondicionPropiedad("5");
         * numeroPredialPartes.setEdificioTorre("00"); numeroPredialPartes.setPiso("00");
         * numeroPredialPartes.setUnidad("0032");
         */
        filtroDatosConsultaTramite.setNumeroPredialPartes(numeroPredialPartes);

        return filtroDatosConsultaTramite;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda de solicitudes por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testCountSolicitudesPaginadasByFiltro() {
        LOGGER.debug("tests: TramiteServiceFacadeTest#testCountSolicitudesPaginadasByFiltro");

        VSolicitudPredio filtro = cargarDatosBaseFiltroSolicitudPredio();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answer = this.tramiteService.contarSolicitudesPaginadasPorFiltro(filtro);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método que crea un filtro de solicitud predio con los datos base
     */
    private VSolicitudPredio cargarDatosBaseFiltroSolicitudPredio() {

        VSolicitudPredio answer = new VSolicitudPredio();

        answer.setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
        answer.setNumeroIdentificacion("1055");

        return answer;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda de solicitudes por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindtSolicitudesPaginadasByFiltro() {
        LOGGER.debug("tests: TramiteServiceFacadeTest#testFindtSolicitudesPaginadasByFiltro");

        VSolicitudPredio filtro = cargarDatosBaseFiltroSolicitudPredio();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            int[] rowStartIdxAndCount = new int[2];
            rowStartIdxAndCount[0] = 1;
            rowStartIdxAndCount[1] = 10;

            List<VSolicitudPredio> answer = this.tramiteService.buscarSolicitudesPaginadasPorFiltro(
                filtro, null, null,
                null, rowStartIdxAndCount);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("Tamaño: " + answer.size());

                for (VSolicitudPredio vsTmp : answer) {
                    LOGGER.debug("Solicitud: " + vsTmp.getSolicitudId());
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testObtenerSolicitantesParaNotificarPorTramiteId() {
        LOGGER.debug(
            "test: TramiteServiceFacadeTest#testObtenerSolicitantesParaNotificarPorTramiteId");

        Long tramiteId = 35684L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Solicitante> answer = this.tramiteService.
                obtenerSolicitantesParaNotificarPorTramiteId(tramiteId);

            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("Tamaño " + answer.size());

                for (Solicitante sTmp : answer) {
                    LOGGER.debug("Nombre: " + sTmp.getNombreCompleto());
                }
            }

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerTramiteDocumentoParaRegistroNotificacion() {
        LOGGER.debug(
            "integration tests: TramiteServiceFacadeTest#testObtenerTramiteDocumentoParaRegistroNotificacion");

        long tramiteId;
        String documentoPersona;

        TramiteDocumento answer = null;

        tramiteId = 42289l;
        documentoPersona = "9999999";

        try {
            answer = this.tramiteService.obtenerTramiteDocumentoParaRegistroNotificacion(tramiteId,
                documentoPersona);

            LOGGER.debug("... passed.");
            if (answer != null) {
                LOGGER.debug("tramiteDocumento id = " + answer.getId());
            } else {
                LOGGER.debug("ocurrió algún error en el query");
            }
            assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error(
                "Error on unit test: TramiteServiceFacadeTest#testObtenerTramiteDocumentoParaRegistroNotificacion: " +
                ex.getMessage());
            assertFalse(true);

        }

    }

    // ---------------------------------------------------------------------------------------------
    @Test
    public void testUpdateAreasDePrediosEdicionGeografica() {
        LOGGER.debug(
            "integration tests: TramiteServiceFacadeTest#testUpdateAreasDePrediosEdicionGeografica");

        List<String> numerosPrediales = new ArrayList<String>();
        numerosPrediales.add("080010101000000640368900000000");
        // numerosPrediales.add("080010101000000280045000000000");
        List<Double> areas = new ArrayList<Double>();
        areas.add(5695.03362695188);
        // areas.add(20.0);
        Long tramiteId = 21416L;
        Tramite tramite = this.tramiteService.buscarTramitePorId(tramiteId);
        UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico18");

        boolean answer;

        try {
            answer = this.tramiteService.updateAreasDePrediosEdicionGeografica(numerosPrediales,
                areas, tramite,
                usuario, Boolean.FALSE);

            LOGGER.debug("... passed.");

            assertTrue(answer);
        } catch (Exception ex) {
            LOGGER.error(
                "Error on unit test: TramiteServiceFacadeTest#testObtenerTramiteDocumentoParaRegistroNotificacion: " +
                ex.getMessage());
            assertFalse(true);

        }

    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testRecalcularAvaluosCatastrales() {

        Long tramiteId = 1L;

        List<PPredio> predios = this.conservacionService.obtenerPPrediosPorTramiteIdActAvaluos(
            tramiteId);

        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("pruatlantico2");
        usuario.setDescripcionTerritorial("ATLANTICO");
        usuario.setCodigoTerritorial("6040");

        if (predios != null && !predios.isEmpty()) {
            for (PPredio pp : predios) {
                Object[] errores = this.formacionService.recalcularAvaluosDecretoParaUnPredio(
                    tramiteId, pp.getId(),
                    new Date(), usuario);

                if (Utilidades.hayErrorEnEjecucionSP(errores)) {
                    // return false;
                    LOGGER.debug("failed...");
                }
            }
            // return true;
            LOGGER.debug("passed...");
        } else {
            // return false;
            LOGGER.debug("failed...");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarTramitesDeEjecutorEnComision() {

        LOGGER.debug(
            "integration test TramiteServiceFacadeTest#testBuscarTramitesDeEjecutorEnComision ...");
        Long idComision = 1747L;
        String idFuncionarioEjecutor = "prutopo";
        List<Tramite> answer;

        LOGGER.debug("id comisión = " + idComision.toString() + " :: idEjecutor = " +
            idFuncionarioEjecutor);
        try {
            answer = this.tramiteService.buscarTramitesDeEjecutorEnComision(idFuncionarioEjecutor,
                idComision);
            LOGGER.debug("passed !!!!!!. found = " + answer.size());

            if (!answer.isEmpty()) {
                LOGGER.debug("data: " + answer.get(0).getDepartamento().getNombre());

                if (answer.get(0).getPredio() != null) {
                    LOGGER.debug("... tiene predio. num predial = " + answer.get(0).getPredio().
                        getNumeroPredial());
                }
            }
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Test para calcular la liquidación de avalúos de un predio
     */
    @Test
    public void testLiquidarAvaluosCatastrales() {

        Long tramiteId = 1L;

        List<PPredio> predios = this.conservacionService.obtenerPPrediosPorTramiteIdActAvaluos(
            tramiteId);

        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin("pruatlantico2");
        usuario.setDescripcionTerritorial("ATLANTICO");
        usuario.setCodigoTerritorial("6040");

        if (predios != null && !predios.isEmpty()) {
            for (PPredio pp : predios) {
                Object[] errores = this.formacionService.liquidarAvaluosParaUnPredio(tramiteId, pp.
                    getId(), new Date(),
                    usuario);

                if (Utilidades.hayErrorEnEjecucionSP(errores)) {
                    // return false;
                    LOGGER.debug("failed...");
                }
            }
            // return true;
            LOGGER.debug("passed...");
        } else {
            // return false;
            LOGGER.debug("failed...");
        }
    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testBuscarFuncionariosPorRolYTerritorial() {
        LOGGER.debug("on TramiteServiceFacadeTest#testBuscarFuncionariosPorRolYTerritorial");

        List<UsuarioDTO> answer;

        try {
            answer = this.tramiteService.buscarFuncionariosPorRolYTerritorial("ATLANTICO",
                ERol.DIGITALIZADOR);
            assertNotNull(answer);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testConsultarTramiteDevuelto() {

        Long idTramite;
        Tramite tramite;

        LOGGER.debug("integration test TramiteServiceFacadeTest#testConsultarTramiteDevuelto ...");

        idTramite = new Long("36750");

        tramite = this.tramiteService.consultarTramiteDevuelto(idTramite);
        assertNotNull(tramite);

        if (tramite.getComisionTramite() == null) {
            fail("No tiene relaciones con comisiones");
        } else {
            if (tramite.getComisionTramite().getComision() == null) {
                fail("No trajo la comisión");
            } else {
                LOGGER.debug("passed. Comisión id = " + tramite.getComisionTramite().getComision().
                    getId());
            }
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para probar la generación de resoluciones
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testGenerarResolucion() {

        LOGGER.debug("Integration test GeneralesServiceFacadeTest#testGenerarResolucion...");

        boolean resultado = true;

        try {
            this.tramiteService.buscarDocmentoFTP();

            LOGGER.debug("... passed!!!");
            assertTrue(resultado);
        } catch (Exception ex) {
            LOGGER.error(
                "Error en integration test GeneralesServiceFacadeTest#testProcesarRegistrosPredialesProductosPDF: " +
                ex.getMessage());
            fail();
            assertFalse(resultado);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author lorena.salamanca
     *
     * @lorena.salamanca :: 17-12-2015 :: SIGDAOv2: Esta prueba comprende el servicio
     * enviarJobEliminarVersion enviarJobEliminarVersion :: Enviar un tramite que tenga version en
     * SDE
     */
    @Test
    public void testCancelarTramite() {
        LOGGER.debug("integration test TramiteServiceFacadeTest#testCancelarTramite ...");

        Long tramiteId = 119680L;
        Tramite tramite = this.tramiteService.buscarTramitePorId(tramiteId);
        UsuarioDTO user = this.generalesService.getCacheUsuario("pruatlantico16");
        TramiteEstado selectedTramiteEstado = this.tramiteService.
            buscarTramiteEstadoVigentePorTramiteId(tramiteId);
        Documento documentoSoporteCancelacion = this.generalesService.buscarDocumentoPorId(566868L);

        boolean result = this.tramiteService.cancelarTramite(tramite, selectedTramiteEstado, user,
            documentoSoporteCancelacion);
        assertNotNull(result);

        LOGGER.debug("Resultado : " + result);

    }

    // --------------------------------------------------------------------------------------------------
    /*
     * @felipe.cadena :: 25-07-2016
     *
     */
    @Test
    public void testBuscarComisionesParaAdministrar() {
        LOGGER.debug(
            "integration test TramiteServiceFacadeTest#testBuscarComisionesParaAdministrar ...");

        String tipoUsuario = ERol.RESPONSABLE_CONSERVACION.getRol();
        // String idTerritorial = "6640";//quindio x
        // String idTerritorial = "6160";//caldas
        // String idTerritorial = "6320";//cordoba
        String idTerritorial = "6280";// cesar x
        // String idTerritorial = "6400";//guajira

        int[] ids = {4982, 4941, 4937, 4936, 4882, 4862, 4793, 4792, 4788,
            // 4785,
            // 4784,
            4751, 4750, 4743, 4742, 4741, 4665, 4663, 4647, 4646, 4607, 4606, 4593, 4592, 4591, 4580,
            4569, 4563};

        int total = this.tramiteService.contarComisionesParaAdministrar(idTerritorial, tipoUsuario);

        LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        LOGGER.debug("Resultado : " + total);
        LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        for (int id : ids) {
            List<VComision> rows = this.tramiteService.
                buscarComisionesParaAdministrar(idTerritorial, tipoUsuario, null,
                    null, null, 0, 8, 1, id);

            assertNotNull(rows);
            LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            LOGGER.debug("Resultado : " + rows.size());
            LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        }

    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testObtenerDocumentosComisionPorTramiteId() {
        LOGGER.debug("Testing method documentoDAO.obtenerDocumentosComisionPorTramiteId");
        List<Documento> lista = this.tramiteService.obtenerDocumentosComisionPorTramiteId(133999L);
        assertNotNull(lista);
    }

    /**
     * Prueba para verificación de tiempos de duración entre las consultas a procesos y base de
     * datos para la obtención de tareas geográficas.
     *
     * @author david.cifuentes
     */
    @Test
    public void testObtenerTareasGeograficasPaginado() {

        LOGGER.debug("TramiteServiceFacadeTest#testObtenerTareasGeograficasPaginado");
        // mvn test -Dtest=co.gov.igac.snc.fachadas.test.TramiteServiceFacadeTest#testObtenerTareasGeograficasV3
        try {

            long startTime = System.nanoTime();
            String login = "pruatlantico17";
            UsuarioDTO user = this.generalesService.getCacheUsuario(login);
            assertNotNull(user);
            ResultadoVO resultado = this.tramiteService.obtenerTareasGeograficasPaginado(user);
            assertNotNull(resultado);
            LOGGER.debug("Actividades: " + resultado.getActividades().size());

            long endTime = System.nanoTime();
            LOGGER.debug("Duración: " + (endTime - startTime) / 1000000 + " ms");

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author leidy.gonzalez
     */
    @Test
    public void testAsociarInfoCampoAdicional() {

        LOGGER.debug("TramiteDAOBeanTests#testAsociarInfoCampoAdicional");

        try {
            TramiteInfoAdicional testEntity = new TramiteInfoAdicional();

            testEntity.setCampo("str::f1y940kvjges0cuqsemi");
            testEntity.setValor("str::5i2fe6my8l6tln02otj4i");

            this.tramiteService.asociarInfoCampoAdicional(EInfoAdicionalCampo.ENGLOBE_VIRTUAL,
                testEntity.getValor());

            LOGGER.debug("***********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

        // end of class
    }
}
