/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.fachadas.test.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import static org.testng.Assert.fail;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * pruebas de integración de los métodos de la interfaz IPruebas
 *
 * @author pedro.garcia
 */
public class PruebasServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PruebasServiceFacadeTest.class);

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.debug("setUp");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * la gracia de esta prueba es que se tenga una transacción en el flujo, se ejecute el sp -de
     * forma aislada- y el flujo pueda continuar con la transacción original haciendo operaciones
     * que requieran commit.
     *
     * @author pedro.garcia
     */
    @Test
    public void testEjecutarSPAislado() {

        LOGGER.debug("unit test PruebasServiceFacadeTest#testEjecutarSPAislado ...");

        Object[] answer;

        try {
            answer = this.pruebasService.ejecutarSPAislado("0", "integration test", true);

            LOGGER.debug("... passed!");
            Assert.assertTrue(true);
        } catch (Exception ex) {
            LOGGER.error("error en PruebasServiceFacadeTest#testEjecutarSPAislado: " +
                ex.getMessage());
            fail();
        }

    }

//end of class    
}
