package co.gov.igac.snc.fachadas.test.integration.iper;

import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.snc.fachadas.test.integration.AbstractFacadeTest;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 *
 * Prueba de integración que procesa los tramites pendientes de IPER
 *
 * @author fredy.wilches
 *
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ProcesarPendientesTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProcesarPendientesTest.class);

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
    }
//--------------------------------------------------------------------------------------------------

    /*
     * Crea una solicitud con un trámite
     */
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 400000)
    // @Test
    public void testProcesarPendientes() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testProcesarPendientes");

        try {
            // /////////////////////////////////////////////////////////////////////////////////////////////
            Solicitud s = this.interrelacionService.procesarPendientes();
            for (Tramite t : s.getTramites()) {
                this.tramiteService.generarProyeccion(t.getId());
            }
            this.interrelacionService.proyectarPropietarios(s);
            //assertNotNull(solicitudCatastral);
            //assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
