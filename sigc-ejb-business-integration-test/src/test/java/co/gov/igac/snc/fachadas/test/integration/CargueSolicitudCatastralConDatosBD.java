package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.fail;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.persistence.util.ERol;

public class CargueSolicitudCatastralConDatosBD
    extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(APIProcesosServiceFacadeTest.class);

    private static final int TIMER = 200;

    private static CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[]{
            new UniqueHashCode(),
            new NotNull(),
            new ParseDate("dd/MM/yy"),
            new Optional(),
            new Optional(),
            new NotNull(),
            new NotNull()
        };

        return processors;
    }

    @Test
    public void testCargarArchivo() throws IOException {
        LOGGER.info("Inicia Cargue de archivo");
        Map<String, Object> mapaSolicitudesCatastrales;
        ICsvMapReader mapReader = null;
        SolicitudCatastral solicitudCatastral;
        Calendar fechaRadicacion = new GregorianCalendar();

        try {
            mapReader = new CsvMapReader(new InputStreamReader(getClass().getResourceAsStream(
                "/csv/fallidos07022014.csv"), "ISO-8859-1"), CsvPreference.STANDARD_PREFERENCE);

            // the header columns are used as the keys to the Map
            final String[] cabecera = mapReader.getHeader(true);
            final CellProcessor[] processors = getProcessors();
            boolean banderaDatos = false;

            List<UsuarioDTO> usuarioPruebas = new ArrayList<UsuarioDTO>();
            while ((mapaSolicitudesCatastrales = mapReader.read(cabecera, processors)) != null) {
                /* System.out.println(String.format("lineNo=%s, rowNo=%s, customerMap=%s",
                 * mapReader.getLineNumber(), mapReader.getRowNumber(), customerMap)); */
                solicitudCatastral = new SolicitudCatastral();
                try {
                    solicitudCatastral = new SolicitudCatastral();
                    solicitudCatastral.setIdentificador(Long.valueOf(
                        (String) mapaSolicitudesCatastrales.get("ID")));
                    solicitudCatastral.setNumeroSolicitud((String) mapaSolicitudesCatastrales.get(
                        "NUMERO"));
                    fechaRadicacion.setTime((Date) mapaSolicitudesCatastrales.get("FECHA"));
                    solicitudCatastral.setFechaRadicacion(fechaRadicacion);
                    solicitudCatastral.setNumeroRadicacion((String) mapaSolicitudesCatastrales.get(
                        "NUMERO_RADICACION"));
                    solicitudCatastral.setNumeroPredial((String) mapaSolicitudesCatastrales.get(
                        "NUMERO_PREDIAL"));
                    solicitudCatastral.setTipoTramite((String) mapaSolicitudesCatastrales.get(
                        "TIPO_TRAMITE"));
                    solicitudCatastral.setTerritorial((String) mapaSolicitudesCatastrales.get(
                        "TERRITORIAL"));
                    solicitudCatastral.setTipoTramite(solicitudCatastral.getTipoTramite().
                        replaceAll("\\s+$", ""));
                    /*
                     * LOGGER.info(solicitudCatastral.getIdentificador() + " " +
                     * solicitudCatastral.getNumeroSolicitud() + " " +
                     * solicitudCatastral.getFechaRadicacion() + " " +
                     * solicitudCatastral.getNumeroRadicacion() + " " +
                     * solicitudCatastral.getNumeroPredial() + " " +
                     * solicitudCatastral.getTipoTramite() + " " +
                     * solicitudCatastral.getTerritorial());
                     */
                    List<ActividadUsuarios> transicionesUsuarios =
                        new ArrayList<ActividadUsuarios>();
                    List<UsuarioDTO> usuariosList = tramiteService.
                        buscarFuncionariosPorRolYTerritorial(
                            solicitudCatastral.getTerritorial(), ERol.DIGITALIZADOR);
                    transicionesUsuarios.add(new ActividadUsuarios(
                        ProcesoDeConservacion.ACT_DIGITALIZACION_ESCANEAR, usuariosList));
                    solicitudCatastral.setActividadesUsuarios(transicionesUsuarios);
                    if (solicitudCatastral.getTipoTramite() != null) {
                        String tipoTramite = solicitudCatastral.getTipoTramite().trim();
                        if (!(tipoTramite.equals("Mutación"))) {

                            String id = procesoService.crearProceso(solicitudCatastral);
                            LOGGER.info(solicitudCatastral.getIdentificador() + "," + id);
                            Thread.sleep(TIMER);
                        } else {
                            LOGGER.info(solicitudCatastral.getIdentificador() + ",Mutación");
                        }

                    }

                } catch (Exception e) {
                    LOGGER.error(solicitudCatastral.getIdentificador() + ",sin proceso");
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        } finally {
            if (mapReader != null) {
                mapReader.close();
            }
        }
        LOGGER.info("Fin Cargue de archivo");
    }

}
