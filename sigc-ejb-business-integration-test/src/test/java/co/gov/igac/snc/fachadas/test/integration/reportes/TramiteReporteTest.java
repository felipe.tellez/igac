package co.gov.igac.snc.fachadas.test.integration.reportes;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.fachadas.test.integration.AbstractFacadeTest;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;

/**
 *
 * Prueba de integración para reportes (Carga datos aleatorios de la base de datos)
 *
 * @author juan.mendez
 * @author javier.aponte
 *
 */
public class TramiteReporteTest extends AbstractFacadeTest {

    public static Logger LOGGER = LoggerFactory.getLogger(TramiteReporteTest.class);

    private UsuarioDTO user;

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");

            user = generalesService.getCacheUsuario("prusoacha07");
            assertNotNull(user);
            LOGGER.debug(user.toString());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     *
     * @author juan.mendez
     * @author javier.aponte
     */
    // Prueba de Carga
    //@Test(threadPoolSize = 2, invocationCount = 1, timeOut = 3000)
    @Test(threadPoolSize = 2, invocationCount = 1, timeOut = 4000)
    public void testConstanciaRadicacionSolicitudConservacion() {
        LOGGER.debug("testConstanciaRadicacionSolicitudConservacion");
        try {

            Solicitud solicitud = pruebasService.cargarSolicitudAleatoria();
            assertNotNull(solicitud);
            assertNotNull(solicitud.getNumero());
            String numeroSolicitud = solicitud.getNumero();
            LOGGER.debug("numeroSolicitud:" + numeroSolicitud);

            String urlReporte = EReporteServiceSNC.CONSTANCIA_RADICACION.getUrlReporte();

            IReporteService service = ReporteServiceFactory.getService();

            Reporte reporte = new Reporte(user);

            reporte.setUrl(urlReporte);
            //reporte.setDirectorioSalida(OUTPUT_DIR);

            reporte.addParameter("NUMERO_SOLICITUD", numeroSolicitud);

            File resultado = service.getReportAsFile(reporte,
                new LogMensajesReportesUtil(this.generalesService, user));
            assertNotNull(resultado);
            LOGGER.debug("Tamaño Archivo:" + resultado.length());
            assertTrue(resultado.getPath().endsWith(".pdf"));
            assertFalse(resultado.length() < 2000, "el archivo se generó con error (" +
                numeroSolicitud + ")");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }
}
