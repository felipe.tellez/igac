package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioPublico;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.enumsOfertasInmob.EOfertaTipoOferta;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;

public class AvaluosServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvaluosServiceFacadeTest.class);

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para actualización de oferta inmobiliaria
     *
     * @author juan.agudelo
     */
    @Test
    public void testGuardarActualizarOfertaInmobiliaria() {
        LOGGER.debug("test: AvaluosServiceFacadeTest#testGuardarActualizarOfertaInmobiliaria");

        OfertaInmobiliaria oi = new OfertaInmobiliaria();
        List<OfertaInmobiliaria> oiList = new ArrayList<OfertaInmobiliaria>();
        List<OfertaInmobiliaria> oiEliminarList = new ArrayList<OfertaInmobiliaria>();
        OfertaInmobiliaria oiHijo = new OfertaInmobiliaria();
        UsuarioDTO usuario = new UsuarioDTO();

        usuario.setLogin("juan.agudelo.test");

        Departamento d = new Departamento();
        d.setCodigo("25");
        oi.setDepartamento(d);
        Municipio m = new Municipio();
        m.setCodigo("25754");
        oi.setMunicipio(m);
        oi.setNumeroBanios(3);
        oi.setCableadoRed(ESiNo.SI.getCodigo());
        oi.setNumeroOficinas(3);
        oi.setRiego(ESiNo.SI.getCodigo());
        oi.setRecursosHidricos(ESiNo.SI.getCodigo());
        oi.setAreaTotalTerrenoCatastral(124.2);
        oi.setAreaTotalConstruccionCata(124.2);
        oi.setAreaTerreno(124.2);
        oi.setAreaConstruccion(124.2);
        oi.setTipoOferta(EOfertaTipoOferta.PROYECTOS.getCodigo());
        oi.setFechaIngresoOferta(new Date());
        oi.setValorPedido(99999.0);
        oi.setPorcentajeNegociacion(0.13);
        oi.setValorArriendo(875325.0);
        oi.setValorAdministracion(85325.0);
        oi.setNumeroPisos(4);
        oi.setNumeroHabitaciones(3);
        oi.setNumeroCocinas(1);

        oiHijo.setDepartamento(d);
        oiHijo.setMunicipio(m);
        oiHijo.setNumeroBanios(3);
        oiHijo.setCableadoRed(ESiNo.SI.getCodigo());
        oiHijo.setNumeroOficinas(3);
        oiHijo.setRiego(ESiNo.SI.getCodigo());
        oiHijo.setRecursosHidricos(ESiNo.SI.getCodigo());
        oiHijo.setAreaTotalTerrenoCatastral(124.2);
        oiHijo.setAreaTotalConstruccionCata(124.2);
        oiHijo.setAreaTerreno(124.2);
        oiHijo.setAreaConstruccion(124.2);
        oiHijo.setTipoOferta(EOfertaTipoOferta.PROYECTOS.getCodigo());
        oiHijo.setFechaIngresoOferta(new Date());
        oiHijo.setValorPedido(99999.0);
        oiHijo.setPorcentajeNegociacion(0.13);
        oiHijo.setValorArriendo(875325.0);
        oiHijo.setValorAdministracion(85325.0);
        oiHijo.setNumeroPisos(4);
        oiHijo.setNumeroHabitaciones(3);
        oiHijo.setNumeroCocinas(1);
        oiHijo.setOfertaInmobiliaria(oi);
        oiList.add(oiHijo);

        oi.setOfertaInmobiliarias(oiList);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            OfertaInmobiliaria answer = this.avaluosService
                .guardarActualizarOfertaInmobiliaria(oi, oiEliminarList,
                    usuario);

            Assert.assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de ofertas inmobiliarias por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarOfertasInmobiliariasPorFiltro() {
        LOGGER.debug("test: AvaluosServiceFacadeTest#testBuscarOfertasInmobiliariasPorFiltro");

        FiltroDatosConsultaOfertasInmob foi = new FiltroDatosConsultaOfertasInmob();
        String codigoOferta = "63602575400000022011";
        foi.setCodigoOferta(codigoOferta);

        // String numeroBanios = "3";
        // foi.setNumeroBanios(numeroBanios);
        //Date fechaOfertaDesde;
        // D: los meses empiezan en 0, y al año le suman 1900
        //fechaOfertaDesde = new Date(2011 - 1900, 10, 17);
        //foi.setFechaOfertaDesde(fechaOfertaDesde);
        //foi.setDepartamentoCodigo("25");
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<OfertaInmobiliaria> answer = this.avaluosService
                .buscarOfertasInmobiliariasPorFiltro(foi, null, null);

            if (answer != null && !answer.isEmpty()) {

                LOGGER.debug("fetched " + answer.size() + " rows");
                for (OfertaInmobiliaria oi : answer) {
                    LOGGER.debug("Oferta id: " + oi.getId());
                    LOGGER.debug("Oferta código: " + oi.getCodigoOferta());
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de contéo de ofertas inmobiliarias por filtro
     *
     * @author juan.agudelo
     */
    @Test
    public void testContarOfertasInmobiliariasPorFiltro() {
        LOGGER.debug("tests: AvaluosServiceFacadeTest#testContarOfertasInmobiliariasPorFiltro");

        FiltroDatosConsultaOfertasInmob foi = new FiltroDatosConsultaOfertasInmob();
        /*
         * String codigoOferta = "1923392591"; foi.setCodigoOferta(codigoOferta);
         */

 /*
         * String numeroBanios = "2"; foi.setNumeroBanios(numeroBanios);
         */
        Double areaOfertaTerrenoDesde = 10.2;
        foi.setAreaOfertaTerrenoDesde(areaOfertaTerrenoDesde);
        Double areaOfertaTerrenoHasta = 1000.2;
        foi.setAreaOfertaTerrenoHasta(areaOfertaTerrenoHasta);
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Long answer = this.avaluosService
                .contarOfertasInmobiliariasPorFiltro(foi);

            if (answer != null) {
                LOGGER.debug("Ofertas: " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba de busqueda de ofertas inmobiliarias con oferta padre por oferta inmobiliaria Id
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarOfertaInmobiliariaOfertaPadreByOfertaId() {
        LOGGER.debug(
            "test: AvaluosServiceFacadeTest#testBuscarOfertaInmobiliariaOfertaPadreByOfertaId");

        Long ofertaInmobiliariaId = 406L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            OfertaInmobiliaria answer = this.avaluosService
                .buscarOfertaInmobiliariaOfertaPadreByOfertaId(ofertaInmobiliariaId);

            if (answer != null) {

                LOGGER.debug("Oferta id: " + answer.getId());

                if (answer.getFotografias() != null &&
                    !answer.getFotografias().isEmpty()) {
                    for (Fotografia foto : answer.getFotografias()) {
                        LOGGER.debug("Foto: " + foto.getArchivoFotografia());
                    }
                }

                if (answer.getOfertaInmobiliaria() != null) {
                    LOGGER.debug("OfertaP id: " +
                        answer.getOfertaInmobiliaria().getId());
                    if (answer.getOfertaInmobiliaria()
                        .getOfertaServicioPublicos() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaServicioPublicos().isEmpty()) {
                        for (OfertaServicioPublico osp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaServicioPublicos()) {
                            LOGGER.debug("OfertaPSP: " +
                                osp.getServicioPublico());
                        }
                    }

                    if (answer.getOfertaInmobiliaria().getFotografias() != null &&
                        !answer.getOfertaInmobiliaria().getFotografias()
                            .isEmpty()) {
                        for (Fotografia foto : answer.getOfertaInmobiliaria()
                            .getFotografias()) {
                            LOGGER.debug("FotoP: " +
                                foto.getArchivoFotografia());
                        }
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Prueba de busqueda de ofertas inmobiliarias con oferta padre por oferta inmobiliaria Id
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarOfertaInmobiliariaOfertaByOfertaId() {
        LOGGER.debug("test: AvaluosServiceFacadeTest#testBuscarOfertaInmobiliariaOfertaByOfertaId");

        Long ofertaInmobiliariaId = 482L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            OfertaInmobiliaria answer = this.avaluosService
                .buscarOfertaInmobiliariaPorId(ofertaInmobiliariaId);

            if (answer != null) {

                LOGGER.debug("Oferta id: " + answer.getId());

                if (answer.getFotografias() != null &&
                    !answer.getFotografias().isEmpty()) {
                    for (Fotografia foto : answer.getFotografias()) {
                        LOGGER.debug("Foto: " + foto.getArchivoFotografia());
                    }
                }

                if (answer.getOfertaInmobiliaria() != null) {
                    LOGGER.debug("OfertaP id: " +
                        answer.getOfertaInmobiliaria().getId());
                    if (answer.getOfertaInmobiliaria()
                        .getOfertaServicioPublicos() != null &&
                        !answer.getOfertaInmobiliaria()
                            .getOfertaServicioPublicos().isEmpty()) {
                        for (OfertaServicioPublico osp : answer
                            .getOfertaInmobiliaria()
                            .getOfertaServicioPublicos()) {
                            LOGGER.debug("OfertaPSP: " +
                                osp.getServicioPublico());
                        }
                    }

                    if (answer.getOfertaInmobiliaria().getFotografias() != null &&
                        !answer.getOfertaInmobiliaria().getFotografias()
                            .isEmpty()) {
                        for (Fotografia foto : answer.getOfertaInmobiliaria()
                            .getFotografias()) {
                            LOGGER.debug("FotoP: " +
                                foto.getArchivoFotografia());
                        }
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

}
