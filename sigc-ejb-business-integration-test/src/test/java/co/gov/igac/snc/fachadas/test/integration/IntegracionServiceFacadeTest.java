package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;

/**
 *
 * Prueba de integración servicios transversales y/o de integración (Correo, JMS, etc)
 *
 * Jms queue vs topic http://lalit-jolania.blogspot.com/2008/07/jms-queue-vs-topic.html
 *
 * @author juan.mendez
 * @version 2.0
 */
public class IntegracionServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(IntegracionServiceFacadeTest.class);

    /**
     * Método para iniciarlizar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.debug("setUp");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testEnviarEmail() {
        LOGGER.debug("*IntegracionServiceFacadeTest#testEnviarEmail");
        try {
            this.generalesService.enviarCorreo("pruatlantico@igac.gov.co",
                "Prueba Integración Email",
                "Esta es una prueba de integración del email ensaje....", null, null);
            LOGGER.debug("email enviado ...");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

//-------------------------------------------------------------------------------------------------	
//TODO :: fredy.wilches :: mover este método a PruebasServiceFacadeTest que es donde deben estar las pruebas de los métodos de IPruebas
    @Test
    public void testBuscarPredioLibreTramite() {
        LOGGER.debug("*#testBuscarPredioLibreTramite");
        try {
            String numerosPrediales = this.pruebasService.buscarPredioLibreTramite(
                CODIGO_ESTRUCTURA_O_ATLANTICO, "08001").getNumeroPredial();
            LOGGER.debug("numerosPrediales:" + numerosPrediales);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

}
