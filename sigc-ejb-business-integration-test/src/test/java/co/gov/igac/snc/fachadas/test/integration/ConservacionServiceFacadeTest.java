package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.persistence.util.EUnidadTipoDominio;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;

/**
 * Ejemplo prueba de Integración - Consulta EJB publicado en JBOSS
 *
 * @author juan.mendez
 *
 */
public class ConservacionServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConservacionServiceFacadeTest.class);

    private List<Predio> predios;
    private int predioPos;
    private Random random;

    protected List<String> listaPredios;

    /* @BeforeClass public void beforeClass() { LOGGER.debug("beforeClass..."); random = new
     * Random();
     *
     * try { //	URL fileUrl = this.getClass().getClassLoader().getResource("predios.csv"); //
     * listaPredios = new ArrayList<String>(); //	CSVReader reader = new CSVReader(new
     * FileReader(fileUrl.getPath())); //	String [] nextLine; //	while ((nextLine =
     * reader.readNext()) != null) { //	listaPredios.add(nextLine[0] ); //	} } catch (Exception e) {
     * LOGGER.error(e.getMessage(), e); fail(e.getMessage(), e); } } */
    // @Test(threadPoolSize = 1, invocationCount = 10, timeOut = 60000)
    @Test
    public void testGetPredioBloqueoError() {
        LOGGER.debug("testGetPredioBloqueoError");
        try {
            PredioBloqueo predioBloqueo = conservacionService
                .getPredioBloqueo("257540216041711057933105117115");
            assertNull(predioBloqueo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // error
            assertFalse(true);
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     *
     * PRE: existe en la bd un predio con el número predial usado
     */
    @Test
    public void testGetPredioBloqueo() {
        LOGGER.debug("integration test ConservacionServiceFacadeTest#testGetPredioBloqueo ...");

        String numeroPredial;

        numeroPredial = "257540102000011540001500000003";
        try {
            PredioBloqueo predioBloqueo = conservacionService.getPredioBloqueo(numeroPredial);

            assertNotNull(predioBloqueo);

            if (predioBloqueo != null) {
                LOGGER.debug("encontró un bloqueo para el predio " + numeroPredial);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testGetPredioByNumeroPredial() {
        LOGGER.debug("testGetPredioByNumeroPredial");

        try {
            Predio predio = conservacionService
                .getPredioByNumeroPredial("257540103000000190023000000000");
            assertNotNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------
    @Test
    public void testGetPredioByNumeroPredialError() {
        LOGGER.debug("testGetPredioByNumeroPredialError");

        try {
            Predio predio = conservacionService
                .getPredioByNumeroPredial("paila");
            assertNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testSearchPredios() {
        LOGGER.debug("testSearchPredios");
        List<Object[]> predios;
        FiltroDatosConsultaPredio datos;

        datos = new FiltroDatosConsultaPredio();
        datos.setNumeroPredial("257540103000000190023000000000");
        try {
            predios = conservacionService.searchPredios(datos, 0, -1);
            assertNotNull(predios);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------
    @Test
    public void testSearchPrediosError() {
        LOGGER.debug("testSearchPrediosError");

        try {
            Predio predio = conservacionService
                .getPredioByNumeroPredial("paila");
            assertNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    @Test
    public void testFindPersonaBloqueoByPersonaId() {
        LOGGER.debug("testFindPersonaBloqueoByPersonaId");

        String idPersona = "3";
        PersonaBloqueo pb;
        try {
            pb = conservacionService.findPersonaBloqueoByPersonaId(new Long(
                idPersona));
            assertNotNull(pb);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }
    // --------------------------------------------------------------------------------------------------

    @Test
    public void testObtenerXmlPorPredioId() {
        LOGGER.debug("testObtenerXmlPorPredioId");

        Long idPredio = 7L;
        try {
            this.conservacionService.obtenerXmlPredioPorPredioId(idPredio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------
    @Test
    public void testFindPersonaBloqueoByPersonaIdError() {
        LOGGER.debug("testFindPersonaBloqueoByPersonaIdError");

        String idPersona = "0";
        PersonaBloqueo pb;
        try {
            pb = conservacionService.findPersonaBloqueoByPersonaId(new Long(
                idPersona));
            assertNull(pb);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetTramitesByNumeroPredialPredio() {
        LOGGER.debug("testGetTramitesByNumeroPredialPredio");

        List<Tramite> tramites;
        String numeroPredial = "257540216041711057933105117117";
        try {
            tramites = conservacionService
                .getTramitesByNumeroPredialPredio(numeroPredial);

            if (tramites != null) {
                LOGGER.debug("Passed. fetched " + tramites.size() + " rows");
            }
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------
    /**
     * @author pedro.garcia
     */
    @Test
    public void testGetTramitesByNumeroPredialPredioError() {
        LOGGER.debug("testGetTramitesByNumeroPredialPredioError");

        List<Tramite> tramites;
        String numeroPredial = "paila";
        try {
            tramites = conservacionService
                .getTramitesByNumeroPredialPredio(numeroPredial);
            assert (tramites == null || tramites.isEmpty());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchAvaluosByNumeroPredial() {
        LOGGER.debug("testGetPredioFetchAvaluosByNumeroPredial");

        Predio predio;
        String numPredial = "257540216041711057933105117116";
        try {
            predio = conservacionService
                .getPredioFetchAvaluosByNumeroPredial(numPredial);
            assertNotNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción método: testGetPredioFetchAvaluosByNumeroPredial");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchAvaluosByNumeroPredialError() {
        LOGGER.debug("testGetPredioFetchAvaluosByNumeroPredialError");

        Predio predio;
        String numPredial = "hola";
        try {
            predio = conservacionService
                .getPredioFetchAvaluosByNumeroPredial(numPredial);
            assertNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción método: testGetPredioFetchAvaluosByNumeroPredialError");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchDerechosPropiedadByNumeroPredial() {
        LOGGER.debug("testGetPredioFetchDerechosPropiedadByNumeroPredial");

        Predio predio;
        String numPredial = "257540216041711057933105117116";
        try {
            predio = conservacionService
                .getPredioFetchDerechosPropiedadByNumeroPredial(numPredial);
            assertNotNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción método: testGetPredioFetchDerechosPropiedadByNumeroPredial");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGetPredioFetchDerechosPropiedadByNumeroPredialError() {
        LOGGER.debug("testGetPredioFetchDerechosPropiedadByNumeroPredialError");

        Predio predio;
        String numPredial = "hola";
        try {
            predio = conservacionService
                .getPredioFetchDerechosPropiedadByNumeroPredial(numPredial);
            assertNull(predio);
        } catch (Exception e) {
            LOGGER.error("Excepción: " + e.getMessage(), e);
            fail("Excepción método: testGetPredioFetchDerechosPropiedadByNumeroPredialError");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testObtenerPredioConDatosUbicacionPorNumeroPredial() {
        LOGGER.debug("testObtenerPredioConDatosUbicacionPorNumeroPredial");

        Predio predio;
        String numPredial = "257540216041711057933105117116";
        try {
            predio = conservacionService
                .obtenerPredioConDatosUbicacionPorNumeroPredial(numPredial);
            assertNotNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción método: testObtenerPredioConDatosUbicacionPorNumeroPredial");
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testObtenerPredioConDatosUbicacionPorNumeroPredialError() {
        LOGGER.debug("testObtenerPredioConDatosUbicacionPorNumeroPredialError");

        Predio predio;
        String numPredial = "hola";
        try {
            predio = conservacionService
                .obtenerPredioConDatosUbicacionPorNumeroPredial(numPredial);
            assertNull(predio);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Excepción método: testObtenerPredioConDatosUbicacionPorNumeroPredialError");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @deprecated porque se usa otro método para hacer las búsquedas
     */
    @Test
    @Deprecated
    public void testContarResultadosBusquedaPredios() {
        LOGGER.debug("testing Iconservacion#contarResultadosBusquedaPredios...");

        FiltroDatosConsultaPredio datos;
        int nofPredios;

        datos = new FiltroDatosConsultaPredio();
        datos.setTerritorialId("11000");
        datos.setDepartamentoId("25");
        datos.setMunicipioId("25754");
        datos.setAreaTerrenoMin(1000);
        datos.setDestinoPredioId("K");
        try {
            nofPredios = conservacionService
                .contarResultadosBusquedaPredios(datos);
            LOGGER.debug("passed. num resultado = " + nofPredios);
            assertTrue(true);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGestionarJustificacion() {

        LOGGER.debug("ConservacionServiceFacadeTest#testgestionarJustificacion");

        PPersonaPredioPropiedad p3 = new PPersonaPredioPropiedad();
        List<PPersonaPredioPropiedad> pl = new ArrayList<PPersonaPredioPropiedad>();

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");

            PPredio pp = this.conservacionService
                .getPPredioFetchPPersonaPredioPorId(7L);
            LOGGER.debug("PPersonaPredio: " + pp.getNombre());
            if (pp.getPPersonaPredios() != null &&
                pp.getPPersonaPredios().size() > 0) {
                p3.setPPersonaPredio(pp.getPPersonaPredios().get(0));
            }
            p3.setTipo("Derecho");
            p3.setUsuarioLog("juan.agudelo");
            p3.setFechaLog(new Date(System.currentTimeMillis()));

            pl.add(p3);

            this.conservacionService.gestionarJustificacion(pl);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testGuardarProyeccion() {
        LOGGER.debug("testGuardarProyeccion");
        PUnidadConstruccion obj = new PUnidadConstruccion();
        PPredio pPredioTest;
        try {
            pPredioTest = this.conservacionService
                .obtenerPPredioCompletoById(Long.valueOf(7));
            obj.setId(Long.valueOf(666));
            obj.setPPredio(pPredioTest);
            obj.setTotalPisosConstruccion(Integer.valueOf("2"));
            obj.setTotalPisosUnidad(Integer.valueOf("2"));
            obj.setAvaluo(Double.valueOf(12000000));
            obj.setAreaConstruida(Double.valueOf(120));
            obj.setAnioConstruccion(1960);
            obj.setTipificacion("5");
            obj.setTotalPuntaje(Double.valueOf(5.0));
            obj.setTipoCalificacion("1");
            obj.setTipoDominio(EUnidadTipoDominio.COMUN.getCodigo());
            obj.setTipoConstruccion(EUnidadTipoConstruccion.CONVENCIONAL
                .getCodigo());
            // obj.setConvencional(ESiNo.NO.getCodigo());
            obj.setCalificacionAnexoId(new Long(40));
            obj.setUnidad("Unity");
            obj.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                .getCodigo());
            /*
             * Usuario usuarioObj = new Usuario(); this.conservacionService.guardarProyeccion(obj);
             */
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("fabio.navarrete");
            this.conservacionService.guardarProyeccion(usuario, obj);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    @Test
    public void testEliminarProyeccion() {
        LOGGER.debug("testEliminarProyeccion");
        IModeloUnidadConstruccion obj = new PUnidadConstruccion();
        try {
            obj = this.conservacionService.obtenerPUnidadConstruccionPorId(Long
                .valueOf(1603017));
            obj.setId(null);
            obj.setCancelaInscribe("I");
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("fabio.navarrete.test");
            obj = (IModeloUnidadConstruccion) this.conservacionService
                .guardarProyeccion(usuario, obj);
            this.conservacionService.eliminarProyeccion(usuario, obj);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void obtenerPPredioPorIdConAvaluosTest() {
        LOGGER.debug("testEliminarProyeccion");
        PPredio pp = this.conservacionService
            .obtenerPPredioPorIdConAvaluos(Long.valueOf(7));
        Assert.assertNotNull(pp);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarFotografiaTramite() {

        LOGGER.debug("ConservacionServiceFacadeTest#testBuscarFotografiaTramite");

        List<Foto> fotosPrueba = null;
        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            fotosPrueba = new ArrayList<Foto>();
            fotosPrueba = this.conservacionService.buscarFotografiasPredio(7L);
            if (fotosPrueba != null && fotosPrueba.size() > 0) {
                LOGGER.debug("Foto dato " + fotosPrueba.get(0).getDescripcion());
                LOGGER.debug("Documento dato " +
                    fotosPrueba.get(0).getDocumento().getArchivo());
            }
            LOGGER.debug("*****************TERMINAMOS****************************");
            Assert.assertNotNull(fotosPrueba);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testActualizarPrediosBloqueo() {

        LOGGER.debug("ConservacionServiceFacadeTest#testActualizarPrediosBloqueo");

        List<PredioBloqueo> pb = null;
        String tipoAvaluo = "02";
        String sectorCodigo = "16";
        UsuarioDTO u = new UsuarioDTO();

        u.setLogin("juan.agudelo");

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            pb = new ArrayList<PredioBloqueo>();

            Departamento dpto = this.generalesService
                .getCacheDepartamentoByCodigo("25");
            Municipio muni = this.generalesService
                .getCacheMunicipioByCodigo("25754");
            FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
            filtro.setDepartamento(dpto);
            filtro.setMunicipio(muni);
            filtro.setTipoAvaluo(tipoAvaluo);
            filtro.setSectorCodigo(sectorCodigo);

            LOGGER.debug("Datos " + filtro.getDepartamento().getNombre());
            LOGGER.debug("Datos " + filtro.getMunicipio().getNombre());
            LOGGER.debug("Datos " + filtro.getTipoAvaluo());

            pb = this.conservacionService.buscarPrediosBloqueo(filtro);

            if (pb != null && pb.size() > 0) {
                LOGGER.debug("Datos " + pb.get(0).getMotivoBloqueo());
                LOGGER.debug("Documento id " +
                    pb.get(0).getDocumentoSoporteBloqueo().getId());
                if (pb.get(0).getPredio() != null) {
                    LOGGER.debug("Documento dato " +
                        pb.get(0).getPredio().getNumeroPredial());
                }
            }

            LOGGER.debug("tamaño: " + pb.size());
            this.conservacionService.actualizarPrediosBloqueo(u, pb);

            Assert.assertTrue(true);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonasBloqueo() {

        LOGGER.debug("ConservacionServiceFacadeTest#testBuscarPersonasBloqueo");

        List<PersonaBloqueo> pb = null;
        String tipoIdentificacion = "CC";
        String numeroIdentificacion = "278641";

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            pb = new ArrayList<PersonaBloqueo>();

            FiltroDatosConsultaPersonasBloqueo filtro = new FiltroDatosConsultaPersonasBloqueo(
                tipoIdentificacion, numeroIdentificacion);

            pb = this.conservacionService.buscarPersonasBloqueo(filtro);

            if (pb != null && pb.size() > 0) {
                LOGGER.debug("Datos " + pb.get(0).getMotivoBloqueo());

                if (pb.get(0).getPersona() != null) {
                    LOGGER.debug("Documento id " +
                        pb.get(0).getPersona().getNombre());
                }
            }
            LOGGER.debug("*****************TERMINAMOS****************************");
            Assert.assertNotNull(pb);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGuardarPFotos() {

        LOGGER.debug("ConservacionServiceFacadeTest#testGuardarPFotos");

        List<PFoto> listaF = new ArrayList<PFoto>();
        Documento doc = new Documento();
        PPredio pp = new PPredio();
        PFoto pf = new PFoto();

        UsuarioDTO u = new UsuarioDTO();
        u.setLogin("juan.agudelo");

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            doc.setArchivo("Ruta de prueba 2");
            doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
            doc.setBloqueado(ESiNo.NO.getCodigo());
            doc.setUsuarioCreador(u.getLogin());
            doc.setUsuarioLog(u.getLogin());
            doc.setFechaLog(new Date());

            pp.setId(7L);
            pf.setPPredio(pp);
            pf.setUsuarioLog(u.getLogin());
            pf.setFechaLog(new Date());
            pf.setFecha(new Date());
            pf.setTipo("T1");
            pf.setDocumento(doc);

            listaF.add(pf);

            this.conservacionService.guardarPFotos(listaF);
            LOGGER.debug("*****************GUARDAMOS****************************");
            Assert.assertTrue(true);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testContarPrediosBloqueoByNumeroPredial() {
        LOGGER.debug("ConservacionServiceFacadeTest#testContarPrediosBloqueoByNumeroPredial");

        String numeroPredial = "257540216041711057933105117115";
        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            Integer conteo = this.conservacionService
                .contarPrediosBloqueoByNumeroPredial(numeroPredial);
            LOGGER.debug("num resultado = " + conteo);

            Assert.assertTrue(conteo != null);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosBloqueoByNumeroPredial() {
        LOGGER.debug("ConservacionServiceFacadeTest#testBuscarPrediosBloqueoByNumeroPredial");

        String numeroPredial = "257540216041711057933105117115";
        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            List<PredioBloqueo> conteo = this.conservacionService
                .buscarPrediosBloqueoByNumeroPredial(numeroPredial);
            LOGGER.debug("num resultado = " + conteo.size());

            for (PredioBloqueo pb : conteo) {
                LOGGER.debug("Fecha inicial = " + pb.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + pb.getMotivoBloqueo());
                LOGGER.debug("Documento = " +
                    pb.getDocumentoSoporteBloqueo().getArchivo());
            }

            Assert.assertTrue(conteo != null);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaBloqueoByNumeroIdentificacion() {
        LOGGER.debug(
            "testing  ConservacionServiceFacadeTest#testBuscarPersonaBloqueoByNumeroIdentificacion");

        String numeroIdentificacion = "764925";
        String tipoIdentificacion = "CC";
        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            List<PersonaBloqueo> pb = this.conservacionService
                .buscarPersonaBloqueoByNumeroIdentificacion(
                    numeroIdentificacion, tipoIdentificacion);
            LOGGER.debug("num resultado = " + pb.size());

            for (PersonaBloqueo pbloq : pb) {

                LOGGER.debug("Fecha inicial = " + pbloq.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + pbloq.getMotivoBloqueo());

                LOGGER.debug("Identificación = " +
                    pbloq.getPersona().getNumeroIdentificacion());
                LOGGER.debug("Documento = " +
                    pbloq.getDocumentoSoporteBloqueo().getArchivo());
            }

            Assert.assertTrue(pb != null);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaBloqueo() {
        LOGGER.debug("testing  ConservacionServiceFacadeTest#testBuscarPersonaBloqueo");

        String tipoIdentificacion = "CC";
        String numeroIdentificacion = "764925";
        FiltroDatosConsultaPersonasBloqueo filtroDatosConsultaPersonasBloqueo =
            new FiltroDatosConsultaPersonasBloqueo(
                tipoIdentificacion, numeroIdentificacion);

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");
            List<PersonaBloqueo> pb = this.conservacionService
                .buscarPersonasBloqueo(filtroDatosConsultaPersonasBloqueo);
            LOGGER.debug("num resultado = " + pb.size());

            for (PersonaBloqueo pbloq : pb) {
                LOGGER.debug("Fecha inicial = " + pbloq.getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " + pbloq.getMotivoBloqueo());
                LOGGER.debug("Identificación = " +
                    pbloq.getPersona().getNumeroIdentificacion());
                if (pbloq.getDocumentoSoporteBloqueo() != null) {
                    LOGGER.debug("Documento = " +
                        pbloq.getDocumentoSoporteBloqueo().getArchivo());
                }
                if (pbloq.getDocumentoSoporteDesbloqueo() != null) {
                    LOGGER.debug("Documento Desbloqueo = " +
                        pbloq.getDocumentoSoporteDesbloqueo()
                            .getArchivo());
                }
            }

            Assert.assertTrue(pb != null);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testActualizarPersonasBloqueo() {
        LOGGER.debug("testing ConservacionServiceFacadeTest#testActualizarPersonasBloqueo...");

        UsuarioDTO usuario = new UsuarioDTO();
        Persona p = new Persona();
        Documento d = new Documento();
        List<PersonaBloqueo> pBList = new ArrayList<PersonaBloqueo>();

        usuario.setLogin("juan.agudelo");
        p.setId(3L);

        try {
            LOGGER.debug("*****************ENTRAMOS******************************");

            p = conservacionService.buscarPersonasById(p.getId());
            d.setId(118L);
            d = generalesService.buscarDocumentoPorId(d.getId());

            PersonaBloqueo pb = new PersonaBloqueo();
            pb.setPersona(p);
            pb.setFechaInicioBloqueo(new Date(System.currentTimeMillis()));
            pb.setFechaRecibido(new Date(System.currentTimeMillis()));
            pb.setUsuarioLog(usuario.getLogin());
            pb.setFechaLog(new Date(System.currentTimeMillis()));
            pb.setDocumentoSoporteBloqueo(d);
            pb.setMotivoBloqueo("Prueba de integración de bloqueo persona");
            pBList.add(pb);

            List<PersonaBloqueo> pBResultado = this.conservacionService
                .actualizarPersonasBloqueo(usuario, pBList);

            LOGGER.debug("num resultado = " + pBResultado.size());

            if (pBResultado.size() > 0) {
                LOGGER.debug("Fecha inicial = " +
                    pBResultado.get(0).getFechaInicioBloqueo());
                LOGGER.debug("Motivo = " +
                    pBResultado.get(0).getMotivoBloqueo());
                LOGGER.debug("Documento = " +
                    pBResultado.get(0).getDocumentoSoporteBloqueo());
            }
            Assert.assertTrue(pBResultado.size() > 0);
            LOGGER.debug("*****************TERMINAMOS****************************");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de trámite con documentación y documento
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPorTramiteId() {

        LOGGER.debug("tests: ConservacionServiceFacadeTest#testBuscarPrediosPorTramiteId");

        Long tramiteId = 1775l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = this.conservacionService
                .buscarPrediosPorTramiteId(tramiteId);

            for (Predio p : answerT) {
                LOGGER.debug("Número predial" + p.getNumeroPredial());
                LOGGER.debug("Id" + p.getId());
            }

            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para buscarPPrediosPanelAvaluoPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPPrediosPanelAvaluoPorTramiteId() {
        LOGGER.debug(
            "integration test ConservacionServiceFacadeTest#testBuscarPPrediosPanelAvaluoPorTramiteId ...");

        List<PPredio> ppredios;
        Long tramiteId = 1770l;

        tramiteId = 42531l;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            ppredios = this.conservacionService
                .buscarPPrediosPanelAvaluoPorTramiteId(tramiteId);

            if (ppredios != null && !ppredios.isEmpty()) {

                for (PPredio p : ppredios) {
                    LOGGER.debug("Avaluos " +
                        p.getPPredioAvaluoCatastrals().get(0)
                            .getValorTotalAvaluoCatastral());
                }

            }

            Assert.assertNotNull(ppredios);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para buscarPPrediosPanelJPropiedadPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPPrediosPanelJPropiedadPorTramiteId() {
        LOGGER.debug(
            "testing ConservacionServiceFacadeTest#testBuscarPPrediosPanelJPropiedadPorTramiteId");

        List<PPredio> ppredios;
        Long tramiteId = 42386l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            ppredios = this.conservacionService
                .buscarPPrediosPanelJPropiedadPorTramiteId(tramiteId);

            if (ppredios != null && !ppredios.isEmpty()) {

                for (PPredio p : ppredios) {
                    if (p.getPPersonaPredios() != null &&
                        !p.getPPersonaPredios().isEmpty()) {
                        for (PPersonaPredio pp : p.getPPersonaPredios()) {

                            LOGGER.debug("PerosnaP id: " + pp.getId());

                            if (pp.getPPersonaPredioPropiedads() != null &&
                                !pp.getPPersonaPredioPropiedads().isEmpty()) {
                                for (PPersonaPredioPropiedad ppp : pp.getPPersonaPredioPropiedads()) {
                                    LOGGER.debug("PersonaPP tipo título: " + ppp.getTipoTitulo());
                                    LOGGER.debug("departamento nombre: " + ppp.getDepartamento().
                                        getNombre());
                                    LOGGER.debug("departamento código: " + ppp.getDepartamento().
                                        getCodigo());
                                    LOGGER.debug("municipio nombre: " + ppp.getMunicipio().
                                        getNombre());
                                    LOGGER.debug("municipio código: " + ppp.getMunicipio().
                                        getCodigo());

                                    if (ppp.getDocumentoSoporte() != null) {
                                        LOGGER.debug("documento id alfresco: " +
                                            ppp.getDocumentoSoporte().getIdRepositorioDocumentos());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Assert.assertNotNull(ppredios);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            Assert.fail();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios simples por tramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelAvaluoPorTramiteId() {
        LOGGER.debug("ConservacionServiceFacadeTest#testBuscarPrediosPanelAvaluoPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = this.conservacionService
                .buscarPrediosPanelAvaluoPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    LOGGER.debug("Avaluos " +
                        p.getPredioAvaluoCatastrals().get(0)
                            .getValorTotalAvaluoCatastral());
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel justificación de propiedad en
     * validación
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelJPropiedadPorTramiteId() {
        LOGGER.debug("ConservacionServiceFacadeTest#testBuscarPrediosPanelJPropiedadPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = this.conservacionService
                .buscarPrediosPanelJPropiedadPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    if (p.getPersonaPredios() != null &&
                        !p.getPersonaPredios().isEmpty()) {
                        for (PersonaPredio pp : p.getPersonaPredios()) {

                            LOGGER.debug("PerosnaP id: " + pp.getId());

                            if (pp.getPersonaPredioPropiedads() != null &&
                                !pp.getPersonaPredioPropiedads()
                                    .isEmpty()) {
                                for (PersonaPredioPropiedad ppp : pp
                                    .getPersonaPredioPropiedads()) {
                                    LOGGER.debug("PerosnaPP tipo título: " +
                                        ppp.getTipoTitulo());
                                }
                            }
                        }
                    }
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel personas y / o propietarios
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelPropietariosPorTramiteId() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testBuscarPrediosPanelPropietariosPorTramiteId");

        Long tramiteId = 1770l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = this.conservacionService
                .buscarPrediosPanelJPropiedadPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    if (p.getPersonaPredios() != null &&
                        !p.getPersonaPredios().isEmpty()) {
                        for (PersonaPredio pp : p.getPersonaPredios()) {

                            LOGGER.debug("PerosnaP id: " + pp.getId());

                            if (pp.getPersona() != null) {

                                LOGGER.debug("Nombre: " +
                                    pp.getPersona().getNombre());
                            }
                        }
                    }
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para buscarPPrediosPanelJPropiedadPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPPrediosPanelPropietariosPorTramiteId() {
        LOGGER.debug(
            "test: ConservacionServiceFacadeTest#testBuscarPPrediosPanelPropietariosPorTramiteId");

        List<PPredio> ppredios;
        Long tramiteId = 2001l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            ppredios = this.conservacionService
                .buscarPPrediosPanelJPropiedadPorTramiteId(tramiteId);

            if (ppredios != null && !ppredios.isEmpty()) {

                for (PPredio p : ppredios) {
                    if (p.getPPersonaPredios() != null &&
                        !p.getPPersonaPredios().isEmpty()) {
                        for (PPersonaPredio pp : p.getPPersonaPredios()) {

                            LOGGER.debug("PerosnaP id: " + pp.getId());

                            if (pp.getPPersona() != null) {

                                LOGGER.debug("PNombre: " +
                                    pp.getPPersona().getNombre());
                            }
                        }
                    }
                }
            }

            Assert.assertNotNull(ppredios);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            Assert.fail();
        }
    }

    /**
     * @author fabio.navarrete
     */
    @Test
    public void testReemplazarPropietarios() {
        LOGGER.debug("ConservacionServiceFacadeTest#testReemplazarPropietarios");
        try {
            List<PPersonaPredio> pPersonaPredios = new ArrayList<PPersonaPredio>();
            PPersonaPredio ppp = this.conservacionService
                .buscarPPersonaPredioPorId(229184L);
            if (ppp != null) {
                pPersonaPredios.add(ppp);
            }

            ppp = this.conservacionService.buscarPPersonaPredioPorId(249299L);
            if (ppp != null) {
                pPersonaPredios.add(ppp);
            }

            ppp = this.conservacionService.buscarPPersonaPredioPorId(249291L);
            if (ppp != null) {
                pPersonaPredios.add(ppp);
            }

            PPredio pPredio = this.conservacionService
                .obtenerPPredioCompletoById(7L);

            this.conservacionService.reemplazarPropietarios(pPredio,
                pPersonaPredios);
            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel ficha matriz
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelFichaMatrizPorTramiteId() {
        LOGGER.debug(
            "test: ConservacionServiceFacadeTest#testBuscarPrediosPanelFichaMatrizPorTramiteId");

        Long tramiteId = 20789L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = this.conservacionService
                .buscarPrediosPanelFichaMatrizPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    LOGGER.debug("Número predial " + p.getNumeroPredial());
                    if (p.getFichaMatrizs() != null &&
                        !p.getFichaMatrizs().isEmpty()) {
                        LOGGER.debug("FichaM " + p.getFichaMatrizs().size());

                        for (FichaMatriz fm : p.getFichaMatrizs()) {
                            LOGGER.debug("Id " + fm.getId());
                        }
                    }
                }

            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de tramites con predios para el panel ubicación de predio
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosPanelUbicacionPredioPorTramiteId() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testBuscarPrediosPanelUbicacionPredioPorTramiteId");

        Long tramiteId = 2115l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answerT = this.conservacionService
                .buscarPrediosPanelUbicacionPredioPorTramiteId(tramiteId);

            if (answerT != null && !answerT.isEmpty()) {

                for (Predio p : answerT) {
                    LOGGER.debug("Número predial " + p.getNumeroPredial());
                    LOGGER.debug("Zonas " + p.getPredioZonas().size());
                    if (p.getPredioZonas() != null &&
                        !p.getPredioZonas().isEmpty()) {
                        LOGGER.debug("ZonaF " +
                            p.getPredioZonas().get(0).getZonaFisica());
                    }
                    LOGGER.debug("Servidumbres " +
                        p.getPredioServidumbres().size());
                    if (p.getPredioServidumbres() != null &&
                        !p.getPredioServidumbres().isEmpty()) {
                        LOGGER.debug("Servidumbre " +
                            p.getPredioServidumbres().get(0)
                                .getServidumbre());
                    }
                }
            }
            assertNotNull(answerT);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para testBuscarPPrediosPanelUbicacionPredioPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPPrediosPanelUbicacionPredioPorTramiteId() {
        LOGGER.debug(
            "test: ConservacionServiceFacadeTest#testBuscarPPrediosPanelUbicacionPredioPorTramiteId");

        List<PPredio> ppredios;
        Long tramiteId = 2001l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            ppredios = this.conservacionService
                .buscarPPrediosPanelUbicacionPredioPorTramiteId(tramiteId);

            if (ppredios != null && !ppredios.isEmpty()) {

                for (PPredio p : ppredios) {
                    LOGGER.debug("Servidumbres " +
                        p.getPPredioServidumbre().size());

                    if (p.getPPredioServidumbre() != null &&
                        !p.getPPredioServidumbre().isEmpty()) {
                        LOGGER.debug("Servidumbre " +
                            p.getPPredioServidumbre().get(0)
                                .getServidumbre());
                    }

                    LOGGER.debug("Direcciones  " +
                        p.getPPredioDireccions().size());

                    if (p.getPPredioDireccions() != null &&
                        !p.getPPredioDireccions().isEmpty()) {
                        LOGGER.debug("Dirección " +
                            p.getPPredioDireccions().get(0)
                                .getDireccion());
                    }

                    LOGGER.debug("Zonas " + p.getPPredioZonas().size());
                    if (p.getPPredioZonas() != null &&
                        !p.getPPredioZonas().isEmpty()) {
                        LOGGER.debug("Zona " +
                            p.getPPredioZonas().get(0).getZonaFisica());
                    }
                }
            }

            Assert.assertNotNull(ppredios);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * Método de prueba para buscar las fotos de un predio asociadas al componente de construcción
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarFotosPredioPorComponenteDeConstruccion() {
        LOGGER.debug(
            "test: ConservacionServiceFacadeTest#testBuscarFotosPredioPorComponenteDeConstruccion");

        Long predioId = 157633L;
        try {
            List<String> fotos = this.conservacionService
                .buscarFotosPredioPorComponenteDeConstruccion(predioId,
                    null);
            LOGGER.debug("num resultado = " + fotos.size());

            for (String f : fotos) {
                LOGGER.debug("Ruta = " + f);
            }

            Assert.assertTrue(fotos != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    /**
     * Método que permite migrar un archivo de foto local dentro del sistema creando la foto y el
     * documento asociados a un predio aleatorio en un municipio especifico y al respectivo
     * componente de construcción
     *
     * @author juan.agudelo
     */
    @SuppressWarnings("unused")
    @Test
    public void migrarFotos() {

        int contador = 0;
        String nombreTemporal = "";
        // Soacha
        // String codigoMunicipio = "25754";
        // Barranquilla
        String codigoMunicipio = "08001";

        // Urbano
        String condicionPropiedad = "01";
        // Rural
        // String condicionPropiedad = "02";

        UsuarioDTO usuario = new UsuarioDTO();
        int cantidadPredios = 0;
        List<String> fotosResultado = new ArrayList<String>();

        // Directorio de lectura de las fotos
        String dir = null;
        // dir = "C://Documents and Settings//juan.agudelo//Desktop//URBANO";
        if (dir != null) {
            File directorio = new File(dir);
            String[] archivos = directorio.list();

            usuario.setLogin("juan.agudelo");
            for (String ruta : archivos) {
                fotosResultado.add(ruta);
            }

            if (fotosResultado != null && !fotosResultado.isEmpty()) {
                cantidadPredios = fotosResultado.size();
            }

            try {
                /*
                 * this.predios = this.conservacionService
                 * .buscarPrediosAleatoriosMunicipio(codigoMunicipio, condicionPropiedad,
                 * cantidadPredios);
                 */

                this.predioPos = 0;

                if (this.predios != null && !this.predios.isEmpty()) {

                    File f = null;
                    Foto foto = null;
                    Documento doc = null;
                    TipoDocumento td = null;
                    Predio predioSeleccionado = null;
                    List<Foto> fotosSeleccionadas = null;

                    for (String archivo : fotosResultado) {

                        String directorioTemporal = "//srvsigcmaps.igac.gov.co/Temp/Temporales/";
                        f = new File(directorioTemporal + archivo);
                        doc = new Documento();
                        foto = new Foto();

                        if (!nombreTemporal.equals(archivo.substring(0, 22))) {
                            this.predioPos++;
                            predioSeleccionado = conseguirSiguientePredio();
                            nombreTemporal = archivo.substring(0, 22);
                        }
                        fotosSeleccionadas = new ArrayList<Foto>();

                        // subir archivo
                        FileOutputStream fileOutputStream = null;

                        try {
                            fileOutputStream = new FileOutputStream(f);

                            byte[] buffer = new byte[Math.round(2048)];

                            int bulk;

                            String rutaLocal = dir + "//";
                            InputStream inputStream = new FileInputStream(
                                new File(rutaLocal + archivo));
                            while (true) {
                                bulk = inputStream.read(buffer);
                                if (bulk == -1) {
                                    break;
                                }
                                fileOutputStream.write(buffer, 0, bulk);

                            }
                            fileOutputStream.flush();
                            fileOutputStream.close();
                            inputStream.close();

                        } catch (IOException e) {
                            LOGGER.error(e.getMessage());
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage());
                        }

                        if (predioSeleccionado != null) {
                            doc.setArchivo(f.getName());
                            doc.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                            td = new TipoDocumento();
                            td.setId(ETipoDocumentoId.FOTOGRAFIA.getId());
                            doc.setTipoDocumento(td);
                            doc.setPredioId(predioSeleccionado.getId());
                            doc.setUsuarioCreador(usuario.getLogin());
                            doc.setBloqueado(ESiNo.NO.getCodigo());
                            doc.setUsuarioLog(usuario.getLogin());
                            doc.setFechaLog(new Date());

                            if (f.getName().contains("FAC")) {
                                foto.setTipo(EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS
                                    .getCodigo());
                            } else if (f.getName().contains("NOM")) {
                                foto.setTipo(EFotoTipo.ACABADOS_PRINCIPALES_NOMENCLATURA
                                    .getCodigo());
                            } else if (f.getName().contains("ACA")) {
                                foto.setTipo(EFotoTipo.ACABADOS_PRINCIPALES
                                    .getCodigo());
                            } else if (f.getName().contains("BAN")) {
                                foto.setTipo(EFotoTipo.BANIO.getCodigo());
                            } else if (f.getName().contains("COC")) {
                                foto.setTipo(EFotoTipo.COCINA.getCodigo());
                            } else if (f.getName().contains("EST")) {
                                foto.setTipo(EFotoTipo.ESTRUCTURA.getCodigo());
                            } else {
                                foto.setTipo(EFotoTipo.COMPLEMENTO_INDUSTRIA_CERCHAS_ALTURA
                                    .getCodigo());
                            }

                            Documento d = this.tramiteService.guardarDocumento(
                                usuario, doc);

                            foto.setDocumento(d);
                            foto.setFecha(new Date());
                            foto.setUsuarioLog(usuario.getLogin());
                            foto.setFechaLog(new Date());
                            foto.setPredio(predioSeleccionado);
                            fotosSeleccionadas.add(foto);

                            this.conservacionService
                                .guardarFotos(fotosSeleccionadas);

                            LOGGER.debug("Foto añadida" + contador++);
                        }
                    }
                }
            } catch (ExcepcionSNC e) {
                throw new ExcepcionSNC("mi codigo de excepcion",
                    ESeveridadExcepcionSNC.ERROR, e.getMensaje(), e);
            }

        }
    }

    /**
     * Retorna el siguiente predio en la busqueda
     *
     * @author juan.agudelo
     * @return
     */
    private Predio conseguirSiguientePredio() {
        if (this.predioPos <= this.predios.size()) {
            return this.predios.get(this.predioPos);
        } else {
            return null;
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de documento en alfresco
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaPorNumeroYTipoIdentificacion() {
        LOGGER.debug(
            "test: ConservacionServiceFacadeTest#testBuscarPersonaPorNumeroYTipoIdentificacion");

        String numeroIdentificacion = "764925";
        String tipoIdentificacion = "CC";
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Persona> answer = this.conservacionService
                .buscarPersonaPorNumeroYTipoIdentificacion(
                    tipoIdentificacion, numeroIdentificacion);

            if (answer != null && !answer.isEmpty()) {

                for (Persona p : answer) {
                    LOGGER.debug("Nombre completo " + p.getNombre());
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testContarPrediosBloqueo() {
        LOGGER.debug("tests: ConservacionServiceFacadeTest#testContarPrediosBloqueo");

        FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtro.setDepartamento(departamento);
        filtro.setMunicipio(municipio);
        filtro.setDepartamentoCodigo("25");
        filtro.setMunicipioCodigo("754");
        filtro.setTipoAvaluo("02");
        filtro.setSectorCodigo("16");
        filtro.setComunaCodigo("04");
        filtro.setBarrioCodigo("17");
        filtro.setManzanaVeredaCodigo("1105");
        filtro.setPredio("7934");
        filtro.setCondicionPropiedad("1");
        filtro.setEdificio("05");
        filtro.setPiso("11");
        filtro.setUnidad("7118");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Integer answer = this.conservacionService
                .contarPrediosBloqueo(filtro);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer);
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test(threadPoolSize = 40, invocationCount = 1000, timeOut = 30000)
    //@Test
    public void testContarPredios() {
        LOGGER.debug("tests: ConservacionServiceFacadeTest#testContarPredios");
        FiltroDatosConsultaPredio filtro = new FiltroDatosConsultaPredio();
        filtro.setDepartamentoId("25");
        /*
         * filtro.setDepartamentoCodigo("25"); filtro.setMunicipioCodigo("754");
         * filtro.setTipoAvaluo("02"); filtro.setSectorCodigo("16"); filtro.setComunaCodigo("04");
         * filtro.setBarrioCodigo("17"); filtro.setManzanaVeredaCodigo("1105");
         * filtro.setPredio("7934"); filtro.setCondicionPropiedad("1"); filtro.setEdificio("05");
         * filtro.setPiso("11"); filtro.setUnidad("7118");
         */
        try {
            //LOGGER.info("Departamento: " +filtro.getDepartamentoId());
            Integer answer = this.conservacionService.contarPredios(filtro);
            assertNotNull(answer);
            LOGGER.info("Tamaño: " + answer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * @author juan.mendez
     */
    @Test(threadPoolSize = 60, invocationCount = 2000, timeOut = 25000)
    //@Test
    public void testContarYBuscarPredios() {
        LOGGER.debug("tests: ConservacionServiceFacadeTest#testContarYBuscarPredios");
        FiltroDatosConsultaPredio filtro = new FiltroDatosConsultaPredio();
        filtro.setDepartamentoId("25");
        /*
         * filtro.setDepartamentoCodigo("25"); filtro.setMunicipioCodigo("754");
         * filtro.setTipoAvaluo("02"); filtro.setSectorCodigo("16"); filtro.setComunaCodigo("04");
         * filtro.setBarrioCodigo("17"); filtro.setManzanaVeredaCodigo("1105");
         * filtro.setPredio("7934"); filtro.setCondicionPropiedad("1"); filtro.setEdificio("05");
         * filtro.setPiso("11"); filtro.setUnidad("7118");
         */
        try {
            //LOGGER.info("Departamento: " +filtro.getDepartamentoId());
            Integer answer = this.conservacionService.contarPredios(filtro);
            assertNotNull(answer);

            int pageSize = 200;
            int totalPages = answer / 50;
            int firstPage = random.nextInt(totalPages);

            //LOGGER.info("Tamaño: " + answer);
            LOGGER.info("firstPage: " + firstPage);

            List<Object[]> results = this.conservacionService.buscarPredios(filtro, firstPage,
                pageSize);
            assertNotNull(results);
            //for (Object[] object : results) {
            //LOGGER.info("object: " + object[0] );
            //LOGGER.info("object: " + object[1] );
            //LOGGER.info("object: " + object[2] );
            //LOGGER.info("object: " + object[3] );
            //}
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPrediosBloqueo() {
        LOGGER.debug("tests: ConservacionServiceFacadeTest#testBuscarPrediosBloqueo");

        FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtro.setDepartamento(departamento);
        filtro.setMunicipio(municipio);
        filtro.setDepartamentoCodigo("25");
        filtro.setMunicipioCodigo("754");
        filtro.setTipoAvaluo("02");
        filtro.setSectorCodigo("16");
        filtro.setComunaCodigo("04");
        filtro.setBarrioCodigo("17");
        filtro.setManzanaVeredaCodigo("1105");
        filtro.setPredio("7934");
        filtro.setCondicionPropiedad("1");
        filtro.setEdificio("05");
        filtro.setPiso("11");
        filtro.setUnidad("7118");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<PredioBloqueo> answer = this.conservacionService
                .buscarPrediosBloqueo(filtro);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer.size());

                if (!answer.isEmpty()) {

                    for (PredioBloqueo pb : answer) {

                        LOGGER.debug("Id: " + pb.getId());
                        LOGGER.debug("Fecha inicia bloqueo: " +
                            pb.getFechaInicioBloqueo());
                        LOGGER.debug("Fecha termina bloqueo: " +
                            pb.getFechaTerminaBloqueo());
                        LOGGER.debug("Fecha desbloqueo: " +
                            pb.getFechaDesbloqueo());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testBuscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo");

        FiltroDatosConsultaPrediosBloqueo filtro = crearFiltroPredioBloqueo();

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.conservacionService
                .buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(filtro);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer.size());

                if (!answer.isEmpty()) {

                    for (Predio pb : answer) {

                        LOGGER.debug("Id: " + pb.getId());
                        LOGGER.debug("NP: " + pb.getNumeroPredial());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que crea un FiltroDatosConsultaPrediosBloqueo con los datos base
     *
     * @author juan.agudelo
     */
    private FiltroDatosConsultaPrediosBloqueo crearFiltroPredioBloqueo() {

        FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtro.setDepartamento(departamento);
        filtro.setMunicipio(municipio);
        filtro.setDepartamentoCodigo("25");
        filtro.setMunicipioCodigo("754");
        filtro.setTipoAvaluo("02");
        filtro.setSectorCodigo("16");
        filtro.setComunaCodigo("04");
        filtro.setBarrioCodigo("17");
        filtro.setManzanaVeredaCodigo("1105");
        filtro.setPredio("7934");
        filtro.setCondicionPropiedad("1");
        filtro.setEdificio("05");
        filtro.setPiso("11");
        filtro.setUnidad("7118");

        return filtro;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda de personas por nombre, tipo y número de documento
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPersonaPorNombrePartesTipoDocNumDoc() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testBuscarPersonaPorNombrePartesTipoDocNumDoc");

        Persona persona = new Persona();
        String numeroIdentificacion = "764925";
        String tipoIdentificacion = "CC";
        String primerNombre = "Fredy";
        String segundoNombre = "Segundo";
        String primerApellido = "Apellido1";
        String segundoApellido = "Apeelido2";

        persona.setTipoIdentificacionValor(tipoIdentificacion);
        persona.setNumeroIdentificacion(numeroIdentificacion);
        persona.setPrimerNombre(primerNombre);
        persona.setSegundoNombre(segundoNombre);
        persona.setPrimerApellido(primerApellido);
        persona.setSegundoApellido(segundoApellido);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            Persona answer = this.conservacionService
                .buscarPersonaPorNombrePartesTipoDocNumDoc(persona);

            if (answer != null) {

                LOGGER.debug("Nombre completo " + answer.getNombre());

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testBuscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testBuscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo");

        FiltroDatosConsultaPrediosBloqueo filtroInicial = crearBaseHastaManzanaFiltroPredioBloqueo();
        FiltroDatosConsultaPrediosBloqueo filtroFinal = crearBaseHastaManzanaFiltroPredioBloqueo();

        filtroInicial.setPredio("0000");
        filtroFinal.setPredio("9999");
        filtroInicial.setCondicionPropiedad("0");
        filtroFinal.setCondicionPropiedad("9");
        filtroInicial.setEdificio("00");
        filtroFinal.setEdificio("99");
        filtroInicial.setPiso("00");
        filtroFinal.setPiso("99");
        filtroInicial.setUnidad("0000");
        filtroFinal.setUnidad("9999");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.conservacionService
                .buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
                    filtroInicial, filtroFinal);

            if (answer != null) {

                LOGGER.debug("Tamaño: " + answer.size());

                if (!answer.isEmpty()) {

                    for (Predio pb : answer) {

                        LOGGER.debug("Id: " + pb.getId());
                        LOGGER.debug("NP: " + pb.getNumeroPredial());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que crea la base hasta manzana de un FiltroDatosConsultaPrediosBloqueo
     *
     * @author juan.agudelo
     */
    private FiltroDatosConsultaPrediosBloqueo crearBaseHastaManzanaFiltroPredioBloqueo() {

        FiltroDatosConsultaPrediosBloqueo filtro = new FiltroDatosConsultaPrediosBloqueo();
        Departamento departamento = new Departamento();
        Municipio municipio = new Municipio();

        departamento.setCodigo("25");
        municipio.setCodigo("25754");

        filtro.setDepartamento(departamento);
        filtro.setMunicipio(municipio);
        filtro.setDepartamentoCodigo("25");
        filtro.setMunicipioCodigo("754");
        filtro.setTipoAvaluo("02");
        filtro.setSectorCodigo("16");
        // TODO no crea comuna código pq hasta el momento nos e ha implementado
        // en la entidad, los dos codigos van en barrio código
        // filtro.setComunaCodigo("04");
        filtro.setBarrioCodigo("0417");
        filtro.setManzanaVeredaCodigo("1105");

        return filtro;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testObtenerPPredioCompletoById() {
        LOGGER.debug("tests: ConservacionServiceFacadeTest#testObtenerPPredioCompletoById");

        Long id = 1113014L;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            PPredio answer = this.conservacionService
                .obtenerPPredioCompletoById(id);

            if (answer != null) {

                LOGGER.debug("Tamaño: " +
                    answer.getPReferenciaCartograficas().size());

                if (!answer.getPReferenciaCartograficas().isEmpty()) {

                    for (PReferenciaCartografica pb : answer
                        .getPReferenciaCartograficas()) {

                        LOGGER.debug("Id: " + pb.getId());
                    }
                }
            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
//-------------------------------------------------------------------------------------------------

    /**
     * test de la búsqueda de predios usando el procedimiento almacenado haciendo que retorne solo
     * el conteo
     *
     * @author pedro.garcia
     */
    @Test
    public void testBuscarPrediosConteo() {

        LOGGER.debug("integration test ConservacionServiceFacadeTest#testBuscarPrediosConteo ...");
        List<Object[]> answer;
        FiltroDatosConsultaPredio datosConsultaPredio;

        //D: prueba para contar
        datosConsultaPredio = new FiltroDatosConsultaPredio();

        LOGGER.debug("... probando conteo (sin criterios de búsqueda)...");
        answer = this.conservacionService.buscarPredios(datosConsultaPredio);

        if (answer == null || answer.isEmpty()) {
            Assert.fail("la respuesta llegó null o vacía");
        } else {
            LOGGER.debug("conteo = " + answer.get(0)[0].toString());
        }
    }
//----------------------

    /**
     * test de la búsqueda de predios usando el procedimiento almacenado
     *
     * @author pedro.garcia
     */
    @Test
    public void testBuscarPredios() {

        LOGGER.debug("integration test ConservacionServiceFacadeTest#testBuscarPredios ...");
        List<Object[]> answer;
        FiltroDatosConsultaPredio datosConsultaPredio;
        int ok = 0;
        int desde, hasta;

        datosConsultaPredio = new FiltroDatosConsultaPredio();

        //D: paila ... se muetre porque se demora mucho
        //LOGGER.debug("probando búsqueda de todos sin criterios ...");
        LOGGER.debug("probando con criterios y SIN restringir el número de filas...");
        datosConsultaPredio.setDepartamentoId("25");
        datosConsultaPredio.setMunicipioId("25754");
        datosConsultaPredio.setPrimerNombrePropietario("PEDRO");
        answer = this.conservacionService.buscarPredios(datosConsultaPredio);
        if (answer == null || answer.isEmpty()) {
            Assert.fail("la respuesta llegó null o vacía");
        } else {
            LOGGER.debug("trajo = " + answer.size() + " registros");
            ok++;
        }

        LOGGER.debug("probando con criterios y restringiendo el número de filas...");
        datosConsultaPredio.setDepartamentoId("25");
        datosConsultaPredio.setMunicipioId("25754");
        datosConsultaPredio.setPrimerNombrePropietario("PEDRO");
        desde = 10;
        hasta = 20;
        answer = this.conservacionService.buscarPredios(datosConsultaPredio, desde, hasta);
        if (answer == null || answer.isEmpty()) {
            Assert.fail("la respuesta llegó null o vacía");
        } else {
            if (answer.size() != (hasta - desde + 1)) {
                Assert.fail("no trajo el número de registros esperado " + (hasta - desde + 1) +
                    " sino " +
                    answer.size());
            } else {
                LOGGER.debug("trajo = " + answer.size() + " registros");
                ok++;
            }
        }

        if (ok >= 2) {
            Assert.assertTrue(true);
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método de prueba para testBuscarPPrediosPanelFichaMatrizPorTramiteId
     *
     * @author juan.agudelo
     */
    @Test
    public void testBuscarPPrediosPanelFichaMatrizPorTramiteId() {
        LOGGER.debug("ConservacionServiceFacadeTest#testBuscarPPrediosPanelFichaMatrizPorTramiteId");

        List<PPredio> ppredios;
        Long tramiteId = 20789l;
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            ppredios = this.conservacionService
                .buscarPPrediosPanelFichaMatrizPorTramiteId(tramiteId);

            if (ppredios != null && !ppredios.isEmpty()) {

                for (PPredio p : ppredios) {
                    LOGGER.debug("Ficha Matriz " + p.getPFichaMatrizs().size());

                    if (p.getPFichaMatrizs() != null &&
                        !p.getPFichaMatrizs().isEmpty()) {
                        LOGGER.debug("Total torres " +
                            p.getPFichaMatrizs().get(0).getTotalTorres());
                    }

                    if (p.getPPredioDireccions() != null &&
                        !p.getPPredioDireccions().isEmpty()) {
                        LOGGER.debug("Dirección " +
                            p.getPPredioDireccions().get(0)
                                .getDireccion());
                    }

                }
            }

            Assert.assertNotNull(ppredios);
            LOGGER.debug("********************SALIMOS**********************");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba busqueda predios colindantes por número predial
     *
     * @author juan.agudelo
     */
    @Test
    public void testFindPrediosColindantesByNumeroPredial() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testFindPrediosColindantesByNumeroPredial");

        String numeroPredial = "080010101000000280012000000000";

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<Predio> answer = this.conservacionService
                .buscarPrediosColindantesPorNumeroPredial(numeroPredial);

            if (answer != null && !answer.isEmpty()) {

                for (Predio temp : answer) {
                    LOGGER.debug("Id predio colindante: " + temp.getId());
                    LOGGER.debug("Número predial colindante: " +
                        temp.getNumeroPredial());
                }

            }
            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prueba para verificar si un predio se encuentra cancelado
     *
     * @author juan.agudelo
     */
    @Test
    public void testIsPredioCanceladoByPredioIdOrNumeroPredial() {
        LOGGER.debug(
            "tests: ConservacionServiceFacadeTest#testIsPredioCanceladoByPredioIdOrNumeroPredial");

        String numeroPredial = null;
        Long predioId = null;

        //numeroPredial = "257540102000011650001500000032";
        predioId = 7L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            boolean answer = this.conservacionService
                .esPredioCanceladoPorPredioIdONumeroPredial(predioId,
                    numeroPredial);

            String cancelado = answer == true ? ESiNo.SI.getCodigo() : ESiNo.NO
                .getCodigo();
            LOGGER.debug("Predio cancelado: " + cancelado);

            assertNotNull(answer);
            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetPredioBloqueosByNumeroPredial() {
        LOGGER.debug("tests: ConservacionServiceFacadeTest#testGetPredioBloqueosByNumeroPredial...");

        String numeroPredial = "080010109000003910015000000000";

        try {
            List<PredioBloqueo> resultado = this.conservacionService
                .buscarPrediosBloqueoByNumeroPredial(numeroPredial);
            LOGGER.debug("num resultado = " + resultado.size());

            if (resultado.size() > 0) {
                for (int i = 0; i < resultado.size(); i++) {
                    LOGGER.debug("id = " + resultado.get(i).getId());
                }
            }
            Assert.assertNotNull(resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }

    // ------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Test
    public void testGetPersonaBloqueosByPersonaId() {
        LOGGER.debug("test: ConservacionServiceFacadeTest#testGetPersonaBloqueosByPersonaId...");

        Long idPersona = 444996L;

        try {
            List<PersonaBloqueo> pb = this.conservacionService
                .obtenerPersonaBloqueosPorPersonaId(idPersona);

            LOGGER.debug("num resultado = " + pb.size());

            for (PersonaBloqueo pbloq : pb) {
                LOGGER.debug("id " + pbloq.getId());
                LOGGER.debug("Fecha inicial = " + pbloq.getFechaInicioBloqueo());
                LOGGER.debug("Fecha final = " + pbloq.getFechaTerminaBloqueo());
                LOGGER.debug("Motivo = " + pbloq.getMotivoBloqueo());
                LOGGER.debug("Identificación = " +
                    pbloq.getPersona().getNumeroIdentificacion());

                if (pbloq.getDocumentoSoporteDesbloqueo() != null) {
                    LOGGER.debug("Documento Desbloqueo = " +
                        pbloq.getDocumentoSoporteDesbloqueo()
                            .getArchivo());
                }
            }

            Assert.assertTrue(pb != null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            assertFalse(true);
        }
    }
//-------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     */
    @Test
    public void testObtenerNumsTerrenoPorCondPropiedadEnManzana() {
        LOGGER.debug(
            "integration test ConservacionServiceFacadeTest#testObtenerNumsTerrenoPorCondPropiedadEnManzana ...");

        String manzana = "08001000200000000";
        ArrayList<String> answer, condicionesPropiedad = new ArrayList<String>();

        condicionesPropiedad.add("5");
        condicionesPropiedad.add("6");

        try {
            answer = (ArrayList<String>) this.conservacionService.
                obtenerNumsTerrenoPorCondPropiedadEnManzana(manzana, condicionesPropiedad);

            //D: solo es necesario que no se totee en la consulta para que pase el test
            assertTrue(true);

            if (answer != null && !answer.isEmpty()) {
                for (String terreno : answer) {
                    LOGGER.debug("terreno: " + terreno);
                }
            }
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }
    }
//-------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     */
    @Test
    public void testGenerarNumeroUnidadPorTerreno() {
        LOGGER.debug(
            "integration test ConservacionServiceFacadeTest#testGenerarNumeroUnidadPorTerreno ...");

        String terreno = "080010002000000010003";
        String answer;

        try {
            answer = this.conservacionService.generarNumeroUnidadPorTerreno(terreno);

            //D: solo es necesario que no se totee en la consulta para que pase el test
            assertTrue(true);

            if (answer != null && !answer.isEmpty()) {
                LOGGER.debug("resultado: " + answer);
            }
        } catch (Exception e) {
            fail("error: ");
            LOGGER.error(e.getMessage(), e);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testObtenerPPredioDatosJustificPropiedad() {

        LOGGER.debug(
            "integration test ConservacionServiceFacadeTest#testObtenerPPredioDatosJustificPropiedad ...");

        Long ppredioId = 516796L;

        PPredio answer;

        try {
            answer = this.conservacionService.obtenerPPredioDatosJustificPropiedad(ppredioId);
            assertNotNull(answer);
        } catch (Exception ex) {
            fail("error: ");
            LOGGER.error(ex.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Test
    public void testBuscarFotografiasPredio() {

        LOGGER.debug(
            "integration test ConservacionServiceFacadeTest#testBuscarFotografiasPredio ...");

        Long idPredio;
        List<Foto> answer;

        idPredio = 525668L;

        try {
            answer = this.conservacionService.buscarFotografiasPredio(idPredio);
            LOGGER.debug("passed");
            LOGGER.debug("   fetched " + answer.size() + " rows");
        } catch (Exception ex) {
            fail("error: ");
            LOGGER.error(ex.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * este método se hizo más para comprobar qué pasa cuando se genera un error en la capa de
     * negocio.
     *
     * @author pedro.garcia
     */
    @Test
    public void testGuardarFotos() {

        LOGGER.debug("integration test ConservacionServiceFacadeTest#testGuardarFotos ...");

        List<Foto> fotos, answer;

        fotos = null;

        try {
            answer = this.conservacionService.guardarFotos(fotos);
            LOGGER.debug("passed");
        } catch (Exception ex) {
            //D si se pone el fail antes, cuando se ejecuta la prueba desde línea de comandos no me deja
            // ver lo que quiero imprimir después
            //fail("error: ");

            //D: la excepción llega sin mensaje, lo útil es el stacktrace
            LOGGER.error("error: ");
            ex.printStackTrace();
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para probar la colindancia relativa de un grupo de predios
     *
     * @author felipe.cadena
     */
    @Test
    public void testDeterminarColindancia() {

        LOGGER.debug("integration test ConservacionLocal#determinarColindancia");

        boolean answer;
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setLogin(SOACHA);
        List<Predio> predios = new ArrayList<Predio>();
        Predio predioBase = conservacionService.buscarPredioPorNumeroPredialParaAvaluoComercial(
            "080010101000000170005000000000");
        Predio p = conservacionService.buscarPredioPorNumeroPredialParaAvaluoComercial(
            "080010101000000170005000000000");
        predios.add(p);
        p = conservacionService.buscarPredioPorNumeroPredialParaAvaluoComercial(
            "080010101000000170004000000000");
        predios.add(p);
        p = conservacionService.buscarPredioPorNumeroPredialParaAvaluoComercial(
            "080010101000000170006000000000");
        predios.add(p);
        p = conservacionService.buscarPredioPorNumeroPredialParaAvaluoComercial(
            "080010101000000170031000000000");
        predios.add(p);
        p = conservacionService.buscarPredioPorNumeroPredialParaAvaluoComercial(
            "086380100000002170024000000000");
        predios.add(p);

        try {
            predios = this.conservacionService.determinarColindancia(predios, predioBase, usuario,
                false);
            LOGGER.debug("RES " + predios.size());
            LOGGER.debug("passed");
        } catch (Exception ex) {
            //D si se pone el fail antes, cuando se ejecuta la prueba desde línea de comandos no me deja
            // ver lo que quiero imprimir después
            //fail("error: ");

            //D: la excepción llega sin mensaje, lo útil es el stacktrace
            LOGGER.error("error: ");
            ex.printStackTrace();
        }

    }

    /**
     * Test consultaWebService
     *
     * @author andres.eslava
     */
    @Test
    public void testBuscaPrediosConsultaCatastralWebService() {

        LOGGER.debug("ConsultaCatastralWebServiceTest#testBuscaPrediosConsultaCatastralWebService");

        try {

            FiltroConsultaCatastralWebService filtro = new FiltroConsultaCatastralWebService();
            filtro.setPredio_numeroPredialNuevo("257540104000001760001500000004");
            List<Predio> prediosRespuesta = new ArrayList<Predio>();
            prediosRespuesta = this.conservacionService.buscaPrediosConsultaCatastralWebService(
                filtro, 1L);
            LOGGER.debug(
                "***********************************************************************************");
            for (Predio unPredio : prediosRespuesta) {
                LOGGER.debug(unPredio.getNumeroPredial());
            }
            Assert.assertNotNull(prediosRespuesta);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test actualizacionPPredioZonas
     *
     * @author felipe.cadena
     */
    @Test
    public void testActualizacionPPredioZonas() {

        LOGGER.debug("ConservacionServiceFacadeTest#testActualizacionPPredioZonas");

        try {

            Tramite tramite = this.tramiteService.buscarTramiteCompletoConResolucion(662557L);
            UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico17");

            this.tramiteService.actualizacionPPredioZonas(tramite, usuario, Boolean.FALSE);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test testGenerarActualizarFichaPredialDigital
     *
     * @author javier.aponte
     */
    @Test
    public void testActualizarZonasPorPredio() {

        LOGGER.debug("ConservacionServiceFacadeTest#testActualizarZonasPorPredio");

        try {

            UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruquindio6");
            Long idJob = 101026L;
//            HashMap<String, List<ZonasFisicasGeoeconomicasVO>> zonasGeograficas = productoCatastralJobDAO.obtenerXMLResultadoObtenerZonasHomegeneas(idJob);

//            this.tramiteService.actualizarZonasPorPredio(usuario, zonasGeograficas);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test testActualizarFichaPredialDigital
     *
     * @author javier.aponte
     */
    @Test
    public void testActualizarFichaPredialDigital() {

        LOGGER.debug("ConservacionServiceFacadeTest#testGenerarActualizarFichaPredialDigital");

        Long[] idsTramites = {
            39685L, 95845L, 97151L, 98414L, 102397L, 108988L, 116793L, 123384L, 124050L, 133575L,
            139905L, 140458L, 146121L, 157447L, 164107L, 164154L, 189193L, 194682L, 196660L, 196809L,
            197732L, 197800L, 198660L, 198920L, 199289L, 201278L, 208970L, 209783L, 211625L, 228631L,
            242092L, 242854L, 245159L, 246260L, 251557L, 255130L, 263372L, 267691L, 269556L, 270425L,
            272933L, 275692L, 276186L, 277035L, 278057L, 278918L, 283990L, 284350L, 285358L, 285590L,
            290557L, 290991L, 291825L, 294669L, 297315L, 299316L, 304277L, 304512L, 304724L, 305835L,
            310164L, 310621L, 310989L, 316778L, 316944L, 317252L, 317439L, 317799L, 318084L, 320899L,
            321304L, 321703L, 322066L, 322573L, 324206L, 324526L, 324634L, 327086L, 327568L, 328349L,
            328396L, 347882L, 347999L, 348188L, 348974L, 348980L, 349070L, 349256L, 349313L, 349472L,
            349643L, 350578L, 350629L, 351598L, 351619L, 351633L, 351885L, 351943L, 352128L, 352182L,
            352231L, 352280L, 352331L, 352428L, 352602L, 352621L, 352726L, 352950L,
            352952L, 353274L, 353422L, 353447L, 354164L, 354358L, 354401L, 354687L, 354944L, 354966L,
            355493L, 355500L, 356087L, 356226L, 356243L, 356374L, 356759L, 356927L, 356935L, 356964L,
            357044L, 357486L, 357569L, 357961L, 358444L, 358525L, 358528L, 358723L, 358766L, 359290L,
            359371L, 359566L, 359706L, 360207L, 360372L, 360482L, 360656L, 361445L, 361543L, 361546L,
            361635L, 361901L, 362244L, 362724L, 362826L, 363026L, 363083L, 363104L, 363109L, 363431L,
            363979L, 364214L, 364221L, 364245L, 364489L, 364905L, 364907L, 365339L, 365389L, 365578L,
            365993L, 365994L, 366060L, 366161L, 366217L, 366517L, 366694L, 367094L, 367224L, 367301L,
            367374L, 367540L, 367545L, 367558L, 367604L, 367761L, 367782L, 368253L, 368464L, 369322L,
            369555L, 369562L, 369710L, 369758L, 369772L, 369839L, 370024L, 370046L, 370048L, 370078L,
            370079L, 370783L, 371030L, 371351L, 371454L, 371625L, 371901L, 371963L, 372175L, 372237L,
            372252L, 372265L, 372274L, 372458L, 372460L, 373117L, 373153L, 373179L, 373203L, 373210L,
            373252L, 373319L, 373324L, 373508L, 373962L, 373963L, 374064L, 374073L, 374103L, 374157L,
            374160L, 374165L, 374167L, 374181L, 374195L, 374302L, 374317L, 374336L, 374348L, 374384L,
            374492L, 374512L, 374566L, 374657L, 374973L, 375013L, 375107L, 375111L, 375121L, 375135L,
            375160L, 375170L, 375190L, 375231L, 375237L, 376589L, 376667L, 376856L, 376903L, 377032L,
            377066L, 377104L, 377230L, 377364L, 377477L, 377537L, 377580L, 377727L, 377774L, 377928L,
            378535L, 378570L, 378575L, 378761L, 378835L, 378885L, 379036L, 379040L, 379755L, 379828L,
            379907L, 380115L, 380222L, 380296L, 380421L, 380463L, 380530L, 380605L, 380636L, 380903L,
            380915L, 380918L, 380954L, 381104L, 381314L, 382490L, 382491L, 383121L, 383378L, 383382L,
            383566L, 383715L, 384172L, 384425L, 386019L
        };

        try {

            UsuarioDTO usuario;

            Tramite tramite;

            boolean fpd;

            for (Long tramiteTemp : idsTramites) {
                tramite = this.tramiteService.consultarTramite(tramiteTemp);
                usuario = this.generalesService.getCacheUsuario(tramite.getUsuarioLog());

                fpd = this.conservacionService.actualizarFichaPredialDigital(tramite.getId(),
                    usuario, true);
                assertTrue(fpd);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test testActualizarCartaCatastralUrbana
     *
     * @author javier.aponte
     */
    @Test
    public void testActualizarCartaCatastralUrbana() {

        LOGGER.debug("ConservacionServiceFacadeTest#testActualizarCartaCatastralUrbana");

        Long[] idsTramites = {
            39685L, 95845L, 97151L, 98414L, 102397L, 108988L, 116793L, 123384L, 124050L, 133575L,
            139905L, 140458L, 146121L, 157447L, 164107L, 164154L, 189193L, 194682L, 196660L, 196809L,
            197732L, 197800L, 198660L, 198920L, 199289L, 201278L, 208970L, 209783L, 211625L, 228631L,
            242092L, 242854L, 245159L, 246260L, 251557L, 255130L, 263372L, 267691L, 269556L, 270425L,
            272933L, 275692L, 276186L, 277035L, 278057L, 278918L, 283990L, 284350L, 285358L, 285590L,
            290557L, 290991L, 291825L, 294669L, 297315L, 299316L, 304277L, 304512L, 304724L, 305835L,
            310164L, 310621L, 310989L, 316778L, 316944L, 317252L, 317439L, 317799L, 318084L, 320899L,
            321304L, 321703L, 322066L, 322573L, 324206L, 324526L, 324634L, 327086L, 327568L, 328349L,
            328396L, 347882L, 347999L, 348188L, 348974L, 348980L, 349070L, 349256L, 349313L, 349472L,
            349643L, 350578L, 350629L, 351598L, 351619L, 351633L, 351885L, 351943L, 352128L, 352182L,
            352231L, 352280L, 352331L, 352428L, 352602L, 352621L, 352726L, 352950L,
            352952L, 353274L, 353422L, 353447L, 354164L, 354358L, 354401L, 354687L, 354944L, 354966L,
            355493L, 355500L, 356087L, 356226L, 356243L, 356374L, 356759L, 356927L, 356935L, 356964L,
            357044L, 357486L, 357569L, 357961L, 358444L, 358525L, 358528L, 358723L, 358766L, 359290L,
            359371L, 359566L, 359706L, 360207L, 360372L, 360482L, 360656L, 361445L, 361543L, 361546L,
            361635L, 361901L, 362244L, 362724L, 362826L, 363026L, 363083L, 363104L, 363109L, 363431L,
            363979L, 364214L, 364221L, 364245L, 364489L, 364905L, 364907L, 365339L, 365389L, 365578L,
            365993L, 365994L, 366060L, 366161L, 366217L, 366517L, 366694L, 367094L, 367224L, 367301L,
            367374L, 367540L, 367545L, 367558L, 367604L, 367761L, 367782L, 368253L, 368464L, 369322L,
            369555L, 369562L, 369710L, 369758L, 369772L, 369839L, 370024L, 370046L, 370048L, 370078L,
            370079L, 370783L, 371030L, 371351L, 371454L, 371625L, 371901L, 371963L, 372175L, 372237L,
            372252L, 372265L, 372274L, 372458L, 372460L, 373117L, 373153L, 373179L, 373203L, 373210L,
            373252L, 373319L, 373324L, 373508L, 373962L, 373963L, 374064L, 374073L, 374103L, 374157L,
            374160L, 374165L, 374167L, 374181L, 374195L, 374302L, 374317L, 374336L, 374348L, 374384L,
            374492L, 374512L, 374566L, 374657L, 374973L, 375013L, 375107L, 375111L, 375121L, 375135L,
            375160L, 375170L, 375190L, 375231L, 375237L, 376589L, 376667L, 376856L, 376903L, 377032L,
            377066L, 377104L, 377230L, 377364L, 377477L, 377537L, 377580L, 377727L, 377774L, 377928L,
            378535L, 378570L, 378575L, 378761L, 378835L, 378885L, 379036L, 379040L, 379755L, 379828L,
            379907L, 380115L, 380222L, 380296L, 380421L, 380463L, 380530L, 380605L, 380636L, 380903L,
            380915L, 380918L, 380954L, 381104L, 381314L, 382490L, 382491L, 383121L, 383378L, 383382L,
            383566L, 383715L, 384172L, 384425L, 386019L
        };

        try {

            UsuarioDTO usuario;

            Tramite tramite;
            boolean carta;

            for (Long tramiteTemp : idsTramites) {
                tramite = this.tramiteService.consultarTramite(tramiteTemp);
                usuario = this.generalesService.getCacheUsuario(tramite.getUsuarioLog());

                carta = this.conservacionService.actualizarCartaCatastralUrbana(tramite.getId(),
                    usuario, true);
                assertTrue(carta);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author javier.aponte
     */
    /* @lorena.salamanca :: 18-12-2015 :: SIGDAOv2: Esta prueba comprende el servicio
     * generarFPDAsync generarFPDAsync :: Enviar predios que tengan info geografica y usuario con
     * rol
     */
    @Test
    public void testGenerarFichaPredialDigital() {
        LOGGER.debug("ConservacionServiceFacadeTest#testGenerarFichaPredialDigital");
        try {
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("pruatlantico3");

            boolean fpd = this.conservacionService.generarFichaPredialDigital(
                "080010105000001180026500000001", usuario, Long.valueOf(0));
            assertTrue(fpd);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author javier.aponte
     */
    /* @lorena.salamanca :: 18-12-2015 :: SIGDAOv2: Esta prueba comprende el servicio
     * generarCCUAsync generarCCUAsync :: Enviar predios que tengan info geografica y usuario con
     * rol
     */
    @Test
    public void testGenerarCartaCatastralUrbana() {
        LOGGER.debug("ConservacionServiceFacadeTest#testGenerarCartaCatastralUrbana");
        try {
            UsuarioDTO usuario = new UsuarioDTO();
            usuario.setLogin("pruatlantico3");

            boolean ccu = this.conservacionService.generarCartaCatastralUrbana(
                "080010105000001180026500000001", usuario, Long.valueOf(0), true);
            assertTrue(ccu);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test testGuardarPlanoPredialRural
     *
     * @author javier.aponte
     */
    @Test
    public void testGuardarPlanoPredialRural() {

        LOGGER.debug("ConservacionServiceFacadeTest#testGuardarPlanoPredialRural");

        try {

            UsuarioDTO usuario = new UsuarioDTO();

            usuario.setLogin("jairo.fabra");

            String numerosManzana;
            String rutaArchivo;

            numerosManzana = "23001000100000001";
            rutaArchivo = FileUtils.getTempDirectory() + System.getProperty("file.separator") +
                numerosManzana + ".jpg";

            boolean fpd = this.conservacionService.guardarPlanoPredialRural(rutaArchivo,
                numerosManzana, usuario.getLogin());
            assertTrue(fpd);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test calcularAreasUnidadesPrediales
     *
     * @author felipe.cadena
     */
    @Test
    public void testCalcularAreasUnidadesPrediales() {

        LOGGER.debug("ConservacionServiceFacadeTest#testCalcularAreasUnidadesPrediales");

        try {

            Tramite tramite = this.tramiteService.buscarTramiteCompletoConResolucion(662115L);

            this.tramiteService.calcularAreasUnidadesPrediales(tramite);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Test testGenerarFichaPredialDigital
     *
     * @author javier.aponte
     */
    @Test
    //    @Test(threadPoolSize = 10, invocationCount = 300, timeOut = 20000)
    public void testprocesarResolucionesConservacionEnJasperServer() {

        LOGGER.debug(
            "ConservacionServiceFacadeTest#testprocesarResolucionesConservacionEnJasperServer");

        try {

            UsuarioDTO usuario = new UsuarioDTO();

            usuario.setLogin("pruatlantico2");

            long index = Math.round(Math.random() * listaPredios.size());

            String numerosPrediales;

            numerosPrediales = listaPredios.get(Integer.parseInt("" + index));

            boolean fpd = this.conservacionService.generarFichaPredialDigital(numerosPrediales,
                usuario, Long.valueOf(0));
            assertTrue(fpd);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba de Creacion de Job para resoluciones de Conservacion
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testCrearJobGenerarResolucionesConservacion() {
        LOGGER.debug("ConservacionServiceFacadeTest#avanzarTramiteDeGenerarResolucionProcess");

        Map<String, String> parameters = new HashMap<String, String>();

        List<Tramite> listaTramitesConservacion;
        long[] currentTramiteIds = new long[]{
            46860L,
            47404L,
            45045L,
            44106L};
        Long[] idTramitesLong = ArrayUtils.toObject(currentTramiteIds);
        List<Long> idTramites = Arrays.asList(idTramitesLong);

        UsuarioDTO usuario = new UsuarioDTO();

        usuario.setLogin("pruatlantico2");
        //usuario.setNombreCompleto("pruatlantico2.................");

        String numeroResolucion = "20";
        Date fechaResolucion;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            listaTramitesConservacion =
                this.tramiteService.buscarTramitesConservacionPorIdTramites(idTramites);

            for (Tramite tramite : listaTramitesConservacion) {

                parameters.put("TRAMITE_ID", "" + tramite.getId());

                parameters.put("COPIA_USO_RESTRINGIDO", "false");

                //parameters.put("NOMBRE_RESPONSABLE_CONSERVACION", usuario.getNombreCompleto());
                UsuarioDTO coordinador = null;
                //coordinador.setNombreCompleto("pruatlantico4............");   

                //parameters.put("NOMBRE_REVISOR", coordinador.getNombreCompleto());
                parameters.put("NUMERO_RESOLUCION", numeroResolucion);
                //parameters.put("FECHA_RESOLUCION", Constantes.FORMAT_FECHA_RESOLUCION.format(fechaResolucion));
                //parameters.put("FECHA_RESOLUCION_CON_FORMATO", Constantes.FORMAT_FECHA_RESOLUCION_2.format(fechaResolucion));

                //this.conservacionService.crearJobGenerarResolucionesConservacion(tramite, parametros, actividad);
            }

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba avance tramites a siguiente actividad por tramiteId
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testAvanzarTramiteDeGenerarResolucionProcess() {
        LOGGER.debug("ConservacionServiceFacadeTest#avanzarTramiteDeGenerarResolucionProcess");

        List<Tramite> listaTramitesConservacion;
        long[] currentTramiteIds = new long[]{347894L};
        Long[] idTramitesLong = ArrayUtils.toObject(currentTramiteIds);
        List<Long> idTramites = Arrays.asList(idTramitesLong);

        UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico3");

        Actividad actividad = new Actividad();
        actividad.setId("_TKI:a01b0150.14982439.35fdee53.7f8a3d84");

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            listaTramitesConservacion =
                this.tramiteService.buscarTramitesConservacionPorIdTramites(idTramites);

            for (Tramite tramite : listaTramitesConservacion) {

                //this.conservacionService.aplicarCambiosActualizacion(tramite, usuario, 3);
                this.conservacionService.avanzarTramiteDeGenerarResolucionProcess(tramite, usuario,
                    actividad);
            }

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba avance tramites a siguiente actividad por tramiteId
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testAvanzarTramiteDeGenerarResolucionActualizacionProcess() {
        LOGGER.debug(
            "ConservacionServiceFacadeTest#testAvanzarTramiteDeGenerarResolucionActualizacionProcess");

        List<Tramite> listaTramitesConservacion;
        long[] currentTramiteIds = new long[]{347890L};
        Long[] idTramitesLong = ArrayUtils.toObject(currentTramiteIds);
        List<Long> idTramites = Arrays.asList(idTramitesLong);

        UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico3");

        Actividad actividad = new Actividad();
        actividad.setId("_TKI:a01b0150.1465d15b.35fdee53.7f8a2c4e");
        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            listaTramitesConservacion =
                this.tramiteService.buscarTramitesConservacionPorIdTramites(idTramites);

            for (Tramite tramite : listaTramitesConservacion) {

                this.actualizacionService.avanzarTramiteDeGenerarResolucionProcess(tramite, usuario,
                    actividad);
            }

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 22-01-2016 :: SIGDAOv2: Esta prueba comprende el servicio
     * exportarDatosAsync
     */
    @Test
    public void testGenerarReplicaDeConsultaTramiteYAvanzarProceso() {
        LOGGER.debug(
            "ConservacionServiceFacadeTest#testGenerarReplicaDeConsultaTramiteYAvanzarProceso");

        UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico3");

        Actividad actividad = new Actividad();
        actividad.setId("_PI:90030152.57a5315e.35fdee53.e8814361");
        //Nota: El idObjetoNegocio es el mismo idTramite -_-
        actividad.setIdObjetoNegocio(121593L);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            this.conservacionService.generarReplicaDeConsultaTramiteYAvanzarProceso(actividad,
                usuario, usuario, false);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * @author lorena.salamanca
     */
    /*
     * @lorena.salamanca :: 22-01-2016 :: SIGDAOv2: Esta prueba comprende el servicio
     * aplicarCambiosAsync
     */
    @Test
    public void testAplicarCambiosDepuracion() {
        LOGGER.debug("ConservacionServiceFacadeTest#testAplicarCambiosDepuracion");

        UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico3");
        Tramite tramite = this.tramiteService.buscarTramitePorId(121630L);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            this.conservacionService.aplicarCambiosDepuracion(tramite, usuario, 1);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testValidarNumeroRadicacion() {
        UsuarioDTO usuario = this.generalesService.getCacheUsuario("pruatlantico3");
        String numeroRadicacion = "123123213ER111";
        try {
            assertTrue(this.conservacionService.validarNumeroRadicacion(numeroRadicacion, usuario));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testBuscarUnidConstPorTramiteYDifNumFichaMatrizEV() {

        String numeroFichaMatriz = "080010104000003930910900000000";
        long currentTramiteIds = 661660L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            this.conservacionService.
                buscarUnidConstPorTramiteYDifNumFichaMatrizEV(currentTramiteIds, numeroFichaMatriz);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para buscar zonas Temp por id Tramite
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testBuscarZonasTempPorIdTramite() {

        long currentTramiteIds = 660994L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            this.conservacionService.buscarZonasTempPorIdTramite(currentTramiteIds);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para buscar unidad construccion componente por id unidad construccion
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testbuscarUnidadDeConstruccionPorUnidadConstruccionId() {

        long currentTramiteIds = 11620907L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");
            List<PUnidadConstruccionComp> unidadesCom =
                this.conservacionService.buscarUnidadDeConstruccionPorUnidadConstruccionId(
                    currentTramiteIds);

            List<PUnidadConstruccionComp> unidadesNuevasConstComp =
                new ArrayList<PUnidadConstruccionComp>();

            if (unidadesCom != null && !unidadesCom.isEmpty()) {

                for (PUnidadConstruccionComp pUnidadConstruccionCompTemp : unidadesCom) {

                    PUnidadConstruccionComp pUnidadConstruccionComp = new PUnidadConstruccionComp();

                    pUnidadConstruccionComp.setComponente(pUnidadConstruccionCompTemp.
                        getComponente());
                    pUnidadConstruccionComp.setElementoCalificacion(pUnidadConstruccionCompTemp.
                        getDetalleCalificacion());
                    pUnidadConstruccionComp.setDetalleCalificacion(pUnidadConstruccionCompTemp.
                        getDetalleCalificacion());
                    pUnidadConstruccionComp.setPuntos(pUnidadConstruccionCompTemp.getPuntos());
                    pUnidadConstruccionComp.setFechaLog(new Date(System.currentTimeMillis()));
                    pUnidadConstruccionComp.setUsuarioLog(pUnidadConstruccionCompTemp.
                        getUsuarioLog());
                    pUnidadConstruccionComp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());

                    unidadesNuevasConstComp.add(pUnidadConstruccionComp);
                }

                this.conservacionService.insertarPUnidadConstruccionCompRegistros(
                    unidadesNuevasConstComp);
            }

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para buscar zonas Temp por id Tramite
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testBuscarPprediosCond9PorIdTramite() {

        long currentTramiteIds = 662115L;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            List<PPredio> pPredio = this.conservacionService.buscarPprediosCond9PorIdTramite(
                currentTramiteIds);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para buscar zonas Temp por id Tramite
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testRedondearNumeroAMil() {

        Double avaluo = 298.33;

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            this.conservacionService.redondearNumeroAMil(avaluo);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para buscar zonas Temp por id Tramite
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testObtenerUnidadesConstruccionPorListaId() {

        List<Long> construccionesIds = new ArrayList<Long>();
        construccionesIds.add(10680307L);
        construccionesIds.add(10680308L);
        construccionesIds.add(10680309L);
        construccionesIds.add(10680310L);
        construccionesIds.add(10680311L);
        construccionesIds.add(10680312L);
        construccionesIds.add(10680313L);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            this.conservacionService.obtenerUnidadesConstruccionPorListaId(construccionesIds);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Prueba para buscar zonas por una lista de id's
     *
     * @author leidy.gonzalez
     */
    @Test
    public void testObtenerZonasPorIds() {

        List<Long> ids = new ArrayList<Long>();
        ids.add(12003564L);
        ids.add(12003565L);
        ids.add(12003566L);
        ids.add(34846486L);
        ids.add(193800L);

        try {
            LOGGER.debug("**************** ENTRAMOS *************************");

            this.conservacionService.obtenerZonasPorIds(ids);

            LOGGER.debug("********************SALIMOS**********************");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    //end of class	
}
