package co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.ejecucion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoAbstractTest;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.util.Constantes;

/**
 *
 * Prueba de integración que mueve el proceso hasta "Comunicar auto a interesado"
 *
 * @author fredy.wilches
 *
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoEjeCargarPruebaTest extends
    MoverProcesoAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoEjeCargarPruebaTest.class);

    /**
     *
     */
    //private static final String CODIGO_PREDIO = "257540103000000190023000000000";
    //private static final long ID_SOLICITUD = 6159L;
    /// RESPONSABLE CONSERVACION
    private static final int RECURSO_REPOSICION = 8;
    private static final int RECURSO_REPOSICION_SUBSIDIO_APELACION = 10;
    /// DIRECTOR_TERRITORIAL
    private static final int QUEJA = 11;
    private static final int RECURSO_APELACION = 9;
    /// RESPONSABLE CONSERVACION
    private static final int REVOCATORIA_DIRECTA = 13;
    private static final int AUTOAVALUO = 12;

    //private static final int DERECHO_PETICION=7;
    /**
     * Método para iniciarlizar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");

            String territorial = MoverProcesoAbstractTest.ATLANTICO;

            RADICADOR = usuarios.get(territorial).get(ERol.FUNCIONARIO_RADICADOR);
            assertNotNull(RADICADOR);
            assertNotNull(RADICADOR.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_CONSERVACION = usuarios.get(territorial).get(ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION.getDescripcionEstructuraOrganizacional());

            EJECUTOR = usuarios.get(territorial).get(ERol.EJECUTOR_TRAMITE);
            assertNotNull(EJECUTOR);
            assertNotNull(EJECUTOR.getDescripcionEstructuraOrganizacional());
            LOGGER.debug("EJECUTOR:" + EJECUTOR);

            DIRECTOR_TERRITORIAL = usuarios.get(territorial).get(ERol.DIRECTOR_TERRITORIAL);
            assertNotNull(DIRECTOR_TERRITORIAL);
            assertNotNull(DIRECTOR_TERRITORIAL.getDescripcionEstructuraOrganizacional());
            LOGGER.debug("DIRECTOR_TERRITORIAL:" + DIRECTOR_TERRITORIAL);

            ABOGADO = usuarios.get(territorial).get(ERol.ABOGADO);
            assertNotNull(ABOGADO);
            assertNotNull(ABOGADO.getDescripcionEstructuraOrganizacional());
            LOGGER.debug("ABOGADO:" + ABOGADO);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * Crea una solicitud con un trámite
     */
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 350000)
    //@Test
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearSolicitud");

        try {
            Integer codigoEstructuraOrganizacional = 6040;
            Predio predio = this.pruebasService.buscarPredioLibreTramite(
                codigoEstructuraOrganizacional, "08001");
            assertNotNull(predio);
            String numeroPredial = predio.getNumeroPredial();
            LOGGER.debug("id:" + predio.getId() + " - Número Predial:" + numeroPredial);
            String codigoDepartamento = numeroPredial.substring(0, 2);
            String codigoMunicipio = numeroPredial.substring(0, 5);
            assertNotNull(codigoDepartamento);
            assertNotNull(codigoMunicipio);

            // /////////////////////////////////////////////////////////////////////////////////////////////
            Solicitud solicitud = new Solicitud();
            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setFecha(new Date());
            solicitud.setUsuarioLog(this.getClass().toString());
            solicitud.setAnexos(0);
            solicitud.setFolios(1);
            solicitud.setFinalizado("NO");
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.toString());

            assertNotNull(solicitud);
            solicitud.setId(null);// forzar crear nuevo objeto

            solicitud.setSistemaEnvio("1");
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());

            Tramite tramite = new Tramite();
            tramite.setPredio(predio);
            Departamento d = new Departamento();

            d.setCodigo(codigoDepartamento);
            Municipio m = new Municipio();
            m.setCodigo(codigoMunicipio);
            tramite.setDepartamento(d);
            tramite.setMunicipio(m);
            tramite.setNumeroPredial(predio.getNumeroPredial());
            tramite.setFuncionarioEjecutor(EJECUTOR.getLogin());
            tramite.setFuncionarioRadicador(RADICADOR.getLogin());

            int numero = 8 + Math.abs((int) (Math.random() * 6));
            tramite.setTipoTramite("" + numero);
            tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
            String descripcion = "";

            switch (Integer.parseInt(tramite.getTipoTramite())) {
                case RECURSO_REPOSICION:
                case RECURSO_REPOSICION_SUBSIDIO_APELACION:
                case RECURSO_APELACION:
                case QUEJA:
                    solicitud.setTipo(TS_VIA_GUBERNATIVA);
                    break;
                case REVOCATORIA_DIRECTA: solicitud.setTipo(TS_REVOCATORIA_DIRECTA);
                    break;
            }

            switch (Integer.parseInt(tramite.getTipoTramite())) {
                case RECURSO_REPOSICION:
                    descripcion = "Recurso de Reposición";
                    break;
                case RECURSO_REPOSICION_SUBSIDIO_APELACION:
                    descripcion = "Recurso Reposición en Subsidio Apelación";
                    break;
                case RECURSO_APELACION:
                    descripcion = "Recurso de Apelación";
                    break;
                case QUEJA:
                    descripcion = "Queja";
                    break;
                case REVOCATORIA_DIRECTA://no lleva num rad
                    descripcion = "Revocatoria Directa";
                    break;
                case AUTOAVALUO://no lleva num rad
                    descripcion = "Autoavalúo";
                    break;
            }

            List<TramiteDocumentacion> tramitesDocumentacion = new ArrayList<TramiteDocumentacion>();

            TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            //tipoD.setId(Constantes.TIPO_DOCUMENTO_OTRO_ID);
            tipoD.setId(ETipoDocumentoId.OTRO.getId());

            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            doc.setDescripcion("rutaDePruebaDeIntegracion");
            doc.setTipoDocumento(tipoD);
            doc.setUsuarioCreador(RADICADOR.getLogin());
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());

            tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
            tramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            tramiteDocumentacion.setDepartamento(d);
            tramiteDocumentacion.setMunicipio(m);
            tramiteDocumentacion.setCantidadFolios(1);
            tramiteDocumentacion.setTipoDocumento(tipoD);
            tramite.setNumeroPredial(numeroPredial);

            // para que la documentación este completa
            tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());
            // Si se queire avanzar el trámite estado a digitalizar
            tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

            tramiteDocumentacion.setFechaAporte(new Date());
            tramiteDocumentacion.setTramite(tramite);
            tramiteDocumentacion.setUsuarioLog("MoverProcesoEjeComunicarAutoInteresadoTest");
            tramiteDocumentacion.setFechaLog(new Date());

            tramitesDocumentacion.add(tramiteDocumentacion);
            tramiteDocumentacion.setDocumentoSoporte(doc);
            tramite.setTramiteDocumentacions(tramitesDocumentacion);

            solicitud.getTramites().add(tramite);

            List<SolicitanteSolicitud> solicitantes = tramiteService
                .getSolicitanteSolicitudAleatorio();
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);// forzar crear nuevo objeto
                ss.setSolicitud(solicitud);
            }

            solicitud.setSolicitanteSolicituds(solicitantes);

            solicitud = tramiteService.guardarActualizarSolicitud(RADICADOR, solicitud, null);
            assertNotNull(solicitud);

            List<Tramite> tramitesCreados = solicitud.getTramites();
            assertTrue(tramitesCreados.size() > 0);
            Tramite tramiteSerializado = tramitesCreados.get(0);
            assertNotNull(tramiteSerializado);
            assertNotNull(tramiteSerializado.getId());

            LOGGER.debug("Id Solicitud:" + solicitud.getId());
            LOGGER.debug("Id tramite:" + tramiteSerializado.getId());
            //LOGGER.debug("Tipo tramite:" + tramiteSerializado.getTipoTramite());
            LOGGER.debug("Núm Radicación:" + tramiteSerializado.getNumeroRadicacion());
            LOGGER.debug("descripcion:" + descripcion);

            if (Integer.parseInt(tramite.getTipoTramite()) != REVOCATORIA_DIRECTA &&
                Integer.parseInt(tramite.getTipoTramite()) != AUTOAVALUO) {
                assertNotNull(tramiteSerializado.getNumeroRadicacion(),
                    "No está generando el número de radicación tramite");
            }
            // /////////////////////////////////////////////////////////////////////////////////////////////
            SolicitudCatastral solicitudCatastral = testCrearYMoverProceso(solicitud,
                tramiteSerializado, descripcion);
            assertNotNull(solicitudCatastral);
            assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    @Implement
    protected void moverActividades(String id, SolicitudCatastral solicitudCatastral,
        Tramite tramite)
        throws Exception {
        // /////////////////////////////////////////////////////////////////////////////////////
        switch (Integer.parseInt(tramite.getTipoTramite())) {
            case RECURSO_APELACION:
            case QUEJA: moverActividad(id, DIRECTOR_TERRITORIAL, solicitudCatastral,
                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);

                moverActividad(id, DIRECTOR_TERRITORIAL, solicitudCatastral,
                    ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_AUTO_PRUEBA);
                break;
            default: moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);

                moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
                    ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_AUTO_PRUEBA);

        }

        moverActividad(id, ABOGADO, solicitudCatastral,
            ProcesoDeConservacion.ACT_EJECUCION_REVISAR_AUTO_PRUEBA);

        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO);

        moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA);

        // /////////////////////////////////////////////////////////////////////////////////////
    }

}
