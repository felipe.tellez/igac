package co.gov.igac.snc.fachadas.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EEstadoProceso;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.utilerias.Duration;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 *
 * Prueba de integración que mueve el proceso hasta "Revisar Trámite Asignado"
 *
 * @author juan.mendez
 *
 */
public abstract class MoverProcesoAbstractTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoAbstractTest.class);

    protected static final String TS_VIA_GUBERNATIVA = "3";
    protected static final String TS_AUTOAVALUO = "4";
    protected static final String TS_REVISION_AVALUO = "5";
    protected static final String TS_REVOCATORIA_DIRECTA = "7";

    /**
     * Crea un proceso
     *
     * @param solicitud
     * @param tramite
     * @return
     * @throws Exception
     */
    protected SolicitudCatastral testCrearYMoverProceso(Solicitud solicitud,
        Tramite tramite, String tipoTramiteProcesos) throws Exception {
        SolicitudCatastral solicitudCatastral = null;
        try {
            assertNotNull(RADICADOR);

            List<UsuarioDTO> RADICADORES = new ArrayList<UsuarioDTO>();
            RADICADORES.add(RADICADOR);

            solicitudCatastral = new SolicitudCatastral();

            solicitudCatastral.setIdentificador(tramite.getId());
            solicitudCatastral.setNumeroSolicitud(solicitud.getNumero());
            solicitudCatastral.setNumeroRadicacion(tramite
                .getNumeroRadicacion());
            solicitudCatastral.setNumeroPredial(tramite.getNumeroPredial());
            solicitudCatastral.setFechaRadicacion(Calendar.getInstance());
            solicitudCatastral.setTipoTramite(tipoTramiteProcesos);
            solicitudCatastral.setObservaciones("Actividad instanciada para pruebas");
            // para iniciar el proceso
            solicitudCatastral.setTransicion(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES);
            solicitudCatastral.setUsuarios(RADICADORES);

            String id = procesoService.crearProceso(solicitudCatastral);
            String estadoProceso = procesoService.obtenerEstadoProceso(id);
            LOGGER.debug("estadoProceso:" + estadoProceso);
            boolean estaProcesando = !(estadoProceso.equals(EEstadoProceso.EJECUCION.toString()));
            while (estaProcesando) {
                LOGGER.debug("Proceso: Obteniendo estado del proceso...");
                Thread.sleep(ASYNC_TIME_SLEEP);
                estadoProceso = procesoService.obtenerEstadoProceso(id);
                LOGGER.debug("estadoProceso:" + estadoProceso);
                estaProcesando = !(estadoProceso.equals(EEstadoProceso.EJECUCION.toString()));
            }
            assertNotNull(id);
            tramite.setProcesoInstanciaId(id);
            tramiteService.actualizarTramite(tramite, RADICADOR);
            LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
            LOGGER.info("ID de proceso: " + id);
            moverActividades(id, solicitudCatastral, tramite);

        } catch (Exception e) {
            throw e;
        }
        return solicitudCatastral;
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    /* protected abstract void moverActividades(String id, SolicitudCatastral solicitudCatastral)
     * throws Exception ; */
    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario. En este se
     * envía como parámetro el trámite porque para avanzarlo hasta alguna actividad a veces se
     * necesita insertar datos en otras tablas en las que se referencian datos de éste
     *
     * @param idInstanciaProceso
     * @param solicitudCatastral
     * @param tramite
     * @throws Exception
     */
    protected abstract void moverActividades(String idInstanciaProceso,
        SolicitudCatastral solicitudCatastral, Tramite tramite) throws Exception;

    /**
     *
     * @param idProceso
     * @param usuario
     * @param solicitudCatastral
     * @param valorTransicionEsperado
     * @return
     * @throws Exception
     */
    protected final Actividad moverActividad(
        String idProceso, UsuarioDTO usuario,
        SolicitudCatastral solicitudCatastral,
        String valorTransicionEsperado) throws Exception {
        return moverActividad(idProceso, usuario, solicitudCatastral, null, valorTransicionEsperado);
    }

    /**
     *
     * @param idProceso
     * @param usuario
     * @param solicitudCatastral
     * @param duracion
     * @param valorTransicionEsperado
     * @return
     * @throws Exception
     */
    //moverActividades(idActividad, transicion, usuarioDestino)
    protected final Actividad moverActividad(
        String idProceso, UsuarioDTO usuario,
        SolicitudCatastral solicitudCatastral, Duration duracion,
        String valorTransicionEsperado) throws Exception {
        LOGGER.debug("************************************************");
        LOGGER.debug("moverActividad   Destino:" + valorTransicionEsperado);
        Actividad resultado = null;
        String transicion = null;
        //Obtener actividades del proceso
        List<Actividad> actividades = null;
        //Se quitó el do-while porque este siempre retrazaba la respuesta 
        //en la primera petición al servidor de procesos.
        actividades = procesoService.obtenerActividadesProceso(idProceso);
        int intentos = 0;
        while (actividades == null || actividades.isEmpty() && intentos < 10) {
            LOGGER.debug("Proceso: Esperando respuesta del process server...");
            Thread.sleep(ASYNC_TIME_SLEEP);
            actividades = procesoService.obtenerActividadesProceso(idProceso);
            intentos++;
        }
        if (intentos == 10) {
            actividades = procesoService.getActividadesPorIdObjetoNegocio(solicitudCatastral.
                getIdentificador());
        }
        assertNotNull(actividades, "getActividadesPorIdObjetoNegocio retornó actividades nulas ");
        assertFalse(actividades.isEmpty(), "getActividadesPorIdObjetoNegocio no obtuvo resultados");
        resultado = actividades.get(0);
        assertNotNull(resultado);
        // Verificar si la actividad acepta el estado especificado
        LinkedHashMap<String, UsuarioDTO> transiciones = new LinkedHashMap<String, UsuarioDTO>();
        transiciones.put(valorTransicionEsperado, usuario);
        Iterator<String> iter = transiciones.keySet().iterator();

        while (iter.hasNext()) {
            intentos = 0;
            LOGGER.debug("Actividad Resultado: " + resultado.getId() + " - " + resultado.
                getRutaActividad().toString());
            // Obtener siguiente transición
            transicion = iter.next();
            // Usuario para las siguientes tareas
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(transiciones.get(transicion));
            // Verificar si la actividad acepta el estado especificado
            for (Actividad actividad : actividades) {
                LOGGER.debug("Transición: " + transicion);
                LOGGER.debug("Transiciones: " + ((actividad.getTransiciones() != null) ? actividad.
                    getTransiciones().toString() : "null"));
                if ((actividad.getTransiciones() == null || actividad.getTransiciones().isEmpty())) {
                    solicitudCatastral.setTransicion(null);
                } else {
                    Set<String> transActividad = new HashSet<String>(
                        actividad.getTransiciones());
                    if (transActividad != null &&
                        transActividad.contains(transicion)) {
                        solicitudCatastral.setTransicion(transicion);
                    }
                }
                solicitudCatastral.setUsuarios(usuarios);
                if (duracion != null) {
                    LOGGER.debug("Moverse a: " + solicitudCatastral.getTransicion());
                    solicitudCatastral = procesoService.avanzarActividadConservacion(idProceso,
                        actividad.getId(), duracion, solicitudCatastral);
                } else {
                    //En el process el avance es asincrónico
                    //El método solo debería continuar cuando llegue una respuesta 
                    // del servidor avisando que ya terminó el avance 
                    LOGGER.debug("Moverse a: " + solicitudCatastral.getTransicion());
                    solicitudCatastral = procesoService.avanzarActividad(actividad.getId(),
                        solicitudCatastral);
                }
                actividades = procesoService.obtenerActividadesProceso(idProceso);
                //estaEjecutando = estaEjecutando(resultado);
                while (actividades.isEmpty() && intentos <= 10) {
                    LOGGER.debug("Actividades: Esperando respuesta del process server...");
                    Thread.sleep(ASYNC_TIME_SLEEP);
                    actividades = procesoService.obtenerActividadesProceso(idProceso);
                    ++intentos;
                }
                if (intentos > 10) {
                    intentos = 0;
                    Long idTramite = solicitudCatastral.getIdentificador();
                    actividades = procesoService.getActividadesPorIdObjetoNegocio(idTramite);
                    //estaEjecutando = estaEjecutando(resultado);
                    while (actividades.isEmpty() && intentos < 10) {
                        LOGGER.debug("Actividades: Esperando respuesta del process server...");
                        Thread.sleep(ASYNC_TIME_SLEEP);
                        actividades = procesoService.getActividadesPorIdObjetoNegocio(idTramite);
                        ++intentos;
                    }
                }
                assertNotNull(actividades);
                assertTrue(actividades.size() > 0);
                resultado = actividades.get(0);
            }
        }
        return resultado;
    }

    /**
     * Valida si una tarea está en ejecución
     *
     * @author juan.mendez
     * @param actividad
     * @return
     */
    @SuppressWarnings("unused")
    private boolean estaEjecutando(Actividad actividad) {

        String estadoActividad = procesoService.obtenerEstadoActividad(actividad.getId());
        //LOGGER.debug("estadoActividad:"+estadoActividad);
        boolean estaEjecutando = estadoActividad.equals(EEstadoActividad.RECLAMADA.toString()) ||
            estadoActividad.equals(EEstadoActividad.POR_RECLAMAR.toString());
        return estaEjecutando;

    }

    /**
     * Obtiene de forma aleatoria un predio para la realización de pruebas. (Por el momento solo
     * tiene en cuenta predios de Soacha y Atlántico dado que son los únicos municipios que por
     * ahora tienen información en el entorno de Desarrollo)
     *
     * @return
     */
    public Predio obtenerPredioAleatorioParaPruebas() {
        boolean esPredioValido = false;
        Predio predio = null;
        while (!esPredioValido) {
            predio = this.pruebasService.cargarPredioAleatorio();
            String numeroPredial = predio.getNumeroPredial();
            LOGGER.debug("id:" + predio.getId() + " - Número Predial:" + numeroPredial);
            if (numeroPredial.startsWith("25754") || numeroPredial.startsWith("08001")) {
                esPredioValido = true;
            }
        }
        return predio;
    }

    /**
     *
     * @param numeroPredialIniciaCon
     * @return
     */
    public Predio obtenerPredioAleatorioParaPruebas(String numeroPredialIniciaCon) {
        boolean esPredioValido = false;
        Predio predio = null;
        while (!esPredioValido) {
            predio = this.pruebasService.cargarPredioAleatorio(numeroPredialIniciaCon);
            String numeroPredial = predio.getNumeroPredial();
            LOGGER.debug("id:" + predio.getId() + " - Número Predial:" + numeroPredial);
            if (numeroPredial.startsWith("25754") || numeroPredial.startsWith("08001")) {
                esPredioValido = true;
            }
        }
        return predio;
    }

}
