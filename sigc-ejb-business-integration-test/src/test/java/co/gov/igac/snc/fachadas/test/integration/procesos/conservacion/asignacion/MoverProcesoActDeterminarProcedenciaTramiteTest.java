package co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.asignacion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoAbstractTest;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

/**
 *
 * Prueba de integración que mueve el proceso hasta "Revisar Trámite Asignado"
 *
 * @author pedro.garcia "migrado" -como diría Gabriel- del ejemplo de Juan CArlos
 *
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoActDeterminarProcedenciaTramiteTest extends MoverProcesoAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActDeterminarProcedenciaTramiteTest.class);

    private Predio predio;
    private String codigoDepartamento;
    private String codigoMunicipio;
    /**
     * @author alejandro.sanchez Este objeto permite la medición de tiempos de respuesta de la
     * creación y consulta de actividades del proceso.
     */
    private Monitor movimientoActividades;

    /**
     *
     */
    private static final String TIPO_TRAMITE = "1";
    private static final String CLASE_MUTACION = EMutacionClase.SEGUNDA.getCodigo();
    private static final String SUBTIPO_TRAMITE = EMutacion2Subtipo.DESENGLOBE.getCodigo();

    private static final String CLASIFICACION_TRAMITE = "TERRENO";
    private static final String TIPO_TRAMITE_PROCESS = "Mutación 2";
    private static String CODIGO_PREDIO;
    //= "257540103000000190023000000000";
    //private static final long ID_SOLICITUD = 6159L;

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.debug("runBeforeClass");

            Integer codigoEstructuraOrganizacional = 6040;
            String territorial = MoverProcesoAbstractTest.ATLANTICO;
            String codigoMunicipio = "08001";
            CODIGO_PREDIO = this.pruebasService.buscarPredioLibreTramite(
                codigoEstructuraOrganizacional, codigoMunicipio).getNumeroPredial();

            assertNotNull(CODIGO_PREDIO);

            codigoDepartamento = CODIGO_PREDIO.substring(0, 2);
            codigoMunicipio = CODIGO_PREDIO.substring(0, 5);

            assertNotNull(codigoDepartamento);
            assertNotNull(codigoMunicipio);

            RADICADOR = usuarios.get(territorial).get(ERol.FUNCIONARIO_RADICADOR);
            assertNotNull(RADICADOR);
            assertNotNull(RADICADOR.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_CONSERVACION = usuarios.get(territorial).get(ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION.getDescripcionEstructuraOrganizacional());

            EJECUTOR = usuarios.get(territorial).get(ERol.EJECUTOR_TRAMITE);
            assertNotNull(EJECUTOR);
            assertNotNull(EJECUTOR.getDescripcionEstructuraOrganizacional());

            predio = conservacionService.getPredioByNumeroPredial(CODIGO_PREDIO);
            assertNotNull(predio);
            LOGGER.debug("Id predio:" + predio.getId());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    @AfterClass
    protected void afterClass() {
        LOGGER.info(movimientoActividades.toString());
    }

//--------------------------------------------------------------------------------------------------
    /*
     * Crea una solicitud con un trámite
     */
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 400000)
    // @Test
    /**
     * Permite mover el proceso hasta Determinar procedencia del trámite.
     *
     * @author alejandro.sanchez
     */
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearSolicitud");

        try {
            // /////////////////////////////////////////////////////////////////////////////////////////////
            Solicitud solicitud = new Solicitud();
            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setFecha(new Date());
            solicitud.setUsuarioLog(this.getClass().toString());
            solicitud.setAnexos(0);
            solicitud.setFolios(1);
            solicitud.setFinalizado("NO");
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.toString());

            solicitud.setSistemaEnvio("1");
            solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());

            Tramite tramite = new Tramite();
            tramite.setPredio(predio);
            Departamento d = new Departamento();

            d.setCodigo(codigoDepartamento);
            Municipio m = new Municipio();
            m.setCodigo(codigoMunicipio);
            tramite.setDepartamento(d);
            tramite.setMunicipio(m);
            tramite.setTipoTramite(TIPO_TRAMITE);
            tramite.setClasificacion(CLASIFICACION_TRAMITE);
            tramite.setComisionado(ESiNo.NO.getCodigo());
            tramite.setNumeroPredial(CODIGO_PREDIO);
            tramite.setFechaRadicacion(new Date());
            //D: deberían llegar sin ejecutor
            //tramite.setFuncionarioEjecutor("prusoacha09");
            tramite.setClaseMutacion(CLASE_MUTACION);
            tramite.setSubtipo(SUBTIPO_TRAMITE);

            List<TramiteDocumentacion> tramitesDocumentacion = new ArrayList<TramiteDocumentacion>();

            TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            //tipoD.setId(Constantes.TIPO_DOCUMENTO_OTRO_ID);
            tipoD.setId(ETipoDocumentoId.OTRO.getId());

            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            doc.setDescripcion("rutaDePruebaDeIntegracion");
            doc.setTipoDocumento(tipoD);
            doc.setUsuarioCreador(RADICADOR.getLogin());
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());

            tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
            tramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            tramiteDocumentacion.setDepartamento(d);
            tramiteDocumentacion.setMunicipio(m);
            tramiteDocumentacion.setCantidadFolios(1);
            tramiteDocumentacion.setTipoDocumento(tipoD);

            // para que la documentación este completa
            tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());
            // Si se queire avanzar el trámite estado a digitalizar
            tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

            tramiteDocumentacion.setFechaAporte(new Date());
            tramiteDocumentacion.setTramite(tramite);
            tramiteDocumentacion.setUsuarioLog("MoverProcesoActDeterminarProcedenciaTramiteTest");
            tramiteDocumentacion.setFechaLog(new Date());

            tramitesDocumentacion.add(tramiteDocumentacion);
            tramiteDocumentacion.setDocumentoSoporte(doc);
            tramite.setTramiteDocumentacions(tramitesDocumentacion);

            solicitud.getTramites().add(tramite);

            List<SolicitanteSolicitud> solicitantes = tramiteService
                .getSolicitanteSolicitudAleatorio();
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);// forzar crear nuevo objeto
                ss.setSolicitud(solicitud);
            }

            solicitud.setSolicitanteSolicituds(solicitantes);

            solicitud = tramiteService.guardarActualizarSolicitud(RADICADOR, solicitud, null);
            assertNotNull(solicitud);

            List<Tramite> tramitesCreados = solicitud.getTramites();
            assertTrue(tramitesCreados.size() > 0);
            Tramite tramiteSerializado = tramitesCreados.get(0);
            assertNotNull(tramiteSerializado);
            assertNotNull(tramiteSerializado.getId());
            LOGGER.debug("Id Solicitud:" + solicitud.getId());
            LOGGER.debug("Id tramite:" + tramiteSerializado.getId());
            // /////////////////////////////////////////////////////////////////////////////////////////////
            movimientoActividades = MonitorFactory.start("procesos.avanzarActividad");
            SolicitudCatastral solicitudCatastral = super.testCrearYMoverProceso(
                solicitud, tramiteSerializado, TIPO_TRAMITE_PROCESS);
            movimientoActividades.stop();
            assertNotNull(solicitudCatastral);
            assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    @Implement
    protected void moverActividades(String id, SolicitudCatastral solicitudCatastral,
        Tramite tramite)
        throws Exception {

        moverActividad(
            id,
            RESPONSABLE_CONSERVACION,
            solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
    }
}
