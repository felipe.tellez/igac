package co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.asignacion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoAbstractTest;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;

/**
 *
 * Prueba de integración que mueve el proceso hasta Generar el trámite geográfico
 *
 * @author juan.mendez
 *
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoGeograficaSegundaDesenglobeTest extends MoverProcesoAbstractTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        MoverProcesoGeograficaSegundaDesenglobeTest.class);

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");
            RADICADOR = usuarios.get(ATLANTICO).get(ERol.FUNCIONARIO_RADICADOR);

            assertNotNull(RADICADOR);
            assertNotNull(RADICADOR.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_CONSERVACION = usuarios.get(ATLANTICO).get(ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION.getDescripcionEstructuraOrganizacional());

            EJECUTOR = usuarios.get(ATLANTICO).get(ERol.EJECUTOR_TRAMITE);
            assertNotNull(EJECUTOR);
            assertNotNull(EJECUTOR.getDescripcionEstructuraOrganizacional());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /*
     * Crea una solicitud con un trámite
     */
    //@Test(threadPoolSize = 2, invocationCount = 2, timeOut = 360000)
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 350000)
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearYAvanzarSolicitud");

        try {
            LOGGER.debug("Obteniendo predio libre para pruebas...");
            String codigoMunicipio = "08001";
            Predio predio = this.pruebasService.buscarPredioLibreTramite(
                CODIGO_ESTRUCTURA_O_ATLANTICO, codigoMunicipio);

            //String codigoInicio = "08001";
            //Predio predio = this.obtenerPredioAleatorioParaPruebas(codigoInicio);
            assertNotNull(predio);
            String numeroPredial = predio.getNumeroPredial();

            LOGGER.debug("id:" + predio.getId());
            LOGGER.debug("Número Predial:" + numeroPredial);

            String codigoDepartamento = numeroPredial.substring(0, 2);
            assertNotNull(codigoDepartamento);
            assertNotNull(codigoMunicipio);

            // /////////////////////////////////////////////////////////////////////////////////////////////
            Solicitud solicitud = new Solicitud();
            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setFecha(new Date());
            solicitud.setUsuarioLog(this.getClass().toString());
            solicitud.setAnexos(0);
            solicitud.setFolios(1);
            solicitud.setFinalizado("NO");
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.toString());

            solicitud.setSistemaEnvio("1");
            solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());

            List<DatoRectificar> datosRectificar = tramiteService.obtenerDatosRectificarPorTipo(
                "PROPIETARIO");
            datosRectificar.addAll(tramiteService.obtenerDatosRectificarPorTipo("PREDIO"));
            datosRectificar.addAll(tramiteService.obtenerDatosRectificarPorTipo("GENERALES"));

            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setSistemaEnvio("1");
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());
            solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());

            Tramite tramite = new Tramite();
            tramite.setPredio(predio);
            tramite.setFechaRadicacion(new Date());

            Departamento d = new Departamento();
            d.setCodigo(codigoDepartamento);

            Municipio m = new Municipio();
            m.setCodigo(codigoMunicipio);

            tramite.setDepartamento(d);
            tramite.setMunicipio(m);
            tramite.setNumeroPredial(predio.getNumeroPredial());
            tramite.setTipoTramite(ETramiteTipoTramite.MUTACION.getCodigo());
            tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
            tramite.setClaseMutacion(EMutacionClase.SEGUNDA.getCodigo());
            tramite.setSubtipo(EMutacion2Subtipo.DESENGLOBE.getCodigo());
            assertTrue(tramite.isTramiteGeografico());

            String descripcion = "Mutación Segunda - Integración";

            tramite.setClasificacion(ETramiteClasificacion.TERRENO.toString());
            tramite.setSubtipo(EMutacion2Subtipo.DESENGLOBE.getCodigo());
            tramite.setPredio(predio);

            List<TramiteDocumentacion> tramitesDocumentacion = new ArrayList<TramiteDocumentacion>();

            TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
            Documento doc = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            tipoD.setId(ETipoDocumentoId.OTRO.getId());

            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            doc.setDescripcion(descripcion);
            doc.setTipoDocumento(tipoD);
            doc.setUsuarioCreador(RADICADOR.getLogin());
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());

            tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
            tramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            tramiteDocumentacion.setDepartamento(d);
            tramiteDocumentacion.setMunicipio(m);
            tramiteDocumentacion.setCantidadFolios(1);
            tramiteDocumentacion.setTipoDocumento(tipoD);
            tramite.setNumeroPredial(numeroPredial);

            // para que la documentación este completa
            tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());
            // Si se queire avanzar el trámite estado a digitalizar
            tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

            tramiteDocumentacion.setFechaAporte(new Date());
            tramiteDocumentacion.setTramite(tramite);
            tramiteDocumentacion.setUsuarioLog("MoverProcesoGeograficaSegundaDesenglobeTest");
            tramiteDocumentacion.setFechaLog(new Date());

            tramitesDocumentacion.add(tramiteDocumentacion);
            tramiteDocumentacion.setDocumentoSoporte(doc);
            tramite.setTramiteDocumentacions(tramitesDocumentacion);

            solicitud.getTramites().add(tramite);

            List<SolicitanteSolicitud> solicitantes = tramiteService
                .getSolicitanteSolicitudAleatorio();
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);// forzar crear nuevo objeto
                ss.setSolicitud(solicitud);
            }

            solicitud.setSolicitanteSolicituds(solicitantes);

            solicitud = tramiteService.guardarActualizarSolicitud(RADICADOR, solicitud, null);
            assertNotNull(solicitud);

            List<Tramite> tramitesCreados = solicitud.getTramites();
            assertTrue(tramitesCreados.size() > 0);
            Tramite tramiteSerializado = tramitesCreados.get(0);
            assertNotNull(tramiteSerializado);
            assertNotNull(tramiteSerializado.getId());
            LOGGER.debug("Id Solicitud:" + solicitud.getId());
            LOGGER.debug("Id tramite:" + tramiteSerializado.getId());

            // /////////////////////////////////////////////////////////////////////////////////////////////
            SolicitudCatastral solicitudCatastral = testCrearYMoverProceso(solicitud,
                tramiteSerializado, descripcion);
            assertNotNull(solicitudCatastral);
            assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    @Implement
    protected void moverActividades(String id, SolicitudCatastral solicitudCatastral,
        Tramite tramite) throws Exception {
        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES);
        moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);
        Actividad actividad = moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);
        this.conservacionService.enviarAModificacionInformacionGeografica(actividad, EJECUTOR);

        String urlImagenGrafo = procesoService.obtenerFlujoEjecucionProceso(actividad.getId());
        assertNotNull(urlImagenGrafo);
        LOGGER.debug("urlImagenGrafo:" + urlImagenGrafo);

        LOGGER.debug(
            "Generando copia geográfica de trabajo. Favor verificar los logs de Arcgis Server y JBoss (La generación es asincrónica)");
        //Thread.sleep(2*60*1000);//espera dos minutos mientras se genera la réplica
    }

}
