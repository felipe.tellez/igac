package co.gov.igac.snc.fachadas.test.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Pruebas de integracion para la interfaz IFormacion
 *
 * @author franz.gamba
 *
 */
public class FormacionServiceFacadeTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FormacionServiceFacadeTest.class);

}
