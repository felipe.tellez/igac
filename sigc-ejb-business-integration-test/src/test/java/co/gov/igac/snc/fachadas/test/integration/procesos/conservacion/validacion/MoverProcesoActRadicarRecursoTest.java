package co.gov.igac.snc.fachadas.test.integration.procesos.conservacion.validacion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.fachadas.test.integration.MoverProcesoAbstractTest;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;

/**
 * Prueba de integración que mueve el proceso hasta "Radicar recurso"
 *
 * @author david.cifuentes
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MoverProcesoActRadicarRecursoTest extends MoverProcesoAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActRadicarRecursoTest.class);

    private Predio predio;
    private String codigoDepartamento;
    private String codigoMunicipio;

    private static final String TIPO_TRAMITE = "2";
    private static final String TIPO_TRAMITE_PROCESS = "Mutación 2";
    private static final String CLASE_MUTACION = EMutacionClase.SEGUNDA
        .getCodigo();
    private static final String SUBTIPO_TRAMITE = EMutacion2Subtipo.DESENGLOBE.getCodigo();
    private static final String CLASIFICACION_TRAMITE = "TERRENO";
    // private static final String CODIGO_PREDIO =
    // "257540103000000190023000000000";
    private static String CODIGO_PREDIO;
    //= "257540216041711057933105117115";

    @SuppressWarnings("unused")
    //private static final Long ID_SOLICITANTE_SOLICITUD = 22395L;
    // private static final Long ID_SOLICITANTE_SOLICITUD = 22395L;
    //private static final long ID_SOLICITUD = 6159L;

    /**
     * Método para inicializar los objetos requeridos por la prueba de integración.
     *
     * @throws Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
        super.setUp();
        try {
            LOGGER.info("runBeforeClass");

            Integer codigoEstructuraOrganizacional = 6040;
            String territorial = MoverProcesoAbstractTest.ATLANTICO;
            String codigoMunicipio = "08001";

            CODIGO_PREDIO = this.pruebasService.buscarPredioLibreTramite(
                codigoEstructuraOrganizacional, codigoMunicipio).getNumeroPredial();
            assertNotNull(CODIGO_PREDIO);

            codigoDepartamento = CODIGO_PREDIO.substring(0, 2);
            codigoMunicipio = CODIGO_PREDIO.substring(0, 5);

            assertNotNull(codigoDepartamento);
            assertNotNull(codigoMunicipio);

            RADICADOR = usuarios.get(territorial).get(ERol.FUNCIONARIO_RADICADOR);
            assertNotNull(RADICADOR);
            assertNotNull(RADICADOR.getDescripcionEstructuraOrganizacional());

            RESPONSABLE_CONSERVACION = usuarios.get(territorial).get(ERol.RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION);
            assertNotNull(RESPONSABLE_CONSERVACION
                .getDescripcionEstructuraOrganizacional());

            EJECUTOR = usuarios.get(territorial).get(ERol.EJECUTOR_TRAMITE);
            assertNotNull(EJECUTOR);
            assertNotNull(EJECUTOR.getDescripcionEstructuraOrganizacional());

            predio = conservacionService
                .getPredioByNumeroPredial(CODIGO_PREDIO);
            assertNotNull(predio);
            LOGGER.debug("Id predio:" + predio.getId());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /*
     * Crea una solicitud con un trámite
     */
    @Test(threadPoolSize = 1, invocationCount = 1, timeOut = 400000)
    // @Test
    public void testCrearYAvanzarSolicitud() {
        LOGGER.debug("********************************************************");
        LOGGER.debug("testCrearSolicitud");

        try {
            // /////////////////////////////////////////////////////////////////////////////////////////////
            Solicitud solicitud = new Solicitud();
            solicitud.setId(null);// forzar crear nuevo objeto
            solicitud.setFecha(new Date());
            solicitud.setUsuarioLog(this.getClass().toString());
            solicitud.setAnexos(0);
            solicitud.setFolios(1);
            solicitud.setFinalizado("NO");
            solicitud.setEstado(ESolicitudEstado.RECIBIDA.toString());

            solicitud.setSistemaEnvio("1");
            solicitud.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
            solicitud.setNumero(null);
            solicitud.setTramites(new ArrayList<Tramite>());

            Tramite tramite = new Tramite();
            tramite.setPredio(predio);
            Departamento d = new Departamento();

            d.setCodigo(codigoDepartamento);
            Municipio m = new Municipio();
            m.setCodigo(codigoMunicipio);
            tramite.setDepartamento(d);
            tramite.setMunicipio(m);
            tramite.setTipoTramite(TIPO_TRAMITE);
            tramite.setClasificacion(CLASIFICACION_TRAMITE);
            tramite.setComisionado(ESiNo.NO.getCodigo());
            tramite.setFuncionarioEjecutor("prusoacha09");
            tramite.setClaseMutacion(CLASE_MUTACION);
            tramite.setSubtipo(SUBTIPO_TRAMITE);
            tramite.setNumeroPredial(CODIGO_PREDIO);
            tramite.setFechaRadicacion(new Date());

            List<TramiteDocumentacion> tramitesDocumentacion = new ArrayList<TramiteDocumentacion>();
            List<TramiteDocumento> tramiteDocumentos = new ArrayList<TramiteDocumento>();

            TramiteDocumentacion tramiteDocumentacion = new TramiteDocumentacion();
            TramiteDocumento tramiteDocumento = new TramiteDocumento();
            Documento doc = new Documento();
            Documento reso = new Documento();
            TipoDocumento tipoD = new TipoDocumento();
            // tipoD.setId(Constantes.TIPO_DOCUMENTO_OTRO_ID);
            tipoD.setId(ETipoDocumentoId.OTRO.getId());

            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            doc.setDescripcion("rutaDePruebaDeIntegracion");
            doc.setTipoDocumento(tipoD);
            doc.setUsuarioCreador(RADICADOR.getLogin());
            doc.setFechaLog(new Date());
            doc.setFechaRadicacion(new Date());

            // ------  Resolucion del trámite ------- //
            TipoDocumento resoTipoD = new TipoDocumento();
            resoTipoD.setId(ETipoDocumentoId.RESOLUCION_CONSERVACION.getId());

            reso.setEstado(EDocumentoEstado.RESOLUCION_EN_PROYECCION
                .getCodigo());
            reso.setArchivo("resolucionCreadaParaTest.pdf");
            reso.setDescripcion("rutaDePruebaDeIntegracion");
            reso.setTipoDocumento(resoTipoD);
            Random random = new Random();
            reso.setNumeroDocumento(Integer.toString(random.nextInt(145212458)));
            reso.setBloqueado(ESiNo.NO.getCodigo());
            reso.setUsuarioCreador(RADICADOR.getLogin());
            reso.setUsuarioLog("MoverProcesoActRadicarRecursoTest");
            reso.setFechaDocumento(new Date());
            reso.setFechaLog(new Date());
            reso.setFechaRadicacion(new Date());

            reso = this.generalesService.guardarDocumento(reso);

            tramiteDocumento.setfecha(new Date());
            tramiteDocumento.setDocumento(reso);
            tramiteDocumento.setNumeroDocumento(Constantes.CONSTANTE_CADENA_VACIA_DB);
            tramiteDocumento.setUsuarioLog("MoverProcesoActRadicarRecursoTest");
            tramiteDocumento.setFechaLog(new Date());

            tramiteDocumentos.add(tramiteDocumento);

            // -------------------------------------- //
            tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
            tramiteDocumentacion.setAdicional(ESiNo.NO.getCodigo());
            tramiteDocumentacion.setDepartamento(d);
            tramiteDocumentacion.setMunicipio(m);
            tramiteDocumentacion.setCantidadFolios(1);
            tramiteDocumentacion.setTipoDocumento(tipoD);

            // para que la documentación este completa
            tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());
            // Si se quiere avanzar el trámite estado a digitalizar
            tramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

            tramiteDocumentacion.setFechaAporte(new Date());
            tramiteDocumentacion.setTramite(tramite);
            tramiteDocumentacion.setUsuarioLog("MoverProcesoActRadicarRecursoTest");
            tramiteDocumentacion.setFechaLog(new Date());

            tramitesDocumentacion.add(tramiteDocumentacion);
            tramiteDocumentacion.setDocumentoSoporte(doc);
            tramite.setTramiteDocumentacions(tramitesDocumentacion);

            tramite.setTramiteDocumentos(tramiteDocumentos);
            tramite.setResultadoDocumento(reso);

            solicitud.getTramites().add(tramite);

            List<SolicitanteSolicitud> solicitantes = tramiteService
                .getSolicitanteSolicitudAleatorio();
            for (SolicitanteSolicitud ss : solicitantes) {
                ss.setId(null);// forzar crear nuevo objeto
                ss.setSolicitud(solicitud);
            }

            solicitud.setSolicitanteSolicituds(solicitantes);

            solicitud = tramiteService.guardarActualizarSolicitud(RADICADOR,
                solicitud, null);
            assertNotNull(solicitud);

            List<Tramite> tramitesCreados = solicitud.getTramites();
            assertTrue(tramitesCreados.size() > 0);
            Tramite tramiteSerializado = tramitesCreados.get(0);
            assertNotNull(tramiteSerializado);
            assertNotNull(tramiteSerializado.getId());
            LOGGER.debug("Id Solicitud:" + solicitud.getId());
            LOGGER.debug("Id tramite:" + tramiteSerializado.getId());
            LOGGER.debug("Id numero predial:" +
                tramiteSerializado.getNumeroPredial());
            LOGGER.debug("Id predio numero predial:" +
                tramiteSerializado.getPredio().getNumeroPredial());
            // /////////////////////////////////////////////////////////////////////////////////////////////
            SolicitudCatastral solicitudCatastral = super
                .testCrearYMoverProceso(solicitud, tramiteSerializado,
                    TIPO_TRAMITE_PROCESS);
            assertNotNull(solicitudCatastral);
            assertNotNull(solicitudCatastral.getIdentificador());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Avanza el proceso por una ruta específica teniendo en cuenta los roles de usuario
     *
     * @param solicitudCatastral
     * @throws Exception
     */
    @Implement
    protected void moverActividades(String id,
        SolicitudCatastral solicitudCatastral, Tramite tramite)
        throws Exception {

        //------ Entro por ejecución -----//
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(
            id,
            RESPONSABLE_CONSERVACION,
            solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);

        // /////////////////////////////////////////////////////////////////////////////////////
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES);

        // /////////////////////////////////////////////////////////////////////////////////////
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);

        // /////////////////////////////////////////////////////////////////////////////////////
        moverActividad(id, EJECUTOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);

        //---- Entro a Validación ----//
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);

        /*
         * // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO moverActividad(id, EJECUTOR,
         * solicitudCatastral,
         * ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA);
         *
         * // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO moverActividad(id, EJECUTOR,
         * solicitudCatastral,
         * ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA);
         */
        // /////////////////////////////////////////////////////////////////////////////////////
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(
            id,
            RESPONSABLE_CONSERVACION,
            solicitudCatastral,
            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION);

        // /////////////////////////////////////////////////////////////////////////////////////
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(id, RESPONSABLE_CONSERVACION, solicitudCatastral,
            ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION);

        // /////////////////////////////////////////////////////////////////////////////////////
        // TODO: FALTA MOVER LOS DATOS DE ORACLE AL NUEVO ESTADO
        moverActividad(id, RADICADOR, solicitudCatastral,
            ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO);

    }

}
