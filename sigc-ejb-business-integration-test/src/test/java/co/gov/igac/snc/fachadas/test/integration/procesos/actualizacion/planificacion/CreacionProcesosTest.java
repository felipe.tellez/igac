package co.gov.igac.snc.fachadas.test.integration.procesos.actualizacion.planificacion;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.fachadas.test.integration.AbstractFacadeTest;

/**
 *
 * @author alejandro.sanchez
 *
 */
public class CreacionProcesosTest extends AbstractFacadeTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CreacionProcesosTest.class);

    private static final long TIME_SLEEP = 3000;// REVISAR ESTOS TIEMPOS

    private UsuarioDTO directorTerritorial;

    @Test
    public void testCrearProceso() {
        try {
            directorTerritorial = generalesService.getCacheUsuario("prucundi02");
            assertNotNull(directorTerritorial);
            List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
            listaUsuarios.add(directorTerritorial);
            Random random = new Random();
            Long identificador = random.nextLong();
            String idDepartamento = Integer.toString(random.nextInt());
            String idMunicipio = Integer.toString(random.nextInt());
            String transicion = ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_INFO_DIAGNOSTICO;

            ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
            actualizacionCatastral.setIdentificador(identificador);
            actualizacionCatastral.setIdDepartamento(idDepartamento);
            actualizacionCatastral.setIdMunicipio(idMunicipio);
            actualizacionCatastral.setResultado(
                "Instancia de proceso de actualización creada por prueba unitaria");
            actualizacionCatastral.setUsuarios(listaUsuarios);
            actualizacionCatastral.setTransicion(transicion);
            String idProceso = procesoService.crearProceso(actualizacionCatastral);

            Thread.sleep(TIME_SLEEP);
            assertNotNull(idProceso);
            LOGGER.info("Proceso de actualización: " + idProceso);

        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
    }

    @Test
    public void testListaActividades() {
        LOGGER.debug("Test consulta tareas (Lista actividades)");

        try {
            UsuarioDTO responsableConservacion;
            responsableConservacion = generalesService
                .getCacheUsuario("prucundi02");
            List<Actividad> actividades = procesoService
                .consultarListaActividades(
                    responsableConservacion);
            assertNotNull(actividades);
            LOGGER.info("Número de actividades: " + actividades.size());
            for (Actividad actividad : actividades) {
                LOGGER.info("Ruta Actividad: " + actividad.getRutaActividad().toString() +
                    ", NOMBRE: " +
                    actividad.getNombre() + ", " +
                    "NOMBRE DESPLIEGUE: " + actividad.getNombreDeDespliegue());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            // test fail
            fail(e.getMessage(), e);
        }
    }
}
