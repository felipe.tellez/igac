package co.gov.igac.snc.fachadas.test.integration.procesos.osmi;

import static org.testng.Assert.fail;
import static org.testng.AssertJUnit.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.test.integration.AbstractFacadeTest;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaAreaCapturaOfertaEstado;
import co.gov.igac.snc.persistence.util.EOfertaComisionEstado;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.persistence.util.ERol;

public abstract class MoverProcesoOsmiAbstractTest extends AbstractFacadeTest {

    // Usuarios??
    protected UsuarioDTO INVESTIGADOR_MERCADO;
    protected UsuarioDTO COORDINADOR_GIT_AVALUOS;
    protected UsuarioDTO RECOLECTOR;
    protected UsuarioDTO DIRECTOR_TERRITORIAL;

    protected Hashtable<ERol, UsuarioDTO> usuariosAtlantico = new Hashtable<ERol, UsuarioDTO>();

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoOsmiAbstractTest.class);

    public static final long TIMER = 3000;

    // Area de Captura de Ofertas
    protected AreaCapturaOferta areaCapturaOferta;

    // Region de Captura de Ofertas
    protected RegionCapturaOferta regionCapturaOferta;

    // Comision
    protected ComisionOferta comisionOferta;

    // Lista de regiones de una comision
    private ArrayList<RegionCapturaOferta> regionesAEliminar;

    // Oferta Inmobiliaria
    protected co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria ofertaInmobiliariaEntity;

    // Id del Area Captura Oferta
    private Long areaId;

    // Lista de detalles captura oferta asociadas a una región
    protected List<DetalleCapturaOferta> listaDetallesCapturaOferta;

    // Bandera que indica si el area es centralizada o no.
    // true = centralizada
    // false = descentralizada
    protected boolean banderaEsAreaCentralizada;

    // Objeto de negocio
    protected OfertaInmobiliaria ofertaInmobiliaria;
    private List<co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria> listaOfertas;

    /**
     * Id para identificar el tester que ejecuta la prueba
     */
    protected String testerId;

    @BeforeMethod
    public void setUp() throws Exception {

        try {
            super.setUp();
            LOGGER.debug("Iniciando usuarios para proceso de Ofertas Inmobiliarias");

            usuariosAtlantico.put(ERol.INVESTIGADOR_MERCADO,
                this.generalesService.getCacheUsuario("pruatlantico22"));

            usuariosAtlantico.put(ERol.COORDINADOR_GIT_AVALUOS,
                this.generalesService.getCacheUsuario("pruatlantico2"));

            usuariosAtlantico.put(ERol.RECOLECTOR_OFERTA_INMOBILIARIA,
                this.generalesService.getCacheUsuario("pruatlantico20"));

            usuariosAtlantico.put(ERol.DIRECTOR_TERRITORIAL,
                this.generalesService.getCacheUsuario("pruatlantico5"));

            testerId = "pruatlantico20";

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    protected abstract void moverActividades(String idInstanciaProceso,
        OfertaInmobiliaria ofertaInmobiliaria) throws Exception;

    /**
     * Crea el o los procesos necesarios para el avance de actividades.
     *
     * @param ofertaInmobiliaria
     * @return Id del proceso creado
     * @throws Exception
     */
    protected abstract String crearProceso(OfertaInmobiliaria ofertaInmobiliaria)
        throws Exception;

    protected Actividad moverActividad(String idProceso,
        OfertaInmobiliaria ofertaInmobiliaria) throws Exception {
        // Avanza actividad
        OfertaInmobiliaria oferta = procesoService
            .avanzarActividadPorIdProceso(idProceso, ofertaInmobiliaria);
        Thread.sleep(TIMER);
        // Consulta actividades proceso
        List<Actividad> listaActividades = procesoService
            .consultarListaActividades(oferta.getUsuarios().get(0),
                ofertaInmobiliaria.getTransicion());
        assertEquals(listaActividades.size(), 1);
        return listaActividades.get(0);
    }

    /**
     * Crea un proceso
     *
     * @param solicitud
     * @param tramite
     * @return
     * @throws Exception
     */
    protected OfertaInmobiliaria crearProcesoYAvanzarActividades(
        OfertaInmobiliaria ofertaInmobiliaria) throws Exception {

        // Crea proceso(s)
        String id = null;
        try {
            id = crearProceso(ofertaInmobiliaria);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail();
        }
        // Avanzar actividades
        Thread.sleep(TIMER);
        moverActividades(id, ofertaInmobiliaria);
        return ofertaInmobiliaria;

    }

    @SuppressWarnings("unused")
    private boolean estaEjecutando(Actividad actividad) {

        String estadoActividad = procesoService
            .obtenerEstadoActividad(actividad.getId());
        // LOGGER.debug("estadoActividad:"+estadoActividad);
        boolean estaEjecutando = estadoActividad
            .equals(EEstadoActividad.RECLAMADA.toString()) ||
            estadoActividad.equals(EEstadoActividad.POR_RECLAMAR
                .toString());
        return estaEjecutando;

    }

    /**
     * Método para cargar los datos según la última transición a probar
     *
     * @author rodrigo.hernandez
     *
     * @param transicion
     */
    protected void cargarDatosPorTransicion(String transicion) {

        if (transicion
            .equals(ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA)) {
            this.cargarDatosAsignarRecolector();
        } else if (transicion
            .equals(ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION)) {
            this.cargarDatosProyectarComisionOferta();
        } else if (transicion
            .equals(ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_APROBAR_AJUSTAR_ORDEN_COMISION)) {
            this.cargarDatosAprobarComisionOferta();
        } else if (transicion
            .equals(ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO)) {
            this.cargarDatosRevisarAreaAsignada();
        } else if (transicion
            .equals(ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_INGRESAR_OFERTAS)) {
            this.cargarDatosIngresarOfertas();
        } else {
            this.cargarDatosEstablecerAreaOferta();
        }

    }

    /**
     * Método para cargar datos para crear proceso de Asignacion Recolector
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosEstablecerAreaOferta() {

        this.cargarDatosUsuarios();
        this.cargarDatosArea();

        this.ofertaInmobiliaria = this.crearObjetoNegocio();
    }

    /**
     * Método para cargar datos en transicion ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosAsignarRecolector() {
        this.cargarDatosEstablecerAreaOferta();
        this.crearRegionCapturaOferta();
    }

    /**
     * Método para cargar datos en transicion ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosProyectarComisionOferta() {
        this.cargarDatosAsignarRecolector();
        this.crearDatosComisionOferta();

    }

    /**
     * Método para cargar datos en transicion ACT_ASIGNACION_APROBAR_AJUSTAR_ORDEN_COMISION
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosAprobarComisionOferta() {
        this.cargarDatosProyectarComisionOferta();
        this.aprobarComisionOferta();
    }

    /**
     * Método para cargar datos en transicion ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosRevisarAreaAsignada() {
        this.cargarDatosAprobarComisionOferta();
    }

    /**
     * Método para cargar datos en transicion ACT_EJECUCION_INGRESAR_OFERTAS
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosIngresarOfertas() {
        this.cargarDatosRevisarAreaAsignada();
    }

    /**
     * Método para cargar datos de un recolector
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosUsuarios() {
        try {

            // Se inician datos del recolector
            this.RECOLECTOR = usuariosAtlantico
                .get(ERol.RECOLECTOR_OFERTA_INMOBILIARIA);

            this.DIRECTOR_TERRITORIAL = usuariosAtlantico
                .get(ERol.DIRECTOR_TERRITORIAL);

            this.COORDINADOR_GIT_AVALUOS = usuariosAtlantico
                .get(ERol.COORDINADOR_GIT_AVALUOS);

            this.INVESTIGADOR_MERCADO = usuariosAtlantico
                .get(ERol.INVESTIGADOR_MERCADO);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }
    }

    /**
     * Método para cargar datos de un AREA_CAPTURA_OFERTA
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosArea() {
        this.areaCapturaOferta = new AreaCapturaOferta();

        Municipio mpio = this.generalesService
            .getCacheMunicipioByCodigo("08001");
        Departamento dpto = this.generalesService
            .getCacheDepartamentoByCodigo("08");
        Zona zona = this.generalesService.getCacheZonaByCodigo("0800101");

        this.areaCapturaOferta.setDepartamento(dpto);
        this.areaCapturaOferta.setDetalleCapturaOfertas(null);
        this.areaCapturaOferta.setEstado(EOfertaAreaCapturaOfertaEstado.VIGENTE
            .getCodigo());
        this.areaCapturaOferta.setEstructuraOrganizacionalCod(this.RECOLECTOR
            .getCodigoEstructuraOrganizacional());
        this.areaCapturaOferta.setFecha(Calendar.getInstance().getTime());
        this.areaCapturaOferta.setFechaLog(Calendar.getInstance().getTime());
        this.areaCapturaOferta.setMunicipio(mpio);
        this.areaCapturaOferta.setObservaciones(
            "Area creada desde prueba para integracion con process");
        this.areaCapturaOferta.setProcesoInstanciaId(null);
        this.areaCapturaOferta.setRegionCapturaOfertas(null);
        this.areaCapturaOferta.setUsuarioLog(testerId);
        this.areaCapturaOferta.setZona(zona);

        this.areaCapturaOferta = this.avaluosService.guardarAreaCapturaOferta(
            this.areaCapturaOferta, this.RECOLECTOR);

        // Se crean las manzanas/veredas
        this.crearDetallesCapturaOferta();

        LOGGER.debug(String.valueOf(this.areaCapturaOferta.getId()));

        // Valida si el area es centralizada
        this.banderaEsAreaCentralizada = this.generalesService
            .municipioPerteneceACatastroDescentralizado(this.areaCapturaOferta
                .getMunicipio().getCodigo());
    }

    /**
     * Método para insertar registros en la tabla DETALLE_CAPTURA_OFERTA
     *
     * @author rodrigo.hernandez
     */
    private void crearDetallesCapturaOferta() {
        List<String> listaDetallesCapturaOfertaStr = new ArrayList<String>();
        this.listaDetallesCapturaOferta = new ArrayList<DetalleCapturaOferta>();

        DetalleCapturaOferta detalleCapturaOferta = null;

        listaDetallesCapturaOfertaStr.add("08001030000000033");
        listaDetallesCapturaOfertaStr.add("08001030000000032");
        listaDetallesCapturaOfertaStr.add("08001030000000018");
        listaDetallesCapturaOfertaStr.add("08001010100000271");
        listaDetallesCapturaOfertaStr.add("08001010100000156");

        for (String codigoManzanaVereda : listaDetallesCapturaOfertaStr) {
            detalleCapturaOferta = new DetalleCapturaOferta();
            detalleCapturaOferta.setManzanaVeredaCodigo(codigoManzanaVereda);
            detalleCapturaOferta.setAreaCapturaOferta(this.areaCapturaOferta);
            detalleCapturaOferta.setFechaLog(Calendar.getInstance().getTime());
            detalleCapturaOferta.setRegionCapturaOferta(null);
            detalleCapturaOferta.setUsuarioLog(testerId);

            this.listaDetallesCapturaOferta.add(detalleCapturaOferta);
        }

        this.listaDetallesCapturaOferta = this.avaluosService
            .crearListaDetallesCapturaOferta(this.listaDetallesCapturaOferta);

        this.areaCapturaOferta
            .setDetalleCapturaOfertas(this.listaDetallesCapturaOferta);
    }

    /**
     * Método para crear un objeto de negocio OfertaInmobiliaria
     *
     * @param regionId Id de la Region creada
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    private OfertaInmobiliaria crearObjetoNegocio() {
        OfertaInmobiliaria ofertaInmobiliaria = new OfertaInmobiliaria();
        DivisionAdministrativa municipio = null;
        DivisionAdministrativa dpto = null;

        LOGGER.debug(this.getClass().getName() +
            " Creando objeto de negocio OfertaInmobiliaria " +
            this.areaCapturaOferta.getId());

        // Llenar el objeto con los datos necesarios
        UsuarioDTO responsableAsignacion;
        responsableAsignacion = this.RECOLECTOR;
        List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();

        // Se asignan los usuarios. Se deja recolector mientras se confirma
        // asignacion de usuarios a casos de uso en BD
        listaUsuario.add(responsableAsignacion);

        listaUsuario.add(usuariosAtlantico.get(ERol.COORDINADOR_GIT_AVALUOS));
        listaUsuario.add(usuariosAtlantico
            .get(ERol.RECOLECTOR_OFERTA_INMOBILIARIA));

        ofertaInmobiliaria.setUsuarios(listaUsuario);

        ofertaInmobiliaria.setTerritorial(responsableAsignacion
            .getDescripcionEstructuraOrganizacional());

        ofertaInmobiliaria.setIdentificador(this.areaCapturaOferta.getId());

        ofertaInmobiliaria
            .setObservaciones("Asignacion de Recolector a Region");
        ofertaInmobiliaria
            .setTransicion(ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA);

        if (!this.banderaEsAreaCentralizada) {
            municipio = new DivisionAdministrativa("08001",
                "BARRANQUILLA (Distrito Especial, Industrial y Portuario)");
            dpto = new DivisionAdministrativa("08", "ATLANTICO");
            ofertaInmobiliaria.setFuncionario(usuariosAtlantico.get(
                ERol.COORDINADOR_GIT_AVALUOS).getLogin());
        } else {
            municipio = new DivisionAdministrativa("11001", "BOGOTA D.C.");
            dpto = new DivisionAdministrativa("11", "CUNDINAMARCA");
            ofertaInmobiliaria.setFuncionario(usuariosAtlantico.get(
                ERol.INVESTIGADOR_MERCADO).getLogin());

        }

        ofertaInmobiliaria.setMunicipio(municipio);
        ofertaInmobiliaria.setDepartamento(dpto);

        return ofertaInmobiliaria;
    }

    /**
     * Crea un objeto RegionCapturaOferta
     *
     * @author rodrigo.hernandez
     *
     * @return RegionCapturaOferta
     */
    private void crearRegionCapturaOferta() {
        RegionCapturaOferta regionCreada = new RegionCapturaOferta();

        // Se crea la región
        regionCreada.setAreaCapturaOferta(this.areaCapturaOferta);
        regionCreada.setAsignadoRecolectorId(this.RECOLECTOR.getLogin());
        regionCreada.setAsignadoRecolectorNombre(this.RECOLECTOR
            .getNombreCompleto());
        regionCreada.setFechaAsignacionRecolector(Calendar.getInstance()
            .getTime());
        regionCreada.setEstado(EEstadoRegionCapturaOferta.ASIGNADA.getEstado());
        regionCreada.setUsuarioLog(testerId);
        regionCreada.setFechaLog(Calendar.getInstance().getTime());
        regionCreada.setObservaciones("Región creada desde pruebas de integración con process");

        // TODO: Crear caso para area descentralizada
        this.regionCapturaOferta = this.avaluosService
            .crearRegionCapturaOferta(regionCreada);

        // Se asocian las manzanas/veredas seleccionadas a la región
        this.avaluosService.asociarManzanasVeredasARegionCreada(
            this.areaCapturaOferta, this.regionCapturaOferta,
            this.cargarListaManzanasVeredas());
    }

    /**
     * Retorna una lista de codigos de manzanas/veredas asociados al area
     *
     *
     * @return lista con codigos de manzanas/veredas
     */
    private String cargarListaManzanasVeredas() {
        String cadenaCodigos = "";

        for (int contador = 0; contador < this.listaDetallesCapturaOferta
            .size(); contador++) {
            if (contador == 0) {
                cadenaCodigos = cadenaCodigos +
                    this.listaDetallesCapturaOferta.get(contador)
                        .getManzanaVeredaCodigo();
            } else {
                cadenaCodigos = cadenaCodigos +
                    "," +
                    this.listaDetallesCapturaOferta.get(contador)
                        .getManzanaVeredaCodigo();
            }
        }

        return cadenaCodigos;
    }

    /**
     * Método de creacion de comision de prueba
     */
    private void crearDatosComisionOferta() {
        try {
            LOGGER.info("Creando comision de oferta de prueba ");

            this.comisionOferta = new ComisionOferta();
            this.regionesAEliminar = new ArrayList<RegionCapturaOferta>();

            String numeroComision;
            DivisionAdministrativa municipio = null;
            DivisionAdministrativa dpto = null;

            municipio = new DivisionAdministrativa("08001",
                "BARRANQUILLA (Distrito Especial, Industrial y Portuario)");
            dpto = new DivisionAdministrativa("08", "ATLANTICO");

            numeroComision = String
                .valueOf(this.generalesService.generarNumeracion(
                    ENumeraciones.NUMERACION_TYA,
                    this.RECOLECTOR.getCodigoEstructuraOrganizacional(),
                    municipio.getNombre(), dpto.getNombre(), 0)[0]);

            //----- Se asignan datos para la comision
            this.comisionOferta.setDuracion(2D);
            this.comisionOferta
                .setEstado(EOfertaComisionEstado.POR_APROBAR
                    .toString());
            this.comisionOferta.setEstructuraOrganizacionalCod(this.RECOLECTOR
                .getCodigoEstructuraOrganizacional());
            this.comisionOferta.setObjeto("Comision creada desde prueba");
            this.comisionOferta
                .setFechaLog(new Date(System.currentTimeMillis()));
            this.comisionOferta.setNumero(numeroComision);

            this.comisionOferta.setRegionCapturaOfertas(Arrays
                .asList(this.regionCapturaOferta));
            this.comisionOferta.setUsuarioLog(testerId);

            this.avaluosService
                .guardarActualizarComisionOferta(this.comisionOferta,
                    this.RECOLECTOR, this.regionesAEliminar);

        } catch (ExcepcionSNC e) {

            LOGGER.error("Error creando comision de oferta de prueba: " + e.getMensaje());

        }

    }

    /**
     * Método para actualizar una comisión de prueba
     */
    private void aprobarComisionOferta() {
        try {
            LOGGER.info("Aprobando comision de oferta de prueba ");
            this.comisionOferta.setEstado(EOfertaComisionEstado.APROBADA
                .toString());
            //this.avaluosService.guardarActualizarComisionesOfertas(this.listaComisiones);
        } catch (Exception e) {
            LOGGER.error("Error aprobando comision de oferta de prueba ");
        }
    }

    /**
     * Crear ofertas de prueba
     */
    private void crearOfertasAIngresar() {
        try {
            LOGGER.info("Creando oferta inmobiliaria de prueba ");
            this.listaOfertas = this.avaluosService
                .buscarListaOfertasInmobiliariasPorRecolector(
                    this.RECOLECTOR.getLogin(),
                    this.regionCapturaOferta.getId(), EOfertaEstado.CREADA.getCodigo());
            for (co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria oferta
                : this.listaOfertas) {
                oferta.setEstado(EOfertaEstado.CREADA.toString());
            }
        } catch (ExcepcionSNC e) {
            e.getMensaje();
            LOGGER.error("Error creando oferta inmobiliaria de prueba");
        }
    }

    /**
     * actualizar estado de ofertas de prueba
     */
    private void cargarOfertasAlSNC() {
        try {
            LOGGER.info("Finalizando oferta inmobiliaria de prueba ");

            for (co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria oferta
                : this.listaOfertas) {
                oferta.setEstado(EOfertaEstado.FINALIZADA.toString());
                this.avaluosService.guardarActualizarOfertaInmobiliaria(oferta,
                    null, this.RECOLECTOR);
            }
        } catch (ExcepcionSNC e) {
            e.getMensaje();
            LOGGER.error("Error finalizando oferta inmobiliaria de prueba ");
        }

    }

}
