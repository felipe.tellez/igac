package co.gov.igac.snc.fachadas.test.integration.procesos.osmi.asignacion;

import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.fachadas.test.integration.procesos.osmi.MoverProcesoOsmiAbstractTest;
import co.gov.igac.snc.persistence.util.ERol;

public class MoverProcesoActRevisarAreaAsignada extends
    MoverProcesoOsmiAbstractTest {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MoverProcesoActRevisarAreaAsignada.class);

    /**
     * Se inician los objetos que se necesiten para las pruebas
     *
     * @author ariel.ortiz
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        try {
            this.testerId = "ariel.ortiz";
            this.cargarDatosPorTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    /**
     * Método para probar asignación de región de una area centralizada
     *
     * @author ariel.ortiz
     */
    @Test
    public void testCrearYAvanzarRevisarAreaAsignada() {
        try {
            this.crearProcesoYAvanzarActividades(this.ofertaInmobiliaria);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail(e.getMessage(), e);
        }

    }

    @Override
    protected void moverActividades(String idInstanciaProceso,
        OfertaInmobiliaria ofertaInmobiliaria) throws Exception {

        LOGGER.debug("En Proyectar Comision");
        List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();

        Thread.sleep(TIMER);

        // ------------------- Se actualiza el objeto de negocio para la transicion siguiente---------------		
        ofertaInmobiliaria
            .setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_APROBAR_AJUSTAR_ORDEN_COMISION);
        ofertaInmobiliaria.setObservaciones("Avance actividad");

        ofertaInmobiliaria.setFuncionario(usuariosAtlantico.get(
            ERol.COORDINADOR_GIT_AVALUOS).getLogin());

        listaUsuarios.add(this.RECOLECTOR);

        ofertaInmobiliaria.setUsuarios(listaUsuarios);

        // ------------------------------ Avanzar a actividad Aprobar ajustar orden comisión	 ----------------------------------
        procesoService.avanzarActividadPorIdProceso(idInstanciaProceso,
            ofertaInmobiliaria);

        LOGGER.debug("En  Aprobar orden comision y haciendo transicion a Revisar Area Asignada");

        // ------------------- Se actualiza el objeto de negocio para la transicion siguiente---------------
        ofertaInmobiliaria
            .setTransicion(
                ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_REVISAR_AREA_MUNICIPIO_ASIGNADO);
        ofertaInmobiliaria.setObservaciones("Avance actividad ariel.ortiz");

        ofertaInmobiliaria.setFuncionario(usuariosAtlantico.get(
            ERol.RECOLECTOR_OFERTA_INMOBILIARIA).getLogin());

        listaUsuarios.add(this.RECOLECTOR);

        ofertaInmobiliaria.setUsuarios(listaUsuarios);

        // ------------------------------ Avanzar a actividad Proyeccion ----------------------------------
        procesoService.avanzarActividadPorIdProceso(idInstanciaProceso,
            ofertaInmobiliaria);

        LOGGER.debug("En  Revisar area asignada y haciendo transicion a ingresar ofertas");

        // ------------------- Se actualiza el objeto de negocio para la transicion siguiente---------------
        ofertaInmobiliaria
            .setTransicion(ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_INGRESAR_OFERTAS);
        ofertaInmobiliaria.setObservaciones("Avance actividad ariel.ortiz");

        ofertaInmobiliaria.setFuncionario(usuariosAtlantico.get(
            ERol.RECOLECTOR_OFERTA_INMOBILIARIA).getLogin());

        listaUsuarios.add(this.RECOLECTOR);

        ofertaInmobiliaria.setUsuarios(listaUsuarios);

    }

    @Override
    protected String crearProceso(OfertaInmobiliaria ofertaInmobiliaria)
        throws Exception {

        String id = procesoService.crearProceso(ofertaInmobiliaria);
        List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();

        try {
            Thread.sleep(TIMER);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        LOGGER.info("Proceso creado: " + ((id != null) ? "SI" : "NO"));
        LOGGER.info("ID de proceso: " + id);

        //---- Se definen datos para nueva transicion
        listaUsuario.add(usuariosAtlantico.get(ERol.COORDINADOR_GIT_AVALUOS));
        listaUsuario.add(usuariosAtlantico
            .get(ERol.RECOLECTOR_OFERTA_INMOBILIARIA));

        ofertaInmobiliaria.setIdCorrelacion(ofertaInmobiliaria
            .getIdentificador());
        ofertaInmobiliaria.setIdentificador(this.regionCapturaOferta.getId());

        ofertaInmobiliaria
            .setTransicion(ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION);

        ofertaInmobiliaria
            .setOperacion(ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION);
        ofertaInmobiliaria.setObservaciones("Avance actividad");

        ofertaInmobiliaria.setUsuarios(listaUsuario);

        // Se crea el 2do proceso y se deja en la transicion PROYECTAR_COMISION
        String idRegion = procesoService.crearProceso(ofertaInmobiliaria);
        LOGGER.info("Proceso creado: " + ((idRegion != null) ? "SI" : "NO"));
        LOGGER.info("ID de proceso: " + idRegion);

        return idRegion;
    }

}
