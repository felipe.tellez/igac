package co.gov.igac.snc.persistence.entity.conservacion;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * HPersonaPredio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_PERSONA_PREDIO", schema = "SNC_CONSERVACION")
public class HPersonaPredio implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = 4446009663557379991L;
    protected HPersonaPredioId id;
    private Predio predio;
    private Persona persona;
    private String tipo;
    private Double participacion;
    private Timestamp fechaInscripcionCatastral;
    private String cancelaInscribe;
    private String usuarioLog;
    private Timestamp fechaLog;
    private HPredio HPredio;

    private List<HPersonaPredioPropiedad> hPersonaPredioPropiedads =
        new ArrayList<HPersonaPredioPropiedad>();
    // Constructors

    /** default constructor */
    public HPersonaPredio() {
    }

    /** minimal constructor
     *
     * @param id
     * @param predio
     * @param persona
     * @param participacion
     * @param usuarioLog
     * @param fechaLog
     * @param hPredio
     */
    public HPersonaPredio(HPersonaPredioId id, Predio predio,
        Persona persona, Double participacion, String usuarioLog,
        Timestamp fechaLog, HPredio hPredio) {
        this.id = id;
        this.predio = predio;
        this.persona = persona;
        this.participacion = participacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.HPredio = hPredio;
    }

    /** full constructor
     *
     * @param id
     * @param predio
     * @param persona
     * @param tipo
     * @param participacion
     * @param fechaInscripcionCatastral
     * @param cancelaInscribe
     * @param usuarioLog
     * @param fechaLog
     * @param hPredio
     * @param hPersonaPredioPropiedads
     */
    public HPersonaPredio(HPersonaPredioId id, Predio predio,
        Persona persona, String tipo, Double participacion,
        Timestamp fechaInscripcionCatastral, String cancelaInscribe,
        String usuarioLog, Timestamp fechaLog, HPredio hPredio,
        List<HPersonaPredioPropiedad> hPersonaPredioPropiedads) {
        this.id = id;
        this.predio = predio;
        this.persona = persona;
        this.tipo = tipo;
        this.participacion = participacion;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.HPredio = hPredio;
//                this.hPersonaPredioPropiedads = hPersonaPredioPropiedads;
    }

    // Property accessors
    @EmbeddedId
    public HPersonaPredioId getId() {
        return this.id;
    }

    public void setId(HPersonaPredioId id) {
        this.id = id;
    }

//	@Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

//	@Column(name = "PERSONA_ID", nullable = false, precision = 10, scale = 0)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PERSONA_ID", nullable = false)
    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Column(name = "TIPO", length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "PARTICIPACION", nullable = false, precision = 5)
    public Double getParticipacion() {
        return this.participacion;
    }

    public void setParticipacion(Double participacion) {
        this.participacion = participacion;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Timestamp getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Timestamp fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

    @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "ID", insertable = false, updatable =
        false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    public HPredio getHPredio() {
        return HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPersonaPredio")
    public List<HPersonaPredioPropiedad> gethPersonaPredioPropiedads() {
        return hPersonaPredioPropiedads;
    }

    public void sethPersonaPredioPropiedads(List<HPersonaPredioPropiedad> hPersonaPredioPropiedads) {
        this.hPersonaPredioPropiedads = hPersonaPredioPropiedads;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HPersonaPredio)) {
            return false;
        }
        HPersonaPredio other = (HPersonaPredio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
