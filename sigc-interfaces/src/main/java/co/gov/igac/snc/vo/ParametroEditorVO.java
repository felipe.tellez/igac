/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * identifica los parametros del editor
 *
 * @author andres.eslava
 */
public class ParametroEditorVO implements Serializable {

    private static final long serialVersionUID = 7276377145365782579L;

    private String nombreParametro;
    private String valorParametro;

    public ParametroEditorVO(String nombre, String valor) {
        this.nombreParametro = nombre;
        this.valorParametro = valor;

    }

    public String getNombreParametro() {
        return nombreParametro;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    public String getValorParametro() {
        return valorParametro;
    }

    public void setValorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
    }

}
