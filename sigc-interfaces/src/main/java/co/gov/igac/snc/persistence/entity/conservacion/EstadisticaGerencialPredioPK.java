package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * The primary key class for the ESTADISTICA_GERENCIAL_PREDIO database table.
 *
 */
@Embeddable
public class EstadisticaGerencialPredioPK implements Serializable {
    //default serial version id, required for serializable classes.

    private static final long serialVersionUID = 1L;
    private Municipio municipio;
    private Integer anio;
    private String zona;
    private String destinoEconomico;
    private String condicionPropiedad;

    public EstadisticaGerencialPredioPK() {
    }

    public Integer getAnio() {
        return this.anio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @Column(name = "DESTINO_ECONOMICO")
    public String getDestinoEconomico() {
        return this.destinoEconomico;
    }

    public void setDestinoEconomico(String destinoEconomico) {
        this.destinoEconomico = destinoEconomico;
    }

    @Column(name = "CONDICION_PROPIEDAD")
    public String getCondicionPropiedad() {
        return this.condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof EstadisticaGerencialPredioPK)) {
            return false;
        }
        EstadisticaGerencialPredioPK castOther = (EstadisticaGerencialPredioPK) other;
        return this.municipio.equals(castOther.municipio) &&
            (this.anio == castOther.anio) &&
            this.zona.equals(castOther.zona) &&
            this.destinoEconomico.equals(castOther.destinoEconomico) &&
            this.condicionPropiedad.equals(castOther.condicionPropiedad);
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.municipio.hashCode();
        hash = hash * prime + ((int) (this.anio ^ (this.anio >>> 32)));
        hash = hash * prime + this.zona.hashCode();
        hash = hash * prime + this.destinoEconomico.hashCode();
        hash = hash * prime + this.condicionPropiedad.hashCode();

        return hash;
    }
}
