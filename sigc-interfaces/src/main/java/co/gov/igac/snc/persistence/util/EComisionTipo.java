/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles tipos de una comisión. Corresponde esta enumeración al dominio
 * COMISION_TIPO
 *
 * @author pedro.garcia
 */
public enum EComisionTipo {

    //D: el nombre de la enumeración corresponde al código en bd 
    //   código(valor)
    CONSERVACION("Conservación"),
    DEPURACION("Depuración");

    private String valor;

    private EComisionTipo(String valorP) {
        this.valor = valorP;
    }

    public String getValor() {
        return this.valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
