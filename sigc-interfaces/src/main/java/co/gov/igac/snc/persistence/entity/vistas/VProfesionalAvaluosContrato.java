package co.gov.igac.snc.persistence.entity.vistas;

import co.gov.igac.snc.persistence.util.ESiNo;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the V_PROFESIONAL_AVALUOS_CONTRATO database table.
 */
/*
 * @modified rodrigo.hernandez - 19-09-2012 - Cambio de tipo de datos. BigDecimal a Long -
 * 20-09-2012 - Adición de atributo VProfesionalAvaluosContratoPK para manejo de id - Adición de
 * nombres de columnas a metodos get faltantes
 *
 * @modified felipe.cadena - 21-09-2012 - Se agrega Transient para el nombre completo
 *
 * @modified pedro.garcia - método transient isTieneContratoActivo
 */
@Entity
@Table(name = "V_PROFESIONAL_AVALUOS_CONTRATO")
public class VProfesionalAvaluosContrato implements Serializable {

    private static final long serialVersionUID = 3279770522646844151L;

    private String activo;
    private Long anio;
    private Long avaluos;
    private Long calificacionPromedio;
    private String celular;
    private Long contratoId;
    private String contratoNumero;
    private String correoElectronico;
    private String direccion;
    private String direccionDepartamentoCodigo;
    private String direccionMunicipioCodigo;
    private String interventor;
    private String nombreDepartamento;
    private String nombreEstructura;
    private String nombreMunicipio;
    private String numeroIdentificacion;
    private String primerApellido;
    private String primerNombre;
    private String profesion;
    private Long profesionalAvaluosId;
    private String segundoApellido;
    private String segundoNombre;
    private String telefono;
    private String territorialCodigo;
    private String tipoIdentificacion;
    private VProfesionalAvaluosContratoPK idVPAC;

    public VProfesionalAvaluosContrato() {
    }

    @EmbeddedId
    public VProfesionalAvaluosContratoPK getIdVPAC() {
        return idVPAC;
    }

    public void setIdVPAC(VProfesionalAvaluosContratoPK idVPAC) {
        this.idVPAC = idVPAC;
    }

    @Column(name = "ACTIVO", nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "ANIO", nullable = false, precision = 4)
    public Long getAnio() {
        return this.anio;
    }

    public void setAnio(Long anio) {
        this.anio = anio;
    }

    @Column(name = "AVALUOS")
    public Long getAvaluos() {
        return this.avaluos;
    }

    public void setAvaluos(Long avaluos) {
        this.avaluos = avaluos;
    }

    @Column(name = "CALIFICACION_PROMEDIO", nullable = false, precision = 6, scale = 2)
    public Long getCalificacionPromedio() {
        return this.calificacionPromedio;
    }

    public void setCalificacionPromedio(Long calificacionPromedio) {
        this.calificacionPromedio = calificacionPromedio;
    }

    @Column(name = "CELULAR", length = 50)
    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Column(name = "CONTRATO_ID", insertable = false, updatable = false)
    public Long getContratoId() {
        return this.contratoId;
    }

    public void setContratoId(Long contratoId) {
        this.contratoId = contratoId;
    }

    @Column(name = "CONTRATO_NUMERO", nullable = false, length = 50)
    public String getContratoNumero() {
        return this.contratoNumero;
    }

    public void setContratoNumero(String contratoNumero) {
        this.contratoNumero = contratoNumero;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "DIRECCION", length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "DIRECCION_DEPARTAMENTO_CODIGO", length = 2)
    public String getDireccionDepartamentoCodigo() {
        return this.direccionDepartamentoCodigo;
    }

    public void setDireccionDepartamentoCodigo(
        String direccionDepartamentoCodigo) {
        this.direccionDepartamentoCodigo = direccionDepartamentoCodigo;
    }

    @Column(name = "DIRECCION_MUNICIPIO_CODIGO", length = 5)
    public String getDireccionMunicipioCodigo() {
        return this.direccionMunicipioCodigo;
    }

    public void setDireccionMunicipioCodigo(String direccionMunicipioCodigo) {
        this.direccionMunicipioCodigo = direccionMunicipioCodigo;
    }

    @Column(name = "INTERVENTOR", nullable = false, length = 100)
    public String getInterventor() {
        return this.interventor;
    }

    public void setInterventor(String interventor) {
        this.interventor = interventor;
    }

    @Column(name = "NOMBRE_DEPARTAMENTO", length = 4000)
    public String getNombreDepartamento() {
        return this.nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    @Column(name = "NOMBRE_ESTRUCTURA", length = 4000)
    public String getNombreEstructura() {
        return this.nombreEstructura;
    }

    public void setNombreEstructura(String nombreEstructura) {
        this.nombreEstructura = nombreEstructura;
    }

    @Column(name = "NOMBRE_MUNICIPIO", length = 4000)
    public String getNombreMunicipio() {
        return this.nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    @Column(name = "NUMERO_IDENTIFICACION", nullable = false, length = 30)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "PRIMER_APELLIDO", nullable = false, length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "PRIMER_NOMBRE", nullable = false, length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "PROFESION", length = 4000)
    public String getProfesion() {
        return this.profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    @Column(name = "PROFESIONAL_AVALUOS_ID", insertable = false, updatable = false)
    public Long getProfesionalAvaluosId() {
        return this.profesionalAvaluosId;
    }

    public void setProfesionalAvaluosId(Long profesionalAvaluosId) {
        this.profesionalAvaluosId = profesionalAvaluosId;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "TELEFONO", length = 50)
    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Column(name = "TERRITORIAL_CODIGO", nullable = false, length = 20)
    public String getTerritorialCodigo() {
        return this.territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que retorna el nombre completo del avaluador
     *
     * @author felipe.cadena
     *
     * */
    @Transient
    public String getNombreCompleto() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if (this.primerNombre != null) {
            sb.append(this.primerNombre).append(" ");
        }
        if (this.segundoNombre != null) {
            sb.append(this.segundoNombre).append(" ");
        }
        if (this.primerApellido != null) {
            sb.append(this.primerApellido).append(" ");
        }
        if (this.segundoApellido != null) {
            sb.append(this.segundoApellido).append(" ");
        }
        return sb.toString();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna true si el campo 'activo' está en SI
     *
     * @author pedro.garcia
     */
    @Transient
    public boolean isTieneContratoActivo() {
        boolean answer = false;

        if (this.activo != null && !this.activo.isEmpty()) {
            if (this.activo.equals(ESiNo.SI.getCodigo())) {
                answer = true;
            }
        }
        return answer;
    }
}
