package co.gov.igac.snc.persistence.entity.i11n;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the PROPIETARIO database table.
 *
 */
@Entity
@Table(name = "PROPIETARIO", schema = "SNC_INTERRELACION")
public class Propietario implements Serializable {

    private static final long serialVersionUID = 1L;
    private String apellido1;
    private String apellido2;
    private Long infInterrelacionadaNciId;
    private Long ncpId;
    private String nombre1;
    private String nombre2;
    private String numeroDocumento;
    private String razonSocial;
    private String tipoDocumento;

    public Propietario() {
    }

    @Column(name = "APELLIDO_1")
    public String getApellido1() {
        return this.apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    @Column(name = "APELLIDO_2")
    public String getApellido2() {
        return this.apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    @Column(name = "INF_INTERRELACIONADA_NCI_ID")
    public Long getInfInterrelacionadaNciId() {
        return this.infInterrelacionadaNciId;
    }

    public void setInfInterrelacionadaNciId(Long infInterrelacionadaNciId) {
        this.infInterrelacionadaNciId = infInterrelacionadaNciId;
    }

    @Id
    @Column(name = "NCP_ID")
    public Long getNcpId() {
        return this.ncpId;
    }

    public void setNcpId(Long ncpId) {
        this.ncpId = ncpId;
    }

    @Column(name = "NOMBRE_1")
    public String getNombre1() {
        return this.nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    @Column(name = "NOMBRE_2")
    public String getNombre2() {
        return this.nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    @Column(name = "NUMERO_DOCUMENTO")
    public String getNumeroDocumento() {
        return this.numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @Column(name = "RAZON_SOCIAL")
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "TIPO_DOCUMENTO")
    public String getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

}
