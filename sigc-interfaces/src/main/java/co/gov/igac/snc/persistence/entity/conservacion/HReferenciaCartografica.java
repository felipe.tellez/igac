package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the H_REFERENCIA_CARTOGRAFICA database table.
 */
// TODO :: david.cifuentes :: Falta colocarle la secuencia al id, hablar con
// Julio.
@Entity
@Table(name = "H_REFERENCIA_CARTOGRAFICA", schema = "SNC_CONSERVACION")
public class HReferenciaCartografica implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String foto;
    private String plancha;
    private BigDecimal predioId;
    private String usuarioLog;
    private String vuelo;

    private String cancelaInscribe;

    public HReferenciaCartografica() {
    }

    @Id
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getPlancha() {
        return this.plancha;
    }

    public void setPlancha(String plancha) {
        this.plancha = plancha;
    }

    @Column(name = "PREDIO_ID")
    public BigDecimal getPredioId() {
        return this.predioId;
    }

    public void setPredioId(BigDecimal predioId) {
        this.predioId = predioId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public String getVuelo() {
        return this.vuelo;
    }

    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

}
