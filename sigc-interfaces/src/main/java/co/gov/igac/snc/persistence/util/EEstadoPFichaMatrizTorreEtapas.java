package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores para el estado de una torre en trámites de PH por etapas.
 *
 * @author david.cifuentes
 */
public enum EEstadoPFichaMatrizTorreEtapas {

    CANCELADA("CANCELADA"),
    EN_FIRME("EN FIRME"),
    NUEVA("NUEVA");

    private String estado;

    EEstadoPFichaMatrizTorreEtapas(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

}
