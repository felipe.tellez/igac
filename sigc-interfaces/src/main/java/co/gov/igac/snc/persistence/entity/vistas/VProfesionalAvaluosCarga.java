package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the V_PROFESIONAL_AVALUOS_CARGA database table.
 *
 */
@Entity
@Table(name = "V_PROFESIONAL_AVALUOS_CARGA")
public class VProfesionalAvaluosCarga implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer anio;
    private Integer avaluosEnccalidad;
    private Integer avaluosEnproceso;
    private Integer avaluosRealizados;
    private Double calificacionPromedio;
    private String contratoActivo;
    private Long contratoId;
    private String contratoNumero;
    private Date fechaDesde;
    private Date fechaHasta;
    private String numeroIdentificacion;
    private String primerApellido;
    private String primerNombre;
    private String profesionalActivo;
    private Long profesionalAvaluosId;
    private Double saldoPorPagar;
    private String segundoApellido;
    private String segundoNombre;
    private String tipoIdentificacion;
    private String tipoVinculacion;
    private String territorialCodigo;
    private Double valorComprometido;
    private Double valorTotalContrato;
    private Double valorTotalPagos;

    public VProfesionalAvaluosCarga() {
    }

    public Integer getAnio() {
        return this.anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "AVALUOS_ENCCALIDAD")
    public Integer getAvaluosEnccalidad() {
        return this.avaluosEnccalidad;
    }

    public void setAvaluosEnccalidad(Integer avaluosEnccalidad) {
        this.avaluosEnccalidad = avaluosEnccalidad;
    }

    @Column(name = "TERRITORIAL_CODIGO")
    public String getTerritorialCodigo() {
        return this.territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    @Column(name = "AVALUOS_ENPROCESO")
    public Integer getAvaluosEnproceso() {
        return this.avaluosEnproceso;
    }

    public void setAvaluosEnproceso(Integer avaluosEnproceso) {
        this.avaluosEnproceso = avaluosEnproceso;
    }

    @Column(name = "AVALUOS_REALIZADOS")
    public Integer getAvaluosRealizados() {
        return this.avaluosRealizados;
    }

    public void setAvaluosRealizados(Integer avaluosRealizados) {
        this.avaluosRealizados = avaluosRealizados;
    }

    @Column(name = "CALIFICACION_PRONEDIO")
    public Double getCalificacionPromedio() {
        return this.calificacionPromedio;
    }

    public void setCalificacionPromedio(Double calificacionPronedio) {
        this.calificacionPromedio = calificacionPronedio;
    }

    @Column(name = "CONTRATO_ACTIVO")
    public String getContratoActivo() {
        return this.contratoActivo;
    }

    public void setContratoActivo(String contratoActivo) {
        this.contratoActivo = contratoActivo;
    }

    @Column(name = "CONTRATO_ID")
    public Long getContratoId() {
        return this.contratoId;
    }

    public void setContratoId(Long contratoId) {
        this.contratoId = contratoId;
    }

    @Column(name = "CONTRATO_NUMERO")
    public String getContratoNumero() {
        return this.contratoNumero;
    }

    public void setContratoNumero(String contratoNumero) {
        this.contratoNumero = contratoNumero;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESDE")
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_HASTA")
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Column(name = "NUMERO_IDENTIFICACION")
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "PRIMER_APELLIDO")
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "PRIMER_NOMBRE")
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "PROFESIONAL_ACTIVO")
    public String getProfesionalActivo() {
        return this.profesionalActivo;
    }

    public void setProfesionalActivo(String profesionalActivo) {
        this.profesionalActivo = profesionalActivo;
    }

    @Id
    @Column(name = "PROFESIONAL_AVALUOS_ID")
    public Long getProfesionalAvaluosId() {
        return this.profesionalAvaluosId;
    }

    public void setProfesionalAvaluosId(Long profesionalAvaluosId) {
        this.profesionalAvaluosId = profesionalAvaluosId;
    }

    @Column(name = "SALDO_POR_PAGAR")
    public Double getSaldoPorPagar() {
        return this.saldoPorPagar;
    }

    public void setSaldoPorPagar(Double saldoPorPagar) {
        this.saldoPorPagar = saldoPorPagar;
    }

    @Column(name = "SEGUNDO_APELLIDO")
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "SEGUNDO_NOMBRE")
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "TIPO_IDENTIFICACION")
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "TIPO_VINCULACION")
    public String getTipoVinculacion() {
        return this.tipoVinculacion;
    }

    public void setTipoVinculacion(String tipoVinculacion) {
        this.tipoVinculacion = tipoVinculacion;
    }

    @Column(name = "VALOR_COMPROMETIDO")
    public Double getValorComprometido() {
        return this.valorComprometido;
    }

    public void setValorComprometido(Double valorComprometido) {
        this.valorComprometido = valorComprometido;
    }

    @Column(name = "VALOR_TOTAL_CONTRATO")
    public Double getValorTotalContrato() {
        return this.valorTotalContrato;
    }

    public void setValorTotalContrato(Double valorTotalContrato) {
        this.valorTotalContrato = valorTotalContrato;
    }

    @Column(name = "VALOR_TOTAL_PAGOS")
    public Double getValorTotalPagos() {
        return this.valorTotalPagos;
    }

    public void setValorTotalPagos(Double valorTotalPagos) {
        this.valorTotalPagos = valorTotalPagos;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que retorna el nombre completo del avaluador
     *
     * @author felipe.cadena
     *
     *
     */
    @Transient
    public String getNombreCompleto() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if (this.primerNombre != null) {
            sb.append(this.primerNombre).append(" ");
        }
        if (this.segundoNombre != null) {
            sb.append(this.segundoNombre).append(" ");
        }
        if (this.primerApellido != null) {
            sb.append(this.primerApellido).append(" ");
        }
        if (this.segundoApellido != null) {
            sb.append(this.segundoApellido).append(" ");
        }
        return sb.toString();
    }
//--------------------------------------------------------------------------------------------------
}
