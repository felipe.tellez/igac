package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio PREDIO_INCONSISTENCIA_ESTAD <br/>
 *
 * @author pedro.garcia
 */
public enum EPredioInconsistenciaEstado {

    //D: ("código", "valor")
    INCONSISTENTE("I", "INCONSISTENTE"),
    NUEVA("N", "NUEVA"),
    SOLUCIONADA("S", "SOLUCIONADA");

    private String codigo;

    private String valor;

    private EPredioInconsistenciaEstado(String cod, String val) {
        this.codigo = cod;
        this.valor = val;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
