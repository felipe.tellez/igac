/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * Entidad que representa la informacion de mananzanas a transmitir por servicio.
 *
 * @author andres.eslava
 */
public class ManzanaVO implements Serializable {

    private static final long serialVersionUID = 3360556872325692776L;

    String codigoManzana;
    String cancelaInscribeManzana;

    public ManzanaVO(String codigoManzana, String cancelaInscribeManzana) {
        this.codigoManzana = codigoManzana;
        this.cancelaInscribeManzana = cancelaInscribeManzana;
    }

    public String getCodigoManzana() {
        return codigoManzana;
    }

    public void setCodigoManzana(String codigoManzana) {
        this.codigoManzana = codigoManzana;
    }

    public String getCancelaInscribeManzana() {
        return cancelaInscribeManzana;
    }

    public void setCancelaInscribeManzana(String cancelaInscribeManzana) {
        this.cancelaInscribeManzana = cancelaInscribeManzana;
    }

}
