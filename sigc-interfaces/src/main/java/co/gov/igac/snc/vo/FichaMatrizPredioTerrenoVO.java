package co.gov.igac.snc.vo;

import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import java.io.Serializable;

/**
 * VO creado para realizar la carga de los terrenos asociados a la ficha resultante en el caso del
 * englobe virtual
 *
 * @author jonathan.chacon
 */
public class FichaMatrizPredioTerrenoVO implements Serializable {

    private static final long serialVersionUID = 4262970936756309656L;
    private Long id;
    private PFichaMatriz fichaMatriz;
    private String predio;
    private String cancelaInscribe;
    private List<UnidadConstruccionVO> unidadesConstruccions;
    private List<PUnidadConstruccionVO> pUnidadesConstruccions;

    /** Empty constructor */
    public FichaMatrizPredioTerrenoVO() {

    }

    /** Constructor a partir de un FichaMatrizPredio */
    public FichaMatrizPredioTerrenoVO(FichaMatrizPredioTerreno predio) {
        this.predio = predio.getPredio().getNumeroPredial();
    }

    /** Constructor a partir de un PFichaMatrizPredio */
    public FichaMatrizPredioTerrenoVO(PFichaMatrizPredioTerreno predio) {
        this.predio = predio.getPredio().getNumeroPredial();
        this.cancelaInscribe = predio.getCancelaInscribe();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PFichaMatriz getFichaMatriz() {
        return fichaMatriz;
    }

    public void setFichaMatriz(PFichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    public String getPredio() {
        return predio;
    }

    public void String(String predio) {
        this.predio = predio;
    }

    public String getCancelaInscribe() {
        return cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public List<UnidadConstruccionVO> getUnidadesConstruccions() {
        return unidadesConstruccions;
    }

    public void setUnidadesConstruccions(List<UnidadConstruccionVO> unidadesConstruccions) {
        this.unidadesConstruccions = unidadesConstruccions;
    }

    public List<PUnidadConstruccionVO> getpUnidadesConstruccions() {
        return pUnidadesConstruccions;
    }

    public void setpUnidadesConstruccions(List<PUnidadConstruccionVO> pUnidadesConstruccions) {
        this.pUnidadesConstruccions = pUnidadesConstruccions;
    }

}
