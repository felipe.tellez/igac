/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * clase que se usa como pk para la vista VEjecutorComision. Hay que usarla porque como no hay un
 * campo que pueda servir de id (campo en solitario que tenga valores únicos en la tabla) en ese
 * entity, las consultas traen resultados extraños
 *
 * La combinación de campos definida como pk es id, municipio, idFuncionarioEjecutor
 *
 * @author pedro.garcia
 */
@Embeddable
public class VEjecutorComisionPK implements Serializable {

    private static final long serialVersionUID = -454315887614421210L;

    private Long id;
    private String municipio;
    private String idFuncionarioEjecutor;

    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "MUNICIPIO")
    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    @Column(name = "ID_FUNCIONARIO_EJECUTOR")
    public String getIdFuncionarioEjecutor() {
        return this.idFuncionarioEjecutor;
    }

    public void setIdFuncionarioEjecutor(String idFuncionarioEjecutor) {
        this.idFuncionarioEjecutor = idFuncionarioEjecutor;
    }

    /**
     * no se usa, pero se implementó para evitar warnings
     *
     * @param another
     * @return
     */
    public boolean equals(VEjecutorComisionPK another) {

        boolean answer = false;

        if (this.id.longValue() == another.id.longValue() &&
            this.municipio.equals(another.municipio) &&
            this.idFuncionarioEjecutor.equals(another.idFuncionarioEjecutor)) {
            answer = true;
        }
        return answer;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result +
            ((this.idFuncionarioEjecutor == null) ? 0 : this.idFuncionarioEjecutor.hashCode());
        result = prime * result + ((this.municipio == null) ? 0 : this.municipio.hashCode());

        return result;

    }

    @Override
    public boolean equals(Object another) {
        return this.equals((VEjecutorComisionPK) another);
    }

//end of class
}
