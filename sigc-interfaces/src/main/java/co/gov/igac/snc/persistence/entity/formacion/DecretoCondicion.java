package co.gov.igac.snc.persistence.entity.formacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DECRETO_CONDICION database table.
 *
 */
@Entity
@Table(name = "DECRETO_CONDICION", schema = "SNC_FORMACION")
public class DecretoCondicion implements Serializable {

    // Fields
    private static final long serialVersionUID = 1L;
    private Long id;
    private String condicion;
    private DecretoAvaluoAnual decretoAvaluoAnual;
    private String descripcion;
    private Date fechaLog;
    private Double incremento;
    private String usuarioLog;

    // Constructors
    /** default constructor */
    public DecretoCondicion() {
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DECRETO_CONDICION_Id_SEQ")
    @SequenceGenerator(name = "DECRETO_CONDICION_Id_SEQ", sequenceName = "DECRETO_CONDICION_Id_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCondicion() {
        return this.condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    //bi-directional many-to-one association to DecretoAvaluoAnual
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DECRETO_AVALUO_ANUAL_ID", nullable = false)
    public DecretoAvaluoAnual getDecretoAvaluoAnual() {
        return this.decretoAvaluoAnual;
    }

    public void setDecretoAvaluoAnual(DecretoAvaluoAnual decretoAvaluoAnual) {
        this.decretoAvaluoAnual = decretoAvaluoAnual;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public Double getIncremento() {
        return this.incremento;
    }

    public void setIncremento(Double incremento) {
        this.incremento = incremento;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
