package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * TramiteEstado entity. @author MyEclipse Persistence Tools
 *
 * @modified by ??? what: - adición de anotaciones para uso de secuencia en el id
 *
 * @modified pedro.garcia - adición de método constructor
 * @modified javier.aponte - adición de campos para almacenar el rol del responsable
 */
//TODO::juan.agudelo::documentar cambios en la clase según se acordó para los entities::pedro.garcia
@Entity
@NamedQueries({@NamedQuery(name = "findTramiteEstadoByTramite", query =
        "from TramiteEstado te where te.tramite= :tramite")})
@Table(name = "TRAMITE_ESTADO", schema = "SNC_TRAMITE")
public class TramiteEstado implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3794661110689541136L;

    // Fields
    private Long id;
    private Tramite tramite;
    private String estado;
    private Date fechaInicio;
    private String observaciones;
    private String motivo;
    private String razonCancelacion;
    private String responsable;
    private String usuarioLog;
    private String rolResponsable;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramiteEstado() {
    }

    /** minimal constructor */
    public TramiteEstado(Long id, Tramite tramite, String estado,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.estado = estado;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteEstado(Long id, Tramite tramite, String estado,
        Date fechaInicio, String motivo, String responsable,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.estado = estado;
        this.fechaInicio = fechaInicio;
        this.motivo = motivo;
        this.responsable = responsable;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * NO BORRAR
     *
     * @pedro.garcia
     * @return
     */
    public TramiteEstado(String estado, Tramite tramite, String usuario) {

        this.estado = estado;
        this.tramite = tramite;
        this.responsable = usuario;
        this.motivo = "";
        this.fechaInicio = new Date();
        this.fechaLog = new Date();
        this.usuarioLog = usuario;
    }

//--------------------------------------------------------------------------------------------------
    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_ESTADO_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_ESTADO_ID_SEQ", sequenceName = "TRAMITE_ESTADO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO", length = 7)
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Column(name = "OBSERVACIONES", nullable = true, length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "MOTIVO", length = 2000)
    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Column(name = "RESPONSABLE", length = 100)
    public String getResponsable() {
        return this.responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    @Column(name = "ROL_RESPONSABLE", length = 200)
    public String getRolResponsable() {
        return rolResponsable;
    }

    public void setRolResponsable(String rolResponsable) {
        this.rolResponsable = rolResponsable;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "RAZON_CANCELACION")
    public String getRazonCancelacion() {
        return razonCancelacion;
    }

    public void setRazonCancelacion(String razonCancelacion) {
        this.razonCancelacion = razonCancelacion;
    }

}
