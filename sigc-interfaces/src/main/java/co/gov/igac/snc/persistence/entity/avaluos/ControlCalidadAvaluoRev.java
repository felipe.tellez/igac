package co.gov.igac.snc.persistence.entity.avaluos;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONTROL_CALIDAD_AVALUO_REV database table.
 *
 */
@Entity
@Table(name = "CONTROL_CALIDAD_AVALUO_REV")
public class ControlCalidadAvaluoRev implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String aprobo;
    private Date fecha;
    private Date fechaLog;
    private Long numero;
    private String observaciones;
    private Long plazo;
    private String usuarioLog;
    private ControlCalidadAvaluo controlCalidadAvaluo;
    private Documento soporteDocumento;
    private List<ControlCalidadEspecificacion> controlCalidadEspecificacions;

    public ControlCalidadAvaluoRev() {
    }

    @Id
    @SequenceGenerator(name = "CONTROL_CALIDAD_AVALUO_REV_ID_GENERATOR", sequenceName =
        "CONTROL_CALIDAD_AVALUO__ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTROL_CALIDAD_AVALUO_REV_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAprobo() {
        return this.aprobo;
    }

    public void setAprobo(String aprobo) {
        this.aprobo = aprobo;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public Long getNumero() {
        return this.numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPlazo() {
        return this.plazo;
    }

    public void setPlazo(Long plazo) {
        this.plazo = plazo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ControlCalidadAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_CONTROL_CALIDAD_ID")
    public ControlCalidadAvaluo getControlCalidadAvaluo() {
        return this.controlCalidadAvaluo;
    }

    public void setControlCalidadAvaluo(ControlCalidadAvaluo controlCalidadAvaluo) {
        this.controlCalidadAvaluo = controlCalidadAvaluo;
    }

    //bi-directional many-to-one association to ControlCalidadAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID")
    public Documento getSoporteDocumento() {
        return this.soporteDocumento;
    }

    public void setSoporteDocumento(Documento soporteDocumento) {
        this.soporteDocumento = soporteDocumento;
    }

    //bi-directional many-to-one association to ControlCalidadAvaluoRev
    @OneToMany(mappedBy = "avaluoControlCalidadRev", cascade = CascadeType.ALL)
    public List<ControlCalidadEspecificacion> getControlCalidadEspecificacions() {
        return this.controlCalidadEspecificacions;
    }

    public void setControlCalidadEspecificacions(
        List<ControlCalidadEspecificacion> controlCalidadEspecificacions) {
        this.controlCalidadEspecificacions = controlCalidadEspecificacions;
    }

}
