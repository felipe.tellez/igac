/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 * Filtro para los campos de la consulta de trámites vigentes
 *
 * @author pedro.garcia
 */
public class FiltroDatosConsultaTramites implements Serializable {

    private static final long serialVersionUID = -517299881164623185L;

    private String departamentoId;
    private String municipioId;
    private String tipoTramiteCodigo;
    private String claseMutacionCodigo;
    private String subtipoMutacionCodigo;
    private String clasificacionCodigo;
    private Long numeroSolicitud;
    private String numeroRadicacion;
    private Date fechaRadicacionInicial;
    private Date fechaRadicacionFinal;
    private String funcionarioRadicadorId;
    private String estadoCodigo;
    private String funcionarioEjecutorId;
    private String numeroPredial;
    private String consecutivoCatastral;
    private String nipNumero;
    private String clasificacion;

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

    public String getTipoTramiteCodigo() {
        return tipoTramiteCodigo;
    }

    public void setTipoTramiteCodigo(String tipoTramiteCodigo) {
        this.tipoTramiteCodigo = tipoTramiteCodigo;
    }

    public String getClaseMutacionCodigo() {
        return claseMutacionCodigo;
    }

    public void setClaseMutacionCodigo(String claseMutacionCodigo) {
        this.claseMutacionCodigo = claseMutacionCodigo;
    }

    public String getSubtipoMutacionCodigo() {
        return subtipoMutacionCodigo;
    }

    public void setSubtipoMutacionCodigo(String subtipoMutacionCodigo) {
        this.subtipoMutacionCodigo = subtipoMutacionCodigo;
    }

    public String getClasificacionCodigo() {
        return clasificacionCodigo;
    }

    public void setClasificacionCodigo(String clasificacionCodigo) {
        this.clasificacionCodigo = clasificacionCodigo;
    }

    public Long getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(Long numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public Date getFechaRadicacionInicial() {
        return fechaRadicacionInicial;
    }

    public void setFechaRadicacionInicial(Date fechaRadicacionInicial) {
        this.fechaRadicacionInicial = fechaRadicacionInicial;
    }

    public Date getFechaRadicacionFinal() {
        return fechaRadicacionFinal;
    }

    public void setFechaRadicacionFinal(Date fechaRadicacionFinal) {
        this.fechaRadicacionFinal = fechaRadicacionFinal;
    }

    public String getFuncionarioRadicadorId() {
        return funcionarioRadicadorId;
    }

    public void setFuncionarioRadicadorId(String funcionarioRadicadorId) {
        this.funcionarioRadicadorId = funcionarioRadicadorId;
    }

    public String getEstadoCodigo() {
        return estadoCodigo;
    }

    public void setEstadoCodigo(String estadoCodigo) {
        this.estadoCodigo = estadoCodigo;
    }

    public String getFuncionarioEjecutorId() {
        return funcionarioEjecutorId;
    }

    public void setFuncionarioEjecutorId(String funcionarioEjecutorId) {
        this.funcionarioEjecutorId = funcionarioEjecutorId;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getConsecutivoCatastral() {
        return consecutivoCatastral;
    }

    public void setConsecutivoCatastral(String consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    public String getNipNumero() {
        return nipNumero;
    }

    public void setNipNumero(String nipNumero) {
        this.nipNumero = nipNumero;
    }

}
