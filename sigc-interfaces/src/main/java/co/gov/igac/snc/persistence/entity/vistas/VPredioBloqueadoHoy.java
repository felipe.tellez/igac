package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the V_PREDIO_BLOQUEADO_HOY database table.
 *
 */
@Entity
@Table(name = "V_PREDIO_BLOQUEADO_HOY")
public class VPredioBloqueadoHoy implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4002125963582202247L;

    private Date fechaDesbloqueo;
    private Date fechaInicioBloqueo;
    private Date fechaLog;
    private Date fechaRecibido;
    private Date fechaTerminaBloqueo;
    private BigDecimal id;
    private String motivoBloqueo;
    private String motivoDesbloqueo;
    private BigDecimal predioId;
    private BigDecimal soporteBloqueoDocumentoId;
    private BigDecimal soporteDbloqueoDocumentoId;
    private String usuarioDesbloqueo;
    private String usuarioLog;

    public VPredioBloqueadoHoy() {
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESBLOQUEO")
    public Date getFechaDesbloqueo() {
        return this.fechaDesbloqueo;
    }

    public void setFechaDesbloqueo(Date fechaDesbloqueo) {
        this.fechaDesbloqueo = fechaDesbloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_BLOQUEO")
    public Date getFechaInicioBloqueo() {
        return this.fechaInicioBloqueo;
    }

    public void setFechaInicioBloqueo(Date fechaInicioBloqueo) {
        this.fechaInicioBloqueo = fechaInicioBloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RECIBIDO")
    public Date getFechaRecibido() {
        return this.fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_TERMINA_BLOQUEO")
    public Date getFechaTerminaBloqueo() {
        return this.fechaTerminaBloqueo;
    }

    public void setFechaTerminaBloqueo(Date fechaTerminaBloqueo) {
        this.fechaTerminaBloqueo = fechaTerminaBloqueo;
    }

    @Id
    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @Column(name = "MOTIVO_BLOQUEO")
    public String getMotivoBloqueo() {
        return this.motivoBloqueo;
    }

    public void setMotivoBloqueo(String motivoBloqueo) {
        this.motivoBloqueo = motivoBloqueo;
    }

    @Column(name = "MOTIVO_DESBLOQUEO")
    public String getMotivoDesbloqueo() {
        return this.motivoDesbloqueo;
    }

    public void setMotivoDesbloqueo(String motivoDesbloqueo) {
        this.motivoDesbloqueo = motivoDesbloqueo;
    }

    @Column(name = "PREDIO_ID")
    public BigDecimal getPredioId() {
        return this.predioId;
    }

    public void setPredioId(BigDecimal predioId) {
        this.predioId = predioId;
    }

    @Column(name = "SOPORTE_BLOQUEO_DOCUMENTO_ID")
    public BigDecimal getSoporteBloqueoDocumentoId() {
        return this.soporteBloqueoDocumentoId;
    }

    public void setSoporteBloqueoDocumentoId(
        BigDecimal soporteBloqueoDocumentoId) {
        this.soporteBloqueoDocumentoId = soporteBloqueoDocumentoId;
    }

    @Column(name = "SOPORTE_DBLOQUEO_DOCUMENTO_ID")
    public BigDecimal getSoporteDbloqueoDocumentoId() {
        return this.soporteDbloqueoDocumentoId;
    }

    public void setSoporteDbloqueoDocumentoId(
        BigDecimal soporteDbloqueoDocumentoId) {
        this.soporteDbloqueoDocumentoId = soporteDbloqueoDocumentoId;
    }

    @Column(name = "USUARIO_DESBLOQUEO")
    public String getUsuarioDesbloqueo() {
        return this.usuarioDesbloqueo;
    }

    public void setUsuarioDesbloqueo(String usuarioDesbloqueo) {
        this.usuarioDesbloqueo = usuarioDesbloqueo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
