package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*
 * MODIFICACIONES PROPIAS A LA CLASE PredioAvaluoCatastral: -> Se agregó el serialVersionUID.
 */
@Entity
@Table(name = "PREDIO_AVALUO_CATASTRAL", schema = "SNC_CONSERVACION")
public class PredioAvaluoCatastral implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5421883208228759448L;

    // Fields
    private Long id;
    private Predio predio;
    private String autoavaluo;
    private String justificacionAvaluo;
    private String notaAvaluo;
    private Double valorTotalAvaluoCatastral;
    private Double valorTerreno;

    private Double valorTerrenoPrivado;
    private Double valorTerrenoComun;
    private Double valorTotalConstruccionPrivada;
    private Double valorConstruccionComun;

    private Double valorTotalConstruccion;
    private Double valorTotalConsConvencional;
    private Double valorTotalConsNconvencional;
    private Double valorTotalAvaluoVigencia;
    private Date vigencia;
    private String usuarioLog;
    private Date fechaLog;

    private Date fechaInscripcionCatastral;

    // Constructors
    /** default constructor */
    public PredioAvaluoCatastral() {
    }

    /** minimal constructor */
    public PredioAvaluoCatastral(Long id, Predio predio, String autoavaluo,
        Double valorTotalAvaluoCatastral, Double valorTerreno,
        Double valorTotalConstruccion, Double valorTotalConsConvencional,
        Double valorTotalConsNconvencional, Date vigencia,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.autoavaluo = autoavaluo;
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
        this.valorTerreno = valorTerreno;
        this.valorTotalConstruccion = valorTotalConstruccion;
        this.valorTotalConsConvencional = valorTotalConsConvencional;
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
        this.vigencia = vigencia;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PredioAvaluoCatastral(Long id, Predio predio, String autoavaluo,
        String justificacionAvaluo, String notaAvaluo,
        Double valorTotalAvaluoCatastral, Double valorTerreno,
        Double valorTerrenoPrivado, Double valorTerrenoComun,
        Double valorTotalConstruccionPrivada, Double valorConstruccionComun,
        Double valorTotalConstruccion, Double valorTotalConsConvencional,
        Double valorTotalConsNconvencional, Double valorTotalAvaluoVigencia, Date vigencia,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.autoavaluo = autoavaluo;
        this.justificacionAvaluo = justificacionAvaluo;
        this.notaAvaluo = notaAvaluo;
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
        this.valorTerreno = valorTerreno;

        this.valorTerrenoPrivado = valorTerrenoPrivado;
        this.valorTerrenoComun = valorTerrenoComun;
        this.valorTotalConstruccionPrivada = valorTotalConstruccionPrivada;
        this.valorConstruccionComun = valorConstruccionComun;

        this.valorTotalConstruccion = valorTotalConstruccion;
        this.valorTotalConsConvencional = valorTotalConsConvencional;
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;

        this.valorTotalAvaluoVigencia = valorTotalAvaluoVigencia;

        this.vigencia = vigencia;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "AUTOAVALUO", nullable = false, length = 2)
    public String getAutoavaluo() {
        return this.autoavaluo;
    }

    public void setAutoavaluo(String autoavaluo) {
        this.autoavaluo = autoavaluo;
    }

    @Column(name = "JUSTIFICACION_AVALUO", length = 600)
    public String getJustificacionAvaluo() {
        return this.justificacionAvaluo;
    }

    public void setJustificacionAvaluo(String justificacionAvaluo) {
        this.justificacionAvaluo = justificacionAvaluo;
    }

    @Column(name = "NOTA_AVALUO", length = 600)
    public String getNotaAvaluo() {
        return this.notaAvaluo;
    }

    public void setNotaAvaluo(String notaAvaluo) {
        this.notaAvaluo = notaAvaluo;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL", nullable = false, precision = 18)
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @Column(name = "VALOR_TOTAL_TERRENO", nullable = false, precision = 18)
    public Double getValorTerreno() {
        return this.valorTerreno;
    }

    public void setValorTerreno(Double valorTerreno) {
        this.valorTerreno = valorTerreno;
    }

    @Column(name = "VALOR_TERRENO_PRIVADO", nullable = false, precision = 18)
    public Double getValorTerrenoPrivado() {
        return this.valorTerrenoPrivado;
    }

    public void setValorTerrenoPrivado(Double valorTerrenoPrivado) {
        this.valorTerrenoPrivado = valorTerrenoPrivado;
    }

    @Column(name = "VALOR_TERRENO_COMUN", nullable = false, precision = 18)
    public Double getValorTerrenoComun() {
        return this.valorTerrenoComun;
    }

    public void setValorTerrenoComun(Double valorTerrenoComun) {
        this.valorTerrenoComun = valorTerrenoComun;
    }

    @Column(name = "VALOR_TOTAL_CONS_PRIVADA", nullable = false, precision = 18)
    public Double getValorTotalConstruccionPrivada() {
        return this.valorTotalConstruccionPrivada;
    }

    public void setValorTotalConstruccionPrivada(Double valorTotalConstruccionPrivada) {
        this.valorTotalConstruccionPrivada = valorTotalConstruccionPrivada;
    }

    @Column(name = "VALOR_CONSTRUCCION_COMUN", nullable = false, precision = 18)
    public Double getValorConstruccionComun() {
        return this.valorConstruccionComun;
    }

    public void setValorConstruccionComun(Double valorConstruccionComun) {
        this.valorConstruccionComun = valorConstruccionComun;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCION", nullable = false, precision = 18)
    public Double getValorTotalConstruccion() {
        return this.valorTotalConstruccion;
    }

    public void setValorTotalConstruccion(Double valorTotalConstruccion) {
        this.valorTotalConstruccion = valorTotalConstruccion;
    }

    @Column(name = "VALOR_CONS_CONVENCIONAL", nullable = false, precision = 18)
    public Double getValorTotalConsConvencional() {
        return this.valorTotalConsConvencional;
    }

    public void setValorTotalConsConvencional(Double valorTotalConsConvencional) {
        this.valorTotalConsConvencional = valorTotalConsConvencional;
    }

    @Column(name = "VALOR_CONS_NO_CONVENCIONAL", nullable = false, precision = 18)
    public Double getValorTotalConsNconvencional() {
        return this.valorTotalConsNconvencional;
    }

    public void setValorTotalConsNconvencional(
        Double valorTotalConsNconvencional) {
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "VIGENCIA", nullable = false, length = 7)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_VIGENCIA", nullable = false, precision = 18)
    public Double getValorTotalAvaluoVigencia() {
        return valorTotalAvaluoVigencia;
    }

    public void setValorTotalAvaluoVigencia(Double valorTotalAvaluoVigencia) {
        this.valorTotalAvaluoVigencia = valorTotalAvaluoVigencia;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static PredioAvaluoCatastral parsePredioAvaluoCatastral(
        HPredioAvaluoCatastral hPredioAvaluoCatastral) {
        PredioAvaluoCatastral unPAC = new PredioAvaluoCatastral();

        unPAC.setId(hPredioAvaluoCatastral.getId().getId());
        unPAC.setPredio(hPredioAvaluoCatastral.getPredio());
        unPAC.setAutoavaluo(hPredioAvaluoCatastral.getAutoavaluo());
        unPAC.setJustificacionAvaluo(hPredioAvaluoCatastral.getJustificacionAvaluo());
        unPAC.setNotaAvaluo(hPredioAvaluoCatastral.getNotaAvaluo());
        unPAC.setValorTotalAvaluoCatastral(hPredioAvaluoCatastral.getValorTotalAvaluoCatastral());
        unPAC.setValorTerreno(hPredioAvaluoCatastral.getValorTerreno());
        unPAC.setValorTerrenoPrivado(hPredioAvaluoCatastral.getValorTerrenoPrivado());
        unPAC.setValorTerrenoComun(hPredioAvaluoCatastral.getValorTerrenoComun());
        unPAC.setValorTotalConstruccionPrivada(hPredioAvaluoCatastral.
            getValorTotalConstruccionPrivada());
        unPAC.setValorConstruccionComun(hPredioAvaluoCatastral.getValorConstruccionComun());
        unPAC.setValorTotalConstruccion(hPredioAvaluoCatastral.getValorTotalConstruccion());
        unPAC.setValorTotalConsConvencional(hPredioAvaluoCatastral.getValorTotalConsConvencional());
        unPAC.
            setValorTotalConsNconvencional(hPredioAvaluoCatastral.getValorTotalConsNconvencional());
        unPAC.setValorTotalAvaluoVigencia(hPredioAvaluoCatastral.getValorTotalAvaluoVigencia());
        unPAC.setVigencia(hPredioAvaluoCatastral.getVigencia());
        unPAC.setUsuarioLog(hPredioAvaluoCatastral.getUsuarioLog());
        unPAC.setFechaLog(hPredioAvaluoCatastral.getFechaLog());

        return unPAC;
    }
}
