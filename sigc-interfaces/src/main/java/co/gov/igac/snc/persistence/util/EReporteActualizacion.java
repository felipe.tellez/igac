package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para los reportes de actualización
 *
 * @author javier.aponte
 *
 */
public enum EReporteActualizacion {

    INCREMENTO_DE_AREA_CONSTRUIDA("8", "Incremento de área construida",
        "ACT_INCREMENTO_AREA_CONSTRUIDA"),
    INCREMENTO_DE_AVALUO_POR_SECTOR("3", "Incremento de avalúo por sector",
        "ACT_INCREMENTO_AVALUO_SECTOR"),
    ESTADISTICA_POR_DESTINO("5", "Estadística por destino", "ACT_ESTADISTICA_POR_DESTINO"),
    ESTADISTICA_POR_ZONA_FISICA("6", "Estadística por zona física",
        "ACT_ESTADISTICA_POR_ZONA_FISICA"),
    ESTADISTICA_POR_ZONA_GEOECONOMICA("7", "Estadística por zona geoeconómica",
        "ACT_ESTADISTICA_POR_ZONA_GEOECONOMICA"),
    LISTADO_AVALUO_POR_PREDIO("4", "Listado avalúo por predio", "ACT_AVALUO_POR_PREDIO"),
    LISTADO_DIFERENCIA_DE_AREA_PREDIOS("1", "Listado de diferencia de área de predios",
        "ACT_DIFERENCIA_AREA"),
    LISTADO_PREDIOS_NO_LIQUIDADOS("2", "Listado de predios no liquidados", "ACT_PREDIO_NO_LIQUIDADO"),
    LISTADO_PREDIOS_POR_USO_CONSTRUCCION("9", "Listado de predios por uso construcción",
        "ACT_PREDIO_POR_USO_CONSTRUCCION");

    private String codigo;
    private String nombre;
    //Variable para el manejo de los jobs
    private String tipoReporte;

    private EReporteActualizacion(String codigo, String valor, String tipoReporte) {
        this.codigo = codigo;
        this.nombre = valor;
        this.tipoReporte = tipoReporte;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getTipoReporte() {
        return tipoReporte;
    }

}
