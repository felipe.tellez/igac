package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion correspondiente al dominio de ZonaCierreVigencia.
 *
 * @author david.cifuentes
 *
 */
public enum EZonaCierreVigencia {

    CORREGIMIENTOS("03", "Corregimientos"),
    RURAL("00", "Rural"),
    URBANA("01", "Urbana");

    private String codigo;
    private String valor;

    private EZonaCierreVigencia(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return valor;
    }
}
