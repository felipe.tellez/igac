package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the MOTIVO_ESTADO_TRAMITE database table.
 *
 */
@Entity
@Table(name = "MOTIVO_ESTADO_TRAMITE")
public class MotivoEstadoTramite implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1522856847181227947L;

    private long id;

    private String estado;

    private Date fechaLog;

    private String motivo;

    private String usuarioLog;

    public MotivoEstadoTramite() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "ESTADO")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MOTIVO")
    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
