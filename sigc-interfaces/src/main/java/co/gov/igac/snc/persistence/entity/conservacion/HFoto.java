package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * HFoto entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_FOTO", schema = "SNC_CONSERVACION")
public class HFoto implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -9077910807250478339L;

    private HFotoId id;
    private HPredio HPredio;
    private Long predioId;
    private HUnidadConstruccion hUnidadConstruccion;
    private Long tramiteId;
    private String tipo;
    private String descripcion;
    private Date fecha;
    private Documento documento;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /**
     * default constructor
     */
    public HFoto() {
    }

    /**
     * minimal constructor
     */
    public HFoto(HFotoId id, HPredio HPredio, Long predioId, String tipo,
        Date fecha, Documento documento, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.tipo = tipo;
        this.fecha = fecha;
        this.documento = documento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /**
     * full constructor
     */
    public HFoto(HFotoId id, HPredio HPredio, Long predioId,
        HUnidadConstruccion hUnidadConstruccion, Long tramiteId, String tipo,
        String descripcion, Date fecha, Documento documento,
        String cancelaInscribe, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.hUnidadConstruccion = hUnidadConstruccion;
        this.tramiteId = tramiteId;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.documento = documento;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "HPredioId", column = @Column(name = "H_PREDIO_ID", nullable =
            false, precision = 10, scale = 0)),
        @AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false, precision =
            10, scale = 0))})
    public HFotoId getId() {
        return this.id;
    }

    public void setId(HFotoId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "UNIDAD_CONSTRUCCION_ID", precision = 10, scale = 0)
    public HUnidadConstruccion getHUnidadConstruccion() {
        return this.hUnidadConstruccion;
    }

    public void setHUnidadConstruccion(HUnidadConstruccion hUnidadConstruccion) {
        this.hUnidadConstruccion = hUnidadConstruccion;
    }

    @Column(name = "TRAMITE_ID", precision = 10, scale = 0)
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "DESCRIPCION", length = 2000)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA", nullable = false, length = 7)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "FOTO_DOCUMENTO_ID", nullable = false, precision = 10, scale = 0)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30, nullable = true)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
