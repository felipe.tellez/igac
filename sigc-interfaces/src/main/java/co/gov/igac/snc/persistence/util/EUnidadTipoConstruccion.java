package co.gov.igac.snc.persistence.util;

public enum EUnidadTipoConstruccion {

    CONVENCIONAL("CONVENCIONAL"),
    NO_CONVENCIONAL("NO CONVENCIONAL");

    private String codigo;

    private EUnidadTipoConstruccion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
