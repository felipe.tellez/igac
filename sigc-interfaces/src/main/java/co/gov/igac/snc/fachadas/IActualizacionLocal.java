package co.gov.igac.snc.fachadas;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.*;
import co.gov.igac.snc.persistence.entity.conservacion.MPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VJurisdiccionProducto;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.FiltroCargueDTO;

/**
 * Fachada para el módulo de Actualización Catastral.
 *
 * @version 2.0
 *
 */
@Local
public interface IActualizacionLocal {

    /**
     * Este método marca el comienzo del proceso de actualización de un municipio al ordenar el
     * registro del diagnóstico previo del municipio.
     *
     * @param departamentoCodigo, código del departamento al que pertenece el municipio que va a ser
     * objeto de actualización.
     * @param municipioCodigo, código del municipio que va a ser actualizado.
     * @param vigencia, corresponde a la vigencia de la actualización del municipio.
     * @param responsableDiagnosticoNombre, nombre completo del funcionario responsable de la
     * elaboración del diagnóstico.
     * @param responsableDiagnosticoId, login del funcionario responsable de la elaboración del
     * diagnóstico.
     * @param usuarioDTO, objeto de transporte con los datos del usuario que hace la operación.
     * @exception ExcepcionSNC en caso de que se presente una situación anormal durante la operación
     * del método.
     * @author jamir.avila
     */
    public Long ordenarRegistroDiagnosticoMunicipio(String departamentoCodigo, String zona,
        String municipioCodigo, java.util.Date vigencia,
        String responsableDiagnosticoNombre, String responsableDiagnosticoId, UsuarioDTO usuarioDTO)
        throws ExcepcionSNC;

    /**
     * Metodo que avanza el proceso a la actividad ACT_PLANIFICACION_REGISTRAR_ESTUDIO_COSTOS
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param usuario
     */
    public void ordenarGestionarEstudioDeCostos(String idActividad, Actualizacion actualizacion,
        UsuarioDTO usuario);

    /**
     * Metodo que avanza el proceso a la actividad ACT_PLANIFICACION_REVISAR_ESTUDIO_COSTOS_PROG
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param usuario
     */
    public void ordenarVerificarEstudioDeCostos(String idActividad, Actualizacion actualizacion,
        List<UsuarioDTO> usuario);

    /**
     * Metodo que avanza el proceso a la actividad ACT_PLANIFICACION_REGISTRAR_ESTUDIO_COSTOS
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param usuario
     */
    public void ordenarRevisionEstudioDeCostos(String idActividad, Actualizacion actualizacion,
        List<UsuarioDTO> usuario);

    /**
     * Metodo que avanza el proceso a la actividad ACT_PLANIFICACION_INGRESAR_CONVENIO
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param usuario
     */
    public void ordenarIngresarConvenios(String idActividad, Actualizacion actualizacion,
        List<UsuarioDTO> usuario);

    /**
     * Método que avanza una actividad a la transicion que se necesite
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param usuario
     * @param transicion
     */
    public boolean ordenarAvanzarASiguienteActividad(
        String idActividad, Actualizacion actualizacion, ERol rol, String transicion,
        String territorial);

    /**
     * Método que avanza una actividad a la transicion que se necesite
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param usuario
     * @param transicion
     */
    public boolean ordenarAvanzarASiguienteActividad(
        String idActividad, Actualizacion actualizacion, UsuarioDTO usuario, String transicion,
        String territorial);

    /**
     * Método que avanza al final de planificacion a las actividades paralelas
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param territorial
     * @param usuario
     */
    public boolean ordenarActualizacionAutorizada(
        String idActividad, Actualizacion actualizacion, String territorial, UsuarioDTO usuario);

    /**
     * Método que avanza la actividad al registro de inducciones y area de trabajo de la
     * planificacion
     *
     * @author franz.gamba
     * @param idActividad
     * @param actualizacion
     * @param territorial
     * @param usuario
     */
    public void ordenarInduccionAlPersonalYRegistroAreaDeTrabajo(
        String idActividad, Actualizacion actualizacion, String territorial, UsuarioDTO responsable);

    /**
     * Este método permite obtener la JurisdicciónProducto, incluyendo la territorial,
     * correspondiente a un municipio a partir de su código.
     *
     * @param codigoMunicipio código del municipio para el que se desea la JurisdiccionProducto.
     * @return el objeto VJurisdiccionProducto correspondiente al código de municipio proporcionado
     * o null en caso de que no haya correspondencia.
     * @exception ExcepcionSNC en caso de que se presente una situación anormal durante la operación
     * del método.
     * @author jamir.avila.
     */
    public VJurisdiccionProducto recuperarJurisdiccionProductoPorCodigoMunicipio(
        String codigoMunicipio) throws ExcepcionSNC;

    /**
     * Obtiene los responsables del proceso de actualización asignados a una territorial, dado el
     * código de la territorial.
     *
     * @param codigoTerritorial código de la territorial.
     * @return una lista de objetos UsuarioDTO que pertenecen a la territorial indicada, mediante su
     * código, y que actúan como responsables de actualización; en caso de que no se encuentre
     * ninguna coincidencia, retorna null.
     * @exception ExcepcionSNC en caso de que se presente una situación anormal durante la operación
     * del método.
     * @author jamir.avila.
     */
    public List<UsuarioDTO> recuperarResponsablesDeActualizacion(String codigoTerritorial) throws
        ExcepcionSNC;

    /**
     * Obtiene el Director de una territorial. Nota: si hay más de un Director activo para una
     * territorial, devuelve el primero de la consulta.
     *
     * @author jamir.avila
     * @param codigoTerritorial código de la territorial.
     * @return el objeto UsuarioDTO con los datos del Director Territorial.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal.
     */
    public UsuarioDTO recuperarDirectorTerritorial(String codigoTerritorial) throws ExcepcionSNC;

    /**
     * Crea las asociaciones entre la actualización de un municipio y las informaciones agrólogicas
     * y catastrales disponibles para realizar el proceso de actualización.
     *
     * @param actualizacion objeto de transporte con la información general de la actualizacion.
     * @param informacionesCartograficas lista con la información agrológica que se debe asociar.
     * @param informacionesAgrologicas lista con la información cartográfica que se debe asociar.
     * @param usuarioDTO, objeto de transporte con los datos del usuario que hace la operación.
     * @throws ExcepcionSNC se lanza en caso de que se presente una situación anormal durante la
     * operación del método.
     * @author jamir.avila.
     */
    public void registrarInformacionDiagnosticoMunicipio(Actualizacion actualizacion,
        List<InformacionBasicaCarto> informacionesCartograficas,
        List<InformacionBasicaAgro> informacionesAgrologicas, UsuarioDTO usuarioDTO) throws
        ExcepcionSNC;

    /**
     * Recupera un objeto actualización dado su identificador.
     *
     * @param idActualizacion identificador único de la actualización a recuperar.
     * @return el objeto Actualizacion correspondiente o null en caso de que no sea encontrado.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal durante la
     * operación del método.
     * @author jamir.avila.
     */
    public Actualizacion recuperarActualizacionPorId(Long idActualizacion) throws ExcepcionSNC;

    /**
     * Recupera el conjunto de convenios pertenecientes a una actualización dado su identificador.
     *
     * @param idActualizacion identificador de la actualización.
     * @return una lista de objetos Convenio que corresponden a la actualización indicada, mediante
     * su identificador, o null si no hay ninguno.
     * @throws ExcepcionSNC lanzada en caso de que se presente una situación anormal durante la
     * operación del método.
     * @author jamir.avila.
     */
    public List<Convenio> recuperarConveniosPorIdActualizacion(Long idActualizacion) throws
        ExcepcionSNC;

    /**
     * Método que elimina una {@link ConvenioEntidad} de un {@link Convenio}
     *
     * @author david.cifuentes
     * @param ConvenioEntidad
     */
    public void eliminarEntidad(ConvenioEntidad entidad);

    /**
     * Método que actualiza un {@link Convenio}
     *
     * @author david.cifuentes
     * @param Convenio
     */
    public Convenio actualizarConvenio(Convenio convenio);

    /**
     * Método que busca una {@link Actualizacion} por su id, trayendo sus convenios y entidades.
     *
     * @author david.cifuentes
     * @param idActualizacion
     */
    public Actualizacion buscarActualizacionPorIdConConveniosYEntidades(Long idActualizacion) throws
        ExcepcionSNC;

    /**
     * Método que guarda un {@link ConvenioEntidad}
     *
     * @author david.cifuentes
     * @param ConvenioEntidad
     */
    public ConvenioEntidad guardarConvenioEntidad(ConvenioEntidad convenioEntidad);

    /**
     * Método que guarda una lista de {@link ConvenioEntidad}
     *
     * @author david.cifuentes
     * @param List<ConvenioEntidad>
     */
    public List<ConvenioEntidad> guardarListaEntidadConvenio(List<ConvenioEntidad> entidades);

    /**
     * Crea un registro de ActualizacionDocuemnto en la base de datos
     *
     * @author franz.gamba
     * @param actualizacionDocumento
     */
    public void crearActualizacionDocumento(ActualizacionDocumento actualizacionDocumento);

    /**
     * Actualiza el registro correspondiente en la base de datos
     *
     * @author franz.gamba
     * @param actualizacionDocumento
     */
    public void updateActualizacionDocumento(ActualizacionDocumento actualizacionDocumento);

    /**
     * Metodo que retorna los contratistas aprobados para una actualización
     *
     * @author franz.gamba
     * @param actualizacionId
     * @return
     */
    public List<ActualizacionContrato> getContratosPorActualizacion(Long actualizacionId);

    /**
     * @author franz.gamba
     * @param actualizacion
     * @return
     */
    public Actualizacion guardarYActualizarActualizacion(Actualizacion actualizacion);

    /**
     * @author franz.gamba
     * @param actualizacionDocumento
     * @return
     */
    public ActualizacionDocumento guardarYActualizarActualizacionDocumento(
        ActualizacionDocumento actualizacionDocumento);

    /**
     * @author franz.gamba
     * @param ActualizacionContrato
     * @return
     */
    public ActualizacionContrato guardarYActualizarActualizacionContrato(
        ActualizacionContrato actualizacionContrato);

    /**
     * @author franz.gamba
     * @param ActualizacionContrato
     * @return
     */
    public List<ActualizacionContrato> guardarYActualizarListActualizacionContrato(
        List<ActualizacionContrato> actualizacionContrato);

    /**
     * @author franz.gamba
     * @param actualizacionDocumentacion
     * @return
     */
    public ActualizacionDocumentacion guardarYActualizarActualizacionDocumentacion(
        ActualizacionDocumentacion actualizacionDocumentacion);

    /**
     * Metodo que actualiza en la base de datos el objeto ActualizacionEvento
     *
     * @author franz.gamba
     * @param evento
     * @return
     */
    public ActualizacionEvento guardarYactualizarActualizacionEvento(ActualizacionEvento evento);

    /**
     * Metodo que elimina de la base de datos el objeto ActualizacionEvento
     *
     * @author franz.gamba
     * @param evento
     * @return
     */
    public void eliminarActualizacionEvento(ActualizacionEvento evento);

    /**
     * Metodo que actualiza en la base de datos el objeto EventoAsistente
     *
     * @author franz.gamba
     * @param evento
     * @return
     */
    public List<EventoAsistente> guardarYactualizarEventoAsistentes(List<EventoAsistente> asistentes);

    /**
     * @author franz.gamba
     * @param actualizacionResponsable
     * @return
     */
    public ActualizacionResponsable guardarYactualizarActualizacionResponsable(
        ActualizacionResponsable actualizacionResponsable);

    /**
     * Metodo que actualiza en la base de datos una lista de recursosHumanos
     *
     * @author franz.gamba
     * @param recursosHumanos
     * @return
     */
    public List<RecursoHumano> guardarYactualizarRecursoHumanoList(
        List<RecursoHumano> recursosHumanos);

    /**
     * Método que guarda un convenio.
     *
     * @author david.cifuentes
     * @param Convenio
     */
    public Convenio guardarConvenio(Convenio convenio);

    /**
     * Método qeu retorna el recurso humano asignado a un responsable de actualizacion
     *
     * @author franz.gamba
     * @param responsableActId
     * @return
     */
    public List<RecursoHumano> getRecursoHumanoByResponsableActId(Long responsableActId);

    /**
     * Retorna el objeto actualizaicon con sus respectivos responsables
     *
     * @author franz.gamba
     * @param actualizacionId
     * @return
     */
    public Actualizacion getActualizacionConResponsables(Long actualizacionId);

    /**
     * Método que actualiza en la base de datos una lista de saldos de conservacion
     *
     * @author franz.gamba
     * @param saldos
     * @return
     */
    public List<SaldoConservacion> guardarYActualizarSaldosDeConservacion(
        List<SaldoConservacion> saldos);

    /**
     * Método que trae los saldos de conservacion asignados a un proceso de actualizacion
     *
     * @author franz.gamba
     * @param actualizacionId
     * @return
     */
    public List<SaldoConservacion> obtenerSaldosConservacionPorActualizacionId(Long actualizacionId);

    /**
     * Metodo que actualiza en la base de datos el objeto actualizacionComision
     *
     * @author franz.gamba
     * @param actualizacionComision
     * @return
     */
    public ActualizacionComision actualizarActualizacionComision(
        ActualizacionComision actualizacionComision);

    /**
     * Metodo que actualiza en la base de datos una lista de objetos actualizacionComision
     *
     * @author franz.gamba
     * @param actualizacionComisiones
     * @return
     */
    public List<ActualizacionComision> actualizarActualizacionComisiones(
        List<ActualizacionComision> actualizacionComisiones);

    /**
     * Metodo que actualiza en la base de datos una lista de objetos ComisionIntegrante
     *
     * @author franz.gamba
     * @param comisionIntegrantes
     * @return
     */
    public List<ComisionIntegrante> actualizarComisionIntegrantes(
        List<ComisionIntegrante> comisionIntegrantes);

    /**
     * retorna un contratista por su id
     *
     * @author franz.gamba
     * @param idContrato
     * @return
     */
    public ActualizacionContrato getActualizacionContratoById(Long idContrato);

    /**
     * Método que retorna la lista de funcionarios y contratistas por el id de actualizacion
     *
     * @param actualizacionId
     * @return
     * @author javier.aponte
     */
    public List<RecursoHumano> getRecursoHumanoPorActualizacionId(Long actualizacionId);

    /**
     * Método que actualiza ó guarda en la base de datos el objeto EventoAsistente
     *
     * @param evento
     * @return
     * @author javier.aponte
     */
    public EventoAsistente guardarYactualizarEventoAsistente(EventoAsistente eventoAsistente);

    /**
     * Método que devuelve la ultima actualización por el municipio y la zona
     *
     * @return
     * @author javier.aponte
     */
    public Actualizacion obtenerUltimaActualizacionPorMunicipioYZona(String codigoMunicipio,
        String zona);

    /**
     * Mètodo que guarda y actualiza un evento asistente
     *
     * @author javier.barajas
     * @param asistenteNuevo
     * @return EventoAsistente
     * @version 1.0
     */
    public EventoAsistente guardarActualizarEventoAsistente(EventoAsistente asistenteNuevo);

    /**
     * Método para cargar un acta en una comisión de campo.
     *
     * @author javier.barajas
     * @param actualizacionId identificador de la actualización.
     * @param actualizacionEvento objeto de transporte con los datos del objeto ActualizacionEvento.
     * @param documento es el objeto de tranporte con el documento y su tipo de documento.
     * @return ActualizacionEvento
     * @version 1.0
     */
    public ActualizacionEvento cargarActaEnComisionDeCampo(Long actualizacionId,
        ActualizacionEvento actualizacionEvento, Documento documento);

    /**
     * Método para obtener predios de la actualizacion por zonas(rural,urbana)
     *
     * @author javier.barajas
     * @param actualizacionId identificador de la actualización.
     * @param otros parametros para paginacion
     * @return List<Object[]>
     * @version 1.0
     */
    public List<Object[]> obtenerPrediosdeActualizacionPorZonas(Long ActualizacionId,
        String sortField, String sortOrder,
        boolean iscount,
        final int... rowStartIdxAndCount);

    /**
     * Método para guardar la generacion de formularios sbc
     *
     * @author javier.barajas
     * @param actualizacionId identificador de la actualización.
     * @param documento de la generacion del formulario sbc
     * @param GeneracionFormularioSbc cual es elemento nuevo a registrar
     * @version 1.0
     */
    public GeneracionFormularioSbc guardarGeneracionFormulariosSbc(Long actualizacionId,
        Documento documento, GeneracionFormularioSbc generarFormularios);

    /**
     * Método para guardar los predios a los que se generaron los formularios sb
     *
     * @author javier.barajas
     * @param Lista de PredioFormularioSbc a los cuales se generar el formulario de sbc
     * @return List<PredioFormularioSbc>
     * @version 1.0
     */
    public List<PredioFormularioSbc> guardarYactualizarPredioFormularioSbc(
        List<PredioFormularioSbc> prediosFormularios);

    /**
     * Método para obtener las mazanas de una zona de actualizacion
     *
     * @author javier.barajas
     * @param Long ActualizacionId
     * @param String barrio
     * @return Lista de ManzanasVeredas
     * @version 1.0
     */
    public List<ManzanaVereda> obtenerManzanasPorZonaActualizacion(String barrio);

    /**
     * Método para obtener las mazanas y/o zonas para hacer levantamiento topografico
     *
     * @author javier.barajas
     * @param Long ActualizacionId
     * @return Lista de ActualizacionLevantamiento
     * @cu 209 Exportar Manzanas
     * @version 1.0
     */
    public List<ActualizacionLevantamiento> obtenerManzanasExportar(Long idActulizacion);

    /**
     * Método que obtiene la una Actualizacioncomision medianet el id de la actualizacion y el
     * objeto
     *
     * @author franz.gamba
     * @param actualizacionId
     * @param objeto
     * @return
     */
    public ActualizacionComision obtenerActComisionPoractualizacionIdYObjeto(Long actualizacionId,
        String objeto);

    /**
     * Método que obtiene los integrantes de una comision segun su actualizacion y su objeto
     *
     * @author franz.gamba
     * @param actualizacionId
     * @param objeto
     * @return
     */
    public List<ComisionIntegrante> obtenerComisionIntegrantesByActualizacionIdYObjeto(
        Long actualizacionId, String objeto);

    /**
     * Método que actualiza el objeto comision integrante en la nbase de datos
     *
     * @author franz.gamba
     * @param integrante
     * @return
     * @version 1.0
     */
    public ComisionIntegrante actualizarComisionIntegrante(ComisionIntegrante integrante);

    /**
     * Método que busca los predios segun en el numero de manzana
     *
     * @author javier.barajas
     * @param numero manzana
     * @return List<Predio>
     * @version 1.0
     */
    public List<Predio> buscarPrediosTramitePorNumeroManzana(String[] manzanasBuscar);

    /**
     * Método que busca para obtener las ActualizacionLevantamiento
     *
     * @author javier.barajas
     * @param ActualizacionId, LevantamientoId
     * @return Lista de ActualizacionLevantamiento
     * @version 1.0
     */
    public List<ActualizacionLevantamiento> buscarActualizacionLevantamientoPorActualizacionId(
        Long actualizacionId, Long levantamientoId);

    /**
     * Método que busca obtener los topografos de los levantamientos asignados
     *
     * @author javier.barajas
     * @param Lista de ActualizacionLevantamiento
     * @return Lista de LevantamientoAsignacion
     * @version 1.0
     */
    public List<LevantamientoAsignacion> buscarTopografosLevantamientoActualizacion(
        List<ActualizacionLevantamiento> listaLevantamiento);

    /**
     * Método que busca exportar la manzanas a formatos (shp,dxf,dwg)
     *
     * @author javier.barajas
     * @param Codigos Manazanas, Formato a exportar, token, usuario
     * @return Ruta del archivo
     * @version 1.0
     */
    public String exportarManzanasaTopografos(String codManzanas, String formato, UsuarioDTO usuario);

    /**
     * Método que devuelve una lista de recurso humano por actividad
     *
     * @param actividad
     * @author javier.aponte
     */
    public List<RecursoHumano> obtenerRecursoHumanoPorActividad(String actividad);

    /**
     * Método encargado de asignar topógrafos a manzanas y áreas
     *
     * @param actualizacionLevantamientos
     * @param topografo
     * @param usuario
     * @return levantamientoAsignacionActualizado con la información del topógrafo enviado como
     * parámetro
     * @author javier.aponte
     */
    public LevantamientoAsignacion asignarTopografoAManzanasYAreas(
        ActualizacionLevantamiento[] actualizacionLevantamientos,
        RecursoHumano topografo, UsuarioDTO usuario);

    /**
     * Método que guarda y actualiza una actualizacionNomenclaturaVia
     *
     * @author david.cifuentes
     * @param actualizacionNomenclaturaVia
     * @return
     */
    public ActualizacionNomenclaturaVia guardarYActualizarActualizacionNomenclaturaVia(
        ActualizacionNomenclaturaVia actualizacionNomenclaturaVia);

    /**
     * Obtiene las manzanas de un municipio
     *
     * @param departamentoMunicipio codigo del departamento concatenado con el codigo municipio
     * @return
     * @author andres.eslava
     */
    public List<ManzanaVereda> obtenerManzanasMunicipio(String departamentoMunicipio);

    /**
     * Guarda el registro del proyecto de actualizacion de perimetro urbano de un municipio
     *
     * @param actualizacionPerimetroUrbano Infomación sobre la actualización de perimetro urbano.
     * @author andres.eslava
     */
    public ActualizacionPerimetroUrbano guardarActualizacionPerimetroUrbano(
        ActualizacionPerimetroUrbano actualizacionPerimetroUrbano, UsuarioDTO usuario);

    /**
     * Guarda la asociacion entre la actualizacion de perimetro urbano y la normas vigentes
     *
     * @param perimetroUrbanoNorma Norma asociada a la actualizacion de perimetro urbano.
     * @author andres.eslava
     */
    public PerimetroUrbanoNorma guardarPerimetroUrbanoNorma(
        PerimetroUrbanoNorma perimetroUrbanoNorma, UsuarioDTO usuario);

    /**
     * Obtener actualizacon con municipio asociado.
     *
     * @param actualizacionId identificador de la actualizacion.
     *
     * @author andres.eslava
     */
    public Actualizacion obtenerActualizacionConMunicipio(Long actualizacionId);

    /**
     * Obtener un rango de manzanas
     *
     * @param rangoInicial cadena con el numero predial inicial hasta la manzana
     * @param rangoFinal cadena con el numero predial final hasta la manzana
     * @return
     * @author andres.eslava
     */
    public List<ManzanaVereda> obtenerManzanasPorRango(String rangoInicial, String rangoFinal);

    /**
     * Persiste la orden de ajuste sobre manzanas que lo requieren.
     *
     * @param actualizacionOrdenAjuste orden de ajuste
     * @param manzanasAjuste listado de manzanas sobre las que se requiere el ajuste
     * @return
     * @author andres.eslava
     *
     */
    public ActualizacionOrdenAjuste guardarActualizacionOrdenAjuste(
        ActualizacionOrdenAjuste actualizacionOrdenAjuste);

    /**
     * Persiste los predios asignados a los reconocedores en una actualizacion.
     *
     * @param reconocimientoPredio predios asignados para reconocimiento.
     * @return
     * @author andres.eslava
     */
    public ReconocimientoPredio guardarReconocimientoPredio(
        ReconocimientoPredio reconocimientoPredio);

    /**
     * Método para consultar los topografos segun el id de actualizacion
     *
     * @author javier.barajas
     * @param actualizacionId
     * @return List<RecursoHumano>
     * @version 1.0
     */
    public List<RecursoHumano> buscarTopografosActualizacionId(Long actualizacionId);

    /**
     * Método para consultar manzanas por topografos
     *
     * @author javier.barajas
     * @param actualizacionId, recursoHumanoId
     * @return List<ActualizacionLevantamiento>
     * @version 1.0
     */
    public List<ActualizacionLevantamiento> buscarManzanasporTopografosActualizacionId(
        Long actualizacionId, Long recursoHumanoId);

    /**
     * Método para insertar criterios de calidad
     *
     * @author javier.barajas
     * @param List<ControlCalidadCriterio>
     * @return List<ControlCalidadCriterio>
     * @version 1.0
     */
    public List<ControlCalidadCriterio> guardarYactualizarControlCalidad(
        List<ControlCalidadCriterio> listaCotrolCalidad);

    /**
     * Método para insertar la muestra del control de calidad
     *
     * @author javier.barajas
     * @param ControlCalidadMuestra
     * @return ControlCalidadMuestra
     * @version 1.0
     */
    public ControlCalidadMuestra guardarYactualizarControlCalidadMuestra(
        ControlCalidadMuestra muestra);

    /**
     * Método para insertar una asignacion de control de calidad
     *
     * @author javier.barajas
     * @param AsignacionControlCalidad
     * @return AsignacionControlCalidad
     * @version 1.0
     */
    public AsignacionControlCalidad guardarYactualizarAsignacionControlCalidad(
        AsignacionControlCalidad asignacionControlCalidad);

    /**
     * Método para buscar Criterios de Control de calidad por id
     *
     * @author javier.barajas
     * @param CriterioControlCalidad
     * @return criterioControlCalidadId
     * @version 1.0
     */
    public CriterioControlCalidad buscarCriterioControlCalidad(Long criterioControlCalidadId);

    /**
     * Método para buscar Levantamiento Asignacion por Id
     *
     * @author javier.barajas
     * @param levantamientoAsignacionId
     * @return LevantamientoAsignacion
     * @version 1.0
     */
    public LevantamientoAsignacion buscarLevantamientoAsignacionId(Long levantamientoAsignacionId);

    /**
     * Método para buscar Recurso Humano por Id
     *
     * @author javier.barajas
     * @param recursoHumanoId
     * @return RecursoHumano
     * @version 1.0
     */
    public RecursoHumano buscarRecursoHumanoporId(Long recursoHumanoId);

    /**
     * Método para buscar Actualizacion Reconocimiento por Id Actualizacion
     *
     * @author javier.barajas
     * @param actualizacionId
     * @return Lista ActualizacionReconocimiento
     * @cu 220 Gestionar Asginacion a Coordinadores
     * @version 1.0
     */
    public List<ActualizacionReconocimiento> buscarActualizacionReconocimientoporId(
        Long actualizacionId);

    /**
     * Método para insertar Actualizacion Reconocimiento
     *
     * @author javier.barajas
     * @param ActualizacionReconocimiento
     * @return ActualizacionReconocimiento
     * @cu 220 Gestionar Asginacion a Coordinadores
     * @version 1.0
     */
    public ActualizacionReconocimiento agregarActualizacionReconocimiento(
        ActualizacionReconocimiento actReconocimiento);

    /**
     * Método para buscar Recurso Humano por Id
     *
     * @author javier.barajas
     * @param recursoHumanoId
     * @param actividad
     * @return RecursoHumano
     * @version 1.0
     */
    public List<RecursoHumano> buscarrecursoHumanoporActividadYActualizacionId(Long actualizacionId,
        String actividad);

    /**
     * Método para buscar Manzanas de un municipio para asignar coordinador por codigo municipio
     *
     * @author javier.barajas
     * @param codigo municipio
     * @param parametros de paginacion
     * @return Lista<String>
     * @version 1.0
     */
    public List<ManzanaVereda> getManazanasporMunicipioAsignarCoordinadorPaginacion(
        String municipio, final int... rowStartIdxAndCount);

    /**
     * Método para buscar Manzanas de un municipio para asignar coordinador por codigo municipio de
     * tabla ActualizacionReconocimiento
     *
     * @author javier.barajas
     * @param actualizacionId
     * @param parametros de paginacion
     * @return Lista<ActualizacionReconocimiento>
     * @version 1.0
     */
    public List<ActualizacionReconocimiento> getManazanasporMunicipioAsignarCoordinadorPaginacionActualizacionReconocimiento(
        Long actualizacionId, final int... rowStartIdxAndCount);

    /**
     * Método que guarda en alfresco un documento de actualización y lo actualiza en la base de
     * datos
     *
     * @param documento
     * @param actualizacion
     * @param usuario
     * @return documento
     * @version 1.0
     * @author javier.aponte
     */
    public Documento guardarDocumentoDeActualizacionEnAlfresco(Documento documento,
        Actualizacion actualizacion,
        UsuarioDTO usuario);

    /**
     * Método que trae la informe del levantamiento topografico con LevantamientoAsignacionId
     *
     * @param levantamientoAsignacionId
     * @return LevantamientoAsignacionInf
     * @version 1.0
     * @cu 212
     * @author javier.barajas
     */
    public LevantamientoAsignacionInf buscaLevantamientoTopograficoInforme(
        Long levantamientoAsignacionId);

    /**
     * Método que trae levantamiento topografico con el recursoHumanoId
     *
     * @param recursoHumanoId
     * @return LevantamientoAsignacion
     * @version 1.0
     * @cu 212
     * @author javier.barajas
     */
    public LevantamientoAsignacion buscaLevantamientoAsignacionconRecursoHumanoId(
        Long recursoHumanoId);

    /**
     * Método que trae los recurso humano de actualizacion reconocimiento
     *
     * @param recursoHumanoId
     * @return LevantamientoAsignacion
     * @version 1.0
     * @cu 212
     * @author javier.barajas
     */
    public List<ActualizacionReconocimiento> buscaActualizacionReconocimientoPorRecursoHumanoId(
        Long recursoHumanoId);

    /**
     * Busca las ordenes de ajuste asignadas a un responsable sig
     *
     * @param responsableSig responsable al que se le asignaron las ordenes de ajuste.
     * @return
     * @author andres.eslava
     */
    public List<ActualizacionOrdenAjuste> obtenerOrdenesAjustePorResponsableSig(
        RecursoHumano responsableSig);

    /**
     * Obtiene las manzas asignadas a un cordinador
     *
     * @param cordinador cordinador del grupo asignadoa a la actualización.
     * @return
     * @author andres.eslava
     */
    public List<ActualizacionReconocimiento> obtenerManzanasAsignadasPorReconocedor(
        RecursoHumano cordinador);

    /**
     * Obtener los predios de una manzana
     *
     * @param codigoManzana codigo de la manzana para obtener los predios que le pertenecen
     * @return
     * @author andres.eslava
     */
    public List<Predio> obtenerPrediosPorManzana(String codigoManzana);

    /**
     * Obtener el recurso humano por el Loggin del usuario
     *
     * @param identificacionRecursoHumano documento de identidad del recurso humano.
     * @param ActualizacionId identificador del proceso de actualizacion.
     * @return
     * @author andres.eslava
     */
    public RecursoHumano obtenerRecursoHumanoPorIdentificacionYActualizacionId(
        String identificacionRecursoHumano, Long ActualizacionId);

    /**
     * Consulta todos los predios asocados a una tarea de reconocimiento por el codigo de manzana
     *
     * @return
     * @author andres.eslava
     */
    public List<ReconocimientoPredio> obtenerReconocimientosPredioPorManzana(String codigoManzana);

    /**
     * Consulta todos los saldos de conservacion de un numero predial;
     *
     * @param NumeroPredial
     * @return
     * @author andres.eslava
     */
    public List<SaldoConservacion> getSaldosConservacionByNumeroPredial(String numeroPredial);

    /**
     * Consulta para obtener los predios
     *
     * @param codigoManzana codigo de la manzana, el numero predial con las 17 primeras cifras
     * @return
     * @author andres.eslava
     *
     */
    public List<Predio> getPrediosLibreActualizacionReconocimientoYSaldoConservacion(
        String codigoManzana);

    /**
     * obtiene una Actualizacion reconocimiento por el id de la actualizacionReconocimiento
     *
     * @param actualizacionReconocimiento
     * @return
     * @author andres.eslava
     */
    public ActualizacionReconocimiento getActualizacionReconocimientoById(
        Long idAactualizacionReconocimiento);

    /**
     * Persiste la informacion de la ActualizacionLevantamiento.
     *
     * @param unaActualizacionLevantamiento informacion suficiente persitir la
     * actualizacionLevantamiento
     * @return
     */
    public ActualizacionLevantamiento guardaActualizacionLevantamiento(
        ActualizacionLevantamiento unaActualizacionLevantamiento);

    /**
     * Método que consulta los {@link MunicipioCierreVigencia} por el código del departamento.
     *
     * @author david.cifuentes
     *
     * @param decretoAvaluoAnual {@link DecretoAvaluoanual} decreto actual.
     *
     * @param codigosMunicipios Lista de {@link String} con los codigos de municipios de un
     * departamento seleccionado previamente.
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     *
     * @return Lista de objetos {@link MunicipioCierreVigencia}
     *
     */
    public List<MunicipioCierreVigencia> consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
        DecretoAvaluoAnual decretoAvaluoAnual, List<String> codigosMunicipios)
        throws ExcepcionSNC;

    /**
     * Método que consulta la existencia de {@link MunicipioCierreVigencia} para el
     * {@link DecretoAvaluoAnual} enviado como parámetro. En caso de que no existan registros de
     * municipios para dicho decreto, se insertará un registro por cada municipio existente,
     * asociado el decreto enviado.
     *
     * @author david.cifuentes
     *
     * @param decretoAvaluoAnual {@link DecretoAvaluoanual} decreto actual.
     * @param usuario {@link UsuarioDTO} usuario en sesión.
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     *
     */
    public void validarExistenciaMunicipiosPorDecreto(
        DecretoAvaluoAnual decretoAvaluoAnual, UsuarioDTO usuario) throws ExcepcionSNC;

    /**
     * Método que actualiza una lista de {@link MunicipioCierreVigencia}. En caso de que realizar la
     * actualización adecuadamente retorna true, de lo contrario retorna false.
     *
     * @author david.cifuentes
     *
     * @param Lista a actualizar de objetos {@link MunicipioCierreVigencia}
     *
     * @eturn true si actualizó correctamente, false si hubo alguna inconsistencia
     */
    public boolean guardarListaMunicipioCierreVigencias(
        List<MunicipioCierreVigencia> listaMunicipioCierreVigencia);

    /**
     * Método que elimina una lista de {@link ZonaCierreVigencia}. En caso de que realizar la
     * eliminación adecuadamente retorna true, de lo contrario retorna false.
     *
     * @author david.cifuentes
     *
     * @param Lista de {@link ZonaCierreVigencia} a eliminar
     *
     * @eturn true si eliminaron correctamente, false si hubo alguna inconsistencia
     */
    public boolean eliminarZonasDelMunicipioCierreVigencia(
        List<ZonaCierreVigencia> listaZonas);

    /**
     * Método que realiza el llamado al procedimiento enviado como parámetro para las diferentes
     * acciones a realizar en el cierre anual.
     *
     * @author david.cifuentes
     *
     * @param nombreProcedimientoFuncion {@link EProcedimientoAlmacenadoFuncion} con el nombre del
     * procedimiento a ejecutar.
     *
     * @param codDeptoAValidar {@link String} con el código del {@link Departamento} a ejecutar el
     * procedimiento.
     *
     * @param municipios Arreglo de {@link MunicipioCierreVigencia} con los municipios para los
     * cuales va a aplicar la ejcución del procedimiento.
     * @param vigencia {@link Long} con el año de la vigencia del decreto actual
     * @param usuario {@link String} con el login del usuario que realiza el cierre
     *
     * @return Array de {@link Object} que tiene la información del cursor con los resultados de la
     * ejecución del procedimiento.
     */
    public Object[] ejecutarProcedimientoCierreAnual(
        EProcedimientoAlmacenadoFuncion nombreProcedimiento,
        String codDepartamento, MunicipioCierreVigencia[] municipios,
        Long vigencia, String usuario);

    /**
     * Obtiene los procesos de actualizacion relacionados a un municipio que se encuentran en un
     * estado en paticular, si el paramtro estado es null retorna todos los proceso del municipio
     *
     * @author felipe.cadena
     * @param municpioCodigo
     * @param estado
     * @param activo
     * @param vigencia
     * @return
     */
    public List<MunicipioActualizacion> obtenerMunicipioActualizacionPorMunicipioYEstado(
        String municpioCodigo, String estado, Boolean activo, Integer vigencia);
    
    /**
    * @author hector.arias
    * @param municpioCodigo
    * @param estado
    * @param activo
    * @param vigencia
    * @param ubucacionGeografico
    * @return 
    */
    public List <MunicipioActualizacion> obtenerPorMunicipioYEstadoGeografico(String municpioCodigo,
        String estado, Boolean activo, Integer vigencia);

    /**
     * Método encargado de generar las resoluciones en JasperServer de Actualizacion
     *
     * @author leidy.gonzalez
     *
     * @param job
     * @return boolean que indica si se generó exitosamente la resolucion
     */
    /**
     * Método encargado de crear un nuevo registro en la tabla producto catastral job, estos jobs
     * están en espera de que sean ejecutados en jasperserver para las resoluciones de conservacion
     *
     * @author leidy.gonzalez
     * @param job productoCatastralJob
     * @param reporte
     * @param tramiteId
     */
    public ProductoCatastralJob crearJobGenerarResolucionesActualizacion(Tramite tramite,
        Map<String, String> parametros, Actividad actividad, UsuarioDTO usuarioLogeado);

    /**
     * Método encargado de generar las resoluciones en JasperServer de actualizacion
     *
     * @author leidy.gonzalez
     *
     * @param job
     * @return boolean que indica si se generó exitosamente la resolucion
     */
    public boolean procesarResolucionesActualizacionEnJasperServer(ProductoCatastralJob job,
        int contador);

    /**
     * Obtiene los procesos de actualizacion relacionados a un municipio y un departamento que se
     * encuentran en un estado en paticular, si el paramtro estado es null retorna todos los proceso
     * del municipio
     *
     *
     * @author leidy.gonzalez
     * @param municpioCodigo
     * @param departamentoCodigo
     * @return
     */
    public List<MunicipioActualizacion> obtenerEstadoTramitePorDepartamentoMunicipio(
        String municpioCodigo, String departamentoCodigo);

    /**
     * Método encargado de avanzar las resoluciones en Process
     *
     * @author leidy.gonzalez
     * @param tramite
     * @param usuario
     * @param actividad
     * @return
     */
    public void avanzarTramiteDeGenerarResolucionProcess(Tramite tramite, UsuarioDTO usuario,
        Actividad actividad);

    /**
     * Método para consultar el resumen de tramites radicados durante la actualizacion asociados a
     * una solicitud en particular
     *
     * @author felipe.cadena
     *
     * @param solicitudId
     * @return
     */
    public List<PredioActualizacion> obtenerPredioActualizacionPorSolicitudId(Long solicitudId);

    /**
     * Guarda o actualiza un objeto MunicipioActualizacion
     *
     * @author felipe.cadena
     * @param municipioActualizacion
     * @return
     */
    public MunicipioActualizacion guardarActualizarMunicipioActualizacion(
        MunicipioActualizacion municipioActualizacion);

    /**
     * Obtiene los MunicipioActualizacion asociados a los tramites del parametro cada municipio
     * actualizacion recuperodo tiene la lista de tramites asociados
     *
     * @author felipe.cadena
     * @param isTramtites
     * @return
     */
    public List<MunicipioActualizacion> obtenerMunicipioActualizacionPorIdsTramites(
        List<Long> isTramtites);

    /**
     * Obtiene los MPredio asociados a los tramites del parametro
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public MPredio obtenerMPredioPorIdTramite(Long idTramite);

    /**
     * Obtiene los archivos de tablas de actualizacion que se han cargado relacionadas a los
     * parametros dados
     *
     * @author felipe.cadena
     *
     * @param municipioCodigo
     * @param deptoCodigo
     * @return
     */
    public List<ValoresActualizacion> obtenerValoresActualizacionPorMunicipioDepartamento(
        String municipioCodigo, String deptoCodigo, String vigencia);

    /**
     * Obtiene los municipios que tienen procesos de actualizacion en curso.
     *
     * @author felipe.cadena
     *
     * @param deptoCodigo
     * @return
     */
    public List<Municipio> obtenerMunicipiosEnActualizacionPorDepto(String deptoCodigo);

    /**
     * Elimina una valor de actualizacion junto con todas las tablas de avaluos asociadas
     *
     * @author felipe.cadena
     *
     * @param valorActualizacionId
     * @param municipioCodigo
     * @return
     */
    public Object[] eliminarValoresActualizacion(Long valorActualizacionId, String municipioCodigo);

    /**
     * Ejecuta el procedimiento para cargar tablas de actualizacion
     *
     * @author felipe.cadena
     *
     * @param municipioCodigo
     * @param valorActualizacionId
     * @param usuario
     * @return
     */
    public Object[] cargarTablasLiquidacion(String municipioCodigo, Long valorActualizacionId,
        String usuario);
    
    /**
     * Obtiene la ruta del archivo de inconsistencias de validaciones geográficas
     * del proceso de actualización
     * @param resultadoJob
     * @return 
     * 
     * @author hector.arias
     */
    public void guardarRegistroArchivoInconsistenciasActualizacionGeo(List <String> resultadoJob);

    /**
     * Ejecuta el procedimiento para calcular los avaluos del municipio basado en las tablas
     * temporales de liquidacion
     *
     * @author felipe.cadena
     * @param municipioCodigo
     * @return
     */
    public Object[] calcularAvaluosTablas(String municipioCodigo);

    /**
     * Ejecuta el procedimiento para poner en firme las tablas de actualizacion
     *
     * @author felipe.cadena
     * @param municipioCodigo
     * @return
     */
    public Object[] aplicarTablasLiquidacion(String municipioCodigo);

    /**
     * Guarda un registro del tipo ValoesActualizacion
     *
     * @author felipe.cadena
     *
     * @param va
     * @return
     */
    public ValoresActualizacion guardarActualizarValoresActualizacion(ValoresActualizacion va);

    /**
     * Consulta los registros de la tabla AX_REPORTE_ACTUALIZACION por el código del municipio
     *
     * @author javier.aponte
     *
     * @param codigoMunicipio Código del municipio
     * @return Lista con registros de la tabla AX_REPORTE_ACTUALIZACION
     */
    public List<Documento> buscarReportesActualizacionPorCodigoMunicipio(
        String CodigoMunicipio, String vigencia);

    /**
     * Método encargado de guardar o actualizar una lista de reportes actualización
     *
     * @author javier.aponte
     *
     * @param reportesActualizacion lista de los reportes de actualización que se desean guardar o
     * actualizar
     * @return Lista con los registros actualizados
     */
    public List<ReporteActualizacion> guardarYActualizarReportesActualizacion(
        List<ReporteActualizacion> reportesActualizacion);

    /**
     * Método encargado de borrar y actualizar los diferentes reportes al cargar las tablas de
     * actualización. si el parametro de generarReportes se encuentra en true, genera nuevamente
     * todos los reportes.
     *
     * @author javier.aponte
     *
     * @param codigoMunicipio codigo del municipio
     * @param usuarioLogin
     * @param generarReportes indica si se debe generar de nuevo los reportes de actualización
     */
    public void borrarYActualizarReportesActualizacion(String codigoMunicipio, String usuarioLogin,
        boolean generarReportes);

    /**
     * Método encargado de generar los reportes de actualización express
     *
     * @author javier.aponte
     *
     * @param job
     * @return boolean que indica si se generó exitosamente el reporte
     */
    public boolean procesarReportesActualizacionEnJasperServer(ProductoCatastralJob job,
        int contador);

    /**
     * Metodo para guardar los archivos asociados al proceso de actualizacion express
     *
     * @author felipe.cadena
     * @param municipioActualizacion
     * @param usuario
     * @param rutaZip
     * @return
     */
    public MunicipioActualizacion guardarArchivosActualizacion(
        MunicipioActualizacion municipioActualizacion, UsuarioDTO usuario, String rutaZip);

    /**
     * Metodo para buscar los predios proyectados en un cargue cica
     *
     * @author felipe.cadena
     * @param filtroCargueDTO
     * @return
     */
    public List<VCargueCica> buscarPPrediosCargueCica(FiltroCargueDTO filtroCargueDTO);

    /**
     * Metodo para contar los predios proyectados en un cargue cica
     *
     * @author felipe.cadena
     * @param filtroCargueDTO
     * @return
     */
     public int countPPrediosCargueCica(FiltroCargueDTO filtroCargueDTO);

    /**
     * Elimina un registro de municipio atualizacion
     *
     * @author felipe.cadena
     * @param municipioActualizacion
     * @return
     */
     public void eliminarMunicipioActualizacion(MunicipioActualizacion municipioActualizacion);

     /**
     * Método para iniciar el cargue de informaciín del Cica
     *
     * @author felipe.cadena
     * @param municipioActualizacionId
     * @param usuario
     * @return
     */
     public Object[] iniciarCargueCica(Long municipioActualizacionId, String usuario);

     /**
     * Método para eliminar proyeccion del Cica
     *
     * @author felipe.cadena
     * @param idsPredios
     * @return
     */
      public Object[] eliminarProyeccionCica(List<Long> idsPredios);

      /**
     * Método para preliquidar la proyeccion del Cica
     *
     * @author felipe.cadena
     * @param municipioCodigo
     * @param vigencia
     * @return
     */
      public Object[] preliquidarCargueAct(String municipioCodigo, Integer vigencia);

    /**
     * Método para aplicar la proyeccion del Cica
     *
     * @author felipe.cadena
     * @param municipioCodigo
     * @param vigencia
     * @return
     */
    public Object[] aplicarCambiosAct(String municipioCodigo, Integer vigencia);

     /**
     * Método buscar predios proyectados en cargue Cica
     *
     * @author felipe.cadena
     * @param predios
     * @return
     */
     public List<VCargueCica> buscarPrediosProyectadosCica(List<String> predios);

     /**
      * Calcula resumen de los predios agrupando por zona-sector-manzana
     *
     * @author felipe.cadena
     * @param filtroCargueDTO
     * @return
     */
     public  List<Object[]>  buscarResumenPredios(FiltroCargueDTO filtroCargueDTO);

     /**
      * Calcula los rangos para los filtros avanzados
     *
     * @author felipe.cadena
     * @param filtroCargueDTO
     * @return
     */
     public  Object[]  buscarRangosFiltros(FiltroCargueDTO filtroCargueDTO);

     /**
      *  Retorna los predios asociados a los enviados en la proyeccion de cargue
     *
     * @author felipe.cadena
     * @param predios
     * @return
     */
     public  List<VCargueCica>  buscarPrediosAsociadosCargue(List<VCargueCica> predios);

}
