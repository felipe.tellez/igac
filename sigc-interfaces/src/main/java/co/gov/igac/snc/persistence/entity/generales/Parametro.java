package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Parametro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "PARAMETRO", schema = "SNC_GENERALES")
public class Parametro implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = -2555985854182298361L;

    @GeneratedValue
    private Long id;
    private String nombre;
    private String tipoDato;
    private Double valorNumero;
    private Date valorFecha;
    private String valorCaracter;
    private String modificable;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public Parametro() {
    }

    /** minimal constructor */
    public Parametro(Long id, String nombre, String tipoDato,
        Double valorNumero, String modificable, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.tipoDato = tipoDato;
        this.valorNumero = valorNumero;
        this.modificable = modificable;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Parametro(Long id, String nombre, String tipoDato,
        Double valorNumero, Date valorFecha, String valorCaracter,
        String modificable, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.tipoDato = tipoDato;
        this.valorNumero = valorNumero;
        this.valorFecha = valorFecha;
        this.valorCaracter = valorCaracter;
        this.modificable = modificable;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TIPO_DATO", nullable = false, length = 30)
    public String getTipoDato() {
        return this.tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    @Column(name = "VALOR_NUMERO", nullable = false, precision = 14)
    public Double getValorNumero() {
        return this.valorNumero;
    }

    public void setValorNumero(Double valorNumero) {
        this.valorNumero = valorNumero;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "VALOR_FECHA", length = 7)
    public Date getValorFecha() {
        return this.valorFecha;
    }

    public void setValorFecha(Date valorFecha) {
        this.valorFecha = valorFecha;
    }

    @Column(name = "VALOR_CARACTER", length = 600)
    public String getValorCaracter() {
        return this.valorCaracter;
    }

    public void setValorCaracter(String valorCaracter) {
        this.valorCaracter = valorCaracter;
    }

    @Column(name = "MODIFICABLE", nullable = false, length = 30)
    public String getModificable() {
        return this.modificable;
    }

    public void setModificable(String modificable) {
        this.modificable = modificable;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
