package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the VARIABLE_EVALUACION_AVALUO database table.
 *
 */
@Entity
@Table(name = "VARIABLE_EVALUACION_AVALUO")
public class VariableEvaluacionAvaluo implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private Date fechaLog;
    private String nombre;
    private String tipo;
    private String usuarioLog;
    private Double valorMaximo;
    private Double valorMinimo;
    private List<AvaluoEvaluacionDetalle> avaluoEvaluacionDetalles;

    public VariableEvaluacionAvaluo() {
    }

    @Id
    @SequenceGenerator(name = "VARIABLE_EVALUACION_AVALUO_ID_GENERATOR", sequenceName =
        "VARIABLE_EVALUACION_AVA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "VARIABLE_EVALUACION_AVALUO_ID_GENERATOR")
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_MAXIMO")
    public Double getValorMaximo() {
        return this.valorMaximo;
    }

    public void setValorMaximo(Double valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    @Column(name = "VALOR_MINIMO")
    public Double getValorMinimo() {
        return this.valorMinimo;
    }

    public void setValorMinimo(Double valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    //bi-directional many-to-one association to AvaluoEvaluacionDetalle
    @OneToMany(mappedBy = "variableEvaluacionAvaluo")
    public List<AvaluoEvaluacionDetalle> getAvaluoEvaluacionDetalles() {
        return this.avaluoEvaluacionDetalles;
    }

    public void setAvaluoEvaluacionDetalles(List<AvaluoEvaluacionDetalle> avaluoEvaluacionDetalles) {
        this.avaluoEvaluacionDetalles = avaluoEvaluacionDetalles;
    }

}
