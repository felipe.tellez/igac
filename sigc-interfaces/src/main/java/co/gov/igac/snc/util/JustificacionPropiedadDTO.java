package co.gov.igac.snc.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;

/**
 * @author fredy.wilches Clase utilizada para simular la normalización de la tabla
 * PPersonaPredioPropiedad
 */
public class JustificacionPropiedadDTO implements java.io.Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = 2148824549630748648L;

    private List<PPersonaPredioPropiedad> justificaciones = new ArrayList<PPersonaPredioPropiedad>();
    private String tipo;
    private String entidadEmisora;
    private Departamento departamento;
    private Municipio municipio;
    private String falsaTradicion;
    private Date fechaRegistro;
    private String numeroRegistro;
    private String modoAdquisicion;
    private Double valor;
    private String tipoTitulo;
    private String numeroTitulo;
    private Date fechaTitulo;
    private String libro;
    private String tomo;
    private String pagina;
    private Documento documentoSoporte;
    private String justificacion;
    private String usuarioLog;
    private Date fechaLog;
    private String cancelaInscribe;

    // Constructors
    /** default constructor */
    public JustificacionPropiedadDTO() {
    }

    /** minimo */
    public JustificacionPropiedadDTO(
        String tipoTitulo, String entidadEmisora, Departamento departamento,
        Municipio municipio,
        String numeroTitulo, Date fechaTitulo, Documento documentoSoporte,
        String usuarioLog, Date fechaLog) {

        this.entidadEmisora = entidadEmisora;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoTitulo = tipoTitulo;
        this.numeroTitulo = numeroTitulo;
        this.fechaTitulo = fechaTitulo;
        this.documentoSoporte = documentoSoporte;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    public JustificacionPropiedadDTO(PPersonaPredioPropiedad ppp) {

        this.tipoTitulo = ppp.getTipoTitulo();
        this.entidadEmisora = ppp.getEntidadEmisora();
        this.departamento = ppp.getDepartamento();
        this.municipio = ppp.getMunicipio();
        this.numeroTitulo = ppp.getNumeroTitulo();
        this.fechaTitulo = ppp.getFechaTitulo();
        this.documentoSoporte = ppp.getDocumentoSoporte();

        this.fechaRegistro = ppp.getFechaRegistro();
        this.modoAdquisicion = ppp.getModoAdquisicion();
        this.valor = ppp.getValor();

        this.cancelaInscribe = ppp.getCancelaInscribe();

    }

    /** full constructor */
    public JustificacionPropiedadDTO(
        String tipo, String entidadEmisora, Departamento departamento,
        Municipio municipio, String falsaTradicion,
        Date fechaRegistro, String numeroRegistro,
        String modoAdquisicion, Double valor, String tipoTitulo,
        String numeroTitulo, Date fechaTitulo, String libro,
        String tomo, String pagina, Documento documentoSoporte,
        String justificacion, String usuarioLog, Date fechaLog) {

        this.tipo = tipo;
        this.entidadEmisora = entidadEmisora;
        this.departamento = departamento;
        this.municipio = municipio;
        this.falsaTradicion = falsaTradicion;
        this.fechaRegistro = fechaRegistro;
        this.numeroRegistro = numeroRegistro;
        this.modoAdquisicion = modoAdquisicion;
        this.valor = valor;
        this.tipoTitulo = tipoTitulo;
        this.numeroTitulo = numeroTitulo;
        this.fechaTitulo = fechaTitulo;
        this.libro = libro;
        this.tomo = tomo;
        this.pagina = pagina;
        this.documentoSoporte = documentoSoporte;
        this.justificacion = justificacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    public String getCancelaInscribe() {
        return cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEntidadEmisora() {
        return this.entidadEmisora;
    }

    public void setEntidadEmisora(String entidadEmisora) {
        this.entidadEmisora = entidadEmisora;
    }

    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getFalsaTradicion() {
        return this.falsaTradicion;
    }

    public void setFalsaTradicion(String falsaTradicion) {
        this.falsaTradicion = falsaTradicion;
    }

    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    public String getModoAdquisicion() {
        return this.modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getTipoTitulo() {
        return this.tipoTitulo;
    }

    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    public String getNumeroTitulo() {
        return this.numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    public Date getFechaTitulo() {
        return this.fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    public Documento getDocumentoSoporte() {
        return this.documentoSoporte;
    }

    public void setDocumentoSoporte(Documento documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    public String getJustificacion() {
        return this.justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    /**
     * Compara los campos de las justificaciones para deerminar si son equivalentes.
     *
     * @param o
     * @return
     * @modified felipe.cadena :: 22/07/2014 :: Se consideran casos adicionales para la comparacion
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof JustificacionPropiedadDTO) {
            JustificacionPropiedadDTO dto = (JustificacionPropiedadDTO) o;

            //v1.1.11
            // Modificado leidy.gonzalez para el campo tipoTitulo se agrega validacion 
            //Se agregaron validaciones para que al comparar los campos, estos no sean nulos
            //cuando existe mas de un propietario con el mismo predio
            if (!compareFields(this.fechaTitulo, dto.fechaTitulo)) {
                return false;
            }
            if (!compareFields(this.numeroTitulo, dto.numeroTitulo)) {
                return false;
            }
            if (!compareFields(this.departamento, dto.departamento)) {
                return false;
            }
            if (!compareFields(this.municipio, dto.municipio)) {
                return false;
            }
            if (!compareFields(this.entidadEmisora, dto.entidadEmisora)) {
                return false;
            }
            if (!compareFields(this.tipoTitulo, dto.tipoTitulo)) {
                return false;
            }
            return true;

        } else {
            return false;
        }
    }

    /**
     * Determina si dos campos son iguales, si los campos son objetos deben tener implementado el
     * metodo equals.
     *
     * @param a
     * @param b
     * @return
     */
    private boolean compareFields(Object a, Object b) {
        if (a == null) {
            if (b != null) {
                return false;
            }
        } else {
            if (!a.equals(b)) {
                return false;
            }
        }
        return true;
    }

    public List<PPersonaPredioPropiedad> getJustificaciones() {
        return this.justificaciones;
    }

    public void setJustificaciones(List<PPersonaPredioPropiedad> justificaciones) {
        this.justificaciones = justificaciones;
    }

    public void adicionarJustificacion(PPersonaPredioPropiedad justificacion) {
        if (!this.justificaciones.contains(justificacion)) {
            this.justificaciones.add(justificacion);
        }
    }

    public String getPropietarios() {
        String p = "";
        if (this.justificaciones != null) {
            for (PPersonaPredioPropiedad ppp : this.justificaciones) {
                if (ppp.getPPersonaPredio() != null &&
                    ppp.getPPersonaPredio().getPPersona() != null &&
                    ppp.getPPersonaPredio().getPPersona().getNumeroIdentificacion() != null &&
                    ppp.getPPersonaPredio().getPPersona().getNombre() != null) {
                    p += ppp.getPPersonaPredio().getPPersona().getNumeroIdentificacion() + " " +
                        ppp.getPPersonaPredio().getPPersona().getNombre() + ", ";
                }
            }
        }
        return p;
    }

    public PPersonaPredioPropiedad crearPPersonaPredioPropiedad(PPersonaPredio pp,
        UsuarioDTO usuario) {
        PPersonaPredioPropiedad ppp = new PPersonaPredioPropiedad();
        ppp.setPPersonaPredio(pp);
        ppp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
        ppp.setDepartamento(this.departamento);
        ppp.setDocumentoSoporte(this.documentoSoporte);
        ppp.setEntidadEmisora(this.entidadEmisora);
        ppp.setFalsaTradicion(this.falsaTradicion);
        ppp.setFechaLog(new Date());
        ppp.setFechaRegistro(this.fechaRegistro);
        ppp.setFechaTitulo(this.fechaTitulo);
        ppp.setJustificacion(this.justificacion);
        ppp.setLibro(this.libro);
        ppp.setModoAdquisicion(this.modoAdquisicion);
        ppp.setMunicipio(this.municipio);
        ppp.setNumeroRegistro(this.numeroRegistro);
        ppp.setNumeroTitulo(this.numeroTitulo);
        ppp.setPagina(this.pagina);
        if (this.tipo == null) {
            ppp.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
        } else {
            ppp.setTipo(this.tipo);
        }

        ppp.setTipoTitulo(this.tipoTitulo);
        ppp.setTomo(this.tomo);
        ppp.setValor(this.valor);
        ppp.setUsuarioLog(usuario.getLogin());
        return ppp;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public PPersonaPredioPropiedad crearPPersonaPredioPropiedadCancelacionMasiva(PPersonaPredio pp,
        UsuarioDTO usuario) {
        PPersonaPredioPropiedad ppp = new PPersonaPredioPropiedad();
        ppp.setId(null);
        ppp.setPPersonaPredio(pp);
        ppp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
        ppp.setDepartamento(this.departamento);
        ppp.setDocumentoSoporte(this.documentoSoporte);
        ppp.setEntidadEmisora(this.entidadEmisora);
        ppp.setFalsaTradicion(this.falsaTradicion);
        ppp.setFechaLog(new Date());
        ppp.setFechaRegistro(this.fechaRegistro);
        ppp.setFechaTitulo(this.fechaTitulo);
        ppp.setJustificacion(this.justificacion);
        ppp.setLibro(this.libro);
        ppp.setModoAdquisicion(this.modoAdquisicion);
        ppp.setMunicipio(this.municipio);
        ppp.setNumeroRegistro(this.numeroRegistro);
        ppp.setNumeroTitulo(this.numeroTitulo);
        ppp.setPagina(this.pagina);
        if (this.tipo == null) {
            ppp.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
        } else {
            ppp.setTipo(this.tipo);
        }

        ppp.setTipoTitulo(this.tipoTitulo);
        ppp.setTomo(this.tomo);
        ppp.setValor(this.valor);
        ppp.setUsuarioLog(usuario.getLogin());
        return ppp;
    }

}
