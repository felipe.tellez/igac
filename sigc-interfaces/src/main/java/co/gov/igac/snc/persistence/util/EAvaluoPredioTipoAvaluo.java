package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de predio asociado a un avalúo comercial corresponde al dominio
 * AVALUO_PREDIO_TIPO_AVALUO
 *
 * @author christian.rodriguez
 */
public enum EAvaluoPredioTipoAvaluo {

    RURAL("00", "Rural"),
    URBANO("01", "Urbano");

    private String codigo;
    private String valor;

    private EAvaluoPredioTipoAvaluo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
