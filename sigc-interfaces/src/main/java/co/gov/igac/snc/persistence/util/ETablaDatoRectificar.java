/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeación para las tablas relacionadas a los datos del predio para rectificación o
 * complementación.
 *
 * @author felipe.cadena
 */
public enum ETablaDatoRectificar {

    PREDIO_ZONA("PREDIO_ZONA"),
    PREDIO_DIRECCION("PREDIO_DIRECCION"),
    UNIDAD_CONSTRUCCION_COMP("UNIDAD_CONSTRUCCION_COMP"),
    UNIDAD_CONSTRUCCION("UNIDAD_CONSTRUCCION"),
    PREDIO("PREDIO"),
    PERSONA_PREDIO_PROPIEDAD("PERSONA_PREDIO_PROPIEDAD"),
    PERSONA_PREDIO("PERSONA_PREDIO"),
    PERSONA("PERSONA"),
    FICHA_MATRIZ_PREDIO("FICHA_MATRIZ_PREDIO");

    private String nombre;

    private ETablaDatoRectificar(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
