package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PredioInconsistencia entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "PREDIO_INCONSISTENCIA", schema = "SNC_CONSERVACION")
public class PredioInconsistencia implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 8178172800748338600L;
    private Long id;
    private Predio predio;
    private String tipo;
    private String estado;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public PredioInconsistencia() {
    }

    /** minimal constructor */
    public PredioInconsistencia(Long id, Predio predio, String tipo,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.tipo = tipo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PredioInconsistencia(Long id, Predio predio, String tipo,
        String estado, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.tipo = tipo;
        this.estado = estado;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "predio_inconsistencia_ID_SEQ")
    @SequenceGenerator(name = "predio_inconsistencia_ID_SEQ", sequenceName =
        "PREDIO_INCONSISTENCIA_ID_SEQ",
        allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "TIPO", nullable = false, length = 4)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "ESTADO", length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
