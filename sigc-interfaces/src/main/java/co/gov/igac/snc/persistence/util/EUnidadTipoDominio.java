package co.gov.igac.snc.persistence.util;

public enum EUnidadTipoDominio {

    COMUN("COMUN"),
    PRIVADO("PRIVADO");

    private String codigo;

    private EUnidadTipoDominio(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
