package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/*
 * The persistent class for the TRAMITE_DIGITALIZACION database table.
 *
 */
@Entity
@Table(name = "TRAMITE_DIGITALIZACION", schema = "SNC_TRAMITE")
public class TramiteDigitalizacion implements Serializable {

    private static final long serialVersionUID = -7431329778124268893L;
    private Long id;
    private Date fechaDigitalizacion;
    private Date fechaEnvia;
    private Date fechaLog;
    private Tramite tramite;
    private String usuarioDigitaliza;
    private String usuarioEnvia;
    private String usuarioLog;

    public TramiteDigitalizacion() {
    }

    @Id
    @SequenceGenerator(name = "TRAMITE_DIGITALIZACION_ID_GENERATOR", sequenceName =
        "TRAMITE_DIGITALIZACION_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "TRAMITE_DIGITALIZACION_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DIGITALIZACION")
    public Date getFechaDigitalizacion() {
        return this.fechaDigitalizacion;
    }

    public void setFechaDigitalizacion(Date fechaDigitalizacion) {
        this.fechaDigitalizacion = fechaDigitalizacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ENVIA")
    public Date getFechaEnvia() {
        return this.fechaEnvia;
    }

    public void setFechaEnvia(Date fechaEnvia) {
        this.fechaEnvia = fechaEnvia;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "USUARIO_DIGITALIZA")
    public String getUsuarioDigitaliza() {
        return this.usuarioDigitaliza;
    }

    public void setUsuarioDigitaliza(String usuarioDigitaliza) {
        this.usuarioDigitaliza = usuarioDigitaliza;
    }

    @Column(name = "USUARIO_ENVIA")
    public String getUsuarioEnvia() {
        return this.usuarioEnvia;
    }

    public void setUsuarioEnvia(String usuarioEnvia) {
        this.usuarioEnvia = usuarioEnvia;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
