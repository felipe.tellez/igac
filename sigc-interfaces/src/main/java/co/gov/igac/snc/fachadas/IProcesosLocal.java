package co.gov.igac.snc.fachadas;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.apache.commons.collections15.MultiMap;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ArbolProcesos;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaContingencia;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaFrecuencia;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.utilerias.Duration;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.util.ERol;

/**
 * Bean que permite el acceso al servicio de procesos
 *
 * @author juan.mendez
 *
 */
@Local
public interface IProcesosLocal {

    public ActualizacionCatastral avanzarActividad(String idActividad,
        ActualizacionCatastral actualizacionCatastral);

    /**
     * Método que permite realizar el avance en el flujo de procesos de determinada actividad de
     * conservación.
     *
     * OJO: no usar este cuando hay varios usuarios posibles de una actividad. Usar {@link #avanzarActividad(co.gov.igac.generales.dto.UsuarioDTO, java.lang.String, co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral)
     * }
     *
     * @param message
     * @modified fabio.navarrete
     */
    public SolicitudCatastral avanzarActividad(String idActividad,
        SolicitudCatastral solicitudCatastral);

    /**
     * Avanza a la siguente actividad de la instancia de proceso de conservacion, reclamando la
     * actividad actual para el usuario especificado en caso que esta no haya sido reclamada.
     *
     * @param usuarioActActual Corresponde al usuario ejecutor de la actividad actual del proceso.
     * En caso que la actividad esté lista, se reclamará la actividad para este usuario y se
     * avanzará la actividad. El usuario, usuarios potenciales, o rol ejecutor de la siguiente
     * actividad será el especificado en el objeto de negocio.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     * @param objNegocio Objeto de negocio para la siguiente actividad.
     * @return El objeto de negocio asociada a la actividad a la que se avanzó.
     *
     * @throws ExcepcionSNC En caso que se presente un error en el avance de la actividad.
     */
    public SolicitudCatastral avanzarActividad(UsuarioDTO usuarioActActual, String idActividad,
        SolicitudCatastral objNegocio);

    /**
     * Permite avanzar a la siguiente actividad del proceso de Productos Catastrales. Ese método
     * sólo debe ser usado cuando la actividad a completarse se encuentra en estado reclamado; en
     * caso que el estado sea diferente, entonces se debe usar el método
     * {@link #avanzarActividad(UsuarioDTO, String, SolicitudProductoCatastral)}
     *
     * @param idActividad Identificador de la actividad a completar.
     *
     * @param solicitudProductoCatastral Objeto de negocio con los datos necesarios para completar
     * la actividad actual y avanzar a la siguiente.
     *
     * @return Objeto de negocio del proceso de Productos Catastrales actualizado.
     */
    public SolicitudProductoCatastral avanzarActividad(String idActividad,
        SolicitudProductoCatastral solicitudProductoCatastral);

    /**
     *
     * @param usuarioActActual Nombre de usuario o login del usuario que completará la actividad
     * actual.
     * @param idActividad Identificador de la actividad que se debe completar.
     *
     * @param solicitudProductoCatastral Objeto de negocio con los datos necesarios para el avance a
     * la siguiente actividad.
     *
     * @return Objeto de negocio del proceso de Productos Catastrales actualizado.
     */
    public SolicitudProductoCatastral avanzarActividad(UsuarioDTO usuarioActActual,
        String idActividad,
        SolicitudProductoCatastral solicitudProductoCatastral);

    /**
     * TODO Renombrar método a avanzarActividadConservacion
     *
     * @param idActividad
     * @param transicion
     * @param usuarios
     * @param observaciones
     * @return
     */
    public SolicitudCatastral avanzarActividad(String idActividad,
        String transicion, List<UsuarioDTO> usuarios, String observaciones);

    /**
     * Permite avanzar una actividad de manera sincrónica, recibiendo respuesta del servidor cuando
     * la actividad actual de la instancia del proceso haya terminado y la nueva actividad pueda
     * consultarse.
     *
     * @param idProceso Identificador único de la instancia del proceso
     * @param idActividad Identificador único de la actividad en la que está actualmente la
     * instancia del proceso.
     * @param duracion Duración de la actividad, en caso de ser la de defecto, se debe especificar
     * en null.
     * @param solicitudCatastral Contenedor con los datos de la solicitud catastral.
     *
     * @return Objeto de negocio con los últimos cambios aplicados en el gestor de procesos.
     *
     * @author alejandro.sanchez
     */
    @Deprecated
    public SolicitudCatastral avanzarActividadConservacion(String idProceso,
        String idActividad, Duration duracion,
        SolicitudCatastral solicitudCatastral);

    @Deprecated
    public ActualizacionCatastral avanzarActividadActualizacion(
        String idActividad, String transicion, List<UsuarioDTO> usuarios,
        String observaciones);

    /**
     * Avanza a la siguente actividad de la instancia de proceso de control de calidad de ofertas.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     * @param controlCalidad Objeto de negocio para la siguiente actividad.
     * @return El objeto de negocio asociada a la actividad a la que se avanzó.
     *
     * @throws ExcepcionSNC En caso que se presente un error en el avance de la actividad.
     *
     * @author alejandro.sanchez
     */
    public ControlCalidad avanzarActividad(String idActividad, ControlCalidad controlCalidad);

    /**
     * Avanza una instancia de proceso de ofertas inmobiliarias.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     *
     * @param ofertaInmobiliaria Objeto de negocio asociado al proceso.
     *
     * @return El objeto de negocio con las modificaciones realizadas durante el avance de la
     * actividad.
     *
     * @author alejandro.sanchez
     */
    public OfertaInmobiliaria avanzarActividad(String idActividad,
        OfertaInmobiliaria ofertaInmobiliaria);

    /**
     * Avanza a la siguente actividad de la instancia de proceso de ofertas, reclamando la actividad
     * actual para el usuario especificado en caso que esta no haya sido reclamada.
     *
     * @param usuarioEjecutorActActual Corresponde al usuario ejecutor de la actividad actual del
     * proceso. En caso que la actividad esté lista, se reclamará la actividad para este usuario y
     * se avanzará la actividad. El usuario, usuarios potenciales, o rol ejecutor de la siguiente
     * actividad será el especificado en el objeto de negocio.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     * @param objNegocio Objeto de negocio para la siguiente actividad.
     * @return El objeto de negocio asociada a la actividad a la que se avanzó.
     *
     * @throws ExcepcionSNC En caso que se presente un error en el avance de la actividad.
     *
     * @author alejandro.sanchez
     */
    public OfertaInmobiliaria avanzarActividad(UsuarioDTO usuarioActActual, String idActividad,
        OfertaInmobiliaria ofertaInmobiliaria);

    /**
     * Avanza a la siguente actividad de la instancia de proceso de control de calidad de ofertas,
     * reclamando la actividad actual para el usuario especificado en caso que esta no haya sido
     * reclamada.
     *
     * @param usuarioEjecutorActActual Corresponde al usuario ejecutor de la actividad actual del
     * proceso. En caso que la actividad esté lista, se reclamará la actividad para este usuario y
     * se avanzará la actividad. El usuario, usuarios potenciales, o rol ejecutor de la siguiente
     * actividad será el especificado en el objeto de negocio.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     * @param controlCalidad Objeto de negocio para la siguiente actividad.
     * @return El objeto de negocio asociada a la actividad a la que se avanzó.
     *
     * @throws ExcepcionSNC En caso que se presente un error en el avance de la actividad.
     *
     * @author alejandro.sanchez
     */
    public ControlCalidad avanzarActividad(UsuarioDTO usuarioEjecutorActActual, String idActividad,
        ControlCalidad controlCalidad);

    /**
     * Mueve una instancia de proceso a determinada actividad a partir del id de proceso que aparece
     * como dato en un trámite. Se usa este método cuando no se tiene disponible el identificador de
     * la instancia de la Actividad.
     *
     * @param idInstanciaProceso id de instancia de proceso contenido en el trámite que se quiere
     * mover de actividad.
     * @param solicitudCatastral objeto usado en el método Iprocesos#avanzarActividad( String
     * idActividad,SolicitudCatastral solicitudCatastral), que es el que se va a usar para mover la
     * actividad.
     */
    public SolicitudCatastral avanzarActividadPorIdProceso(String idProceso,
        SolicitudCatastral solicitudCatastral);

    /**
     * Avanza a la siguente actividad de la instancia de proceso de conservación, reclamando la
     * actividad actual para el usuario especificado en caso que esta no haya sido reclamada.
     *
     * @param usuarioEjecutorActActual Corresponde al usuario ejecutor de la actividad actual del
     * proceso. En caso que la actividad esté lista, se reclamará la actividad para este usuario y
     * se avanzará la actividad. El usuario, usuarios potenciales, o rol ejecutor de la siguiente
     * actividad será el especificado en el objeto de negocio.
     *
     * @param idProceso Identificador de la instancia del proceso en el gestor de procesos.
     * @param objNegocio Objeto de negocio para la siguiente actividad.
     * @return El objeto de negocio asociada a la actividad a la que se avanzó.
     *
     * @throws ExcepcionSNC En caso que se presente un error en el avance de la actividad.
     *
     * @author alejandro.sanchez
     */
    public SolicitudCatastral avanzarActividadPorIdProceso(UsuarioDTO usuarioEjecutorActActual,
        String idProceso, SolicitudCatastral solicitudCatastral);

    /**
     *
     * @param idProceso
     * @param ofertaInmobiliaria
     * @return
     */
    public OfertaInmobiliaria avanzarActividadPorIdProceso(String idProceso,
        OfertaInmobiliaria ofertaInmobiliaria);

    /**
     * Avanza a la siguente actividad de la instancia de proceso de ofertas, reclamando la actividad
     * actual para el usuario especificado en caso que esta no haya sido reclamada.
     *
     * @param usuarioEjecutorActActual Corresponde al usuario ejecutor de la actividad actual del
     * proceso. En caso que la actividad esté lista, se reclamará la actividad para este usuario y
     * se avanzará la actividad. El usuario, usuarios potenciales, o rol ejecutor de la siguiente
     * actividad será el especificado en el objeto de negocio.
     *
     * @param idProceso Identificador de la instancia del proceso en el gestor de procesos.
     * @param objNegocio Objeto de negocio para la siguiente actividad.
     * @return El objeto de negocio asociada a la actividad a la que se avanzó.
     *
     * @throws ExcepcionSNC En caso que se presente un error en el avance de la actividad.
     *
     * @author alejandro.sanchez
     */
    public OfertaInmobiliaria avanzarActividadPorIdProceso(UsuarioDTO usuarioEjecutorActActual,
        String idProceso, OfertaInmobiliaria ofertaInmobiliaria);

    /**
     * Permite la cancelación de un proceso a partir del identificador del proceso.
     *
     * @param idProceso Identificador de la instancia del proceso en el formato del gestor de
     * procesos.
     * @param motivo Motivo por el cual la instancia del proceso es cancelada.
     * @return Verdadero(true) en caso de que el proceso se haya cancelado satisfactoriamente, falso
     * (false) en caso contrario.
     *
     * @author alejandro.sanchez
     */
    public boolean cancelarProcesoPorIdProceso(String idProceso, String motivo);

    /**
     * Permite la creación de un proceso de ofertas inmobiliarias.
     *
     * @param ofertaInmobiliaria Objeto de negocio del proceso de ofertas inmobiliarias.
     *
     * @return Identificador de la instancia de proceso en el motor de procesos.
     *
     * @author alejandro.sanchez
     */
    public String crearProceso(OfertaInmobiliaria ofertaInmobiliaria);

    /**
     * Permite la creación de un proceso de control de calidad de ofertas inmobiliarias.
     *
     * @param controlCalidad Objeto de negocio asociado con el control de calidad.
     *
     * @return Identificador de la instancia de proceso en el motor de procesos.
     *
     * @author alejandro.sanchez
     */
    public String crearProceso(ControlCalidad controlCalidad);

    /**
     * Permite la creación de un proceso de actualización.
     *
     * @param actualizacionCatastral Objeto de negocio del proceso de actualización.
     *
     * @return Identificador de la instancia de proceso en el motor de procesos.
     *
     * @author alejandro.sanchez
     */
    public String crearProceso(ActualizacionCatastral actualizacionCatastral);

    /**
     * Permite la creación de un proceso de conservación.
     *
     * @param solicitudCatastral Objeto de negocio del proceso de conservación.
     *
     * @return Identificador de la instancia de proceso en el motor de procesos.
     */
    public String crearProceso(SolicitudCatastral solicitudCatastral);

    /**
     * Permite la creación de un proceso de Productos Catastrales.
     *
     * @param solicitudProductoCatastral Objeto de negocio para creación de una instancia del
     * proceso de productos catastrales.
     *
     * @return Identificador de la instancia de proceso en el motor de procesos.
     */
    public String crearProceso(SolicitudProductoCatastral solicitudProductoCatastral);

    /**
     * Permite crear de manera masiva, instancias de procesos de actualización express.
     *
     * @param municipioCodigo Un identificador de municipio válido.
     */
    public void crearProcesosActualizacionExpress(String municipioCodigo);

    /**
     * Crea de forma masiva las instancias de procesos asociadas a los objetos de negocio de
     * Conservación especificados
     *
     * @param solicitudesCatastrales Objeto de negocio del Proceso de Conservación.
     *
     * @throws ExcepcionSNC En caso que ocurra un error en la invoicación del método
     */
    public void crearProcesosConservacion(List<SolicitudCatastral> solicitudesCatastrales);

    /**
     * Permite reanudar de manera masiva, instancias de procesos de actualización express.
     *
     * @param municipioCodigo Un identificador de municipio válido.
     */
    public void reanudarProcesosActualizacionExpress(String municipioCodigo);

    /**
     * Obtiene el estado actual de un proceso
     *
     * @param idProceso Identificador del proceso en el gestor de procesos.
     * @return El estado del proceso definido en la enumeración @link EEstadoProceso. No se retorna
     * la enumaración por incompatibilidad de los objetos entre el cliente y el servidor de
     * procesos.
     *
     */
    public String obtenerEstadoProceso(String idProceso);

    public List<Actividad> obtenerActividadesProceso(String idProceso);

    /**
     * Consultar actividades de usuario en el Sistema Nacional Catastral.
     *
     * @param usuario Identificación en el directorio activo del usuario del sistema.
     * @return Estructura de datos con las actividades del usuario en el Sistema Nacional Catastral.
     */
    public ArbolProcesos consultarTareas(UsuarioDTO usuario);

    /**
     * Consultar la lista de actividades de un usuario en el Sistema Nacional Catastral.
     *
     * @param usuario Usuario en el directorio activo del usuario del sistema.
     * @return Una lista con las actividades del usuario especificado como parámetro.
     */
    public List<Actividad> consultarListaActividades(UsuarioDTO usuario);
    
    /**
	 * Consultar la lista de actividades de un usuario en el Sistema Nacional
	 * Catastral filtrando por el estado de la actividad.
	 * 
	 * @param usuario
	 *            Usuario en el directorio activo del usuario del
	 *            sistema.
	 * @return Una lista con las actividades del usuario especificado como
	 *         parámetro.
	 */
	public List<Actividad> consultarListaActividadesPorActividades(UsuarioDTO usuario);    

    /**
     * Consultar la lista de actividades de un usuario en el Sistema Nacional Catastral
     * especificando un rol en particular.
     *
     * @param usuario Usuario del sistema que se encuentra registrado en el directorio activo.
     *
     * @param rol Un rol existente en el sistema. Este método sólo es soportado para el proceso de
     * Conservación.
     *
     * @return Lista de actividades de un usuario en un rol particular.
     */
    public List<Actividad> consultarListaActividades(UsuarioDTO usuario, ERol rol);

    /**
     *
     * @param usuario Usuario en el directorio activo del usuario del sistema.
     * @param actividad Tipo de actividad a consultar
     * @return Lista actividades del tipo especificado por el usuario.
     */
    public List<Actividad> consultarListaActividades(UsuarioDTO usuario, String actividad);

    /**
     * Retorna la lista de actividades de acuerdo a los parámetros especificados y soportados
     *
     * @see co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades.
     *
     * @param parametros Parámetros de consulta, por ejemplo: territorial, nombre actividad. Los
     * parámetros o filtros soportados se especifican en
     * @see co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades
     *
     * @return Lista de actividades encontradas en el gestor de procesos.
     */
    public List<Actividad> consultarListaActividades(
        Map<EParametrosConsultaActividades, String> parametros);

    public List<Actividad> consultarListaActividades(String territorial, String nombreActividad);

    public List<Actividad> consultarListaActividadesFiltroNombreActividad(UsuarioDTO usuario,
        String actividad);

    /**
     * Retorna la lista de actividades asociadas a la instancia de proceso con el identificador de
     * objeto de negocio especificado como parámetro.
     *
     * @param idObjNegocio Identificador del objeto de negocio.
     * @param nombreActividad [opcional] Nombre de la actividad
     * @return
     */
    public List<Actividad> getActividadesPorIdObjetoNegocio(Long idObjNegocio,
        String nombreActividad);

    public List<Actividad> getActividadesPorIdObjetoNegocio(Long idObjNegocio);

    /**
     * Este método no debería usarse debido a que una actividad generalmente tiene más de usuario
     * que puede ejecutarla, por tanto, es necesario especificar cuál de esos usuarios completa la
     * actividad actual para avanzar a la siguiente. Debe usarse {@link #avanzarActividad(UsuarioDTO, String, SolicitudCatastral)
     * }
     *
     * @param idActividad
     * @param duracion
     * @param solicitudCatastral
     * @return
     */
    @Deprecated
    public SolicitudCatastral avanzarActividadConservacion(String idActividad,
        Duration duracion, SolicitudCatastral solicitudCatastral);

    /**
     * Reclama la actividad idActividad para el usuario especificado. Si la actividad puede ser
     * realizado por cualquier usuario de una lista de usuarios potenciales, puede utilizarse este
     * método para asignársela a uno en particular.
     *
     * @param idActividad Identificador de la actividad a reclamar.
     * @param usuario Identificación en el directorio activo del usuario del sistema.
     * @return
     */
    public SolicitudCatastral reclamarActividad(String idActividad,
        UsuarioDTO usuario) throws ExcepcionSNC;

    /**
     * Obtiene el estado actual de una actividad.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     * @return El estado de la actividad definido en la enumeración @link EEstadoActividad. No se
     * retorna la enumaración por incompatibilidad de los objetos entre el cliente y el servidor de
     * procesos.
     *
     */
    public String obtenerEstadoActividad(String idActividad);

    /**
     * Obtiene el objeto de negocio en su estado actual de una instancia de proceso de conservación.
     *
     * @param idActividad
     * @return Un objeto de negocio del proceso de conservación
     */
    public SolicitudCatastral obtenerObjetoNegocioConservacion(
        String idActividad);

    /**
     * Obtiene el objeto de negocio en su estado actual de una instancia del proceso de
     * actualización.
     *
     * @param idActividad
     * @return Un objeto de negocio del proceso de actualización
     */
    public ActualizacionCatastral obtenerObjetoNegocioActualizacion(
        String idActividad);

    /**
     * Obtiene el objeto de negocio asociado a la actividad e instancia de proceso de negocio
     * especificadas.
     *
     * @author alejandro.sanchez
     *
     * @param idProceso Identificador de la instancia del proceso en el formato del gestor de
     * procesos.
     * @param nombreActividad Nombre de la actividad de la que quiere recuperarse el objeto de
     * negocio.
     *
     * @return El objeto de negocio asociado a la instancia de proceso y actividad especificadas.
     */
    public SolicitudCatastral obtenerObjetoNegocioConservacion(
        String idProceso, String nombreActividad);

    /**
     * Obtiene el objeto de negocio asociado a la actividad e instancia de proceso de negocio
     * especificadas y del ID de trámite asociado. Consulta utilizada para poder obtener trámites
     * asociados a un trámite padre (GLPI_39021)
     *
     * @author william.guevara
     *
     * @param idProceso Identificador de la instancia del proceso en el formato del gestor de
     * procesos.
     * @param idTramite Identificador del trámite asociado.
     * @param nombreActividad Nombre de la actividad de la que quiere recuperarse el objeto de
     * negocio.
     *
     * @return El objeto de negocio asociado a la instancia de proceso y actividad especificadas.
     */
    public SolicitudCatastral obtenerObjetoNegocioConservacion(
        String idProceso, String idTramite, String nombreActividad);

    /**
     *
     * @author alejandro.sanchez
     *
     * Obtiene el objeto de negocio de la instancia de proceso con el identificador especificado.
     *
     * @param idProceso Identificador del proceso en el formato del gestor de procesos.
     *
     * @return El objeto de negocio en su estado actual, para la instancia del proceso de
     * conservación con el identificador especificado como parámetro.
     */
    public SolicitudCatastral obtenerObjetoNegocioConservacionPorIdProceso(String idProceso);

    /**
     * @author alejandro.sanchez
     *
     * Obtiene el objeto de negocio de la instancia de proceso con el identificador especificado.
     *
     * @param idProceso Identificador del proceso en el formato del gestor de procesos.
     *
     * @return El objeto de negocio en su estado actual, para la instancia del proceso de
     * actualización con el identificador especificado como parámetro.
     */
    public ActualizacionCatastral obtenerObjetoNegocioActualizacionPorIdProceso(String idProceso);

    /**
     * @author alejandro.sanchez
     *
     * Obtiene el objeto de negocio de la instancia de proceso con el identificador especificado.
     *
     * @param idProceso Identificador del proceso en el formato del gestor de procesos.
     *
     * @return El objeto de negocio en su estado actual, para la instancia del proceso de control de
     * calidad con el identificador especificado como parámetro.
     */
    public ControlCalidad obtenerObjetoNegocioControlCalidadPorIdProceso(String idProceso);

    /**
     * @author alejandro.sanchez
     *
     * Obtiene el objeto de negocio de la instancia de proceso con el identificador especificado.
     *
     * @param idProceso Identificador del proceso en el formato del gestor de procesos.
     *
     * @return El objeto de negocio en su estado actual, para la instancia del proceso de ofertas
     * inmobiliarias con el identificador especificado como parámetro.
     */
    public OfertaInmobiliaria obtenerObjetoNegocioOfertasInmobiliariasPorIdProceso(String idProceso);

    /**
     * Genera una imagen con el grafo con el flujo de ejecución del proceso al que pertenece la
     * actividad con el identificador especificado como parámetro.
     *
     * El contenido del archivo está en el formato especificado por {@link http://www.graphviz.org/}
     *
     * @author alejandro.sanchez
     *
     * @param idActividad Id de la actividad actual del proceso.
     *
     * @return Obtiene url de la imagen del grafo publicada en el servidor web
     */
    public String obtenerFlujoEjecucionProceso(String idActividad);

    /**
     * @author alejandro.sanchez
     *
     * Genera una imagen con el grafo con el flujo de ejecución del proceso con el id de proceso
     * especificado. Es necesario especifiar el tipo de proceso porque no se garantiza unicidad de
     * los identificados para diferentes procesos, es decir, el identificador del proceso de negocio
     * es único por proceso (Conservación, Actualización, etc.).
     *
     * El contenido del archivo está en el formato especificado por {@link http://www.graphviz.org/}
     *
     * @param mapaProceso Cadena macroproceso.proceso del proceso de negocio.
     * @param idObjetoNegocio Identificador del objeto de negocio.
     *
     * @return Obtiene url de la imagen del grafo publicada en el servidor web.
     */
    public String obtenerFlujoEjecucionProceso(String mapaProceso, long idObjetoNegocio);

    /**
     * Obtiene el conteo de actividades por tipo de actividad para un usuario.
     *
     * @param usuario Identificación en el directorio activo del usario del sistema.
     * @return
     */
    public TablaFrecuencia getConteoActividadesUsuario(UsuarioDTO usuario);

    /**
     * Obtiene el conteo de actividades por nombre actividad, variable objeto de negocio.
     *
     * @param usuario Usuario para el que se desea obtener el conteo de actividades.
     * @param atributo Variable del objeto de negocio por la que se desea filtrar.
     * @return Una tabla de contingencia con los conteos.
     */
    public TablaContingencia getConteoActividadesConFiltro(UsuarioDTO usuario,
        String atributo);

    /**
     * Permite reanudar una actividad suspendida. La actividad se reanudará a partir de la fecha
     * actual y la fecha de vencimiento corresponderá al tiempo restante que quedaba para ejecutar
     * la actividad hasta que dicha actividad se suspendió.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     *
     * @return La nueva fecha de vencimiento de la actividad.
     */
    public Calendar reanudarActividad(String idActividad);

    /**
     *
     * @param idActividad Identificador de la actividad que se va a transferir.
     * @param usuario Usuario al que se van a transferir las actividades.
     * @return Verdadero si la operación fue exitosa, falso en caso contrario
     */
    public boolean transferirActividadConservacion(String idActividad, UsuarioDTO usuario);

    /**
     *
     * @param idsActividades Lista de identificadores de actividades por avanzar
     * @param usuario Usuario al que se van a transferir las actividades.
     * @return Verdadero si la operación fue exitosa, falso en caso contrario
     */
    public boolean transferirActividadConservacion(List<String> idsActividades, UsuarioDTO usuario);

    /**
     * Permite suspender la actividad hasta la fecha especificada. Durante este tiempo, la actividad
     * no será listada en la lista de tareas del usuario.
     *
     * @param idActividad Identificador de la actividad en el gestor de procesos.
     *
     * @param fechaReanudacion Fecha en la que se reanudará la actividad (volverá a aparecer en la
     * lista de actividades del usuario). Se usa null si la suspensión es a término indefinido
     *
     * @param motivo Motivo por el cual se suspende la actividad.
     *
     * @return La fecha hasta la cual fue suspendida la actividad.
     */
    public Calendar suspenderActividad(String idActividad, Calendar fechaReanudacion, String motivo);

    /**
     * Método que consulta una lista de {@link Actividad} basadas en los parámetros enviados en el
     * {@link MultiMap}.
     *
     * @nota: Copiado y ajustado para {@link MultiMap} del método null null     consultarListaActividades(Map<EParametrosConsultaActividades,
	 *        String>). Se decidió hacer otro debido al impacto que podría tener la modificación del
     * anterior a aspectos que se encuentren funcionando.
     *
     * @author david.cifuentes
     *
     * @param parametrosHashTable {@link MultiMap} en el cual sus llaves son constantes de tipo
     * {@link EParametrosConsultaActividades} y sus values los valores respectivos a buscar.
     *
     * @return Lista de objetos {@link Actividad} encontrados en el gestor de procesos.
     *
     */
    // Esto no me funcionó, lo dejo por si es útil cuando se vayan a integrar
    // los historicos, me dí cuenta que la consulta a process se hace por un
    // objeto de negocio a la vez (a la fecha), más adelante es posible que
    // pueda ser usable.
    public List<Actividad> consultarListaActividadesPorParametrosMultiMap(
        MultiMap<EParametrosConsultaActividades, String> parametrosMultiMap);

    /**
     * Método que permite consultar el listado de usuarios ejecutores de una actividad definida. El
     * primer usuario retornado hace referencia al propietario de la actividad, cuando esta haya
     * sido reclamada.
     *
     *
     * @author william.guevara
     *
     * @param idActividad Identificador de actividad para la cual se va a consultar los usuarios
     * ejevutores
     *
     * @return Lista de usuarios {@link UsuarioDTO} encontrados para la actividad.
     *
     */
    public List<UsuarioDTO> obtenerUsuariosActividad(String idActividad);
    
    /**
     * Método que permite consultar el codigo de la territorial del tramite en el modelo BPM
     * @author Michael Peña
     *
     * @param idActividad Identificador de actividad para la cual se va a consultar los usuarios
     * ejevutores
     *
     * @return Lista de usuarios {@link UsuarioDTO} encontrados para la actividad.
     *
     */
    
    public String obtenerCodTerritorial(String idActividad);

    /**
     * Permite replanificar el tiempo de expiración o vencimiento de una tarea de un proceso
     * catastral.
     *
     * @param idActividad Identificador en el gestor de procesos de la tarea catastral.
     * @param fechaReplanificacion Nueva fecha de expiración o vencimiento.
     * @param motivo Motivo por el cual se realiza la replanificación.
     *
     * @return Nueva fecha de expiración o vencimiento.
     */
    public Calendar replanificarActividad(String idActividad, Calendar fechaReplanificacion,
        String motivo);

}
