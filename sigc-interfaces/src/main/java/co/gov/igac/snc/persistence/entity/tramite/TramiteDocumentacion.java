package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * TramiteDocumentacion entity.
 */
/*
 * fredy.wilches -> nuevo campo transient oficinaEmisoraDescripcion fredy.wilches -> Campo
 * Soporte_documento_id cambiado por Documento_soporte_id fredy.wilches -> inclusión Secuencia
 * fredy.wilches -> nuevo campo aporta
 *
 * @modified by: pedro.garcia what: - adición de anotaciones @Temporal para los campos de fecha
 *
 * @modified	by: juan.agudelo 29-08-2011 adición de método transient digitalizable
 *
 * @modified by: javier.aponte 25-07-2012 se implementa la interfaz Cloneable y se agrega método
 * clone
 *
 * @modified by: juanfelipe.garcia :: 05-09-2013 :: adición de variable transient analogo
 */
@Entity
@Table(name = "TRAMITE_DOCUMENTACION", schema = "SNC_TRAMITE")
public class TramiteDocumentacion implements java.io.Serializable, Cloneable {

    // Fields
    /**
     * se realizaron los joins sobre departamentoCodio, municipio codigo y tipoDocumentoId
     */
    private Long id;
    private Tramite tramite;
    private String aportado;
    private TipoDocumento tipoDocumento;
    private Date fechaAporte;
    private Date fechaSolicitudDocumento;
    private Documento documentoSoporte;
    private String adicional;
    private Integer cantidadFolios;
    private Date fechaTitulo;
    private String idRepositorioDocumentos;
    private String numeroTitulo;
    private String oficinaEmisora;
    private Departamento departamento;
    private Municipio municipio;
    private String detalle;
    private String requerido;
    private String usuarioLog;
    private Date fechaLog;

    private String oficinaEmisoraDescripcion;
    private String digital;
    private boolean analogo;

    @SuppressWarnings("unused")
    private boolean aporta;
    @SuppressWarnings("unused")
    private boolean digitalizable;

    // Constructors
    /** default constructor */
    public TramiteDocumentacion() {
    }

    /** minimal constructor */
    public TramiteDocumentacion(Long id, String aportado, Long tipoDocumentoId,
        String adicional, Integer cantidadFolios, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.aportado = aportado;
        this.adicional = adicional;
        this.cantidadFolios = cantidadFolios;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteDocumentacion(Long id, Tramite tramite, String aportado,
        Long tipoDocumentoId, Date fechaAporte,
        Date fechaSolicitudDocumento, Documento documentoSoporte,
        String adicional, Integer cantidadFolios, Date fechaTitulo,
        String numeroTitulo, String oficinaEmisora,
        String departamentoCodigo, String municipioCodigo, String detalle,
        String requerido, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.aportado = aportado;
        this.fechaAporte = fechaAporte;
        this.fechaSolicitudDocumento = fechaSolicitudDocumento;
        this.documentoSoporte = documentoSoporte;
        this.adicional = adicional;
        this.cantidadFolios = cantidadFolios;
        this.fechaTitulo = fechaTitulo;
        this.numeroTitulo = numeroTitulo;
        this.oficinaEmisora = oficinaEmisora;

        this.detalle = detalle;
        this.requerido = requerido;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_DOCUMENTACION_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_DOCUMENTACION_ID_SEQ", sequenceName =
        "TRAMITE_DOCUMENTACION_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "APORTADO", nullable = false, length = 2)
    public String getAportado() {
        return this.aportado;
    }

    public void setAportado(String aportado) {
        this.aportado = aportado;
    }

    @Column(name = "FECHA_APORTE", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaAporte() {
        return this.fechaAporte;
    }

    public void setFechaAporte(Date fechaAporte) {
        this.fechaAporte = fechaAporte;
    }

    @Column(name = "FECHA_SOLICITUD_DOCUMENTO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaSolicitudDocumento() {
        return this.fechaSolicitudDocumento;
    }

    public void setFechaSolicitudDocumento(Date fechaSolicitudDocumento) {
        this.fechaSolicitudDocumento = fechaSolicitudDocumento;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = true)
    public Documento getDocumentoSoporte() {
        return this.documentoSoporte;
    }

    public void setDocumentoSoporte(Documento documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    @Column(name = "ADICIONAL", nullable = false, length = 2)
    public String getAdicional() {
        return this.adicional;
    }

    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }

    @Column(name = "CANTIDAD_FOLIOS", nullable = false, precision = 6, scale = 0)
    public Integer getCantidadFolios() {
        return this.cantidadFolios;
    }

    public void setCantidadFolios(Integer cantidadFolios) {
        this.cantidadFolios = cantidadFolios;
    }

    @Column(name = "FECHA_TITULO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaTitulo() {
        return this.fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS")
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @Column(name = "NUMERO_TITULO", length = 20)
    public String getNumeroTitulo() {
        return this.numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    @Column(name = "OFICINA_EMISORA", length = 30)
    public String getOficinaEmisora() {
        return this.oficinaEmisora;
    }

    public void setOficinaEmisora(String oficinaEmisora) {
        this.oficinaEmisora = oficinaEmisora;
    }

    @Column(name = "DETALLE", length = 2000)
    public String getDetalle() {
        return this.detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @Column(name = "REQUERIDO", length = 2)
    public String getRequerido() {
        return this.requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    @Column(name = "DIGITAL")
    public String getDigital() {
        return this.digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = true)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID", nullable = false)
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Transient
    public String getOficinaEmisoraDescripcion() {
        return oficinaEmisoraDescripcion;
    }

    public void setOficinaEmisoraDescripcion(String oficinaEmisoraDescripcion) {
        this.oficinaEmisoraDescripcion = oficinaEmisoraDescripcion;
    }

    public void setAporta(boolean aporta) {
        if (aporta) {
            aportado = ESiNo.SI.getCodigo();
        } else {
            aportado = ESiNo.NO.getCodigo();
        }
    }

    @Transient
    public boolean isAporta() {
        return this.aportado != null && this.aportado.equals(ESiNo.SI.getCodigo()) ? true : false;
    }

    public void setDigitalizable(boolean digitalizable) {
        if (digitalizable) {
            this.digital = ESiNo.SI.getCodigo();
        } else {
            this.digital = ESiNo.NO.getCodigo();
        }
    }

    @Transient
    public boolean isDigitalizable() {
        return this.digital != null &&
            this.digital.equals(ESiNo.SI.getCodigo()) ? true : false;
    }

    private Long idTemporal;

    @Transient
    public Long getIdTemporal() {
        return idTemporal;
    }

    public void setIdTemporal(Long idTemporal) {
        this.idTemporal = idTemporal;
    }

    /**
     * @author fredy.wilches Usado en asociar documentacion adicional en ejecucion
     */
    public boolean equals(Object o) {
        if (o instanceof TramiteDocumentacion) {
            TramiteDocumentacion td = (TramiteDocumentacion) o;
            if (this.id == null) {
                return this.idTemporal != null && td.getIdTemporal() != null &&
                    this.idTemporal.equals(td.getIdTemporal());
            } else {
                return this.id != null && td.getId() != null &&
                    this.id.equals(td.getId());
            }
        } else {
            return false;
        }

    }

    /**
     * @author javier.aponte Método para poder hacer un clon de trámite documentación
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    @Transient
    public boolean isAnalogo() {
        return analogo;
    }

    public void setAnalogo(boolean analogo) {
        this.analogo = analogo;
    }

}
