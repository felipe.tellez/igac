package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * The persistent class for the P_FICHA_MATRIZ_MODELO database table.
 *
 * @modified david.cifuentes Adicion del campo descripcion :: 20/04/2012 Eliminación del campo
 * usoContruccion por cambio en el modelo de datos :: 20/04/2012 Cambio en el tipo de dato para
 * numeroUnidades y areaTerreno de BigDecimal a Double :: 23/04/2012 Eliminación del campo
 * tipoCalificacion por cambio en el modelo de datos :: 25/04/2012 Adición de la secuencia
 * FICHA_MATRIZ_MODELO_ID_SEQ :: 26/04/2012
 *
 */
@Entity
@Table(name = "P_FICHA_MATRIZ_MODELO")
public class PFichaMatrizModelo implements Serializable, IProyeccionObject {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Double areaConstruida;
    private Double areaTerreno;
    private String cancelaInscribe;
    private Date fechaLog;
    private String nombre;
    private Double numeroUnidades;
    private BigDecimal planoDocumentoId;
    private String usuarioLog;
    private PFichaMatriz PFichaMatriz;
    private List<PFmModeloConstruccion> PFmModeloConstruccions;

    private String descripcion;

    private String estado;

    public PFichaMatrizModelo() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FICHA_MATRIZ_MODELO_ID_SEQ")
    @SequenceGenerator(name = "FICHA_MATRIZ_MODELO_ID_SEQ", sequenceName =
        "FICHA_MATRIZ_MODELO_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_UNIDADES")
    public Double getNumeroUnidades() {
        return this.numeroUnidades;
    }

    public void setNumeroUnidades(Double numeroUnidades) {
        this.numeroUnidades = numeroUnidades;
    }

    @Column(name = "PLANO_DOCUMENTO_ID")
    public BigDecimal getPlanoDocumentoId() {
        return this.planoDocumentoId;
    }

    public void setPlanoDocumentoId(BigDecimal planoDocumentoId) {
        this.planoDocumentoId = planoDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to PFichaMatriz
    @ManyToOne
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public PFichaMatriz getPFichaMatriz() {
        return this.PFichaMatriz;
    }

    public void setPFichaMatriz(PFichaMatriz PFichaMatriz) {
        this.PFichaMatriz = PFichaMatriz;
    }

    //bi-directional many-to-one association to PFmModeloConstruccion
    @OneToMany(mappedBy = "PFichaMatrizModelo")
    public List<PFmModeloConstruccion> getPFmModeloConstruccions() {
        return this.PFmModeloConstruccions;
    }

    public void setPFmModeloConstruccions(List<PFmModeloConstruccion> PFmModeloConstruccions) {
        this.PFmModeloConstruccions = PFmModeloConstruccions;
    }

    @Column(length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
