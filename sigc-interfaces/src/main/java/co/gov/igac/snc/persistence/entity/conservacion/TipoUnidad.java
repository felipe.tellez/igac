package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TipoUnidad entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TIPO_UNIDAD", schema = "SNC_CONSERVACION")
public class TipoUnidad implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private FichaMatriz fichaMatriz;
    private String nombre;
    private Double areaConstruida;
    private Double areaTerreno;
    private Long planoDocumentoId;
    private Short numeroUnidades;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TipoUnidad() {
    }

    /** minimal constructor */
    public TipoUnidad(Long id, FichaMatriz fichaMatriz, String nombre,
        Double areaConstruida, Double areaTerreno, Short numeroUnidades,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.fichaMatriz = fichaMatriz;
        this.nombre = nombre;
        this.areaConstruida = areaConstruida;
        this.areaTerreno = areaTerreno;
        this.numeroUnidades = numeroUnidades;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TipoUnidad(Long id, FichaMatriz fichaMatriz, String nombre,
        Double areaConstruida, Double areaTerreno, Long planoDocumentoId,
        Short numeroUnidades, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.fichaMatriz = fichaMatriz;
        this.nombre = nombre;
        this.areaConstruida = areaConstruida;
        this.areaTerreno = areaTerreno;
        this.planoDocumentoId = planoDocumentoId;
        this.numeroUnidades = numeroUnidades;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHA_MATRIZ_ID", nullable = false)
    public FichaMatriz getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(FichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    @Column(name = "NOMBRE", nullable = false, length = 50)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "AREA_CONSTRUIDA", nullable = false, precision = 10)
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 10)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "PLANO_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getPlanoDocumentoId() {
        return this.planoDocumentoId;
    }

    public void setPlanoDocumentoId(Long planoDocumentoId) {
        this.planoDocumentoId = planoDocumentoId;
    }

    @Column(name = "NUMERO_UNIDADES", nullable = false, precision = 4, scale = 0)
    public Short getNumeroUnidades() {
        return this.numeroUnidades;
    }

    public void setNumeroUnidades(Short numeroUnidades) {
        this.numeroUnidades = numeroUnidades;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }
}
