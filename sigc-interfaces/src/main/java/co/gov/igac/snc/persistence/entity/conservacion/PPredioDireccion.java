package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * PPredioDireccion entity. @author MyEclipse Persistence Tools
 *
 */
/*
 * MODIFICACIONES PROPIAS A LA CLASE PPredioDireccion: @author null Se agregó serialVersionUID
 * @author javier.aponte Se agrega propiedad Transient selectedPrincipal para manejo de atributo
 * 'Principal' (SI/NO) como booleano @author fabio.navarrete Se agregó el método y atributo
 * transient para consultar el valor de la propiedad cancelaInscribe
 *
 * @modified :: david.cifuentes :: Se implementa la interfaz Cloneable y se agrega método clone ::
 * 22/08/12
 */
@Entity
@NamedQueries({@NamedQuery(name = "findPPredioDireccionByPPredio", query =
        "FROM PPredioDireccion ppd WHERE ppd.PPredio.id = :idPPredio")})
@Table(name = "P_PREDIO_DIRECCION", schema = "SNC_CONSERVACION")
public class PPredioDireccion implements java.io.Serializable, IProyeccionObject, Cloneable,
    IInscripcionCatastral {

    /**
     *
     */
    private static final long serialVersionUID = -3578156248321986737L;
    // Fields

    private Long id;
    private PPredio PPredio;
    private String direccion;
    private String principal;
    private String referencia;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private Date fechaInscripcionCatastral;
    private String cancelaInscribeValor;
    private String codigoPostal;
    private boolean original;

    // Constructors
    /** default constructor */
    public PPredioDireccion() {
    }

    /** minimal constructor */
    public PPredioDireccion(Long id, PPredio PPredio, String direccion,
        String principal, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.direccion = direccion;
        this.principal = principal;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PPredioDireccion(Long id, PPredio PPredio, String direccion,
        String principal, String referencia, String cancelaInscribe, Date fechaInscripcionCatastral,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.direccion = direccion;
        this.principal = principal;
        this.referencia = referencia;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    public PPredioDireccion(PredioDireccion pd) {
        //ppd.setId(pd.getId());
        this.direccion = pd.getDireccion();
        this.fechaInscripcionCatastral = pd.getFechaInscripcionCatastral();
        this.fechaLog = new Date();
        this.principal = pd.getPrincipal();
        this.referencia = pd.getReferencia();
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREDIO_DIRECCION_ID_SEQ")
    @SequenceGenerator(name = "PREDIO_DIRECCION_ID_SEQ", sequenceName = "PREDIO_DIRECCION_ID_SEQ",
        allocationSize = 0)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "DIRECCION", nullable = false, length = 250)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "PRINCIPAL", nullable = false, length = 2)
    public String getPrincipal() {
        return this.principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    @Column(name = "REFERENCIA", length = 100)
    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    @Override
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    @Override
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    @Override
    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "CODIGO_POSTAL")
    public String getCodigoPostal() {
        return this.codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Override
    public Date getFechaLog() {
        return this.fechaLog;
    }

    @Override
    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * @author david.cifuentes Método que realiza un clone del objeto pPredioDireccion
     */
    @Override
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }
//--------------------------------------------------------------------------------------------------    

    @Transient
    public boolean getSelectedPrincipal() {
        if (this.principal != null) {
            return this.principal.equals(ESiNo.SI.getCodigo());
        }
        return false;
    }

    public void setSelectedPrincipal(boolean selected) {
        if (selected) {
            this.principal = ESiNo.SI.getCodigo();
        } else {
            this.principal = ESiNo.NO.getCodigo();
        }
    }

    @Transient
    public String getCancelaInscribeValor() {
        return this.cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    /**
     * Determina si esta direccion correspone a al proyeccion de alguna que pertenecia al predio
     * antes de la proyeccion. Se debe inicializar manualmente cuando se quiera usar.
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isOriginal() {
        return original;
    }

    public void setOriginal(boolean original) {
        this.original = original;
    }

}
