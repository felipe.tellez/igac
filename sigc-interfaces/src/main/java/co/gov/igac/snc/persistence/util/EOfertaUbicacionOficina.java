/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los codigos de ubicacion para oficina
 *
 * @author christian.rodriguez
 */
public enum EOfertaUbicacionOficina {

    CASA("CASA"),
    CENTRO_COMERCIAL("CENTRO COMERCIAL"),
    EDIFICIO("EDIFICIO");

    private String codigo;

    private EOfertaUbicacionOficina(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
