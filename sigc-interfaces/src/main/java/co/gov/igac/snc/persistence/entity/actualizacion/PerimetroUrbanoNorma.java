package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * Entidad Perimetro Urbano Norma
 *
 * @author andres.eslava
 *
 */
@Entity
@Table(name = "PERIMETRO_URBANO_NORMA")
public class PerimetroUrbanoNorma implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long actualizacionPerimetroUrbId;
    private String norma;
    private String Descripcion;
    private String usuarioLog;
    private Date fechaLog;

    @Id
    @SequenceGenerator(name = "PERIMETRO_URBANO_NORMA_GENERATOR", sequenceName =
        "PERIMETRO_URBANO_NORMA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PERIMETRO_URBANO_NORMA_GENERATOR")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTUALIZACION_PERIMETRO_URB_ID")
    public Long getActualizacionPerimetroUrbId() {
        return actualizacionPerimetroUrbId;
    }

    public void setActualizacionPerimetroUrbId(Long actualizacionPerimetroUrbId) {
        this.actualizacionPerimetroUrbId = actualizacionPerimetroUrbId;
    }

    @Column(name = "NORMA")
    public String getNorma() {
        return norma;
    }

    public void setNorma(String norma) {
        this.norma = norma;
    }

    @Column(name = "DESCRIPCION")
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
