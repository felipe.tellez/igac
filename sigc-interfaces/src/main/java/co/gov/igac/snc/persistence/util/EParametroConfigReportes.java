package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente para los parametros de configuracion avanzda de reportes.
 *
 * @author leidy.gonzalez
 */
public enum EParametroConfigReportes {
    NUMERO_PREDIAL("NUMERO PREDIAL"),
    DEPARTAMENTO("DEPARTAMENTO"),
    MUNICIPIO("MUNICIPIO"),
    TIPO_IDENTIFCACION("TIPO IDENTIFICACION"),
    NUMERO_IDENTIFICACION("NUMERO_IDENTIFICACION"),
    MODO_ADQUISICION("MODO ADQUISICION"),
    TIPO_TITULO("TIPO TITULO"),
    ENTIDAD("ENTIDAD"),
    DIRECCION("DIRECCION"),
    MATRICULA_INMOBILIARIA("MATRICULA INMOBILIARIA"),
    NUMERO_REGISTRO("NUMERO REGISTRO"),
    TIPO_PREDIO("TIPO PREDIO"),
    DESTINO_ECONOMICO("DESTINO ECONOMICO"),
    AREA_TERRENO("AREA TERRENO"),
    AREA_CONSTRUIDA("AREA CONSTRUIDA"),
    AVALUO_TERRENO("AVALUO TERRENO"),
    AVALUO_TOTAL("AVALUO TOTAL"),
    ZONA_FISICA("ZONA FISICA"),
    ZONA_GEOECONOMICA("ZONA GEOECONOMICA"),
    USO_CONSTRUCCION("USO CONSTTRUCCION"),
    FECHA_INICIO_BLOQUEO("FECHA INICIO BLOQUEO"),
    FECHA_DESBLOQUEO("FECHA DESBLOQUEO"),
    NUMERO_PREDIAL_MANZANA("NUMERO PREDIAL MANZANA");

    private String codigo;

    private EParametroConfigReportes(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
