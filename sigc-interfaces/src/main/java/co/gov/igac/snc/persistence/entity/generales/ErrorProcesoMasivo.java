package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ErrorProcesoMasivo entity.
 *
 * @Modified 13-12-11 juan.agudelo se agregó la secuencia ERROR_PROCESO_MASIVO_ID_SEQ se adicionó el
 * objeto soporteDocumento
 */
@Entity
@Table(name = "ERROR_PROCESO_MASIVO")
public class ErrorProcesoMasivo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2012204267819291129L;

    // Fields
    private long id;
    private String errorTecnico;
    private String errorUsuario;
    private Date fecha;
    private String programa;
    private String textoFuente;
    private Documento soporteDocumento;

    // Constructors
    /** default constructor */
    public ErrorProcesoMasivo() {
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ERROR_PROCESO_MASIVO_ID_SEQ")
    @SequenceGenerator(name = "ERROR_PROCESO_MASIVO_ID_SEQ", sequenceName =
        "ERROR_PROCESO_MASIVO_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "ERROR_TECNICO", length = 600)
    public String getErrorTecnico() {
        return this.errorTecnico;
    }

    public void setErrorTecnico(String errorTecnico) {
        this.errorTecnico = errorTecnico;
    }

    @Column(name = "ERROR_USUARIO", length = 600)
    public String getErrorUsuario() {
        return this.errorUsuario;
    }

    public void setErrorUsuario(String errorUsuario) {
        this.errorUsuario = errorUsuario;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA", length = 7)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(nullable = false, length = 250)
    public String getPrograma() {
        return this.programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    @Column(name = "TEXTO_FUENTE", length = 4000)
    public String getTextoFuente() {
        return this.textoFuente;
    }

    public void setTextoFuente(String textoFuente) {
        this.textoFuente = textoFuente;
    }

    // bi-directional many-to-one association to Documento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = false)
    public Documento getSoporteDocumento() {
        return this.soporteDocumento;
    }

    public void setSoporteDocumento(Documento soporteDocumento) {
        this.soporteDocumento = soporteDocumento;
    }

}
