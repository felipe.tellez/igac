package co.gov.igac.snc.util;

import java.io.Serializable;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * Datos basicos para la busqueda de personas bloqueadas
 *
 * @author juan.agudelo
 *
 */
public class FiltroDatosConsultaPersonasBloqueo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6445584706810085167L;

    private String tipoIdentificacion;
    private String numeroIdentificacion;

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public FiltroDatosConsultaPersonasBloqueo(String tipoIdentificacion,
        String numeroIdentificacion) {
        super();
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
    }
}
