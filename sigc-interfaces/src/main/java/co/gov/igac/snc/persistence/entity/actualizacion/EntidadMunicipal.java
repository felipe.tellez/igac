package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the ENTIDAD_MUNICIPAL database table.
 */
/**
 * Modificaciones a la clase EntidadMunicipal:
 */
@Entity
@Table(name = "ENTIDAD_MUNICIPAL")
public class EntidadMunicipal implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String direccion;
    private Date fechaLog;
    private String nombre;
    private String representanteCargo;
    private String representanteNombre;
    private String representanteTratamiento;
    private String usuarioLog;
    private List<EntidadMunicipalFormato> entidadMunicipalFormatos;
    private List<NomenclaturaVialEntidad> nomenclaturaVialEntidads;

    public EntidadMunicipal() {
    }

    @Id
    @SequenceGenerator(name = "ENTIDAD_MUNICIPAL_ID_GENERATOR", sequenceName =
        "ENTIDAD_MUNICIPAL_ID_SEQ")
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENTIDAD_MUNICIPAL_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "REPRESENTANTE_CARGO", length = 100)
    public String getRepresentanteCargo() {
        return this.representanteCargo;
    }

    public void setRepresentanteCargo(String representanteCargo) {
        this.representanteCargo = representanteCargo;
    }

    @Column(name = "REPRESENTANTE_NOMBRE", nullable = false, length = 100)
    public String getRepresentanteNombre() {
        return this.representanteNombre;
    }

    public void setRepresentanteNombre(String representanteNombre) {
        this.representanteNombre = representanteNombre;
    }

    @Column(name = "REPRESENTANTE_TRATAMIENTO", length = 30)
    public String getRepresentanteTratamiento() {
        return this.representanteTratamiento;
    }

    public void setRepresentanteTratamiento(String representanteTratamiento) {
        this.representanteTratamiento = representanteTratamiento;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to EntidadMunicipalFormato
    @OneToMany(mappedBy = "entidadMunicipal", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<EntidadMunicipalFormato> getEntidadMunicipalFormatos() {
        return this.entidadMunicipalFormatos;
    }

    public void setEntidadMunicipalFormatos(List<EntidadMunicipalFormato> entidadMunicipalFormatos) {
        this.entidadMunicipalFormatos = entidadMunicipalFormatos;
    }

    //bi-directional many-to-one association to NomenclaturaVialEntidad
    @OneToMany(mappedBy = "entidadMunicipal", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<NomenclaturaVialEntidad> getNomenclaturaVialEntidads() {
        return this.nomenclaturaVialEntidads;
    }

    public void setNomenclaturaVialEntidads(List<NomenclaturaVialEntidad> nomenclaturaVialEntidads) {
        this.nomenclaturaVialEntidads = nomenclaturaVialEntidads;
    }

}
