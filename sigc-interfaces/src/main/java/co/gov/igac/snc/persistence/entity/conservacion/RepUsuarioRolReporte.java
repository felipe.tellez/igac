package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the REP_USUARIO_ROL_REPORTE database table.
 *
 * MODIFICACIONES PROPIAS A LA CLASE RepReporteEjecucion:
 *
 * david.cifuentes :: Se agregaron los métodos transient isConsultar e isGenerar :: 16/08/2016
 */
@Entity
@Table(name = "REP_USUARIO_ROL_REPORTE")
@NamedQuery(name = "RepUsuarioRolReporte.findAll", query = "SELECT r FROM RepUsuarioRolReporte r")
public class RepUsuarioRolReporte implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String consultar;
    private String estado;
    private String generar;
    private RepReporte repReporte;
    private String rolAsignado;
    private String usuarioAsignado;
    private String entidadCreacion;
    private String usuarioLog;
    private Date fechaLog;
    private List<RepTerritorialReporte> repTerritorialReportes;

    public RepUsuarioRolReporte() {
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_USUARIO_ROL_REPORTE_ID_SEQ")
    @SequenceGenerator(name = "REP_USUARIO_ROL_REPORTE_ID_SEQ", sequenceName =
        "REP_USUARIO_ROL_REPORTE_ID_SEQ", allocationSize = 0)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConsultar() {
        return this.consultar;
    }

    public void setConsultar(String consultar) {
        this.consultar = consultar;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getGenerar() {
        return this.generar;
    }

    public void setGenerar(String generar) {
        this.generar = generar;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTE_ID")
    public RepReporte getRepReporte() {
        return this.repReporte;
    }

    public void setRepReporte(RepReporte repReporte) {
        this.repReporte = repReporte;
    }

    @Column(name = "ROL_ASIGNADO")
    public String getRolAsignado() {
        return this.rolAsignado;
    }

    public void setRolAsignado(String rolAsignado) {
        this.rolAsignado = rolAsignado;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "USUARIO_ASIGNADO")
    public String getUsuarioAsignado() {
        return this.usuarioAsignado;
    }

    public void setUsuarioAsignado(String usuarioAsignado) {
        this.usuarioAsignado = usuarioAsignado;
    }

    @Column(name = "ENTIDAD_CREACION")
    public String getEntidadCreacion() {
        return entidadCreacion;
    }

    public void setEntidadCreacion(String entidadCreacion) {
        this.entidadCreacion = entidadCreacion;
    }

    //bi-directional many-to-one association to RepTerritorialReporte
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "repUsuarioRolReporte")
    public List<RepTerritorialReporte> getRepTerritorialReportes() {
        return this.repTerritorialReportes;
    }

    public void setRepTerritorialReportes(List<RepTerritorialReporte> repTerritorialReportes) {
        this.repTerritorialReportes = repTerritorialReportes;
    }

    public RepTerritorialReporte addRepTerritorialReporte(
        RepTerritorialReporte repTerritorialReporte) {
        getRepTerritorialReportes().add(repTerritorialReporte);
        repTerritorialReporte.setRepUsuarioRolReporte(this);

        return repTerritorialReporte;
    }

    public RepTerritorialReporte removeRepTerritorialReporte(
        RepTerritorialReporte repTerritorialReporte) {
        getRepTerritorialReportes().remove(repTerritorialReporte);
        repTerritorialReporte.setRepUsuarioRolReporte(null);

        return repTerritorialReporte;
    }

    /**
     * Método que retorna si el usuario tien permisos de generación de un reporte.
     *
     * @return
     */
    @Transient
    public boolean isPermisoGenerar() {
        return (ESiNo.SI.getCodigo().equals(this.generar)) ? true : false;
    }

    /**
     * Método que retorna si el usuario tien permisos de consulta de un reporte.
     *
     * @return
     */
    @Transient
    public boolean isPermisoConsultar() {
        return (ESiNo.SI.getCodigo().equals(this.consultar)) ? true : false;
    }

    /**
     * Método que realiza un clone del objeto {@link RepUsuarioRolReporte}.
     *
     * @author david.cifuentes
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }
}
