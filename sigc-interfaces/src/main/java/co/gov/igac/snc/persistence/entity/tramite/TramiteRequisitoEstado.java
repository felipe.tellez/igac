package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TramiteRequisitoEstado entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAMITE_REQUISITO_ESTADO", schema = "SNC_TRAMITE")
public class TramiteRequisitoEstado implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private TramiteRequisito tramiteRequisito;
    private String estadoInicial;
    private String estadoSiguiente;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramiteRequisitoEstado() {
    }

    /** full constructor */
    public TramiteRequisitoEstado(Long id, TramiteRequisito tramiteRequisito,
        String estadoInicial, String estadoSiguiente, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tramiteRequisito = tramiteRequisito;
        this.estadoInicial = estadoInicial;
        this.estadoSiguiente = estadoSiguiente;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_REQUISITO_ID", nullable = false)
    public TramiteRequisito getTramiteRequisito() {
        return this.tramiteRequisito;
    }

    public void setTramiteRequisito(TramiteRequisito tramiteRequisito) {
        this.tramiteRequisito = tramiteRequisito;
    }

    @Column(name = "ESTADO_INICIAL", nullable = false, length = 30)
    public String getEstadoInicial() {
        return this.estadoInicial;
    }

    public void setEstadoInicial(String estadoInicial) {
        this.estadoInicial = estadoInicial;
    }

    @Column(name = "ESTADO_SIGUIENTE", nullable = false, length = 30)
    public String getEstadoSiguiente() {
        return this.estadoSiguiente;
    }

    public void setEstadoSiguiente(String estadoSiguiente) {
        this.estadoSiguiente = estadoSiguiente;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
