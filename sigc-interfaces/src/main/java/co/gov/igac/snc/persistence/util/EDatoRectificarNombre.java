/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración que contiene los nombres de los datos a rectificar
 *
 * @author franz.gamba
 *
 */
/*
 * @modified by leidy.gonzalez:: #15413::09/12/2015:: Se agregan constrantes para rectificar
 */
public enum EDatoRectificarNombre {

    AREA_CONSTRUIDA("Area total construída"),
    AREA_TERRENO("Area total terreno"),
    CANCELACION_DOBLE_INSCRIPCION("Cancelación doble inscripción"),
    COEFICIENTE("Coeficiente de copropiedad"),
    DETALLE_CALIFICACION("Detalle calificación"),
    DIMENSION("Dimension"),
    DIRECCION_PRINCIPAL("Dirección principal"),
    FOTO("Foto"),
    NUMERO_IDENTIFICACION("Nro. identificación"),
    PARTICIPACION("Participación"),
    PRIMER_APELLIDO("Primer apellido"),
    PRIMER_NOMBRE("Primer nombre"),
    RAZON_SOCIAL("Razón social"),
    SEGUNDO_APELLIDO("Segundo apellido"),
    SEGUNDO_NOMBRE("Segundo nombre"),
    SIGLA("Sigla"),
    TIPO_IDENTIFICACION("Tipo identificación"),
    ZONA_FISICA("Zona física"),
    ZONA_GEOECONOMICA("Zona Geoeconómica"),
    DIRECCION("Dirección"),
    TIPO("Tipo de predio"),
    DESTINO("Destino del predio"),
    MODO_ADQUISICION("Modo de adquisición"),
    VALOR("Valor de compra"),
    TIPO_TITULO("Tipo Título"),
    ENTIDAD_EMISORA("Entidad emisora"),
    DEPARTAMENTO("Departamento"),
    MUNICIPIO("Municipio"),
    NUMERO_TITULO("Número Título"),
    FECHA_TITULO("Fecha Título"),
    FECHA_REGISTRO("Fecha Registro"),
    ANIO_CONSTRUCCION("Año construccion"),
    TOTAL_PISOS_CONSTRUCCION("Total pisos construcción"),
    TOTAL_HABITACIONES("Total Habitaciones"),
    TOTAL_BANIOS("Total baños"),
    TOTAL_LOCALES("Total locales");

    private String codigo;

    private EDatoRectificarNombre(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
