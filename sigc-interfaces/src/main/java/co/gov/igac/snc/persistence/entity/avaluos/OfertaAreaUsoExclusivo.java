package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the OFERTA_AREA_USO_EXCLUSIVO database table.
 *
 */
@Entity
@Table(name = "OFERTA_AREA_USO_EXCLUSIVO")
public class OfertaAreaUsoExclusivo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String areaUsoExclusivo;
    private Date fechaLog;
    private String usuarioLog;
    private OfertaInmobiliaria ofertaInmobiliaria;

    public OfertaAreaUsoExclusivo() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OFERTA_AREA_USO_EXCLUSI_ID_SEQ")
    @SequenceGenerator(name = "OFERTA_AREA_USO_EXCLUSI_ID_SEQ", sequenceName =
        "OFERTA_AREA_USO_EXCLUSI_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_USO_EXCLUSIVO")
    public String getAreaUsoExclusivo() {
        return this.areaUsoExclusivo;
    }

    public void setAreaUsoExclusivo(String areaUsoExclusivo) {
        this.areaUsoExclusivo = areaUsoExclusivo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to OfertaInmobiliaria
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID")
    public OfertaInmobiliaria getOfertaInmobiliaria() {
        return this.ofertaInmobiliaria;
    }

    public void setOfertaInmobiliaria(OfertaInmobiliaria ofertaInmobiliaria) {
        this.ofertaInmobiliaria = ofertaInmobiliaria;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((areaUsoExclusivo == null) ? 0 : areaUsoExclusivo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OfertaAreaUsoExclusivo other = (OfertaAreaUsoExclusivo) obj;
        if (areaUsoExclusivo == null) {
            if (other.areaUsoExclusivo != null) {
                return false;
            }
        } else if (!areaUsoExclusivo.equals(other.areaUsoExclusivo)) {
            return false;
        }
        return true;
    }

}
