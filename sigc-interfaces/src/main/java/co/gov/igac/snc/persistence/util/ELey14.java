package co.gov.igac.snc.persistence.util;

/**
 *
 * @author franz.gamba
 *
 */
public enum ELey14 {

    LEY_14("L");

    private String codigo;

    private ELey14(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
