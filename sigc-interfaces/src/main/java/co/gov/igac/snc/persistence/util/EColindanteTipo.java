package co.gov.igac.snc.persistence.util;

/**
 * Enumeración para los tipos de predios colindantes
 *
 * @author franz.gamba
 *
 */
public enum EColindanteTipo {
    PREDIO("PREDIO"),
    VIA("VIA");

    private String codigo;

    private EColindanteTipo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
