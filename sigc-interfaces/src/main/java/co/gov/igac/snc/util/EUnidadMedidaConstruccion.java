package co.gov.igac.snc.util;

/**
 * Enumeración para el dominio OFERTA_UNIDAD_AREA
 *
 * @author ariel.ortiz
 *
 * @modified rodrigo.hernandez - adición de atributo valor y de datos segun dominio
 * OFERTA_UNIDAD_AREA
 */
public enum EUnidadMedidaConstruccion {

    METRO_LINEAL("METRO LINEAL", "Metro lineal"),
    METRO_CUADRADO("METRO CUADRADO", "Metro cuadrado"),
    METRO_CUBICO("METRO CUBICO", "Metro cúbico");

    private String codigo;
    private String valor;

    private EUnidadMedidaConstruccion(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
