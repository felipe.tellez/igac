package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/*
 * MODIFICACIONES PROPIAS A LA CLASE PUnidadConstruccion:
 *
 * @author fabio.navarrete Se agregó serialVersionUID @author fabio.navarrete Se modificó el tipo de
 * dato de usoId por una referencia al objeto UsoConstruccion
 *
 * @author pedro.garcia Cambio de tipo de datos de Byte(estúpido generador de myeclipse) a Integer
 * para los campos totalBanios, totalHabitaciones, totalLocales, totalPisosUnidad,
 * totalPisosConstruccion
 *
 * @author fabio.navarrete Se agregó el método y atributo transient para consultar el valor de la
 * propiedad cancelaInscribe
 *
 * @author pedro.garcia Adición de la secuencia generadora del id
 *
 * @modified david.cifuentes :: Adición del atributo booleano 'selected' y los métodos transient
 * isSelected, y setSelected :: 18/07/12
 *
 * @modified david.cifuentes :: Se implementa la interfaz Cloneable y se agrega método clone ::
 * 21/09/12
 *
 * @modified david.cifuentes :: Adición del atributo transient booleano 'pertenecePredioProyectado'
 * y sus respectivos get y set :: 21/11/2013
 *
 * @modified leidy.gonzalez :: Adición del atributo anioCancelacion y sus respectivos get y set ::
 * 22/05/2014 @modified david.cifuentes :: AdiciÃ³n del atributo originalTramite y sus respectivos
 * get y set :: 16/09/2015
 */
@Entity
@Table(name = "P_UNIDAD_CONSTRUCCION", schema = "SNC_CONSERVACION")
public class PUnidadConstruccion
    implements java.io.Serializable, IProyeccionObject, IModeloUnidadConstruccion, Comparable,
    Cloneable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 1453718648776931199L;

    private Long id;
    private PPredio PPredio;
    private UsoConstruccion usoConstruccion;
    private String unidad;
    private Long calificacionAnexoId;
    private String tipoConstruccion;
    private String descripcion;
    private String observaciones;
    private String tipoCalificacion;
    private Double totalPuntaje;
    private String tipificacion;
    private Integer anioConstruccion;
    private Double areaConstruida;
    private Double avaluo;
    private Integer totalBanios;
    private Integer totalHabitaciones;
    private Integer totalLocales;
    private Integer totalPisosUnidad;
    private Integer totalPisosConstruccion;
    private Double valorM2Construccion;
    private String pisoUbicacion;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private List<PUnidadConstruccionComp> PUnidadConstruccionComps =
        new ArrayList<PUnidadConstruccionComp>();
    private String tipoDominio;
    private Date fechaInscripcionCatastral;
    private String cancelaInscribeValor;
    private String tipificacionValor;
    private Predio provienePredio;
    private String provieneUnidad;
    private Integer anioCancelacion;
    private boolean selected;

    /**
     * Marca los predios iniciales de un tramite, solo aplica para tramites de condiciones
     * especiales {@link ETramiteRadicacionEspecial}
     */
    private String originalTramite;

    //Transient
    private List<PUnidadConstruccionComp> punidadConstruccionCompsConv;
    private List<PUnidadConstruccionComp> punidadConstruccionCompsNoConv;

    private boolean pertencePredioProyectado;

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getCancelaInscribeValor()
     */
    @Transient
    @Override
    public String getCancelaInscribeValor() {
        return this.cancelaInscribeValor;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setCancelaInscribeValor(java.lang.String)
     */
    @Override
    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTipificacionValor()
     */
    @Transient
    @Override
    public String getTipificacionValor() {
        return this.tipificacionValor;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTipificacionValor(java.lang.String)
     */
    @Override
    public void setTipificacionValor(String tipificacionValor) {
        this.tipificacionValor = tipificacionValor;
    }

    // Constructors
    /** default constructor */
    public PUnidadConstruccion() {
    }

    /** minimal constructor */
    public PUnidadConstruccion(Long id, PPredio PPredio, String unidad,
        UsoConstruccion usoConstruccion,
        Long calificacionAnexoId, String tipoConstruccion, String tipoCalificacion,
        Double totalPuntaje, String tipificacion,
        Integer anioConstruccion, Double areaConstruida, Double avaluo,
        Integer totalPisosUnidad, Integer totalPisosConstruccion, Date fechaInscripcionCatastral,
        String cancelaInscribe, String tipoDominio) {
        this.id = id;
        this.PPredio = PPredio;
        this.usoConstruccion = usoConstruccion;
        this.unidad = unidad;
        this.calificacionAnexoId = calificacionAnexoId;
        this.tipoConstruccion = tipoConstruccion;
        this.tipoCalificacion = tipoCalificacion;
        this.totalPuntaje = totalPuntaje;
        this.tipificacion = tipificacion;
        this.anioConstruccion = anioConstruccion;
        this.areaConstruida = areaConstruida;
        this.avaluo = avaluo;
        this.totalPisosUnidad = totalPisosUnidad;
        this.totalPisosConstruccion = totalPisosConstruccion;
        this.cancelaInscribe = cancelaInscribe;
        this.tipoDominio = tipoDominio;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    /** full constructor */
    public PUnidadConstruccion(Long id, PPredio PPredio,
        UsoConstruccion usoConstruccion, String unidad,
        Long calificacionAnexoId, String tipoConstruccion, String descripcion,
        String observaciones, String tipoCalificacion, Double totalPuntaje,
        String tipificacion, Integer anioConstruccion,
        Double areaConstruida, Double avaluo, Integer totalBanios,
        Integer totalHabitaciones, Integer totalLocales, Integer totalPisosUnidad,
        Integer totalPisosConstruccion, Double valorM2Construccion,
        String pisoUbicacion, String cancelaInscribe, String usuarioLog,
        Date fechaLog, List<PUnidadConstruccionComp> PUnidadConstruccionComps,
        String tipoDominio, Predio provienePredio, String provieneUnidad, Integer anioCancelacion) {
        this.id = id;
        this.PPredio = PPredio;
        this.usoConstruccion = usoConstruccion;
        this.unidad = unidad;
        this.calificacionAnexoId = calificacionAnexoId;
        this.tipoConstruccion = tipoConstruccion;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.tipoCalificacion = tipoCalificacion;
        this.totalPuntaje = totalPuntaje;
        this.tipificacion = tipificacion;
        this.anioConstruccion = anioConstruccion;
        this.areaConstruida = areaConstruida;
        this.avaluo = avaluo;
        this.totalBanios = totalBanios;
        this.totalHabitaciones = totalHabitaciones;
        this.totalLocales = totalLocales;
        this.totalPisosUnidad = totalPisosUnidad;
        this.totalPisosConstruccion = totalPisosConstruccion;
        this.valorM2Construccion = valorM2Construccion;
        this.pisoUbicacion = pisoUbicacion;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.PUnidadConstruccionComps = PUnidadConstruccionComps;
        this.tipoDominio = tipoDominio;
        this.provienePredio = provienePredio;
        this.provieneUnidad = provieneUnidad;
        this.anioCancelacion = anioCancelacion;

    }

    public PUnidadConstruccion(PUnidadConstruccion PUnidadConstruccion) {
        this.anioConstruccion = PUnidadConstruccion.getAnioConstruccion();
        this.areaConstruida = PUnidadConstruccion.getAreaConstruida();
        this.avaluo = PUnidadConstruccion.getAvaluo();
        this.calificacionAnexoId = PUnidadConstruccion.getCalificacionAnexoId();
        this.cancelaInscribe = PUnidadConstruccion.getCancelaInscribe();
        this.descripcion = PUnidadConstruccion.getDescripcion();
        this.fechaInscripcionCatastral = PUnidadConstruccion.getFechaInscripcionCatastral();
        this.fechaLog = PUnidadConstruccion.getFechaLog();
        this.id = PUnidadConstruccion.getId();
        this.observaciones = PUnidadConstruccion.getObservaciones();
        this.pisoUbicacion = PUnidadConstruccion.getPisoUbicacion();
        this.PPredio = PUnidadConstruccion.getPPredio();
        this.PUnidadConstruccionComps = PUnidadConstruccion.getPUnidadConstruccionComps();
        this.tipificacion = PUnidadConstruccion.getTipificacion();
        this.tipoCalificacion = PUnidadConstruccion.getTipoCalificacion();
        this.tipoConstruccion = PUnidadConstruccion.getTipoConstruccion();
        this.tipoDominio = PUnidadConstruccion.getTipoDominio();
        this.totalBanios = PUnidadConstruccion.getTotalBanios();
        this.totalHabitaciones = PUnidadConstruccion.getTotalHabitaciones();
        this.totalLocales = PUnidadConstruccion.getTotalLocales();
        this.totalPisosConstruccion = PUnidadConstruccion.getTotalPisosConstruccion();
        this.totalPisosUnidad = PUnidadConstruccion.getTotalPisosUnidad();
        this.totalPuntaje = PUnidadConstruccion.getTotalPuntaje();
        this.unidad = PUnidadConstruccion.getUnidad();
        this.usoConstruccion = PUnidadConstruccion.getUsoConstruccion();
        this.usuarioLog = PUnidadConstruccion.getUsuarioLog();
        this.anioCancelacion = PUnidadConstruccion.getAnioCancelacion();
    }

    public PUnidadConstruccion(UnidadConstruccion PUnidadConstruccion, String usuario) {
        this.anioConstruccion = PUnidadConstruccion.getAnioConstruccion();
        this.areaConstruida = PUnidadConstruccion.getAreaConstruida();
        this.avaluo = PUnidadConstruccion.getAvaluo();
        this.calificacionAnexoId = PUnidadConstruccion.getCalificacionAnexoId();
        this.cancelaInscribe = EProyeccionCancelaInscribe.INSCRIBE.getCodigo();
        this.descripcion = PUnidadConstruccion.getDescripcion();
        this.fechaInscripcionCatastral = PUnidadConstruccion.getFechaInscripcionCatastral();
        this.fechaLog = PUnidadConstruccion.getFechaLog();
        this.id = null;
        this.observaciones = PUnidadConstruccion.getObservaciones();
        this.pisoUbicacion = PUnidadConstruccion.getPisoUbicacion();
        //this.PPredio = PUnidadConstruccion.getPPredio();
        this.PUnidadConstruccionComps = new ArrayList<PUnidadConstruccionComp>();
        for (UnidadConstruccionComp ucc : PUnidadConstruccion.getUnidadConstruccionComps()) {
            this.PUnidadConstruccionComps.add(new PUnidadConstruccionComp(ucc, usuario));
        }
        this.tipificacion = PUnidadConstruccion.getTipificacion();
        this.tipoCalificacion = PUnidadConstruccion.getTipoCalificacion();
        this.tipoConstruccion = PUnidadConstruccion.getTipoConstruccion();
        this.tipoDominio = PUnidadConstruccion.getTipoDominio();
        this.totalBanios = PUnidadConstruccion.getTotalBanios();
        this.totalHabitaciones = PUnidadConstruccion.getTotalHabitaciones();
        this.totalLocales = PUnidadConstruccion.getTotalLocales();
        this.totalPisosConstruccion = PUnidadConstruccion.getTotalPisosConstruccion();
        this.totalPisosUnidad = PUnidadConstruccion.getTotalPisosUnidad();
        this.totalPuntaje = PUnidadConstruccion.getTotalPuntaje();
        this.unidad = PUnidadConstruccion.getUnidad();
        this.usoConstruccion = PUnidadConstruccion.getUsoConstruccion();
        this.usuarioLog = usuario;
        this.fechaLog = new Date();
        this.anioCancelacion = PUnidadConstruccion.getAnioCancelacion();

    }
    // Property accessors

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "generador_id_P_UNIDAD_CONSTRUCCION")
    @SequenceGenerator(name = "generador_id_P_UNIDAD_CONSTRUCCION",
        sequenceName = "UNIDAD_CONSTRUCCION_ID_SEQ", allocationSize = 0)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getUsoConstruccion()
     */
    @Override
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USO_ID", nullable = true)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setUsoConstruccion(co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion)
     */
    @Override
    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    /*
     * @see co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getUnidad()
     */
    @Override
    @Column(name = "UNIDAD", nullable = false, length = 50)
    public String getUnidad() {
        return this.unidad;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setUnidad(java.lang.String)
     */
    @Override
    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getCalificacionAnexoId()
     */
    @Override
    @Column(name = "CALIFICACION_ANEXO_ID", precision = 10, scale = 0)
    public Long getCalificacionAnexoId() {
        return this.calificacionAnexoId;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setCalificacionAnexoId(java.lang.Long)
     */
    @Override
    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.calificacionAnexoId = calificacionAnexoId;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTipoConstruccion()
     */
    @Override
    @Column(name = "TIPO_CONSTRUCCION", nullable = false, length = 30)
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTipoConstruccion(java.lang.String)
     */
    @Override
    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getDescripcion()
     */
    @Override
    @Column(name = "DESCRIPCION", length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setDescripcion(java.lang.String)
     */
    @Override
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getObservaciones()
     */
    @Override
    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setObservaciones(java.lang.String)
     */
    @Override
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTipoCalificacion()
     */
    @Override
    @Column(name = "TIPO_CALIFICACION", nullable = false, length = 30)
    public String getTipoCalificacion() {
        return this.tipoCalificacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTipoCalificacion(java.lang.String)
     */
    @Override
    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTotalPuntaje()
     */
    @Override
    @Column(name = "TOTAL_PUNTAJE", nullable = false, precision = 10)
    public Double getTotalPuntaje() {
        return this.totalPuntaje;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTotalPuntaje(java.lang.Double)
     */
    @Override
    public void setTotalPuntaje(Double totalPuntaje) {
        this.totalPuntaje = totalPuntaje;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTipificacion()
     */
    @Override
    @Column(name = "TIPIFICACION", nullable = true, length = 30)
    public String getTipificacion() {
        return this.tipificacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTipificacion(java.lang.String)
     */
    @Override
    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getAnioConstruccion()
     */
    @Override
    @Column(name = "ANIO_CONSTRUCCION", nullable = true, precision = 4, scale = 0)
    public Integer getAnioConstruccion() {
        return this.anioConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setAnioConstruccion(java.lang.Short)
     */
    @Override
    public void setAnioConstruccion(Integer anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getAreaConstruida()
     */
    @Override
    @Column(name = "AREA_CONSTRUIDA", nullable = false, precision = 18)
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setAreaConstruida(java.lang.Double)
     */
    @Override
    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    /*
     * @see co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getAvaluo()
     */
    @Override
    @Column(name = "AVALUO", nullable = false, precision = 18)
    public Double getAvaluo() {
        return this.avaluo;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setAvaluo(java.lang.Double)
     */
    @Override
    public void setAvaluo(Double avaluo) {
        this.avaluo = avaluo;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTotalBanios()
     */
    @Override
    @Column(name = "TOTAL_BANIOS", precision = 2, scale = 0)
    public Integer getTotalBanios() {
        return this.totalBanios;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTotalBanios(java.lang.Integer)
     */
    @Override
    public void setTotalBanios(Integer totalBanios) {
        this.totalBanios = totalBanios;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTotalHabitaciones()
     */
    @Override
    @Column(name = "TOTAL_HABITACIONES", precision = 2, scale = 0)
    public Integer getTotalHabitaciones() {
        return this.totalHabitaciones;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTotalHabitaciones(java.lang.Integer)
     */
    @Override
    public void setTotalHabitaciones(Integer totalHabitaciones) {
        this.totalHabitaciones = totalHabitaciones;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTotalLocales()
     */
    @Override
    @Column(name = "TOTAL_LOCALES", precision = 2, scale = 0)
    public Integer getTotalLocales() {
        return this.totalLocales;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTotalLocales(java.lang.Integer)
     */
    @Override
    public void setTotalLocales(Integer totalLocales) {
        this.totalLocales = totalLocales;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTotalPisosUnidad()
     */
    @Override
    @Column(name = "TOTAL_PISOS_UNIDAD", nullable = false, precision = 2, scale = 0)
    public Integer getTotalPisosUnidad() {
        return this.totalPisosUnidad;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTotalPisosUnidad(java.lang.Integer)
     */
    @Override
    public void setTotalPisosUnidad(Integer totalPisosUnidad) {
        this.totalPisosUnidad = totalPisosUnidad;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTotalPisosConstruccion()
     */
    @Override
    @Column(name = "TOTAL_PISOS_CONSTRUCCION", nullable = false, precision = 2, scale = 0)
    public Integer getTotalPisosConstruccion() {
        return this.totalPisosConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTotalPisosConstruccion(java.lang.Integer)
     */
    @Override
    public void setTotalPisosConstruccion(Integer totalPisosConstruccion) {
        this.totalPisosConstruccion = totalPisosConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getValorM2Construccion()
     */
    @Override
    @Column(name = "VALOR_M2_CONSTRUCCION", precision = 18)
    public Double getValorM2Construccion() {
        return this.valorM2Construccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setValorM2Construccion(java.lang.Double)
     */
    @Override
    public void setValorM2Construccion(Double valorM2Construccion) {
        this.valorM2Construccion = valorM2Construccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getPisoUbicacion()
     */
    @Override
    @Column(name = "PISO_UBICACION", length = 30)
    public String getPisoUbicacion() {
        return this.pisoUbicacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setPisoUbicacion(java.lang.String)
     */
    @Override
    public void setPisoUbicacion(String pisoUbicacion) {
        this.pisoUbicacion = pisoUbicacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getCancelaInscribe()
     */
    @Override
    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setCancelaInscribe(java.lang.String)
     */
    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getPUnidadConstruccionComps()
     */
    @Override
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PUnidadConstruccion")
    public List<PUnidadConstruccionComp> getPUnidadConstruccionComps() {
        return this.PUnidadConstruccionComps;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setPUnidadConstruccionComps(java.util.List)
     */
    @Override
    public void setPUnidadConstruccionComps(
        List<PUnidadConstruccionComp> PUnidadConstruccionComps) {
        this.PUnidadConstruccionComps = PUnidadConstruccionComps;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getTipoDominio()
     */
    @Override
    @Column(name = "TIPO_DOMINIO", nullable = false, length = 30)
    public String getTipoDominio() {
        return this.tipoDominio;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setTipoDominio(java.lang.String)
     */
    @Override
    public void setTipoDominio(String tipoDominio) {
        this.tipoDominio = tipoDominio;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getFechaInscripcionCatastral()
     */
    @Override
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setFechaInscripcionCatastral(java.util.Date)
     */
    @Override
    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#getAnioConstruccion()
     */
    @Override
    @Column(name = "ANIO_CANCELACION", nullable = true, precision = 4, scale = 0)
    public Integer getAnioCancelacion() {
        return anioCancelacion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion#setAnioCancelacion(java.util.Date)
     */
    @Override
    public void setAnioCancelacion(Integer anioCancelacion) {
        this.anioCancelacion = anioCancelacion;
    }
    // -------------------------------------------------------------------------

    @Transient
    public List<PUnidadConstruccionComp> getPunidadConstruccionCompsConv() {

        if (PUnidadConstruccionComps != null &&
            !PUnidadConstruccionComps.isEmpty()) {
            punidadConstruccionCompsConv = new ArrayList<PUnidadConstruccionComp>();

            for (PUnidadConstruccionComp ucc : PUnidadConstruccionComps) {

                if (ucc.getPUnidadConstruccion() != null &&
                    ucc.getPUnidadConstruccion()
                        .getTipoConstruccion()
                        .equals(EUnidadTipoConstruccion.CONVENCIONAL
                            .getCodigo())) {

                    punidadConstruccionCompsConv.add(ucc);
                }
            }
        }

        return punidadConstruccionCompsConv;
    }

    @Transient
    public List<PUnidadConstruccionComp> getPunidadConstruccionCompsNoConv() {

        if (PUnidadConstruccionComps != null &&
            !PUnidadConstruccionComps.isEmpty()) {
            punidadConstruccionCompsNoConv = new ArrayList<PUnidadConstruccionComp>();

            for (PUnidadConstruccionComp ucc : PUnidadConstruccionComps) {

                if (ucc.getPUnidadConstruccion() != null &&
                    ucc.getPUnidadConstruccion()
                        .getTipoConstruccion()
                        .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL
                            .getCodigo())) {

                    punidadConstruccionCompsNoConv.add(ucc);
                }
            }
        }

        return punidadConstruccionCompsNoConv;
    }

    // -------------------------------------------------------------------//
    /**
     * Método que retorna si está o no seleccionada la PUnidadConstruccion
     */
    @Transient
    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    // -------------------------------------------------------------------//
    /**
     * Método que retorna si la PUnidadConstruccion pertenece al predio inicial de la proyección, el
     * atributo pertencePredioProyectado debe setearse al realizar el cargue del predio.
     */
    @Transient
    public boolean isPertenecePredioProyectado() {
        return this.pertencePredioProyectado;
    }

    public void setPertenecePredioProyectado(boolean pertenece) {
        this.pertencePredioProyectado = pertenece;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof PUnidadConstruccion) {
            PUnidadConstruccion puc = (PUnidadConstruccion) o;
            if (this.unidad != null && puc.unidad != null) {
                return this.unidad.compareTo(puc.unidad);
            } else {
                return 0;
            }
        }
        return 0;
    }

    @Column(name = "PROVIENE_UNIDAD", nullable = true, length = 50)
    public String getProvieneUnidad() {
        return this.provieneUnidad;
    }

    public void setProvieneUnidad(String provieneUnidad) {
        this.provieneUnidad = provieneUnidad;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVIENE_PREDIO_ID", nullable = true)
    public Predio getProvienePredio() {
        return this.provienePredio;
    }

    public void setProvienePredio(Predio provienePredio) {
        this.provienePredio = provienePredio;
    }

    @Column(name = "ORIGINAL_TRAMITE")
    public String getOriginalTramite() {
        return this.originalTramite;
    }

    public void setOriginalTramite(String originalTramite) {
        this.originalTramite = originalTramite;
    }

    /**
     * @author david.cifuentes Método que realiza un clone del objeto PUnidadConstruccion
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    /**
     * Sobrecarga del método equals para la comparación de {@link PUnidadConstruccion} basado en su id y su unidad
     *
     * @author david.cifuentes
     */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PUnidadConstruccion)) {
            return false;
        }
        PUnidadConstruccion castOther = (PUnidadConstruccion) o;
        return (this.id != null && this.id.equals(castOther.getId()) &&
                this.unidad != null && this.unidad.equals(castOther.
                getUnidad()));
    }

}
