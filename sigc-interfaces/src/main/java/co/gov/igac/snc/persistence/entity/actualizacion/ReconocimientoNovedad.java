package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the RECONOCIMIENTO_NOVEDAD database table.
 *
 */
@Entity
@Table(name = "RECONOCIMIENTO_NOVEDAD")
public class ReconocimientoNovedad implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private BigDecimal actualizacionLevantamientoId;
    private BigDecimal actualizacionOrdenAjusteId;
    private String digitalizarNuevasAreas;
    private Date fechaLog;
    private String observaciones;
    private String usuarioLog;
    private ActualizacionReconocimiento actualizacionReconocimiento;
    private RecursoHumano recursoHumano;

    public ReconocimientoNovedad() {
    }

    @Id
    @SequenceGenerator(name = "RECONOCIMIENTO_NOVEDAD_ID_GENERATOR", sequenceName =
        "RECONOCIMIENTO_NOVEDAD_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "RECONOCIMIENTO_NOVEDAD_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTUALIZACION_LEVANTAMIENTO_ID")
    public BigDecimal getActualizacionLevantamientoId() {
        return this.actualizacionLevantamientoId;
    }

    public void setActualizacionLevantamientoId(BigDecimal actualizacionLevantamientoId) {
        this.actualizacionLevantamientoId = actualizacionLevantamientoId;
    }

    @Column(name = "ACTUALIZACION_ORDEN_AJUSTE_ID")
    public BigDecimal getActualizacionOrdenAjusteId() {
        return this.actualizacionOrdenAjusteId;
    }

    public void setActualizacionOrdenAjusteId(BigDecimal actualizacionOrdenAjusteId) {
        this.actualizacionOrdenAjusteId = actualizacionOrdenAjusteId;
    }

    @Column(name = "DIGITALIZAR_NUEVAS_AREAS")
    public String getDigitalizarNuevasAreas() {
        return this.digitalizarNuevasAreas;
    }

    public void setDigitalizarNuevasAreas(String digitalizarNuevasAreas) {
        this.digitalizarNuevasAreas = digitalizarNuevasAreas;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionReconocimiento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_RECO_ID")
    public ActualizacionReconocimiento getActualizacionReconocimiento() {
        return this.actualizacionReconocimiento;
    }

    public void setActualizacionReconocimiento(
        ActualizacionReconocimiento actualizacionReconocimiento) {
        this.actualizacionReconocimiento = actualizacionReconocimiento;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID")
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

}
