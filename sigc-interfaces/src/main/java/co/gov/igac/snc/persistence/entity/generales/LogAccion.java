package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the LOG_ACCION database table.
 *
 */
@Entity
@Table(name = "LOG_ACCION")
public class LogAccion implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private long LogAccesoId;
    private Date fecha;
    private String evento;
    private String eventoMensaje;
    private String programa;
    private String datos;

    public LogAccion() {
    }

    public LogAccion(long LogAccesoId, String evento, String eventoMensaje, String programa,
        String datos) {
        this.LogAccesoId = LogAccesoId;
        this.evento = evento;
        this.eventoMensaje = eventoMensaje;
        this.programa = programa;
        this.datos = datos;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOG_ACCION_ID_SEQ")
    @SequenceGenerator(name = "LOG_ACCION_ID_SEQ", sequenceName = "LOG_ACCION_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "LOG_ACCESO_ID")
    public long getLogAccesoId() {
        return LogAccesoId;
    }

    public void setLogAccesoId(long LogAccesoId) {
        this.LogAccesoId = LogAccesoId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO_SESION")
    public Date getFecha() {
        return fecha;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "EVENTO_MENSAJE")
    public String getEventoMensaje() {
        return eventoMensaje;
    }

    public void setEventoMensaje(String eventoMensaje) {
        this.eventoMensaje = eventoMensaje;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

}
