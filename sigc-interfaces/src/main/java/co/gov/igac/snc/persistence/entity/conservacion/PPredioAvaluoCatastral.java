package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/*
 * MODIFICACIONES PROPIAS A LA CLASE PPredioAvaluoCatastral: @author fabio.navarrete Se agregó
 * serialVersionUID @author fabio.navarrete Se agregó el método y atributo transient para consultar
 * el valor de la propiedad cancelaInscribe
 */
@Entity
@Table(name = "P_PREDIO_AVALUO_CATASTRAL", schema = "SNC_CONSERVACION")
public class PPredioAvaluoCatastral implements java.io.Serializable, IProyeccionObject,
    IInscripcionCatastral {

    /**
     *
     */
    private static final long serialVersionUID = 6527359715540515982L;
    // Fields

    private Long id;
    private PPredio PPredio;
    private String autoavaluo;
    private String justificacionAvaluo;
    private String notaAvaluo;
    private Double valorTotalAvaluoCatastral;
    private Double valorTotalAvaluoVigencia;
    private Double valorTerreno;
    private Double valorTerrenoPrivado;
    private Double valorTerrenoComun;
    private Double valorTotalConstruccionPrivada;
    private Double valorConstruccionComun;
    private Double valorTotalConstruccion;
    private Double valorTotalConsConvencional;
    private Double valorTotalConsNconvencional;
    private Date vigencia;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private Date fechaInscripcionCatastral;
    private String cancelaInscribeValor;

    // Constructors
    /** default constructor */
    public PPredioAvaluoCatastral() {
    }

    /** minimal constructor */
    public PPredioAvaluoCatastral(Long id, PPredio PPredio, String autoavaluo,
        Double valorTotalAvaluoCatastral, Double valorTerreno,
        Double valorTotalConstruccion, Double valorTotalConsConvencional,
        Date vigencia, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.autoavaluo = autoavaluo;
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
        this.valorTerreno = valorTerreno;
        this.valorTotalConstruccion = valorTotalConstruccion;
        this.valorTotalConsConvencional = valorTotalConsConvencional;
        this.vigencia = vigencia;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PPredioAvaluoCatastral(Long id, PPredio PPredio, String autoavaluo,
        String justificacionAvaluo, String notaAvaluo,
        Double valorTotalAvaluoCatastral, Double valorTerreno,
        Double valorTerrenoPrivado, Double valorTerrenoComun,
        Double valorTotalConstruccionPrivada, Double valorConstruccionComun,
        Double valorTotalConstruccion, Double valorTotalConsConvencional,
        Double valorTotalConsNconvencional, Date vigencia, Date fechaInscripcionCatastral,
        String cancelaInscribe, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.autoavaluo = autoavaluo;
        this.justificacionAvaluo = justificacionAvaluo;
        this.notaAvaluo = notaAvaluo;
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
        this.valorTerreno = valorTerreno;
        this.valorTerrenoPrivado = valorTerrenoPrivado;
        this.valorTerrenoComun = valorTerrenoComun;
        this.valorTotalConstruccionPrivada = valorTotalConstruccionPrivada;
        this.valorConstruccionComun = valorConstruccionComun;
        this.valorTotalConstruccion = valorTotalConstruccion;
        this.valorTotalConsConvencional = valorTotalConsConvencional;
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
        this.vigencia = vigencia;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREDIO_AVALUO_CATASTRAL_ID_SEQ")
    @GenericGenerator(name = "PREDIO_AVALUO_CATASTRAL_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {@Parameter(
                name = "sequence", value = "PREDIO_AVALUO_CATASTRAL_ID_SEQ"), @Parameter(name =
                "allocationSize", value = "1")})
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "AUTOAVALUO", nullable = false, length = 2)
    public String getAutoavaluo() {
        return this.autoavaluo;
    }

    public void setAutoavaluo(String autoavaluo) {
        /**
         * Lo siguiente es necesario porque al hacer post por ajax desde web no pasa por el
         * converter y llegan esas cadenas
         */
        if (autoavaluo != null && autoavaluo.equals("true")) {
            autoavaluo = ESiNo.SI.getCodigo();
        }
        if (autoavaluo != null && autoavaluo.equals("false")) {
            autoavaluo = ESiNo.NO.getCodigo();
        }
        this.autoavaluo = autoavaluo;
    }

    @Column(name = "JUSTIFICACION_AVALUO", length = 600)
    public String getJustificacionAvaluo() {
        return this.justificacionAvaluo;
    }

    public void setJustificacionAvaluo(String justificacionAvaluo) {
        this.justificacionAvaluo = justificacionAvaluo;
    }

    @Column(name = "NOTA_AVALUO", length = 600)
    public String getNotaAvaluo() {
        return this.notaAvaluo;
    }

    public void setNotaAvaluo(String notaAvaluo) {
        this.notaAvaluo = notaAvaluo;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL", nullable = false, precision = 18)
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_VIGENCIA", nullable = false, precision = 18)
    public Double getValorTotalAvaluoVigencia() {
        return valorTotalAvaluoVigencia;
    }

    public void setValorTotalAvaluoVigencia(Double valorTotalAvaluoVigencia) {
        this.valorTotalAvaluoVigencia = valorTotalAvaluoVigencia;
    }

    @Column(name = "VALOR_TOTAL_TERRENO", nullable = false, precision = 18)
    public Double getValorTerreno() {
        return this.valorTerreno;
    }

    public void setValorTerreno(Double valorTerreno) {
        this.valorTerreno = valorTerreno;
    }

    @Column(name = "VALOR_TERRENO_PRIVADO", nullable = false, precision = 18)
    public Double getValorTerrenoPrivado() {
        return this.valorTerrenoPrivado;
    }

    public void setValorTerrenoPrivado(Double valorTerrenoPrivado) {
        this.valorTerrenoPrivado = valorTerrenoPrivado;
    }

    @Column(name = "VALOR_TERRENO_COMUN", nullable = false, precision = 18)
    public Double getValorTerrenoComun() {
        return this.valorTerrenoComun;
    }

    public void setValorTerrenoComun(Double valorTerrenoComun) {
        this.valorTerrenoComun = valorTerrenoComun;
    }

    @Column(name = "VALOR_TOTAL_CONS_PRIVADA", nullable = false, precision = 18)
    public Double getValorTotalConstruccionPrivada() {
        return this.valorTotalConstruccionPrivada;
    }

    public void setValorTotalConstruccionPrivada(Double valorTotalConstruccionPrivada) {
        this.valorTotalConstruccionPrivada = valorTotalConstruccionPrivada;
    }

    @Column(name = "VALOR_CONSTRUCCION_COMUN", nullable = false, precision = 18)
    public Double getValorConstruccionComun() {
        return this.valorConstruccionComun;
    }

    public void setValorConstruccionComun(Double valorConstruccionComun) {
        this.valorConstruccionComun = valorConstruccionComun;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCION", nullable = false, precision = 18)
    public Double getValorTotalConstruccion() {
        return this.valorTotalConstruccion;
    }

    public void setValorTotalConstruccion(Double valorTotalConstruccion) {
        this.valorTotalConstruccion = valorTotalConstruccion;
    }

    @Column(name = "VALOR_CONS_CONVENCIONAL", nullable = false, precision = 18)
    public Double getValorTotalConsConvencional() {
        return this.valorTotalConsConvencional;
    }

    public void setValorTotalConsConvencional(Double valorTotalConsConvencional) {
        this.valorTotalConsConvencional = valorTotalConsConvencional;
    }

    @Column(name = "VALOR_CONS_NO_CONVENCIONAL", precision = 18)
    public Double getValorTotalConsNconvencional() {
        return this.valorTotalConsNconvencional;
    }

    public void setValorTotalConsNconvencional(
        Double valorTotalConsNconvencional) {
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "VIGENCIA", nullable = false, length = 7)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

}
