/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles movimientos realizados sobre un avalúo
 *
 * @author felipe.cadena
 */
public enum EMovimientoAvaluoTipo {

    AMPLIACION("AMPLIACION"),
    SUSPENCION("SUSPENCION"),
    REACTIVACION("REACTIVACION"),
    CANCELACION("CANCELACION");

    private String codigo;

    private EMovimientoAvaluoTipo(String codigoP) {
        this.codigo = codigoP;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
