package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.util.Utilidades;

/**
 * PredioZona entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "PREDIO_ZONA", schema = "SNC_CONSERVACION")
public class PredioZona implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2536835878073148222L;

    // Fields
    @GeneratedValue
    private Long id;
    private Predio predio;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private Double valorM2Terreno;
    private Double area;
    private Double avaluo;
    private Date vigencia;
    private String usuarioLog;
    private Date fechaLog;

    private Date fechaInscripcionCatastral;

    // Constructors
    /** default constructor */
    public PredioZona() {
    }

    /** full constructor */
    public PredioZona(Long id, Predio predio, String zonaFisica,
        String zonaGeoeconomica, Double valorM2Terreno, Double area,
        Double avaluo, Date vigencia, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.zonaFisica = zonaFisica;
        this.zonaGeoeconomica = zonaGeoeconomica;
        this.valorM2Terreno = valorM2Terreno;
        this.area = area;
        this.avaluo = avaluo;
        this.vigencia = vigencia;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "ZONA_FISICA", nullable = false, length = 4)
    public String getZonaFisica() {
        return this.zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

    @Column(name = "ZONA_GEOECONOMICA", nullable = false, length = 4)
    public String getZonaGeoeconomica() {
        return this.zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    @Column(name = "VALOR_M2_TERRENO", nullable = false, precision = 12)
    public Double getValorM2Terreno() {
        return this.valorM2Terreno;
    }

    public void setValorM2Terreno(Double valorM2Terreno) {
        this.valorM2Terreno = valorM2Terreno;
    }

    @Column(name = "AREA", nullable = false, precision = 18, scale = 2)
    public Double getArea() {
        return this.area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Column(name = "AVALUO", nullable = false, precision = 18)
    public Double getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Double avaluo) {
        this.avaluo = avaluo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "VIGENCIA", nullable = false, length = 7)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }
    
    /**
     * Implementado para comparar predioZona
     */
    public boolean equals(Object o) {
        if (o instanceof PredioZona) {
            PredioZona p = (PredioZona) o;
            return this.id.equals(p.getId());
        }
        return false;
    }
}
