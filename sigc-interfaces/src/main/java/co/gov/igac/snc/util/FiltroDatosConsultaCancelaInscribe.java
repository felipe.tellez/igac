package co.gov.igac.snc.util;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;

public class FiltroDatosConsultaCancelaInscribe {

    private PPredio[] predios;
    private Dominio[] acciones;
    private String[] secciones;

    public FiltroDatosConsultaCancelaInscribe() {
    }

    public PPredio[] getPredios() {
        return predios;
    }

    public void setPredios(PPredio[] predios) {
        this.predios = predios;
    }

    public Dominio[] getAcciones() {
        return acciones;
    }

    public void setAcciones(Dominio[] acciones) {
        this.acciones = acciones;
    }

    public String[] getSecciones() {
        return secciones;
    }

    public void setSecciones(String[] secciones) {
        this.secciones = secciones;
    }

}
