package co.gov.igac.snc.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DivisionAdministrativaDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1390218831495919541L;

    private String divisionAdministrativa;

    private List<TotalPredialDTO> datos = new ArrayList<TotalPredialDTO>();

    public DivisionAdministrativaDTO(String divisionAdministrativa) {
        super();
        this.divisionAdministrativa = divisionAdministrativa;
    }

    public String getDivisionAdministrativa() {
        return divisionAdministrativa;
    }

    public void setDivisionAdministrativa(String divisionAdministrativa) {
        this.divisionAdministrativa = divisionAdministrativa;
    }

    public List<TotalPredialDTO> getDatos() {
        return datos;
    }

    public TotalPredialDTO getDato(String zona) {
        for (TotalPredialDTO d : this.datos) {
            if (d.getZona().equals(zona)) {
                return d;
            }
        }
        return null;
    }

    public void setDatos(List<TotalPredialDTO> datos) {
        this.datos = datos;
    }

    public BigDecimal getTotalPredios() {
        BigDecimal p = new BigDecimal(0);
        for (TotalPredialDTO d : this.datos) {
            p = p.add(d.getTotalPredios());
        }
        return p;
    }

    public BigDecimal getTotalPropietarios() {
        BigDecimal p = new BigDecimal(0);
        for (TotalPredialDTO d : this.datos) {
            p = p.add(d.getTotalPropietarios());
        }
        return p;
    }

    public BigDecimal getTotalAreaTerreno() {
        BigDecimal p = new BigDecimal(0);
        for (TotalPredialDTO d : this.datos) {
            p = p.add(d.getAreaTerreno());
        }
        return p;
    }

    public BigDecimal getTotalAreaConstruccion() {
        BigDecimal p = new BigDecimal(0);
        for (TotalPredialDTO d : this.datos) {
            p = p.add(d.getAreaConstruccion());
        }
        return p;
    }

    public BigDecimal getTotalAvaluos() {
        BigDecimal p = new BigDecimal(0);
        for (TotalPredialDTO d : this.datos) {
            p = p.add(d.getTotalAvaluos());
        }
        return p;
    }

}
