package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the ACTUALIZACION_RECONOCIMIENTO database table.
 *
 */
@Entity
@Table(name = "ACTUALIZACION_RECONOCIMIENTO")
public class ActualizacionReconocimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fecha;
    private Date fechaLog;
    private String manzanaCodigo;
    private String tipo;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private RecursoHumano recursoHumano;
    private List<ReconocimientoNovedad> reconocimientoNovedads;
    private List<ReconocimientoPredio> reconocimientoPredios;

    public ActualizacionReconocimiento() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_RECONOCIMIENTO_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_SEDE_COMI_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_RECONOCIMIENTO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MANZANA_CODIGO")
    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID")
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

    //bi-directional many-to-one association to ReconocimientoNovedad
    @OneToMany(mappedBy = "actualizacionReconocimiento")
    public List<ReconocimientoNovedad> getReconocimientoNovedads() {
        return this.reconocimientoNovedads;
    }

    public void setReconocimientoNovedads(List<ReconocimientoNovedad> reconocimientoNovedads) {
        this.reconocimientoNovedads = reconocimientoNovedads;
    }

    //bi-directional many-to-one association to ReconocimientoPredio
    @OneToMany(mappedBy = "actualizacionReconocimiento")
    public List<ReconocimientoPredio> getReconocimientoPredios() {
        return this.reconocimientoPredios;
    }

    public void setReconocimientoPredios(List<ReconocimientoPredio> reconocimientoPredios) {
        this.reconocimientoPredios = reconocimientoPredios;
    }

}
