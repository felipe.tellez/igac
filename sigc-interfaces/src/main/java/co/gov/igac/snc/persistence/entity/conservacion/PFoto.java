package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

import javax.persistence.CascadeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * PFoto entity.
 */

/*
 * MODIFICACIONES PROPIAS A LA CLASE PFoto: @author fabio.navarrete Se agregó serialVersionUID
 * @author fabio.navarrete Se agregó el método y atributo transient para consultar el valor de la
 * propiedad cancelaInscribe @author juan.agudelo 05/06/11 modifico fotoDocumentoId por el objeto
 * Documento
 *
 * @author pedro.garcia - 23-06-2011 adición del attributo cascade a la anotación onetoone del
 * atributo Documento - adición de namedQueries - OJO: descripción de campos
 */
@Entity
@NamedQueries({
    @NamedQuery(
        name = "findByUnidadConstruccionIdFetchDocumento",
        query = "from PFoto pf join fetch pf.documento where pf.PUnidadConstruccion.id = :ucIdParam"),
    @NamedQuery(
        name = "deleteByUnidadConstruccionId",
        query = "delete from PFoto pf where pf.PUnidadConstruccion.id = :ucIdParam"),
    @NamedQuery(
        name = "findByUnidadConstruccionId",
        query = "from PFoto pf where pf.PUnidadConstruccion.id = :ucIdParam")
})
@Table(name = "P_FOTO", schema = "SNC_CONSERVACION")
public class PFoto implements java.io.Serializable, IProyeccionObject {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -398027908239424321L;

    private Long id;
    private PPredio PPredio;
    private PUnidadConstruccion PUnidadConstruccion;
    private Long tramiteId;
    private String tipo;

    /**
     * es por este campo que se van a distinguir las fotos de diferentes componentes de la misma
     * unidad de construcción
     */
    private String descripcion;

    private Date fecha;
    private Documento documento;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;

    private String cancelaInscribeValor;

    // Constructors
    /** default constructor */
    public PFoto() {
    }

    /** minimal constructor */
    public PFoto(Long id, PPredio PPredio, String tipo, Date fecha,
        Documento documento, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.tipo = tipo;
        this.fecha = fecha;
        this.documento = documento;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PFoto(Long id, PPredio PPredio, PUnidadConstruccion unidadConstruccionId,
        Long tramiteId, String tipo, String descripcion, Date fecha,
        Documento documento, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.PUnidadConstruccion = unidadConstruccionId;
        this.tramiteId = tramiteId;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.documento = documento;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FOTO_ID_SEQ")
    @SequenceGenerator(name = "FOTO_ID_SEQ", sequenceName = "FOTO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIDAD_CONSTRUCCION_ID", nullable = true)
    public PUnidadConstruccion getPUnidadConstruccion() {
        return this.PUnidadConstruccion;
    }

    public void setPUnidadConstruccion(PUnidadConstruccion unidadConstruccionId) {
        this.PUnidadConstruccion = unidadConstruccionId;
    }

    @Column(name = "TRAMITE_ID", precision = 10, scale = 0)
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "DESCRIPCION", length = 2000)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA", nullable = false, length = 7)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY
    //, targetEntity=co.gov.igac.snc.persistence.entity.generales.Documento.class
    )
    @JoinColumn(name = "FOTO_DOCUMENTO_ID", nullable = false)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    @Override
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    @Override
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    @Override
    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Override
    public Date getFechaLog() {
        return this.fechaLog;
    }

    @Override
    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

}
