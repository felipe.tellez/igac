package co.gov.igac.snc.persistence.util;

/**
 * Enumeración para listar los objetos para las comisiones de un proceso de actualización
 *
 * @author franz.gamba
 *
 */
public enum EActualizacionComisionObjeto {
    COMISION_RECORRIDO_PRELIMINAR("Comision de recorrido preliminiar al Municipio ");

    private String codigo;

    private EActualizacionComisionObjeto(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
