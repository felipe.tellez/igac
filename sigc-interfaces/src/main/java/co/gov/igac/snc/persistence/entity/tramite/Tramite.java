package co.gov.igac.snc.persistence.entity.tramite;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionada;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EComisionTramiteDatoDato;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacion5Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudFormaPeticion;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteProceso;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.ETramiteTipoInscripcion;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tramite entity.
 */
/*
 * FGWL Cambio para que traiga con EAGER la Solicitud FGWL Cambio de Long predioId por Predio predio
 * ======= MODIFICACIONES PROPIAS A LA CLASE Tramite: -> FGWL Cambio para que traiga con EAGER la
 * Solicitud -> Secuencia -> Se agregó el serialVersionUID. -> Se agregó el Named Query:
 * findTramiteByNoRadicacion
 *
 * -> @author juan.agudelo 23-05-12 Se modificó el tipo del campo comisionTramites de Set a List. ->
 * Se adicionan los transient para las descripciones de estado y tipo de trámite. No se usaron POJOS
 * porque son de Dominio
 *
 * @modified by: pedro.garcia. what?: - adición método constructor - Cambio de tipo de dato
 * java.util.Date -> sql.Timestamp - adición de named queries - adición de método transient
 * getTipoTramiteCompleto - adición de propiedad, getters y setters transient -> tipoTramiteValor -
 * adición de propiedad, getters y setters transient -> subtipoValor - adición de propiedad, getters
 * y setters transient -> claseMutacionValor ESTAS 3 SE LLENAN EN LA INTERFAZ QUE CONSULTE LOS
 * TRÁMITES - adición de propiedad, getters y setters transient -> duracionTramite - cambio de tipo
 * de dato para los atributos areaTerreno, areaConstruccion, autoavaluoTerreno,
 * autoavaluoConstruccion de BigDecimal a Double - remoción del cascade.all para el atributo
 * comisionTramites - adición de método transient isEsRectificacion 28-05-2013 adición de campo
 * transient 'numeroComisionesHaEstado' 18-06-2013 adición de campo transient 'ejecutorDepuracion'
 * 17-10-2013 adición de método transient 'isViaGubernativa'
 *
 * @modified by: fabio.navarrete: -> Se modificaron todos los Set por List. -> Se agrega getter
 * transient isTramiteDeAutoavaluo. -> Se agrega getter transient getPredios. -> Se agrega método
 * transient isTipoInscripcionPH -> Se agrega método transient isTipoInscripcionCondominio -> Se
 * agregan todos los getter Transient para los tipos de trámites.
 *
 * @modified by: javier.aponte -> Adición de propiedad, getters y setters transient ->
 * fechaDocumento -> Cambio en un constructor de pedro.garcia porque dos campos ya no existen en la
 * tabla Tramite los campos son resolucionResultado y fechaResolucionResultado se cambiaron por
 * resultadoDocumentoId y por el atributo transient fechaDocumento que es de la tabla
 * Tramite_Documento respectivamente. -> Se agregá el método transient isMutacion -> Se agregá el
 * método transient isQuinta -> Se agregá el método transient isSegunda
 *
 * @modified by: juan.agudelo 05-09-2011 -> Se agrega la entidad tramitePruebas @modified by:
 * david.cifuentes -> Se modifica el metodo transient getEstadoDocumentos para que utilice las
 * consantes - (05-09-2011) -> Se modifica el metodo transient getDocumentacionAdicional y
 * getDocumentacionRequerida para validar que no cargue documentacion requerida adicional en ambas
 * listas - (15-09-2011) @modified by: juan.agudelo 22-09-11 -> se agrega el método transient
 * tramiteDocumentacionsTemp @modified by: javier.aponte 22-09-2011 -> Se agregá el método transient
 * isTramiteComisionado @modified by: david.cifuentes 29-09-2011 -> Se agregó atributo
 * documentacionCompleta con su getter y setter por adición de esta columna en la base de datos.
 * @modified by: david.cifuentes 04-10-2011 -> Se modificó el atributo resultadoDocumentoId de tipo
 * Long a resultadoDocumento de tipo Documento, se actualizaron los getters, setters y
 * constructores. @modified by: franz.gamba 01-11-2011 -> Se agrega el metodo transient
 * isSeleccionAprobar @modified by: - javier.aponte se cambia la anotacion @Temporal de fecha de
 * radicación de TemporalType.DATE a TemporalType.TIMESTAMP @modified by: david.cifuentes 01-10-2012
 * -> Adición de las variables actividadActualTramite, usuarioActividadTramite y sus respectivos
 * métodos transient. Adición del método transient getNumeroResolucion que me retorna el numero de
 * resolución del resultadoDocumento si este existe. @modified by: david.cifuentes 12-06-2013 ->
 * Adición de la variable tramiteDepuracions. @modified by: david.cifuentes 26/06/2013 -> Adición de
 * la variable fechaAsignacionActividadActual y sus respectivos métodos transient. @modified by:
 * david.cifuentes 12/08/2013 -> Mapeo de la variable tramiteInconsistencias y sus respectivos get y
 * set. @modified ?? adición de campo transient seleccionAprobar @modified javier.aponte se agrega
 * campo formaPeticion para los trámites que se van a marcar como derecho de petición o tutela
 * 13/08/2013 @modified felipe.cadena :: se agrega el campo readicacionEspecial :: 08-05-2015
 * @modified by: david.cifuentes 18-08-2015 -> Se adiciona el método transient isQuintaMasivo.
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findTramiteByNoRadicacion",
        query = "from Tramite t where t.numeroRadicacion= :numeroRadicacion"),
    @NamedQuery(name = "updateEstadoByIdTerritorial",
        query = "update Tramite t " +
        " set t.estado = :tramEstadoNew " +
        " where t.estado = :tramEstadoAnt " +
        " and exists (" +
        "select j.id from Jurisdiccion j join j.municipio m " +
        " where j.estructuraOrganizacional.codigo = :territorialCod and " +
        "m.codigo = t.municipio.codigo)"),
    @NamedQuery(name = "updateEstadoByIdTerritorialAndClasif", query = "update Tramite t " +
        " set t.estado = :tramEstadoNew " +
        " where t.estado = :tramEstadoAnt and t.clasificacion = :tramClasif " +
        " and exists (" +
        "select j.id from Jurisdiccion j join j.municipio m " +
        " where j.estructuraOrganizacional.codigo = :territorialCod and " +
        "m.codigo = t.municipio.codigo)")})
@Table(name = "TRAMITE", schema = "SNC_TRAMITE", uniqueConstraints = @UniqueConstraint(columnNames =
    "NUMERO_RADICACION"))
public class Tramite implements java.io.Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = 4262970936756309656L;

    // Fields
    private Long id;
    private String archivado = ESiNo.NO.getCodigo();
    private String cargoFuncionarioEjecutor;
    private String claseMutacion;
    private String clasificacion;
    private String comisionado;
    private Departamento departamento;
    private String documentacionCompleta;
    private String estado;
    private Date fechaInscripcionCatastral;
    private Date fechaRadicacion;
    private String fuente;
    private String funcionarioEjecutor;
    private String funcionarioRadicador;
    private String fundamentoPeticion;
    private String mantieneNumeroPredial;
    private String marcoJuridico;
    private Municipio municipio;
    private String nombreFuncionarioEjecutor;
    private String nombreFuncionarioRadicador;
    private String numeroRadicacion;
    private String numeroRadicacionReferencia;
    private String observacionesRadicacion;
    private Integer ordenEjecucion;
    private Predio predio;
    private String procesoInstanciaId;
    private String proyeccionAsociada;
    private String resolucionContraProcede;
    private Documento resultadoDocumento;
    private Solicitud solicitud;
    private String subtipo;
    private String tipoInscripcion;
    private String tipoTramite;
    private Tramite tramite;
    private String omitidoNuevo;
    private String estadoGdb;
    private String formaPeticion;

    /**
     * OJO: estos campos se deben usar solo para trámites de quinta o autoavalúo!!!
     */
    private Double areaConstruccion;
    private Double areaTerreno;
    private Double autoavaluoConstruccion;
    private Double autoavaluoTerreno;
    private String nombrePredio;
    private String numeroPredial;
    private String resumen;
    private String ubicacionPredio;
    private TramitePrueba tramitePruebas;

    private String usuarioLog;
    private Date fechaLog;
    private Integer manzanasNuevas;

    private List<ComisionTramite> comisionTramites = new ArrayList<ComisionTramite>();
    private List<SolicitanteTramite> solicitanteTramites = new ArrayList<SolicitanteTramite>();
    private List<TramiteAvisoRegistro> tramiteAvisoRegistros = new ArrayList<TramiteAvisoRegistro>();
    private List<TramiteDocumento> tramiteDocumentos = new ArrayList<TramiteDocumento>();
    private List<TramiteDocumentacion> tramiteDocumentacions = new ArrayList<TramiteDocumentacion>();
    private List<TramiteEstado> tramiteEstados = new ArrayList<TramiteEstado>();
    private List<TramitePredioAvaluo> tramitePredioAvaluos = new ArrayList<TramitePredioAvaluo>();
    private List<TramitePredioEnglobe> tramitePredioEnglobes = new ArrayList<TramitePredioEnglobe>();
    private List<TramiteRectificacion> tramiteRectificacions = new ArrayList<TramiteRectificacion>();
    private List<TramiteRelacionTramite> tramiteRelacionTramitesHijos;
    private List<TramiteRelacionTramite> tramiteRelacionTramitesPadres;
    private List<Tramite> tramites = new ArrayList<Tramite>();
    private List<TramiteTextoResolucion> tramiteTextoResolucions =
        new ArrayList<TramiteTextoResolucion>();

    private List<TramiteDepuracion> tramiteDepuracions;
    private List<TramiteInconsistencia> tramiteInconsistencias =
        new ArrayList<TramiteInconsistencia>();
    private List<TramiteDigitalizacion> tramiteDigitalizacions =
        new ArrayList<TramiteDigitalizacion>();
    private List<TramiteInfoAdicional> tramiteInfoAdicionals = new ArrayList<TramiteInfoAdicional>();
    private List<PFichaMatrizPredioTerreno> fichaMatrizPredioTerrenos =
        new ArrayList<PFichaMatrizPredioTerreno>();
    private final List<Predio> predios = new ArrayList<Predio>();

    // TODO:: el creador de estos atribueos:: 09-04-12:: no se sabe a que
    // corresponden estos campos, son transient??? no presentan relación directa
    // en la DB :: juan.agudelo
    private String estadoDescripcion;
    private String tipoTramiteDescripcion;

    /* private String tipoTramiteValor; private String subtipoValor; private String
     * claseMutacionValor; */
    private Double duracionTramite;
    private Date fechaDocumento;

    /**
     * campo transient para manejar la selección de trámites en tablas con checkbox por fila
     */
    private boolean seleccionAprobar;

    /**
     * @author pedro.garcia cambios transient para mantener el constructor que necesito en un select
     * new
     */
    Long predioId;
    Long resultadoDocumentoId;

    //Transient
    /**
     * @author juan.agudelo retorna el tipo de trámite completo en una cadena
     */
    public String tipoTramiteCadenaCompleto;

    //Transient
    /**
     * @author franz.gamba almacena los documentos de opruebas para la pestaña pruebas aportadas
     */
    public List<TramiteDocumento> documentosPruebas;

    /**
     * Transient - Variables para la consulta del estado actual en process del trámite,su actividad
     * y el usuario que la reclamo. Las siguientes variables se deben instanciar en el bean que las
     * use, pues está información se encuentra en process y no en la base de datos.
     *
     * @author david.cifuentes
     */
    private Actividad actividadActualTramite;
    private UsuarioDTO usuarioActividadTramite;
    private Date fechaAsignacionActividadActual;

    private InfInterrelacionada infInterrelacionada;

    /**
     * @author fredy.wilches Campo pasado de la solicitud al trámite
     */
    private Date fechaRadicacionTesoreria;

    /**
     * Campo transient que dice el número de comisiones efectivas (que han llegado hasta
     * 'finalizada') en las que ha estado este trámite
     */
    private int numeroComisionesHaEstado;

    /**
     * campos transient que almacenan el nombre y el login del ejecutor o topógrafo del trámite de
     * depuración al que está asociado este trámite. Se crearon para poder usar las mismas consultas
     * para la parte de comisiones de trámites
     */
    private String nombreEjecutorDepuracion;
    private String ejecutorDepuracion;

    /**
     * Campo transient para almacenar el rol del usuario actual que tiene el trámite, esta
     * información se obtiene de process y se asigna en este campo para mostrarla en la pantalla de
     * búsqueda de tramite
     *
     * @author juanfelipe.garcia
     */
    private String nombreRolUsuarioActual;

    /**
     * Campo transient para almacenar la última razón de rechazo depuración
     *
     * @author juanfelipe.garcia
     */
    private String ultimaRazonRechazoDepuracion;

    /**
     * Atributo transient para verificar si un trámite es o no proveniente del proceso de
     * depuración.
     */
    private String tramiteProvenienteDepuracion;

    /**
     * @author felipe.cadena Campos de control para los proceso complejos asincronicos relacionados
     * al tramite
     */
    private String tarea;
    private String paso;
    private String resultado;

    private String oficinaCodigo;

    /**
     * @author felipe.cadena Campo para los tramites de comportamientos especiales, como los
     * desenglobes por etapas
     */
    private String radicacionEspecial;

    /**
     * @author felipe.cadena Campo para identificar tramites que vienen de diferentes procesos como
     * actualizacion y conservacion
     */
    private String proceso;

    /**
     * @author felipe.cadena Campo para identificar tramites que vienen de diferentes procesos como
     * actualizacion y conservacion
     */
    private Tramite tramiteActualizacionAsociado;

    /**
     * @author leidy.gonzalez Campo para identificar tramites ya les fueron generadas la resolucion
     * en la pantalla de Generar Resolucion
     */
    private String estadoResolucion;

    /**
     * @author felipe.cadena Campo transient para determinar si la actividad del tramite esta
     * reclamada se debe instanciar manualmente donde se quiera utilizar.
     */
    private boolean reclamado;

    /**
     * @author dumar.penuela Campo para determinar el estado del tramite dependiendo del bloqueo de
     * predios
     */
    private String estadoBloqueo;

    //Transient
    /**
     * @author juan.cruz identifica si el tramite ha sido seleccionado para ser cancelado pruebas
     * aportadas
     */
    private boolean porCancelar;

    private static final Logger LOGGER = LoggerFactory.getLogger(Tramite.class);

    // Constructors
    /** default constructor */
    public Tramite() {
        this.fechaRadicacion = new Date();
    }

    /** minimal constructor */
    public Tramite(Long id, Solicitud solicitud, String tipoTramite,
        String estado, String archivado, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.solicitud = solicitud;
        this.tipoTramite = tipoTramite;
        this.estado = estado;
        this.archivado = archivado;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaRadicacion = new Date();
    }

    /**
     * ********************* NO BORRAR ******************************
     *
     * @author pedro.garcia Se usa en un query. Tiene todos los campos que son obligatorios y los
     * que se necesitan mostrar
     */
    public Tramite(Long id, Long predioId, String tipoTramite,
        String numeroRadicacion, String estado, String claseMutacion,
        String subtipo, Long resultadoDocumentoId, Date fechaDocumento,
        String archivado, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predioId = predioId;
        this.tipoTramite = tipoTramite;
        this.numeroRadicacion = numeroRadicacion;
        this.estado = estado;
        this.claseMutacion = claseMutacion;
        this.subtipo = subtipo;
        this.resultadoDocumentoId = resultadoDocumentoId;
        this.fechaDocumento = fechaDocumento;
        this.archivado = archivado;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaRadicacion = new Date();
    }

    /** full constructor */
    public Tramite(Long id, Solicitud solicitud, Tramite tramite,
        Predio predio, String fuente, String tipoTramite,
        String numeroRadicacion, Date fechaRadicacion,
        String funcionarioRadicador, String nombreFuncionarioRadicador,
        String observacionesRadicacion, String estado,
        String resolucionContraProcede, String fundamentoPeticion,
        String funcionarioEjecutor, String nombreFuncionarioEjecutor,
        String claseMutacion, String subtipo, String tipoInscripcion,
        String clasificacion, String proyeccionAsociada, String archivado,
        String marcoJuridico, String revisionSubsidioImpugnacion,
        String tipoImpugnacion, String numeroRadicacionReferencia,
        String tipoSolicitud, String procesoInstanciaId,
        Date fechaInscripcionCatastral, Documento resultadoDocumento,
        String usuarioLog, Date fechaLog,
        List<TramiteDocumento> tramiteDocumentos,
        List<TramitePredioAvaluo> tramitePredioAvaluos,
        List<TramiteDocumentacion> tramiteDocumentacions,
        List<Tramite> tramites,
        List<TramiteTextoResolucion> tramiteTextoResolucions,
        List<TramiteRectificacion> tramiteRectificacions,
        List<SolicitanteTramite> solicitanteTramites,
        List<TramiteAvisoRegistro> tramiteAvisoRegistros,
        List<ComisionTramite> comisionTramites,
        List<TramiteEstado> tramiteEstados, TramitePrueba tramitePruebas,
        List<TramitePredioEnglobe> tramitePredioEnglobes,
        String cargoFuncionarioEjecutor, Integer ordenEjecucion,
        Departamento departamento, Municipio municipio,
        String documentacionCompleta, String mantieneNumeroPredial, String formaPeticion,
        String omitidoNuevo, String estadoGdb) {
        this.id = id;
        this.archivado = archivado;
        this.solicitud = solicitud;
        this.tramite = tramite;
        this.predio = predio;
        this.fuente = fuente;
        this.tipoTramite = tipoTramite;
        this.numeroRadicacion = numeroRadicacion;
        this.fechaRadicacion = fechaRadicacion;
        this.funcionarioRadicador = funcionarioRadicador;
        this.nombreFuncionarioRadicador = nombreFuncionarioRadicador;
        this.observacionesRadicacion = observacionesRadicacion;
        this.estado = estado;
        this.tramiteTextoResolucions = tramiteTextoResolucions;
        this.resolucionContraProcede = resolucionContraProcede;
        this.fundamentoPeticion = fundamentoPeticion;
        this.funcionarioEjecutor = funcionarioEjecutor;
        this.nombreFuncionarioEjecutor = nombreFuncionarioEjecutor;
        this.claseMutacion = claseMutacion;
        this.subtipo = subtipo;
        this.tipoInscripcion = tipoInscripcion;
        this.clasificacion = clasificacion;
        this.proyeccionAsociada = proyeccionAsociada;
        this.marcoJuridico = marcoJuridico;
        this.numeroRadicacionReferencia = numeroRadicacionReferencia;
        this.procesoInstanciaId = procesoInstanciaId;
        this.departamento = departamento;
        this.municipio = municipio;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.resultadoDocumento = resultadoDocumento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.tramiteDocumentos = tramiteDocumentos;
        this.tramitePredioAvaluos = tramitePredioAvaluos;
        this.tramiteDocumentacions = tramiteDocumentacions;
        this.tramites = tramites;
        this.tramiteRectificacions = tramiteRectificacions;
        this.solicitanteTramites = solicitanteTramites;
        this.tramiteAvisoRegistros = tramiteAvisoRegistros;
        this.comisionTramites = comisionTramites;
        this.tramiteEstados = tramiteEstados;
        this.tramitePredioEnglobes = tramitePredioEnglobes;
        this.tramitePruebas = tramitePruebas;
        this.cargoFuncionarioEjecutor = cargoFuncionarioEjecutor;
        this.ordenEjecucion = ordenEjecucion;
        this.documentacionCompleta = documentacionCompleta;
        this.mantieneNumeroPredial = mantieneNumeroPredial;
        this.omitidoNuevo = omitidoNuevo;
        this.estadoGdb = estadoGdb;
        this.formaPeticion = formaPeticion;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_ID_SEQ", sequenceName = "TRAMITE_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASOCIADO_TRAMITE_ID")
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "FUENTE", length = 30)
    public String getFuente() {
        return this.fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @Column(name = "ESTADO_GDB", length = 30, nullable = true)
    public String getEstadoGdb() {
        return this.estadoGdb;
    }

    public void setEstadoGdb(String estadoGdb) {
        this.estadoGdb = estadoGdb;
    }

    @Column(name = "TIPO_TRAMITE", nullable = false, length = 30)
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "NUMERO_RADICACION", unique = true, length = 20)
    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    @Column(name = "FECHA_RADICACION", length = 7, nullable = false)
    //	@Temporal(javax.persistence.TemporalType.DATE)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaRadicacion() {
        return this.fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    @Column(name = "FUNCIONARIO_RADICADOR", length = 100)
    public String getFuncionarioRadicador() {
        return this.funcionarioRadicador;
    }

    public void setFuncionarioRadicador(String funcionarioRadicador) {
        this.funcionarioRadicador = funcionarioRadicador;
    }

    @Column(name = "NOMBRE_FUNCIONARIO_RADICADOR", length = 100)
    public String getNombreFuncionarioRadicador() {
        return this.nombreFuncionarioRadicador;
    }

    public void setNombreFuncionarioRadicador(String nombreFuncionarioRadicador) {
        this.nombreFuncionarioRadicador = nombreFuncionarioRadicador;
    }

    @Column(name = "OBSERVACIONES_RADICACION", length = 2000)
    public String getObservacionesRadicacion() {
        return this.observacionesRadicacion;
    }

    public void setObservacionesRadicacion(String observacionesRadicacion) {
        this.observacionesRadicacion = observacionesRadicacion;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    /**
     * Recibe una entidad {@link TramiteEstado TramiteEstado} que ya debe estar persistida con el
     * cambio de estado del tramite, si el tramites estado no tiene id se lanza una excepcion
     *
     * @author felipe.cadena
     * @param estado
     */
    public void setTramiteEstado(TramiteEstado estado) throws Exception {
        if (estado.getId() != null) {
            this.estado = estado.getEstado();
        } else {
            throw new Exception("Se debe persistir la entidad tramite estado relacionada al tramite");
        }

    }

    /**
     *
     * @param estado
     * @deprecated :: felipe.cadena:: Se debe usar el metodo
     * {@link Tramite#setTramiteEstado(co.gov.igac.snc.persistence.entity.tramite.TramiteEstado)  setTramiteEstado()}
     * para mantener la trazabilidad de los cambios de estado de los tramites
     */
    @Deprecated
    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "RESOLUCION_CONTRA_PROCEDE", length = 20)
    public String getResolucionContraProcede() {
        return this.resolucionContraProcede;
    }

    public void setResolucionContraProcede(String resolucionContraProcede) {
        this.resolucionContraProcede = resolucionContraProcede;
    }

    @Column(name = "FUNDAMENTO_PETICION", length = 250)
    public String getFundamentoPeticion() {
        return this.fundamentoPeticion;
    }

    public void setFundamentoPeticion(String fundamentoPeticion) {
        this.fundamentoPeticion = fundamentoPeticion;
    }

    @Column(name = "FUNCIONARIO_EJECUTOR", length = 100)
    public String getFuncionarioEjecutor() {
        return this.funcionarioEjecutor;
    }

    public void setFuncionarioEjecutor(String funcionarioEjecutor) {
        this.funcionarioEjecutor = funcionarioEjecutor;
    }

    @Column(name = "NOMBRE_FUNCIONARIO_EJECUTOR", length = 100)
    public String getNombreFuncionarioEjecutor() {
        return this.nombreFuncionarioEjecutor;
    }

    public void setNombreFuncionarioEjecutor(String nombreFuncionarioEjecutor) {
        this.nombreFuncionarioEjecutor = nombreFuncionarioEjecutor;
    }

    @Column(name = "CLASE_MUTACION", length = 30)
    public String getClaseMutacion() {
        return this.claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    @Column(name = "SUBTIPO", length = 30)
    public String getSubtipo() {
        return this.subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    @Column(name = "TIPO_INSCRIPCION", length = 30)
    public String getTipoInscripcion() {
        return this.tipoInscripcion;
    }

    public void setTipoInscripcion(String tipoInscripcion) {
        this.tipoInscripcion = tipoInscripcion;
    }

    @Column(name = "CLASIFICACION", length = 30)
    public String getClasificacion() {
        return this.clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    @Column(name = "PROYECCION_ASOCIADA", length = 15)
    public String getProyeccionAsociada() {
        return this.proyeccionAsociada;
    }

    public void setProyeccionAsociada(String proyeccionAsociada) {
        this.proyeccionAsociada = proyeccionAsociada;
    }

    @Column(name = "ARCHIVADO", nullable = false, length = 2)
    public String getArchivado() {
        return this.archivado;
    }

    public void setArchivado(String archivado) {
        this.archivado = archivado;
    }

    @Column(name = "MARCO_JURIDICO", length = 30)
    public String getMarcoJuridico() {
        return this.marcoJuridico;
    }

    public void setMarcoJuridico(String marcoJuridico) {
        this.marcoJuridico = marcoJuridico;
    }

    @Column(name = "NUMERO_RADICACION_REFERENCIA", length = 20)
    public String getNumeroRadicacionReferencia() {
        return this.numeroRadicacionReferencia;
    }

    public void setNumeroRadicacionReferencia(String numeroRadicacionReferencia) {
        this.numeroRadicacionReferencia = numeroRadicacionReferencia;
    }

    @Column(name = "PROCESO_INSTANCIA_ID", length = 100)
    public String getProcesoInstanciaId() {
        return this.procesoInstanciaId;
    }

    public void setProcesoInstanciaId(String procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RESULTADO_DOCUMENTO_ID")
    public Documento getResultadoDocumento() {
        return this.resultadoDocumento;
    }

    public void setResultadoDocumento(Documento resultadoDocumento) {
        this.resultadoDocumento = resultadoDocumento;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MANZANAS_NUEVAS")
    public Integer getManzanasNuevas() {
        return manzanasNuevas;
    }

    public void setManzanasNuevas(Integer manzanasNuevas) {
        this.manzanasNuevas = manzanasNuevas;
    }

    @Column(name = "COMISIONADO", length = 2)
    public String getComisionado() {
        return this.comisionado;
    }

    public void setComisionado(String com) {
        this.comisionado = com;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteDocumento> getTramiteDocumentos() {
        return this.tramiteDocumentos;
    }

    public void setTramiteDocumentos(List<TramiteDocumento> tramiteDocumentos) {
        this.tramiteDocumentos = tramiteDocumentos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramitePredioAvaluo> getTramitePredioAvaluos() {
        return this.tramitePredioAvaluos;
    }

    public void setTramitePredioAvaluos(
        List<TramitePredioAvaluo> tramitePredioAvaluos) {
        this.tramitePredioAvaluos = tramitePredioAvaluos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteTextoResolucion> getTramiteTextoResolucions() {
        return this.tramiteTextoResolucions;
    }

    public void setTramiteTextoResolucions(
        List<TramiteTextoResolucion> tramiteTextoResolucions) {
        this.tramiteTextoResolucions = tramiteTextoResolucions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteDocumentacion> getTramiteDocumentacions() {
        return this.tramiteDocumentacions;
    }

    public void setTramiteDocumentacions(
        List<TramiteDocumentacion> tramiteDocumentacions) {
        this.tramiteDocumentacions = tramiteDocumentacions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<Tramite> getTramites() {
        return this.tramites;
    }

    public void setTramites(List<Tramite> tramites) {
        this.tramites = tramites;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteRectificacion> getTramiteRectificacions() {
        return this.tramiteRectificacions;
    }

    public void setTramiteRectificacions(
        List<TramiteRectificacion> tramiteRectificacions) {
        this.tramiteRectificacions = tramiteRectificacions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<SolicitanteTramite> getSolicitanteTramites() {
        return this.solicitanteTramites;
    }

    public void setSolicitanteTramites(
        List<SolicitanteTramite> solicitanteTramites) {
        this.solicitanteTramites = solicitanteTramites;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteAvisoRegistro> getTramiteAvisoRegistros() {
        return this.tramiteAvisoRegistros;
    }

    public void setTramiteAvisoRegistros(
        List<TramiteAvisoRegistro> tramiteAvisoRegistros) {
        this.tramiteAvisoRegistros = tramiteAvisoRegistros;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tramite", cascade = CascadeType.MERGE)
    public List<ComisionTramite> getComisionTramites() {
        return this.comisionTramites;
    }

    public void setComisionTramites(List<ComisionTramite> comisionTramites) {
        this.comisionTramites = comisionTramites;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteEstado> getTramiteEstados() {
        return this.tramiteEstados;
    }

    public void setTramiteEstados(List<TramiteEstado> tramiteEstados) {
        this.tramiteEstados = tramiteEstados;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramitePredioEnglobe> getTramitePredioEnglobes() {
        return this.tramitePredioEnglobes;
    }

    public void setTramitePredioEnglobes(
        List<TramitePredioEnglobe> tramitePredioEnglobes) {
        this.tramitePredioEnglobes = tramitePredioEnglobes;
    }

    @Column(name = "FORMA_PETICION")
    public String getFormaPeticion() {
        return formaPeticion;
    }

    public void setFormaPeticion(String formaPeticion) {
        this.formaPeticion = formaPeticion;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AUTOAVALUO_TERRENO")
    public Double getAutoavaluoTerreno() {
        return this.autoavaluoTerreno;
    }

    public void setAutoavaluoTerreno(Double autoavaluoTerreno) {
        this.autoavaluoTerreno = autoavaluoTerreno;
    }

    @Column(name = "AUTOAVALUO_CONSTRUCCION")
    public Double getAutoavaluoConstruccion() {
        return this.autoavaluoConstruccion;
    }

    public void setAutoavaluoConstruccion(Double autoavaluoConstruccion) {
        this.autoavaluoConstruccion = autoavaluoConstruccion;
    }

    @Column(name = "UBICACION_PREDIO")
    public String getUbicacionPredio() {
        return this.ubicacionPredio;
    }

    public void setUbicacionPredio(String ubicacionPredio) {
        this.ubicacionPredio = ubicacionPredio;
    }

    @Column(name = "NOMBRE_PREDIO")
    public String getNombrePredio() {
        return this.nombrePredio;
    }

    public void setNombrePredio(String nombrePredio) {
        this.nombrePredio = nombrePredio;
    }

    @Column(name = "RESUMEN")
    public String getResumen() {
        return this.resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public TramitePrueba getTramitePruebas() {
        return this.tramitePruebas;
    }

    public void setTramitePruebas(TramitePrueba tramitePruebas) {
        this.tramitePruebas = tramitePruebas;
    }

    @Column(name = "ORDEN_EJECUCION")
    public Integer getOrdenEjecucion() {
        return this.ordenEjecucion;
    }

    public void setOrdenEjecucion(Integer ordenEjecucion) {
        this.ordenEjecucion = ordenEjecucion;
    }

    @Column(name = "CARGO_FUNCIONARIO_EJECUTOR")
    public String getCargoFuncionarioEjecutor() {
        return this.cargoFuncionarioEjecutor;
    }

    public void setCargoFuncionarioEjecutor(String cargoFuncionarioEjecutor) {
        this.cargoFuncionarioEjecutor = cargoFuncionarioEjecutor;
    }

    @Column(name = "DOCUMENTACION_COMPLETA", length = 2)
    public String getDocumentacionCompleta() {
        return this.documentacionCompleta == null ? ESiNo.NO.getCodigo() :
            this.documentacionCompleta;
    }

    public void setDocumentacionCompleta(String documentacionCompleta) {
        this.documentacionCompleta = documentacionCompleta;
    }

    // bi-directional many-to-one association to TramiteRelacionTramite
    @OneToMany(mappedBy = "tramite1")
    public List<TramiteRelacionTramite> getTramiteRelacionTramitesHijos() {
        return this.tramiteRelacionTramitesHijos;
    }

    public void setTramiteRelacionTramitesHijos(
        List<TramiteRelacionTramite> tramiteRelacionTramites1) {
        this.tramiteRelacionTramitesHijos = tramiteRelacionTramites1;
    }

    // bi-directional many-to-one association to TramiteRelacionTramite
    @OneToMany(mappedBy = "tramiteRelacionado")
    public List<TramiteRelacionTramite> getTramiteRelacionTramitesPadres() {
        return this.tramiteRelacionTramitesPadres;
    }

    public void setTramiteRelacionTramitesPadres(
        List<TramiteRelacionTramite> tramiteRelacionTramites2) {
        this.tramiteRelacionTramitesPadres = tramiteRelacionTramites2;
    }

    //bi-directional many-to-one association to TramiteDepuracion
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteDepuracion> getTramiteDepuracions() {
        return this.tramiteDepuracions;
    }

    public void setTramiteDepuracions(List<TramiteDepuracion> tramiteDepuracions) {
        this.tramiteDepuracions = tramiteDepuracions;
    }

    // bi-directional many-to-one association to TramiteDepuracion
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteInconsistencia> getTramiteInconsistencias() {
        return this.tramiteInconsistencias;
    }

    public void setTramiteInconsistencias(
        List<TramiteInconsistencia> tramiteInconsistencias) {
        this.tramiteInconsistencias = tramiteInconsistencias;
    }

    // bi-directional many-to-one association to TramiteDepuracion
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<TramiteDigitalizacion> getTramiteDigitalizacions() {
        return this.tramiteDigitalizacions;
    }

    public void setTramiteDigitalizacions(
        List<TramiteDigitalizacion> tramiteDigitalizacions) {
        this.tramiteDigitalizacions = tramiteDigitalizacions;
    }

    // bi-directional many-to-one association to TramieInfoAdicional
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "tramite")
    public List<TramiteInfoAdicional> getTramiteInfoAdicionals() {
        return this.tramiteInfoAdicionals;
    }

    public void setTramiteInfoAdicionals(
        List<TramiteInfoAdicional> tramiteInfoAdicionals) {
        this.tramiteInfoAdicionals = tramiteInfoAdicionals;
    }

    // bi-directional many-to-one association to FichaMatrizPredioTerreno
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramite")
    public List<PFichaMatrizPredioTerreno> getFichaMatrizPredioTerrenos() {
        return this.fichaMatrizPredioTerrenos;
    }

    public void setFichaMatrizPredioTerrenos(
        List<PFichaMatrizPredioTerreno> fichaMatrizPredioTerreno) {
        this.fichaMatrizPredioTerrenos = fichaMatrizPredioTerreno;
    }

    @Column(name = "FECHA_RADICACION_TESORERIA", length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaRadicacionTesoreria() {
        return this.fechaRadicacionTesoreria;
    }

    public void setFechaRadicacionTesoreria(Date fechaRadicacionTesoreria) {
        this.fechaRadicacionTesoreria = fechaRadicacionTesoreria;
    }

    @Column(name = "ESTADO_BLOQUEO", length = 30)
    public String getEstadoBloqueo() {
        return estadoBloqueo;
    }

    public void setEstadoBloqueo(String estadoBloqueo) {
        this.estadoBloqueo = estadoBloqueo;
    }

    // --------------------------------------------------------------------------------------------------
    @Transient
    public InfInterrelacionada getInfInterrelacionada() {
        return infInterrelacionada;
    }

    public void setInfInterrelacionada(InfInterrelacionada infInterrelacionada) {
        this.infInterrelacionada = infInterrelacionada;
    }

    @Transient
    public String getEstadoDescripcion() {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(String estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }

    @Transient
    public String getTipoTramiteDescripcion() {
        return tipoTramiteDescripcion;
    }

    public void setTipoTramiteDescripcion(String tipoTramiteDescripcion) {
        this.tipoTramiteDescripcion = tipoTramiteDescripcion;
    }

    @Transient
    public List<TramiteDocumentacion> getDocumentacionRequerida() {
        List<TramiteDocumentacion> tds = new ArrayList<TramiteDocumentacion>();
        if (this.tramiteDocumentacions != null && !this.tramiteDocumentacions.isEmpty()) {
            for (TramiteDocumentacion td : this.tramiteDocumentacions) {
                if (td.getRequerido() != null &&
                    td.getRequerido().equals(ESiNo.SI.toString())) {
                    tds.add(td);
                }
            }
        }
        return tds;
    }

    @Transient
    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        List<TramiteDocumentacion> tds = new ArrayList<TramiteDocumentacion>();
        for (TramiteDocumentacion td : this.tramiteDocumentacions) {
            if (td.getAdicional() != null &&
                td.getAdicional().equals(ESiNo.SI.toString()) &&
                td.getRequerido() != null &&
                td.getRequerido().equals(ESiNo.NO.toString())) {
                tds.add(td);
            }
        }
        return tds;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo para poder manejar listas de temporales de documentos aportados para
     * evitar duplicación de documentos
     */
//TODO :: juan.agudelo :: documentar la finalidad de este método. ¿por qué no usar el getter del atributo?
    @Transient
    public List<TramiteDocumentacion> getTramiteDocumentacionsTemp() {
        return this.tramiteDocumentacions;
    }

    public void setTramiteDocumentacionsTemp(
        List<TramiteDocumentacion> tramiteDocumentacions) {
        this.tramiteDocumentacions = tramiteDocumentacions;
    }

    @Transient
    public String getEstadoDocumentos() {
        String estadoDocumentos = Constantes.NO_APLICA;
        int contadorDocIncompleta = 0;

        if (this.tramiteDocumentacions != null) {
            for (TramiteDocumentacion tramiteDocuAux : this.tramiteDocumentacions) {
                if ((tramiteDocuAux.getRequerido() != null && tramiteDocuAux
                    .getRequerido().equals(ESiNo.SI.getCodigo())) &&
                    (tramiteDocuAux.getAportado() == null || tramiteDocuAux
                    .getAportado().equals(ESiNo.NO.getCodigo()))) {
                    contadorDocIncompleta++;
                }
            }

            if (contadorDocIncompleta > 0) {
                estadoDocumentos = Constantes.DOCUMENTACION_ESTADO_INCOMPLETO;
            } else if (contadorDocIncompleta == 0) {
                estadoDocumentos = Constantes.DOCUMENTACION_ESTADO_COMPLETO;
            }
        }
        return estadoDocumentos;
    }

    @Deprecated
    @Transient
    public String getEstadoDocumentacion() {
        String estadoDocumentos = Constantes.NO_APLICA;
        int contadorDocIncompleta = 0;

        if (this.tramiteDocumentacions != null) {
            for (TramiteDocumentacion tramiteDocuAux : this.tramiteDocumentacions) {
                if ((tramiteDocuAux.getRequerido() != null && tramiteDocuAux
                    .getRequerido().equals(ESiNo.SI.getCodigo())) &&
                    (tramiteDocuAux.getAportado() == null || tramiteDocuAux
                    .getAportado().equals(ESiNo.NO.getCodigo()))) {
                    contadorDocIncompleta++;
                }
            }

            if (contadorDocIncompleta > 0) {
                estadoDocumentos = Constantes.DOCUMENTACION_ESTADO_INCOMPLETO;
            } else if (contadorDocIncompleta == 0) {
                estadoDocumentos = Constantes.DOCUMENTACION_ESTADO_COMPLETO;
            }
        }
        return estadoDocumentos;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Transient
    public Double getDuracionTramite() {
        return this.duracionTramite;
    }

    public void setDuracionTramite(Double duracionTramite) {
        this.duracionTramite = duracionTramite;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     */
    @Transient
    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que indica si el tipo de trámite es de mutación
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isMutacion() {

        if (this.tipoTramite != null) {
            if (this.tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si el tipo de trámite es Recurso de reposicion
     *
     * @return
     * @author fredy.wilches
     */
    @Transient
    public boolean isRecursoReposicion() {

        if (tipoTramite != null) {
            if (tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo())) {
                return true;
            }
        }
        return false;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que indica si el subtipo del trámite es de quinta
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isQuinta() {

        if (tipoTramite != null && claseMutacion != null) {
            if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                claseMutacion.equals(EMutacionClase.QUINTA.getCodigo())) {
                return true;
            }
        }
        return false;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que indica si el subtipo del trámite es de segunda
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isSegunda() {
        boolean esSegunda = false;
        if (ETramiteTipoTramite.MUTACION.getCodigo().equals(tipoTramite) &&
            EMutacionClase.SEGUNDA.getCodigo().equals(claseMutacion)) {
            esSegunda = true;
        }
        return esSegunda;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Dice si este trámite es de tipo 'Rectificación'
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isEsRectificacion() {
        boolean answer = false;

        if (this.tipoTramite != null) {
            answer =
                (this.tipoTramite.equalsIgnoreCase(ETramiteTipoTramite.RECTIFICACION.getCodigo())) ?
                true : false;
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Dice si este trámite es de tipo 'Complementacion'
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isEsComplementacion() {
        boolean answer = false;

        if (this.tipoTramite != null) {
            answer = (this.tipoTramite
                .equalsIgnoreCase(ETramiteTipoTramite.COMPLEMENTACION
                    .getCodigo())) ? true : false;
        }

        return answer;
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Determina si un trámite es de Autoavalúo
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isTramiteDeAutoavaluo() {
        if (this.tipoTramite != null) {
            return this.tipoTramite.equals(ETramiteTipoTramite.AUTOESTIMACION
                .getCodigo());
        }
        return false;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Determina si un trámite es de revisión de avalúo
     *
     * @author pedro.garcia
     */
    @Transient
    public boolean isEsTramiteDeRevisionDeAvaluo() {
        if (this.tipoTramite != null) {
            return this.tipoTramite
                .equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo());
        }
        return false;
    }

    /**
     * Determina si un trámite se realiza sobre predios urbanos o rurales
     *
     * @return
     */
    @Transient
    public boolean isTramiteRural() {
        if (this.tramitePredioEnglobes != null &&
            this.tramitePredioEnglobes.size() > 0) {
            return this.tramitePredioEnglobes.get(0).getPredio()
                .isPredioRural();
        }
        if (this.predio != null) {
            return this.predio.isPredioRural();
        }
        return false;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * determina si el trámite es de alguno de los tipos especiales considerados en la vista de
     * detalles de un trámite; es decir, si es autoavalúo, revisión de avalúo, rectificación,
     * complementación.
     *
     * @author pedro.garcia
     */
    @Transient
    public boolean isEsCasoEspecialParaVerDetalles() {
        boolean answer = false;

        if (this.isEsRectificacion() || this.isEsComplementacion() ||
            this.isTramiteDeAutoavaluo() ||
            this.isEsTramiteDeRevisionDeAvaluo()) {
            answer = true;
        }

        return answer;
    }

    /**
     * Método que retorna los predios asociados al trámite, bien sean predios por ser un trámite de
     * englobe o el predio único asociado al trámite
     *
     * @return
     * @author fabio.navarrete
     * @modified juan.agudelo
     * @modified felipe.cadena -- 10/07/2013 -- Se agrega validación cuando es tramite de
     * rectificación
     * @modified juanfelipe.garcia :: 17-09-2013 :: se quitan los predios repetidos de
     * tramitePredioEnglobes
     */
    @Transient
    public List<Predio> getPredios() {
        if (predios.isEmpty()) {
            if (this.tipoTramite.equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()) &&
                !this.isRectificacionMatriz()) {
                predios.add(this.predio);
                return predios;
            }

            if (this.tramitePredioEnglobes != null &&
                !this.tramitePredioEnglobes.isEmpty()) {
                for (TramitePredioEnglobe tpe : this.tramitePredioEnglobes) {
                    predios.add(tpe.getPredio());
                }
                //se valida que no tenga predios repetidos
                HashSet hs = new HashSet();
                hs.addAll(predios);
                predios.clear();
                predios.addAll(hs);

            } else {
                if (this.predio != null) {
                    predios.add(predio);
                }
            }

            if (predios.isEmpty()) {

                if (this.tramite != null && this.tramite.isQuinta() == true) {
                    Predio predioTemp = new Predio();

                    predioTemp.setNumeroPredial(numeroPredial);
                    predios.add(predioTemp);

                }
            }
        }
        return predios;
    }

    /**
     * Método que retorna la comisión de conservación a la cuál pertenece el trámite
     */
    /*
     * @modified felipe.cadena --Con el proceso de depuraciòn el tramite puede tener dos comisiones:
     * una de conservación y una de depuración, asi que se deben filtrar por tipo. @modified
     * juanfelipe.garcia adicion de validacion en tipo comision @return
     */
    @Transient
    public ComisionTramite getComisionTramite() {

        ComisionTramite resultado = null;
        if (this.comisionTramites != null && !this.comisionTramites.isEmpty()) {
            for (ComisionTramite ct : this.comisionTramites) {
                if (ct.getComision().getTipo() != null && EComisionTipo.CONSERVACION.getCodigo().
                    equals(ct.getComision().getTipo())) {
                    resultado = ct;
                    break;
                }
            }
        }
        return resultado;
    }

    /**
     * Método para retornar la comisión que corresponde a la depuración
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public ComisionTramite getComisionTramiteDepuracion() {

        ComisionTramite resultado = null;
        if (this.comisionTramites != null && !this.comisionTramites.isEmpty()) {
            for (ComisionTramite ct : this.comisionTramites) {
                if (ct.getComision().getTipo().equals(EComisionTipo.DEPURACION.getCodigo())) {
                    resultado = ct;
                    break;
                }
            }
        }
        return resultado;
    }

    /**
     * Reemplaza la primera (que debiera ser la única) ComisionTramite de este trámite
     *
     * @author pedro.garcia
     * @param ct
     *
     *
     * @modified felipe.cadena -- Se reemplaza solo la comisión correspondiente al proceso de
     * conservación
     */
    public void setComisionTramite(ComisionTramite ct) {

        int idx = -1;

        if (this.comisionTramites != null && !this.comisionTramites.isEmpty()) {
            for (ComisionTramite cTramite : this.comisionTramites) {
                if (cTramite.getComision().getTipo().equals(EComisionTipo.CONSERVACION.getCodigo())) {
                    idx = this.comisionTramites.indexOf(cTramite);
                    break;
                }
            }
        }
        if (idx >= 0) {
            this.comisionTramites.remove(idx);
            this.comisionTramites.add(ct);
        }

    }

    /**
     * Reemplaza la comisión correspondiente a depuración
     *
     * @author felipe.cadena
     * @param ct
     */
    public void setComisionTramiteDepuracion(ComisionTramite ct) {

        int idx = -1;

        if (this.comisionTramites != null && !this.comisionTramites.isEmpty()) {
            for (ComisionTramite cTramite : this.comisionTramites) {
                if (cTramite.getComision().getTipo().equals(EComisionTipo.DEPURACION.getCodigo())) {
                    idx = this.comisionTramites.indexOf(cTramite);
                    break;
                }
            }
        }
        if (idx >= 0) {
            this.comisionTramites.remove(idx);
            this.comisionTramites.add(ct);
        }

    }

// --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete Método que arma el número predial
     * @return Número predial completo con el formato especificado.
     */
    @Transient
    public String getFormattedNumeroPredialCompleto() {
        String formatedNumPredial = new String();

        // Departamento
        formatedNumPredial = numeroPredial.substring(0, 2).concat("-");
        // Municipio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(2, 5)).concat("-");
        // Tipo Avalúo
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(5, 7)).concat("-");
        // Sector
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(7, 9)).concat("-");
        // Barrio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(9, 13)).concat("-");
        // Manzana
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(13, 17)).concat("-");
        // Predio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(17, 21)).concat("-");
        // Condición Propiedad
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(21, 22)).concat("-");
        // Edificio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(22, 24)).concat("-");
        // Piso
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(24, 26)).concat("-");
        // Unidad
        formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(
            26, 30));
        return formatedNumPredial;
    }

    /**
     * @author fabio.navarrete Método que arma el número predial con el formato usado en los caos de
     * uso de consulta para la interfaz gráfica del sistema. Retorna el dato sin códigos de
     * departamento y municipio.
     * @return Número predial con el formato especificado.
     */
    @Transient
    public String getFormattedNumeroPredial() {
        String formatedNumPredial = new String();
        /*
         * //Departamento formatedNumPredial = numeroPredial.substring(0, 2).concat("-");
         * //Municipio formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(2, 5))
         * .concat("-");
         */
        // Tipo Avalúo
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(5, 7)).concat("-");
        // Sector
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(7, 9)).concat("-");
        // Barrio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(9, 13)).concat("-");
        // Manzana
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(13, 17)).concat("-");
        // Predio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(17, 21)).concat("-");
        // Condición Propiedad
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(21, 22)).concat("-");
        // Edificio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(22, 24)).concat("-");
        // Piso
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(24, 26)).concat("-");
        // Unidad
        formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(
            26, 30));
        return formatedNumPredial;
    }

    /**
     * Determina si un trámite es de terreno
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isTramiteTerreno() {
        if (this.clasificacion != null) {
            return this.clasificacion.equals(ETramiteClasificacion.TERRENO
                .toString());
        }
        return false;
    }

    /**
     * Determina si un trámite es de oficina
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isTramiteOficina() {
        if (this.clasificacion != null) {
            return this.clasificacion.equals(ETramiteClasificacion.OFICINA
                .toString());
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Determina si un trámite es de tipo geográfico
     *
     * @return
     */
    /*
     * @modified.by :: jonathan.chacon Se adiciona esta validacion para que los tramites que tengan
     * predios fiscales se compoerten como tramites de oficina
     */
 /*
     * @modified.by leidy.gonzalez :: #16861 :: 12/04/2016 Se agregan mutaciones de cuarta para
     * saber sie le tramite es geografico o no
     */
    @Transient
    public boolean isTramiteGeografico() {

        boolean pf;
        if (getPredio() != null) {

            if (getPredio().getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.
                getCodigo())) {
                pf = true;
            } else {
                pf = false;
            }
        } else {
            pf = false;
        }

        if (pf == true) {
            return false;
        } else {
            return ((isMutacion() && (isSegunda() || isTercera() || isQuinta())) ||
                (isRectificacion() && isRectificacionArea()) ||
                (isRectificacion() && isRectificacionCancelacionDobleInscripcionGeo()) ||
                (isRevisionAvaluo() && isRevisionAvaluoAreaOUsoOPuntaje()) ||
                isCancelacionMasiva());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que determina si un trámite es de tercera
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isTercera() {
        if (tipoTramite != null && claseMutacion != null &&
            tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
            claseMutacion.equals(EMutacionClase.TERCERA.getCodigo())) {
            return true;
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que determina si un trámite es de cuarta
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isCuarta() {
        if (this.tipoTramite != null && this.claseMutacion != null &&
            this.tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
            this.claseMutacion.equals(EMutacionClase.CUARTA.getCodigo())) {
            return true;
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que determina si un trámite es de cancelación
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isCancelacionPredio() {
        if (this.tipoTramite != null &&
            this.tipoTramite
                .equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo())) {
            return true;
        }
        return false;
    }

    /**
     * Método que determina si un trámite es de cancelación
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isModificacionInscripcionCatastral() {
        if (this.tipoTramite != null &&
            this.tipoTramite
                .equals(ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL
                    .getCodigo())) {
            return true;
        }
        return false;
    }

    /**
     * Método que determina si un trámite está comisionado
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isTramiteComisionado() {
        if (comisionado != null && comisionado.equals(ESiNo.SI.getCodigo())) {
            return true;
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     *
     * @modified felipe.cadena --Con el proceso de depuraciòn el tramite puede tener dos comisiones
     * una de conservaciòn y una de depuraciòn, asi que se deben filtrar por tipo.
     */
    @Transient
    public String getNumeroComision() {

        String answer = "";
        if (this.comisionTramites != null && !this.comisionTramites.isEmpty()) {
            for (ComisionTramite ct : this.comisionTramites) {
                if (ct.getComision().getTipo().equals(EComisionTipo.CONSERVACION.getCodigo())) {
                    answer = ct.getComision().getNumero();
                    break;
                }
            }
        }
        return answer;
    }

    /**
     * Método para retornar el número de comision de depuración
     *
     * @author felipe.cadena
     *
     *
     */
    @Transient
    public String getNumeroComisionDepuracion() {

        String answer = "";
        if (this.comisionTramites != null && !this.comisionTramites.isEmpty()) {
            for (ComisionTramite ct : this.comisionTramites) {
                if (ct.getComision().getTipo().equals(EComisionTipo.DEPURACION.getCodigo())) {
                    answer = ct.getComision().getNumero();
                    break;
                }
            }
        }
        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author franz.gamba
     */
    @Transient
    public boolean isSeleccionAprobar() {
        return this.seleccionAprobar;
    }

    public void setSeleccionAprobar(boolean seleccionAprobar) {
        this.seleccionAprobar = seleccionAprobar;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Transient
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long id) {
        this.predioId = id;
    }

    @Transient
    public Long getResultadoDocumentoId() {
        return this.resultadoDocumentoId;
    }

    public void setResultadoDocumentoId(Long id) {
        this.resultadoDocumentoId = id;
    }

    @Transient
    public boolean isTipoInscripcionPH() {
        if (this.tipoInscripcion != null) {
            return this.tipoInscripcion.equals(ETramiteTipoInscripcion.PH
                .getCodigo());
        }
        return false;
    }

    @Transient
    public boolean isTipoInscripcionCondominio() {
        if (this.tipoInscripcion != null) {
            return this.tipoInscripcion
                .equals(ETramiteTipoInscripcion.CONDOMINIO.getCodigo());
        }
        return false;
    }

    @Transient
    public boolean isTipoInscripcionMixto() {
        if (this.tipoInscripcion != null) {
            return this.tipoInscripcion
                .equals(ETramiteTipoInscripcion.MIXTO.getCodigo());
        }
        return false;
    }

    /**
     * Método que indica si el subtipo del trámite es Desenglobe
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSegundaDesenglobe() {

        if (tipoTramite != null && claseMutacion != null &&
            this.subtipo != null) {
            if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                claseMutacion.equals(EMutacionClase.SEGUNDA.getCodigo()) &&
                this.subtipo.equals(EMutacion2Subtipo.DESENGLOBE
                    .getCodigo())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si el subtipo del trámite es Englobe
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSegundaEnglobe() {

        if (tipoTramite != null && claseMutacion != null &&
            this.subtipo != null) {
            if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                claseMutacion.equals(EMutacionClase.SEGUNDA.getCodigo()) &&
                this.subtipo.equals(EMutacion2Subtipo.ENGLOBE
                    .getCodigo())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si trámite es de primera
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isPrimera() {

        if (tipoTramite != null && claseMutacion != null) {
            if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                claseMutacion.equals(EMutacionClase.PRIMERA.getCodigo())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si trámite es de quinta y el subtipo es nuevo
     *
     * @return
     * @author fabio.navarrete
     * @modified by fredy.wilches
     */
    /*
     * @modified fredy.WIsh_a_Lot_of_Comments_He'd_use_on_Every_Sentence se tiene en cuenta el nuevo
     * campo que dice si es un trámite de quinta omitido al que se le creó un predio nuevo
     */
    @Transient
    public boolean isQuintaNuevo() {
        if (this.tipoTramite != null && this.claseMutacion != null && this.subtipo != null) {
            if (this.tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                this.claseMutacion.equals(EMutacionClase.QUINTA.getCodigo()) &&
                this.subtipo.equals(EMutacion5Subtipo.NUEVO.getCodigo()) &&
                (this.omitidoNuevo == null || this.omitidoNuevo.equals(ESiNo.NO.getCodigo()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si trámite es de quinta y el subtipo es omitido
     *
     * @return
     * @author fabio.navarrete
     * @modified by fredy.wilches
     */
    @Transient
    public boolean isQuintaOmitido() {
        if (this.tipoTramite != null && this.claseMutacion != null && this.subtipo != null) {
            if (this.tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) &&
                this.claseMutacion.equals(EMutacionClase.QUINTA.getCodigo()) &&
                this.subtipo.equals(EMutacion5Subtipo.OMITIDO.getCodigo()) &&
                (this.omitidoNuevo == null || this.omitidoNuevo.equals(ESiNo.NO.getCodigo()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si trámite es de quinta y el subtipo es omitido, pero que al no encontrarlo
     * se crea nuevo
     *
     * @return
     * @author fredy.wilches
     */
    @Transient
    public boolean isQuintaOmitidoNuevo() {
        if (this.omitidoNuevo != null && this.omitidoNuevo.equals(ESiNo.SI.getCodigo())) {
            return true;
        }
        return false;
    }

    /**
     * Método que indica si el trámite es una Rectificación
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isRectificacion() {
        if (this.tipoTramite != null) {
            return this.tipoTramite.equals(ETramiteTipoTramite.RECTIFICACION.getCodigo());
        }
        return false;
    }

    /**
     * Método que indica si el trámite es una Revisión de avalúo
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isRevisionAvaluo() {
        if (this.tipoTramite != null) {
            return this.tipoTramite.equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo());
        }
        return false;
    }

    /**
     * Método que indica si el trámite es una Revocatoria Directa
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isRevocatoriaDirecta() {
        if (this.tipoTramite != null) {
            return this.tipoTramite.equals(ETramiteTipoTramite.REVOCATORIA_DIRECTA.getCodigo());
        }
        return false;
    }

    @Column(name = "MANTIENE_NUMERO_PREDIAL", nullable = true, length = 2)
    public String getMantieneNumeroPredial() {
        return mantieneNumeroPredial;
    }

    public void setMantieneNumeroPredial(String mantieneNumeroPredial) {
        this.mantieneNumeroPredial = mantieneNumeroPredial;
    }

    /**
     * Método que determina si la rectificación es de área
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isRectificacionArea() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATOS_RECTIFICAR_AREA.contains(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Método que determina si la rectificación es de Año de construcción
     *
     * @return
     * @author carlos.ferro
     */
    @Transient
    public boolean isRectificacionAnioConstruccion() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATO_RECTIFICAR_ANIO_CONSTRUCCION.contains(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determina si la rectificacion del tramite corresponde con el area de la construccion
     *
     * @author andres.eslava
     */
    @Transient
    public boolean isRectificacionGeograficaAreaConstruccion() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (EDatoRectificarNombre.AREA_CONSTRUIDA.getCodigo().equals(tr.getDatoRectificar().
                    getNombre()) || EDatoRectificarNombre.DIMENSION.getCodigo().equals(tr.
                        getDatoRectificar().getNombre())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determina si la rectificacion del tramite corresponde con el area de terreno
     *
     * @author andres.eslava
     */
    @Transient
    public boolean isRectificacionGeograficaAreaTerreno() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (EDatoRectificarNombre.AREA_TERRENO.getCodigo().equals(tr.getDatoRectificar().
                    getNombre()) || EDatoRectificarNombre.DIMENSION.getCodigo().equals(tr.
                        getDatoRectificar().getNombre())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si la rectificación es de área de construcción
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isRectificacionAreaConstruccion() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.AREA_CONSTRUIDA.equals(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si la rectificación es de detalle de calificacion
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isRectificacionDetalleCalificacion() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DETALLE_CALIFICACION.equals(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si la rectificación es de zonas
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isRectificacionZona() {
        //felipe.cadena::26-06-2015::#13167::Los tramites de rectificacion masiva no entran en esta categoria
        if (this.isRectificacionMatriz()) {
            return false;
        }
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATOS_RECTIFICAR_ZONA.contains(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si la rectificación es de zonas
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isRectificacionZonaMatriz() {
        if (!this.isRectificacionMatriz()) {
            return false;
        }
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATOS_RECTIFICAR_ZONA.contains(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si el tramite es complementacion de detalle calificacion
     *
     * @return
     * @author fredy.wilches
     */
    @Transient
    public boolean isComplementacionDetalleCalificacion() {

        if (this.isEsComplementacion() && this.tramiteRectificacions != null &&
            !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATO_RECTIFICAR_DETALLE_CALIFICACION.contains(tr.getDatoRectificar().
                    getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Méotodo que determina si la rectificación es de uso de construcción
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isRectificacionUso() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATOS_RECTIFICAR_USO.contains(tr.getDatoRectificar().getColumna())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si la revisión de avalúo es de área, puntaje o uso de construcción
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isRevisionAvaluoAreaOUsoOPuntaje() {
        ComisionTramite ct = this.getComisionTramite();
        if (ct != null && ct.getComisionTramiteDatos() != null && !ct.getComisionTramiteDatos().
            isEmpty()) {
            for (ComisionTramiteDato ctd : ct.getComisionTramiteDatos()) {
                if (ctd.getModificado().equals(ESiNo.SI.getCodigo())) {
                    if (ctd.getDato().equals(EComisionTramiteDatoDato.AREA_DE_TERRENO.getValor()) ||
                        ctd.getDato().equals(EComisionTramiteDatoDato.AREA_CONSTRUIDA.getValor()) ||
                        ctd.getDato().equals(EComisionTramiteDatoDato.PUNTAJE_DE_CONSTRUCCION.
                            getValor()) ||
                        ctd.getDato().equals(EComisionTramiteDatoDato.USOS_DE_LA_CONSTRUCCION.
                            getValor())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que recupera el tipo de trámite completo en una cadena
     *
     * @author juan.agudelo
     */
    @Transient
    public String getTipoTramiteCadenaCompleto() {

        StringBuilder tipoTramiteTemp = new StringBuilder();
        if (this.tipoTramite != null && !this.tipoTramite.isEmpty()) {

            for (ETramiteTipoTramite tt : ETramiteTipoTramite.values()) {
                if (tt.getCodigo().equals(this.tipoTramite)) {
                    tipoTramiteTemp.append(tt.getNombre());
                }
            }

            if (this.tipoTramite.equals(ETramiteTipoTramite.MUTACION
                .getCodigo())) {

                if (this.claseMutacion != null && !this.claseMutacion.isEmpty()) {

                    for (EMutacionClase mt : EMutacionClase.values()) {

                        if (mt.getCodigo().equals(this.claseMutacion)) {
                            tipoTramiteTemp.append(" ");
                            tipoTramiteTemp.append(mt.getNombre());
                        }
                    }

                    if (this.subtipo != null && !this.subtipo.isEmpty()) {

                        if (this.claseMutacion.equals(EMutacionClase.SEGUNDA
                            .getCodigo())) {

                            for (EMutacion2Subtipo mst : EMutacion2Subtipo
                                .values()) {

                                if (mst.getCodigo().equals(this.subtipo)) {

                                    tipoTramiteTemp.append(" ");
                                    tipoTramiteTemp.append(mst.getNombre());
                                }
                            }
                        } else if (this.claseMutacion
                            .equals(EMutacionClase.QUINTA.getCodigo())) {

                            for (EMutacion5Subtipo mqt : EMutacion5Subtipo
                                .values()) {

                                if (mqt.getCodigo().equals(this.subtipo)) {

                                    tipoTramiteTemp.append(" ");
                                    tipoTramiteTemp.append(mqt.getNombre());
                                }
                            }
                        }
                    }
                }
            }
        }
        if (this.isTipoTramiteViaGubernativaModificado() || this.
            isTipoTramiteViaGubModSubsidioApelacion()) {

            return this.getObservacionesRadicacion();
        } else {
            tipoTramiteCadenaCompleto = tipoTramiteTemp.toString();
            return tipoTramiteCadenaCompleto;
        }
    }

    /**
     * Método modificado para la parte geográfica, que recupera el tipo de trámite completo en una
     * cadena. Nota: el método original (getTipoTramiteCadenaCompleto), tiene asociada lógica para
     * trámites de vía gubernativa, que no es compatible con los métodos del editor geográfico.
     *
     * @author juanfelipe.garcia
     */
    @Transient
    public String getTipoTramiteCadenaCompletoEditorGeografico() {

        StringBuilder tipoTramiteTemp = new StringBuilder();
        for (ETramiteTipoTramite tt : ETramiteTipoTramite.values()) {
            if (tt.getCodigo().equals(this.tipoTramite)) {
                tipoTramiteTemp.append(tt.getNombre());
            }
        }

        if (this.tipoTramite.equals(ETramiteTipoTramite.MUTACION
            .getCodigo())) {

            if (this.claseMutacion != null && !this.claseMutacion.isEmpty()) {

                for (EMutacionClase mt : EMutacionClase.values()) {

                    if (mt.getCodigo().equals(this.claseMutacion)) {
                        tipoTramiteTemp.append(" ");
                        tipoTramiteTemp.append(mt.getNombre());
                    }
                }

                if (this.subtipo != null && !this.subtipo.isEmpty()) {

                    if (this.claseMutacion.equals(EMutacionClase.SEGUNDA
                        .getCodigo())) {

                        for (EMutacion2Subtipo mst : EMutacion2Subtipo
                            .values()) {

                            if (mst.getCodigo().equals(this.subtipo)) {

                                tipoTramiteTemp.append(" ");
                                tipoTramiteTemp.append(mst.getNombre());
                            }
                        }
                    } else if (this.claseMutacion
                        .equals(EMutacionClase.QUINTA.getCodigo())) {

                        for (EMutacion5Subtipo mqt : EMutacion5Subtipo
                            .values()) {

                            if (mqt.getCodigo().equals(this.subtipo)) {

                                tipoTramiteTemp.append(" ");
                                tipoTramiteTemp.append(mqt.getNombre());
                            }
                        }
                    }
                }
            }
        }

        tipoTramiteCadenaCompleto = tipoTramiteTemp.toString();
        return tipoTramiteCadenaCompleto;

    }

    @Column(name = "OMITIDO_NUEVO", length = 2)
    public String getOmitidoNuevo() {
        return this.omitidoNuevo == null ? ESiNo.NO.getCodigo() : this.omitidoNuevo;
    }

    public void setOmitidoNuevo(String omitidoNuevo) {
        this.omitidoNuevo = omitidoNuevo;
    }

    /**
     * @author franz.gamba almacena los documentos asociados a pruebas sobre el trámite
     * @return
     */
    @Transient
    public List<TramiteDocumento> getDocumentosPruebas() {
        return documentosPruebas;
    }

    public void setDocumentosPruebas(List<TramiteDocumento> documentosPruebas) {
        this.documentosPruebas = documentosPruebas;
    }

    //@Transient
    public boolean tieneReplica() {
        for (TramiteDocumento td : this.tramiteDocumentos) {
            if (td.getDocumento().getTipoDocumento().getId().equals(
                ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId())) {
                return true;
            }
        }
        return false;
    }

    public boolean tieneReplicaFinal() {
        for (TramiteDocumento td : this.tramiteDocumentos) {
            if (td.getDocumento().getTipoDocumento().getId().equals(
                ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId())) {
                return true;
            }
        }
        return false;
    }

    // ------------------------------------------------------------ //
    /**
     * Método transient para consultarla actividad actual del trámite, dicha actividad se debe
     * setear desde el bean que se esté trabajando, si no se hace, el método instancia una actividad
     * vacía.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public Actividad getActividadActualTramite() {
        if (this.actividadActualTramite == null) {
            this.actividadActualTramite = new Actividad();
            this.actividadActualTramite.setNombre("");
        }
        return actividadActualTramite;
    }

    public void setActividadActualTramite(Actividad actividadActualTramite) {
        this.actividadActualTramite = actividadActualTramite;
    }

    // ------------------------------------------------------------ //
    /**
     * Método transient para consultar la fecha de asignación de la actividad actual del trámite,
     * Dicha actividad se debe setear desde el bean que se esté trabajando, si no se hace, el método
     * instancia una actividad vacía, por lo que la fecha de asignación cuando la Actividad no
     * exista será null.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public Date getFechaAsignacionActividadActual() {

        if (this.actividadActualTramite != null &&
            this.actividadActualTramite.getFechaAsignacion() != null) {
            fechaAsignacionActividadActual = this.actividadActualTramite
                .getFechaAsignacion().getTime();
            return fechaAsignacionActividadActual;
        } else {
            return null;
        }
    }

    public void setFechaAsignacionActividadActual(
        Date fechaAsignacionActividadActual) {
        this.fechaAsignacionActividadActual = fechaAsignacionActividadActual;
    }

    // ------------------------------------------------------------ //
    /**
     * Método transient para consultar el usuario que ejecuta la actividad del trámite, dicho
     * usuario se debe setear desde el bean que se esté trabajando, si no se hace, el método
     * instancia un usuario vacío.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public UsuarioDTO getUsuarioActividadTramite() {
        if (this.usuarioActividadTramite == null) {
            this.usuarioActividadTramite = new UsuarioDTO();
            this.usuarioActividadTramite.setPrimerNombre("");
            this.usuarioActividadTramite.setPrimerApellido("");
            String roles[] = {""};
            this.usuarioActividadTramite.setRoles(roles);
        }
        return usuarioActividadTramite;
    }

    public void setUsuarioActividadTramite(UsuarioDTO usuarioActividadTramite) {
        this.usuarioActividadTramite = usuarioActividadTramite;
    }

    // ------------------------------------------------------------ //
    /**
     * Método transient para consultar el número de la resolución del trámite.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public String getNumeroResolucion() {
        String numeroResolucion = new String("");
        if (this.resultadoDocumento != null &&
            this.resultadoDocumento.getNumeroDocumento() != null &&
            !this.resultadoDocumento.getNumeroDocumento().trim()
                .isEmpty()) {
            numeroResolucion = this.resultadoDocumento.getNumeroDocumento();
        }
        return numeroResolucion;
    }

    /**
     * Retorna rojo amarillo o verde segun el tiempo que lleva el tramite
     *
     * @return
     */
    @Transient
    public String getSemaforo() {
        Date d = this.fechaRadicacion;
        Date hoy = new Date();
        int dias = (int) ((hoy.getTime() - d.getTime()) / (1000 * 60 * 60 * 24));
        String imagen;
        if (dias <= 10) {
            imagen = Constantes.SEMAFORO_EN_VERDE;
        } else if (dias <= 20) {
            imagen = Constantes.SEMAFORO_EN_AMARILLO;
        } else {
            imagen = Constantes.SEMAFORO_EN_ROJO;
        }
        return imagen;
    }
//--------------------------------------------------------------------------------------------------

    @Transient
    public int getNumeroComisionesHaEstado() {
        return this.numeroComisionesHaEstado;
    }

    public void setNumeroComisionesHaEstado(int numeroComisionesHaEstado) {
        this.numeroComisionesHaEstado = numeroComisionesHaEstado;
    }
//--------------------------------------------------------------------------------------------------

    @Transient
    public String getNombreEjecutorDepuracion() {
        return this.nombreEjecutorDepuracion;
    }

    public void setNombreEjecutorDepuracion(String nombreEjecutorDepuracionP) {
        this.nombreEjecutorDepuracion = nombreEjecutorDepuracionP;
    }

    @Transient
    public String getEjecutorDepuracion() {
        return this.ejecutorDepuracion;
    }

    public void setEjecutorDepuracion(String ejecutorDepuracionP) {
        this.ejecutorDepuracion = ejecutorDepuracionP;
    }

    @Transient
    public String getNombreRolUsuarioActual() {
        return this.nombreRolUsuarioActual;
    }

    public void setNombreRolUsuarioActual(String nombreRolUsuarioActual) {
        this.nombreRolUsuarioActual = nombreRolUsuarioActual;
    }
    //-------------------------------------------------------------------------------------------------

    /*
     * retorna true si este trámite tiene como tipo alguno de los que lo definen como de solicitud
     * de tipo vía gubernativa.
     *
     * @author pedro.garcia
     */
    @Transient
    public boolean isViaGubernativa() {

        boolean answer = false;

        if (this.tipoTramite != null) {
            if (this.tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo()) ||
                this.tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_APELACION.getCodigo()) ||
                this.tipoTramite.equals(
                    ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getCodigo()) ||
                this.tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_QUEJA.getCodigo())) {
                answer = true;
            }
        }

        return answer;
    }

    //-------------------------------------------------------------------------------------------------
    /*
     * retorna true si el trámite es un recurso de apelación (Solicitud de vía gubernativa).
     *
     * @author juanfelipe.garcia
     */
    @Transient
    public boolean isRecursoApelacion() {
        boolean answer = false;
        if (this.tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_APELACION.getCodigo())) {
            answer = true;
        }
        return answer;
    }

    //-------------------------------------------------------------------------------------------------
    /*
     * retorna true si el trámite es una queja (Solicitud de vía gubernativa).
     *
     * @author juanfelipe.garcia
     */
    @Transient
    public boolean isQueja() {
        boolean answer = false;
        if (this.tipoTramite.equals(ETramiteTipoTramite.RECURSO_DE_QUEJA.getCodigo())) {
            answer = true;
        }
        return answer;
    }

    //-------------------------------------------------------------------------------------------------
    /*
     * Retorna true si el trámite es recurso de reposición en subsidio de apelación (Solicitud de
     * vía gubernativa).
     *
     * @author juanfelipe.garcia
     */
    @Transient
    public boolean isRecursoReposicionEnSubsidioApelacion() {
        boolean answer = false;
        if (this.tipoTramite.equals(ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.
            getCodigo())) {
            answer = true;
        }
        return answer;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * determina si el trámite, siendo de rectificación, tiene un dato (que debería ser único) por
     * rectificar de tipo Generales-Cancelación doble inscripción. </br>
     * Se usa porque los trámites de este tipo requieren validaciones diferentes
     *
     * @author pedro.garcia
     */
    @Transient
    public boolean isRectificacionCDI() {

        boolean answer = false;
        DatoRectificar datoRectif;

        if (this.isRectificacion()) {
            if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
                if (this.tramiteRectificacions.size() == 1) {
                    if (this.tramiteRectificacions.get(0) != null &&
                        this.tramiteRectificacions.get(0).getDatoRectificar() != null) {
                        datoRectif = this.tramiteRectificacions.get(0).getDatoRectificar();
                        if (datoRectif.isDatoRectifCDI()) {
                            answer = true;
                        }
                    }
                }
            }
        }

        return answer;
    }

    /**
     * Método que determina si la rectificación es de cancelación de doble inscripción
     *
     * @return
     * @author fredy.wilches
     */
    @Transient
    public boolean isRectificacionCancelacionDobleInscripcion() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (Constantes.DATO_RECTIFICAR_CANCELACION_DOBLE_INSCRIPCION.equals(tr.
                    getDatoRectificar().getNombre())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si la rectificación es de cancelación de doble inscripción y es un
     * tramite geografico
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isRectificacionCancelacionDobleInscripcionGeo() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if ((tr.getDatoRectificar().getId().equals(
                    Constantes.TIPO_DATO_RECTIFICACION_CDI_GEO))) {
                    return true;
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------
    /*
     * retorna la ultima razon de rechazo depuración.
     *
     * @author juanfelipe.garcia
     */
    @Transient
    public String getUltimaRazonRechazoDepuracion() {
        return ultimaRazonRechazoDepuracion;
    }

    public void setUltimaRazonRechazoDepuracion(String ultimaRazonRechazoDepuracion) {
        this.ultimaRazonRechazoDepuracion = ultimaRazonRechazoDepuracion;
    }

    @Transient
    public String getTramiteProvenienteDepuracion() {
        return tramiteProvenienteDepuracion;
    }

    public void setTramiteProvenienteDepuracion(String tramiteProvenienteDepuracion) {
        this.tramiteProvenienteDepuracion = tramiteProvenienteDepuracion;
    }

    /**
     * Para tramites de vía gubernativa, en los que se MODIFICO el resultado de la resolución del
     * trámite original, se cambia el tipo de tramite al del trámite original y se guarda en el
     * campo observacionesRadicacion la referencia al tipo de trámite de vía gubernativa
     *
     * @return
     * @author juanfelipe.garcia
     */
    @Transient
    public boolean isTipoTramiteViaGubernativaModificado() {
        boolean answer = false;

        if (this.observacionesRadicacion != null && !this.observacionesRadicacion.isEmpty()) {
            if (this.observacionesRadicacion.equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.
                getNombre()) ||
                this.observacionesRadicacion.equals(ETramiteTipoTramite.RECURSO_DE_APELACION.
                    getNombre()) ||
                this.observacionesRadicacion.equals(ETramiteTipoTramite.RECURSO_DE_QUEJA.
                    getNombre())) {
                answer = true;
            }
        }

        return answer;
    }

    /**
     * Para tramites de vía gubernativa RECURSO_REPOSICION_EN_SUBSIDIO_APELACION, en los que se
     * MODIFICO el resultado de la resolución del trámite original, se cambia el tipo de tramite al
     * del trámite original y se guarda en el campo observacionesRadicacion la referencia al tipo de
     * trámite de vía gubernativa
     *
     * @return
     * @author juanfelipe.garcia
     */
    @Transient
    public boolean isTipoTramiteViaGubModSubsidioApelacion() {
        boolean answer = false;

        if (this.observacionesRadicacion != null && !this.observacionesRadicacion.isEmpty()) {
            if (this.observacionesRadicacion.equals(
                ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getNombre())) {
                answer = true;
            }
        }

        return answer;
    }

    /**
     * Retorna los documentos asociados al tramite adicionales
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public List<TramiteDocumentacion> getTramiteDocumentacionAdicional() {
        List<TramiteDocumentacion> answer = new ArrayList<TramiteDocumentacion>();

        if (this.tramiteDocumentacions != null) {
            for (TramiteDocumentacion td : this.tramiteDocumentacions) {
                if (ESiNo.SI.getCodigo().equals(td.getAdicional())) {
                    answer.add(td);
                }
            }
        }
        return answer;
    }

    /**
     * Retorna los documentos asociados al tramite no adicionales
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public List<TramiteDocumentacion> getTramiteDocumentacionNoAdicional() {
        List<TramiteDocumentacion> answer = new ArrayList<TramiteDocumentacion>();

        if (this.tramiteDocumentacions != null) {
            for (TramiteDocumentacion td : this.tramiteDocumentacions) {
                if (!ESiNo.SI.getCodigo().equals(td.getAdicional())) {
                    answer.add(td);
                }
            }
        }
        return answer;
    }

    @Column(name = "TAREA", length = 60)
    public String getTarea() {
        return tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    @Column(name = "PASO", length = 60)
    public String getPaso() {
        return paso;
    }

    public void setPaso(String paso) {
        this.paso = paso;
    }

    @Column(name = "RESULTADO", length = 30)
    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Column(name = "RADICACION_ESPECIAL", length = 50)
    public String getRadicacionEspecial() {
        return radicacionEspecial;
    }

    public void setRadicacionEspecial(String resultado) {
        this.radicacionEspecial = resultado;
    }

    @Column(name = "PROCESO")
    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    /**
     * Oficina a la cual esta relacionado el tramite, sea territorial o UOC solo se inicializa en
     * las operaciones de reasignacion de usuaios
     *
     * @author felipe.cadena
     *
     * @return
     *
     */
    @Transient
    public String getOficinaCodigo() {
        return this.oficinaCodigo;
    }

    public void setOficinaCodigo(String oficinaCodigo) {
        this.oficinaCodigo = oficinaCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tramite other = (Tramite) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     * Determina si es un tramite de un desenglobe PH o Condominio por etapas.
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isDesenglobeEtapas() {

        if (this.radicacionEspecial != null) {
            if (ETramiteRadicacionEspecial.POR_ETAPAS.getCodigo().equals(this.radicacionEspecial)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determina si es un tramite de rectificacion masiva sobre una ficha matriz.
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isRectificacionMatriz() {

        if (this.radicacionEspecial != null) {
            if (ETramiteRadicacionEspecial.RECTIFICACION_MATRIZ.getCodigo().equals(
                this.radicacionEspecial)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determina si es un tramite de cancelacion masiva sobre una ficha matriz.
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isCancelacionMasiva() {

        if (this.radicacionEspecial != null) {
            if (ETramiteRadicacionEspecial.CANCELACION_MASIVA.getCodigo().equals(
                this.radicacionEspecial)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si el tipo de trámite es tercera de ph o condominio masiva
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isTerceraMasiva() {

        if (this.radicacionEspecial != null) {
            if (ETramiteRadicacionEspecial.TERCERA_MASIVA.getCodigo().
                equals(this.radicacionEspecial)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que indica si el tramite esta catalogado como masivo, muchos predios involucrados en
     * la radicacion
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isTramiteMasivo() {

        if (this.isRectificacionMatriz() ||
            this.isTerceraMasiva() ||
            this.isCancelacionMasiva() ||
            this.isDesenglobeEtapas() ||
            this.isQuintaMasivo() ||
            this.isSegundaEnglobe()) {
            return true;
        }
        return false;
    }

    /**
     * Método que indica si el tramite esta catalogado de actualizacion
     *
     * @return
     * @author leidy.gonzalez
     */
    @Transient
    public boolean isActualizacion() {

        if (ETramiteProceso.ACTUALIZACION.getCodigo().equals(this.proceso)) {
            return true;
        }

        return false;
    }

    /**
     * Método que determina si alguno de los predios asociados al tramites se encuentra en
     * actualizacion si es asi retorna el radicado de actualizacion el campo se debe asignar segun
     * una consulta externa al tramite
     *
     * @author felipe.cadena
     *
     * @return
     *
     */
    @Transient
    public Tramite getTramiteActualizacionAsociado() {

        return tramiteActualizacionAsociado;
    }

    public void setTramiteActualizacionAsociado(Tramite tramiteActualizacionAsociado) {
        this.tramiteActualizacionAsociado = tramiteActualizacionAsociado;
    }

    /**
     * Método retorna como un boolean si alguno de los predios asociados al tramite tiene tramites
     * asociados
     *
     * @author felipe.cadena
     *
     * @return
     *
     */
    @Transient
    public boolean isTramitesActualizacionRelacionados() {
        return this.tramiteActualizacionAsociado != null;
    }

    /**
     * Método que determina si alguno de los tramites ya les fue generado o no una resolucion
     *
     * @author leidy.gonzalez
     *
     * @return
     *
     */
    @Transient
    public String getEstadoResolucion() {
        return estadoResolucion;
    }

    public void setEstadoResolucion(String estadoResolucion) {
        this.estadoResolucion = estadoResolucion;
    }

    /**
     * Método retorna como un boolean si el tramite tiene en proceso la generacion de la resolucion
     *
     * @author leidy.gonzalez
     *
     * @return
     *
     */
    @Transient
    public boolean isEstadoProcesoResolucion() {
        return this.estadoResolucion != null;
    }

    /**
     * Método que verifica si un trámite que es mutación de quinta es masiva o no.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isQuintaMasivo() {
        if (ETramiteTipoTramite.MUTACION.getCodigo().equals(this.tipoTramite) &&
            EMutacionClase.QUINTA.getCodigo().equals(this.claseMutacion) &&
            ETramiteRadicacionEspecial.QUINTA_MASIVO.getCodigo().equals(this.radicacionEspecial)) {
            return true;
        }
        return false;
    }

    // ----------------------------------------------- //
    /**
     * Método para obtener el valor de la enumeración {@link EMutacionClase} de la clase de mutación
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public EMutacionClase getEMutacionClase() {
        EMutacionClase answer = null;
        if (this.isMutacion()) {
            if (this.claseMutacion != null && !this.claseMutacion.isEmpty()) {
                for (EMutacionClase clase : EMutacionClase.values()) {

                    if (clase.getCodigo().equals(this.claseMutacion)) {
                        answer = clase;
                        break;
                    }
                }
            }
        }
        return answer;
    }

    //----------------------------------------------- //
    /**
     * Método para obtener el valor de la enumeración {@link ETramiteTipoTramite} según los valores
     * del tipo de trámite.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public ETramiteTipoTramite getETipoTramite() {
        ETramiteTipoTramite answer = null;
        if (this.tipoTramite != null && !this.tipoTramite.isEmpty()) {
            for (ETramiteTipoTramite tipo : ETramiteTipoTramite.values()) {
                if (tipo.getCodigo().equals(this.tipoTramite)) {
                    answer = tipo;
                    break;
                }
            }
        }
        return answer;
    }

    @Transient
    public boolean isReclamado() {
        return reclamado;
    }

    public void setReclamado(boolean reclamado) {
        this.reclamado = reclamado;
    }

    /*
     * Metodo que visualiza si un tramite tiene o no asociado un derecho de peticion. @author
     * leidy.gonzalez
     */
    @Transient
    public String getFormaPeticionTramite() {

        String derechoPeticionTutela = "";

        if (this.getFormaPeticion() != null && this.getFormaPeticion().equals(
            ESolicitudFormaPeticion.DERECHO_DE_PETICION.getCodigo())) {
            derechoPeticionTutela = ESolicitudFormaPeticion.DERECHO_DE_PETICION.getCodigo();
        } else if (this.getFormaPeticion() != null && this.getFormaPeticion().equals(
            ESolicitudFormaPeticion.TUTELA.getCodigo())) {
            derechoPeticionTutela = ESolicitudFormaPeticion.TUTELA.getCodigo();
        } else if (this.getSolicitud().getAsunto() != null && (this.getSolicitud().getAsunto().
            contains(ESolicitudFormaPeticion.DERECHO_DE_PETICION.getCodigo()) ||
            this.getSolicitud().getAsunto().contains(ESolicitudFormaPeticion.DERECHO_DE_PETICION.
                getValor()))) {
            derechoPeticionTutela = ESolicitudFormaPeticion.DERECHO_DE_PETICION.getCodigo();
        } else if (this.getSolicitud().getAsunto() != null && (this.getSolicitud().getAsunto().
            contains(ESolicitudFormaPeticion.TUTELA.getCodigo()) || this.getSolicitud().getAsunto().
            contains(ESolicitudFormaPeticion.TUTELA.getValor()))) {
            derechoPeticionTutela = ESolicitudFormaPeticion.TUTELA.getCodigo();
        }
        return derechoPeticionTutela;
    }

    /**
     * Método que determina si la rectificación es de foto
     *
     * @return
     * @author jonathan.chacon
     */
    @Transient
    public boolean isRectificacionFoto() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (EDatoRectificarNombre.FOTO.getCodigo().
                    equals(tr.getDatoRectificar().getNombre())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Método que determina si un tramite se encuentra en un estado que ya no sera modificable
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isEnEstadoFinal() {
        if ((this.estado != null) &&
            (this.estado.equals(ETramiteEstado.FINALIZADO_APROBADO.getCodigo()) ||
            this.estado.equals(ETramiteEstado.CANCELADO.getCodigo()) ||
            this.estado.equals(ETramiteEstado.ARCHIVADO.getCodigo()) ||
            this.estado.equals(ETramiteEstado.RECHAZADO.getCodigo()))) {
            return true;
        }
        return false;
    }

    @Transient
    public boolean isPorCancelar() {
        return porCancelar;
    }

    public void setPorCancelar(boolean porCancelar) {
        this.porCancelar = porCancelar;
    }

    /**
     * MÃ©todo que determina si la rectificaciÃ³n es de foto
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isEnglobeVirtual() {
        if (ESiNo.SI.getCodigo().equals(this.obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.ENGLOBE_VIRTUAL))) {
            return true;
        }
        return false;
    }

    /**
     * Metodo que determina si el englobe virtual del tramite ya fue aprobado
     *
     * @return
     * @author felipe.cadena
     */
    @Transient
    public boolean isEnglobeVirtualAprobado() {
        if (ESiNo.SI.getCodigo().equals(this.obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.ENGLOBE_VIRTUAL_APROBADO))) {
            return true;
        }
        return false;
    }

    /**
     * MÃ©todo que obtiene el valor del campo dado desde la inormacion adicional del tramite
     *
     * @return
     * @author felipe.cadena
     */
    public String obtenerInfoCampoAdicional(EInfoAdicionalCampo campo) {

        if (this.tramiteInfoAdicionals != null &&
            !this.tramiteInfoAdicionals.isEmpty()) {

            for (TramiteInfoAdicional infoAdicional : this.tramiteInfoAdicionals) {
                if (infoAdicional.getCampo().equals(campo.getValor())) {
                    return infoAdicional.getValor();
                }
            }
        }

        return null;
    }

    /**
     * MÃ©todo que permite asociar los campos a la entidad TramiteInfoAdicional del tramite
     *
     * @return
     * @author leidy.gonzalez
     */
    public void asociarInfoCampoAdicional(EInfoAdicionalCampo campo, String valor) {

        if (this.tramiteInfoAdicionals != null &&
            !this.tramiteInfoAdicionals.isEmpty()) {

            boolean existe = false;

            for (TramiteInfoAdicional infoAdicional : this.tramiteInfoAdicionals) {

                if (infoAdicional.getCampo().equals(campo.getValor())) {

                    infoAdicional.setValor(valor);
                    existe = true;

                    break;
                }
            }

            if (!existe) {

                TramiteInfoAdicional infoAdicional = new TramiteInfoAdicional();

                infoAdicional.setId(null);
                infoAdicional.setCampo(campo.getValor());
                infoAdicional.setValor(valor);
                infoAdicional.setTramite(this);
                infoAdicional.setUsuarioLog(this.usuarioLog);
                infoAdicional.setFechaLog(new Date());

                this.tramiteInfoAdicionals.add(infoAdicional);
            }

        } else {

            this.tramiteInfoAdicionals = new ArrayList<TramiteInfoAdicional>();

            TramiteInfoAdicional infoAdicional = new TramiteInfoAdicional();

            infoAdicional.setId(null);
            infoAdicional.setCampo(campo.getValor());
            infoAdicional.setValor(valor);
            infoAdicional.setTramite(this);
            infoAdicional.setUsuarioLog(this.usuarioLog);
            infoAdicional.setFechaLog(new Date());

            this.tramiteInfoAdicionals.add(infoAdicional);
        }

    }
 
    /**
     * Determina si la rectificacion del tramite es de direccion
     *
     * @author leidy.gonzalez
     */
    @Transient
    public boolean isRectificacionDireccion() {
        if (this.tramiteRectificacions != null && !this.tramiteRectificacions.isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteRectificacions) {
                if (EDatoRectificarNombre.DIRECCION.getCodigo().equals(tr.getDatoRectificar().getNombre())
                        || EDatoRectificarNombre.DIRECCION_PRINCIPAL.getCodigo().equals(tr.getDatoRectificar().getNombre())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Transient
    public boolean isForProcess() {
        if((this.isEsComplementacion() || this.isPrimera()) && !getProcesoInstanciaId().contains("_PI:")){
            return false;
        }else{
            return true;
        }
    }

    public Object clone1() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

// end of class    
}
