package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the REGION_CAPTURA_OFERTA database table.
 *
 * @modified pedro.garcia + adición de secuencia para el id + cambio de tipo de dato del id: de long
 * a Long
 *
 * @modified rodrigo.hernandez + adición de método transient para obtener cadena de códigos de
 * manzanas/veredas usada en el Visor GIS
 */
@Entity
@Table(name = "REGION_CAPTURA_OFERTA")
public class RegionCapturaOferta implements Serializable {

    private static final long serialVersionUID = 190896764L;

    private Long id;
    private String asignadoRecolectorId;
    private String asignadoRecolectorNombre;
    private ComisionOferta comisionOferta;
    private String estado;
    private Date fechaAsignacionRecolector;
    private Date fechaLog;
    private String observaciones;
    private String procesoInstanciaId;
    private String usuarioLog;
    private List<DetalleCapturaOferta> detalleCapturaOfertas;
    private AreaCapturaOferta areaCapturaOferta;

    public RegionCapturaOferta() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RegionCapturaOferta_ID_SEQ")
    @SequenceGenerator(name = "RegionCapturaOferta_ID_SEQ",
        sequenceName = "REGION_CAPTURA_OFERTA_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ASIGNADO_RECOLECTOR_ID")
    public String getAsignadoRecolectorId() {
        return this.asignadoRecolectorId;
    }

    public void setAsignadoRecolectorId(String asignadoRecolectorId) {
        this.asignadoRecolectorId = asignadoRecolectorId;
    }

    @Column(name = "ASIGNADO_RECOLECTOR_NOMBRE")
    public String getAsignadoRecolectorNombre() {
        return this.asignadoRecolectorNombre;
    }

    public void setAsignadoRecolectorNombre(String asignadoRecolectorNombre) {
        this.asignadoRecolectorNombre = asignadoRecolectorNombre;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ASIGNACION_RECOLECTOR")
    public Date getFechaAsignacionRecolector() {
        return this.fechaAsignacionRecolector;
    }

    public void setFechaAsignacionRecolector(Date fechaAsignacionRecolector) {
        this.fechaAsignacionRecolector = fechaAsignacionRecolector;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to DetalleCapturaOferta
    @OneToMany(mappedBy = "regionCapturaOferta")
    public List<DetalleCapturaOferta> getDetalleCapturaOfertas() {
        return this.detalleCapturaOfertas;
    }

    public void setDetalleCapturaOfertas(List<DetalleCapturaOferta> detalleCapturaOfertas) {
        this.detalleCapturaOfertas = detalleCapturaOfertas;
    }

    //bi-directional many-to-one association to AreaCapturaOferta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AREA_CAPTURA_OFERTA_ID")
    public AreaCapturaOferta getAreaCapturaOferta() {
        return this.areaCapturaOferta;
    }

    public void setAreaCapturaOferta(AreaCapturaOferta areaCapturaOferta) {
        this.areaCapturaOferta = areaCapturaOferta;
    }

    @Column(name = "PROCESO_INSTANCIA_ID")
    public String getProcesoInstanciaId() {
        return this.procesoInstanciaId;
    }

    public void setProcesoInstanciaId(String procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    //bi-directional many-to-one association to ComisionOferta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMISION_OFERTA_ID")
    public ComisionOferta getComisionOferta() {
        return this.comisionOferta;
    }

    public void setComisionOferta(ComisionOferta comisionOferta) {
        this.comisionOferta = comisionOferta;
    }

    @Transient
    public String getCadenaDeManzanasVeredas() {
        String cadenaManzanasVeredas = "";

        if (this.detalleCapturaOfertas != null &&
            !this.detalleCapturaOfertas.isEmpty()) {

            for (DetalleCapturaOferta detalle : this.detalleCapturaOfertas) {

                if (detalle.getAreaCapturaOferta().getId()
                    .equals(this.areaCapturaOferta.getId())) {
                    if (!cadenaManzanasVeredas.isEmpty()) {
                        cadenaManzanasVeredas = cadenaManzanasVeredas
                            .concat("," + detalle.getManzanaVeredaCodigo());
                    } else {
                        cadenaManzanasVeredas = detalle
                            .getManzanaVeredaCodigo();
                    }
                }
            }
        }
        return cadenaManzanasVeredas;
    }

}
