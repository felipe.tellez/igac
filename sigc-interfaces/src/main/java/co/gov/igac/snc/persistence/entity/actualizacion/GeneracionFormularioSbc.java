package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * The persistent class for the GENERACION_FORMULARIO_SBC database table.
 *
 * @author javier.barajas Creacion entity GeneracionFormularioSBC
 */
@Entity
@Table(name = "GENERACION_FORMULARIO_SBC")
public class GeneracionFormularioSbc implements Serializable {

    private static final long serialVersionUID = 3769824482066464313L;
    private Long id;
    private Long cantidadPredios;
    private Date fechaGeneracion;
    private Date fechaLog;
    private Documento documento;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private List<PredioFormularioSbc> predioFormularioSbcs;

    public GeneracionFormularioSbc() {
        this.predioFormularioSbcs = new ArrayList<PredioFormularioSbc>();
    }

    @Id
    @SequenceGenerator(name = "GENERACION_FORMULARIO_S_ID_GENERATOR", sequenceName =
        "GENERACION_FORMULARIO_S_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "GENERACION_FORMULARIO_S_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CANTIDAD_PREDIOS")
    public Long getCantidadPredios() {
        return this.cantidadPredios;
    }

    public void setCantidadPredios(Long cantidadPredios) {
        this.cantidadPredios = cantidadPredios;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_GENERACION")
    public Date getFechaGeneracion() {
        return this.fechaGeneracion;
    }

    public void setFechaGeneracion(Date fechaGeneracion) {
        this.fechaGeneracion = fechaGeneracion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to PredioFormularioSbc
    @OneToMany(mappedBy = "generacionFormularioSbc")
    public List<PredioFormularioSbc> getPredioFormularioSbcs() {
        return this.predioFormularioSbcs;
    }

    public void setPredioFormularioSbcs(List<PredioFormularioSbc> predioFormularioSbcs) {
        this.predioFormularioSbcs = predioFormularioSbcs;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID")
    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }
}
