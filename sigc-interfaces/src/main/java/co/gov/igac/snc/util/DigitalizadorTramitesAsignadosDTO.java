package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Clase DTO que contiene datos de los datos de los trámites asignados a un digitalizador en el
 * proceso de conservación y el proceso de depuración.
 *
 * @author david.cifuentes
 *
 */
public class DigitalizadorTramitesAsignadosDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -237836781561692187L;

    private String funcionario;
    private Long tramitesConservacion;
    private Long tramitesDepuracion;
    private String funcionarioId;

    public DigitalizadorTramitesAsignadosDTO() {

    }

    public DigitalizadorTramitesAsignadosDTO(String funcionario,
        Long tramitesConservacion, Long tramitesDepuracion,
        String funcionarioId) {
        this.funcionario = funcionario;
        this.tramitesConservacion = tramitesConservacion;
        this.tramitesDepuracion = tramitesDepuracion;
        this.funcionarioId = funcionarioId;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public Long getTramitesConservacion() {
        return tramitesConservacion;
    }

    public void setTramitesConservacion(Long tramitesConservacion) {
        this.tramitesConservacion = tramitesConservacion;
    }

    public Long getTramitesDepuracion() {
        return tramitesDepuracion;
    }

    public void setTramitesDepuracion(Long tramitesDepuracion) {
        this.tramitesDepuracion = tramitesDepuracion;
    }

    public String getFuncionarioId() {
        return funcionarioId;
    }

    public void setFuncionarioId(String funcionarioId) {
        this.funcionarioId = funcionarioId;
    }

}
