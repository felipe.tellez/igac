package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 *
 * @author lorena.salamanca
 *
 */
public class ZonasFisicasGeoeconomicasVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1167842632452580660L;

    private String numeroPredial;
    private String codigoFisica;
    private String codigoGeoeconomica;
    private String area;

    public String getCodigoFisica() {
        return codigoFisica;
    }

    public void setCodigoFisica(String codigoFisica) {
        this.codigoFisica = codigoFisica;
    }

    public String getCodigoGeoeconomica() {
        return codigoGeoeconomica;
    }

    public void setCodigoGeoeconomica(String codigoGeoeconomica) {
        this.codigoGeoeconomica = codigoGeoeconomica;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

}
