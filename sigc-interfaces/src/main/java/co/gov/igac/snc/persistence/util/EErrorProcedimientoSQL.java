package co.gov.igac.snc.persistence.util;

/**
 * Enumeración para identificar los errores enviados en los cursores de error de los procedimientos
 * almacenados.
 *
 * @author felipe.cadena
 */
public enum EErrorProcedimientoSQL {

    //TODO::felipe.cadena::definir los tipos de errores con el DBA    
    COD_100("No se encontraron datos asociados a la consulta.");

    private String codigo;

    EErrorProcedimientoSQL(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
