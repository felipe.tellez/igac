package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the OFERTA_SERVICIO_COMUNAL database table.
 *
 */
@Entity
@Table(name = "OFERTA_SERVICIO_COMUNAL")
public class OfertaServicioComunal implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String servicioComunal;
    private String usuarioLog;
    private OfertaInmobiliaria ofertaInmobiliaria;

    public OfertaServicioComunal() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OFERTA_SERVICIO_COMUNAL_ID_SEQ")
    @SequenceGenerator(name = "OFERTA_SERVICIO_COMUNAL_ID_SEQ", sequenceName =
        "OFERTA_SERVICIO_COMUNAL_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "SERVICIO_COMUNAL")
    public String getServicioComunal() {
        return this.servicioComunal;
    }

    public void setServicioComunal(String servicioComunal) {
        this.servicioComunal = servicioComunal;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to OfertaInmobiliaria
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID")
    public OfertaInmobiliaria getOfertaInmobiliaria() {
        return this.ofertaInmobiliaria;
    }

    public void setOfertaInmobiliaria(OfertaInmobiliaria ofertaInmobiliaria) {
        this.ofertaInmobiliaria = ofertaInmobiliaria;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((servicioComunal == null) ? 0 : servicioComunal.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OfertaServicioComunal other = (OfertaServicioComunal) obj;
        if (servicioComunal == null) {
            if (other.servicioComunal != null) {
                return false;
            }
        } else if (!servicioComunal.equals(other.servicioComunal)) {
            return false;
        }
        return true;
    }

}
