package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.Hibernate;
import org.hibernate.LazyInitializationException;

import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EMovimientoAvaluoTipo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;

/**
 * The persistent class for the AVALUO database table.
 *
 */

/*
 * @modified pedro.garcia - adición de método isTienePrediosEnDiferentesDeptos - adición de método
 * isTienePrediosEnDiferentesMunicipios - cambio de tipo de dato para valorCotizacion: de Long a
 * Double - cambio de tipo de dato para 'avaluoComercial' (de Long a Double)
 *
 * @modified felipe.cadena - cambio de tipo de dato para 'valorPreliminar': de Long a Double -
 * cambio de tipo de dato para 'valorIva': de Long a Double - se agrega un campo transient para
 * almacenar el valor avaluo comercial con formato moneda
 *
 * @modified christian.rodriguez - Adición de método getSolicitante - Adición de relación con {@link
 * AvaluoMovimiento} - Adición de método getCancelacion - Adición de método equals - Adición de
 * método hashcode - Adición de método getValorhonorariosIGAC - Adición de método
 * isTieneMultiplesAvaluadores - Adición de método getAvaluadores - Adición de método
 * getProfesionalControlCalidadSedeCentral - Adición de método getReservadoBoolean - Adición de
 * método setReservadoBoolean
 *
 * @modified rodrigo.hernandez - Adición de atributo realizarContrato - Adición de atributo
 * foliosAnexos
 *
 */
@Entity
@Table(name = "AVALUO", schema = "SNC_AVALUOS")
public class Avaluo implements Serializable {

    private static final long serialVersionUID = -614674725698405189L;

    private Long id;
    private Long anioAprobacion;
    private Long anioAvaluoCalculado;
    private Long anioAvaluoComercial;
    private String aprobado;
    private Long avaluoCalculado;
    private Double avaluoComercial;
    private Long avaluoOrigenImpugnacion;
    private Long avaluoOrigenRevision;
    private Long avaluoResultadoImpugnacion;
    private Long avaluoResultadoRevision;
    private String bloqueado;
    private Long calificacion;
    private String claseSuelo;
    private Long danioEmergente;
    private String externo;
    private Date fechaAprobacion;
    private Date fechaLog;
    private Long lucroCesante;
    private String numeroRadicacionImpugnacion;
    private String numeroRadicacionOriImpugnac;
    private String numeroRadicacionOriRevision;
    private String numeroRadicacionRevision;
    private String observaciones;
    private Long plazoEjecucion;
    private String requiereControlCalidad;
    private String requiereControlCalidadSede;
    private String requiereControlCalidadTerr;
    private String reservado;
    private String resumenNormativo;
    private String secRadicado;
    private String tipoAvaluo;
    private Tramite tramite;
    private String unidadMedidaAreaTerreno;
    private String usuarioLog;
    private Double valorCotizacion;
    private Double valorIva;
    private Long valorPredio;
    private Double valorPreliminar;
    private Long valorTotalAnexos;
    private Long valorTotalConstrucciones;
    private Long valorTotalCultivos;
    private Long valorTotalMaquinaria;
    private Long valorTotalTerreno;
    private String estructuraOrganizacionalCod;

    private List<AvaluoPredio> avaluoPredios;
    private List<AvaluoMovimiento> avaluoMovimientos;
    private List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals;
    private List<ContratoPagoAvaluo> contratoPagoAvaluos;
    private AvaluoLiquidacionCosto avaluoLiquidacionCosto;
    private List<AvaluoPermiso> avaluoPermisos;

    private String avaluoComercialAsString;

    private String realizarContrato;
    private Long foliosAnexos;

    public Avaluo() {
        this.reservado = ESiNo.NO.getCodigo();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVALUO_ID_GENERATOR")
    @SequenceGenerator(name = "AVALUO_ID_GENERATOR", sequenceName = "AVALUO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ANIO_APROBACION")
    public Long getAnioAprobacion() {
        return this.anioAprobacion;
    }

    public void setAnioAprobacion(Long anioAprobacion) {
        this.anioAprobacion = anioAprobacion;
    }

    @Column(name = "ANIO_AVALUO_CALCULADO")
    public Long getAnioAvaluoCalculado() {
        return this.anioAvaluoCalculado;
    }

    public void setAnioAvaluoCalculado(Long anioAvaluoCalculado) {
        this.anioAvaluoCalculado = anioAvaluoCalculado;
    }

    @Column(name = "ANIO_AVALUO_COMERCIAL")
    public Long getAnioAvaluoComercial() {
        return this.anioAvaluoComercial;
    }

    public void setAnioAvaluoComercial(Long anioAvaluoComercial) {
        this.anioAvaluoComercial = anioAvaluoComercial;
    }

    public String getAprobado() {
        return this.aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    @Column(name = "AVALUO_CALCULADO")
    public Long getAvaluoCalculado() {
        return this.avaluoCalculado;
    }

    public void setAvaluoCalculado(Long avaluoCalculado) {
        this.avaluoCalculado = avaluoCalculado;
    }

    @Column(name = "AVALUO_COMERCIAL")
    public Double getAvaluoComercial() {
        return this.avaluoComercial;
    }

    public void setAvaluoComercial(Double avaluoComercial) {
        this.avaluoComercial = avaluoComercial;
    }

    @Column(name = "AVALUO_ORIGEN_IMPUGNACION")
    public Long getAvaluoOrigenImpugnacion() {
        return this.avaluoOrigenImpugnacion;
    }

    public void setAvaluoOrigenImpugnacion(Long avaluoOrigenImpugnacion) {
        this.avaluoOrigenImpugnacion = avaluoOrigenImpugnacion;
    }

    @Column(name = "AVALUO_ORIGEN_REVISION")
    public Long getAvaluoOrigenRevision() {
        return this.avaluoOrigenRevision;
    }

    public void setAvaluoOrigenRevision(Long avaluoOrigenRevision) {
        this.avaluoOrigenRevision = avaluoOrigenRevision;
    }

    @Column(name = "AVALUO_RESULTADO_IMPUGNACION")
    public Long getAvaluoResultadoImpugnacion() {
        return this.avaluoResultadoImpugnacion;
    }

    public void setAvaluoResultadoImpugnacion(Long avaluoResultadoImpugnacion) {
        this.avaluoResultadoImpugnacion = avaluoResultadoImpugnacion;
    }

    @Column(name = "AVALUO_RESULTADO_REVISION")
    public Long getAvaluoResultadoRevision() {
        return this.avaluoResultadoRevision;
    }

    public void setAvaluoResultadoRevision(Long avaluoResultadoRevision) {
        this.avaluoResultadoRevision = avaluoResultadoRevision;
    }

    public String getBloqueado() {
        return this.bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    public Long getCalificacion() {
        return this.calificacion;
    }

    public void setCalificacion(Long calificacion) {
        this.calificacion = calificacion;
    }

    @Column(name = "CLASE_SUELO")
    public String getClaseSuelo() {
        return this.claseSuelo;
    }

    public void setClaseSuelo(String claseSuelo) {
        this.claseSuelo = claseSuelo;
    }

    @Column(name = "DANIO_EMERGENTE")
    public Long getDanioEmergente() {
        return this.danioEmergente;
    }

    public void setDanioEmergente(Long danioEmergente) {
        this.danioEmergente = danioEmergente;
    }

    public String getExterno() {
        return this.externo;
    }

    public void setExterno(String externo) {
        this.externo = externo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_APROBACION")
    public Date getFechaAprobacion() {
        return this.fechaAprobacion;
    }

    public void setFechaAprobacion(Date fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "LUCRO_CESANTE")
    public Long getLucroCesante() {
        return this.lucroCesante;
    }

    public void setLucroCesante(Long lucroCesante) {
        this.lucroCesante = lucroCesante;
    }

    @Column(name = "NUMERO_RADICACION_IMPUGNACION")
    public String getNumeroRadicacionImpugnacion() {
        return this.numeroRadicacionImpugnacion;
    }

    public void setNumeroRadicacionImpugnacion(String numeroRadicacionImpugnacion) {
        this.numeroRadicacionImpugnacion = numeroRadicacionImpugnacion;
    }

    @Column(name = "NUMERO_RADICACION_ORI_IMPUGNAC")
    public String getNumeroRadicacionOriImpugnac() {
        return this.numeroRadicacionOriImpugnac;
    }

    public void setNumeroRadicacionOriImpugnac(String numeroRadicacionOriImpugnac) {
        this.numeroRadicacionOriImpugnac = numeroRadicacionOriImpugnac;
    }

    @Column(name = "NUMERO_RADICACION_ORI_REVISION")
    public String getNumeroRadicacionOriRevision() {
        return this.numeroRadicacionOriRevision;
    }

    public void setNumeroRadicacionOriRevision(String numeroRadicacionOriRevision) {
        this.numeroRadicacionOriRevision = numeroRadicacionOriRevision;
    }

    @Column(name = "NUMERO_RADICACION_REVISION")
    public String getNumeroRadicacionRevision() {
        return this.numeroRadicacionRevision;
    }

    public void setNumeroRadicacionRevision(String numeroRadicacionRevision) {
        this.numeroRadicacionRevision = numeroRadicacionRevision;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "PLAZO_EJECUCION")
    public Long getPlazoEjecucion() {
        return this.plazoEjecucion;
    }

    public void setPlazoEjecucion(Long plazoEjecucion) {
        this.plazoEjecucion = plazoEjecucion;
    }

    @Column(name = "REQUIERE_CONTROL_CALIDAD")
    public String getRequiereControlCalidad() {
        return this.requiereControlCalidad;
    }

    public void setRequiereControlCalidad(String requiereControlCalidad) {
        this.requiereControlCalidad = requiereControlCalidad;
    }

    @Column(name = "REQUIERE_CONTROL_CALIDAD_SEDE")
    public String getRequiereControlCalidadSede() {
        return this.requiereControlCalidadSede;
    }

    public void setRequiereControlCalidadSede(String requiereControlCalidadSede) {
        this.requiereControlCalidadSede = requiereControlCalidadSede;
    }

    @Column(name = "REQUIERE_CONTROL_CALIDAD_TERR")
    public String getRequiereControlCalidadTerr() {
        return this.requiereControlCalidadTerr;
    }

    public void setRequiereControlCalidadTerr(String requiereControlCalidadTerr) {
        this.requiereControlCalidadTerr = requiereControlCalidadTerr;
    }

    public String getReservado() {
        return this.reservado;
    }

    public void setReservado(String reservado) {
        this.reservado = reservado;
    }

    @Column(name = "RESUMEN_NORMATIVO")
    public String getResumenNormativo() {
        return this.resumenNormativo;
    }

    public void setResumenNormativo(String resumenNormativo) {
        this.resumenNormativo = resumenNormativo;
    }

    @Column(name = "SEC_RADICADO")
    public String getSecRadicado() {
        return this.secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    @Column(name = "TIPO_AVALUO")
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_COTIZACION")
    public Double getValorCotizacion() {
        return this.valorCotizacion;
    }

    public void setValorCotizacion(Double valorCotizacion) {
        this.valorCotizacion = valorCotizacion;
    }

    @Column(name = "VALOR_IVA")
    public Double getValorIva() {
        return this.valorIva;
    }

    public void setValorIva(Double valorIva) {
        this.valorIva = valorIva;
    }

    @Column(name = "VALOR_PREDIO")
    public Long getValorPredio() {
        return this.valorPredio;
    }

    public void setValorPredio(Long valorPredio) {
        this.valorPredio = valorPredio;
    }

    @Column(name = "VALOR_PRELIMINAR")
    public Double getValorPreliminar() {
        return this.valorPreliminar;
    }

    public void setValorPreliminar(Double valorPreliminar) {
        this.valorPreliminar = valorPreliminar;
    }

    @Column(name = "VALOR_TOTAL_ANEXOS")
    public Long getValorTotalAnexos() {
        return this.valorTotalAnexos;
    }

    public void setValorTotalAnexos(Long valorTotalAnexos) {
        this.valorTotalAnexos = valorTotalAnexos;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCIONES")
    public Long getValorTotalConstrucciones() {
        return this.valorTotalConstrucciones;
    }

    public void setValorTotalConstrucciones(Long valorTotalConstrucciones) {
        this.valorTotalConstrucciones = valorTotalConstrucciones;
    }

    @Column(name = "VALOR_TOTAL_CULTIVOS")
    public Long getValorTotalCultivos() {
        return this.valorTotalCultivos;
    }

    public void setValorTotalCultivos(Long valorTotalCultivos) {
        this.valorTotalCultivos = valorTotalCultivos;
    }

    @Column(name = "VALOR_TOTAL_MAQUINARIA")
    public Long getValorTotalMaquinaria() {
        return this.valorTotalMaquinaria;
    }

    public void setValorTotalMaquinaria(Long valorTotalMaquinaria) {
        this.valorTotalMaquinaria = valorTotalMaquinaria;
    }

    @Column(name = "VALOR_TOTAL_TERRENO")
    public Long getValorTotalTerreno() {
        return this.valorTotalTerreno;
    }

    public void setValorTotalTerreno(Long valorTotalTerreno) {
        this.valorTotalTerreno = valorTotalTerreno;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD")
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    // bi-directional many-to-one association to AvaluoPredio
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "avaluo")
    public List<AvaluoPredio> getAvaluoPredios() {
        return this.avaluoPredios;
    }

    public void setAvaluoPredios(List<AvaluoPredio> avaluoPredios) {
        this.avaluoPredios = avaluoPredios;
    }

    // bi-directional many-to-one association to AvaluoPredio
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "avaluo")
    public List<AvaluoMovimiento> getAvaluoMovimientos() {
        return this.avaluoMovimientos;
    }

    public void setAvaluoMovimientos(List<AvaluoMovimiento> avaluoMovimientos) {
        this.avaluoMovimientos = avaluoMovimientos;
    }

    @OneToMany(mappedBy = "avaluo")
    public List<AvaluoAsignacionProfesional> getAvaluoAsignacionProfesionals() {
        return this.avaluoAsignacionProfesionals;
    }

    public void setAvaluoAsignacionProfesionals(
        List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals) {
        this.avaluoAsignacionProfesionals = avaluoAsignacionProfesionals;
    }

    // bi-directional many-to-one association to ContratoPagoAvaluo
    @OneToMany(mappedBy = "avaluo")
    public List<ContratoPagoAvaluo> getContratoPagoAvaluos() {
        return this.contratoPagoAvaluos;
    }

    public void setContratoPagoAvaluos(
        List<ContratoPagoAvaluo> contratoPagoAvaluos) {
        this.contratoPagoAvaluos = contratoPagoAvaluos;
    }

    // bi-directional many-to-one association to AvaluoLiquidacionCosto
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "avaluo")
    public AvaluoLiquidacionCosto getAvaluoLiquidacionCosto() {
        return this.avaluoLiquidacionCosto;
    }

    public void setAvaluoLiquidacionCosto(
        AvaluoLiquidacionCosto avaluoLiquidacionCosto) {
        this.avaluoLiquidacionCosto = avaluoLiquidacionCosto;
    }

    // bi-directional many-to-one association to AvaluoPermiso
    @OneToMany(mappedBy = "avaluo")
    public List<AvaluoPermiso> getAvaluoPermisos() {
        return this.avaluoPermisos;
    }

    public void setAvaluoPermisos(List<AvaluoPermiso> avaluoPermisos) {
        this.avaluoPermisos = avaluoPermisos;
    }

    @Column(name = "UNIDAD_MEDIDA_AREA_TERRENO")
    public String getUnidadMedidaAreaTerreno() {
        return this.unidadMedidaAreaTerreno;
    }

    public void setUnidadMedidaAreaTerreno(String unidadMedidaAreaTerreno) {
        this.unidadMedidaAreaTerreno = unidadMedidaAreaTerreno;
    }

    @Column(name = "REALIZAR_CONTRATO")
    public String isRealizarContrato() {
        return realizarContrato;
    }

    public void setRealizarContrato(String realizarContrato) {
        this.realizarContrato = realizarContrato;
    }

    @Column(name = "FOLIOS_ANEXOS")
    public Long getFoliosAnexos() {
        return foliosAnexos;
    }

    public void setFoliosAnexos(Long foliosAnexos) {
        this.foliosAnexos = foliosAnexos;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Determina si el avalúo tiene predios en diferentes departamentos
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isTienePrediosEnDiferentesDeptos() {

        boolean answer = false;
        String codigoDepartamento1, codigoDepartamento2;

        try {
            if (null != this.avaluoPredios && !this.avaluoPredios.isEmpty() &&
                this.avaluoPredios.size() > 1) {

                codigoDepartamento1 = this.avaluoPredios.get(0).getDepartamento().getCodigo();
                for (AvaluoPredio ap : this.avaluoPredios) {
                    codigoDepartamento2 = ap.getDepartamento().getCodigo();
                    if (!codigoDepartamento1.equals(codigoDepartamento2)) {
                        answer = true;
                        break;
                    }
                }
            }
        } //N: si no se hace catch de esto, cuando no se ha hecho fetch de los predios 
        //   no es capaz de comparar this.avaluoPredios con null u otra cosa y se totea
        catch (LazyInitializationException liEx) {
            answer = false;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Determina si el avalúo tiene predios en diferentes municipios
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isTienePrediosEnDiferentesMunicipios() {

        boolean answer = false;
        String codigoMunicipio1, codigoMunicipio2;

        try {
            if (this.avaluoPredios != null && !this.avaluoPredios.isEmpty() &&
                this.avaluoPredios.size() > 1) {

                codigoMunicipio1 = this.avaluoPredios.get(0).getMunicipio().getCodigo();
                for (AvaluoPredio ap : this.avaluoPredios) {
                    codigoMunicipio2 = ap.getMunicipio().getCodigo();
                    if (!codigoMunicipio1.equals(codigoMunicipio2)) {
                        answer = true;
                        break;
                    }
                }
            }
        } //N: si no se hace catch, cuando no se ha hecho fetch de los predios 
        //   no es capaz de comparar this.avaluoPredios con null u otra cosa y se produce una
        //   excepción LazyInitializationException
        catch (Exception liEx) {
            answer = false;
        }

        return answer;
    }

    // ---------------------------------------------------------------------------
    /**
     * Retorna el {@link Solicitante} del avalúo. Requiere que el tramite y los solicitantes
     * asociados al tramite hayan sido cargados OJO: los solicitantes los lee de la solicitud
     *
     * @author christian.rodriguez
     * @return {@link Solicitante} del avalúo
     */
    @Transient
    public SolicitanteSolicitud getSolicitante() {
        if (Hibernate.isInitialized(this.tramite) &&
            Hibernate.isInitialized(this.tramite.getSolicitud()) &&
            Hibernate.isInitialized(this.tramite.getSolicitud()
                .getSolicitanteSolicituds())) {
            for (SolicitanteSolicitud sol : this.tramite.getSolicitud().getSolicitanteSolicituds()) {
                if (sol.getRelacion().equals(ESolicitanteSolicitudRelac.PROPIETARIO.toString())) {
                    return sol;
                }
            }
        }
        return null;
    }

    // ---------------------------------------------------------------------------
    /**
     * Retorna el {@link AvaluoMovimiento} correspondiente a la cancelación del avalúo
     *
     * @author christian.rodriguez
     * @return el {@link AvaluoMovimiento} correspondiente a la cancelación del avalúo, null si no
     * tiene movimientos de cancelacion
     */
    @Transient
    public AvaluoMovimiento getCancelacion() {
        if (Hibernate.isInitialized(this.avaluoMovimientos)) {
            for (AvaluoMovimiento movimiento : this.avaluoMovimientos) {
                if (movimiento.getTipoMovimiento().equalsIgnoreCase(
                    EMovimientoAvaluoTipo.CANCELACION.getCodigo())) {
                    return movimiento;
                }
            }
        }
        return null;
    }

    // ---------------------------------------------------------------------------
    /**
     * Retorna el valor de los honorarios del avaluo
     *
     * @pre debe haberse hecho el fetch del {@link AvaluoLiquidacionCosto} o de lo contrario
     * devolvera 0.0
     *
     * @author christian.rodriguez
     * @return valor de los honorarios del avaluo, 0.0 si no se inicializó el objeto
     * {@link AvaluoLiquidacionCosto} asociado al avaluo o no se tiene una liquidación relacionada
     */
    @Transient
    public Double getValorHonorariosIGAC() {
        if (Hibernate.isInitialized(this.avaluoLiquidacionCosto) &&
            this.avaluoLiquidacionCosto != null) {
            return this.avaluoLiquidacionCosto.getHonorariosIgac();
        }
        return 0.0;
    }

    // ---------------------------------------------------------------------------
    /**
     * Determina si el avalúo tiene asignados varios avaluadores
     *
     * @author christian.rodriguez
     * @pre se debe cargar las {@link AvaluoAsignacionProfesional} asociadas
     * @return true si tiene asignados varios avaluadores, false si no o si no se cargaron las
     * asignaciónes previamente
     */
    @Transient
    public boolean isTieneMultiplesAvaluadores() {
        if (Hibernate.isInitialized(this.avaluoAsignacionProfesionals)) {
            int numeroAvaluadores = this.getAvaluadores().size();

            if (numeroAvaluadores > 1) {
                return true;
            }
        }
        return false;
    }

    // ---------------------------------------------------------------------------
    /**
     * Retorna la lista de los avaluadores activos (sin fecha_hasta en la asignación) asignados al
     * avaluo
     *
     * @author christian.rodriguez
     * @pre debe haberse cargado los {@link AvaluoAsignacionProfesional} y los repsectivos
     * {@link ProfesionalAvaluo} de cada asignacion
     * @return una lista con los {@link ProfesionalAvaluo} del avaluo, null si no tiene asignaciones
     * o si no se cargaron los objetos requeridos
     */
    @Transient
    public List<ProfesionalAvaluo> getAvaluadores() {

        if (Hibernate.isInitialized(this.avaluoAsignacionProfesionals)) {
            List<ProfesionalAvaluo> avaluadores = new ArrayList<ProfesionalAvaluo>();
            for (AvaluoAsignacionProfesional asignacion : this.avaluoAsignacionProfesionals) {
                if (asignacion.getActividad().equalsIgnoreCase(
                    EAvaluoAsignacionProfActi.AVALUAR
                        .getCodigo())) {
                    if (Hibernate.isInitialized(asignacion
                        .getProfesionalAvaluo())) {
                        if (asignacion.getFechaHasta() == null) {
                            avaluadores.add(asignacion.getProfesionalAvaluo());
                        }
                    }
                }
            }
            return avaluadores;
        }
        return null;
    }

    // ---------------------------------------------------------------------------
    /**
     * Retorna el profesional de control de calidad de sede central
     *
     * @author christian.rodriguez
     * @pre debe haberse cargado los {@link AvaluoAsignacionProfesional} y los respectivos
     * {@link ProfesionalAvaluo} de cada asignacion
     * @return un {@link ProfesionalAvaluo} del avaluo, null si no tiene asignaciones de control
     * calidad de sede central o si no se cargaron los objetos requeridos
     */
    @Transient
    public ProfesionalAvaluo getProfesionalControlCalidadSedeCentral() {
        if (Hibernate.isInitialized(this.avaluoAsignacionProfesionals)) {
            for (AvaluoAsignacionProfesional asignacion : this.avaluoAsignacionProfesionals) {
                if (asignacion.getActividad().equalsIgnoreCase(
                    EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo())) {
                    if (Hibernate.isInitialized(asignacion
                        .getProfesionalAvaluo())) {
                        return asignacion.getProfesionalAvaluo();
                    }
                }
            }
        }
        return null;
    }

    /**
     * Método que genera una cadena de texto con los número prediales de los predios del avalúo
     * separados por coma
     *
     * @author christian.rodriguez
     * @return String con los números prediales del avalúo
     */
    @Transient
    public String getCadenaDeNumerosPrediales() {
        String cadenaNumerosPrediales = "";

        if (Hibernate.isInitialized(this.avaluoPredios) &&
            !this.avaluoPredios.isEmpty()) {

            for (AvaluoPredio predio : this.avaluoPredios) {

                if (predio.getNumeroPredial() != null &&
                    !predio.getNumeroPredial().isEmpty()) {

                    if (!cadenaNumerosPrediales.isEmpty()) {
                        cadenaNumerosPrediales = cadenaNumerosPrediales
                            .concat("," + predio.getNumeroPredial());
                    } else {
                        cadenaNumerosPrediales = predio.getNumeroPredial();
                    }
                }
            }
        }
        return cadenaNumerosPrediales;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Avaluo other = (Avaluo) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el {@link Solicitante} del avalúo. Requiere que el tramite y los solicitantes
     * asociados al tramite hayan sido cargados OJO: en Avalúos el solicitante del trámite es el
     * mismo de la solicitud
     *
     * @author pedro.garcia
     * @return {@link Solicitante} del avalúo
     */
    @Transient
    public SolicitanteTramite getSolicitanteTramite() {
        if (Hibernate.isInitialized(this.tramite) &&
            Hibernate.isInitialized(this.tramite.getSolicitanteTramites())) {
            for (SolicitanteTramite sol : this.tramite.getSolicitanteTramites()) {
                if (sol.getRelacion().equals(ESolicitanteSolicitudRelac.PROPIETARIO.toString())) {
                    return sol;
                }
            }
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el {@link Solicitante} del avalúo que tenga como rol ser el rerpesentante legal del
     * solicitante. Requiere que el tramite y los solicitantes asociados al tramite hayan sido
     * cargados OJO: en Avalúos el solicitante del trámite es el mismo de la solicitud
     *
     * @author pedro.garcia
     * @return {@link Solicitante} del avalúo
     */
    @Transient
    public SolicitanteTramite getRepresentanteLegalSolicitanteTramite() {
        if (Hibernate.isInitialized(this.tramite) &&
            Hibernate.isInitialized(this.tramite.getSolicitanteTramites())) {
            for (SolicitanteTramite sol : this.tramite.getSolicitanteTramites()) {
                if (sol.getRelacion().equals(
                    ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL.toString())) {
                    return sol;
                }
            }
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el nombre del tipo de tramite de avaluo Requiere que el tramite haya sido cargados
     *
     * @author felipe.cadena
     * @return Nombre del tramite de avalúo
     */
    @Transient
    public String getNombreTipoTramite() {
        for (ETramiteTipoTramite tt : ETramiteTipoTramite.values()) {
            if (tt.getCodigo().equals(this.tramite.getTipoTramite())) {
                return tt.getNombre();
            }
        }
        return null;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Retorna la lista con los {@link AvaluoPermiso} activos es decir que su fecha inicio y fecha
     * fin contengan la fecha actual
     *
     * @author christian.rodriguez
     * @return lista con los {@link AvaluoPermiso} activos
     */
    @Transient
    public List<AvaluoPermiso> getAvaluoPermisosActivos() {

        List<AvaluoPermiso> permisosActivos = null;

        if (Hibernate.isInitialized(this.avaluoPermisos)) {
            permisosActivos = new ArrayList<AvaluoPermiso>();

            Date fechaActual = new Date();

            for (AvaluoPermiso permiso : this.avaluoPermisos) {
                if (permiso.getFechaDesde().before(fechaActual) &&
                    permiso.getFechaHasta().after(fechaActual)) {
                    permisosActivos.add(permiso);
                }
            }
        }
        return permisosActivos;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método utilizado para acceder al campo {@link #reservado} como si fuera un boolean, se creo
     * con el fin de poder editar este campo desde una tabla
     *
     * @author christian.rodriguez
     * @return true si el campo {@link #reservado} es SI, false en otro caso
     */
    @Transient
    public boolean isReservadoBoolean() {
        if (this.reservado != null &&
            this.reservado.equals(ESiNo.SI.getCodigo())) {
            return true;
        }
        return false;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método utilizado para asignar al campo {@link #reservado} como si fuera un boolean, se creo
     * con el fin de poder editar este campo desde un selectoncheckbox
     *
     * @author christian.rodriguez
     */
    public void setReservadoBoolean(boolean reservadoBoolean) {
        if (reservadoBoolean) {
            this.reservado = ESiNo.SI.getCodigo();
        } else {
            this.reservado = ESiNo.NO.getCodigo();
        }
    }

    /**
     * El valor del campo se debe procesar en MB donde se use
     *
     * @author felipe.cadena
     */
    @Transient
    public String getAvaluoComercialAsString() {
        return this.avaluoComercialAsString;
    }

    public void setAvaluoComercialAsString(String valor) {
        this.avaluoComercialAsString = valor;
    }

    /**
     * Retorna el AvaluoMovimiento más reciente de tipo AMPLIACION
     *
     * @author rodrigo.hernandez
     *
     */
    @Transient
    public AvaluoMovimiento getUltimoMovimientoAmpliacion() {

        AvaluoMovimiento movimiento = new AvaluoMovimiento();

        int contador = 0;

        /*
         * Se valida que la lista de movimientos esté vacía
         */
        if (this.avaluoMovimientos.isEmpty()) {
            /*
             * Si sí, se crea un momvimiento de tipo AMPLIACION
             */
            movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.AMPLIACION
                .getCodigo());
        } else {
            /*
             * Si no, se recorre la lista en busca de un movimiento de tipo AMPLIACION
             */
            for (AvaluoMovimiento am : this.avaluoMovimientos) {
                if (am.getTipoMovimiento().equals(
                    EMovimientoAvaluoTipo.AMPLIACION.getCodigo())) {
                    movimiento = am;
                    contador++;
                }
            }

            /*
             * Si no se encuentra dentro de la lista un movimiento de tipo AMPLIACION, se crea uno
             * nuevo
             */
            if (contador == 0) {
                movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.AMPLIACION
                    .getCodigo());
            }
        }

        return movimiento;
    }

    /**
     * Retorna el AvaluoMovimiento más reciente de tipo SUSPENSION
     *
     * @author rodrigo.hernandez
     *
     */
    @Transient
    public AvaluoMovimiento getUltimoMovimientoSuspension() {
        AvaluoMovimiento movimiento = new AvaluoMovimiento();

        int contador = 0;

        /*
         * Se valida que la lista de movimientos esté vacía
         */
        if (this.avaluoMovimientos.isEmpty()) {
            /*
             * Si sí, se crea un momvimiento de tipo SUSPENSION
             */
            movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.SUSPENCION
                .getCodigo());
        } else {
            /*
             * Si no, se recorre la lista en busca de un movimiento de tipo SUSPENSION
             */
            for (AvaluoMovimiento am : this.avaluoMovimientos) {
                if (am.getTipoMovimiento().equals(
                    EMovimientoAvaluoTipo.SUSPENCION.getCodigo())) {
                    movimiento = am;
                    contador++;
                }
            }

            /*
             * Si no se encuentra dentro de la lista un movimiento de tipo SUSPENSION, se crea uno
             * nuevo
             */
            if (contador == 0) {
                movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.SUSPENCION
                    .getCodigo());
            }
        }

        return movimiento;
    }

    /**
     * Retorna el AvaluoMovimiento más reciente de tipo REACTIVACION
     *
     * @author rodrigo.hernandez
     *
     */
    @Transient
    public AvaluoMovimiento getUltimoMovimientoReactivacion() {
        AvaluoMovimiento movimiento = new AvaluoMovimiento();

        int contador = 0;

        /*
         * Se valida que la lista de movimientos esté vacía
         */
        if (this.avaluoMovimientos.isEmpty()) {
            /*
             * Si sí, se crea un momvimiento de tipo REACTIVACION
             */
            movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.REACTIVACION
                .getCodigo());
        } else {
            /*
             * Si no, se recorre la lista en busca de un movimiento de tipo REACTIVACION
             */
            for (AvaluoMovimiento am : this.avaluoMovimientos) {
                if (am.getTipoMovimiento().equals(
                    EMovimientoAvaluoTipo.REACTIVACION.getCodigo())) {
                    movimiento = am;
                    contador++;
                }
            }

            /*
             * Si no se encuentra dentro de la lista un movimiento de tipo REACTIVACION, se crea uno
             * nuevo
             */
            if (contador == 0) {
                movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.REACTIVACION
                    .getCodigo());
            }
        }

        return movimiento;
    }

    /**
     * Método que compara los ids de los ultimos movimientos del avaluo para determinar si el avaluo
     * esta suspendido o no. </br> </br><b>OJO: </b> Se comparan los ids ya que estos indican el
     * momento del registro del movimiento. Es decir, entre mayor sea el id, mas reciente fue hecho
     * el movimiento del avalúo
     *
     * @author rodrigo.hernandez
     */
    @Transient
    public boolean isEstaSuspendido() {
        boolean result = true;

        /*
         * Se consultan los ultimos movimientos del avaluo
         */
        AvaluoMovimiento ultimoMovimientoAmpliacion = this
            .getUltimoMovimientoAmpliacion();
        AvaluoMovimiento ultimoMovimientoSuspension = this
            .getUltimoMovimientoSuspension();
        AvaluoMovimiento ultimoMovimientoReactivacion = this
            .getUltimoMovimientoReactivacion();

        /*
         * Se valida si el avaluo tiene un ultimo movimiento de SUSPENSION
         */
        if (ultimoMovimientoSuspension.getId() == null) {
            result = false;
        } else {
            /*
             * Se validan si los movimientos de AMPLIACION y REACTIVACION tienen ids
             */
            if (ultimoMovimientoAmpliacion.getId() != null &&
                ultimoMovimientoReactivacion.getId() != null) {
                /*
                 * Se valida si el id del movimiento de SUSPENSION es menor a los ids de los otros
                 * movimientos
                 */
                if (ultimoMovimientoSuspension.getId() < ultimoMovimientoAmpliacion
                    .getId() || ultimoMovimientoSuspension.getId() < ultimoMovimientoReactivacion
                    .getId()) {
                    result = false;
                }
            } /*
             * Si no, se procede a comparar con el movimiento de SUSPENSION con el movimiento que
             * tenga un id diferente a null.
             */ else {

                /*
                 * Se valida si el avaluo tiene un ultimo movimiento de AMPLIACION
                 */
                if (ultimoMovimientoAmpliacion.getId() != null) {
                    /*
                     * Se valida que el id del movimiento de SUSPENSION sea menor al id del
                     * movimiento de AMPLIACION
                     */
                    if (ultimoMovimientoSuspension.getId() < ultimoMovimientoAmpliacion
                        .getId()) {
                        result = false;
                    }
                }

                /*
                 * Se valida si el avaluo tiene un ultimo movimiento de REACTIVACION
                 */
                if (ultimoMovimientoReactivacion.getId() != null) {
                    /*
                     * Se valida que el id del movimiento de SUSPENSION sea menor al id del
                     * movimiento de REACTIVACION
                     */
                    if (ultimoMovimientoSuspension.getId() < ultimoMovimientoReactivacion
                        .getId()) {
                        result = false;
                    }
                }
            }
        }

        return result;
    }

}
