/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos actividades que puede realizar un profesional de avaluos en un comite
 * de avaluos. Corresponde al dominio AVALUO_COMITE_PROF_ACTI
 *
 * @author christian.rodriguez
 */
public enum EAvaluoComiteProfActi {

    AVALUADOR("AVALUADOR", "Avaluador"),
    ASISTENTE("ASISTENTE", "Asistente"),
    CONTROL_CALIDAD("CONTROL CALIDAD", "Control de calidad");

    private String codigo;
    private String valor;

    private EAvaluoComiteProfActi(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
