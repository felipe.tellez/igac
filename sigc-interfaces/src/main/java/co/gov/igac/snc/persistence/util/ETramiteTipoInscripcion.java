package co.gov.igac.snc.persistence.util;

public enum ETramiteTipoInscripcion {
    CONDOMINIO("8"),
    PH("9"),
    MIXTO("1");

    private String codigo;

    ETramiteTipoInscripcion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
