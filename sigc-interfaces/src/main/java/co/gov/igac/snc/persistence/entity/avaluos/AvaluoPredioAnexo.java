package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the AVALUO_PREDIO_ANEXO database table.
 *
 */
@Entity
@Table(name = "AVALUO_PREDIO_ANEXO")
public class AvaluoPredioAnexo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Avaluo avaluo;
    private Long avaluoPredioId;
    private String descripcion;
    private Date fechaLog;
    private String fuente;
    private Double medida;
    private String tipo;
    private String unidadMedida;
    private String usuarioLog;
    private Double valorTotal;
    private Double valorUnidad;

    //Transients para mostrar información del predio
    private String numeroPredial;
    private String codigoMunicipio;
    private String codigoDepartamento;

    public AvaluoPredioAnexo() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PREDIO_ANEXO_ID_GENERATOR", sequenceName =
        "AVALUO_PREDIO_ANEXO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_PREDIO_ANEXO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "AVALUO_PREDIO_ID")
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getFuente() {
        return this.fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public Double getMedida() {
        return this.medida;
    }

    public void setMedida(Double medida) {
        this.medida = medida;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "UNIDAD_MEDIDA")
    public String getUnidadMedida() {
        return this.unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL")
    public Double getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Column(name = "VALOR_UNIDAD")
    public Double getValorUnidad() {
        return this.valorUnidad;
    }

    public void setValorUnidad(Double valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    @Transient
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Transient
    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    @Transient
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

}
