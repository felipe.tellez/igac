package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONTROL_CALIDAD_MUESTRA database table.
 *
 */
@Entity
@Table(name = "CONTROL_CALIDAD_MUESTRA")
public class ControlCalidadMuestra implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1606552590405533755L;

    private Long id;
    private Date fechaLog;
    private String usuarioLog;
    private ActualizacionLevantamiento actualizacionLevantamiento;
    private AsignacionControlCalidad asignacionControlCalidad;

    public ControlCalidadMuestra() {
    }

    @Id
    @SequenceGenerator(name = "CONTROL_CALIDAD_MUESTRA_ID_GENERATOR", sequenceName =
        "CONTROL_CALIDAD_MUESTRA_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTROL_CALIDAD_MUESTRA_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionLevantamiento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_LEVANTAMIENTO_ID", nullable = false)
    public ActualizacionLevantamiento getActualizacionLevantamiento() {
        return this.actualizacionLevantamiento;
    }

    public void setActualizacionLevantamiento(ActualizacionLevantamiento actualizacionLevantamiento) {
        this.actualizacionLevantamiento = actualizacionLevantamiento;
    }

    //bi-directional many-to-one association to AsignacionControlCalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASIGNACION_CONTROL_CALIDAD_ID", nullable = false)
    public AsignacionControlCalidad getAsignacionControlCalidad() {
        return this.asignacionControlCalidad;
    }

    public void setAsignacionControlCalidad(AsignacionControlCalidad asignacionControlCalidad) {
        this.asignacionControlCalidad = asignacionControlCalidad;
    }

}
