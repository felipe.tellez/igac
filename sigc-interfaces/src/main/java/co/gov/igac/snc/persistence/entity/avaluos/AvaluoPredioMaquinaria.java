package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the AVALUO_PREDIO_MAQUINARIA database table.
 *
 */
@Entity
@Table(name = "AVALUO_PREDIO_MAQUINARIA")
public class AvaluoPredioMaquinaria implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Avaluo avaluo;
    private Long avaluoPredioId;
    private Double cantidad;
    private String descripcion;
    private String especificacion;
    private Date fechaLog;
    private String unidadMedida;
    private String usuarioLog;
    private Double valorTotal;
    private Double valorUnidad;

    //Transients para mostrar información del predio
    private String numeroPredial;
    private String codigoMunicipio;
    private String codigoDepartamento;

    public AvaluoPredioMaquinaria() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PREDIO_MAQUINARIA_ID_GENERATOR", sequenceName =
        "AVALUO_PREDIO_MAQUINARI_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_PREDIO_MAQUINARIA_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "AVALUO_PREDIO_ID")
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    public Double getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEspecificacion() {
        return this.especificacion;
    }

    public void setEspecificacion(String especificacion) {
        this.especificacion = especificacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "UNIDAD_MEDIDA")
    public String getUnidadMedida() {
        return this.unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL")
    public Double getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Column(name = "VALOR_UNIDAD")
    public Double getValorUnidad() {
        return this.valorUnidad;
    }

    public void setValorUnidad(Double valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    @Transient
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Transient
    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    @Transient
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

}
