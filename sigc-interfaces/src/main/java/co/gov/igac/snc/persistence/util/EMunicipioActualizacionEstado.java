/*
 * Proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Representa los estados posibles de los procsos de actualizacion
 *
 * @author felipe.cadena
 */
public enum EMunicipioActualizacionEstado {

    CARGUE("CARGUE"),
    VALIDACION_INICIAL("VALIDACION_INICIAL"),
    VALIDACION_TRAMITES("VALIDACION_TRAMITES"),
    VALIDACION_EXITOSA("VALIDACION_EXITOSA"),
    VALIDACION_EXITOSA_TYR("VALIDACION_EXITOSA_TYR"),
    VALIDACION_FALLIDA("VALIDACION_FALLIDA"),
    RADICACION("RADICACION"),
    RADICACION_FALLIDA("RADICACION_FALLIDA"),
    RADICACION_EXITOSA("RADICACION_EXITOSA"),
    GENERAR_RESOLUCION("GENERAR_RESOLUCION"),
    FINALIZADO("FINALIZADO");

    private String codigo;

    EMunicipioActualizacionEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
