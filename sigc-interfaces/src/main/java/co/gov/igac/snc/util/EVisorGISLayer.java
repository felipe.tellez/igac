package co.gov.igac.snc.util;

/**
 * Enumeración con las posibles capas que puede tener el visor GIS
 *
 * @author christian.rodriguez
 */
public enum EVisorGISLayer {

    // D: valor ("codigo")
    ACTUALIZACION("ac"),
    AVALUOS("av"),
    CONSERVACION("co"),
    OFERTAS_INMOBILIARIAS("of");

    private String codigo;

    EVisorGISLayer(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
