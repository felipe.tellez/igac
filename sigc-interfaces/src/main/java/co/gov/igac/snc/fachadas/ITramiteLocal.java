/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.fachadas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.RepConsultaPredial;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.DeterminaTramitePH;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudAvisoRegistro;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInfoAdicional;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.entity.tramite.VRadicacion;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.DetallesDTO;
import co.gov.igac.snc.util.EventoScheduleComisionesDTO;
import co.gov.igac.snc.util.FiltroDatosConsultaCancelarTramites;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.TramiteConEjecutorAsignadoDTO;
import co.gov.igac.snc.vo.ActividadVO;
import co.gov.igac.snc.vo.ResultadoVO;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

/**
 * Interfaz para los métodos de trámites
 */
@Local
public interface ITramiteLocal {

    public void lanzarExcepcionEntreCapasTest();

    /**
     * Método que muestra los detalles de un predio correspondiente a un Tramite, mediante su numero
     * de redicacion
     *
     * @param numeroRadicacion
     *
     * @return
     */
    public DetallesDTO detallesPredioDeTramite(Long tramiteId);

    /**
     * Crea una solicitud y un trámite de avalúo correspondiente en una sola operación.
     *
     * @param usuario datos del usuario que realiza la operación.
     * @param solicitud datos de la solicitud, incluyendo el trámite, a crear.
     * @return la solicitud creada con el identificador (llave primaria) asignada.
     * @author jamir.avila
     */
    public Solicitud crearSolicitudTramite(UsuarioDTO usuario, Solicitud solicitud);

    /**
     * Método utilizado para actualizar una Solicitud.
     *
     * @param solicitud Solicitud a almacenar
     * @return Solicitud con información actualizada.
     * @author fabio.navarrete
     * @deprecated 23-10-2013 pedro.garcia usar guardarActualizarSolicitud2 para enviar info al
     * usuario
     */
    @Deprecated
    public Solicitud guardarActualizarSolicitud(UsuarioDTO usuario, Solicitud solicitud,
        List<Long> tramitesPrevios);

    /**
     * Método utilizado para consultar los trámites asociados a una Solicitud.
     *
     * @param solicitudId
     * @return lista con los trámites de la solicitud
     */
    public List<Tramite> consultarTramitesSolicitud(Long solicitudId);

    /**
     * @author fredy.wilches Método utilizado para consultar los trámites asociados a una Solicitud.
     * @param numeroSolicitud
     * @return lista con los trámites de la solicitud
     */
    public List<Tramite> consultarTramitesSolicitud(String numeroSolicitud);

    /**
     * Metodo utilizado para buscar los documentos asociados a una solicitud
     *
     * @author juan.agudelo
     * @param numeroRadicacion
     * @return lista de documentos asociados a solicitud
     */
    public List<TramiteDocumentacion> buscarDocumentosTramitePorNumeroDeRadicacion(
        String numeroRadicacion);

    /**
     * Busca los TramitePredioEnglobe que correspondan al tramite con el id dado
     *
     * @author nn
     * @param tramiteId
     * @return
     */
    /*
     * @modified pedro.garcia 21-08-2012 El -chambón autor- mandaba el trámite completo cuando lo
     * único que se necesita es el id
     */
    public List<TramiteDetallePredio> buscarPredioEnglobesPorTramite(Long tramiteId);

    /**
     * Regresa las solicitudes que cumplen con los criterios de búsqueda dados.
     *
     * @author jamir.avila
     * @param filtroDatosSolicitud
     * @param desde indica la posición a partir de la cual se deben recuperar los registros.
     * @param cantidad representa la cantidad de registros a retornar, un valor igual o menor a cero
     * indica que se deben regresar todos los registros cuya posición sea igual o superior al
     * parámetro desde.
     * @return una lista de objetos Solicitud.
     */
    public List<Solicitud> buscarSolicitudes(FiltroDatosSolicitud filtroDatosSolicitud, int desde,
        int cantidad);

    /**
     * Regresa las solicitudes de cotización que cumplen con los criterios de búsqueda datos.
     *
     * @author jamir.avila
     * @param filtroDatosSolicitud objeto de transporte con los criterios de búsqueda.
     * @param desde posición a partir de la cual se deben recuperar los registros resultantes.
     * @param cantidad número de registros a retornar, un valor igual o menor a cero indica que se
     * deben regresar todos los registros cuya posición sea igual o mayor al parámetro desde.
     * @return una lista de objetos Solicitud.
     */
    public List<VRadicacion> buscarSolicitudesCotizacionAvaluos(
        FiltroDatosSolicitud filtroDatosSolicitud, int desde, int cantidad);

    /**
     * Obtiene el número de solicitudes de cotización que cumplen con los criterios de búsqueda
     * dados.
     *
     * @author jamir.avila
     * @param filtro objeto de transporte con los criterios de búsqueda.
     * @return la cantidad de solicitudes que coinciden con los criterios de búsqueda.
     */
    public long contarSolicitudesCotizacionAvaluos(FiltroDatosSolicitud filtro);

    /**
     * @mdifiedBy fabio.navarrete
     *
     * @param numeroDocumento
     * @param tipoDocumento
     * @return
     */
    public Solicitante buscarSolicitantePorTipoYNumeroDeDocumento(
        String numeroDocumento, String tipoDocumento);

    /**
     * @author fabio.navarrete Guarda o actualiza un solicitante en la base de datos.
     * @param solicitante objeto Solicitante a guardar
     */
    public Solicitante guardarActualizarSolicitante(Solicitante solicitante);

    /**
     * Guarda o actualiza un solicitante solicitud en la base de datos.
     *
     * @author christian.rodriguez
     * @param solicitante objeto SolicitanteSolicitud a guardar
     */
    public SolicitanteSolicitud guardarActualizarSolicitanteSolicitud(
        SolicitanteSolicitud solicitante);

    /**
     * Elimina un solicitante solicitud de la BD
     *
     * @author christian.rodriguez
     * @param solicitante solicitante solicitud a eliminar
     */
    public void eliminarSolicitanteSolicitud(SolicitanteSolicitud solicitante);

    /**
     * Busca los trámites con id contenido en idsTramites. No se usa un método genérico para esta
     * consulta porque dependiendo del subproceso se necesita traer más o menos información del
     * trámite. No se usa la territorial como parámetro porque los ids de trámites me los da el bpm.
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Tramite> buscarTramitesParaAsignacionDeEjecutor(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Cuenta los trámites que estén pendientes de asignación o reclasificación por parte del
     * responsable de conservación de la territorial dada Estos trámites son los que el jefe de
     * conservación ve en la pantalla "Asignar trámites"
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     */
    public int contarTramitesParaAsignacionDeEjecutor(long[] idsTramites);

    /**
     * Actualizar un arrerglo de tramites
     *
     * @author juan.agudelo
     */
    public void actualizarTramites(Tramite[] tramitesReclasificados);

    /**
     * Actualizar una lista de tramites
     *
     * @author pedro.garcia
     */
    public boolean actualizarTramites(List<Tramite> tramites);

    /**
     * Buscar trámite con trámites asociados por tramiteId
     *
     * @author juan.agudelo
     */
    public Tramite buscarTramiteTramitesPorTramiteId(Long tramiteId);

    /**
     * Buscar trámite con trámites asociados por tramiteId para proyeccion
     *
     * @author franz.gamba
     */
    public Tramite buscarTramitePorTramiteIdProyeccion(Long tramiteId);

    /**
     * Cuenta los trámites que estén con ejecutor asignado, pero aún sin cambiar de estado en el BPM
     * de conservación de la territorial dada Estos trámites son los que el jefe de conservación ve
     * en la pantalla "Asignar trámites"
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     */
    public int contarTramitesConEjecutorAsignado(long[] idsTramites);

    /**
     * Busca los trámites con id contenido en idsTramites. No se usa un método genérico para esta
     * consulta porque dependiendo del subproceso se necesita traer más o menos información del
     * trámite. No se usa la territorial como parámetro porque los ids de trámites me los da el bpm.
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaño del conjunto resultado
     * @return
     */
    public List<Tramite> buscarTramitesConEjecutorAsignado(
        long[] idsTramites, int... contadoresDesdeYCuantos);

    /**
     * Busca los trámites que estén en el arreglo idsTramites que tengan el campo 'comisionado' en
     * false. Estos trámites son los que el jefe de conservación ve en la pantalla "Manejo de
     * comisiones para trámites"
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     * @param territorialId id de la territorial. Se necesita para el cálculo de los tiempos de la
     * comisión
     * @param tipoComision tipo de comisión que se va a usar para el cálculo del número de
     * comisiones en las que ha estado el trámite
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     * @param sortOrder nombre del ordenamiento (UNSORTED, DESCENDING, DESCENDING)
     * @param filters filtros de búsqueda de la consulta
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     *
     * @return
     */
    public List<Tramite> buscarTramitesPorComisionar(long[] idsTramites, String territorialId,
        String tipoComision, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Cuenta los trámites cuyos id estén en el arreglo idsTramites que tengan el campo
     * 'comisionado' en false
     *
     * Estos trámites son los que el jefe de conservación ve en la pantalla "Manejo de comisiones
     * para trámites".
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los id de trámite
     */
    public int contarTramitesPorComisionar(long[] idsTramites, String tipoComision);

    /**
     * Cuenta las comisiones que estén sin memorando generado y que estén relacionadas con trámites
     * que estén a cargo del jefe de conservación de la territorial dada Estas comisiones son las
     * que el jefe de conservación ve en la pantalla "Manejo de comisiones para trámites" en el tab
     * de comisiones
     *
     * @author pedro.garcia
     * @param idTerritorial
     * @param idTramites ids de los trámites que vienen dados por el proceso, sobre los cuales se
     * hace la búsqueda
     * @param tipoComision nombre del tipo de comisión
     */
    public int contarComisionesSinMemo(String idTerritorial, List<Long> idTramites,
        String tipoComision);

    /**
     * Busca las comisiones (siempre están asociadas a algún trámite) para las que no se haya
     * generado memorando y que estén relacionadas con trámites en la territorial dada. Tener en
     * cuenta que la consulta se hace sobre una vista y que los objetos devueltos son entities
     * generados a partir de ésta.
     *
     * @author pedro.garcia
     * @param territorialId id de la territorial
     * @param idTramites ids de los trámites que vienen dados por el proceso, sobre los cuales se
     * hace la búsqueda
     * @param tipoComision nombre del tipo de comisión
     * @param filters filtros de búsqueda de la consulta
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    public List<VComision> buscarComisionesSinMemo(String territorialId, List<Long> idTramites,
        String tipoComision, Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Crea un predio y lo vincula con un avalúo.
     *
     * @author jamir.avila
     * @param tramitePredioAvaluo objeto con los datos del predio a vincular.
     * @return el objeto de transporte con los datos del predio.
     */
    public TramitePredioAvaluo adicionarPredioAAvaluo(TramitePredioAvaluo tramitePredioAvaluo);

    /**
     * Remueve el vínculo entre un predio y un avalúo.
     *
     * @author jamir.avila
     * @param idTramitePredioAvaluo identificador de la relación trámite - predio de avalúo a
     * remover.
     */
    public void removerPredioDeAvaluo(Long idTramitePredioAvaluo);

    /**
     * Actualiza los datos de un predio que pertenece a un avalúo.
     *
     * @author jamir.avila
     * @param predioAvaluo objeto con los datos actualizados del predio.
     */
    public void actualizarPredioDeAvaluo(TramitePredioAvaluo predioAvaluo);

    /**
     * Crea un trámite correspondiente a una solicitud de cotización de avalúo comercial.
     *
     * @author jamir.avila
     * @param tramite objeto con los datos para crear el trámite.
     * @return el objeto con los datos con los que se creó el trámite.
     */
    public Tramite crearSolicitudCotizacionAvaluo(Tramite tramite);

    /**
     * Ingresa una lista de documentos adicionales solicitados
     *
     * @author juan.agudelo
     */
    public void ingresarDocumentosAdicionalesRequeridos(
        List<TramiteDocumentacion> documentosAdicionales);

    /**
     * Obtiene una solucitud a partir de su identificador.
     *
     * @author jamir.avila
     * @param id identificador de la solicitud a recuperar.
     * @return el objeto entity correspondiente o null en caso de que no exista.
     */
    public Solicitud findSolicitudPorId(Long id);

    /**
     * inserta una nueva comisión en la base de datos.
     *
     * @author pedro.garcia
     * @param nuevaComision
     * @param datosUsuario
     */
    public Comision insertarComision(Comision nuevaComision, UsuarioDTO datosUsuario);

    /**
     * @author fabio.navarrete Retorna un solicitante con datos adicionales: País, Departamento,
     * municipio y solicitantes relacionados
     *
     * @param numeroDocumento
     * @param tipoDocumento
     * @return
     */
    public Solicitante buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
        String numeroDocumento, String tipoDocumento);

    /**
     * @author fabio.navarrete Método que consulta el radicado de correspondencia y retorna una
     * Solicitud interna del sistema
     * @param numeroRadicacionCorrespondencia
     * @param usuario
     * @return
     */
    public Solicitud obtenerSolicitudDeCorrespondencia(
        String numeroRadicacionCorrespondencia, UsuarioDTO usuario);

    /**
     * inserta en masa registros a la tabla COMISION_TRAMITE
     *
     * @author pedro.garcia
     * @param registros
     */
    public void insertarRegistrosComisionTramite(List<ComisionTramite> registros);

    /**
     * Método que busca los trámites asociados a los id's de comisión enviados como parámetro.
     *
     * @author david.cifuentes
     * @param comisionesId Lista de id's de las comisiones.
     * @return
     */
    public List<Tramite> buscarTramitesDeComisiones(List<Long> comisionesId);

    /**
     * Busca los trámites asociados a una comision del tipo dado
     *
     * @author pedro.garcia
     * @param comisionId id de la comisión
     * @param tipoComision tip de comisión que se debe tener en cuenta para la búsqueda
     * @return
     */
    public List<Tramite> buscarTramitesDeComision(Long comisionId, String tipoComision);

    /**
     * Cuenta los trámites asignados a un ejecutor Estos trámites son los que el ejecutor ve en la
     * pantalla "Validar"
     *
     * @author juan.agudelo
     * @param ejecutor
     * @return
     */
    public Integer contarTramitesDeEjecutorAsignado(String ejecutor);

    /**
     * Buscar los trámites asignados a un ejecutor Estos trámites son los que el ejecutor ve en la
     * pantalla "Validar"
     *
     * @author juan.agudelo
     * @param idTerritorial
     * @param ejecutor
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    public List<Tramite> buscarTramitesDeEjecutorAsignado(String territorialId,
        String ejecutor, int... contadoresDesdeYCuantos);

    /**
     *
     * @param territorialId
     * @param ejecutor
     * @param contadoresDesdeYCuantos
     * @return
     */
    public List<Tramite> buscarTramitesAsignadosAEjecutor(String territorialId,
        String ejecutor, int... contadoresDesdeYCuantos);

    /**
     * Método para consultar la configuracion de las secciones para la ejecucion de un tramite
     *
     * @author fredy.wilches
     */
    /*
     * @modified juan.agudelo
     */
    public TramiteSeccionDatos consultarConfiguracionTramite(String tipoTramite,
        String claseMutacion, String subtipo, String radicacionEspecial);

    /**
     * Método para consultar un tramite por llave primaria, junto con la solicitud
     *
     * @author fredy.wilches
     */
    public Tramite consultarTramite(Long tramiteId);

    /**
     * Método que ejecuta un SP para generar la proyección de un trámite
     */
    public Object[] generarProyeccion(Long tramiteId);

    /**
     * Actualiza los datos de una comisión a partir de los datos en una VComision. Se recibe un
     * VComision, pero hay que guardarlo como Comision
     *
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-04 CU-CO-A-07
     *
     * @param loggedInUser
     * @param selectedComision de tipo VComision porque la tabla muestra datos de ese tipo.
     */
    public void actualizarComision(UsuarioDTO loggedInUser, VComision selectedComision);

    /**
     * Borra los trámites (no todos, solo los que vengan en la lista) de una comisión Se realizó un
     * cambio porque no estaba haciendo la eliminación multiple
     *
     * @author pedro.garcia
     * @modified by javier.aponte
     * @param loggedInUser
     * @param comisionTramiteRels Lista de objetos ComisionTramite que se deben borrar
     * @return true si no ocurrió alguna excepción
     */
    public boolean borrarComisionesTramites(UsuarioDTO loggedInUser,
        List<ComisionTramite> comisionTramiteRels);

    /**
     * Borra un registro de la tabla COMISION_TRAMITE donde la comisión y el trámite sean los dados
     *
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-07, CU-CO-A-04
     *
     * @param loggedInUser
     * @param tramiteId
     * @param comisionId
     */
    public void borrarRelacionTramiteComision(UsuarioDTO loggedInUser, Long tramiteId,
        Long comisionId);

    /**
     *
     * @param numeroRadicacionCorrespondencia
     * @param usuario
     * @return
     */
    public Solicitud obtenerSolicitudAvaluosCorrespondencia(
        String numeroRadicacionCorrespondencia, UsuarioDTO usuario);

    /**
     * Actualiza los datos de una comisión
     *
     * @author juan.agudelo
     * @param loggedInUser
     */
    public Comision actualizarComision(UsuarioDTO loggedInUser, Comision comision);

    /**
     * Actualiza los datos de una comisión
     *
     * @author pedro.garcia
     *
     * @param usuarioActual
     * @param comision
     * @return true si todo resulta bien
     */
    public boolean actualizarComision2(UsuarioDTO usuarioActual, Comision comision);

    /**
     * Método que cuenta los trámites que tiene asignado un funcionario ejecutor
     *
     * @author javier.aponte
     */
    public Integer contarTramitesDeFuncionarioEjecutor(String ejecutor);

    /**
     * Método que busca las comisiones que tengan un estado igual al parametro estado, tal que los
     * trámites que pertenecen a esa comisión sean para un predio que este en la territorial del
     * parametro que se le pasa
     *
     * @author javier.aponte
     * @param idTerritorial
     * @param estado
     * @return
     */
    public List<Comision> buscarComisionesPorIdTerritorialAndEstadoComision(
        String territorialId, String estadoComision);

    /**
     * Guarda un SolicitudAvisoRegistro
     *
     * @param solicitudAviso
     *
     * @author pedro.garcia
     */
    public void guardarSolicitudAvisoRegistro(SolicitudAvisoRegistro solicitudAviso);

    /**
     * Busca una solicitud en las tablas del sistema SIC. Si no la encuentra realiza la búsqueda en
     * el sistema de correspondencia.
     *
     * @param numeroSolicitud Número de la solicitud a buscar.
     * @author fabio.navarrete
     */
    public Solicitud obtenerSolicitudInternaCorrespondencia(String numeroSolicitud,
        UsuarioDTO usuario);

    /**
     * Retorna el listado de solicitantes de una solicitud
     *
     * @author fredy.wilches
     *
     * @param solicitudId
     * @return
     */
    public List<SolicitanteSolicitud> obtenerSolicitantesSolicitud(Long solicitudId);

    /**
     * Obtiene la información que se muestra en la tabla de avisos rechazados de una solicitud, en
     * forma de objetos AvisoRegistroRechazo a los que se les a cada uno de los elementos de su
     * lista de AvisoRegistroRechazoPredio se le ha asignado un Predio
     *
     * @author pedro.garcia
     * @param numeroSolicitud
     * @return
     */
    public List<AvisoRegistroRechazo> obtenerInfoAvisosRechazadosPorSolicitud(String numeroSolicitud);

    /**
     * Método que guardar un {@link TramiteTextoResolucion}
     *
     * @author david.cifuentes
     * @param tramiteTextoResolucion
     * @return
     */
    public TramiteTextoResolucion guardarTramiteTextoResolucion(
        TramiteTextoResolucion tramiteTextoResolucion);

    /**
     * Método que busca el texto motivo de un tramite por el numero de motivo
     *
     * @param String numeroMotivo
     * @author david.cifuentes
     */
    public String findTextoMotivoByNumeroMotivo(String numeroMotivo);

    /**
     * Método que permite archivar un trámite
     *
     * @author juan.agudelo
     * @param usuario
     * @param tramiteParaArchivar
     * @return
     */
    public boolean archivarTramite(UsuarioDTO usuario,
        Tramite tramiteParaArchivar);

    /**
     * Método que busca un {@link ModeloResolucion} por el tipo de tramite y la clase de mutacion
     *
     * @param String tipoTramite, String claseMutacion
     * @return List<ModeloResolucion>
     * @author david.cifuentes
     */
    public List<ModeloResolucion> findModelosResolucionByTipoTramite(String tipoTramite,
        String claseMutacion, String subtipo);

    /**
     * Método que busca un {@link ModeloResolucion} por el tipo de tramite ,la clase de mutacion, Subtipo y la delegada
     *
     * @param String tipoTramite, String claseMutacion, subtipo y delagada
     * @return List<ModeloResolucion>
     * @author carlos.ferro
     */
    public List<ModeloResolucion> findModelosResolucionByTipoTramite(String tipoTramite,
        String claseMutacion, String subtipo, String delegada);
    /**
     * Método que permite buscar un trámite texto resolución por id del trámite
     *
     * @author javier.aponte
     * @param id
     * @return
     */
    public TramiteTextoResolucion buscarTramiteTextoResolucionPorIdTramite(
        Long id);

    /**
     * Método que permite buscar un trámite estado por id del trámite
     *
     * @author javier.aponte
     * @param id
     * @return
     */
    public TramiteEstado buscarTramiteEstadoVigentePorTramiteId(Long id);

    /**
     * Método que busca un tramite con Solicitud,SolicitanteTramite, TramiteDocumentacions, Predio y
     * TramiteEstado
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Tramite buscarTramiteSolicitudPorId(Long tramiteId);

    /**
     * Método que busca una {@link Solicitud} en base a ciertos criterios
     *
     * @param datosFiltroSolicitud
     * @return SolicitanteSolicitud
     * @author david.cifuentes
     */
    public List<VSolicitudPredio> buscarSolicitudPorFiltros(
        VSolicitudPredio datosFiltroSolicitud, int desde, int cantidad);

    /**
     * Busca -en la vista V_EJECUTOR_COMISION- los ejecutores de los trámites que están relacionados
     * con una comisión
     *
     * @author pedro.garcia
     * @param numeroComision
     * @return
     */
    public List<VEjecutorComision> buscarEjecutoresPorNumeroComision(String numeroComision);

    /**
     * busca los trámites que tienen asignado el funcionario ejecutor dado y que están asignados a
     * la comisión dada
     *
     * @author pedro.garcia
     *
     * @param idFuncionarioEjecutor
     * @param idComision
     *
     * @return
     */
    public List<Tramite> buscarTramitesDeEjecutorEnComision(
        String idFuncionarioEjecutor, Long idComision);

    /**
     * crea un trámite documento cuando un trámite se selecciona para notificar
     *
     * @author javier.aponte
     * @param tramiteDocumento
     */
    public void crearTramiteDocumento(TramiteDocumento tramiteDocumento);

    /**
     * actualiza el estado
     *
     * @param tramitesEjecutorComision
     * @param estado
     */
    public void actualizarEstadoTramites(List<Tramite> tramitesEjecutorComision, String estado);

    /**
     * obtiene la duración de una comisión a partir de las relaciones entre comisión y trámites. Se
     * calcula como el máximo de las duraciones de los trámites asociados con ella
     *
     * @author pedro.garcia
     * @param idComision
     * @return
     * @deprecated la regla para el cálculo de la duración de las comisiones cambió. Usar método
     * obtenerDuracionComision2
     */
    @Deprecated
    public Double obtenerDuracionComision(Long idComision);

    /**
     * Método que busca un {@link Solicitante} en base a ciertos criterios
     *
     * @param datosFiltroSolicitante
     * @param cargarDeptoYMuni true si se quiere cargar el departamento y el municipio
     * @return Solicitante
     * @author david.cifuentes
     */
    public List<Solicitante> buscarSolicitantesPorFiltro(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante, boolean cargarPaisDeptoYMuni);

    /**
     * Método que busca los solicitantes que tienen contrato itneradminsitrativo asociado
     *
     * @author christian.rodriguez
     * @param datosFiltroSolicitante filtro de tipo {@link FiltroDatosConsultaSolicitante}
     * @return Lista de {@link Solicitante} encontrados
     */
    public List<Solicitante> buscarSolicitantesPorFiltroConContratoInteradministrativo(
        FiltroDatosConsultaSolicitante datosFiltroSolicitante);

    /**
     * Método encargado de almacenar o actualizar un objeto del tipo AvisoRegistroRechazo
     *
     * @param usuario
     * @param avisoRechazo
     * @return
     * @author fabio.navarrete
     */
    public AvisoRegistroRechazo guardarActualizarAvisoRegistroRechazo(
        UsuarioDTO usuario, AvisoRegistroRechazo avisoRechazo);

    /**
     * retorna la lista de predios asociados a un trámite
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosDeTramite(Long tramiteId);

    /**
     * retorna la lista de predios asociados a un trámite, para predios de quinta nuevo finalizado
     * busca en hpredio, para no finalizado busca en ppredio
     *
     * @author carlos.ferro
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosDeTramiteQuintaNuevo(Long tramiteId);

    /**
     * retorna la lista de DatosRectificar según el tipo
     *
     * @author pedro.garcia
     * @param tipoDatoRectifCompl
     * @return
     */
    public List<DatoRectificar> obtenerDatosRectificarPorTipo(String tipoDatoRectifCompl);

    /**
     * retorna la lista de DatosRectificar según el tipo de datos a rectificar o complementar y la
     * naturaleza (rectificación o complementación) de los mismos
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tipoDatoRectifCompl
     * @param naturaleza
     * @return
     */
    public List<DatoRectificar> obtenerDatosRectificarPorTipoYNaturaleza(
        String tipoDatoRectifCompl, String naturaleza);

    /**
     * Método que obtiene una solicitud con TODOS los datos asociados a ésta requeridos para cargar
     * una solicitud de lectura en la pantalla de gestionarSolicitudes. Si no la encuentra en el
     * sistema SIC busca la solicitud en correspondencia.
     *
     * @param numeroSolicitud
     * @param usuario
     * @return
     * @author fabio.navarrete
     */
    public Solicitud obtenerSolicitudCompletaInternaCorrespondencia(
        String numeroSolicitud, UsuarioDTO usuario);

    /**
     * Método que obtiene una lista de documentos requeridos para un trámite con base en las
     * carateristicas especificsa del trámite
     *
     * @author juan.agudelo
     * @param tramite
     * @return
     */
    public List<TramiteRequisito> conseguirDocumentacionTramiteRequerida(
        Tramite tramite);

    /**
     * Retorna la lista de tipos de trámites según el tipo de solicitud
     *
     * @author pedro.garcia
     * @param tipoSolicitud
     * @return
     */
    public List<TipoSolicitudTramite> obtenerTiposDeTramitePorSolicitud(String tipoSolicitud);

    /**
     * Consulta la documentación de un trámite. Trae el trámite habiéndole hecho fetch con los
     * TramiteDocumentacion y con la solicitud (para obtener datos de esta)
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite consultarDocumentacionDeTramite(Long tramiteId);

    /**
     * Consulta la documentación completa de un trámite Trae la informacion de los documentos
     * aportados y los generados por el sistema almacenados en TramiteDocumento, ComisionEstado y
     * TramitePruebas
     *
     * @author juanfelipe.garcia
     * @param tramiteId
     * @return
     */
    public Tramite consultarDocumentacionCompletaDeTramite(Long tramiteId);

    /**
     * Método que actualiza un {@link Tramite} Este método no actualiza la documentación y no
     * devuelven el trámite. Este es el que se debe usar cuando se cuenta con un UsuarioDTO
     *
     * @param tramite
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 27-11-2013 cambia el tipo de retorno del método para poder determinar
     * si fue exitoso
     */
    public boolean actualizarTramite(Tramite tramite, UsuarioDTO usuario);

    /**
     * Actualiza el trámite este método no actualiza la documentación y no devuelven el trámite.
     * OJO: este se usa para las pruebas de integración de mover procesos hasta un determinado punto
     */
    public boolean actualizarTramite(Tramite tramite);

    /**
     * Método que actualiza un trámite, teniendo en cuenta específicamente los documentos asociados,
     * los cuales también actualiza
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tramite
     * @param usuario
     * @return
     */
    public Tramite actualizarTramiteDocumentos(Tramite tramite, UsuarioDTO usuario);

    /**
     * Método que actualiza un trámite y el documento de notificación asociado. Según el autor del
     * método, cuando hay un documento de autorización este no se guarda como TramiteDocumento, sino
     * que se relaciona al TramiteDocumento del documento de notificación (por el campo
     * AUTORIZACION_DOCUMENTO_ID)
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tramite
     * @param documentoAutorizacion
     * @param documentoNotificacion
     * @param tramiteDocumentoNotificacion
     * @param usuario
     * @return
     * @deprecated No usar este método nuevamente. Dice que actualiza, pero no lo hace (solo toma en
     * cuenta el caso de documentos nuevos, en cuyo caso es insertar). La implementación del método
     * tiene sentencias de dudosa índole
     */
    /*
     * @documented pedro.garcia @modified pedro.garcia 17-07-2012 deprecated luego de analizar el
     * método
     */
    @Deprecated
    public Tramite actualizarTramiteDocumentosNotificacion(Tramite tramite,
        Documento documentoAutorizacion, Documento documentoNotificacion,
        TramiteDocumento tramiteDocumentoNotificacion, UsuarioDTO usuario);

    /**
     * consulta los TramiteRectificaciones de un trámite
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<TramiteRectificacion> obtenerTramiteRectificacionesDeTramite(Long idTramite);

    /**
     *
     * Método que busca los Tramites a los que se les haya aplicado cierta resolución
     *
     * @author david.cifuentes
     *
     * @param resolucion
     * @param documentoEstado
     * @param documentoEstadoAdicional
     * @return
     */
    public List<Tramite> getTramitesByResolucion(String resolucion, String documentoEstado,
        String documentoEstadoAdicional);

    /**
     * consulta los solicitantes de un trámite. Se supone que solo los que son de tipo aviso o
     * autoavalúo (llegado de tesorería) tienen solicitantes (diferentes a los de la solicitud)
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<SolicitanteTramite> obtenerSolicitantesDeTramite(Long idTramite);

    /**
     * Hace la misma consulta que Itramite#obtenerSolicitantesSolicitud , pero el query hace fetch
     * del municipio, departamento y país. Se implementó como método aparte para no -posiblemente-
     * dañar el que había antes
     *
     * @author pedro.garcia
     * @param solicitudId
     * @return
     */
    public List<SolicitanteSolicitud> obtenerSolicitantesSolicitud2(Long solicitudId);

    /**
     * Método que busca Trámites filtrados por numero predial del predio y por resolucion del
     * trámite
     *
     * @param resolucion, numeroPredial
     * @author david.cifuentes
     * @return List<Tramite>
     */
    public List<Tramite> buscarTramitesConResolucionYNumeroPredialPredio(String resolucion,
        String numeroPredial);

    /**
     * Método que busca un Tramite con sus predios, solicitantes y documentación recibiendo como
     * parametro el tramiteId
     *
     * @author david.cifuentes
     * @param tramiteId
     * @return Tramite
     */
    public Tramite buscarTramiteConSolicitantesDocumentacionYPrediosPorTramiteId(Long tramiteId);

    /**
     * Método que consulta en el LDAP los datos de los funcionarios ejecutores de una territorial y
     * luego asocia la cantidad de trámites de terreno y de oficina que tienen estos funcionarios
     * ejecutores
     *
     * @author javier.aponte
     * @param codigoTerritorial
     * @param rol
     * @return
     */
    public List<SEjecutoresTramite> buscarFuncionariosEjecutoresPorTerritorial(String idTerritorial);

    /**
     * Metodo que retorna la lista de funcionarios ejecutores de una territorial, obteniendo sus
     * tramites y la clasificación de estos (Terreno u Oficina)
     *
     * @author franz.gamba
     * @param idTerritorial
     * @return
     */
    public List<SEjecutoresTramite> buscarFuncionariosTerritorialConClasificacionTramite(
        String idTerritorial);

    /**
     * Método que consulta en el LDAP los datos de los funcionarios ejecutores de una territorial
     *
     * @author franz.gamba
     * @param codigoTerritorial
     * @param rol
     * @return
     */
    public List<UsuarioDTO> buscarFuncionariosEjecutoresPorTerritorialDTO(String idTerritorial);

    /**
     * Método que consulta en el LDAP los datos de los funcionarios Digitalizadores de una
     * territorial
     *
     * @author felipe.cadena
     * @param codigoTerritorial
     * @param rol
     * @return
     */
    public List<UsuarioDTO> buscarFuncionariosDigitalizadoresPorTerritorialDTO(String idTerritorial);

    /**
     * Método que consulta en el LDAP los datos de los funcionarios Topografos de una territorial
     *
     * @author felipe.cadena
     * @param codigoTerritorial
     * @param rol
     * @return
     */
    public List<UsuarioDTO> buscarFuncionariosTopografosPorTerritorialDTO(String idTerritorial);

    /**
     * Retorna el trámite y las entidades encargadas a pruebas solicitadas si existen a partir del
     * id del trámite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public Tramite findTramiteConTramitePruebaEntidadByTramiteId(Long tramiteId);

    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite
     *
     * @author juan.agudelo
     * @param solicitudId
     * @return
     */
    public Tramite findTramitePruebasByTramiteId(Long tramiteId);

    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite para el
     * CU cargar Pruebas
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public Tramite obtenerTramiteCargarPruebasByTramiteId(Long tramiteId);

    /**
     * Método que guarda o actualiza una solicitud de prueba
     *
     * @author juan.agudelo
     * @param tp
     * @param usuario
     * @param estadoFuturoTramite
     * @return
     */
    public TramitePrueba guardarActualizarTramitePruebas(TramitePrueba tp,
        UsuarioDTO usuario, String estadoFuturoTramite);

    /**
     * Método que actualiza los {@link TramiteDocumentacion} de un {@link Tramite}
     *
     * @author david.cifuentes
     * @param tramiteDocumentacionActual
     */
    public void actualizarTramiteDocumentacion(List<TramiteDocumentacion> tramiteDocumentacionActual);

    /**
     * Método que elimina los {@link TramiteDocumentacion} de un {@link Tramite}
     *
     * @author david.cifuentes
     * @param tramiteDocumentacionEliminar
     */
    public void eliminarTramitesDocumentacion(
        List<TramiteDocumentacion> tramiteDocumentacionEliminar);

    /**
     * Método que actualiza un {@link TramiteDocumento}
     *
     * @author david.cifuentes
     * @param tramiteDocumento
     */
    public TramiteDocumento actualizarTramiteDocumento(TramiteDocumento tramiteDocumento);

    /**
     * Método que recupera una solicitud con toda la información necesaria para la vista de detalles
     * de solicitud
     *
     * @author juan.agudelo
     * @param solicitudId
     * @return
     */
    public Solicitud buscarSolicitudFetchTramitesBySolicitudId(Long solicitudId);

    /**
     * Método que retorna una lista de tramites con sus respectivas comisiones. Se usa
     * específicamente para consultar los trámites en la actividad 'alistar información'. No se
     * tienen en cuenta las comisiones cuyo estado sea 'finalizada'
     *
     * OJO: modificar solo si es para la misma actividad del proceso
     *
     * @param idSolicitud
     * @return
     * @author pedro.garcia
     */
    /*
     * @modified pedro.garcia 30-05-2013 se excluyen las comisiones canceladas y finalizadas
     * nov-2013 sí se tienen en cuenta las canceladas por CC-NP-CO-034
     *
     */
    public List<Tramite> consultarTramitesParaAlistarInfo(long idTramite[]);

    /**
     * Método que retorna una lista de TramiteDocumentacion asociada a un trámite específico,
     * ingresando el id del trámite.
     *
     * @param idTramite
     * @return
     * @author fabio.navarrete
     */
    public List<TramiteDocumentacion> obtenerTramiteDocumentacionPorIdTramite(Long idTramite);

    /**
     * Método que busca en comision trámite por el id del trámite y devuelve una lista de tipo
     * comisión trámite con información de las comisiones asociadas al trámite
     *
     * @param idTramite
     * @return
     * @author javier.aponte
     */
    public List<ComisionTramite> buscarComisionesTramitePorIdTramite(Long idTramite);

    /**
     * @author fredy.wilches
     * @see ITramite#buscarFuncionariosPorRolYTerritorial(String, )
     */
    public List<UsuarioDTO> buscarFuncionariosPorRolYTerritorial(String idTerritorial, ERol rol);

    /**
     * Metodo para consultar todos los usuarios asociados a una territorial o UOC
     *
     *
     * @author felipe.cadena
     * @param idTerritorial
     * @param idUoc
     * @return
     */
    public List<UsuarioDTO> buscarFuncionariosPorTerritorial(String idTerritorial, String idUoc);

    //public List<UsuarioDTO> buscarFuncionariosPorRolYTerritorial2(String idTerritorial, ERol rol);
    /**
     * Método que elimina un trámite estado
     *
     * @author javier.aponte
     * @param estadoTramite
     */
    public void eliminarTramiteEstado(TramiteEstado estadoTramite);

    /**
     * Método encargado de retornar tódos los objetos de la tabla MotivoEstadoTramite
     *
     * @return
     */
    public List<MotivoEstadoTramite> getCacheMotivoEstadoTramites();

    /**
     * Retorna un listado de trámites para los ids que entran como parámetro
     *
     * @param tramiteIds
     * @return
     */
    public List<Tramite> obtenerTramitesPorIds(List<Long> tramiteIds);

    /**
     * Retorna un listado de trámites para los ids que entran como parámetro (Paginado)
     *
     * @param tramiteIds
     * @return
     */
    public List<Tramite> obtenerTramitesPorIds(List<Long> tramiteIds, String sortField,
        String sortOrder, Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Método que actualiza una lista de trámites.
     *
     * @param tramites
     * @return
     * @deprecated desde 04-09-2013 porque mezcla actualización con consulta de relaciones que no
     * siempre se necesitan, porque asume que si el trámite no tiene id de debe poner en el estado
     * POR_APROBAR (lo cual no en cualquier sitio es válido)
     */
    /*
     * OJO: hace chambonadas que no debería y no tienen sentido. El próximo que necesite usar un
     * método como este debe implementar uno nuevo en el que no haga nada no relacionado con la
     * inserción o actualización estricta de los trámites @modified pedro.garcia 04-09-2013
     * deprecated
     */
    @Deprecated
    public List<Tramite> guardarActualizarTramites(List<Tramite> tramites);

    /**
     * Método que actualiza una lista de trámites cuando estos contienen una lista de documentos
     * asociados.
     *
     * OJO: se usa para los trámites con trámites asociados. Si quiere un método normalito para
     * actualizar el trámite junto con los documentos, usar
     * ItramiteLocal#actualizarTramiteDocumentos
     *
     * @author juan.agudelo
     * @param tramites
     * @return
     */
    public List<Tramite> guardarActualizarTramitesConDocumentos(
        List<Tramite> tramites, UsuarioDTO usuario);

    /**
     * Método que busca un {@link TramiteDocumento} con soporte en Alfresco por su id
     *
     * @author david.cifuentes
     * @param idTramite
     * @return
     */
    public TramiteDocumento buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(Long idTramite);

    /**
     * Método que actualiza un trámite estado y retorna el trámite estado actualizado
     *
     * @author javier.aponte
     * @param tramiteEstadoActual
     * @return
     */
    public TramiteEstado actualizarTramiteEstado(TramiteEstado tramiteEstadoActual,
        UsuarioDTO usuario);

    /**
     * Método que almacena o actualiza un trámite específico
     *
     * @param tramiteSeleccionado
     * @deprecated desde 02-09-2013 porque hacen consultas adicionales que un método de solo
     * actualización no debería hacer. Usar {@link #guardarActualizarTramite2(Tramite tram)}
     */
    @Deprecated()
    public Tramite guardarActualizarTramite(Tramite tramiteSeleccionado);

    /**
     * Metodo que consulta los tramites asociados a un tramite padre
     *
     * @author franz.gamba
     * @param tramiteId
     */
    public List<Tramite> consultarTramitesAsociadosATramiteId(Long tramiteId);

    /**
     * Método que retorna un trámite junto con la documentación asociada y la información de los
     * documentos
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     *
     * @deprecated no se usa
     */
//TODO::juan.agudelo::26-09-2011:: se acordó que los nombres de los métodos en estas interfaces van en español
    @Deprecated
    public Tramite findTramiteTramiteDocumentacionDocumentoById(Long tramiteId);

    /**
     * Busca un trámite dado su id, haciendo fetch de los datos que se encesitan para las pantallas
     * de asignación. </br>
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite buscarTramitePorTramiteIdAsignacion(Long tramiteId);

    /**
     * Método que almacena un documento en la base de datos y en alfresco, debe contener por lo
     * menos un trámite o un predio asociados (el id de alguno de ellos), un objeto del tipo
     * tipoDocumento, el nombre del archivo, el estado del documento y el identificador de si se
     * encuentra bloqueado
     *
     * Si el documento contiene un trámite o un trámite y un predio vinculados al documento este se
     * asociara al tramite, si contiene solo contiene un predio vinculado el documento se asociara
     * al predio, si no contiene ni trámite ni predio vinculados el documento no se cargara
     *
     * @author juan.agudelo
     * @param usuario
     * @param documento
     * @return
     */
    public Documento guardarDocumento(UsuarioDTO usuario, Documento documento);

    /**
     * Método que se encarga de eliminar un solicitantTramite
     *
     * @param solicitanteTramite
     * @author fabio.navarrete
     */
    public void eliminarSolicitanteTramite(SolicitanteTramite solicitanteTramite);

    /**
     * Método que se encarga de eliminar una lista de solicitantTramite
     *
     * @param selectedSolicitantesAfectados
     * @author fabio.navarrete
     */
    public void eliminarSolicitanteTramites(
        List<SolicitanteTramite> solicitanteTramites);

    /**
     * Método que almacena o actualiza un objeto de tipo SolicitanteTramite retornando una instancia
     * Detached del objeto
     *
     * @param solicitanteTramite
     * @return
     * @author fabio.navarrete
     */
    public SolicitanteTramite guardarActualizarSolicitanteTramite(
        SolicitanteTramite solicitanteTramite);

    /**
     * Método que busca realiza una búsqueda de objetos {@link Tramite} por su estado
     *
     * @author david.cifuentes
     * @param estado
     * @return
     */
    public List<Tramite> buscarTramitesPorEstado(String estado);

    /**
     * Método que permite buscar un documento de prueba radicado en correspondencia
     *
     * @author juan.agudelo
     * @param numeroRadicacion
     * @param usuario
     * @return
     */
    public Documento obtenerDocumentoPruebaCorrespondencia(String numeroRadicacion,
        UsuarioDTO usuario);

    /**
     * Busca los trámites cuyo id sea alguno de los que viena en el arreglo, haciendo fetch de los
     * datos que se necesitan mostrar para el CU cargue de información al sistema. </br>
     *
     * El parametro tipoComisio permite consultar comisines de depuracion como de conservacion.
     * </br>
     *
     * @author pedro.garcia
     * @param idsTramites arreglo con los ids a buscar
     * @param tipoComision
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    public List<Tramite> buscarTramitesParaCargueInfo(
        long[] idsTramites, String tipoComision, int... contadoresDesdeYCuantos);

    /**
     * Método que permite guardar o actualizar un trámite con tramiteDocumentos, almacenando los
     * documentos en alfresco
     *
     * @author juan.agudelo
     * @param tramite
     * @param usuario
     * @return
     */
    public Tramite guardarActualizarTramiteDocumentos(Tramite tramite,
        List<TramiteDocumento> documentosPruebasAEliminar, UsuarioDTO usuario);

    /**
     * Método que recupera un trámite sencillo solo con el predio base asociado
     *
     * @author juan.agudelo
     * @return
     */
    public Tramite buscarTramitePorId(Long tramiteId);

    /**
     * Método que busca un trámite por id recuperando adicionalmente la información base del predio
     * o de los predios segun corresponda, no recupera información de objetos asociados a los
     * predios
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Tramite findTramitePrediosSimplesByTramiteId(Long tramiteId);

    /**
     * Método que se encarga de guardar o actualizar un objeto de tipo TramitePruebaEntidad
     *
     * @param tramitePruebaEntidad
     * @return
     * @author fabio.navarrete
     */
    public TramitePruebaEntidad guardarActualizarTramitePruebaEntidad(
        TramitePruebaEntidad tramitePruebaEntidad);

    /**
     * Obtiene los solicitantes de un trámite (o bien su solicitud) a los que se debe hacer la
     * comunicación de una notificación. Aplica la regla para determinar a qué solicitante se le
     * envía la comunicación de notificación:
     *
     * prioridad 1: Si es apoderado a este debe dirigirse la comunicación. prioridad 2: Si la
     * relación del solicitante es representante legal a este debe dirigirse la comunicación.
     * prioridad 3: Si existe solicitante en la solicitud y en el trámite debe enviarse a ambos,
     * exceptuando aquellos que son de aviso (en cuyo caso se le envía al del trámite). El que se
     * seleccione debe ser el PRIMERO que se haya ingresado como solicitante (se supone que el que
     * tenga el menor id en la tabla)
     *
     * @author pedro.garcia
     * @modified david.cifuentes
     * @param idTramite
     * @param idSolicitud
     * @param tipoSolicitantes 1 = trae solicitantes de la solicitud; 2 = trae solicitantes del
     * trámite 3 = trae solicitantes del trámite y de la solicitud; tipoSolicitantes 4 = trae los
     * solicitantes del trámite y sus afectados
     * @return
     */
    public List<Solicitante> obtenerSolicitsParaEnvioComunicNotif(long idTramite, long idSolicitud,
        int tipoSolicitantes);

    /**
     * obtiene la lista de objetos TramiteDocumento (haciendo fetch del Documento) del trámite dado,
     * para los cuales el Documento relacionado es del tipo dado
     *
     * @author pedro.garcia
     * @param tramiteId
     * @param idTipoDocumento
     * @return
     */
    public List<TramiteDocumento> obtenerTramiteDocumentosDeTramitePorTipoDoc(
        long tramiteId, long idTipoDocumento);

    /**
     * obtiene la lista de objetos TramiteDocumento (haciendo fetch del Documento) del trámite dado,
     * para los cuales el Documento relacionado es de pruebas
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<TramiteDocumento> obtenerTramiteDocumentosDePruebasPorTramiteId(
        long tramiteId);

    /**
     * Método que recupera la lista de {@link Tramite} de una lista de actividades que están en la
     * actividad ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA
     *
     * @author david.cifuentes
     * @param actividades
     * @return
     */
    public List<Tramite> recuperarTramitesConDocumentosFaltantes(List<Actividad> actividades);

    /**
     * obtiene la lista completa de solicitantes de un trámite, la cual es así: todos los
     * solicitantes del trámite (si los hay) + todos los solicitantes de la solicitud (que los
     * debería haber); excepto en el caso de que la solicitud sea de tipo Avisos, cuando trae solo
     * los del trámite
     *
     * @author pedro.garcia
     * @param tramiteId
     * @param solicitudId
     * @param tipoSolicitud
     * @return
     */
    public List<Solicitante> obtenerSolicitantesTodosDeTramite(
        long tramiteId, long solicitudId, String tipoSolicitud);

    /**
     * Obtiene la lista de tareas geográficas de un usuario incluyendo los detalles de cada una de
     * las actividades.
     *
     * @author juan.mendez
     * @param usuario
     * @return
     */
    public ResultadoVO obtenerTareasGeograficas(UsuarioDTO usuario);

    /**
     * Obtiene la lista de tareas geográficas de un usuario incluyendo los detalles los tramites de
     * depuracion y las rectificaciones de areas con linderos de cada una de las actividades.
     *
     * @author andres.eslava
     * @param usuario
     * @return
     */
    public ResultadoVO obtenerTareasGeograficas2(UsuarioDTO usuario);

    /**
     * Obtiene un {@link ModeloResolucion} por su id
     *
     * @author david.cifuentes
     * @param idModeloResolucion
     * @return
     */
    public ModeloResolucion buscarModeloResolucionPorId(Long idModeloResolucion);

    /**
     * Obtiene una lista de {@link Tramite} filtrada por diferentes parametros
     *
     * @author david.cifuentes
     * @param estadoTramite
     * @param numeroSolicitudBusqueda
     * @param numeroRadicacionBusqueda
     * @param datosConsultaPredio
     * @param solicitanteBusqueda
     * @return
     */
    public List<Tramite> buscarTramitesPorFiltros(String estadoTramite,
        String numeroSolicitudBusqueda, String numeroRadicacionBusqueda,
        FiltroDatosConsultaPredio datosConsultaPredio,
        FiltroDatosConsultaSolicitante solicitanteBusqueda);

    /**
     * Método que permite recuperar las imagenes inicial y final del procesamiento geográfico del
     * trámite, dondel el primer campo corresponde a la ruta del repositorio documental para la
     * imagen inicial del predio y el segundo campo a la imagen final
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public String[] recuperarRutaAlfrescoImagenInicialYfinalDelTramite(
        Long tramiteId);

    /**
     * Método que permite recuperar el trámite y trae el documento resolución asociado
     *
     * @author javier.aponte
     * @param currentTramiteId
     * @return Tramite
     */
    public Tramite buscarTramiteTraerDocumentoResolucionPorIdTramite(Long currentTramiteId);

    /**
     * Método que elimina una solicitud aviso registro
     *
     * @param solicitudAvisoRegistro
     * @author fabio.navarrete
     */
    public void eliminarSolicitudAvisoRegistro(
        SolicitudAvisoRegistro solicitudAvisoRegistro);

    /**
     * Método que almacena el trámite y luego de esto ejecuta un SP para generar la proyección de
     * dicho trámite.
     *
     * @param tramite
     * @author fabio.navarrete
     */
    public Object[] generarProyeccion(Tramite tramite);

    /**
     * Método encargado de eliminar la proyección asociada a un trámite
     *
     * @param tramite
     * @author fabio.navarrete
     */
    public void eliminarProyeccion(Tramite tramite);

    /**
     * Método que genera la proyección de un trámite y actualiza la condición de propiedad requerida
     * para el predio principal
     *
     * @param tramite
     * @author fabio.navarrete
     */
    public Object[] generarProyeccionActualizarCondicionPropiedad(Tramite tramite,
        String condicionPropiedad);

    /**
     * Retorna todos los solicitantes de la tabla
     *
     * @param startIdx índice inicial para búsqueda en la tabla solicitante
     * @param rowNum cantidad de registros a retornar
     * @return
     * @author fabio.navarrete
     */
    public List<Solicitante> buscarTodosSolicitantes(int startIdx, int rowNum);

    /**
     * inserta en masa registros a la tabla COMISION_TRAMITE_DATO
     *
     * @author pedro.garcia
     * @param registros
     */
    public void insertarRegistrosComisionTramiteDato(List<ComisionTramiteDato> registros);

    /**
     * Actualiza la el registro en tabla COMISION_TRAMITE correspondienta a esta objeto Se modifica
     * para que retorne el objeto actualizado
     *
     * @author pedro.garcia
     * @modified javier.aponte
     * @param comisionTramite
     */
    public ComisionTramite actualizarComisionTramite(ComisionTramite comisionTramite);

    /**
     * Borra los registros de la tabla COMISION_TRAMITE_DATO que correspondan al id de la
     * ComisionTramite
     *
     * @author pedro.garcia
     * @param idComisionTramite id de la ComisionTramite por el que se buscan los registros que se
     * van a eliminar
     */
    public void borrarComisionTramiteDatosPorComisionTramite(Long idComisionTramite);

    /**
     * Método que consulta una lista de trámites a partir de los datos ingresados en el filtro
     *
     * <b>OJO: solo usar para la búsqueda de trámites para cancelación porque además de los datos en
     * el filtro, filtra también por ciertos estados de los trámites</b>
     *
     * @param filtro
     * @return
     * @author fabio.navarrete
     */
    public List<Tramite> buscarTramitesParaCancelacion(String estructuraOrgCod,
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize);

    /**
     * Método que consulta una lista de trámites a partir de los datos ingresados en el filtro
     *
     * <b>OJO: solo usar para la búsqueda de trámites para cancelación porque además de los datos en
     * el filtro, filtra también por ciertos estados de los trámites</b>
     *
     * @param filtro
     * @return
     * @author cesar.vega
     */
    public List<Tramite> buscarTramitesParaCancelacion(
        FiltroDatosConsultaCancelarTramites filtro, int first, int pageSize);

    /**
     * Busca un trámite por su id haciendo fetch del Documento resultado que es donde se guarda la
     * resolución y de todos los demás datos que necesita para ver detalles del trámite
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite buscarTramiteCompletoConResolucion(Long tramiteId);

    /**
     * Busca los trámites por su id haciendo fetch del Documento resultado que es donde se guarda la
     * resolución y de todos los demás datos que necesita para ver detalles del trámite
     *
     * @author pedro.garcia
     * @param tramitesIds
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    public List<Tramite> buscarTramitesCompletosConResolucion(
        long[] tramitesIds, int... contadoresDesdeYCuantos);

    /**
     * Cuenta las comisiones que estén relacionadas con trámites que estén a cargo del jefe de
     * conservación o del director de la territorial dada, cuyo rol se identifica con el parámetro
     * rolUsuario.
     *
     * Estas comisiones son las que se ven en la pantalla "Administración de comisiones"
     *
     * @author pedro.garcia
     * @param idTerritorial
     * @param rolUsuario
     */
    public int contarComisionesParaAdministrar(String idTerritorial, String rolUsuario);

    /**
     * Busca las comisiones (siempre están asociadas a algún trámite) que estén relacionadas con
     * trámites que estén a cargo del jefe de conservación o del director de la territorial dada,
     * cuyo rol se identifica con el parámetro rolUsuario. Tener en cuenta que la consulta se hace
     * sobre una vista y que los objetos devueltos son entities generados a partir de ésta.
     *
     * Estas comisiones son las que se ven en la pantalla "Administración de comisiones"
     *
     * @author pedro.garcia
     *
     * @param territorialId
     * @param rolUsuario
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    public List<VComision> buscarComisionesParaAdministrar(String idTerritorial, String rolUsuario,
        String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Método que retorna el contéo de la búsqueda de trámites para cancelación.
     *
     * @param filtro
     * @return
     * @author fabio.navarrete
     */
    public int contarResultadosBusquedaTramiteCancelacion(String estructuraOrgCod,
        FiltroDatosConsultaCancelarTramites filtro);

    /**
     * Método que retorna el contéo de la búsqueda de trámites para cancelación.
     *
     * @param filtro
     * @return
     * @author cesar.vega
     */
    public int contarResultadosBusquedaTramiteCancelacion(
        FiltroDatosConsultaCancelarTramites filtro);

    /**
     * inserta una ComisionEstado
     *
     * @author pedro.garcia
     * @param nuevaComisionEstado
     * @param loggedInUser
     */
    public void insertarComisionEstado(ComisionEstado nuevaComisionEstado, UsuarioDTO loggedInUser);

    /**
     *
     * Método que actualiza el trámite, genera la proyección, actualiza la condición de propiedad
     * del predio y crea la ficha matriz asociada. Método elaborado para trámites de desenglobe con
     * relación a PH o Condominio
     *
     * @param tramite
     * @param condicionPropiedad
     * @param usuarioDto
     * @author fabio.navarrete
     */
    public void generarProyeccionActualizarCondicionPropiedadYFichaMatriz(
        Tramite tramite, String condicionPropiedad, UsuarioDTO usuarioDto);

    /**
     * Consulta la documentación de un trámite. Trae el trámite habiéndole hecho fetch con los
     * TramiteDocumentacion y con la solicitud (para obtener datos de esta)
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public Tramite getDocsOfTramite(Long tramiteId);

    /**
     * Consulta el último registro de cambio de estado de una comisión en la tabla COMISION_ESTADO.
     *
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-07
     *
     * @param idComision id de la comisión
     * @return
     */
    public ComisionEstado consultarUltimoCambioEstadoComision(Long idComision);

    /**
     * Consulta el histórico de estados de una comisión en la tabla COMISION_ESTADO.
     *
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-07
     *
     * @param idComision id de la comisión
     * @return una lista con los objetos correspondientes a los registros
     */
    public List<ComisionEstado> consultarHistoricoCambiosEstadoComision(Long idComision);

    /**
     * Elimina los registros de la tabla COMISION_TRAMITE que están asociados a la comisión con el
     * id dado. Se usa cuando se cancela una comisión, lo que implica que todos los trámites
     * asociados deben liberarse.
     *
     * @pedro.garcia
     * @version 2.0
     * @cu CU-CO-A-07
     * @param currentUser
     * @param idComision
     */
    public boolean borrarComisionTramitesDeComision(UsuarioDTO currentUser, Long idComision);

    /**
     * Elimina un solicitante de la base de datos
     *
     * @author franz.gamba
     * @version 2.0
     * @cu CU-CO-E-09
     * @param solicitante
     */
    public void eliminarSolicitante(Solicitante solicitante);

    /**
     * Elimina un solicitante de la base de datos
     *
     * @author franz.gamba
     * @version 2.0
     * @cu CU-CO-E-09
     * @param solicitante
     */
    public void eliminarSolicitantes(List<Solicitante> solicitantes);

    /**
     * Elimina un TramitePruebaEntidad de la base de datos
     *
     * @author franz.gamba
     * @version 2.0
     * @cu CU-CO-E-09
     * @param tramitePruebaEntidad
     */
    public void eliminarTramitePruebaEntidad(TramitePruebaEntidad tramitePruebaEntidad);

    /**
     * Cancela un trámite en cualquier momento del proceso
     *
     * @author juan.agudelo
     * @version 2.0
     * @cu CU-NP-CO-166
     * @param selectedTramite
     * @param selectedTramiteEstado
     * @param usuario
     * @param documentoSoporteCancelacion
     * @return
     */
    public boolean cancelarTramite(Tramite selectedTramite,
        TramiteEstado selectedTramiteEstado, UsuarioDTO usuario,
        Documento documentoSoporteCancelacion);

    /**
     * Busca los trámites que estén en ejecución y relacionados con un predio
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId
     * @return
     */
    public List<Tramite> buscarTramitesPendientesDePredio(Long predioId);

    /**
     * Busca los trámites que ya hayan sido finalizados relacionados con un predio
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId
     * @return
     */
    public List<Tramite> buscarTramitesFinalizadosDePredio(Long predioId);

    /**
     * Metodo que retorna las entidades asignadas a pruebas de un tramite segun el id del tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<TramitePruebaEntidad> getTramitePruebaEntidadsByTramiteId(Long tramiteId);

    /**
     * Busqueda de trámites por filtro
     *
     * @author juan.agudelo
     * @version 2.0
     * @cu CU-NP-CO-166
     * @param filtroBusquedaTramite
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> buscarTramiteSolicitudPorFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        final int... rowStartIdxAndCount);

    /**
     * Contar trámites por filtro
     *
     * @author juan.agudelo
     * @version 2.0
     * @cu CU-NP-CO-166
     * @param filtroBusquedaTramite
     * @return
     */
    public Integer contarTramiteSolicitudPorFiltroTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite);

    /**
     *
     * Metodo que retorna n objetos de SolicitanteSolicitud para utilizarlos en pruebas
     *
     * @author fredy.wilches
     * @version 2.0
     * @return
     */
    public List<SolicitanteSolicitud> getSolicitanteSolicitudAleatorio();

    /**
     * Metodo que retorna los tramites sin ejecutor asignado mediante un listado de identificadores
     *
     * @author franz.gamba
     * @param tramiteIds
     * @return
     */
    public List<Tramite> obtenerSaldosDeConservacion(List<Long> tramiteIds);

    /**
     * Metodo que retorna 1 Comision existente usada para pruebas
     *
     * @author fredy.wilches
     * @version 2.0
     * @return
     */
    public Comision getComisionParaTests();

    /**
     * Método que busca una solicitud por filtro de busqueda
     *
     * @author juan.agudelo
     * @version 2.0
     * @param datosFiltroSolicitud
     * @param first
     * @return
     */
    public List<VSolicitudPredio> buscarSolicitudesPaginadasPorFiltro(
        VSolicitudPredio datosFiltroSolicitud, String sortField, String sortOrder,
        Map<String, String> filters,
        final int... first);

    /**
     * Método que busca una solicitud por filtro de busqueda
     *
     * @author juan.agudelo
     * @version 2.0
     * @param datosFiltroSolicitud
     * @return
     */
    public Integer contarSolicitudesPaginadasPorFiltro(
        VSolicitudPredio datosFiltroSolicitud);

    /**
     * obtiene la lista completa de solicitantes y afectados de un trámite por trámite id
     *
     * @author juan.agudelo
     * @version 2.0
     * @param tramiteId
     * @return
     */
    public List<Solicitante> obtenerSolicitantesParaNotificarPorTramiteId(
        long tramiteId);

    /**
     * Obtiene la duración de una comisión a partir de las relaciones entre comisión y trámites. En
     * este método se aplica la nueva regla definida para el cálculo de la duración de las
     * comisiones de Conservación. Está en sql nativo. Ver implementación del método para la
     * documentación de la regla.
     *
     * @author pedro.garcia
     * @param idComision
     *
     * @return
     */
    public Double obtenerDuracionComision2(Long idComision);

    /**
     *
     * Método que guarda un {@link TramiteDocumentacion}.
     *
     * @author david.cifuentes
     * @param tramiteDocumentacion
     * @return
     */
    public TramiteDocumentacion guardarTramiteDocumentacion(
        TramiteDocumentacion tramiteDocumentacion);

    /**
     *
     * Método que busca una {@link Solicitud} con sus {@link SolicitanteSolicitud} asociados.
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    public Solicitud buscarSolicitudConSolicitantesSolicitudPorId(Long solicitudId);

    /**
     *
     * Método que busca una {@link Solicitud} con sus {@link SolicitanteSolicitud} asociados. sin
     * hacer fetch de los tramites
     *
     * @author felipe.cadena
     * @param Long
     * @return
     */
    public Solicitud buscarSolicitudConSolicitantesSolicitudPorIdNoTramites(Long solicitudId);

    /**
     * Método que almacena un documento en la base de datos y en alfresco, debe contener por lo
     * menos un trámite o un predio asociados (el id de alguno de ellos), un objeto del tipo
     * tipoDocumento, el nombre del archivo, el estado del documento y el identificador de si se
     * encuentra bloqueado
     *
     * Si el documento contiene un trámite o un trámite y un predio vinculados al documento este se
     * asociara al tramite, si contiene solo contiene un predio vinculado el documento se asociara
     * al predio, si no contiene ni trámite ni predio vinculados el documento no se cargará
     *
     * Copia del método de juan.agudelo ajustado para llamar la función en alfresco que asocia el
     * documento sin un predio asociado. Se dá para los trámites de mutación de quinta y las
     * solicitudes de via gubernativa.
     *
     * @author david.cifuentes
     * @param usuario
     * @param documento
     * @return
     */
    public Documento guardarDocumentoSinPredio(UsuarioDTO usuario,
        Documento documento);

    /**
     *
     * Método que busca el trámite asociado al número de resolución enviado como parametro.
     *
     * @author david.cifuentes
     *
     * @param resolucion
     * @return
     */
    public List<Tramite> buscarTramitesPorNumeroDeResolucion(String resolucion);

    /**
     * Método que obtiene los tramiteDocumentos asociados a un trámite por trámite id, Recupera los
     * Documento relacionados a cada TramiteDocumento
     *
     * @author juan.agudelo
     * @version 2.0
     * @return
     */
    public List<TramiteDocumento> obtenerTramiteDocumentosPorTramiteId(Long tramiteId);

    /**
     * Método que guarda o actualiza un ComisionEstado
     *
     * @author david.cifuentes
     * @param comisionEstado
     */
    public ComisionEstado guardarActualizarComisionEstado(ComisionEstado comisionEstado);

    /**
     *
     * Método que retorna los números prediales asociados a uno o varios trámites de segunda
     * (englobe)
     *
     * @author lorena.salamanca
     * @param tramiteIds
     * @return
     */
    public List<String> getNumerosPredialesByTramiteId(List<Long> tramiteIds);

    /**
     * Método que actualiza los TramiteDocumentacion de un tramite, asociando los documentos
     * cargados a alfresco.
     *
     * @author david.cifuentes
     * @param tramiteDocumentacion
     * @param solicitud
     * @param usuario
     */
    public List<TramiteDocumentacion> actualizarTramiteDocumentacionConAlfresco(
        List<TramiteDocumentacion> tramiteDocumentacion, Solicitud solicitud, UsuarioDTO usuario);

    /**
     * Método que retorna las entidades encargadas de las pruebas de un tramite con el numero de
     * documentos asociados a dichas pruebas
     *
     * @param tramiteId
     * @return
     */
    public List<TramitePruebaEntidad> buscarTramitePruebaEntidadsconNumeroDocumentos(Long tramiteId);

    /**
     * Método que retorna los documentos asociados a las pruebas del trámite con la entidad
     * seleccionada
     *
     * @param tramiteId
     * @param identificacion
     * @return
     */
    public List<TramiteDocumento> obtenerDocumentosPruebasDeEntidad(Long tramRiteId,
        String identificacion);

    /**
     * Método qeu actualiza un objeto de tipo documento en la base de datos
     *
     * @author franz.gamba
     * @param documento
     * @return
     */
    public Documento guardarYactualizarDocumento(Documento documento);

    /**
     * Método que elimina un TramiteDocumento con el Documento asociado a este
     *
     * @author franz.gamba
     * @param tramiteDocumento
     */
    public boolean eliminarTramiteDocumentoConDocumentoAsociado(TramiteDocumento tramiteDocumento);

    /**
     * Método para actualizar el archivo asociado al TramiteDocumentacion en alfresco
     *
     * @author felipe.cadena
     * @param documento
     * @param usuario
     * @return
     */
    public TramiteDocumentacion actualizarTramiteDocumentacionEnAlfresco(
        TramiteDocumentacion tramiteDocumentacion, UsuarioDTO usuario);

    /**
     * Método que elimina un TramiteDocumentacion con el Documento asociado a este
     *
     * @author franz.gamba
     * @param tramiteDocumentacion
     */
    public boolean eliminarTramiteDocumentacionConDocumentoAsociado(
        TramiteDocumentacion tramiteDocumentacion);

    /**
     * Método que permite determinar si es necesario mostrar el visor dependiendo del tipo de
     * trámite
     *
     * @author javier.aponte
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipoTramite
     */
    public boolean verVisorGISPorTipoTramite(String tipoTramite, String claseMutacion,
        String subtipoTramite);

    /**
     * Método que retorna la lista de los ids de tramites que se encuentran para notificar,
     * consultados por el filtro de busqueda
     *
     * @author franz.gamba
     * @param filtroBusquedaTramite
     * @param nombreActividadSelected
     * @param rowStartIdxAndCount
     * @return
     */
    /**
     * @modified by leidy.gonzalez::08-08-2018::#70681::Reclamar actividad por parte del usuario autenticado
     */ 
    public List<Long> obtenerTramiteIdsParaNotificarPorFiltro(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String nombreActividadSelected, List<Long> iDsTramitesProc,final int... rowStartIdxAndCount);

    /**
     * Método que retorna lso tramites que se encuentran en la actividad de notifircar Resolucion
     * mediante su Id y el filtro de busqueda
     *
     * @author franz.gamba
     * @param tramiteIds
     * @param sortField
     * @param filtroBusquedaTramite
     * @param sortOrder
     * @param filters
     * @param contadoresDesdeYCuantos
     * @return
     */
    public List<Tramite> obtenerTramitesParaNotificarPorFiltro(List<Long> tramiteIds,
        String sortField, FiltroDatosConsultaTramite filtroBusquedaTramite,
        String sortOrder, Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Método que retona los ids de los tramites asociados a un tramite mediante el id del tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<Long> obtenerIdsDeTramitesAsociadosPorTramiteId(Long tramiteId);

    /**
     * Método que trae los tramites para aprobar mediante los ids
     *
     * @author franz.gamba
     * @param tramiteIds
     * @return
     */
    public List<Tramite> obtenerTramitesParaAprobarPorTramiteIds(List<Long> tramiteIds);

    /**
     * Obtiene (debería existir) el TramiteDocumento que se crea en el momento de comunicar la
     * notificación de resolución y que se va a modificar cuando se registra la notificación
     *
     * PRE: el documento debe existir. Si no, hay un error en el proceso
     *
     * @author pedro.garcia
     *
     * @param idTramite id del trámite
     * @param docPersonaNotificacion documento de la persona a notificar
     * @return
     */
    public TramiteDocumento obtenerTramiteDocumentoParaRegistroNotificacion(
        Long idTramite, String docPersonaNotificacion);

    /**
     * Guarda los Documento correspondientes al documento de notificación y el de autorización del
     * registro de notificación, y hace la actualización del TramiteDocumento relacionado
     *
     * @author pedro.garcia
     * @version 2.0
     * @cu CU-CO-V-09
     *
     * @param idTramite
     * @param documentoNotificacion
     * @param documentoAutorizacion
     * @param tramiteDocumentoNotificacion
     * @param usuario
     * @return
     */
    public boolean guardarDocumentosRegistroNotificacion(Long idTramite,
        Documento documentoNotificacion, Documento documentoAutorizacion,
        TramiteDocumento tramiteDocumentoNotificacion, UsuarioDTO usuario);

    /**
     * Método que retorna el objeto completo de tramite de los tramites asociados a un tramite
     *
     * @author lorena.salamanca
     * @param tramiteId
     * @return
     */
    public List<Tramite> getTramiteTramitesbyTramiteId(Long tramiteId);

    /**
     * Método que permite obtener la url del reporte en el servidor de jasper de los reportes de las
     * resoluciones de conservación
     *
     * @author javier.aponte
     * @param tramiteId
     * @return EReporteServiceSNC
     */
    public EReporteServiceSNC obtenerUrlReporteResoluciones(Long tramiteId);

    /**
     * Método que permite obtener la url del subreporte en el servidor de jasper de la parte del
     * resuelve de las resoluciones
     *
     * @author javier.aponte
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @return
     */
    public String obtenerUrlSubreporteResolucionesResuelve(Long tramiteId);

    /**
     * Método que elimina un trámite de la base de datos
     *
     * @author franz.gamba
     * @param tramite
     * @return
     */
    public boolean removerTramite(Tramite tramite);

    /**
     * Metodo que avanza a elaborar informe desde cargar prueba
     *
     * @param activityId
     * @param tramite
     * @param usuario
     */
    public void enviarTramitesAsociadosAElaborarInforme(String activityId, Tramite tramite,
        UsuarioDTO usuario);

    /**
     * Metodo que avanza a modificar información alfanumérica desde cargar prueba
     *
     * @param activityId
     * @param tramite
     * @param usuario
     */
    public void enviarTramitesAsociadosAModificarInformacionAlfanumerica(String activityId,
        Tramite tramite, UsuarioDTO usuario);

    /**
     * Metodo que avanza a aprobar desde Asociar documento al nuevo tramite
     *
     * @param activityId
     * @param tramite
     * @param usuario
     */
    public void enviarTramitesAsociadosAAprobar(String activityId, Tramite tramite,
        UsuarioDTO usuario);

    /**
     * Método que avanza el proceso de modificar info. geográfica a modificar info. alfanumérica
     *
     * @author franz.gamba
     * @param usrLogin
     * @return
     */
    public boolean avanzarProcesoAModifInfoAlfanumerica(Long tramiteId, String actividadActual,
        UsuarioDTO unUsuario);

    /**
     * Carga una lsita de solicitantes solicitud filtradaa por id solicitud y relación
     *
     * @author christian.rodriguez
     * @param idSolicitud
     * @param solicitanteSolicitudEstado
     * @return
     */
    public List<SolicitanteSolicitud> buscarSolicitantesSolicitudBySolicitudIdRelacion(
        Long idSolicitud, String solicitanteSolicitudEstado);

    /**
     * Busca un solicitante predio en la BD
     *
     * @author christian.rodriguez
     * @param solicitudPredioId Id del solicitud predio que se desea consultar
     * @return objeto de tipo solicitud predio si existe en la bd o null si no
     */
    public SolicitudPredio buscarSolicitudPredioById(Long solicitudPredioId);

    /**
     * Busca los predios de una solicitud segun el id de la solicitud
     *
     * @author christian.rodriguez
     * @param idSolicitud id de la solicitud
     * @return lista con los predios (SolicitudPredio) asociados a la solicitud
     */
    public List<SolicitudPredio> buscarSolicitudesPredioBySolicitudId(Long idSolicitud);

    /**
     * Busca los predios asociados a una solicitud pero que no estén asociados a ningún avalúo de la
     * solicitud
     *
     * @author christian.rodriguez
     * @param idSolicitud id de la solicitud
     * @return lista de los predios asociados a la solicitud con el contacto, departamento y
     * municipio cargados
     */
    public List<SolicitudPredio> buscarSolicitudPredioSinAvaluoPorSolicitudId(Long idSolicitud);

    /**
     * Guarda o actualziar una lsita de solicitudes predio
     *
     * @author christian.rodriguez
     * @param solicitudesPredio lista con las solicitudes predio
     * @return lsita con las solicitudes predio que fueron guardadas
     */
    public List<SolicitudPredio> guardarActualizarSolicitudesPredios(
        List<SolicitudPredio> solicitudesPredio);

    /**
     * Guarda o actualziar un solicitud predio
     *
     * @author christian.rodriguez
     * @param solicitudesPredio
     * @return solicitud predio modificado
     */
    public SolicitudPredio guardarActualizarSolicitudPredio(SolicitudPredio solicitudesPredio);

    /**
     * Borra de la bd los predios de la solicitud enviados
     *
     * @author christian.rodriguez
     * @param solicitudPredios lista de solicitud predios a borrar
     */
    public void eliminarSolicitudPredios(List<SolicitudPredio> solicitudPredios);

    /**
     * Actualiza en BD un TramitePredioEnglobe
     *
     * @author franz.gamba
     * @param tramitePredioEnglobe
     * @return
     */
    public TramitePredioEnglobe guardarYactualizarTramitePredioEnglobe(
        TramitePredioEnglobe tramitePredioEnglobe);

    /**
     * Método que toma la informacion de la edicion geografica y la actualiza en la BD
     *
     * @author franz.gamba
     * @param predios
     * @param areas
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean updateAreasDePrediosEdicionGeografica(
        List<String> predios, List<Double> areas, Tramite tramite, UsuarioDTO usuario,
        Boolean isTramiteMasivo);

    /**
     * Actualiza las zonas asociadas a los predios de un tramite
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean actualizacionPPredioZonas(Tramite tramite, UsuarioDTO usuario,
        Boolean isTramiteMasivo);

    /**
     * Actualiza las zonas asociadas a los predios de un tramite
     *
     *
     * @author felipe.cadena
     * @param zonasPredio
     * @param usuario
     * @param numPredio
     *
     *
     */
    public void actualizarZonasPorPredio(UsuarioDTO usuario,
        HashMap<String, List<ZonasFisicasGeoeconomicasVO>> zonasGeograficas, Tramite tramite);

    /**
     * Método que borra un objeto TramiteDocumentacion
     *
     * @author lorena.salamanca
     * @param tramiteDocumentacionEliminar
     */
    public void eliminarTramiteDocumentacion(TramiteDocumentacion tramiteDocumentacionEliminar);

    /**
     * Método que retorna la lista de PUnidadConstruccion asociados a un tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstruccionsByTramiteId(Long tramiteId);

    /**
     * Método que retorna la lista de PUnidadConstruccion asociados a un predio
     *
     * @author franz.gamba
     * @modifiedBy fredy.wilches
     * @param predioId
     * @param incluirCancelados
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstruccionsByPredioId(Long predioId,
        boolean incluirCancelados);

    /**
     * Método que almacena o actualiza una solicitud de documentación
     *
     * @author felipe.cadena
     * @param solicitudDocumentacion
     *
     * @return
     */
    public SolicitudDocumentacion guardarActualizarSolicitudDocumentacion(
        SolicitudDocumentacion solicitudDocumentacion);

    /**
     * Método que retorna un boolean diciendo si el trámite existe o no en base de datos segun su id
     *
     * @param tramiteId
     * @return
     */
    public boolean existeElTramite(Long tramiteId);

    /**
     * Busqueda de trámites por filtro para el caso de uso de administración de asignaciones
     *
     * @author javier.aponte
     * @version 2.0
     * @cu CU-NP-CO-171
     * @param filtroBusquedaTramite
     * @param sortField
     * @param sortOrder
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Tramite> buscarTramitePorFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount);

    /**
     * Método que cuenta el número de trámites por filtro para el caso de uso de administración de
     * asignaciones
     *
     * @author javier.aponte
     * @version 1.0
     * @cu CU-NP-CO-171
     * @param filtroBusquedaTramite
     * @return
     */
    public Integer contarTramitePorFiltroTramiteAdministracionAsignaciones(
        FiltroDatosConsultaTramite filtroBusquedaTramite);

    /**
     * Método que retorna la lista de numeros de radicados de los tramites asociados al numero
     * predial
     *
     * @param Numero Predial
     * @author javier.barajas
     * @return
     */
    public List<Tramite> buscarNoRadicadosTramiteporNumeroPredial(String numPredial);

    /**
     * Método que retorna la lista de tramites en sus distintos estados asociados a un predio
     *
     * @param numPredial
     * @author dumar.penuela
     * @return
     */
    public List<Tramite> buscarTramitesTiposTramitesByNumeroPredial(String numPredial);

    /**
     * Método que retorna la lista de tramites en sus distintos estados asociados a un predio a
     * partir de una persona
     *
     * @param numIdentificacion
     * @param tipoIdentificacion
     * @author dumar.penuela
     * @return
     */
    public List<Tramite> buscarTramitesTiposTramitesByNumeroIdentificacion(String numIdentificacion,
        String tipoIdentificacion);

    /**
     * Método que retorna las unidades de contruccion asociadas a un predio
     *
     * @author franz.gamba
     * @param predioId
     * @return
     */
    public List<UnidadConstruccion> obtenerUnidadesConstruccionsPorPredioId(Long predioId);

    /**
     * Método que retorna la imagen posterior a la edicion geografica de un tramite en caso que esta
     * exista
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public Documento obtenerImagenDespuesEdicionPorTramiteId(Long tramiteId);

    /**
     *
     * @author lorena.salamanca
     * @param tramiteId, idTipoDocumento
     *
     */
    public String getIdRepositorioDocumentosDeReplicaGDBdeTramite(Long tramiteId,
        Long idTipoDocumento);

    /**
     * valida si existe una imagen inicial para la edicion de un tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public boolean existeImagenInicialTramitePorIdTramite(Long tramiteId);

    /**
     * Guarda un registro de solicitud documento
     *
     * @author felipe.cadena
     * @param solicitudDocumento
     * @return
     */
    public SolicitudDocumento guardarActualizarSolicitudDocumento(
        SolicitudDocumento solicitudDocumento);

    /**
     * Método que busca las comisiones trámite dato por el id del trámite
     *
     * @param tramiteId
     * @return
     * @author javier.aponte
     */
    public List<ComisionTramiteDato> buscarComisionTramiteDatoPorTramiteId(Long tramiteId);

    /**
     * Método que remueve una tramiteDetallePredio de la Base de datos
     *
     * @author franz.gamba
     * @param tdp
     * @return
     */
    public boolean removerTramiteDetallePredio(TramiteDetallePredio tdp);

    /**
     * Método que consulta el procedimiento almacenado validarProyeccion que realiza una validación
     * de la proyeccion hecha a un trámite y sus predios asociados para las secciones a las que
     * aplique dicho trámite.
     *
     * @author david.cifuentes
     * @param tramiteId - Id del trámite al cual se va a ejecutar la validación de su proyección.
     * @param tramiteSeccionDatosId - Id del tramiteSeccionDatos donde se especifica que secciones
     * aplican para dicho trámite.
     *
     * @return Arreglo de objetos con el id del trámite, el id del predio, el nombre de la sección y
     * el mensaje de error dirigido al usuario.
     */
    public Object[] validarProyeccion(Long tramiteId, Long tramiteSeccionDatosId);

    /**
     * Método qeu actualiza el registro de un tramiteDetallePredio
     *
     * @author franz.gamba
     * @param tpe
     * @return
     */
    public TramiteDetallePredio guardarYActualizarTramiteDetallePredio(TramiteDetallePredio tpe);

    /**
     * Metodo para persistir una lista de registros {@link TramiteDetallePredio}
     *
     * @author felipe.cadena
     * @param tdps
     */
    public void guardarYActualizarTramiteDetallePredioMultiple(
        List<TramiteDetallePredio> tdps);

    /**
     * Método que consulta una lista de trámites filtrada por diferentes parametros.
     *
     * @author david.cifuentes
     *
     * @param filtroBaseDeDatosBool
     * @param filtrosTramite
     * @param filtrosPredio
     * @param filtrosSolicitante
     * @param filtroProcessBool
     * @param filtroFuncionarioEjecutor
     * @param filtroTerritorial
     * @param filtroUOC
     * @return
     */
    public List<Tramite> consultaDeTramitesPorFiltros(
        boolean filtroBaseDeDatosBool,
        FiltroDatosConsultaTramite filtrosTramite,
        FiltroDatosConsultaPredio filtrosPredio,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        boolean filtroProcessBool,
        String filtroFuncionarioEjecutor,
        EstructuraOrganizacional filtroTerritorial,
        EstructuraOrganizacional filtroUOC);

    /**
     * Método para eliminar un objeto de tipo SolicitudDocumentacion
     *
     * @author rodrigo.hernandez
     *
     * @param solicitudDocumentacion
     */
    public void borrarSolicitudDocumentacion(SolicitudDocumentacion solicitudDocumentacion);

    /**
     * Método para guardar/actualizar una lista de objetos de tipo TramiteDocumentacion
     *
     * @author rodrigo.hernandez
     *
     * @param tramiteDocumentacion
     */
    public void guardarActualizarListaTramiteDocumentacions(
        List<TramiteDocumentacion> listaTramiteDocumentacions);

    /**
     * Método para borrar un objeto de tipo TramiteDocumentacion
     *
     * @author rodrigo.hernandez
     *
     * @param tramiteDocumentacion
     */
    public void borrarListaTramiteDocumentacions(
        List<TramiteDocumentacion> listaTramiteDocumentacions);

    /**
     * Método que devuelve la Solicitud con los productos y documentos soporte
     *
     * @cu: CU-TV-PR-0001,CU-TV-PR-0002,CU-TV-PR-0003,CU-TV-PR-0004
     * @param solicitudId
     * @return Solicitud
     * @version:1.0
     * @author javier.barajas
     */
    public Solicitud buscarSolicitudConProductosYDocsSoporte(Long solicitudId);

    /**
     * Método que guarda y actualiza una Solicitud
     *
     * @param Solicitud
     * @return Solicitud
     * @version:1.0
     * @author javier.barajas
     */
    public Solicitud guardaYActualizaSolicitud(Solicitud sol);

    /**
     * Busca las comisiones (siempre están asociadas a algún trámite) que estén relacionadas con
     * trámites en el arreglo que se pasa como parametro, y que sean del tipo dado. Tener en cuenta
     * que la consulta se hace sobre una vista y que los objetos devueltos son entities generados a
     * partir de ésta.
     *
     * @note Método copiado del método buscarComisionesParaAdministrar y ajustado para las
     * comisiones para Aprobar.
     *
     * @author david.cifuentes
     *
     * @param idTramites lista de ids de trámites que se buscan
     * @param tipoComision tipo de comisiones que se buscan
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters Parámetros usados cuando se hacen filtros en las tablas
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     * @return
     */
    /*
     * @modified pedro.garcia 27-06-2013 adición de parámetro para tener en cuenta el tipo de
     * comisión
     */
    public List<VComision> buscarComisionesParaAprobar(List<Long> idTramites, String tipoComision,
        String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Cuenta las comisiones que estén relacionadas con trámites que estén en la lista de ids Estas
     * comisiones son las que el director de la territorial ve en la pantalla "Aprobar comisiones"
     *
     * @note Método copiado del método buscarComisionesParaAdministrar y ajustado para las
     * comisiones para Aprobar.
     *
     * @author david.cifuentes
     * @param idTramites
     * @param tipoComision tipo de comisiones que se buscan
     */
    public int contarComisionesParaAprobar(List<Long> idTramites, String tipoComision);

    /**
     * Utilizado para insertar varios TramiteEstado, usado inicialmente en la administración de
     * asignaciones
     *
     * @author fredy.wilches
     */
    public void insertarTramiteEstados(List<TramiteEstado> estados);

    /**
     * Método que busca una comisión por su id.
     *
     * @author david.cifuentes
     * @param idComision
     */
    public Comision buscarComisionPorId(Long idComision);

    /**
     * Trae los predios de una manzana(Utilizado en seleccion por visor)
     *
     * @cu: CU-TV-PR-0002 Certificaciones Prediales
     * @param String codManzana
     * @return List<Predio>
     * @version:1.0
     * @author javier.barajas
     */
    public List<Predio> buscaPrediosPorManzana(String codManzana);

    /**
     * Metodo para guardar la generacion de productos catastrales
     *
     * @param List<ProductoCatastral>
     * @return List<ProductoCatastral @cu CU-TV- PR-0001 INGRESAR SOLICITUD PRODUCTOS CATASTRALES
     * @version 1.0 @author javier.barajas
     *
     */
    public List<ProductoCatastral> guardarYactualizarProductosCatastralesTramite(
        List<ProductoCatastral> listaProductos);

    /**
     * Metodo para consultar producto catastral por id
     *
     * @param Long productoCatastralId
     * @return PorductoCatastral
     * @cu CU-TV-PR-0002 SOLICITUD DE PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public ProductoCatastral buscarProductosCatastralesporId(Long productoCatastralId);

    /**
     * Metodo para guardar y actualizar un producto catastral
     *
     * @param ProductoCatastral
     * @return ProductoCatastral
     * @cu CU-TV-PR-0001 INGRESAR SOLICITUD PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public ProductoCatastral guardarYactualizarProductoCatastralTramite(ProductoCatastral producto);

    /**
     * Consulta producto catastral detalle
     *
     * @param Long productoCatastralDetalleId
     * @return ProductoCatastralDetalle
     * @cu CU-TV-PR-0002 SOLICITUD GENERACION PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public ProductoCatastralDetalle buscaProductoCatastralDetalle(Long productoCatastralDetalleId);

    /**
     * Consulta las depuraciones asociadas a un Tramite por el id de este.
     *
     * @author felipe.cadena
     * @param idTramite
     * @return List<TramiteDepuracion>
     *
     */
    public List<TramiteDepuracion> buscarTramiteDepuracionPorIdTramite(Long idTramite);

    /**
     * Mètodo para obtener el último tramiteDepuracion asociado a un trámite
     *
     * @param idTramite
     * @return
     */
    public TramiteDepuracion buscarUltimoTramiteDepuracionPorIdTramite(Long idTramite);

    /**
     * Método para actulizar la entidad TramiteDepuracion
     *
     * @author felipe.cadena
     * @param tramiteDepuracion entidad a actualizar.
     * @return
     */
    public TramiteDepuracion actualizarTramiteDepuracion(TramiteDepuracion tramiteDepuracion);

    /**
     * Mètodo para guardar las inconsistencias geograficas asociadas a un tramite.
     *
     * @author felipe.cadena
     * @param tramiteInconsistencia
     * @return
     */
    public TramiteInconsistencia guardarActualizarTramiteInconsistencia(
        TramiteInconsistencia tramiteInconsistencia);

    /**
     * Mètodo para guardar las inconsistencias geograficas asociadas a un tramite de forma masiva
     *
     * @param tramiteInconsistencias
     * @return
     */
    public List<TramiteInconsistencia> guardarActualizarTramiteInconsistencia(
        List<TramiteInconsistencia> tramiteInconsistencias);

    /**
     * Consulta los ComisionTramite correspondientes al trámite con id dado, para los cuales la
     * comisión fue efectiva, es decir, llegó al estado 'finalizada'
     *
     * @author pedro.garcia
     * @param idTramite
     * @return
     */
    public List<ComisionTramite> consultarComisionesTramiteEfectivasTramite(Long idTramite);

    /**
     * Busca los trámites con id contenido en idsTramites.
     *
     * @author felipe.cadena
     * @param idsTramites arreglo con los id de trámite
     * @return
     */
    public List<TramiteDepuracion> buscarTramitesDepuracionParaAsignacionDeFuncionario(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Elimina las relaciones entre trámite y comisión que tengan como id de trámite alguno de los
     * ids que vienen en la lista, y como id de comisión el que viene como parámetro.
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param idsTramites
     * @param idComision
     *
     */
    public boolean eliminarTramiteComisionPorTramitesYComision(List<Long> idsTramites,
        Long idComision);

    /**
     * Método que consulta las actividades de una lista de trámites y asocia la respectiva actividad
     * del trámite al campo transient actividadActualTramite del trámite.
     *
     * @author david.cifuentes
     *
     * @param tramites Lista de objetos {@link Tramite} a los que se le consultarán su respectiva
     * {@link Actividad} en el proceso.
     *
     * @return Lista de objetos {@link Tramite} con {@link Actividad} asociada en el campo
     * actividadActualTramite.
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     */
    public List<Tramite> consultarActividadesDeTramites(List<Tramite> tramites)
        throws ExcepcionSNC;

    /**
     * Método para consultar la lista de trámites detalle predio por el id del trámite
     *
     * @param idTramite
     * @author javier.aponte
     */
    public List<TramiteDetallePredio> getTramiteDetallePredioByTramiteId(Long idTramite);

    /**
     * Hace las consultas y cálculos necesarios para armar los objetos con los datos que se
     * necesitan para los eventos de las programaciones de comisiones. Las programaciones de
     * comisiones se arman por territorial, para las comisiones de algún tipo, y para algún estado
     * de comisión
     *
     * @author pedro.garcia
     *
     * @param codigoEstructuraOrganizacional
     * @param tipoComision
     * @param estadoComision
     * @return
     */
    public List<EventoScheduleComisionesDTO> armarEventosScheduleComisiones(
        String codigoEstructuraOrganizacional, String tipoComision, String estadoComision);

    /**
     * Método que busca las comisiones que sean del tipo y estado dados, tal que los trámites que
     * pertenecen a esa comisión sean para un predio que esté en la territorial dada
     *
     * Basado en el método buscarComisionesPorIdTerritorialAndEstadoComision
     *
     * @author pedro.garcia
     *
     * @param idTerritorial
     * @param tipoComision (Conservación, Depuración, etc)
     * @param estado
     * @return
     */
    public List<Comision> buscarComisionesPorTerritorialYTipoYEstado(
        String territorialId, String tipoComision, String estadoComision);

    /**
     * Método que actualiza un documento
     *
     * @author juanfelipe.garcia
     * @param documento
     * @return
     */
    public boolean actualizarDocumento(Documento documento);

    /**
     * Método que actualiza TramiteDocumentacion particular
     *
     * @author juanfelipe.garcia
     * @param tramiteDocumentacionActual
     */
    public void actualizarTramiteDocumentacionSimple(TramiteDocumentacion tramiteDocumentacionActual);

    /**
     * Método que busca un documento por su id
     *
     * @author juanfelipe.garcia
     * @param tramiteDocumentacionActual
     */
    public Documento buscarDocumentoPorId(Long id);

    /**
     * Método que busca los documentos asociados a una solicitud por su id
     *
     * @author juanfelipe.garcia
     * @param tramiteDocumentacionActual
     */
    public List<Documento> buscarDocumentosPorSolicitudId(Long id);

    /**
     * Método para consultar un objeto VComision por su id
     *
     * @param idVComision
     * @author javier.aponte
     */
    public VComision getVComisionById(Long idVComision);

    /**
     * Método para consultar las inconsistencias asociadas a un Tramite
     *
     * @author felipe.cadena
     * @param idTramite
     * @return
     */
    public List<TramiteInconsistencia> buscarTramiteInconsistenciaPorTramiteId(Long idTramite);

    /**
     * Método para consultar las inconsistencias filtradas asociadas a un trámite para mostrar en la
     * capa web
     *
     * @author juanfelipe.garcia
     * @param idTramite
     * @return
     */
    public List<TramiteInconsistencia> buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(
        Long idTramite);

    /**
     * Método encargado de buscar los trámites por filtro de busqueda para asignarle derecho de
     * petición o tutela
     *
     * @author javier.aponte
     */
    public List<Tramite> buscarTramitePorFiltroTramiteDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante,
        String sortField, String sortOrder,
        final int... rowStartIdxAndCount);

    /**
     * Método encargado de contar los trámites por filtro de busqueda para asignarle derecho de
     * petición o tutela
     *
     * @author javier.aponte
     */
    public Integer contarTramitePorFiltroTramiteDerechoPeticionOTutela(
        FiltroDatosConsultaTramite filtroBusquedaTramite,
        FiltroDatosConsultaSolicitante filtrosSolicitante);

    /**
     * Metodo que recupera la informacion completa del tramite para edicion geografica
     *
     * @param idTramite identificador del tramite
     * @return resultado con informacion del tramite y predios asociados completos
     * @author andres.eslava
     */
    public ResultadoVO obtenerInformacionTramiteDepuracion(UsuarioDTO usuario, Long idTramite);

    /**
     * Metodo que recupera la informacion del predio en firme para consultar la informacion
     * alfanumerica por medio de un servicio REST
     *
     * @param usuario usuario autenticado
     * @param idTramite identificador del tramite
     * @return informacion expuesta a través del servicio REST
     *
     * @author andres.eslava
     */
    public ResultadoVO consultarPredioDepuracion(UsuarioDTO usuario, String numeroPredial);

    /**
     * Método que actualiza un {@link Tramite} Este método solo actualiza el trámite como tal y la
     * fechaLog y usuarioLog de los trámites asociados. No devuelven el trámite. Este es el que se
     * debe usar cuando se cuenta con un UsuarioDTO
     *
     * @author pedro.garcia (basado en {@link #actualizarTramite(co.gov.igac.snc.persistence.entity.tramite.Tramite, co.gov.igac.generales.dto.UsuarioDTO)
     * }
     *
     * @param tramite
     * @return true ai todo resulta bien
     */
    public boolean actualizarTramite2(Tramite tramite, UsuarioDTO usuario);

    /**
     * Método que guarda o actualiza un ComisionEstado Basado en
     * {@link #guardarActualizarComisionEstado(co.gov.igac.snc.persistence.entity.tramite.ComisionEstado)},
     * pero este retorna un booleano que indica si se pudo hacer la inserción/actualización
     *
     * @author pedro.garcia
     * @param comisionEstado
     * @return true si todo resulta bien
     */
    public boolean guardarActualizarComisionEstado2(ComisionEstado comisionEstado);

    /**
     * Decide si se debe actualizar el estado de una comisión cuando se está en la actividad 'cargar
     * información al SNC' En este caso se revisa si para todos los Comision_Tramite relacionados
     * con la comisión tienen valor 'SI' en el campo 'realizada', lo que indica que ya se dio el
     * resultado para todos ellos.
     *
     * @author pedro.garcia
     * @param idComision
     */
    public boolean decidirActualizacionEstadoComision1(Long idComision);

    /**
     * Metodo que elimina las tramites depuracion existentes para un tramite.
     *
     * @param tramiteId identificar del tramite
     * @author andres.eslava
     */
    public void eliminarTramitesInconsistencia(Long tramiteId);

    /**
     * Metodo para eliminar inconsistencias de tramite de manera masiva
     *
     * @author felipe.cadena
     * @param inconsistencias
     */
    public void eliminarTramiteInconsistencia(List<TramiteInconsistencia> inconsistencias);

    /**
     * Metodo que mantiene actualizada las inconsistencias de un tramite en depuracion
     *
     * @param inconsistencias inconsistencias enviadas por el editor.
     * @param actividadActual actividad process en la que se corrieron los validadores.
     * @param tramiteId identificador del tramite
     * @author andres.eslava
     *
     */
    public void actualizarTramitesDepuracion(List<TramiteInconsistencia> inconsistencias,
        String actividadActual, Long tramiteId, UsuarioDTO usuario);

    /**
     * Método que almacena o actualiza un trámite. NO modificar!!
     *
     * @author pedro.garcia
     * @param tramite
     * @return el trámite recién guardado o actualizado
     */
    public Tramite guardarActualizarTramite2(Tramite tramite);

    /**
     * Metodo para buscar las digitalizaciones asociadas a un tramite
     *
     * @author felipe.cadena
     *
     *
     * @param idTramite
     * @return
     */
    public List<TramiteDigitalizacion> buscarTramiteDigitalizacionPorTramite(Long idTramite);

    /**
     * Metodo para buscar las digitalizaciones activas asociadas a un usuario
     *
     * @author felipe.cadena
     *
     *
     * @param digitalizador
     * @return
     */
    public List<TramiteDigitalizacion> buscarTramiteDigitalizacionPorDigitalizador(
        String digitalizador);

    /**
     * Metodo para obtener el total de digitalizaciones activas de un usuario
     *
     * @author felipe.cadena
     *
     *
     * @param digitalizador
     * @return
     */
    public Integer contarTramiteDigitalizacionPorDigitalizador(String digitalizador);

    /**
     * Método para guardar el registro de las digitalizaciones
     *
     * @author felipe.cadena
     *
     * @param tramiteDigitalizacion
     * @return
     */
    public TramiteDigitalizacion guardarActualizarTramiteDigitalizacion(
        TramiteDigitalizacion tramiteDigitalizacion);

    /**
     * Método para obtener la digitalización activa asociada al trámite
     *
     * @author felipe.cadena
     *
     * @param usuario
     * @param idTramite
     * @return
     */
    public TramiteDigitalizacion buscarTramiteDigitalizacionActivaTramite(String usuario,
        Long idTramite);

    /**
     * Método para obtener el registro de digitalización por ejecutor
     *
     * @author felipe.cadena
     *
     * @param ejecutor
     * @param idTramite
     * @return
     */
    public TramiteDigitalizacion buscarTramiteDigitalizacionPorEjecutor(String ejecutor,
        Long idTramite);

    /**
     *
     * Método para obtener el tramite detalle predio asociado a un prtedio y un tramite determinado
     *
     * @author felipe.cadena
     *
     * @param idTramite
     * @param idpredio
     * @return
     *
     */
    public TramiteDetallePredio buscarTramiteDetallePredioPorTramiteIdYPredioId(Long idTramite,
        Long idpredio);

    /**
     *
     * Método para obtener las asociaciones ComisionTramite por el id de la comisión determinado
     *
     * @author juanfelipe.garcia
     *
     * @param idComision
     * @return
     *
     */
    public List<ComisionTramite> buscarComisionesTramitePorComisionId(Long idComision);

    /**
     * Actualizar una lista de trámites adicionales
     *
     * @author juanfelipe.garcia
     */
    public List<Tramite> actualizarTramitesAdicionales(List<Tramite> tramites);

    /**
     * Obtiene el último registro de cambio de estado para la comisión con id dado. Si el parámetro
     * estadoComision no es nulo, busca el último registro con ese estado. </br>
     *
     * @author pedro.garcia
     * @param idComision id de la comisión
     * @param estadoComision estado de la comisión
     * @return null o el registro buscado
     */
    public ComisionEstado obtenerUltimoComisionEstadoDeComision(Long id, String estadoComision);

    /**
     * Obtiene los tramites geograficos vigentes relacionados a una manzana determinada
     *
     * @author felipe.cadena
     * @param numeroManzana
     * @return
     */
    public List<Tramite> obtenerTramitesGeograficosPorManzana(String numeroManzana);

    /**
     * Método utilizado para guardar o actualizar una Solicitud. </br>
     * Se diferencia (aunque la lógica es la misma) de 
     * {@link #guardarActualizarSolicitud(co.gov.igac.generales.dto.UsuarioDTO, co.gov.igac.snc.persistence.entity.tramite.Solicitud) }
     * en que en este se devuelve, además, un hashmap con mensajes que quisiera ver el usuario, que
     * le dicen si el proceso de creación de la solicitud tuvo algún inconveniente.
     *
     * @author pedro.garcia
     * @param solicitud Solicitud a almacenar
     * @param usuario usuario de la sesión
     * @return Solicitud con información actualizada.
     *
     */
    public Object[] guardarActualizarSolicitud2(UsuarioDTO usuario, Solicitud solicitud);

    /**
     * Retorna los tramites geograficos de la manzana que esten en actividades del proceso
     * posteriores a la actividad enviada.
     *
     * @author felipe.cadena
     * @param numeroManzana
     * @return
     */
    public List<Tramite> obtenerTramitesPorManzanaPosteriores(String numeroManzana);

    /**
     * Metodo pra obtener todos los tramites de depuracion asociados a la manzana.
     *
     * @uthor felipe.cadena
     * @param numeroManzana
     * @param actividad
     * @return
     */
    public List<Tramite> obtenerTramitesPorManzanaDepuracion(String numeroManzana);

    /**
     *
     * Metodo para obtener la información predial proyectada asociada a un tramite
     *
     * @param idTramite identificador del tramite
     * @return objeto serializable que representa una agrupacion en respuesta del WS con la
     * informacion proyectada asociada al tramite
     *
     * @author andres.eslava
     *
     */
    public ActividadVO obtenerInformacionPredialProyectadaTramite(Long idTramite);

    /**
     * Método que realiza la consulta de los trámites depuración asociados a un digitalizador.
     *
     * @author david.cifuentes
     * @param digitalizador
     * @return
     */
    public List<TramiteDepuracion> buscarTramiteDepuracionPorDigitalizador(
        String digitalizador);

    /**
     * Método que busca los documentos asociados a una solicitud por su id
     *
     * @author javier.aponte
     * @param id
     */
    public List<Documento> buscarDocumentacionPorSolicitudId(Long id);

    /**
     * Consulta un trámite para la actividad 'revisar trámite devuelto'. </br>
     * Trae la documentación del trámite, la solicitud y la comisión de conservación a la que
     * pertenece, si es de oficina.
     *
     * @author pedro.garcia
     * @param tramiteId
     * @return
     */
    public Tramite consultarTramiteDevuelto(Long tramiteId);

    /**
     * Consulta las comisiones activas (estados: por ejecutar, en ejecución, aplazada, suspendida)
     * del tipo dado en las que está involucrado un trámite
     *
     * @param idTramite
     * @param tipoComision si es null, no se hace distinción del tipo de comisión
     * @return
     */
    public List<Comision> consultarComisionesActivasTramite(Long idTramite, String tipoComision);

    /**
     * Realiza la proyeccion del predio para via gubernativa
     *
     * @param tramiteId
     * @param tramiteRecurridoId
     * @param tipoRecurso
     * @return
     */
    public Object[] recurrirProyeccion(Long tramiteId, Long tramiteRecurridoId, String tipoRecurso,
        String usuario);

    /**
     * Método para eliminar un {@link TramiteInconsistencia}
     *
     * @author david.cifuentes
     * @param tramiteInconsistencia
     * @return
     */
    public boolean eliminarTramiteInconsistencia(
        TramiteInconsistencia tramiteInconsistencia);

    /**
     * Método que dada una lista de Id's de {@link Tramlte} verifica cuales tienen asociado un
     * {@link TramiteDepuracion}.
     *
     * @author david.cifuentes
     * @param tramiteIds
     * @return
     */
    public List<Long> verificarTramitesConRegistroTramiteDepuracion(
        List<Long> tramiteIds);

    /**
     * Método encargado de contar las solicitudes por filtro de busqueda
     *
     * @param filtroDatosSolicitud
     * @author javier.aponte
     */
    public Integer contarSolicitudesByFiltroSolicitud(FiltroDatosSolicitud filtroDatosSolicitud);

    /**
     * Obtiene la informacion basica para lista de tareas entragada al editor geografico
     *
     * @param usuario
     * @return objeto resultado serializable por el WS.
     * @author andres.eslava
     */
    public ResultadoVO obtenerTareasGeograficas3(UsuarioDTO usuario);

    /**
     * Obtiene la informacion detallada de un tramite para la edicion geografica
     *
     * @param tramiteId identificador del tramite
     * @return objeto resultado serializable por el WS.
     * @author andres.eslava
     */
    public ResultadoVO obtenerInformacionDetalladaEdicionGeografica(Long tramiteId,
        UsuarioDTO usuario);

    /**
     * Método para recibir radicado.
     *
     * @author juanfelipe.garcia
     * @param pradicacion es el número de radicado a marcar como recibido
     * @return
     */
    public Object[] recibirRadicado(String pradicacion, String usuario);

    /**
     * Método para finalizar radicado.
     *
     * @author juanfelipe.garcia
     * @param pradicacion es el número de radicado a marcar como recibido
     * @param pradicacionresponde es el número de radicado con el que se da repuesta
     * @return
     */
    public Object[] finalizarRadicacion(String pradicacion, String pradicacionResponde,
        String usuario);

    /**
     * Metodo para calcular las areas y las zonas de los predios que hacen parte de un de PH o de
     * condominio, Solo es valido para trámites de PH o Condominio
     *
     * @author felipe.cadena
     *
     * @param tramite
     */
    public void calcularAreasUnidadesPrediales(Tramite tramite);

    /**
     * Método que obtiene un trámite por el numero de radicación
     *
     * @author juanfelipe.garcia
     * @param numeroRadicacion
     * @return
     */
    public Tramite buscarTramitePorNumeroRadicacion(String numeroRadicacion);

    /**
     * Metodo para guardar o actualizar un tramite estado
     *
     * @author felipe.cadena
     *
     * @param te
     * @return
     */
    public TramiteEstado guardarActualizarTramiteEstado(TramiteEstado te);

    /**
     * Busca los trámites que estén en el arreglo idsTramites Estos trámites son los que el
     * responsable de conservación ve en la pantalla "Establecer procedencia de la solicitud"
     *
     * @author javier.aponte
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se va a ordenar en la tabla de resultados
     * @param sortOrder nombre del ordenamiento (UNSORTED, ASCENDING, DESCENDING)
     * @param filters filtros de búsqueda de la consulta
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     *
     * @return
     */
    public List<Tramite> buscarTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites,
        String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Cuenta los trámites cuyos id estén en el arreglo idsTramites
     *
     * Estos trámites son los que el jefe de conservación ve en la pantalla "Establecer procedencia
     * de la solicitud"
     *
     * @author javier.aponte
     * @param idsTramites arreglo con los id de trámite
     */
    public int contarTramitesParaDeterminarProcedenciaSolicitud(long[] idsTramites);

    /**
     * Busca documentos a los que no se le aplicaron cambios y se genero la resolucion pero no
     * genero ruta en el ftp
     *
     * @author leidy.gonzalez
     */
    public List<Documento> buscarDocmentoFTP();

    /**
     * Método encargado de consultar los ids de trámites con su respectiva clasificación
     *
     * @param idsTramites
     * @return Map con clave el id del trámite y el valor la clasificación del trámite
     * correspondiente
     * @author javier.aponte
     */
    public List<TramiteConEjecutorAsignadoDTO> findIdsClasificacionTramitesConEjecutor(
        long[] idsTramites);

    /**
     * Metodo para consultar los tramites para la recuperacion.
     *
     * @author felipe.cadena
     *
     * @param filtroTramite
     * @param filtroSolicitante
     * @return
     */
    public List<Tramite> buscarTramitesParaRecuperacion(FiltroDatosConsultaTramite filtroTramite,
        FiltroDatosConsultaSolicitante filtroSolicitante);

    /**
     * Metodo para consultar los tramites para la asignacion de actividades Se retorna un arreglo de
     * 2 posiciones. La primera contiene un String con un mensaje de error, es null si no se
     * presento ningun error. La segunda posicion contiene la lista de tramites de la consulta, es
     * null si se presentaron errores en la consulta.
     *
     *
     * @author felipe.cadena
     *
     * @param filtroTramite
     * @param filtroSolicitante
     * @return Object[]
     */
    public Object[] buscarTramitesParaAdminAsignaciones(FiltroDatosConsultaTramite filtroTramite,
        FiltroDatosConsultaSolicitante filtroSolicitante);

    /**
     * Metodo para consultar las actividades relacionadas a los criterios de busqueda, ademas llevan
     * asociados los tramites de cada actividad retorna un arreglo de 2 posiciones. La primera
     * contiene un String con un mensaje de error, es null si no se presento ningun error. La
     * segunda posicion contiene la lista de Tramites de la consulta, es null si se presentaron
     * errores en la consulta.
     *
     * @author felipe.cadena
     *
     * @param codigoOficina
     * @param usuarios
     * @return
     */
    public Object[] buscarActividadesParaAdminAsignaciones(String codigoOficina,
        List<String> usuarios);

    /**
     *
     * @author felipe.cadena
     *
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @param consicionPropiedad
     * @return
     */
    public List<DeterminaTramitePH> obtenerDeterminaTramitePHPorParametrosTramite(String tipoTramite,
        String claseMutacion, String subtipo, String consicionPropiedad);

    /**
     * Método que busca en comision trámite por el id del trámite y devuelve una lista de tipo
     * comisión trámite con información de las comisiones asociadas al trámite que no han sido
     * realizadas
     *
     * @param idTramite
     * @return
     * @author javier.aponte
     */
    public List<ComisionTramite> buscarComisionesTramiteNoRealizadasPorIdTramite(Long idTramite);

    /**
     * Método para obtener solitantes propietarios de los predios, inicialmente si no existen los
     * crea y los retorna, de lo contrario retorna los ya existentes. Solo aplica para tramites de
     * rectificacion y tercera masivos.
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public List<Solicitante> obtenerSolicitantesParaNotificacion(Long tramiteId);

    /**
     * Método que permite buscar un trámite con resolución simple
     *
     * @author javier.aponte
     * @param id
     * @return
     */
    public Tramite buscarTramiteConResolucionSimple(Long tramiteId);

    /**
     * Método para obtener los tramites de actualizacion, agrupandolos por el id del predio
     *
     * @author leidy.gonzalez
     * @param tramitesIds
     * @return
     */
    public List<Tramite> buscarTramitesActualizacionAgrupadoPorPredio(
        long[] tramitesIds);

    /**
     * Retorna los tramites de actualizacion de una manza en particular
     *
     * @author felipe.cadena
     *
     * @param numeroManzana
     * @return
     */
    public List<Tramite> buscarTramitesActualizacionPorManzana(String numeroManzana);

    /**
     * Retorna los tramites de conservacion de una lista de tramites
     *
     * @author leidy.gonzalez
     *
     * @param tramitesIds
     * @return
     */
    public List<Tramite> buscarTramitesConservacionPorIdTramites(List<Long> tramiteIds);

    /**
     * Metodo para invocar el procedimiento de radicacion de actualizacion
     *
     * @author felipe.cadena
     *
     * @param codigoMunicipio
     * @param idSolicitud
     * @param usuario
     * @return
     */
    public Object[] radicarActualizacion(Long idMunicipioActualizacion, Long idSolicitud,
        String usuario);

    /**
     * Ejecuta el procedimiento para validar si el proceso de actualizacion se puede radicar
     *
     * @author felipe.cadena
     *
     * @param idMunicipioActualizacion
     * @return
     */
    public int validarRadicacionActualizacion(Long idMunicipioActualizacion);

    /**
     * Metodo para invocar el procedimiento de validacion de actualizacion
     *
     * @author felipe.cadena
     *
     * @param idMunicipioActualizacion
     * @param usuario
     * @return
     */
    public Object[] validarActualizacion(Long idMunicipioActualizacion, String usuario);

    /**
     * Elimina un procso de actualizacion dado
     *
     * @author felipe.cadena
     *
     * @param idMunicipioActualizacion
     * @param usuario
     * @return
     */
    public Object[] eliminarActualizacion(Long idMunicipioActualizacion, String usuario);

    /**
     * Método que permite obtener la url del reporte en el servidor de jasper de los reportes de las
     * resoluciones para actualización
     *
     * @author javier.aponte
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @param radicacionEspecial
     * @param isRectificacionCancelacionDobleInscripcion
     * @return EReporteServiceSNC
     */
    public EReporteServiceSNC obtenerUrlReporteResolucionesActualizacion(String tipoTramite,
        String claseMutacion, String subtipo,
        String radicacionEspecial, boolean isRectificacionCancelacionDobleInscripcion);

    /**
     * Guarda o actualiza un objeto tipo TramiteReasignacion y retorna el objeto actualizado.
     *
     * @author felipe.cadena
     *
     * @param reasignacion
     * @return
     */
    public TramiteReasignacion guardarActualizarTramiteReasignacion(TramiteReasignacion reasignacion);

    /**
     * Guarda o actualiza una lista de objetos tipo TramiteReasignacion y retorna el objeto
     * actualizado.
     *
     * @author felipe.cadena
     *
     * @param reasignaciones
     */
    public void guardarActualizarTramiteReasignacionMultiple(
        List<TramiteReasignacion> reasignaciones);

    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite creando
     * una nueva transacción para la consulta
     *
     * @author david.cifuentes
     * @param tramiteId
     * @return
     */
    public Tramite findTramitePruebasByTramiteIdNuevaTransaccion(Long tramiteId);

    /**
     * Método que permite buscar la documentacion generada por el sistema de un trámite
     *
     * @author leidy.gonzalez
     * @param id
     * @return
     */
    public List<Tramite> consultarDocumentacionDeTramiteGenerada(Long tramiteId);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial y final
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial, final y
     * fechas
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucion(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de rango inicial que
     * aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorNumeroResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     * en que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicialAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha inicial, final
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionFinalEInicial(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucion(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Metodo para consultar las resoluciones relacionadas a los criterios de fecha resolucion en
     * que aplicaron cambios
     *
     * @author leidy.gonzalez
     *
     * @param productoCatastralDetalle
     * @return
     */
    public List<Tramite> consultarResolucionesPorFechaResolucionAplicaCambios(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Método que permite reversar la proyección de los predios que están asociados a un trámite
     *
     * @author javier.aponte
     * @param tramite
     * @return
     */
    public boolean reversarProyeccion(Tramite tramite);

    /**
     * Método que permite cancelar un trámite en el process
     *
     * @author javier.aponte
     * @param tramite
     * @param motivo
     * @return
     */
    public boolean cancelarTramiteEnProcess(Tramite tramite, String motivo);

    /**
     * Método que elimina los documentos que se relacionaron al TramiteDocumento, cuando la acción
     * no es confirmada por el usuario.
     *
     * @author dumar.penuela
     * @param documentos
     * @param tramitesDocumentacion
     * @param solicitudesDocumentacion
     */
    public void eliminarDeTramiteDocumentoDocumentosAsociado(
        List<Documento> documentos,
        List<TramiteDocumentacion> tramitesDocumentacion,
        List<SolicitudDocumentacion> solicitudesDocumentacion);

    /**
     * Método que realiza la búsqueda de solicitantes regresando una lista de tipo
     * {@link Solicitante} en caso de que haya más de uno
     *
     * @param tipoDocumento
     * @param numeroDocumento
     * @return
     */
    public List<Solicitante> buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacionsList(
        String tipoDocumento, String numeroDocumento);

    /**
     * Método que obtiene una lista de tramites rectificaciones por el id del trámite
     *
     * @param tramiteId
     * @return lista de tramites rectificaciones
     * @author javier.aponte
     */
    public List<TramiteRectificacion> findTramiteRectificacionesByTramiteId(Long tramiteId);

    /**
     * Genera la proyeccion de los tramites originales asociados a la cancelacion masiva
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param usuario
     * @return
     */
    public Object[] generarProyeccionCancelacionBasica(Long tramiteId, String usuario);

    /**
     * Busca los TramitePredioEnglobe que correspondan al predio con el id dado
     *
     * @author leidy.gonzalez
     * @param predioId
     * @return
     */
    public List<TramiteDetallePredio> buscarPredioEnglobesPorPredio(Long predioId);

    /**
     * Elimina registro dado de la base de datos
     *
     * @author felipe.cadena
     * @param registro
     */
    public void eliminarTramitePredioEnglobe(TramiteDetallePredio registro);

    /**
     * Método que busca un predio
     *
     * @author dumar.penuela
     * @param numPredial
     */
    public Predio buscaPredio(String numPredial);

    /**
     * Finaliza un radicado interno (IE)
     *
     * @param pradicacion número de radicación a cerrar
     * @param pradicacionResponde número de radicación que responde al radicado a cerrar
     * (generalmente EE)
     * @param usuario usuario que cierra el radicado
     * @return objeto que contiene el mensaje de base de datos del procedimiento ejecutado
     */
    Object[] finalizarRadicacionInterno(String pradicacion, String pradicacionResponde,
        String usuario);

    /**
     * Obtiene una lista de documentos de tipo comisión de un trámite determinado
     *
     * @param idTramite id del trámite que tiene los documentos
     * @return lista de documentos asociados al trámite
     */
    List<Documento> obtenerDocumentosComisionPorTramiteId(final Long idTramite);

    /**
     * Metodo para actualizar el estado de un tramite insertando registro en TramiteEstado y
     * restringiendo cambios de estado no validos
     *
     * @author felipe.cadena
     * @param tramite
     * @return
     */
    public TramiteEstado actualizarEstadoTramite(Tramite tramite, ETramiteEstado nuevoEstado,
        UsuarioDTO usuario);

    public List<UsuarioDTO> buscarTodosFuncionarios();

    public List<UsuarioDTO> buscarFuncionariosPorRolYEstructuraOrganizacional(
        String nombreEstructuraOrganizacional, ERol rol);

    /**
     * Metodo para consultar la configuracion del tipo de informe para visualizar en la consulta
     * avanzada
     *
     * @author leidy.gonzalez
     *
     * @param tipoInforme
     * @return
     */
    public RepConsultaPredial getConfiguracionReportePredial(Long tipoInforme);

    /**
     * Método que realiza la consulta de tareas geograficas de forma paginada.
     *
     * @author david.cifuentes
     * @param usuario
     * @return
     */
    public ResultadoVO obtenerTareasGeograficasPaginado(UsuarioDTO usuario);

    /**
     * Eliminar tramiteDetallePredio masivo
     *
     * @author felipe.cadena
     *
     *
     * @param tdps
     */
    public void eliminarTramiteDetallePredioMultiple(
        List<TramiteDetallePredio> tdps);

    public void asociarInfoCampoAdicional(EInfoAdicionalCampo campo, String valor);

    /**
     * Elimina el registro PFichaMatrizPredioTerreno permanentemente
     *
     *
     * @author felipe.cadena
     * @param registro
     */
    public void eliminarPFichaMatrizPT(PFichaMatrizPredioTerreno registro);

    /**
     *
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param usuario
     * @return
     */
    public Object[] aceptarEnglobeVirtual(Long tramiteId, String usuario);

    /**
     * Se genera el numero predial para las fichas nuevas en las modificaciones de PH
     *
     * @param condicionPropiedad
     * @param usuario
     * @return
     */
    public Object[] generarNumeroEnglobeVirtual(Long tramiteId, String condicionPropiedad,
        String usuario);

    /**
     * MÃ©todo que obtiene un tramiteDocumento asociados a un idTramite, docPersonaNotificacion y
     * nombrePersonaNotif
     *
     * @author leidy.gonzalez
     * @param tramiteId
     * @param docPersonaNotificacion
     * @param nombrePersonaNotif
     * @return
     */
    public TramiteDocumento obtenerDocNotificacion(
        Long idTramite, String docPersonaNotificacion, String nombrePersonaNotif);

    /**
     * Obtiene (deberÃ­a existir) lista de TramiteDocumentos que se crea en el momento de comunicar
     * la notificaciÃ³n de resoluciÃ³n y que se va a modificar cuando se registra la notificaciÃ³n
     *
     * PRE: el documento debe existir. Si no, hay un error en el proceso
     *
     * @author leidy.gonzalez
     *
     * @param idTramite id del trÃ¡mite
     * @param Lista de docsPersonaNotificacion documento de la persona a notificar
     * @return
     */
    public List<TramiteDocumento> obtenerTramiteDocumentosParaRegistrosNotificacion(
        Long idTramite, List<String> docsPersonaNotificacion);

    /**
     * MÃ©todo que guarda y actualiza una Lista de tramiteDocumentos
     *
     * @author leidy.gonzalez
     * @param tds
     * @return
     */
    public void guardarYActualizarTramiteDocumentoMultiple(
        List<TramiteDocumento> tds);

    /**
     * MÃ©todo que obtiene una lista de tramiteDocumentos de notificaion y autorizacion asociados a
     * un idTramite,docPersonaNotificacion y nombrePersonaNotif
     *
     * @author leidy.gonzalez
     * @param tramiteId
     * @param docPersonaNotificacion
     * @param nombrePersonaNotif
     * @return
     */
    public List<TramiteDocumento> obtenerDocsComunicacionYAutorizacion(
        Long idTramite, String docPersonaNotificacion, String nombrePersonaNotif);

    /**
     * Elimina lista de atributos asociados al tramite
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param tias
     * @return
     */
    public void eliminarTramiteInfoMultiple(List<TramiteInfoAdicional> tias);

    /**
     * Metodo que retorna todas las fichas matrices asociadas a un tramite.
     *
     * @author leidy.gonzalez
     *
     *
     * @param tramiteId
     * @return
     */
    public List<PFichaMatriz> obtenerFichasAsociadasATramite(Long tramiteId);

    /**
     * Método para obtener solitantes propietarios de los predios condicion 0 y 5, inicialmente si
     * no existen los crea y los retorna, de lo contrario retorna los ya existentes. Solo aplica
     * para tramites de rectificacion y tercera masivos.
     *
     * @author leidy.gonzalez
     * @param tramiteId
     * @return
     */
    public List<Solicitante> obtenerSolicitantesParaNotificacionEV(Long tramiteId);

    /**
     * Retorna lista con los tipos de tramites disponibles en la BD
     *
     * @return
     *
     * @author felipe.cadena
     */
    public List<String[]> obtenerTipoTramiteCompuestoCodigo();

    //end of interface
}
