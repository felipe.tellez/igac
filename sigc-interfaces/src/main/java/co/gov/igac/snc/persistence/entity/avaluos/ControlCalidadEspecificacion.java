package co.gov.igac.snc.persistence.entity.avaluos;

import co.gov.igac.snc.persistence.util.ESiNo;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the CONTROL_CALIDAD_ESPECIFICACION database table.
 *
 */
@Entity
@Table(name = "CONTROL_CALIDAD_ESPECIFICACION")
public class ControlCalidadEspecificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private ControlCalidadAvaluoRev avaluoControlCalidadRev;
    private String especificacion;
    private String cumple;
    private String descripcion;
    private Date fechaLog;
    private String usuarioLog;

    public ControlCalidadEspecificacion() {
    }

    @Id
    @SequenceGenerator(name = "CONTROL_CALIDAD_ESPECIFICACION_ID_GENERATOR", sequenceName =
        "CONTROL_CALIDAD_ESPECIF_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTROL_CALIDAD_ESPECIFICACION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //bi-directional many-to-one association to ControlCalidadAvaluoRev
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_CONTROL_CALIDAD_ID")
    public ControlCalidadAvaluoRev getAvaluoControlCalidadRev() {
        return this.avaluoControlCalidadRev;
    }

    public void setAvaluoControlCalidadRev(ControlCalidadAvaluoRev avaluoControlCalidadRev) {
        this.avaluoControlCalidadRev = avaluoControlCalidadRev;
    }

    public String getEspecificacion() {
        return this.especificacion;
    }

    public void setEspecificacion(String especificacion) {
        this.especificacion = especificacion;
    }

    public String getCumple() {
        return this.cumple;
    }

    public void setCumple(String cumple) {
        this.cumple = cumple;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Transient
    public Boolean getCumpleBoolean() {

        if (this.cumple != null && !"".equals(this.cumple)) {
            if (this.cumple.equals(ESiNo.SI.getCodigo())) {
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }

    public void setCumpleBoolean(Boolean cumple) {

        if (cumple) {
            this.cumple = ESiNo.SI.getCodigo();
        } else {
            this.cumple = ESiNo.NO.getCodigo();
        }
    }

}
