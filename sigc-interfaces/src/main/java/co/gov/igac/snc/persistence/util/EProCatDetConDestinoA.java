package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd PRO_CAT_DET_CON_DESTINO_A
 *
 * @author javier.aponte
 */
public enum EProCatDetConDestinoA {

    LIBRETA_MILITAR("1", "Libreta militar"),
    SUBSIDIO_DE_VIVIENDA("2", "Subsidio de vivienda"),
    CENTRO_EDUCATIVO_OFICIAL("3", "Centro educativo oficial"),
    JUZGADO("4", "Juzgado"),
    INCODER("5", "Incoder"),
    PENITENCIARIA("6", "Penitenciaria"),
    PROCURADURIA("7", "Procuraduría"),
    CONTRALORIA("8", "Contraloría"),
    TRAMITE_NOTARIAL_Y_REGISTRAL("9", "Trámite notarial y registral"),
    OTRO("10", "Otro");

    private String codigo;
    private String nombre;

    private EProCatDetConDestinoA(String codigo, String nombre) {

        this.codigo = codigo;
        this.nombre = nombre;

    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

}
