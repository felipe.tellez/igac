package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONTRATO_PAGO_AVALUO database table.
 *
 */
@Entity
@Table(name = "CONTRATO_PAGO_AVALUO")
public class ContratoPagoAvaluo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String usuarioLog;
    private Avaluo avaluo;
    private ProfesionalContratoPago profesionalContratoPago;

    public ContratoPagoAvaluo() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTRATO_PAGO_AVALUO_ID_GENERATOR")
    @SequenceGenerator(name = "CONTRATO_PAGO_AVALUO_ID_GENERATOR", sequenceName =
        "CONTRATO_PAGO_AVALUO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Avaluo
    @ManyToOne
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    //bi-directional many-to-one association to ProfesionalContratoPago
    @ManyToOne
    @JoinColumn(name = "PROFESIONAL_CONTRATO_PAGO_ID")
    public ProfesionalContratoPago getProfesionalContratoPago() {
        return this.profesionalContratoPago;
    }

    public void setProfesionalContratoPago(ProfesionalContratoPago profesionalContratoPago) {
        this.profesionalContratoPago = profesionalContratoPago;
    }

}
