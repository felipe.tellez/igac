/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Clase con los datos que se necesitan manejar de un funcionario recolector de ofertas
 *
 * @author pedro.garcia
 * @modified rodrigo.hernandez - Adición de atributo seleccionado
 */
public class FuncionarioRecolector implements Serializable {

    private int id;
    /**
     * Bandera que indica si un recolector ha sido seleccionado para ser asignado a un area
     */
    private boolean seleccionado;
    private String login;
    private String nombreFuncionario;
    private String correoElectronicoFuncionario;
    private boolean recolectandoOfertas;
    private String tieneAsignaciones;

    //-------------------------     methods   -------------
    public String getNombreFuncionario() {
        return this.nombreFuncionario;
    }

    public void setNombreFuncionario(String nombreFuncionario) {
        this.nombreFuncionario = nombreFuncionario;
    }

    public String getCorreoElectronicoFuncionario() {
        return this.correoElectronicoFuncionario;
    }

    public void setCorreoElectronicoFuncionario(String correoElectronicoFuncionario) {
        this.correoElectronicoFuncionario = correoElectronicoFuncionario;
    }

    public boolean isRecolectandoOfertas() {
        return this.recolectandoOfertas;
    }

    public void setRecolectandoOfertas(boolean recolectandoOfertas) {
        this.recolectandoOfertas = recolectandoOfertas;
    }

    public String getTieneAsignaciones() {
        return this.tieneAsignaciones;
    }

    public void setTieneAsignaciones(String tieneAsignaciones) {
        this.tieneAsignaciones = tieneAsignaciones;
    }

    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
