package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad Plantilla.
 *
 * @author MyEclipse Persistence Tools
 */
@NamedQueries(
    @NamedQuery(name = "recuperarPlantillaPorCodigo", query =
        "SELECT p FROM Plantilla p WHERE p.codigo = :codigo")
)
@Entity
@Table(name = "PLANTILLA", schema = "SNC_GENERALES")
public class Plantilla implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    // Fields
    @Id
    @SequenceGenerator(name = "PLANTILLA_ID_GENERATOR", sequenceName = "PLANTILLA_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLANTILLA_ID_GENERATOR")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TIPO_DOCUMENTO_ID")
    private TipoDocumento tipoDocumento;

    private String codigo;

    private String titulo;

    @Lob()
    private String html;

    @Column(name = "JASPER_URL")
    private String jasperUrl;

    private String descripcion;

    private String activa;

    @Column(name = "USUARIO_LOG")
    private String usuarioLog;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public Plantilla() {
    }

    /** minimal constructor */
    public Plantilla(TipoDocumento tipoDocumento, String usuarioLog, Date fechaLog) {
        this.tipoDocumento = tipoDocumento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Plantilla(Long id, TipoDocumento tipoDocumento, String codigo,
        String titulo, String html, String jasperUrl, String descripcion,
        String activa, String usuarioLog, Date fechaLog) {
        super();
        this.id = id;
        this.tipoDocumento = tipoDocumento;
        this.codigo = codigo;
        this.titulo = titulo;
        this.html = html;
        this.jasperUrl = jasperUrl;
        this.descripcion = descripcion;
        this.activa = activa;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getJasperUrl() {
        return jasperUrl;
    }

    public void setJasperUrl(String jasperUrl) {
        this.jasperUrl = jasperUrl;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActiva() {
        return activa;
    }

    public void setActiva(String activa) {
        this.activa = activa;
    }

    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }
}
