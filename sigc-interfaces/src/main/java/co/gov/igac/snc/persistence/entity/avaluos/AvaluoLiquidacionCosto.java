package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the AVALUO_LIQUIDACION_COSTO database table.
 *
 * @modified rodrigo.hernandez - 27/09/2012 - Se ajustaron los tipos de datos para valores numéricos
 *
 * @modified rodrigo.hernandez - 05/10/2012 - Se hizo ajuste a entity segun cambio de columnas en la
 * tabla
 *
 * @modified christian.rodriguez - 11/10/2012 - Se agrega relación con el entity avalúos
 */
@Entity
@Table(name = "AVALUO_LIQUIDACION_COSTO")
public class AvaluoLiquidacionCosto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3416405463405963055L;

    private Long id;
    private Date fechaLiquidacion;
    private Date fechaLog;
    private Double honorariosIgac;
    private Double liquidacionBase;
    private String tipoLiquidacion;
    private Double totalHonorariosIgac;
    private String usuarioLog;
    private Double valorAvaluo;
    private Double valorTipoLiquidacion;

    private Avaluo avaluo;

    public AvaluoLiquidacionCosto() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_LIQUIDACION_COSTO_ID_GENERATOR", sequenceName =
        "AVALUO_LIQUIDACION_COST_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_LIQUIDACION_COSTO_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LIQUIDACION")
    public Date getFechaLiquidacion() {
        return this.fechaLiquidacion;
    }

    public void setFechaLiquidacion(Date fechaLiquidacion) {
        this.fechaLiquidacion = fechaLiquidacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "HONORARIOS_IGAC")
    public Double getHonorariosIgac() {
        return this.honorariosIgac;
    }

    public void setHonorariosIgac(Double honorariosIgac) {
        this.honorariosIgac = honorariosIgac;
    }

    @Column(name = "LIQUIDACION_BASE")
    public Double getLiquidacionBase() {
        return this.liquidacionBase;
    }

    public void setLiquidacionBase(Double liquidacionBase) {
        this.liquidacionBase = liquidacionBase;
    }

    @Column(name = "TIPO_LIQUIDACION")
    public String getTipoLiquidacion() {
        return this.tipoLiquidacion;
    }

    public void setTipoLiquidacion(String tipoLiquidacion) {
        this.tipoLiquidacion = tipoLiquidacion;
    }

    @Column(name = "TOTAL_HONORARIOS_IGAC")
    public Double getTotalHonorariosIgac() {
        return this.totalHonorariosIgac;
    }

    public void setTotalHonorariosIgac(Double totalHonorariosIgac) {
        this.totalHonorariosIgac = totalHonorariosIgac;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_AVALUO")
    public Double getValorAvaluo() {
        return this.valorAvaluo;
    }

    public void setValorAvaluo(Double valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    @Column(name = "VALOR_TIPO_LIQUIDACION")
    public Double getValorTipoLiquidacion() {
        return this.valorTipoLiquidacion;
    }

    public void setValorTipoLiquidacion(Double valorTipoLiquidacion) {
        this.valorTipoLiquidacion = valorTipoLiquidacion;
    }

    // bi-directional many-to-one association to Avaluo
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", unique = true, nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

}
