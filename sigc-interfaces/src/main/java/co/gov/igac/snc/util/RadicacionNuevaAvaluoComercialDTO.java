package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;

/**
 * Clase para almacenar información de una radicación nueva de correspondencia.</br>
 * <b><i>(Usada en Avalúos Comerciales)</i></b>
 *
 */
/*
 * @author rodrigo.hernandez @modified rodrigo.hernandez - adicion de metodo transient para obtener
 * tipo de persona
 */
public class RadicacionNuevaAvaluoComercialDTO implements Serializable {

    /**
     * ID Generado
     */
    private static final long serialVersionUID = 5667839067877500784L;

    private String numeroRadicacion;
    private Date fechaRadicacion;
    private String tipoDocumentoSolicitante;
    private String numeroDocumentoSolicitante;
    private String nombreSolicitante;
    private String tipoTramite;
    private String nombreTramite;
    private String codigoTerritorial;
    private String nombreTerritorial;
    private String codigoDepartamento;
    private String nombreDepartamento;
    private String codigoMunicipio;
    private String nombreMunicipio;

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public Date getFechaRadicacion() {
        return fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public String getTipoDocumentoSolicitante() {
        return tipoDocumentoSolicitante;
    }

    public void setTipoDocumentoSolicitante(String tipoDocumentoSolicitante) {
        this.tipoDocumentoSolicitante = tipoDocumentoSolicitante;
    }

    public String getNumeroDocumentoSolicitante() {
        return numeroDocumentoSolicitante;
    }

    public void setNumeroDocumentoSolicitante(String numeroDocumentoSolicitante) {
        this.numeroDocumentoSolicitante = numeroDocumentoSolicitante;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getNombreTramite() {
        return nombreTramite;
    }

    public void setNombreTramite(String nombreTramite) {
        this.nombreTramite = nombreTramite;
    }

    public String getCodigoTerritorial() {
        return codigoTerritorial;
    }

    public void setCodigoTerritorial(String codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public String getNombreTerritorial() {
        return nombreTerritorial;
    }

    public void setNombreTerritorial(String nombreTerritorial) {
        this.nombreTerritorial = nombreTerritorial;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    @Transient
    public String getTipoPersona() {
        String result = "";

        if (this.tipoDocumentoSolicitante.equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.
            getCodigo())) {
            result = EPersonaTipoPersona.NATURAL.toString();
        } else if (this.tipoDocumentoSolicitante.equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
            result = EPersonaTipoPersona.JURIDICA.toString();
        }

        return result;

    }
}
