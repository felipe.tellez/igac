package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the NOMENCLATURA_VIAL_CONDICION database table.
 */
/**
 * Modificaciones a la clase NomenclaturaVialCondicion:
 */
@Entity
@Table(name = "NOMENCLATURA_VIAL_CONDICION", schema = "SNC_ACTUALIZACION")
public class NomenclaturaVialCondicion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date fechaLog;
    private String texto;
    private String usuarioLog;
    private ActualizacionNomenclaturaVia actualizacionNomenclaturaVia;

    public NomenclaturaVialCondicion() {
    }

    @Id
    @SequenceGenerator(name = "NOMENCLATURA_VIAL_CONDI_ID_GENERATOR", sequenceName =
        "NOMENCLATURA_VIAL_CONDI_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "NOMENCLATURA_VIAL_CONDI_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, length = 2000)
    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionNomenclaturaVia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_NOME_VIAL_ID", nullable = false)
    public ActualizacionNomenclaturaVia getActualizacionNomenclaturaVia() {
        return this.actualizacionNomenclaturaVia;
    }

    public void setActualizacionNomenclaturaVia(
        ActualizacionNomenclaturaVia actualizacionNomenclaturaVia) {
        this.actualizacionNomenclaturaVia = actualizacionNomenclaturaVia;
    }

}
