package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PlantillaSeccionReporte entity.
 */
@Entity
@Table(name = "PLANTILLA_SECCION_REPORTE", schema = "SNC_GENERALES")
public class PlantillaSeccionReporte implements java.io.Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = 8126739421844862949L;

    // Fields
    private Long id;
    private String estructuraOrganizacionalCod;
    private String urlCabecera;
    private String urlMarcaDeAgua;
    private String urlPieDePagina;
    private String rutaCabecera;
    private String rutaMarcaDeAgua;
    private String rutaPieDePagina;
    private Date fechaEnFirme;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public PlantillaSeccionReporte() {
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PLANTILLA_SECCION_REPORTE_ID_SEQ_GEN")
    @SequenceGenerator(name = "PLANTILLA_SECCION_REPORTE_ID_SEQ_GEN", sequenceName =
        "PLANTILLA_SECCION_REP_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "URL_CABECERA", nullable = false, length = 1000)
    public String getUrlCabecera() {
        return this.urlCabecera;
    }

    public void setUrlCabecera(String urlCabecera) {
        this.urlCabecera = urlCabecera;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD", nullable = true, length = 20)
    public String getEstructuraOrganizacionalCod() {
        return estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(
        String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Column(name = "URL_MARCA_DE_AGUA", nullable = true, length = 1000)
    public String getUrlMarcaDeAgua() {
        return this.urlMarcaDeAgua;
    }

    public void setUrlMarcaDeAgua(String urlMarcaDeAgua) {
        this.urlMarcaDeAgua = urlMarcaDeAgua;
    }

    @Column(name = "URL_PIE_DE_PAGINA", nullable = true, length = 1000)
    public String getUrlPieDePagina() {
        return this.urlPieDePagina;
    }

    public void setUrlPieDePagina(String urlPieDePagina) {
        this.urlPieDePagina = urlPieDePagina;
    }

    @Column(name = "RUTA_CABECERA", nullable = true, length = 1000)
    public String getRutaCabecera() {
        return this.rutaCabecera;
    }

    public void setRutaCabecera(String rutaCabecera) {
        this.rutaCabecera = rutaCabecera;
    }

    @Column(name = "RUTA_MARCA_DE_AGUA", nullable = true, length = 1000)
    public String getRutaMarcaDeAgua() {
        return this.rutaMarcaDeAgua;
    }

    public void setRutaMarcaDeAgua(String rutaMarcaDeAgua) {
        this.rutaMarcaDeAgua = rutaMarcaDeAgua;
    }

    @Column(name = "RUTA_PIE_DE_PAGINA", nullable = true, length = 1000)
    public String getRutaPieDePagina() {
        return this.rutaPieDePagina;
    }

    public void setRutaPieDePagina(String rutaPieDePagina) {
        this.rutaPieDePagina = rutaPieDePagina;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EN_FIRME", nullable = true, length = 7)
    public Date getFechaEnFirme() {
        return this.fechaEnFirme;
    }

    public void setFechaEnFirme(Date fechaEnFirme) {
        this.fechaEnFirme = fechaEnFirme;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

//end of class    
}
