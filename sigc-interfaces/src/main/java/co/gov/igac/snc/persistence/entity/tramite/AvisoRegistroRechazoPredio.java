package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AvisoRegistroRechazoPredio entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia - adición de anotación @Temporal a los campos Date (experimental) -
 * cambio de tipo de datos TimeStamp a Date - adición de anotaciones para la secuencia del id -
 * cambio de predioId a Predio - método full constructor
 *
 */
@Entity
@Table(name = "AVISO_REGISTRO_RECHAZO_PREDIO", schema = "SNC_TRAMITE")
public class AvisoRegistroRechazoPredio implements java.io.Serializable {

    // Fields
    private Long id;
    private AvisoRegistroRechazo avisoRegistroRechazo;
    private String numeroPredial;
    private String numeroPredialAnterior;
    private Predio predio;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public AvisoRegistroRechazoPredio() {
    }

    /** full constructor */
    public AvisoRegistroRechazoPredio(Long id,
        AvisoRegistroRechazo avisoRegistroRechazo, String numeroPredial,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.avisoRegistroRechazo = avisoRegistroRechazo;
        this.numeroPredial = numeroPredial;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /**
     * Full constructor
     *
     * @author pedro.garcia
     * @param id
     * @param avisoRegistroRechazo
     * @param numeroPredial
     * @param numeroPredialAnterior
     * @param predio
     * @param usuarioLog
     * @param fechaLog
     */
    public AvisoRegistroRechazoPredio(Long id,
        AvisoRegistroRechazo avisoRegistroRechazo, String numeroPredial,
        String numeroPredialAnterior, Predio predio, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.avisoRegistroRechazo = avisoRegistroRechazo;
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.predio = predio;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AvisoRegistroRechazoPredio_ID_SEQ")
    @SequenceGenerator(name = "AvisoRegistroRechazoPredio_ID_SEQ",
        sequenceName = "AVISO_REGISTRO_RECHAZO__ID_SEQ", allocationSize = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVISO_REGISTRO_RECHAZO_ID", nullable = false)
    public AvisoRegistroRechazo getAvisoRegistroRechazo() {
        return this.avisoRegistroRechazo;
    }

    public void setAvisoRegistroRechazo(
        AvisoRegistroRechazo avisoRegistroRechazo) {
        this.avisoRegistroRechazo = avisoRegistroRechazo;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_ANTERIOR", length = 20)
    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(value = TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }
//--------------------------------------------------------------------------------------------------

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = true)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio p) {
        this.predio = p;
    }

}
