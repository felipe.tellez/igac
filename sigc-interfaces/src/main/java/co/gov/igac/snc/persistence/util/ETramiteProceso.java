/*
 *Proyecto SNC 2015
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * Representa los valores del campo PROCESO de la tabla tramite
 *
 * @author felipe.cadena
 *
 */
public enum ETramiteProceso {

    CONSERVACION("CONSERVCAION"),
    ACTUALIZACION("ACTUALIZACION");

    private String codigo;

    private ETramiteProceso(String codigoP) {
        this.codigo = codigoP;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
