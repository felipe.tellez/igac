package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the LISTADO_PREDIO database table.
 *
 */
@Entity
@Table(name = "LISTADO_PREDIO")
@NamedQuery(name = "ListadoPredio.findAll", query = "SELECT l FROM ListadoPredio l")
public class ListadoPredio implements Serializable {

    private static final long serialVersionUID = 1L;
    private ListadoPredioPK id;
    private Long areaConstruccionCierre;
    private Long areaConstruccionPrecierre;
    private Long areaTerrenoCierre;
    private Long areaTerrenoPrecierre;
    private String condicionPropiedad;
    private String destino;
    private String numeroPredial;
    private String tipo;
    private String tipoAvaluo;
    private Long valorAvaluoCierre;
    private Long valorAvaluoPrecierre;

    public ListadoPredio() {
    }

    @EmbeddedId
    public ListadoPredioPK getId() {
        return this.id;
    }

    public void setId(ListadoPredioPK id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUCCION_CIERRE")
    public Long getAreaConstruccionCierre() {
        return this.areaConstruccionCierre;
    }

    public void setAreaConstruccionCierre(Long areaConstruccionCierre) {
        this.areaConstruccionCierre = areaConstruccionCierre;
    }

    @Column(name = "AREA_CONSTRUCCION_PRECIERRE")
    public Long getAreaConstruccionPrecierre() {
        return this.areaConstruccionPrecierre;
    }

    public void setAreaConstruccionPrecierre(Long areaConstruccionPrecierre) {
        this.areaConstruccionPrecierre = areaConstruccionPrecierre;
    }

    @Column(name = "AREA_TERRENO_CIERRE")
    public Long getAreaTerrenoCierre() {
        return this.areaTerrenoCierre;
    }

    public void setAreaTerrenoCierre(Long areaTerrenoCierre) {
        this.areaTerrenoCierre = areaTerrenoCierre;
    }

    @Column(name = "AREA_TERRENO_PRECIERRE")
    public Long getAreaTerrenoPrecierre() {
        return this.areaTerrenoPrecierre;
    }

    public void setAreaTerrenoPrecierre(Long areaTerrenoPrecierre) {
        this.areaTerrenoPrecierre = areaTerrenoPrecierre;
    }

    @Column(name = "CONDICION_PROPIEDAD")
    public String getCondicionPropiedad() {
        return this.condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "TIPO_AVALUO")
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @Column(name = "VALOR_AVALUO_CIERRE")
    public Long getValorAvaluoCierre() {
        return this.valorAvaluoCierre;
    }

    public void setValorAvaluoCierre(Long valorAvaluoCierre) {
        this.valorAvaluoCierre = valorAvaluoCierre;
    }

    @Column(name = "VALOR_AVALUO_PRECIERRE")
    public Long getValorAvaluoPrecierre() {
        return this.valorAvaluoPrecierre;
    }

    public void setValorAvaluoPrecierre(Long valorAvaluoPrecierre) {
        this.valorAvaluoPrecierre = valorAvaluoPrecierre;
    }

    @Transient
    public Long getDiferenciaAreaConstruccion() {
        if (this.areaConstruccionCierre != null && this.areaConstruccionPrecierre != null) {
            return areaConstruccionCierre.longValue() - this.areaConstruccionPrecierre.longValue();
        }
        return 0l;
    }

    @Transient
    public Long getDiferenciaAreaTerreno() {
        if (this.areaTerrenoCierre != null && this.areaTerrenoPrecierre != null) {
            return areaTerrenoCierre.longValue() - this.areaTerrenoPrecierre.longValue();
        }
        return 0l;
    }

    @Transient
    public Long getDiferenciaValorAvaluo() {
        if (this.valorAvaluoCierre != null && this.valorAvaluoPrecierre != null) {
            return valorAvaluoCierre.longValue() - this.valorAvaluoPrecierre.longValue();
        }
        return 0l;
    }

    @Transient
    public Double getIncrementoPorcentaje() {
        if (this.valorAvaluoCierre != null && this.valorAvaluoPrecierre != null &&
            this.valorAvaluoPrecierre > 0) {
            Double porcentajeIncremento = (this.valorAvaluoCierre.doubleValue() * 100) /
                this.valorAvaluoPrecierre;
            return porcentajeIncremento - 100;
        }
        return 0d;
    }

}
