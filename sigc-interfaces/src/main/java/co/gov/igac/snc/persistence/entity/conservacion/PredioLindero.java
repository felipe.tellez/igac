package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.*;

/**
 * PredioLindero entity
 *
 * @author leidy.gonzalez
 */
@Entity
@Table(name = "PREDIO_LINDERO", schema = "SNC_CONSERVACION")
public class PredioLindero implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7400131431268398546L;

    @GeneratedValue
    private Long id;
    private String numeroPredial;
    private String tipoLindero;
    private String nodoDe;
    private Double norteNodoDe;
    private Double esteNodoDe;
    private String nodoA;
    private Double norteNodoA;
    private Double esteNodoA;
    private Double distancia;
    private String colindante;
    private String piso;
    private Double predioId;
    private String usuarioLog;
    private Date fechaLog;

    /** default constructor */
    public PredioLindero() {
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "TIPO_LINDERO", length = 30)
    public String getTipoLindero() {
        return tipoLindero;
    }

    public void setTipoLindero(String tipoLindero) {
        this.tipoLindero = tipoLindero;
    }

    @Column(name = "NODO_DE", length = 2)
    public String getNodoDe() {
        return nodoDe;
    }

    public void setNodoDe(String nodoDe) {
        this.nodoDe = nodoDe;
    }

    @Column(name = "NORTE_NODO_DE", precision = 18, scale = 6)
    public Double getNorteNodoDe() {
        return norteNodoDe;
    }

    public void setNorteNodoDe(Double norteNodoDe) {
        this.norteNodoDe = norteNodoDe;
    }

    @Column(name = "ESTE_NODO_DE", precision = 18, scale = 6)
    public Double getEsteNodoDe() {
        return esteNodoDe;
    }

    public void setEsteNodoDe(Double esteNodoDe) {
        this.esteNodoDe = esteNodoDe;
    }

    @Column(name = "NODO_A", length = 2)
    public String getNodoA() {
        return nodoA;
    }

    public void setNodoA(String nodoA) {
        this.nodoA = nodoA;
    }

    @Column(name = "NORTE_NODO_A", precision = 18, scale = 6)
    public Double getNorteNodoA() {
        return norteNodoA;
    }

    public void setNorteNodoA(Double norteNodoA) {
        this.norteNodoA = norteNodoA;
    }

    @Column(name = "ESTE_NODO_A", precision = 18, scale = 6)
    public Double getEsteNodoA() {
        return esteNodoA;
    }

    public void setEsteNodoA(Double esteNodoA) {
        this.esteNodoA = esteNodoA;
    }

    @Column(name = "DISTANCIA", nullable = false, precision = 10, scale = 2)
    public Double getDistancia() {
        return distancia;
    }

    public void setDistancia(Double distancia) {
        this.distancia = distancia;
    }

    @Column(name = "COLINDANTE", length = 50)
    public String getColindante() {
        return colindante;
    }

    public void setColindante(String colindante) {
        this.colindante = colindante;
    }

    @Column(name = "PISO", length = 50)
    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    @Column(name = "PREDIO_ID", precision = 10)
    public Double getPredioId() {
        return predioId;
    }

    public void setPredioId(Double predioId) {
        this.predioId = predioId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
