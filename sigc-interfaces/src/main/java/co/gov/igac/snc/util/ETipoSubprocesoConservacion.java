package co.gov.igac.snc.util;

public enum ETipoSubprocesoConservacion {
    RADICACION("Radicación"),
    ASIGNACION("Asignación"),
    EJECUCION("Ejecución"),
    VALIDACION("Validación"),
    DIGITALIZACION("Digitalización");
    private String codigo;

    private ETipoSubprocesoConservacion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
