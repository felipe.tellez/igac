package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para la clase de los Tipos de Documentos
 *
 * @author david.cifuentes
 *
 */
public enum ETipoDocumentoClase {
    COMUNICACION,
    DOCUMENTO_SOPORTE_PRODUCTO,
    FOTOGRAFIA,
    NOTIFICACION,
    OFICIO,
    OTRO,
    SERVICIO,
    JUSTIFICATIVO_RECTIFICACION,
    RESOLUCION;
}
