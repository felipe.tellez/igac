package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion que indica el orden del ActualizacionDocumento en las pantallas de planificacion
 *
 * @author franz.gamba
 *
 */
public enum EOrdenActualizacionDocumento {
    PRIMERO("1"),
    SEGUNDO("2"),
    TERCERO("3"),
    CUARTO("4"),
    QUINTO("5");
    private String valor;

    private EOrdenActualizacionDocumento(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
};
