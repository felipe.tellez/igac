package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the V_PROPIEDAD_TIERRA database table.
 *
 */
@Entity
@Table(name = "V_PROPIEDAD_TIERRA")
public class VPropiedadTierra implements Serializable {

    private static final long serialVersionUID = 1L;

    private VPropiedadTierraPK id;
    private BigDecimal areaTerrenoHectareas;
    private String departamentoCodigo;
    private String municipioCodigo;
    private BigDecimal propietarios;
    private String tipoPropiedad;
    private String zonaUnidadOrganica;

    public VPropiedadTierra() {
    }

    @EmbeddedId
    public VPropiedadTierraPK getId() {
        return id;
    }

    public void setId(VPropiedadTierraPK id) {
        this.id = id;
    }

    @Column(name = "AREA_TERRENO_HECTAREAS", insertable = false, updatable = false)
    public BigDecimal getAreaTerrenoHectareas() {
        return this.areaTerrenoHectareas;
    }

    public void setAreaTerrenoHectareas(BigDecimal areaTerrenoHectareas) {
        this.areaTerrenoHectareas = areaTerrenoHectareas;
    }

    @Column(name = "DEPARTAMENTO_CODIGO", insertable = false, updatable = false)
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    @Column(name = "MUNICIPIO_CODIGO", insertable = false, updatable = false)
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public BigDecimal getPropietarios() {
        return this.propietarios;
    }

    public void setPropietarios(BigDecimal propietarios) {
        this.propietarios = propietarios;
    }

    @Column(name = "TIPO_PROPIEDAD", insertable = false, updatable = false)
    public String getTipoPropiedad() {
        return this.tipoPropiedad;
    }

    public void setTipoPropiedad(String tipoPropiedad) {
        this.tipoPropiedad = tipoPropiedad;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", insertable = false, updatable = false)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

}
