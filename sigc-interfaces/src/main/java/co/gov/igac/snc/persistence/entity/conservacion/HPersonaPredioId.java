package co.gov.igac.snc.persistence.entity.conservacion;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * HPersonaPredioId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class HPersonaPredioId implements java.io.Serializable {

    // Fields
    private Long HPredioId;
    private Long id;

    // Constructors
    /** default constructor */
    public HPersonaPredioId() {
    }

    /** full constructor
     *
     * @param HPredioId
     * @param id
     */
    public HPersonaPredioId(Long HPredioId, Long id) {
        this.HPredioId = HPredioId;
        this.id = id;
    }

    // Property accessors
    @Column(name = "H_PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getHPredioId() {
        return this.HPredioId;
    }

    public void setHPredioId(Long HPredioId) {
        this.HPredioId = HPredioId;
    }

    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof HPersonaPredioId)) {
            return false;
        }
        HPersonaPredioId castOther = (HPersonaPredioId) other;

        return ((this.getHPredioId() == castOther.getHPredioId()) || (this
            .getHPredioId() != null && castOther.getHPredioId() != null && this
            .getHPredioId().equals(castOther.getHPredioId()))) &&
            ((this.getId() == castOther.getId()) || (this.getId() != null &&
            castOther.getId() != null && this.getId().equals(
            castOther.getId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result +
            (getHPredioId() == null ? 0 : this.getHPredioId().hashCode());
        result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "org.ejemplo.jpa.HPersonaPredioId[ hPredioId=" + HPredioId + ", id=" + id + " ]";
    }

}
