package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * @author franz.gamba
 */
public class EstadisticasRegistroVO implements Serializable {

    private static final long serialVersionUID = -5977741543799531977L;
    private String tipoPropiedad;
    private Long propietarios;
    private Long hectareas;

    public EstadisticasRegistroVO(String tipoPropiedad, Long propietarios, Long hectareas) {
        this.tipoPropiedad = tipoPropiedad;
        this.propietarios = propietarios;
        this.hectareas = hectareas;
    }

    public String getTipoPropiedad() {
        return tipoPropiedad;
    }

    public void setTipoPropiedad(String tipoPropiedad) {
        this.tipoPropiedad = tipoPropiedad;
    }

    public Long getPropietarios() {
        return propietarios;
    }

    public void setPropietarios(Long propietarios) {
        this.propietarios = propietarios;
    }

    public Long getHectareas() {
        return hectareas;
    }

    public void setHectareas(Long hectareas) {
        this.hectareas = hectareas;
    }

}
