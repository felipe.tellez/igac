package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.generales.Documento;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the AVALUO_EVALUACION database table.
 *
 * @modified rodrigo.hernandez campo Documento agregado manualmente por cambio en la tabla -
 * 26/11/12
 */
@Entity
@Table(name = "AVALUO_EVALUACION")
public class AvaluoEvaluacion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long avaluoId;
    private Long asignacionId;
    private Date fecha;
    private Date fechaLog;
    private String observaciones;
    private String usuarioLog;
    private List<AvaluoEvaluacionDetalle> avaluoEvaluacionDetalles;
    private Documento documento;

    public AvaluoEvaluacion() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_EVALUACION_ID_GENERATOR", sequenceName =
        "AVALUO_EVALUACION_ID_SEQ", allocationSize = 1)
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVALUO_EVALUACION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AVALUO_ID")
    public Long getAvaluoId() {
        return this.avaluoId;
    }

    public void setAvaluoId(Long avaluoId) {
        this.avaluoId = avaluoId;
    }

    @Column(name = "ASIGNACION_ID")
    public Long getAsignacionId() {
        return this.asignacionId;
    }

    public void setAsignacionId(Long asignacionId) {
        this.asignacionId = asignacionId;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = false)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    // bi-directional many-to-one association to AvaluoEvaluacionDetalle
    @OneToMany(mappedBy = "avaluoEvaluacion", cascade = CascadeType.ALL)
    public List<AvaluoEvaluacionDetalle> getAvaluoEvaluacionDetalles() {
        return this.avaluoEvaluacionDetalles;
    }

    public void setAvaluoEvaluacionDetalles(
        List<AvaluoEvaluacionDetalle> avaluoEvaluacionDetalles) {
        this.avaluoEvaluacionDetalles = avaluoEvaluacionDetalles;
    }

}
