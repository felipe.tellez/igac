package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * Mensaje para notificación de errores de los servicios Rest
 *
 * @author franz.gamba
 */
public class ErrorVO implements Serializable {

    private static final long serialVersionUID = 7276377145365782579L;

    private String mensajeError;

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public ErrorVO(String mensaje) {
        this.mensajeError = mensaje;
    }

}
