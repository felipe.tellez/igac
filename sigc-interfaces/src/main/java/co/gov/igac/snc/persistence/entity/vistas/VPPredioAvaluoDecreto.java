package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * The persistent class for the V_P_PREDIO_AVALUO_DECRETO database table.
 *
 * @author david.cifuentes
 * @modified by: javier.aponte: Se agrega el campo cancela inscribe que corresponde al
 * PPredioAvaluoCatastral Se cambio Predio por PPredio porque la vista corresponde a Ppredio y no a
 * Predio
 * @modified by: javier.aponte 07-07-2013 se implementa la interfaz Cloneable y se agrega método
 * clone
 *
 *
 * @modified by javier.aponte 21/02/2014
 *
 */
@Entity
@Table(name = "V_P_PREDIO_AVALUO_DECRETO")
public class VPPredioAvaluoDecreto implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private String autoavaluo;
    private Date decretoFecha;
    private String decretoNumero;
    private Date fechaInscripcionCatastral;
    private Long id;
    private String justificacionAvaluo;
    private String notaAvaluo;
    private String observaciones;
    private PPredio PPredio;
    private Documento soporteDocumento;
    private Double valorTerreno;
    private Double valorTotalAvaluoCatastral;
    private Double valorTotalConsConvencional;
    private Double valorTotalConsNconvencional;
    private Double valorTotalConstruccion;
    private Date vigencia;
    private String cancelaInscribe;
    private String actualizacion;

    public VPPredioAvaluoDecreto() {
    }

    @Id
    @Column(nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false, length = 2)
    public String getAutoavaluo() {
        return this.autoavaluo;
    }

    public void setAutoavaluo(String autoavaluo) {
        this.autoavaluo = autoavaluo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DECRETO_FECHA", nullable = false)
    public Date getDecretoFecha() {
        return this.decretoFecha;
    }

    public void setDecretoFecha(Date decretoFecha) {
        this.decretoFecha = decretoFecha;
    }

    @Column(name = "DECRETO_NUMERO", length = 50)
    public String getDecretoNumero() {
        return this.decretoNumero;
    }

    public void setDecretoNumero(String decretoNumero) {
        this.decretoNumero = decretoNumero;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "JUSTIFICACION_AVALUO", length = 600)
    public String getJustificacionAvaluo() {
        return this.justificacionAvaluo;
    }

    public void setJustificacionAvaluo(String justificacionAvaluo) {
        this.justificacionAvaluo = justificacionAvaluo;
    }

    @Column(name = "NOTA_AVALUO", length = 600)
    public String getNotaAvaluo() {
        return this.notaAvaluo;
    }

    public void setNotaAvaluo(String notaAvaluo) {
        this.notaAvaluo = notaAvaluo;
    }

    @Column(length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID")
    public Documento getSoporteDocumento() {
        return this.soporteDocumento;
    }

    public void setSoporteDocumento(Documento soporteDocumento) {
        this.soporteDocumento = soporteDocumento;
    }

    @Column(name = "VALOR_TERRENO", nullable = false, precision = 18, scale = 2)
    public Double getValorTerreno() {
        return this.valorTerreno;
    }

    public void setValorTerreno(Double valorTerreno) {
        this.valorTerreno = valorTerreno;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL", nullable = false, precision = 18, scale = 2)
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(
        Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @Column(name = "VALOR_TOTAL_CONS_CONVENCIONAL", nullable = false, precision = 18, scale = 2)
    public Double getValorTotalConsConvencional() {
        return this.valorTotalConsConvencional;
    }

    public void setValorTotalConsConvencional(
        Double valorTotalConsConvencional) {
        this.valorTotalConsConvencional = valorTotalConsConvencional;
    }

    @Column(name = "VALOR_TOTAL_CONS_NCONVENCIONAL", precision = 18, scale = 2)
    public Double getValorTotalConsNconvencional() {
        return this.valorTotalConsNconvencional;
    }

    public void setValorTotalConsNconvencional(
        Double valorTotalConsNconvencional) {
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCION", nullable = false, precision = 18, scale = 2)
    public Double getValorTotalConstruccion() {
        return this.valorTotalConstruccion;
    }

    public void setValorTotalConstruccion(Double valorTotalConstruccion) {
        this.valorTotalConstruccion = valorTotalConstruccion;
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public String getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(String actualizacion) {
        this.actualizacion = actualizacion;
    }

    /**
     * @author javier.aponte Método para poder hacer un clon de un objeto VPPredioAvaluoDecreto
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
