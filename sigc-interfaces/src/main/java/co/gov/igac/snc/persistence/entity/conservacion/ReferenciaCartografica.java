package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.*;

/**
 * ReferenciaCartografica entity. @author MyEclipse Persistence Tools
 */
@Entity
@NamedQueries({@NamedQuery(name = "findReferenciaCartograficaByPredio", query =
        "from ReferenciaCartografica rc where rc.predio = :predio")})
@Table(name = "REFERENCIA_CARTOGRAFICA", schema = "SNC_CONSERVACION")
public class ReferenciaCartografica implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3361950309868613341L;

    // Fields
    /* TODO: Validar si los datos para la anotación en el comentario son correctos
     * @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REFERENCIA_CARTOGRAFICA_ID_SEQ")
     */
    @GeneratedValue
    private Long id;
    private Predio predio;
    private String plancha;
    private String vuelo;
    private String foto;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public ReferenciaCartografica() {
    }

    /** minimal constructor */
    public ReferenciaCartografica(Long id, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public ReferenciaCartografica(Long id, Predio predio, String plancha,
        String vuelo, String foto, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.plancha = plancha;
        this.vuelo = vuelo;
        this.foto = foto;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "PLANCHA", length = 20)
    public String getPlancha() {
        return this.plancha;
    }

    public void setPlancha(String plancha) {
        this.plancha = plancha;
    }

    @Column(name = "VUELO", length = 20)
    public String getVuelo() {
        return this.vuelo;
    }

    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    @Column(name = "FOTO", length = 20)
    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
