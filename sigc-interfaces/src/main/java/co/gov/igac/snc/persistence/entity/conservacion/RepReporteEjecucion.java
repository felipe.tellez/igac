package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ERepReporteEjecucionEstado;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the REP_REPORTE_EJECUCION database table.
 *
 */
@Entity
@Table(name="REP_REPORTE_EJECUCION")
public class RepReporteEjecucion implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String departamentoCodigo;
	private String descripcion;
	private String estado;
	private Date fechaFin;
	private Date fechaInicio;
	private Double logProcesoId;
	private String municipioCodigo;
	private String resultadoGeneracion;
	private String rutaReporteGenerado;
	private String usuario;
	private List<RepReporteEjecucionDato> repReporteEjecucionDatos;
	private List<RepReporteEjecucionDetalle> repReporteEjecucionDetalles;
	private RepReporte repReporte;
	private String observaciones;
	private String extencionFormato;
	private EstructuraOrganizacional territorial;
	private EstructuraOrganizacional UOC;
	private String formato;
	private String codigoReporte;
	private BigDecimal valorTamano;
	private String unidadTamano;
	private String fechaCorte;
	private String vigencia;
	private String departamentoNombre;
	private String municipioNombre;
	private String ejecucionHorario;
	private Date fechaGeneracion;
	private Integer numeroEnvio;
    private String usuarioLog;
    private Date fechaLog;
    private String reporteVacio;

	// Transient
	private boolean selected;
	private boolean asociado;

	public RepReporteEjecucion() {
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REP_REPORTE_EJECUCION_ID_SEQ")
	@SequenceGenerator(name="REP_REPORTE_EJECUCION_ID_SEQ", sequenceName = "REP_REPORTE_EJECUCION_ID_SEQ", allocationSize = 1 )
	@Column(name = "ID", nullable = false, precision = 10, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="DEPARTAMENTO_CODIGO")
	public String getDepartamentoCodigo() {
		return this.departamentoCodigo;
	}

	public void setDepartamentoCodigo(String departamentoCodigo) {
		this.departamentoCodigo = departamentoCodigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_FIN")
	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_INICIO")
	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}


	@Column(name="LOG_PROCESO_ID")
	public Double getLogProcesoId() {
		return this.logProcesoId;
	}

	public void setLogProcesoId(Double logProcesoId) {
		this.logProcesoId = logProcesoId;
	}

	@Column(name="MUNICIPIO_CODIGO")
	public String getMunicipioCodigo() {
		return this.municipioCodigo;
	}

	public void setMunicipioCodigo(String municipioCodigo) {
		this.municipioCodigo = municipioCodigo;
	}

	@Column(name="RESULTADO_GENERACION")
	public String getResultadoGeneracion() {
		return this.resultadoGeneracion;
	}

	public void setResultadoGeneracion(String resultadoGeneracion) {
		this.resultadoGeneracion = resultadoGeneracion;
	}

	@Column(name="RUTA_REPORTE_GENERADO")
	public String getRutaReporteGenerado() {
		return this.rutaReporteGenerado;
	}

	public void setRutaReporteGenerado(String rutaReporteGenerado) {
		this.rutaReporteGenerado = rutaReporteGenerado;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	//bi-directional many-to-one association to RepReporteEjecucionDato
	@OneToMany(mappedBy="repReporteEjecucion")
	public List<RepReporteEjecucionDato> getRepReporteEjecucionDatos() {
		return this.repReporteEjecucionDatos;
	}

	public void setRepReporteEjecucionDatos(List<RepReporteEjecucionDato> repReporteEjecucionDatos) {
		this.repReporteEjecucionDatos = repReporteEjecucionDatos;
	}

	public RepReporteEjecucionDato addRepReporteEjecucionDato(RepReporteEjecucionDato repReporteEjecucionDato) {
		getRepReporteEjecucionDatos().add(repReporteEjecucionDato);
		repReporteEjecucionDato.setRepReporteEjecucion(this);

		return repReporteEjecucionDato;
	}

	public RepReporteEjecucionDato removeRepReporteEjecucionDato(RepReporteEjecucionDato repReporteEjecucionDato) {
		getRepReporteEjecucionDatos().remove(repReporteEjecucionDato);
		repReporteEjecucionDato.setRepReporteEjecucion(null);

		return repReporteEjecucionDato;
	}

	//bi-directional many-to-one association to RepReporteEjecucionDetalle
	@OneToMany(mappedBy="repReporteEjecucion")
	public List<RepReporteEjecucionDetalle> getRepReporteEjecucionDetalles() {
		return this.repReporteEjecucionDetalles;
	}

	public void setRepReporteEjecucionDetalles(List<RepReporteEjecucionDetalle> repReporteEjecucionDetalles) {
		this.repReporteEjecucionDetalles = repReporteEjecucionDetalles;
	}

	public RepReporteEjecucionDetalle addRepReporteEjecucionDetalle(RepReporteEjecucionDetalle repReporteEjecucionDetalle) {
		getRepReporteEjecucionDetalles().add(repReporteEjecucionDetalle);
		repReporteEjecucionDetalle.setRepReporteEjecucion(this);

		return repReporteEjecucionDetalle;
	}

	public RepReporteEjecucionDetalle removeRepReporteEjecucionDetalle(RepReporteEjecucionDetalle repReporteEjecucionDetalle) {
		getRepReporteEjecucionDetalles().remove(repReporteEjecucionDetalle);
		repReporteEjecucionDetalle.setRepReporteEjecucion(null);

		return repReporteEjecucionDetalle;
	}

	//bi-directional many-to-one association to RepReporte
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORTE_ID")
	public RepReporte getRepReporte() {
		return this.repReporte;
	}

	public void setRepReporte(RepReporte repReporte) {
		this.repReporte = repReporte;
	}

	@Transient
	public String getObservaciones() {
		if(this.estado != null){
			if(ERepReporteEjecucionEstado.ERROR.getCodigo().equals(this.estado)){
				this.observaciones = "Ha ocurrido un error al ejecutar el reporte, por favor informe administrador o vuelva a generarlo.";
			}
		}
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Transient
	public String getExtencionFormato() {

		if(this.rutaReporteGenerado != null){
			String extFormato[] = this.rutaReporteGenerado.split("\\.");

			for (String resulTadoExtFormato : extFormato) {
				extencionFormato=resulTadoExtFormato;
			}
		}
		return extencionFormato;
	}

	public void setExtencionFormato(String extencionFormato) {
		this.extencionFormato = extencionFormato;
	}

	@Column(name="FORMATO", length = 3)
	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	@Column(name="CODIGO_REPORTE", length = 20)
	public String getCodigoReporte() {
		return codigoReporte;
	}

	public void setCodigoReporte(String codigoReporte) {
		this.codigoReporte = codigoReporte;
	}

	@Column(name = "VALOR_TAMANO", precision = 10, scale = 2)
	public BigDecimal getValorTamano() {
		return valorTamano;
	}

	public void setValorTamano(BigDecimal valorTamano) {
		this.valorTamano = valorTamano;
	}

	@Column(name="UNIDAD_TAMANO", length = 20)
	public String getUnidadTamano() {
		return unidadTamano;
	}

	public void setUnidadTamano(String unidadTamano) {
		this.unidadTamano = unidadTamano;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TERRITORIAL_CODIGO", nullable = false)
	public EstructuraOrganizacional getTerritorial() {
		return this.territorial;
	}

	public void setTerritorial(EstructuraOrganizacional territorial) {
		this.territorial = territorial;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UOC_CODIGO")
	public EstructuraOrganizacional getUOC() {
		return this.UOC;
	}

	public void setUOC(EstructuraOrganizacional uoc) {
		this.UOC = uoc;
	}

	@Column(name="FECHA_CORTE", length = 20)
	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	@Column(name="VIGENCIA", length = 20)
	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	@Column(name="DEPARTAMENTO_NOMBRE", length = 100)
	public String getDepartamentoNombre() {
		return departamentoNombre;
	}

	public void setDepartamentoNombre(String departamentoNombre) {
		this.departamentoNombre = departamentoNombre;
	}

	@Column(name="MUNICIPIO_NOMBRE", length = 100)
	public String getMunicipioNombre() {
		return municipioNombre;
	}

	public void setMunicipioNombre(String municipioNombre) {
		this.municipioNombre = municipioNombre;
	}

	@Column(name="EJECUCION_HORARIO", length = 50)
	public String getEjecucionHorario() {
		return ejecucionHorario;
	}

	public void setEjecucionHorario(String ejecucionHorario) {
		this.ejecucionHorario = ejecucionHorario;
	}

	/**
	 * Determina si el {@link RepReporteEjecucion} está seleccionado en alguna
	 * pantalla o sitio en que se requiera.
	 *
	 * @return
	 */
	@Transient
	public boolean isSelected() {
		return this.selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_GENERACION")
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	@Column(name = "NUMERO_ENVIO", nullable = true, length = 10)
	public Integer getNumeroEnvio() {
		return this.numeroEnvio;
	}

	public void setNumeroEnvio(Integer numeroEnvio) {
		this.numeroEnvio = numeroEnvio;
	}

    @Column(name="REPORTE_VACIO", length = 100, nullable = true)
    public String getReporteVacio() {
        return this.reporteVacio;
    }

    public void setReporteVacio(String reporteVacio) {
        this.reporteVacio = reporteVacio;
    }

        @Column(name = "USUARIO_LOG", nullable = false, length = 100)
	public String getUsuarioLog() {
		return this.usuarioLog;
	}

	public void setUsuarioLog(String usuarioLog) {
		this.usuarioLog = usuarioLog;
	}

        @Temporal(TemporalType.DATE)
	@Column(name = "FECHA_LOG", nullable = false, length = 7)
	public Date getFechaLog() {
		return this.fechaLog;
	}

	public void setFechaLog(Date fechaLog) {
		this.fechaLog = fechaLog;
	}

	/**
	 * Usuario autenticado asociado, solo aplica para vista web
	 * @return
	 */
	@Transient
	public boolean isAsociado() {
		return asociado;
	}

	public void setAsociado(boolean asociado) {
		this.asociado = asociado;
	}
}