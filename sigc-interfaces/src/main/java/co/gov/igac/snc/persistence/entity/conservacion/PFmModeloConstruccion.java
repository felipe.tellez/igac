package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * The persistent class for the P_FM_MODELO_CONSTRUCCION database table.
 *
 * @modified david.cifuentes :: Adicion del campo usoConstruccion :: 20/04/2012 Adicion del campo
 * tipoCalificacion :: 25/04/2012 Adición de la secuencia FM_MODELO_CONSTRUCCION_ID_SEQ Cambio del
 * tipo de dato dimension a Double :: 21/08/12 Adición de la variable unidad y sus correspondientes
 * métodos get y set transient :: 23/11/12
 *
 */
@Entity
@Table(name = "P_FM_MODELO_CONSTRUCCION")
public class PFmModeloConstruccion implements Serializable, IProyeccionObject {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Integer anioConstruccion;
    private Double areaConstruida;
    private Long calificacionAnexoId;
    private String cancelaInscribe;
    private String descripcion;
    private Double dimension;
    private String dimensionUnidadMedida;
    private Date fechaLog;
    private String observaciones;
    private String tipificacion;
    private String tipoConstruccion;
    private Integer totalBanios;
    private Integer totalHabitaciones;
    private Integer totalLocales;
    private Integer totalPisosConstruccion;
    private Integer totalPisosUnidad;
    private Double totalPuntaje;
    private String usuarioLog;
    private List<PFmConstruccionComponente> PFmConstruccionComponentes;
    private PFichaMatrizModelo PFichaMatrizModelo;

    private UsoConstruccion usoConstruccion;
    private String tipoCalificacion;

    private String unidad;

    public PFmModeloConstruccion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FM_MODELO_CONSTRUCCION_ID_SEQ")
    @SequenceGenerator(name = "FM_MODELO_CONSTRUCCION_ID_SEQ", sequenceName =
        "FM_MODELO_CONSTRUCCION_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ANIO_CONSTRUCCION")
    public Integer getAnioConstruccion() {
        return this.anioConstruccion;
    }

    public void setAnioConstruccion(Integer anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "CALIFICACION_ANEXO_ID")
    public Long getCalificacionAnexoId() {
        return this.calificacionAnexoId;
    }

    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.calificacionAnexoId = calificacionAnexoId;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getDimension() {
        return this.dimension;
    }

    public void setDimension(Double dimension) {
        this.dimension = dimension;
    }

    @Column(name = "DIMENSION_UNIDAD_MEDIDA")
    public String getDimensionUnidadMedida() {
        return this.dimensionUnidadMedida;
    }

    public void setDimensionUnidadMedida(String dimensionUnidadMedida) {
        this.dimensionUnidadMedida = dimensionUnidadMedida;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTipificacion() {
        return this.tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @Column(name = "TIPO_CONSTRUCCION")
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    @Column(name = "TOTAL_BANIOS")
    public Integer getTotalBanios() {
        return this.totalBanios;
    }

    public void setTotalBanios(Integer totalBanios) {
        this.totalBanios = totalBanios;
    }

    @Column(name = "TOTAL_HABITACIONES")
    public Integer getTotalHabitaciones() {
        return this.totalHabitaciones;
    }

    public void setTotalHabitaciones(Integer totalHabitaciones) {
        this.totalHabitaciones = totalHabitaciones;
    }

    @Column(name = "TOTAL_LOCALES")
    public Integer getTotalLocales() {
        return this.totalLocales;
    }

    public void setTotalLocales(Integer totalLocales) {
        this.totalLocales = totalLocales;
    }

    @Column(name = "TOTAL_PISOS_CONSTRUCCION")
    public Integer getTotalPisosConstruccion() {
        return this.totalPisosConstruccion;
    }

    public void setTotalPisosConstruccion(Integer totalPisosConstruccion) {
        this.totalPisosConstruccion = totalPisosConstruccion;
    }

    @Column(name = "TOTAL_PISOS_UNIDAD")
    public Integer getTotalPisosUnidad() {
        return this.totalPisosUnidad;
    }

    public void setTotalPisosUnidad(Integer totalPisosUnidad) {
        this.totalPisosUnidad = totalPisosUnidad;
    }

    @Column(name = "TOTAL_PUNTAJE")
    public Double getTotalPuntaje() {
        return this.totalPuntaje;
    }

    public void setTotalPuntaje(Double totalPuntaje) {
        this.totalPuntaje = totalPuntaje;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to PFmConstruccionComponente
    @OneToMany(mappedBy = "PFmModeloConstruccion")
    public List<PFmConstruccionComponente> getPFmConstruccionComponentes() {
        return this.PFmConstruccionComponentes;
    }

    public void setPFmConstruccionComponentes(
        List<PFmConstruccionComponente> PFmConstruccionComponentes) {
        this.PFmConstruccionComponentes = PFmConstruccionComponentes;
    }

    //bi-directional many-to-one association to PFichaMatrizModelo
    @ManyToOne
    @JoinColumn(name = "FICHA_MATRIZ_MODELO_ID")
    public PFichaMatrizModelo getPFichaMatrizModelo() {
        return this.PFichaMatrizModelo;
    }

    public void setPFichaMatrizModelo(PFichaMatrizModelo PFichaMatrizModelo) {
        this.PFichaMatrizModelo = PFichaMatrizModelo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USO_ID", nullable = true)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "TIPO_CALIFICACION", length = 30)
    public String getTipoCalificacion() {
        return this.tipoCalificacion;
    }

    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    // ----------------------------------- //
    /**
     * Método transient para el manejo de la enumeracion correspondietnte al PFmModeloConstruccion.
     */
    @Transient
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

}
