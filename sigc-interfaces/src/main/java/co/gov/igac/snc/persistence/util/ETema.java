package co.gov.igac.snc.persistence.util;

/**
 * enumeración con los temas usados en la consulta gerencial a nivel predial
 *
 * @author fredy.wilches
 */
public enum ETema {
    TEMA1("Total Predial"),
    TEMA2("Total Manzanas"),
    TEMA3("Destino Económico"),
    TEMA4("Condición de Propiedad"),
    TEMA5("Rango de Avalúos"),
    TEMA6("Rango de Superficies"),
    TEMA7("Comparativo por años");

    private String descripcion;

    private ETema(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
