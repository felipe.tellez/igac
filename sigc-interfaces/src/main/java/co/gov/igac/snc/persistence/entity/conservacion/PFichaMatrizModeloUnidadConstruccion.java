package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import java.util.List;

import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/*
 * MODIFICACIONES PROPIAS A LA CLASE PFichaMatrizModeloUnidadConstruccion:
 *
 * @modified leidy.gonzalez :: Adición del atributo anioCancelacion y sus respectivos get y set ::
 * 22/05/2014
 *
 */
public class PFichaMatrizModeloUnidadConstruccion implements IProyeccionObject,
    IModeloUnidadConstruccion {

    private PFmModeloConstruccion pFmModeloConstruccion;
    private PFichaMatrizModelo pFichaMatrizModelo;

    public PFichaMatrizModeloUnidadConstruccion(
        PFmModeloConstruccion pFmModeloConstruccion,
        PFichaMatrizModelo pFichaMatrizModelo) {
        super();
        this.pFmModeloConstruccion = pFmModeloConstruccion;
        this.pFichaMatrizModelo = pFichaMatrizModelo;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public UsoConstruccion getUsoConstruccion() {
        //return this.pFichaMatrizModelo.getUsoConstruccion();
        return null;
    }

    @Override
    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        //this.pFichaMatrizModelo.setUsoConstruccion(usoConstruccion);
    }

    @Override
    public String getUnidad() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setUnidad(String unidad) {
        // TODO Auto-generated method stub

    }

    @Override
    public Long getCalificacionAnexoId() {
        return this.pFmModeloConstruccion.getCalificacionAnexoId();
    }

    @Override
    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.pFmModeloConstruccion.setCalificacionAnexoId(calificacionAnexoId);
    }

    @Override
    public String getTipoConstruccion() {
        return this.pFmModeloConstruccion.getTipoConstruccion();
    }

    @Override
    public void setTipoConstruccion(String tipoConstruccion) {
        this.pFmModeloConstruccion.setTipoConstruccion(tipoConstruccion);
    }

    @Override
    public String getDescripcion() {
        return this.pFmModeloConstruccion.getDescripcion();
    }

    @Override
    public void setDescripcion(String descripcion) {
        this.pFmModeloConstruccion.setDescripcion(descripcion);
    }

    @Override
    public String getObservaciones() {
        return this.pFmModeloConstruccion.getObservaciones();
    }

    @Override
    public void setObservaciones(String observaciones) {
        this.pFmModeloConstruccion.setObservaciones(observaciones);
    }

    @Override
    public String getTipoCalificacion() {
        return null;
    }

    @Override
    public void setTipoCalificacion(String tipoCalificacion) {
        //this.pFichaMatrizModelo.setTipoCalificacion(tipoCalificacion);
    }

    @Override
    public Double getTotalPuntaje() {
        return this.pFmModeloConstruccion.getTotalPuntaje();
    }

    @Override
    public void setTotalPuntaje(Double totalPuntaje) {
        this.pFmModeloConstruccion.setTotalPuntaje(totalPuntaje);
    }

    @Override
    public String getTipificacion() {
        return this.pFmModeloConstruccion.getTipificacion();
    }

    @Override
    public void setTipificacion(String tipificacion) {
        this.pFmModeloConstruccion.setTipificacion(tipificacion);
    }

    @Override
    public Integer getAnioConstruccion() {
        return this.pFmModeloConstruccion.getAnioConstruccion();
    }

    @Override
    public void setAnioConstruccion(Integer anioConstruccion) {
        this.pFmModeloConstruccion.setAnioConstruccion(anioConstruccion);
    }

    @Override
    public Double getAreaConstruida() {
        if (!this.pFichaMatrizModelo.getAreaConstruida().equals(
            this.pFmModeloConstruccion.getAreaConstruida())) {
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Inconsistencia de datos en modelos de ficha matriz - Area Construida");
        }
        return this.pFmModeloConstruccion.getAreaConstruida();
    }

    @Override
    public void setAreaConstruida(Double areaConstruida) {
        this.pFichaMatrizModelo.setAreaConstruida(areaConstruida);
        this.pFmModeloConstruccion.setAreaConstruida(areaConstruida);

    }

    @Override
    public Double getAvaluo() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setAvaluo(Double avaluo) {
        // TODO Auto-generated method stub

    }

    @Override
    public Integer getTotalBanios() {
        return this.pFmModeloConstruccion.getTotalBanios();
    }

    @Override
    public void setTotalBanios(Integer totalBanios) {
        this.pFmModeloConstruccion.setTotalBanios(totalBanios);
    }

    @Override
    public Integer getTotalHabitaciones() {
        return this.pFmModeloConstruccion.getTotalHabitaciones();
    }

    @Override
    public void setTotalHabitaciones(Integer totalHabitaciones) {
        this.pFmModeloConstruccion.setTotalHabitaciones(totalHabitaciones);
    }

    @Override
    public Integer getTotalLocales() {
        return this.pFmModeloConstruccion.getTotalLocales();
    }

    @Override
    public void setTotalLocales(Integer totalLocales) {
        this.pFmModeloConstruccion.setTotalLocales(totalLocales);
    }

    @Override
    public Integer getTotalPisosUnidad() {
        return this.pFmModeloConstruccion.getTotalPisosUnidad();
    }

    @Override
    public void setTotalPisosUnidad(Integer totalPisosUnidad) {
        this.pFmModeloConstruccion.setTotalPisosUnidad(totalPisosUnidad);
    }

    @Override
    public Integer getTotalPisosConstruccion() {
        return this.pFmModeloConstruccion.getTotalPisosConstruccion();
    }

    @Override
    public void setTotalPisosConstruccion(Integer totalPisosConstruccion) {
        this.pFmModeloConstruccion.setTotalPisosConstruccion(totalPisosConstruccion);
    }

    @Override
    public Double getValorM2Construccion() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setValorM2Construccion(Double valorM2Construccion) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getPisoUbicacion() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPisoUbicacion(String pisoUbicacion) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCancelaInscribe() {
        if (!this.pFichaMatrizModelo.getCancelaInscribe().equals(
            this.pFmModeloConstruccion.getCancelaInscribe())) {
            throw new ExcepcionSNC("algúnCódigo", ESeveridadExcepcionSNC.ERROR,
                "Inconsistencia de datos en modelos de ficha matriz - CancelaInscribe");
        }
        return this.pFmModeloConstruccion.getCancelaInscribe();
    }

    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.pFichaMatrizModelo.setCancelaInscribe(cancelaInscribe);
        this.pFmModeloConstruccion.setCancelaInscribe(cancelaInscribe);
    }

    @Override
    public List<PUnidadConstruccionComp> getPUnidadConstruccionComps() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPUnidadConstruccionComps(
        List<PUnidadConstruccionComp> PUnidadConstruccionComps) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getTipoDominio() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTipoDominio(String tipoDominio) {
        // TODO Auto-generated method stub

    }

    @Override
    public Date getFechaInscripcionCatastral() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCancelaInscribeValor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getUsuarioLog() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setUsuarioLog(String usuarioLog) {
        // TODO Auto-generated method stub

    }

    @Override
    public Date getFechaLog() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFechaLog(Date fechaLog) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getTipificacionValor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTipificacionValor(String tipificacionValor) {
        // TODO Auto-generated method stub

    }

    @Override
    public Integer getAnioCancelacion() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setAnioCancelacion(Integer anioConstruccion) {
        // TODO Auto-generated method stub

    }

}
