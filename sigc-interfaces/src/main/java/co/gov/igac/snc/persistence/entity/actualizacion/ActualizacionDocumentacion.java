package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EActualizacionDocumentacionZona;
import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the ACTUALIZACION_DOCUMENTACION database table.
 *
 * @author franz.gamba
 *
 * @modified by franz.gamba :: 08/05/2012:: se adicionaron los metodos transient isUrbano, isRural,
 * isAnalogo, isDigital e isNo	para el caso de uso CU-NP-FA-203
 */
@Entity
@Table(name = "ACTUALIZACION_DOCUMENTACION")
public class ActualizacionDocumentacion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3789824482046464613L;
    private Long id;
    private String categoria;
    private Date fechaLog;
    private String formato;
    private String requerido;
    private Documento soporteDocumento;
    private TipoDocumento tipoDocumento;
    private String urbanoRural;
    private String urlServicio;
    private String usuarioLog;
    private Actualizacion actualizacion;

    /**
     * Transient
     */
    private boolean digital;
    private boolean analogo;
    private boolean no;

    public ActualizacionDocumentacion() {
    }

    /** minimal constructor */
    public ActualizacionDocumentacion(String categoria, Date fechaLog,
        String formato, String requerido, Documento soporteDocumento,
        TipoDocumento tipoDocumento, String usuarioLog,
        Actualizacion actualizacion) {
        super();
        this.categoria = categoria;
        this.fechaLog = fechaLog;
        this.formato = formato;
        this.requerido = requerido;
        this.soporteDocumento = soporteDocumento;
        this.tipoDocumento = tipoDocumento;
        this.usuarioLog = usuarioLog;
        this.actualizacion = actualizacion;
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_DOCUMENTACION_ID_GENERATOR",
        sequenceName = "ACTUALIZACION_DOCUMENTA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_DOCUMENTACION_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getFormato() {
        return this.formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getRequerido() {
        return this.requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID")
    public Documento getSoporteDocumento() {
        return this.soporteDocumento;
    }

    public void setSoporteDocumento(Documento soporteDocumento) {
        this.soporteDocumento = soporteDocumento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID", nullable = false)
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "URBANO_RURAL")
    public String getUrbanoRural() {
        return this.urbanoRural;
    }

    public void setUrbanoRural(String urbanoRural) {
        this.urbanoRural = urbanoRural;
    }

    @Column(name = "URL_SERVICIO")
    public String getUrlServicio() {
        return this.urlServicio;
    }

    public void setUrlServicio(String urlServicio) {
        this.urlServicio = urlServicio;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //---------------------------------------------------------------------------------------------
    @Transient
    public boolean isRural() {
        if (this.urbanoRural != null && (this.urbanoRural.equals(
            EActualizacionDocumentacionZona.RURAL.getDescripcion()) ||
            this.urbanoRural.equals(EActualizacionDocumentacionZona.URBANO_RURAL.getDescripcion()))) {
            return true;
        } else {
            return false;
        }
    }

    @Transient
    public boolean isUrbano() {
        if (this.urbanoRural != null && (this.urbanoRural.equals(
            EActualizacionDocumentacionZona.URBANO.getDescripcion()) ||
            this.urbanoRural.equals(EActualizacionDocumentacionZona.URBANO_RURAL.getDescripcion()))) {
            return true;
        } else {
            return false;
        }
    }

    @Transient
    public boolean isDigital() {
        return digital;
    }

    @Transient
    public void setDigital(boolean digital) {
        if (!digital) {
            this.digital = digital;
        } else {
            this.digital = digital;
            this.analogo = false;
            this.no = false;
        }
    }

    @Transient
    public boolean isAnalogo() {
        return analogo;
    }

    @Transient
    public void setAnalogo(boolean analogo) {
        if (!analogo) {
            this.analogo = analogo;
        } else {
            this.analogo = analogo;
            this.digital = false;
            this.no = false;
        }
    }

    @Transient
    public boolean isNo() {
        return no;
    }

    @Transient
    public void setNo(boolean no) {
        if (!no) {
            this.no = no;
        } else {
            this.analogo = false;
            this.digital = false;
            this.no = no;
        }
    }

    @Transient
    public boolean isOpcional() {
        if (this.requerido != null && (this.requerido.equals(ESiNo.NO.getCodigo()))) {
            return true;
        } else {

            return false;
        }
    }

}
