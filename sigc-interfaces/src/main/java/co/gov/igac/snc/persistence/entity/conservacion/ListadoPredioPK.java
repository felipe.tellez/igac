package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the LISTADO_PREDIO database table.
 *
 */
@Embeddable
public class ListadoPredioPK implements Serializable {
    //default serial version id, required for serializable classes.

    private static final long serialVersionUID = 1L;
    private String municipioCodigo;
    private long vigencia;
    private long predioId;

    public ListadoPredioPK() {
    }

    @Column(name = "MUNICIPIO_CODIGO")
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public long getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(long vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "PREDIO_ID")
    public long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(long predioId) {
        this.predioId = predioId;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ListadoPredioPK)) {
            return false;
        }
        ListadoPredioPK castOther = (ListadoPredioPK) other;
        return this.municipioCodigo.equals(castOther.municipioCodigo) &&
            (this.vigencia == castOther.vigencia) &&
            (this.predioId == castOther.predioId);
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.municipioCodigo.hashCode();
        hash = hash * prime + ((int) (this.vigencia ^ (this.vigencia >>> 32)));
        hash = hash * prime + ((int) (this.predioId ^ (this.predioId >>> 32)));

        return hash;
    }
}
