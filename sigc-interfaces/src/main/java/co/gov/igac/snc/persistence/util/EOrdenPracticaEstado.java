/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles estados de una orden de práctica. Coresponde a dominio
 * ORDEN_PRACTICA_ESTADO
 *
 * @author pedro.garcia
 */
public enum EOrdenPracticaEstado {

    //D: el atributo 'codigo' corresponde al código en la bd
    //   el nombre de cada enumeración es el usado en la aplicación
    ENVIADA("ENVIADA"),
    GENERADA("GENERADA");

    private String codigo;

    private EOrdenPracticaEstado(String codigoP) {
        this.codigo = codigoP;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
