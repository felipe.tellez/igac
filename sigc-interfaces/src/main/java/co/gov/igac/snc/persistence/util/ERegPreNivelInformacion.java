package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd RegPreNivelInformacion
 *
 * @author javier.aponte
 */
public enum ERegPreNivelInformacion {

    MUNICIPIO("1"),
    ZONA("2"),
    SECTOR("3"),
    MANZANA("4");

    private String codigo;

    private ERegPreNivelInformacion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
