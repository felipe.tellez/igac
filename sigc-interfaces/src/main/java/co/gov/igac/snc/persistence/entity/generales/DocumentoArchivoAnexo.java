package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DOCUMENTO_ARCHIVO_ANEXO database table.
 *
 * @Modified 13-12-11 juan.agudelo se agregó la secuencia DOCUMENTO_ARCHIVO_ANEXO_ID_SEQ y se
 * adicionó el objeto documento 22-12-12 Se agrego la relación con idRepositorioDocumentos
 */
@Entity
@Table(name = "DOCUMENTO_ARCHIVO_ANEXO")
public class DocumentoArchivoAnexo implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private String archivo;
    private String descripcion;
    private String estado;
    private Date fechaLog;
    private String formato;
    private String idRepositorioDocumentos;
    private String usuarioLog;
    private Documento documento;

    public DocumentoArchivoAnexo() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOCUMENTO_ARCHIVO_ANEXO_ID_SEQ")
    @SequenceGenerator(name = "DOCUMENTO_ARCHIVO_ANEXO_ID_SEQ", sequenceName =
        "DOCUMENTO_ARCHIVO_ANEXO_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(nullable = false, length = 250)
    public String getArchivo() {
        return this.archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    @Column(length = 2000)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, length = 50)
    public String getFormato() {
        return this.formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Documento
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "DOCUMENTO_ID", nullable = false)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS", length = 100)
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

}
