/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles estados de un trámite
 *
 * @author pedro.garcia
 */
public enum ETramiteEstado {

    //D: el atributo 'codigo' corresponde al código en la bd
    //   el nombre de cada enumeración es el usado en la aplicación
    //D: los códigos de los dominios son varchar2 en la bd
    ARCHIVADO("ARCHIVADO"),
    CANCELADO("CANCELADO"),
    DEVUELTO("DEVUELTO"),
    DEVUELTO_RADICACION("DEVUELTO RADICACION"),
    // Este estado no afecta al trámite y solo se utiliza para consultar el
    // registro historico de observaciónes sobre el trámite
    DEVUELTO_REVISION_PROYECCION("DEVUELTO REVISION PROYECCION"),
    /**
     * Este estado solo se utiliza para mantener persistida una observación temporal mientras se
     * avanza el proceso.
     */
    ESPERANDO_CONFIRMAR_DEVOLUCION("ESPERANDO CONFIRMAR DEVOLUCION"),
    EN_ESPERA_DE_COMPLETAR_DOC("EN ESPERA DE COMPLETAR DOC"),
    EN_ESPERA_DE_VIA_GUBERNATIVA("EN ESPERA DE VIA GUBERNATIVA"),
    EN_PROCESO("EN PROCESO"),
    FINALIZADO("FINALIZADO"),
    /**
     * esta se usa para diferenciar el 'finalizado' por alguna otra causa, al finalizado porque el
     * proceso se completó y se hizo lo que se pedía en en trámite
     */
    FINALIZADO_APROBADO("FINALIZADO_APROBADO"),
    APROBADO("APROBADO"),
    NO_APROBADO("NO APROBADO"),
    POR_APROBAR("POR APROBAR"),
    POR_CANCELAR("POR CANCELAR"),
    REASIGNADO("REASIGNADO"),
    RECHAZADO("RECHAZADO"),
    RECIBIDO("RECIBIDO"),
    ACTIVO(
        "APROBADO,EN ESPERA DE COMPLETAR DOC,EN PROCESO,NO APROBADO,POR APROBAR,POR CANCELAR,RECIBIDO");

    /*
     * Se crea la enumeración el estado ACTIVO para evitar confusiones en el manejo y el tipo de
     * estados en BD que comprende. Inicialmente se consideró como todo lo no rechazado, no
     * cancelado, no finalizado aprobado y no finalizado.
     */
    private String codigo;

    private ETramiteEstado(String codigoP) {
        this.codigo = codigoP;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
