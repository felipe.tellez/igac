package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 * The persistent class for the V_TRAMITE_PREDIO database table.
 *
 * @author david.cifuentes
 *
 */
@Entity
@Table(name = "V_TRAMITE_PREDIO")
public class VTramitePredio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CONSECUTIVO")
    private Long consecutivo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    private Predio predio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    private Tramite tramite;

    public VTramitePredio() {
    }

    public Long getConsecutivo() {
        return this.consecutivo;
    }

    public void setConsecutivo(Long consecutivo) {
        this.consecutivo = consecutivo;
    }

    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramiteId(Tramite tramite) {
        this.tramite = tramite;
    }

}
