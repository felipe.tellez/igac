/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos de estados de bloqueo de tramites.
 *
 * @author dumar.penuela
 */
public enum ETramiteEstadoBloqueo {

    SUSPENDIDO("SUSPENDIDO", "Trámite suspendido por bloqueo de predio");

    private String codigo;
    private String valor;

    private ETramiteEstadoBloqueo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return valor;
    }
}
