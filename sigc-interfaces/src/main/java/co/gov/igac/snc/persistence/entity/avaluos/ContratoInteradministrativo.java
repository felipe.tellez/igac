package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import javax.persistence.Transient;

/**
 * The persistent class for the CONTRATO_INTERADMINISTRATIVO database table.
 */
/*
 * @modified christian.rodriguez Se agrega el atributo consignaciones @modified christian.rodriguez
 * Se agrega el atributo entidadSolicitante @modified christian.rodriguez Se eliminan los atributos
 * interventorDocumento, interventorNombre por nueva estructura de la tabla @modified
 * christian.rodriguez Se agrega el campo interventor
 *
 */
@Entity
@Table(name = "CONTRATO_INTERADMINISTRATIVO", schema = "SNC_AVALUOS")
public class ContratoInteradministrativo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String actaLiquidacion;
    private Long actaLiquidacionDocumentoId;
    private String contactoCorreoElectronico;
    private String contactoPrimerApellido;
    private String contactoPrimerNombre;
    private String contactoSegundoApellido;
    private String contactoSegundoNombre;
    private String contactoTelefono;
    private Long contratoDocumentoId;
    private Date fechaFin;
    private Date fechaInicio;
    private Date fechaLog;
    private Date fechaTerminacion;
    private String numero;
    private Long numeroAvaluosContratados;
    private String objeto;
    private String regimen;
    private String usuarioLog;
    private Double valor;
    private Long vigencia;

    private ProfesionalAvaluo interventor;
    private Solicitante entidadSolicitante;
    private List<Consignacion> consignaciones;

    public ContratoInteradministrativo() {
    }

    @Id
    @SequenceGenerator(name = "CONTRATO_INTERADMINISTRATIVO_ID_GENERATOR", sequenceName =
        "CONTRATO_INTERADMINISTR_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTRATO_INTERADMINISTRATIVO_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTA_LIQUIDACION", nullable = false, length = 2)
    public String getActaLiquidacion() {
        return this.actaLiquidacion;
    }

    public void setActaLiquidacion(String actaLiquidacion) {
        this.actaLiquidacion = actaLiquidacion;
    }

    @Column(name = "ACTA_LIQUIDACION_DOCUMENTO_ID", precision = 10)
    public Long getActaLiquidacionDocumentoId() {
        return this.actaLiquidacionDocumentoId;
    }

    public void setActaLiquidacionDocumentoId(Long actaLiquidacionDocumentoId) {
        this.actaLiquidacionDocumentoId = actaLiquidacionDocumentoId;
    }

    @Column(name = "CONTACTO_CORREO_ELECTRONICO", length = 50)
    public String getContactoCorreoElectronico() {
        return this.contactoCorreoElectronico;
    }

    public void setContactoCorreoElectronico(String contactoCorreoElectronico) {
        this.contactoCorreoElectronico = contactoCorreoElectronico;
    }

    @Column(name = "CONTACTO_PRIMER_APELLIDO", length = 100)
    public String getContactoPrimerApellido() {
        return this.contactoPrimerApellido;
    }

    public void setContactoPrimerApellido(String contactoPrimerApellido) {
        this.contactoPrimerApellido = contactoPrimerApellido;
    }

    @Column(name = "CONTACTO_PRIMER_NOMBRE", length = 100)
    public String getContactoPrimerNombre() {
        return this.contactoPrimerNombre;
    }

    public void setContactoPrimerNombre(String contactoPrimerNombre) {
        this.contactoPrimerNombre = contactoPrimerNombre;
    }

    @Column(name = "CONTACTO_SEGUNDO_APELLIDO", length = 100)
    public String getContactoSegundoApellido() {
        return this.contactoSegundoApellido;
    }

    public void setContactoSegundoApellido(String contactoSegundoApellido) {
        this.contactoSegundoApellido = contactoSegundoApellido;
    }

    @Column(name = "CONTACTO_SEGUNDO_NOMBRE", length = 100)
    public String getContactoSegundoNombre() {
        return this.contactoSegundoNombre;
    }

    public void setContactoSegundoNombre(String contactoSegundoNombre) {
        this.contactoSegundoNombre = contactoSegundoNombre;
    }

    @Column(name = "CONTACTO_TELEFONO", length = 50)
    public String getContactoTelefono() {
        return this.contactoTelefono;
    }

    public void setContactoTelefono(String contactoTelefono) {
        this.contactoTelefono = contactoTelefono;
    }

    @Column(name = "CONTRATO_DOCUMENTO_ID", precision = 10)
    public Long getContratoDocumentoId() {
        return this.contratoDocumentoId;
    }

    public void setContratoDocumentoId(Long contratoDocumentoId) {
        this.contratoDocumentoId = contratoDocumentoId;
    }

    // bi-directional many-to-one association to Solicitante
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ENTIDAD_SOLICITANTE_ID", nullable = false)
    public Solicitante getEntidadSolicitante() {
        return this.entidadSolicitante;
    }

    public void setEntidadSolicitante(Solicitante entidadSolicitante) {
        this.entidadSolicitante = entidadSolicitante;
    }

    // bi-directional many-to-one association to ProfesionalAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INTERV_PROFESIONAL_AVALUOS_ID", nullable = false)
    public ProfesionalAvaluo getInterventor() {
        return this.interventor;
    }

    public void setInterventor(ProfesionalAvaluo interventor) {
        this.interventor = interventor;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN", nullable = false)
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO", nullable = false)
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_TERMINACION")
    public Date getFechaTerminacion() {
        return this.fechaTerminacion;
    }

    public void setFechaTerminacion(Date fechaTerminacion) {
        this.fechaTerminacion = fechaTerminacion;
    }

    @Column(nullable = false, length = 20)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "NUMERO_AVALUOS_CONTRATADOS", nullable = false, precision = 6)
    public Long getNumeroAvaluosContratados() {
        return this.numeroAvaluosContratados;
    }

    public void setNumeroAvaluosContratados(Long numeroAvaluosContratados) {
        this.numeroAvaluosContratados = numeroAvaluosContratados;
    }

    @Column(length = 2000)
    public String getObjeto() {
        return this.objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    @Column(length = 30)
    public String getRegimen() {
        return this.regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(nullable = false, precision = 20, scale = 2)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    // bi-directional many-to-one association to Consignacion
    @OneToMany(mappedBy = "contratoInteradministrativo")
    public List<Consignacion> getConsignaciones() {
        return this.consignaciones;
    }

    public void setConsignaciones(List<Consignacion> consignaciones) {
        this.consignaciones = consignaciones;
    }

    @Column(nullable = false, precision = 4)
    public Long getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Long vigencia) {
        this.vigencia = vigencia;
    }

    /**
     * Método para retornar el nombre completo del contacto.
     *
     * @author felipe.cadena
     *
     * @return
     */
    @Transient
    public String getNombreCompletoContacto() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if (this.contactoPrimerNombre != null) {
            sb.append(this.contactoPrimerNombre).append(" ");
        }
        if (this.contactoSegundoNombre != null) {
            sb.append(this.contactoSegundoNombre).append(" ");
        }
        if (this.contactoPrimerApellido != null) {
            sb.append(this.contactoPrimerApellido).append(" ");
        }
        if (this.contactoSegundoApellido != null) {
            sb.append(this.contactoSegundoApellido).append(" ");
        }
        return sb.toString();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Override
    public boolean equals(Object another) {

        if (another == null) {
            return false;
        }
        if (this.getClass() != another.getClass()) {
            return false;
        }

        ContratoInteradministrativo ci = (ContratoInteradministrativo) another;
        if (ci.getId().compareTo(this.id) != 0 ||
            ci.getNumero().compareToIgnoreCase(this.numero) != 0) {

            return false;
        }

        return true;
    }
//---------------------------------------------------------------

    /**
     * autogenerado
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 71 * hash + (this.numero != null ? this.numero.hashCode() : 0);
        return hash;
    }

}
