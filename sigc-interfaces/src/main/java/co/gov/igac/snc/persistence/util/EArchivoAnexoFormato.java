package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio ARCHIVO_ANEXO_FORMATO
 *
 * @author juan.agudelo
 */
public enum EArchivoAnexoFormato {

    ARCHIVO_EXCEL_XLS("2", "application/vnd.ms-excel", "xls"),
    ARCHIVO_EXCEL_XLSX("3", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "xlsx"),
    ARCHIVO_PDF("4", "application/pdf", "pdf"),
    ARCHIVO_PLANO("1", "text/plain", "txt");

    private String codigo;
    private String extension;
    private String valor;

    private EArchivoAnexoFormato(String codigo, String valor, String extension) {
        this.codigo = codigo;
        this.extension = extension;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

}
