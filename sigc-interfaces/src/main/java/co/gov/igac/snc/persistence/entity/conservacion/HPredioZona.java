package co.gov.igac.snc.persistence.entity.conservacion;

import java.sql.Timestamp;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * HPredioZona entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_PREDIO_ZONA", schema = "SNC_CONSERVACION")
public class HPredioZona implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -4737168751947356328L;
    private HPredioZonaId id;
    private HPredio HPredio;
    private Long predioId;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private Double valorM2Terreno;
    private Double area;
    private Double avaluo;
    private Timestamp vigencia;
    private Timestamp fechaInscripcionCatastral;
    private String cancelaInscribe;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public HPredioZona() {
    }

    /** minimal constructor */
    public HPredioZona(HPredioZonaId id, HPredio HPredio, Long predioId,
        String zonaFisica, Double valorM2Terreno, Double area,
        Double avaluo, Timestamp vigencia, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.zonaFisica = zonaFisica;
        this.valorM2Terreno = valorM2Terreno;
        this.area = area;
        this.avaluo = avaluo;
        this.vigencia = vigencia;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HPredioZona(HPredioZonaId id, HPredio HPredio, Long predioId,
        String zonaFisica, String zonaGeoeconomica, Double valorM2Terreno,
        Double area, Double avaluo, Timestamp vigencia,
        Timestamp fechaInscripcionCatastral, String cancelaInscribe,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.zonaFisica = zonaFisica;
        this.zonaGeoeconomica = zonaGeoeconomica;
        this.valorM2Terreno = valorM2Terreno;
        this.area = area;
        this.avaluo = avaluo;
        this.vigencia = vigencia;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "HPredioId", column = @Column(name = "H_PREDIO_ID", nullable =
            false, precision = 10, scale = 0)),
        @AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false, precision =
            10, scale = 0))})
    public HPredioZonaId getId() {
        return this.id;
    }

    public void setId(HPredioZonaId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "ZONA_FISICA", nullable = false, length = 4)
    public String getZonaFisica() {
        return this.zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

    @Column(name = "ZONA_GEOECONOMICA", length = 4)
    public String getZonaGeoeconomica() {
        return this.zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    @Column(name = "VALOR_M2_TERRENO", nullable = false, precision = 12)
    public Double getValorM2Terreno() {
        return this.valorM2Terreno;
    }

    public void setValorM2Terreno(Double valorM2Terreno) {
        this.valorM2Terreno = valorM2Terreno;
    }

    @Column(name = "AREA", nullable = false, precision = 10)
    public Double getArea() {
        return this.area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Column(name = "AVALUO", nullable = false, precision = 18)
    public Double getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Double avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "VIGENCIA", nullable = false, length = 7)
    public Timestamp getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Timestamp vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Timestamp getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Timestamp fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
