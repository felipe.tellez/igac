package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.util.ESiNo;
import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the COMISION_TRAMITE_DATO database table.
 *
 * @modified pedro.garcia adición de secuencia para el id adición de método constructor
 */
@Entity
@Table(name = "COMISION_TRAMITE_DATO")
public class ComisionTramiteDato implements Serializable {

    private static final long serialVersionUID = -4374410931005351459L;

    private long id;
    private String dato;
    private Date fechaLog;
    private String modificado;
    private String usuarioLog;
    private String verificado;
    private ComisionTramite comisionTramite;

    public ComisionTramiteDato() {
    }

    //---------------------   NO TOCAR
    public ComisionTramiteDato(String datoP) {
        this.dato = datoP;
        this.modificado = ESiNo.NO.getCodigo();
        this.verificado = ESiNo.NO.getCodigo();
    }

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMISION_TRAMITE_ID_generator")
    @SequenceGenerator(name = "COMISION_TRAMITE_ID_generator",
        sequenceName = "COMISION_TRAMITE_DATO_ID_SEQ", allocationSize = 1)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDato() {
        return this.dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getModificado() {
        return this.modificado;
    }

    public void setModificado(String modificado) {
        this.modificado = modificado;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public String getVerificado() {
        return this.verificado;
    }

    public void setVerificado(String verificado) {
        this.verificado = verificado;
    }

    //bi-directional many-to-one association to ComisionTramite
    @ManyToOne
    @JoinColumn(name = "COMISION_TRAMITE_ID")
    public ComisionTramite getComisionTramite() {
        return this.comisionTramite;
    }

    public void setComisionTramite(ComisionTramite comisionTramite) {
        this.comisionTramite = comisionTramite;
    }

}
