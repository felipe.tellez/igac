/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.fachadas;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.HPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFmConstruccionComponente;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.conservacion.PModeloConstruccionFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioServidumbre;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioInconsistencia;
import co.gov.igac.snc.persistence.entity.conservacion.PredioPropietarioHis;
import co.gov.igac.snc.persistence.entity.conservacion.PredioServidumbre;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.RangoReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepControlReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionDetalle;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.conservacion.TmpBloqueoMasivo;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.UsoHomologado;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.persistence.util.IProyeccionObject;
import co.gov.igac.snc.util.DivisionAdministrativaDTO;
import co.gov.igac.snc.util.FiltroConsultaCatastralWebService;
import co.gov.igac.snc.util.FiltroDatosConsultaParametrizacionReporte;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.FiltroDatosConsultaReportesRadicacion;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.HorarioTerritorialDTO;
import co.gov.igac.snc.util.JustificacionPropiedadDTO;
import co.gov.igac.snc.util.MunicipioManzanaDTO;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.vo.EstadisticasRegistroVO;
import co.gov.igac.snc.vo.HistoricoAvaluosVO;
import co.gov.igac.snc.vo.ListDetallesVisorVO;
import co.gov.igac.snc.vo.PredioInfoVO;

/**
 * Interfaz para los métodos de conservación
 *
 * @version 2.0
 */
@Local
public interface IConservacionLocal {

    /**
     * Retorna un predio por el número predial especificado
     *
     * @param numPredial número predial del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioByNumeroPredial(String numPredial);

    /**
     * Retorna un predio por el número predial especificado y además obtiene los datos de la tabla
     * persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se requiera inicializar
     * los datos personPredio asociados al predio.
     *
     * @param numPredial número predial del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchPersonasById(Long predioId);

    /**
     * TODO: Documentar el método -> Andrés Salamanca
     *
     * @param consecutivoCatastral
     * @return
     */
    public List<HPredio> findHPredioListByConsecutivoCatastral(
        BigDecimal consecutivoCatastral);

    /**
     * Metodo encargado de buscar las servidumbres asociadas a un predio.
     *
     * @param predio Predio sobre el cual se realiza la búsqueda de servidumbres.
     * @return Retorna las servidumbres asociadas al predio.
     */
    public List<PredioServidumbre> getPredioServidumbreByPredio(Predio predio);

    /**
     * Hace la búsqueda de los Predios que cumplen con los criterios de búsqueda dados
     *
     * @author pedro.garcia
     * @param datosConsultaPredio Objeto creado con los criterios de búsqueda dados por el usuario
     * @param registroDesde índice del primer resultado (de la lista de resultados) que se va a
     * devolver.
     * @param cuantosRegistros máximo número de registros que se van a devolver. Si = -1 => se
     * devuelven todos
     * @return Lista de Predios que cumplen con los criterios de búsqueda. Cada objeto uno es el
     * nombre del departamento, del municipio, y el predio como tal
     */
    public List<Object[]> searchPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, int registroDesde, int cuantosRegistros);

    /**
     * Obtiene, so lo hay, el Predio_Bloqueo vigente para el predio dado
     *
     * @param numeroPredial
     */
    public PredioBloqueo getPredioBloqueo(String numeroPredial);

    /**
     * Obtiene la lista de bloqueos asociados a un predio por el número predial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public List<PredioBloqueo> obtenerPredioBloqueosPorNumeroPredial(
        String numeroPredial);

    /**
     * Obtiene el contedo de la lista de predios bloqueados de acuerdo a los parametros de la
     * busqueda
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPredioBloqueo
     * @return
     */
    public Integer contarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo);

    /**
     * Obtiene la lista de predios bloqueados de acuerdo a los parametros de la busqueda
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPredioBloqueo
     * @return
     */
    public List<PredioBloqueo> buscarPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo,
        final int... rowStartIdxAndCount);

    /**
     * Actualiza una lista de predios bloqueados solo si el usuario es el jefe de conservación
     *
     * @author juan.agudelo
     * @param usuario
     * @param prediosBloqueados
     */
    public ResultadoBloqueoMasivo actualizarPrediosBloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloqueados);

    /**
     * Obtiene la lista de personas bloqueadas de acuerdo a los parametros de la busqueda
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPersonasBloqueo
     * @return
     */
    public List<PersonaBloqueo> buscarPersonasBloqueo(
        FiltroDatosConsultaPersonasBloqueo filtroDatosConsultaPersonasBloqueo);

    /**
     * Actualiza una lista de personas bloqueadas solo si el usuario es el jefe de conservación
     *
     * @author juan.agudelo
     * @param usuario
     * @param personasBloqueadas
     */
    public List<PersonaBloqueo> actualizarPersonasBloqueo(UsuarioDTO usuario,
        List<PersonaBloqueo> personasBloqueadas);

    /**
     * Busqueda de la unidad de construcion de un predio, por medio del mismo
     *
     * @param predio
     * @return UnidadConstruccion
     */
    public UnidadConstruccion findUnidadConstruccionByPredioANDUnidad(
        Predio predio, String unidad);

    /**
     * Busqueda de los Componentes de la unidad de construcion de un predio, por medio de la unidad
     * de construccion
     *
     * @param predio
     * @return UnidadConstruccionComp
     */
    public List<UnidadConstruccionComp> findUnidadConstruccionCompByUnidadConstruccion(
        UnidadConstruccion unidad);

    /**
     * Retorna un predio por su número predial especificado y además obtiene los datos de la tabla
     * predio_avaluo_catastral asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos predioAvaluoCatastral asociados al predio.
     *
     * @param numPredial número predial del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchAvaluosByNumeroPredial(String numPredial);

    /**
     * Retorna un objeto PersonaBloqueo si existe un bloqueo actual para la persona dada
     *
     * @author pedro.garcia
     * @param idPersona Id de la persona
     * @return
     */
    public PersonaBloqueo findPersonaBloqueoByPersonaId(Long idPersona);

    /**
     * Retorna la lista de bloqueos vigentes para una persona dada
     *
     * @juan.agudelo
     * @version 2.0
     * @param idPersona
     * @return
     */
    public List<PersonaBloqueo> obtenerPersonaBloqueosPorPersonaId(
        Long idPersona);

    /**
     * Obtiene la lista de elementos de la tabla USO_CONSTRUCCION
     *
     * @author pedro.garcia
     * @return vacía si no encuentra.
     */
    public List<UsoConstruccion> obtenerUsosConstruccion();

    /**
     * Obtiene la lista de elementos de la vista V_USO_CONSTRUCCION_ZONA
     *
     * @author fredy.wilches
     * @param zona
     * @return vacía si no encuentra.
     */
    public List<VUsoConstruccionZona> obtenerVUsosConstruccionZona(String zona);

    /**
     * Retorna un predio por su número predial y además obtiene los datos de la tabla
     * persona_predio_propiedad asociados.
     *
     * @param numPredial número predial del predio a obtener.
     * @return
     * @author fabio.navarrete
     */
    public Predio getPredioFetchDerechosPropiedadByNumeroPredial(String numPredial);

    /**
     * Retorna la lista de trámites asociados a un predio
     *
     * @author pedro.garcia
     * @param numeroPredial número predial del predio
     * @return
     */
    public List<Tramite> getTramitesByNumeroPredialPredio(String numeroPredial);

    /**
     * Retorna la ficha matriz asociados a un predio por medio del número predial
     *
     * @author javier.aponte
     * @param numeroPredial número predial del predio
     * @return
     */
    public FichaMatriz getFichaMatrizByNumeroPredialPredio(String numeroPredial);

    /**
     * Retorna un Predio sin hacer fetch de nada. OJO: NO modificar
     *
     * @author pedro.garcia
     * @param predioId
     * @return
     */
    public Predio obtenerPredioPorId(Long predioId);

    /**
     * Retorna un predio con los datos de ubicación asociados según el número predial ingresado
     *
     * @param numPredial número predial del predio a obtener
     * @return Predio requerido con las listas inicializadas
     * @author fabio.navarrete
     */
    public Predio obtenerPredioConDatosUbicacionPorNumeroPredial(
        String numPredial);

    /**
     * Cuenta el número de registros que devuelve la búsqueda de predios con los criterios dados
     *
     * @author pedro.garcia
     * @param datosConsultaPredio Objeto creado con los criterios de búsqueda dados por el usuario
     * @return
     */
    public int contarResultadosBusquedaPredios(
        FiltroDatosConsultaPredio datosConsultaPredio);

    /**
     * Método encargado de buscar un PPredio y traer los datos asociados de la entidad
     * PPredioAvaluoCatastral
     *
     * @param idPredio Id del predio a buscar
     * @return
     * @author fabio.navarrete
     */
    public PPredio obtenerPPredioPorIdConAvaluos(Long idPredio);

    /**
     * Retorna la lista de registros de la tabla CALIFICACION_CONSTRUCCION
     *
     * @return
     */
    public List<CalificacionConstruccion> obtenerTodosCalificacionConstruccion();

    /**
     * actualiza el ppredio
     *
     * @author javier.aponte
     * @param predioSeleccionado
     */
    public PPredio actualizarPPredio(PPredio predioSeleccionado);

    /**
     * obtiene ppredio por id y trae las direcciones y las servidumbres asociadas
     *
     * @author javier.aponte
     * @param idPredio
     */
    public PPredio obtenerPPredioPorIdTraerDirecciones(Long idPredio);

    /**
     * borra las direcciones asociadas a un pprredio
     *
     * @param predioDirecciones
     * @author javier.aponte
     */
    public void borrarPPredioDirecciones(PPredioDireccion[] predioDirecciones);

    /**
     * borra las servidumbres asociadas a un pprredio
     *
     * @param predioServidumbres
     * @author javier.aponte
     */
    public void borrarPPredioServidumbres(
        PPredioServidumbre[] predioServidumbres);

    /**
     * adiciona una servidumbre asociada a un pprredio
     *
     * @param servidumbreAdicional
     * @author javier.aponte
     */
    public PPredioServidumbre adicionarServidumbre(PPredioServidumbre servidumbreAdicional);

    /**
     * Ingresa los dados de la justificacion en PPersonaPredioPropiedad
     *
     * @author juan.agudelo
     */
    public void gestionarJustificacion(
        List<PPersonaPredioPropiedad> gestionarJustificacion);

    /**
     * Retorna un PPredio por id y además obtiene los datos de la tabla persona_predio asociados a
     * éste.
     *
     * @param id id del predio a obtener
     * @return
     * @author juan.agudelo
     */
    public PPredio getPPredioFetchPPersonaPredioPorId(Long id);

    /**
     * Metodo que recibe una direccion y la retorna normalizada conforme a las covenciones
     *
     * @author david.cifuentes
     * @param direccion
     * @return direccion normalizada
     */
    public String normalizarDireccion(String direccion);

    /**
     * Metodo que consulta pPredio pero lo trae con todas sus colecciones dependientes
     *
     * @throws Exception
     * @author fredy.wilches
     */
    public PPredio obtenerPPredioCompletoById(Long idPredio) throws Exception;

    /**
     * Metodo que consulta pPredio pero lo trae con todas sus colecciones dependientes
     *
     * @throws Exception
     * @author fredy.wilches
     */
    public PPredio obtenerPPredioCompletoByIdyTramite(Long idPredio, Long idTramite) throws
        Exception;

    /**
     * @author fredy.wilches Metodo que consulta pPredio, buscando por el id del tramite pero lo
     * trae con todas sus colecciones dependientes. A usarse unicamente en quinta nuevo
     * @throws Exception
     */
    public PPredio obtenerPPredioCompletoByIdTramite(Long idTramite) throws Exception;

    /**
     * Elimina un objeto de proyección según las condiciones específicas. Cambia el estado del
     * objeto de proyección de acuerdo a las reglas de negocio.
     *
     * @param objP
     * @return Retorna el objeto actualizado según se requiera
     * @author fabio.navarrete
     */
    //public Object eliminarProyeccion(UsuarioDTO usuario, Object objP);
    /**
     * Elimina un objeto de proyección según las condiciones específicas. Cambia el estado del
     * objeto de proyección de acuerdo a las reglas de negocio.
     *
     * @param objP
     * @return Retorna el objeto actualizado según se requiera
     * @author fabio.navarrete
     */
    public IProyeccionObject eliminarProyeccion(UsuarioDTO usuario, IProyeccionObject objP);

    /**
     * Método que guarda una proyección de manera que de a cuerdo a su estado lo actualiza para
     * ponerlo acorde a las reglas de negocio.
     *
     * @param objP
     * @return
     * @author fabio.navarrete
     */
    public Object guardarProyeccion(UsuarioDTO usuario, IProyeccionObject objP);

    /**
     * Método que retorna un objeto PUnidadConstruccion a partir de su ID
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public IModeloUnidadConstruccion obtenerPUnidadConstruccionPorId(Long id);

    /**
     * Método que almacena o actualiza un objeto de tipo PPredioAvaluoCatastral
     *
     * @param ppaval
     * @return
     * @author fabio.navarrete
     */
    public PPredioAvaluoCatastral actualizarPPredioAvaluoCatastral(
        PPredioAvaluoCatastral ppaval);

    /**
     * Eliminar un avaluo de un ppredio
     *
     * @param avaluo
     */
    public void eliminarPPredioAvaluoCatastral(PPredioAvaluoCatastral avaluo);

    /**
     * Metodo que permite almacenar una PFoto en la base de datos
     *
     * @author juan.agudelo
     * @param pFoto
     */
    //public void guardarPFoto(PFoto pFoto, String archivo);
    /**
     * Metodo que almacena el documento, y pfoto en la BD y el archivo lo sube a alfresco.
     *
     * @author fredy.wilches
     * @param pFoto
     * @param usuario
     */
    /*
     * @modified pedro.garcia 08-05-2013 el parámetro PUnidadConstruccionDto no se usaba!!
     */
    public PFoto guardarPFoto(PFoto pFoto, UsuarioDTO usuario);

    /**
     * Obtiene el registro de la tabla TIPIFICACION_UNIDAD_CONST que corresponda a los puntos dados
     *
     * @author pedro.garcia
     * @param puntos
     * @return
     */
    public TipificacionUnidadConst obtenerTipificacionUnidadConstrPorPuntos(int puntos);

    /**
     * Inserta nuevos registros en la tabla P_UNIDAD_CONSTRUCCION_COMP
     *
     * @author pedro.garcia
     * @param newUnidadesConstruccionComp Lista de objetos que se van a insertar
     */
    public List<PUnidadConstruccionComp> insertarPUnidadConstruccionCompRegistros(
        List<PUnidadConstruccionComp> newUnidadesConstruccionComp);

    /**
     * Recupera la lista de fotos asociadas a un predio OJO: NO carga los archivos de las fotos del
     * gestor documental
     *
     * @author juan.agudelo
     * @param predioId
     * @return
     */
    public List<Foto> buscarFotografiasPredio(Long predioId);

    /**
     * Retorna la lista de registros de la tabla CALIFICACION_ANEXO donde el id del uso de
     * construcción sea el dado
     *
     * @author pedro.garcia
     * @param selectedUsoUnidadConstruccionId
     * @return
     */
    public List<CalificacionAnexo> obtenerCalificacionesAnexoPorIdUsoConstruccion(
        Long selectedUsoUnidadConstruccionId);

    /**
     * Actualiza los datos de la tabla P_UNIDAD_CONSTRUCCION_COMP para la unidad de construcción que
     * viene como atributo de los objetos de la lista de objetos a insertar. La actualización
     * consiste en borrar todos los registros anteriores que estén relacionados con esa unidad de
     * construcción e insertar los nuevos. Se borran, y no se actualizan, porque es más fácil debido
     * a la forma como se arma el árbol de componentes-elementos-detalles
     *
     * @author pedro.garcia
     * @param unidadesConstruccionComp Lista de objetos que se van a actualizar
     */
    public List<PUnidadConstruccionComp> actualizarPUnidadConstruccionCompRegistros(
        List<PUnidadConstruccionComp> unidadesConstruccionComp);

    /**
     * Retorna un predio por el identificador del predio especificado y además obtiene los datos de
     * la tabla predio_avaluo_catastral asociados a éste. Sólo debe usarse cuando REALMENTE se
     * requiera inicializar los datos predioAvaluoCatastral asociados al predio.
     *
     * @param numPredial número predial del predio a obtener
     * @return
     * @author fabio.navarrete
     */
    public Predio obtenerPredioConAvaluosPorId(Long predioId);

    /**
     * Elimina los registros de la tabla P_UNIDAD_CONSTRUCCION_COMP que correspondan a la unidad de
     * construcción con el id suministrado
     *
     * @author pedro.garcia
     * @param unidadConstruccionId
     */
    public void borrarPUnidadConstruccionCompRegistros(Long unidadConstruccionId);

    /**
     * Método encargado de buscar los predios proyectados asociados a un trámite específico
     *
     * @param tramiteId Id del trámite al que se desean buscar los predios proyectados asociados.
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> buscarPPrediosPorTramiteId(Long tramiteId);

    /**
     * Retorna un predio por el identificador del predio especificado y además obtiene los datos de
     * la tabla persona_predio asociados a éste. Sólo debe usarse cuando REALMENTE se requiera
     * inicializar los datos personaPredio asociados al predio.
     *
     * @param predioId
     * @return
     */
    public Predio obtenerPredioConPersonasPorPredioId(Long predioId);

    /**
     * Metodo para guardar una PFoto y un Documento vinculados
     *
     * @author juan.agudelo
     * @param pFotosList
     */
    public void guardarPFotos(List<PFoto> pFotosList);

    /**
     * busca los objetos PFoto relacionados con una unidad de construcción, y, si la bandera lo
     * indica mueve los archivos correspondientes del gestor documental a la carpeta web temporal
     * (para poder ser visualizados)
     *
     * @author pedro.garcia
     * @param unidadConstruccionId
     * @param cargarEnWebTemporal si es true, se copian los archivos del gestor documental a la
     * carpeta web temporal
     * @return
     */
    public List<PFoto> buscarPFotosUnidadConstruccion(Long unidadConstruccionId,
        boolean cargarEnWebTemporal);

    /**
     * Metodo que retorna las imagenes antes y despues del tramite
     *
     * @param t
     * @return
     */
    public List<Documento> buscarImagenesTramite(Tramite t);

    /**
     * Método que busca los objetos {@link PModeloConstruccionFoto} relacionados con un
     * {@link PFmModeloConstruccion}.
     *
     * @author david.cifuentes
     * @param unidadDelModeloId id de la {@link PFmModeloConstruccion}
     * @param cargarEnWebTemporal Variable {@link Boolean} para cargar o no las fotos en la carpeta
     * web temporal
     * @return
     */
    public List<PModeloConstruccionFoto> buscarPModeloConstruccionFotosPorIdUnidad(
        Long unidadDelModeloId, boolean cargarEnWebTemporal);

    /**
     * Actualiza los registros de la tabla PFoto asociados a una unidad de construcción. Lo que hace
     * es borrar los registros existentes e insertar los nuevos. El id de la unidad de construcción
     * viene como atributo de los objetos de la lista.
     *
     * @author pedro.garcia
     * @param pFotosList
     */
    public void actualizarPFotosUnidadConstruccion(List<PFoto> pFotosList);

    /**
     * Cuenta una lista de prediosBloqueo por número predial
     *
     * @author juan.agudelo
     * @param numeroPredial
     * @return
     */
    public Integer contarPrediosBloqueoByNumeroPredial(String numeroPredial);

    /**
     * Busca los bloqueos asociados a un predio por numero predial
     *
     * @author juan.agudelo
     * @param numeroPredial
     * @return
     */
    public List<PredioBloqueo> buscarPrediosBloqueoByNumeroPredial(
        String numeroPredial, final int... rowStartIdxAndCount);

    /**
     * Actualiza una lsita de predios a desbloquear
     *
     * @author juan.agudelo
     * @param usuario
     * @param prediosBloq
     */
    public List<PredioBloqueo> actualizarPrediosDesbloqueo(UsuarioDTO usuario,
        List<PredioBloqueo> prediosBloq);

    /**
     * Busca una persona bloqueada por número de cedula
     *
     * @author juan.agudelo
     * @param numeroCedula
     * @return
     */
    public List<PersonaBloqueo> buscarPersonaBloqueoByNumeroIdentificacion(
        String numeroIdentificacion, String tipoIdentificacion);

    /**
     * Busca una persona por primer nombre, segundo nombre, primer apellido, segundo apellido, tipo
     * documento, número documento y DV
     *
     * @author juan.agudelo
     * @param personaDatos
     * @return
     */
    public Persona buscarPersonaByNombreDocumentoDV(Persona personaDatos);

    /**
     * Método que sirve para desbloquear una persona
     *
     * @author juan.agudelo
     * @param usuario
     * @param personaBloqueada
     * @return
     */
    public List<PersonaBloqueo> actualizarPersonaDesbloqueo(UsuarioDTO usuario,
        List<PersonaBloqueo> personaBloqueada);

    /**
     * Actualiza una lista de predios bloqueados solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public List<PredioBloqueo> actualizarPrediosBloqueoM(UsuarioDTO usuario,
        ArrayList<PredioBloqueo> bloqueoMasivo);

    /**
     * Actualiza una lista de personas bloqueadas solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public List<PersonaBloqueo> actualizarPersonaBloqueoM(UsuarioDTO usuario,
        ArrayList<PersonaBloqueo> bloqueoMasivo);

    /**
     * Método que consulta los usos de la construcción dependiendo de los números prediales que se
     * le envien como parametro
     *
     * @author javier.aponte
     * @param numerosPrediales
     * @return
     */
    public List<UsoConstruccion> buscarUsosConstruccionPorNumerosPredialesPredio(
        List<String> numerosPrediales);

    /**
     * Metodo que recupera un predio a partir del numero predial busqueda no nativa
     *
     * @param numeroPredial
     * @return
     */
    public Predio getPredioPorNumeroPredial(String numeroPredial);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPorTramiteId(Long tramiteId);

    /**
     * Método que recupera una lista de {@link Predio} basados en una lista de id's de trámites.
     *
     * @author david.cifuentes
     * @param idsTramites
     * @return
     */
    public List<Predio> buscarPrediosPorListaDeIdsTramite(List<Long> idsTramites);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle del avalúo
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelAvaluoPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle justificación de propiedad
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelJPropiedadPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle propietarios y / o poseedores
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelPropietariosPorTramiteId(
        Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle ficha matriz unicamente si el tramite es una mutación de
     * segunda y desenglobe
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelFichaMatrizPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle ficha matriz unicamente si el tramite es una mutación de
     * segunda y desenglobe
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     * @deprecated a la fecha 23-08-2012 no se usa. No usar al menos que en realidad se necesite
     * traer esa información de varios PPredios al mismo tiempo (en tal caso quitar la anotación
     * @deprecated)
     */
    public List<PPredio> buscarPPrediosPanelFichaMatrizPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios iniciales asociados a un trámite con los datos
     * correspondientes al modulo detalle ubicación predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de ppredios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle del avalúo
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     * @deprecated a la fecha 23-08-2012 no se usa. No usar al menos que en realidad se necesite
     * traer esa información de varios PPredios al mismo tiempo (en tal caso quitar la anotación
     * @deprecated)
     */
    public List<PPredio> buscarPPrediosPanelAvaluoPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de ppredios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle justificación de propiedad
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     * @deprecated a la fecha 23-08-2012 no se usa. No usar al menos que en realidad se necesite
     * traer esa información de varios PPredios al mismo tiempo (en tal caso quitar la anotación
     * @deprecated)
     */
    public List<PPredio> buscarPPrediosPanelJPropiedadPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de ppredios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle propietarios y / o poseedores
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     * @deprecated a la fecha 23-08-2012 no se usa. No usar al menos que en realidad se necesite
     * traer esa información de varios PPredios al mismo tiempo (en tal caso quitar la anotación
     * @deprecated)
     */
    public List<PPredio> buscarPPrediosPanelPropietariosPorTramiteId(Long tramiteId);

    /**
     * Método que recupera la lista de predios proyectados asociados a un trámite con los datos
     * correspondientes al modulo detalle ubicación predio
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     * @deprecated a la fecha 22-08-2012 no se usa. No usar al menos que en realidad se necesite
     * traer esa información de varios PPredios al mismo tiempo (en tal caso quitar la anotación
     * @deprecated)
     */
    public List<PPredio> buscarPPrediosPanelUbicacionPredioPorTramiteId(Long tramiteId);

    /**
     * Retorna los predios segun valores del codigo catastral (Para el visor)
     *
     * @author franz.gamba
     */
    public List<Predio> getPrediosPorCodigo(String codigo);

    /**
     * Método que permite buscar una persona por personaId
     *
     * @author juan.agudelo
     * @param personaId
     * @return
     */
    public Persona buscarPersonasById(Long personaId);

    /**
     * Retorna los predios segun municipio y matricula inmmobiliaria
     *
     * @author franz.gamba
     */
    public List<Predio> buscarPrediosPorMunicipioYMatricula(String municipioCod, String matriculaInm);

    /**
     * Retorna los predios segun municipio y direccion
     *
     * @author franz.gamba
     */
    public List<Predio> buscarPrediosPorMunicipioYDireccion(String municipioCod, String direccion);

    /**
     * Método que retorna una lista de objetos de tipo PPersona según su número de identificación y
     * tipo de documento. La búsqueda es realizada en la tabla Persona pues en las proyecciones no
     * se tiene todo el listado de personas del sistema
     *
     * @param numeroIdentificacion
     * @param tipoIdentificacion
     * @return
     * @author fabio.navarrete
     */
    public List<PPersona> buscarPPersonasPorTipoNumeroIdentificacion(
        String numeroIdentificacion, String tipoIdentificacion);

    /**
     * Método encargado de eliminar una lista de PPersonaPredio
     *
     * @param pPersonaPredios
     * @author fabio.navarrete
     */
    public void eliminarPPersonaPredios(List<PPersonaPredio> pPersonaPredios);

    /**
     * Método encargado de guardar o actualizar un objeto de tipo PPredio
     *
     * @param pPredio
     * @return
     * @author fabio.navarrete
     */
    public PPredio guardarActualizarPPredio(PPredio pPredio);

    /**
     * Elimina un objeto de tipo PPersonaPredio
     *
     * @param propietarioSeleccionado
     * @author fabio.navarrete
     */
    public void eliminarPPersonaPredio(PPersonaPredio propietarioSeleccionado);

    /**
     * Método encargado de reemplazar los PPersonaPredio asociados a un PPredio
     *
     * @param predioSeleccionado
     * @param propietariosNuevos
     * @return
     * @author fabio.navarrete
     */
    public PPredio reemplazarPropietarios(PPredio predioSeleccionado,
        List<PPersonaPredio> pPersonaPredios);

    /**
     * Método encargado de buscar un objeto de tipo PPersonaPredio por su respectivo id
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public PPersonaPredio buscarPPersonaPredioPorId(Long id);

    /**
     * actualiza los personaPredio con las referencias insertadas a persona predio propiedad y
     * además de esto debe crear los registros para los personaPredioPropiedad inscritos
     *
     * @param personaPredioPropiedadInscritos
     */
    public void proyectarJustificacionDerechoPropiedad(UsuarioDTO usuario,
        List<PPersonaPredioPropiedad> personaPredioPropiedadInscritos);

    /**
     * Método que elimina un {@link PPredioDireccion}
     *
     * @author david.cifuentes
     * @param pPredioDireccion
     */
    public void eliminarPPredioDireccion(PPredioDireccion pPredioDireccion);

    /**
     * Permite recuperar las url de las fotos de un Componente de Construcción esprecífico en un
     * Predio, si no se envía ningun parámetro de Componente de Construcción se retornaran la lista
     * correspondiente a la fachada del predio
     *
     * @author juan.agudelo
     * @param predioId
     * @param componenteConstruccion
     * @return
     */
    public List<String> buscarFotosPredioPorComponenteDeConstruccion(
        Long predioId, String componenteConstruccion);

    /**
     * Método que permite guardar una lista de fotos
     *
     * @author juan.agudelo
     * @param fotos
     * @return
     */
    public List<Foto> guardarFotos(List<Foto> fotos);

    /**
     * Método que guarda o actualiza la proyección de la ficha matriz
     *
     * @param fichaMatriz
     * @author fabio.navarrete
     */
    public PFichaMatriz guardarActualizarPFichaMatriz(PFichaMatriz fichaMatriz);

    /**
     * Método que consulta una proyección de ficha matriz según el id del PPredio al cual está
     * asociada
     *
     * @param id
     * @return
     * @author fabio.navarrete
     */
    public PFichaMatriz buscarPFichaMatrizPorPredioId(Long id);

    /**
     * Método que guarda o actualiza un objeto de tipo PFichaMatrizTorre
     *
     * @param fichaMatrizTorreSeleccionada
     * @author fabio.navarrete
     */
    public PFichaMatrizTorre guardarActualizarPFichaMatrizTorre(
        PFichaMatrizTorre pFichaMatrizTorre);

    /**
     * Método que guarda o actualiza una lista de objetos PFichaMatrizTorre
     *
     * @param pFichaMatrizTorres
     * @return
     * @author fabio.navarrete
     */
    public List<PFichaMatrizTorre> guardarActualizarPFichaMatrizTorres(
        List<PFichaMatrizTorre> pFichaMatrizTorres);

    /**
     * Método que guarda un {@link PPredioDireccion}
     *
     * @author david.cifuentes
     * @param nuevaDireccion
     * @return
     */
    public PPredioDireccion guardarPPredioDireccion(PPredioDireccion direccion);

    /**
     * Método que busca las zonas homogéneas de tipo {@link PPredioZona} de un {@link PPredio} por
     * su id.
     *
     * @author david.cifuentes
     * @param idPPredio
     * @return
     */
    public List<PPredioZona> buscarZonasHomogeneasPorPPredioId(Long idPPredio);

    /**
     * Consulta zonas pr predioId y por estado cancela inscribe
     *
     * @param idPPredio
     * @param estado
     * @return
     */
    public List<PPredioZona> buscarZonasHomogeneasPorPPredioIdYEstadoP(Long idPPredio, String estado);

    /**
     * Metodo que retorna una lista de predios correspondientes a un arreglo de Strings con numeros
     * prediales
     *
     * @param numPrediales
     * @author franz.gamba
     */
    public List<Predio> buscarPrediosPorNumerosPrediales(String numPrediales);

    /**
     * Metodo que retorna una lista de predios correspondientes a un arreglo de Strings con numeros
     * prediales y a su ves retorna la lista de predios que no tienen informacion
     *
     * @param numPrediales
     * @author franz.gamba
     */
    public ListDetallesVisorVO buscarPrediosPorNumerosPredialesVisor(String numPrediales);

    /**
     * Método encargado de eliminar una ficha matriz torre de la base de datos.
     *
     * @param fichaMatrizTorreSeleccionada
     * @author fabio.navarrete
     */
    public void eliminarPFichaMatrizTorre(
        PFichaMatrizTorre pFichaMatrizTorre);

    /**
     * Método encargado para eliminar {@link PFichaMatrizPredio} de la base de datos.
     *
     * @author felipe.cadena
     * @param pFichaMatrizPredio
     *
     */
    public void eliminarPFichaMatrizPredio(
        PFichaMatrizPredio pFichaMatrizPredio);

    /**
     * Metodo que retorna las estadisticas por concectracion de propiedades segun los valores que
     * sean ingresados como criterios
     *
     * @param departamentoCodigo
     * @param municipioCodigo
     * @param zonaUnicacOrganica
     * @return
     * @author franz.gamba
     */
    public List<EstadisticasRegistroVO> obtenerEstadisticasConcentracionPropiedad(
        String departamentoCodigo, String municipioCodigo, String zonaUnicacOrganica);

    /**
     * Retorna una lista de HPersonaPredioPropiedads relacionados con el Id de predio ingresado
     *
     * @param predioId
     * @return
     */
    public List<HPersonaPredioPropiedad> buscarHPersonaPredioPropiedadPorPredioId(
        Long predioId);

    /**
     * Método que actualiza la {@link PFichaMatriz}.
     *
     * @author david.cifuentes
     * @param fichaMatriz
     * @return
     */
    public PFichaMatriz actualizarFichaMatriz(PFichaMatriz fichaMatriz);

    /**
     * Método que actualiza la lista de direcciones de un {@link PPredio}
     *
     * @author david.cifuentes
     * @param pPredioDirecciones
     * @return
     */
    public List<PPredioDireccion> actualizarPPredioDirecciones(
        List<PPredioDireccion> pPredioDirecciones);

    /**
     * Método que busca el último número predial asignado de una manzana, retornandome el número
     * disponible siguiente.
     *
     * @author david.cifuentes
     * @param numeroPredialManzana
     * @return
     */
    public String buscarUltimoNumeroPredialDisponible(String numeroPredialManzana);

    /**
     * Método que almacena una lista de PPersonaPredios
     *
     * @param asList
     * @return
     */
    public List<PPersonaPredio> guardarActualizarPPersonaPredios(
        List<PPersonaPredio> pPersonaPredios);

    /**
     * Método que busca una lista de PPersonaPredio asociada al id del predio ingresado
     *
     * @param id
     * @return
     * @author fabio.navarrete
     *
     * @deprecated por aparente no uso
     */
    @Deprecated
    public List<PPersonaPredio> buscarPPersonaPrediosPorPredioId(Long idPredio);

    /**
     * Método usado para almacenar las justificaciones de derecho de propiedad desde la pantalla de
     * proyecciones
     *
     * @param JustificacionPropiedadDTO
     * @param propietariosNuevosSeleccionados
     * @param usuario
     * @return
     * @author fredy.wilches
     */
    public JustificacionPropiedadDTO guardarJustificacionesPropiedad(
        JustificacionPropiedadDTO JustificacionPropiedadDTO,
        PPersonaPredio[] propietariosNuevosSeleccionados,
        UsuarioDTO usuario);

    /**
     * Método usado para guardar o actualizar un objeto de tipo PPersonaPredio
     *
     * @param pPersonaPredio
     * @return
     * @author fabio.navarrete
     */
    public PPersonaPredio guardarActualizarPPersonaPredio(
        PPersonaPredio pPersonaPredio);

    /**
     * Método encargado de guardar o actualizar un objeto de tipo PPersonaPredioPropiedad
     *
     * @param pPersonaPredioPropiedad
     * @return
     * @author fabio.navarrete
     */
    public PPersonaPredioPropiedad guardarActualizarPPersonaPredioPropiedad(
        PPersonaPredioPropiedad pPersonaPredioPropiedad);

    /**
     * Método que elimina un objeto PPersonaPredioPropiedad de la base de datos.
     *
     * @param personaPredioPropiedadSeleccionado
     */
    public void eliminarPPersonaPredioPropiedad(
        PPersonaPredioPropiedad pPersonaPredioPropiedad);

    /**
     * Método que actualiza un arreglo de {@link PFichaMatrizPredio de la {@link PFichaMatriz}.
     * @author david.cifuentes
     *
     * @param prediosFichaMatriz
     */
    public void actualizarPFichaMatrizPredios(PFichaMatrizPredio[] prediosFichaMatriz);

    /**
     * Método que guarda una lista de {@link PFichaMatrizPredio} de la {@link PFichaMatriz}.
     *
     * @author david.cifuentes
     * @param prediosFichaMatriz
     * @return
     */
    public List<PFichaMatrizPredio> guardarPrediosFichaMatriz(
        List<PFichaMatrizPredio> prediosFichaMatriz);

    /**
     * Método que guarda una lista de {@link PPredio}.
     *
     * @author david.cifuentes
     * @param pPredios
     * @return
     */
    public List<PPredio> guardarPPredios(List<PPredio> pPredios);

    /**
     * Método que elimina una lista de {@link PFichaMatrizPredio} de la {@link PFichaMatriz}.
     *
     * @author david.cifuentes
     * @param prediosFichaMatriz
     * @return
     */
    public void eliminarPrediosFichaMatriz(List<PFichaMatrizPredio> prediosFichaMatriz);

    /**
     * Método que retorna un arreglo de tipo double con los datos del área total de terreno
     * catastral y el área total construida
     *
     * @param numeroPredial
     * @author javier.aponte
     * @return
     */
    public double[] getAreasCatastralesPorNumeroPredial(String numeroPredial);

    /**
     * Retorna una lista de predios con los campos requeridos para la proyección a partir de una
     * lista de Ids ingresada
     *
     * @param predioIds
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> obtenerPPrediosCompletosPorIds(List<Long> predioIds);

    /**
     * Envía una actividad al coordinador teniendo en cuenta validaciones de lógica de negocio
     *
     * @author juan.mendez
     * @param idActividad
     * @param usuario
     */
    public void enviarACoordinador(Actividad actividad, UsuarioDTO usuario);

    /**
     * Envia una actividad a Generar Resolucion, teniendo en cuenta que es el usuario vigente, y que
     * puede ser Responsable de conservacion o Director Territorial
     *
     * @author fredy.wilches
     * @param actividad
     * @param usuario
     */
    public void enviarAGenerarResolucion(Actividad actividad, UsuarioDTO usuario);

    /**
     * Envía la actividad a modificar informacion geográfica en validación
     *
     * @author franz.gamba
     * @param actividad
     * @param usuario
     */
    public void enviarAModificacionInformacionGeografica(Actividad actividad, UsuarioDTO usuario);

    /**
     * Envía la actividad a modificar informacion geográfica sin utilizar la cola de Jboss, la tarea
     * se mueve al usuario tiempos muertos mietras se genera la copia de la base de datos
     * geografica.
     *
     * @author felipe.cadena
     * @param actividad
     * @param usuario
     */
    public void enviarAModificacionInformacionGeograficaSincronica(Actividad actividad,
        UsuarioDTO usuario);

    /**
     * Envía la actividad a revisar proyecciíon de resolución en validación
     *
     * @author franz.gamba
     * @param actividad
     * @param usuario
     */
    public void enviarARevisionProyeccionDeResolucion(Actividad actividad, UsuarioDTO usuario);

    /**
     * Método que elimina una {@link PReferenciaCartografica} de un {@link PPredio}.
     *
     * @author david.cifuentes
     * @param referenciaCartografica
     */
    public void eliminarReferenciaCartografica(PReferenciaCartografica referenciaCartografica);

    /**
     * Método que guarda una {@link PReferenciaCartografica}.
     *
     * @author david.cifuentes
     * @param referenciaCartografica
     * @return
     */
    public PReferenciaCartografica guardarPReferenciaCartografica(
        PReferenciaCartografica referenciaCartografica);

    /**
     * Método que permite recuperar la información de una persona por el número y tipo de
     * identificación
     *
     * @author juan.agudelo
     * @param tipoIdentificacion
     * @param numeroIdentificacion
     * @return
     */
    public List<Persona> buscarPersonaPorNumeroYTipoIdentificacion(
        String tipoIdentificacion, String numeroIdentificacion);

    /**
     * Actualiza una lista de personas bloqueadas solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public ResultadoBloqueoMasivo bloquearMasivamentePersonas(
        UsuarioDTO usuario, ArrayList<PersonaBloqueo> bloqueoMasivo);

    /**
     * Actualiza una lista de predios bloqueados solo si el usuario es el jefe de conservación y si
     * se carga desde un archivo de bloquéo masivo
     *
     * @author juan.agudelo
     * @param usuario
     * @param bloqueoMasivo
     */
    public ResultadoBloqueoMasivo bloquearMasivamentePredios(
        UsuarioDTO usuario, ArrayList<PredioBloqueo> bloqueoMasivo);

    /**
     * Metodo que consulta pPredios NO CANCELADOS asociados a un trámite, los trae con todas sus
     * colecciones dependientes
     *
     * @param idTramite
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> buscarPPrediosCompletosPorTramiteId(Long idTramite);

    /**
     * Método que elimina una lista de objetos de proyección.
     *
     * @param usuarioDto
     * @param prediosResultantesSeleccionados
     * @author fabio.navarrete
     */
    public void eliminarProyecciones(UsuarioDTO usuarioDto,
        List<IProyeccionObject> proyeccionObjects);

    /**
     * Método que busca predios por FiltroDatosConsultaPrediosBloqueo
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaPredioBloqueo
     * @return
     */
    public List<Predio> buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroDatosConsultaPredioBloqueo);

    /**
     * Método que almacena un objeto de tipo PPersonaPredio. Almacena primero el objeto PPersona y
     * luego el objeto PPersonaPredio
     *
     * @param usuario
     * @param pPersonaPredio
     * @return
     * @author fabio.navarrete
     */
    public PPersonaPredio guardarPPersonaPredio(UsuarioDTO usuario,
        PPersonaPredio pPersonaPredio);

    /**
     * Método que permite buscar una persona por tipo de documento de identificación, número de
     * identificación, primer nombre, segundo nombre, primer apellido y segundo apellido, soporta
     * campos nulos exceptuando el tipo de documento de identificación
     *
     * @author juan.agudelo
     * @param persona
     * @return
     */
    public Persona buscarPersonaPorNombrePartesTipoDocNumDoc(
        Persona persona);

    /**
     * Método encargado de traer la lista de pisos según el edificio dado. El método retorna datos
     * si el predio de dicho terreno tiene condición de propiedad Propiedad Horizontal PH
     *
     * @param numeroPredial
     * @return
     * @author fabio.navarrete
     */
    public List<String> buscarPisosEdificio(String numeroPredial);

    /**
     * Temporal por datos migrados de cobol Método que permite buscar una persona por tipo de
     * documento de identificación, número de identificación, primer nombre, segundo nombre, primer
     * apellido y segundo apellido, soporta campos nulos exceptuando el tipo de documento de
     * identificación
     *
     * @author juan.agudelo
     * @param persona
     * @return
     */
    public List<Persona> buscarPersonaPorNombrePartesTipoDocNumDocTemp(
        Persona persona);

    /**
     * Método encargado de traer la lista de terrenos(Predios) según la manzana o vereda ingresada.
     *
     * @param numeroPredial
     * @return
     * @author fabio.navarrete
     */
    public List<String> buscarTerrenosManzana(String numeroPredial);

    /**
     * Método encargado de traer la lista de edificios según el terreno (Predio) dado. El método
     * retorna datos si el predio de dicho terreno tiene condición de propiedad Propiedad Horizontal
     * PH
     *
     * @param numeroPredial
     * @return
     * @author fabio.navarrete
     */
    public List<String> buscarEdificiosTerreno(String numeroPredial);

    /**
     * Retorna una lista de predios con los campos requeridos para la proyección a partir de una
     * lista de Ids ingresada
     *
     * @param predioIds
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> obtenerPPrediosCompletosPorIdsPHCondominio(
        List<Long> predioIds);

    /**
     * Metodo que consulta pPredios NO CANCELADOS de PH y Condominio asociados a un trámite, los
     * trae con todas sus colecciones dependientes
     *
     * @param idTramite
     * @return
     * @author fabio.navarrete
     */
    public List<PPredio> buscarPPrediosCompletosPorTramiteIdPHCondominio(Long idTramite);

    /**
     * Método que busca predios en un rango por FiltroDatosConsultaPrediosBloqueo inicial y final
     *
     * @author juan.agudelo
     * @param filtroCPredioB
     * @param filtroFinalCPredioB
     * @return
     */
    public List<Predio> buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB,
        FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB);

    /**
     * Método que almacena un PPredio y a continuación la ficha matriz asociada a él.
     *
     * @param pPredio
     * @param usuario
     * @return
     * @author fabio.navarrete
     */
    public PFichaMatriz guardarPPredioYFichaMatriz(PPredio pPredio,
        UsuarioDTO usuario);

    /**
     * Método que realiza el conteo de la cantidad de predios nacionales
     *
     * @author david.cifuentes
     * @return
     */
    public Long contarPrediosNacionales();

    /**
     * Busca un predio dado su id haciendo fetch de los datos relacionados con la ubicación del
     * predio (pestaña "ubicación" en detalles del predio)
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId id del predio buscado
     * @return
     */
    public Predio obtenerPredioConDatosUbicacionPorId(Long predioId);

    /**
     * Retorna la lista de pPersonaPredioPropiedad de un PPersonaPredio.
     *
     * @author fredy.wilches
     * @param PPersonaPredio
     * @return
     */
    public List<PPersonaPredioPropiedad> buscarPPersonaPredioPropiedadPorPPersonaPredio(
        PPersonaPredio pp);

    /**
     * Hace la búsqueda de los Predios que cumplen con los criterios de búsqueda dados
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param datosConsultaPredio Objeto creado con los criterios de búsqueda dados por el usuario
     * @param idPrimeraFilaYCuantos parámetros opcionales para restringir el número de filas
     * resultado
     * @return Lista de arreglos de objetos donde cada objeto uno es el nombre del departamento, del
     * municipio, y los datos del predio como tal
     */
    public List<Object[]> buscarPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, int... idPrimeraFilaYCuantos);

    /**
     * Hace la búsqueda de los Predios que cumplen con los criterios de búsqueda dados
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param datosConsultaPredio Objeto creado con los criterios de búsqueda dados por el usuario
     * @param idPrimeraFilaYCuantos parámetros opcionales para restringir el número de filas
     * resultado
     * @return el número de registros que cumplen con las condiciones dadas
     */
    public int contarPredios(
        FiltroDatosConsultaPredio datosConsultaPredio, int... idPrimeraFilaYCuantos);

    /**
     * Obtiene la lista de elementos de la tabla TIPIFICACION_UNIDAD_CONST
     *
     * @author pedro.garcia
     * @return vacía si no encuentra.
     * @version 2.0
     */
    public List<TipificacionUnidadConst> obtenerTipificacionesUnidadConstruccion();

    /**
     * Método que busca un modelo de construcción por su id.
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    public PFichaMatrizModelo cargarModeloDeConstruccion(Long pFichaMatrizModeloId);

    /**
     * Método que guarda un {@link PFichaMatrizModelo} (modelo de construcción)
     *
     * @author david.cifuentes
     * @param pFichaMatrizModelo
     * @return
     */
    public PFichaMatrizModelo guardarModeloDeConstruccion(PFichaMatrizModelo pFichaMatrizModelo);

    /**
     * Método que guarda los componentes de la unidad del modelo de construcción, es decir una lista
     * de {@link PFmConstruccionComponente}.
     *
     * @author david.cifuentes
     * @param List
     * <PFmConstruccionComponente> Lista de objetos que se van a insertar
     */
    public void insertarPFmConstruccionComponenteRegistros(
        List<PFmConstruccionComponente> newPFmConstruccionComponenteList);

    /**
     * Actualiza los datos de la tabla P_FM_CONSTRUCCION_COMPONENTE para la unidad del modelo de
     * construcción que viene como atributo de los objetos de la lista de objetos a insertar. La
     * actualización consiste en borrar todos los registros anteriores que estén relacionados con
     * esa unidad de construcción e insertar los nuevos. Se borran, y no se actualizan, porque es
     * más fácil debido a la forma como se arma el árbol de componentes-elementos-detalles
     *
     * Copia del método de pedro.garcia actualizarPUnidadConstruccionCompRegistros ajustado para
     * modelos de construcción.
     *
     * @author david.cifuentes
     * @param List
     * <PFmConstruccionComponente> Lista de objetos que se van a actualizar
     */
    public void actualizarPFmConstruccionComponenteRegistros(
        List<PFmConstruccionComponente> newPFmConstruccionComponenteList);

    /**
     * Elimina los registros de la tabla P_FM_CONSTRUCCION_COMPONENTE que correspondan al
     * PFmModeloConstruccion con el id suministrado.
     *
     * Copia del método de pedro.garcia borrarPUnidadConstruccionCompRegistros ajustado para modelos
     * de construcción.
     *
     * @author david.cifuentes
     * @param pFmModeloConstruccionId
     */
    public void borrarPFmConstruccionComponenteRegistros(
        Long pFmModeloConstruccionId);

    /**
     * Método que permite insertar masivamente en la tabla TmpBloqueoMasivo los ids de los bloqueos
     * masivos
     *
     * @param
     * @author javier.aponte
     */
    public List<TmpBloqueoMasivo> guardarActualizarIdsBloqueoMasivo(
        List<TmpBloqueoMasivo> tmpBloqueoMasivo);

    /**
     * Método que permite borrar masivamente en la tabla TmpBloqueoMasivo los ids de las bloqueos
     * masivos
     *
     * @param
     * @author javier.aponte
     */
    public void borrarIdsBloqueoMasivo(List<TmpBloqueoMasivo> tmpBloqueoMasivo);

    /**
     * Metodo para guardar una lista de {@link PModeloConstruccionFoto} de una unidad de un modelo
     * de construcción.
     *
     * @author david.cifuentes
     * @param listPFotosUnidadDelModelo
     */
    public void guardarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> listPFotosUnidadDelModelo);

    /**
     * Actualiza los registros de la tabla P_MODELO_CONSTRUCCION_FOTO asociados a una unidad de un
     * modelo de construcción. Lo que hace es borrar los registros existentes e insertar los nuevos.
     *
     * Copia del método de pedro.garcia actualizarPFotosUnidadConstruccion ajustado para modelos de
     * construcción.
     *
     * @author david.cifuentes
     * @param pModeloConstruccionFotoList
     */
    public void actualizarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> pModeloConstruccionFotoList);

    /**
     * Metodo que permite almacenar un {@link PModeloConstruccionFoto} asociados a una unidad de un
     * modelo de construcción.
     *
     * Copia del método guardarPFoto ajustado para modelos de construcción.
     *
     * @author david.cifuentes
     * @param pModeloConstruccionFoto
     * @return
     */
    /*
     * @modified pedro.garcia 08-05-2013 eliminación de parámetro no usado
     */
    public PModeloConstruccionFoto guardarPModeloConstruccionFoto(
        PModeloConstruccionFoto pModeloConstruccionFoto, UsuarioDTO usuario);

    /**
     * Metodo que busca un {@link PFmModeloConstruccion} por su id.
     *
     * @author david.cifuentes
     * @param idPFmModeloConstruccion
     */
    public PFmModeloConstruccion buscarUnidadDelModeloDeConstruccionPorSuId(
        Long idPFmModeloConstruccion);

    /**
     * Método que recupera la lista de predios colindantes de un predio por número predial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param numeroPredial
     * @return
     */
    public List<Predio> buscarPrediosColindantesPorNumeroPredial(
        String numeroPredial);

    /**
     * Método que genera un número de secuencia de la tabla TmpBloqueoMasivo
     *
     * @author javier.aponte
     */
    public BigDecimal generarSecuenciaTmpBloqueoMasivo();

    /**
     * Método que revisa si un predio se encuentra cancelado por predioId o numeroPredial
     *
     * @author juan.agudelo
     * @version 2.0
     * @param predio
     * @return
     */
    public boolean esPredioCanceladoPorPredioIdONumeroPredial(Long predioId,
        String numeroPredial);

    /**
     * Método encargado de buscar el historico de justificaciones de un predio.
     *
     * @author fredy.wilches
     * @version 2.0
     * @param predio Predio asociado a las justificaciones a obtener.
     * @return Retorna la Lista de justificaciones historicas asociadas a un predio
     */
    public List<PredioPropietarioHis> getPredioPropietarioHisByPredio(Long predioId);

    /**
     * Metodo que busca una PUnidadConstruccion por su id.
     *
     * @author david.cifuentes
     * @param idPUnidadConstruccion
     */
    public PUnidadConstruccion buscarUnidadDeConstruccionPorSuId(Long idPUnidadConstruccion);

    /**
     * obtiene la lista de números de terreno que están en una manzana y asociados a las condiciones
     * de propiedad dadas
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param manzana numero predial hasta el componente manzana (17 primeras posiciones del número
     * predial)
     * @param condicionesPropiedad
     * @return
     */
    public List<String> obtenerNumsTerrenoPorCondPropiedadEnManzana(
        String manzana, List<String> condicionesPropiedad);

    /**
     * obtiene la lista de números de edificio o torre que se encuentran asociados al número de
     * terreno dado
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param terreno numero predial hasta el componente terreno (21 primeras posiciones del número
     * predial)
     * @return
     */
    public List<String> obtenerNumsEdificioTorrePorNumeroTerreno(String terreno);

    /**
     * obtiene la lista de números de piso que se encuentran asociados al número de edificio o torre
     * dado
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param edificioTorre numero predial hasta el componente 'edificio o torre' (24 primeras
     * posiciones del número predial)
     *
     * @return
     */
    public List<String> obtenerNumsPisoPorNumeroEdificioTorre(String edificioTorre);

    /**
     * Dice si existe un predio con el número predial dado y que tenga alguno de los estados dados
     *
     * @author pedro.garcia
     * @param numeroPredial
     * @param listaEstados lista de estados con los que se filtra la búsqueda. Si es null, no se
     * filtra por ese criterio
     * @version 2.0
     * @return true si existe al menos un predio con el número predial dado
     */
    public boolean existePredio(String numeroPredial, String... listaEstados);

    /**
     * Busca los predios con el número predial dado sin hacer fetch de nada excepto del departamento
     * y municipio (para mostrar nombres)
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param numeroPredial
     * @param listaEstados opcional.
     * @return
     */
    public List<Predio> buscarPrediosPorNumPredialYEstadosNF(String numeroPredial,
        String... listaEstados);

    /**
     * Llama al procedimiento almacenado que genera un número predial (específicamente el segmento
     * número de unidad) a partir de una cadena que contiene el número predial hasta el segmento
     * 'condición de propiedad'
     *
     * @author pedro.garcia
     * @version 2.0
     * @param npHastaCondProp número predial hasta el segmento 'condición de propiedad'
     * @return cadena con el número predial que contiene el segmento generado 'número de unidad'
     */
    /*
     * @modified pedro.garcia 02-10-2012 Julio había dicho que le enviara hasta 'terreno' y luego
     * dijo que hasta 'condición de propiedad'
     */
    public String generarNumeroUnidadPorTerreno(String npHastaCondProp);

    /**
     * Llama al procedimiento almacenado que genera un número predial (específicamente el segmento
     * número de unidad) a partir de una cadena que contiene el número predial hasta el segmento
     * 'piso'. Dependiendo de si es para una mejora o no se llama a un procedimiento almacenado
     * diferente
     *
     * @author pedro.garcia
     * @version 2.0
     * @param piso número predial hasta el segmento 'piso'
     * @param esMejora define si el número que se va a generar es para una mejora o no.
     * @return cadena con el número predial que contiene el segmento generado 'número de unidad'
     */
    public String generarNumeroUnidadPorPiso(String piso, boolean esMejora);

    /**
     * Método que retorna los ppredios relacionados con un trámite que se Mantienen o se Inscriben
     *
     * @param tramiteId
     * @return
     */
    public List<PPredio> buscarPprediosResultantesPorTramiteId(Long tramiteId);

    /**
     * Método que retorna un predio a partir de su numero predial, con el fin de mostrar los
     * siguientes datos: <br>
     * <ol>
     * <li>Circulo Registral</li>
     * <li>Dirección principal</li>
     * <li>Matricula inmobiliaria</li>
     * <li>Area terreno</li>
     * <li>Area construcción</li>
     * </ol>
     * (Usado en Avaluos Comerciales)
     *
     *
     * @author rodrigo.hernandez
     *
     * @param numeroPredial - Numero Predial
     * @return
     */
    public Predio buscarPredioPorNumeroPredialParaAvaluoComercial(String numeroPredial);

    /**
     * Metodo para obtener un PredioAvaluoCatastral a partir del id (Usado inicialmente en la
     * proyección)
     *
     * @author fredy.wilches
     * @param id
     * @return
     */
    public PredioAvaluoCatastral getPredioAvaluoCatastralById(Long id);

    /**
     * Hace la búsqueda de los Predios que cumplen con los criterios de búsqueda dados
     *
     * @author javier.barajas
     * @version 2.0
     *
     * @param datosConsultaPredio Objeto creado con los criterios de búsqueda dados por el usuario
     * @param idPrimeraFilaYCuantos parámetros opcionales para restringir el número de filas
     * resultado
     * @param datosADesplegar
     * @return Lista de arreglos de objetos donde cada objeto uno es el nombre del departamento, del
     * municipio, y los datos del predio como tal
     */
    public List<Object[]> buscarPrediosCriterios(
        FiltroGenerarReportes datosConsultaPredio, int datosADesplegar,
        int... idPrimeraFilaYCuantos);

    /**
     * Método que realiza la suma de los avalúos de los predios asociados a la ficha matriz.
     *
     * @author david.cifuentes
     *
     * @param idTramite
     * @return
     */
    public Double calcularSumaAvaluosTotalesDePrediosFichaMatriz(Long idTramite);

    /**
     * Método que retorna una persona por tipo y numero de identificación
     *
     * @author franz.gamba
     * @param tipoIdentificacion
     * @param numeroIdentificacion
     * @return
     */
    public Persona buscarPersonaPorTipoYNumeroDeIdentificacion(String tipoIdentificacion,
        String numeroIdentificacion);

    /**
     * Método que consulta todas las {@link PUnidadConstruccion} asociadas a un {@link PPredio}.
     *
     * @author david.cifuentes
     * @modifiedBy fredy.wilches
     * @param idPPredio
     * @param incluirCanceladas
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionPorPPredioId(Long idPPredio,
        boolean incluirCanceladas);

    /**
     * Busca los PPredio relacionados con un trámite (posiblemente filtrados por el valor del campo
     * 'cancela_inscribe) No hace fetch de nada.
     *
     * @author pedro.garcia
     *
     * @param idTramite
     * @param cmiValuesToUse Lista de valores de 'cancela_inscribe' para filtrar la búsqueda
     * @return
     */
    public List<PPredio> buscarPPrediosPorTramiteId(Long idTramite, List<String> cmiValuesToUse);

    /**
     * Método que obtiene un PPredio con los datos correspondientes al módulo 'detalle ubicación
     * predio'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerPPredioDatosUbicacion(Long ppredioId);

    /**
     * Método que obtiene un PPredio con los datos correspondientes al módulo 'justificación de
     * propiedad'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerPPredioDatosJustificPropiedad(Long ppredioId);

    /**
     * Método que obtiene un PPredio con los datos correspondientes al módulo 'avalúo'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerPPredioDatosAvaluo(Long ppredioId);

    /**
     * Método que obtiene un PPredio con los datos correspondientes al módulo 'propietarios o
     * poseedores'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerPPredioDatosPropietarios(Long ppredioId);

    /**
     * Método que obtiene un PPredio con los datos correspondientes al módulo 'ficha matriz'
     *
     * @author pedro.garcia
     * @param ppredioId
     * @return
     */
    public PPredio obtenerPPredioDatosFichaMatriz(Long ppredioId);

    /**
     * Método que retorna la lista de ppredios asociados a un tramite Nota: usado para actualizacion
     * del avaluo catastral
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<PPredio> obtenerPPrediosPorTramiteIdActAvaluos(Long tramiteId);

    /**
     * Método que retorna los personaPredio asociados a un predio
     *
     * @author franz.gamba
     * @param predioId
     * @return
     */
    public List<PersonaPredio> buscarPersonaPredioPorPredioId(Long predioId);

    /**
     * Método que obtiene los predios colindantes de un predio mediante el servicio WEB Nota: Se usa
     * solo para cuando no se encuentran los colindantes en Base de datos
     *
     * @author franz.gamba
     * @param usuario
     * @param numPredial
     * @return
     */
    public List<Predio> buscarPrediosColindantesPorServicioWeb(UsuarioDTO usuario, String numPredial);

    /**
     * Método para obtener los colindantes de una mejora
     *
     * @author felipe.cadena
     * @param usuario
     * @param numPredial
     * @return
     */
    public List<Predio> buscarPrediosColindantesMejorasPorServicioWeb(UsuarioDTO usuario,
        String numPredial);

    /**
     * Metodo que cuanta cuantos persona predios hay asociados a una persona
     *
     * @param personaId
     * @return
     */
    public Long conteoPersonaPrediosPorPersonaId(Long personaId);

    /**
     * Metodo quie actualiza en la base de datos un PPersona
     *
     * @param persona
     * @return
     */
    public PPersona actualizarPPersona(PPersona persona);

    /**
     * Método encargado de guardar la ficha predial digital generada por el sistema en el alfresco
     *
     * @param documento
     * @param usuario
     * @return
     */
    public Documento guardarFichaPredialGenerada(Documento documento, UsuarioDTO usuario);

    /**
     * Método encargado de guardar el certificado plano predial catastral generado por el sistema en
     * el alfresco
     *
     * @param documento
     * @param usuario
     * @return
     */
    public Documento guardarCertificadoPlanoPredialCatastral(Documento documento, UsuarioDTO usuario);

    /**
     * Método para obtener el xml resultante de la serialización de un predio para dispositivos
     * móviles..
     *
     * @author ariel.ortiz
     * @param idPredio
     */
    public void obtenerXmlPredioPorPredioId(Long idPredio);

    /**
     * Método que retorna las unidades de construccion de un predio por su número predial
     *
     * @param numPred
     * @return
     */
    public List<UnidadConstruccion> obtenerUnidadesConstruccionsPorNumeroPredial(String numPred);

    /**
     * Método que retorna las p unidades de construccion de un predio por su número predial
     *
     * @param numPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstruccionsPorNumeroPredial(String numPred);

    /**
     * Método que actualiza una lista de unidades de construccion
     *
     * @param pUnidadConstruccions
     * @return
     */
    public List<PUnidadConstruccion> guardarListaPUnidadConstruccion(
        List<PUnidadConstruccion> pUnidadConstruccions);

    /**
     * Método que actualiza una lista de {@link PUnidadConstruccionComp} borrando los registros de
     * {@link PUnidadConstruccionComp} anteriores asociados a la unidad de construcción.
     *
     * @author david.cifuentes
     * @param unidadesConstruccionComp
     */
    public List<PUnidadConstruccionComp> actualizarListaUnidadConstruccionComp(
        List<PUnidadConstruccionComp> unidadesConstruccionComp);

    /**
     * Método que elimina las {@link PFoto} asociadas a la PUnidadConstruccion.
     *
     * @author david.cifuentes
     *
     * @param unidadConstruccionId
     */
    public void borrarPFotosDeLaPUnidadConstruccion(Long unidadConstruccionId);

    /**
     * Método que elimina los {@link PModeloConstruccionFoto} asociadas a la
     * {@link PFmModeloConstruccion}.
     *
     * @author david.cifuentes
     *
     * @param pFmModeloConstruccionId
     */
    public void borrarPModeloConstruccionFotosDelPFmModeloConstruccion(
        Long pFmModeloConstruccionId);

    /**
     * Método que obtiene una {@link CalificacionAnexo} por su id.
     *
     * @author david.cifuentes
     * @param calificacionAnexoId
     * @return
     */
    public CalificacionAnexo buscarCalificacionConstruccionPorId(Long calificacionAnexoId);

    /**
     * Método para buscar los Datos fisicos segun criterios de buqueda
     *
     * @author javier.barajas
     *
     * @param List<Predio>, prametros de paginacion
     */
    public List<Object[]> buscarDatosFisicos(List<String> listaPredios, int first, int pageSize);

    /**
     * Método para buscar la Ficha Matriz segun criterios de buqueda
     *
     * @author javier.barajas
     *
     * @param List<Predio>, prametros de paginacion
     */
    public List<Object[]> buscarFichaMatrizReportes(List<String> listaPredios, int first,
        int pageSize);

    /**
     * Método encargado de generar o actualizar la carta catastral urbana a partir de un número
     * predial y la almacena en el gestor documental, si el parámetro de reemplazar está en true,
     * genera la carta para los predios asociados al trámite y reemplaza las cartas que ya existían
     *
     * @param tramiteId
     * @param usuario
     * @param reemplazar indica si se debe reemplazar la carta
     * @return variable booleana que indica si el llamado a generar las imagenes de la carta
     * catastral urbana se realizó correctamente
     * @author javier.aponte
     */
    public boolean actualizarCartaCatastralUrbana(Long tramiteId, UsuarioDTO usuario,
        boolean reemplazar);

    /**
     * Método encargado de generar la carta catastral urbana a partir de los números prediales
     * separados por coma y la almacena en el gestor documental
     *
     * @param numerosPrediales
     * @param usuario
     * @param tramiteId
     * @return variable booleana que indica si el llamado a generar las imagenes de la carta
     * catastral urbana se realizó correctamente
     * @author javier.aponte
     */
    public boolean generarCartaCatastralUrbana(String numerosPrediales, UsuarioDTO usuario,
        Long tramiteId, boolean generarEnPDF);

    /**
     * Método encargado de generar la ficha predial digital a partir de los números prediales
     * separados por coma y la almacena en el gestor documental
     *
     * @param numerosPrediales
     * @param usuario
     * @param tramiteId
     * @return variable booleana que indica si el llamado a generar las imagenes de la ficha predial
     * se realizó correctamente
     * @author javier.aponte
     */
    public boolean generarFichaPredialDigital(String numerosPrediales, UsuarioDTO usuario,
        Long tramiteId);

    /**
     * Método encargado de generar o actualizar la ficha predial digital a partir de un trámite y la
     * almacena en el gestor documental, si el parametro de reemplazar está en true, genera la ficha
     * para los predios asociados al trámite y reemplaza las fichas que ya existían
     *
     * @param tramiteId
     * @param usuario
     * @param reemplazar indica si se debe reemplazar la fiha
     * @return variable booleana que indica si el llamado a generar las imagenes de la ficha predial
     * se realizó correctamente
     * @author javier.aponte
     */
    public boolean actualizarFichaPredialDigital(Long tramiteId, UsuarioDTO usuario,
        boolean reemplazar);

    /**
     * Método que calcula con el procedimineto almacenado el valor por m2 para una zona geoeconomica
     *
     * @author franz.gamba
     * @param zonaCodigo
     * @param destino
     * @param zonaGeoEconomica
     * @param predioId
     * @return
     */
    public Double obtenerValorUnidadTerreno(String zonaCodigo, String destino,
        String zonaGeoEconomica, Long predioId);
    
    /**
     * Método que calcula con el procedimineto almacenado el valor por m2 para una zona geoeconomica
     *
     * @author vsocarras
     * @param zonaCodigo
     * @param destino
     * @param zonaGeoEconomica
     * @param predioId
     * @param vigencia
     * @return
     */
    public Double obtenerValorUnidadTerreno(String zonaCodigo, String destino,
        String zonaGeoEconomica, Long predioId,Timestamp vigencia) ;

    /**
     * Método que obtiene los {@link PPredioAvaluoCatastral} del {@link PPredio}
     *
     * @author david.cifuentes
     * @param pPredioId
     * @return
     */
    public List<PPredioAvaluoCatastral> obtenerListaPPredioAvaluoCatastralPorIdPPredio(
        Long pPredioId);

    /**
     * Metodo para buscar predios para la generacion de formularios SBC
     *
     * @cu 204
     * @author javier.barajas
     * @param String cadena de busqueda
     * @return List<Predio>
     */
    public List<Predio> buscarPredioGenereacionFormularioSBCporCadena(String cadenaBusqueda);

    /**
     * Método que retorna las direccione proyectadas segun el predio id
     *
     * @author franz.gamba
     * @param predioId
     * @return
     */
    public List<PPredioDireccion> obtenerPpredioDirecionesPorPredioId(Long predioId);

    /**
     * Método que busca un documento por el id del predio y el tipo de documento
     *
     * @param predioId
     * @param idTipoDocumento
     * @return documento
     * @version 1.0
     * @author javier.aponte
     */
    public Documento buscarDocumentoPorPredioIdAndTipoDocumento(Long predioId, Long idTipoDocumento);

    /**
     * Método que busca un documento por el número predial y el tipo de documento
     *
     * @param numeroPredial
     * @param idTipoDocumento
     * @return documento
     * @version 1.0
     * @author javier.aponte
     */
    public Documento buscarDocumentoPorNumeroPredialAndTipoDocumento(String numeroPredial,
        Long idTipoDocumento);

    /**
     * Retorna un ppredio por el número predial especificado
     *
     * @param numPredial número predial del ppredio a obtener
     * @return
     * @author javier.aponte
     */
    public PPredio getPPredioByNumeroPredial(String numeroPredial);

    /**
     * Retorna un predio por el id con los datos necesarios para las validaciones de rectificación y
     * complementación
     *
     * @author felipe.cadena
     * @param idPredio
     * @return
     */
    public Predio obtenerPredioValidacionesRectificacionById(Long idPredio);

    /**
     * Método para obtener las fotos relacionadas a una unidad de constuccion de un tipo
     * determinado.
     *
     * @author felipe.cadena
     * @param unidadId
     * @param codigoTipoFoto
     * @return
     */
    public List<Foto> buscarFotoPorIdUnidadConstruccionYTipo(Long unidadId, String codigoTipoFoto);

    /**
     * Borra los registros correspondientes a las PFoto junto con los Documento asociados y los
     * archivos en el gestor documental
     *
     * @author pedro.garcia
     * @param listaPFotosABorrar
     */
    public void borrarPFotos(List<PFoto> listaPFotosABorrar);

    /**
     * Consulta los registros de la tabla Predio_Avaluo_Catastral para el predio dado
     *
     * @author pedro.garcia
     * @param predioId
     * @return
     */
    public List<PredioAvaluoCatastral> obtenerHistoricoAvaluoPredio(Long predioId);

    /**
     * Metodo que guarda en la base de datos la {@link PModeloConstruccionFoto} y sube al
     * repositorio documental la foto asociada.
     *
     * @nota Implementación basada en el método guardarPFoto(Pfoto, UsuarioDTO)
     *
     * @author david.cifuentes
     * @param pModeloConstruccionFoto
     * @param usuario
     */
    public PModeloConstruccionFoto guardarPFotoUnidadDelModelo(
        PModeloConstruccionFoto pModeloConstruccionFoto, UsuarioDTO usuario);

    /**
     * Método que borra los registros correspondientes a las {@link PmodeloConstruccionFoto} junto
     * con sus Documento asociados y los archivos en el gestor documental
     *
     * @nota Implementación basada en el método borrarPFotos(Pfoto, UsuarioDTO)
     *
     * @author david.cifuentes
     *
     * @param lista de {@link PModeloConstruccionFoto} a borrar.
     */
    public void borrarPModeloConstruccionFotos(
        List<PModeloConstruccionFoto> listaPModeloConstFotoABorrar);

    /**
     * Metodo para determinar la colindancia entre un grupo de predios seleccionados con respecto a
     * un predio base.
     *
     * @auhtor felipe.cadena
     * @param predios
     * @param predioBase
     * @param usuario
     * @param validarColindantesWS
     * @return Lista de predios que no son colindantes con el predio base. si es vacia todos los
     * predios enviados son colindadntes directa o indirectamente con el predio base.
     */
    public List<Predio> determinarColindancia(List<Predio> predios, Predio predioBase,
        UsuarioDTO usuario, boolean validarColindantesWS);

    /**
     * Metodo para determinar la colindancia entre un grupo de predios tipo mejora seleccionados
     *
     * @auhtor felipe.cadena
     * @param predios
     * @param usuario
     * @return Lista de predios que no son colindantes entre si. si es vacia todos los predios
     * enviados son colindadntes directa o indirectamente.
     */
    public List<Predio> determinarColindanciaMejoras(List<Predio> predios, UsuarioDTO usuario);

    /**
     * Metodo para reversar la proyeccion de un predio en particular.
     *
     * @auhtor felipe.cadena
     * @param idTramite
     * @param idPredio
     */
    public void reversarProyeccionPredio(Long idTramite, Long idPredio);

    /**
     * Obtiene los predios asociados a un PH.
     *
     * @author felipe.cadena
     * @param terreno numero predial hasta el componente terreno (21 primeras posiciones del número
     * predial)
     * @return
     */
    public List<Predio> obtenerPrediosPH(String terreno);

    /**
     * Obtiene la ficha matriz de origen del predio asociado al número predial
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public FichaMatriz obtenerFichaMatrizOrigen(String numeroPredial);

    /**
     * Método que consulta una lista de tipo PredioInconsistencia por el id del predio
     *
     * @param predioId
     * @return
     * @author javier.aponte
     */
    public List<PredioInconsistencia> obtenerPrediosInconsistenciaPorIdPredio(Long idPredio);

    /**
     * Método para programar la generación de la replica de solo consulta para un trámite. Si la
     * replica ya existe solo avanza proceso
     *
     * @author felipe.cadena
     * @param actividad
     * @param usuario
     * @param responsableSIG - solo se usa para cuando se envia a depuración, puede ser nulo en otro
     * caso.
     * @param depuracion - Determina si el tramite sera enviado a depuraciòn
     */
    public void generarReplicaDeConsultaTramiteYAvanzarProceso(Actividad actividad,
        UsuarioDTO usuario, UsuarioDTO responsableSIG, boolean depuracion);

    /**
     * Metodo encargado de consultar la información para el módulo gerencial en cuanto a predios se
     * refiere
     *
     * @param anio
     * @param codigoDepartamento
     * @param codigoMunicipio
     * @param zona
     * @return
     */
    public List<DivisionAdministrativaDTO> consultaEstadisticaGerencialPredio(Long anio,
        String codigoDepartamento, String codigoMunicipio, String zona);

    /**
     * Consulta de predios por filtro para respuesta servicio web de servicio catastrales.
     *
     * @param filtro filtro construido a partir de la conversion de los datos enviados en el request
     * del servicio web que esta representada por ConsultaIGACParams.java.
     * @return predios resultantes de la consulta de datos.
     *
     * @author andres.eslava
     */
    public List<Predio> buscaPrediosConsultaCatastralWebService(
        FiltroConsultaCatastralWebService filtro, Long numeroPagina) throws Exception;

    /**
     * Método encargado de consultar la información para el módulo gerencial en cuanto a manzanas se
     * refiere
     *
     * @param anio
     * @param codigoMunicipio
     * @param zona
     * @return
     */
    public List<MunicipioManzanaDTO> consultaEstadisticaGerencialManzana(Long anio,
        String codigoMunicipio, String zona);

    /**
     * Método que realiza lo correspondiente a aplicar cambios de la proyección luego de que el
     * resultado de lo que se hace en el arcgis server ha sido obtenido. Por eso se llama AP2,
     * porque es como se hace de modo asincrónico y es la segunda parte del flujo completo.
     *
     * Las actualizaciones de ficha predial digital y carta catastral se siguen ejecutando
     * asincrónicamente.
     *
     * Este método se invoca cuando el cron que revisa la tabla Producto_Catastral_Job encuentra un
     * registro en el que se indica que la operación del arcgis server ya acabó para algún trámite.
     *
     * @author pedro.garcia
     * @param registroTrabajoFinalizado Entidad en la que están los resultados del arcgis server
     * @return true si no ocurrieron errores (al menos en la parte sincrónica)
     */
    public boolean aplicarCambiosDeProyeccionAP2(ProductoCatastralJob registroTrabajoFinalizado);

    /**
     * Método para guardar el documento relacionado a la copia de la base de datos geografica cuando
     * esta se termina de generar
     *
     * @author felipe.cadena
     *
     * @param job
     *
     * @return true -- Si se ejecuto con exito false -- Si existe algun error
     */
    public boolean guardarDocumentoReplica(ProductoCatastralJob job);

    /**
     * Método para guardar el documento relacionado a la copia de la base de datos geografica de
     * solo consulta cuando esta se termina de generar
     *
     * @author felipe.cadena
     *
     * @param job
     *
     * @return true -- Si se ejecuto con exito false -- Si existe algun error
     */
    public boolean guardarDocumentoReplicaConsulta(ProductoCatastralJob job);

    /**
     * Método para reenviar el job de generaciòn de la replica cuando se presento un error en previo
     *
     * @author felipe.cadena
     *
     * @param job
     */
    public void reenviarJobGeneracionReplica(ProductoCatastralJob job);

    /**
     * Método encargado de consultar los reportes control reporte por usuario
     *
     * @param usuarioLogin
     * @return Lista de objetos reporte control reporte
     * @author javier.aponte
     */
    public List<RepControlReporte> buscarReporteControlReportePorUsuario(String usuarioLogin);

    /**
     * Método encargado de contar la cantidad de registros en la tabla reporte datos reporte por id
     * de reporte control reporte
     *
     * @param idReporteControlReporte
     * @return Cantidad de registros en la tabla reporte datos reporte
     * @author javier.aponte
     */
    public Integer contarRegistrosReporteDatosReportePorControlReporteId(Long idControlReporte);

    /**
     * Método para generar la ficha predial digital
     *
     * @author javier.aponte
     *
     * @param job
     * @return boolean que indica si se generó exitosamente la ficha predial digital
     */
    public String procesarFichaPredialDigital(ProductoCatastralJob job, int contador);

    /**
     * Método para generar la carta catastral urbana
     *
     * @author javier.aponte
     * @param job
     * @return boolean que indica si se generó exitosamente la carta catastral urbana
     */
    public String procesarCartaCatastralUrbana(ProductoCatastralJob job, int contador);

    /**
     * Método para guardar el plano predial rural
     *
     * @param ruta ruta en la carpeta local del plano predial rural
     * @author javier.aponte
     * @return boolean que indica si se generó exitosamente el plano predial rural
     */
    public boolean guardarPlanoPredialRural(String rutaArchivo, String numeroManzana,
        String usuarioLogin);

    /**
     * Método para consultar los predios asociados a un grupo de números prediales
     *
     * @author felipe.cadena
     * @param numeros
     * @return
     */
    public List<Predio> obtenerPrediosPorNumerosPrediales(List<String> numeros);

    /**
     * Método que recupera la lista de {@link Predio} iniciales asociados a un trámite con sus
     * listas dependientes.
     *
     * @author david.cifuentes
     * @param tramiteId
     * @return
     */
    public List<Predio> buscarPrediosCompletosPorTramiteId(Long tramiteId);

    /**
     * Método que se ejecuta una vez se ha terminado la sincronización de datos geograficos retorna
     * el usuario de la actividad a la que debe llegar una vez terminada la depuraciòn.
     *
     * @author felipe.cadena
     * @param registroTrabajoFinalizado
     * @return
     */
    public UsuarioDTO finalizarCambiosDepuracion(ProductoCatastralJob registroTrabajoFinalizado);

    /**
     * Método encargado de cambiar el tipo de documento de la copia de bd geográfica original y de
     * la final por copia de bd geográfica original depuración y final depuración
     *
     * @author javier.aponte
     * @param idTramite
     */
    public void cambiarTipoDocumentoCopiaBdGeografica(Long idTramite);

    /**
     * Detemina si un predio tiene mejoras asociadas.
     *
     * @author pedro.garcia
     *
     * @param numeroPredioEnNumeroPredial segmento del número predial que contiene el número de
     * predio
     * @return true si alguno de los predios que compartan el número de predio tiene condición de
     * propiedad 5 o 6
     */
    public boolean tieneMejorasPredio(String numeroPredioEnNumeroPredial);

    /**
     * Retorna la fecha de la ultima vigencia para una zona en particular, retorna null si no existe
     * fecha de actualizaciòn para la zona
     *
     * @author felipe.cadena
     * @param zona
     * @return
     */
    public Date obtenerVigenciaUltimaActualizacionPorZona(String zona);

    /**
     * Retorna un predio por el identificador del predio especificado y además obtiene los datos de
     * la tabla predio_avaluo_catastral asociados a éste. <br />
     * Sólo debe usarse para la consulta del detalle del avalúo en la consulta de detalles del
     * predio.
     *
     * @param predioId id del predio que se consulta
     * @return
     * @author pedro.garcia
     */
    public Predio obtenerPredioConDetalleAvaluoPorId(Long predioId);

    /**
     * Método para copiar del predio actual, los propietarios a todos los predios del desenglobe
     *
     * @param predioOrigen
     * @param predios
     * @param usuario
     */
    public List<PPredio> copiarPropietarios(PPredio predioOrigen, List<PPredio> predios,
        String usuarioLog);

    /**
     * Metodo para obtener las zonas desde el componente geografico, el tramite debe tener la copia
     * de consulta generada para poder obtener las zonas.
     *
     * @author felipe.cadena
     *
     * @param idTramite
     * @param usuario
     * @param sincronico -- Determina si se quiere ejecutar el proceso de forma asincronica (false),
     * en tal caso no retorna resultados
     * @return
     */
    public List<PPredioZona> rectificarZonas(Long idTramite, UsuarioDTO usuario, boolean sincronico);

    /**
     * Elimina la entidad PPredioZona
     *
     * @author felipe.cadena
     * @param pZona
     */
    public void eliminarZona(PPredioZona pZona);

    /**
     * Actualiza una zona determinada
     *
     * @author felipe.cadena
     * @param pZona
     * @return
     */
    public PPredioZona guardarActualizarZona(PPredioZona pZona);

    /**
     * Método que busca las fotos de un predio por el tipo de la foto
     *
     * @param predioId
     * @param fotoTipo
     * @author javier.aponte
     * @return List<Foto>
     */
    public List<Foto> buscarFotografiaPorIdPredioAndFotoTipo(Long predioId, String codigoTipoFoto);

    /**
     * Consulta un avaluo proyectado por id
     *
     * @author felipe.cadena
     * @param id
     * @return
     */
    public PPredioAvaluoCatastral obtenerPPredioAvaluosCatastral(Long id);//felipe.cadena::redmine #7538

    /**
     * Obtiene los jobs asociados al tramite determinado, si los parametros de estao y tipo son
     * nulos se obtendra todos los jobs asociados al tramite
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @param tipo
     * @param estado
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsGeograficosPorTramiteTipoEstado(Long tramiteId,
        String tipo, String estado);

    /**
     * Obtiene las zonas de un predio correspondientes a la ultima vigencia existenete
     *
     * @author felipe.cadena
     * @param predioId
     * @return
     */
    public List<PredioZona> obtenerZonasUltimaVigenciaPorIdPredio(Long predioId);//felipe.cadena::redmine #8490

    /**
     * Obtiene las fichas proyectadas ingresadas al sistema por medio de una opción de menú.
     *
     * @author felipe.cadena
     * @param usuario
     * @return
     */
    public List<PFichaMatriz> buscarFichasEnProcesoDeCarga(String usuario);// felipe.cadena redmine #7799

    /**
     * Obtiene la ficha matriz proyectada de un numero predial, retorna nulo si esta no existe
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public PFichaMatriz buscarFichaPorNumeroPredialProyectada(String numeroPredial);// felipe.cadena redmine #7799

    /**
     * Obtiene los predios asociados a este número predial.
     *
     * @param raizNumPredial
     * @return
     */
    public List<Predio> buscarPrediosPorRaizNumPredial(String raizNumPredial);// felipe.cadena redmine #7799

    /**
     * Proyecta una ficha para ingresar la información por parte del usuario
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @param fecha
     * @param usuario
     * @return
     */
    public Object[] proyectarFichaMigrada(String numeroPredial, Date fecha, String usuario);// felipe.cadena redmine #7799

    /**
     * Reversa la proyeccion de una ficha migrada
     *
     * @param numeroPredial
     * @return
     */
    public Object[] reversarProyeccionFichaMigrada(Long numeroPredial);// felipe.cadena redmine #7799

    /**
     * Confirma la proyeccion de una ficha migrada
     *
     * @param numeroPredial
     * @return
     */
    public Object[] confirmarProyeccionFichaMigrada(Long numeroPredial);// felipe.cadena redmine #7799

    /**
     * Elimina una unidad de construccion proyectada
     *
     * @author felipe.cadena
     * @param puc
     * @return
     */
    public boolean eliminarPunidadConstruccion(PUnidadConstruccion puc);

    /**
     * Método que almacena un objeto de tipo PPersonaPredio
     *
     * @param usuario
     * @param pPersonaPredio
     * @return
     * @author juanfelipe.garcia
     */
    public PPersonaPredio guardarPPersonaPredio2(UsuarioDTO usuario, PPersonaPredio pPersonaPredio);

    /**
     * Consulta las manzanas proyectadas asociadas a un predio
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public List<PManzanaVereda> obtenerManzanasProyectadasPorTramite(Long tramiteId);

    /**
     * Persiste la informacion asociada a una PManzanaVereda
     *
     * @author felipe.cadena
     * @param pmv
     * @return
     */
    public PManzanaVereda guardarActualizarPManzanaVereda(PManzanaVereda pmv);

    /**
     * Persiste la informacion asociada a una lista PManzanaVereda
     *
     * @author felipe.cadena
     * @param pmvs
     * @return
     */
    public List<PManzanaVereda> guardarActualizarMultiplePManzanaVereda(List<PManzanaVereda> pmvs);

    /**
     * Elimina la informacion asociada a una PManzanaVereda
     *
     * @author felipe.cadena
     * @param pmv
     */
    public void eliminarPManzanaVereda(PManzanaVereda pmv);

    /**
     * Genera el numero de manzana siguiente basado en el codigo del barrio
     *
     * @author felipe.cadena
     * @param numeroBarrio
     * @return
     */
    public String generarNumeroManzana(String numeroBarrio);

    /**
     * Método que realiza la suma de los avalúos de terreno de los predios, las construcciones
     * convencionales y las no convncionales asociados a la ficha matriz proyectada.
     *
     * @author felipe.cadena
     *
     * @param idFicha
     * @return -- Arreglo de tres posiciones con los datos de: <br/>
     * - Total avaluo terreno Predios <br/>
     * - Total avaluo construcciones Convencionales <br/>
     * - Total avaluo construcciones No Convencionales <br/>
     */
    public double[] calcularSumaAvaluosTotalesDePrediosMigradosFichaMatriz(Long idFicha);

    /**
     * Metodo que calcula el valor de unidad(m2) de una construccion por medio de un procedimiento
     * SQL
     *
     * @author felipe.cadena
     *
     * @param zonacodigo
     * @param sector
     * @param destino
     * @param usoConstruccion
     * @param zonaGeoEconomica
     * @param puntaje
     * @return
     */
    public Double obtenerValorUnidadConstruccionPorProcedimientoDB(String zonacodigo, String sector,
        String destino, String usoConstruccion, String zonaGeoEconomica, Long puntaje, Long predioId);

    /**
     * Metodo que obtiene una PFichaMatrizPredio por numero predial.
     *
     * @author juanfelipe.garcia
     *
     * @param numeroPredial
     * @return
     */
    public PFichaMatrizPredio obtenerPFichaMatrizPredioPorNumeroPredial(String numeroPredial);

    /**
     * Metodo que guarda y actualiza un predio en la tabla PPredio
     *
     * @author leidy.gonzalez
     *
     * @return
     */
    public PPredio guardarPPredio(PPredio pPredio);

    /**
     * Obtiene unidades no canceledas asociadas a un predio
     *
     * @author felipe.cadena
     * @param predioId
     * @return
     */
    public List<UnidadConstruccion> obtenerUnidadesNoCanceladasByPredioId(Long predioId);

    /**
     * Actualiza la entidad tramite tarea
     *
     * @author felipe.cadena
     *
     * @param tt
     * @return
     */
    public TramiteTarea actualizarTramiteTarea(TramiteTarea tt);

    /**
     * Obtiene las tareas realcionadas a un tramite en particular
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param tarea
     * @return
     */
    public List<TramiteTarea> obtenerTramiteTareaPorIdTramite(Long tramiteId, String tarea);

    /**
     * Metodo para invocar el aplicar cambios desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez::12/11/2015:: Se modifica tipo de retorno del metodo.
     */
    public boolean aplicarCambiosConservacion(Tramite tramite, UsuarioDTO usuario, int paso);

    /**
     * Metodo para invocar el proceso aplicar cambios de depuracion desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void aplicarCambiosDepuracion(Tramite tramite, UsuarioDTO usuario, int paso);

    /**
     * Metodo para invocar el proceso aplicar cambios de actualizacion desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    /*
     * @modified by leidy.gonzalez::23/10/2015:: Se modifica retorno del metodo.
     */
    public boolean aplicarCambiosActualizacion(Tramite tramite, UsuarioDTO usuario, int paso);

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param usuarioResponsableSIG -- Solo se usa para el envio a depuracion(paso 0), puede ser
     * null en los demas casos
     * @param paso
     */
    public void generarReplicaConsulta(Tramite tramite, UsuarioDTO usuario,
        UsuarioDTO usuarioResponsableSIG, int paso);

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void generarReplicaEdicion(Tramite tramite, UsuarioDTO usuario, int paso);

    /**
     * Método para obtener los tramites que se encuantran actualmnte en un proceso geografico
     *
     * @author felipe.cadena
     * @return
     */
    public List<TramiteTarea> obtenerTramitesEnJobGeografico();

    /**
     * Metodo para consultar un job por id
     *
     * @author felipe.cadena
     * @param jobId
     * @return
     */
    public ProductoCatastralJob obtenerProductoCatastralJobById(Long jobId);

    /**
     * Método que se usa exclusivamente para actualizar el objeto de tipo PPersonaPredio
     *
     * @param pPersonaPredio
     * @return
     * @author javier.aponte
     */
    //Este método es diferente del que tenía fabio.navarrete que se llama guardarActualizarPPersonaPredio
    //porque allá hace unas consultas adicionales que no son necesarias acá, este método sólo actualiza el
    //objeto PPersonaPredio
    public PPersonaPredio actualizarPPersonaPredio(PPersonaPredio pPersonaPredio);

    /**
     * Método que se usa exclusivamente para actualizar el objeto de tipo PPersonaPredioPropiedad
     *
     * @param pPersonaPredioPropiedad
     * @return
     * @author javier.aponte
     */
    //Este método es diferente del que tenía fabio.navarrete que se llama guardarActualizarPPersonaPredioPropiedad
    //porque allá hace unas consultas adicionales que no son necesarias acá, este método sólo actualiza el
    //objeto PPersonaPredioPropiedad
    public PPersonaPredioPropiedad actualizarPPersonaPredioPropiedad(
        PPersonaPredioPropiedad pPersonaPredioPropiedad);

    /**
     * Método que almacena una lista de PPersonaPredioPropiedad
     *
     * @param asList
     * @return
     * @author javier.aponte
     */
    public void guardarActualizarPPersonaPredioPropiedades(
        List<PPersonaPredioPropiedad> pPersonaPredioPropiedades);

    /**
     * Método encargado de actualizar los PPersonaPredio y los PPersonaPredioPropiedad
     *
     * @author javier.aponte
     */
    public List<PPersonaPredio> actualizarPPersonaPredioAndPPersonaPredioPropiedad(
        PersonaPredio[] propietariosVigentesSeleccionados,
        List<PPersonaPredio> pPersonaPredioPropiedades, UsuarioDTO usuario);

    /**
     * Método encargado de consultar los reportes ejecución por usuario
     *
     * @param usuarioLogin
     * @return Lista de objetos reporte control reporte
     * @author leidy.gonzalez
     * @param reporteEjecucionDetalle
     */
    public List<RepReporteEjecucion> buscarReporteEjecucionPorUsuario(String usuarioLogin);

    /**
     * actualiza el repReporteEjecucion
     *
     * @author leidy.gonzalez
     * @param reporteEjecucion
     */
    public RepReporteEjecucion actualizarRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion);

    /**
     * actualiza el repReporteEjecucionDetalle
     *
     * @author leidy.gonzalez
     * @param reporteEjecucionDetalle
     */
    public RepReporteEjecucionDetalle actualizarRepReporteEjecucionDetalle(
        RepReporteEjecucionDetalle repReporteEjecucionDetalle);

    /**
     * Método para llamar el procedimiento almacenado de reportes prediales con datos básicos
     *
     * @param parameters
     * @param usuario
     * @author javier.aponte
     * @modified by leidy.gonzalez
     */
    public Object[] generarReportePredialDatosBasicos(RepReporteEjecucion repReporteEjecucion);

    /**
     * Método que llama al procedimiento que consulta los reportes historicos de cierre anual
     * generados para un departamento o municipio.
     *
     * @author david.cifuentes
     *
     * @param codigoDepartamento
     * @param codigoMunicipio
     * @return
     */
    public List<RepReporteEjecucion> buscarReporteEjecucionCierreAnualHistoricos(
        String codigoDepartamento, String codigoMunicipio);

    /**
     * Método para llamar al procedimiento almacenado que genera los reportes para cierre anual.
     *
     * @note: Copia del método generarReportePredialDatosBasicos de leidy.gonzalez, usado en la
     * consulta de reportes prediales.
     *
     * @author david.cifuentes
     *
     * @param repReporteEjecucion
     * @return
     */
    public Object[] generarReporteCierreAnual(
        RepReporteEjecucion repReporteEjecucion);

    /**
     * Método para obtener en cierre de año los ids de los predios de un municipio seleccionado dada
     * una vigencia.
     *
     * @author david.cifuentes
     *
     * @param municipioCodigo
     * @param vigenciaDecreto
     * @return
     */
    public String[] cargarListadoPrediosPorMunicipioVigencia(String municipioCodigo,
        Long vigenciaDecreto);

    /**
     * Método que realiza el conteo de un array de números prediales enviados como parametro.
     *
     * @author david.cifuentes
     *
     * @param idsListadoPredios
     * @return
     */
    public int contarListadoPredios(String[] idsListadoPredios);

    /**
     * Método que realiza la busqueda de {@link ListadoPredio} asociados en una lista de números
     * prediales.
     *
     * @author david.cifuentes
     *
     * @param numerosPredialesListadoPredios
     * @param sortField
     * @param sortOrder
     * @param filters
     * @param contadores
     * @return
     */
    public List<ListadoPredio> buscarListadoPrediosCierreAnual(
        String[] numerosPredialesListadoPredios, String sortField, String sortOrder,
        Map<String, String> filters, int... contadores);

    /**
     * Método que realiza la busqueda de {@link ListadoPredio} para un municipio y una vigencia dada
     *
     * @author david.cifuentes
     *
     * @param municipioCodigo
     * @param vigenciaDecreto
     * @param sortField
     * @param sortOrder
     * @param filters
     * @param first
     * @param pageSize
     * @return
     */
    public List<ListadoPredio> buscarListadoPrediosCierreAnualPorMunicipioVigencia(
        String municipioCodigo, Long vigenciaDecreto, String sortField,
        String sortOrder, Map<String, String> filters, int first,
        int pageSize);

    /**
     * Método que realiza el conteo del listado de predios dada una vigencia y un codigo de
     * municipio
     *
     * @author david.cifuentes
     *
     * @param municipioCodigo
     * @param vigenciaDecreto
     * @param filters
     * @return
     */
    int contarListadoPredios(String municipioCodigo, Long vigenciaDecreto,
        Map<String, String> filters);

    /**
     * Metodo que obtiene una FichaMatrizPredio por numero predial.
     *
     * @author javier.aponte
     *
     * @param numeroPredial
     * @return
     */
    public FichaMatrizPredio obtenerFichaMatrizPredioPorNumeroPredial(String numeroPredial);

    /**
     * Método para verificar si una {@link PFichaMatrizTorre} que se encuentra proyectada, viene de
     * una {@link FichaMatrizTorre} en firme de la {@link FichaMatriz}
     *
     * @param torre
     * @param id
     * @return
     */
    boolean existeTorreEnFirmePorIdFichaMatrizYNumeroTorre(Long numeroTorre, Long idFichaMatriz);

    /**
     * Obtiene los predio proyectados asociados a datos de regsitro determinados
     *
     * @author felipe.cadena
     * @param circuloRegistralCodigo
     * @param numeroRegistro
     * @return
     */
    public List<PPredio> buscarPPredioPorMatricula(String circuloRegistralCodigo,
        String numeroRegistro);

    /**
     * Obtiene los predio en firme asociados a datos de regsitro determinados
     *
     * @author felipe.cadena
     * @param circuloRegistralCodigo
     * @param numeroRegistro
     * @return
     */
    public List<Predio> buscarPredioPorMatricula(String circuloRegistralCodigo,
        String numeroRegistro);

    /**
     * Metodo que retorna la direccion principal de una ficha matriz, recibe como parametro en
     * numero hasta la posicion de condicion de propiedad (21)
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public PPredioDireccion obtenerDireccionPrincipalFM(String numeroPredial);

    /**
     * Método para consultar los {@link PFichaMatrizPredio} con determinado valor cancela inscribe
     * de proyección y de una {@link PFichaMatriz}
     *
     * @author david.cifuentes
     * @param valorCancelaInscribe
     * @param fichaMatrizId
     * @return
     */
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(
        String valorCancelaInscribe, Long fichaMatrizId);

    /**
     * Método usado para actualizar un lista de {@link PFichaMatrizPredio}
     *
     * @param listaPFichaMatrizPredio
     */
    void actualizarListaPFichaMatrizPredios(List<PFichaMatrizPredio> listaPFichaMatrizPredio);

    /**
     * Método usado para consultar una {@link FichaMatriz} dado su id
     */
    public FichaMatriz obtenerFichaMatrizPorId(Long idFichaMatriz);

    /**
     * Método usado para consultar una {@link FichaMatriz} dado el id del predio
     *
     * @author javier.aponte
     */
    public FichaMatriz buscarFichaMatrizPorPredioId(Long idFichaMatriz);

    /**
     * Metodo que determina cuales de los predios asociados a un tramite han sido modificados con
     * respecto a una tabla en particular.
     * <BR/>
     * <BR/>
     * La tabla asociada debe estar relacionada directamente al predio con un campo predio_id. Si no
     * existen registros en la tabla para un predio no habra entrada de ese predio en particular. Si
     * la tabla no esta asociada el metodo retorna null
     * <BR/>
     * <BR/>
     * Retorna un mapa con los ids de los predios como llave y un boolean que indica si el predio
     * fue modificado o no.
     *
     * @author felipe.cadena
     * @param idTramite
     * @param nombreTablaAsociada
     * @return
     */
    public TreeMap<Long, Boolean> obtenerPredioModificadoPorTamite(Long idTramite,
        String nombreTablaAsociada);

    /**
     * Metodo para la recuperacion de zonas despues del proceso geografico solicitadas por el
     * componente web o por el editor
     *
     * @author andres.eslava
     */
    public Boolean actualizacionZonasHomogeneasAsync(ProductoCatastralJob job);

    /**
     * Retorna el numero de predios asociados a un predio cuando se radica.
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public Integer contarPrediosAsociadosATramite(Long tramiteId);

    /**
     * Método encargado de generar la ficha predial digital en JasperServer cuando ya se tiene
     * generado la imagen la localizaciòn geográfica y de la localización de la manzana
     *
     * @author javier.aponte
     *
     * @param job
     * @return boolean que indica si se generó exitosamente la ficha predial digital
     */
    public boolean procesarFichaPredialDigitalEnJasperServer(ProductoCatastralJob job);

    /**
     * Metodo para eliminar la proyeccion de un predio, se realiza mediante un query nativo ya que
     * en la DB el borrado esta en cascada. Se deber tener cuidado de actualizar las entidades
     * cuando se ejecute este metodo.
     *
     * @author felipe.cadena
     * @param idPredio
     */
    public void eliminarProyeccionPredio(Long idPredio);

    /**
     * Método que busca las zonas homogéneas de tipo {@link PPredioZona} de un {@link PPredio} por
     * su nùmero predial
     *
     * @author javier.aponte
     * @param numeroPredial
     * @return
     */
    public List<PPredioZona> buscarZonasHomogeneasPorNumeroPredialPPredio(String numeroPredial);

    /**
     * Metodo que borra los pPrediosServidumbre con el id enviado.
     *
     * @author leidy.gonzalez
     *
     * @param idServidumbre
     * @return
     */
    public void borrarPPredioServidumbres1(Long idServidumbre);

    /**
     * Obtiene los numeros prediales que estan relacionados con tramites geograficos finalizados.
     *
     * @param codigoManzana numero predial que incluye los primeros 17 digitos.
     * @return lista de predios.
     * @author andres.eslava
     */
    public List<PredioInfoVO> obtenerPrediosInvariantesDepuracion(String codigoManzana) throws
        Exception;

    /**
     * Busqueda de la unidad de construcion por su id
     *
     * @param idUnidadConstruccion
     * @return UnidadConstruccion
     */
    public UnidadConstruccion findUnidadConstruccionById(Long idUnidadConstruccion);

    /**
     * Método que busca las unidades de construcción que han sido generadas a partir de la
     * asociación de la unidad de construcción y el trámite enviados como parámetros.
     *
     * @author david.cifuentes
     * @param idTramite
     * @param unidadConstruccionPredioOriginal
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(
        Long idTramite, UnidadConstruccion unidadConstruccionPredioOriginal);

    /**
     * Metodo que obtiene la lista de resoluciones o decretos del predio consultandolos con el id
     * del predio enviado
     *
     * @author leidy.gonzalez:: #12528::26/05/2015 Se modifica por CU_187
     * @param predioId
     * @return
     */
    public ArrayList<HistoricoAvaluosVO> obtenerResolucionODecreto(Long predioId);

    /**
     * Metodo que busca el horario de atencion de una territorial o UOC y lo devuelve como DTO
     *
     * @author lorena.salamanca
     * @param estructuraOrganizacionalCodigo
     * @return
     */
    public List<HorarioTerritorialDTO> buscarHorarioAtencionByEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo);

    /**
     * Metodo guarda/actualiza el horario de atencion de una territorial o UOC
     *
     * @author lorena.salamanca
     * @param horarioList
     * @param estructuraOrganizacionalCodigo
     * @param usuario
     */
    public void guardarActualizarHorarioAtencion(List<HorarioTerritorialDTO> horarioList,
        String estructuraOrganizacionalCodigo, UsuarioDTO usuario);

    /**
     * Metodo que elimina el horario de atencion de una territorial o UOC
     *
     * @author lorena.salamanca
     * @param horarioList
     * @param estructuraOrganizacionalCodigo
     * @param usuario
     */
    public void eliminarHorarioAtencion(List<HorarioTerritorialDTO> horarioList,
        String estructuraOrganizacionalCodigo, UsuarioDTO usuario);

    /**
     * Metodo que busca el horario de atencion de una territorial o UOC y lo devuelve como la
     * entidad
     *
     * @author lorena.salamanca
     * @param estructuraOrganizacionalCodigo
     * @return
     */
    public HorarioAtencionTerritorial buscarHorarioAtencionByEstructuraOrganizacionalCodigo(
        String estructuraOrganizacionalCodigo);
//end of interface

    /**
     * Método encargado de crear un nuevo registro en la tabla producto catastral job, estos jobs
     * están en espera de que sean ejecutados en jasperserver para las resoluciones de conservacion
     *
     * @see IConservacion#crearJobGenerarResolucionesConservacion(tramite,parametros)
     * @author leidy.gonzalez
     * @param job productoCatastralJob
     * @param reporte
     * @param tramiteId
     */
    public ProductoCatastralJob crearJobGenerarResolucionesConservacion(Tramite tramite,
        Map<String, String> parametros, Actividad actividad, UsuarioDTO usuarioLogeado);

    /**
     * Método encargado de generar las resoluciones en JasperServer de conservacion
     *
     * @author leidy.gonzalez
     *
     * @param job
     * @param contador
     * @return boolean que indica si se generó exitosamente la resolucion
     */
    public boolean procesarResolucionesConservacionEnJasperServer(ProductoCatastralJob job,
        int contador);

    /**
     * Método encargado de avanzar las resoluciones en Process
     *
     * @author leidy.gonzalez
     * @param tramite
     * @param usuario
     * @return
     */
    public void avanzarTramiteDeGenerarResolucionProcess(Tramite tramite,
        UsuarioDTO usuario, Actividad actividad);

    /**
     * Consulta los predios cancelados de una Ficha Matriz verificando hasta Condicion de propiedad
     *
     * @author lorena.salamanca
     * @param numeroPredialFichaMatriz
     * @return
     */
    public List<Predio> obtenerPrediosCanceladosQuintaCondominioPH(String numeroPredialFichaMatriz);

    /**
     * Método para consultar los {@link PFichaMatrizPredio} por su estado y el id de la
     * {@link PFichaMatriz}
     *
     * @author david.cifuentes
     * @param estadoPredio
     * @param idFichaMatriz
     * @return
     */
    public List<PFichaMatrizPredio> buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(
        String estadoPredio, Long idFichaMatriz);

    /**
     * Método para consultar los {@link HFichaMatrizPredio} por su estado y el id de la
     * {@link FichaMatriz}
     *
     * @author david.cifuentes
     * @param estado
     * @param idFichaMatriz
     * @return
     */
    public List<HFichaMatrizPredio> buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(
        String estado, Long idFichaMatriz);

    /**
     * Método que consulta un {@link HPredio} por su número predial
     *
     * @author david.cifuentes
     * @param numeroPredial
     * @param estado
     * @return
     */
    public HPredio buscarHPredioPorNumeroPredialYEstado(String numeroPredial, String estado);

    /**
     * Método que persiste un {@link PPredio}
     *
     * @author lorena.salamnaca
     * @param pPredio
     */
    public void agregarPPredio(PPredio pPredio);

    /**
     *
     * Metodo que obtiene los PPredios ACTIVOS completos
     *
     * @author lorena.salamanca
     * @param idTramite
     * @return
     */
    public List<PPredio> findPPrediosCompletosActivosByTramiteId(Long idTramite);

    /**
     * Método que realiza la consulta de un único {@link PPredio} basado en logica asociada al tipo
     * de trámite
     *
     * @author david.cifuentes
     * @param tramite
     * @return
     */
    public PPredio buscarPPredioPorTramite(Tramite tramite);

    /**
     * Metodo que determina cuales de los predios asociados a un tramite se le has modificado el
     * número predial
     * <BR/>
     * Retorna un mapa con los números prediales como llave y un boolean que indica si el predio fue
     * modificado o no.
     *
     * @author javier.aponte
     * @param idTramite
     * @return
     */
    public TreeMap<String, Boolean> obtenerPredioConNumeroPredialModificadoPorTamite(Long idTramite);

    /**
     * Metodo para persistir avaluo proyectado Predios Fiscales
     *
     * @param ppaval
     * @return
     */
    public PPredioAvaluoCatastral guardarPPredioAvaluoCatastral(PPredioAvaluoCatastral ppaval);

    /**
     * Metodo para actualizar unidades de construccion proyectadas
     *
     * @param unidad
     * @return
     */
    public PUnidadConstruccion actualizarPUnidadDeConstruccion(PUnidadConstruccion unidad);

    /**
     * Método encargado de cargar el decreto correspondiente a la vigencia enviada por parametro
     *
     * @param anio
     * @return
     */
    public DecretoAvaluoAnual buscarDecretoPorVIgencia(String anio);

    /**
     * Método que utiliza el SP que retorna el valor de liquidacion para una vigencia y un predioID
     *
     * @param vigencia
     * @param predioId
     * @return
     */
    public double obtenerPorcentajeIncrementoVigenciaPredio(String vigencia, long predioId);

    /**
     * Método que elimina toda la proyeccion de avaluos de un ppredio
     *
     * @param avaluo
     */
    public void eliminarPPredioAvaluoCatastralCompleto(List<PPredioAvaluoCatastral> avaluo);

    /**
     * Método para buscar un predio por su id con todos sus atributos en fetch.
     *
     * @param predioId
     * @return
     */
    public Predio buscarPredioCompletoPorPredioId(Long predioId);

    /**
     * Metodo para obtener las direcciones asociadas a un predio determinado
     *
     * @author felipe.cadena
     *
     * @param predio
     * @return
     */
    public List<PredioDireccion> obtenerDireccionesPredio(Predio predio);

    /**
     * Método encargado de contar la cantidad de linderos que tiene un Predio
     *
     * @param Integer
     * @return
     */
    public Integer contarLinderosPorNumeroPredial(String numeroPredial);

    /**
     * Metodo para actualizar una lista de pfotos, este método es diferente del método
     * actualizarPFotosUnidadConstruccion ya que sólo actualiza las pfotos y no hace nada más.
     *
     * @author javier.aponte
     *
     * @param List<PFoto>
     * @return
     */
    public List<PFoto> actualizarPFotos(List<PFoto> pFotos);

    /**
     * Obtiene un {@link PPredio} por el numero predial con la informacion del tramite asociado
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public PPredio obtenerPPredioPorNumeroPredialConTramite(String numeroPredial);

    /**
     * Método que llama el procedimiento almacenado para actualizar el año y el número de última
     * resolución de todos los predios asociados a un trámite.
     *
     * @param tramiteId
     * @param anioUltimaResolucion
     * @param numeroUltimaResolucion
     * @param usuario
     * @return
     */
    public Object[] establecerAnioYNumeroUltimaResolucion(Long tramiteId, int anioUltimaResolucion,
        String numeroUltimaResolucion, UsuarioDTO usuario);

    /**
     * Método que realiza la búsqueda de un predio por su id, pero no realiza fetch sobre ninguna
     * tabla, en vez de ello realiza la inicialización de los campos a través de un
     * Hibernate.inicialize
     *
     * @param idPredio
     * @return
     * @throws ExcepcionSNC
     */
    public PPredio buscarPredioCompletoPorIdSinFetch(Long idPredio)
        throws ExcepcionSNC;

    /**
     * Método que realiza la búsqueda de entidades por un nombre y un estado enviados como
     * parámetros.
     *
     * @author david.cifuentes
     *
     * @param nombreEntidad
     * @param estadoEntidad
     * @return
     */
    public List<Entidad> buscarEntidadesPorNombreYEstado(String nombreEntidad,
        String estadoEntidad);

    /**
     * Método que realiza la búsqueda de entidades por un nombre, estado, departamento y municipio
     * enviados como parámetros.
     *
     * @author dumar.penuela
     *
     * @param nombreEntidad
     * @param estadoEntidad
     * @param departamento
     * @param municipio
     * @return
     */
    public List<Entidad> buscarEntidades(String nombreEntidad,
        String estadoEntidad, String departamento, String municipio);

    /**
     * Método que guarda o actualiza los datos de una {@link Entidad}.
     *
     * @author david.cifuentes
     *
     * @param entidad
     * @return
     */
    public Entidad guardarEntidad(Entidad entidad);

    /**
     * Método que verifica si una entidad se encuentra o no asociada al bloqueo de un predio o una
     * persona.
     *
     * @param entidad
     * @return
     */
    public boolean existeAsociacionEntidadBloqueo(Entidad entidad);

    /**
     * Método encargado de eliminar una entidad de la base de datos.
     *
     * @author david.cifuentes
     *
     * @param entidad
     */
    public void eliminarEntidad(Entidad entidad);

    /**
     * Método que realiza la búsqueda de las {@link Entidad} por estado
     *
     * @author david.cifuentes
     *
     * @param estado
     */
    public List<Entidad> buscarEntidadesPorEstado(String estado);

    /**
     * Método que realiza la búsqueda de una {@link Entidad} por su id
     *
     * @author david.cifuentes
     *
     * @param idEntidadBloqueo
     */
    public Entidad findEntidadById(Long idEntidadBloqueo);

    /**
     * Método que realiza la búsqueda de una {@link Entidad}
     *
     * @author david.cifuentes
     * @param nombreEntidad
     * @param codDepartamento
     * @param codMunicipio
     * @param correo
     * @return
     */
    public Entidad buscarEntidad(String nombreEntidad, String codDepartamento, String codMunicipio,
        String correo);

    /**
     * Retorna la solicitud solo con los datos basicos
     *
     * @author felipe.cadena
     * @param numeroSolicitud
     * @return
     */
    public Solicitud findSolicitudByNumSolicitud(String numeroSolicitud);

    /* Metodo para obtener los municipios registrados para actualizacion segun la vigencia dada
     *
     *
     * @author felipe.cadena @param municipioCodigo @param vigencia @return
     */
    public List<AxMunicipio> obtenerAxMunicipioPorMunicipioVigencia(String municipioCodigo,
        String vigencia);

    /* Método para consultar el orario de Atencion de una Territorial
     *
     * @author leidy.gonzalez @param Codigo de la Estructura Organizacional @return
     */
    public Object[] consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(
        String estructuraOrganizacionalCodigo);

    /**
     * Método que realiza la verificación de la existencia de entidades{@link Entidad}
     *
     * @param nombreEntidad
     * @param estadoEntidad
     *
     * @param codDepartamento
     * @param codMunicipio
     * @return
     */
    public boolean validaExistenciaEntidad(String nombreEntidad, String estadoEntidad,
        String codDepartamento, String codMunicipio);

    /**
     * Método que realiza la verificación de la existencia de un número de radicado
     *
     * @param numeroRadicacionCorrespondencia
     * @param usuario
     *
     * @return
     */
    public boolean validarNumeroRadicacion(String numeroRadicacionCorrespondencia,
        UsuarioDTO usuario);

    /**
     * Método que retorna un predio bloqueado según el tipo de bloqueo
     *
     * @param numeroPredial
     * @param tipoBloqueo
     * @return
     */
    public PredioBloqueo busquedaPredioBloqueoByTipoBloqueo(String numeroPredial, String tipoBloqueo);

    /**
     * Método que retorna predio bloqueado más reciente
     *
     * @param numeroPredial
     * @return
     */
    public PredioBloqueo busquedaPredioBloqueoReciente(String numeroPredial);

    /**
     * Método que retorna la lista de predios desbloqueados
     *
     * @param numeroPredial
     * @return
     */
    public List<PredioBloqueo> getPredioDesBloqueosByNumeroPredial(String numeroPredial);

    /**
     * Método que retorna predio desbloqueado más reciente
     *
     * @param numeroPredial
     * @return
     */
    public PredioBloqueo busquedaPredioDesBloqueoReciente(String numeroPredial);

    /**
     * Obtiene los municipios en proceso de actualizacion express
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<AxMunicipio> obtenerAxMunicipioEnActualizacion();

    /**
     * Obtiene los personas predio asociadas a una persona
     *
     * @author leidy.gonzalez
     * @param personaId
     *
     * @return
     */
    public List<PersonaPredio> getPersonaPrediosByPersonaId(Long personaId);

    /**
     * Obtiene los p_persona_predio asociadas a una lista de predios
     *
     * @author leidy.gonzalez
     * @param predioIds
     *
     * @return
     */
    public List<PPersonaPredio> buscarPPersonasPredioContandoPredios(List<Long> predioIds);

    /**
     * Obtiene los p_persona_predio asociadas a una lista de predios
     *
     * @author leidy.gonzalez
     * @param predioIds
     *
     * @return
     */
    public List<PPersonaPredio> buscarPPersonaPrediosPorIdPredios(List<Long> predioIds);

    /**
     * Método encargado de actualizar los PPersonaPredio y los PPersonaPredioPropiedad de los
     * tramites cancelacion masiva
     *
     * @author leidy.gonzalez
     * @param propietariosCanceladosSeleccionados
     * @param pPersonaPredioPropiedades
     * @return
     */
    public List<PPersonaPredio> actualizarPPersonaPredioYPropiedadCancelados(
        List<PPersonaPredio> pPersonaPredioPropiedades, UsuarioDTO usuario,
        PPredio ppredioSeleccionado);

    /**
     * Obtiene los bloqueos qu tiene asociado un predio en particular, tambien filtra por el estado
     * del bloqueo, si el stado es nulo los retorna todos.
     *
     * @author felipe.cadena
     * @param predioId
     * @param estadoBloqueo
     * @return
     */
    public List<PredioBloqueo> obtenerBloqueosPredio(Long predioId, String estadoBloqueo);

    /**
     * Obtiene la fecha en la que fue generado el Predio
     *
     * @author leidy.gonzalez
     * @param pPredioId
     * @return
     */
    public Date consultarFechaCreacionPredio(Long pPredioId);

    /**
     * Obtiene las justificaciones asociadas al propietario de un predio
     *
     *
     * @author felipe.cadena
     *
     * @param personaPredioId
     */
    public List<PPersonaPredioPropiedad> consultarJustificacionesPorPropetario(Long personaPredioId);

    /**
     * Método usado para almacenar las justificaciones de derecho de propiedad desde la pantalla de
     * proyecciones para predios que no fueron cancelados en los tramites de cancelacion masiva
     *
     * @param justificacionSeleccionada
     * @param listPersonasPredioSinCanc
     * @param usuario
     * @return
     * @author leidy.gonzalez
     */
    public JustificacionPropiedadDTO guardarJustificacionesPropiedadPrediosNoCancelados(
        JustificacionPropiedadDTO justificacionSeleccionada,
        JustificacionPropiedadDTO justificacionSeleccionadaSinModificar,
        List<PPersonaPredio> listPersonasPredioSinCanc,
        UsuarioDTO usuario);

    /**
     * Valida si el conjunto de predios del parametro se existen el la BD y estan en estado activo
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarExistenciaYActivosDePredios(List<String> numeros);

    /**
     * @param numeroPredial
     * @param estadoCancelaInscribe
     * @return
     */
    public HPredio buscarHPredioPorNumeroPredialYEstadoCancelaInscribe(
        String numeroPredial, String estadoCancelaInscribe);

    //end of interface
    /**
     * Obtiene todos los usos homologados para las cancelaciones
     *
     * @author felipe.cadena
     * @return
     */
    public List<UsoHomologado> obtenerUsosHomologados();

    /**
     * Método que retorna los PersonaPredios asociados a un predio con cancela/Inscribe en Inscribe
     *
     * @author leidy.gonzalez
     * @param idPPredio
     * @return
     */
    public List<PPersonaPredio> buscarPPersonaPrediosPorPredioIdInscritos(Long idPPredio);

    /**
     * Retorna los predios asociados a una ficha matriz filtrado por el estado, si el estado es null
     * retorna todos los predios.
     *
     * @param estado
     * @param idFicha
     * @return
     */
    public List<PFichaMatrizPredio> obtenerPrediosPorFichaEstado(String estado, Long idFicha);

    /**
     * Método usado para almacenar las justificaciones de derecho de propiedad desde la pantalla de
     * proyecciones para tarmites de Cancelacion Masiva
     *
     * @param JustificacionPropiedadDTO
     * @param propietariosNuevosSeleccionados
     * @param usuario
     * @return
     * @author leidy.gonzalez
     */
    public JustificacionPropiedadDTO guardarJustificacionesPropiedadCancelacionMasiva(
        JustificacionPropiedadDTO justificacionSeleccionada,
        JustificacionPropiedadDTO justificacionSeleccionadaSinModificar,
        PPersonaPredio[] propietariosNuevosSeleccionados, UsuarioDTO usuario);

    /**
     * Método encargado de generar las resoluciones en JasperServer de Cancelacion Masiva
     *
     * @author leidy.gonzalez
     *
     * @param job
     * @param contador
     * @return boolean que indica si se generó exitosamente la resolucion
     */
    public boolean procesarResolucionesCancelacionMasivaEnJasperServer(ProductoCatastralJob job,
        Tramite tramite, int contador);

    /**
     * Retorna la ficha matriz asociada a un predio sin atributos asociados
     *
     *
     * @author felipe.cadena
     * @param idPredio
     * @return
     */
    public FichaMatriz buscarFichaMatrizPorPredioIdSinAsociaciones(Long idPredio);

    /**
     * Retorna las mejoras asociadas a un numeo predial dado
     *
     * @author felipe.cadena
     * @param numeroPredial
     * @return
     */
    public List<Predio> buscarMejorasPorNumeroPredial(String numeroPredial);

    /**
     * Retorna los pPredios asociados a los ids
     *
     * @author leidy.gonzalez
     * @param predioIds
     * @return
     */
    public List<PPredio> buscarPPredioPorIdPredios(List<Long> predioIds);

    /**
     * Determina la existencia de las matriculas dadas en los predios en firme
     *
     * @author felipe.cadena
     *
     * @param matriculas
     * @param municipioCodigo
     * @return
     */
    public List<Predio> validarExistenciaMatriculasPorMunicipio(List<String> matriculas,
        String municipioCodigo);

    /**
     * Retorna los pPredios asociados a los ids
     *
     * @author leidy.gonzalez
     * @param predioIds
     * @return
     */
    public List<PPredioZona> buscarZonasVigentes(Long idPPredio);

    /**
     * Determina la existencia de los predios asociados a los numeros prediales dados
     *
     * @author felipe.cadena
     *
     * @param nPrediales
     * @return
     */
    public List<Predio> consultaPrediosPorNumeroPredial(List<String> nPrediales);

    /**
     * Determina la existencia de los predios proyectados asociados a los numeros prediales dados
     *
     * @author felipe.cadena
     *
     * @param nPrediales
     * @return
     */
    public List<PPredio> consultaPPrediosPorNumeroPredial(List<String> nPrediales);

    /**
     * Determina la existencia de los predios proyectados asociados a los numeros prediales dados y filtra por tipo de tramite
     *
     * @author felipe.cadena
     *
     * @param nPrediales
     * @return
     */
    public List<PPredio> consultaPPrediosPorNumeroPredial(List<String> nPrediales, String tipoTramite,  Boolean equal);

    /**
     * Obtiene los predios en firme que estan asociados a un tramite
     *
     *
     * @author felipe.cadena
     *
     * @param tramiteId
     * @return
     */
    public List<Predio> obtenerPrediosCompletosPorTramiteId(Long tramiteId);

    /**
     * Retorna los PPredio asociados a una a una vigencia y un id.
     *
     * @author leidy.gonzalez
     * @param idPPredio,vigencia
     * @return
     */
    public List<PPredioZona> buscarZonasPorPPredioIdYVigencia(Long idPPredio, Date vigencia);

    /**
     * Retorna los fichaMatrizPredio asociados a una ficha.
     *
     * @author felipe.cadena
     * @param fichaId
     * @return
     */
    public List<PFichaMatrizPredio> buscarFichaMatrizPredioPorPFichaMatrizId(Long fichaId);

    /**
     * Método encargado de consultar los reportes por los filtros tipo reporte, departamento y
     * municipio
     *
     * @param tipoReporte
     * @param codigoDepartamento
     * @param codigoMunicipio
     * @return Lista de objetos reporte ejecucion generados y finalizados
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> buscarReportePorDepartamentoYMunicipio(Long tipoReporte,
        String codigoDepartamento, String codigoMunicipio);

    /**
     * Método encargado de consultar los reportes por el id de reporte generado
     *
     * @param idReporteConsultar
     * @return Lista de objetos reporte ejecucion generados y finalizados
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> buscarReportesPorCodigo(String idReporteConsultar);

    /**
     * Retorna el reporte que esta asociado al codigo enviado por parametro
     *
     * @author leidy.gonzalez
     *
     * @param codigoReporte
     * @return
     */
    public RepReporteEjecucion buscarReportePorCodigo(String codigoReporte);

    /**
     * Método encargado de consultar los reportes por el id de reporte generado
     *
     * @param idReporteConsultar
     * @return Lista de objetos reporte ejecucion generados y finalizados
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicos(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedDepartamentoCod, String selectedMunicipioCod, String territorial, String uoc);

    /**
     * Método encargado de consultar los reportes por los filtros Avanzados Generados
     *
     * @param datosConsultaPredio
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucionDetalle> buscarReportePorFiltrosAvanzados(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedMunicipioCod, Long idReporteEjecucion,
        RepConfigParametroReporte configuracionAvanzada);

    /**
     * Método encargado de consultar los reportes por el id de reporte generado
     *
     * @param idReporteConsultar
     * @return Lista de objetos reporte ejecucion generados y finalizados
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> buscarReportePorId(Long idReporteConsultar);

    /**
     * Método encargado de consultar los reportes asociados por usuario y categoria
     *
     * @param idReporteConsultar
     * @return Lista de objetos reporte ejecucion generados y finalizados
     * @author leidy.gonzalez
     */
    public List<RepUsuarioRolReporte> buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
        UsuarioDTO usuario, String categoria);

    /**
     * Método encargado de consultar los reportes por los filtros ingresados omitiendo el municipio
     *
     * @return Lista de objetos reporte ejecucion generados y finalizados
     * @author leidy.gonzalez
     */
    public List<RepReporteEjecucion> buscarReportePorFiltrosBasicosSinMuncipio(
        FiltroGenerarReportes datosConsultaPredio,
        String selectedDepartamentoCod, String territorial, String uoc);

    /**
     * Método encargado de consultar la cantidad de predios, dentro de un rango de numeros prediales
     *
     * @param numeroPredial
     * @param numeroPredialFinal
     * @author leidy.gonzalez
     */
    public Integer contarPrediosPorRangoNumeroPredial(String numeroPredial,
        String numeroPredialFinal);

    /**
     * Método encargado de consultar la cantidad de predios, dentro de un rango de numeros prediales
     * por historico
     *
     * @param numeroPredial
     * @param numeroPredialFinal
     * @author leidy.gonzalez
     */
    public Integer contarPrediosPorRangoNumeroPredialHistorico(String numeroPredial,
        String numeroPredialFinal);

    public List<RepReporteEjecucion> buscarReportesRadicacionGenerados(
        FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion);

    public Object[] generarReporteRadicacion(RepReporteEjecucion repReporteEjecucion);

    /**
     * Retorna las subscripciones de un usuario determinado
     *
     * @author leidy.gonzalez
     *
     * @param usuario, idReporte
     * @return
     */
    public List<RepReporteEjecucionUsuario> buscarSubscripcionPorUsuarioIdReporte(String usuario,
        Long idReporte);

    /**
     * actualiza el repReporteEjecucionUsuario
     *
     * @author leidy.gonzalez
     * @param repReporteEjecucionUsuario
     */
    public RepReporteEjecucionUsuario actualizarRepReporteEjecucionUsuario(
        RepReporteEjecucionUsuario repReporteEjecucionUsuario);

    /**
     * Metodo que retorna si el predio es migrado de cobol
     *
     * Detalle: Validar que la primer proyección del numero predial sea inscribe si es “modifica”
     * validar que sea clase mutación Segunda / Quinta y en el campo MantieneNumeroPredial debe ser
     * 'Si', de lo contrario es migrado.
     *
     * @author juan.cruz
     * @param numeroPredial
     * @return boolean que indica si el predio es migrado de cobol
     */
    public boolean isPredioMigradoCobol(String numeroPredial);

    /** Método que realiza la búsqueda de un {@link RepUsuarioRolReporte} por su id trayendo sus
     * relaciones en fetch.
     *
     * @param id
     * @return
     */
    public RepUsuarioRolReporte buscarParametrizacionReporteUsuarioCompletaPorId(Long id);

    public List<RepUsuarioRolReporte> buscarParametrizacionReporteUsuario(
        FiltroDatosConsultaParametrizacionReporte datosConsultaParametrizacion);

    /**
     * Retorna lasd subscripciones de un usuario determinado
     *
     * @author felipe.cadena
     *
     * @param usuario
     * @return
     */
    public List<RepReporteEjecucionUsuario> buscarSubscripcionPorUsuario(String usuario);

    public List<RepReporteEjecucion> buscarReportesPorIds(List<Long> idsReportes);

    public boolean eliminarRepUsuarioRolReporte(List<Long> idsParametrizaciones);

    public RepUsuarioRolReporte guardarRepUsuarioRolReporte(RepUsuarioRolReporte reporteUsuario);

    /**
     * Retorna la ruta temporal de un documento
     *
     * @author leidy.gonzalez
     *
     * @param idRepositorio
     * @return
     */
    public String descargarOficioDeGestorDocumentalATempLocalMaquina(String idRepositorio);

    /**
     * Método que realiza la búsqueda de un {@link RepConfigParametroReporte} por su id trayendo sus
     * relaciones en fetch.
     *
     * @param idReporte
     * @return
     */
    public List<RepConfigParametroReporte> consultaConfiguracionReportePredial(Long idReporte);

    /**
     * MÃ©todo que busca las unidades de construcciÃ³n que han sido generadas a partir del trÃ¡mite
     * y el numero Ficha Matriz enviados como parÃ¡metros.
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @param numeroFichaMatriz
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidConstPorTramiteYDifNumFichaMatrizEV(
        Long idTramite, String numeroFichaMatriz);

    /**
     * MÃ©todo que retorna las p unidades de construccion asociadas a un predio por su id de predio
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadsConstruccionPorIdPredio(
        Long idPred);

    /**
     * Metodo para calcular las zonas fisicas y geoeconomicas segun una vigencia y zona dada
     *
     * @author leidy.gonzalez
     *
     * @param vigencia
     * @param zona
     * @return
     */
    public Object[] calculoZonasFisicasYGeoeconomicas(Date vigencia, String zona);

    /**
     * Retorna un FichaMatrizTorre sin hacer fetch de nada
     *
     * @author leidy.gonzalez
     * @param fichaMatrizTorreId
     * @return
     */
    public FichaMatrizTorre obtenerFichaMatrizTorrePorId(Long fichaMatrizTorreId);

    /**
     * MÃ©todo que retorna las p unidades de construccion CONVENCIONALES asociadas a un predio por
     * su id de predio
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstConvPorIdPredio(
        Long idPred);

    /**
     * MÃ©todo que retorna las p unidades de construccion CONVENCIONALES asociadas a un predio por
     * su id de predio
     *
     * @author leidy.gonzalez
     * @param idPred
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadesConstNoConvPorIdPredio(
        Long idPred);

    /**
     * MÃ©todo que retorna las ficha matriz torres asociadas a un id de ficha matriz
     *
     * @author leidy.gonzalez
     * @param fichaMatrizId
     * @return
     */
    public List<PFichaMatrizTorre> obtenerFichaMatrizTorrePorFichaId(Long fichaMatrizId);

    /**
     * MÃ©todo que guarda un {@link PFichaMatrizPredio} de la {@link PFichaMatrizPredio}.
     *
     * @author leidy.gonzalez
     * @param prediosFichaMatriz
     * @return
     */
    public void actualizarListaPFichaMatrizPredio(PFichaMatrizPredio predioFichaMatriz);

    /**
     * MÃ©todo que guarda un {@link PFichaMatrizPredioTerreno} de la
     * {@link PFichaMatrizPredioTerreno}.
     *
     * @author leidy.gonzalez
     * @param prediosFichaMatriz
     * @return
     */
    public void actualizarListaPFichaMatrizPredioTerreno(
        PFichaMatrizPredioTerreno predFichaMatrizPredTerr);

    /**
     * MÃ©todo que retorna las p unidades de construccion asociadas a un tramite que se enceuntran
     * canceladas y que las construcciones con condicion de propiedad 8 y 9
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidConstOriginalesCanceladasPorTramite(
        Long idTramite);

    /**
     * MÃ©todo que retorna las p unidades de construccion asociadas a un tramite que se enceuntran
     * canceladas y que son diferentes a las construcciones con condicion de propiedad 8 y 9
     *
     * @author leidy.gonzalez
     * @param idPredio
     * @return
     */
    public List<PUnidadConstruccion> buscarUnidConstOriginalesPorIdPredio(
        Long idPredio);

    /**
     * Retorna los pPredios asociados a los ids de predio que no esten cancelados
     *
     * @author leidy.gonzalez
     * @param predioIds
     * @return
     */
    public List<PPredioZona> buscarZonasNoCanceladaPorPPredioId(Long idPPredio);

    /**
     * Retorna los PPredioZona asociados a los ids de predio que tengan cancela/inscribe Temp
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredioZona> buscarZonasTempPorIdTramite(Long idTram);

    /**
     * Almacena lista PPredioZona
     *
     * @author leidy.gonzalez
     * @param pPredioZona
     * @return
     */
    public void guardarListaPPredioZona(List<PPredioZona> pPredioZona);

    /**
     * Elimina una lista de PFichaMatrizPredioTerreno
     *
     * @author felipe.cadena
     * @param predFichaMatrizPredTerr
     */
    public void eliminarFichaMatrizPredioTerrenoMul(
        List<PFichaMatrizPredioTerreno> predFichaMatrizPredTerr);

    /**
     * MÃ©todo que retorna las p unidades de construccion comp asociadas a una unidad de
     * construccion
     *
     * @author leidy.gonzalez
     * @param unidadConstruccionId
     * @return
     */
    public List<PUnidadConstruccionComp> buscarUnidadDeConstruccionPorUnidadConstruccionId(
        Long unidadConstruccionId);

    /**
     * Almacena lista PUnidadConstruccionComp
     *
     * @author leidy.gonzalez
     * @param PUnidadConstruccionComp
     * @return
     */
    public void insertarMultiplePUnidadConstruccionComp(
        List<PUnidadConstruccionComp> newUnidadesConstruccionComp);

    /**
     * Obtiene una lista de PPredio de consdicion 9 que se encuentren activos consultandolos por id
     * de tramite
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredio> buscarPprediosCond9PorIdTramite(Long idTramite);

    /**
     * MÃ©todo para redondear un nÃºmero a 1000
     *
     * @author leidy.gonzalez
     * @param numero numero double que se desea redondear
     */
    public void redondearNumeroAMil(double numero);

    /**
     * Obtiene todos lo predios asociados a una ficha matriz consultandolos por id de la ficha
     * matriz del tramite
     *
     * @author leidy.gonzalez
     * @param fichaTramId
     * @return
     */
    public List<PPredio> obtenerPrediosProyectadosAsociadosAFicha(Long fichaTramId);

    /**
     * Obtiene una lista de PPredio que fueron modificados consultandolos por id de tramite
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredio> buscarPPrediosModPorTramiteId(Long tramId, Long fichaMatrizId);

    /**
     * Metodo que retorna las ficha matriz asociadas a un tramite de englobe virtual.
     *
     * @author leidy.gonzalez
     *
     * @param numeroPredial
     * @return
     */
    public PFichaMatriz obtenerFichaMatrizTramiteEV(String numeroPredial);

    /**
     * Obtiene una lista de PPredio diferentes a la(s) Ficha Matriz(ces), a condicion de propiedad 0
     * o 5 y que no se encuentren cancelados consultandolos por id de tramite
     *
     * @author leidy.gonzalez
     * @param idTramite
     * @return
     */
    public List<PPredio> buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(
        Long idTramite);

    /**
     * Método usado para almacenar las justificaciones de derecho de propiedad desde la pantalla de
     * proyecciones para englobe virtual
     *
     * @param JustificacionPropiedadDTO
     * @param propietariosNuevosSeleccionados
     * @param usuario
     * @return
     * @author leidy.gonzalez
     */
    public JustificacionPropiedadDTO guardarJustificacionesPropiedadEV(
        JustificacionPropiedadDTO justificacionSeleccionada,
        PPersonaPredio[] propietariosNuevosSeleccionados,
        UsuarioDTO usuario);

    /**
     * Método para consultar una {@link PFichaMatrizTorre} que se encuentra proyectada, viene de una
     * {@link FichaMatrizTorre} en firme de la {@link FichaMatriz}
     *
     * @param torre
     * @param id
     * @return
     */
    public FichaMatrizTorre buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
        Long numeroTorre, Long idFichaMatriz);

    /**
     * Obtiene las zonas correspondientes a un predio
     *
     * @author leidy.gonzalez
     * @param predioId
     * @return
     */
    public List<PredioZona> obtenerZonasPorIdPredio(Long predioId);

    /**
     * Metodo que retorna la direccion principal de una ficha matriz, recibe como parametro el id
     * del predio
     *
     * @author leidy.gonzalez
     * @param predioId
     * @return
     */
    public PPredioDireccion obtenerDireccionPrincipalFichaMatrizId(Long predioId);

    /**
     * Metodo que retorna las p unidades de construccion asociadas a un predio por su id de predio y
     * unidad
     *
     * @author leidy.gonzalez
     * @param idPred, unidad
     * @return
     */
    public PUnidadConstruccion obtenerPUnidadsConstruccionPorIdPredioYUnidad(
        Long idPred, String unidad);

    /**
     * Retorna los predios de los ids dados
     *
     * @author felipe.cadena
     * @param predioIds
     * @return
     */
    public List<Predio> consultarPrediosPorPredioIds(List<Long> predioIds);

    /**
     * Método que realiza la búsqueda de un {@link RepConfigParametroReporte} por su categoria.
     *
     *
     * @author felipe.cadena
     * @param categoria
     * @return
     */
    public List<RepConfigParametroReporte> consultaConfiguracionReportePorCat(String categoria);

    /**
     * Método para llamar el procedimiento almacenado de generación de reportes estadisticos
     *
     * @param repReporteEjecucion
     * @author felipe.cadena
     */
    public Object[] generarReporteEstadistico(RepReporteEjecucion repReporteEjecucion);

    /**
     * Método para almacenar una lista de rango reportes
     *
     * @param rangos
     * @author felipe.cadena
     */
    public void guardarRangoReporteMultiple(List<RangoReporte> rangos);

    //end of interface
    /**
     * retorna primer proyección del Numero Predial
     *
     * @author juan.cruz
     * @param numeroPredial
     * @return
     */
    public HPredio buscarPrimerProyeccionNumeroPredial(String numeroPredial);

    /**
     * retorna la lista de hPredios origen de un trámite, si dentro de los predios origen, encuentra
     * FichasMatriz, solo retornará estas.
     *
     * @author juan.cruz
     * @param tramiteId
     * @return
     */
    public List<HPredio> buscarPrediosOrigenDeTramite(Long tramiteId);

    /**
     * Busca un predio dado su id haciendo fetch de los datos relacionados con la ubicación del
     * predio (pestaña "ubicación" en detalles del predio)
     *
     * @author pedro.garcia
     * @version 2.0
     *
     * @param predioId id del predio buscado
     * @return
     */
    public HPredio obtenerHPredioOrigenDetallado(Long predioId);

    /**
     * Metodo que obtiene una PFichaMatrizPredio por numero predial.
     *
     * @author jleidy.gonzalez
     *
     * @param numeroPredial
     * @return
     */
    public List<PFichaMatrizPredio> obtenerPFichaMatrizPredioContieneNumeroPredial(
        String numeroPredial);

    /**
     * obtiene la lista de números de terreno que están en una manzana
     *
     * @author leidy.gonzalez
     *
     * @param manzana numero predial hasta el componente manzana (17 primeras posiciones del número
     * predial)
     * @return
     */
    public List<String> obtenerNumerosTerrenoPorPredioHastaManzana(String predioAManzana);

    /**
     * obtiene la lista de construcciones en firme de acuerdo a una lista de id de predios enviados
     *
     * @author leidy.gonzalez
     *
     * @param idPUnidadMod ids de predios a consultar construcciones
     * @return
     */
    public List<UnidadConstruccion> obtenerUnidadesConstruccionPorListaId(
        List<Long> idPUnidadMod);

    /**
     * Método que retorna las p unidades de construccion asociadas a una lista de id's
     *
     * @author leidy.gonzalez
     * @param id
     * @return
     */
    public List<PUnidadConstruccion> obtenerPUnidadConstruccionPorIds(List<Long> id);

    /**
     * Método para consultar lista PredioZona por sus id's
     *
     * @author leidy.gonzalez
     * @param id
     * @return
     */
    public List<PredioZona> obtenerZonasPorIds(List<Long> id);

    /**
     * Método para consultar lista PPredioZona por sus id's
     *
     * @author leidy.gonzalez
     * @param id
     * @return
     */
    public List<PPredioZona> obtenerPZonasPorIds(List<Long> id);

    //end of interface                       
}
