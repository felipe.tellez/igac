/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los identificadores de los tabs para revisión de proyección
 *
 * @author juan.agudelo
 */
public enum EIdTabRevisionProyeccion {

    INFORMACION_CANCELACION_INSCRIPCION("infoCanIns"),
    MOTIVACION("motivacion"),
    TRAMITES("tramites"),
    VER_PROYECCION("verProyeccion"),
    VER_RESOLUCION("verResolucion"),
    VISOR_GIS("visor");

    private String id;

    private EIdTabRevisionProyeccion(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
