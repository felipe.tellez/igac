package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the COMISION_ESTADO database table.
 */
/*
 * Se inserta un registro en esta tabla al crear una comisión y cada vez que una comisión cambie de
 * estado para tener el histórico.
 *
 * @modified pedro.garcia - adición de secuencia para el id - cambio de id del documento
 * (memorandoDocumentoId) por el objeto Documento (memorandoDocumento)
 */
@Entity
@Table(name = "COMISION_ESTADO")
public class ComisionEstado implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5109790355811366371L;

    private Long id;
    private String estado;
    private Date fecha;
    private Date fechaFin;
    private Date fechaInicio;
    private Date fechaLog;
    private Documento memorandoDocumento;
    private String motivo;
    private String observacion;
    private String usuarioLog;
    private Comision comision;

    public ComisionEstado() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ComisionEstado_id_seq")
    @SequenceGenerator(name = "ComisionEstado_id_seq", sequenceName = "COMISION_ESTADO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMORANDO_DOCUMENTO_ID")
    public Documento getMemorandoDocumento() {
        return this.memorandoDocumento;
    }

    public void setMemorandoDocumento(Documento memorandoDocumento) {
        this.memorandoDocumento = memorandoDocumento;
    }

    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Comision
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMISION_ID", nullable = false)
    public Comision getComision() {
        return this.comision;
    }

    public void setComision(Comision comision) {
        this.comision = comision;
    }

}
