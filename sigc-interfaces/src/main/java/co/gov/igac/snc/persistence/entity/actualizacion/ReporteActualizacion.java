package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;

/**
 * Entidad relacionada a los reportes de actualizacion
 *
 * @author javier.aponte
 */
@Entity
@Table(name = "AX_REPORTE_ACTUALIZACION")
public class ReporteActualizacion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String codigoReporte;

    private Date fechaGeneracion;

    private Date fechaLog;

    private String idRepositorioDocumentos;

    private Municipio municipio;

    private String nombreReporte;

    private String observaciones;

    private String usuarioLog;

    private ProductoCatastralJob productoCatastralJob;

    public ReporteActualizacion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AX_REPORTE_ACT_ID_SEQ")
    @SequenceGenerator(name = "AX_REPORTE_ACT_ID_SEQ", sequenceName = "AX_REPORTE_ACT_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CODIGO_REPORTE")
    public String getCodigoReporte() {
        return this.codigoReporte;
    }

    public void setCodigoReporte(String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_GENERACION")
    public Date getFechaGeneracion() {
        return this.fechaGeneracion;
    }

    public void setFechaGeneracion(Date fechaGeneracion) {
        this.fechaGeneracion = fechaGeneracion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS")
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_MUNICIPIO")
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "NOMBRE_REPORTE")
    public String getNombreReporte() {
        return this.nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTO_CATASTRAL_JOB_ID")
    public ProductoCatastralJob getProductoCatastralJob() {
        return productoCatastralJob;
    }

    public void setProductoCatastralJob(ProductoCatastralJob productoCatastralJob) {
        this.productoCatastralJob = productoCatastralJob;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
