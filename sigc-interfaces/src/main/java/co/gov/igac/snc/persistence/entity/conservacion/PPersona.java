package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.entity.i11n.Propietario;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * PPersona entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia merge por cambios en campos de teléfono
 * @modified by fredy.wilches se agrego campo transient tipoIdentificacionValor adicion del campo
 * transient bloqueada Cambio de los atributos de Pais, Dpto y Municipio a pojos
 * @author fabio.navarrete -> Se agregó serialVersionUID -> Se agregó el método y atributo transient
 * para consultar el valor de la propiedad cancelaInscribe -> Se agrega método transient
 * isTipoIdentificacionNIT
 */
@Entity
@Table(name = "P_PERSONA", schema = "SNC_CONSERVACION")
public class PPersona implements java.io.Serializable, IProyeccionObject, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -7940401373822617210L;
    // Fields

    private Long id;
    private String tipoPersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String razonSocial;
    private String sigla;
    private String direccion;
    private Pais direccionPais;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;
    private String telefonoPrincipal;
    private String telefonoPrincipalExt;
    private String telefonoSecundario;
    private String telefonoSecundarioExt;
    private String telefonoCelular;
    private String fax;
    private String faxExt;
    private String correoElectronico;
    private String correoElectronicoSecundario;
    private String estadoCivil;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private List<PPersonaPredio> PPersonaPredios = new ArrayList<PPersonaPredio>();

    private String cancelaInscribeValor;

    private String tipoIdentificacionValor;

    // Constructors
    /** default constructor */
    public PPersona() {
    }

    /** minimal constructor */
    public PPersona(Long id, String tipoPersona, String tipoIdentificacion,
        String numeroIdentificacion, String cancelaInscribe,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PPersona(Long id, String tipoPersona, String tipoIdentificacion,
        String numeroIdentificacion, String digitoVerificacion,
        String primerNombre, String segundoNombre, String primerApellido,
        String segundoApellido, String razonSocial, String sigla,
        String direccion, Pais direccionPais,
        Departamento direccionDepartamento,
        Municipio direccionMunicipio,
        String telefonoPrincipal,
        String telefonoPrincipalExt, String telefonoSecundario,
        String telefonoSecundarioExt, String telefonoCelular, String fax, String faxExt,
        String correoElectronico, String correoElectronicoSecundario,
        String estadoCivil, String cancelaInscribe, String usuarioLog,
        Date fechaLog, List<PPersonaPredio> PPersonaPredios) {
        this.id = id;
        this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.digitoVerificacion = digitoVerificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.razonSocial = razonSocial;
        this.sigla = sigla;
        this.direccion = direccion;
        this.direccionPais = direccionPais;
        this.direccionDepartamento = direccionDepartamento;
        this.direccionMunicipio = direccionMunicipio;
        this.telefonoPrincipal = telefonoPrincipal;
        this.telefonoPrincipalExt = telefonoPrincipalExt;
        this.telefonoSecundario = telefonoSecundario;
        this.telefonoSecundarioExt = telefonoSecundarioExt;
        this.telefonoCelular = telefonoCelular;
        this.fax = fax;
        this.faxExt = faxExt;
        this.correoElectronico = correoElectronico;
        this.correoElectronicoSecundario = correoElectronicoSecundario;
        this.estadoCivil = estadoCivil;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.PPersonaPredios = PPersonaPredios;
    }

    public PPersona(Propietario p) {
        this.tipoIdentificacion = p.getTipoDocumento();
        this.tipoPersona = p.getRazonSocial() != null && !p.getRazonSocial().equals("") ?
            EPersonaTipoPersona.JURIDICA.getCodigo() : EPersonaTipoPersona.NATURAL.getCodigo();
        this.numeroIdentificacion = p.getNumeroDocumento();
        this.fechaLog = new Date();
        this.usuarioLog = "IPER";
        this.primerNombre = p.getNombre1();
        this.segundoNombre = p.getNombre2();
        this.primerApellido = p.getApellido1();
        this.segundoApellido = p.getApellido2();
        this.razonSocial = p.getRazonSocial();
    }

    // Property accessors
    @Id
    @GeneratedValue(generator = "PERSONA_ID_SEQ")
    //@SequenceGenerator(name="PERSONA_ID_SEQ", sequenceName = "PERSONA_ID_SEQ", allocationSize=1)
    @GenericGenerator(name = "PERSONA_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {@Parameter(
                name = "sequence", value = "PERSONA_ID_SEQ"), @Parameter(name = "allocationSize",
                value = "1")})
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_PERSONA", nullable = false, length = 30)
    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION", nullable = false, length = 50)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "DIGITO_VERIFICACION", length = 2)
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    @Column(name = "PRIMER_NOMBRE", length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "PRIMER_APELLIDO", length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "RAZON_SOCIAL", length = 100)
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "SIGLA", length = 50)
    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "DIRECCION", length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_PAIS_CODIGO", nullable = true)
    public Pais getDireccionPais() {
        return this.direccionPais;
    }

    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    public void setDireccionDepartamento(
        Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_MUNICIPIO_CODIGO", nullable = true)
    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    @Column(name = "TELEFONO_PRINCIPAL", length = 15)
    public String getTelefonoPrincipal() {
        return this.telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    @Column(name = "TELEFONO_PRINCIPAL_EXT", length = 6)
    public String getTelefonoPrincipalExt() {
        return this.telefonoPrincipalExt;
    }

    public void setTelefonoPrincipalExt(String telefonoPrincipalExt) {
        this.telefonoPrincipalExt = telefonoPrincipalExt;
    }

    @Column(name = "TELEFONO_SECUNDARIO", length = 15)
    public String getTelefonoSecundario() {
        return this.telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    @Column(name = "TELEFONO_SECUNDARIO_EXT", length = 6)
    public String getTelefonoSecundarioExt() {
        return this.telefonoSecundarioExt;
    }

    public void setTelefonoSecundarioExt(String telefonoSecundarioExt) {
        this.telefonoSecundarioExt = telefonoSecundarioExt;
    }

    @Column(name = "TELEFONO_CELULAR", length = 15)
    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    @Column(name = "FAX", length = 15)
    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "FAX_EXT", length = 6)
    public String getFaxExt() {
        return this.faxExt;
    }

    public void setFaxExt(String faxExt) {
        this.faxExt = faxExt;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "CORREO_ELECTRONICO_SECUNDARIO", length = 100)
    public String getCorreoElectronicoSecundario() {
        return this.correoElectronicoSecundario;
    }

    public void setCorreoElectronicoSecundario(
        String correoElectronicoSecundario) {
        this.correoElectronicoSecundario = correoElectronicoSecundario;
    }

    @Column(name = "ESTADO_CIVIL", length = 30)
    public String getEstadoCivil() {
        return this.estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPersona")
    public List<PPersonaPredio> getPPersonaPredios() {
        return this.PPersonaPredios;
    }

    public void setPPersonaPredios(List<PPersonaPredio> PPersonaPredios) {
        this.PPersonaPredios = PPersonaPredios;
    }

    @Transient
    public String getTipoIdentificacionValor() {
        return tipoIdentificacionValor;
    }

    public void setTipoIdentificacionValor(String tipoIdentificacionValor) {
        this.tipoIdentificacionValor = tipoIdentificacionValor;
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    /**
     * Método encargado de concatenar el nombre de una persona si es natural y retornar la razón
     * social de la persona si es jurídica.
     *
     * @return Se retorna el nombre o razón social
     */
    @Transient
    public String getNombre() {
        String name = "";

        if ((this.tipoPersona != null && this.tipoPersona.equals("JURIDICA")) ||
            (this.razonSocial != null && !this.razonSocial.equals(""))) {
            name = this.razonSocial;
        } else {
            if (this.primerNombre != null) {
                name = this.primerNombre.concat(" ");
            }
            if (this.segundoNombre != null) {
                name = name.concat(this.segundoNombre).concat(" ");
            }
            if (this.primerApellido != null) {
                name = name.concat(this.primerApellido).concat(" ");
            }
            if (this.segundoApellido != null) {
                name = name.concat(this.segundoApellido).concat(" ");
            }
        }
        return name != null ? name.trim() : "";
    }

    public void incorporarDatosPersona(Persona persona) {
        this.correoElectronico = persona.getCorreoElectronico();
        this.correoElectronicoSecundario = persona.getCorreoElectronicoSecundario();
        this.digitoVerificacion = persona.getDigitoVerificacion();
        this.direccion = persona.getDireccion();
        this.direccionDepartamento = persona.getDireccionDepartamento();
        this.direccionMunicipio = persona.getDireccionMunicipio();
        this.direccionPais = persona.getDireccionPais();
        this.estadoCivil = persona.getEstadoCivil();
        this.fax = persona.getFax();
        this.faxExt = persona.getFaxExt();
        this.id = persona.getId();
        this.numeroIdentificacion = persona.getNumeroIdentificacion();
        this.primerApellido = persona.getPrimerApellido();
        this.primerNombre = persona.getPrimerNombre();
        this.razonSocial = persona.getRazonSocial();
        this.segundoApellido = persona.getSegundoApellido();
        this.segundoNombre = persona.getSegundoNombre();
        this.sigla = persona.getSigla();
        this.telefonoCelular = persona.getTelefonoCelular();
        this.telefonoPrincipal = persona.getTelefonoPrincipal();
        this.telefonoPrincipalExt = persona.getTelefonoPrincipalExt();
        this.telefonoSecundario = persona.getTelefonoSecundario();
        this.telefonoSecundarioExt = persona.getTelefonoSecundarioExt();
        this.tipoIdentificacion = persona.getTipoIdentificacion();
        this.tipoIdentificacionValor = persona.getTipoIdentificacionValor();
        this.tipoPersona = persona.getTipoPersona();
    }

    public void copiarPPersona(PPersona ppersona) {
        this.correoElectronico = ppersona.getCorreoElectronico();
        this.correoElectronicoSecundario = ppersona.getCorreoElectronicoSecundario();
        this.digitoVerificacion = ppersona.getDigitoVerificacion();
        this.direccion = ppersona.getDireccion();
        this.direccionDepartamento = ppersona.getDireccionDepartamento();
        this.direccionMunicipio = ppersona.getDireccionMunicipio();
        this.direccionPais = ppersona.getDireccionPais();
        this.estadoCivil = ppersona.getEstadoCivil();
        this.fax = ppersona.getFax();
        this.faxExt = ppersona.getFaxExt();
        this.id = ppersona.getId();
        this.numeroIdentificacion = ppersona.getNumeroIdentificacion();
        this.primerApellido = ppersona.getPrimerApellido();
        this.primerNombre = ppersona.getPrimerNombre();
        this.razonSocial = ppersona.getRazonSocial();
        this.segundoApellido = ppersona.getSegundoApellido();
        this.segundoNombre = ppersona.getSegundoNombre();
        this.sigla = ppersona.getSigla();
        this.telefonoCelular = ppersona.getTelefonoCelular();
        this.telefonoPrincipal = ppersona.getTelefonoPrincipal();
        this.telefonoPrincipalExt = ppersona.getTelefonoPrincipalExt();
        this.telefonoSecundario = ppersona.getTelefonoSecundario();
        this.telefonoSecundarioExt = ppersona.getTelefonoSecundarioExt();
        this.tipoIdentificacion = ppersona.getTipoIdentificacion();
        this.tipoIdentificacionValor = ppersona.getTipoIdentificacionValor();
        this.tipoPersona = ppersona.getTipoPersona();
    }

    /* @PostLoad public void postLoad(){ if (this.direccionPais==null){ this.direccionPais=new
     * Pais(); }
     *
     * } */
    /**
     * Método que identifica si el tipo de indentificación del objeto PPersona es NIT
     *
     * @author fabio.navarrete
     * @return
     */
    @Transient
    public boolean isTipoIdentificacionNIT() {
        if (this.tipoIdentificacion != null) {
            return this.tipoIdentificacion
                .equals(EPersonaTipoIdentificacion.NIT.getCodigo());
        }
        return false;
    }

    /**
     * @author juanfelipe.garcia Método que realiza un clone del objeto pPersonaPredio.
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que indica si el tipo de documento es NUIP
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isTipoIdentificacionNUIP() {

        if (this.tipoIdentificacion != null) {
            if (this.tipoIdentificacion.equals(EPersonaTipoIdentificacion.NUIP.getCodigo())) {
                return true;
            }
        }
        return false;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que indica si el tipo de documento es Pasaporte
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isTipoIdentificacionPasaporte() {

        if (this.tipoIdentificacion != null) {
            if (this.tipoIdentificacion.equals(EPersonaTipoIdentificacion.PASAPORTE.getCodigo())) {
                return true;
            }
        }
        return false;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que indica si el tipo de documento es cedula de extranjeria
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isTipoIdentificacionCedulaExtranjeria() {

        if (this.tipoIdentificacion != null) {
            if (this.tipoIdentificacion.equals(EPersonaTipoIdentificacion.CEDULA_EXTRANJERIA.
                getCodigo())) {
                return true;
            }
        }
        return false;

    }

}
