package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;

/**
 * Conjunto de listas con los resultados de los predios bloqueados y los objetos fallidos de un
 * bloqueo masivo
 *
 * @author juan.agudelo
 *
 */
public class ResultadoBloqueoMasivo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9049882450821999340L;

    private List<PredioBloqueo> bloqueoMasivoPrediosExitoso;
    private List<PersonaBloqueo> bloqueoMasivoPersonasExitoso;
    private List<ErrorProcesoMasivo> erroresProcesoMasivo;

    public List<PredioBloqueo> getBloqueoMasivoPrediosExitoso() {
        return bloqueoMasivoPrediosExitoso;
    }

    public void setBloqueoMasivoPrediosExitoso(
        List<PredioBloqueo> bloqueoMasivoPrediosExitoso) {
        this.bloqueoMasivoPrediosExitoso = bloqueoMasivoPrediosExitoso;
    }

    public List<PersonaBloqueo> getBloqueoMasivoPersonasExitoso() {
        return bloqueoMasivoPersonasExitoso;
    }

    public void setBloqueoMasivoPersonasExitoso(
        List<PersonaBloqueo> bloqueoMasivoPersonasExitoso) {
        this.bloqueoMasivoPersonasExitoso = bloqueoMasivoPersonasExitoso;
    }

    public List<ErrorProcesoMasivo> getErroresProcesoMasivo() {
        return erroresProcesoMasivo;
    }

    public void setErroresProcesoMasivo(
        List<ErrorProcesoMasivo> erroresProcesoMasivo) {
        this.erroresProcesoMasivo = erroresProcesoMasivo;
    }

    public ResultadoBloqueoMasivo(
        List<PredioBloqueo> bloqueoMasivoPrediosExitoso,
        List<PersonaBloqueo> bloqueoMasivoPersonasExitoso,
        List<ErrorProcesoMasivo> erroresProcesoMasivo) {
        super();
        this.bloqueoMasivoPrediosExitoso = bloqueoMasivoPrediosExitoso;
        this.bloqueoMasivoPersonasExitoso = bloqueoMasivoPersonasExitoso;
        this.erroresProcesoMasivo = erroresProcesoMasivo;
    }

}
