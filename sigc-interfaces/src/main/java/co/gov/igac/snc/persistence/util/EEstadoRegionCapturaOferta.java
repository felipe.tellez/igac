package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de AREA_CAPTURA_DETALLE_EST
 *
 * @author rodrigo.hernandez
 */
public enum EEstadoRegionCapturaOferta {
    ASIGNADA("ASIGNADA"),
    CREADA("CREADA"),
    CONTROL_CALIDAD("CONTROL CALIDAD"),
    EN_COMISION("EN COMISION"),
    EN_RECOLECCION("EN RECOLECCION"),
    FINALIZADA("FINALIZADA");

    private String valor;

    private EEstadoRegionCapturaOferta(String valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return this.valor;
    }
}
