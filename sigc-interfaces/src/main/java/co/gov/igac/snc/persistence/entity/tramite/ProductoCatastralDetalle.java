package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;

/**
 * The persistent class for the PRODUCTO_CATASTRAL_DETALLE database table. modified by
 * leidy.gonzalez Se modifica el tipo de dato del campo anio Y se agrega ingreso de nuevas
 * validaciones para visualizar en las resoluciones con el campo NombreProductoDetalle modified by
 * leidy.gonzalez 29/08/2014 Se agrega interfaz clonable al al objeto Producto Catastral Detalle
 * modified by leidy.gonzalez 11/09/2014 Se cambia el tipo de dato para la columna
 * productoDocumentoId dejandola como tipo Documento y agregandole el tipo de mapeo ManyToOne
 */
@Entity
@Table(name = "PRODUCTO_CATASTRAL_DETALLE", schema = "SNC_TRAMITE")
public class ProductoCatastralDetalle implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue
    private Long id;
    private String aSolicitudDe;
    private Integer anio;
    private String conDestinoA;
    private String datosCertificar;
    private String departamentoCodigo;
    private String escala;
    private Date fechaLog;
    private Date fechaResolucionFinal;
    private Date fechaResolucionInicial;
    private String formato;
    private String municipioCodigo;
    private String numeroIdentificacion;
    private String numeroPlancha;
    private String numeroPredial;
    private String numeroPredialFinal;
    private String numeroPredialInicial;
    private String numeroResolucion;
    private String numeroResolucionFinal;
    private String numeroResolucionInicial;
    private String observaciones;
    private String otro;
    private String primerApellido;
    private String primerNombre;
    private Documento productoDocumentoId;
    private String razonSocial;
    private String segundoApellido;
    private String segundoNombre;
    private String sigla;
    private String tipoConstruccion;
    private String tipoIdentificacion;
    private String tipoPersona;
    private Double usoConstruccionId;
    private String usuarioLog;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private String zonaUnidadOrganica;
    private ProductoCatastral productoCatastral;
    private String tramiteCatastralDescripcion;
    private String tramiteCatastralNota;
    private String tramiteCatastralOtro;
    private String depuracionGeoDescripcion;
    private String depuracionGeoNota;
    private String depuracionGeoOtro;
    private String digitoVerificacion;
    private String rutaProductoEjecutado;
    private String nombreProductoDetalle;
    private String pcdFinAprobado;

    public ProductoCatastralDetalle() {
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCTO_CATASTRAL_DETA_ID_SEQ")
    @SequenceGenerator(name = "PRODUCTO_CATASTRAL_DETA_ID_SEQ", sequenceName =
        "PRODUCTO_CATASTRAL_DETA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "A_SOLICITUD_DE", length = 30)
    public String getASolicitudDe() {
        return this.aSolicitudDe;
    }

    public void setASolicitudDe(String aSolicitudDe) {
        this.aSolicitudDe = aSolicitudDe;
    }

    @Column(name = "ANIO", precision = 4, scale = 0, length = 4)
    public Integer getAnio() {
        return this.anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "CON_DESTINO_A", length = 30)
    public String getConDestinoA() {
        return this.conDestinoA;
    }

    public void setConDestinoA(String conDestinoA) {
        this.conDestinoA = conDestinoA;
    }

    @Column(name = "DATOS_CERTIFICAR", length = 1000)
    public String getDatosCertificar() {
        return this.datosCertificar;
    }

    public void setDatosCertificar(String datosCertificar) {
        this.datosCertificar = datosCertificar;
    }

    @Column(name = "DEPARTAMENTO_CODIGO", length = 2)
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    @Column(length = 30)
    public String getEscala() {
        return this.escala;
    }

    public void setEscala(String escala) {
        this.escala = escala;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RESOLUCION_FINAL")
    public Date getFechaResolucionFinal() {
        return this.fechaResolucionFinal;
    }

    public void setFechaResolucionFinal(Date fechaResolucionFinal) {
        this.fechaResolucionFinal = fechaResolucionFinal;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RESOLUCION_INICIAL")
    public Date getFechaResolucionInicial() {
        return this.fechaResolucionInicial;
    }

    public void setFechaResolucionInicial(Date fechaResolucionInicial) {
        this.fechaResolucionInicial = fechaResolucionInicial;
    }

    @Column(length = 50)
    public String getFormato() {
        return this.formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    @Column(name = "MUNICIPIO_CODIGO", length = 5)
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "NUMERO_IDENTIFICACION", length = 50)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "NUMERO_PLANCHA", length = 30)
    public String getNumeroPlancha() {
        return this.numeroPlancha;
    }

    public void setNumeroPlancha(String numeroPlancha) {
        this.numeroPlancha = numeroPlancha;
    }

    @Column(name = "NUMERO_PREDIAL", length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_FINAL")
    public String getNumeroPredialFinal() {
        return this.numeroPredialFinal;
    }

    public void setNumeroPredialFinal(String numeroPredialFinal) {
        this.numeroPredialFinal = numeroPredialFinal;
    }

    @Column(name = "NUMERO_PREDIAL_INICIAL")
    public String getNumeroPredialInicial() {
        return this.numeroPredialInicial;
    }

    public void setNumeroPredialInicial(String numeroPredialInicial) {
        this.numeroPredialInicial = numeroPredialInicial;
    }

    @Column(name = "NUMERO_RESOLUCION", length = 20)
    public String getNumeroResolucion() {
        return this.numeroResolucion;
    }

    public void setNumeroResolucion(String numeroResolucion) {
        this.numeroResolucion = numeroResolucion;
    }

    @Column(name = "NUMERO_RESOLUCION_FINAL")
    public String getNumeroResolucionFinal() {
        return this.numeroResolucionFinal;
    }

    public void setNumeroResolucionFinal(String numeroResolucionFinal) {
        this.numeroResolucionFinal = numeroResolucionFinal;
    }

    @Column(name = "NUMERO_RESOLUCION_INICIAL")
    public String getNumeroResolucionInicial() {
        return this.numeroResolucionInicial;
    }

    public void setNumeroResolucionInicial(String numeroResolucionInicial) {
        this.numeroResolucionInicial = numeroResolucionInicial;
    }

    @Column(length = 1000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(length = 100)
    public String getOtro() {
        return this.otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    @Column(name = "PRIMER_APELLIDO", length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "PRIMER_NOMBRE", length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTO_DOCUMENTO_ID")
    public Documento getProductoDocumentoId() {
        return this.productoDocumentoId;
    }

    public void setProductoDocumentoId(Documento productoDocumentoId) {
        this.productoDocumentoId = productoDocumentoId;
    }

    @Column(name = "RAZON_SOCIAL", length = 100)
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(length = 50)
    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "TIPO_CONSTRUCCION", length = 30)
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    @Column(name = "TIPO_IDENTIFICACION", length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "TIPO_PERSONA", length = 30)
    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    @Column(name = "USO_CONSTRUCCION_ID", precision = 10)
    public Double getUsoConstruccionId() {
        return this.usoConstruccionId;
    }

    public void setUsoConstruccionId(Double usoConstruccionId) {
        this.usoConstruccionId = usoConstruccionId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "ZONA_FISICA", length = 4)
    public String getZonaFisica() {
        return this.zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

    @Column(name = "ZONA_GEOECONOMICA", length = 4)
    public String getZonaGeoeconomica() {
        return this.zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", length = 30)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    // bi-directional many-to-one association to ProductoCatastral
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTO_CATASTRAL_ID", nullable = false)
    public ProductoCatastral getProductoCatastral() {
        return this.productoCatastral;
    }

    public void setProductoCatastral(ProductoCatastral productoCatastral) {
        this.productoCatastral = productoCatastral;
    }

    @Column(name = "TRAMITE_CATASTRAL_DESCRIPCION")
    public String getTramiteCatastralDescripcion() {
        return this.tramiteCatastralDescripcion;
    }

    public void setTramiteCatastralDescripcion(String tramiteCatastralDescripcion) {
        this.tramiteCatastralDescripcion = tramiteCatastralDescripcion;
    }

    @Column(name = "TRAMITE_CATASTRAL_NOTA")
    public String getTramiteCatastralNota() {
        return this.tramiteCatastralNota;
    }

    public void setTramiteCatastralNota(String tramiteCatastralNota) {
        this.tramiteCatastralNota = tramiteCatastralNota;
    }

    @Column(name = "TRAMITE_CATASTRAL_OTRO")
    public String getTramiteCatastralOtro() {
        return this.tramiteCatastralOtro;
    }

    public void setTramiteCatastralOtro(String tramiteCatastralOtro) {
        this.tramiteCatastralOtro = tramiteCatastralOtro;
    }

    @Column(name = "DEPURACION_GEO_DESCRIPCION")
    public String getDepuracionGeoDescripcion() {
        return this.depuracionGeoDescripcion;
    }

    public void setDepuracionGeoDescripcion(String depuracionGeoDescripcion) {
        this.depuracionGeoDescripcion = depuracionGeoDescripcion;
    }

    @Column(name = "DEPURACION_GEO_NOTA")
    public String getDepuracionGeoNota() {
        return this.depuracionGeoNota;
    }

    public void setDepuracionGeoNota(String depuracionGeoNota) {
        this.depuracionGeoNota = depuracionGeoNota;
    }

    @Column(name = "DEPURACION_GEO_OTRO")
    public String getDepuracionGeoOtro() {
        return this.depuracionGeoOtro;
    }

    public void setDepuracionGeoOtro(String depuracionGeoOtro) {
        this.depuracionGeoOtro = depuracionGeoOtro;
    }

    @Column(name = "DIGITO_VERIFICACION")
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    @Column(name = "RUTA_PRODUCTO_EJECUTADO")
    public String getRutaProductoEjecutado() {
        return this.rutaProductoEjecutado;
    }

    public void setRutaProductoEjecutado(String rutaProductoEjecutado) {
        this.rutaProductoEjecutado = rutaProductoEjecutado;
    }

    /**
     * Método encargado de determinar si el tipo de identificación es NIT
     *
     * @author javier.aponte
     */
    @Transient
    public boolean isEsNIT() {

        if (this.tipoIdentificacion != null) {
            if (EPersonaTipoIdentificacion.NIT.getCodigo().equals(this.tipoIdentificacion)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método encargado de determinar si el tipo de persona es persona natural
     *
     * @author javier.aponte
     */
    @Transient
    public boolean isPersonaNatural() {

        if (this.tipoPersona != null) {
            if (EPersonaTipoPersona.NATURAL.getCodigo().equals(this.tipoPersona)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método encargado de determinar si el tipo de persona es persona juridica
     *
     * @author javier.aponte
     */
    @Transient
    public boolean isPersonaJuridica() {

        if (this.tipoPersona != null) {
            if (EPersonaTipoPersona.JURIDICA.getCodigo().equals(this.tipoPersona)) {
                return true;
            }
        }
        return false;
    }

    public void setNombreProductoDetalle(String nombreProductoDetalle) {
        this.nombreProductoDetalle = nombreProductoDetalle;
    }

    /**
     * Método encargado de determinar el nombre del producto detalle
     *
     * @author leidy.gonzalez
     * @modified by leidy.gonzalez 26/08/2014 Redmine: 9008 Se modifica el valor de la variable
     * nombreProductoDetalle cuando existe numeroPredialFinal, numeroResolucionFinal y
     * fechaResolucionFinal
     */
    @Transient
    public String getNombreProductoDetalle() {

        if (this.tipoIdentificacion != null &&
            this.numeroIdentificacion != null) {
            this.nombreProductoDetalle = this.tipoIdentificacion + this.numeroIdentificacion;
        } else if (this.numeroPredial != null) {
            this.nombreProductoDetalle = this.numeroPredial;
            if (this.numeroPredialFinal != null) {
                this.nombreProductoDetalle = this.numeroPredial + " -\r" + this.numeroPredialFinal;
            }
        } else if (this.numeroResolucion != null) {
            this.nombreProductoDetalle = this.numeroResolucion;
        }
        if (this.numeroResolucionFinal != null) {
            this.nombreProductoDetalle = this.numeroResolucion + " -\r" + this.numeroResolucionFinal;
        } else if (this.fechaResolucionInicial != null) {
            this.nombreProductoDetalle = this.fechaResolucionInicial.toString();
        }
        if (this.fechaResolucionFinal != null) {
            this.nombreProductoDetalle = this.fechaResolucionInicial.toString() + " -\r" +
                this.fechaResolucionFinal.toString();
        }

        return this.nombreProductoDetalle;
    }

    /**
     * @author leidy.gonzalez Método para poder hacer un clon de producto catastral detalle
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    @Column(name = "PCD_FIN_APROBADO", length = 2)
    public String getPcdFinAprobado() {
        return pcdFinAprobado;
    }

    public void setPcdFinAprobado(String pcdFinAprobado) {
        this.pcdFinAprobado = pcdFinAprobado;
    }

}
