package co.gov.igac.snc.util;

/**
 * Enumeración con los posibles valores para Programa del dominio PROGRAMA_ERROR_PROC_MASIVO
 *
 * @author juan.agudelo
 */
public enum EProgramaGeneraError {

    //D: valor ("codigo")
    PERSONA_BLOQUEO("1"),
    PREDIO_BLOQUEO("2");

    private String codigo;

    EProgramaGeneraError(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
