package co.gov.igac.snc.fachadas;

import javax.ejb.Remote;

/**
 * Interfaz Local para el acceso a los servicios de IGenerales
 *
 * Nota: Los Métodos se declaran en la interfaz Local (IGeneralesLocal)
 *
 * @author juan.mendez
 *
 */
@Remote
public interface IGenerales extends IGeneralesLocal {

}
