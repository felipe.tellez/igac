package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * Clase para la serializacion de los xml de lista de tareas geograficas para la edicion almacena
 * una direccion
 *
 * @author franz.gamba
 *
 */
public class PredioDireccionVO implements Serializable {

    private static final long serialVersionUID = 2448449379567352225L;
    private String valor;
    private Boolean principal;

    public PredioDireccionVO(String valor) {
        this.valor = valor;
        this.principal = false;
    }

    public PredioDireccionVO(String valor, Boolean principal) {

        this.valor = valor;
        this.principal = true;

    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

}
