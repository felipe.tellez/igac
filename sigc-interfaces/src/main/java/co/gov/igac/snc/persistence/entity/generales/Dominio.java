package co.gov.igac.snc.persistence.entity.generales;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;

/**
 * Dominio entity. @author MyEclipse Persistence Tools FGWL Comparator3, 2, Valor y codigo
 */
@Entity
@Table(name = "DOMINIO", schema = "SNC_GENERALES")
public class Dominio implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 5498691801604868902L;

    @GeneratedValue
    private Long id;
    private String nombre;
    private String codigo;
    private String valor;
    private String descripcion;
    private String tipoDato;
    private String activo;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public Dominio() {
    }

    /** minimal constructor */
    public Dominio(Long id, String nombre, String codigo, String tipoDato,
        String activo, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
        this.tipoDato = tipoDato;
        this.activo = activo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Dominio(Long id, String nombre, String codigo, String valor,
        String descripcion, String tipoDato, String activo,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
        this.valor = valor;
        this.descripcion = descripcion;
        this.tipoDato = tipoDato;
        this.activo = activo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "CODIGO", nullable = false, length = 30)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "VALOR", length = 250)
    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Column(name = "DESCRIPCION", length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "TIPO_DATO", nullable = false, length = 30)
    public String getTipoDato() {
        return this.tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    @Column(name = "ACTIVO", nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    /**
     * Metodo utilizado para ordenar una lista de Dominio por el campo codigo con tres digitos
     *
     * @return
     */
    public static Comparator<Dominio> getComparator3() {
        return new Comparator<Dominio>() {
            public int compare(Dominio d1, Dominio d2) {
                return StringUtils.leftPad(d1.codigo, 3, '0').compareTo(StringUtils.leftPad(
                    d2.codigo, 3, '0'));
            }
        };
    }

    /**
     * Metodo utilizado para ordenar una lista de Dominio por el campo codigo con dos digitos
     *
     * @return
     */
    public static Comparator<Dominio> getComparator2() {
        return new Comparator<Dominio>() {
            public int compare(Dominio d1, Dominio d2) {
                return StringUtils.leftPad(d1.codigo, 2, '0').compareTo(StringUtils.leftPad(
                    d2.codigo, 2, '0'));
            }
        };
    }

    /**
     * Metodo utilizado para ordenar una lista de Dominio por el campo valor
     *
     * @return
     */
    public static Comparator<Dominio> getComparatorValor() {
        return new Comparator<Dominio>() {
            public int compare(Dominio d1, Dominio d2) {
                return d1.valor.compareTo(d2.valor);
            }
        };
    }

}
