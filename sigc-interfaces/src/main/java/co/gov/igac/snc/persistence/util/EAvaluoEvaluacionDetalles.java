package co.gov.igac.snc.persistence.util;

/**
 * Enumeración usada en CU-SA-AC-024 - Evaluar Desempeño profesional del avaluador, para establecer
 * que variable de la evaluacion de tipo "EFICIENCIA" o "EFICACIA" va a ser editada teniendo en
 * cuenta el numero de dias de atraso del avaluo y el numero de devoluciones
 */
public enum EAvaluoEvaluacionDetalles {
    /*
     * Detalles de tipo EFICACIA
     */
    FECHA_ACORDADA("En la Fecha Acordada", 0),
    UN_DIA_ATRASO("1 Día de Atraso", 1),
    DOS_DIAS_ATRASO("2 Días de atraso", 2),
    TRES_O_MAS_DIAS_ATRASO("3 Días de Atraso", 3),
    /*
     * Detalles de tipo EFICIENCIA
     */
    SIN_DEVOLUCIONES("0 Devoluciones", 0),
    UNA_DEVOLUCION("1 Devolución", 1),
    DOS_DEVOLUCIONES("2 Devoluciones", 2),
    SIN_RESPUESTA_NO_APROBACION("Sin Respuesta o No aprobación", 3);

    private String nombreDetalle;
    private int valorDetalle;

    private EAvaluoEvaluacionDetalles(String nombreDetalle, int valorDetalle) {

        this.nombreDetalle = nombreDetalle;
        this.valorDetalle = valorDetalle;
    }

    public String getNombreDetalle() {
        return nombreDetalle;
    }

    public int getValorDetalle() {
        return valorDetalle;
    }
}
