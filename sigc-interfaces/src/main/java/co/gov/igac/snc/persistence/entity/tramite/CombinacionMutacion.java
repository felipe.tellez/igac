package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CombinacionMutacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "COMBINACION_MUTACION", schema = "SNC_TRAMITE")
public class CombinacionMutacion implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private String clase1;
    private String subtipo1;
    private String clase2;
    private String subtipo2;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public CombinacionMutacion() {
    }

    /** minimal constructor */
    public CombinacionMutacion(Long id, String clase1, String clase2) {
        this.id = id;
        this.clase1 = clase1;
        this.clase2 = clase2;
    }

    /** full constructor */
    public CombinacionMutacion(Long id, String clase1, String subtipo1,
        String clase2, String subtipo2, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.clase1 = clase1;
        this.subtipo1 = subtipo1;
        this.clase2 = clase2;
        this.subtipo2 = subtipo2;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CLASE1", nullable = false, length = 20)
    public String getClase1() {
        return this.clase1;
    }

    public void setClase1(String clase1) {
        this.clase1 = clase1;
    }

    @Column(name = "SUBTIPO1", length = 30)
    public String getSubtipo1() {
        return this.subtipo1;
    }

    public void setSubtipo1(String subtipo1) {
        this.subtipo1 = subtipo1;
    }

    @Column(name = "CLASE2", nullable = false, length = 20)
    public String getClase2() {
        return this.clase2;
    }

    public void setClase2(String clase2) {
        this.clase2 = clase2;
    }

    @Column(name = "SUBTIPO2", length = 30)
    public String getSubtipo2() {
        return this.subtipo2;
    }

    public void setSubtipo2(String subtipo2) {
        this.subtipo2 = subtipo2;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
