package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/*
 * Proyecto SNC 2016
 */
/**
 * Clase asociada a los procesos de actualizacion express por municipio
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "AX_MUNICIPIO")
public class AxMunicipio implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 4262970936756309656L;

// Campos bd
    private Long id;

    private String departamentoCodigo;

    private String municipioCodigo;

    private Date vigencia;

    private Date fechaInicio;

    private Date fechaFin;

    private String estadoTramites;

    private Date fechaCierreTramites;

    private String estadoTablas;

    private Date fechaCierreTablas;

    private String usuarioLog;

    private Date fechaLog;

// Campos transient(Deben tener autor y descripcion)
    /** default constructor */
    public AxMunicipio() {
    }
// Metodos

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AX_MUNICIPIO_ID_SEQ_GEN")
    @SequenceGenerator(name = "AX_MUNICIPIO_ID_SEQ_GEN", sequenceName = "AX_MUNICIPIO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DEPARTAMENTO_CODIGO")
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    @Column(name = "MUNICIPIO_CODIGO")
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "VIGENCIA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "FECHA_INICIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Column(name = "FECHA_FIN")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Column(name = "ESTADO_TRAMITES")
    public String getEstadoTramites() {
        return this.estadoTramites;
    }

    public void setEstadoTramites(String estadoTramites) {
        this.estadoTramites = estadoTramites;
    }

    @Column(name = "FECHA_CIERRE_TRAMITES")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaCierreTramites() {
        return this.fechaCierreTramites;
    }

    public void setFechaCierreTramites(Date fechaCierreTramites) {
        this.fechaCierreTramites = fechaCierreTramites;
    }

    @Column(name = "ESTADO_TABLAS")
    public String getEstadoTablas() {
        return this.estadoTablas;
    }

    public void setEstadoTablas(String estadoTablas) {
        this.estadoTablas = estadoTablas;
    }

    @Column(name = "FECHA_CIERRE_TABLAS")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaCierreTablas() {
        return this.fechaCierreTablas;
    }

    public void setFechaCierreTablas(Date fechaCierreTablas) {
        this.fechaCierreTablas = fechaCierreTablas;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

// Metodos transient(Deben tener autor y descripcion)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AxMunicipio other = (AxMunicipio) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }
}
