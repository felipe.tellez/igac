package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd PRODUCTO_CATASTRAL_ESTADO
 *
 * @author leidy.gonzalez
 */
public enum EProductoCatastralEstado {
    SIN_EJECUTAR("SIN EJECUTAR"),
    EN_EJECUCION("EN EJECUCION"),
    ERROR("ERROR"),
    TERMINADO("TERMINADO"),
    DISPONIBLE_DESCARGA("DISPONIBLE DESCARGA");
    //v1.1.9
    private String codigo;

    private EProductoCatastralEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
