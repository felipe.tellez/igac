package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;

/**
 * The persistent class for the ENTIDAD_MUNICIPAL_FORMATO database table.
 *
 */
@Entity
@Table(name = "ENTIDAD_MUNICIPAL_FORMATO")
public class EntidadMunicipalFormato implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6802600537220589688L;

    private Long id;
    private Date fechaLog;
    private TipoDocumento tipoDocumento;
    private String usuarioLog;
    private EntidadMunicipal entidadMunicipal;

    public EntidadMunicipalFormato() {
    }

    @Id
    @SequenceGenerator(name = "ENTIDAD_MUNICIPAL_FORMA_ID_GENERATOR", sequenceName =
        "ENTIDAD_MUNICIPAL_FORMA_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ENTIDAD_MUNICIPAL_FORMA_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID", nullable = false)
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to EntidadMunicipal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ENTIDAD_MUNICIPAL_ID", nullable = false)
    public EntidadMunicipal getEntidadMunicipal() {
        return this.entidadMunicipal;
    }

    public void setEntidadMunicipal(EntidadMunicipal entidadMunicipal) {
        this.entidadMunicipal = entidadMunicipal;
    }

}
