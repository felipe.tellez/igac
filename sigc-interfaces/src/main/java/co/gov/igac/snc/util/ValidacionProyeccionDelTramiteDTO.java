package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Objeto que almacena la validación de la proyección hecha a un trámite, para cada uno de sus
 * predios en las diferentes secciones a las que aplique dicho trámite.
 *
 * @author david.cifuentes
 */
public class ValidacionProyeccionDelTramiteDTO implements Serializable {

    private static final long serialVersionUID = -7457063150673208637L;

    /**
     * Datos
     */
    private Long tramiteId;
    private Long predioId;
    private String seccion;
    private String mensajeError;

    /**
     * Constructor
     */
    public ValidacionProyeccionDelTramiteDTO() {
        tramiteId = 0L;
        predioId = 0L;
        seccion = "";
        mensajeError = "";
    }

    public Long getTramiteId() {
        return tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    public Long getPredioId() {
        return predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

}
