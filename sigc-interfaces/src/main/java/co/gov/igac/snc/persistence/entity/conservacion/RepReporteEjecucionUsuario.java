/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * Clase entidad para representar la tabla REP_REPORTE_EJECUCION_USUARIO
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "REP_REPORTE_EJECUCION_USUARIO")
public class RepReporteEjecucionUsuario implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 4262970936756309656L;

// Campos bd
    private Long id;

    private Long reporteEjecucionId;

    private String usuarioGenera;

    private String usuarioLog;

    private Date fechaLog;

// Campos transient(Deben tener autor y descripcion)
    /**
     * default constructor
     */
    public RepReporteEjecucionUsuario() {
    }
// Metodos

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "REP_EJECUCION_USUARIO_ID_SEQ_GEN")
    @SequenceGenerator(name = "REP_EJECUCION_USUARIO_ID_SEQ_GEN", sequenceName =
        "REP_EJECUCION_USUARIO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "REPORTE_EJECUCION_ID")
    public Long getReporteEjecucionId() {
        return this.reporteEjecucionId;
    }

    public void setReporteEjecucionId(Long reporteEjecucionId) {
        this.reporteEjecucionId = reporteEjecucionId;
    }

    @Column(name = "USUARIO_GENERA")
    public String getUsuarioGenera() {
        return this.usuarioGenera;
    }

    public void setUsuarioGenera(String usuarioGenera) {
        this.usuarioGenera = usuarioGenera;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

// Metodos transient(Deben tener autor y descripcion)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RepReporteEjecucionUsuario other = (RepReporteEjecucionUsuario) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
