/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los tipos de avaluo
 *
 * @author pedro.garcia
 * @modified juan.agudelo texto a mayuscula inicial acorde con el estandar del proyecto
 */
public enum ETipoAvaluo {

    RURAL("00", "Rural"),
    // D: cualquier código diferente a 00 se considera urbano. 01 es
    // urbano-cabecera municipal,
    // 02 es otros núcleos-corregimientos
    URBANO("01", "Urbano"),
    CORR("0X", "Corregimientos");

    private final String codigo;
    private final String nombre;

    private ETipoAvaluo(String codigoP, String nombreP) {
        this.codigo = codigoP;
        this.nombre = nombreP;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

}
