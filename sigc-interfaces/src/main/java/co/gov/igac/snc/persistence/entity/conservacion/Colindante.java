package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the COLINDANTE database table.
 *
 * @modified pedro.garcia adición de secuencvia para id
 *
 */
@Entity
public class Colindante implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    private String cardinalidad;
    private Date fechaLog;
    private String numeroPredial;
    private String numeroPredialColindante;
    private String tipo;
    private String usuarioLog;

    public Colindante() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Colindante_Id_Seq")
    @SequenceGenerator(name = "Colindante_Id_Seq", sequenceName = "COLINDANTE_ID_SEQ",
        allocationSize = 1)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCardinalidad() {
        return this.cardinalidad;
    }

    public void setCardinalidad(String cardinalidad) {
        this.cardinalidad = cardinalidad;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_COLINDANTE")
    public String getNumeroPredialColindante() {
        return this.numeroPredialColindante;
    }

    public void setNumeroPredialColindante(String numeroPredialColindante) {
        this.numeroPredialColindante = numeroPredialColindante;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
