package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the AVALUO_CAPITULO database table.
 *
 */
@Entity
@Table(name = "AVALUO_CAPITULO")
public class AvaluoCapitulo implements Serializable {

    private static final long serialVersionUID = 5934430913602085261L;
    private Long id;
    private Long avaluoId;
    private String codigo;
    private String descripcion;
    private Date fechaLog;
    private String otraDescripcion;
    private String titulo;
    private String usuarioLog;

    /**
     * Atributo creado para tener como identificar el objeto en vista cuando se cargan en las tablas
     * de las paginas ya que no siempre este objeto se carga desde BD.
     */
    private int idProvisional;

    public AvaluoCapitulo() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_CAPITULO_ID_GENERATOR", sequenceName =
        "AVALUO_CAPITULO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVALUO_CAPITULO_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AVALUO_ID")
    public Long getAvaluoId() {
        return this.avaluoId;
    }

    public void setAvaluoId(Long avaluoId) {
        this.avaluoId = avaluoId;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "OTRA_DESCRIPCION")
    public String getOtraDescripcion() {
        return this.otraDescripcion;
    }

    public void setOtraDescripcion(String otraDescripcion) {
        this.otraDescripcion = otraDescripcion;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    /**
     * Indica si el AvaluoCapitulo fue creado por el usuario o no.
     *
     * @return
     */
    @Transient
    public boolean isEsCreadoPorUsuario() {
        boolean result = true;

        if (this.codigo != null) {
            result = false;
        }

        return result;
    }

    @Transient
    public int getIdProvisional() {
        return idProvisional;
    }

    @Transient
    public void setIdProvisional(int idProvisional) {
        this.idProvisional = idProvisional;
    }

}
