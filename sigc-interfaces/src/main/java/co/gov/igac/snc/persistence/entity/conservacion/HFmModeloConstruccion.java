package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import javax.persistence.JoinColumns;

/**
 * The persistent class for the H_FM_MODELO_CONSTRUCCION database table.
 *
 * @modified david.cifuentes :: Adicion del campo usoConstruccion :: 20/04/2012 Adicion del campo
 * tipoCalificacion :: 25/04/2012
 */
@Entity
@Table(name = "H_FM_MODELO_CONSTRUCCION")
public class HFmModeloConstruccion implements Serializable {

    private static final long serialVersionUID = 1L;
    private HFmModeloConstruccionPK id;
    private Short anioConstruccion;
    private Double areaConstruida;
    private Long calificacionAnexoId;
    private String cancelaInscribe;
    private String descripcion;
    private Double dimension;
    private String dimensionUnidadMedida;
    private Date fechaLog;
    private String observaciones;
    private String tipificacion;
    private String tipoConstruccion;
    private Byte totalBanios;
    private Byte totalHabitaciones;
    private Byte totalLocales;
    private Byte totalPisosConstruccion;
    private Byte totalPisosUnidad;
    private Double totalPuntaje;
    private String usuarioLog;
    private HFichaMatrizModelo HFichaMatrizModelo;
    private HPredio HPredio;

    private UsoConstruccion usoConstruccion;
    private String tipoCalificacion;

    public HFmModeloConstruccion() {
    }

    @EmbeddedId
    public HFmModeloConstruccionPK getId() {
        return this.id;
    }

    public void setId(HFmModeloConstruccionPK id) {
        this.id = id;
    }

    @Column(name = "ANIO_CONSTRUCCION")
    public Short getAnioConstruccion() {
        return this.anioConstruccion;
    }

    public void setAnioConstruccion(Short anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "CALIFICACION_ANEXO_ID")
    public Long getCalificacionAnexoId() {
        return this.calificacionAnexoId;
    }

    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.calificacionAnexoId = calificacionAnexoId;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getDimension() {
        return this.dimension;
    }

    public void setDimension(Double dimension) {
        this.dimension = dimension;
    }

    @Column(name = "DIMENSION_UNIDAD_MEDIDA")
    public String getDimensionUnidadMedida() {
        return this.dimensionUnidadMedida;
    }

    public void setDimensionUnidadMedida(String dimensionUnidadMedida) {
        this.dimensionUnidadMedida = dimensionUnidadMedida;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "H_PREDIO_ID", nullable = false,
            insertable = false, updatable = false),
        @JoinColumn(name = "FICHA_MATRIZ_MODELO_ID", referencedColumnName = "ID", nullable = false,
            insertable = false, updatable = false)})
    public HFichaMatrizModelo getHFichaMatrizModelo() {
        return this.HFichaMatrizModelo;
    }

    public void setHFichaMatrizModelo(HFichaMatrizModelo HFichaMatrizModelo) {
        this.HFichaMatrizModelo = HFichaMatrizModelo;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTipificacion() {
        return this.tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @Column(name = "TIPO_CONSTRUCCION")
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    @Column(name = "TOTAL_BANIOS")
    public Byte getTotalBanios() {
        return this.totalBanios;
    }

    public void setTotalBanios(Byte totalBanios) {
        this.totalBanios = totalBanios;
    }

    @Column(name = "TOTAL_HABITACIONES")
    public Byte getTotalHabitaciones() {
        return this.totalHabitaciones;
    }

    public void setTotalHabitaciones(Byte totalHabitaciones) {
        this.totalHabitaciones = totalHabitaciones;
    }

    @Column(name = "TOTAL_LOCALES")
    public Byte getTotalLocales() {
        return this.totalLocales;
    }

    public void setTotalLocales(Byte totalLocales) {
        this.totalLocales = totalLocales;
    }

    @Column(name = "TOTAL_PISOS_CONSTRUCCION")
    public Byte getTotalPisosConstruccion() {
        return this.totalPisosConstruccion;
    }

    public void setTotalPisosConstruccion(Byte totalPisosConstruccion) {
        this.totalPisosConstruccion = totalPisosConstruccion;
    }

    @Column(name = "TOTAL_PISOS_UNIDAD")
    public Byte getTotalPisosUnidad() {
        return this.totalPisosUnidad;
    }

    public void setTotalPisosUnidad(Byte totalPisosUnidad) {
        this.totalPisosUnidad = totalPisosUnidad;
    }

    @Column(name = "TOTAL_PUNTAJE")
    public Double getTotalPuntaje() {
        return this.totalPuntaje;
    }

    public void setTotalPuntaje(Double totalPuntaje) {
        this.totalPuntaje = totalPuntaje;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to HPredio
    @ManyToOne
    @JoinColumn(name = "H_PREDIO_ID", insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USO_ID", nullable = true)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "TIPO_CALIFICACION", length = 30)
    public String getTipoCalificacion() {
        return this.tipoCalificacion;
    }

    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

}
