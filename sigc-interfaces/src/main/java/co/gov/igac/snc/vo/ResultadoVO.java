package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;

import co.gov.igac.snc.fachadas.ITramite;

/**
 * Objeto utilizado para serializar la información a enviar por REST / XML
 *
 * -> modificado por franz.gamba El objeto almacena un usuario y una lista de ActividadVO. En el
 * constructor de ACtividadVO el objeto toma lo que necesita de la clase Actividad y se inicializa
 * usando la interfaz proporcionada ITramite
 *
 * @author juan.mendez
 *
 */
public class ResultadoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6206310402679240058L;

    private UsuarioDTO usuario;
    private List<ActividadVO> actividades = new ArrayList<ActividadVO>();
    private Date logintime;

    public ResultadoVO(UsuarioDTO usuario, List<Actividad> actividades, ITramite tram) {
        this.setLogintime(new Date());

        this.setUsuario(usuario);
        for (Actividad a : actividades) {
            ActividadVO aVo = new ActividadVO(a, tram);
            this.getActividades().add(aVo);
        }
    }

    /** Full constructor */
    public ResultadoVO(UsuarioDTO usuario, List<ActividadVO> actividadesVo) {
        this.setLogintime(new Date());

        this.setUsuario(usuario);
        this.setActividades(actividadesVo);
    }

    public ResultadoVO(List<ActividadVO> actividadesVo) {
        this.setActividades(actividadesVo);
    }

    /**
     *
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     *
     */
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public Date getLogintime() {
        return logintime;
    }

    public void setLogintime(Date logtime) {
        this.logintime = logtime;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<ActividadVO> getActividades() {
        return actividades;
    }

    public void setActividades(List<ActividadVO> actividades) {
        this.actividades = actividades;
    }

}
