/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para el dominio DATO_RECTIFICAR_TIPO
 *
 * @author pedro.garcia
 */
public enum EDatoRectificarTipo {

    //D: esta enum tiene la forma código("valor")
    GENERALES("Generales"),
    PREDIO("Predio"),
    PROPIETARIO("Propietario");

    private String valor;

    public String getValor() {
        return this.valor;
    }

    private EDatoRectificarTipo(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
