package co.gov.igac.snc.persistence.entity.tramite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;

/**
 * SolicitudAvisoRegistro entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia - adición de anotaciones para la secuencia del id - cambio de tipo de
 * datos TimeStamp a Date - cambio de tipo de datos Set a List - adición de método
 * getAvisosPendientes - modificación de default constructor - adición de anotación @Temporal a los
 * campos Date (experimental)
 *
 * @modified by fabio.navarrete -> Se cambia la anotación @ManyToOne por @OneToOne a la columna
 * "SOLICITUD_ID"
 *
 */
@Entity
@Table(name = "SOLICITUD_AVISO_REGISTRO", schema = "SNC_TRAMITE", uniqueConstraints =
    @UniqueConstraint(columnNames = "SOLICITUD_ID"))
public class SolicitudAvisoRegistro implements java.io.Serializable {

    // Fields   
    private Long id;
    private Solicitud solicitud;
    private CirculoRegistral circuloRegistral;
    private Date fechaDocumentoSoporte;
    private String numeroDocumentoSoporte;
    private Long soporteDocumentoId;
    private Integer avisosTotal;
    private Integer avisosAceptados;
    private Integer avisosRechazados;
    private String usuarioLog;
    private Date fechaLog;
    private List<AvisoRegistroRechazo> avisoRegistroRechazos = new ArrayList<AvisoRegistroRechazo>();

    // Constructors
    /** default constructor */
    /**
     * @modified by pedro.garcia poner en 0 variables para que no chille en la inserci�n de uno
     * nuevo
     */
    public SolicitudAvisoRegistro() {
        this.avisosTotal = 0;
        this.avisosAceptados = 0;
        this.avisosRechazados = 0;
    }

    /** minimal constructor */
    public SolicitudAvisoRegistro(Long id, Solicitud solicitud,
        CirculoRegistral circuloRegistral, Integer avisosTotal,
        Integer avisosAceptados, Integer avisosRechazados,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.solicitud = solicitud;
        this.circuloRegistral = circuloRegistral;
        this.avisosTotal = avisosTotal;
        this.avisosAceptados = avisosAceptados;
        this.avisosRechazados = avisosRechazados;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public SolicitudAvisoRegistro(Long id, Solicitud solicitud,
        CirculoRegistral circuloRegistral, Date fechaDocumentoSoporte,
        String numeroDocumentoSoporte, Long soporteDocumentoId,
        Integer avisosTotal, Integer avisosAceptados,
        Integer avisosRechazados, String usuarioLog, Date fechaLog,
        List<AvisoRegistroRechazo> avisoRegistroRechazos) {
        this.id = id;
        this.solicitud = solicitud;
        this.circuloRegistral = circuloRegistral;
        this.fechaDocumentoSoporte = fechaDocumentoSoporte;
        this.numeroDocumentoSoporte = numeroDocumentoSoporte;
        this.soporteDocumentoId = soporteDocumentoId;
        this.avisosTotal = avisosTotal;
        this.avisosAceptados = avisosAceptados;
        this.avisosRechazados = avisosRechazados;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.avisoRegistroRechazos = avisoRegistroRechazos;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SolicitudAvisoRegistro_ID_SEQ")
    @SequenceGenerator(name = "SolicitudAvisoRegistro_ID_SEQ",
        sequenceName = "SOLICITUD_AVISO_REGISTR_ID_SEQ", allocationSize = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", unique = true, nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIRCULO_REGISTRAL", nullable = false)
    public CirculoRegistral getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(CirculoRegistral circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Temporal(value = TemporalType.DATE)
    @Column(name = "FECHA_DOCUMENTO_SOPORTE", length = 7)
    public Date getFechaDocumentoSoporte() {
        return this.fechaDocumentoSoporte;
    }

    public void setFechaDocumentoSoporte(Date fechaDocumentoSoporte) {
        this.fechaDocumentoSoporte = fechaDocumentoSoporte;
    }

    @Column(name = "NUMERO_DOCUMENTO_SOPORTE", length = 20)
    public String getNumeroDocumentoSoporte() {
        return this.numeroDocumentoSoporte;
    }

    public void setNumeroDocumentoSoporte(String numeroDocumentoSoporte) {
        this.numeroDocumentoSoporte = numeroDocumentoSoporte;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Long soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "AVISOS_TOTAL", nullable = false, precision = 6, scale = 0)
    public Integer getAvisosTotal() {
        return this.avisosTotal;
    }

    public void setAvisosTotal(Integer avisosTotal) {
        this.avisosTotal = avisosTotal;
    }

    @Column(name = "AVISOS_ACEPTADOS", nullable = false, precision = 6, scale = 0)
    public Integer getAvisosAceptados() {
        return this.avisosAceptados;
    }

    public void setAvisosAceptados(Integer avisosAceptados) {
        this.avisosAceptados = avisosAceptados;
    }

    @Column(name = "AVISOS_RECHAZADOS", nullable = false, precision = 6, scale = 0)
    public Integer getAvisosRechazados() {
        return this.avisosRechazados;
    }

    public void setAvisosRechazados(Integer avisosRechazados) {
        this.avisosRechazados = avisosRechazados;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(value = TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
        "solicitudAvisoRegistro")
    public List<AvisoRegistroRechazo> getAvisoRegistroRechazos() {
        return this.avisoRegistroRechazos;
    }

    public void setAvisoRegistroRechazos(
        List<AvisoRegistroRechazo> avisoRegistroRechazos) {
        this.avisoRegistroRechazos = avisoRegistroRechazos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna el número de avisos (o trámites que aún no son trámites) pendientes
     *
     * @author pedro.garcia
     */
    @Transient
    public int getAvisosPendientes() {
        int answer = 0;
        if (this.avisosTotal != null && this.avisosAceptados != null &&
            this.avisosRechazados != null) {
            answer = this.avisosTotal - (this.avisosAceptados + this.avisosRechazados);
        }
        return answer;
    }
}
