package co.gov.igac.snc.persistence.util;

/**
 * Enumeraciòn que identifica los tipos de recursos que proceden de acuerdo al tipo de trámite
 *
 * @author javier.aponte
 *
 */
public enum ETramiteRecursoProcede {

    REPOSICION("Reposición"),
    REPOSICION_Y_APELACION("Reposición y apelación");

    private String codigo;

    ETramiteRecursoProcede(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
