package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * Foto entity.
 */
/*
 * MODIFICACIONES
 *
 * @author juan.agudelo 05/06/11 se modifico fotoDocumentoId por el objeto Documento juan.agudelo
 * 01/11/11 se agregó la secuencia FOTO_ID_SEQ
 *
 */
//TODO :: fredy.wilches :: documentar cambio que hizo con el atributo unidadConstruccionId :: pedro.garcia
@Entity
@Table(name = "FOTO", schema = "SNC_CONSERVACION")
public class Foto implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3637125037316943371L;

    // Fields
    private Long id;
    private Predio predio;
    //private Long unidadConstruccionId;
    private UnidadConstruccion unidadConstruccion;
    private Long tramiteId;
    private String tipo;
    private String descripcion;
    private Date fecha;
    private Documento documento;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public Foto() {
    }

    /** minimal constructor */
    public Foto(Long id, String tipo, Date fecha, Documento documento,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipo = tipo;
        this.fecha = fecha;
        this.documento = documento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Foto(Long id, Predio predio, UnidadConstruccion unidadConstruccion,
        Long tramiteId, String tipo, String descripcion, Date fecha,
        Documento documento, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.unidadConstruccion = unidadConstruccion;
        this.tramiteId = tramiteId;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.documento = documento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FOTO_ID_SEQ")
    @SequenceGenerator(name = "FOTO_ID_SEQ", sequenceName = "FOTO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIDAD_CONSTRUCCION_ID")
    public UnidadConstruccion getUnidadConstruccion() {
        return this.unidadConstruccion;
    }

    public void setUnidadConstruccion(UnidadConstruccion unidadConstruccion) {
        this.unidadConstruccion = unidadConstruccion;
    }

    @Column(name = "TRAMITE_ID", precision = 10, scale = 0)
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "DESCRIPCION", length = 2000)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA", nullable = false, length = 7)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOTO_DOCUMENTO_ID", nullable = false)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static Foto parseFoto(HFoto hFoto) {
        Foto foto = new Foto();

        foto.setId(hFoto.getId().getId());
        foto.setPredio(Predio.parsePredio(hFoto.getHPredio()));
        foto.setUnidadConstruccion(UnidadConstruccion.parseUnidadConstruccion(hFoto.
            getHUnidadConstruccion()));
        foto.setTramiteId(hFoto.getTramiteId());
        foto.setTipo(hFoto.getTipo());
        foto.setDescripcion(hFoto.getDescripcion());
        foto.setFecha(hFoto.getFecha());
        foto.setDocumento(hFoto.getDocumento());
        foto.setUsuarioLog(hFoto.getUsuarioLog());
        foto.setFechaLog(hFoto.getFechaLog());

        return foto;

    }

}
