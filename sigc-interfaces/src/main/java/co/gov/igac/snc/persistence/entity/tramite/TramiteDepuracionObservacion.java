package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TRAMITE_DEPURACION_OBSERVACION database table.
 *
 */
@Entity
@Table(name = "TRAMITE_DEPURACION_OBSERVACION", schema = "SNC_TRAMITE")
public class TramiteDepuracionObservacion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String apruebaControlCalidad;
    private Date fecha;
    private Date fechaLog;
    private Long numeroRevision;
    private String observacion;
    private String usuarioLog;
    private TramiteDepuracion tramiteDepuracion;

    public TramiteDepuracionObservacion() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_DEPURACION_OBSE_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_DEPURACION_OBSE_ID_SEQ", sequenceName =
        "TRAMITE_DEPURACION_OBSE_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "APRUEBA_CONTROL_CALIDAD")
    public String getApruebaControlCalidad() {
        return this.apruebaControlCalidad;
    }

    public void setApruebaControlCalidad(String apruebaControlCalidad) {
        this.apruebaControlCalidad = apruebaControlCalidad;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_REVISION")
    public Long getNumeroRevision() {
        return this.numeroRevision;
    }

    public void setNumeroRevision(Long numeroRevision) {
        this.numeroRevision = numeroRevision;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to TramiteDepuracion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_DEPURACION_ID")
    public TramiteDepuracion getTramiteDepuracion() {
        return this.tramiteDepuracion;
    }

    public void setTramiteDepuracion(TramiteDepuracion tramiteDepuracion) {
        this.tramiteDepuracion = tramiteDepuracion;
    }

}
