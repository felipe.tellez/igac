package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeracion para manejar llamado de paginas que generan reportes usados en
 * RealizarInformeControlCalidadMB
 *
 * @author rodrigo.hernandez
 *
 */
public enum EGeneracionReporteControlCalidadAvaluo {

    REPORTE_CONTROL_CALIDAD_TERRITORIAL("generarReporteControlCalidadTerritorialForm",
        "reporteControlCalidadTerritorialWV"),
    OFICIO_REMISORIO_TERRITORIAL("oficioRemisorioTerritorialForm", "oficioRemisorioTerritorialWV"),
    REPORTE_CONTROL_CALIDAD_GIT_AVALUOS("reporteCCGITForm", "reporteCCGITWV"),
    REPORTE_NO_APROBACION_GIT_AVALUOS("moverTarea", "moverTareaWV"),
    REPORTE_NO_APROBACION_TERRITORIAL("", "");

    /**
     * Form de la página
     */
    private String form;
    /**
     * Widget que identifica el dialog dentro del que se incluye la página
     */
    private String widgetVar;

    private EGeneracionReporteControlCalidadAvaluo(String form, String widgetVar) {
        this.form = form;
        this.widgetVar = widgetVar;
    }

    public String getForm() {
        return this.form;
    }

    public String getWidgetVar() {
        return this.widgetVar;
    }
}
