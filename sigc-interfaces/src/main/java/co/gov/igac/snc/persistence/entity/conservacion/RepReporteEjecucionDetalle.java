package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the REP_REPORTE_EJECUCION_DETALLE database table.
 * 
 */
@Entity
@Table(name="REP_REPORTE_EJECUCION_DETALLE")
public class RepReporteEjecucionDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private BigDecimal areaConstruccionDesde;
    private BigDecimal areaConstruccionHasta;
    private BigDecimal areaTerrenoDesde;
    private BigDecimal areaTerrenoHasta;
    private BigDecimal avaluoDesde;
    private BigDecimal avaluoHasta;
    private String circuloRegistral;
    private String construccionDestino;
    private String construccionTipificacion;
    private String construccionUso;
    private String departamentoCodigo;
    private String direccion;
    private String matriculaInmobiliaria;
    private String modoAdquisicion;
    private String municipioCodigo;
    private String fechaCorte;
    private String matriculaInmobiliariaHasta;
    private String vigencia;
    private Date fechaInicioBloqueo;
    private Date fechaDesbloqueo;
    private String tipoTitulo;
    private String nombrePropietario;
    private String numeroIdentificacion;
    private String numeroPredialDesde;
    private String numeroPredialHasta;
    private String predioDestino;
    private String predioTipo;
    private String tipoIdentificacion;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private String uOCCodigo;
    private String territorialCodigo;
    private RepReporteEjecucion repReporteEjecucion;

    private String clasificacion;
    private String subproceso;
    private String actividad;
    private String rol;
    private String nombreFuncionario;

    private Date fechaInicial;
    private Date fechaFinal;

    private String diasTranscurridos;

    private String usuarioLog;
    private Date fechaLog;

    private Integer tipoRango;
    private String entidad;


    private String nacional;
    private String incluirUocs;
    private String consolidado;
    private String tipoPersona;

	
	public RepReporteEjecucionDetalle() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REP_REPORTE_EJECUCION_D_ID_SEQ")
	@SequenceGenerator(name="REP_REPORTE_EJECUCION_D_ID_SEQ", sequenceName = "REP_REPORTE_EJECUCION_D_ID_SEQ", allocationSize = 1 )
	@Column(name = "ID", nullable = false, precision = 10, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Column(name="AREA_CONSTRUCCION_DESDE")
	public BigDecimal getAreaConstruccionDesde() {
		return this.areaConstruccionDesde;
	}

	public void setAreaConstruccionDesde(BigDecimal areaConstruccionDesde) {
		this.areaConstruccionDesde = areaConstruccionDesde;
	}


	@Column(name="AREA_CONSTRUCCION_HASTA")
	public BigDecimal getAreaConstruccionHasta() {
		return this.areaConstruccionHasta;
	}

	public void setAreaConstruccionHasta(BigDecimal areaConstruccionHasta) {
		this.areaConstruccionHasta = areaConstruccionHasta;
	}


	@Column(name="AREA_TERRENO_DESDE")
	public BigDecimal getAreaTerrenoDesde() {
		return this.areaTerrenoDesde;
	}

	public void setAreaTerrenoDesde(BigDecimal areaTerrenoDesde) {
		this.areaTerrenoDesde = areaTerrenoDesde;
	}


	@Column(name="AREA_TERRENO_HASTA")
	public BigDecimal getAreaTerrenoHasta() {
		return this.areaTerrenoHasta;
	}

	public void setAreaTerrenoHasta(BigDecimal areaTerrenoHasta) {
		this.areaTerrenoHasta = areaTerrenoHasta;
	}


	@Column(name="AVALUO_DESDE")
	public BigDecimal getAvaluoDesde() {
		return this.avaluoDesde;
	}

	public void setAvaluoDesde(BigDecimal avaluoDesde) {
		this.avaluoDesde = avaluoDesde;
	}


	@Column(name="AVALUO_HASTA")
	public BigDecimal getAvaluoHasta() {
		return this.avaluoHasta;
	}

	public void setAvaluoHasta(BigDecimal avaluoHasta) {
		this.avaluoHasta = avaluoHasta;
	}


	@Column(name="CIRCULO_REGISTRAL")
	public String getCirculoRegistral() {
		return this.circuloRegistral;
	}

	public void setCirculoRegistral(String circuloRegistral) {
		this.circuloRegistral = circuloRegistral;
	}


	@Column(name="CONSTRUCCION_DESTINO")
	public String getConstruccionDestino() {
		return this.construccionDestino;
	}

	public void setConstruccionDestino(String construccionDestino) {
		this.construccionDestino = construccionDestino;
	}


	@Column(name="CONSTRUCCION_TIPIFICACION")
	public String getConstruccionTipificacion() {
		return this.construccionTipificacion;
	}

	public void setConstruccionTipificacion(String construccionTipificacion) {
		this.construccionTipificacion = construccionTipificacion;
	}


	@Column(name="CONSTRUCCION_USO")
	public String getConstruccionUso() {
		return this.construccionUso;
	}

	public void setConstruccionUso(String construccionUso) {
		this.construccionUso = construccionUso;
	}


	@Column(name="DEPARTAMENTO_CODIGO")
	public String getDepartamentoCodigo() {
		return this.departamentoCodigo;
	}

	public void setDepartamentoCodigo(String departamentoCodigo) {
		this.departamentoCodigo = departamentoCodigo;
	}

        @Column(name="DIRECCION", length = 250)
	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	@Column(name="MATRICULA_INMOBILIARIA", length = 20)
	public String getMatriculaInmobiliaria() {
		return this.matriculaInmobiliaria;
	}

	public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
		this.matriculaInmobiliaria = matriculaInmobiliaria;
	}


	@Column(name="MODO_ADQUISICION")
	public String getModoAdquisicion() {
		return this.modoAdquisicion;
	}

	public void setModoAdquisicion(String modoAdquisicion) {
		this.modoAdquisicion = modoAdquisicion;
	}


	@Column(name="MUNICIPIO_CODIGO")
	public String getMunicipioCodigo() {
		return this.municipioCodigo;
	}

	public void setMunicipioCodigo(String municipioCodigo) {
		this.municipioCodigo = municipioCodigo;
	}


	@Column(name="NOMBRE_PROPIETARIO")
	public String getNombrePropietario() {
		return this.nombrePropietario;
	}

	public void setNombrePropietario(String nombrePropietario) {
		this.nombrePropietario = nombrePropietario;
	}


	@Column(name="NUMERO_IDENTIFICACION")
	public String getNumeroIdentificacion() {
		return this.numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}


	@Column(name="NUMERO_PREDIAL_DESDE")
	public String getNumeroPredialDesde() {
		return this.numeroPredialDesde;
	}

	public void setNumeroPredialDesde(String numeroPredialDesde) {
		this.numeroPredialDesde = numeroPredialDesde;
	}


	@Column(name="NUMERO_PREDIAL_HASTA")
	public String getNumeroPredialHasta() {
		return this.numeroPredialHasta;
	}

	public void setNumeroPredialHasta(String numeroPredialHasta) {
		this.numeroPredialHasta = numeroPredialHasta;
	}


	@Column(name="PREDIO_DESTINO")
	public String getPredioDestino() {
		return this.predioDestino;
	}

	public void setPredioDestino(String predioDestino) {
		this.predioDestino = predioDestino;
	}


	@Column(name="PREDIO_TIPO")
	public String getPredioTipo() {
		return this.predioTipo;
	}

	public void setPredioTipo(String predioTipo) {
		this.predioTipo = predioTipo;
	}


	@Column(name="TIPO_IDENTIFICACION")
	public String getTipoIdentificacion() {
		return this.tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}


	@Column(name="ZONA_FISICA")
	public String getZonaFisica() {
		return this.zonaFisica;
	}

	public void setZonaFisica(String zonaFisica) {
		this.zonaFisica = zonaFisica;
	}


	@Column(name="ZONA_GEOECONOMICA")
	public String getZonaGeoeconomica() {
		return this.zonaGeoeconomica;
	}

	public void setZonaGeoeconomica(String zonaGeoeconomica) {
		this.zonaGeoeconomica = zonaGeoeconomica;
	}
        
        @Column(name="FECHA_CORTE", length = 20)
	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
        
        @Column(name="VIGENCIA", length = 20)
	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
        
        @Column(name="TIPO_TITULO", length = 30)
	public String getTipoTitulo() {
		return tipoTitulo;
	}

	public void setTipoTitulo(String tipoTitulo) {
		this.tipoTitulo = tipoTitulo;
	}

	@Column(name="TERRITORIAL_CODIGO")
	public String getTerritorialCodigo() {
		return this.territorialCodigo;
	}
	
	public void setTerritorialCodigo(String territorialCodigo) {
		this.territorialCodigo = territorialCodigo;
	}
        
        @Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIO_BLOQUEO")
	public Date getFechaInicioBloqueo() {
		return fechaInicioBloqueo;
	}

	public void setFechaInicioBloqueo(Date fechaInicioBloqueo) {
		this.fechaInicioBloqueo = fechaInicioBloqueo;
	}
        
        @Temporal(TemporalType.DATE)
	@Column(name="FECHA_DESBLOQUEO")
	public Date getFechaDesbloqueo() {
		return fechaDesbloqueo;
	}

	public void setFechaDesbloqueo(Date fechaDesbloqueo) {
		this.fechaDesbloqueo = fechaDesbloqueo;
	}
	
	@Column(name = "CLASIFICACION", length = 30)
	public String getClasificacion() {
		return this.clasificacion;
	}
	
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	
	@Column(name = "ROL", length = 100)
	public String getRol() {
		return this.rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
	@Column(name = "NOMBRE_FUNCIONARIO", length = 100)
	public String getNombreFuncionario() {
		return this.nombreFuncionario;
	}

	public void setNombreFuncionario(String nombreFuncionario) {
		this.nombreFuncionario = nombreFuncionario;
	}
	
	@Column(name = "SUBPROCESO", length = 30)
	public String getSubproceso() {
		return this.subproceso;
	}

	public void setSubproceso(String subproceso) {
		this.subproceso = subproceso;
	}
	
	@Column(name = "ACTIVIDAD", length = 200)
	public String getActividad() {
		return this.actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	//bi-directional many-to-one association to RepReporteEjecucion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REP_REPORTE_EJECUCION_ID")
	public RepReporteEjecucion getRepReporteEjecucion() {
		return this.repReporteEjecucion;
	}

	public void setRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion) {
		this.repReporteEjecucion = repReporteEjecucion;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_FINAL")
	public Date getFechaFinal() {
		return this.fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIAL")
	public Date getFechaInicial() {
		return this.fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	@Column(name = "DIAS_TRANSCURRIDOS", length = 50)
	public String getDiasTranscurridos() {
		return diasTranscurridos;
	}


	public void setDiasTranscurridos(String diasTranscurridos) {
		this.diasTranscurridos = diasTranscurridos;
	}
        
        @Column(name="MATRICULA_INMOBILIARIA_HASTA", length = 20)
	public String getMatriculaInmobiliariaHasta() {
		return matriculaInmobiliariaHasta;
	}


	public void setMatriculaInmobiliariaHasta(String matriculaInmobiliariaHasta) {
		this.matriculaInmobiliariaHasta = matriculaInmobiliariaHasta;
	}

	@Column(name="UOC_CODIGO")
	public String getuOCCodigo() {
		return uOCCodigo;
	}


	public void setuOCCodigo(String uOCCodigo) {
		this.uOCCodigo = uOCCodigo;
	}

	@Column(name="TIPO_RANGO")
	public Integer getTipoRango() {
		return tipoRango;
	}

	public void setTipoRango(Integer TipoRango) {
		this.tipoRango = TipoRango;
	}

	@Column(name="ENTIDAD")
	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	
	@Column(name = "USUARIO_LOG", nullable = false, length = 100)
	public String getUsuarioLog() {
		return this.usuarioLog;
	}

	public void setUsuarioLog(String usuarioLog) {
		this.usuarioLog = usuarioLog;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_LOG", nullable = false, length = 7)
	public Date getFechaLog() {
		return this.fechaLog;
	}

	public void setFechaLog(Date fechaLog) {
		this.fechaLog = fechaLog;
	}

	@Column(name = "NACIONAL", length = 2)
	public String getNacional() {
		return nacional;
	}

	public void setNacional(String nacional) {
		this.nacional = nacional;
	}

	@Column(name = "INCLUIR_UOCS", length = 2)
	public String getIncluirUocs() {
		return incluirUocs;
	}

	public void setIncluirUocs(String incluirUocs) {
		this.incluirUocs = incluirUocs;
	}

	@Column(name = "CONSOLIDADO", length = 2)
	public String getConsolidado() {
		return consolidado;
	}

	public void setConsolidado(String consolidado) {
		this.consolidado = consolidado;
	}

        @Column(name = "TIPO_PERSONA", length = 30)
        public String getTipoPersona() {
            return tipoPersona;
        }

        public void setTipoPersona(String tipoPersona) {
            this.tipoPersona = tipoPersona;
        }
}