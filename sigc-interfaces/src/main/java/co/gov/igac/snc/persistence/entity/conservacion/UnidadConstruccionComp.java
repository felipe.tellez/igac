package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * UnidadConstruccionComp entity.
 */
/*
 * @modified juan.agudelo 21-03-12 Adición de la secuencia UNIDAD_CONSTRUCCION_COMP_ID_SEQ
 */
@Entity
@NamedQueries({@NamedQuery(name = "findUnidadConstruccionCompByUnidadConstruccion",
        query =
        "from UnidadConstruccionComp componente where componente.unidadConstruccion= :unidadConstruccion")})
@Table(name = "UNIDAD_CONSTRUCCION_COMP", schema = "SNC_CONSERVACION")
public class UnidadConstruccionComp implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7941754986284669720L;

    // Fields
    private Long id;
    private String componente;
    private String elementoCalificacion;
    private String detalleCalificacion;
    private Double puntos;
    private String usuarioLog;
    private Date fechaLog;
    private UnidadConstruccion unidadConstruccion;

    // Constructors
    /** default constructor */
    public UnidadConstruccionComp() {
    }

    /** minimal constructor */
    public UnidadConstruccionComp(Long id,
        UnidadConstruccion unidadConstruccion, Double puntos,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.unidadConstruccion = unidadConstruccion;
        this.puntos = puntos;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public UnidadConstruccionComp(Long id,
        UnidadConstruccion unidadConstruccion, String componente,
        String elementoCalificacion, String detalleCalificacion,
        Double puntos, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.unidadConstruccion = unidadConstruccion;
        this.componente = componente;
        this.elementoCalificacion = elementoCalificacion;
        this.detalleCalificacion = detalleCalificacion;
        this.puntos = puntos;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "UNIDAD_CONSTRUCCION_COMP_ID_SEQ")
    @SequenceGenerator(name = "UNIDAD_CONSTRUCCION_COMP_ID_SEQ", sequenceName =
        "UNIDAD_CONSTRUCCION_COMP_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIDAD_CONSTRUCCION_ID", nullable = false)
    public UnidadConstruccion getUnidadConstruccion() {
        return this.unidadConstruccion;
    }

    public void setUnidadConstruccion(UnidadConstruccion unidadConstruccion) {
        this.unidadConstruccion = unidadConstruccion;
    }

    @Column(name = "COMPONENTE", length = 30)
    public String getComponente() {
        return this.componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Column(name = "ELEMENTO_CALIFICACION", length = 30)
    public String getElementoCalificacion() {
        return this.elementoCalificacion;
    }

    public void setElementoCalificacion(String elementoCalificacion) {
        this.elementoCalificacion = elementoCalificacion;
    }

    @Column(name = "DETALLE_CALIFICACION", length = 250)
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    @Column(name = "PUNTOS", nullable = false, precision = 5)
    public Double getPuntos() {
        return this.puntos;
    }

    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static UnidadConstruccionComp parseUnidadConstruccionComp(
        HUnidadConstruccionComp hUnidadConstruccionComp) {
        UnidadConstruccionComp unidadConstruccionComp = new UnidadConstruccionComp();

        unidadConstruccionComp.setId(hUnidadConstruccionComp.getId().getId());
        unidadConstruccionComp.setUnidadConstruccion(UnidadConstruccion.parseUnidadConstruccion(
            hUnidadConstruccionComp.getHUnidadConstruccion()));
        unidadConstruccionComp.setComponente(hUnidadConstruccionComp.getComponente());
        unidadConstruccionComp.setElementoCalificacion(hUnidadConstruccionComp.
            getElementoCalificacion());
        unidadConstruccionComp.setDetalleCalificacion(hUnidadConstruccionComp.
            getDetalleCalificacion());
        unidadConstruccionComp.setPuntos(hUnidadConstruccionComp.getPuntos());
        unidadConstruccionComp.setUsuarioLog(hUnidadConstruccionComp.getUsuarioLog());
        unidadConstruccionComp.setFechaLog(hUnidadConstruccionComp.getFechaLog());

        return unidadConstruccionComp;
    }

}
