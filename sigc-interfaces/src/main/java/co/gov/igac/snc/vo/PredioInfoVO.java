package co.gov.igac.snc.vo;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioPropietarioHis;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que reduce la informacion del predio para ser mostradas en las tareas geograficas
 *
 * @author franz.gamba
 * @modified andres.eslava::refs #12603::15/05/2015::15/05/2015:: soporte a Id del predio para
 * relacion en editor.
 * @modified alvaro.gomez::refs #20423::23/01/2017 soporte Estado para caso de Canselacion Masiva
 * PH-CONDOMINIO
 */
public class PredioInfoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7312830130461751925L;
    // Se agrega el id tramite para asociacion de antes y despues de los predios proyectados  y en firme.
    private Long id;
    private String numeroPredial;
    private String numeroPredialAnterior;
    private String cancelaInscribe;
    private String estado;
    private Double areaTerreno;
    private List<PredioDireccionVO> direcciones;
    private List<PPredioDireccionVO> pDirecciones;
    private List<UnidadConstruccionVO> unidadConstruccions;
    private List<PUnidadConstruccionVO> pUnidadConstruccions;

    //FichaMatriz
    private FichaMatrizVO fichaMatriz;

    /**
     * Full constructor
     *
     * @param numeroPredial
     * @param numeroPredialAnterior
     * @param direcciones
     * @param unidadConstruccions
     * @param pUnidadConstruccions
     */
    public PredioInfoVO(String numeroPredial, String numeroPredialAnterior,
        List<PredioDireccionVO> direcciones,
        List<PPredioDireccionVO> pDirecciones,
        List<UnidadConstruccionVO> unidadConstruccions,
        List<PUnidadConstruccionVO> pUnidadConstruccions) {
        super();
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.direcciones = direcciones;
        this.pDirecciones = pDirecciones;
        this.unidadConstruccions = unidadConstruccions;
        this.pUnidadConstruccions = pUnidadConstruccions;
    }

    /**
     * Numeros Prediales constructor
     */
    public PredioInfoVO(String numeroPredial, String numeroPredialAnterior,
        List<PredioDireccionVO> direcciones, List<PPredioDireccionVO> pDirecciones) {
        super();
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.direcciones = direcciones;
        this.pDirecciones = pDirecciones;
    }

    /**
     * Empty constructor
     */
    public PredioInfoVO() {

    }

    /**
     * Constructor de informacion de envio de predios base
     *
     * @param predioOriginalAsociadoTramite predio de la tabla predio que representa el predio en
     * firme.
     * @param definicionTipoTramite definicion de
     * @author andres.eslava
     */
    public PredioInfoVO(Predio predioOriginalAsociadoTramite, Tramite definicionTipoTramite) {
        this.id = predioOriginalAsociadoTramite.getId();
        this.direcciones = new ArrayList<PredioDireccionVO>();
        this.unidadConstruccions = new ArrayList<UnidadConstruccionVO>();
        this.areaTerreno = predioOriginalAsociadoTramite.getAreaTerreno();

        //Campo para la direccion principal del predio(sí la tiene).
        String direccionPrincipal = null;

        // Obtiene la informacion de número predial.
        this.numeroPredial = predioOriginalAsociadoTramite.getNumeroPredial();

        if (predioOriginalAsociadoTramite.getNumeroPredialAnterior() != null) {
            this.numeroPredialAnterior = predioOriginalAsociadoTramite.getNumeroPredialAnterior();
        }

        // Obtiene las direcciones del predio.
        if (null != predioOriginalAsociadoTramite.getPredioDireccions() &&
            !predioOriginalAsociadoTramite.getPredioDireccions().isEmpty()) {

            for (PredioDireccion unaDireccion : predioOriginalAsociadoTramite.getPredioDireccions()) {
                if (unaDireccion.getPrincipal().equals("SI")) {
                    this.direcciones.add(new PredioDireccionVO(unaDireccion.getDireccion(), true));
                } else {
                    this.direcciones.add(new PredioDireccionVO(unaDireccion.getDireccion()));
                }
            }
        }
        // obtiene las construcciones asociadas al predio.           
        if (null != predioOriginalAsociadoTramite.getUnidadConstruccions() &&
            !predioOriginalAsociadoTramite.getUnidadConstruccions().isEmpty()) {
            for (UnidadConstruccion unaUnidadDeConstruccion : predioOriginalAsociadoTramite.
                getUnidadConstruccions()) {
                this.unidadConstruccions.add(new UnidadConstruccionVO(unaUnidadDeConstruccion));
            }
        }
    }

    /**
     * Constructor de informacion de envio de predios proyectados
     *
     * @param predioProyectado
     * @author andres.eslava
     */
    public PredioInfoVO(PPredio prediosProyectadoAsociadoTramite, Tramite definicionTipoTramite) {
        this.id = prediosProyectadoAsociadoTramite.getId();
        this.pDirecciones = new ArrayList<PPredioDireccionVO>();
        this.pUnidadConstruccions = new ArrayList<PUnidadConstruccionVO>();
        this.areaTerreno = prediosProyectadoAsociadoTramite.getAreaTerreno();
        this.estado = prediosProyectadoAsociadoTramite.getEstado();
        String direccionPrincipal = null;

        // Obtiene la informacion de número predial.
        this.numeroPredial = prediosProyectadoAsociadoTramite.getNumeroPredial();

        if (prediosProyectadoAsociadoTramite.getNumeroPredialAnterior() != null) {
            this.numeroPredialAnterior = prediosProyectadoAsociadoTramite.getNumeroPredialAnterior();
        }

        // Obtiene las direcciones del predio proyectado.
        if (null != prediosProyectadoAsociadoTramite.getPPredioDireccions() ||
            !prediosProyectadoAsociadoTramite.getPPredioDireccions().isEmpty()) {
            for (PPredioDireccion unaPDireccion : prediosProyectadoAsociadoTramite.
                getPPredioDireccions()) {
                if (unaPDireccion.getPrincipal().equals("SI")) {
                    this.pDirecciones.
                        add(new PPredioDireccionVO(unaPDireccion.getDireccion(), true));
                } else {
                    this.pDirecciones.add(new PPredioDireccionVO(unaPDireccion.getDireccion()));
                }
            }
        }

        // obtiene las construcciones asociadas del predio proyectado. 
        if (null != prediosProyectadoAsociadoTramite.getPUnidadConstruccions() ||
            !prediosProyectadoAsociadoTramite.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion unaUnidadDeConstruccion : prediosProyectadoAsociadoTramite.
                getPUnidadConstruccions()) {
                this.pUnidadConstruccions.add(new PUnidadConstruccionVO(unaUnidadDeConstruccion));
            }
        }

        // Adiciona el cancela inscribe del predio 
        if (prediosProyectadoAsociadoTramite.getCancelaInscribe() != null &&
            !prediosProyectadoAsociadoTramite.getCancelaInscribe().isEmpty()) {
            this.cancelaInscribe = prediosProyectadoAsociadoTramite.getCancelaInscribe();
        }
    }

    /*
     * Constructor de informacion para la edicion geografica involucrada en un tramite @author
     * andres.eslava
     */
    public PredioInfoVO(PPredio prediosProyectadoAsociadoTramite,
        Predio predioOriginalAsociadoTramite, Tramite definicionTipoTramite) {
        this.pDirecciones = new ArrayList<PPredioDireccionVO>();
        String direccionPrincipalPredioOriginal = null;
        String direccionPrincipalPredioProyectado = null;
        // Obtiene la informacion de número predial.
        this.numeroPredial = predioOriginalAsociadoTramite.getNumeroPredial();
        this.areaTerreno = predioOriginalAsociadoTramite.getAreaTerreno();

        if (predioOriginalAsociadoTramite.getNumeroPredialAnterior() != null) {
            this.numeroPredialAnterior = predioOriginalAsociadoTramite.getNumeroPredialAnterior();
        }

        // Obtiene las direcciones del predio.
        if (null != predioOriginalAsociadoTramite.getPredioDireccions() &&
            predioOriginalAsociadoTramite.getPredioDireccions().isEmpty()) {
            if (predioOriginalAsociadoTramite.getDireccionPrincipal() != null &&
                predioOriginalAsociadoTramite.getDireccionPrincipal().isEmpty()) {
                this.direcciones.add(new PredioDireccionVO(predioOriginalAsociadoTramite.
                    getDireccionPrincipal(), true));
                direccionPrincipalPredioOriginal = predioOriginalAsociadoTramite.
                    getDireccionPrincipal();
            }
            for (PredioDireccion unaDireccion : predioOriginalAsociadoTramite.getPredioDireccions()) {
                if (!direccionPrincipalPredioOriginal.equals(predioOriginalAsociadoTramite.
                    getDireccionPrincipal())) {
                    this.direcciones.add(new PredioDireccionVO(numeroPredial));
                }
            }
        }
        // obtiene las construcciones asociadas al predio.           
        if (null != predioOriginalAsociadoTramite.getUnidadConstruccions() &&
            predioOriginalAsociadoTramite.getUnidadConstruccions().isEmpty()) {
            for (UnidadConstruccion unaUnidadDeConstruccion : predioOriginalAsociadoTramite.
                getUnidadConstruccions()) {
                this.unidadConstruccions.add(new UnidadConstruccionVO(unaUnidadDeConstruccion));
            }
        }

        // Obtiene las direcciones del predio proyectado.
        if (null != prediosProyectadoAsociadoTramite.getPPredioDireccions() &&
            prediosProyectadoAsociadoTramite.getPPredioDireccions().isEmpty()) {
            direccionPrincipalPredioProyectado = prediosProyectadoAsociadoTramite.
                getDireccionPrincipal();
            this.pDirecciones.add(new PPredioDireccionVO(direccionPrincipalPredioProyectado, true));

            for (PPredioDireccion unaDireccion : prediosProyectadoAsociadoTramite.
                getPPredioDireccions()) {
                this.pDirecciones.add(new PPredioDireccionVO(unaDireccion.getDireccion()));
            }
        }

        // obtiene las construcciones asociadas del predio proyectado. 
        if (null != prediosProyectadoAsociadoTramite.getPUnidadConstruccions() &&
            prediosProyectadoAsociadoTramite.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion unaUnidadDeConstruccion : prediosProyectadoAsociadoTramite.
                getPUnidadConstruccions()) {
                this.pUnidadConstruccions.add(new PUnidadConstruccionVO(unaUnidadDeConstruccion));
            }
        }
    }

    public Double getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNumeroPredialAnterior() {
        return numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    public List<PredioDireccionVO> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(List<PredioDireccionVO> direcciones) {
        this.direcciones = direcciones;
    }

    public List<PPredioDireccionVO> getpDirecciones() {
        return pDirecciones;
    }

    public void setpDirecciones(List<PPredioDireccionVO> pDirecciones) {
        this.pDirecciones = pDirecciones;
    }

    public List<UnidadConstruccionVO> getUnidadConstruccions() {
        return unidadConstruccions;
    }

    public void setUnidadConstruccions(
        List<UnidadConstruccionVO> unidadConstruccions) {
        this.unidadConstruccions = unidadConstruccions;
    }

    public List<PUnidadConstruccionVO> getpUnidadConstruccions() {
        return pUnidadConstruccions;
    }

    public void setpUnidadConstruccions(
        List<PUnidadConstruccionVO> pUnidadConstruccions) {
        this.pUnidadConstruccions = pUnidadConstruccions;
    }

    public FichaMatrizVO getFichaMatriz() {
        return fichaMatriz;
    }

    public void setFichaMatriz(FichaMatrizVO fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

}
