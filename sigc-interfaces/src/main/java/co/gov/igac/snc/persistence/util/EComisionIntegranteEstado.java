package co.gov.igac.snc.persistence.util;

/**
 * Enumeración para manejar los estados de un integrante de una comision
 *
 * @author franz.gamba
 *
 */
public enum EComisionIntegranteEstado {
    ORIGINAL,
    RETIRADO,
    ADICIONADO;
}
