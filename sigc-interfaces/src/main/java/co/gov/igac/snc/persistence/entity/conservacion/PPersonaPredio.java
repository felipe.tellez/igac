package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PPersonaPredio:
 *
 * @modified fabio.navarrete -> Se agregó el método y atributo transient para consultar el valor de
 * la propiedad cancelaInscribe -> Se sobreescribe el método Equals -> Se agregó el método
 * integrarDatos
 *
 * @modified juan.agudelo 30-05-11 PPersonaPredioPropiedad Set a List.
 *
 * @modified Uso de CustomProyeccionIdGenerator para la generación del id.
 *
 * @modified david.cifuentes :: Se implementa la interfaz Cloneable y se agrega método clone ::
 * 20/09/12
 */
@Entity
@Table(name = "P_PERSONA_PREDIO", schema = "SNC_CONSERVACION")
public class PPersonaPredio implements java.io.Serializable, IProyeccionObject, Cloneable,
    IInscripcionCatastral {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 6017167048378738491L;
    private Long id;
    private PPersona PPersona;
    private PPredio PPredio;
    private String tipo;
    private Double participacion;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private List<PPersonaPredioPropiedad> PPersonaPredioPropiedads =
        new ArrayList<PPersonaPredioPropiedad>();
    private Date fechaInscripcionCatastral;
    private String cancelaInscribeValor;

    private boolean cantidadPredios;
    private List<PPredio> listPPredioPorPersona = new ArrayList<PPredio>();

    // Constructors
    /** default constructor */
    public PPersonaPredio() {
    }

    /** minimal constructor */
    public PPersonaPredio(Long id, PPersona PPersona, PPredio PPredio,
        String tipo, Double participacion, String cancelaInscribe,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPersona = PPersona;
        this.PPredio = PPredio;
        this.tipo = tipo;
        this.participacion = participacion;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PPersonaPredio(Long id, PPersona PPersona, PPredio PPredio,
        String tipo, Double participacion, String cancelaInscribe,
        String usuarioLog, Date fechaLog, Date fechaInscripcionCatastral,
        List<PPersonaPredioPropiedad> PPersonaPredioPropiedads) {
        this.id = id;
        this.PPersona = PPersona;
        this.PPredio = PPredio;
        this.tipo = tipo;
        this.participacion = participacion;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.PPersonaPredioPropiedads = PPersonaPredioPropiedads;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    /** copy constructor */
    public PPersonaPredio(PPersonaPredio pPersonaPredio) {
        this.id = pPersonaPredio.getId();
        this.PPersona = pPersonaPredio.getPPersona();
        this.PPredio = pPersonaPredio.getPPredio();
        this.tipo = pPersonaPredio.getTipo();
        this.participacion = pPersonaPredio.getParticipacion();
        this.cancelaInscribe = pPersonaPredio.getCancelaInscribe();
        this.usuarioLog = pPersonaPredio.getUsuarioLog();
        this.fechaLog = pPersonaPredio.getFechaLog();
        this.PPersonaPredioPropiedads = new ArrayList<PPersonaPredioPropiedad>(pPersonaPredio.
            getPPersonaPredioPropiedads());
        this.fechaInscripcionCatastral = pPersonaPredio.getFechaInscripcionCatastral();
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONA_PREDIO_ID_SEQ")
    //@SequenceGenerator(name="PERSONA_PREDIO_ID_SEQ", sequenceName = "PERSONA_PREDIO_ID_SEQ", allocationSize=1)
    @GenericGenerator(name = "PERSONA_PREDIO_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {@Parameter(
                name = "sequence", value = "PERSONA_PREDIO_ID_SEQ"), @Parameter(name =
                "allocationSize", value = "1")})
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PERSONA_ID", nullable = false)
    public PPersona getPPersona() {
        return this.PPersona;
    }

    public void setPPersona(PPersona PPersona) {
        this.PPersona = PPersona;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "PARTICIPACION", nullable = true, precision = 5)
    public Double getParticipacion() {
        return this.participacion;
    }

    public void setParticipacion(Double participacion) {
        this.participacion = participacion;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPersonaPredio")
    public List<PPersonaPredioPropiedad> getPPersonaPredioPropiedads() {
        return this.PPersonaPredioPropiedads;
    }

    public void setPPersonaPredioPropiedads(
        List<PPersonaPredioPropiedad> PPersonaPredioPropiedads) {
        this.PPersonaPredioPropiedads = PPersonaPredioPropiedads;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Override
    public boolean equals(Object o) {
        // OJO Con ir a modificarlo, es usado en proyeccion para saber si existe en una lista
        if (this.id == null || o == null || !(o instanceof PPersonaPredio)) {
            return false;
        }
        if (((PPersonaPredio) o).getId() == null) {
            return false;
        }
        return this.id.equals(((PPersonaPredio) o).getId());
    }

    /**
     * Método encargado de integrar los datos de un objeto PPersonaPredio en el objeto actual.
     *
     * @author fabio.navarrete
     */
    public void integrarDatos(PPersonaPredio pPersonaPredio) {
        this.cancelaInscribe = pPersonaPredio.getCancelaInscribe();
        this.fechaInscripcionCatastral = pPersonaPredio.getFechaInscripcionCatastral();
        this.fechaLog = pPersonaPredio.getFechaLog();
        this.id = pPersonaPredio.getId();
        this.participacion = pPersonaPredio.getParticipacion();
        this.PPersona = pPersonaPredio.getPPersona();
        this.PPersonaPredioPropiedads = pPersonaPredio.getPPersonaPredioPropiedads();
        this.PPredio = pPersonaPredio.getPPredio();
        this.tipo = pPersonaPredio.getTipo();
        this.usuarioLog = pPersonaPredio.getUsuarioLog();
    }

    @Transient
    public String getPorcentajeParticipacion() {
        if (participacion == null) {
            return null;
        }
        return "" + this.participacion;
    }

    public void setPorcentajeParticipacion(String porcentajeParticipacion) {
        if (porcentajeParticipacion == null || porcentajeParticipacion.equals("")) {
            this.participacion = null;
        } else {
            try {
                this.participacion = new Double(porcentajeParticipacion);
            } catch (NumberFormatException e) {
                this.participacion = null;
            }
        }
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    /**
     * @author david.cifuentes Método que realiza un clone del objeto pPersonaPredio.
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    @Transient
    public boolean isCantidadPredios() {
        return cantidadPredios;
    }

    public void setCantidadPredios(boolean cantidadPredios) {
        this.cantidadPredios = cantidadPredios;
    }

    @Transient
    public List<PPredio> getListPPredioPorPersona() {
        return listPPredioPorPersona;
    }

    public void setListPPredioPorPersona(List<PPredio> listPPredioPorPersona) {
        this.listPPredioPorPersona = listPPredioPorPersona;
    }

}
