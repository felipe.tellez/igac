package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the V_PERMISO_GRUPO database table.
 *
 */
@Entity
@Table(name = "V_PERMISO_GRUPO")
public class VPermisoGrupo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String camino;

    private String consulta;

    private String crea;

    private String edita;

    private String ejecuta;

    private String elimina;

    private String grupo;

    private BigDecimal id;

    private BigDecimal nivel;

    private String nombre;

    private BigDecimal orden;

    private BigDecimal padreComponenteId;

    private BigDecimal sistemaId;

    private String tipo;

    private String url;

    private String urlAyuda;

    private String ambito;

    public VPermisoGrupo() {
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo, String nombre, String url,
        String urlAyuda, BigDecimal orden, BigDecimal padreId, String camino, BigDecimal sistemaId,
        String ambito, String consulta, String crea, String edita, String elimina, String ejecuta) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.url = url;
        this.urlAyuda = urlAyuda;
        this.orden = orden;
        this.padreComponenteId = padreId;
        this.camino = camino;
        this.sistemaId = sistemaId;
        this.consulta = consulta;
        this.crea = crea;
        this.edita = edita;
        this.elimina = elimina;
        this.ejecuta = ejecuta;
        this.ambito = ambito;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id) {
        this.nivel = nivel;
        this.id = id;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo, String nombre) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo, String nombre, String url) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.url = url;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo, String nombre, String url,
        String urlAyuda) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.url = url;
        this.urlAyuda = urlAyuda;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo, String nombre, String url,
        String urlAyuda, BigDecimal orden, BigDecimal padreId) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.url = url;
        this.urlAyuda = urlAyuda;
        this.orden = orden;
        this.padreComponenteId = padreId;
    }

    public VPermisoGrupo(BigDecimal nivel, BigDecimal id, String tipo, String nombre, String url,
        String urlAyuda, BigDecimal orden, BigDecimal padreId, String camino, BigDecimal sistemaId) {
        this.nivel = nivel;
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.url = url;
        this.urlAyuda = urlAyuda;
        this.orden = orden;
        this.padreComponenteId = padreId;
        this.camino = camino;
        this.sistemaId = sistemaId;
    }

    public String getCamino() {
        return this.camino;
    }

    public void setCamino(String camino) {
        this.camino = camino;
    }

    public String getConsulta() {
        return this.consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public String getCrea() {
        return this.crea;
    }

    public void setCrea(String crea) {
        this.crea = crea;
    }

    public String getEdita() {
        return this.edita;
    }

    public void setEdita(String edita) {
        this.edita = edita;
    }

    public String getEjecuta() {
        return this.ejecuta;
    }

    public void setEjecuta(String ejecuta) {
        this.ejecuta = ejecuta;
    }

    public String getElimina() {
        return this.elimina;
    }

    public void setElimina(String elimina) {
        this.elimina = elimina;
    }

    public String getGrupo() {
        return this.grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    @Id
    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getNivel() {
        return this.nivel;
    }

    public void setNivel(BigDecimal nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getOrden() {
        return this.orden;
    }

    public void setOrden(BigDecimal orden) {
        this.orden = orden;
    }

    @Column(name = "PADRE_COMPONENTE_ID")
    public BigDecimal getPadreComponenteId() {
        return this.padreComponenteId;
    }

    public void setPadreComponenteId(BigDecimal padreComponenteId) {
        this.padreComponenteId = padreComponenteId;
    }

    @Column(name = "SISTEMA_ID")
    public BigDecimal getSistemaId() {
        return this.sistemaId;
    }

    public void setSistemaId(BigDecimal sistemaId) {
        this.sistemaId = sistemaId;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "URL_AYUDA")
    public String getUrlAyuda() {
        return this.urlAyuda;
    }

    public void setUrlAyuda(String urlAyuda) {
        this.urlAyuda = urlAyuda;
    }

    @Column(name = "AMBITO", nullable = false, length = 30)
    public String getAmbito() {
        return this.ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

}
