package co.gov.igac.snc.persistence.util;

import co.gov.igac.snc.persistence.entity.conservacion.Entidad;

/**
 * Enumeración con los posibles estados para una {@link Entidad} que realiza bloqueos de predios y
 * personas.
 *
 * @author david.cifuentes
 *
 */
public enum EEntidadEstado {

    ACTIVO("A"),
    INACTIVO("I");

    private String estado;

    private EEntidadEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

}
