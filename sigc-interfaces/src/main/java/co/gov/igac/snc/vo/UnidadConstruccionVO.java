package co.gov.igac.snc.vo;

import java.io.Serializable;

import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;

public class UnidadConstruccionVO implements Serializable {

    /**
     * Clase que reduce la informacion de las uniddes de construccion para ser mostradas en el visor
     *
     * @author franz.gamba
     */
    private static final long serialVersionUID = 7046886541502774857L;

    private String unidad;
    private Double areaConstruccionUnidad;
    private Integer numeroBanios;
    private Integer numeroHabitaciones;
    private Integer numeroLocales;
    private Integer numeroPisos;
    private Double puntaje;
    private String usoConstruccion;
    private String tipoConstruccion;
    private String tipoDominio;
    private String pisoUbicacion;

    /**
     * Empty constructor
     */
    public UnidadConstruccionVO() {

    }

    /**
     * Constructor a partir de una unidad de construccion
     */
    public UnidadConstruccionVO(UnidadConstruccion unidad) {
        this.unidad = unidad.getUnidad();
        this.areaConstruccionUnidad = unidad.getAreaConstruida();
        this.numeroBanios = unidad.getTotalBanios();
        this.numeroHabitaciones = unidad.getTotalHabitaciones();
        this.numeroLocales = unidad.getTotalLocales();
        this.numeroPisos = unidad.getTotalPisosConstruccion();
        this.puntaje = unidad.getTotalPuntaje();
        if (unidad.getUsoConstruccion() != null) {
            this.usoConstruccion = unidad.getUsoConstruccion().getNombre();
        }
        this.tipoConstruccion = unidad.getTipoConstruccion();
        this.pisoUbicacion = unidad.getPisoUbicacion();
        this.tipoDominio = unidad.getTipoDominio();
    }

    /**
     * Full constructor
     */
    public UnidadConstruccionVO(Double areaConstruccion, Integer numeroBanios,
        Integer numeroHabitaciones,
        Integer numeroLocales, Integer numeroPisos, Double puntaje, String usoContruccion) {
        this.areaConstruccionUnidad = areaConstruccion;
        this.numeroBanios = numeroBanios;
        this.numeroHabitaciones = numeroHabitaciones;
        this.numeroLocales = numeroLocales;
        this.numeroPisos = numeroPisos;
        this.puntaje = puntaje;
        this.usoConstruccion = usoContruccion;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Double getAreaConstruccionUnidad() {
        return areaConstruccionUnidad;
    }

    public void setAreaConstruccionUnidad(Double areaConstruccionUnidad) {
        this.areaConstruccionUnidad = areaConstruccionUnidad;
    }

    public Integer getNumeroBanios() {
        return numeroBanios;
    }

    public void setNumeroBanios(Integer numeroBanios) {
        this.numeroBanios = numeroBanios;
    }

    public Integer getNumeroHabitaciones() {
        return numeroHabitaciones;
    }

    public void setNumeroHabitaciones(Integer numeroHabitaciones) {
        this.numeroHabitaciones = numeroHabitaciones;
    }

    public Integer getNumeroLocales() {
        return numeroLocales;
    }

    public void setNumeroLocales(Integer numeroLocales) {
        this.numeroLocales = numeroLocales;
    }

    public Integer getNumeroPisos() {
        return numeroPisos;
    }

    public void setNumeroPisos(Integer numeroPisos) {
        this.numeroPisos = numeroPisos;
    }

    public Double getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Double puntaje) {
        this.puntaje = puntaje;
    }

    public String getUsoConstruccion() {
        return usoConstruccion;
    }

    public void setUsoConstruccion(String usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    public String getTipoConstruccion() {
        return tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    public String getPisoUbicacion() {
        return pisoUbicacion;
    }

    public void setPisoUbicacion(String pisoUbicacion) {
        this.pisoUbicacion = pisoUbicacion;
    }

    public String getTipoDominio() {
        return tipoDominio;
    }

    public void setTipoDominio(String tipoDominio) {
        this.tipoDominio = tipoDominio;
    }
}
