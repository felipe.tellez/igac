/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;

import co.gov.igac.persistence.entity.generales.Profesion;

/**
 *
 * Clase que tiene los campos que se usan como criterios de consulta de avaluadores
 *
 * @author pedro.garcia
 */
public class FiltroDatosConsultaProfesionalAvaluos implements Serializable {

    private static final long serialVersionUID = -917224347709167L;

    private String contratoActivo;
    private String numeroIdentificacion;
    private String primerNombre;
    private String profesion;
    private String primerApellido;
    private String segundoApellido;
    private String segundoNombre;

    /**
     * Territorial que contrató al avaluador
     */
    private String territorialContrato;
    private String tipoIdentificacion;
    private String tipoVinculacion;

    /**
     * Id de la {@link Profesion} del profesional
     */
    private Long profesionId;

    /**
     * Codigo de la territorial a la que está asociado el profesional
     */
    private String territorialCodigoProfesional;

//-------------------   methods   -----------------------------------------
    public String getTerritorialContrato() {
        return this.territorialContrato;
    }

    public void setTerritorialContrato(String territorialContrato) {
        this.territorialContrato = territorialContrato;
    }

    public String getTipoVinculacion() {
        return this.tipoVinculacion;
    }

    public void setTipoVinculacion(String tipoVinculacion) {
        this.tipoVinculacion = tipoVinculacion;
    }

    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getProfesion() {
        return this.profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getContratoActivo() {
        return this.contratoActivo;
    }

    public void setContratoActivo(String contratoActivo) {
        this.contratoActivo = contratoActivo;
    }

    public Long getProfesionId() {
        return this.profesionId;
    }

    public void setProfesionId(Long profesionId) {
        this.profesionId = profesionId;
    }

    public String getTerritorialCodigoProfesional() {
        return this.territorialCodigoProfesional;
    }

    public void setTerritorialCodigoProfesional(
        String territorialCodigoProfesional) {
        this.territorialCodigoProfesional = territorialCodigoProfesional;
    }

//end of class
}
