/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores para el dominio UNIDAD_MEDIDA_AREA
 *
 * @author pedro.garcia
 */
public enum EUnidadMedidaArea {

    HECTAREA("Ha", "Hectáreas"),
    METRO_CUADRADO("M2", "Metros Cuadrados");

    private String codigo;
    private String valor;

    private EUnidadMedidaArea(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
