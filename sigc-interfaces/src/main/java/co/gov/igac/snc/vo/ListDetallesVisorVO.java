package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.List;

public class ListDetallesVisorVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8290710581258388344L;

    private List<DetalleVisorVO> listaDetalles;
    private List<String> prediosNulos;

    public List<String> getPrediosNulos() {
        return prediosNulos;
    }

    public void setPrediosNulos(List<String> prediosNulos) {
        this.prediosNulos = prediosNulos;
    }

    public List<DetalleVisorVO> getListaDetalles() {
        return listaDetalles;
    }

    public void setListaDetalles(List<DetalleVisorVO> listaDetalles) {
        this.listaDetalles = listaDetalles;
    }

}
