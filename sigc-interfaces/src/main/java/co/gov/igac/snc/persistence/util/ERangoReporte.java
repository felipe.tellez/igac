/*
 Proyecto SNC 2017
 */
package co.gov.igac.snc.persistence.util;

/**
 * Rangos disponibles paa los reportes estadisticos
 * @author felipe.cadena
 */
public enum ERangoReporte {
    
    PRESTABLECIDOS("Preestablecidos", 1),
	ESTADISTICOS("Estadísticos", 2),
	PERSONALIZADOS("Personalizados", 3);
	
	private final String label;
	private final Integer codigo;

	ERangoReporte(String label, Integer codigo){
		this.label=label;
		this.codigo=codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getLabel() {
		return label;
	}

}
