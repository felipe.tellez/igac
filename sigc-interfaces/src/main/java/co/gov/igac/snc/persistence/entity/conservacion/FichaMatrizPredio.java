package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the FICHA_MATRIZ_PREDIO database table.
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia FICHA_MATRIZ_PREDIO_ID_SEQ juan.agudelo
 * 18-04-12 Adición del atributo transient valorAvaluo, este campo debe ser asignado desde un ejb
 *
 * david.cifuentes :: Adición de la variable estado :: 08/09/15 jonathan.chacon :: Adición de ficha
 * matriz proviene para englobe virtual
 */
@Entity
@Table(name = "FICHA_MATRIZ_PREDIO", schema = "SNC_CONSERVACION")
public class FichaMatrizPredio implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3071378639480746185L;

    private Long id;
    private Double coeficiente;
    private Date fechaLog;
    private String numeroPredial;
    private String usuarioLog;
    private FichaMatriz fichaMatriz;
    private String estado;
    private FichaMatriz provieneFichaMatriz;

    //Transient
    private Double valorAvaluo;

    public FichaMatrizPredio() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FICHA_MATRIZ_PREDIO_ID_SEQ")
    @SequenceGenerator(name = "FICHA_MATRIZ_PREDIO_ID_SEQ", sequenceName =
        "FICHA_MATRIZ_PREDIO_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "COEFICIENTE", precision = 16)
    public Double getCoeficiente() {
        return this.coeficiente;
    }

    public void setCoeficiente(Double coeficiente) {
        this.coeficiente = coeficiente;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to FichaMatriz
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public FichaMatriz getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(FichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVIENE_FICHA_MATRIZ_ID")
    public FichaMatriz getProvieneFichaMatriz() {
        return this.provieneFichaMatriz;
    }

    public void setProvieneFichaMatriz(FichaMatriz fichaMatriz) {
        this.provieneFichaMatriz = fichaMatriz;
    }

    //-------------------------------------------------------------------------
    @Transient
    public Double getValorAvaluo() {
        return valorAvaluo;
    }

    public void setValorAvaluo(Double valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static FichaMatrizPredio parseFichaMatrizPredio(HFichaMatrizPredio hFichaMatrizPredio) {
        FichaMatrizPredio fichaMatrizPredio = new FichaMatrizPredio();

        fichaMatrizPredio.setId(hFichaMatrizPredio.getId().getId());
        fichaMatrizPredio.setCoeficiente(hFichaMatrizPredio.getCoeficiente());
        fichaMatrizPredio.setFechaLog(hFichaMatrizPredio.getFechaLog());
        fichaMatrizPredio.setNumeroPredial(hFichaMatrizPredio.getNumeroPredial());
        fichaMatrizPredio.setUsuarioLog(hFichaMatrizPredio.getUsuarioLog());
        fichaMatrizPredio.setFichaMatriz(FichaMatriz.parseFichaMatriz(hFichaMatrizPredio.
            getHFichaMatriz()));
        fichaMatrizPredio.setEstado(hFichaMatrizPredio.getEstado());

        return fichaMatrizPredio;
    }

}
