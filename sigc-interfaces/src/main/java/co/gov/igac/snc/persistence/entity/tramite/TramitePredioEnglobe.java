package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;

/**
 * TramitePredioEnglobe entity.
 *
 * @deprecated Porque la tabla se llama TRAMITE_DETALLE_PREDIO. OJO Usar el entity y las clases
 * asociadas TramiteDetallePredio
 *
 * OJO: NO LE PEGA A UNA TABLA QUE SE LLAME IGUAL QUE LA ENTIDAD!!!!
 */

/*
 * Modificado FGWL predioId por Predio Secuencia
 *
 */
@Deprecated
@Entity
@NamedQueries({
    @NamedQuery(
        name = "findTramitePredioEnglobeByTramite",
        query = "from TramitePredioEnglobe tp where tp.tramite= :tramite")})
@Table(name = "TRAMITE_DETALLE_PREDIO", schema = "SNC_TRAMITE")
public class TramitePredioEnglobe implements java.io.Serializable {

    // Fields
    private Long id;
    private Tramite tramite;
    private Predio predio;
    private String englobePrincipal;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramitePredioEnglobe() {
    }

    /** full constructor */
    public TramitePredioEnglobe(Long id, Tramite tramite, Predio predio,
        String principal, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.predio = predio;
        this.englobePrincipal = principal;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_DETALLE_PREDIO_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_DETALLE_PREDIO_ID_SEQ", sequenceName =
        "TRAMITE_DETALLE_PREDIO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    /*
     * @modified by leidy.gonzalez::#15855::06/04/2016 Se elimina valor de nullable, debido a que no
     * es campo obligatorio para almacenar en la Base de Datos.
     */
    @Column(name = "ENGLOBE_PRINCIPAL", length = 2)
    public String getEnglobePrincipal() {
        return this.englobePrincipal;
    }

    public void setEnglobePrincipal(String principal) {
        this.englobePrincipal = principal;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
