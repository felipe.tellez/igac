/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles tipos de vinculación de un Profesional de avalúos Dominio
 * PROFESIONAL_TIPO_VINCULA
 *
 * @author felipe.cadena
 */
/*
 * @modified pedro.garcia 09-11-2012 adición del campo código y cambio de nombre al campo 'valor'
 * (antes 'codigo')
 */
public enum EProfesionalTipoVinculacion {

    CONTRATO_DE_PRESTACION_DE_SERVICIOS("1", "CONTRATO DE PRESTACION DE SERVICIOS"),
    CARRERA_ADMINISTRATIVA("2", "CARRERA ADMINISTRATIVA");

    private String codigo;
    private String valor;

    private EProfesionalTipoVinculacion(String codigoP, String valorP) {
        this.codigo = codigoP;
        this.valor = valorP;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
