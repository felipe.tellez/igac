package co.gov.igac.snc.util;

import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacion5Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class Utilidades {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utilidades.class);

    /**
     * Utilizado para determinar si al ejecutar un SP realmente se devolvieron mensajes de error o
     * son informativos
     *
     * @param mensajes
     * @return
     * @author fredy.wilches
     */
    public static boolean hayErrorEnEjecucionSP(Object mensajes[]) {
        if (mensajes != null) {
            int numeroErrores = 0;
            if (mensajes.length > 0) {
                for (Object o : mensajes) {
                    List l = (List) o;
                    if (l.size() > 0) {
                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                            }
                        }
                        if (numeroErrores > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @http://www.java-forums.org/advanced-java/4130-rounding-double-two-decimal-places.html
     * @author Joao
     *
     * gives you control of how many numbers after the point are needed
     * @param d number to round
     * @param c number of decimal places
     * @return
     */
    public static double roundToDecimals(double d, int c) {
        int temp = (int) ((d * Math.pow(10, c)));
        return (((double) temp) / Math.pow(10, c));
    }

    /**
     * retorna un double que es el resultado de redondear a c decimales el número d aplicando la
     * función techo a la parte decimal de d
     *
     * @author pedro.garcia
     *
     * gives you control of how many numbers after the point are needed
     * @param d número a redondear
     * @param c númeo de cifras decimales
     * @return
     */
    public static double roundToDecimalsAndCeil(double d, int c) {
        int temp = (int) (Math.ceil(d * Math.pow(10, c)));
        return (((double) temp) / Math.pow(10, c));
    }

    /**
     * Metodo que convierte un area dada en hectareas y m
     *
     * @author felipe.cadena
     * @param area
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     */
    public static int[] convertirAreaAformatoHectareas(Double area) {
        int[] result = new int[2];
        int aux;

        result[0] = (int) (area / 10000);
        aux = (result[0] * 10000);
        result[1] = (int) (area - aux);

        return result;
    }

    /**
     * Método para redondear un número a 2 cifras decimales
     *
     * @author javier.aponte
     * @param numero numero double que se desea redondear
     */
    public static double redondearNumeroADosCifrasDecimales(double numero) {

        BigDecimal numeroRedondeado;

        numeroRedondeado = new BigDecimal(numero);
        numeroRedondeado = numeroRedondeado.setScale(2, BigDecimal.ROUND_HALF_UP);

        return numeroRedondeado.doubleValue();

    }

    public static String delTildes(String inTexto) {
        String outTexto = inTexto;

        outTexto = outTexto.replace('á', 'a');
        outTexto = outTexto.replace('é', 'e');
        outTexto = outTexto.replace('í', 'i');
        outTexto = outTexto.replace('ó', 'o');
        outTexto = outTexto.replace('ú', 'u');

        outTexto = outTexto.replace('à', 'a');
        outTexto = outTexto.replace('è', 'e');
        outTexto = outTexto.replace('ì', 'i');
        outTexto = outTexto.replace('ò', 'o');
        outTexto = outTexto.replace('ù', 'u');

        outTexto = outTexto.replace('Á', 'A');
        outTexto = outTexto.replace('É', 'E');
        outTexto = outTexto.replace('Í', 'I');
        outTexto = outTexto.replace('Ó', 'O');
        outTexto = outTexto.replace('Ú', 'U');

        outTexto = outTexto.replace('À', 'A');
        outTexto = outTexto.replace('È', 'E');
        outTexto = outTexto.replace('Ì', 'I');
        outTexto = outTexto.replace('Ò', 'O');
        outTexto = outTexto.replace('Ù', 'U');

        return outTexto;
    }

    //-------------------------------------------------------------------------------------------------    
    /**
     * Recupera los parametros XML del job
     *
     * @author felipe.cadena
     * @param campoXML
     * @param path
     * @return
     */
    public static Map<String, String> obtenerParametrosXMLJob(String campoXML, String path) {

        Map<String, String> params = new HashMap<String, String>();
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(new ByteArrayInputStream(campoXML.getBytes("UTF-8")));

            XPathFactory factory = XPathFactory.newInstance();

            XPath xpath = factory.newXPath();
            XPathExpression expr = xpath.compile(path + "/name | " + path + "/Name");

            Object result = expr.evaluate(doc, XPathConstants.NODESET);

            NodeList nodesName = (NodeList) result;

            expr = xpath.compile(path + "/value | " + path + "/Value");

            result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodesValue = (NodeList) result;

            for (int i = 0; i < nodesName.getLength(); i++) {
                params.put(nodesName.item(i).getTextContent(), nodesValue.item(i).getTextContent());
            }

        } catch (Exception e) {
            LOGGER.error("error en Utilidades#obtenerParametrosXMLJob: " +
                e.getMessage());
        }
        return params;
    }

    /**
     * Retorna una lista de los atributos que coincidan con la ruta y el atributo enviados
     *
     * @author felipe.cadena
     *
     * @param campoXML
     * @param path
     * @param attrib
     * @return
     */
    public static List<String> obtenerAtributoXML(String campoXML, String path, String attrib) {

        List<String> attValues = new ArrayList<String>();

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(new ByteArrayInputStream(campoXML.getBytes("UTF-8")));

            XPathFactory factory = XPathFactory.newInstance();

            XPath xpath = factory.newXPath();
            XPathExpression expr = xpath.compile(path);

            Object result = expr.evaluate(doc, XPathConstants.NODESET);

            NodeList nodes = (NodeList) result;

            for (int i = 0; i < nodes.getLength(); i++) {
                if (nodes.item(i).hasAttributes()) {
                    attValues.add(nodes.item(i).getAttributes().getNamedItem(attrib).getNodeValue());
                }
            }

        } catch (Exception e) {
            LOGGER.error("error en Utilidades#obtenerParametrosXMLJob: " +
                e.getMessage());
        }

        return attValues;
    }

    /**
     * Método para convertir lista de tramitePredioEnglobe en TramiteDetallePredio
     *
     * @author felipe.cadena
     * @param tramitePredioEnglobe
     */
    public static List<TramiteDetallePredio> convertirTPEtoTDPMultiple(
        List<TramitePredioEnglobe> originales) {

        List<TramiteDetallePredio> resultado = new ArrayList<TramiteDetallePredio>();

        for (TramitePredioEnglobe tpe : originales) {
            TramiteDetallePredio pdp = new TramiteDetallePredio(tpe);
            resultado.add(pdp);
        }

        return resultado;

    }

    /**
     * Método para redondear un número a 1000
     *
     * @author leidy.gonzalez
     * @param numero numero double que se desea redondear
     */
    public static double redondearNumeroAMil(double numero) {

        Double numDiv = numero / 1000;

        Double numeroRedondeado =
            redondearCifrasDecimales(numDiv, 0);

        Double numMult = numeroRedondeado * 1000;

        numeroRedondeado = numMult;

        return numeroRedondeado.doubleValue();

    }

    /**
     * Método para redondear un número a n cantidad de cifras decimeles
     *
     * @author leidy.gonzalez
     * @param numero numero double que se desea redondear
     */
    public static double redondearCifrasDecimales(double numero, int cantidadCifras) {

        BigDecimal numeroRedondeado;

        numeroRedondeado = new BigDecimal(numero);
        numeroRedondeado = numeroRedondeado.setScale(cantidadCifras, BigDecimal.ROUND_HALF_UP);

        return numeroRedondeado.doubleValue();

    }

    /**
     * Retorna la forma textual del tipo de tramite compuesto basado en el codigo
     *
     * @author felipe.cadena
     * @param codigo
     * @return
     */
    public static String getTipoTramiteCadenaCompleto(String codigo) {

        String subtipo = null;
        String[] vals = (codigo).split("-");
        String tipoTramite = vals[0].trim();
        String claseMutacion = vals[1].trim();
        if (vals.length > 2) {
            subtipo = vals[2].trim();
        }

        StringBuilder tipoTramiteTemp = new StringBuilder();
        if (tipoTramite != null && !tipoTramite.isEmpty()) {

            for (ETramiteTipoTramite tt : ETramiteTipoTramite.values()) {
                if (tt.getCodigo().equals(tipoTramite)) {
                    tipoTramiteTemp.append(tt.getNombre());
                }
            }

            if (tipoTramite.equals(ETramiteTipoTramite.MUTACION
                .getCodigo())) {

                if (claseMutacion != null && !claseMutacion.isEmpty()) {

                    for (EMutacionClase mt : EMutacionClase.values()) {

                        if (mt.getCodigo().equals(claseMutacion)) {
                            tipoTramiteTemp.append(" ");
                            tipoTramiteTemp.append(mt.getNombre());
                        }
                    }

                    if (subtipo != null && !subtipo.isEmpty()) {

                        if (claseMutacion.equals(EMutacionClase.SEGUNDA
                            .getCodigo())) {

                            for (EMutacion2Subtipo mst : EMutacion2Subtipo
                                .values()) {

                                if (mst.getCodigo().equals(subtipo)) {

                                    tipoTramiteTemp.append(" ");
                                    tipoTramiteTemp.append(mst.getNombre());
                                }
                            }
                        } else if (claseMutacion
                            .equals(EMutacionClase.QUINTA.getCodigo())) {

                            for (EMutacion5Subtipo mqt : EMutacion5Subtipo
                                .values()) {

                                if (mqt.getCodigo().equals(subtipo)) {

                                    tipoTramiteTemp.append(" ");
                                    tipoTramiteTemp.append(mqt.getNombre());
                                }
                            }
                        }
                    }
                }
            }
        }

        return tipoTramiteTemp.toString();
    }

    public static String compileOrListCondition(List<Object> params, int splitSize, String field){

        int len = params.size();
        int items;
        String finalQuery = "";
        String queryItem = field + " in (:";
        String paramName = "lisParam";
        if(len<splitSize){
            queryItem += paramName+" )";
            return queryItem;
        }

        items = len/splitSize;

        for(int i = 0; i<items; i++){
            if(i>0) {
                finalQuery += " OR ";
            }
            finalQuery += (queryItem +paramName+i+")");
        }

        return finalQuery;

    }

    public static Query compileOrListParams(List<Object> params, int splitSize, Query query){

        int len = params.size();
        int items;
        String paramName = "lisParam";
        if(len<splitSize){
            query.setParameter(paramName, params);
            return query;
        }

        items = len/splitSize;

        for(int i = 0; i<items; i++){
            int fromIndex = i*splitSize;
            int toIndex = fromIndex+splitSize;
            if(toIndex>len){
                toIndex = len-1;
            }
            List<Object> subList = params.subList(fromIndex, toIndex);
            query.setParameter(paramName+i, subList);

        }

        return query;

    }

}
