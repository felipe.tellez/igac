package co.gov.igac.snc.persistence.entity.formacion;

import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * CalificacionConstruccion entity
 */

/* @modified pedro.garcia what: - cambio del constructor por defecto. Todos los campos (usados para
 * el treenode) son inicializados - cambio de tipo de datos de Byte(estúpido generador de myeclipse)
 * a Integer para los campos puntosResidencial, puntosComercial, puntosIndustrial - adición de
 * métodos toString(), hashCode(), equals() - adición de campo transient "puntos" - cambio de tipo
 * de datos Date a Date @modified david.cifuentes :: Se agregó el atributo validar y el método
 * transient is validarCampo ::14/12/2013
 */
@Entity
@Table(name = "CALIFICACION_CONSTRUCCION", schema = "SNC_FORMACION", uniqueConstraints =
    @UniqueConstraint(columnNames = {
    "COMPONENTE", "ELEMENTO", "DETALLE_CALIFICACION"}))
public class CalificacionConstruccion implements java.io.Serializable {

    // Fields
    private Long id;
    private String componente;
    private String elemento;
    private String detalleCalificacion;
    private Integer puntosResidencial;
    private Integer puntosComercial;
    private Integer puntosIndustrial;
    private String usuarioLog;
    private Date fechaLog;

    private String validar;

    //D: no borrar
    private int puntos;

    // Constructors
    /** default constructor */
    public CalificacionConstruccion() {
        this.componente = "";
        this.elemento = "";
        this.detalleCalificacion = "";

    }

    /** full constructor */
    public CalificacionConstruccion(Long id, String componente,
        String elemento, String detalleCalificacion,
        Integer puntosResidencial, Integer puntosComercial,
        Integer puntosIndustrial, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.componente = componente;
        this.elemento = elemento;
        this.detalleCalificacion = detalleCalificacion;
        this.puntosResidencial = puntosResidencial;
        this.puntosComercial = puntosComercial;
        this.puntosIndustrial = puntosIndustrial;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    //D.no borrar
    @Transient
    public int getPuntos() {
        return this.puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
//------------------------------------------------
    // Property accessors

    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "COMPONENTE", nullable = false, length = 100)
    public String getComponente() {
        return this.componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Column(name = "ELEMENTO", nullable = false, length = 100)
    public String getElemento() {
        return this.elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }

    @Column(name = "DETALLE_CALIFICACION", nullable = false, length = 100)
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    @Column(name = "PUNTOS_RESIDENCIAL", nullable = false, precision = 2, scale = 0)
    public Integer getPuntosResidencial() {
        return this.puntosResidencial;
    }

    public void setPuntosResidencial(Integer puntosResidencial) {
        this.puntosResidencial = puntosResidencial;
    }

    @Column(name = "PUNTOS_COMERCIAL", nullable = false, precision = 2, scale = 0)
    public Integer getPuntosComercial() {
        return this.puntosComercial;
    }

    public void setPuntosComercial(Integer puntosComercial) {
        this.puntosComercial = puntosComercial;
    }

    @Column(name = "PUNTOS_INDUSTRIAL", nullable = false, precision = 2, scale = 0)
    public Integer getPuntosIndustrial() {
        return this.puntosIndustrial;
    }

    public void setPuntosIndustrial(Integer puntosIndustrial) {
        this.puntosIndustrial = puntosIndustrial;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "VALIDAR")
    public String getValidar() {
        return this.validar;
    }

    public void setValidar(String validar) {
        this.validar = validar;
    }

    /**
     * Método transiente para verificar si el campo calificación construcción necesita ser validado.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isValidarCampo() {
        if (ESiNo.SI.getCodigo().equals(this.validar)) {
            return true;
        }
        return false;
    }

//-------  pilas!!!  -------------------------------------------------------------------------------
    /**
     * OJO:
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append(this.id).append(stringSeparator);
        objectAsString.append(this.componente).append(stringSeparator);
        objectAsString.append(this.elemento).append(stringSeparator);
        objectAsString.append(this.detalleCalificacion).append(stringSeparator);
        objectAsString.append(this.puntos);

        return objectAsString.toString();
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }

    /**
     * este lo genera el netbeans automáticamente al implementar el hashCode()
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CalificacionConstruccion other = (CalificacionConstruccion) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.componente == null) ? (other.componente != null) : !this.componente.equals(
            other.componente)) {
            return false;
        }
        if ((this.elemento == null) ? (other.elemento != null) : !this.elemento.equals(
            other.elemento)) {
            return false;
        }
        if ((this.detalleCalificacion == null) ? (other.detalleCalificacion != null) :
            !this.detalleCalificacion.equals(other.detalleCalificacion)) {
            return false;
        }
        if (this.puntos != other.puntos) {
            return false;
        }
        return true;
    }
}
