package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the AVALUO_PREDIO_ZONA database table.
 *
 */
@Entity
@Table(name = "AVALUO_PREDIO_ZONA")
public class AvaluoPredioZona implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    //private Double areaHectareas;
    private Double area;
    private Avaluo avaluo;
    private Long avaluoPredioId;
    private String descripcion;
    private Date fechaLog;
    private String usuarioLog;
    private Double valorTerreno;
    private Double valorUnitarioTerreno;

    //Transients para mostrar información del predio
    private String numeroPredial;
    private String codigoMunicipio;
    private String codigoDepartamento;

    public AvaluoPredioZona() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PREDIO_ZONA_ID_GENERATOR", sequenceName =
        "AVALUO_PREDIO_ZONA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_PREDIO_ZONA_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA")
    public Double getArea() {
        return this.area;
    }

    public void setArea(Double areaM2) {
        this.area = areaM2;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "AVALUO_PREDIO_ID")
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TERRENO")
    public Double getValorTerreno() {
        return this.valorTerreno;
    }

    public void setValorTerreno(Double valorTerreno) {
        this.valorTerreno = valorTerreno;
    }

    @Column(name = "VALOR_UNITARIO_TERRENO")
    public Double getValorUnitarioTerreno() {
        return this.valorUnitarioTerreno;
    }

    public void setValorUnitarioTerreno(Double valorUnitarioTerreno) {
        this.valorUnitarioTerreno = valorUnitarioTerreno;
    }

    @Column(name = "DESCRIPCION")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcionP) {
        this.descripcion = descripcionP;
    }

//---------------
    @Transient
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Transient
    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    @Transient
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

}
