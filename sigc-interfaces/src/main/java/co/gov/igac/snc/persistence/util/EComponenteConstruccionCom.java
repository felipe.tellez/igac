/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los 'componentes' de una construcción. Los valores son correspondientes al
 * dominio COMPONENTE_CONSTRUCCION_COM
 *
 * @author pedro.garcia
 */
public enum EComponenteConstruccionCom {

    ACABADOS_PRINCIPALES("ACABADOS PRINCIPALES"),
    BANIO("BAÑO"),
    COCINA("COCINA"),
    COMPLEMENTO_INDUSTRIA("COMPLEMENTO INDUSTRIA"),
    ESTRUCTURA("ESTRUCTURA");

    private String codigo;

    private EComponenteConstruccionCom(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
