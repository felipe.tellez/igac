package co.gov.igac.snc.fachadas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;
import co.gov.igac.snc.persistence.entity.formacion.ParametroDecretoCondicion;
import co.gov.igac.snc.persistence.entity.formacion.TablaTerreno;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.entity.vistas.VPredioAvaluoDecreto;

/**
 * Interfaz para los métodos de conservación
 */
@Local
public interface IFormacionLocal {

    /**
     * Retorna la lista de todos los {@link DecretoAvaluoAnual}
     *
     * @author david.cifuentes
     * @return
     */
    public List<DecretoAvaluoAnual> buscarDecretos();

    /**
     * Método que guarda un {@link DecretoAvaluoAnual}
     *
     * @author david.cifuentes
     * @param DecretoAvaluoAnual
     * @return
     */
    public DecretoAvaluoAnual guardarDecreto(DecretoAvaluoAnual decretoNuevo);

    /**
     * Método que elimina un array de {@link DecretoAvaluoAnual}
     *
     * @author david.cifuentes
     * @param DecretoAvaluoAnual[] selectedDecretos
     */
    public void eliminarDecretos(DecretoAvaluoAnual[] selectedDecretos);

    /**
     * Método que consulta todos los {@link ParametroDecretoCondicion}
     *
     * @author david.cifuentes
     * @return
     */
    public List<ParametroDecretoCondicion> consultarParametrosDecretoCondicion();

    /**
     * Método que elimina un {@link DecretoCondicion}
     *
     * @author david.cifuentes
     * @param DecretoAvaluoAnual condicional
     */
    public void eliminarCondicional(DecretoCondicion condicional);

    /**
     * Método que valida un {@link DecretoCondicion} de un {@link DecretoAvaluoAnual}.
     *
     * @author javier.aponte
     * @param Long
     * @return
     */
    public Object[] validarCondicional(Long decretoCondicionId);

    /**
     * Método que guarda un {@link DecretoCondicion} de un {@link DecretoAvaluoAnual}.
     *
     * @author david.cifuentes
     * @param DecretoCondicion
     * @return
     */
    public DecretoCondicion guardarCondicional(DecretoCondicion condicional);

    /**
     * Método que genera un reporte que me muestra los predios afectados por las
     * {@link DecretoCondicion} del {@link DecretoAvaluoAnual}. seleccionado
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    public Object[] buscarCalculoDePrediosAfectadoPorDecreto(Long decretoId);

    /**
     * Método que genera el calculo de avaluos para un predio, se agrega parametro de usuario
     *
     * @author david.cifuentes
     * @modified javier.aponte
     * @param Long, Long, Date, Usuario
     * @return
     */
    public Object[] recalcularAvaluosDecretoParaUnPredio(Long tramiteId, Long predioId,
        Date fechaDelCalculo, UsuarioDTO usuario);

    /**
     * Método que busca los avaluos históricos de un pPredio
     *
     * @author david.cifuentes
     * @param Long
     * @return
     */
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredio(
        Long pPredioId);

    /**
     * Método que busca los avaluos históricos de un {@link PPredio} en la vista de
     * {@link VPPredioAvaluoDecreto}
     *
     * @author david.cifuentes
     * @param fechaDelCalculo
     * @param Long
     * @return
     */
    public List<VPPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPPredioParaVPPredioAvaluoDecreto(
        Long pPredioId, Date fechaDelCalculo);

    /**
     * Método que revierte la proyección de avalúos para un predio.
     *
     * @param Long
     * @param Long
     * @return
     * @author javier.aponte
     */
    public Object[] revertirProyeccionAvaluosParaUnPredio(Long tramiteId, Long predioId);

    /**
     * Retorna la tabla de liquidación para una zona y vigencia
     *
     * @param zona
     * @param vigencia
     * @return
     */
    public List<TablaTerreno> findByZonaVigencia(String zona, Date vigencia);

    /**
     * Método que retorna el {@link DecretoAvaluoAnual} registrado para el presente año.
     *
     * @author david.cifuentes
     * @return
     */
    public DecretoAvaluoAnual buscarDecretoActual();

    /**
     * Método que retorna el {@link DecretoAvaluoAnual} registrado para la vigencia del año enviada
     * como parametro.
     *
     * @author david.cifuentes
     *
     * @param {@link String} con el formato yyyy del año.
     * @return
     */
    public DecretoAvaluoAnual buscarDecretoPorVigencia(String anioVigencia);

    /**
     * Método que genera la liquidación de avaluos para un predio,
     *
     * @modified javier.aponte
     * @param Long, Long, Date, Usuario
     * @return
     */
    public Object[] liquidarAvaluosParaUnPredio(Long tramiteId, Long predioId, Date fechaDelCalculo,
        UsuarioDTO usuario);

    /**
     * Método que recupera todos los registros de calificación anexo
     *
     * @author javier.aponte
     * @return
     */
    public List<CalificacionAnexo> getAllCalificacionAnexo();

    /**
     * Método que busca los avaluos históricos de un {@link Predio} en la vista de
     * {@link VPredioAvaluoDecreto}
     *
     * @author leidy.gonzalez :: #12528::26/05/2015::CU_187
     * @param pPredioId
     * @return
     */
    public ArrayList<VPredioAvaluoDecreto> buscarAvaluosCatastralesPorIdPredio(
        Long pPredioId);

}
