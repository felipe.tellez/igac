/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_FORMA_NEGOCIACION
 *
 * @author christian.rodriguez
 */
public enum EOfertaFormaNegociacion {

    //D: código (valor)
    CALCULADA("Calculada"),
    DEPURADA("Depurada");

    private String valor;

    private EOfertaFormaNegociacion(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return this.valor;
    }
}
