package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONVENIO database table.
 *
 */
@NamedQueries(@NamedQuery(name = "recuperarConveniosPorIdActualizacion", query =
    "SELECT c FROM Convenio c WHERE c.actualizacion.id = :idActualizacion"))
@Entity
@Table(name = "CONVENIO", schema = "SNC_ACTUALIZACION")
public class Convenio implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1878926985841868044L;
    private Long id;
    private String clase;
    private String estado;
    private Date fechaLog;
    private String numero;
    private String objeto;
    private Date plazoEjecucion;
    private Long soporteDocumentoId;
    private String tipoProyecto;
    private String usuarioLog;
    private Double valor;
    private String zona;
    private List<ConvenioEntidad> convenioEntidads;
    private Actualizacion actualizacion;

    public Convenio() {
    }

    @Id
    @SequenceGenerator(name = "CONVENIO_ID_SEQ",
        sequenceName = "CONVENIO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONVENIO_ID_SEQ")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 30)
    public String getClase() {
        return this.clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    @Column(nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, length = 20)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(length = 2000)
    public String getObjeto() {
        return this.objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "PLAZO_EJECUCION", nullable = false)
    public Date getPlazoEjecucion() {
        return this.plazoEjecucion;
    }

    public void setPlazoEjecucion(Date plazoEjecucion) {
        this.plazoEjecucion = plazoEjecucion;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID", precision = 10)
    public Long getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Long soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "TIPO_PROYECTO", nullable = false, length = 30)
    public String getTipoProyecto() {
        return this.tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(nullable = false, precision = 18, scale = 2)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(nullable = false, length = 30)
    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    // bi-directional many-to-one association to ConvenioEntidad
    @OneToMany(mappedBy = "convenio")
    public List<ConvenioEntidad> getConvenioEntidads() {
        return this.convenioEntidads;
    }

    public void setConvenioEntidads(List<ConvenioEntidad> convenioEntidads) {
        this.convenioEntidads = convenioEntidads;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

}
