package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * Clase que reduce la informacion de predioZonas para ser mostradas en el visor Web
 *
 * @author franz.gamba
 *
 */
public class ZonaVO implements Serializable {

    private static final long serialVersionUID = 2249817785924600145L;

    private String zonaGeoeconomica;
    private String zonaFisica;

    /**
     * Empty constructor
     */
    public ZonaVO() {

    }

    /**
     * Full constructor
     */
    public ZonaVO(String zonaGeoeconomica, String zonaFisica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
        this.zonaFisica = zonaFisica;
    }

    public String getZonaGeoeconomica() {
        return zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    public String getZonaFisica() {
        return zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

}
