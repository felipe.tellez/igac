package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/*
 * @author leidy.gonzalez
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "getMPredioByNumeroPredial",
        query =
        "from MPredio p join fetch p.departamento join fetch p.municipio where p.numeroPredial = :numeroPredial")})

@Table(name = "M_PREDIO", schema = "SNC_CONSERVACION")
public class MPredio implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -2839349185829646384L;

    private Long id;
    private Tramite tramite;
    private Departamento departamento;
    private Municipio municipio;
    private String tipoAvaluo;
    private String sectorCodigo;
    private String barrioCodigo;
    private String manzanaCodigo;
    private String predioId;
    private Predio predio;
    private String condicionPropiedad;
    private String edificio;
    private String piso;
    private String unidad;
    private String numeroPredial;
    private String numeroPredialAnterior;
    private String nip;
    private BigDecimal consecutivoCatastral;
    private BigDecimal consecutivoCatastralAnterior;
    private String zonaUnidadOrganica;
    private String localidadCodigo;
    private String corregimientoCodigo;
    private String nombre;
    private String destino;
    private String tipo;
    private Double areaTerreno;
    private CirculoRegistral circuloRegistral;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String tipoCatastro;
    private String este;
    private String norte;
    private String estado;
    private String estrato;
    private String chip;
    private String cancelaInscribe;
    private String numeroUltimaResolucion;
    private Short anioUltimaResolucion;
    private String usuarioLog;
    private Date fechaLog;
    private Double areaConstruccion;
    // Constructors

    /** default constructor */
    public MPredio() {
    }

    /** minimal constructor */
    public MPredio(Long id, Departamento departamento, Municipio municipio,
        String tipoAvaluo, String sectorCodigo, String barrioCodigo,
        String manzanaCodigo, String predioId, Predio predio, String condicionPropiedad,
        String edificio, String piso, String unidad, String numeroPredial,
        String destino, String tipo, Double areaTerreno,
        String tipoCatastro, String estado, String cancelaInscribe,
        String usuarioLog, Date fechaLog, Double areaConstruccion) {
        this.id = id;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predioId = predioId;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.tipoCatastro = tipoCatastro;
        this.estado = estado;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.areaConstruccion = areaConstruccion;
    }

    /** full constructor */
    public MPredio(Long id, Tramite tramite,
        Departamento departamento, Municipio municipio, String tipoAvaluo,
        String sectorCodigo, String barrioCodigo, String manzanaCodigo,
        String predioId, Predio predio, String condicionPropiedad, String edificio,
        String piso, String unidad, String numeroPredial,
        String numeroPredialAnterior, String nip,
        BigDecimal consecutivoCatastral,
        BigDecimal consecutivoCatastralAnterior, String zonaUnidadOrganica,
        String localidadCodigo, String corregimientoCodigo, String nombre,
        String destino, String tipo, Double areaTerreno,
        CirculoRegistral circuloRegistral, String numeroRegistro,
        String numeroRegistroAnterior, String tipoCatastro, String este,
        String norte, String estado, String estrato, String chip,
        String cancelaInscribe, String numeroUltimaResolucion,
        Short anioUltimaResolucion, String usuarioLog, Date fechaLog,
        List<PFichaMatriz> PFichaMatrizs, List<PPredioZona> PPredioZonas,
        List<PPredioDireccion> PPredioDireccions,
        List<PPredioServidumbre> PPredioServidumbre, List<PFoto> PFotos,
        List<PPersonaPredio> PPersonaPredios,
        List<PReferenciaCartografica> PReferenciaCartograficas,
        List<PUnidadConstruccion> PUnidadConstruccions,
        Date fechaInscripcionCatastral, Date fechaRegistro,
        List<PPredioAvaluoCatastral> PPredioAvaluoCatastrals,
        Double areaConstruccion) {
        this.id = id;
        this.tramite = tramite;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predioId = predioId;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.nip = nip;
        this.consecutivoCatastral = consecutivoCatastral;
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
        this.zonaUnidadOrganica = zonaUnidadOrganica;
        this.localidadCodigo = localidadCodigo;
        this.corregimientoCodigo = corregimientoCodigo;
        this.nombre = nombre;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.circuloRegistral = circuloRegistral;
        this.numeroRegistro = numeroRegistro;
        this.numeroRegistroAnterior = numeroRegistroAnterior;
        this.tipoCatastro = tipoCatastro;
        this.este = este;
        this.norte = norte;
        this.estado = estado;
        this.estrato = estrato;
        this.chip = chip;
        this.cancelaInscribe = cancelaInscribe;
        this.numeroUltimaResolucion = numeroUltimaResolucion;
        this.anioUltimaResolucion = anioUltimaResolucion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.areaConstruccion = areaConstruccion;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "M_PREDIO_ID_SEQ")
    @GenericGenerator(name = "M_PREDIO_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {
            @Parameter(name = "sequence", value = "M_PREDIO_ID_SEQ"),
            @Parameter(name = "allocationSize", value = "1")})
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    // D: cambiados para reflejar el cambio de tipo de dato en estas columnas
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "TIPO_AVALUO", nullable = false, length = 2)
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @Column(name = "SECTOR_CODIGO", nullable = false, length = 2)
    public String getSectorCodigo() {
        return this.sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    @Column(name = "BARRIO_CODIGO", nullable = false, length = 4)
    public String getBarrioCodigo() {
        return this.barrioCodigo;
    }

    public void setBarrioCodigo(String barrioCodigo) {
        this.barrioCodigo = barrioCodigo;
    }

    @Column(name = "MANZANA_CODIGO", nullable = false, length = 4)
    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    @Column(name = "CONDICION_PROPIEDAD", nullable = false, length = 1)
    public String getCondicionPropiedad() {
        return this.condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    @Column(name = "EDIFICIO", nullable = false, length = 2)
    public String getEdificio() {
        return this.edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    @Column(name = "PISO", nullable = false, length = 2)
    public String getPiso() {
        return this.piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    @Column(name = "UNIDAD", nullable = false, length = 4)
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_ANTERIOR", length = 20)
    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    @Column(name = "NIP", length = 20)
    public String getNip() {
        return this.nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastral() {
        return this.consecutivoCatastral;
    }

    public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL_ANTERIOR", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastralAnterior() {
        return this.consecutivoCatastralAnterior;
    }

    public void setConsecutivoCatastralAnterior(
        BigDecimal consecutivoCatastralAnterior) {
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", length = 30)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    @Column(name = "LOCALIDAD_CODIGO", length = 7)
    public String getLocalidadCodigo() {
        return this.localidadCodigo;
    }

    public void setLocalidadCodigo(String localidadCodigo) {
        this.localidadCodigo = localidadCodigo;
    }

    @Column(name = "CORREGIMIENTO_CODIGO", length = 7)
    public String getCorregimientoCodigo() {
        return this.corregimientoCodigo;
    }

    public void setCorregimientoCodigo(String corregimientoCodigo) {
        this.corregimientoCodigo = corregimientoCodigo;
    }

    @Column(name = "NOMBRE", length = 250)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "DESTINO", nullable = false, length = 30)
    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 18, scale = 2)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

//------------------
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIRCULO_REGISTRAL", nullable = true)
    public CirculoRegistral getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(CirculoRegistral circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }
//------------------

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR", length = 20)
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "TIPO_CATASTRO", nullable = false, length = 2)
    public String getTipoCatastro() {
        return this.tipoCatastro;
    }

    public void setTipoCatastro(String tipoCatastro) {
        this.tipoCatastro = tipoCatastro;
    }

    @Column(name = "ESTE", length = 20)
    public String getEste() {
        return this.este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    @Column(name = "NORTE", length = 20)
    public String getNorte() {
        return this.norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRATO", length = 30)
    public String getEstrato() {
        return this.estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    @Column(name = "CHIP", length = 14)
    public String getChip() {
        return this.chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "NUMERO_ULTIMA_RESOLUCION", length = 30)
    public String getNumeroUltimaResolucion() {
        return this.numeroUltimaResolucion;
    }

    public void setNumeroUltimaResolucion(String numeroUltimaResolucion) {
        this.numeroUltimaResolucion = numeroUltimaResolucion;
    }

    @Column(name = "ANIO_ULTIMA_RESOLUCION", precision = 4, scale = 0)
    public Short getAnioUltimaResolucion() {
        return this.anioUltimaResolucion;
    }

    public void setAnioUltimaResolucion(Short anioUltimaResolucion) {
        this.anioUltimaResolucion = anioUltimaResolucion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "AREA_CONSTRUCCION", nullable = false, precision = 18)
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "PREDIO_ID", nullable = false, length = 10)
    public String getPredioId() {
        return predioId;
    }

    public void setPredioId(String predioId) {
        this.predioId = predioId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO", nullable = true)
    public Predio getPredio() {
        return predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

}
