package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correpondiente al dominio SOLICITANTE_SOLICITUD_RELAC La enumeración (cada miembro)
 * corresponde al código y el atributo es el valor de dominio
 *
 * @author fabio.chambonete
 */
/*
 * @documented pedro.garcia @modified pedro.garcia 31-07-2012 se cambia el atributo y el método a
 * 'valor' y 'getValor' porque lo que se usa como tales corresponde al valor en el dominio
 */
public enum ESolicitanteSolicitudRelac {

    //OJO: El nombre de la enumeración debe corresponder al código del dominio.
    //     Si se cambia eso, debe revisarse todo el código y hacer los cambios necesarios donde se
    //     use esta enumeración
    AFECTADO("Afectado"),
    APODERADO("Apoderado"),
    CONTACTO("Contacto"),
    ENTE("Ente"),
    INTERMEDIARIO("Intermediario"),
    TESORERIA("Tesorería"),
    PROPIETARIO("Propietario"),
    REPRESENTANTE_LEGAL("Representante");

    private String valor;

    ESolicitanteSolicitudRelac(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return this.valor;
    }

}
