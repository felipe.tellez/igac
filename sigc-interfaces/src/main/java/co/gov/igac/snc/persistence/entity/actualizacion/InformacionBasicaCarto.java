package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the INFORMACION_BASICA_CARTO database table.
 *
 */
@Entity
@Table(name = "INFORMACION_BASICA_CARTO")
public class InformacionBasicaCarto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2289937106045499181L;

    private Long id;
    private String descripcion;
    private Date fechaLog;
    private String informacion;
    private String servicio;
    private String tipo;
    private String usuarioLog;
    private Actualizacion actualizacion;

    public InformacionBasicaCarto() {
    }

    @Id
    @SequenceGenerator(name = "INFO_BASICA_CART_ID_GENERATOR",
        sequenceName = "INFORMACION_BASICA_CART_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INFO_BASICA_CART_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 2000)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, length = 600)
    public String getInformacion() {
        return this.informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    @Column(length = 600)
    public String getServicio() {
        return this.servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    @Column(nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG", nullable = false)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

}
