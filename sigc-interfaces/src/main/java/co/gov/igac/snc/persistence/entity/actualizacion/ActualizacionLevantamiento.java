package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ACTUALIZACION_LEVANTAMIENTO database table.
 *
 */
@Entity
@Table(name = "ACTUALIZACION_LEVANTAMIENTO")
public class ActualizacionLevantamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String identificador;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private LevantamientoAsignacion levantamientoAsignacion;
    private List<ControlCalidadMuestra> controlCalidadMuestras;

    public ActualizacionLevantamiento() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_LEVANTAMIENTO_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_LEVANTAMI_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_LEVANTAMIENTO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getIdentificador() {
        return this.identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to LevantamientoAsignacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LEVANTAMIENTO_ASIGNACION_ID")
    public LevantamientoAsignacion getLevantamientoAsignacion() {
        return this.levantamientoAsignacion;
    }

    public void setLevantamientoAsignacion(LevantamientoAsignacion levantamientoAsignacion) {
        this.levantamientoAsignacion = levantamientoAsignacion;
    }

    //bi-directional many-to-one association to ControlCalidadMuestra
    @OneToMany(mappedBy = "actualizacionLevantamiento")
    public List<ControlCalidadMuestra> getControlCalidadMuestras() {
        return this.controlCalidadMuestras;
    }

    public void setControlCalidadMuestras(List<ControlCalidadMuestra> controlCalidadMuestras) {
        this.controlCalidadMuestras = controlCalidadMuestras;
    }
}
