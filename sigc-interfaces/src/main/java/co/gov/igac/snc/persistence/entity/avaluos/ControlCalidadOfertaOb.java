package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the CONTROL_CALIDAD_OFERTA_OBS database table.
 *
 */
@Entity
@Table(name = "CONTROL_CALIDAD_OFERTA_OBS")
public class ControlCalidadOfertaOb implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String aprobado;
    private Date fecha;
    private Date fechaLog;
    private String motivo;
    private String usuario;
    private String usuarioLog;
    private ControlCalidadOferta controlCalidadOferta;

    public ControlCalidadOfertaOb() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTROL_CALIDAD_OFERTA__ID_SEQ")
    @SequenceGenerator(name = "CONTROL_CALIDAD_OFERTA__ID_SEQ", sequenceName =
        "CONTROL_CALIDAD_OFERTA__ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAprobado() {
        return this.aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getUsuario() {
        return this.usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ControlCalidadOferta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTROL_CALIDAD_OFERTA_ID")
    public ControlCalidadOferta getControlCalidadOferta() {
        return this.controlCalidadOferta;
    }

    public void setControlCalidadOferta(ControlCalidadOferta controlCalidadOferta) {
        this.controlCalidadOferta = controlCalidadOferta;
    }

}
