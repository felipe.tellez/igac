package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;

/**
 * Clase que reduce la informacion de la ficha matriz para ser mostrada en las tareas geograficas
 *
 * @author franz.gamba
 */
public class FichaMatrizVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8948709862551748215L;
    private Long id;
    private String cancelaInscribe;
    private Double areaTotalConstruidaComun;
    private Double areaTotalConstruidaPrivada;
    private Double areaTotalTerrenoComun;
    private Double areaTotalTerrenoPrivada;
    private Long totalUnidadesPrivadas;
    private Double valorTotalAvaluoCatastral;
    private Long totalUnidadesSotanos;
    private List<FichaMatrizModeloVO> fichaMatrizModelos;
    private List<FichaMatrizPredioVO> fichaMatrizPredios;
    private List<FichaMatrizPredioTerrenoVO> fichaMatrizPrediosTerreno;
    private List<FichaMatrizTorreVO> fichaMatrizTorres;

    /** Empty constructor */
    public FichaMatrizVO() {

    }

    /** Constructor a partir de una fichaMatriz */
    public FichaMatrizVO(FichaMatriz fichaMatriz) {
        this.id = fichaMatriz.getId();
        this.areaTotalConstruidaComun = fichaMatriz.getAreaTotalConstruidaComun();
        this.areaTotalConstruidaPrivada = fichaMatriz.getAreaTotalConstruidaPrivada();
        this.areaTotalTerrenoComun = fichaMatriz.getAreaTotalTerrenoComun();
        this.areaTotalTerrenoPrivada = fichaMatriz.getAreaTotalTerrenoPrivada();
        this.totalUnidadesPrivadas = fichaMatriz.getTotalUnidadesPrivadas();
        this.totalUnidadesSotanos = fichaMatriz.getTotalUnidadesSotanos();
        this.valorTotalAvaluoCatastral = fichaMatriz.getValorTotalAvaluoCatastral();

        if (fichaMatriz.getPredio().getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_1.
            getCodigo())) {

            if (fichaMatriz.getFichaMatrizModelos() != null && !fichaMatriz.getFichaMatrizModelos().
                isEmpty()) {
                List<FichaMatrizModeloVO> modelos = new ArrayList<FichaMatrizModeloVO>();
                for (FichaMatrizModelo m : fichaMatriz.getFichaMatrizModelos()) {
                    modelos.add(new FichaMatrizModeloVO(m));
                }
                this.fichaMatrizModelos = modelos;
            }

            //Llena los datos de las torres, ubicando a los predios dentro de la torre correspondiente
            if (fichaMatriz.getFichaMatrizTorres() != null && !fichaMatriz.getFichaMatrizTorres().
                isEmpty()) {
                List<FichaMatrizTorreVO> torres = new ArrayList<FichaMatrizTorreVO>();
                List<Long> numtorres = new ArrayList<Long>();

                for (FichaMatrizTorre t : fichaMatriz.getFichaMatrizTorres()) {
                    torres.add(new FichaMatrizTorreVO(t));
                    numtorres.add(t.getTorre());
                }

                Collections.sort(numtorres);

                if (fichaMatriz.getFichaMatrizPredios() != null && !fichaMatriz.
                    getFichaMatrizPredios().isEmpty()) {
                    //Almacena los predios segun sus torres
                    for (FichaMatrizTorreVO t : torres) {
                        List<FichaMatrizPisoVO> pisosNuevos = new ArrayList<FichaMatrizPisoVO>();
                        for (int i = 1; i <= t.getPisos(); i++) {
                            FichaMatrizPisoVO pisoNuevo = new FichaMatrizPisoVO();
                            pisoNuevo.setNumero(new Long(i));
                            pisosNuevos.add(pisoNuevo);
                        }
                        for (FichaMatrizPisoVO pn : pisosNuevos) {
                            List<FichaMatrizPredioVO> prediosVo =
                                new ArrayList<FichaMatrizPredioVO>();
                            for (FichaMatrizPredio p : fichaMatriz.getFichaMatrizPredios()) {
                                if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                    getTorre() &&
                                    Long.parseLong(p.getNumeroPredial().substring(24, 26)) == pn.
                                    getNumero() &&
                                    Long.parseLong(p.getNumeroPredial().substring(26, 27)) != 9L) {
                                    prediosVo.add(new FichaMatrizPredioVO(p));
                                }
                            }
                            pn.setPredios(prediosVo);
                        }
                        t.setFichaMatrizPisos(pisosNuevos);
                        //---------------------------------------------
                        // SOTANOS
                        //---------------------------------------------
                        List<FichaMatrizPredioVO> sotanos = new ArrayList<FichaMatrizPredioVO>();
                        for (FichaMatrizPredio p : fichaMatriz.getFichaMatrizPredios()) {
                            if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                getTorre() &&
                                (Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 9 ||
                                Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 8)) {
                                sotanos.add(new FichaMatrizPredioVO(p));
                            }
                        }
                        if (!sotanos.isEmpty()) {
                            //Se calcula el numero de 'Pisos' en los sotanos
                            Long numSotanos = new Long(0);
                            for (FichaMatrizPredioVO s : sotanos) {

                                if (100 - Long.parseLong(s.getNumeroPredial().substring(24, 26)) >
                                    numSotanos) {
                                    numSotanos = 100 - Long.parseLong(s.getNumeroPredial().
                                        substring(24, 26));
                                }
                            }
                            List<FichaMatrizSotanoVO> sotanosTorre =
                                new ArrayList<FichaMatrizSotanoVO>();
                            for (int i = 0; i < numSotanos; i++) {
                                FichaMatrizSotanoVO sota = new FichaMatrizSotanoVO();
                                sota.setNumero("S" + (i + 1));
                                sotanosTorre.add(sota);
                            }
                            //Se asignan los predios a los respectivos sotanos
                            for (FichaMatrizSotanoVO soT : sotanosTorre) {
                                List<FichaMatrizPredioVO> soNuevo =
                                    new ArrayList<FichaMatrizPredioVO>();
                                for (FichaMatrizPredioVO pre : sotanos) {
                                    if (100 - Long.parseLong(pre.getNumeroPredial().
                                        substring(24, 26)) == Long.parseLong(soT.getNumero().
                                        substring(1))) {
                                        soNuevo.add(pre);
                                    }
                                }
                                if (!soNuevo.isEmpty()) {
                                    soT.setPredios(soNuevo);
                                }
                            }
                            t.setFichaMatrizSotanos(sotanosTorre);
                        }
                    }
                }
                this.fichaMatrizTorres = torres;
            }

            if (fichaMatriz.getFichaMatrizPredios() != null && !fichaMatriz.getFichaMatrizPredios().
                isEmpty()) {

                List<FichaMatrizPredioVO> predios = new ArrayList<FichaMatrizPredioVO>();
                for (FichaMatrizPredio p : fichaMatriz.getFichaMatrizPredios()) {
                    predios.add(new FichaMatrizPredioVO(p));
                }
                this.fichaMatrizPredios = predios;
            }

            if (fichaMatriz.getFichaMatrizPredioTerrenos() != null && !fichaMatriz.
                getFichaMatrizPredioTerrenos().isEmpty()) {

                List<FichaMatrizPredioTerrenoVO> prediosTerreno =
                    new ArrayList<FichaMatrizPredioTerrenoVO>();
                for (FichaMatrizPredioTerreno p : fichaMatriz.getFichaMatrizPredioTerrenos()) {
                    prediosTerreno.add(new FichaMatrizPredioTerrenoVO(p));
                }
                this.fichaMatrizPrediosTerreno = prediosTerreno;

            }

        } else {

            //Llena los modelos a partir de los modelos de la ficha matriz origen
            if (fichaMatriz.getFichaMatrizModelos() != null && !fichaMatriz.getFichaMatrizModelos().
                isEmpty()) {
                List<FichaMatrizModeloVO> modelos = new ArrayList<FichaMatrizModeloVO>();
                for (FichaMatrizModelo m : fichaMatriz.getFichaMatrizModelos()) {
                    modelos.add(new FichaMatrizModeloVO(m));
                }

                this.fichaMatrizModelos = modelos;
            }

            //Llena los datos de las torres, ubicando a los predios dentro de la torre correspondiente
            if (fichaMatriz.getFichaMatrizTorres() != null && !fichaMatriz.getFichaMatrizTorres().
                isEmpty()) {

                List<FichaMatrizTorreVO> torres = new ArrayList<FichaMatrizTorreVO>();
                List<Long> numtorres = new ArrayList<Long>();

                for (FichaMatrizTorre t : fichaMatriz.getFichaMatrizTorres()) {
                    torres.add(new FichaMatrizTorreVO(t));
                    numtorres.add(t.getTorre());
                }

                Collections.sort(numtorres);

                if (fichaMatriz.getFichaMatrizPredios() != null && !fichaMatriz.
                    getFichaMatrizPredios().isEmpty()) {
                    //Almacena los predios segun sus torres
                    for (FichaMatrizTorreVO t : torres) {
                        //---------------------------------------------
                        // PISOS
                        //---------------------------------------------
                        List<FichaMatrizPisoVO> pisosNuevos = new ArrayList<FichaMatrizPisoVO>();
                        for (int i = 1; i <= t.getPisos(); i++) {
                            FichaMatrizPisoVO pisoNuevo = new FichaMatrizPisoVO();
                            pisoNuevo.setNumero(new Long(i));
                            pisosNuevos.add(pisoNuevo);
                        }
                        for (FichaMatrizPisoVO pn : pisosNuevos) {
                            List<FichaMatrizPredioVO> prediosVo =
                                new ArrayList<FichaMatrizPredioVO>();
                            for (FichaMatrizPredio p : fichaMatriz.getFichaMatrizPredios()) {
                                if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                    getTorre() &&
                                    Long.parseLong(p.getNumeroPredial().substring(
                                        24, 26)) == pn.getNumero() &&
                                    Long.parseLong(p.getNumeroPredial().substring(
                                        26, 27)) != 9L) {
                                    prediosVo.add(new FichaMatrizPredioVO(p));
                                }
                            }
                            pn.setPredios(prediosVo);
                        }
                        t.setFichaMatrizPisos(pisosNuevos);
                        //---------------------------------------------
                        // SOTANOS
                        //---------------------------------------------
                        List<FichaMatrizPredioVO> sotanos = new ArrayList<FichaMatrizPredioVO>();
                        for (FichaMatrizPredio p : fichaMatriz.getFichaMatrizPredios()) {
                            if ((Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                getTorre()) &&
                                (Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 9L) ||
                                (Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 8L)) {
                                sotanos.add(new FichaMatrizPredioVO(p));
                            }
                        }
                        if (!sotanos.isEmpty()) {
                            //Se calcula el numero de 'Pisos' en los sotanos
                            Long numSotanos = new Long(0);
                            for (FichaMatrizPredioVO s : sotanos) {
                                if (100 - Long.parseLong(s.getNumeroPredial().substring(24, 26)) >
                                    numSotanos) {
                                    numSotanos = 100 - Long.parseLong(s.getNumeroPredial().
                                        substring(24, 26));
                                }
                            }
                            List<FichaMatrizSotanoVO> sotanosTorre =
                                new ArrayList<FichaMatrizSotanoVO>();
                            for (int i = 0; i < numSotanos; i++) {
                                FichaMatrizSotanoVO sota = new FichaMatrizSotanoVO();
                                sota.setNumero("S" + (i + 1));
                                sotanosTorre.add(sota);
                            }
                            //Se asignan los predios a los respectivos sotanos
                            for (FichaMatrizSotanoVO soT : sotanosTorre) {
                                List<FichaMatrizPredioVO> soNuevo =
                                    new ArrayList<FichaMatrizPredioVO>();
                                for (FichaMatrizPredioVO pre : sotanos) {
                                    if (pre.getNumeroPredial().substring(24, 26).equals(soT.
                                        getNumero().substring(1))) {
                                        soNuevo.add(pre);
                                    }
                                }
                                if (!soNuevo.isEmpty()) {
                                    soT.setPredios(soNuevo);
                                }
                            }
                            t.setFichaMatrizSotanos(sotanosTorre);
                        }
                    }
                }
                this.fichaMatrizTorres = torres;
            }

            //Llena los fichaMatrizPredios cuando no existen torres
            if ((fichaMatriz.getFichaMatrizTorres() == null ||
                fichaMatriz.getFichaMatrizTorres().isEmpty()) && (fichaMatriz.
                getFichaMatrizPredios() != null &&
                !fichaMatriz.getFichaMatrizPredios().isEmpty())) {

                List<FichaMatrizPredioVO> predios = new ArrayList<FichaMatrizPredioVO>();
                for (FichaMatrizPredio p : fichaMatriz.getFichaMatrizPredios()) {
                    predios.add(new FichaMatrizPredioVO(p));
                }
                this.fichaMatrizPredios = predios;
            }
            if (fichaMatriz.getFichaMatrizPredioTerrenos() != null && !fichaMatriz.
                getFichaMatrizPredioTerrenos().isEmpty()) {

                List<FichaMatrizPredioTerrenoVO> prediosTerreno =
                    new ArrayList<FichaMatrizPredioTerrenoVO>();
                for (FichaMatrizPredioTerreno p : fichaMatriz.getFichaMatrizPredioTerrenos()) {
                    prediosTerreno.add(new FichaMatrizPredioTerrenoVO(p));
                }
                this.fichaMatrizPrediosTerreno = prediosTerreno;

            }
        }
    }

    /** Constructor a partir de una PFichaMatriz */
    public FichaMatrizVO(PFichaMatriz fichaMatriz) {
        this.id = fichaMatriz.getId();
        this.cancelaInscribe = fichaMatriz.getCancelaInscribe();
        this.areaTotalConstruidaComun = fichaMatriz.getAreaTotalConstruidaComun();
        this.areaTotalConstruidaPrivada = fichaMatriz.getAreaTotalConstruidaPrivada();
        this.areaTotalTerrenoComun = fichaMatriz.getAreaTotalTerrenoComun();
        this.areaTotalTerrenoPrivada = fichaMatriz.getAreaTotalTerrenoPrivada();
        this.totalUnidadesPrivadas = fichaMatriz.getTotalUnidadesPrivadas();
        this.totalUnidadesSotanos = fichaMatriz.getTotalUnidadesSotanos();
        this.valorTotalAvaluoCatastral = fichaMatriz.getValorTotalAvaluoCatastral();

        //Se adiciona caso de ficha mixta
        if (fichaMatriz.getPPredio().getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_1.
            getCodigo())) {

            if (fichaMatriz.getPFichaMatrizModelos() != null && !fichaMatriz.
                getPFichaMatrizModelos().isEmpty()) {
                List<FichaMatrizModeloVO> modelos = new ArrayList<FichaMatrizModeloVO>();
                for (PFichaMatrizModelo m : fichaMatriz.getPFichaMatrizModelos()) {
                    modelos.add(new FichaMatrizModeloVO(m));
                }
                this.fichaMatrizModelos = modelos;
            }

            //Llena los datos de las torres, ubicando a los predios dentro de la torre correspondiente
            if (fichaMatriz.getPFichaMatrizTorres() != null && !fichaMatriz.getPFichaMatrizTorres().
                isEmpty()) {
                List<FichaMatrizTorreVO> torres = new ArrayList<FichaMatrizTorreVO>();
                List<Long> numtorres = new ArrayList<Long>();

                for (PFichaMatrizTorre t : fichaMatriz.getPFichaMatrizTorres()) {
                    torres.add(new FichaMatrizTorreVO(t));
                    numtorres.add(t.getTorre());
                }

                Collections.sort(numtorres);

                if (fichaMatriz.getPFichaMatrizPredios() != null && !fichaMatriz.
                    getPFichaMatrizPredios().isEmpty()) {
                    //Almacena los predios segun sus torres
                    for (FichaMatrizTorreVO t : torres) {
                        List<FichaMatrizPisoVO> pisosNuevos = new ArrayList<FichaMatrizPisoVO>();
                        for (int i = 1; i <= t.getPisos(); i++) {
                            FichaMatrizPisoVO pisoNuevo = new FichaMatrizPisoVO();
                            pisoNuevo.setNumero(new Long(i));
                            pisosNuevos.add(pisoNuevo);
                        }
                        for (FichaMatrizPisoVO pn : pisosNuevos) {
                            List<FichaMatrizPredioVO> prediosVo =
                                new ArrayList<FichaMatrizPredioVO>();
                            for (PFichaMatrizPredio p : fichaMatriz.getPFichaMatrizPredios()) {
                                if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                    getTorre() &&
                                    Long.parseLong(p.getNumeroPredial().substring(24, 26)) == pn.
                                    getNumero() &&
                                    Long.parseLong(p.getNumeroPredial().substring(26, 27)) != 9L) {
                                    prediosVo.add(new FichaMatrizPredioVO(p));
                                }
                            }
                            pn.setPredios(prediosVo);
                        }
                        t.setFichaMatrizPisos(pisosNuevos);
                        //---------------------------------------------
                        // SOTANOS
                        //---------------------------------------------
                        List<FichaMatrizPredioVO> sotanos = new ArrayList<FichaMatrizPredioVO>();
                        for (PFichaMatrizPredio p : fichaMatriz.getPFichaMatrizPredios()) {
                            if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                getTorre() &&
                                (Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 9 ||
                                Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 8)) {
                                sotanos.add(new FichaMatrizPredioVO(p));
                            }
                        }
                        if (!sotanos.isEmpty()) {
                            //Se calcula el numero de 'Pisos' en los sotanos
                            Long numSotanos = new Long(0);
                            for (FichaMatrizPredioVO s : sotanos) {

                                if (100 - Long.parseLong(s.getNumeroPredial().substring(24, 26)) >
                                    numSotanos) {
                                    numSotanos = 100 - Long.parseLong(s.getNumeroPredial().
                                        substring(24, 26));
                                }
                            }
                            List<FichaMatrizSotanoVO> sotanosTorre =
                                new ArrayList<FichaMatrizSotanoVO>();
                            for (int i = 0; i < numSotanos; i++) {
                                FichaMatrizSotanoVO sota = new FichaMatrizSotanoVO();
                                sota.setNumero("S" + (i + 1));
                                sotanosTorre.add(sota);
                            }
                            //Se asignan los predios a los respectivos sotanos
                            for (FichaMatrizSotanoVO soT : sotanosTorre) {
                                List<FichaMatrizPredioVO> soNuevo =
                                    new ArrayList<FichaMatrizPredioVO>();
                                for (FichaMatrizPredioVO pre : sotanos) {
                                    if (100 - Long.parseLong(pre.getNumeroPredial().
                                        substring(24, 26)) == Long.parseLong(soT.getNumero().
                                        substring(1))) {
                                        soNuevo.add(pre);
                                    }
                                }
                                if (!soNuevo.isEmpty()) {
                                    soT.setPredios(soNuevo);
                                }
                            }
                            t.setFichaMatrizSotanos(sotanosTorre);
                        }
                    }
                }
                this.fichaMatrizTorres = torres;
            }

            if (fichaMatriz.getPFichaMatrizPredios() != null && !fichaMatriz.
                getPFichaMatrizPredios().isEmpty()) {

                List<FichaMatrizPredioVO> predios = new ArrayList<FichaMatrizPredioVO>();
                for (PFichaMatrizPredio p : fichaMatriz.getPFichaMatrizPredios()) {
                    predios.add(new FichaMatrizPredioVO(p));
                }
                this.fichaMatrizPredios = predios;
            }

            if (fichaMatriz.getPFichaMatrizPredioTerrenos() != null && !fichaMatriz.
                getPFichaMatrizPredioTerrenos().isEmpty()) {

                List<FichaMatrizPredioTerrenoVO> prediosTerreno =
                    new ArrayList<FichaMatrizPredioTerrenoVO>();
                for (PFichaMatrizPredioTerreno p : fichaMatriz.getPFichaMatrizPredioTerrenos()) {
                    prediosTerreno.add(new FichaMatrizPredioTerrenoVO(p));
                }
                this.fichaMatrizPrediosTerreno = prediosTerreno;

            }

        } else {

            if (fichaMatriz.getPFichaMatrizModelos() != null && !fichaMatriz.
                getPFichaMatrizModelos().isEmpty()) {
                List<FichaMatrizModeloVO> modelos = new ArrayList<FichaMatrizModeloVO>();
                for (PFichaMatrizModelo m : fichaMatriz.getPFichaMatrizModelos()) {
                    modelos.add(new FichaMatrizModeloVO(m));
                }
                this.fichaMatrizModelos = modelos;
            }

            //Llena los datos de las torres, ubicando a los predios dentro de la torre correspondiente
            if (fichaMatriz.getPFichaMatrizTorres() != null && !fichaMatriz.getPFichaMatrizTorres().
                isEmpty()) {

                List<FichaMatrizTorreVO> torres = new ArrayList<FichaMatrizTorreVO>();
                List<Long> numtorres = new ArrayList<Long>();

                for (PFichaMatrizTorre t : fichaMatriz.getPFichaMatrizTorres()) {
                    torres.add(new FichaMatrizTorreVO(t));
                    numtorres.add(t.getTorre());
                }

                Collections.sort(numtorres);

                if (fichaMatriz.getPFichaMatrizPredios() != null && !fichaMatriz.
                    getPFichaMatrizPredios().isEmpty()) {
                    //Almacena los predios segun sus torres
                    for (FichaMatrizTorreVO t : torres) {
                        List<FichaMatrizPisoVO> pisosNuevos = new ArrayList<FichaMatrizPisoVO>();
                        for (int i = 1; i <= t.getPisos(); i++) {
                            FichaMatrizPisoVO pisoNuevo = new FichaMatrizPisoVO();
                            pisoNuevo.setNumero(new Long(i));
                            pisosNuevos.add(pisoNuevo);
                        }
                        for (FichaMatrizPisoVO pn : pisosNuevos) {
                            List<FichaMatrizPredioVO> prediosVo =
                                new ArrayList<FichaMatrizPredioVO>();
                            for (PFichaMatrizPredio p : fichaMatriz.getPFichaMatrizPredios()) {
                                if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                    getTorre() &&
                                    Long.parseLong(p.getNumeroPredial().substring(24, 26)) == pn.
                                    getNumero() &&
                                    Long.parseLong(p.getNumeroPredial().substring(26, 27)) != 9L) {
                                    prediosVo.add(new FichaMatrizPredioVO(p));
                                }
                            }
                            pn.setPredios(prediosVo);
                        }
                        t.setFichaMatrizPisos(pisosNuevos);
                        //---------------------------------------------
                        // SOTANOS
                        //---------------------------------------------
                        List<FichaMatrizPredioVO> sotanos = new ArrayList<FichaMatrizPredioVO>();
                        for (PFichaMatrizPredio p : fichaMatriz.getPFichaMatrizPredios()) {
                            if (Long.parseLong(p.getNumeroPredial().substring(22, 24)) == t.
                                getTorre() &&
                                (Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 9 ||
                                Long.parseLong(p.getNumeroPredial().substring(24, 25)) == 8)) {
                                sotanos.add(new FichaMatrizPredioVO(p));
                            }
                        }
                        if (!sotanos.isEmpty()) {
                            //Se calcula el numero de 'Pisos' en los sotanos
                            Long numSotanos = new Long(0);
                            for (FichaMatrizPredioVO s : sotanos) {

                                if (100 - Long.parseLong(s.getNumeroPredial().substring(24, 26)) >
                                    numSotanos) {
                                    numSotanos = 100 - Long.parseLong(s.getNumeroPredial().
                                        substring(24, 26));
                                }
                            }
                            List<FichaMatrizSotanoVO> sotanosTorre =
                                new ArrayList<FichaMatrizSotanoVO>();
                            for (int i = 0; i < numSotanos; i++) {
                                FichaMatrizSotanoVO sota = new FichaMatrizSotanoVO();
                                sota.setNumero("S" + (i + 1));
                                sotanosTorre.add(sota);
                            }
                            //Se asignan los predios a los respectivos sotanos
                            for (FichaMatrizSotanoVO soT : sotanosTorre) {
                                List<FichaMatrizPredioVO> soNuevo =
                                    new ArrayList<FichaMatrizPredioVO>();
                                for (FichaMatrizPredioVO pre : sotanos) {
                                    if (100 - Long.parseLong(pre.getNumeroPredial().
                                        substring(24, 26)) == Long.parseLong(soT.getNumero().
                                        substring(1))) {
                                        soNuevo.add(pre);
                                    }
                                }
                                if (!soNuevo.isEmpty()) {
                                    soT.setPredios(soNuevo);
                                }
                            }
                            t.setFichaMatrizSotanos(sotanosTorre);
                        }
                    }
                }
                this.fichaMatrizTorres = torres;
            }

            //Llena los fichaMatrizPredios cuando no existen torres
            if ((fichaMatriz.getPFichaMatrizTorres() == null || fichaMatriz.getPFichaMatrizTorres().
                isEmpty()) &&
                (fichaMatriz.getPFichaMatrizPredios() != null &&
                !fichaMatriz.getPFichaMatrizPredios().isEmpty())) {

                List<FichaMatrizPredioVO> predios = new ArrayList<FichaMatrizPredioVO>();
                for (PFichaMatrizPredio p : fichaMatriz.getPFichaMatrizPredios()) {
                    predios.add(new FichaMatrizPredioVO(p));
                }
                this.fichaMatrizPredios = predios;
            }
            if (fichaMatriz.getPFichaMatrizPredioTerrenos() != null && !fichaMatriz.
                getPFichaMatrizPredioTerrenos().isEmpty()) {

                List<FichaMatrizPredioTerrenoVO> prediosTerreno =
                    new ArrayList<FichaMatrizPredioTerrenoVO>();
                for (PFichaMatrizPredioTerreno p : fichaMatriz.getPFichaMatrizPredioTerrenos()) {
                    prediosTerreno.add(new FichaMatrizPredioTerrenoVO(p));
                }
                this.fichaMatrizPrediosTerreno = prediosTerreno;

            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAreaTotalConstruidaComun() {
        return areaTotalConstruidaComun;
    }

    public void setAreaTotalConstruidaComun(Double areaTotalConstruidaComun) {
        this.areaTotalConstruidaComun = areaTotalConstruidaComun;
    }

    public Double getAreaTotalConstruidaPrivada() {
        return areaTotalConstruidaPrivada;
    }

    public void setAreaTotalConstruidaPrivada(Double areaTotalConstruidaPrivada) {
        this.areaTotalConstruidaPrivada = areaTotalConstruidaPrivada;
    }

    public Double getAreaTotalTerrenoComun() {
        return areaTotalTerrenoComun;
    }

    public void setAreaTotalTerrenoComun(Double areaTotalTerrenoComun) {
        this.areaTotalTerrenoComun = areaTotalTerrenoComun;
    }

    public Double getAreaTotalTerrenoPrivada() {
        return areaTotalTerrenoPrivada;
    }

    public void setAreaTotalTerrenoPrivada(Double areaTotalTerrenoPrivada) {
        this.areaTotalTerrenoPrivada = areaTotalTerrenoPrivada;
    }

    public Long getTotalUnidadesPrivadas() {
        return totalUnidadesPrivadas;
    }

    public void setTotalUnidadesPrivadas(Long totalUnidadesPrivadas) {
        this.totalUnidadesPrivadas = totalUnidadesPrivadas;
    }

    public Double getValorTotalAvaluoCatastral() {
        return valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    public Long getTotalUnidadesSotanos() {
        return totalUnidadesSotanos;
    }

    public void setTotalUnidadesSotanos(Long totalUnidadesSotanos) {
        this.totalUnidadesSotanos = totalUnidadesSotanos;
    }

    public List<FichaMatrizTorreVO> getFichaMatrizTorres() {
        return fichaMatrizTorres;
    }

    public void setFichaMatrizTorres(List<FichaMatrizTorreVO> fichaMatrizTorres) {
        this.fichaMatrizTorres = fichaMatrizTorres;
    }

    public List<FichaMatrizPredioVO> getFichaMatrizPredios() {
        return fichaMatrizPredios;
    }

    public void setFichaMatrizPredios(
        List<FichaMatrizPredioVO> fichaMatrizPredios) {
        this.fichaMatrizPredios = fichaMatrizPredios;
    }

    public List<FichaMatrizModeloVO> getFichaMatrizModelos() {
        return fichaMatrizModelos;
    }

    public void setFichaMatrizModelos(List<FichaMatrizModeloVO> fichaMatrizModelos) {
        this.fichaMatrizModelos = fichaMatrizModelos;
    }

    public String getCancelaInscribe() {
        return cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public List<FichaMatrizPredioTerrenoVO> getFichaMatrizPrediosTerreno() {
        return fichaMatrizPrediosTerreno;
    }

    public void setFichaMatrizPrediosTerreno(
        List<FichaMatrizPredioTerrenoVO> fichaMatrizPrediosTerreno) {
        this.fichaMatrizPrediosTerreno = fichaMatrizPrediosTerreno;
    }
}
