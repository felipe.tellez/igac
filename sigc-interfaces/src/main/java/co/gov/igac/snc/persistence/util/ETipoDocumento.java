package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para los Tipos de documentos. Las enumeraciones corresponden a los datos de la tabla
 * Tipo_Documento
 *
 * OJO: no se sabe porqué el autor no hizo coincidir el nombre de la enumeración con el dato de la
 * columna 'nombre' de la tabla.
 *
 * @author david.cifuentes
 */
/*
 * @modified juan.agudelo @documented pedro.garcia @modified leidy.gonzalez # 13223 se agrega:
 * FIRMA_MECANICA_USUARIO
 */
public enum ETipoDocumento {

    ACUERDO_DE_BARRIOS(Long.valueOf(74), "Acuerdo_de_barrios"),
    ACUERDO_DE_NOMENCLATURA_VIAL(Long.valueOf(72), "Acuerdo_de_nomenclatura_vial"),
    ACUERDO_DE_PERIMETROS(Long.valueOf(75), "Acuerdo_de_perímetros"),
    ACUERDO_DE_VEREDAS(Long.valueOf(76), "Acuerdo_de_veredas"),
    APROBAR_SOLICITUD_DE_COMISION(Long.valueOf(3007), "Aprobar_solicitud_de_comision"),
    AREAS_HOMOGENEAS_DE_TIERRA(Long.valueOf(63), "Areas_homogéneas_de_tierra"),
    AUTO_DE_PRUEBAS_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA(Long.valueOf(3010),
        "Auto_de_pruebas_queja_apelacion_y_revocatoria_directa"),
    AUTO_DE_PRUEBAS_REVISION_AVALUO_AUTOESTIMACION_Y_REPOSICION(Long.valueOf(3011),
        "Auto_de_pruebas_revision_avaluo_autoestimacion_y_reposicion"),
    AVALUOS_ACTA_COMITE(Long.valueOf(1502), "Documento del acta de comité de avalúos"),
    AVALUOS_DOCUMENTO_LIQUIDACION_COSTOS(Long.valueOf(1501),
        "Documento de liquidación de costos avalúos"),
    AVALUOS_OFICIO_NO_APROBACION(Long.valueOf(1500), "Oficio de no aprobación de avalúo"),
    CARTAS_CATASTRALES_ANALOGAS(Long.valueOf(79), "Cartas_catastrales_análogas"),
    CARTA_CATASTRAL_URBANA(Long.valueOf(107), "Carta_catastral_urbana"),
    CARGUE_PREDIOS_AVALUOS(Long.valueOf(87), "Documento asociado al cargue de predios de avalúos"),
    CERTIFICADO_DE_LIBERTAD_Y_TRADICION_ORIGINAL_O_COPIA(Long.valueOf(6),
        "Certificado_de_libertad_y_tradicion(original_o_copia)"),
    CERTIFICADO_DE_NOMENCLATURA(Long.valueOf(18), "Certificado_de_Nomenclatura"),
    CERTIFICADO_PLANO_PREDIAL_CATASTRAL(Long.valueOf(106), "Ficha_predial_digital"),
    COMUNAS(Long.valueOf(77), "Comunas"),
    COMUNICACION_AUTO_DE_PRUEBAS(Long.valueOf(85), "Comunicación_auto_de_pruebas"),
    COMUNICACION_DE_NOTIFICACION(Long.valueOf(36), "Comunicación_de_notificación"),
    COMUNICACION_DE_PREDIOS_DESACTUALIZADOS(Long.valueOf(55),
        "Comunicación_de_predios_desactualizados"),
    COMUNICADO_CANCELACION_TRAMITE_CATASTRAL(Long.valueOf(2007),
        "Oficio_cancelación_trámite_catastral"),
    CONSTANCIA_DE_RADICACION(Long.valueOf(3001), "Constancia_de_radicacion"),
    CONVENIO_PARA_ACTUALIZACION(Long.valueOf(105), "Convenio para Actualizacion"),
    COPIA_BD_GEOGRAFICA_ORIGINAL(Long.valueOf(97), "CopiaBDGeograficaOriginal"),
    COPIA_BD_GEOGRAFICA_FINAL(Long.valueOf(98), "CopiaBDGeograficaFinal"),
    COPIA_BD_GEOGRAFICA_ORIGINAL_DEPURACION(Long.valueOf(109), "CopiaBDGeograficaOriginalDepuracion"),
    COPIA_BD_GEOGRAFICA_FINAL_DEPURACION(Long.valueOf(110), "CopiaBDGeograficaFinalDepuracion"),
    COPIA_BD_GEOGRAFICA_CONSULTA(Long.valueOf(2000), "Copia BD geográfica consulta"),
    COPIA_CARTOGRAFIA_POT(Long.valueOf(69), "Copia_de_cartografía_POT"),
    COPIA_ACUERDO_POT(Long.valueOf(70), "Copia_del_acuerdo_del_POT"),
    COPIA_PLANES_PARCIALES(Long.valueOf(71), "Copia_planes_parciales"),
    DECLARACION_DE_CONSTRUCCION(Long.valueOf(16), "Declaracion_de_Construccion"),
    DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION(Long.valueOf(40),
        "Documento_de_autorización_de_notificación"),
    DOCUMENTO_DE_NOTIFICACION(Long.valueOf(39), "Documento_de_notificación"),
    DOCUMENTO_DONDE_SE_ESPECIFIQUE_EL_AREA_DE_CONSTRUCCION_CON_SU_RESPECTIVO_VALOR_POR_METRO_CUADRADO(
        Long.valueOf(26),
        "Documento_donde_se_especifique_el_Area_de_Construcción,_con_su_respectivo_valor_por_metro_cuadrado"),
    DOCUMENTO_DONDE_SE_ESPECIFIQUE_EL_AREA_DE_TERRENO_CON_SU_RESPECTIVO_VALOR_POR_METRO_CUADRADO(
        Long.valueOf(25),
        "Documento_donde_se_especifique_el_Area_de_terreno,_con_su_respectivo_valor_por_metro_cuadrado"),
    DOCUMENTO_QUE_SUSTENTE_LA_SOLICITUD(Long.valueOf(27), "Documento_que_sustente_la_solicitud"),
    DOCUMENTO_RESULTADO_DE_PRUEBAS(Long.valueOf(32), "Documento_resultado_de_pruebas"),
    DOCUMENTO_RESULTADO_DE_ACTUALIZACION(Long.valueOf(3111), "Documento_Actualización"),
    DOCUMENTO_SOPORTE_DE_BLOQUEO_MASIVO_DE_PERSONA(Long.valueOf(48),
        "Documento_soporte_de_bloqueo_masivo_de_persona"),
    DOCUMENTO_SOPORTE_DE_BLOQUEO_MASIVO_DE_PREDIO(Long.valueOf(47),
        "Documento_soporte_de_bloqueo_masivo_de_predio"),
    DOCUMENTO_SOPORTE_DE_BLOQUEO_PERSONA(Long.valueOf(46), "Documento_soporte_de_bloqueo_persona"),
    DOCUMENTO_SOPORTE_DE_BLOQUEO_PREDIO(Long.valueOf(45), "Documento_soporte_de_bloqueo_predio"),
    DOCUMENTO_SOPORTE_DE_CANCELACION_DE_TRAMITE(Long.valueOf(54),
        "Documento_soporte_de_cancelación_de_trámite"),
    DOCUMENTO_SOPORTE_DE_DESBLOQUEO_PERSONA(Long.valueOf(44),
        "Documento_soporte_de_desbloqueo_persona"),
    DOCUMENTO_SOPORTE_DE_DESBLOQUEO_PREDIO(Long.valueOf(43),
        "Documento_soporte_de_desbloqueo_predio"),
    DOCUMENTO_SOPORTE_DE_AMPLIACION_TIEMPOS(Long.valueOf(99),
        "Documento_soporte_de_ampliacion_tiempos"),
    EDICTO_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA(Long.valueOf(3019),
        "Edicto_queja_apelacion_y_revocatoria_directa"),
    EDICTO_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_Y_REPOSICION(Long.valueOf(3020),
        "edicto_tramites_catastrales_revision_de_avaluo_autoavaluo_y_reposicion"),
    ESCRITURAS_FOLIOS_BASES_DE_DATOS(Long.valueOf(81),
        "Escrituras_/_Folios_/_Bases_de_datos_registrales"),
    ESTUDIO_DETALLADO_DE_SUELOS(Long.valueOf(64), "Estudio_detallado_de_suelos"),
    FICHA_PREDIAL_DIGITAL(Long.valueOf(101), "Ficha_Predial_Digital"),
    FICHA_PREDIAL_ESCANEADA(Long.valueOf(100), "Ficha_Predial_Escaneada"),
    FICHA_PREDIAL_ESCANEADA_ANEXO_PLANO(Long.valueOf(108), "Ficha_Predial_Escaneada_Anexo"),
    FOTOCOPIA_DE_ESCRITURA_PUBLICA(Long.valueOf(10), "Fotocopia_de_escritura_Publica"),
    FOTOCOPIA_DE_LA_CEDULA(Long.valueOf(5), "Fotocopia_de_la_cédula"),
    FOTOCOPIA_DE_LA_ESCRITURA_PUBLICA_AJUSTADA(Long.valueOf(30),
        "Fotocopia_de_la_Escritura_Pública_Ajustada"),
    FOTOCOPIA_DE_LA_ESCRITURA_PUBLICA_DONDE_SE_INCLUYE_EL_REGIMEN_DE_PROPIEDAD_HORIZONTAL(Long.
        valueOf(23),
        "Fotocopia_de_la_Escritura_Pública_donde_se_incluye_el_régimen_de_propiedad_Horizontal"),
    FOTOCOPIA_DE_LA_ESCRITURA_PUBLICA_O_CERTIFICADO_DE_LIBERTAD_ORIGINAL_O_COPIA(Long.valueOf(22),
        "Fotocopia_de_la_escritura_pública_o_Certificado_de_Libertad_Original_o_Copia"),
    FOTOCOPIA_DEL_CERTIFICADO_DE_EXISTENCIA_Y_REPRESENTACION_LEGAL15(Long.valueOf(15),
        "Fotocopia_del_Certificado_de_existencia_y_representacion_legal15"),
    FOTOGRAFIA(Long.valueOf(9), "Fotografia"),
    FOTOGRAFIAS_AEREAS_CON_INDICES_DE_VUELO(Long.valueOf(67),
        "Fotografías_aéreas_con_índices_de_vuelo"),
    HISTORICO_MIGRADO(Long.valueOf(38), "Histórico_migrado"),
    IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE(Long.valueOf(34),
        "Imagen_geográfica_del_predio_antes_del_trámite"),
    IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE(Long.valueOf(35),
        "Imagen_geográfica_del_predio_despues_del_trámite"),
    INFORME_EVOLUCION_MANZANAS_CATASTRALES(Long.valueOf(78),
        "Informe_de_evolución_de_manzanas_catastrales_vigentes"),
    INFORME_VISITA_REVISION_O_AUTOAVALUO(Long.valueOf(96),
        "Informe_de_visita_de_revisión_o_autoavalúo"),
    INSUMOS_AGROLOGICOS(Long.valueOf(61), "Insumos_agrológicos"),
    INSUMOS_CARTOGRAFICOS(Long.valueOf(60), "Insumos_cartográficos"),
    INVENTARIO_EQUIPOS_Y_VERIFICACION_CALIBRACION(Long.valueOf(68),
        "Inventario_de_equipos_y_verificación_de_calibración"),
    LIMITE_MUNICIPAL_OFICIAL(Long.valueOf(56), "Límite_municipal_oficial"),
    MEMORANDO_CREACION_COMISION(Long.valueOf(84), "Memorando_de_creación_de_comisión"),
    MEMORANDO_DE_AMPLIACION_DE_COMISION(Long.valueOf(53), "Memorando_de_ampliación_de_comisión"),
    MEMORANDO_DE_APLAZAMIENTO_DE_COMISION(Long.valueOf(51), "Memorando_de_aplazamiento_de_comisión"),
    MEMORANDO_DE_APROBACION_DE_COMISION(Long.valueOf(3003), "Memorando de aprobación de comisión"),
    MEMORANDO_DE_CANCELACION_DE_COMISION(Long.valueOf(50), "Memorando_de_cancelación_de_comisión"),
    MEMORANDO_PARA_RETIRAR_TRAMITE_DE_COMISION(Long.valueOf(3006),
        "Memorando_para_retirar_tramite_de_comisión"),
    MEMORANDO_DE_REACTIVACION_DE_COMISION(Long.valueOf(52), "Memorando_de_reactivación_de_comisión"),
    MEMORANDO_DE_ORDEN_DE_PRACTICA(Long.valueOf(102), "Memorando de orden de práctica"),
    MEMORANDO_DE_SUSPENSION_DE_COMISION(Long.valueOf(49), "Memorando_de_suspensión_de_comisión"),
    MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC(Long.valueOf(3013),
        "Memorando_solicitud_de_pruebas_a_oficina_interna_igac"),
    NOTIFICACION(Long.valueOf(3018), "Notificacion"),
    OFICIO_COMUNICACION_DE_NOTIFICACION(Long.valueOf(3017), "oficio_comunicacion_de_notificacion"),
    OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS(Long.valueOf(3014),
        "Oficio_solicitud_de_pruebas_a_solicitantes_y_afectados"),
    OFICIO_NO_PROCEDENCIA_AUTOESTIMACION_Y_REVISION_DE_AVALUO(Long.valueOf(3002),
        "Oficion_no_procedencia_autoestimacion_y_revision_avaluo"),
    OFICIO_NO_PROCEDENCIA_MUTACIONES(Long.valueOf(3026), "Oficion_no_procedencia_mutaciones"),
    OFICIO_NO_PROCEDENCIA_VIA_GUBERNATIVA(Long.valueOf(3004), "Oficion_no_procedencia_gubernativa"),
    OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA(Long.valueOf(3012),
        "Oficio_solicitud_de_pruebas_a_entidad_externa"),
    OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES(Long.valueOf(3009),
        "Oficio_solicitud_documentos_faltantes_revision_avaluo_y_mutaciones"),
    OFICIOS_REMISORIOS(Long.valueOf(3), "Oficios_Remisorios"),
    ORTOFOTOS(Long.valueOf(65), "Ortofotos"),
    OTRAS_IMAGENES(Long.valueOf(66), "Otras_imágenes"),
    OTRO(Long.valueOf(7), "Otro"),
    PLANO_CONJUNTO_CABECERAS_Y_CORREGIMIENTOS(Long.valueOf(73),
        "Plano_de_conjunto,_cabeceras_y_corregimientos"),
    PROYECTO_GESTION_PERIMETRO_URBANO(Long.valueOf(103), "Proyecto_gestion_perimetro_urbano"),
    PUNTOS_GEODESICOS_DEL_DEPARTAMENTO(Long.valueOf(62), "Puntos_geodésicos_del_departamento"),
    RADICAR_RECURSO(Long.valueOf(3039), "Registro de radicación del recurso"),
    RECHAZO_DE_COMISION(Long.valueOf(3008), "Rechazo_de_comision"),
    RESOLUCION_DE_ACTUALIZACION_DE_LA_FORMACION_CATASTRAL(Long.valueOf(42),
        "Resolución_de_Actualización_de_la_Formación_Catastral"),
    RESOLUCION_DE_CONSERVACION(Long.valueOf(8), "Resolucion_de_Conservación"),
    RESOLUCION_MIGRADA(Long.valueOf(1), "Resolución_migrada"),
    RESOLUCION_PRUEBA(Long.valueOf(33), "Resolucion_Prueba"),
    RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA(Long.valueOf(3016),
        "Resoluciones_de_conservacion_queja_apelacion_revocatoria_directa"),
    RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION(Long.
        valueOf(3015),
        "Resoluciones_de_conservacion_tramites_catastrales_revision_de_avaluo_autoavaluo_reposicion"),
    SOLICITUD_DOCUMENTOS_CATASTRALES(Long.valueOf(31), "Solicitud_de_Documentos_Catastrales"),
    SOLICITUD_ESCRITA_VERBAL_O_POR_CORREO(Long.valueOf(20), "Solicitud_Escrita,_verbal_o_por_correo"),
    SOLICITUD_ESCRITA_CON_SOPORTES(Long.valueOf(29), "Solicitud_Escrita_con_soportes"),
    SOLICITUD_ESCRITA_POR_CORREO_CERTIFICADO(Long.valueOf(24),
        "Solicitud_escrita_por_correo_certificado"),
    STICKERS_DEL_PROCESO(Long.valueOf(80), "Stickers_del_proceso"),
    COMUNICACION_DE_CANCELACION_CREACION_TRAMITE(Long.valueOf(3040),
        "Comunicación_de_cancelación_creación_de_trámite"),
    DOC_MASIVO_PRODUCTO_PREDIO(Long.valueOf(3005), "Doc_masivo_producto_predio"),
    DOC_MASIVO_PRODUCTO_PERSONA(Long.valueOf(3105), "Doc_masivo_producto_persona"),
    FIRMA_MECANICA_USUARIO(Long.valueOf(3106), "Firma_Mecanica_Usuario"),
    REPORTE_INCONSISTENCIAS_ACTUALIZACION(Long.valueOf(3109),
        "Reporte_Inconsistencias_Actualizacion"),
    REPORTE_RADICACION_ACTUALIZACION(Long.valueOf(3110), "Reporte_Radicacion_Actualizacion"),
    RESOLUCIONES_DE_ACTUALIZACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION(
        Long.valueOf(3107),
        "Resoluciones_de_actualizacion_tramites_catastrales_revision_de_avaluo_autoavaluo_reposicion"),
    RESOLUCIONES_DE_ACTUALIZACION_QUEJA_APELACION_REVOCATORIA_DIRECTA(Long.valueOf(3108),
        "Resoluciones_de_actualizacion_queja_apelacion_revocatoria_directa"),
    REPORTE_INCONSISTENCIAS_CARGUE_CICA(Long.valueOf(3200),
            "Reporte inconsistencias cargue cica"),
    LISTADO_PREDIOS_CARGUE_CICA(Long.valueOf(3201),
            "Listado predios cargue cica"),
    DOC_BD_GEOGRAFICA_ACTUALIZACION(Long.valueOf(3202),
            "Archivo BD geografica actualizacion"),
    REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA(Long.valueOf(3203),
            "Archivo resultante validaciones geográficas actualización")
    ;

    private Long id;
    private String nombre;

    private ETipoDocumento(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    /**
     * retorna el id del tipo de documento
     *
     * @return
     */
    /*
     * @documented pedro.garcia
     */
    public Long getId() {
        return this.id;
    }

    /**
     * retorna el nombre del tipo de documento
     *
     * @return
     */
    /*
     * @documented pedro.garcia
     */
    public String getNombre() {
        return this.nombre;
    }

}
