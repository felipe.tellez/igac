/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con datos de la tabla antes despues para el panel de resumen correspondiente a
 * ubicación de predio
 *
 * @author juan.agudelo
 */
/*
 * @modified pedro.garcia 16-08-2012 Ordenado según estándar
 */
public enum EDatosPanelUbicacionPredio {

    BARRIO_VEREDA("Barrio / vereda"),
    CONSECUTIVO_CATASTRAL("Consecutivo catastral"),
    CORREGIMIENTO("Corregimiento"),
    DESTINO_PREDIO("Destino del predio"),
    ESTADO_PREDIO("Estado del predio"),
    DEPARTAMENTO("Departamento"),
    LOCALIDAD_COMUNA("Localidad / comuna"),
    MATRICULA_INMOBILIARIA("Matrícula inmobiliaria"),
    MUNICIPIO("Municipio"),
    NUMERO_PREDIAL("Número predial"),
    NIP("NIP"),
    NUMERO_PREDIAL_ANTERIOR("Número predial anterior"),
    PREDIO_LEY_14("Predio ley 14"),
    TIPO_PREDIO("Tipo de predio");

    private String dato;

    private EDatosPanelUbicacionPredio(String dato) {
        this.dato = dato;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String id) {
        this.dato = id;
    }

}
