package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ZONA_CIERRE_VIGENCIA database table.
 *
 */
@Entity
@Table(name = "ZONA_CIERRE_VIGENCIA", schema = "SNC_CONSERVACION")
public class ZonaCierreVigencia implements Serializable {

    private static final long serialVersionUID = -4780783027540409838L;

    private Long id;
    private String estadoCierre;
    private Date fechaFinActualizacion;
    private Date fechaFinCierre;
    private Date fechaFinFormacion;
    private Date fechaInicioActualizacion;
    private Date fechaInicioCierre;
    private Date fechaInicioFormacion;
    private Date fechaLog;
    private Long resolucionActualiacionId;
    private Long resolucionFormacionId;
    private String usuarioLog;
    private String zonaCodigo;
    private MunicipioCierreVigencia municipioCierreVigencia;

    public ZonaCierreVigencia() {
    }

    @Id
    @Column(unique = true, nullable = false, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZONA_CIERRE_VIGENCIA_ID_SEQ")
    @SequenceGenerator(name = "ZONA_CIERRE_VIGENCIA_ID_SEQ", sequenceName =
        "ZONA_CIERRE_VIGENCIA_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ESTADO_CIERRE", length = 50)
    public String getEstadoCierre() {
        return this.estadoCierre;
    }

    public void setEstadoCierre(String estadoCierre) {
        this.estadoCierre = estadoCierre;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_ACTUALIZACION")
    public Date getFechaFinActualizacion() {
        return this.fechaFinActualizacion;
    }

    public void setFechaFinActualizacion(Date fechaFinActualizacion) {
        this.fechaFinActualizacion = fechaFinActualizacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_CIERRE")
    public Date getFechaFinCierre() {
        return this.fechaFinCierre;
    }

    public void setFechaFinCierre(Date fechaFinCierre) {
        this.fechaFinCierre = fechaFinCierre;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_FORMACION")
    public Date getFechaFinFormacion() {
        return this.fechaFinFormacion;
    }

    public void setFechaFinFormacion(Date fechaFinFormacion) {
        this.fechaFinFormacion = fechaFinFormacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_ACTUALIZACION")
    public Date getFechaInicioActualizacion() {
        return this.fechaInicioActualizacion;
    }

    public void setFechaInicioActualizacion(Date fechaInicioActualizacion) {
        this.fechaInicioActualizacion = fechaInicioActualizacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_CIERRE")
    public Date getFechaInicioCierre() {
        return this.fechaInicioCierre;
    }

    public void setFechaInicioCierre(Date fechaInicioCierre) {
        this.fechaInicioCierre = fechaInicioCierre;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_FORMACION")
    public Date getFechaInicioFormacion() {
        return this.fechaInicioFormacion;
    }

    public void setFechaInicioFormacion(Date fechaInicioFormacion) {
        this.fechaInicioFormacion = fechaInicioFormacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "RESOLUCION_ACTUALIACION_ID", precision = 10, scale = 2)
    public Long getResolucionActualiacionId() {
        return this.resolucionActualiacionId;
    }

    public void setResolucionActualiacionId(Long resolucionActualiacionId) {
        this.resolucionActualiacionId = resolucionActualiacionId;
    }

    @Column(name = "RESOLUCION_FORMACION_ID", precision = 10)
    public Long getResolucionFormacionId() {
        return this.resolucionFormacionId;
    }

    public void setResolucionFormacionId(Long resolucionFormacionId) {
        this.resolucionFormacionId = resolucionFormacionId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "ZONA_CODIGO", nullable = false, length = 7)
    public String getZonaCodigo() {
        return this.zonaCodigo;
    }

    public void setZonaCodigo(String zonaCodigo) {
        this.zonaCodigo = zonaCodigo;
    }

    // bi-directional many-to-one association to MunicipioCierreVigencia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CIERRE_VIGENCIA_ID", nullable = false)
    public MunicipioCierreVigencia getMunicipioCierreVigencia() {
        return this.municipioCierreVigencia;
    }

    public void setMunicipioCierreVigencia(
        MunicipioCierreVigencia municipioCierreVigencia) {
        this.municipioCierreVigencia = municipioCierreVigencia;
    }

}
